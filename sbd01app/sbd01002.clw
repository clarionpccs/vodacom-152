

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01002.INC'),ONCE        !Local module procedure declarations
                     END








Retail_Single_Invoice PROCEDURE(BYTE CopyInvoice,LONG fInvoiceNumber)
Use_Euro             BYTE
locInvoiceNumber     LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
RejectRecord         LONG,AUTO
tmp:CompanyName      STRING(30)
tmp:AddressLine1     STRING(30)
tmp:AddressLine2     STRING(30)
tmp:AddressLine3     STRING(30)
tmp:Postcode         STRING(30)
tmp:TelephoneNumber  STRING(30)
tmp:FaxNumber        STRING(30)
tmp:EmailAddress     STRING(255)
tmp:VATNumber        STRING(30)
tmp:PrintedBy        STRING(255)
tmp:InvoiceTitle     STRING(18)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
save_rtp_id          USHORT,AUTO
vat_rate_temp        REAL
Delivery_Name_Temp   STRING(30)
Delivery_Address_Line_1_Temp STRING(60)
save_res_id          USHORT,AUTO
Invoice_Name_temp    STRING(30)
Invoice_address_Line_1 STRING(60)
items_total_temp     REAL
vat_temp             REAL
item_cost_temp       REAL
line_cost_temp       REAL
despatch_note_number_temp STRING(10)
line_cost_total_temp REAL
vat_total_temp       REAL
Total_temp           REAL
payment_date_temp    DATE,DIM(4)
other_amount_temp    REAL
total_paid_temp      REAL
payment_type_temp    STRING(20),DIM(4)
payment_amount_temp  REAL,DIM(4)
sub_total_temp       REAL
courier_cost_temp    REAL
tmp:TotalExchangeValue REAL
tmp:ItemCost         REAL
Accessory_Cost       REAL
PartsList            QUEUE,PRE(tmpque)
OrderNumber          STRING(30),NAME('OrderNumber')
Quantity             LONG
PartNumber           STRING(60),NAME('PartNumber')
Description          STRING(30)
ExchangeValue        REAL
ItemCost             REAL
LineCost             REAL
                     END
tmp:AccountVatNumber STRING(30)
Code_Temp            BYTE,AUTO
Option_Temp          BYTE,AUTO
Bar_Code_String_Temp CSTRING(21),AUTO
Bar_Code_Temp        CSTRING(21),AUTO
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Invoice_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,4573,7521,4292),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(354,490,7521,10896),USE(?unnamed)
                         STRING('Date Printed:'),AT(5000,938),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5990,938),USE(ReportRunDate),TRN,LEFT,FONT('Arial',8,,FONT:bold)
                         STRING('Tel:'),AT(4063,2500),USE(?string22:4),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,781),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@s255),AT(5990,781),USE(tmp:PrintedBy),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(5010,0),USE(Bar_Code_Temp),TRN,RIGHT,FONT('C128 High 12pt LJ3',12,,,CHARSET:ANSI)
                         STRING(@s18),AT(3958,156,2344,313),USE(tmp:InvoiceTitle),TRN,RIGHT,FONT(,12,,FONT:bold)
                         STRING(@s8),AT(6354,156),USE(inv:Invoice_Number),TRN,RIGHT,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5990,469),USE(tmp:VATNumber),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('V.A.T. Number:'),AT(5000,469),USE(?string22:12),TRN,FONT(,8,,)
                         STRING(@n08),AT(5990,1094),USE(ret:Ref_Number),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@n3),AT(5990,1250),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6250,1250),USE(?string22:14),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6406,1250,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,1250),USE(?string22:13),TRN,FONT(,8,,)
                         STRING('Date Of Invoice:'),AT(5000,625),USE(?String65),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5990,625),USE(inv:Date_Created),TRN,FONT(,8,,FONT:bold)
                         STRING('Sale Number:'),AT(5000,1094),USE(?string22:10),TRN,FONT(,8,,)
                         STRING('Account No'),AT(208,3177),USE(?string22:6),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(208,3385),USE(ret:Account_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Purch Order No'),AT(1615,3177),USE(?string22:7),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name'),AT(3125,3177),USE(?string22:11),TRN,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(1615,3385),USE(ret:Purchase_Order_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s20),AT(3125,3385),USE(ret:Contact_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date'),AT(208,8698),USE(?String66),TRN,FONT(,7,,FONT:bold)
                         STRING('Type'),AT(708,8698),USE(?String67),TRN,FONT(,7,,FONT:bold)
                         STRING('Amount'),AT(1969,8698),USE(?String70:5),TRN,FONT(,7,,FONT:bold)
                         STRING(@d6b),AT(156,8854),USE(payment_date_temp[1]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,8854),USE(payment_type_temp[1]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,8854),USE(payment_amount_temp[1]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@d6b),AT(156,9167),USE(payment_date_temp[4]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,9167),USE(payment_type_temp[4]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,9167),USE(payment_amount_temp[4]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         TEXT,AT(2917,8750,1719,781),USE(ret:Invoice_Text),FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(156,8958),USE(payment_date_temp[2]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,8958),USE(payment_type_temp[2]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,8958),USE(payment_amount_temp[2]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@d6b),AT(156,9063),USE(payment_date_temp[3]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,9063),USE(payment_type_temp[3]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,9063),USE(payment_amount_temp[3]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('Others:'),AT(208,9323),USE(?Others),TRN,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1823,9323),USE(other_amount_temp),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         LINE,AT(208,9479,1979,0),USE(?Line4),COLOR(COLOR:Black)
                         STRING('Total:'),AT(208,9531),USE(?String109),TRN,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1823,9531),USE(total_paid_temp),TRN,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1719),USE(ret:Company_Name_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1719),USE(ret:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(4063,1875,3281,208),USE(Delivery_Address_Line_1_Temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(208,1875,3646,208),USE(Invoice_address_Line_1),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2031),USE(ret:Address_Line2_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,2031),USE(ret:Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(4531,2500),USE(ret:Telephone_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2500),USE(?string22:9),TRN,FONT(,8,,)
                         STRING(@s15),AT(469,2500),USE(ret:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(5781,2448),USE(?string22:5),TRN,FONT(,8,,)
                         STRING(@s10),AT(4063,2344),USE(ret:Postcode_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s10),AT(208,2344),USE(ret:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2188),USE(ret:Address_Line3_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,2188),USE(ret:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(6250,2448),USE(ret:Fax_Number_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4531,2656),USE(tmp:AccountVatNumber),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('VAT No:'),AT(4063,2656),USE(?string22:3),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(208,2656),USE(?string22:8),TRN,FONT(,8,,)
                         STRING(@s15),AT(469,2656),USE(ret:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
break2                 BREAK(endofreport),USE(?unnamed:2)
detail2                  DETAIL,AT(,,,208),USE(?unnamed:5)
                           STRING(@s4),AT(1354,0),USE(tmpque:Quantity),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6563,0),USE(tmpque:LineCost),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s22),AT(52,0,1406,208),USE(tmpque:OrderNumber),TRN,LEFT,FONT(,7,,)
                           STRING(@n14.2),AT(4896,0),USE(tmpque:ExchangeValue),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(5729,0),USE(tmpque:ItemCost),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s60),AT(1667,0,2031,208),USE(tmpque:PartNumber),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s20),AT(3750,0,1198,156),USE(tmpque:Description),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0),USE(?unnamed:6)
                           STRING('Total Exchange Value:'),AT(2240,104),USE(?TotalExchangeValue),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(4844,104),USE(tmp:TotalExchangeValue),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(2052,313,3427,0),USE(?TotalExchangeLine),COLOR(COLOR:Black)
                           STRING('Warranty Note: You may raise a warranty claim to a value equal to the total exch' &|
   'ange value detailed above'),AT(156,365,7240,208),USE(?WarrantyNote),TRN,CENTER
                           LINE,AT(156,52,7188,0),USE(?TotalExchangeLine2),COLOR(COLOR:Black)
                         END
                       END
                       FOOTER,AT(365,9219,7521,1938),USE(?unnamed:4)
                         STRING('Courier Cost:'),AT(4948,0),USE(?CourierCost),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6531,0),USE(courier_cost_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('<128>'),AT(6354,0),USE(?String100),TRN,HIDE,FONT('Arial',8,,)
                         STRING('Total Line Cost (Ex VAT)'),AT(4948,208),USE(?TotalLineCost),TRN,FONT(,8,,)
                         STRING('<128>'),AT(6354,375),USE(?String100:3),TRN,HIDE,FONT('Arial',8,,)
                         STRING(@n14.2),AT(6531,167),USE(sub_total_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('V.A.T.:'),AT(4948,417),USE(?VAT),TRN,FONT(,8,,)
                         STRING('<128>'),AT(6354,167),USE(?String100:2),TRN,HIDE,FONT('Arial',8,,)
                         STRING(@n14.2),AT(6531,375),USE(vat_total_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(6354,573,1042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('<128>'),AT(6354,635),USE(?String100:4),TRN,HIDE,FONT('Arial',8,,)
                         STRING('Total (Inc VAT):'),AT(4948,635),USE(?TotalVat),TRN,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(6531,635),USE(Total_temp),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(104,1094,7292,781),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(354,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,83,3844,240),USE(def:OrderCompanyName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,323,3844,156),USE(def:OrderAddressLine1),TRN
                         STRING(@s30),AT(156,479,3844,156),USE(def:OrderAddressLine2),TRN
                         STRING(@s30),AT(156,635,3844,156),USE(def:OrderAddressLine3),TRN
                         STRING(@s30),AT(156,802,1156,156),USE(def:OrderPostcode),TRN
                         STRING('Tel:'),AT(156,958),USE(?String16),TRN
                         STRING(@s30),AT(677,958),USE(def:OrderTelephoneNumber),TRN
                         STRING('Fax: '),AT(156,1094),USE(?String19),TRN
                         STRING(@s30),AT(677,1094),USE(def:OrderFaxNumber),TRN
                         STRING(@s255),AT(677,1250,3844,208),USE(def:OrderEmailAddress),TRN
                         STRING('Email:'),AT(156,1250),USE(?String19:2),TRN
                         STRING('Qty'),AT(1458,3854),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1708,3854),USE(?strPartNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3792,3854),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6917,3823),USE(?LineCost1),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost'),AT(5990,3854),USE(?ItemCost),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('(Ex.V.A.T.)'),AT(6896,3917),USE(?LineCost2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Order No'),AT(104,3854),USE(?string24:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Exchange Value'),AT(4823,3854),USE(?ExchangeValue),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('INVOICE ADDRESS'),AT(208,1469),USE(?string44:2),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4063,1469),USE(?string44:3),TRN,FONT(,9,,FONT:bold)
                         STRING('PAYMENT DETAILS'),AT(104,8490),USE(?string44:4),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4740,8490),USE(?string44:5),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE TEXT'),AT(2906,8490),USE(?string44:6),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Retail_Single_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Retail_Single_Invoice')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'retinv'
  If PrintOption(PreviewReq,glo:ExportReport,'Retail Invoice') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('locInvoiceNumber',locInvoiceNumber)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  ! Before Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  locInvoiceNumber = fInvoiceNumber
  
  
  ! After Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:RETDESNO.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:RETSALES.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:RETPAY.UseFile
  Access:LOAN.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = locInvoiceNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        sub_total_temp = 0
        
        Save_res_ID = Access:RETSTOCK.SaveFile()
        Access:RETSTOCK.ClearKey(res:Part_Number_Key)
        res:Ref_Number  = ret:Ref_Number
        Set(res:Part_Number_Key,res:Part_Number_Key)
        Loop
            If Access:RETSTOCK.NEXT()
               Break
            End !If
            If res:Ref_Number  <> ret:Ref_Number      |
                Then Break.  ! End If
            If res:Despatched <> 'YES'
                Cycle
            End !If res:Despatched <> 'YES'
        
            IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
                SETTARGET(REPORT)
                ?StrPartNumber{prop:Text} = 'Part Number / Model Number'
                SETTARGET()
                ! #12127 List each individual IMEI Number (Bryan: 04/07/2011)
                tmpque:OrderNumber = res:Purchase_Order_Number
                
                tmpque:PartNumber = res:Part_Number
        
                Access:STOCK.Clearkey(sto:Location_Key)
                sto:Location = MainStoreLocation()
                sto:Part_Number = res:Part_Number
                IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
                    
                END ! IF (Access:STOCK.Clearkey(sto:Location_Key) = Level:Benign)
        
                IF (ret:ExchangeOrder = 1)
                    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                    xch:Ref_Number = res:ExchangeRefNumber
                    IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
                        tmpque:Description = '* EXCHANGE MISSING *'
                    ELSE
                        tmpque:Description = xch:ESN
                    END
                    tmpque:PartNumber = CLIP(tmpque:PartNumber) & ' ' & Clip(sto:ExchangeModelNumber)
                END
                IF (ret:LoanOrder = 1)
                    Access:LOAN.Clearkey(loa:Ref_Number_Key)
                    loa:Ref_Number = res:LoanRefNumber
                    IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
                        tmpque:Description = '* LOAN MISSING *'
                    ELSE
                        tmpque:Description = loa:ESN
                    END
                    tmpque:PartNumber = CLIP(tmpque:PartNumber) & ' ' & Clip(sto:LoanModelNumber)
                END ! IF (ret:LoanOrder = 1)
                tmpque:Quantity = 1
                tmpque:ItemCost = res:Sale_Cost
                tmpque:LineCost = res:Sale_cost
                ADD(PartsList)
                ! Bit of a bodge, but will of been invoiced, so just use those totals
                sub_total_temp  += res:Sale_cost
                Courier_Cost_Temp   = inv:Courier_Paid
                Vat_Total_Temp = Round((inv:Courier_Paid + Sub_Total_Temp) * Vat_Rate_Temp/100,.01)
                total_temp  = inv:courier_paid + Round(vat_total_temp,.01) + (sub_total_temp)
            ELSE
        
        
                If Use_Euro = False
                    Accessory_Cost  = res:AccessoryCost
                    tmp:TotalExchangeValue += res:AccessoryCost * res:Quantity
                    Line_Cost_Temp = Round(res:Item_Cost * res:Quantity,.01)
                    Case ret:Payment_Method
                        Of 'EXC'
                            tmp:ItemCost    = res:AccessoryCost * res:Quantity
                        Else
                            tmp:ItemCost    = res:Item_Cost
                    End !Case ret:Payment_Method
                    sub_total_temp  += line_cost_temp
        
                    Courier_Cost_Temp   = inv:Courier_Paid
                    Vat_Total_Temp = Round((inv:Courier_Paid + Sub_Total_Temp) * Vat_Rate_Temp/100,.01)
                    total_temp  = inv:courier_paid + Round(vat_total_temp,.01) + (sub_total_temp)
                Else !If Use_Euro = False
                    Case ret:Payment_Method
                        Of 'EXC'
                            tmp:ItemCost    = res:AccessoryCost * res:Quantity * inv:EuroExhangeRate
                        Else
                            tmp:ItemCost    = res:Item_Cost * inv:EuroExhangeRate
                    End !Case ret:Payment_Method
                    Accessory_Cost  = res:AccessoryCost * inv:EuroExhangeRate
                    tmp:TotalExchangeValue += ((res:AccessoryCost * inv:EuroExhangeRate) * res:Quantity)
                    Line_Cost_Temp  = Round((res:Item_Cost * inv:EuroExhangeRate) * res:Quantity,.01)
                    sub_total_temp  += line_cost_temp
                    Courier_Cost_temp   = inv:Courier_Paid * inv:EuroExhangeRate
                    vat_total_temp = (Round(((inv:courier_paid * inv:EuroExhangeRate) + sub_total_temp) * vat_rate_temp/100,.01))
                    total_temp  = (inv:courier_paid * inv:EuroExhangeRate) + Round(vat_total_temp,.01) + (sub_total_temp)
                End !If Use_Euro = False
        
                ! #12251 Add to queue after the costs are calculated. (Bryan: 16/08/2011)
                tmpque:OrderNumber  = res:Purchase_Order_Number
                tmpque:PartNumber   = res:Part_number
                Get(PartsList,'OrderNumber,PartNumber')
                If Error()
                    tmpque:OrderNumber  = res:Purchase_Order_Number
                    tmpque:PartNumber   = res:Part_Number
                    tmpque:Quantity     = res:Quantity
                    tmpque:ExchangeValue    = Accessory_Cost
                    tmpque:ItemCost     = tmp:ItemCost
                    tmpque:LineCost     = Line_Cost_Temp
                    tmpque:Description = res:Description ! #12127 Why doesn't this appear? (Bryan: 04/07/2011)
                    Add(PartsList,tmpque:OrderNumber,tmpque:PartNumber)
                Else !If Error()
                    tmpque:Quantity     += res:Quantity
                    tmpque:LineCost     += Line_Cost_Temp
                    Put(PartsList,tmpque:OrderNumber,tmpque:PartNumber)
                End !If Error()
            END
        End !Loop
        Access:RETSTOCK.RestoreFile(Save_res_ID)
        
        Loop x# = 1 To Records(PartsList)
            Get(PartsList,x#)
            Print(rpt:detail2)
        End !x# = 1 To Records(PartsList)
        
        !request from harvey to log all action on invoice generation
        !MakeInvoiceLog('STF')  ! #13608 No point having logging here as the document isn't compulsory, nor would you trap the error (DBH: 28/09/2015)
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','Retail_Single_Invoice')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:RETDESNO.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !printed by
  set(defaults)
  access:defaults.next()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  !is this a copy   ?sent variable
  if CopyInvoice then
      tmp:InvoiceTitle = 'COPY TAX INVOICE: '
  ELSE
      tmp:InvoiceTitle = 'TAX INVOICE: '
  END
  
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'INVOICE - RETAIL'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  
  
  access:retsales.clearkey(ret:invoice_number_key)
  ret:invoice_number = inv:invoice_number
  access:retsales.tryfetch(ret:invoice_number_key)
  
  If inv:UseAlternativeAddress
      Use_Euro = True
  Else !inv:UseAlternativeAddress
      Use_Euro = FALSE
  End !inv:UseAlternativeAddress
  
  ! Display the account's Vat Number - TrkBs: 5364 (DBH: 20-04-2005)
  Access:TRADEACC.ClearKey(tra:StoresAccountKey)
  tra:StoresAccount = ret:Account_Number
  If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign
      !Found
      tmp:AccountVATNumber = tra:VAT_Number
  Else !If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign
      !Error
  End !If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign
  
  
  IF inv:AccountType = 'MAI'
    Access:TradeAcc.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = inv:Account_Number
    IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
      !Error!
    ELSE
    END
  ELSE
    Access:SubTracc.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = inv:Account_Number
    IF Access:SubTracc.Fetch(sub:Account_Number_Key)
      !Error!
    ELSE
    END
  END
  
  despatch_note_number_temp   = 'D-' & locInvoiceNumber
  If ret:payment_method = 'CAS'
      Delivery_Name_Temp  = RET:Contact_Name
      Invoice_Name_Temp   = ret:contact_name
  Else!If ret:payment_method = 'CAS'
      Delivery_Name_Temp  = RET:Company_Name_Delivery
      invoice_name_temp   = ret:company_name
  End!If ret:payment_method = 'CAS'
  If ret:building_name    = ''
      Invoice_address_Line_1     = Clip(RET:Address_Line1)
  Else!    If ret:building_name    = ''
      Invoice_address_Line_1     = Clip(RET:Building_Name) & ' ' & Clip(RET:Address_Line1)
  End!
  If ret:building_name_delivery   = ''
      Delivery_Address_Line_1_Temp    = Clip(RET:Address_Line1_Delivery)
  Else!    If ret:building_name_delivery   = ''
      Delivery_Address_Line_1_Temp    = Clip(RET:Building_Name_Delivery) & ' ' & Clip(RET:Address_Line1_Delivery)
  End!    If ret:building_name_delivery   = ''
  
  vat_rate_temp   = inv:vat_rate_retail
  
  IF Use_Euro = FALSE
      sub_total_temp  = inv:parts_paid
      courier_cost_temp   = inv:courier_paid
      vat_total_temp = Round((inv:courier_paid + inv:parts_paid) * vat_rate_temp/100,.01)
      total_temp  = inv:courier_paid + Round(vat_total_temp,.01) + inv:parts_paid
      tmp:CompanyName       = def:Invoice_Company_Name
      tmp:AddressLine1      = def:Invoice_Address_Line1
      tmp:AddressLine2      = def:Invoice_Address_Line2
      tmp:AddressLine3      = def:Invoice_Address_Line3
      tmp:Postcode          = def:Invoice_Postcode
      tmp:TelephoneNumber   = def:Invoice_Telephone_Number
      tmp:FaxNumber         = def:Invoice_Fax_Number
      tmp:EmailAddress      = def:InvoiceEmailAddress
      tmp:VatNumber         = def:Invoice_VAT_Number
  ELSE
     !Line removed as is now in loop.
  
      !sub_total_temp  = inv:parts_paid * inv:EuroExhangeRate
  
      !courier_cost_temp   = inv:courier_paid * inv:EuroExhangeRate
      !vat_total_temp = (Round(((inv:courier_paid * inv:EuroExhangeRate) + sub_total_temp) * vat_rate_temp/100,.01))
      !total_temp  = (inv:courier_paid * inv:EuroExhangeRate) + Round(vat_total_temp,.01) + (sub_total_temp)
  
      tmp:CompanyName       = DE2:Inv2CompanyName
      tmp:AddressLine1      = DE2:Inv2AddressLine1
      tmp:AddressLine2      = DE2:Inv2AddressLine2
      tmp:AddressLine3      = DE2:Inv2AddressLine3
      tmp:Postcode          = DE2:Inv2Postcode
      tmp:TelephoneNumber   = DE2:Inv2TelephoneNumber
      tmp:FaxNumber         = DE2:Inv2FaxNumber
      tmp:EmailAddress      = DE2:Inv2EmailAddress
      tmp:VatNumber         = DE2:Inv2VATNumber
  END
  !Payments
  
  x# = 0
  save_rtp_id = access:retpay.savefile()
  access:retpay.clearkey(rtp:date_key)
  rtp:ref_number   = ret:ref_number
  set(rtp:date_key,rtp:date_key)
  loop
      if access:retpay.next()
         break
      end !if
      if rtp:ref_number   <> ret:ref_number      |
          then break.  ! end if
  
      x# += 1
      If x# > 0 And x# < 5
          payment_date_temp[x#] = rtp:date
          payment_type_temp[x#] = rtp:payment_type
          IF Use_Euro = FALSE
            payment_amount_temp[x#] = rtp:amount
          ELSE
            payment_amount_temp[x#] = rtp:amount * inv:EuroExhangeRate
          END
      Else
          IF Use_Euro = FALSE
            other_amount_temp += rtp:amount
          ELSE
            other_amount_temp += rtp:amount * inv:EuroExhangeRate
          END
      End!If x# < 5
      IF Use_Euro = FALSE
        total_paid_temp += rtp:amount
      ELSE
        total_paid_temp += rtp:amount * inv:EuroExhangeRate
      END
  end !loop
  access:retpay.restorefile(save_rtp_id)
  
  settarget(report)
  If other_amount_temp <> 0
      Unhide(?others)
  Else!If other_amount_temp <> 0
      Hide(?others)
  End!If other_amount_temp <> 0
  If ret:Payment_Method = 'EXC'
      ?couriercost{prop:Hide} = 1
      ?totallinecost{prop:Hide} = 1
      ?vat{prop:Hide} = 1
      ?totalvat{prop:Hide} = 1
  
      ?courier_cost_temp{prop:Hide} = 1
      ?sub_total_temp{prop:Hide} = 1
      ?vat_total_temp{prop:Hide} = 1
      ?total_temp{prop:Hide} = 1
  
      ?tmpque:ExchangeValue{prop:Hide} = 0
      ?tmpque:LineCost{prop:Hide} = 1
  
      ?ExchangeValue{prop:Hide} = 0
      ?ItemCost{prop:Text} = 'Total'
      ?LineCost1{prop:Hide} = 1
      ?LineCost2{prop:Hide} = 1
      ?tmp:TotalExchangeValue{prop:Hide} = 0
      ?TotalExchangeValue{prop:Hide} = 0
      ?TotalExchangeLine{prop:Hide} = 0
      ?TotalExchangeLine2{prop:Hide} = 0
      ?WarrantyNote{prop:Hide} = 0
  Else !ret:Payment_Method = 'EXC'
      ?couriercost{prop:Hide} = 0
      ?totallinecost{prop:Hide} = 0
      ?vat{prop:Hide} = 0
      ?totalvat{prop:Hide} = 0
  
      ?courier_cost_temp{prop:Hide} = 0
      ?sub_total_temp{prop:Hide} = 0
      ?vat_total_temp{prop:Hide} = 0
      ?total_temp{prop:Hide} = 0
  
      ?tmpque:ExchangeValue{prop:Hide} = 1
      ?tmpque:LineCost{prop:Hide} = 0
  
      ?ExchangeValue{prop:Hide} = 1
      ?LineCost1{prop:Hide} = 0
      ?LineCost2{prop:Hide} = 0
      ?tmp:TotalExchangeValue{prop:Hide} = 1
      ?TotalExchangeValue{prop:Hide} = 1
      ?TotalExchangeLine{prop:Hide} = 1
      ?TotalExchangeLine2{prop:Hide} = 1
      ?WarrantyNote{prop:Hide} = 1
  End !ret:Payment_Method = 'EXC'
  IF Use_Euro = TRUE
      ?String100{prop:Hide} = FALSE
      ?String100:2{prop:Hide} = FALSE
      ?String100:3{prop:Hide} = FALSE
      ?String100:4{prop:Hide} = FALSE
  END
  
  settarget()
  ! #12249 Show Barcode (DBH: 17/01/2012)
  Code_Temp = 3
  Option_Temp = 0
  Bar_Code_String_Temp = inv:Invoice_Number
  SEQUENCE2(Code_Temp,Option_Temp,Bar_Code_String_Temp,Bar_Code_Temp)
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Retail_Single_Invoice'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













Exception_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
count_temp           LONG
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Unit_Type)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Exceptions Report'),AT(396,2771,7521,8531),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,521,7521,1760),USE(?unnamed)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('WARRANTY EXCEPTIONS REPORT'),AT(4063,0,3385,260),USE(?String20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,313,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,469,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,625,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING('Manufacturer:'),AT(4896,417),USE(?String35),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,417),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(156,781,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING(@s30),AT(5885,1042),USE(SUB:Company_Name),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN,FONT(,9,,)
                         STRING(@s4),AT(5885,1458),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Batch Number'),AT(4896,625),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s9),AT(5885,625,1760,240),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Date'),AT(4896,1250),USE(?String31),TRN,FONT(,8,,)
                         STRING(@D6),AT(5885,1250),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@T3),AT(6563,1250),USE(ReportRunTime),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s255),AT(573,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Account Number: '),AT(4896,833),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s15),AT(5885,833,1000,240),USE(SUB:Account_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Account Name: '),AT(4896,1042),USE(?String36),TRN,FONT(,8,,)
                         STRING('Page Number'),AT(4896,1458,833,208),USE(?String34),TRN,FONT(,8,,)
                       END
break1                 BREAK(EndOfReport),USE(?unnamed:4)
detail1                  DETAIL,USE(?unnamed:5)
                           STRING(@s20),AT(1146,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s25),AT(2469,0),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                           STRING(@s16),AT(5625,0),USE(job:MSN),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(104,0),USE(tmp:Ref_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s16),AT(4125,0),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,646),USE(?unnamed:6)
                           LINE,AT(271,208,6979,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total:'),AT(313,313),USE(?String30),TRN,FONT(,9,,FONT:bold)
                           STRING(@n8),AT(729,313),USE(count_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(365,11417,7552,260),USE(?unnamed:2),ABSOLUTE
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('Job Number'),AT(104,2083),USE(?String33),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(1146,2083),USE(?String51),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(2469,2083),USE(?String52),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4125,2083),USE(?String53),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(5625,2083),USE(?String54),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Exception_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Exception_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select2',GLO:Select2)
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job:EDI_Key)
      Process:View{Prop:Filter} = |
      'UPPER(job:Manufacturer) = UPPER(GLO:Select1) AND (Upper(job:edi) = ''YE' & |
      'S'' And Upper(job:edi_batch_number) = Upper(glo:select2))'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        count_temp += 1
        !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
        ! 26 Aug 2002 John
        !
        tmp:Ref_number = job:Ref_number
        if glo:WebJob then
            !Change the tmp ref number if you
            !can Look up from the wob file
            access:Webjob.clearkey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_number
            if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                access:tradeacc.clearkey(tra:Account_Number_Key)
                tra:Account_Number = ClarioNET:Global.Param2
                access:tradeacc.fetch(tra:Account_Number_Key)
                tmp:Ref_Number = Job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
            END
        
        END
        !--------------------------------------------------------------
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(RPT:detail1)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Exception_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = glo:select3
  access:subtracc.fetch(sub:account_number_key)
  count_temp = 0
  !--- Webify SETUP ClarioNET:UseReportPreview(0) ---------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  END
  !--------------------------------------------------------------
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Exception_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage













Multiple_Chargeable_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
save_job_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Invoice_Number_Temp  STRING(28)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Labour_vat_info_temp STRING(15)
Parts_vat_info_temp  STRING(15)
batch_number_temp    STRING(28)
labour_vat_temp      REAL
parts_vat_temp       REAL
balance_temp         REAL
job_total_temp       REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Invoice_VAT_Number)
                       PROJECT(inv:Labour_Paid)
                       PROJECT(inv:Parts_Paid)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,4563,7521,4281),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,521,7521,4042)
                         STRING(@s28),AT(4917,917),USE(batch_number_temp),TRN,CENTER,FONT(,,,FONT:bold)
                         STRING('Page Number: '),AT(4083,2396),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@N4),AT(5521,2396),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s28),AT(4917,521),USE(Invoice_Number_Temp),TRN,CENTER,FONT(,,,FONT:bold)
                         STRING(@s30),AT(156,1677),USE(Invoice_Name_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('V.A.T. Number: '),AT(4083,1677),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1844,1917,156),USE(Address_Line1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(5521,1677,1760,240),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2000),USE(Address_Line2_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2156),USE(Address_Line3_Temp),TRN,FONT(,8,,)
                         STRING('Date Of Invoice:'),AT(4083,1917),USE(?String31),TRN,FONT(,8,,)
                         STRING(@D6),AT(5521,1917),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@T3),AT(6156,1917),USE(ReportRunTime),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2323),USE(Address_Line4_Temp),TRN,FONT(,8,,)
                         STRING('Account Number: '),AT(4083,2156),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s15),AT(5521,2156,1000,240),USE(SUB:Account_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Job Number'),AT(323,3844),USE(?String33),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(1240,3844),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3000,3844),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4396,3844),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(5521,3844),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('Value'),AT(6677,3844),USE(?String48),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2521,1323,156),USE(inv:Invoice_VAT_Number),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,156),USE(?DetailBand)
                           STRING(@s15),AT(240,0),USE(tmp:Ref_Number),TRN,RIGHT,FONT(,8,,)
                           STRING(@s25),AT(1240,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s20),AT(3000,0),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(4396,0),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                           STRING(@s10),AT(5521,0),USE(job:MSN),TRN,LEFT,FONT(,8,,)
                           STRING(@n14.2),AT(6156,0),USE(job:Invoice_Sub_Total),RIGHT,FONT(,8,,)
                         END
                         FOOTER,AT(396,8802,,2281),USE(?unnamed),ABSOLUTE
                           STRING('Parts Sub Total'),AT(4802,719),USE(?String37),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6323,719),USE(inv:Parts_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6323,875),USE(parts_vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6323,563),USE(labour_vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@p<<<<<<#p),AT(1042,719),USE(job_total_temp),TRN,RIGHT
                           STRING('Labour Sub Total'),AT(4802,396),USE(?String39),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6323,396),USE(inv:Labour_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING('Total Jobs: '),AT(396,719),USE(?String50),TRN
                           STRING(@s15),AT(4802,563),USE(Labour_vat_info_temp),TRN,LEFT,FONT(,8,,)
                           STRING('Balance Due'),AT(4802,1042),USE(?String38),TRN,FONT(,10,,FONT:bold)
                           STRING('<128>'),AT(6042,1042),USE(?Euro),TRN,HIDE,FONT(,10,,FONT:bold)
                           STRING(@n14.2),AT(6094,1042),USE(balance_temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING(@s15),AT(4802,875),USE(Parts_vat_info_temp),TRN,FONT(,8,,)
                         END
                       END
NewPage                DETAIL,PAGEAFTER(1),AT(,,,42),USE(?NewPage)
                       END
                       FOOTER,AT(396,10323,7521,1156)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,83,3844,240),USE(def:Invoice_Company_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('INVOICE'),AT(5604,0),USE(?String20),TRN,FONT(,20,,FONT:bold)
                         STRING(@s30),AT(156,323,3844,156),USE(def:Invoice_Address_Line1),TRN
                         STRING(@s30),AT(156,479,3844,156),USE(def:Invoice_Address_Line2),TRN
                         STRING(@s30),AT(156,635,3844,156),USE(def:Invoice_Address_Line3),TRN
                         STRING(@s15),AT(156,802,1156,156),USE(def:Invoice_Postcode),TRN
                         STRING('Tel:'),AT(156,958),USE(?String16),TRN
                         STRING(@s15),AT(677,958,3844,208),USE(def:Invoice_Telephone_Number)
                         STRING('Fax: '),AT(156,1094),USE(?String19),TRN
                         STRING(@s15),AT(677,1094,3844,208),USE(def:Invoice_Fax_Number)
                         STRING(@s255),AT(677,1250,3844,208),USE(def:InvoiceEmailAddress),TRN
                         STRING('Email:'),AT(156,1250),USE(?String19:2),TRN
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Multiple_Chargeable_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Multiple_Chargeable_Invoice')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = ''
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
            access:subtracc.clearkey(sub:account_number_key)
            sub:account_number = inv:account_number
            if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                invoice_name_temp      = sub:company_name
                address_line1_temp     = sub:address_line1
                address_line2_temp     = sub:address_line2
                If sup:address_line3   = ''
                    address_line3_temp = sub:postcode
                    address_line4_temp = ''
                Else
                    address_line3_temp  = sub:address_line3
                    address_line4_temp  = sub:postcode
                End
            end
            invoice_number_temp     = 'Invoice Number: ' & Format(glo:Queue2,@p<<<<<<<#p)
            labour_vat_info_temp    = 'V.A.T. @ ' & Format(inv:vat_rate_labour,@n6.2)
            labour_vat_temp         = inv:labour_paid * (inv:vat_rate_labour/100)
            parts_vat_info_temp     = 'V.A.T. @ ' & Format(inv:vat_rate_parts,@n6.2)
            parts_vat_temp          = inv:parts_paid * (inv:vat_rate_parts/100)
            balance_temp            = inv:labour_paid + labour_vat_temp + inv:parts_paid + parts_vat_temp
        !    end!if access:invoice.fetch(inv:invoice_number_key)
            job_total_temp = 0
            setcursor(cursor:wait)
            save_job_id = access:jobs.savefile()
            access:jobs.clearkey(job:InvoiceNumberKey)
            job:invoice_number = inv:invoice_number
            set(job:InvoiceNumberKey,job:InvoiceNumberKey)
            loop
                if access:jobs.next()
                   break
                end !if
                if job:invoice_number <> inv:invoice_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                job_total_temp += 1
                Print(rpt:detail)
            end !loop
            access:jobs.restorefile(save_job_id)
            setcursor()
            Print(rpt:newpage)
        !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
        ! 26 Aug 2002 John
        !
        tmp:Ref_number = job:Ref_number
        if glo:WebJob then
            !Change the tmp ref number if you
            !can Look up from the wob file
            access:Webjob.clearkey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_number
            if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                access:tradeacc.clearkey(tra:Account_Number_Key)
                tra:Account_Number = ClarioNET:Global.Param2
                access:tradeacc.fetch(tra:Account_Number_Key)
                tmp:Ref_Number = Job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
            END
        
        END
        !--------------------------------------------------------------
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(RPT:DETAIL)
          PRINT(RPT:NewPage)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Multiple_Chargeable_Invoice')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  access:vatcode.clearkey(vat:vat_code_key)
  vat:vat_code = def:vat_rate_labour
  if access:vatcode.fetch(vat:vat_code_key) = level:benign
      labour_vat_info_temp = 'V.A.T. @ ' & Format(vat:vat_rate,@n6.2)
  end
  access:vatcode.clearkey(vat:vat_code_key)
  vat:vat_code = def:vat_rate_labour
  if access:vatcode.fetch(vat:vat_code_key) = level:benign
      parts_vat_info_temp = 'V.A.T. @ ' & Format(vat:vat_rate,@n6.2)
  end
  
  access:vatcode.clearkey(vat:vat_code_key)
  vat:vat_code = def:vat_rate_labour
  access:vatcode.tryfetch(vat:vat_code_key)
  labour_vat_info_temp = 'V.A.T. @ ' & Format(vat:vat_rate,@n6.2)
  labour_vat_temp      = inv:labour_paid * (vat:vat_rate/100)
  
  access:vatcode.clearkey(vat:vat_code_key)
  vat:vat_code = def:vat_rate_parts
  access:vatcode.tryfetch(vat:vat_code_key)
  parts_vat_info_temp = 'V.A.T. @ ' & Format(vat:vat_rate,@n6.2)
  parts_vat_temp      = inv:parts_paid * (vat:vat_rate/100)
  
  balance_temp            = inv:labour_paid + labour_vat_temp + inv:parts_paid + parts_vat_temp
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE'
  access:stantext.fetch(stt:description_key)
  !--- Webify SETUP ClarioNET:UseReportPreview(0) ---------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  END
  !--------------------------------------------------------------
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Multiple_Chargeable_Invoice'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END













Single_Invoice PROCEDURE(f:Prefix,f:Suffix)
Default_Invoice_Company_Name_Temp STRING(30)
save_tradeacc_id     USHORT,AUTO
save_invoice_id      USHORT,AUTO
save_jobs_id         USHORT,AUTO
save_jobse_id        USHORT,AUTO
save_audit_id        USHORT,AUTO
save_aud_id          USHORT,AUTO
tmp:PrintA5Invoice   BYTE(0)
tmp:Ref_Number       STRING(20)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
save_par_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
customer_name_temp   STRING(60)
sub_total_temp       REAL
payment_date_temp    DATE,DIM(4)
other_amount_temp    REAL
total_paid_temp      REAL
payment_type_temp    STRING(20),DIM(4)
payment_amount_temp  REAL,DIM(4)
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
DefaultAddress       GROUP,PRE(address)
Name                 STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
Telephone            STRING(30)
Fax                  STRING(30)
EmailAddress         STRING(255)
                     END
tmp:LabourCost       REAL
tmp:PartsCost        REAL
tmp:CourierCost      REAL
tmp:InvoiceNumber    STRING(30)
locAuditNotes        STRING(255)
XMLType              STRING(3)
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Date_Created)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,6521,7521,2323),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,5844),USE(?unnamed)
                         STRING('Job Number: '),AT(4896,313),USE(?String25),TRN,FONT(,8,,)
                         STRING(@s20),AT(5917,313),USE(tmp:Ref_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Of Invoice:'),AT(4896,469),USE(?String86),TRN,FONT(,8,,)
                         STRING('Invoice Number: '),AT(4896,625),USE(?String87),TRN,FONT(,8,,)
                         STRING(@s30),AT(5917,625),USE(tmp:InvoiceNumber),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5917,781),USE(job:Order_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Order Number:'),AT(4896,781),USE(?String87:3),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5917,938),USE(job:date_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Date Booked:'),AT(4896,938),USE(?String87:2),TRN,FONT(,8,,)
                         STRING('INVOICE ADDRESS'),AT(156,1302),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('V.A.T. No: '),AT(1615,1302),USE(?String102),TRN,FONT(,9,,)
                         STRING(@s20),AT(2240,1302),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1302,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING(@s60),AT(156,1563),USE(customer_name_temp),TRN,FONT(,8,,)
                         STRING(@s60),AT(4115,1563),USE(customer_name_temp,,?customer_name_temp:2),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5917,469),USE(inv:Date_Created),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@s22),AT(1604,3240),USE(job:Order_Number,,?JOB:Order_Number:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4604,3240),USE(job:Charge_Type,,?JOB:Charge_Type:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s22),AT(3156,3240),USE(job:Authority_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(6083,3240),USE(job:Repair_Type,,?JOB:Repair_Type:2),TRN,FONT(,8,,)
                         STRING(@s25),AT(156,3917,1000,156),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(6250,3906),USE(job:Mobile_Number),TRN,FONT(,8,,)
                         STRING(@s25),AT(1250,3906,1323,156),USE(job:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s25),AT(2656,3906,1396,156),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s16),AT(4167,3906,990,156),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                         STRING(@s16),AT(5208,3906),USE(job:MSN),TRN,FONT(,8,,)
                         STRING('REPORTED FAULT'),AT(156,4323),USE(?String64),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1823,4323,5417,469),USE(jbn:Fault_Description),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('ENGINEERS REPORT'),AT(156,5000),USE(?String88),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1823,5000,5417,469),USE(jbn:Invoice_Text),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('PARTS USED'),AT(156,5604),USE(?String79),TRN,FONT(,9,,FONT:bold)
                         STRING('Qty'),AT(1521,5604),USE(?String80),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1917,5604),USE(?String81),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3677,5604),USE(?String82),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Cost'),AT(5719,5604),USE(?String83),TRN,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(6677,5604),USE(?String89),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1396,5760,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4115,2500),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4375,2500),USE(job:Telephone_Delivery),FONT(,8,,)
                         STRING('Tel:'),AT(156,2500),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(417,2500),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(1927,2500),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2188,2500),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3240),USE(job:Account_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1719),USE(job:Company_Name_Delivery),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1875,1917,156),USE(Delivery_Address1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2031),USE(Delivery_address2_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2188),USE(Delivery_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2344),USE(Delivery_address4_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@n8b),AT(1156,0),USE(par:Quantity),TRN,RIGHT,FONT(,8,,)
                           STRING(@s25),AT(1917,0),USE(part_number_temp),TRN,FONT(,8,,)
                           STRING(@s25b),AT(3677,0),USE(par:Description),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(5396,0),USE(par:Sale_Cost),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,0),USE(line_cost_temp),TRN,RIGHT,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,9146,7521,2406),USE(?unnamed:4)
                         STRING('Date'),AT(156,0),USE(?String66),TRN,FONT(,7,,FONT:bold)
                         STRING('Type'),AT(677,0),USE(?String67),TRN,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1771,156),USE(payment_amount_temp[1]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('Labour:'),AT(4844,0),USE(?String90),TRN,FONT(,8,,)
                         STRING(@n-14.2),AT(6406,0),USE(tmp:LabourCost),TRN,RIGHT,FONT(,8,,)
                         STRING(@s20),AT(677,156),USE(payment_type_temp[1]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('Amount'),AT(1938,0),USE(?String70),TRN,FONT(,7,,FONT:bold)
                         STRING(@s20),AT(3125,281),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(104,260),USE(payment_date_temp[2]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s8),AT(2875,83,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                         STRING(@d6b),AT(104,156),USE(payment_date_temp[1]),TRN,RIGHT,FONT(,7,,)
                         STRING('Carriage:'),AT(4844,313),USE(?String93),TRN,FONT(,8,,)
                         STRING(@n-14.2),AT(6406,156),USE(tmp:PartsCost),TRN,RIGHT,FONT(,8,,)
                         STRING(@n10.2b),AT(1771,260),USE(payment_amount_temp[2]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1771,365),USE(payment_amount_temp[3]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('V.A.T.'),AT(4844,625),USE(?String94),TRN,FONT(,8,,)
                         STRING(@n-14.2),AT(6406,313),USE(tmp:CourierCost),TRN,RIGHT,FONT(,8,,)
                         STRING('Total:'),AT(4844,781),USE(?String95),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(6302,729,1000,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING(@d6b),AT(104,469),USE(payment_date_temp[4]),TRN,RIGHT,FONT(,7,,)
                         STRING(@n-14.2),AT(6427,781),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Others:'),AT(156,625),USE(?Others),TRN,FONT(,7,,FONT:bold)
                         STRING(@s20),AT(677,365),USE(payment_type_temp[3]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1771,469),USE(payment_amount_temp[4]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         LINE,AT(6302,417,1000,0),USE(?Line2:2),COLOR(COLOR:Black)
                         STRING(@n14.2),AT(6406,469),USE(sub_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING(@d6b),AT(104,365),USE(payment_date_temp[3]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(677,260),USE(payment_type_temp[2]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('Sub Total:'),AT(4844,469),USE(?String106),TRN,FONT(,8,,)
                         STRING(@s16),AT(2875,521,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                         LINE,AT(156,781,1979,0),USE(?Line4),COLOR(COLOR:Black)
                         STRING('Total:'),AT(156,833),USE(?String109),TRN,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1771,625),USE(other_amount_temp),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n14.2),AT(6406,625),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                         STRING(@s20),AT(677,469),USE(payment_type_temp[4]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@s24),AT(2875,719,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                         STRING(@n10.2b),AT(1771,833),USE(total_paid_temp),TRN,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('<128>'),AT(6344,781),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Parts:'),AT(4844,156),USE(?String92),TRN,FONT(,8,,)
                         TEXT,AT(104,1302,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('INVOICE'),AT(5604,0),USE(?Text:Invoice),TRN,RIGHT,FONT(,20,,FONT:bold)
                         STRING(@s30),AT(156,52,3844,240),USE(address:Name),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,323,2240,156),USE(address:AddressLine1),TRN
                         STRING(@s30),AT(156,479,2240,156),USE(address:AddressLine2),TRN
                         STRING(@s30),AT(156,635,2240,156),USE(address:AddressLine3),TRN
                         STRING(@s30),AT(156,802,1156,156),USE(address:Postcode),TRN
                         STRING('Tel:'),AT(156,958),USE(?String16),TRN
                         STRING(@s30),AT(677,958),USE(address:Telephone),TRN
                         STRING('Fax: '),AT(156,1094,313,208),USE(?String19),TRN
                         STRING(@s30),AT(677,1094),USE(address:Fax),TRN
                         STRING('Email:'),AT(156,1240,417,208),USE(?String19:2),TRN
                         STRING(@s255),AT(677,1240),USE(address:EmailAddress),TRN
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('PAYMENT DETAILS'),AT(156,8479),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?String74),TRN,FONT(,9,,FONT:bold)
                         STRING('Model'),AT(156,3854),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(156,3177),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(1604,3177),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Authority Number'),AT(3073,3177),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(4531,3177),USE(?String40:5),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(6000,3177),USE(?String40:6),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1250,3854),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(2656,3854),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4167,3854),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(5208,3854),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('Mobile No'),AT(6250,3854,625,208),USE(?String44:2),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3635),USE(?String50),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Single_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
      ! Save Window Name
   AddToLog('Report','Open','Single_Invoice')
  !Call Vodacom Delivery Note
  ! Inserting (DBH 12/06/2006) #7149 - Move code earlier to prevent the preview message appearing
  tmp:PrintA5Invoice = GETINI('PRINTING','PrintA5Invoice',,CLIP(PATH())&'\SB2KDEF.INI')
  If tmp:PrintA5Invoice = TRUE
      ! Vodacom Delivery Note is specific to Vodacom
      if (glo:WebJob = 1)
          !MESSAGE('PRINTING')
          !ClarioNET:UseReportPreview(0)
          access:jobs.clearkey(job:InvoiceNumberKey)
          job:invoice_number = glo:select1   !was inv:invoice_number
          access:jobs.fetch(job:InvoiceNumberKey)
          Vodacom_Delivery_Note_Web(job:Ref_Number,f:Prefix,f:Suffix)
          !ClarioNET:CallClientProcedure('SetClientPrinter','Default')
      else
          Vodacom_Delivery_Note(job:Ref_Number)
      end
  
      do ProcedureReturn
  Else !Instring('VODACOM',def:User_Name,1,1)
  
      ! End (DBH 12/06/2006) #7149
      Save_TRADEACC_ID = Access:TRADEACC.SaveFile()
      Save_JOBS_ID = Access:JOBS.SaveFile()
      Save_INVOICE_ID = Access:INVOICE.SaveFile()
      Save_JOBSE_ID = Access:JOBSE.SaveFile()
      
      If glo:WebJob
          Access:JOBS.ClearKey(job:InvoiceNumberKey)
          job:Invoice_Number = glo:Select1
          If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
              !Found
          Else ! If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
              !Error
          End ! If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
      
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = glo:Select1
          If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
              !Found
          Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
              !Error
          End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
      
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = Clarionet:Global.Param2
          If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Found
          Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      
          If inv:RRCInvoiceDate = 0
              inv:RRCInvoiceDate = Today()
              If inv:ExportedRRCOracle = 0
                  jobe:InvRRCCLabourCost  = jobe:RRCCLabourCost
                  jobe:InvRRCCPartsCost   = jobe:RRCCPartsCost
                  jobe:InvRRCCSubTotal    = jobe:RRCCSubTotal
                  jobe:InvoiceHandlingFee = jobe:HandlingFee
                  jobe:InvoiceExchangeRate    = jobe:ExchangeRate
                  Access:JOBSE.TryUpdate()
      
                  inv:ExportedRRCOracle = TRUE
                  Access:INVOICE.TryUpdate()
      
                  Line500_XML('RIV')
                  !Invoice logging requested by harvey - 07/05/15
                  MakeInvoiceLog('RIV')
  
              End ! If inv:ExportedRRCOracle = 0
              FoundAudit# = 0
              Save_aud_ID = Access:AUDIT.SaveFile()
              Access:AUDIT.ClearKey(aud:Action_Key)
              aud:Ref_Number = job:Ref_Number
              aud:Action     = 'INVOICE CREATED'
              Set(aud:Action_Key,aud:Action_Key)
              Loop
                  If Access:AUDIT.NEXT()
                     Break
                  End !If
                  If aud:Ref_Number <> job:Ref_Number      |
                  Or aud:Action     <> 'INVOICE CREATED'      |
                      Then Break.  ! End If
                  Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                  aud2:AUDRecordNumber = aud:Record_Number
                  IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                       If Instring('INVOICE NUMBER: ' & CLip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification),aud2:Notes,1,1)
                           FoundAudit# = 1
                           Break
                       End !If Instring('INVOICE NUMBER: ' & CLip(inv:Invoice_Number),aud:Notes,1,1)
                  END ! IF
              End !Loop
              Access:AUDIT.RestoreFile(Save_aud_ID)
              If FoundAudit# = 0
                  locAuditNotes   = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                  If LoanAttachedToJob(job:Ref_Number)
                      locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(job:Invoice_Courier_Cost,@n14.2)
                  End !If LoanAttachedToJob(job:Ref_Number)
                  If jobe:Engineer48HourOption = 1
                      If jobe:ExcReplcamentCharge
                          locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(job:Invoice_Courier_Cost,@n14.2)
                      End !If jobe:ExcReplcamentCharge
                  End !If jobe:Engineer48HourOption = 1
                  locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(jobe:InvRRCCPartsCost,@n14.2)
                  locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(jobe:InvRRCCLabourCost,@n14.2)
                  locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(jobe:InvRRCCSubTotal,@n14.2)
                  locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((jobe:InvRRCCPartsCOst * inv:RRCVatRateParts/100) + |
                                                  (jobe:InvRRCCLabourCost * inv:RRCVatRateLabour/100) + |
                                                  (job:Invoice_Courier_Cost * inv:RRCVatRateLabour/100),@n14.2)
                  locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(jobe:InvRRCCSubTotal + (jobe:InvRRCCPartsCost * inv:RRCVatRateParts/100) + |
                                                  (jobe:InvRRCCLabourCost * inv:RRCVatRateLabour/100) + |
                                                  (job:Invoice_Courier_Cost * inv:RRCVatRateLabour/100),@n14.2)
  
                  IF (AddToAudit(job:ref_number,'JOB','INVOICE CREATED',locAuditNotes))
                  END ! IF
              End !If FoundAudit# = 0
          Else ! If inv:RRCInvoiceDate = 0
              If AddToAudit(job:Ref_Number,'JOB','INVOICE REPRINTED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                                                                      '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06) )
      
              End ! '<13,10>INVOICE DATE: ' & FOrmat(inv:Date_Created,@d06)
          End !If inv:RRCInvoiceDate = 0
      
      Else ! If glo:WebJob
          Access:JOBS.ClearKey(job:InvoiceNumberKey)
          job:Invoice_Number = glo:Select1
          If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
              !Found
          Else ! If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
              !Error
          End ! If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
      
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = glo:Select1
          If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
              !Found
          Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
              !Error
          End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
      
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
          If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Found
              If inv:ARCInvoiceDate = 0
                  inv:ARCInvoiceDate = Today()
                  job:Invoice_Labour_Cost = job:Labour_Cost
                  job:Invoice_Courier_Cost = job:Courier_Cost
                  job:Invoice_Parts_Cost = job:Parts_Cost
                  job:Invoice_Sub_Total = job:Invoice_Labour_Cost + job:Invoice_Parts_Cost + job:Invoice_Courier_Cost
                  Access:JOBS.TryUpdate()
      
                  inv:ExportedARCOracle = True
                  Access:INVOICE.TryUpdate()
      
                  Access:JOBSE.ClearKey(jobe:RefNumberKey)
                  jobe:RefNumber = job:Ref_Number
                  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                      !Found
                  Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                      !Error
                  End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      
                  Line500_XML('AOW')
                  !Invoice logging requested by harvey - 07/05/15
                  MakeInvoiceLog('AOW')
  
                  ! Bodge to see if invoice audit entry already created. If not, do it (DBH: 17/05/2007)
                  FoundAudit# = 0
                  Save_AUDIT_ID = Access:AUDIT.SaveFile()
                  Access:AUDIT.Clearkey(aud:Action_Key)
                  aud:Ref_Number = job:Ref_Number
                  aud:Action = 'INVOICE CREATED'
                  Set(aud:Action_Key,aud:Action_Key)
                  Loop ! Begin Loop
                      If Access:AUDIT.Next()
                          Break
                      End ! If Access:AUDIT.Next()
                      If aud:Ref_Number <> job:Ref_Number
                          Break
                      End ! If aud:Ref_Number <> job:Ref_Number
                      If aud:Action <> 'INVOICE CREATED'
                          Break
                      End ! If aud:Action <> 'INVOICE CREATED'
                        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                        aud2:AUDRecordNumber = aud:Record_Number
                        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                              If Instring('INVOICE NUMBER: ' & CLip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification),aud2:Notes,1,1)
                                  FoundAudit# = 1
                                  Break
                              End !If Instring('INVOICE NUMBER: ' & CLip(inv:Invoice_Number),aud:Notes,1,1)
                        END ! IF
  
      
                  End ! Loop
                  Access:AUDIT.RestoreFile(Save_AUDIT_ID)
      
                  If FoundAudit# = 0
                      locAuditNotes   = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                      If LoanAttachedToJob(job:Ref_Number)
                          ! Changing (DBH 27/06/2006) #7584 - If loan attached, then invoice number is not being written to the audit trail
                          ! aud:Notes   = '<13,10>LOST LOAN FEE: ' & Format(job:Invoice_Courier_Cost,@n14.2)
                          ! to (DBH 27/06/2006) #7584
                          locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(job:Invoice_Courier_Cost,@n14.2)
                          ! End (DBH 27/06/2006) #7584
  
                      End !If LoanAttachedToJob(job:Ref_Number)
                      If jobe:Engineer48HourOption = 1
                          If jobe:ExcReplcamentCharge
                              locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(job:Invoice_Courier_Cost,@n14.2)
                          End !If jobe:ExcReplcamentCharge
                      End !If jobe:Engineer48HourOption = 1
                      locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(job:Invoice_Parts_Cost,@n14.2)
                      locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(job:Invoice_Labour_Cost,@n14.2)
                      locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(job:Invoice_Sub_Total,@n14.2)
                      locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                                                      (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                                                      (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100),@n14.2)
                      locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(job:Invoice_Sub_Total + (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                                                      (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                                                      (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100),@n14.2)
  
                        IF (AddToAudit(job:ref_number,'JOB','INVOICE CREATED',locAuditNotes))
                        END ! IF
  
                  End ! If FoundAudit# = 0
      
              Else
                  If AddToAudit(job:Ref_Number,'JOB','INVOICE REPRINTED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                                                                          '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))
                  End ! '<13,10>INVOICE DATE: ' & FOrmat(inv:Date_Created,@d06)
              End ! If inv:ARCInvoiceDate = 0
          Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      End ! If glo:WebJob
      Access:JOBS.RestoreFile(Save_JOBS_ID)
      Access:TRADEACC.RestoreFile(Save_TRADEACC_ID)
      Access:INVOICE.RestoreFile(Save_INVOICE_ID)
      Access:JOBSE.RestoreFile(Save_JOBSE_ID)
  End !Instring('VODACOM',def:User_Name,1,1)
  ! After Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  ! Before Embed Point: %AfterPreviewReq) DESC(After Setting Preview Request (PreviewReq)) ARG()
  END                                                 !---ClarioNET 83
  Omit('**override printoption**')
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'Invoice'
  If PrintOption(PreviewReq,glo:ExportReport,'Invoice') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  **override printoption**
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  If f:Prefix <> ''
      glo:ReportName = 'Credit'
      If PrintOption(PreviewReq,glo:ExportReport,'Credit') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  
  Else ! If f:Prefix <> ''
      glo:ReportName = 'Invoice'
      If PrintOption(PreviewReq,glo:ExportReport,'Invoice') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  End ! If f:Prefix <> ''
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  ! After Embed Point: %AfterPreviewReq) DESC(After Setting Preview Request (PreviewReq)) ARG()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBPAYMT.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:JOBACC.UseFile
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSE.UseFile
  Access:AUDIT.UseFile
  Access:JOBSINV.UseFile
  Access:AUDIT2.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        count# = 0
        setcursor(cursor:wait)
        save_par_id = access:parts.savefile()
        access:parts.clearkey(par:part_number_key)
        par:ref_number  = job:ref_number
        set(par:part_number_key,par:part_number_key)
        loop
            if access:parts.next()
               break
            end !if
            if par:ref_number  <> job:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            count# += 1
            part_number_temp = par:part_number
            line_cost_temp = par:quantity * par:sale_cost
            Print(rpt:detail)
        end !loop
        access:parts.restorefile(save_par_id)
        setcursor()
        If count# = 0
            part_number_temp = 'No Parts Attached'
            Print(rpt:detail)
        End!If count# = 0
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
  ! Before Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  !clarionet stuff
  if glo:webjob then
      ClarioNET:CallClientProcedure('SetClientPrinter','Default')
  END !If this was a web call
      ! Save Window Name
   AddToLog('Report','Close','Single_Invoice')
  ! After Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBPAYMT.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  access:jobs.clearkey(job:InvoiceNumberKey)
  job:invoice_number = glo:select1   !was inv:invoice_number
  access:jobs.fetch(job:InvoiceNumberKey)
  
  !********************Code added by Paul*****************
  access:webjob.clearkey(wob:RefNumberKey)
  wob:refnumber = job:Ref_Number
  if access:webjob.fetch(wob:RefNumberKey)
      tmp:ref_number = job:Ref_Number
  else
      access:tradeacc.clearkey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      access:tradeacc.fetch(tra:Account_Number_Key)
      tmp:ref_number = job:ref_number & '-' &tra:BranchIdentification &wob:jobnumber
  
  end
  tmp:InvoiceNumber = Clip(job:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
  
  access:jobnotes.clearkey(jbn:RefNumberKey)
  jbn:RefNumber   = job:Ref_number
  access:jobnotes.tryfetch(jbn:RefNumberKey)
  
  delivery_address1_temp     = job:address_line1_delivery
  delivery_address2_temp     = job:address_line2_delivery
  If job:address_line3_delivery   = ''
      delivery_address3_temp = job:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp  = job:address_line3_delivery
      delivery_address4_temp  = job:postcode_delivery
  End
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job:account_number
  access:subtracc.fetch(sub:account_number_key)
  access:tradeacc.clearkey(tra:account_number_key)
  tra:account_number = sub:main_account_number
  access:tradeacc.fetch(tra:account_number_key)
  if tra:invoice_sub_accounts = 'YES'
      If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = job:address_line1
          invoice_address2_temp     = job:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = job:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job:address_line3
              invoice_address4_temp  = job:postcode
          End
          invoice_company_name_temp   = job:company_name
          invoice_telephone_number_temp   = job:telephone_number
          invoice_fax_number_temp = job:fax_number
          if job:title = '' and job:initial = ''
              customer_name_temp = clip(job:surname)
          elsif job:title = '' and job:initial <> ''
              customer_name_temp = clip(job:initial) & ' ' & clip(job:surname)
          elsif job:title <> '' and job:initial = ''
              customer_name_temp = clip(job:title) & ' ' & clip(job:surname)
          elsif job:title <> '' and job:initial <> ''
              customer_name_temp = clip(job:title) & ' ' & clip(job:initial) & ' ' & clip(job:surname)
          else
              customer_name_temp = ''
          end
  
      Else!If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = sub:address_line1
          invoice_address2_temp     = sub:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = sub:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = sub:address_line3
              invoice_address4_temp  = sub:postcode
          End
          invoice_company_name_temp   = sub:company_name
          invoice_telephone_number_temp   = sub:telephone_number
          invoice_fax_number_temp = sub:fax_number
          customer_name_temp = sub:contact_name
  
      End!If sub:invoice_customer_address = 'YES'
  
  else!if tra:use_sub_accounts = 'YES'
      If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = job:address_line1
          invoice_address2_temp     = job:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = job:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job:address_line3
              invoice_address4_temp  = job:postcode
          End
          invoice_company_name_temp   = job:company_name
          invoice_telephone_number_temp   = job:telephone_number
          invoice_fax_number_temp = job:fax_number
          if job:title = '' and job:initial = ''
              customer_name_temp = clip(job:surname)
          elsif job:title = '' and job:initial <> ''
              customer_name_temp = clip(job:initial) & ' ' & clip(job:surname)
          elsif job:title <> '' and job:initial = ''
              customer_name_temp = clip(job:title) & ' ' & clip(job:surname)
          elsif job:title <> '' and job:initial <> ''
              customer_name_temp = clip(job:title) & ' ' & clip(job:initial) & ' ' & clip(job:surname)
          else
              customer_name_temp = ''
          end
  
      Else!If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = tra:address_line1
          invoice_address2_temp     = tra:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = tra:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = tra:address_line3
              invoice_address4_temp  = tra:postcode
          End
          invoice_company_name_temp   = tra:company_name
          invoice_telephone_number_temp   = tra:telephone_number
          invoice_fax_number_temp = tra:fax_number
          customer_name_temp = tra:contact_name
      End!If tra:invoice_customer_address = 'YES'
  End!!if tra:use_sub_accounts = 'YES'
  
  Total_Price('I',vat",total",balance")
  vat_temp = vat"
  total_temp = total"
  ! Inserting (DBH 18/05/2007) # 9024 - Show relevant costs for site
  If glo:WebJob
      tmp:LabourCost = jobe:InvRRCCLabourCost
      tmp:PartsCost = jobe:InvRRCCPartsCost
      tmp:CourierCost = job:Invoice_Courier_Cost
  Else ! If glo:WebJob
      tmp:LabourCost = job:Invoice_Labour_Cost
      tmp:PartsCost = job:Invoice_Parts_Cost
      tmp:CourierCOst = job:Invoice_Courier_Cost
  End ! If glo:WebJob
  Sub_Total_Temp = tmp:LabourCost + tmp:PartsCost + tmp:CourierCost
  ! End (DBH 18/05/2007) #9024
  
  access:users.clearkey(use:user_code_key)
  use:user_code = job:despatch_user
  If access:users.fetch(use:user_code_key) = Level:Benign
      despatched_user_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  End!If access:users.fetch(use:user_code_key) = Level:Benign
  
  !Payments
  x# = 0
  save_jpt_id = access:jobpaymt.savefile()
  access:jobpaymt.clearkey(jpt:all_date_key)
  jpt:ref_number = job:ref_number
  set(jpt:all_date_key,jpt:all_date_key)
  loop
      if access:jobpaymt.next()
         break
      end !if
      if jpt:ref_number <> job:Ref_number      |
          then break.  ! end if
      x# += 1
      If x# > 0 And x# < 5
          payment_date_temp[x#] = jpt:date
          payment_type_temp[x#] = jpt:payment_type
          payment_amount_temp[x#] = jpt:amount
      Else
          other_amount_temp += jpt:amount
      End!If x# < 5
      total_paid_temp += jpt:amount
  end !loop
  access:jobpaymt.restorefile(save_jpt_id)
  
  settarget(report)
  If other_amount_temp <> 0
      Unhide(?others)
  Else!If other_amount_temp <> 0
      Hide(?others)
  End!If other_amount_temp <> 0
  settarget()
  
  
  code_temp            = 3
  option_temp          = 0
  bar_code_string_temp = Clip(job:ref_number)
  job_number_temp      = 'Job No: ' & Clip(job:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job:esn)
  esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  
  ! Inserting (DBH 24/07/2007) # 9141 - Show suffix on Invoice Number
  If f:Suffix <> '' And f:Prefix = ''
      ! Printing an invoice (DBH: 27/07/2007)
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = job:Ref_Number
      jov:Type = 'I'
      jov:Suffix = f:Suffix
      If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Found
      Else ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Error
      End ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
      tmp:InvoiceNumber = Clip(tmp:InvoiceNumber) & Clip(f:Suffix)
  End ! If f:Suffix <> ''
  If f:Prefix <> ''
      ! Printing a Credit Note (DBH: 27/07/2007)
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = job:Ref_Number
      jov:Type = 'C'
      jov:Suffix = f:Suffix
      If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Found
      Else ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Error
      End ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
      tmp:InvoiceNumber = 'CN' & Clip(tmp:InvoiceNumber) & Clip(f:Suffix)
  End ! If f:Prefix <> ''
  
  
  If f:Suffix <> ''
      Total_Temp = jov:NewTotalCost
  End ! If f:Suffix <> ''
  If f:Prefix <> ''
      Total_Temp = -jov:CreditAmount
      SetTarget(Report)
      ?Text:Invoice{prop:Text} = 'CREDIT NOTE'
      SetTarget()
  End ! If f:Prefix <> ''
  ! End (DBH 24/07/2007) #9141
  !*********CHANGE LICENSE ADDRESS*********
  set(defaults)
  access:defaults.next()
  Access:WEBJOB.Clearkey(wob:RefNumberKey)
  wob:RefNumber   = job:Ref_Number
  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Found
      if glo:WebJob then
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = wob:HeadAccountNumber
          IF access:tradeacc.fetch(tra:account_number_key)
            !Error!
          END
          address:Name            = tra:Company_Name
          address:AddressLine1    = tra:Address_Line1
          address:AddressLine2    = tra:Address_Line2
          address:AddressLine3    = tra:Address_Line3
          address:Postcode        = tra:Postcode
          address:Telephone       = tra:Telephone_Number
          address:Fax             = tra:Fax_Number
          address:EmailAddress    = tra:EmailAddress
      Else
          address:Name            = def:User_Name
          address:AddressLine1    = def:Address_Line1
          address:AddressLine2    = def:Address_Line2
          address:AddressLine3    = def:Address_Line3
          address:Postcode        = def:Postcode
          address:Telephone       = def:Telephone_Number
          address:Fax             = def:Fax_Number
          address:EmailAddress    = def:EmailAddress
      end
  
  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Error
  End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Single_Invoice'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END













Summary_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
count_temp           LONG
tmp:PrintedBy        STRING(255)
save_job_id          USHORT,AUTO
first_page_temp      LONG(1)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
account_number_temp  STRING(15)
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
labour_total_temp    REAL
parts_total_temp     REAL
carriage_total_temp  REAL
vat_total_temp       REAL
grand_total_temp     REAL
total_lines_temp     STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(SUBTRACC)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(458,2083,10938,4448),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),LANDSCAPE,THOUS
                       HEADER,AT(438,323,10917,1719),USE(?unnamed)
                         STRING(@s30),AT(104,104,3438,260),USE(Default_Invoice_Company_Name_Temp),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(3750,260),USE(Invoice_Name_Temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,365,3438,260),USE(Default_Invoice_Address_Line1_Temp),TRN,FONT(,9,,)
                         STRING(@s30),AT(3750,417),USE(Address_Line1_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,521,3438,260),USE(Default_Invoice_Address_Line2_Temp),TRN,FONT(,9,,)
                         STRING('Date Of Invoice:'),AT(7448,729),USE(?String44),TRN,FONT(,8,,)
                         STRING(@d6b),AT(8594,729),USE(inv:Date_Created),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,677,3438,260),USE(Default_Invoice_Address_Line3_Temp),TRN,FONT(,9,,)
                         STRING('Account Number:'),AT(7448,885),USE(?String45),TRN,FONT(,8,,)
                         STRING(@s15),AT(8594,885),USE(inv:Account_Number),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(104,833,1156,156),USE(Default_Invoice_Postcode_Temp),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(104,1042),USE(?String16),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,1042),USE(Default_Invoice_Telephone_Number_Temp),FONT(,9,,)
                         STRING(@s30),AT(3750,729),USE(Address_Line3_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Fax: '),AT(104,1198),USE(?String19),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,1198),USE(Default_Invoice_Fax_Number_Temp),FONT(,9,,)
                         STRING('Tel:'),AT(3750,1042),USE(?String47),TRN,FONT(,8,,)
                         STRING(@s15),AT(4010,1042),USE(job:Telephone_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('V.A.T. Number:'),AT(7448,1042),USE(?String46),TRN,FONT(,8,,)
                         STRING(@s30),AT(8594,1042),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(7448,1198),USE(?String46:2),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(3750,1198),USE(?String48),TRN,FONT(,8,,)
                         STRING(@s15),AT(4010,1198),USE(job:Fax_Number),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(9063,1198,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s255),AT(521,1354,3438,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(104,1354),USE(?String19:2),TRN,FONT(,8,,)
                         STRING(@n-7),AT(8594,1198),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(8854,1198,156,260),USE(?String46:3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(3750,885),USE(Address_Line4_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Invoice Number:'),AT(7448,573),USE(?String43),TRN,FONT(,8,,)
                         STRING(@s10),AT(8594,573),USE(inv:Invoice_Number),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(3750,573),USE(Address_Line2_Temp),TRN,FONT(,8,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s20),AT(52,0),USE(tmp:Ref_Number),TRN,RIGHT,FONT(,8,,)
                           STRING(@s17),AT(1063,0),USE(job:Order_Number),TRN,FONT(,8,,)
                           STRING(@s25),AT(2135,0,1354,208),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s25),AT(3542,0),USE(job:Manufacturer),TRN,FONT(,8,,)
                           STRING(@s16),AT(5156,0),USE(job:ESN),TRN,FONT(,8,,)
                           STRING(@s16),AT(6198,0),USE(job:MSN),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(7240,0),USE(labour_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(8125,0),USE(parts_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(9010,0),USE(courier_cost_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(9896,0),USE(line_cost_temp),TRN,RIGHT,FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,1594),USE(?unnamed:2),ABSOLUTE
                           STRING('Number Of Lines:'),AT(156,52),USE(?String54),TRN,FONT(,,,FONT:bold)
                           STRING(@s9),AT(1458,52),USE(count_temp),TRN,LEFT
                           STRING('Labour:'),AT(7490,365),USE(?String49),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(9896,365),USE(labour_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Parts:'),AT(7490,521),USE(?String50),TRN,FONT(,8,,)
                           STRING('Carriage:'),AT(7490,677),USE(?String51),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(9896,521),USE(parts_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(7490,833),USE(?String52),TRN,FONT(,8,,)
                           STRING('Total:'),AT(7490,1042),USE(?String53),TRN,FONT(,8,,FONT:bold)
                           STRING(@n14.2),AT(9896,677),USE(carriage_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(9896,833),USE(vat_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(9896,1042),USE(grand_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('<128>'),AT(9844,1042),USE(?Euro),TRN,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
Title                  DETAIL,PAGEBEFORE(-1),AT(,,,52),USE(?Title)
                       END
                       FOOTER,AT(448,6927,10917,844),USE(?unnamed:3)
                         TEXT,AT(104,52,7135,729),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(438,417,10917,7417)
                         IMAGE('Rinvlan.gif'),AT(0,0,10938,7448),USE(?Image1)
                         STRING('SUMMARY INVOICE'),AT(8906,104),USE(?String42),TRN,FONT(,14,,FONT:bold)
                         STRING('Order Number'),AT(1073,1458),USE(?String17),TRN,FONT(,8,,FONT:bold)
                         STRING('Job Number'),AT(104,1458),USE(?String18),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(3552,1458),USE(?String20),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(5167,1458),USE(?String21),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6208,1458),USE(?String37),TRN,FONT(,8,,FONT:bold)
                         STRING('Labour'),AT(7677,1458),USE(?String38),TRN,FONT(,8,,FONT:bold)
                         STRING('Parts'),AT(8667,1458),USE(?String39),TRN,FONT(,8,,FONT:bold)
                         STRING('Carriage'),AT(9365,1458),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(10188,1458),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Model'),AT(2156,1458),USE(?String35),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Summary_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Summary_Invoice')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:SUBTRACC.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:INVOICE.UseFile
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(SUBTRACC)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(SUBTRACC,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(sub:Account_Number_Key)
      Process:View{Prop:Filter} = ''
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        parts_total_temp = 0
        labour_total_temp = 0
        carriage_total_temp = 0
        vat_total_temp = 0
        grand_total_temp = 0
        
        
        Sort(glo:q_invoice,glo:account_number)
        Loop x# = 1 To Records(glo:q_invoice)
            Get(glo:q_invoice,x#)
            If glo:account_number <> sub:account_number Then Cycle.
        
            parts_temp = ''
            labour_temp = ''
            courier_cost_temp = ''
            vat_temp = ''
            line_cost_temp = ''
        
            count_temp = 0
            setcursor(cursor:wait)
            save_job_id = access:jobs.savefile()
            access:jobs.clearkey(job:InvoiceNumberKey)
            job:invoice_number = glo:invoice_number
            set(job:InvoiceNumberKey,job:InvoiceNumberKey)
            loop
                if access:jobs.next()
                   break
                end !if
                if job:invoice_number <> glo:invoice_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
        
                !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
                ! 26 Aug 2002 John
                !
                tmp:Ref_number = job:Ref_number
                if glo:WebJob then
                    !Change the tmp ref number if you
                    !can Look up from the wob file
                    access:Webjob.clearkey(wob:RefNumberKey)
                        wob:RefNumber = job:Ref_number
                    if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                        access:tradeacc.clearkey(tra:Account_Number_Key)
                            tra:Account_Number = ClarioNET:Global.Param2
                        access:tradeacc.fetch(tra:Account_Number_Key)
                        tmp:Ref_Number = Job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
                    END
                END
                !--------------------------------------------------------------
        
                access:tradeacc.clearkey(tra:account_number_key)
                tra:account_number = sub:main_account_number
                if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                    if tra:invoice_sub_accounts = 'YES'
                        invoice_name_temp   = sub:company_name
                        address_line1_temp  = sub:address_line1
                        address_line2_temp  = sub:address_line2
                        If sup:address_line3 = ''
                            address_line3_temp = sub:postcode
                            address_line4_temp = ''
                        Else
                            address_line3_temp  = sub:address_line3
                            address_line4_temp  = sub:postcode
                        End
                    else!if tra:use_sub_accounts = 'YES'
                        invoice_name_temp   = tra:company_name
                        address_line1_temp  = tra:address_line1
                        address_line2_temp  = tra:address_line2
                        If sup:address_line3 = ''
                            address_line3_temp = tra:postcode
                            address_line4_temp = ''
                        Else
                            address_line3_temp  = tra:address_line3
                            address_line4_temp  = tra:postcode
                        End
                    end!if tra:use_sub_accounts = 'YES'
                end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
        
                access:invoice.clearkey(inv:invoice_number_key)
                inv:invoice_number = glo:invoice_number
                if access:invoice.fetch(inv:invoice_number_key)
                    Cycle
                end
        
                If first_page_temp = 1
                    account_number_temp = sub:account_number
                    first_page_temp = 0
                End!If first_page_temp = 1
        
                If account_number_temp <> sub:account_number
                    Print(rpt:title)
                    account_number_temp = sub:account_number
                End!If account_number_temp <> sub:account_number
        
                parts_temp = job:invoice_parts_cost
                labour_temp = job:invoice_labour_cost
                courier_cost_temp = job:invoice_courier_cost
        !        vat_temp = Round((job:invoice_parts_cost * inv:vat_rate_labour/100),.01) + |
        !                    Round((job:invoice_labour_cost * inv:vat_rate_labour/100),.01) +|
        !                    Round((job:invoice_courier_cost * inv:vat_rate_labour/100),.01)
                vat_temp = (job:invoice_parts_cost * inv:vat_rate_labour/100) + |
                            (job:invoice_labour_cost * inv:vat_rate_labour/100) +|
                            (job:invoice_courier_cost * inv:vat_rate_labour/100)
        
                line_cost_temp = parts_temp + labour_temp + courier_cost_temp
        
                parts_total_temp += job:invoice_parts_cost
                labour_total_temp += job:invoice_labour_cost
                carriage_total_temp += job:invoice_courier_cost
        
        !        vat_total_temp = Round((parts_total_temp * inv:vat_rate_labour/100),.01) + |
        !                    Round((labour_total_temp * inv:vat_rate_labour/100),.01) +|
        !                    Round((carriage_total_temp * inv:vat_rate_labour/100),.01)
                vat_total_temp = (parts_total_temp * inv:vat_rate_labour/100) + |
                            (labour_total_temp * inv:vat_rate_labour/100) +|
                            (carriage_total_temp * inv:vat_rate_labour/100)
        
                grand_total_temp = vat_total_temp + (parts_total_temp + labour_total_temp + carriage_total_temp)
        
                count_temp += 1
                Print(rpt:detail)
        
        
            end !loop
            access:jobs.restorefile(save_job_id)
            setcursor()
        
        End!Loop x# = 1 To Records(glo:q_invoice)
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(SUBTRACC,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Summary_Invoice')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'SUBTRACC')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  !--- Webify SETUP ClarioNET:UseReportPreview(0) ---------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  END
  !--------------------------------------------------------------
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvlan.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Summary_Invoice'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END













Summary_Warranty_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Invoice_Company_Temp STRING(30)
courier_vat_temp     REAL
total_temp           REAL
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
Invoice_Number_Temp  STRING(28)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Labour_vat_info_temp STRING(15)
Parts_vat_info_temp  STRING(15)
batch_number_temp    STRING(28)
labour_vat_temp      REAL
parts_vat_temp       REAL
balance_temp         REAL
vat_temp             REAL
vat_total_temp       REAL
sub_total_temp       REAL
tmp:VATNumber        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Account_Number)
                       PROJECT(inv:Batch_Number)
                       PROJECT(inv:Courier_Paid)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Invoice_Number)
                       PROJECT(inv:Labour_Paid)
                       PROJECT(inv:Parts_Paid)
                       PROJECT(inv:jobs_count)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,4573,7521,4271),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,3604),USE(?unnamed:3)
                         STRING('Date Of Invoice:'),AT(4948,781),USE(?String86),TRN,FONT(,8,,)
                         STRING('Invoice Number: '),AT(4948,365),USE(?String87),TRN,FONT(,8,,)
                         STRING(@s10),AT(5885,365),USE(inv:Invoice_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(156,1302),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('V.A.T. No: '),AT(4948,573),USE(?String102),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,573),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1302,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING(@d6b),AT(5885,781),USE(inv:Date_Created),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(1771,3229),USE(inv:Date_Created,,?INV:Date_Created:2),TRN,LEFT,FONT(,8,,)
                         STRING(@p<<<<<<#p),AT(4844,3229,469,208),USE(inv:Batch_Number),TRN,RIGHT,FONT(,8,,)
                         STRING('WARRANTY'),AT(6042,3229),USE(?String71),TRN,FONT(,8,,)
                         STRING(@p<<<<<<<<#p),AT(3385,3229),USE(inv:Invoice_Number,,?INV:Invoice_Number:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(208,2240),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(208,2396),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(625,2396),USE(job:Telephone_Number),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(1917,2396),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2198,2396),USE(job:Fax_Number),TRN,LEFT,FONT(,8,,)
                         STRING('Vat No:'),AT(208,2552),USE(?String34:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(625,2552),USE(tmp:VATNumber),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3229),USE(inv:Account_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(208,1615),USE(Invoice_Company_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(208,1760),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(208,2083),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(208,1917),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@n14.2),AT(3854,0),USE(inv:Labour_Paid),TRN,RIGHT
                           STRING(@n14.2),AT(5052,0),USE(inv:Parts_Paid),TRN,RIGHT
                           STRING(@n14.2),AT(6146,0),USE(vat_total_temp,,?vat_total_temp:2),TRN,RIGHT
                           STRING(@n14.2),AT(2708,0),USE(inv:Courier_Paid,,?INV:Courier_Paid:2),TRN,RIGHT
                           STRING('WARRANTY'),AT(1635,0),USE(?String72),TRN
                           STRING(@s8),AT(208,0),USE(inv:jobs_count),TRN,RIGHT
                         END
                         FOOTER,AT(396,9125,,1458),USE(?unnamed:2),TOGETHER,ABSOLUTE
                           STRING('Despatch Date:'),AT(208,208),USE(?String66),TRN,FONT(,8,,)
                           STRING('Courier: '),AT(208,365),USE(?String67),TRN,FONT(,8,,)
                           STRING('Labour Cost:'),AT(4844,208,885,208),USE(?String90),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6406,52,833,188),USE(inv:Courier_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6406,208),USE(inv:Labour_Paid,,?INV:Labour_Paid:2),TRN,RIGHT,FONT(,8,,)
                           STRING('Consignment No:'),AT(208,521),USE(?String70),TRN,FONT(,8,,)
                           STRING('Despatched By:'),AT(208,677),USE(?String68),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6406,365),USE(inv:Parts_Paid,,?INV:Parts_Paid:2),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(4844,677),USE(?String94),TRN,FONT(,8,,)
                           STRING('Total:'),AT(4844,833),USE(?String95),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(6302,781,1000,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING(@n14.2),AT(6406,833),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('<128>'),AT(6344,833),USE(?Euro),TRN,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(6302,469,1000,0),USE(?Line2:2),COLOR(COLOR:Black)
                           STRING(@n14.2),AT(6406,677),USE(vat_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Sub Total:'),AT(4844,521),USE(?String75),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6406,521),USE(sub_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Exchange Fee'),AT(4844,52),USE(?String65),TRN,FONT(,8,,)
                           STRING('Parts Cost:'),AT(4844,365),USE(?String92),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1156)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(406,469,7521,11198),USE(?unnamed)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,83,3844,240),USE(def:Invoice_Company_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('INVOICE'),AT(5604,0),USE(?String20),TRN,FONT(,20,,FONT:bold)
                         STRING(@s30),AT(156,323,3844,156),USE(def:Invoice_Address_Line1),TRN
                         STRING(@s30),AT(156,479,3844,156),USE(def:Invoice_Address_Line2),TRN
                         STRING(@s30),AT(156,635,3844,156),USE(def:Invoice_Address_Line3),TRN
                         STRING(@s15),AT(156,802,1156,156),USE(def:Invoice_Postcode),TRN
                         STRING('Tel:'),AT(156,958),USE(?String16),TRN
                         STRING(@s15),AT(677,958),USE(def:Invoice_Telephone_Number)
                         STRING('Fax: '),AT(156,1094),USE(?String19),TRN
                         STRING(@s15),AT(677,1094),USE(def:Invoice_Fax_Number)
                         STRING('Email:'),AT(156,1250),USE(?String19:2),TRN
                         STRING(@s255),AT(677,1250,3844,208),USE(def:InvoiceEmailAddress),TRN
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY DETAILS'),AT(156,8479),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?String74),TRN,FONT(,9,,FONT:bold)
                         STRING('No Of Jobs'),AT(156,3854),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(208,3177),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Invoice Date'),AT(1667,3177),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Invoice Number'),AT(3125,3177),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Batch Number'),AT(4583,3177),USE(?BatchNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(6042,3177),USE(?String40:6),TRN,FONT(,8,,FONT:bold)
                         STRING('Charge Type'),AT(1635,3854),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Exchange Fee'),AT(3031,3854),USE(?String69),TRN,FONT(,8,,FONT:bold)
                         STRING('Labour Cost'),AT(4208,3854),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('Parts Cost'),AT(5510,3854),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('V.A.T.'),AT(6875,3854),USE(?String44),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Summary_Warranty_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Summary_Warranty_Invoice')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(RPT:DETAIL)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Summary_Warranty_Invoice')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  !Is this a OBF Invoice?
  If inv:Batch_Number = 0
      SetTarget(Report)
      ?inv:Batch_Number{prop:Hide} = 1
      ?BatchNumber{prop:Hide} = 1
  End !inv:Batch_Number = 0
  
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  
  If inv:Batch_Number = 0
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = inv:Account_Number
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          invoice_company_temp   = tra:company_name
          invoice_address1_temp  = tra:address_line1
          invoice_address2_temp  = tra:address_line2
          If tra:address_line3 = ''
              invoice_address3_temp = tra:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = tra:address_line3
              invoice_address4_temp  = tra:postcode
          End
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  Else !inv:Batch_Number = 0
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = inv:account_number
      if access:subtracc.fetch(sub:account_number_key) = level:benign
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = sub:main_account_number
          if access:tradeacc.fetch(tra:account_number_key) = level:benign
              if tra:invoice_sub_accounts = 'YES'
                  invoice_company_temp   = sub:company_name
                  invoice_address1_temp  = sub:address_line1
                  invoice_address2_temp  = sub:address_line2
                  If sub:address_line3 = ''
                      invoice_address3_temp = sub:postcode
                      invoice_address4_temp = ''
                  Else
                      invoice_address3_temp  = sub:address_line3
                      invoice_address4_temp  = sub:postcode
                  End
  
              else!if tra:use_sub_accounts = 'YES'
                  invoice_company_temp   = tra:company_name
                  invoice_address1_temp  = tra:address_line1
                  invoice_address2_temp  = tra:address_line2
                  If tra:address_line3 = ''
                      invoice_address3_temp = tra:postcode
                      invoice_address4_temp = ''
                  Else
                      invoice_address3_temp  = tra:address_line3
                      invoice_address4_temp  = tra:postcode
                  End
  
              end!if tra:use_sub_accounts = 'YES'
          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
      end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  
  End !inv:Batch_Number = 0
  
  ! Start - Show the Manufacturer's Vat Number - TrkBs: 5364 (DBH: 26-04-2005)
  Access:MANUFACT.ClearKey(man:Manufacturer_Key)
  man:Manufacturer = inv:Manufacturer
  If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      !Found
      tmp:VatNumber = man:VatNumber
  Else !If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      !Error
  End !If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
  ! End   - Show the Manufacturer's Vat Number - TrkBs: 5364 (DBH: 26-04-2005)
  
  
  
  
  labour_vat_temp      = Round(inv:labour_paid * (INV:Vat_Rate_labour/100),.01)
  parts_vat_temp      = Round(inv:parts_paid * (INV:Vat_Rate_Parts/100),.01)
  courier_vat_temp     = Round(inv:courier_paid * (inv:vat_rate_labour/100),.01)
  sub_total_temp       = Round(inv:labour_paid,.01) + Round(inv:parts_paid,.01) + Round(inv:courier_paid,.01)
  vat_total_temp      = Round(labour_vat_temp,.01) + Round(parts_vat_temp,.01) + Round(courier_vat_temp,.01)
  total_temp           = Round(inv:labour_paid,.01) + Round(inv:parts_paid,.01) + Round(vat_total_temp,.01) + Round(inv:courier_paid,.01)
  
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Summary_Warranty_Invoice'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END













Retail_Picking_Note PROCEDURE(func:type)
RejectRecord         LONG,AUTO
tmp:RecordsCount     LONG
save_res_id          USHORT,AUTO
tmp:PrintedBy        STRING(60)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Address_Group        GROUP,PRE()
Postcode             STRING(10)
Company_Name         STRING(30)
Building_Name        STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Address_Delivery_Group GROUP,PRE()
Postcode_Delivery    STRING(10)
Company_Name_Delivery STRING(30)
Building_Name_Delivery STRING(30)
Address_Line1_Delivery STRING(30)
Address_Line2_Delivery STRING(30)
Address_Line3_Delivery STRING(30)
Telephone_Delivery   STRING(15)
Fax_Number_Delivery  STRING(15)
                     END
Purchase_Order_Number STRING(30)
Account_Number       STRING(15)
Stock_queue          QUEUE,PRE(STOQUE)
Ref_Number           REAL
Location             STRING(30)
Shelf_Location       STRING(30)
Second_Location      STRING(30)
Quantity             REAL
                     END
Delivery_Name_Temp   STRING(30)
Delivery_Address_Line_1_Temp STRING(60)
Invoice_Name_temp    STRING(30)
Invoice_address_Line_1 STRING(60)
items_total_temp     REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Type             STRING(4)
!-----------------------------------------------------------------------------
Process:View         VIEW(RETSALES)
                       PROJECT(ret:Contact_Name)
                       PROJECT(ret:date_booked)
                       PROJECT(ret:time_booked)
                       PROJECT(ret:Ref_Number)
                       JOIN(res:Part_Number_Key,ret:Ref_Number)
                       END
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,3875,7521,6010),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(354,760,7521,2740),USE(?unnamed)
                         STRING('Date Printed:'),AT(5000,333),USE(?string22),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@d6b),AT(5885,333),USE(ret:date_booked),TRN,LEFT,FONT('Arial',8,,FONT:bold)
                         STRING(@t1),AT(6615,333),USE(ret:time_booked),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Ordered'),AT(5000,156),USE(?String65),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@d6b),AT(5885,156),USE(orh:thedate),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6594,156),USE(orh:thetime),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2240),USE(?string22:4),TRN,FONT(,8,,)
                         STRING('Sales Number:'),AT(5000,500),USE(?string22:2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Account No:'),AT(5000,667),USE(?string22:3),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(5885,500),USE(GLO:Select1),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Purch Order No:'),AT(5000,833),USE(?string22:6),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s15),AT(5885,667),USE(Account_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Contact Name:'),AT(5000,990),USE(?string22:7),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(5885,833),USE(Purchase_Order_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5885,990),USE(ret:Contact_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1458),USE(Delivery_Name_Temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1458),USE(Invoice_Name_temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(208,1615,3438,208),USE(Delivery_Address_Line_1_Temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(4063,1615,3281,208),USE(Invoice_address_Line_1),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1771),USE(Address_Line2_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1771),USE(Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(521,2240),USE(Telephone_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(4063,2240),USE(?string22:9),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2240),USE(Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(208,2396),USE(?string22:5),TRN,FONT(,8,,)
                         STRING(@s10),AT(208,2083),USE(Postcode_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s10),AT(4063,2083),USE(Postcode),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1927),USE(Address_Line3_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1927),USE(Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(521,2396),USE(Fax_Number_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(4063,2396),USE(?string22:8),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2396),USE(Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
break1                 BREAK(RES:Ref_Number),USE(?unnamed:5)
detail1                  DETAIL,AT(,,,188),USE(?unnamed:6)
                           STRING(@s20),AT(167,0),USE(sto:Part_Number,,?sto:Part_Number:2),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s25),AT(1458,0),USE(sto:Description,,?sto:Description:2),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s4),AT(3073,0),USE(tmp:Type,,?tmp:Type:2),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s6),AT(6771,0),USE(STOQUE:Quantity),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           BOX,AT(7240,0,156,156),USE(?Box1),COLOR(COLOR:Black)
                           STRING(@s14),AT(5781,0),USE(sto:Second_Location),FONT(,8,,)
                           STRING(@s18),AT(3385,0),USE(sto:Location),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s18),AT(4583,0),USE(sto:Shelf_Location),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,510),USE(?unnamed:7)
                           LINE,AT(208,208,7083,0),USE(?line1),COLOR(COLOR:Black)
                           STRING('Total No Of Lines:'),AT(208,260),USE(?string26),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(1302,260),USE(tmp:RecordsCount),TRN,FONT(,8,,FONT:bold)
                           STRING('Total No Of Items:'),AT(2396,260),USE(?string26:2),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(3438,260),USE(items_total_temp),TRN,DECIMAL(14),FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(385,10198,7521,958),USE(?unnamed:4)
                         TEXT,AT(208,52,7083,781),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(354,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:OrderCompanyName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('PICKING NOTE'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:OrderAddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:OrderAddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:OrderAddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(def:OrderPostcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,990),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,990),USE(def:OrderTelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1146),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1146),USE(def:OrderFaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1302,3844,198),USE(def:OrderEmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1302),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Qty'),AT(6979,3177),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('2nd Location'),AT(5781,3177),USE(?string24:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Site Location'),AT(3385,3177),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Shelf Location'),AT(4583,3177),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Part Number'),AT(167,3177),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Description'),AT(1458,3177),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Type'),AT(3073,3177),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Picked By:'),AT(240,9510),USE(?String54),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Name (Capitals):'),AT(2792,9510),USE(?String55),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Picked:'),AT(5813,9510),USE(?String56),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(260,9688,7031,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING('DELIVERY ADDRESS'),AT(208,1521),USE(?string44:2),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(4063,1521),USE(?string44:3),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Retail_Picking_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Retail_Picking_Note')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'PickingNote'
  If PrintOption(PreviewReq,glo:ExportReport,'Picking Note') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:RETSALES.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:ORDHEAD.Open
  Relate:STANTEXT.Open
  Relate:STOCK.Open
  Access:RETSTOCK.UseFile
  
  
  RecordsToProcess = RECORDS(RETSALES)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(RETSALES,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = Records(retstock)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        access:retsales.clearkey(ret:ref_number_key)
        ret:ref_number = glo:select1
        if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
        
            If func:Type = 'ALL'
        
            End!If func:Type = 'ALL'
            save_res_id = access:retstock.savefile()
        
            If func:Type = 'ALL'
                access:retstock.clearkey(res:Part_Number_key)
                res:ref_number = ret:ref_number
                Set(res:Part_Number_Key,res:Part_Number_Key)
            Else
                access:retstock.clearkey(res:Despatched_Only_Key)
                res:ref_number = ret:ref_number
                res:despatched = func:type
                Set(res:Despatched_Only_Key,res:Despatched_Only_Key)
            End!If func:Type = 'ALL'
        
            loop
                if access:retstock.next()
                   break
                end !if
                if res:ref_number <> ret:ref_number
                    Break
                End!if res:ref_number <> ret:ref_number
                If func:Type <> 'ALL'
                    If res:despatched <> func:Type
                        Break
                    End!If res:despatched <> func:Type
                End!If func:Type <> 'ALL'
                tmp:RecordsCount += 1
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = res:part_ref_number
                access:stock.tryfetch(sto:ref_number_key)
        
                Sort(Stock_Queue,stoque:Ref_Number)
                stoque:Ref_Number   = sto:Ref_Number
                Get(Stock_Queue,stoque:Ref_Number)
                If Error()
                    stoque:ref_number   = sto:ref_number
                    stoque:location     = sto:location
                    stoque:shelf_location   = sto:shelf_location
                    stoque:second_location  = sto:second_location
                    stoque:quantity = res:quantity
                    Add(Stock_Queue)
                Else !If Error()
                    stoque:Quantity += res:Quantity
                    Put(Stock_Queue)
                End !If Error()
            end !loop
            access:retstock.restorefile(save_res_id)
        
            RecordsToProcess += Records(Stock_queue)
        
            Sort(Stock_queue,stoque:location,stoque:shelf_location,stoque:second_location)
            Loop x# = 1 To Records(Stock_queue)
                Get(stock_queue,x#)
                RecordsProcessed += 1
                Do Displayprogress
        
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number  = stoque:ref_number
                If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                    items_total_temp    += stoque:quantity
                    Print(rpt:detail1)
                End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            End!Loop x# = 1 To Records(Stock_queue)
        end!if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RETSALES,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','Retail_Picking_Note')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:ORDHEAD.Close
    Relate:RETSALES.Close
    Relate:STANTEXT.Close
    Relate:STOCK.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'PICKING NOTE - RETAIL'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  ! Inserting (DBH 20/10/2006) # 8381 - Allow to set how many copies are printed
  If glo:ReportCopies > 0
      printer{propprint:Copies} = glo:ReportCopies
      glo:ReportCopies = 0
  End ! If glo:ReportCopies > 0
  ! End (DBH 20/10/2006) #8381
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  access:retsales.clearkey(ret:ref_number_key)
  ret:ref_number = glo:select1
  if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      If ret:Payment_Method = 'EXC'
          tmp:Type = 'EXCH'
      Else !If ret:Payment_Method = 'EXC'
          tmp:Type = 'SALE'
      End !If ret:Payment_Method = 'EXC'
  
      If ret:payment_method = 'CAS'
          Delivery_Name_Temp  = RET:Contact_Name
          Invoice_Name_Temp   = ret:contact_name
      Else!If ret:payment_method = 'CAS'
          Delivery_Name_Temp  = RET:Company_Name_Delivery
          invoice_name_temp   = ret:company_name
      End!If ret:payment_method = 'CAS'
      If ret:building_name    = ''
          Invoice_address_Line_1     = Clip(RET:Address_Line1)
      Else!    If ret:building_name    = ''
          Invoice_address_Line_1     = Clip(RET:Building_Name) & ' ' & Clip(RET:Address_Line1)
      End!
      If ret:building_name_delivery   = ''
          Delivery_Address_Line_1_Temp    = Clip(RET:Address_Line1_Delivery)
      Else!    If ret:building_name_delivery   = ''
          Delivery_Address_Line_1_Temp    = Clip(RET:Building_Name_Delivery) & ' ' & Clip(RET:Address_Line1_Delivery)
      End!    If ret:building_name_delivery   = ''
  
  
      Postcode= ret:Postcode
      Company_Name=ret:Company_Name
      Building_Name=ret:Building_Name
      Address_Line1=ret:Address_Line1
      Address_Line2=ret:Address_Line2
      Address_Line3=ret:Address_Line3
      Telephone_Number=ret:Telephone_Number
      Fax_Number=ret:Fax_Number
  
  
      Postcode_Delivery= ret:Postcode_Delivery
      Company_Name_Delivery=ret:Company_Name_Delivery
      Building_Name_Delivery=ret:Building_Name_Delivery
      Address_Line1_Delivery=ret:Address_Line1_Delivery
      Address_Line2_Delivery=ret:Address_Line2_Delivery
      Address_Line3_Delivery=ret:Address_Line3_Delivery
      Telephone_Delivery=ret:Telephone_Delivery
      Fax_Number_Delivery=ret:Fax_Number_Delivery
      Purchase_Order_Number = ret:purchase_order_number
      Account_Number = ret:account_number
  
      !TB12539 - J - 13/02/14 - need the ordering date from ordhead
      Access:ORDHEAD.ClearKey(orh:KeyOrder_no)
      orh:Order_no = ret:WebOrderNumber
      Access:ordhead.TryFetch(orh:KeyOrder_no)
      !End TB12539
  
      ! Inserting (DBH 08/11/2006) # 8308 - Save the date the picking note is printed
      If ret:DatePickingNotePrinted = ''
          ret:DatePickingNotePrinted = Today()
          ret:TimePickingNotePrinted = Clock()
          Access:RETSALES.Update()
      End ! If ret:DatePickingNotePrinted = ''
      ! End (DBH 08/11/2006) #8308
  
  
  End !access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Retail_Picking_Note'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













Credit_Note PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
tmp:Default_Invoice_Company_Name STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_ivp_id          USHORT,AUTO
tmp:Default_Invoice_Address_Line1 STRING(30)
tmp:Default_Invoice_Address_Line2 STRING(30)
tmp:Default_Invoice_Address_Line3 STRING(30)
tmp:Default_Invoice_Postcode STRING(15)
tmp:Default_Invoice_Telephone_Number STRING(15)
tmp:Default_Invoice_Fax_Number STRING(15)
tmp:Default_Invoice_VAT_Number STRING(30)
RejectRecord         LONG,AUTO
tmp:printedby        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
save_rtp_id          USHORT,AUTO
tmp:vat_rate         REAL
tmp:Delivery_Name    STRING(30)
tmp:Delivery_Address_Line_1 STRING(30)
save_res_id          USHORT,AUTO
tmp:Invoice_Name     STRING(30)
tmp:Invoice_address_L STRING(30)
tmp:items_total      REAL
tmp:vat              REAL
tmp:item_cost        REAL
tmp:line_cost        REAL
tmp:despatch_note_number STRING(10)
tmp:line_cost_total  REAL
tmp:vat_total        REAL
tmp:Total            REAL
tmp:payment_date     DATE,DIM(4)
tmp:other_amount     REAL
tmp:total_paid       REAL
tmp:payment_type     STRING(20),DIM(4)
tmp:payment_amount   REAL,DIM(4)
tmp:sub_total        REAL
tmp:courier_cost     REAL
tmp:PartsCost        REAL
tmp:labour_cost      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Invoice_Number)
                       PROJECT(inv:PrevInvoiceNo)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,4573,7521,4260),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(344,427,7521,10896),USE(?unnamed)
                         STRING('Date Printed:'),AT(5000,938),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5990,938),USE(ReportRunDate),TRN,LEFT,FONT('Arial',8,,FONT:bold)
                         STRING('Tel:'),AT(4063,2500),USE(?string22:4),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,781),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@s255),AT(5990,781),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('CREDIT NOTE:'),AT(4792,52),USE(?string22:3),TRN,FONT(,16,,FONT:bold)
                         STRING(@s8),AT(6354,52),USE(inv:Invoice_Number),TRN,LEFT,FONT('Arial',16,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5990,469),USE(tmp:Default_Invoice_VAT_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('V.A.T. Number:'),AT(5000,469),USE(?string22:12),TRN,FONT(,8,,)
                         STRING(@s8),AT(5990,1094),USE(inv:PrevInvoiceNo),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@n3),AT(5990,1250),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6250,1250),USE(?string22:14),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6406,1250,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,1250),USE(?string22:13),TRN,FONT(,8,,)
                         STRING('Date Of Credit:'),AT(5000,625),USE(?String65),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5990,625),USE(inv:Date_Created),TRN,FONT(,8,,FONT:bold)
                         STRING('Original Invoice No:'),AT(5000,1094),USE(?string22:10),TRN,FONT(,8,,)
                         STRING('Account No'),AT(208,3177),USE(?string22:6),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(208,3385),USE(ret:Account_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Purch Order No'),AT(1615,3177),USE(?string22:7),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name'),AT(3125,3177),USE(?string22:11),TRN,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(1615,3385),USE(ret:Purchase_Order_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s20),AT(3125,3385),USE(ret:Contact_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date'),AT(208,8698),USE(?String66),TRN,HIDE,FONT(,7,,FONT:bold)
                         STRING('Type'),AT(708,8698),USE(?String67),TRN,HIDE,FONT(,7,,FONT:bold)
                         STRING('Amount'),AT(1969,8698),USE(?String70:5),TRN,HIDE,FONT(,7,,FONT:bold)
                         STRING(@d6b),AT(156,8854),USE(tmp:payment_date[1]),TRN,HIDE,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,8854),USE(tmp:payment_type[1]),TRN,HIDE,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,8854),USE(tmp:payment_amount[1]),TRN,HIDE,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@d6b),AT(156,9167),USE(tmp:payment_date[4]),TRN,HIDE,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,9167),USE(tmp:payment_type[4]),TRN,HIDE,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,9167),USE(tmp:payment_amount[4]),TRN,HIDE,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         TEXT,AT(2917,8750,1719,781),USE(ret:Invoice_Text),FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(156,8958),USE(tmp:payment_date[2]),TRN,HIDE,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,8958),USE(tmp:payment_type[2]),TRN,HIDE,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,8958),USE(tmp:payment_amount[2]),TRN,HIDE,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@d6b),AT(156,9063),USE(tmp:payment_date[3]),TRN,HIDE,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,9063),USE(tmp:payment_type[3]),TRN,HIDE,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,9063),USE(tmp:payment_amount[3]),TRN,HIDE,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('Others:'),AT(208,9323),USE(?Others),TRN,HIDE,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1823,9323),USE(tmp:other_amount),TRN,HIDE,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         LINE,AT(208,9479,1979,0),USE(?Line4),HIDE,COLOR(COLOR:Black)
                         STRING('Total:'),AT(208,9531),USE(?String109),TRN,HIDE,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1823,9531),USE(tmp:total_paid),TRN,HIDE,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1719),USE(ret:Company_Name_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1719),USE(ret:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1875),USE(tmp:Delivery_Address_Line_1),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1875),USE(tmp:Invoice_address_L),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2031),USE(ret:Address_Line2_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,2031),USE(ret:Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(4271,2500),USE(ret:Telephone_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2500),USE(?string22:9),TRN,FONT(,8,,)
                         STRING(@s15),AT(469,2500),USE(ret:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(4063,2656),USE(?string22:5),TRN,FONT(,8,,)
                         STRING(@s10),AT(4063,2344),USE(ret:Postcode_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s10),AT(208,2344),USE(ret:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2188),USE(ret:Address_Line3_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,2188),USE(ret:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(4271,2656),USE(ret:Fax_Number_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(208,2656),USE(?string22:8),TRN,FONT(,8,,)
                         STRING(@s15),AT(469,2656),USE(ret:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
break2                 BREAK(endofreport),USE(?unnamed:2)
detail2                  DETAIL,AT(,,,208),USE(?unnamed:5)
                           STRING(@n-14),AT(5573,0),USE(ivp:CreditQuantity),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6531,0),USE(tmp:line_cost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(4167,0),USE(ivp:RetailCost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s25),AT(156,0),USE(ivp:PartNumber),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(1771,0),USE(ivp:Description),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(365,9219,7521,1938),USE(?unnamed:4)
                         STRING('Courier Cost:'),AT(4948,0),USE(?String70:4),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6563,0),USE(tmp:courier_cost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@n14.2),AT(6563,156),USE(tmp:PartsCost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Labour Cost:'),AT(4948,313),USE(?String70:7),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6563,313),USE(tmp:labour_cost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Parts Cost:'),AT(4948,156),USE(?String70:6),TRN,FONT(,8,,)
                         STRING('Total (Ex VAT)'),AT(4948,469),USE(?String70),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6563,469),USE(tmp:sub_total),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('V.A.T.:'),AT(4948,625),USE(?String70:2),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6563,625),USE(tmp:vat_total),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(6354,729,1042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total (Inc VAT):'),AT(4948,781),USE(?String70:3),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(6354,417,1042,0),USE(?Line1:2),COLOR(COLOR:Black)
                         STRING(@n14.2),AT(6563,781),USE(tmp:Total),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s3),AT(6354,781),USE(de2:CurrencySymbol),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(104,1094,7292,781),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(354,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,83,3844,240),USE(tmp:Default_Invoice_Company_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,323,3844,156),USE(tmp:Default_Invoice_Address_Line1),TRN
                         STRING(@s30),AT(156,479,3844,156),USE(tmp:Default_Invoice_Address_Line2),TRN
                         STRING(@s30),AT(156,635,3844,156),USE(tmp:Default_Invoice_Address_Line3),TRN
                         STRING(@s15),AT(156,802,1156,156),USE(tmp:Default_Invoice_Postcode),TRN
                         STRING('Tel:'),AT(156,958),USE(?String16),TRN
                         STRING(@s15),AT(677,958),USE(tmp:Default_Invoice_Telephone_Number)
                         STRING('Fax: '),AT(156,1094),USE(?String19),TRN
                         STRING(@s15),AT(677,1094),USE(tmp:Default_Invoice_Fax_Number)
                         STRING(@s255),AT(677,1250,3844,208),USE(def:EmailAddress),TRN
                         STRING('Email:'),AT(156,1250),USE(?String19:2),TRN
                         STRING('Quantity'),AT(5698,3854),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(156,3854),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(1792,3854),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6917,3823),USE(?string25:4),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost'),AT(4479,3854),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('(Ex.V.A.T.)'),AT(6896,3917),USE(?string25:5),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('INVOICE ADDRESS'),AT(208,1469),USE(?string44:2),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4063,1469),USE(?string44:3),TRN,FONT(,9,,FONT:bold)
                         STRING('PAYMENT DETAILS'),AT(104,8490),USE(?string44:4),TRN,HIDE,FONT(,9,,FONT:bold)
                         STRING('CREDIT DETAILS'),AT(4740,8490),USE(?string44:5),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE TEXT'),AT(2906,8490),USE(?string44:6),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Credit_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Credit_Note')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:INVPARTS.Open
  Relate:RETDESNO.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:RETSALES.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:RETPAY.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        save_ivp_id = access:invparts.savefile()
        access:invparts.clearkey(ivp:invoicenokey)
        ivp:invoicenumber = inv:invoice_number
        set(ivp:invoicenokey,ivp:invoicenokey)
        loop
            if access:invparts.next()
               break
            end !if
            if ivp:invoicenumber <> inv:invoice_number      |
                then break.  ! end if
            Case inv:invoice_Type
                Of 'RET'
                    tmp:line_cost  = Round(ivp:retailcost * ivp:creditquantity,.01)
                Of 'SIN'
                    tmp:line_cost  = Round(ivp:salecost * ivp:creditquantity,.01)
                Of 'CHA'
                    tmp:line_cost  = Round(ivp:salecost * ivp:creditquantity,.01)
                Of 'WAR'
                    tmp:line_cost  = Round(ivp:purchasecost * ivp:creditquantity,.01)
            End!Case inv:invoice_Type
            tmp:recordscount += 1
            print(rpt:detail2)
        
        end !loop
        access:invparts.restorefile(save_ivp_id)
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Credit_Note')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:INVPARTS.Close
    Relate:RETDESNO.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','CREDIT NOTE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'CREDIT NOTE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      tmp:Default_Invoice_Company_Name       = DEF:Invoice_Company_Name
      tmp:Default_Invoice_Address_Line1      = DEF:Invoice_Address_Line1
      tmp:Default_Invoice_Address_Line2      = DEF:Invoice_Address_Line2
      tmp:Default_Invoice_Address_Line3      = DEF:Invoice_Address_Line3
      tmp:Default_Invoice_Postcode           = DEF:Invoice_Postcode
      tmp:Default_Invoice_Telephone_Number   = DEF:Invoice_Telephone_Number
      tmp:Default_Invoice_Fax_Number         = DEF:Invoice_Fax_Number
      tmp:Default_Invoice_VAT_Number         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      tmp:Default_Invoice_Company_Name       = DEF:User_Name
      tmp:Default_Invoice_Address_Line1      = DEF:Address_Line1
      tmp:Default_Invoice_Address_Line2      = DEF:Address_Line2
      tmp:Default_Invoice_Address_Line3      = DEF:Address_Line3
      tmp:Default_Invoice_Postcode             = DEF:Postcode
      tmp:Default_Invoice_Telephone_Number   = DEF:Telephone_Number
      tmp:Default_Invoice_Fax_Number    = DEF:Fax_Number
      tmp:Default_Invoice_VAT_Number         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  
  case inv:invoice_type
      of 'RET' !retail invoice
          access:retsales.clearkey(ret:invoice_number_key)
          ret:invoice_number = inv:previnvoiceno
          access:retsales.tryfetch(ret:invoice_number_key)
          tmp:despatch_note_number   = 'D-' & glo:select1
          If ret:payment_method = 'CAS'
              tmp:Delivery_Name  = RET:Contact_Name
              tmp:Invoice_Name   = ret:contact_name
          Else!If ret:payment_method = 'CAS'
              tmp:Delivery_Name  = RET:Company_Name_Delivery
              tmp:invoice_name   = ret:company_name
          End!If ret:payment_method = 'CAS'
          If ret:building_name    = ''
              tmp:Invoice_address_L     = Clip(RET:Address_Line1)
          Else!    If ret:building_name    = ''
              tmp:Invoice_Address_L     = Clip(RET:Building_Name) & ' ' & Clip(RET:Address_Line1)
          End!
          If ret:building_name_delivery   = ''
              tmp:Delivery_Address_Line_1    = Clip(RET:Address_Line1_Delivery)
          Else!    If ret:building_name_delivery   = ''
              tmp:Delivery_Address_Line_1    = Clip(RET:Building_Name_Delivery) & ' ' & Clip(RET:Address_Line1_Delivery)
          End!    If ret:building_name_delivery   = ''
  
      of 'CHA' !multiple chargeable invoice
  
      of 'SIN' !single chargeable invoice
  
      of 'WAR' !warranty invoice
  
  end!case inv:invoice_type
  
  tmp:vat_rate   = inv:vat_rate_retail
  
  
  
  tmp:courier_cost   = inv:courier_paid
  tmp:partsCost  = inv:parts_paid
  tmp:Labour_Cost = inv:labour_paid
  tmp:sub_total = tmp:courier_Cost + tmp:partsCost + tmp:labour_cost
  tmp:vat_total = Round((inv:courier_paid + inv:parts_paid) * tmp:vat_total/100,.01)
  tmp:Total  = inv:courier_paid + Round(tmp:vat_total,.01) + inv:parts_paid
  
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'CREDIT NOTE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'CREDIT NOTE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Credit_Note'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













Batch_Despatch_Note PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
save_jea_id          USHORT,AUTO
save_job_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
save_jpt_id          USHORT,AUTO
print_group          QUEUE,PRE(prngrp)
esn                  STRING(30)
msn                  STRING(30)
job_number           REAL
                     END
Invoice_Address_Group GROUP,PRE(INVGRP)
Postcode             STRING(15)
Company_Name         STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Delivery_Address_Group GROUP,PRE(DELGRP)
Postcode             STRING(15)
Company_Name         STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Unit_Details_Group   GROUP,PRE(UNIGRP)
Model_Number         STRING(30)
Manufacturer         STRING(30)
ESN                  STRING(16)
MSN                  STRING(16)
Unit_Type            STRING(30)
                     END
Unit_Ref_Number_Temp REAL
Despatch_Type_Temp   STRING(3)
Invoice_Account_Number_Temp STRING(15)
Delivery_Account_Number_Temp STRING(15)
count_temp           REAL
model_number_temp    STRING(50)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
BranchIdentification STRING(2)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(SUBTRACC)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,3865,7521,6490),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,823,7521,2542),USE(?unnamed)
                         STRING('Courier:'),AT(4896,104),USE(?String23),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,104),USE(GLO:Select4),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Consignment No:'),AT(4896,260),USE(?String27),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,260),USE(GLO:Select3,,?glo:select3:2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Despatch Date:'),AT(4896,417),USE(?String27:2),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6042,417),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatched By:'),AT(4896,573),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s60),AT(6042,573),USE(tmp:PrintedBy),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Despatch Batch No:'),AT(4896,729,990,208),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,729),USE(GLO:Select5),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4896,885),USE(?String62),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@n3),AT(6042,885),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(4073,1354),USE(INVGRP:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1510),USE(INVGRP:Address_Line1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Account No:'),AT(208,2292),USE(?String38),TRN,FONT(,8,,)
                         STRING(@s15),AT(885,2292),USE(GLO:Select1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(4073,2135),USE(?String36:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Fax:'),AT(5521,2135),USE(?String37:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4375,2135),USE(INVGRP:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4688,2292),USE(GLO:Select1,,?glo:select1:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Account No:'),AT(4073,2292),USE(?String38:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,2135),USE(INVGRP:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1667),USE(INVGRP:Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1823),USE(INVGRP:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2135),USE(?String36),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Fax:'),AT(1719,2135),USE(?String37),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4073,1979),USE(INVGRP:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s30),AT(146,0),USE(prngrp:esn),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(3594,0),USE(UNIGRP:Unit_Type),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(1250,0),USE(prngrp:msn),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s3),AT(5938,0),USE(Despatch_Type_Temp),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(6302,0,1198,208),USE(job:Order_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(2292,0),USE(UNIGRP:Model_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(4896,0,1042,208),USE(tmp:Ref_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(,,,354),USE(?Totals)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,104,677,156),USE(?String59),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(1042,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(365,10344,7521,854),USE(?unnamed:2)
                         LINE,AT(208,52,7135,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(208,104,7135,677),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(385,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('BATCH DESPATCH NOTE'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,990),USE(?Telephone),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,990),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(156,1146),USE(?fax),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1146),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1302,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1302),USE(?Email),TRN,FONT(,9,,)
                         STRING('INVOICE ADDRESS'),AT(208,1510),USE(?String29),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4063,1510),USE(?String29:2),TRN,FONT(,9,,FONT:bold)
                         STRING('Model Number'),AT(2302,3177),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(156,3177),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('M.S.N.'),AT(1260,3177),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Type'),AT(5948,3177),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Order Number'),AT(6302,3177),USE(?string25:5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Unit Type'),AT(3604,3177),USE(?string44:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Job No'),AT(4906,3177),USE(?string25:4),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Batch_Despatch_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
      ! Save Window Name
   AddToLog('Report','Open','Batch_Despatch_Note')
  ! Add To Log
  !
  ! EMBED ID:           %ProcedureSetup
  ! EMBED Description:  Procedure Setup
  ! EMBED Parameters:   
  ! CW Version:         5507
  !
  ! #AT(%ProcedureSetup,) <- Place quotes around non-numeric parameters
  !
  ! After Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:SUBTRACC.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:LOAN.UseFile
  
  
  RecordsToProcess = RECORDS(SUBTRACC)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(SUBTRACC,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      recordstoprocess    = Records(jobs)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Free(print_group)
        Clear(print_group)
        
        setcursor(cursor:wait)
        
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:batch_number_key)
        job:batch_number = glo:select2
        set(job:batch_number_key,job:batch_number_key)
        loop
            if access:jobs.next()
               break
            end !if
            if job:batch_number <> glo:select2  |
                then break.  ! end if
            recordsprocessed += 1
            Do DisplayProgress
        
            !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
            ! 26 Aug 2002 John
            !
            tmp:Ref_number = job:Ref_number
            if glo:WebJob then
                !Change the tmp ref number if you
                !can Look up from the wob file
                access:Webjob.clearkey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_number
                if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                    tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
                END
            END
            !--------------------------------------------------------------
        
            If job:exchange_unit_number <> ''
                If job:exchange_consignment_number <> glo:select3
                    Cycle
                End!If job:exchange_consignment_number <> glo:select2
            Else!If job:exchange_unit_number <> ''
                If job:consignment_number <> glo:select3
                    Cycle
                End!If job:consignment_number <> glo:select2
            End!If job:exchange_unit_number <> ''
            prngrp:esn  = job:esn
            prngrp:msn  = job:msn
            prngrp:job_number = job:ref_number
            Add(print_group)
        
        end !loop
        access:jobs.restorefile(save_job_id)
        
        RecordsToProcess = Records(Print_Group)
        
        Sort(print_group,prngrp:esn,prngrp:msn)
        
        Loop x# = 1 To Records(print_group)
            Get(print_group,x#)
            recordsprocessed += 1
            Do DisplayProgress
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = prngrp:job_number
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
                If job:exchange_unit_number <> ''
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number = job:exchange_unit_number
                    if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                        UNIGRP:Model_Number = XCH:Model_Number
                        UNIGRP:Manufacturer = xch:manufacturer
                        UNIGRP:ESN          = xch:esn
                        UNIGRP:MSN          = xch:msn
                        UNIGRP:Unit_Type    = 'N/A'
                    end
                    despatch_Type_temp  = 'EXC'
                Else!If job:exchange_unit_number <> ''
                    UNIGRP:Model_Number = job:Model_Number
                    UNIGRP:Manufacturer = job:Manufacturer
                    UNIGRP:ESN          = job:ESN
                    UNIGRP:MSN          = job:MSN
                    UNIGRP:Unit_Type    = job:Unit_Type
                    despatch_Type_temp  = 'JOB'
                End!If job:exchange_unit_number <> ''
                Print(Rpt:detail)
            end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        
        End!Loop x# = 1 To Records(print_group)
        count_temp = Records(print_group)
        Print(rpt:totals)
        
        setcursor()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(SUBTRACC,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  ! Before Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  ! Add To Log End
  !
  ! EMBED ID:           %AfterClosingReport
  ! EMBED Description:  After Closing Report
  ! EMBED Parameters:   
  ! CW Version:         5507
  !
  ! #AT(%AfterClosingReport,) <- Place quotes around non-numeric parameters
  !
      ! Save Window Name
   AddToLog('Report','Close','Batch_Despatch_Note')
  ! After Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','DESPATCH NOTE - BATCH')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'DESPATCH NOTE - BATCH'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !--- Webify SETUP ClarioNET:UseReportPreview(0) tmp:BranchIdentification ---------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  
      access:tradeacc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = ClarioNET:Global.Param2
      IF Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
          tmp:BranchIdentification = tra:BranchIdentification
      ELSE
          tmp:BranchIdentification = ''
      END !IF
  
  END
  !---------------------------------------------------------------------------------------
  HideDespAdd# = 0
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = glo:select1
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          If tra:Invoice_Sub_Accounts = 'YES'
              If sub:HideDespAdd = 1
                  HideDespAdd# = 1
              End!If sub:HideDespAdd = 1
          Else!If tra:Invoice_Sub_Accounts = 'YES'
              If tra:HideDespAdd = 1
                  HideDespAdd# = 1
              End!If tra:HideDespAdd = 1
          End!If tra:Invoice_Sub_Accounts = 'YES'
          if tra:use_sub_accounts = 'YES'
              DELGRP:Postcode         = sub:Postcode
              DELGRP:Company_Name     = sub:Company_Name
              DELGRP:Address_Line1    = sub:Address_Line1
              DELGRP:Address_Line2    = sub:Address_Line2
              DELGRP:Address_Line3    = sub:Address_Line3
              DELGRP:Telephone_Number = sub:Telephone_Number
              DELGRP:Fax_Number       = sub:Fax_Number
              If tra:invoice_sub_accounts = 'YES'
                  INVGRP:Postcode         = sub:Postcode
                  INVGRP:Company_Name     = sub:Company_Name
                  INVGRP:Address_Line1    = sub:Address_Line1
                  INVGRP:Address_Line2    = sub:Address_Line2
                  INVGRP:Address_Line3    = sub:Address_Line3
                  INVGRP:Telephone_Number = sub:Telephone_Number
                  INVGRP:Fax_Number       = sub:Fax_Number
              Else
                  INVGRP:Postcode         = tra:Postcode
                  INVGRP:Company_Name     = tra:Company_Name
                  INVGRP:Address_Line1    = tra:Address_Line1
                  INVGRP:Address_Line2    = tra:Address_Line2
                  INVGRP:Address_Line3    = tra:Address_Line3
                  INVGRP:Telephone_Number = tra:Telephone_Number
                  INVGRP:Fax_Number       = tra:Fax_Number
              End!If tra:invoice_sub_accounts = 'YES'
          else!if tra:use_sub_accounts = 'YES'
              INVGRP:Postcode         = tra:Postcode
              INVGRP:Company_Name     = tra:Company_Name
              INVGRP:Address_Line1    = tra:Address_Line1
              INVGRP:Address_Line2    = tra:Address_Line2
              INVGRP:Address_Line3    = tra:Address_Line3
              INVGRP:Telephone_Number = tra:Telephone_Number
              INVGRP:Fax_Number       = tra:Fax_Number
              DELGRP:Postcode         = tra:Postcode
              DELGRP:Company_Name     = tra:Company_Name
              DELGRP:Address_Line1    = tra:Address_Line1
              DELGRP:Address_Line2    = tra:Address_Line2
              DELGRP:Address_Line3    = tra:Address_Line3
              DELGRP:Telephone_Number = tra:Telephone_Number
              DELGRP:Fax_Number       = tra:Fax_Number
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  !Hide Despatch Address
  If HideDespAdd# = 1
      Settarget(Report)
      ?def:User_Name{prop:Hide} = 1
      ?def:Address_Line1{prop:Hide} = 1
      ?def:Address_Line2{prop:Hide} = 1
      ?def:Address_Line3{prop:Hide} = 1
      ?def:Postcode{prop:hide} = 1
      ?tmp:DefaultTelephone{prop:hide} = 1
      ?tmp:DefaultFax{prop:hide} = 1
      ?telephone{prop:hide} = 1
      ?fax{prop:hide} = 1
      ?email{prop:hide} = 1
      ?def:EmailAddress{prop:Hide} = 1
      Settarget()
  End!If HideDespAdd# = 1
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE - BATCH'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'DESPATCH NOTE - BATCH'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Batch_Despatch_Note'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













Third_Party_Single_Despatch PROCEDURE(f_type)
RejectRecord         LONG,AUTO
save_trb_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_jac_id          USHORT,AUTO
tmp:accessoryque     QUEUE,PRE(accque)
accessory            STRING(30)
                     END
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
code_temp            BYTE
option_temp          BYTE
bar_code_string_temp CSTRING(21)
bar_code_temp        CSTRING(21)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
BranchIdentification LIKE(tra:BranchIdentification)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:DOP)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:ThirdPartyDateDesp)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:date_booked)
                       PROJECT(job:Ref_Number)
                       JOIN(jbn:RefNumberKey,job:Ref_Number)
                         PROJECT(jbn:Fault_Description)
                       END
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,7406),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?header)
                         STRING('3rd Party Agent:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(GLO:Select3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,4240),USE(?detailband)
                           STRING('Job Number:'),AT(313,260),USE(?String2),TRN,FONT(,16,,FONT:bold,CHARSET:ANSI)
                           STRING(@s20),AT(4792,313,1771,208),USE(bar_code_temp),LEFT,FONT('C128 High 12pt LJ3',12,,FONT:regular,CHARSET:ANSI)
                           STRING(@s15),AT(2917,313),USE(tmp:Ref_Number,,?job:Ref_Number:2),TRN,LEFT,FONT(,12,,FONT:bold)
                           STRING('Unit Details:'),AT(313,625),USE(?String2:2),TRN,FONT(,16,,FONT:bold,CHARSET:ANSI)
                           STRING(@d6b),AT(6563,313),USE(job:date_booked),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Manufacturer:'),AT(2917,615),USE(?String6:2),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,615),USE(job:Manufacturer),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Model Number:'),AT(2917,927),USE(?String6:3),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,927),USE(job:Model_Number),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Unit Type:'),AT(2917,1240),USE(?String6:4),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,1240),USE(job:Unit_Type),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('I.M.E.I. Number:'),AT(2917,1552),USE(?String6:5),TRN,FONT(,12,,FONT:bold)
                           STRING(@s16),AT(4792,1552),USE(job:ESN),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('M.S.N.:'),AT(2917,1875),USE(?String6:6),TRN,FONT(,12,,FONT:bold)
                           STRING(@s16),AT(4792,1865),USE(job:MSN),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING(@d6),AT(4750,2167),USE(job:DOP),TRN,LEFT,FONT(,12,,)
                           STRING('Date Of Purchase:'),AT(2917,2167),USE(?String46),TRN,FONT(,12,,FONT:bold)
                           STRING('Reported Fault:'),AT(2917,2479),USE(?String6:14),TRN,FONT(,12,,FONT:bold)
                           TEXT,AT(4792,2500,2552,885),USE(jbn:Fault_Description),TRN,FONT('Arial',10,,,CHARSET:ANSI)
                           STRING('Date Despatched:'),AT(2917,3438),USE(?String6:7),TRN,FONT(,12,,FONT:bold)
                           STRING(@d6b),AT(4792,3438),USE(job:ThirdPartyDateDesp),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING(@s40),AT(4792,3698),USE(GLO:Select2),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Exchange'),AT(2917,3958,4427,208),USE(?Exchange),TRN,HIDE,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING('Batch Number:'),AT(2917,3698),USE(?String6:8),TRN,FONT(,12,,FONT:bold)
                           STRING('Despatch Details:'),AT(260,3385),USE(?String2:3),TRN,FONT(,16,,FONT:bold,CHARSET:ANSI)
                         END
detail2                  DETAIL,AT(,,,1125),USE(?InvoiceText)
                           STRING('Invoice Text:'),AT(260,52),USE(?String2:5),TRN,FONT(,16,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(2917,104,4479,885),USE(jbn:Fault_Description,,?jbn:Fault_Description:2),TRN,FONT('Arial',10,,,CHARSET:ANSI)
                         END
detail3                  DETAIL,USE(?Accessories2:2)
                           STRING('Accessories:'),AT(260,52),USE(?Accessories),TRN,HIDE,FONT(,16,,FONT:bold,CHARSET:ANSI)
                           STRING('Retained'),AT(2917,104,4427,260),USE(?DespatchText),TRN,FONT(,12,,FONT:bold)
                         END
                       END
detail1                DETAIL,AT(,,,219),USE(?unnamed)
                         STRING(@s30),AT(4792,0),USE(jac:Accessory),TRN,FONT(,12,,)
                       END
                       FOOTER,AT(385,10260,7521,1156),USE(?unnamed:2),FONT('Arial',,,)
                         STRING('BOUNCER REPORT:'),AT(208,52),USE(?bouncer1),TRN,HIDE,FONT(,10,,FONT:bold)
                         STRING('This unit has previously been repaired by your workshop'),AT(1563,52),USE(?bouncer2),TRN,HIDE,FONT(,10,,FONT:bold)
                         LINE,AT(365,260,6802,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(156,313,7188,833),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('3RD PARTY DESPATCH NOTE'),AT(4010,0,3438,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Third_Party_Single_Despatch')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Third_Party_Single_Despatch')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = '3rdPartyDespatch'
  If PrintOption(PreviewReq,glo:ExportReport,'Third Party Despatch Note') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:JOBNOTES.UseFile
  Access:JOBACC.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:TRDBATCH.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        PrintSkipDetails = FALSE
        
        
        
        
        ! Before Embed Point: %AfterPrint) DESC(After Printing Detail Section) ARG()
        Print(rpt:detail)
        first# = 1
        ret# = 0
        save_jac_id = access:jobacc.savefile()
        access:jobacc.clearkey(jac:ref_number_key)
        jac:ref_number = job:ref_number
        set(jac:ref_number_key,jac:ref_number_key)
        loop
            if access:jobacc.next()
               break
            end !if
            if jac:ref_number <> job:ref_number      |
                then break.  ! end if
            ! Insert --- Only show accessories that were sent to the ARC (DBH: 07/09/2009) #11056
            if (jac:Attached <> 1)
                Cycle
            end ! if (jac:Attached <> 1)
            ! end --- (DBH: 07/09/2009) #11056
            Case f_Type
                Of 'RET'
                    If first# = 1
                        SetTarget(Report)
                        ?Accessories{prop:Hide} = 0
                        ?DespatchText{prop:Text} = 'Retained By ' & Clip(Capitalize(def:User_Name)) & ' :-'
                        SetTarget()
                        Print(Rpt:Detail3)
                        first# = 0
                    End !If first# = 1
                    Print(rpt:Detail1)
                Of 'DES'
                    Sort(glo:Queue,glo:Pointer)
                    glo:Pointer = jac:Accessory
                    Get(glo:Queue,glo:Pointer)
                    IF Error()
                        If first# = 1
                            SetTarget(Report)
                            ?Accessories{prop:Hide} = 0
                            ?DespatchText{prop:Text} = 'Retained By ' & Clip(Capitalize(def:user_name)) & ' :-'
                            Print(rpt:Detail3)
                            SetTarget()
                            first# = 0
                            ret# = 1
                        End !If first# = 1
                        print(rpt:detail1)
                    End !IF Error()
            End !Case f_Type
        end !loop
        access:jobacc.restorefile(save_jac_id)
        
        If f_Type = 'DES'
            first# = 1
            save_jac_id = access:jobacc.savefile()
            access:jobacc.clearkey(jac:ref_number_key)
            jac:ref_number = job:ref_number
            set(jac:ref_number_key,jac:ref_number_key)
            loop
                if access:jobacc.next()
                   break
                end !if
                if jac:ref_number <> job:ref_number      |
                    then break.  ! end if
                ! Insert --- Only show accessories that were sent to the ARC (DBH: 07/09/2009) #11056
                if (jac:Attached <> 1)
                    Cycle
                end ! if (jac:Attached <> 1)
                ! end --- (DBH: 07/09/2009) #11056
                Sort(glo:Queue,glo:Pointer)
                glo:Pointer = jac:Accessory
                Get(glo:Queue,glo:Pointer)
                IF ~Error()
                    If first# = 1
                        SetTarget(Report)
                        If ret# = 0
                            ?Accessories{Prop:Hide} = 0
                        Else
                            ?Accessories{Prop:Hide} = 1
                        End !If ret# = 0
                        ?DespatchText{prop:Text} = 'Despatched With Unit:-'
                        SetTarget()
                        Print(rpt:Detail3)
                        first# = 0
                    End !If first# = 1
                    print(rpt:detail1)
                End !IF Error()
            end !loop
            access:jobacc.restorefile(save_jac_id)
        End !f_Type = 'DET'
        Print(rpt:Detail2)
        ! After Embed Point: %AfterPrint) DESC(After Printing Detail Section) ARG()
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','Third_Party_Single_Despatch')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBACC.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE - THIRD PARTY'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  ! Inserting (DBH 15/02/2008) # 9775 - Show who printed the document
  Access:USERS.Clearkey(use:Password_Key)
  use:Password = glo:Password
  If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      tmp:PrintedBy = Clip(use:Forename) & ' ' & Clip(use:Surname)
  End ! If Access:USERS.TryFetch(use:PasswordKey) = Level:Benign
  ! End (DBH 15/02/2008) #9775
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
  ! 26 Aug 2002 John
  !
  tmp:Ref_number = job:Ref_number
  if glo:WebJob then
      !Change the tmp ref number if you
      !can Look up from the wob file
      access:Webjob.clearkey(wob:RefNumberKey)
      wob:RefNumber = job:Ref_number
      if access:Webjob.fetch(wob:refNumberKey)=level:benign then
          access:tradeacc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = ClarioNET:Global.Param2
          access:tradeacc.fetch(tra:Account_Number_Key)
          tmp:Ref_Number = Job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
      END
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  END
  !--------------------------------------------------------------
    count# = 0
    save_jac_id = access:jobacc.savefile()
    access:jobacc.clearkey(jac:ref_number_key)
    jac:ref_number = job:ref_number
    set(jac:ref_number_key,jac:ref_number_key)
    loop
        if access:jobacc.next()
           break
        end !if
        if jac:ref_number <> job:ref_number      |
            then break.  ! end if
        ! Insert --- Don't count accessories not attached at ARC (DBH: 07/09/2009) #11056
        if (jac:Attached <> 1)
            Cycle
        end ! if (jac:Attached <> 1)
        ! end --- (DBH: 07/09/2009) #11056
        count# = 1
        Break
    end !loop
    access:jobacc.restorefile(save_jac_id)

  !  If count# = 1
  !      Settarget(report)
  !      ?retain_text{prop:text} = 'Retained By ' & Clip(def:user_name) & ' :-'
  !      ?DespatchText{prop:Text} = 'Despatched With Unit:-'
  !      Settarget()
  !  Else!If records(tmp:accessoryque)
  !      Settarget(report)
  !      ?retain_text{prop:text} = 'No Accessories'
  !      Settarget()
  !  End!If records(tmp:accessoryque)
    !barcode bit
    code_temp            = 3
    option_temp          = 0
    bar_code_string_temp = Clip(job:ref_number)
    sequence2(code_temp,option_temp,bar_code_string_temp,bar_code_temp)
  
  
    If job:exchange_unit_number <> ''
        Settarget(Report)
        Unhide(?exchange)
        ?exchange{prop:text} = 'Exchange Unit ' & Clip(job:exchange_unit_number) & ' issued.'
        Settarget()
    End!If job:exchange_unit_number <> ''
    !Bouncer?
    found# = 0
    save_trb_id = access:trdbatch.savefile()
    clear(trb:record, -1)
    trb:esn = job:esn
    set(trb:esn_only_key,trb:esn_only_key)
    loop
        next(trdbatch)
        if errorcode()         |
           or trb:esn <> job:esn      |
           then break.  ! end if
        If trb:company_name = job:third_party_site
            access:jobs_alias.clearkey(job_ali:ref_number_key)
            job_ali:ref_number  = trb:ref_number
            If access:jobs_alias.tryfetch(job_ali:ref_number_key) = Level:Benign
                If job_ali:ThirdPartyDateDesp <> Today()
                    If job_ali:ThirdPartyDateDesp > Today() - 90
                        found# = 1
                        Break
                    End!If job:third_party_despatch_date > Today() - 90
                End!If job:third_party_despatch_date <> Today()
            End!If access:jobs_alias.tryfetch(job_ali:ref_number_key) = Level:Benign
        End!If trb:company_name = job:third_party_site
    end !loop
    access:trdbatch.restorefile(save_trb_id)
    If found# = 1
        Settarget(Report)
        Unhide(?bouncer1)
        Unhide(?bouncer2)
        Settarget()
    End!If found# = 1
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Third_Party_Single_Despatch'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END







