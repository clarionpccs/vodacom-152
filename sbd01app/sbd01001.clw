

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD01001.INC'),ONCE        !Local module procedure declarations
                     END


Multiple_Invoice PROCEDURE (f_batch_number,f_account_number) !Generated from procedure template - Report

Default_Invoice_Company_Name_Temp STRING(30)
WebMaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(16)
                     END
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
tmp:printer          STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Invoice_Number_Temp  STRING(28)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Labour_vat_info_temp STRING(15)
Parts_vat_info_temp  STRING(15)
batch_number_temp    STRING(28)
labour_vat_temp      REAL
parts_vat_temp       REAL
balance_temp         REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS)
                       PROJECT(job:ESN)
                       PROJECT(job:Invoice_Number_Warranty)
                       PROJECT(job:MSN)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Unit_Type)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(43,42,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

report               REPORT,AT(396,4604,7521,4240),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,),THOUS
                       HEADER,AT(396,521,7521,4042),USE(?unnamed)
                         STRING(@s30),AT(156,83,3844,240),USE(def:Invoice_Company_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,365,3844,156),USE(def:Invoice_Address_Line1),TRN
                         STRING(@s30),AT(156,521,3844,156),USE(def:Invoice_Address_Line2),TRN
                         STRING(@s30),AT(156,677,3844,156),USE(def:Invoice_Address_Line3),TRN
                         STRING(@s15),AT(156,833,1156,156),USE(def:Invoice_Postcode),TRN
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN
                         STRING(@s15),AT(625,1042),USE(def:Invoice_Telephone_Number),TRN
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN
                         STRING(@s15),AT(625,1198),USE(def:Invoice_Fax_Number),TRN
                         STRING(@s255),AT(625,1354,3844,208),USE(def:InvoiceEmailAddress),TRN
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN
                         STRING('INVOICE'),AT(5521,0),USE(?String20),TRN,FONT(,20,,FONT:bold)
                         STRING(@s28),AT(4917,917),USE(batch_number_temp),TRN,CENTER,FONT(,,,FONT:bold)
                         STRING('Page Number: '),AT(4083,2604),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@N4),AT(5521,2604),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s28),AT(4917,521),USE(Invoice_Number_Temp),TRN,CENTER,FONT(,,,FONT:bold)
                         STRING(@s30),AT(156,1677),USE(Invoice_Name_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('V.A.T. Number: '),AT(4083,1677),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s30),AT(5521,1677),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,1844,1917,156),USE(Address_Line1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2000),USE(Address_Line2_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2156),USE(Address_Line3_Temp),TRN,FONT(,8,,)
                         STRING('Date Of Invoice:'),AT(4083,1917),USE(?String31),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5521,1917),USE(inv:Date_Created),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2323),USE(Address_Line4_Temp),TRN,FONT(,8,,)
                         STRING('Printed:'),AT(4083,2396),USE(?String46),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5521,2396),USE(ReportRunDate),FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6156,2396),USE(ReportRunTime),FONT(,8,,FONT:bold)
                         STRING('Account Number: '),AT(4083,2156),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s15),AT(5521,2156,1000,240),USE(sub:Account_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Job Number'),AT(323,3844),USE(?String33),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(1240,3844),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3000,3844),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4396,3844),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(5521,3844),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2521,1323,156),USE(inv:Invoice_VAT_Number),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,156),USE(?DetailBand)
                           STRING(@s15),AT(156,0),USE(tmp:Ref_Number),TRN,RIGHT,FONT(,8,,)
                           STRING(@s25),AT(1240,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s20),AT(3000,0),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(4396,0),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                           STRING(@s10),AT(5521,0),USE(job:MSN),TRN,LEFT,FONT(,8,,)
                         END
                         FOOTER,AT(396,8802,,2281),USE(?unnamed:2),ABSOLUTE
                           STRING('Parts Sub Total'),AT(4802,719),USE(?String37),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6323,719),USE(inv:Parts_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6323,875),USE(parts_vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6323,563),USE(labour_vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Labour Sub Total'),AT(4802,396),USE(?String39),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6323,396),USE(inv:Labour_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING(@s15),AT(4802,563),USE(Labour_vat_info_temp),TRN,LEFT,FONT(,8,,)
                           STRING('Balance Due'),AT(4802,1146),USE(?String38),TRN,FONT(,10,,FONT:bold)
                           STRING(@n14.2),AT(6083,1146),USE(balance_temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING('<128>'),AT(6021,1146),USE(?Euro),TRN,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                           STRING(@s15),AT(4802,875),USE(Parts_vat_info_temp),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10323,7521,1156)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(report)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Multiple_Invoice')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  BIND('GLO:Select1',GLO:Select1)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:INVOICE.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Invoice_Number_Warranty)
  ThisReport.AddSortOrder(job:WarInvoiceNoKey)
  ThisReport.AddRange(job:Invoice_Number_Warranty,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Report','Close','Multiple_Invoice')
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  set(defaults)
  access:defaults.next()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  !--------------------------------------------------------------
  ! Webify tmp:Ref_Number Using job:Ref_number
  ! 26 Aug 2002 John
  !
  tmp:Ref_number = job:Ref_number
  if glo:WebJob then
      !Change the tmp ref number if you
      !can Look up from the wob file
      access:Webjob.clearkey(wob:RefNumberKey)
      wob:RefNumber = job:Ref_number
      if access:Webjob.fetch(wob:refNumberKey)=level:benign then
          access:tradeacc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = ClarioNET:Global.Param2
          access:tradeacc.fetch(tra:Account_Number_Key)
          tmp:Ref_Number = Job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
      END
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  END
  !--------------------------------------------------------------
  
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  
  reportrundate = Today()
  reportruntime = Clock()
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = f_account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          if tra:invoice_sub_accounts = 'YES'
              invoice_name_temp   = sub:company_name
              address_line1_temp  = sub:address_line1
              address_line2_temp  = sub:address_line2
              If sup:address_line3 = ''
                  address_line3_temp = sub:postcode
                  address_line4_temp = ''
              Else
                  address_line3_temp  = sub:address_line3
                  address_line4_temp  = sub:postcode
              End
          else!if tra:use_sub_accounts = 'YES'
              invoice_name_temp   = tra:company_name
              address_line1_temp  = tra:address_line1
              address_line2_temp  = tra:address_line2
              If sup:address_line3 = ''
                  address_line3_temp = tra:postcode
                  address_line4_temp = ''
              Else
                  address_line3_temp  = tra:address_line3
                  address_line4_temp  = tra:postcode
              End
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  
  
  invoice_number_temp = 'Invoice Number: ' & Format(glo:select1,@p<<<<<<<#p)
  batch_number_temp   = 'Batch Number  : ' & Format(f_batch_number,@p<<<<<<<#p)
  
  access:invoice.clearkey(inv:invoice_type_key)
  inv:invoice_type   = 'WAR'
  inv:invoice_number = glo:select1
  If access:invoice.fetch(inv:invoice_type_key) = Level:Benign
      labour_vat_info_temp = 'V.A.T. @ ' & Format(INV:Vat_Rate_labour,@n6.2)
      labour_vat_temp      = inv:labour_paid * (INV:Vat_Rate_labour/100)
      parts_vat_info_temp = 'V.A.T. @ ' & Format(INV:Vat_Rate_Parts,@n6.2)
      parts_vat_temp      = inv:parts_paid * (INV:Vat_Rate_Parts/100)
      balance_temp            = inv:labour_paid + labour_vat_temp + inv:parts_paid + parts_vat_temp
  End!If access:invoice.fetch(inv:invoice_type_key) = Level:Benign
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  report{Prop:Text} = 'Invoice'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
      ! Save Window Name
   AddToLog('Report','Open','Multiple_Invoice')
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Open_Files PROCEDURE                                  !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Open_Files')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXPAUDIT.Open
  Relate:EXPGENDM.Open
  Relate:EXPPARTS.Open
  Relate:EXPWARPARTS.Open
  Relate:LABLGTMP.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Open_Files')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXPAUDIT.Close
    Relate:EXPGENDM.Close
    Relate:EXPPARTS.Close
    Relate:EXPWARPARTS.Close
    Relate:LABLGTMP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Open_Files')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






Third_Party_Despatch_Note PROCEDURE
WebMaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(16)
                     END
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
printed_temp         BYTE(0)
pos                  STRING(255)
count_temp           REAL
user_name_temp       STRING(40)
job_count_temp       REAL
total_jobs_temp      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:DefaultEmail     STRING(255)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:DOP)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Unit_Type)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,2740,7521,7458),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,1677),USE(?unnamed)
                         STRING('Service Centre:'),AT(4906,566),USE(?String35),TRN,FONT(,8,,)
                         STRING('Despatch Date:'),AT(4906,943),USE(?String36),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5849,943),USE(GLO:Select3),TRN,FONT(,8,,FONT:bold)
                         STRING('Raised By:'),AT(4906,1132),USE(?String31),TRN,FONT(,8,,)
                         STRING(@s40),AT(5849,1132),USE(user_name_temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s3),AT(5849,1321),USE(ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5849,566),USE(trd:Company_Name),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number: '),AT(4906,755),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s30),AT(5849,755),USE(trd:Account_Number),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('Page Number'),AT(4906,1321,917,198),USE(?String34),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
detail1                  DETAIL,USE(?unnamed:2)
                           STRING(@s16),AT(188,0),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                           STRING(@s25),AT(3917,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s16),AT(1229,0),USE(job:MSN),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(2229,0),USE(tmp:Ref_Number),TRN,RIGHT,FONT(,8,,)
                           STRING(@s30),AT(5573,0,1771,208),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                           STRING(@d6),AT(3292,0),USE(job:DOP),TRN,LEFT,FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,719),USE(?unnamed:5)
                           LINE,AT(365,83,6927,1),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(5760,156),USE(?String30),TRN,FONT(,10,,FONT:bold)
                           STRING(@n8),AT(6677,156),USE(count_temp),TRN,LEFT,FONT(,10,,FONT:bold)
                         END
                       END
                       FOOTER,AT(385,10260,7521,1156),USE(?unnamed:3),FONT('Arial',,,)
                         LINE,AT(365,52,6802,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(156,156,7198,958),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(396,469,7521,11198),USE(?unnamed:4)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11198),USE(?Image1)
                         STRING(@s30),AT(156,52,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('THIRD PARTY DESPATCH NOTE'),AT(4323,52),USE(?title),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,313,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,469,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,625,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,781,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,990),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,990),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1146),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1146),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1302,3844,198),USE(tmp:DefaultEmail),TRN,FONT(,9,,)
                         STRING('D.O.P.'),AT(3292,2083),USE(?String37),TRN,FONT(,8,,FONT:bold)
                         STRING('Email:'),AT(156,1302),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('I.M.E.I. Number'),AT(156,2083),USE(?String29),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(1229,2083),USE(?String29:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Job Number'),AT(2229,2083),USE(?String29:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Nuber'),AT(3917,2083),USE(?String29:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(5542,2083),USE(?String29:5),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Third_Party_Despatch_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Third_Party_Despatch_Note')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = '3rdPartyDespatch'
  If PrintOption(PreviewReq,glo:ExportReport,'Third Party Despatch Note') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:TRDPARTY.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  Access:LOCATION.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job:ThirdEsnKey)
      Process:View{Prop:Filter} = |
      'UPPER(job:Third_Party_Site) = UPPER(GLO:Select1) AND (Upper(job:third_' & |
      'party_despatch_printed) = Upper(glo:select2))'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        ! Webify tmp:Ref_Number Using job:Ref_number ------------------
        ! 26 Aug 2002 John
        !
        tmp:Ref_number = job:Ref_number
        if glo:WebJob then
            !Change the tmp ref number if you
            !can Look up from the wob file
            access:Webjob.clearkey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_number
            if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                access:tradeacc.clearkey(tra:Account_Number_Key)
                tra:Account_Number = ClarioNET:Global.Param2
                access:tradeacc.fetch(tra:Account_Number_Key)
                tmp:Ref_Number = Job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
            END
        !    !Set up no preview on client      ! MOVED TO:-Start of "After Opening Report"
        !    ClarioNET:UseReportPreview(0)
        
        END
        !--------------------------------------------------------------
        
        printed_temp = 1
        If glo:select2 = 'YES'
            If job:ThirdPartyDateDesp = glo:select3
                count_temp += 1
                Print(rpt:detail1)
            End!If job:third_party_despatch_date < glo:select3 Or job:third_party_despatch_date > glo:select4
        Else!If glo:select2 = 'YES'
            pos = Position(job:thirdesnkey)
            job:Third_Party_Printed = 'YES'
            job:ThirdPartyDateDesp = Today()
            LocationChange(Clip(GETINI('RRC','RTMLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
            access:jobs.update()
            Reset(job:thirdesnkey,pos)
            count_temp += 1
            Print(rpt:detail1)
        End!If glo:select2 = 'YES'
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','Third_Party_Despatch_Note')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE - THIRD PARTY'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  Access:USERS.Clearkey(use:Password_Key)
  use:Password = glo:Password
  If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      tmp:PrintedBy = CLip(use:Forename) & ' ' & Clip(use:Surname)
  End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  ! Webify tmp:Ref_Number Using job:Ref_number ------------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  END
  !--------------------------------------------------------------
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Third_Party_Despatch_Note'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END







Third_Party_Despatch_Criteria PROCEDURE               !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
window               WINDOW('Select Third Party Service Centre'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Third Party Despatch Note Report'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Select Third Party Service Centre'),USE(?Tab1)
                           PROMPT('Service Centre'),AT(212,156),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(288,156,124,10),USE(GLO:Select1),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           OPTION('Report Type'),AT(200,176,284,36),USE(GLO:Select2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Print New Despatches'),AT(208,192),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('NO')
                             RADIO('Reprint Old Despatches'),AT(368,192),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES')
                           END
                           GROUP,AT(208,216,212,20),USE(?Date_Group),DISABLE
                             PROMPT('Despatch Date'),AT(212,224),USE(?glo:select3:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@d6b),AT(288,224,64,10),USE(GLO:Select3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             BUTTON,AT(356,220),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           END
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Show_Hide       Routine
    Case glo:select2
        Of 'YES'
            Enable(?date_group)
        Of 'NO'
            Disable(?date_group)
    End!Case glo:select2
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020148'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Third_Party_Despatch_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:TRDPARTY.Open
  SELF.FilesOpened = True
  glo:select1 = ''
  glo:select2 = 'NO'
  glo:select3 = Today()
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Do show_hide
      ! Save Window Name
   AddToLog('Window','Open','Third_Party_Despatch_Criteria')
  Bryan.CompFieldColour()
  ?glo:select3{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB1.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(trd:Company_Name_Key)
  FDCB1.AddField(trd:Company_Name,FDCB1.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  glo:select1 = ''
  glo:select2 = ''
  glo:select3 = ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRDPARTY.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Third_Party_Despatch_Criteria')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020148'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020148'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020148'&'0')
      ***
    OF ?GLO:Select2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select2, Accepted)
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select2, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select3 = TINCALENDARStyle1(GLO:Select3)
          Display(?glo:select3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If glo:select1 = ''
          Select(?glo:select1)
      Else!If glo:select1 <> ''
          Third_Party_Despatch_Note
      End!If glo:select1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?GLO:Select3
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






Job_Card PROCEDURE
tmp:PrintedBy        STRING(60)
who_booked           STRING(50)
Webmaster_Group      GROUP,PRE(tmp)
Ref_number           STRING(20)
BarCode              STRING(20)
BranchIdentification STRING(2)
                     END
save_job2_id         USHORT,AUTO
tmp:accessories      STRING(255)
RejectRecord         LONG,AUTO
save_joo_id          USHORT,AUTO
save_maf_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_lac_id          USHORT
save_loa_id          USHORT
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
save_jea_id          USHORT,AUTO
fault_code_field_temp STRING(30),DIM(12)
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Delivery_Telephone_Temp STRING(30)
InvoiceAddress_Group GROUP,PRE()
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
invoice_EMail_Address STRING(255)
                     END
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(70)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(30)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
engineer_temp        STRING(30)
part_type_temp       STRING(4)
customer_name_temp   STRING(40)
delivery_name_temp   STRING(40)
exchange_unit_number_temp STRING(20)
exchange_model_number STRING(30)
exchange_manufacturer_temp STRING(30)
exchange_unit_type_temp STRING(30)
exchange_esn_temp    STRING(16)
exchange_msn_temp    STRING(20)
exchange_unit_title_temp STRING(15)
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:bouncers         STRING(255)
tmp:InvoiceText      STRING(255)
tmp:LoanModel        STRING(30)
tmp:LoanIMEI         STRING(30)
tmp:LoanAccessories  STRING(255)
tmp:LoanDepositPaid  REAL
DefaultAddress       GROUP,PRE(address)
SiteName             STRING(40)
Name                 STRING(40)
Name2                STRING(40)
Location             STRING(40)
AddressLine1         STRING(40)
AddressLine2         STRING(40)
AddressLine3         STRING(40)
AddressLine4         STRING(40)
Telephone            STRING(40)
RegistrationNo       STRING(40)
Fax                  STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
delivery_Company_Name_temp STRING(30)
endUserTelNo         STRING(15)
clientName           STRING(65)
tmp:ReplacementValue REAL
tmp:FaultCodeDescription STRING(255),DIM(6)
tmp:BookingOption    STRING(30)
tmp:ExportReport     BYTE
tmp:RepairNotes      STRING(1000)
locTermsText         STRING(255)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Account_Number)
                       PROJECT(job_ali:Charge_Type)
                       PROJECT(job_ali:Courier)
                       PROJECT(job_ali:DOP)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:Date_QA_Passed)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Location)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Order_Number)
                       PROJECT(job_ali:Repair_Type)
                       PROJECT(job_ali:Repair_Type_Warranty)
                       PROJECT(job_ali:Time_Completed)
                       PROJECT(job_ali:Time_QA_Passed)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:Warranty_Charge_Type)
                       PROJECT(job_ali:date_booked)
                       PROJECT(job_ali:time_booked)
                       PROJECT(job_ali:Ref_Number)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                       END
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('ORDERS Report'),AT(260,6458,7750,813),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),THOUS
                       HEADER,AT(260,615,7750,8250),USE(?unnamed),FONT('Tahoma',7,,)
                         STRING(@s50),AT(6094,469),USE(who_booked),TRN,FONT(,8,,FONT:bold)
                         STRING('Job Number: '),AT(5104,52),USE(?String25),TRN,FONT(,8,,)
                         STRING(@s20),AT(5792,21),USE(tmp:Ref_number),TRN,LEFT,FONT(,10,,FONT:bold)
                         STRING('Date Booked: '),AT(5104,313),USE(?String58),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6094,313),USE(job_ali:date_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6979,313),USE(job_ali:time_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Booked By:'),AT(5104,469),USE(?String158),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6094,781),USE(job_ali:DOP),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Engineer:'),AT(5104,625),USE(?String96),TRN,FONT(,8,,)
                         STRING(@s30),AT(6094,625),USE(engineer_temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Of Purchase:'),AT(5104,781),USE(?String96:2),TRN,FONT(,8,,)
                         STRING('Model'),AT(177,3500),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Location'),AT(177,2833),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Warranty Type'),AT(4708,2833),USE(?warranty_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Warranty Repair Type'),AT(6219,2833),USE(?warranty_repair_type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(1688,2833),USE(?Chargeable_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(3198,2833),USE(?Repair_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1688,3500),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3198,3500),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4708,3510),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6219,3510),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(1688,3073),USE(job_ali:Charge_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3198,3073),USE(job_ali:Repair_Type),TRN,FONT(,8,,)
                         STRING(@s20),AT(177,3073),USE(job_ali:Location),TRN,FONT(,8,,)
                         STRING(@s30),AT(177,3698,1000,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(1688,3698,1323,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3198,3698,1396,156),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(4708,3698,1396,156),USE(job_ali:ESN),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(6219,3698),USE(job_ali:MSN),TRN,FONT(,8,,)
                         STRING(@s20),AT(6219,4010),USE(exchange_msn_temp),TRN,FONT(,8,,)
                         STRING(@s16),AT(4708,4010,1396,156),USE(exchange_esn_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3198,4010,1396,156),USE(exchange_unit_type_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(1688,4010,1323,156),USE(exchange_manufacturer_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(177,4010,1000,156),USE(exchange_model_number),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(177,3854),USE(exchange_unit_title_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(1354,3854),USE(exchange_unit_number_temp),TRN,LEFT
                         STRING('REPORTED FAULT'),AT(3646,4219),USE(?String64),TRN,FONT(,8,,FONT:bold)
                         TEXT,AT(4688,4219,2344,833),USE(jbn_ali:Fault_Description),TRN,FONT(,7,,)
                         STRING('ESTIMATE'),AT(156,5052),USE(?Estimate),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING(@s70),AT(1615,5052,5625,208),USE(estimate_value_temp),TRN,FONT(,8,,)
                         STRING('ENGINEERS REPORT'),AT(156,4635),USE(?String88),TRN,FONT(,8,,FONT:bold)
                         TEXT,AT(1615,4219,1927,365),USE(tmp:RepairNotes),TRN,FONT(,7,,)
                         STRING('REPAIR NOTES'),AT(156,4219),USE(?String88:3),TRN,FONT(,8,,FONT:bold)
                         TEXT,AT(1615,4635,1927,417),USE(tmp:InvoiceText),TRN,FONT(,7,,)
                         TEXT,AT(1615,5208,5417,313),USE(tmp:accessories),TRN,FONT(,7,,)
                         STRING('ACCESSORIES'),AT(156,5208),USE(?String100),TRN,FONT(,8,,FONT:bold)
                         STRING('Type'),AT(521,5677),USE(?String98),TRN,FONT(,7,,FONT:bold)
                         STRING('PARTS REQUIRED'),AT(156,5521),USE(?String79),TRN,FONT(,8,,FONT:bold)
                         STRING('Qty'),AT(156,5677),USE(?String80),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(885,5677),USE(?String81),TRN,FONT(,7,,FONT:bold)
                         STRING('Description'),AT(2292,5677),USE(?String82),TRN,FONT(,7,,FONT:bold)
                         STRING('Unit Cost'),AT(4063,5677),USE(?String83),TRN,FONT(,7,,FONT:bold)
                         STRING('Line Cost'),AT(4948,5677),USE(?String89),TRN,FONT(,7,,FONT:bold)
                         LINE,AT(156,5833,7083,0),USE(?Line1),COLOR(COLOR:Black)
                         LINE,AT(156,6667,7083,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('LOAN UNIT ISSUED'),AT(156,6719,1354,156),USE(?LoanUnitIssued),TRN,HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(1667,6719),USE(tmp:LoanModel),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                         STRING('IMEI'),AT(3646,6719),USE(?IMEITitle),TRN,HIDE,FONT('Tahoma',9,,FONT:bold,CHARSET:ANSI)
                         STRING(@s16),AT(4010,6719),USE(tmp:LoanIMEI),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                         STRING('CUSTOMER SIGNATURE'),AT(156,8021),USE(?String820),TRN,FONT('Tahoma',8,,FONT:bold)
                         STRING('REPLACEMENT VALUE'),AT(5104,6719),USE(?LoanValueText),TRN,HIDE,FONT('Tahoma',9,,FONT:bold,CHARSET:ANSI)
                         STRING(@n10.2),AT(6406,6719),USE(tmp:ReplacementValue),TRN,HIDE,RIGHT,FONT(,8,,)
                         LINE,AT(4740,8177,1406,0),USE(?Line5),COLOR(COLOR:Black)
                         STRING('Date: {13}/ {14}/'),AT(4531,8021,1563,156),USE(?String830),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1615,8177,2917,0),USE(?Line5:2),COLOR(COLOR:Black)
                         STRING('LOAN ACCESSORIES'),AT(156,6875,1354,156),USE(?LoanAccessoriesTitle),TRN,HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s255),AT(1667,6875,5417,208),USE(tmp:LoanAccessories),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                         STRING('EXTERNAL DAMAGE CHECK LIST'),AT(156,7083),USE(?ExternalDamageCheckList),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Notes:'),AT(5781,7083),USE(?Notes),TRN,FONT(,7,,)
                         TEXT,AT(6094,7083,1458,469),USE(jobe2:XNotes),TRN,FONT(,6,,)
                         CHECK('None'),AT(5156,7083,625,156),USE(jobe2:XNone),TRN
                         CHECK('Keypad'),AT(156,7240,729,156),USE(jobe2:XKeypad),TRN
                         CHECK('Charger'),AT(2083,7240,781,156),USE(jobe2:XCharger),TRN
                         CHECK('Antenna'),AT(2083,7083,833,156),USE(jobe2:XAntenna),TRN
                         CHECK('Lens'),AT(2927,7083,573,156),USE(jobe2:XLens),TRN
                         CHECK('F/Cover'),AT(3552,7083,781,156),USE(jobe2:XFCover),TRN
                         CHECK('B/Cover'),AT(4438,7083,729,156),USE(jobe2:XBCover),TRN
                         CHECK('Battery'),AT(990,7240,729,156),USE(jobe2:XBattery),TRN
                         CHECK('LCD'),AT(2927,7250,625,156),USE(jobe2:XLCD),TRN
                         CHECK('System Connector'),AT(4438,7250,1250,156),USE(jobe2:XSystemConnector),TRN
                         TEXT,AT(156,7500,5885,260),USE(locTermsText),TRN,FONT(,7,,)
                         CHECK('Sim Reader'),AT(3552,7240,885,156),USE(jobe2:XSimReader),TRN
                         TEXT,AT(156,7771,7396,250),USE(stt:Text),TRN,FONT(,7,,)
                         STRING(@s30),AT(156,1927),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4167,2083),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4427,2083),USE(Delivery_Telephone_Temp),TRN,FONT(,8,,)
                         STRING(@s50),AT(521,2083,3490,156),USE(invoice_EMail_Address),TRN,LEFT,FONT(,8,,)
                         STRING('Email:'),AT(156,2083),USE(?unnamed:4),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,2240),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2240),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(156,2396),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2396),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(2760,1302),USE(job_ali:Account_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(4708,3073),USE(job_ali:Warranty_Charge_Type),TRN,FONT(,8,,)
                         STRING(@s20),AT(6219,3073),USE(job_ali:Repair_Type_Warranty),TRN,FONT(,8,,)
                         STRING('Acc No:'),AT(2188,1302),USE(?String94),TRN,FONT(,8,,)
                         STRING(@s30),AT(4167,1302),USE(delivery_Company_Name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4167,1458,1917,156),USE(Delivery_Address1_Temp),TRN,FONT(,8,,)
                         STRING(@s20),AT(2760,1458),USE(job_ali:Order_Number),TRN,FONT(,8,,)
                         STRING('Order No:'),AT(2188,1458),USE(?String140),TRN,FONT(,8,,)
                         STRING(@s30),AT(4167,1615),USE(Delivery_address2_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4167,1771),USE(Delivery_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4167,1927),USE(Delivery_address4_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1302),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1458),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1771),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1615),USE(invoice_address2_temp),TRN,FONT(,8,,)
                         STRING(@s65),AT(4167,2396,2865,156),USE(clientName),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:3)
DETAIL                   DETAIL,AT(,,,115),USE(?DetailBand)
                           STRING(@n8b),AT(-104,0),USE(Quantity_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s25),AT(885,0),USE(part_number_temp),TRN,FONT(,7,,)
                           STRING(@s25),AT(2292,0),USE(Description_temp),TRN,FONT(,7,,)
                           STRING(@n14.2b),AT(3802,0),USE(Cost_Temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n14.2b),AT(4740,0),USE(line_cost_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s4),AT(521,0),USE(part_type_temp),TRN,FONT(,7,,)
                         END
                         FOOTER,AT(396,8896,,2406),USE(?unnamed:2),TOGETHER,ABSOLUTE
                           STRING('Loan Deposit Paid'),AT(4844,21),USE(?LoanDepositPaidTitle),TRN,HIDE,FONT(,8,,FONT:regular,CHARSET:ANSI)
                           STRING(@n14.2b),AT(6396,21),USE(tmp:LoanDepositPaid),TRN,HIDE,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Completed:'),AT(52,156),USE(?String66),TRN,FONT(,8,,)
                           STRING(@D6b),AT(1094,167),USE(job_ali:Date_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@t1b),AT(2135,167),USE(job_ali:Time_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('QA Passed:'),AT(52,313),USE(?qa_passed),TRN,HIDE,FONT(,8,,)
                           STRING('Labour:'),AT(4844,167),USE(?labour_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,167),USE(labour_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Outgoing Courier:'),AT(52,573),USE(?String66:2),TRN,FONT(,8,,)
                           STRING(@s20),AT(2917,469,1760,198),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(2917,260,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Carriage:'),AT(4844,458),USE(?carriage_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,313),USE(parts_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(4844,604),USE(?vat_String),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,458),USE(courier_cost_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@d6b),AT(1094,313),USE(job_ali:Date_QA_Passed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('Total:'),AT(4844,781),USE(?total_string),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(6281,781,1000,0),USE(?line),COLOR(COLOR:Black)
                           STRING(@n14.2b),AT(6302,781),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('<128>'),AT(6250,781),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                           STRING('FAULT CODES'),AT(52,1146),USE(?String107),TRN,FONT(,8,,FONT:bold)
                           STRING('Booking Option:'),AT(4844,938),USE(?BookingOption),TRN,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(5823,938),USE(tmp:BookingOption),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s5),AT(52,885),USE(tmp:bouncers),TRN,HIDE,FONT(,,,FONT:bold)
                           STRING(@s30),AT(52,1302),USE(fault_code_field_temp[1]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,1302,4844,208),USE(tmp:FaultCodeDescription[1]),TRN,FONT(,8,,)
                           STRING(@s30),AT(52,1458),USE(fault_code_field_temp[2]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,1458,4844,208),USE(tmp:FaultCodeDescription[2]),FONT(,8,,)
                           STRING(@s30),AT(52,1615),USE(fault_code_field_temp[3]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,1615,4844,208),USE(tmp:FaultCodeDescription[3]),FONT(,8,,)
                           STRING(@s30),AT(52,1771),USE(fault_code_field_temp[4]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,1771,4844,208),USE(tmp:FaultCodeDescription[4]),FONT(,8,,)
                           STRING(@s30),AT(52,1927),USE(fault_code_field_temp[5]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,1927,4844,208),USE(tmp:FaultCodeDescription[5]),FONT(,8,,)
                           STRING(@s30),AT(52,2083),USE(fault_code_field_temp[6]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,2083,4844,208),USE(tmp:FaultCodeDescription[6]),FONT(,8,,)
                           STRING(@s16),AT(2917,729,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@n14.2b),AT(6396,604),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s13),AT(1094,729),USE(jobe2:IDNumber),TRN,FONT(,8,,FONT:bold)
                           STRING('Customer ID No:'),AT(52,729),USE(?String66:3),TRN,FONT(,8,,)
                           STRING(@s20),AT(1094,573),USE(job_ali:Courier),TRN,FONT(,8,,FONT:bold)
                           STRING(@t1b),AT(2135,313),USE(job_ali:Time_QA_Passed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(2760,938,2031,260),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Parts:'),AT(4844,313),USE(?parts_string),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10271,7521,1156),USE(?Fault_Code9:2)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('JOB CARD'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),TRN,FONT(,8,,FONT:bold)
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s20),AT(625,677,1771,156),USE(address:VatNumber),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),TRN,FONT(,8,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(156,1510),USE(?String24),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('GENERAL DETAILS'),AT(156,2990),USE(?String91),TRN,FONT(,8,,FONT:bold)
                         STRING('COMPLETION DETAILS'),AT(156,8563),USE(?String73),TRN,FONT(,8,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4844,8563),USE(?String74),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3667),USE(?String50),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(5052,417,2646,1042),USE(?BoxGrey:TopDetails),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,1719,3604,1302),USE(?BoxGrey:Address1),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4115,1719,3604,1302),USE(?BoxGrey:Address2),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,3177,7552,521),USE(?BoxGrey:Title),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,3854,7552,4740),USE(?BoxGrey:Detail),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,8750,2813,1042),USE(?BoxGrey:Total1),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4896,8750,2813,1042),USE(?BoxGrey:Total2),ROUND,FILL(COLOR:Silver)
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,8698,2813,1042),USE(?Box:Total1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(4844,8698,2813,1042),USE(?Box:Total2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Job_Card')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Job_Card')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'JobCard'
  If PrintOption(PreviewReq,glo:ExportReport,'Job Card') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBEXACC.Open
  Relate:JOBPAYMT.Open
  Relate:JOBS2_ALIAS.Open
  Relate:LOAN.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:JOBNOTES_ALIAS.UseFile
  Access:JOBACC.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  Access:MANUFACT.UseFile
  Access:JOBOUTFL.UseFile
  Access:LOANACC.UseFile
  Access:JOBSE.UseFile
  Access:MANFAULO.UseFile
  Access:MANFAULT.UseFile
  Access:JOBSE2.UseFile
  Access:JOBRPNOT.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        count# = 0
        
        If job_ali:warranty_job = 'YES'
            setcursor(cursor:wait)
            save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:part_number_key)
            wpr:ref_number  = job_ali:ref_number
            set(wpr:part_number_key,wpr:part_number_key)
            loop
                if access:warparts.next()
                   break
                end !if
                if wpr:ref_number  <> job_ali:ref_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                count# += 1
                part_number_temp = wpr:part_number
                part_type_temp = 'Warr'
                description_temp = wpr:description
                quantity_temp = wpr:quantity
                cost_temp = ''
                line_cost_temp = ''
                Print(rpt:detail)
            end !loop
            access:warparts.restorefile(save_wpr_id)
            setcursor()
        
        End!If job_ali:warranty_job = 'YES'
        
        If job_ali:chargeable_job = 'YES'
            setcursor(cursor:wait)
            save_par_id = access:parts.savefile()
            access:parts.clearkey(par:part_number_key)
            par:ref_number  = job_ali:ref_number
            set(par:part_number_key,par:part_number_key)
            loop
                if access:parts.next()
                   break
                end !if
                if par:ref_number  <> job_ali:ref_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                count# += 1
                part_Type_temp = 'Char'
                description_temp = par:description
                quantity_temp = par:quantity
                part_number_temp = par:part_number
                if glo:webjob then
                    cost_temp = par:RRCsalecost
                    line_cost_temp = par:quantity * par:RRCsalecost
                    !message('Cost '&par:rrcsalecost&'|Ave price'&par:RRCAveragePurchaseCost)
                    !showed that all rrcsalecosts were zero
                else
                    cost_temp = par:sale_cost
                    line_cost_temp = par:quantity * par:sale_cost
                end !if glo:webjob
        
                Print(rpt:detail)
            end !loop
            access:parts.restorefile(save_par_id)
            setcursor()
        
        End!If job_ali:chargeable_job = 'YES'
        If count# = 0
            part_number_temp = ''
            Print(rpt:detail)
        End!If count# = 0
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','Job_Card')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:JOBEXACC.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:LOAN.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job_ali:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Found
            tmp:Ref_Number = job_ali:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Error
            tmp:Ref_Number = job_ali:Ref_Number
        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
        tmp:Ref_Number = job_ali:Ref_Number
    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

    !**************************************************
    access:users.clearkey(use:user_code_key)
    use:user_code   = job_ali:engineer
    IF access:users.fetch(use:user_code_Key) = Level:Benign
        engineer_temp = Clip(use:forename) & ' ' & Clip(use:surname)
    ELSE
        engineer_temp = job_ali:engineer
    END ! IF access:users.fetch(use:user_code_Key) = Level:Benign
    !**********Look up who booked the job**************
    access:users.clearkey(use:user_code_key)
    use:user_code = job_ali:who_booked
    IF access:users.fetch(use:user_code_key) = Level:Benign
        Who_Booked = Clip(use:Forename) & ' ' & Clip(use:Surname)
    ELSE
        Who_Booked = job_ali:Who_Booked
    END ! IF access:users.fetch(use:user_code_key) = Level:Benign
    !**************************************************
    access:users.clearkey(use:user_code_key)
    use:user_code = job_ali:despatch_user
    If access:users.fetch(use:user_code_key) = Level:Benign
        despatched_user_temp = Clip(use:forename) & ' ' & Clip(use:surname)
    ELSE
        despatched_user_temp = job_ali:despatch_user
    End! If access:users.fetch(use:user_code_key) = Level:Benign
      !**************************************************
  !*********CHANGE LICENSE ADDRESS*********
  set(defaults)
  access:defaults.next()
  Access:WEBJOB.Clearkey(wob:RefNumberKey)
  wob:RefNumber   = glo:Select1
  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      If (glo:WebJob = 1)
          tra:Account_Number = wob:HeadAccountNumber
      ELSE ! If (glo:WebJob = 1)
          tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      END ! If (glo:WebJob = 1)
      If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          address:SiteName        = tra:Company_Name
          address:Name            = tra:coTradingName
          address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
          address:Location        = tra:coLocation
          address:RegistrationNo  = tra:coRegistrationNo
          address:AddressLine1    = tra:coAddressLine1
          address:AddressLine2    = tra:coAddressLine2
          address:AddressLine3    = tra:coAddressLine3
          address:AddressLine4    = tra:coAddressLine4
          address:Telephone       = tra:coTelephoneNumber
          address:Fax             = tra:coFaxNumber
          address:EmailAddress    = tra:coEmailAddress
          address:VatNumber       = tra:coVATNumber
      END
  
  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Error
  End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  !Barcode Bit and setup refno
  code_temp            = 3
  option_temp          = 0
  
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp      = 'Job No: ' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp             = 'I.M.E.I.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
    Set(DEFAULT2)
    Access:DEFAULT2.Next()

    set(defaults)
    access:defaults.next()

    Access:jobse.clearkey(jobe:RefNumberKey)
    jobe:RefNumber = job_ali:ref_number
    access:jobse.fetch(jobe:RefNumberKey)

    ! Inserting (DBH 10/04/2006) #7305 - Get JOBSE2 Record
    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber = job_ali:Ref_Number
    If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
    ! Found

    Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
    ! Error
    End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
    ! End (DBH 10/04/2006) #7305

    ! Inserting (DBH 21/04/2006) #7551 - Get WEB job to show the extra fault codes
    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job_ali:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
    ! End (DBH 21/04/2006) #7551

    Settarget(Report)

    ! Show Engineering Option If Set - 4285 (DBH: 10-06-2004)
    Case jobe:Engineer48HourOption
    Of 1
        tmp:BookingOption         = '48 Hour Exchange'
        ?BookingOPtion{prop:Text} = 'Engineer Option:'
    Of 2
        tmp:BookingOption         = 'ARC Repair'
        ?BookingOPtion{prop:Text} = 'Engineer Option:'
    Of 3
        tmp:BookingOption         = '7 Day TAT'
        ?BookingOPtion{prop:Text} = 'Engineer Option:'
    Of 4
        tmp:BookingOption         = 'Standard Repair'
        ?BookingOPtion{prop:Text} = 'Engineer Option:'
    Else
        Case jobe:Booking48HourOption
        Of 1
            tmp:BookingOption = '48 Hour Exchange'
        Of 2
            tmp:BookingOption = 'ARC Repair'
        Of 3
            tmp:BookingOption = '7 Day TAT'
        Else
            tmp:BookingOption = 'N/A'
        End ! Case jobe:Booking48HourOption
    End ! Case jobe:Engineer48HourOption

    If job_ali:warranty_job <> 'YES'
        Hide(?job_ali:warranty_charge_type)
        Hide(?job_ali:repair_type_warranty)
        Hide(?warranty_type)
        Hide(?Warranty_repair_Type)
    End! If job_ali:warranty_job <> 'YES'

    If job_ali:chargeable_job <> 'YES'
        Hide(?job_ali:charge_type)
        Hide(?job_ali:repair_type)
        Hide(?Chargeable_type)
        Hide(?repair_Type)
    End! If job_ali:chargeable_job <> 'YES'

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = job_ali:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        ! Found
        If man:UseQA
            UnHide(?qa_passed)
        End! If def:qa_required <> 'YES'

    Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
    ! Error
    End ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign


    If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
        Hide(?labour_string)
        Hide(?parts_string)
        Hide(?Carriage_string)
        Hide(?vat_string)
        Hide(?total_string)
        Hide(?line)
        Hide(?Euro)
    End! If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'

    If job_ali:estimate = 'YES' and job_ali:Chargeable_Job = 'YES'
        Unhide(?estimate)
        estimate_value_temp = 'ESTIMATE REQUIRED IF REPAIR COST EXCEEDS ' & Clip(Format(job_ali:estimate_if_over, @n14.2)) & ' (Plus V.A.T.)'
    End ! If





    !------------------------------------------------------------------
    IF CLIP(job_ali:title) = ''
        customer_name_temp = job_ali:initial
    ELSIF CLIP(job_ali:initial) = ''
        customer_name_temp = job_ali:title
    ELSE
        customer_name_temp = CLIP(job_ali:title) & ' ' & CLIP(job_ali:initial)
    END ! IF
    !------------------------------------------------------------------
    IF CLIP(customer_name_temp) = ''
        customer_name_temp = job_ali:surname
    ELSIF CLIP(job_ali:surname) = ''
    ! customer_name_temp = customer_name_temp ! tautology
    ELSE
        customer_name_temp = CLIP(customer_name_temp) & ' ' & CLIP(job_ali:surname)
    END ! IF
    !------------------------------------------------------------------

    Settarget()

    endUserTelNo = ''

    if jobe:EndUserTelNo <> ''
        endUserTelNo = clip(jobe:EndUserTelNo)
    end ! if

    if customer_name_temp <> ''
        clientName = 'Client: ' & clip(customer_name_temp) & '  Tel: ' & clip(endUserTelNo)
    else
        if endUserTelNo <> ''
            clientName = 'Tel: ' & clip(endUserTelNo)
        end ! if
    end ! if
    !**************************************************
    delivery_name_temp         = customer_name_temp
    delivery_company_Name_temp = job_ali:Company_Name_Delivery

    delivery_address1_temp = job_ali:address_line1_delivery
    delivery_address2_temp = job_ali:address_line2_delivery
    If job_ali:address_line3_delivery   = ''
        delivery_address3_temp = job_ali:postcode_delivery
        delivery_address4_temp = ''
    Else
        delivery_address3_temp = job_ali:address_line3_delivery
        delivery_address4_temp = job_ali:postcode_delivery
    End ! If
    delivery_telephone_temp     = job_ali:Telephone_Delivery
    !**************************************************


    !**************************************************

    ! If an RRC job, then put the RRC as the invoice address,

    If ~glo:WebJob And jobe:WebJob
        SetTarget(Report)
        ?DeliveryAddress{prop:Text} = 'CUSTOMER ADDRESS'
        SetTarget()
        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber   = job_ali:Ref_Number
        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            ! Found
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = wob:HeadAccountNumber
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                ! Found
                invoice_address1_temp = tra:address_line1
                invoice_address2_temp = tra:address_line2
                If job_ali:address_line3_delivery   = ''
                    invoice_address3_temp = tra:postcode
                    invoice_address4_temp = ''
                Else
                    invoice_address3_temp = tra:address_line3
                    invoice_address4_temp = tra:postcode
                End ! If
                invoice_company_name_temp     = tra:company_name
                invoice_telephone_number_temp = tra:telephone_number
                invoice_fax_number_temp       = tra:fax_number
                invoice_EMail_Address         = tra:EmailAddress
            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Error
            End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
        End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

    Else ! If ~glo:WebJob And jobe:WebJob


        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job_ali:account_number
        access:subtracc.fetch(sub:account_number_key)

        access:tradeacc.clearkey(tra:account_number_key)
        tra:account_number = sub:main_account_number
        access:tradeacc.fetch(tra:account_number_key)
        if tra:invoice_sub_accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
            If sub:invoice_customer_address = 'YES'
                invoice_address1_temp = job_ali:address_line1
                invoice_address2_temp = job_ali:address_line2
                If job_ali:address_line3_delivery   = ''
                    invoice_address3_temp = job_ali:postcode
                    invoice_address4_temp = ''
                Else
                    invoice_address3_temp = job_ali:address_line3
                    invoice_address4_temp = job_ali:postcode
                End ! If
                invoice_company_name_temp     = job_ali:company_name
                invoice_telephone_number_temp = job_ali:telephone_number
                invoice_fax_number_temp       = job_ali:fax_number
                invoice_EMail_Address         = jobe:EndUserEmailAddress  ! '' ! tra:EmailAddress
            ! MEssage('case 1')
            Else! If sub:invoice_customer_address = 'YES'
                invoice_address1_temp = sub:address_line1
                invoice_address2_temp = sub:address_line2
                If job_ali:address_line3_delivery   = ''
                    invoice_address3_temp = sub:postcode
                    invoice_address4_temp = ''
                Else
                    invoice_address3_temp = sub:address_line3
                    invoice_address4_temp = sub:postcode
                End ! If
                invoice_company_name_temp     = sub:company_name
                invoice_telephone_number_temp = sub:telephone_number
                invoice_fax_number_temp       = sub:fax_number
                invoice_EMail_Address         = sub:EmailAddress

            End! If sub:invoice_customer_address = 'YES'

        else! if tra:use_sub_accounts = 'YES'
            If tra:invoice_customer_address = 'YES'
                invoice_address1_temp = job_ali:address_line1
                invoice_address2_temp = job_ali:address_line2
                If job_ali:address_line3_delivery   = ''
                    invoice_address3_temp = job_ali:postcode
                    invoice_address4_temp = ''
                Else
                    invoice_address3_temp = job_ali:address_line3
                    invoice_address4_temp = job_ali:postcode
                End ! If
                invoice_company_name_temp     = job_ali:company_name
                invoice_telephone_number_temp = job_ali:telephone_number
                invoice_fax_number_temp       = job_ali:fax_number
                invoice_EMail_Address         = jobe:EndUserEmailAddress ! '' ! tra:EmailAddress

            Else! If tra:invoice_customer_address = 'YES'
                !            UseAlternativeAddress# = 1
                invoice_address1_temp = tra:address_line1
                invoice_address2_temp = tra:address_line2
                If job_ali:address_line3_delivery   = ''
                    invoice_address3_temp = tra:postcode
                    invoice_address4_temp = ''
                Else
                    invoice_address3_temp = tra:address_line3
                    invoice_address4_temp = tra:postcode
                End ! If
                invoice_company_name_temp     = tra:company_name
                invoice_telephone_number_temp = tra:telephone_number
                invoice_fax_number_temp       = tra:fax_number
                invoice_EMail_Address         = tra:EmailAddress

            End! If tra:invoice_customer_address = 'YES'
        end!!if tra:use_sub_accounts = 'YES'
    End ! If ~glo:WebJob And jobe:WebJob

    !**************************************************


    !*************************set the display address to the head account if booked on as a web job***********************
    ! but only if this was not from a generic account


    !*********************************************************************************************************************


    !**************************************************
    If job_ali:chargeable_job <> 'YES'
        labour_temp       = ''
        parts_temp        = ''
        courier_cost_temp = ''
        vat_temp          = ''
        total_temp        = ''
    Else! If job_ali:chargeable_job <> 'YES'
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job_ali:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Error
        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        If job_ali:invoice_number <> ''
            ! Total_Price('I',vat",total",balance")
            access:invoice.clearkey(inv:invoice_number_key)
            inv:invoice_number = job_ali:invoice_number
            access:invoice.fetch(inv:invoice_number_key)
            if glo:webjob then
                If inv:ExportedRRCOracle
                    Labour_Temp = jobe:InvRRCCLabourCost
                    Parts_Temp  = jobe:InvRRCCPartsCost
                    Vat_Temp    = jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100 + |
                    jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100 +                 |
                    job_ali:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100
                    courier_cost_temp = job_ali:invoice_courier_cost
                Else ! If inv:ExportedRRCOracle
                    Labour_Temp = jobe:RRCCLabourCost
                    Parts_Temp  = jobe:RRCCPartsCost
                    Vat_Temp    = jobe:RRCClabourCost * VatRate(job_ali:Account_Number, 'L') / 100 + |
                    jobe:RRCCPartsCost * VatRate(job_ali:Account_Number, 'P') / 100 +                |
                    job_ali:Courier_Cost * VatRate(job_ali:Account_Number, 'L') / 100
                    courier_cost_temp = job_ali:courier_cost
                End ! If inv:ExportedRRCOracle
            else
                labour_temp = job_ali:invoice_labour_cost
                Parts_temp  = job_ali:Invoice_Parts_Cost
                Vat_temp    = job_ali:Invoice_Labour_Cost * inv:Vat_Rate_Labour / 100 + |
                job_ali:Invoice_Parts_Cost * inv:Vat_Rate_Parts / 100 +                 |
                job_ali:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100
                courier_cost_temp = job_ali:invoice_courier_cost
            end ! if

        Else! If job_ali:invoice_number <> ''
            If glo:Webjob Then
                Labour_Temp = jobe:RRCCLabourCost
                Parts_Temp  = jobe:RRCCPartsCost
                Vat_Temp    = jobe:RRCClabourCost * VatRate(job_ali:Account_Number, 'L') / 100 + |
                jobe:RRCCPartsCost * VatRate(job_ali:Account_Number, 'P') / 100 +                |
                job_ali:Courier_Cost * VatRate(job_ali:Account_Number, 'L') / 100
            Else
                labour_temp = job_ali:labour_cost
                parts_temp  = job_ali:parts_cost
                Vat_Temp    = job_ali:Labour_Cost * VatRate(job_ali:Account_Number, 'L') / 100 + |
                job_ali:Parts_Cost * VatRate(job_ali:Account_Number, 'P') / 100 +                |
                job_ali:Courier_Cost * VatRate(job_ali:Account_Number, 'L') / 100
            End ! If

            courier_cost_temp = job_ali:courier_cost
        End! If job_ali:invoice_number <> ''
        Total_Temp  = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp

    End! If job_ali:chargeable_job <> 'YES'
    !**************************************************

    setcursor(cursor:wait)

    FieldNumber# = 1
    save_maf_id  = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = job_ali:manufacturer
    set(maf:field_number_key, maf:field_number_key)
    loop
        if access:manfault.next()
            break
        end ! if
        if maf:manufacturer <> job_ali:manufacturer      |
            then break   ! end if
        end ! if
        yldcnt# += 1
        if yldcnt# > 25
            yield() 
            yldcnt# = 0
        end ! if

        If maf:Compulsory_At_Booking <> 'YES'
            Cycle
        End ! If maf:Compulsory_At_Booking <> 'YES'

        fault_code_field_temp[FieldNumber#] = maf:field_name

        Access:MANFAULO.ClearKey(mfo:Field_Key)
        mfo:Manufacturer = job_ali:Manufacturer
        mfo:Field_Number = maf:Field_Number


        Case maf:Field_Number
        Of 1
            mfo:Field = job_ali:Fault_Code1
        Of 2
            mfo:Field = job_ali:Fault_Code2
        Of 3
            mfo:Field = job_ali:Fault_Code3
        Of 4
            mfo:Field = job_ali:Fault_Code4
        Of 5
            mfo:Field = job_ali:Fault_Code5
        Of 6
            mfo:Field = job_ali:Fault_Code6
        Of 7
            mfo:Field = job_ali:Fault_Code7
        Of 8
            mfo:Field = job_ali:Fault_Code8
        Of 9
            mfo:Field = job_ali:Fault_Code9
        Of 10
            mfo:Field = job_ali:Fault_Code10
        Of 11
            mfo:Field = job_ali:Fault_Code11
        Of 12
            mfo:Field = job_ali:Fault_Code12
        ! Inserting (DBH 21/04/2006) #7551 - Display the extra fault codes, if comp at booking
        Of 13
            mfo:Field = wob:FaultCode13
        Of 14
            mfo:Field = wob:FaultCode14
        Of 15
            mfo:Field = wob:FaultCode15
        Of 16
            mfo:Field = wob:FaultCode16
        Of 17
            mfo:Field = wob:FaultCode17
        Of 18
            mfo:Field = wob:FaultCode18
        Of 19
            mfo:Field = wob:FaultCode19
        Of 20
            mfo:Field = wob:FaultCode20
        End ! Case maf:Field_Number
        ! End (DBH 21/04/2006) #7551

        tmp:FaultCodeDescription[FieldNumber#] = mfo:Field

        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            ! Found
            tmp:FaultCodeDescription[FieldNumber#] = Clip(tmp:FaultCodeDescription[FieldNumber#]) & ' - ' & Clip(mfo:Description)
        Else! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
            ! Error
            ! Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign

        FieldNumber# += 1
        If FieldNumber# > 6
            Break
        End ! If FieldNumber# > 6
    end ! loop
    access:manfault.restorefile(save_maf_id)
    setcursor()

    tmp:accessories = ''
    setcursor(cursor:wait)
    x#          = 0
    Save_jac_ID = Access:JOBACC.SaveFile()
    Access:JOBACC.ClearKey(jac:DamagedKey)
    jac:Ref_Number = job_ali:Ref_Number
    jac:Damaged    = 0
    jac:Pirate     = 0
    Set(jac:DamagedPirateKey, jac:DamagedPirateKey)
    Loop
        If Access:JOBACC.NEXT()
            Break
        End ! If
        If jac:Ref_Number <> job_ali:Ref_Number |
            Then Break
        End ! If
        yldcnt# += 1
        if yldcnt# > 25
            yield() 
            yldcnt# = 0
        end ! if
        ! If ARC, only show accessories that were "sent to ARC" - 4285 (DBH: 10-06-2004)
        If ~glo:WebJob
            IF ~jac:Attached
                Cycle
            END ! IF ~jac:Attached
        End ! If ~glo:WebJob And jac:Attached
        x# += 1
        If jac:Damaged
            jac:Accessory = 'DAMAGED ' & Clip(jac:Accessory)
        End ! If jac:Damaged
        If jac:Pirate
            jac:Accessory = 'PIRATE ' & Clip(jac:Accessory)
        End ! If jac:Pirate
        If tmp:accessories = ''
            tmp:accessories = jac:accessory
        Else! If tmp:accessories = ''
            tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
        End! If tmp:accessories = ''
    End ! loop
    access:jobacc.restorefile(save_jac_id)
    setcursor()


    If job_ali:exchange_unit_number <> ''
        access:exchange.clearkey(xch:ref_number_key)
        xch:ref_number = job_ali:exchange_unit_number
        if access:exchange.fetch(xch:ref_number_key) = Level:Benign
            exchange_unit_number_temp  = 'Unit: ' & Clip(job_ali:exchange_unit_number)
            exchange_model_number      = xch:model_number
            exchange_manufacturer_temp = xch:manufacturer
            exchange_unit_type_temp    = 'N/A'
            exchange_esn_temp          = xch:esn
            exchange_unit_title_temp   = 'EXCHANGE UNIT'
            exchange_msn_temp          = xch:msn
        end! if access:exchange.fetch(xch:ref_number_key) = Level:Benign
    Else! If job_ali:exchange_unit_number <> ''
        x#          = 0
        save_jea_id = access:jobexacc.savefile()
        access:jobexacc.clearkey(jea:part_number_key)
        jea:job_ref_number = job_ali:ref_number
        set(jea:part_number_key, jea:part_number_key)
        loop
            if access:jobexacc.next()
                break
            end ! if
            if jea:job_ref_number <> job_ali:ref_number      |
                then break   ! end if
            end ! if
            x# = 1
            Break
        end ! loop
        access:jobexacc.restorefile(save_jea_id)
        If x# = 1
            exchange_unit_number_temp  = ''
            exchange_model_number      = 'N/A'
            exchange_manufacturer_temp = job_ali:manufacturer
            exchange_unit_type_temp    = 'ACCESSORY'
            exchange_esn_temp          = 'N/A'
            exchange_unit_title_temp   = 'EXCHANGE UNIT'
            exchange_msn_temp          = 'N/A'
        End! If x# = 1
    End! If job_ali:exchange_unit_number <> ''

    ! Check For Bouncer
    access:manufact.clearkey(man:manufacturer_key)
    man:manufacturer = job_ali:manufacturer
    access:manufact.tryfetch(man:manufacturer_key)

    tmp:bouncers = 0

    If job_ali:ESN <> 'N/A' And job_ali:ESN <> ''
        setcursor(cursor:wait)
        save_job2_id = access:jobs2_alias.savefile()
        access:jobs2_alias.clearkey(job2:esn_key)
        job2:esn = job_ali:esn
        set(job2:esn_key, job2:esn_key)
        loop
            if access:jobs2_alias.next()
                break
            end ! if
            if job2:esn <> job_ali:esn      |
                then break   ! end if
            end ! if
            yldcnt# += 1
            if yldcnt# > 25
                yield() 
                yldcnt# = 0
            end ! if
            If job2:Cancelled = 'YES'
                Cycle
            End ! If job2:Cancelled = 'YES'

            If job2:ref_number <> job_ali:ref_number
! Deleting (DBH 14/02/2008) # 9718 - Show all bouncers regardless
!                If job2:date_booked + man:warranty_period > job_ali:date_booked And job2:date_booked <= job_ali:date_booked
! End (DBH 14/02/2008) #9718
                tmp:Bouncers += 1
! Delete --- Don't show bouncers anymore. Only show a "code" (DBH: 07/07/2010) #11467
!                    If tmp:bouncers = ''
!                        tmp:bouncers = 'Previous Job Number(s): ' & job2:ref_number
!                    Else! If tmp:bouncer = ''
!                        tmp:bouncers = CLip(tmp:bouncers) & ', ' & job2:ref_number
!                    End! If tmp:bouncer = ''
!
! end --- (DBH: 07/07/2010) #11467
!                End! If job2:date_booked + man:warranty_period < Today()
            End! If job2:esn <> job2:ref_number
        end ! loop
        access:jobs2_alias.restorefile(save_job2_id)
        setcursor()
    End ! job_ali:ESN <> 'N/A' And job_ali:ESN <> ''

    If tmp:bouncers <> 0! And ~glo:WebJob
        Settarget(report)
!        Unhide(?bouncertitle)
        ! #11467 Only show bouncers as a count at both ARC and RRC(DBH: 07/07/2010)
        tmp:Bouncers = clip(tmp:Bouncers) & 'H'
        Unhide(?tmp:bouncers)
        Settarget()
    End! If CheckBouncer(job_ali:ref_number)

    If man:UseInvTextForFaults
        tmp:InvoiceText = ''
        Save_joo_ID     = Access:JOBOUTFL.SaveFile()
        Access:JOBOUTFL.ClearKey(joo:LevelKey)
        joo:JobNumber = job_ali:Ref_Number
        joo:Level     = 10
        Set(joo:LevelKey, joo:LevelKey)
        Loop
            If Access:JOBOUTFL.PREVIOUS()
                Break
            End ! If
            If joo:JobNumber <> job_ali:Ref_Number      |
                Then Break   ! End If
            End ! If
            !do not include any 0 entry values
            If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                Cycle
            End ! If Instring('IMEI VALIDATION: ',joo:Description,1,1)
            If tmp:InvoiceText = ''
                tmp:InvoiceText = Clip(joo:FaultCode) & ' - ' & Clip(joo:Description)
            Else ! If tmp:InvoiceText = ''
                tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(joo:FaultCode) & ' - ' & Clip(joo:Description)
            End ! If tmp:InvoiceText = ''
        End ! Loop
        Access:JOBOUTFL.RestoreFile(Save_joo_ID)
    Else ! man:UseInvTextForFaults
        tmp:InvoiceText = jbn_ali:Invoice_Text
    End ! man:UseInvTextForFaults

    ! Loan unit information (Added by Gary (7th Aug 2002))
    ! //------------------------------------------------------------------------
    tmp:LoanModel       = ''
    tmp:LoanIMEI        = ''
    tmp:LoanAccessories = ''

    if job_ali:Loan_Unit_Number <> 0                                            ! Is a loan unit attached to this job ?

        save_loa_id = Access:Loan.SaveFile()
        Access:Loan.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number  = job_ali:Loan_Unit_Number
        if not Access:Loan.TryFetch(loa:Ref_Number_Key)                         ! Fetch loan unit record
            tmp:LoanModel        = clip(loa:Manufacturer) & ' ' & loa:Model_Number
            tmp:LoanIMEI         = loa:ESN
            tmp:ReplacementValue = jobe:LoanReplacementValue

            save_lac_id = Access:LoanAcc.SaveFile()
            Access:LoanAcc.ClearKey(lac:ref_number_key)
            lac:ref_number = loa:ref_number
            set(lac:ref_number_key, lac:ref_number_key)
            loop until Access:LoanAcc.Next()                                    ! Fetch loan accessories
                if lac:ref_number <> loa:ref_number then break.
                if lac:Accessory_Status <> 'ISSUE' then cycle.                  ! Only display issued loan accessories
                if tmp:LoanAccessories = ''
                    tmp:LoanAccessories = lac:Accessory
                else
                    tmp:LoanAccessories = clip(tmp:LoanAccessories) & ', ' & lac:Accessory
                end ! if

            end ! loop
            Access:LoanAcc.RestoreFile(save_lac_id)

        end ! if
        Access:Loan.RestoreFile(save_loa_id)

    end ! if

    if tmp:LoanModel <> ''                                                      ! Display loan details on report
        SetTarget(report)
        Unhide(?LoanUnitIssued)
        Unhide(?tmp:LoanModel)
        Unhide(?IMEITitle)
        Unhide(?tmp:LoanIMEI)
        Unhide(?LoanAccessoriesTitle)
        Unhide(?tmp:LoanAccessories)
        Unhide(?tmp:ReplacementValue)
        Unhide(?LoanValueText)
        SetTarget()
    end ! if


    tmp:LoanDepositPaid = 0

    Access:JobPaymt.ClearKey(jpt:Loan_Deposit_Key)
    jpt:Ref_Number   = job_ali:Ref_Number
    jpt:Loan_Deposit = True
    set(jpt:Loan_Deposit_Key, jpt:Loan_Deposit_Key)
    loop until Access:JobPaymt.Next()                                           ! Loop through payments attached to job
        if jpt:Ref_Number <> job_ali:Ref_Number then break.
        if jpt:Loan_Deposit <> True then break.
        tmp:LoanDepositPaid += jpt:Amount                                       ! Check for deposit value
    end ! loop

    if tmp:LoanDepositPaid <> 0
        SetTarget(report)
        Unhide(?LoanDepositPaidTitle)
        Unhide(?tmp:LoanDepositPaid)                                            ! Display fields if a deposit was found
        SetTarget()
    end ! if
    ! //------------------------------------------------------------------------
    ! Inserting (DBH 06/11/2007) # 9278 - Show the Repair Notes
    Access:JOBRPNOT.Clearkey(jrn:TheDateKey)
    jrn:RefNumber = job_ali:Ref_Number
    Set(jrn:TheDateKey,jrn:TheDateKey)
    Loop
        If Access:JOBRPNOT.Next()
            Break
        End ! If Access:JOBRPNOT.Previous()
        If jrn:RefNumber <> job_ali:Ref_Number
            Break
        End ! If jrn:RefNumber <> job_ali:Ref_Number
        tmp:RepairNotes = Clip(tmp:RepairNotes) & ' ' & Clip(jrn:Notes)
    End ! Loop
    tmp:RepairNotes = Sub(tmp:RepairNotes,3,1000)
    ! End (DBH 06/11/2007) #9278

  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'JOB CARD'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  
  locTermsText = GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI')   ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Job Card'
  END
  Report{Prop:Preview} = PrintPreviewImage













Estimate PROCEDURE(String f:Type)
RejectRecord         LONG,AUTO
tmp:Ref_Number       STRING(20)
tmp:PrintedBy        STRING(255)
save_epr_id          USHORT,AUTO
save_joo_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Company_Name_Temp STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Delivery_Telephone_Number_Temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
customer_name_temp   STRING(60)
sub_total_temp       REAL
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:InvoiceText      STRING(255)
DefaultAddress       GROUP,PRE(address)
SiteName             STRING(40)
Name                 STRING(40)
Name2                STRING(40)
Location             STRING(40)
AddressLine1         STRING(40)
AddressLine2         STRING(40)
AddressLine3         STRING(40)
AddressLine4         STRING(40)
Telephone            STRING(40)
RegistrationNo       STRING(40)
Fax                  STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Authority_Number)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Ref_Number)
                       JOIN(jbn:RefNumberKey,job:Ref_Number)
                         PROJECT(jbn:Fault_Description)
                       END
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,6917,7521,1927),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,6313),USE(?unnamed)
                         STRING('Job No:'),AT(4927,365),USE(?String25),TRN,FONT(,12,,)
                         STRING(@s20),AT(5760,385),USE(tmp:Ref_Number),TRN,LEFT,FONT(,10,,FONT:bold)
                         STRING('Estimate Date:'),AT(4927,573),USE(?String58),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5760,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(156,1563),USE(customer_name_temp),TRN,FONT(,8,,)
                         STRING(@s60),AT(4063,1563),USE(customer_name_temp,,?customer_name_temp:2),TRN,FONT(,8,,)
                         STRING(@s22),AT(1604,3240),USE(job:Order_Number,,?JOB:Order_Number:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4531,3240),USE(job:Charge_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3156,3240),USE(job:Authority_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(6042,3229),USE(job:Repair_Type),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,3917,1000,156),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(1604,3917,1323,156),USE(job:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3156,3917,1396,156),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(4604,3917,1396,156),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(6083,3917),USE(job:MSN),TRN,FONT(,8,,)
                         STRING('Reported Fault: '),AT(156,4323),USE(?String64),TRN,FONT(,8,,FONT:bold)
                         TEXT,AT(1615,4323,5625,417),USE(jbn:Fault_Description),TRN,FONT(,8,,,CHARSET:ANSI)
                         TEXT,AT(1615,5052,5625,885),USE(tmp:InvoiceText),TRN,FONT(,8,,,CHARSET:ANSI)
                         STRING('Engineers Report:'),AT(156,5000),USE(?String88),TRN,FONT(,8,,FONT:bold)
                         GROUP,AT(104,5938,7500,208),USE(?PartsHeadingGroup),FONT('Arial',,,)
                           STRING('PARTS REQUIRED'),AT(156,6042),USE(?String79),TRN,FONT(,9,,FONT:bold)
                           STRING('Qty'),AT(1510,6042),USE(?String80),TRN,FONT(,8,,FONT:bold)
                           STRING('Part Number'),AT(1917,6042),USE(?String81),TRN,FONT(,8,,FONT:bold)
                           STRING('Description'),AT(3677,6042),USE(?String82),TRN,FONT(,8,,FONT:bold)
                           STRING('Unit Cost'),AT(5719,6042),USE(?UnitCost),TRN,FONT(,8,,FONT:bold)
                           STRING('Line Cost'),AT(6677,6042),USE(?LineCost),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(1406,6198,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         END
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4063,2500),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4323,2500),USE(Delivery_Telephone_Number_Temp),FONT(,8,,)
                         STRING('Tel:'),AT(156,2500),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(417,2500),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(1927,2500),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2240,2500),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3240),USE(job:Account_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,1719),USE(Delivery_Company_Name_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,1875,1917,156),USE(Delivery_Address1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2031),USE(Delivery_address2_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2188),USE(Delivery_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2344),USE(Delivery_address4_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@n8b),AT(1156,0),USE(Quantity_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s25),AT(1917,0),USE(part_number_temp),TRN,FONT(,8,,)
                           STRING(@s25),AT(3677,0),USE(Description_temp),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(5396,0),USE(Cost_Temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,0),USE(line_cost_temp),TRN,RIGHT,FONT(,8,,)
                         END
                         FOOTER,AT(396,9198,,1479),USE(?unnamed:2),TOGETHER,ABSOLUTE
                           STRING('Labour:'),AT(4844,52),USE(?labour_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6406,52),USE(labour_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s20),AT(3125,281),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(2875,83,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Carriage:'),AT(4844,365),USE(?carriage_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6406,208),USE(parts_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(4844,677),USE(?vat_String),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6406,365),USE(courier_cost_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Total:'),AT(4844,833),USE(?total_string),TRN,FONT(,8,,FONT:bold)
                           STRING('<128>'),AT(6302,833,156,208),USE(?Euro),TRN,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(6302,781,1000,0),USE(?line),COLOR(COLOR:Black)
                           LINE,AT(6302,469,1000,0),USE(?line:2),COLOR(COLOR:Black)
                           STRING(@n14.2b),AT(6406,833),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('Sub Total:'),AT(4844,521),USE(?String92),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6406,521),USE(sub_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s16),AT(2865,521,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@n14.2b),AT(6406,677),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s24),AT(2875,719,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Parts:'),AT(4844,208),USE(?parts_string),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1156)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7552,11198),USE(?Image1)
                         STRING('ESTIMATE'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,365,3073,208),USE(address:Name2),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,521,2760,208),USE(address:Location),TRN,FONT(,8,,FONT:bold)
                         STRING('REG NO:'),AT(104,677),USE(?stringREGNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(625,677,1667,146),USE(address:RegistrationNo),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('VAT NO: '),AT(104,781),USE(?stringVATNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s20),AT(625,781,1771,156),USE(address:VatNumber),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,885,2240,156),USE(address:AddressLine1),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,990,2240,156),USE(address:AddressLine2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1094,2240,156),USE(address:AddressLine3),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1198),USE(address:AddressLine4),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('TEL:'),AT(2385,885),USE(?stringTEL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2625,885,1458,208),USE(address:Telephone),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('FAX:'),AT(2385,990,313,208),USE(?stringFAX),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2625,990,1510,188),USE(address:Fax),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('EMAIL:'),AT(104,1323,417,208),USE(?stringEMAIL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s255),AT(625,1313,3333,208),USE(address:EmailAddress),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(156,1458),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1510,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('AUTHORISATION DETAILS'),AT(156,8479),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?String74),TRN,FONT(,9,,FONT:bold)
                         STRING('Estimate Accepted'),AT(208,8750),USE(?String94),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(1406,8750,156,156),USE(?Box1),COLOR(COLOR:Black)
                         STRING('Estimate Refused'),AT(208,8958),USE(?String95),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(1406,8958,156,156),USE(?Box2),COLOR(COLOR:Black)
                         STRING('Name (Capitals)'),AT(208,9219),USE(?String96),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1198,9323,1354,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Signature'),AT(208,9479),USE(?String97),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(833,9583,1719,0),USE(?Line4),COLOR(COLOR:Black)
                         STRING('Model'),AT(156,3844),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(156,3156),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(1604,3156),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Authority Number'),AT(3083,3156),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(4531,3156),USE(?Chargeable_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(6000,3156),USE(?Repair_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1635,3844),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3125,3844),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4563,3844),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6083,3844),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3635),USE(?String50),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Estimate')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Estimate')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  ! Before Embed Point: %AfterPreviewReq) DESC(After Setting Preview Request (PreviewReq)) ARG()
  END                                                 !---ClarioNET 83
  If f:Type = 'ALERT'
      PreviewReq = True
      If ClarioNETServer:Active()
          ClarioNET:UseReportPreview(PreviewReq)
      End ! If ClarioNETServer:Active()
  Else ! If f:Type = 'ALERT'
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'Estimate'
  If PrintOption(PreviewReq,glo:ExportReport,'Estimate') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  End ! If f:Type = 'ALERT'
  ! After Embed Point: %AfterPreviewReq) DESC(After Setting Preview Request (PreviewReq)) ARG()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:JOBNOTES.UseFile
  Access:JOBACC.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  Access:AUDIT.UseFile
  Access:JOBOUTFL.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAUPA.UseFile
  Access:MANFAULO.UseFile
  Access:MANFAULT.UseFile
  Access:JOBSE.UseFile
  Access:CHARTYPE.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
        ! 26 Aug 2002 John
        !
        tmp:Ref_number = job:Ref_number
        
        if glo:WebJob then
            !Change the tmp ref number if you
            !can Look up from the wob file
            access:Webjob.clearkey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_number
            if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                access:tradeacc.clearkey(tra:Account_Number_Key)
                tra:Account_Number = ClarioNET:Global.Param2
                access:tradeacc.fetch(tra:Account_Number_Key)
                tmp:Ref_Number = Job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
            END
        END
        !--------------------------------------------------------------
        
        count# = 0
        FixedCharge# = 0
        Access:CHARTYPE.ClearKey(cha:Warranty_Key)
        cha:Warranty    = 'NO'
        cha:Charge_Type = job:Charge_Type
        If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Found
            If glo:WebJob
                If cha:Zero_Parts
                    FixedCharge# = 1
                End !If cha:Fixed_Charge
            Else !If glo:WebJob
                If cha:Zero_Parts_ARC
                    FixedCharge# = 1
                End !If cha:Fixed_Charge_ARC
            End !If glo:WebJob
        Else!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        
        If FixedCharge#
            SetTarget(Report)
            ?Cost_Temp{prop:Hide} = 1
            ?Line_Cost_Temp{prop:Hide} = 1
            ?UnitCost{prop:Hide} = 1
            ?LineCost{prop:Hide} = 1
            SetTarget()
        End !FixedCharge#
        
        setcursor(cursor:wait)
        
        save_epr_id = access:estparts.savefile()
        access:estparts.clearkey(epr:part_number_key)
        epr:ref_number  = job:ref_number
        set(epr:part_number_key,epr:part_number_key)
        loop
            if access:estparts.next()
               break
            end !if
            if epr:ref_number  <> job:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            count# += 1
            part_number_temp = epr:part_number
            description_temp = epr:description
            quantity_temp = epr:quantity
            cost_temp = epr:sale_cost
            line_cost_temp = epr:quantity * epr:sale_cost
            Print(rpt:detail)
        
        end !loop
        access:estparts.restorefile(save_epr_id)
        setcursor()
        
        If count# = 0
            SetTarget(Report)
            ?PartsHeadingGroup{Prop:Hide} = 1
            Part_Number_Temp = ''
            !Print Blank Line
            Print(Rpt:Detail)
            SetTarget()
        End!If count# = 0
        
        
        If job:estimate = 'YES' And job:estimate_accepted <> 'YES' and job:estimate_rejected <> 'YES'
            GetStatus(520,1,'JOB') !estimate sent
            Access:JOBS.TryUpdate() ! #13151 Job record is not being saved (for some reason) (DBH: 29/08/2013)
        
            IF (AddToAudit(job:ref_number,'JOB','ESTIMATE SENT','ESTIMATE VALUE: ' & Format(Total_Temp,@n14.2)))
            END ! IF
        
        End!If job:estimate = 'YES' And job:estimate_accepted <> 'YES' and job:estimate_refused <> 'YES'
        
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','Estimate')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Before Embed Point: %EndOfProcedure) DESC(End of Procedure) ARG()
  If f:Type <> 'ALERT'
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  End ! If f:Type <> 'ALERT'
  If f:Type = 'ALERT'
      glo:EstimateReportName = GETINI('ALERTS', 'EstimateFolder',, Clip(Path()) & '\SB2KDEF.INI') & '\Estimate ' & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ').wmf'
      Copy(NameVar,glo:EstimateReportName)
  End ! If f:Type = 'ALERT'
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF f:Type <> 'ALERT'
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  End ! IF f:Type <> 'ALERT'
  ! After Embed Point: %EndOfProcedure) DESC(End of Procedure) ARG()
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      !Set up no preview on client
!      ClarioNET:UseReportPreview(0)
      !Fetch the job record
      access:jobs.clearkey(job:Ref_Number_Key)
      job:ref_number = glo:select1
      access:jobs.fetch(job:ref_number_key)
  END
  !--------------------------------------------------------------
  
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  set(defaults)
  access:defaults.next()
  Delivery_Company_Name_Temp = job:Company_Name_Delivery
  delivery_address1_temp     = job:address_line1_delivery
  delivery_address2_temp     = job:address_line2_delivery
  If job:address_line3_delivery   = ''
      delivery_address3_temp = job:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp  = job:address_line3_delivery
      delivery_address4_temp  = job:postcode_delivery
  End
  Delivery_Telephone_Number_Temp = job:Telephone_Delivery
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job:account_number
  access:subtracc.fetch(sub:account_number_key)
  access:tradeacc.clearkey(tra:account_number_key)
  tra:account_number = sub:main_account_number
  access:tradeacc.fetch(tra:account_number_key)
  if tra:invoice_sub_accounts = 'YES'
      If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = job:address_line1
          invoice_address2_temp     = job:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = job:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job:address_line3
              invoice_address4_temp  = job:postcode
          End
          invoice_company_name_temp   = job:company_name
          invoice_telephone_number_temp   = job:telephone_number
          invoice_fax_number_temp = job:fax_number
      Else!If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = sub:address_line1
          invoice_address2_temp     = sub:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = sub:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = sub:address_line3
              invoice_address4_temp  = sub:postcode
          End
          invoice_company_name_temp   = sub:company_name
          invoice_telephone_number_temp   = sub:telephone_number
          invoice_fax_number_temp = sub:fax_number
  
  
      End!If sub:invoice_customer_address = 'YES'
  
  else!if tra:use_sub_accounts = 'YES'
      If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = job:address_line1
          invoice_address2_temp     = job:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = job:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job:address_line3
              invoice_address4_temp  = job:postcode
          End
          invoice_company_name_temp   = job:company_name
          invoice_telephone_number_temp   = job:telephone_number
          invoice_fax_number_temp = job:fax_number
  
      Else!If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = tra:address_line1
          invoice_address2_temp     = tra:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = tra:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = tra:address_line3
              invoice_address4_temp  = tra:postcode
          End
          invoice_company_name_temp   = tra:company_name
          invoice_telephone_number_temp   = tra:telephone_number
          invoice_fax_number_temp = tra:fax_number
  
      End!If tra:invoice_customer_address = 'YES'
  End!!if tra:use_sub_accounts = 'YES'
  
  
  if job:title = '' and job:initial = ''
      customer_name_temp = clip(job:surname)
  elsif job:title = '' and job:initial <> ''
      customer_name_temp = clip(job:initial) & ' ' & clip(job:surname)
  elsif job:title <> '' and job:initial = ''
      customer_name_temp = clip(job:title) & ' ' & clip(job:surname)
  elsif job:title <> '' and job:initial <> ''
      customer_name_temp = clip(job:title) & ' ' & clip(job:initial) & ' ' & clip(job:surname)
  else
      customer_name_temp = ''
  end
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'ESTIMATE'
  access:stantext.fetch(stt:description_key)
  
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  courier_cost_temp = job:courier_cost_estimate
  
  !If booked at RRC, use RRC costs for estimate
  If jobe:WebJob
      Labour_Temp = jobe:RRCELabourCost
      Parts_Temp  = jobe:RRCEPartsCost
      Sub_Total_Temp = jobe:RRCELabourCost + jobe:RRCEPartsCost + job:Courier_Cost_Estimate
  Else !jobe:WebJob
      labour_temp = job:labour_cost_estimate
      parts_temp  = job:parts_cost_estimate
      sub_total_temp  = job:labour_cost_estimate + job:parts_cost_estimate + job:courier_cost_estimate
  End !jobe:WebJob


    ! Inserting (DBH 03/12/2007) # 8218 - Use the ARC details, if it's an RRC-ARC estimate job
    If SentToHub(job:Ref_Number) And jobe:WebJob
        ! ARC Estimate, use ARC Details
        If ~glo:WebJob
            ! Use ARC Details
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number = wob:HeadAccountNumber
                If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                    Customer_Name_Temp = ''
                    Invoice_Company_Name_Temp = tra:Company_Name
                    Invoice_Address1_Temp = tra:Address_Line1
                    Invoice_Address2_Temp = tra:Address_Line2
                    Invoice_Address3_Temp = tra:Address_Line3
                    Invoice_Address4_Temp = tra:Postcode
                    Invoice_Telephone_Number_Temp = tra:Telephone_Number
                    Invoice_Fax_Number_Temp = tra:Fax_Number

                    Delivery_Company_Name_Temp = tra:Company_Name
                    Delivery_Address1_Temp = tra:Address_Line1
                    Delivery_Address2_Temp = tra:Address_Line2
                    Delivery_Address3_Temp = tra:Address_Line3
                    Delivery_Address4_Temp = tra:Postcode
                    Delivery_Telephone_Number_Temp = tra:Telephone_Number

                    labour_temp = job:labour_cost_estimate
                    parts_temp  = job:parts_cost_estimate
                    sub_total_temp  = job:labour_cost_estimate + job:parts_cost_estimate + job:courier_cost_estimate

                End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        End ! If glo:WebJob
    End ! If SentToHub(job:Ref_Number) And jobe:WebJob
    ! End (DBH 03/12/2007) #8218

  Total_Price('E',vat",total",balance")
  vat_temp = vat"
  total_temp = total"

  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = job:Manufacturer
  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Found
      If ~man:UseInvTextForFaults
          tmp:InvoiceText = jbn:Invoice_Text
      Else !If ~man:UseInvTextForFaults
          tmp:InvoiceText = ''
          Save_joo_ID = Access:JOBOUTFL.SaveFile()
          Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
          joo:JobNumber = job:Ref_Number
          Set(joo:JobNumberKey,joo:JobNumberKey)
          Loop
              If Access:JOBOUTFL.NEXT()
                 Break
              End !If
              If joo:JobNumber <> job:Ref_Number      |
                  Then Break.  ! End If
              ! Inserting (DBH 16/10/2006) # 8059 - Do not show the IMEI Validation text on the paperwork
              If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                  Cycle
              End ! If Instring('IMEI VALIDATION: ',joo:Description,1,1)
              ! End (DBH 16/10/2006) #8059
              If tmp:InvoiceText = ''
                  tmp:InvoiceText = Clip(joo:FaultCode) & ' ' & Clip(joo:Description)
              Else !If tmp:Invoice_Text = ''
                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(joo:FaultCode) & ' ' & Clip(joo:Description)
              End !If tmp:Invoice_Text = ''
          End !Loop
          Access:JOBOUTFL.RestoreFile(Save_joo_ID)
  
          Access:MANFAUPA.ClearKey(map:MainFaultKey)
          map:Manufacturer = job:Manufacturer
          map:MainFault    = 1
          If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
              !Found
              Access:MANFAULT.ClearKey(maf:MainFaultKey)
              maf:Manufacturer = job:Manufacturer
              maf:MainFault    = 1
              If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                  !Found
                  Save_epr_ID = Access:ESTPARTS.SaveFile()
                  Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                  epr:Ref_Number  = job:Ref_Number
                  Set(epr:Part_Number_Key,epr:Part_Number_Key)
                  Loop
                      If Access:ESTPARTS.NEXT()
                         Break
                      End !If
                      If epr:Ref_Number  <> job:Ref_Number      |
                          Then Break.  ! End If
                      Access:MANFAULO.ClearKey(mfo:Field_Key)
                      mfo:Manufacturer = job:Manufacturer
                      mfo:Field_Number = maf:Field_Number
  
                      Case map:Field_Number
                          Of 1
                              mfo:Field   = epr:Fault_Code1
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code1) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          Of 2
                              mfo:Field   = epr:Fault_Code2
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code2) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          Of 3
                              mfo:Field   = epr:Fault_Code3
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code3) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          Of 4
                              mfo:Field   = epr:Fault_Code4
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code4) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          Of 5
                              mfo:Field   = epr:Fault_Code5
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code5) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          Of 6
                              mfo:Field   = epr:Fault_Code6
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code6) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          Of 7
                              mfo:Field   = epr:Fault_Code7
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code7) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          Of 8
                              mfo:Field   = epr:Fault_Code8
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code8) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          Of 9
                              mfo:Field   = epr:Fault_Code9
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code9) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          Of 10
                              mfo:Field   = epr:Fault_Code10
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code10) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          Of 11
                              mfo:Field   = epr:Fault_Code11
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code11) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          Of 12
                              mfo:Field   = epr:Fault_Code12
                              If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Found
                                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code12) & ' ' & Clip(mfp:Description)
                              Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                      End !Case map:Field_Number
                  End !Loop
                  Access:ESTPARTS.RestoreFile(Save_epr_ID)
  
              Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
          Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
  
      End !If ~man:UseInvTextForFaults
  Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Error
  End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  !Barcode Bit
  code_temp            = 3
  option_temp          = 0
  bar_code_string_temp = Clip(job:ref_number)
  job_number_temp      = 'Job No: ' & Clip(job:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job:esn)
  esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  !*********CHANGE LICENSE ADDRESS*********
  set(defaults)
  access:defaults.next()
  Access:WEBJOB.Clearkey(wob:RefNumberKey)
  wob:RefNumber   = job:Ref_Number
  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      If (glo:WebJob = 1)
          tra:Account_Number = wob:HeadAccountNumber
      ELSE ! If (glo:WebJob = 1)
          tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      END ! If (glo:WebJob = 1)
      If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          address:SiteName        = tra:Company_Name
          address:Name            = tra:coTradingName
          address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
          address:Location        = tra:coLocation
          address:RegistrationNo  = tra:coRegistrationNo
          address:AddressLine1    = tra:coAddressLine1
          address:AddressLine2    = tra:coAddressLine2
          address:AddressLine3    = tra:coAddressLine3
          address:AddressLine4    = tra:coAddressLine4
          address:Telephone       = tra:coTelephoneNumber
          address:Fax             = tra:coFaxNumber
          address:EmailAddress    = tra:coEmailAddress
          address:VatNumber       = tra:coVATNumber
      END
  
  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Error
  End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  ! Inserting (DBH 8/05/2006) #7597 - Set report for for attaching to email
  If f:Type = 'ALERT'
      glo:ReportName = 'ESTIMATEALERT'
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  Else ! If f:Type = 'ALERT'
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  End ! If f:Type = 'ALERT'
  ! End (DBH 18/05/2006) #7597
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Estimate'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END













Despatch_Note_Without_Pricing PROCEDURE
RejectRecord         LONG,AUTO
tmp:Ref_number       STRING(20)
tmp:PrintedBy        STRING(255)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_jobs_id         USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(30)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
customer_name_temp   STRING(60)
sub_total_temp       REAL
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
tmp:LoanExchangeUnit STRING(100)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:accessories      STRING(255)
tmp:OriginalIMEI     STRING(30)
tmp:FinalIMEI        STRING(20)
tmp:DefaultEmailAddress STRING(255)
tmp:EngineerReport   STRING(255)
OutFaults_Q          QUEUE,PRE(ofq)
OutFaultsDescription STRING(60)
                     END
DeliveryAddress      GROUP,PRE(delivery)
CustomerName         STRING(30)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
                     END
endUserTelNo         STRING(15)
clientName           STRING(65)
tmp:ReplacementValue REAL
tmp:Terms            STRING(1000)
tmp:IDNumber         STRING(13)
DefaultAddress       GROUP,PRE(address)
SiteName             STRING(40)
Name                 STRING(40)
Name2                STRING(40)
Location             STRING(40)
AddressLine1         STRING(40)
AddressLine2         STRING(40)
AddressLine3         STRING(40)
AddressLine4         STRING(40)
Telephone            STRING(40)
RegistrationNo       STRING(40)
Fax                  STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
locTermsText         STRING(255)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Account_Number)
                       PROJECT(job_ali:Authority_Number)
                       PROJECT(job_ali:Consignment_Number)
                       PROJECT(job_ali:Courier)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:Despatch_Number)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Mobile_Number)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Order_Number)
                       PROJECT(job_ali:Time_Completed)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:date_booked)
                       PROJECT(job_ali:time_booked)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,6875,7521,1208),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,521,7552,8948),USE(?unnamed)
                         STRING('Job Number: '),AT(4917,396),USE(?String25),TRN,FONT(,8,,)
                         STRING(@s20),AT(6156,396),USE(tmp:Ref_number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Booked: '),AT(4917,719),USE(?String58),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6156,719),USE(job_ali:date_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6760,719),USE(job_ali:time_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Date Completed:'),AT(4917,875),USE(?String85),TRN,FONT(,8,,)
                         STRING('Despatch Batch No:'),AT(4917,563),USE(?String86),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6156,875,635,198),USE(job_ali:Date_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6760,875),USE(job_ali:Time_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@s10b),AT(6156,563),USE(job_ali:Despatch_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s22),AT(1604,3240),USE(job_ali:Order_Number,,?job_ali:Order_Number:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4531,3229),USE(charge_type_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3156,3240),USE(job_ali:Authority_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(6000,3240),USE(repair_type_temp),TRN,FONT(,8,,)
                         STRING(@s20),AT(156,3906,1000,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(5052,3906,1094,156),USE(tmp:FinalIMEI),TRN,HIDE,LEFT,FONT(,8,,)
                         STRING(@s20),AT(1250,3906,1323,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(2604,3906,1396,156),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3906,3906,1094,156),USE(tmp:OriginalIMEI),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(6198,3906),USE(job_ali:MSN),TRN,FONT(,8,,)
                         STRING('REPORTED FAULT'),AT(156,4167),USE(?String64),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,4167,5573,417),USE(jbn_ali:Fault_Description),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('ENGINEER REPORT'),AT(156,4635),USE(?String88),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,4635,5573,677),USE(tmp:EngineerReport),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('EXCHANGE UNIT'),AT(156,5625),USE(?Exchange_Unit),TRN,HIDE,FONT(,9,,FONT:bold)
                         STRING(@s100),AT(1615,5625,5781,208),USE(tmp:LoanExchangeUnit),TRN,HIDE,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s13),AT(1615,5833),USE(tmp:IDNumber),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('ID NUMBER'),AT(156,5833),USE(?IDNumber),TRN,FONT(,9,,FONT:bold)
                         STRING('ACCESSORIES'),AT(156,5365),USE(?String105),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,5354,5573,260),USE(tmp:accessories),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('PARTS USED'),AT(156,6094),USE(?Parts_Used),TRN,FONT(,9,,FONT:bold)
                         STRING('Qty'),AT(1521,6094),USE(?Qty),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1917,6094),USE(?Part_Number),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3698,6094),USE(?description),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Cost'),AT(5719,6094),USE(?Unit_Cost),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(6677,6094),USE(?Line_Cost),TRN,HIDE,FONT(,8,,FONT:bold)
                         LINE,AT(1406,6250,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         TEXT,AT(156,7448,5885,260),USE(locTermsText),TRN,FONT(,7,,)
                         STRING('Customer Signature'),AT(156,7813),USE(?Terms2),TRN,FONT(,8,,FONT:bold)
                         STRING('Date: {13}/ {14}/'),AT(5156,7813),USE(?Terms3),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1406,7969,5313,0),USE(?SigLine),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,2188),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4115,2344),USE(?stringDelTel),TRN,FONT(,8,,)
                         STRING(@s30),AT(4323,2344),USE(delivery:TelephoneNumber),FONT(,8,,)
                         STRING(@s65),AT(4115,2813,2865,156),USE(clientName),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(2781,2031),USE(job_ali:Mobile_Number),TRN,LEFT,FONT(,8,,)
                         STRING('Mobile:'),AT(2229,2031),USE(?String35:3),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(2229,1719),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2781,1719),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(2229,1875),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2781,1875),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3240),USE(job_ali:Account_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1563),USE(delivery:CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1719,1917,156),USE(delivery:AddressLine1),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1875),USE(delivery:AddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2031),USE(delivery:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2188),USE(delivery:Postcode),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1563),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,115),USE(?DetailBand)
                           STRING(@n8b),AT(1198,0),USE(Quantity_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s25),AT(1917,0),USE(part_number_temp),TRN,FONT(,7,,)
                           STRING(@s25),AT(3677,0),USE(Description_temp),TRN,FONT(,7,,)
                           STRING(@n14.2b),AT(5531,0),USE(Cost_Temp),TRN,HIDE,RIGHT,FONT(,7,,)
                           STRING(@n14.2b),AT(6521,0),USE(line_cost_temp),TRN,HIDE,RIGHT,FONT(,7,,)
                         END
                         FOOTER,AT(396,8802,,1479),USE(?LoanValueText:2),TOGETHER,ABSOLUTE
                           STRING('Despatch Date:'),AT(156,83),USE(?String66),TRN,FONT(,8,,)
                           STRING(@d6b),AT(1250,83),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING('Courier: '),AT(156,240),USE(?String67),TRN,FONT(,8,,)
                           STRING(@s20),AT(1250,240),USE(job_ali:Courier),TRN,FONT(,8,,FONT:bold)
                           STRING('Labour:'),AT(4844,83),USE(?labour_string),TRN,FONT(,8,,)
                           STRING('Consignment No:'),AT(156,396),USE(?String70),TRN,FONT(,8,,)
                           STRING(@s20),AT(1250,417),USE(job_ali:Consignment_Number),TRN,FONT(,8,,FONT:bold)
                           STRING(@s20),AT(3125,281),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('0.00'),AT(6896,396,219,188),USE(?zero:3),TRN,FONT(,8,,)
                           STRING('0.00'),AT(6896,83),USE(?zero),TRN,FONT(,8,,)
                           STRING('0.00'),AT(6896,240,219,188),USE(?zero:2),TRN,FONT(,8,,)
                           STRING(@s8),AT(2875,83,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Carriage:'),AT(4844,396),USE(?carriage_string),TRN,FONT(,8,,)
                           STRING('V.A.T.'),AT(4844,563),USE(?vat_String),TRN,FONT(,8,,)
                           STRING('Total:'),AT(4844,729),USE(?total_string),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(6281,719,1000,0),USE(?line),COLOR(COLOR:Black)
                           STRING('Vodacom Repairs Loan Phone Terms And Conditions'),AT(156,1094),USE(?LoanTermsText),TRN,HIDE,FONT('Arial',7,,FONT:bold+FONT:underline)
                           STRING('Loan Replace Value:'),AT(156,625),USE(?LoanValueText),TRN,HIDE,FONT(,8,,)
                           STRING(@n14.2),AT(1250,625),USE(tmp:ReplacementValue),TRN,HIDE,FONT(,8,,FONT:bold)
                           STRING('0.00'),AT(6896,563,219,188),USE(?zero:4),TRN,FONT(,8,,)
                           STRING(@s16),AT(2865,521,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING('<128>'),AT(6250,729,156,208),USE(?Euro),TRN,FONT(,8,,FONT:bold)
                           STRING('0.00'),AT(6896,729,219,188),USE(?zero:5),TRN,FONT(,8,,)
                           STRING(@s30),AT(2875,719,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Parts:'),AT(4844,240),USE(?parts_string),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10083,7521,1156),USE(?unnamed:3)
                         TEXT,AT(104,52,7344,990),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:4)
                         IMAGE('RINVDET.GIF'),AT(0,0,7552,10781),USE(?Image1)
                         STRING('DESPATCH NOTE'),AT(3750,0,3698,260),USE(?Title),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,365,3073,208),USE(address:Name2),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,521,2760,208),USE(address:Location),TRN,FONT(,8,,FONT:bold)
                         STRING('REG NO:'),AT(104,677),USE(?stringREGNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(625,677,1667,146),USE(address:RegistrationNo),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('VAT NO: '),AT(104,781),USE(?stringVATNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s20),AT(625,781,1771,156),USE(address:VatNumber),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,885,2240,156),USE(address:AddressLine1),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,990,2240,156),USE(address:AddressLine2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1094,2240,156),USE(address:AddressLine3),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1198),USE(address:AddressLine4),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('TEL:'),AT(2385,885),USE(?stringTEL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2625,885,1458,208),USE(address:Telephone),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('FAX:'),AT(2385,990,313,208),USE(?stringFAX),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2625,990,1510,188),USE(address:Fax),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('EMAIL:'),AT(104,1323,417,208),USE(?stringEMAIL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s255),AT(625,1313,3333,208),USE(address:EmailAddress),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),TRN,FONT(,9,,FONT:bold)
                         STRING('CUSTOMER ADDRESS'),AT(156,1458),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1458,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING('GENERAL DETAILS'),AT(156,2865),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY DETAILS'),AT(156,8177),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4792,8177),USE(?charge_details),TRN,FONT(,9,,FONT:bold)
                         STRING('Model'),AT(156,3750),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(156,3073),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(1604,3073),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Authority Number'),AT(3083,3073),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(4531,3073),USE(?Chargeable_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(6000,3073),USE(?Repair_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1250,3750),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Exch I.M.E.I. No'),AT(5052,3750),USE(?IMEINo:2),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(2604,3750),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(3906,3750),USE(?IMEINo),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6198,3750),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3490),USE(?String50),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Despatch_Note_Without_Pricing')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Despatch_Note_Without_Pricing')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'DespatchNote'
  If ClarioNETServer:Active() = True
      If PrintOption(PreviewReq,glo:ExportReport,'Despatch Note') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  End ! If ClarioNETServer:Active() = True
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:STANTEXT.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:JOBACC.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  Access:STOCK.UseFile
  Access:WARPARTS.UseFile
  Access:LOAN.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBOUTFL.UseFile
  Access:JOBSE.UseFile
  Access:MANFAULO.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAUPA.UseFile
  Access:MANFPALO.UseFile
  Access:JOBSE2.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        If job_ali:exchange_unit_number <> ''
            part_number_temp = ''
            Print(rpt:detail)
        Else!If job_ali:exchange_unit_number <> ''
            count# = 0
        
            If job_ali:warranty_job = 'YES'
                setcursor(cursor:wait)
                save_wpr_id = access:warparts.savefile()
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = job_ali:ref_number
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> job_ali:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    part_number_temp = wpr:part_number
                    description_temp = wpr:description
                    quantity_temp = wpr:quantity
                    cost_temp = wpr:purchase_cost
                    line_cost_temp = wpr:quantity * wpr:purchase_cost
                    Print(rpt:detail)
                end !loop
                access:warparts.restorefile(save_wpr_id)
                setcursor()
        
            Else!If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
                setcursor(cursor:wait)
                save_par_id = access:parts.savefile()
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = job_ali:ref_number
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                       break
                    end !if
                    if par:ref_number  <> job_ali:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    description_temp = par:description
                    quantity_temp = par:quantity
                    cost_temp = par:sale_cost
                    part_number_temp = par:part_number
                    line_cost_temp = par:quantity * par:sale_cost
                    Print(rpt:detail)
                end !loop
                access:parts.restorefile(save_par_id)
                setcursor()
            End!If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
            If count# = 0
                part_number_temp = ''
                Print(rpt:detail)
            End!If count# = 0
        End!If job_ali:exchange_unit_number <> ''
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','Despatch_Note_Without_Pricing')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  If glo:Select2 >= 1  ! Was ( > 1 )
      Printer{PropPrint:Copies} = glo:Select2
  End!If glo:Select2 > 1
  !Printer{PropPrint:Paper} = Paper:Letter
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! Set Report Layout (DBH: 21-06-2006)
  
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  Settarget(Report)
  ! #12084 Show "zeros" (Bryan: 17/05/2011)
  IF (job_ali:Warranty_Job <> 'YES')
      ?Labour_String{prop:Hide} = 1
      ?Parts_String{prop:Hide} = 1
      ?Carriage_String{prop:Hide} = 1
      ?VAT_String{prop:Hide} = 1
      ?Total_String{prop:Hide} = 1
      ?zero{prop:Hide} = 1
      ?zero:2{prop:Hide} = 1
      ?zero:3{prop:Hide} = 1
      ?zero:4{prop:Hide} = 1
      ?zero:5{prop:Hide} = 1
  END
  !Hide(?labour_string)
  !Hide(?labour_temp)
  !Hide(?Parts_temp)
  !Hide(?parts_string)
  !Hide(?Carriage_string)
  !Hide(?vat_string)
  !Hide(?total_string)
  !Hide(?line)
  hide(?unit_Cost)
  hide(?line_cost)
  hide(?cost_temp)
  hide(?line_cost_temp)
  !hide(?labour_string)
  !hide(?parts_string)
  !hide(?courier_cost_temp)
  !hide(?vat_temp)
  !hide(?total_temp)
  hide(?charge_details)
  ?Euro{prop:Hide} = 1
  Settarget()
  
  If job_ali:warranty_job = 'YES'! And job_ali:chargeable_job <> 'YES'
      Settarget(Report)
      ?chargeable_type{prop:text} = 'Warranty Type'
      ?repair_type{prop:text}     = 'Warranty Repair Type'
  !    Hide(?labour_string)
  !    Hide(?parts_string)
  !    Hide(?Carriage_string)
  !    Hide(?vat_string)
  !    Hide(?total_string)
  !    Hide(?line)
      Settarget()
      charge_type_temp = job_ali:warranty_charge_type
      repair_type_temp = job_ali:repair_type_warranty
  Else
      charge_type_temp = job_ali:charge_type
      repair_type_temp = job_ali:repair_type
  End! If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job_ali:account_number
  IF access:subtracc.fetch(sub:account_number_key)
    ! Error!
  END ! IF
  access:tradeacc.clearkey(tra:account_number_key)
  tra:account_number = sub:main_account_number
  IF access:tradeacc.fetch(tra:account_number_key)
    ! Error!
  END ! IF
  ! If tra:price_despatch <> 'YES'
  !    Settarget(report)
  !    hide(?unit_Cost)
  !    hide(?line_cost)
  !    hide(?cost_temp)
  !    hide(?line_cost_temp)
  !    hide(?labour_string)
  !    hide(?parts_string)
  !    hide(?carriage_String)
  !    hide(?vat_string)
  !    hide(?total_string)
  !    hide(?labour_temp)
  !    hide(?parts_temp)
  !    hide(?courier_cost_temp)
  !    hide(?vat_temp)
  !    hide(?total_temp)
  !    hide(?charge_details)
  !    ?Euro{prop:Hide} = 1
  !    Settarget()
  ! End!If tra:price_despatch <> 'YES'
  ! IF sub:PriceDespatchNotes = TRUE
  !  Settarget(report)
  !  unhide(?unit_Cost)
  !  unhide(?line_cost)
  !  unhide(?cost_temp)
  !  unhide(?line_cost_temp)
  !  unhide(?labour_string)
  !  unhide(?parts_string)
  !  unhide(?carriage_String)
  !  unhide(?vat_string)
  !  unhide(?total_string)
  !  unhide(?labour_temp)
  !  unhide(?parts_temp)
  !  unhide(?courier_cost_temp)
  !  unhide(?vat_temp)
  !  unhide(?total_temp)
  !  unhide(?charge_details)
  !  Settarget()
  ! END
  
  !*************************set the display address to the head account if booked on as a web job***********************
  if glo:webjob then
      def:User_Name        = tra:Company_Name
      def:Address_Line1    = tra:Address_Line1
      def:Address_Line2    = tra:Address_Line2
      def:Address_Line3    = tra:Address_Line3
      def:Postcode         = tra:Postcode
      tmp:DefaultTelephone = tra:Telephone_Number
      tmp:DefaultFax       = tra:Fax_Number
      def:EmailAddress     = tra:EmailAddress
  end ! if
  !*********************************************************************************************************************
  
  HideDespAdd# = 0
  if tra:invoice_sub_accounts = 'YES'
      If sub:HideDespAdd = 1
          HideDespAdd# = 1
      End! If sub:HideDespAdd = 1
  
      If SUB:UseCustDespAdd = 'YES'
          invoice_address1_temp = job_ali:address_line1
          invoice_address2_temp = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp = job_ali:address_line3
              invoice_address4_temp = job_ali:postcode
          End ! If
          invoice_company_name_temp     = job_ali:company_name
          invoice_telephone_number_temp = job_ali:telephone_number
          invoice_fax_number_temp       = job_ali:fax_number
      Else! If sub:invoice_customer_address = 'YES'
          invoice_address1_temp = sub:address_line1
          invoice_address2_temp = sub:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = sub:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp = sub:address_line3
              invoice_address4_temp = sub:postcode
          End ! If
          invoice_company_name_temp     = sub:company_name
          invoice_telephone_number_temp = sub:telephone_number
          invoice_fax_number_temp       = sub:fax_number
  
  
      End! If sub:invoice_customer_address = 'YES'
  
      else! if tra:use_sub_accounts = 'YES'
      If tra:HideDespAdd = 1
          HideDespAdd# = 1
      End! If tra:HideDespAdd = 'YES'
  
      If TRA:UseCustDespAdd = 'YES'
          invoice_address1_temp = job_ali:address_line1
          invoice_address2_temp = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp = job_ali:address_line3
              invoice_address4_temp = job_ali:postcode
          End ! If
          invoice_company_name_temp     = job_ali:company_name
          invoice_telephone_number_temp = job_ali:telephone_number
          invoice_fax_number_temp       = job_ali:fax_number
  
      Else! If tra:invoice_customer_address = 'YES'
          invoice_address1_temp = tra:address_line1
          invoice_address2_temp = tra:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = tra:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp = tra:address_line3
              invoice_address4_temp = tra:postcode
          End ! If
          invoice_company_name_temp     = tra:company_name
          invoice_telephone_number_temp = tra:telephone_number
          invoice_fax_number_temp       = tra:fax_number
  
      End! If tra:invoice_customer_address = 'YES'
  end!!if tra:use_sub_accounts = 'YES'
  
  ! Hide Despatch Address
  ! #12079 Seems to be trying to remove the wrong address.
  ! Fixed Now. (Bryan: 14/04/2011)
  If HideDespAdd# = 1
      Settarget(Report)
      ?delivery:CompanyName{prop:Hide}         = 1
      ?delivery:AddressLine1{prop:Hide} = 1
      ?delivery:AddressLine2{prop:Hide} = 1
      ?delivery:AddressLine3{prop:Hide} = 1
      ?delivery:Postcode{prop:hide}     = 1
      ?delivery:TelephoneNumber{prop:hide}    = 1
      ?stringDelTel{prop:hide}          = 1
  !    ?telephone{prop:hide}            = 1
  !    ?fax{prop:hide}                  = 1
  !    ?email{prop:hide}                = 1
  !    ?address:EmailAddress{prop:Hide} = 1
      Settarget()
  End! If HideDespAdd# = 1
  
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job_ali:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  ! Inserting (DBH 10/04/2006) #7305 - Get JOBSE2 Record
  Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
  jobe2:RefNumber = job_ali:Ref_Number
  If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Found
      tmp:IDNumber = jobe2:IDNumber
  Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
  ! End (DBH 10/04/2006) #7305
  
  
        !------------------------------------------------------------------
  IF CLIP(job_ali:title) = ''
      customer_name_temp = job_ali:initial
  ELSIF CLIP(job_ali:initial) = ''
      customer_name_temp = job_ali:title
  ELSE
      customer_name_temp = CLIP(job_ali:title) & ' ' & CLIP(job_ali:initial)
  END ! IF
        !------------------------------------------------------------------
  IF CLIP(customer_name_temp) = ''
      customer_name_temp = job_ali:surname
  ELSIF CLIP(job_ali:surname) = ''
            ! customer_name_temp = customer_name_temp ! tautology
  ELSE
      customer_name_temp = CLIP(customer_name_temp) & ' ' & CLIP(job_ali:surname)
  END ! IF
        !------------------------------------------------------------------
  
  endUserTelNo = ''
  
  if jobe:EndUserTelNo <> ''
      endUserTelNo = clip(jobe:EndUserTelNo)
  end ! if
  
  if customer_name_temp <> ''
      clientName = 'Client: ' & clip(customer_name_temp) & '  Tel: ' & clip(endUserTelNo)
  else
      if endUserTelNo <> ''
          clientName = 'Tel: ' & clip(endUserTelNo)
      end ! if
  end ! if
  
  delivery:TelephoneNumber = job_ali:Telephone_Delivery
  delivery:CompanyName     = job_ali:Company_Name_Delivery
  delivery:AddressLine1    = job_ali:address_line1_delivery
  delivery:AddressLine2    = job_ali:address_line2_delivery
  If job_ali:address_line3_delivery   = ''
      delivery:AddressLine3 = job_ali:postcode_delivery
      delivery:Postcode     = ''
  Else
      delivery:AddressLine3 = job_ali:address_line3_delivery
      delivery:Postcode     = job_ali:postcode_delivery
  End ! If
  
  if job_ali:title = '' and job_ali:initial = ''
      delivery:CustomerName = clip(job_ali:surname)
  elsif job_ali:title = '' and job_ali:initial <> ''
      delivery:CustomerName = clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial = ''
      delivery:CustomerName = clip(job_ali:title) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial <> ''
      delivery:CustomerName = clip(job_ali:title) & ' ' & clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  else
      delivery:CustomerName = ''
  end ! if
  
  
  ! Change the delivery address to the RRC, if it's at the ARC
  ! going back to the RRC, otherwise its the delivery address on the job
  If ~glo:WebJob And jobe:WebJob
      Access:WEBJOB.Clearkey(wob:RefNumberKey)
      wob:RefNumber   = job_ali:Ref_Number
      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          ! Found
          Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
          tra_ali:Account_Number  = wob:HeadAccountNumber
          If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
              ! Found
              delivery:CustomerName    = tra_ali:Account_Number
              delivery:CompanyName     = tra_ali:Address_Line1
              delivery:AddressLine1    = tra_ali:Address_Line2
              delivery:AddressLine2    = tra_ali:Address_Line3
              delivery:AddressLine3    = tra_ali:Postcode
              delivery:Postcode        = ''
              delivery:TelephoneNumber = tra_ali:Telephone_Number
  
          Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
          ! Error
          End ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      ! Error
      End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      Else ! glo:WebJob And jobe:WebJob
  
  End ! glo:WebJob And jobe:WebJob
  
  
  
  
  labour_temp       = ''
  parts_temp        = ''
  courier_cost_temp = ''
  vat_temp          = ''
  total_temp        = ''
  If job_ali:chargeable_job = 'YES'
  
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job_ali:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Error
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      If job_ali:invoice_number <> ''
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job_ali:invoice_number
          access:invoice.fetch(inv:invoice_number_key)
          If glo:WebJob
              If inv:ExportedRRCOracle
                  Labour_Temp       = jobe:InvRRCCLabourCost
                  Parts_Temp        = jobe:InvRRCCPartsCost
                  Courier_Cost_Temp = job_ali:Invoice_Courier_Cost
                  Vat_Temp          = (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100) + |
                  (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100) + |
                  (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
              Else ! If inv:ExportedRRCOracle
                  Labour_Temp       = jobe:RRCCLabourCost
                  Parts_Temp        = jobe:RRCCPartsCost
                  Courier_Cost_Temp = job_ali:Courier_Cost
                  Vat_Temp          = (jobe:RRCCLabourCost * VatRate(job_ali:Account_Number, 'L') / 100) + |
                  (jobe:RRCCPartsCost * VatRate(job_ali:Account_Number, 'P') / 100) + |
                  (job:Courier_Cost * VatRate(job_ali:Account_Number, 'L') / 100)
              End ! If inv:ExportedRRCOracle
          Else ! If glo:WebJob
              labour_temp       = job_ali:invoice_labour_cost
              parts_temp        = job_ali:invoice_parts_cost
              courier_cost_temp = job_ali:invoice_courier_cost
              Vat_Temp          = (job_ali:Invoice_Labour_Cost * inv:Vat_Rate_Labour / 100) + |
              (job_ali:Invoice_Parts_Cost * inv:Vat_Rate_Parts / 100) + |
              (job_ali:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
          End ! If glo:WebJob
  
      Else! If job_ali:invoice_number <> ''
          If glo:WebJob
              Labour_Temp       = jobe:RRCCLabourCost
              Parts_Temp        = jobe:RRCCPartsCost
              Courier_Cost_Temp = job_ali:Courier_Cost
              Vat_Temp          = (jobe:RRCCLabourCost * VatRate(job_ali:Account_Number, 'L') / 100) + |
              (jobe:RRCCPartsCost * VatRate(job_ali:Account_Number, 'P') / 100) + |
              (job:Courier_Cost * VatRate(job_ali:Account_Number, 'L') / 100)
  
          Else ! If glo:WebJob
              Labour_Temp       = job_ali:Labour_Cost
              Parts_temp        = job_ali:Parts_Cost
              Courier_Cost_temp = job_ali:Courier_Cost
              Vat_Temp          = (job_ali:Labour_Cost * VatRate(job_ali:Account_Number, 'L') / 100) + |
              (job_ali:Parts_Cost * VatRate(job_ali:Account_Number, 'P') / 100) + |
              (job_ali:Courier_Cost * VatRate(job_ali:Account_Number, 'L') / 100)
          End ! If glo:WebJob
  
      End! If job_ali:invoice_number <> ''
      total_temp = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
  End! If job_ali:chargeable_job <> 'YES'
  
  If job_ali:Warranty_Job = 'YES'
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job_ali:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Error
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      If job_ali:invoice_number_warranty <> ''
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job_ali:invoice_number_Warranty
          access:invoice.fetch(inv:invoice_number_key)
          If glo:WebJob
              Labour_Temp       = jobe:InvRRCWLabourCost
              Parts_Temp        = jobe:InvRRCWPartsCost
              Courier_Cost_Temp = job_ali:WInvoice_Courier_Cost
              Vat_Temp          = (jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour / 100) + |
              (jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts / 100) + |
              (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
          Else ! If glo:WebJob
              labour_temp       = jobe:InvoiceClaimValue
              parts_temp        = job_ali:Winvoice_parts_cost
              courier_cost_temp = job_ali:Winvoice_courier_cost
              Vat_Temp          = (jobe:InvoiceClaimValue * inv:Vat_Rate_Labour / 100) + |
              (job_ali:WInvoice_Parts_Cost * inv:Vat_Rate_Parts / 100) +                 |
              (job_ali:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
          End ! If glo:WebJob
  
      Else! If job_ali:invoice_number <> ''
          If glo:WebJob
              Labour_Temp       = jobe:RRCWLabourCost
              Parts_Temp        = jobe:RRCWPartsCost
              Courier_Cost_Temp = job_ali:Courier_Cost_Warranty
              Vat_Temp          = (jobe:RRCWLabourCost * VatRate(job_ali:Account_Number, 'L') / 100) + |
              (jobe:RRCWPartsCost * VatRate(job_ali:Account_Number, 'P') / 100) + |
              (job:Courier_Cost_Warranty * VatRate(job_ali:Account_Number, 'L') / 100)
  
          Else ! If glo:WebJob
              Labour_Temp       = jobe:ClaimValue
              Parts_temp        = job_ali:Parts_Cost_Warranty
              Courier_Cost_temp = job_ali:Courier_Cost_Warranty
              Vat_Temp          = (jobe:ClaimValue * VatRate(job_ali:Account_Number, 'L') / 100) + |
              (job_ali:Parts_Cost_Warranty * VatRate(job_ali:Account_Number, 'P') / 100) +         |
              (job_ali:Courier_Cost_Warranty * VatRate(job_ali:Account_Number, 'L') / 100)
          End ! If glo:WebJob
  
      End! If job_ali:invoice_number <> ''
      total_temp = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
  End ! job_ali:Warranty_Job = 'YES'
  
  access:users.clearkey(use:user_code_key)
  use:user_code = job_ali:despatch_user
  If access:users.fetch(use:user_code_key) = Level:Benign
      despatched_user_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  End! If access:users.fetch(use:user_code_key) = Level:Benign
  
  setcursor(cursor:wait)
  tmp:accessories = ''
  save_jac_id     = access:jobacc.savefile()
  access:jobacc.clearkey(jac:ref_number_key)
  jac:ref_number = job_ali:ref_number
  set(jac:ref_number_key, jac:ref_number_key)
  loop
      if access:jobacc.next()
          break
      end ! if
      if jac:ref_number <> job_ali:ref_number      |
          then break   ! end if
      end ! if
      yldcnt# += 1
      if yldcnt# > 25
          yield() 
          yldcnt# = 0
      end ! if
      If tmp:accessories = ''
          tmp:accessories = jac:accessory
      Else! If tmp:accessories = ''
          tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
      End! If tmp:accessories = ''
  end ! loop
  access:jobacc.restorefile(save_jac_id)
  setcursor()
  
  
  Access:JOBOUTFL.ClearKey(joo:LevelKey) ! Added by Gary (28th August 2002)
  joo:JobNumber = job_ali:ref_number
  set(joo:LevelKey, joo:LevelKey)
  loop until Access:JOBOUTFL.Next()
      if joo:JobNumber <> job_ali:ref_number then break.
      OutFaults_Q.ofq:OutFaultsDescription = joo:Description
      get(OutFaults_Q, OutFaults_Q.ofq:OutFaultsDescription) ! Queue is used to prevent duplicates
      if error()
          tmp:EngineerReport                   = clip(tmp:EngineerReport) & clip(joo:Description) & '<13,10>'
          OutFaults_Q.ofq:OutFaultsDescription = joo:Description
          add(OutFaults_Q, OutFaults_Q.ofq:OutFaultsDescription)
      end ! if
  end ! loop
  
  exchange_note# = 0
  
  ! Exchange Loan
  If job_ali:exchange_unit_number <> ''
      Exchange_Note# = 1
  End! If job_ali:exchange_unit_number <> ''
  ! Check Chargeable Parts for an "Exchange" part
  
  save_par_id = access:parts.savefile()
  access:parts.clearkey(par:part_number_key)
  par:ref_number  = job_ali:ref_number
  set(par:part_number_key, par:part_number_key)
  loop
      if access:parts.next()
          break
      end ! if
      if par:ref_number  <> job_ali:ref_number      |
          then break   ! end if
      end ! if
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number = par:part_ref_number
      if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
          If sto:ExchangeUnit = 'YES'
              exchange_note# = 1
              Break
          End! If sto:ExchangeUnit = 'YES'
      end! if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
  end ! loop
  access:parts.restorefile(save_par_id)
  
  If exchange_note# = 0
  ! Check Warranty Parts for an "Exchange" part
      save_wpr_id = access:warparts.savefile()
      access:warparts.clearkey(wpr:part_number_key)
      wpr:ref_number  = job_ali:ref_number
      set(wpr:part_number_key, wpr:part_number_key)
      loop
          if access:warparts.next()
              break
          end ! if
          if wpr:ref_number  <> job_ali:ref_number      |
              then break   ! end if
          end ! if
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = wpr:part_ref_number
          if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
              If sto:ExchangeUnit = 'YES'
                  exchange_note# = 1
                  Break
              End! If sto:ExchangeUnit = 'YES'
          end! if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
      end ! loop
      access:warparts.restorefile(save_wpr_id)
  End! If exchange_note# = 0
  
  If Exchange_Note# = 1
      ! Do not put Exchange Unit Repair on the
      ! Despatch note heading if it's a failed assessment -  (DBH: 07-11-2003)
      If jobe:WebJob And ~glo:WebJob And jobe:Engineer48HourOption = 1
          save_JOBS_id = Access:JOBS.SaveFile()
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number  = job_ali:Ref_Number
          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              ! Found
              If PassExchangeAssessment() = False
                  Exchange_Note# = 0
              End ! If PassExchangeAssessment() = False
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          ! Error
          End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          Access:JOBS.RestoreFile(save_JOBS_id)
      End ! If jobe:HubRepair And ~glo:WebJob And jobe:Exchange48HourOption = 1
  End ! Exchange_Note# = 1
  
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  
  
  If job_ali:loan_unit_number <> ''
      Settarget(report)
      ?exchange_unit{prop:text} = 'LOAN UNIT'
      ! Changing (DBH 21/06/2006) #7878 - Check if being despatched from Webmaster
      ! If job_ali:despatch_Type = 'LOA'
      ! to (DBH 21/06/2006) #7878
      !If (glo:WebJob = True And jobe:DespatchType = 'LOA') Or (glo:WebJob = False And job_ali:Despatch_Type = 'LOA')
      ! End (DBH 21/06/2006) #7878
          tmp:IDNumber = jobe2:LoanIDNumber
          ?title{prop:text} = 'LOAN DESPATCH NOTE'
      !End! If job_ali:despatch_Type = 'EXC'
      Unhide(?exchange_unit)
      Unhide(?tmp:LoanExchangeUnit)
      access:loan.clearkey(loa:ref_number_key)
      loa:ref_number = job_ali:loan_unit_number
      if access:loan.tryfetch(loa:ref_number_key) = Level:Benign
          tmp:LoanExchangeUnit = '(' & Clip(loa:Ref_number) & ') ' & CLip(loa:manufacturer) & ' ' & CLip(loa:model_number) & |
          ' - ' & CLip(loa:esn)
      end ! if
      Settarget()
  
      ! #12084 New standard texts (Bryan: 17/05/2011)
      Access:STANTEXT.Clearkey(stt:Description_Key)
      stt:Description = 'LOAN DESPATCH NOTE'
      IF (Access:STANTEXT.TryFetch(stt:Description_Key))
      END
  
  End! If job_ali:exchange_unit_number <> ''
  
  tmp:OriginalIMEI = job_ali:ESN
  
  ! Was the unit exchanged at Third Party?
  If job_ali:Third_Party_Site <> ''
      IMEIError# = 0
      If job_ali:Third_Party_Site <> ''
          Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
          jot:RefNumber = job_ali:Ref_Number
          Set(jot:RefNumberKey, jot:RefNumberKey)
          If Access:JOBTHIRD.NEXT()
              IMEIError# = 1
          Else ! If Access:JOBTHIRD.NEXT()
              If jot:RefNumber <> job_ali:Ref_Number
                  IMEIError# = 1
              Else ! If jot:RefNumber <> job_ali:Ref_Number
                  If jot:OriginalIMEI <> job_ali:esn
                      IMEIError# = 0
                  Else
                      IMEIError# = 1
                  End ! If jot:OriginalIMEI <> job_ali:esn
  
              End ! If jot:RefNumber <> job_ali:Ref_Number
          End ! If Access:JOBTHIRD.NEXT()
      Else ! job_ali:Third_Party_Site <> ''
          IMEIError# = 1
      End ! job_ali:Third_Party_Site <> ''
  
      If IMEIError# = 1
          tmp:OriginalIMEI     = job_ali:esn
      Else ! IMEIError# = 1
          Settarget(Report)
          tmp:OriginalIMEI          = jot:OriginalIMEI
          tmp:FinalIMEI             = job_ali:esn
          ?IMEINo{prop:text}        = 'Orig I.M.E.I. No'
          ?IMEINo:2{prop:hide}      = 0
          ?tmp:FinalIMEI{prop:hide} = 0
          Settarget()
      End ! IMEIError# = 1
  End ! job_ali:Third_Party_Site <> ''
  
  If exchange_note# = 1
      Settarget(report)
      ?title{prop:text} = 'EXCHANGE DESPATCH NOTE'
      Unhide(?exchange_unit)
      Unhide(?tmp:LoanExchangeUnit)
      Hide(?parts_used)
      Hide(?qty)
      Hide(?part_number)
      Hide(?Description)
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number = job_ali:exchange_unit_number
      if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
          tmp:LoanExchangeUnit = '(' & Clip(xch:Ref_number) & ') ' & CLip(xch:manufacturer) & ' ' & CLip(xch:model_number) & |
          ' - ' & CLip(xch:esn)
          tmp:OriginalIMEI          = job_ali:ESN
          tmp:FinalIMEI             = xch:ESN
          ?tmp:FinalIMEI{prop:Hide} = 0
          ?IMEINo:2{prop:Hide}      = 0
      end ! if
      Settarget()
  End! If exchange_note# = 1
  
  ! Get Job Notes!
  Access:JobNotes_Alias.ClearKey(jbn_ali:RefNumberKey)
  jbn_ali:RefNumber = job_ali:Ref_Number
  IF Access:JobNotes_Alias.Fetch(jbn_ali:RefNumberKey)
    ! Error!
  END ! IF
  
  ! If loan attached, show replacement value
  tmp:Terms = stt:Text
  If job_ali:Loan_Unit_Number <> '' And job_ali:Exchange_Unit_Number = 0
      SetTarget(Report)
      ?tmp:ReplacementValue{prop:Hide} = 0
      ?LoanValueText{prop:Hide}        = 0
      tmp:ReplacementValue             = jobe:LoanReplacementValue
      ?LoanTermsText{prop:Hide}        = 0
      !?Terms1{prop:Hide}               = 0
      ?Terms2{prop:Hide}               = 0
      ?Terms3{prop:Hide}               = 0
      ?SigLine{prop:Hide}              = 0
      SetTarget()
  !    tmp:Terms = '1) The loan phone and its accessories remain the property of Vodacom Service Provider Company and is given to the customer to use while his / her phone is being repaired. It is the customers responsibility to return the loan phone and its accessories in proper working condition.<13,10>'
  !    tmp:Terms = Clip(tmp:Terms) & '2) In the event of damage to the loan phone and/or its accessories, the customer will be liable for any costs incurred to repair or replace the loan phone and/or its accessories. <13,10>'
  !    tmp:Terms = Clip(tmp:Terms) & '3) In the event of loss or theft of the loan phone and/or its accessories the customer will be liable for the replacement cost of the loan phone and/or its accessories or he/she may replace the lost/stolen phone and/or its accessories with new ones of the same or similar make and model, or same replacement value.<13,10>'
  !    tmp:Terms = Clip(tmp:Terms) & '4) Any phone replaced by the customer must be able to operate on the Vodacom network. <13,10>'
  !    tmp:Terms = Clip(tmp:Terms) & '5) The customers repaired phone will not be returned until the loan phone and its accessories has been returned, repaired or replaced. '
  
  End ! job_ali:Loan_Unit_Number <> ''
  !Use Alternative Contact Nos
  Case UseAlternativeContactNos(job_ali:Account_Number,0)
      Of 1
          tmp:DefaultEmailAddress = tra:AltEmailAddress
          tmp:DefaultTelephone    = tra:AltTelephoneNumber
          tmp:DefaultFax          = tra:AltFaxNumber
      Of 2
          tmp:DefaultEmailAddress = sub:AltEmailAddress
          tmp:DefaultTelephone    = sub:AltTelephoneNumber
          tmp:DefaultFax          = sub:AltFaxNumber
      Else
          tmp:DefaultEmailAddress = def:EmailAddress
  End !UseAlternativeContactNos(job_ali:Account_Number)
  !*********CHANGE LICENSE ADDRESS*********
  set(defaults)
  access:defaults.next()
  Access:WEBJOB.Clearkey(wob:RefNumberKey)
  wob:RefNumber   = job_ali:Ref_Number
  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Found
  
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      If (glo:WebJob = 1)
          tra:Account_Number = wob:HeadAccountNumber
      ELSE ! If (glo:WebJob = 1)
          tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      END ! If (glo:WebJob = 1)
      If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          address:SiteName        = tra:Company_Name
          address:Name            = tra:coTradingName
          address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
          address:Location        = tra:coLocation
          address:RegistrationNo  = tra:coRegistrationNo
          address:AddressLine1    = tra:coAddressLine1
          address:AddressLine2    = tra:coAddressLine2
          address:AddressLine3    = tra:coAddressLine3
          address:AddressLine4    = tra:coAddressLine4
          address:Telephone       = tra:coTelephoneNumber
          address:Fax             = tra:coFaxNumber
          address:EmailAddress    = tra:coEmailAddress
          address:VatNumber       = tra:coVATNumber
      END
  
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = wob:HeadAccountNumber
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          ! Found
  
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          ! Error
      End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      tmp:Ref_Number = job_ali:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
  
  
  
  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Error
      tmp:Ref_Number = job_ali:Ref_Number
  End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !Barcode bit
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(tmp:Ref_number)
  job_number_temp = 'Job No: ' & Clip(tmp:Ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'I.M.E.I. No.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  locTermsText = GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI')   ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Despatch_Note_Without_Pricing'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END













Job_Receipt PROCEDURE
RejectRecord         LONG,AUTO
tmp:accessories      STRING(255)
tmp:PrintedBy        STRING(255)
tmp:Ref_number       STRING(20)
save_jea_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
save_loa_id          USHORT
save_lac_id          USHORT
save_stt_id          USHORT
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
received_by_temp     STRING(60)
fault_code_field_temp STRING(20),DIM(12)
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Name_Temp   STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Delivery_Company_Name_Temp STRING(30)
InvoiceAddress_Group GROUP,PRE()
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
invoice_Email_Address STRING(255)
                     END
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(70)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
engineer_temp        STRING(30)
part_type_temp       STRING(4)
amount_received_temp REAL
customer_name_temp   STRING(40)
exchange_unit_number_temp STRING(20)
exchange_model_number STRING(30)
exchange_manufacturer_temp STRING(30)
exchange_unit_type_temp STRING(30)
exchange_esn_temp    STRING(16)
exchange_msn_temp    STRING(15)
exchange_unit_title_temp STRING(15)
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:LoanModel        STRING(30)
tmp:LoanIMEI         STRING(30)
tmp:LoanAccessories  STRING(255)
tmp:StandardText     STRING(500)
tmp:LoanDepositPaid  REAL
DefaultAddress       GROUP,PRE(address)
SiteName             STRING(40)
Name                 STRING(40)
Name2                STRING(40)
Location             STRING(40)
AddressLine1         STRING(40)
AddressLine2         STRING(40)
AddressLine3         STRING(40)
AddressLine4         STRING(40)
Telephone            STRING(40)
RegistrationNo       STRING(40)
Fax                  STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
Delivery_Telephone_Temp STRING(30)
endUserTelNo         STRING(15)
clientName           STRING(65)
TermsAndConditions   STRING(1000)
tmp:BookingOption    STRING(30)
locTermsText         STRING(255)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Account_Number)
                       PROJECT(job_ali:Charge_Type)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Incoming_Consignment_Number)
                       PROJECT(job_ali:Incoming_Courier)
                       PROJECT(job_ali:Incoming_Date)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Order_Number)
                       PROJECT(job_ali:Repair_Type)
                       PROJECT(job_ali:Repair_Type_Warranty)
                       PROJECT(job_ali:Transit_Type)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:Warranty_Charge_Type)
                       PROJECT(job_ali:date_booked)
                       PROJECT(job_ali:time_booked)
                       PROJECT(job_ali:Ref_Number)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                       END
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Job Receipt'),AT(250,4323,7750,5813),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',8,,,CHARSET:ANSI),THOUS
                       HEADER,AT(323,438,7521,4115),USE(?unnamed),FONT('Tahoma',,,)
                         STRING('Job Number: '),AT(5052,365),USE(?String25),TRN,FONT(,8,,)
                         STRING(@s20),AT(5885,365),USE(tmp:Ref_number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Booked: '),AT(5052,677),USE(?String58),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5990,677),USE(job_ali:date_booked),TRN,FONT(,8,,FONT:bold)
                         STRING(@t1),AT(6875,677),USE(job_ali:time_booked),TRN,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(1604,3240),USE(job_ali:Charge_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4635,3240),USE(job_ali:Warranty_Charge_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3125,3240),USE(job_ali:Repair_Type),TRN,FONT(,8,,)
                         STRING(@s20),AT(6146,3240),USE(job_ali:Repair_Type_Warranty),TRN,FONT(,8,,)
                         STRING('Model'),AT(156,3646),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1604,3646),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3125,3646),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4635,3646),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6146,3646),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2188),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4083,2344),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4323,2344),USE(Delivery_Telephone_Temp),FONT(,8,,)
                         STRING(@s65),AT(4063,2500,2865,156),USE(clientName),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Order Number'),AT(156,3000),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(1604,3000),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(3125,3000),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Warranty Type'),AT(4635,3000),USE(?String40:5),TRN,FONT(,8,,FONT:bold)
                         STRING('Warranty Repair Type'),AT(6146,3000),USE(?String40:6),TRN,FONT(,8,,FONT:bold)
                         STRING('Tel:'),AT(2198,2031),USE(?String34:3),TRN,FONT(,8,,)
                         STRING('Acc No:'),AT(2188,1563),USE(?String78),TRN,FONT(,8,,)
                         STRING('Email:'),AT(156,2344),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2667,2031),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(2198,2188),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2667,2188),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(2656,1563),USE(job_ali:Account_Number,,?job_ali:Account_Number:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3240),USE(job_ali:Order_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1563),USE(Delivery_Company_Name_Temp),TRN,FONT(,8,,)
                         STRING(@s50),AT(469,2344,3125,208),USE(invoice_Email_Address),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1719,1917,156),USE(Delivery_Address1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1875),USE(Delivery_address2_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2031),USE(Delivery_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2188),USE(Delivery_address4_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1563),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:4)
DETAIL                   DETAIL,AT(,,,5646),USE(?DetailBand),FONT('Tahoma',8,,),TOGETHER
                           STRING(@s30),AT(229,0,1000,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(1677,0,1323,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(3198,0,1396,156),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(4708,0,1396,156),USE(job_ali:ESN),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(6219,0),USE(job_ali:MSN),TRN,FONT(,8,,)
                           STRING(@s15),AT(6219,365),USE(exchange_msn_temp),TRN,FONT(,8,,)
                           STRING(@s16),AT(4708,365,1396,156),USE(exchange_esn_temp),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(3198,365,1396,156),USE(exchange_unit_type_temp),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(1677,365,1323,156),USE(exchange_manufacturer_temp),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(229,365,1000,156),USE(exchange_model_number),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(229,208),USE(exchange_unit_title_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING(@s20),AT(1354,208),USE(exchange_unit_number_temp),TRN,LEFT
                           STRING('ACCESSORIES'),AT(156,1042),USE(?String63),TRN,FONT(,8,,FONT:bold)
                           TEXT,AT(1927,1042,5313,365),USE(tmp:accessories),TRN,FONT(,7,,)
                           STRING('EXTERNAL DAMAGE CHECK LIST'),AT(156,1458),USE(?ExternalDamageCheckList),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           STRING('Notes:'),AT(5990,1458),USE(?Notes),TRN,FONT(,7,,)
                           TEXT,AT(6302,1458,1250,677),USE(jobe2:XNotes),TRN,FONT(,6,,)
                           CHECK('None'),AT(5365,1458,625,156),USE(jobe2:XNone),TRN
                           CHECK('Keypad'),AT(156,1615,729,156),USE(jobe2:XKeypad),TRN
                           CHECK('Charger'),AT(2146,1615,781,156),USE(jobe2:XCharger),TRN
                           CHECK('Antenna'),AT(2146,1448,833,156),USE(jobe2:XAntenna),TRN
                           CHECK('Lens'),AT(2938,1448,573,156),USE(jobe2:XLens),TRN
                           CHECK('F/Cover'),AT(3563,1448,781,156),USE(jobe2:XFCover),TRN
                           CHECK('B/Cover'),AT(4448,1448,729,156),USE(jobe2:XBCover),TRN
                           CHECK('Battery'),AT(990,1615,729,156),USE(jobe2:XBattery),TRN
                           CHECK('LCD'),AT(2938,1615,625,156),USE(jobe2:XLCD),TRN
                           CHECK('System Connector'),AT(4448,1615,1250,156),USE(jobe2:XSystemConnector),TRN
                           TEXT,AT(156,2344,7344,1875),USE(stt:Text),TRN
                           CHECK('Sim Reader'),AT(3563,1604,885,156),USE(jobe2:XSimReader),TRN
                           STRING('Initial Transit Type: '),AT(156,4740),USE(?String66),TRN,FONT(,8,,)
                           STRING(@s20),AT(1250,4740),USE(job_ali:Transit_Type),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING('Courier: '),AT(156,4896),USE(?String67),TRN,FONT(,8,,)
                           STRING(@s30),AT(1240,4896,1302,188),USE(job_ali:Incoming_Courier),TRN,FONT(,8,,FONT:bold)
                           STRING('Consignment No:'),AT(156,5052),USE(?String70),TRN,FONT(,8,,)
                           STRING(@s30),AT(1240,5052,1302,188),USE(job_ali:Incoming_Consignment_Number),TRN,FONT(,8,,FONT:bold)
                           STRING('Customer ID No:'),AT(4948,5156),USE(?CustomerIDNo),TRN,FONT(,8,,)
                           STRING('Date Received: '),AT(156,5208),USE(?String68),TRN,FONT(,8,,)
                           STRING(@d6b),AT(1240,5208),USE(job_ali:Incoming_Date),TRN,FONT(,8,,FONT:bold)
                           STRING('Received By: '),AT(156,5365),USE(?String69),TRN,FONT(,8,,)
                           STRING(@s22),AT(1250,5365),USE(received_by_temp),TRN,FONT(,8,,FONT:bold)
                           STRING('REPORTED FAULT'),AT(156,573),USE(?String64),TRN,FONT(,8,,FONT:bold)
                           TEXT,AT(1927,573,5313,417),USE(jbn_ali:Fault_Description),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s16),AT(3073,5156,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@s24),AT(3073,5365,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Booking Option:'),AT(4948,5365),USE(?String122),TRN,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(6094,5365),USE(tmp:BookingOption),TRN,FONT(,8,,)
                           STRING(@s13),AT(6094,5156),USE(jobe2:IDNumber),TRN,FONT(,8,,)
                           STRING(@s20),AT(3073,4948,1760,198),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Amount:'),AT(4948,4948),USE(?amount_text),TRN,FONT(,8,,)
                           STRING('<128>'),AT(5938,4948,52,208),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                           STRING(@n-14.2b),AT(6094,4948),USE(amount_received_temp),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(3073,4740,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Loan Deposit Paid'),AT(4948,4740),USE(?LoanDepositPaidTitle),TRN,HIDE,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n-14.2b),AT(6094,4740),USE(tmp:LoanDepositPaid),TRN,HIDE,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1156),USE(?unnamed:2)
                         STRING('Estimate Required If Repair Costs Exceed:'),AT(156,63),USE(?estimate_text),TRN,HIDE,FONT(,8,,)
                         STRING(@s70),AT(2344,52,4115,260),USE(estimate_value_temp),TRN,HIDE,FONT(,8,,)
                         TEXT,AT(83,83,10,10),USE(tmp:StandardText),HIDE,FONT(,8,,)
                         STRING('LOAN UNIT ISSUED'),AT(156,208),USE(?LoanUnitIssued),TRN,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(1927,208),USE(tmp:LoanModel),TRN,HIDE,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('IMEI'),AT(3958,208),USE(?IMEITitle),TRN,HIDE,FONT('Arial',9,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4323,208),USE(tmp:LoanIMEI),TRN,HIDE,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('LOAN ACCESSORIES'),AT(156,365),USE(?LoanAccessoriesTitle),TRN,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s255),AT(1927,365,5417,208),USE(tmp:LoanAccessories),TRN,HIDE,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         TEXT,AT(156,573,5885,260),USE(locTermsText),TRN,FONT(,7,010101H,,CHARSET:ANSI)
                         STRING('Customer Signature'),AT(156,844),USE(?String82),TRN,FONT(,8,,FONT:bold)
                         STRING('Date: {13}/ {14}/'),AT(5313,833),USE(?String83),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1406,1042,3677,0),USE(?Line1),COLOR(COLOR:Black)
                         LINE,AT(5573,1042,1521,0),USE(?Line2),COLOR(COLOR:Black)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('JOB RECEIPT'),AT(5917,0,1521,240),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),TRN,FONT(,8,,FONT:bold)
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s20),AT(625,677,1771,156),USE(address:VatNumber),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),TRN,FONT(,8,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(156,1510),USE(?String24),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('GOODS RECEIVED DETAILS'),AT(156,8563),USE(?String73),TRN,FONT(,8,,FONT:bold)
                         STRING('PAYMENT RECEIVED (INC. V.A.T.)'),AT(4844,8563),USE(?String73:2),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3677),USE(?String50),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(5052,417,2646,1042),USE(?BoxGrey:TopDetails),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,1719,3604,1302),USE(?BoxGrey:Address1),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4115,1719,3604,1302),USE(?BoxGrey:Address2),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,3177,7552,521),USE(?BoxGrey:Title),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,3854,7552,4740),USE(?BoxGrey:Detail),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,8750,2813,1042),USE(?BoxGrey:Total1),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4896,8750,2813,1042),USE(?BoxGrey:Total2),ROUND,FILL(COLOR:Silver)
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,8698,2813,1042),USE(?Box:Total1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(4844,8698,2813,1042),USE(?Box:Total2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Job_Receipt')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Job_Receipt')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'JobReceipt'
  If PrintOption(PreviewReq,glo:ExportReport,'Job Receipt') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBEXACC.Open
  Relate:JOBPAYMT.Open
  Relate:LOAN.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:JOBNOTES_ALIAS.UseFile
  Access:JOBACC.UseFile
  Access:JOBS.UseFile
  Access:JOBNOTES.UseFile
  Access:LOANACC.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSE.UseFile
  Access:JOBSE2.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        Print(rpt:detail)
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','Job_Receipt')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBACC.Close
    Relate:JOBEXACC.Close
    Relate:JOBPAYMT.Close
    Relate:LOAN.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !********************Code added by Paul*****************
  
  access:webjob.clearkey(wob:RefNumberKey)
  wob:refnumber = job_ali:Ref_Number
  if access:webjob.fetch(wob:RefNumberKey)
      tmp:ref_number = job_ali:Ref_Number
  else
      access:tradeacc.clearkey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      access:tradeacc.fetch(tra:Account_Number_Key)
      tmp:ref_number = job_ali:ref_number & '-' &tra:BranchIdentification &wob:jobnumber
              invoice_company_name_temp     = tra:Company_Name
              invoice_address1_temp         = tra:Address_Line1
              invoice_address2_temp         = tra:Address_Line2
              invoice_address3_temp         = tra:Address_Line3
              invoice_address4_temp         = tra:Postcode
              invoice_telephone_number_temp = tra:Telephone_Number
              invoice_fax_number_temp       = tra:Fax_Number
              def:EmailAddress              = tra:EmailAddress
  
  end
  
  
  !*********CHANGE LICENSE ADDRESS*********
  set(defaults)
  access:defaults.next()
  Access:WEBJOB.Clearkey(wob:RefNumberKey)
  wob:RefNumber   = job_ali:Ref_Number
  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      If (glo:WebJob = 1)
          tra:Account_Number = wob:HeadAccountNumber
      ELSE ! If (glo:WebJob = 1)
          tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      END ! If (glo:WebJob = 1)
      If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          address:SiteName        = tra:Company_Name
          address:Name            = tra:coTradingName
          address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
          address:Location        = tra:coLocation
          address:RegistrationNo  = tra:coRegistrationNo
          address:AddressLine1    = tra:coAddressLine1
          address:AddressLine2    = tra:coAddressLine2
          address:AddressLine3    = tra:coAddressLine3
          address:AddressLine4    = tra:coAddressLine4
          address:Telephone       = tra:coTelephoneNumber
          address:Fax             = tra:coFaxNumber
          address:EmailAddress    = tra:coEmailAddress
          address:VatNumber       = tra:coVATNumber
      END
  
  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Error
  End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  
  tmp:StandardText = stt:Text             ! The standard text is set outside of this procedure
                                          ! Temporary variable used because I may need to append the loan
                                          ! disclaimer text.
  
  !*********************************************************************************************************************
  access:users.clearkey(use:password_key)
  use:password = job_ali:despatch_user
  access:users.fetch(use:password_key)
  received_by_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  !*********************************************************************************************************************
  
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job_ali:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  ! Inserting (DBH 10/04/2006) #7305 - Get JOBSE2 Record
  Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
  jobe2:RefNumber = job_ali:Ref_Number
  If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Found
  
      Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
  ! End (DBH 10/04/2006) #7305
  
  
      ! Set Booking Option -  (DBH: 28-11-2003)
  Case jobe:Booking48HourOption
  Of 1
      tmp:BookingOption = '48 Hour Exchange'
  Of 2
      tmp:BookingOption = 'ARC Repair'
  Of 3
      tmp:BookingOption = '7 Day TAT'
  Else
      tmp:BookingOption = 'N/A'
  End ! Case jobe:Booking48HourOption
  
  !*********************************************************************************************************************
  !  customer_name_temp = AppendString(     job_ali:title, job_ali:initial, ' ')
  !  customer_name_temp = AppendString(customer_name_temp, job_ali:surname, ' ')
          !------------------------------------------------------------------
  IF CLIP(job_ali:title) = ''
      customer_name_temp = job_ali:initial
  ELSIF CLIP(job_ali:initial) = ''
      customer_name_temp = job_ali:title
  ELSE
      customer_name_temp = CLIP(job_ali:title) & ' ' & CLIP(job_ali:initial)
  END ! IF
          !------------------------------------------------------------------
  IF CLIP(customer_name_temp) = ''
      customer_name_temp = job_ali:surname
  ELSIF CLIP(job_ali:surname) = ''
              ! customer_name_temp = customer_name_temp ! tautology
  ELSE
      customer_name_temp = CLIP(customer_name_temp) & ' ' & CLIP(job_ali:surname)
  END ! IF
          !------------------------------------------------------------------
  
  endUserTelNo = ''
  
  if jobe:EndUserTelNo <> ''
      endUserTelNo = clip(jobe:EndUserTelNo)
  end ! if
  
  if customer_name_temp <> ''
      clientName = 'Client: ' & clip(customer_name_temp) & '  Tel: ' & clip(endUserTelNo)
  else
      if endUserTelNo <> ''
          clientName = 'Tel: ' & clip(endUserTelNo)
      end ! if
  end ! if
  
  ! if job_ali:title = '' and job_ali:initial = ''
  !    customer_name_temp = clip(job_ali:surname)
  ! elsif job_ali:title = '' and job_ali:initial <> ''
  !    customer_name_temp = clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  ! elsif job_ali:title <> '' and job_ali:initial = ''
  !    customer_name_temp = clip(job_ali:title) & ' ' & clip(job_ali:surname)
  ! elsif job_ali:title <> '' and job_ali:initial <> ''
  !    customer_name_temp = clip(job_ali:title) & ' ' & clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  ! else
  !    customer_name_temp = ''
  ! end
  !*********************************************************************************************************************
  
  
  !*********************************************************************************************************************
  delivery_Company_Name_temp = job_ali:Company_Name_Delivery
  delivery_address1_temp     = job_ali:address_line1_delivery
  delivery_address2_temp     = job_ali:address_line2_delivery
  If job_ali:address_line3_delivery   = ''
      delivery_address3_temp = job_ali:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp = job_ali:address_line3_delivery
      delivery_address4_temp = job_ali:postcode_delivery
  End ! If
  delivery_Telephone_temp = job_ali:Telephone_Delivery
  !*********************************************************************************************************************
  
  
  !*********************************************************************************************************************
      ! If an RRC job, then put the RRC as the invoice address,
      ! and the customer address as the delivery address
  If ~glo:WebJob And jobe:WebJob
      SetTarget(Report)
      ?DeliveryAddress{prop:Text} = 'CUSTOMER ADDRESS'
      SetTarget()
      Access:WEBJOB.Clearkey(wob:RefNumberKey)
      wob:RefNumber   = job_ali:Ref_Number
      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          ! Found
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = wob:HeadAccountNumber
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              ! Found
              invoice_address1_temp = tra:address_line1
              invoice_address2_temp = tra:address_line2
              If job_ali:address_line3_delivery   = ''
                  invoice_address3_temp = tra:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = tra:address_line3
                  invoice_address4_temp = tra:postcode
              End ! If
              invoice_company_name_temp     = tra:company_name
              invoice_telephone_number_temp = tra:telephone_number
              invoice_fax_number_temp       = tra:fax_number
              invoice_EMail_Address         = tra:EmailAddress
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          ! Error
          End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      ! Error
      End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  
  
  
  Else ! If ~glo:WebJob And jobe:WebJob
  
  
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job_ali:account_number
      access:subtracc.fetch(sub:account_number_key)
  
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.fetch(tra:account_number_key)
      if tra:invoice_sub_accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
          If sub:invoice_customer_address = 'YES'
              invoice_address1_temp = job_ali:address_line1
              invoice_address2_temp = job_ali:address_line2
              If job_ali:address_line3_delivery   = ''
                  invoice_address3_temp = job_ali:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = job_ali:address_line3
                  invoice_address4_temp = job_ali:postcode
              End ! If
              invoice_company_name_temp     = job_ali:company_name
              invoice_telephone_number_temp = job_ali:telephone_number
              invoice_fax_number_temp       = job_ali:fax_number
              invoice_EMail_Address         = jobe:EndUserEmailAddress  ! '' ! tra:EmailAddress
          ! MEssage('case 1')
          Else! If sub:invoice_customer_address = 'YES'
              invoice_address1_temp = sub:address_line1
              invoice_address2_temp = sub:address_line2
              If job_ali:address_line3_delivery   = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = sub:address_line3
                  invoice_address4_temp = sub:postcode
              End ! If
              invoice_company_name_temp     = sub:company_name
              invoice_telephone_number_temp = sub:telephone_number
              invoice_fax_number_temp       = sub:fax_number
              invoice_EMail_Address         = sub:EmailAddress
  
          End! If sub:invoice_customer_address = 'YES'
  
      else! if tra:use_sub_accounts = 'YES'
          If tra:invoice_customer_address = 'YES'
              invoice_address1_temp = job_ali:address_line1
              invoice_address2_temp = job_ali:address_line2
              If job_ali:address_line3_delivery   = ''
                  invoice_address3_temp = job_ali:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = job_ali:address_line3
                  invoice_address4_temp = job_ali:postcode
              End ! If
              invoice_company_name_temp     = job_ali:company_name
              invoice_telephone_number_temp = job_ali:telephone_number
              invoice_fax_number_temp       = job_ali:fax_number
              invoice_EMail_Address         = jobe:EndUserEmailAddress ! '' ! tra:EmailAddress
  
          Else! If tra:invoice_customer_address = 'YES'
              !            UseAlternativeAddress# = 1
              invoice_address1_temp = tra:address_line1
              invoice_address2_temp = tra:address_line2
              If job_ali:address_line3_delivery   = ''
                  invoice_address3_temp = tra:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = tra:address_line3
                  invoice_address4_temp = tra:postcode
              End ! If
              invoice_company_name_temp     = tra:company_name
              invoice_telephone_number_temp = tra:telephone_number
              invoice_fax_number_temp       = tra:fax_number
              invoice_EMail_Address         = tra:EmailAddress
  
          End! If tra:invoice_customer_address = 'YES'
      end!!if tra:use_sub_accounts = 'YES'
  End ! If ~glo:WebJob And jobe:WebJob!*********************************************************************************************************************
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'JOB RECEIPT'
  access:stantext.fetch(stt:description_key)
  
  If job_ali:exchange_unit_number <> ''
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number = job_ali:exchange_unit_number
      if access:exchange.fetch(xch:ref_number_key) = Level:Benign
          exchange_unit_number_temp  = 'Unit: ' & Clip(job_ali:exchange_unit_number)
          exchange_model_number      = xch:model_number
          exchange_manufacturer_temp = xch:manufacturer
          exchange_unit_type_temp    = 'N/A'
          exchange_esn_temp          = xch:esn
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = xch:msn
      end! if access:exchange.fetch(xch:ref_number_key) = Level:Benign
      Else! If job_ali:exchange_unit_number <> ''
      x#          = 0
      save_jea_id = access:jobexacc.savefile()
      access:jobexacc.clearkey(jea:part_number_key)
      jea:job_ref_number = job_ali:ref_number
      set(jea:part_number_key, jea:part_number_key)
      loop
          if access:jobexacc.next()
              break
          end ! if
          if jea:job_ref_number <> job_ali:ref_number      |
              then break   ! end if
          end ! if
          x# = 1
          Break
      end ! loop
      access:jobexacc.restorefile(save_jea_id)
      If x# = 1
          exchange_unit_number_temp  = ''
          exchange_model_number      = 'N/A'
          exchange_manufacturer_temp = job_ali:manufacturer
          exchange_unit_type_temp    = 'ACCESSORY'
          exchange_esn_temp          = 'N/A'
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = 'N/A'
      End! If x# = 1
  End! If job_ali:exchange_unit_number <> ''
  
  
  x# = 1
  setcursor(cursor:wait)
  ! tmp:accessories= ''
  ! save_jac_id = access:jobacc.savefile()
  ! access:jobacc.clearkey(jac:ref_number_key)
  ! jac:ref_number = job:ref_number
  ! set(jac:ref_number_key,jac:ref_number_key)
  ! loop
  !    if access:jobacc.next()
  !       break
  !    end !if
  !    if jac:ref_number <> job:ref_number      |
  !        then break.  ! end if
  !    yldcnt# += 1
  !    if yldcnt# > 25
  !       yield() ; yldcnt# = 0
  !    end !if
  !    If tmp:accessories = ''
  !        tmp:accessories = jac:accessory
  !    Else!If tmp:accessories = ''
  !        tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
  !    End!If tmp:accessories = ''
  ! end !loop
  ! access:jobacc.restorefile(save_jac_id)
  tmp:Accessories = ''
  Save_jac_ID     = Access:JOBACC.SaveFile()
  Access:JOBACC.ClearKey(jac:DamagedKey)
  jac:Ref_Number = job_ali:Ref_Number
  jac:Damaged    = 0
  Set(jac:DamagedKey, jac:DamagedKey)
  Loop
      If Access:JOBACC.NEXT()
          Break
      End ! If
      If jac:Ref_Number <> job_ali:Ref_Number      |
          Then Break   ! End If
      End ! If
  
      If jac:Damaged = 1
          jac:Accessory = 'DAMAGED ' & Clip(jac:Accessory)
      End ! If jac:Damaged = 1
  
      If tmp:Accessories = ''
          tmp:Accessories = jac:Accessory
      Else ! If tmp:Accessories = ''
          tmp:Accessories = Clip(tmp:Accessories) & ', ' & jac:Accessory
      End ! If tmp:Accessories = ''
  End ! Loop
  Access:JOBACC.RestoreFile(Save_jac_ID)
  setcursor()
  
  
  
  If job_ali:estimate = 'YES'
      Settarget(report)
      Unhide(?estimate_text)
      Unhide(?estimate_value_temp)
      estimate_value_temp = Clip(Format(job_ali:Estimate_If_Over, @n14.2)) & ' (Plus VAT)'
      Settarget()
  End! If job_ali:estimate = 'YES'
  
  
  If job_ali:chargeable_job = 'YES'
      setcursor(cursor:wait)
      save_jpt_id = access:jobpaymt.savefile()
      access:jobpaymt.clearkey(jpt:all_date_key)
      jpt:ref_number = job_ali:ref_number
      set(jpt:all_date_key, jpt:all_date_key)
      loop
          if access:jobpaymt.next()
              break
          end ! if
          if jpt:ref_number <> job_ali:ref_number      |
              then break   ! end if
          end ! if
          yldcnt# += 1
          if yldcnt# > 25
              yield() 
              yldcnt# = 0
          end ! if
          amount_received_temp += jpt:amount
      end ! loop
      access:jobpaymt.restorefile(save_jpt_id)
      setcursor()
      Else! If job_ali:chargeable_job = 'YES'
      Settarget(report)
      Hide(?amount_text)
      Settarget()
  End! If job_ali:chargeable_job = 'YES'
  
  ! Added by Neil 03/07/2001!
  
  IF job_ali:Chargeable_Job <> 'YES'
      Settarget(report)
      Hide(?job_ali:Charge_Type)
      Hide(?String40:3)
      Settarget()
  END ! IF
  
  ! Loan unit information (Added by Gary (7th Aug 2002))
  ! //------------------------------------------------------------------------
  tmp:LoanModel       = ''
  tmp:LoanIMEI        = ''
  tmp:LoanAccessories = ''
  
  if job_ali:Loan_Unit_Number <> 0                                            ! Is a loan unit attached to this job ?
  
      save_loa_id = Access:Loan.SaveFile()
      Access:Loan.Clearkey(loa:Ref_Number_Key)
      loa:Ref_Number  = job_ali:Loan_Unit_Number
      if not Access:Loan.TryFetch(loa:Ref_Number_Key)                         ! Fetch loan unit record
          tmp:LoanModel = clip(loa:Manufacturer) & ' ' & loa:Model_Number
          tmp:LoanIMEI  = loa:ESN
  
          save_lac_id = Access:LoanAcc.SaveFile()
          Access:LoanAcc.ClearKey(lac:ref_number_key)
          lac:ref_number = loa:ref_number
          set(lac:ref_number_key, lac:ref_number_key)
          loop until Access:LoanAcc.Next()                                    ! Fetch loan accessories
              if lac:ref_number <> loa:ref_number then break.
              if lac:Accessory_Status <> 'ISSUE' then cycle.                  ! Only display issued loan accessories
              if tmp:LoanAccessories = ''
                  tmp:LoanAccessories = lac:Accessory
              else
                  tmp:LoanAccessories = clip(tmp:LoanAccessories) & ', ' & lac:Accessory
              end ! if
  
          end ! loop
          Access:LoanAcc.RestoreFile(save_lac_id)
  
          save_stt_id = Access:StanText.SaveFile()                            ! Attach the 'loan disclaimer' standard text
          Access:StanText.ClearKey(stt:Description_Key)
          stt:Description = 'LOAN DISCLAIMER'
          if not Access:StanText.Fetch(stt:Description_Key)
              tmp:StandardText = clip(tmp:StandardText) & '<13,10>' & stt:Text
          end ! if
          Access:StanText.RestoreFile(save_stt_id)
  
      end ! if
      Access:Loan.RestoreFile(save_loa_id)
  
  end ! if
  
  if tmp:LoanModel <> ''                                                      ! Display loan details on report
      SetTarget(report)
      Unhide(?LoanUnitIssued)
      Unhide(?tmp:LoanModel)
      Unhide(?IMEITitle)
      Unhide(?tmp:LoanIMEI)
      Unhide(?LoanAccessoriesTitle)
      Unhide(?tmp:LoanAccessories)
      SetTarget()
  end ! if
  
  tmp:LoanDepositPaid = 0
  
  Access:JobPaymt.ClearKey(jpt:Loan_Deposit_Key)
  jpt:Ref_Number   = job_ali:Ref_Number
  jpt:Loan_Deposit = True
  set(jpt:Loan_Deposit_Key, jpt:Loan_Deposit_Key)
  loop until Access:JobPaymt.Next()                                           ! Loop through payments attached to job
      if jpt:Ref_Number <> job_ali:Ref_Number then break.
      if jpt:Loan_Deposit <> True then break.
      tmp:LoanDepositPaid += jpt:Amount                                       ! Check for deposit value
  end ! loop
  
  if tmp:LoanDepositPaid <> 0
      SetTarget(report)
      Unhide(?LoanDepositPaidTitle)
      Unhide(?tmp:LoanDepositPaid)                                            ! Display fields if a deposit was found
      SetTarget()
  end ! if
  
  ! //------------------------------------------------------------------------
  !Barcode Bit
  code_temp   = 3
  option_temp = 0
  
  bar_code_string_temp = job_ali:ref_number
  job_number_temp      = 'Job No: ' & job_ali:ref_number
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp             = 'I.M.E.I.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'JOB RECEIPT'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  
  locTermsText = GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI')   ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Job Receipt'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END













Retail_Despatch_Note PROCEDURE(LONG fRefNumber)
RejectRecord         LONG,AUTO
locRefNumber         LONG
hide_temp            BYTE
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
vat_rate_parts_temp  REAL
vat_total_temp       REAL
total_temp           REAL
vat_rate_temp        REAL
Delivery_Name_Temp   STRING(30)
Delivery_Address_Line_1_Temp STRING(60)
save_res_id          USHORT,AUTO
Invoice_Name_temp    STRING(30)
Invoice_address_Line_1 STRING(60)
items_total_temp     REAL
vat_temp             REAL
item_cost_temp       REAL
line_cost_temp       REAL
despatch_note_number_temp STRING(10)
courier_cost_temp    REAL
sub_total_temp       REAL
line_cost_ex_temp    REAL
PartsList            QUEUE,PRE(tmpque)
RecordNumber         LONG,NAME('RecordNumber')
OrderNumber          STRING(30),NAME('OrderNumber')
PartNumber           STRING(60),NAME('PartNumber')
Quantity             LONG,NAME('Quantity')
Description          STRING(30)
                     END
tmp:DefaultEmailAddress STRING(255)
tmp:RecordsCount     LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(RETDESNO)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,4573,7521,4260),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(354,760,7521,9469),USE(?unnamed)
                         STRING('Date Printed:'),AT(5010,979),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6),AT(6000,979),USE(ReportRunDate),TRN,LEFT,FONT('Arial',8,,FONT:bold)
                         STRING(@T1),AT(6667,979),USE(ReportRunTime),TRN,FONT(,8,,FONT:bold)
                         STRING('Tel:'),AT(208,2240),USE(?string22:4),TRN,FONT(,8,,)
                         STRING('Despatch Number:'),AT(5000,156),USE(?string22:3),TRN,FONT(,8,,)
                         STRING(@s10),AT(5990,156),USE(despatch_note_number_temp),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s8),AT(5990,365),USE(ret:Ref_Number),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Sale Number:'),AT(5000,365),USE(?string22:10),TRN,FONT(,8,,)
                         STRING('Account No'),AT(208,2917),USE(?string22:6),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(208,3125),USE(ret:Account_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Contact Name'),AT(3125,2917),USE(?string22:11),TRN,FONT(,8,,FONT:bold)
                         STRING('Purchase Order No'),AT(1667,2917),USE(?string22:7),TRN,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(3125,3125),USE(ret:Contact_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s20),AT(1667,3125),USE(ret:Purchase_Order_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(260,8490,2448,781),USE(ret:Delivery_Text),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(208,1458),USE(ret:Company_Name_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1458),USE(ret:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(208,1615,3438,208),USE(Delivery_Address_Line_1_Temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(4063,1615,3281,208),USE(Invoice_address_Line_1),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1771),USE(ret:Address_Line2_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1771),USE(ret:Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(500,2240),USE(ret:Telephone_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(4063,2240),USE(?string22:9),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2240),USE(ret:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(208,2396),USE(?string22:5),TRN,FONT(,8,,)
                         STRING(@s10),AT(208,2083),USE(ret:Postcode_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s10),AT(4063,2083),USE(ret:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1927),USE(ret:Address_Line3_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1927),USE(ret:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(500,2396),USE(ret:Fax_Number_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(4063,2396),USE(?string22:8),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2396),USE(ret:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
break2                 BREAK(endofreport),USE(?unnamed:2)
detail2                  DETAIL,AT(,,,208),USE(?unnamed:5)
                           STRING(@n10.2),AT(5833,0),USE(res:Item_Cost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n10.2b),AT(6719,0),USE(line_cost_ex_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s15),AT(156,0),USE(res:Purchase_Order_Number),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s6),AT(1146,0),USE(tmpque:Quantity),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s60),AT(1615,0,2552,208),USE(tmpque:PartNumber),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s25),AT(4219,0),USE(tmpque:Description),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                         END
BackTitle                DETAIL,AT(,,,531),USE(?BackTitle)
                           LINE,AT(156,52,7188,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING('BACK ORDER PARTS'),AT(104,104),USE(?string44:5),TRN,FONT(,9,,FONT:bold)
                         END
                       END
                       FOOTER,AT(365,9167,7521,2000),USE(?unnamed:4)
                         STRING('Courier Cost:'),AT(4948,104),USE(?Courier_Cost),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6563,104),USE(courier_cost_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Total Line Cost (Ex VAT)'),AT(4948,313),USE(?Sub_Total),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6542,313),USE(sub_total_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('V.A.T.:'),AT(4948,521),USE(?Vat),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6542,521),USE(vat_total_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(6354,729,1042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total (Inc VAT):'),AT(4948,781),USE(?Total),TRN,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(6542,781),USE(total_temp),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(104,1146,7292,781),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(354,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:OrderCompanyName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('DESPATCH NOTE'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:OrderAddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:OrderAddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:OrderAddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(def:OrderPostcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,958),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,958),USE(def:OrderTelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1115),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1115),USE(def:OrderFaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1250,3844,198),USE(def:OrderEmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1271),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Qty'),AT(1427,3854),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('(Ex. V.A.T.)'),AT(6906,3927),USE(?Line_Cost:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Part Number'),AT(1656,3854),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(4260,3854),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6927,3844),USE(?line_cost:1),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Order No'),AT(198,3854),USE(?string24:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost'),AT(5927,3854),USE(?item_Cost),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('DELIVERY ADDRESS'),AT(208,1490),USE(?string44:2),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(4063,1490),USE(?string44:3),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4740,8490),USE(?Charge_Details),TRN,FONT(,9,,FONT:bold)
                         STRING('(Despatched Items Only)'),AT(5938,8510),USE(?despatched_items_only),TRN,FONT(,7,,FONT:bold)
                         STRING('DELIVERY TEXT'),AT(104,8490),USE(?Charge_Details:2),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Retail_Despatch_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Retail_Despatch_Note')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'RetailDesp'
  If ClarioNETServer:Active() = True
      If PrintOption(PreviewReq,glo:ExportReport,'Retail Despatch Note') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  End ! If ClarioNETServer:Active() = True
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('locRefNumber',locRefNumber)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  ! Before Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  locRefNumber = fRefNumber
  ! After Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  FileOpensReached = True
  FilesOpened = True
  Relate:RETDESNO.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:INVOICE.Open
  Relate:STANTEXT.Open
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:RETSALES.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(RETDESNO)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(RETDESNO,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(rdn:Despatch_Number_Key)
      Process:View{Prop:Filter} = |
      'rdn:Despatch_Number = locRefNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
    Clear(PartsList)
    Free(PartsList)
    setcursor(cursor:wait)
    save_res_id = access:retstock.savefile()
    access:retstock.clearkey(res:despatched_only_key)
    res:ref_number = ret:ref_number
    res:despatched = 'YES'
    set(res:despatched_only_key,res:despatched_only_key)
    loop
        if access:retstock.next()
           break
        end !if
        if res:ref_number <> ret:ref_number      |
        or res:despatched <> 'YES'      |
            then break.  ! end if
        tmp:RecordsCount += 1
        If hide_temp = 1
            Settarget(report)
            Hide(?res:item_cost)
            Hide(?line_cost:1)
            Hide(?line_cost:2)
            HIde(?line_cost_ex_temp)
            Settarget()
        End!If hide_temp = 1
        IF (ret:ExchangeOrder = 1)
            ! #12127 Show invididual imei numbers (Bryan: 04/07/2011)
            tmpque:RecordNumber = res:Record_Number
            tmpque:OrderNumber = res:Purchase_Order_Number
            tmpque:PartNumber = res:Part_Number

            Access:STOCK.Clearkey(sto:Location_Key)
            sto:Location = MainStoreLocation()
            sto:Part_Number = res:Part_Number
            IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
                tmpque:PartNumber = CLIP(tmpque:PartNumber) & ' ' & Clip(sto:ExchangeModelNumber)
            END ! IF (Access:STOCK.Clearkey(sto:Location_Key) = Level:Benign)

            tmpque:Quantity = 1
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number = res:ExchangeRefNumber
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
                tmpque:Description = ' * EXCHANGE MISSING *'
            ELSE
                tmpque:Description = xch:ESN
            END
            ADD(PartsList)
        ELSE

            tmpque:OrderNumber  = res:Purchase_Order_Number
            tmpque:PartNumber   = res:Part_number
            Get(PartsList,'OrderNumber,PartNumber')
            If Error()
                tmpque:RecordNumber = res:Record_Number
                tmpque:OrderNumber  = res:Purchase_Order_Number
                tmpque:PartNumber   = res:Part_Number
                tmpque:Quantity     = res:Quantity
                tmpque:Description  = res:Description
                Add(PartsList)
            Else !If Error()
                tmpque:Quantity     += res:Quantity
                Put(PartsList)
            End !If Error()
        END
    end !loop
    access:retstock.restorefile(save_res_id)
    setcursor()

    Sort(PartsList,tmpque:OrderNumber,tmpque:PartNumber)
    Loop x# = 1 To Records(PartsList)
        Get(PartsList,x#)
        Access:RETSTOCK.ClearKey(res:Record_Number_Key)
        res:Record_Number = tmpque:RecordNumber
        If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
            line_cost_ex_temp = Round(res:item_cost * tmpque:quantity,.01)
            sub_total_temp += line_cost_ex_temp
            Print(rpt:Detail2)
        End !If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
    End !x# = 1 To Records(PartsList)

    vat_total_temp = Round((ret:courier_cost + sub_total_temp) * vat_rate_temp/100,.01)
    total_temp  = ret:courier_cost + Round(vat_total_temp,.01) + Round(sub_total_temp,.01)

        Clear(PartsList)
        Free(PartsList)
        first# = 1
        save_res_id = access:retstock.savefile()
        access:retstock.clearkey(res:despatched_key)
        res:ref_number           = ret:ref_number
        res:despatched           = 'PEN'
        set(res:despatched_key,res:despatched_key)
        loop
            if access:retstock.next()
               break
            end !if
            if res:ref_number           <> ret:ref_number      |
            or res:despatched           <> 'PEN'      |
                then break.  ! end if
            If first# = 1
                Print(rpt:backtitle)
                first# = 0
            End!If first# = 1
            Settarget(report)
            Hide(?res:item_cost)
            Hide(?line_cost_ex_temp)
            Settarget()
            vat_temp     = ''
            line_cost_temp  = ''
            item_cost_temp  = ''
            tmpque:OrderNumber  = res:Purchase_Order_Number
            tmpque:PartNumber   = res:Part_number
            Get(PartsList,'OrderNumber,PartNumber')
            If Error()
                tmpque:RecordNumber = res:Record_Number
                tmpque:OrderNumber  = res:Purchase_Order_Number
                tmpque:PartNumber   = res:Part_Number
                tmpque:Quantity     = res:Quantity
                tmpque:Description  = res:Description
                Add(PartsList)
            Else !If Error()
                tmpque:Quantity     += res:Quantity
                Put(PartsList)
            End !If Error()
        
        end !loop
        access:retstock.restorefile(save_res_id)
        
        Sort(PartsList,tmpque:OrderNumber,tmpque:PartNumber)
        Loop x# = 1 To Records(PartsList)
            Get(PartsList,x#)
            Access:RETSTOCK.ClearKey(res:Record_Number_Key)
            res:Record_Number = tmpque:RecordNumber
            If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                Print(rpt:Detail2)
            End !If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
        End !x# = 1 To Records(PartsList)
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RETDESNO,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','Retail_Despatch_Note')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:RETDESNO.Close
    Relate:STANTEXT.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'RETDESNO')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE - RETAIL'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  ! Inserting (DBH 20/10/2006) # 8381 - Allow to set how many copies are printed
  If glo:ReportCopies > 0
      printer{propprint:Copies} = glo:ReportCopies
      glo:ReportCopies = 0
  End ! If glo:ReportCopies > 0
  ! End (DBH 20/10/2006) #8381
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  access:retsales.clearkey(ret:ref_number_key)
  ret:ref_number = RDN:Sale_Number
  access:retsales.tryfetch(ret:ref_number_key)
  
  
  despatch_note_number_temp   = 'D-' & locRefNumber
  If ret:payment_method = 'CAS'
      Delivery_Name_Temp  = RET:Contact_Name
      Invoice_Name_Temp   = ret:contact_name
  Else!If ret:payment_method = 'CAS'
      Delivery_Name_Temp  = RET:Company_Name_Delivery
      invoice_name_temp   = ret:company_name
  End!If ret:payment_method = 'CAS'
  If ret:building_name    = ''
      Invoice_address_Line_1     = Clip(RET:Address_Line1)
  Else!    If ret:building_name    = ''
      Invoice_address_Line_1     = Clip(RET:Building_Name) & ' ' & Clip(RET:Address_Line1)
  End!
  If ret:building_name_delivery   = ''
      Delivery_Address_Line_1_Temp    = Clip(RET:Address_Line1_Delivery)
  Else!    If ret:building_name_delivery   = ''
      Delivery_Address_Line_1_Temp    = Clip(RET:Building_Name_Delivery) & ' ' & Clip(RET:Address_Line1_Delivery)
  End!    If ret:building_name_delivery   = ''
  
  courier_cost_temp   = ret:courier_cost
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = ret:account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          If tra:price_retail_despatch <> 'YES'
              hide_temp = 1
              Settarget(Report)
              Hide(?courier_cost)
              Hide(?sub_total)
              Hide(?vat)
              Hide(?total)
              Hide(?line1)
              Hide(?charge_details)
              Hide(?vat_total_temp)
              Hide(?total_temp)
              Hide(?courier_cost_temp)
              Hide(?sub_total_temp)
              Hide(?item_cost)
              Hide(?line_cost:1)
              Hide(?line_cost:2)
              Hide(?despatched_items_only)
              Settarget()
          End!If tra:price_retail_despatch <> 'YES'
          if tra:invoice_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = sub:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          else!if tra:use_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = tra:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  !Alternative Contact Nos
  Case UseAlternativeContactNos(ret:Account_Number,0)
      Of 1
          tmp:DefaultTelephone    = tra:AltTelephoneNumber
          tmp:DefaultFax          = tra:AltFaxNumber
          tmp:DefaultEmailAddress = tra:AltEmailAddress
      Of 2
          tmp:DefaultTelephone    = sub:AltTelephoneNumber
          tmp:DefaultFax          = sub:AltFaxNumber
          tmp:DefaultEmailAddress = sub:AltEmailAddress
      Else !UseAlternativeContactNos(ret:Account_Number)
          tmp:DefaultEmailAddress = def:EmailAddress
  End !UseAlternativeContactNos(ret:Account_Number)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Retail_Despatch_Note'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













Multiple_Despatch_Note PROCEDURE(func:AccountNumber,func:ConsignmentNumber,func:BatchNumber,func:Courier,func:Type)
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
                     END
save_jea_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
save_jpt_id          USHORT,AUTO
print_group          QUEUE,PRE(prngrp)
esn                  STRING(30)
msn                  STRING(30)
job_number           REAL
                     END
Invoice_Address_Group GROUP,PRE(INVGRP)
Postcode             STRING(15)
Company_Name         STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Delivery_Address_Group GROUP,PRE(DELGRP)
Postcode             STRING(15)
Company_Name         STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Unit_Details_Group   GROUP,PRE(UNIGRP)
Model_Number         STRING(30)
Manufacturer         STRING(30)
ESN                  STRING(16)
MSN                  STRING(16)
CRepairType          STRING(30)
WRepairType          STRING(30)
                     END
Unit_Ref_Number_Temp REAL
Despatch_Type_Temp   STRING(3)
Invoice_Account_Number_Temp STRING(15)
Delivery_Account_Number_Temp STRING(15)
count_temp           REAL
model_number_temp    STRING(50)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:DefaultEmailAddress STRING(255)
tmp:Courier          STRING(30)
tmp:ConsignmentNumber STRING(30)
tmp:DespatchBatchNumber STRING(30)
!-----------------------------------------------------------------------------
Process:View         VIEW(SUBTRACC)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,3865,7521,6490),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,823,7521,2542),USE(?unnamed)
                         STRING('Courier:'),AT(4896,104),USE(?String23),TRN,FONT(,8,,)
                         STRING(@s30),AT(6042,104),USE(tmp:Courier),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Consignment No:'),AT(4896,260),USE(?String27),TRN,FONT(,8,,)
                         STRING(@s30),AT(6042,260),USE(tmp:ConsignmentNumber),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Despatch Date:'),AT(4896,417),USE(?String27:2),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6042,417),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatch Batch No:'),AT(4896,729,990,208),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(6042,729),USE(tmp:DespatchBatchNumber),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4896,885),USE(?String62),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@n3),AT(6042,885),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(4073,1354),USE(INVGRP:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1510),USE(INVGRP:Address_Line1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Account No:'),AT(208,2292),USE(?String38),TRN,FONT(,8,,)
                         STRING(@s15),AT(885,2292),USE(GLO:Select1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(4073,2135),USE(?String36:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Fax:'),AT(5521,2135),USE(?String37:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4375,2135),USE(INVGRP:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4688,2292),USE(GLO:Select1,,?glo:select1:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Account No:'),AT(4073,2292),USE(?String38:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,2135),USE(INVGRP:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1667),USE(INVGRP:Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1823),USE(INVGRP:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2135),USE(?String36),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Fax:'),AT(1719,2135),USE(?String37),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4073,1979),USE(INVGRP:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,146),USE(?detailband)
                           STRING(@s30),AT(156,0),USE(prngrp:esn),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(3021,0),USE(UNIGRP:CRepairType),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(1042,0),USE(prngrp:msn),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s3),AT(5729,0),USE(Despatch_Type_Temp),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s25),AT(6042,0),USE(job:Order_Number),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s20),AT(4115,0),USE(UNIGRP:WRepairType),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(1927,0),USE(UNIGRP:Model_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(5208,0),USE(tmp:Ref_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(,,,354),USE(?Totals)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,104,677,156),USE(?String59),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(1042,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(365,10344,7521,854),USE(?unnamed:2)
                         LINE,AT(208,52,7135,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(208,104,7135,677),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(385,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('MULTIPLE DESPATCH NOTE'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3542,208),USE(tmp:DefaultEmailAddress),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?telephone),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(156,1198),USE(?fax),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('INVOICE ADDRESS'),AT(208,1510),USE(?String29),TRN,FONT(,9,,FONT:bold)
                         STRING('Email:'),AT(156,1354),USE(?Email),TRN,FONT(,9,,)
                         STRING('DELIVERY ADDRESS'),AT(4063,1510),USE(?String29:2),TRN,FONT(,9,,FONT:bold)
                         STRING('Model Number'),AT(1938,3177),USE(?string44),TRN,FONT(,7,,FONT:bold)
                         STRING('I.M.E.I. Number.'),AT(156,3177),USE(?string25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('M.S.N.'),AT(1052,3177),USE(?string25:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Type'),AT(5729,3177),USE(?string25:3),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Order Number'),AT(6052,3177),USE(?string25:5),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('War Repair Type'),AT(4125,3177),USE(?string44:3),TRN,FONT(,7,,FONT:bold)
                         STRING('Char Repair Type'),AT(3031,3177),USE(?string44:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Job No'),AT(5219,3177),USE(?string25:4),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Multiple_Despatch_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Multiple_Despatch_Note')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'MultipleDespatchNote'
  If ClarioNETServer:Active() = True
      If PrintOption(PreviewReq,glo:ExportReport,'Multiple Despatch Note') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  End ! If ClarioNETServer:Active() = True
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:SUBTRACC.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:LOAN.UseFile
  
  
  RecordsToProcess = RECORDS(SUBTRACC)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(SUBTRACC,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      recordstoprocess    = Records(glo:q_jobnumber) * 2
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
        ! 26 Aug 2002 John
        !
        tmp:Ref_number = job:Ref_number
        if glo:WebJob then
            !Change the tmp ref number if you
            !can Look up from the wob file
            access:Webjob.clearkey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_number
            if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                access:tradeacc.clearkey(tra:Account_Number_Key)
                    tra:Account_Number = ClarioNET:Global.Param2
                access:tradeacc.fetch(tra:Account_Number_Key)
        
                tmp:Ref_Number = Job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
            END
        END
        !--------------------------------------------------------------
        Free(print_group)
        Clear(print_group)
        setcursor(cursor:wait)
        
        Loop x# = 1 To Records(glo:q_jobnumber)
            Get(glo:q_jobnumber,x#)
            recordsprocessed += 1
            Do DisplayProgress
        
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = glo:job_number_pointer
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
                prngrp:esn        = job:esn
                prngrp:msn        = job:msn
                prngrp:job_number = job:ref_number
                Add(print_group)
            End!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        End!Loop x# = 1 To Records(glo:q_jobnumber)
        
        Sort(print_group,prngrp:esn,prngrp:msn)
        
        Loop x# = 1 To Records(print_group)
            Get(print_group,x#)
            recordsprocessed += 1
            Do DisplayProgress
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = prngrp:job_number
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
                If job:Chargeable_Job = 'YES'
                    unigrp:CRepairType = job:Repair_Type
                Else !If job:Chargeable_Job = 'YES'
                    unigrp:CRepairType  = ''
                End !If job:Chargeable_Job = 'YES'
                If job:Warranty_job = 'YES'
                    unigrp:WRepairType = job:Repair_Type_Warranty
                Else !If job:Warranty_job = 'YES'
                    unigrp:WRepairType = ''
                End !If job:Warranty_job = 'YES'
                If JOB:Consignment_Number = func:ConsignmentNumber
                    INVGRP:Postcode         = job:postcode
                    INVGRP:Company_Name     = job:company_name
                    INVGRP:Address_Line1    = job:address_line1
                    INVGRP:Address_Line2    = job:address_line2
                    INVGRP:Address_Line3    = job:address_line3
                    INVGRP:Telephone_Number = job:telephone_number
                    INVGRP:Fax_Number       = job:fax_number
                    UNIGRP:Model_Number     = job:Model_Number
                    UNIGRP:Manufacturer     = job:manufacturer
                    UNIGRP:ESN              = job:esn
                    UNIGRP:MSN              = job:msn
                    despatch_Type_temp  = 'JOB'
                End!If JOB:Incoming_Consignment_Number = glo:select2
                If JOB:Exchange_Consignment_Number = func:ConsignmentNumber
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number = job:exchange_unit_number
                    if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                        UNIGRP:Model_Number = XCH:Model_Number
                        UNIGRP:Manufacturer = xch:manufacturer
                        UNIGRP:ESN          = xch:esn
                        UNIGRP:MSN          = xch:msn
                    end
                    despatch_Type_temp  = 'EXC'
                End!If JOB:Exchange_Consignment_Number = glo:select2
                If JOB:Loan_Consignment_Number = func:ConsignmentNumber
                    access:loan.clearkey(loa:ref_number_key)
                    loa:ref_number = job:loan_unit_number
                    if access:loan.fetch(loa:ref_number_key) = Level:Benign
                        UNIGRP:Model_Number = loa:Model_Number
                        UNIGRP:Manufacturer = loa:manufacturer
                        UNIGRP:ESN          = loa:esn
                        UNIGRP:MSN          = loa:msn
                    end
                    despatch_Type_temp  = 'LOA'
                End!If JOB:Exchange_Consignment_Number = glo:select2
                Print(Rpt:detail)
            end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        
        End!Loop x# = 1 To Records(print_group)
        count_temp = Records(glo:q_jobnumber)
        Print(rpt:totals)
        
        setcursor()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(SUBTRACC,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','Multiple_Despatch_Note')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  tmp:ConsignmentNumber = func:ConsignmentNumber
  tmp:DespatchBatchNumber = func:BatchNumber
  tmp:Courier = func:Courier
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !printed by
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  !Hide Despatch Address
  If HideDespAdd# = 1
      Settarget(Report)
      ?def:User_Name{prop:Hide} = 1
      ?def:Address_Line1{prop:Hide} = 1
      ?def:Address_Line2{prop:Hide} = 1
      ?def:Address_Line3{prop:Hide} = 1
      ?def:Postcode{prop:hide} = 1
      ?tmp:DefaultTelephone{prop:hide} = 1
      ?tmp:DefaultFax{prop:hide} = 1
      ?telephone{prop:hide} = 1
      ?fax{prop:hide} = 1
      ?email{prop:hide} = 1
      ?tmp:DefaultEmailAddress{prop:Hide} = 1
      Settarget()
  End!If HideDespAdd# = 1
  !--- Webify SETUP ClarioNET:UseReportPreview(0) ---------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  END
  !--------------------------------------------------------------
  HideDespAdd# = 0
  
  If func:Type = 'TRA'
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = func:AccountNumber
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          INVGRP:Postcode           = tra:Postcode
          INVGRP:Company_Name       = tra:Company_Name
          INVGRP:Address_Line1      = tra:Address_Line1
          INVGRP:Address_Line2      = tra:Address_Line2
          INVGRP:Address_Line3      = tra:Address_Line3
          INVGRP:Telephone_Number   = tra:Telephone_Number
          INVGRP:Fax_Number         = tra:Fax_Number
          DELGRP:Postcode           = tra:postcode
          DELGRP:Company_Name       = tra:company_name
          DELGRP:Address_Line1      = tra:address_line1
          DELGRP:Address_Line2      = tra:address_line2
          DELGRP:Address_Line3      = tra:address_line3
          DELGRP:Telephone_Number   = tra:telephone_number
          DELGRP:Fax_Number         = tra:fax_number
  
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  Else !func:Type = 'TRA'
  
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = func:AccountNumber
      if access:subtracc.fetch(sub:account_number_key) = level:benign
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = sub:main_account_number
          if access:tradeacc.fetch(tra:account_number_key) = level:benign
              if tra:invoice_sub_accounts = 'YES'
                  If sub:HideDespAdd = 1
                      HideDespAdd# = 1
                  End!If sub:HideDespAdd = 1
              Else
                  If tra:HideDespAdd = 1
                      HideDespAdd# = 1
                  End!If sub:HideDespAdd = 1
              End
              if tra:use_sub_accounts = 'YES'
                  DELGRP:Postcode           = sub:postcode
                  DELGRP:Company_Name       = sub:company_name
                  DELGRP:Address_Line1      = sub:address_line1
                  DELGRP:Address_Line2      = sub:address_line2
                  DELGRP:Address_Line3      = sub:address_line3
                  DELGRP:Telephone_Number   = sub:telephone_number
                  DELGRP:Fax_Number         = sub:fax_number
                  If tra:invoice_sub_accounts = 'YES'
                      INVGRP:Postcode           = sub:Postcode
                      INVGRP:Company_Name       = sub:Company_Name
                      INVGRP:Address_Line1      = sub:Address_Line1
                      INVGRP:Address_Line2      = sub:Address_Line2
                      INVGRP:Address_Line3      = sub:Address_Line3
                      INVGRP:Telephone_Number   = sub:Telephone_Number
                      INVGRP:Fax_Number         = sub:Fax_Number
                  Else
                      INVGRP:Postcode           = tra:Postcode
                      INVGRP:Company_Name       = tra:Company_Name
                      INVGRP:Address_Line1      = tra:Address_Line1
                      INVGRP:Address_Line2      = tra:Address_Line2
                      INVGRP:Address_Line3      = tra:Address_Line3
                      INVGRP:Telephone_Number   = tra:Telephone_Number
                      INVGRP:Fax_Number         = tra:Fax_Number
                  End!If tra:invoice_sub_accounts = 'YES'
              else!if tra:use_sub_accounts = 'YES'
                  INVGRP:Postcode           = tra:Postcode
                  INVGRP:Company_Name       = tra:Company_Name
                  INVGRP:Address_Line1      = tra:Address_Line1
                  INVGRP:Address_Line2      = tra:Address_Line2
                  INVGRP:Address_Line3      = tra:Address_Line3
                  INVGRP:Telephone_Number   = tra:Telephone_Number
                  INVGRP:Fax_Number         = tra:Fax_Number
                  DELGRP:Postcode           = tra:postcode
                  DELGRP:Company_Name       = tra:company_name
                  DELGRP:Address_Line1      = tra:address_line1
                  DELGRP:Address_Line2      = tra:address_line2
                  DELGRP:Address_Line3      = tra:address_line3
                  DELGRP:Telephone_Number   = tra:telephone_number
                  DELGRP:Fax_Number         = tra:fax_number
              end!if tra:use_sub_accounts = 'YES'
          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
      end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  End !func:Type = 'TRA'
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE'
  access:stantext.fetch(stt:description_key)
  
  !Hide Despatch Address
  If HideDespAdd# = 1
      Settarget(Report)
      ?def:User_Name{prop:Hide} = 1
      ?def:Address_Line1{prop:Hide} = 1
      ?def:Address_Line2{prop:Hide} = 1
      ?def:Address_Line3{prop:Hide} = 1
      ?def:Postcode{prop:hide} = 1
      ?tmp:DefaultTelephone{prop:hide} = 1
      ?tmp:DefaultFax{prop:hide} = 1
      ?telephone{prop:hide} = 1
      ?fax{prop:hide} = 1
      ?email{prop:hide} = 1
      tmp:DefaultEmailAddress{prop:Hide} = 1
      Settarget()
  End!If HideDespAdd# = 1
  
  
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  !Alternative Contact Nos
  UseHead# = 0
  If func:Type = 'TRA'
      UseHead# = 1
  End !func:Type = 'TRA'
  Case UseAlternativeContactNos(func:AccountNumber,useHead#)
      Of 1
          tmp:DefaultEmailAddress = tra:AltEmailAddress
          tmp:DefaultTelephone    = tra:AltTelephoneNumber
          tmp:DefaultFax          = tra:AltFaxNumber
      Of 2
          tmp:DefaultEmailAddress = sub:AltEmailAddress
          tmp:DefaultTelephone    = sub:AltTelephoneNumber
          tmp:DefaultFax          = sub:AltFaxNumber
      Else !UseAlternativeContactNos(job_ali:Account_Number)
          tmp:DefaultEmailAddress = def:EmailAddress
  End !UseAlternativeContactNos(job_ali:Account_Number)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Multiple_Despatch_Note'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END







