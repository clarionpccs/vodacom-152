

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01003.INC'),ONCE        !Local module procedure declarations
                     END








Chargeable_Summary_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
count_temp           LONG
tmp:PrintedBy        STRING(255)
save_job_id          USHORT,AUTO
first_page_temp      LONG(1)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
account_number_temp  STRING(15)
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
labour_total_temp    REAL
parts_total_temp     REAL
carriage_total_temp  REAL
vat_total_temp       REAL
grand_total_temp     REAL
total_lines_temp     STRING(20)
invoice_telephone    STRING(20)
invoice_Fax_number   STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
BranchIdentification LIKE(tra:BranchIdentification)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Account_Number)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Invoice_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(458,2083,10938,4448),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),LANDSCAPE,THOUS
                       HEADER,AT(438,323,10917,1719),USE(?unnamed)
                         STRING(@s30),AT(104,104,3438,260),USE(def:Invoice_Company_Name),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(3750,260),USE(Invoice_Name_Temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,365,3438,260),USE(def:Invoice_Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(3750,417),USE(Address_Line1_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,521,3438,260),USE(def:Invoice_Address_Line2),TRN,FONT(,9,,)
                         STRING('Date Of Invoice:'),AT(7448,729),USE(?String44),TRN,FONT(,8,,)
                         STRING(@d6b),AT(8594,729),USE(inv:Date_Created),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,677,3438,260),USE(def:Invoice_Address_Line3),TRN,FONT(,9,,)
                         STRING('Account Number:'),AT(7448,885),USE(?String45),TRN,FONT(,8,,)
                         STRING(@s15),AT(8594,885),USE(inv:Account_Number),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(104,833,1156,156),USE(def:Invoice_Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(104,1042),USE(?String16),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,1042),USE(def:Invoice_Telephone_Number),FONT(,9,,)
                         STRING(@s30),AT(3750,729),USE(Address_Line3_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Fax: '),AT(104,1198),USE(?String19),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,1198),USE(def:Invoice_Fax_Number),FONT(,9,,)
                         STRING('Tel:'),AT(3750,1042),USE(?String47),TRN,FONT(,8,,)
                         STRING(@s20),AT(4010,1042),USE(invoice_telephone),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('V.A.T. Number:'),AT(7448,1042),USE(?String46),TRN,FONT(,8,,)
                         STRING(@s30),AT(8594,1042),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(7448,1198),USE(?String46:2),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(3750,1198),USE(?String48),TRN,FONT(,8,,)
                         STRING(@s20),AT(4010,1198),USE(invoice_Fax_number),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(9063,1198,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s255),AT(521,1354,3438,198),USE(def:InvoiceEmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(104,1354),USE(?String19:2),TRN,FONT(,8,,)
                         STRING(@s3),AT(8594,1198),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(8854,1198,156,260),USE(?String46:3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(3750,885),USE(Address_Line4_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Invoice Number:'),AT(7448,573),USE(?String43),TRN,FONT(,8,,)
                         STRING(@s10),AT(8594,573),USE(inv:Invoice_Number),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(3750,573),USE(Address_Line2_Temp),TRN,FONT(,8,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s15),AT(156,0),USE(tmp:Ref_Number),TRN,RIGHT,FONT(,7,,)
                           STRING(@s20),AT(990,0,833,156),USE(job:Order_Number),TRN,FONT(,7,,)
                           STRING(@s20),AT(1875,0),USE(job:Model_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s18),AT(2969,0),USE(job:Manufacturer),TRN,FONT(,7,,)
                           STRING(@s16),AT(3958,0),USE(job:ESN),TRN,FONT(,7,,)
                           STRING(@s16),AT(4844,0),USE(job:MSN),TRN,FONT(,7,,)
                           STRING(@n10.2),AT(8698,0),USE(labour_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(9167,0),USE(parts_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(9635,0),USE(courier_cost_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(10208,0),USE(line_cost_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s20),AT(7656,0),USE(job:Repair_Type),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s20),AT(6563,0),USE(job:Charge_Type),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s15),AT(5729,0),USE(job:Mobile_Number),TRN,FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,1594),USE(?unnamed:2),ABSOLUTE
                         END
                       END
                       FOOTER,AT(448,6927,10917,844),USE(?unnamed:3)
                         TEXT,AT(104,52,7135,729),USE(stt:Text),FONT(,8,,)
                         STRING('Labour:'),AT(7490,52),USE(?String49),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(9896,52),USE(labour_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('Parts:'),AT(7490,208),USE(?String50),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(9896,208),USE(parts_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('Carriage:'),AT(7490,365),USE(?String51),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(9896,365),USE(carriage_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('V.A.T.'),AT(7490,521),USE(?String52),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(9896,521),USE(vat_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('Total:'),AT(7490,729),USE(?String53),TRN,FONT(,8,,FONT:bold)
                         STRING('<128>'),AT(9833,729),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(9896,729),USE(grand_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                       END
                       FORM,AT(438,417,10917,7417),USE(?unnamed:4)
                         IMAGE('Rinvlan.gif'),AT(0,0,10938,7448),USE(?Image1)
                         STRING('Number Of Lines:'),AT(104,6198),USE(?String54),TRN,FONT(,,,FONT:bold)
                         STRING(@s9),AT(1354,6198),USE(count_temp),TRN,LEFT
                         STRING('SUMMARY INVOICE'),AT(8906,104),USE(?String42),TRN,FONT(,14,,FONT:bold)
                         STRING('Order Number'),AT(1010,1458),USE(?String17),TRN,FONT(,7,,FONT:bold)
                         STRING('Job No'),AT(104,1458),USE(?String18),TRN,FONT(,7,,FONT:bold)
                         STRING('Make'),AT(3021,1458),USE(?String20),TRN,FONT(,7,,FONT:bold)
                         STRING('I.M.E.I. No'),AT(4083,1458),USE(?String21),TRN,FONT(,7,,FONT:bold)
                         STRING('M.S.N.'),AT(4865,1458),USE(?String37),TRN,FONT(,7,,FONT:bold)
                         STRING('Labour'),AT(8854,1458),USE(?String38),TRN,FONT(,7,,FONT:bold)
                         STRING('Parts'),AT(9375,1458),USE(?String39),TRN,FONT(,7,,FONT:bold)
                         STRING('Carriage'),AT(9688,1458),USE(?String40),TRN,FONT(,7,,FONT:bold)
                         STRING('Line Cost'),AT(10208,1458),USE(?String41),TRN,FONT(,7,,FONT:bold)
                         STRING('Repair Type'),AT(7677,1458),USE(?RepairType),TRN,FONT(,7,,FONT:bold)
                         STRING('Charge Type'),AT(6583,1458),USE(?ChargeType),TRN,FONT(,7,,FONT:bold)
                         STRING('Mobile No'),AT(5750,1458),USE(?String37:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Model'),AT(1896,1458),USE(?String35),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Chargeable_Summary_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Chargeable_Summary_Invoice')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        parts_total_temp    = 0
        labour_total_temp   = 0
        carriage_total_temp = 0
        vat_total_temp      = 0
        grand_total_temp    = 0
        
        parts_temp        = ''
        labour_temp       = ''
        courier_cost_temp = ''
        vat_temp          = ''
        line_cost_temp    = ''
        
        count_temp = 0
        setcursor(cursor:wait)
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:InvoiceNumberKey)
        job:invoice_number = inv:invoice_number
        set(job:InvoiceNumberKey,job:InvoiceNumberKey)
        loop
            if access:jobs.next()
               break
            end !if
            if job:invoice_number <> inv:invoice_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
        
            !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
            ! 26 Aug 2002 John
            !
            tmp:Ref_number = job:Ref_number
            if glo:WebJob then
                !Change the tmp ref number if you
                !can Look up from the wob file
                access:Webjob.clearkey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_number
                if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                    tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
                END
            END
            !--------------------------------------------------------------
        
            parts_temp = job:invoice_parts_cost
            labour_temp = job:invoice_labour_cost
            courier_cost_temp = job:invoice_courier_cost
        !        vat_temp = Round((job:invoice_parts_cost * inv:vat_rate_labour/100),.01) + |
        !                    Round((job:invoice_labour_cost * inv:vat_rate_labour/100),.01) +|
        !                    Round((job:invoice_courier_cost * inv:vat_rate_labour/100),.01)
            vat_temp = (job:invoice_parts_cost * inv:vat_rate_labour/100) + |
                        (job:invoice_labour_cost * inv:vat_rate_labour/100) +|
                        (job:invoice_courier_cost * inv:vat_rate_labour/100)
        
            line_cost_temp = parts_temp + labour_temp + courier_cost_temp
        
            parts_total_temp += job:invoice_parts_cost
            labour_total_temp += job:invoice_labour_cost
            carriage_total_temp += job:invoice_courier_cost
        
        !        vat_total_temp = Round((parts_total_temp * inv:vat_rate_labour/100),.01) + |
        !                    Round((labour_total_temp * inv:vat_rate_labour/100),.01) +|
        !                    Round((carriage_total_temp * inv:vat_rate_labour/100),.01)
            vat_total_temp = (parts_total_temp * inv:vat_rate_labour/100) + |
                        (labour_total_temp * inv:vat_rate_labour/100) +|
                        (carriage_total_temp * inv:vat_rate_labour/100)
        
            grand_total_temp = vat_total_temp + (parts_total_temp + labour_total_temp + carriage_total_temp)
        
            count_temp += 1
            Print(rpt:detail)
        
        end !loop
        access:jobs.restorefile(save_job_id)
        setcursor()
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Chargeable_Summary_Invoice')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !--- Webify SETUP ClarioNET:UseReportPreview(0) tmp:BranchIdentification ---------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      access:tradeacc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = ClarioNET:Global.Param2
      IF Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
          tmp:BranchIdentification = tra:BranchIdentification
      ELSE
          tmp:BranchIdentification = ''
      END !IF
  
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  
  END
  !---------------------------------------------------------------------------------------
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvlan.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  access:invoice.clearkey(inv:invoice_Number_key)
  inv:invoice_number  = glo:select1
  access:invoice.fetch(inv:invoice_number_key)
  case inv:accounttype
      Of 'MAI'
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number  = inv:account_number
          if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
              invoice_name_temp   = tra:company_name
              address_line1_temp  = tra:address_line1
              address_line2_temp  = tra:address_line2
              If sup:address_line3 = ''
                  address_line3_temp = tra:postcode
                  address_line4_temp = ''
              Else
                  address_line3_temp  = tra:address_line3
                  address_line4_temp  = tra:postcode
              End
              invoice_telephone   = tra:telephone_number
              invoice_fax_number         = tra:fax_number
          end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
      Else
          access:subtracc.clearkey(sub:account_number_key)
          sub:account_number  = inv:account_number
          access:subtracc.fetch(sub:account_number_key)
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = sub:main_account_number
          if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
              if tra:invoice_sub_accounts = 'YES'
                  invoice_name_temp   = sub:company_name
                  address_line1_temp  = sub:address_line1
                  address_line2_temp  = sub:address_line2
                  If sup:address_line3 = ''
                      address_line3_temp = sub:postcode
                      address_line4_temp = ''
                  Else
                      address_line3_temp  = sub:address_line3
                      address_line4_temp  = sub:postcode
                  End
                  invoice_telephone   = sub:telephone_number
                  invoice_fax_number         = sub:fax_number
  
              else!if tra:use_sub_accounts = 'YES'
                  invoice_name_temp   = tra:company_name
                  address_line1_temp  = tra:address_line1
                  address_line2_temp  = tra:address_line2
                  If sup:address_line3 = ''
                      address_line3_temp = tra:postcode
                      address_line4_temp = ''
                  Else
                      address_line3_temp  = tra:address_line3
                      address_line4_temp  = tra:postcode
                  End
                  invoice_telephone   = tra:telephone_number
                  invoice_fax_number         = tra:fax_number
  
              end!if tra:use_sub_accounts = 'YES'
          end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
  
  End!case inv:accounttype
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Chargeable_Summary_Invoice'
  END
  Report{Prop:Preview} = PrintPreviewImage













Single_Invoice_Summary PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
TMP:Account_No       STRING(20)
tmp:totalcourier     REAL
tmp:totalparts       REAL
tmp:totallabour      REAL
tmp:totalexvat       REAL
tmp:totalinvat       REAL
tmp:totalvat         REAL
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:telephonenumber  STRING(20)
tmp:faxnumber        STRING(20)
tmp:total            REAL
tmp:count            LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
BranchIdentification LIKE(tra:BranchIdentification)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Invoice_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,927,7521,1000),USE(?unnamed)
                         STRING(@s20),AT(5885,167),USE(TMP:Account_No),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('Account No.'),AT(5000,167),USE(?String64),TRN,FONT('Arial',8,,)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,781),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,781),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,781),USE(?string26),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,167),USE(?detailband)
                           STRING(@s8),AT(156,0),USE(inv:Invoice_Number),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(5042,0),USE(job:Invoice_Labour_Cost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(677,0,833,156),USE(tmp:Ref_Number),TRN,RIGHT(1),FONT(,7,,,CHARSET:ANSI)
                           STRING(@s30),AT(1563,0,729,156),USE(job:Order_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(4469,0),USE(job:Invoice_Courier_Cost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(5615,0),USE(job:Invoice_Parts_Cost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(6188,0),USE(job:Invoice_Sub_Total),TRN,RIGHT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n10.2),AT(6771,0),USE(tmp:total),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(3438,0,1094,156),USE(job:Repair_Type),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(2344,0,1042,156),USE(job:Charge_Type),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,1813),USE(?unnamed:2)
                           STRING(@n10.2),AT(4375,52),USE(tmp:totalcourier),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(4948,52),USE(tmp:totallabour),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(5521,52),USE(tmp:totalparts),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           LINE,AT(156,0,7240,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING(@n10.2),AT(6094,52),USE(tmp:totalexvat),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(6677,52),USE(tmp:totalinvat),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Summary Analysis for'),AT(208,469),USE(?String62),TRN,FONT('Arial',8,,)
                           STRING(@s8),AT(1323,469),USE(tmp:count,,?tmp:count:2),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING('Jobs'),AT(1938,469),USE(?String63),TRN,FONT('Arial',8,,)
                           STRING('Courier'),AT(208,677),USE(?String57),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n-14.2),AT(990,1208),USE(tmp:totalexvat,,?tmp:totalexvat:2),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING('VAT'),AT(208,1385),USE(?String61),TRN,FONT('Arial',8,,)
                           STRING(@n-14.2),AT(990,1385),USE(tmp:totalvat),TRN,RIGHT,FONT('Arial',8,,)
                           LINE,AT(1042,1563,729,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING('Sub Total'),AT(208,1208),USE(?String56),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING('Total'),AT(208,1594),USE(?String58),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n-14.2),AT(990,1031),USE(tmp:totalparts,,?tmp:totalparts:2),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n-14.2),AT(990,854),USE(tmp:totallabour,,?tmp:totallabour:2),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Parts'),AT(208,1031),USE(?String55),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n-14.2),AT(990,1594),USE(tmp:totalinvat,,?tmp:totalinvat:2),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n-14.2),AT(990,677),USE(tmp:totalcourier,,?tmp:totalcourier:2),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Labour'),AT(208,854),USE(?String54),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING('<128>'),AT(4323,52),USE(?Euro),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING('Total: '),AT(208,52,313,156),USE(?String40),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(781,52),USE(tmp:count),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:Invoice_Company_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('INVOICE SUMMARY'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Invoice_Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Invoice_Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Invoice_Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Invoice_Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s15),AT(573,1042),USE(def:Invoice_Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s15),AT(573,1198),USE(def:Invoice_Fax_Number),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1354,3844,198),USE(def:InvoiceEmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Inv No'),AT(208,2083),USE(?string44),TRN,FONT(,7,,FONT:bold)
                         STRING('Order No'),AT(1563,2083),USE(?string45),TRN,FONT(,7,,FONT:bold)
                         STRING('Labour'),AT(5156,2083),USE(?string24),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Parts'),AT(5781,2083),USE(?string25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Total (Ex VAT)'),AT(6042,2083),USE(?string25:3),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Total (Inc VAT)'),AT(6719,2083),USE(?string25:4),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Repair Type'),AT(3438,2083),USE(?string25:6),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Charge Type'),AT(2333,2083),USE(?string25:5),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Courier'),AT(4531,2083),USE(?string25:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Job No'),AT(677,2083),USE(?string44:2),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Single_Invoice_Summary')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Single_Invoice_Summary')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Loop x# = 1 To Records(glo:Queue3)
            Get(glo:Queue3,x#)
            access:invoice.clearkey(inv:invoice_number_key)
            inv:invoice_number  = glo:pointer3
            If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
                access:jobs.clearkey(job:ref_number_key)
                job:invoice_number  = glo:pointer3
                If access:jobs.tryfetch(job:InvoiceNumberKey) = Level:Benign
                    !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
                    ! 26 Aug 2002 John
                    !
                    tmp:Ref_number = job:Ref_number
                    if glo:WebJob then
                        !Change the tmp ref number if you
                        !can Look up from the wob file
                        access:Webjob.clearkey(wob:RefNumberKey)
                        wob:RefNumber = job:Ref_number
                        if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                            tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
                        END
                    END
                    !--------------------------------------------------------------
                    Total_Price('I',vat",total",balance")
                    tmp:total           = total"
                    tmp:totalcourier    += JOB:Invoice_Courier_Cost
                    tmp:totalparts      += job:invoice_parts_cost
                    tmp:totallabour     += job:invoice_labour_cost
                    tmp:totalexvat      += job:invoice_sub_total
                    tmp:totalinvat      += tmp:total
                    tmp:totalvat        += tmp:total - job:invoice_sub_total
                    tmp:count           += 1
                    !--------------------------------------------------------------
                    Print(rpt:detail)
                End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
            End!If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
        End!Loop x# = 1 To Records(glo:Queue3)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Single_Invoice_Summary')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !--- Webify SETUP ClarioNET:UseReportPreview(0) tmp:BranchIdentification ---------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      access:tradeacc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = ClarioNET:Global.Param2
      IF Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
          tmp:BranchIdentification = tra:BranchIdentification
      ELSE
          tmp:BranchIdentification = '--'
      END !IF
  
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  
  END
  !---------------------------------------------------------------------------------------
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  Get(glo:Queue3,1)
  access:invoice.clearkey(inv:invoice_number_key)
  inv:invoice_number  = glo:pointer3
  If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
      access:jobs.clearkey(job:ref_number_key)
      job:invoice_number  = glo:pointer3
      If access:jobs.tryfetch(job:InvoiceNumberKey) = Level:Benign
        TMP:Account_No = job:Account_Number
      End
  End
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Single_Invoice_Summary'
  END
  report{Prop:Preview} = PrintPreviewImage













Chargeable_Proforma_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:defaultfax       STRING(20)
save_job_id          USHORT,AUTO
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:telephonenumber  STRING(20)
tmp:faxnumber        STRING(20)
tmp:AuthorisedQueue  QUEUE,PRE(autque)
AuthQty              LONG
AuthJobType          STRING(30)
AuthRepairType       STRING(30)
AuthItemPrice        REAL
AuthLinePrice        REAL
AuthVatAmount        REAL
                     END
Invoice_Number_Temp  STRING(28)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
tmp:AuthCount        LONG
tmp:LineTotal        REAL
tmp:VatTotal         REAL
tmp:QueryCount       LONG
tmp:ValueTotal       REAL
tmp:InvoiceTotal     REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
BranchIdentification LIKE(tra:BranchIdentification)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:InvoiceQuery)
                       PROJECT(job:Sub_Total)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,3854,7521,6500),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,),THOUS
                       HEADER,AT(396,521,7521,2948),USE(?unnamed)
                         STRING(@s30),AT(156,83,3844,240),USE(def:Invoice_Company_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,365,3844,156),USE(def:Invoice_Address_Line1),TRN
                         STRING(@s8),AT(5729,469),USE(prv:BatchNumber),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(156,521,3844,156),USE(def:Invoice_Address_Line2),TRN
                         STRING('Account No:'),AT(2083,1667),USE(?String31:4),TRN,FONT(,8,,)
                         STRING('Batch Number:'),AT(4896,469),USE(?String31:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,677,3844,156),USE(def:Invoice_Address_Line3),TRN
                         STRING(@s15),AT(2760,1667),USE(prv:AccountNumber),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Batch Date:'),AT(4896,625),USE(?String31:3),TRN,FONT(,8,,)
                         STRING(@s15),AT(156,833,1156,156),USE(def:Invoice_Postcode),TRN
                         STRING(@d6),AT(5729,625),USE(prv:DateCreated),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN
                         STRING(@s15),AT(573,1042),USE(def:Invoice_Telephone_Number),TRN
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN
                         STRING(@s15),AT(573,1198),USE(def:Invoice_Fax_Number),TRN
                         STRING('Page Number: '),AT(4896,1198),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@N4),AT(5729,1198),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Of '),AT(6042,1198),USE(?PagePrompt:2),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,1667),USE(Invoice_Name_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6250,1198,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s255),AT(573,1354,3844,208),USE(def:InvoiceEmailAddress),TRN
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN
                         STRING('V.A.T. Number: '),AT(156,2656),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s30),AT(990,2656),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,1844,1917,156),USE(Address_Line1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2000),USE(Address_Line2_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2156),USE(Address_Line3_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2323),USE(Address_Line4_Temp),TRN,FONT(,8,,)
                         STRING('Printed:'),AT(4896,1042),USE(?String46),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5729,1042),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6354,1042),USE(ReportRunTime),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2500,1323,156),USE(INV:Invoice_VAT_Number),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,156),USE(?DetailBand)
                           STRING(@s8),AT(156,0),USE(autque:AuthQty),TRN,RIGHT(1),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(833,0),USE(autque:AuthJobType),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(2760,0),USE(autque:AuthRepairType),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(5573,0),USE(autque:AuthLinePrice),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6458,0),USE(autque:AuthVatAmount),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
AuthTotal                DETAIL,AT(,,,583),USE(?AuthTotal)
                           LINE,AT(156,52,1042,0),USE(?Line5),COLOR(COLOR:Black)
                           LINE,AT(5625,52,1667,0),USE(?Line6),COLOR(COLOR:Black)
                           STRING('Totals:'),AT(156,104),USE(?String57:4),TRN,FONT('Arial',8,,FONT:bold)
                           STRING(@s8),AT(573,104),USE(tmp:AuthCount),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n14.2),AT(5573,104),USE(tmp:LineTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n14.2),AT(6458,104),USE(tmp:VatTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
QueryTitle               DETAIL,AT(,,,625),USE(?QueryTitle)
                           STRING('QUERY REPAIR DETAILS'),AT(156,52),USE(?String56),TRN,FONT('Arial',9,,FONT:bold)
                           LINE,AT(156,417,7135,0),USE(?Line1:3),COLOR(COLOR:Black)
                           STRING('Job No'),AT(156,260),USE(?String57),TRN,FONT('Arial',8,,FONT:bold)
                           STRING('Job Value (Net)'),AT(1146,260),USE(?String57:2),TRN,FONT('Arial',8,,FONT:bold)
                           STRING('Failure Reason'),AT(2292,260),USE(?String57:3),TRN,FONT('Arial',8,,FONT:bold)
                         END
Query                    DETAIL,AT(,,,427),USE(?Query)
                           TEXT,AT(2292,0,5052,365),USE(job:InvoiceQuery),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s20),AT(156,0),USE(tmp:Ref_Number),TRN,RIGHT(1),FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(1146,0),USE(job:Sub_Total),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
QueryTotal               DETAIL,AT(,,,583),USE(?QueryTotal)
                           LINE,AT(156,52,1823,0),USE(?Line7),COLOR(COLOR:Black)
                           STRING('Totals:'),AT(156,104),USE(?String57:5),TRN,FONT('Arial',8,,FONT:bold)
                           STRING(@n14.2),AT(1146,104),USE(tmp:ValueTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(573,104),USE(tmp:QueryCount),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
AuthFooter               DETAIL,USE(?AuthFooter)
                           STRING('PROFORMA INVOICE TOTAL'),AT(5104,52),USE(?String56:2),TRN,FONT('Arial',9,,FONT:bold)
                           STRING('Line Total:'),AT(5104,260),USE(?String55),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6458,260),USE(tmp:LineTotal,,?tmp:LineTotal:2),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING('V.A.T. Total:'),AT(5104,469),USE(?String55:2),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6458,469),USE(tmp:VatTotal,,?tmp:VatTotal:2),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(5104,677,2188,0),USE(?Line9),COLOR(COLOR:Black)
                           STRING(@n14.2),AT(6458,729),USE(tmp:InvoiceTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING('<128>'),AT(6427,729),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                           STRING('Proforma Inv Total:'),AT(5104,729),USE(?String55:3),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(385,10323,7521,1156),USE(?unnamed:4)
                         LINE,AT(156,52,7240,0),USE(?Line8),COLOR(COLOR:Black)
                         TEXT,AT(156,156,7240,781),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVSIM.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('BATCH PROFORMA INVOICE'),AT(4635,104),USE(?String49),TRN,FONT('Arial',14,,FONT:bold)
                         STRING('Qty'),AT(417,3177),USE(?String47),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Job Type'),AT(833,3177),USE(?String47:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Repair Type'),AT(2760,3177),USE(?String47:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Price'),AT(5938,3177),USE(?String47:5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('V.A.T. Amount'),AT(6573,3177),USE(?String47:6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('AUTHORISED FOR INVOICING'),AT(156,2969),USE(?String47:7),TRN,FONT('Arial',9,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Chargeable_Proforma_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Chargeable_Proforma_Invoice')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:PROINV.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Alternative Invoice Address
        If def:use_invoice_address = 'YES'
            Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
            Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
            Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
            Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
            Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
            Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
            Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
            Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
        Else!If def:use_invoice_address = 'YES'
            Default_Invoice_Company_Name_Temp       = DEF:User_Name
            Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
            Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
            Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
            Default_Invoice_Postcode_Temp           = DEF:Postcode
            Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
            Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
            Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
        End!If def:use_invoice_address = 'YES'
        Set(DEFAULT2)
        Access:DEFAULT2.Next()
        
        
        access:proinv.clearkey(prv:refnumberkey)
        prv:refnumber = glo:select1
        if access:proinv.tryfetch(prv:refnumberkey) = Level:Benign
            Case PRV:AccountType
                Of 'MAI'
                    access:tradeacc.clearkey(tra:account_number_key)
                    tra:account_number  = prv:AccountNumber
                    If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                        Invoice_Name_Temp   = tra:company_name
                        Address_Line1_Temp  = tra:address_line1
                        Address_Line2_Temp  = tra:address_line2
                        Address_Line3_Temp  = tra:address_line3
                        Address_Line4_Temp  = tra:postcode
                    End!If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                    access:vatcode.clearkey(VAT:Vat_code_Key)
                    VAT:VAT_Code = tra:Labour_VAT_Code
                    If access:vatcode.tryfetch(vat:vat_code_key) = Level:Benign
                        labour_rate$ = vat:vat_rate
                    End!If access:vatcode.clearkey(vat:vat_code_key) = Level:Benign
                    access:vatcode.clearkey(VAT:Vat_code_Key)
                    VAT:VAT_Code = tra:Parts_VAT_Code
                    If access:vatcode.tryfetch(vat:vat_code_key) = Level:Benign
                        parts_rate$ = vat:vat_rate
                    End!If access:vatcode.clearkey(vat:vat_code_key) = Level:Benign
        
                Of 'SUB'
                    access:subtracc.clearkey(sub:account_number_key)
                    sub:account_number  = prv:AccountNumber
                    If access:subtracc.tryfetch(tra:account_number_key) = Level:Benign
                        Invoice_Name_Temp   = sub:company_name
                        Address_Line1_Temp  = sub:address_line1
                        Address_Line2_Temp  = sub:address_line2
                        Address_Line3_Temp  = sub:address_line3
                        Address_Line4_Temp  = sub:postcode
                    End!If access:subtracc.tryfetch(tra:account_number_key) = Level:Benign
                    access:vatcode.clearkey(VAT:Vat_code_Key)
                    VAT:VAT_Code = sub:Labour_VAT_Code
                    If access:vatcode.tryfetch(vat:vat_code_key) = Level:Benign
                        labour_rate$ = vat:vat_rate
                    End!If access:vatcode.clearkey(vat:vat_code_key) = Level:Benign
                    access:vatcode.clearkey(VAT:Vat_code_Key)
                    VAT:VAT_Code = sub:Parts_VAT_Code
                    If access:vatcode.tryfetch(vat:vat_code_key) = Level:Benign
                        parts_rate$ = vat:vat_rate
                    End!If access:vatcode.clearkey(vat:vat_code_key) = Level:Benign
        
            End!Case PRV:Status
            save_job_id = access:jobs.savefile()
            access:jobs.clearkey(job:batchstatuskey)
            job:invoiceaccount = prv:AccountNumber
            job:invoicebatch   = prv:BatchNumber
            job:invoicestatus  = 'AUT'
            set(job:batchstatuskey,job:batchstatuskey)
            loop
                if access:jobs.next()
                   break
                end !if
                if job:invoiceaccount <> prv:AccountNumber      |
                or job:invoicebatch   <> prv:BatchNumber      |
                or job:invoicestatus  <> 'AUT'      |
                    then break.  ! end if
                tmp:AuthCount += 1
                Sort(tmp:AuthorisedQueue,autque:AuthJobType,autque:AuthRepairType)
                autque:authrepairtype   = job:repair_type
                Get(tmp:AuthorisedQueue,autque:AuthRepairType)
                If ~Error()
                    autque:Authitemprice += ''
                    autque:AuthLinePrice += job:sub_total
                    autque:AuthVatAmount   += Round(job:labour_cost * labour_rate$/100,.01) + |
                                                Round(job:parts_cost * parts_rate$/100,.01) + |
                                                Round(job:courier_cost * labour_rate$/100,.01)
                    autque:Authqty += 1
                    Put(tmp:AuthorisedQueue)
                Else!If Error()
                    autque:AuthQty          = 1
                    autque:AuthJobType      = job:charge_type
                    autque:AuthRepairType   = job:repair_type
                    autque:AuthItemPrice    = ''
                    autque:AuthLinePrice    = job:sub_total
                    autque:AuthVatAmount    = Round(job:labour_cost * labour_rate$/100,.01) + |
                                                Round(job:parts_cost * parts_rate$/100,.01) + |
                                                Round(job:courier_cost * labour_rate$/100,.01)
                    Add(tmp:AuthorisedQueue)
                End!If Error()
                tmp:LineTotal += job:sub_total
                tmp:VatTotal  += Round(job:labour_cost * labour_rate$/100,.01) + |
                                 Round(job:parts_cost * parts_rate$/100,.01) + |
                                 Round(job:courier_cost * labour_rate$/100,.01)
            end !loop
            access:jobs.restorefile(save_job_id)
        
        end!if access:proinv.tryfetch(prv:refnumberkey) = Level:Benign
        
        If Records(tmp:AuthorisedQueue)
            Loop x# = 1 To Records(tmp:AuthorisedQueue)
                Get(tmp:AuthorisedQueue,x#)
                Print(rpt:detail)
            End!Loop x# = 1 To Records(tmp:AuthorisedQueue)
            tmp:InvoiceTotal    = tmp:LineTotal + tmp:VatTotal
            Print(rpt:AuthTotal)
            Print(rpt:AuthFooter)
        End!If Records(tmp:AuthorisedQueue)
        
        first# = 1
        count# = 0
        
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:batchstatuskey)
        job:invoiceaccount = prv:AccountNumber
        job:invoicebatch   = prv:BatchNumber
        job:invoicestatus  = 'QUE'
        set(job:batchstatuskey,job:batchstatuskey)
        loop
            if access:jobs.next()
               break
            end !if
            if job:invoiceaccount <> prv:AccountNumber      |
            or job:invoicebatch   <> prv:BatchNumber      |
            or job:invoicestatus  <> 'QUE'      |
                then break.  ! end if
            !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
            ! 26 Aug 2002 John
            !
            tmp:Ref_number = job:Ref_number
            if glo:WebJob then
                !Change the tmp ref number if you
                !can Look up from the wob file
                access:Webjob.clearkey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_number
                if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                    tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
                END
            END
            !--------------------------------------------------------------
            If first# = 1
                Print(rpt:Querytitle)
                first# = 0
            End!If first# = 1
            tmp:QueryCount += 1
            tmp:ValueTotal += job:sub_total
            Print(rpt:query)
            count# += 1
        end !loop
        access:jobs.restorefile(save_job_id)
        If count# <> 0
            Print(rpt:QueryTotal)
        End!If count# <> 0
        
        
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Chargeable_Proforma_Invoice')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:PROINV.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE - PROFORMA')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE - PROFORMA'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !--- Webify SETUP ClarioNET:UseReportPreview(0) tmp:BranchIdentification ---------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      access:tradeacc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = ClarioNET:Global.Param2
      IF Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
          tmp:BranchIdentification = tra:BranchIdentification
      ELSE
          tmp:BranchIdentification = '--'
      END !IF
  
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  
  END
  !---------------------------------------------------------------------------------------
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE - PROFORMA'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE - PROFORMA'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Chargeable_Proforma_Invoice'
  END
  report{Prop:Preview} = PrintPreviewImage













Chargeable_Batch_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:defaultfax       STRING(20)
save_job_id          USHORT,AUTO
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:telephonenumber  STRING(20)
tmp:faxnumber        STRING(20)
tmp:AuthorisedQueue  QUEUE,PRE(autque)
AuthQty              LONG
AuthJobType          STRING(30)
AuthRepairType       STRING(30)
AuthItemPrice        REAL
AuthLinePrice        REAL
AuthVatAmount        REAL
                     END
Invoice_Number_Temp  STRING(28)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
tmp:AuthCount        LONG
tmp:LineTotal        REAL
tmp:VatTotal         REAL
tmp:QueryCount       LONG
tmp:ValueTotal       REAL
tmp:InvoiceTotal     REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
BranchIdentification LIKE(tra:BranchIdentification)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:InvoiceQuery)
                       PROJECT(job:Sub_Total)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,3854,7521,6500),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,),THOUS
                       HEADER,AT(396,521,7521,2948),USE(?unnamed)
                         STRING(@s30),AT(156,83,3844,240),USE(def:Invoice_Company_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,365,3844,156),USE(def:Invoice_Address_Line1),TRN
                         STRING('Invoice Date:'),AT(4896,521),USE(?String31:6),TRN,FONT(,8,,)
                         STRING(@s8),AT(5729,885),USE(inv:Batch_Number),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Invoice Number:'),AT(4896,365),USE(?String31:5),TRN,FONT(,8,,)
                         STRING(@s8),AT(5729,365),USE(inv:Invoice_Number),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(156,521,3844,156),USE(def:Invoice_Address_Line2),TRN
                         STRING(@d6b),AT(5729,521),USE(inv:Date_Created),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Account No:'),AT(2083,1667),USE(?String31:4),TRN,FONT(,8,,)
                         STRING('Batch Number:'),AT(4896,885),USE(?String31:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,677,3844,156),USE(def:Invoice_Address_Line3),TRN
                         STRING(@s15),AT(2760,1667),USE(inv:Account_Number),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(156,833,1156,156),USE(def:Invoice_Postcode),TRN
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN
                         STRING(@s15),AT(573,1042),USE(def:Invoice_Telephone_Number),TRN
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN
                         STRING(@s15),AT(573,1198),USE(def:Invoice_Fax_Number),TRN
                         STRING('Page Number: '),AT(4896,1198),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@N4),AT(5729,1198),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Of '),AT(6042,1198),USE(?PagePrompt:2),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6198,1198,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN
                         STRING(@s255),AT(573,1354,3844,208),USE(def:InvoiceEmailAddress),TRN
                         STRING(@s30),AT(156,1667),USE(Invoice_Name_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('V.A.T. Number: '),AT(156,2656),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s30),AT(990,2656),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,1844,1917,156),USE(Address_Line1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2000),USE(Address_Line2_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2156),USE(Address_Line3_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2323),USE(Address_Line4_Temp),TRN,FONT(,8,,)
                         STRING('Printed:'),AT(4896,1042),USE(?String46),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5729,1042),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6354,1042),USE(ReportRunTime),TRN,FONT(,8,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,156),USE(?DetailBand)
                           STRING(@s8),AT(156,0),USE(autque:AuthQty),TRN,RIGHT(1),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(833,0),USE(autque:AuthJobType),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(2760,0),USE(autque:AuthRepairType),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(5573,0),USE(autque:AuthLinePrice),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6458,0),USE(autque:AuthVatAmount),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
AuthTotal                DETAIL,AT(,,,583),USE(?AuthTotal)
                           LINE,AT(156,52,1042,0),USE(?Line5),COLOR(COLOR:Black)
                           LINE,AT(5625,52,1667,0),USE(?Line6),COLOR(COLOR:Black)
                           STRING('Totals:'),AT(156,104),USE(?String57:4),TRN,FONT('Arial',8,,FONT:bold)
                           STRING(@s8),AT(573,104),USE(tmp:AuthCount),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n14.2),AT(5573,104),USE(tmp:LineTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n14.2),AT(6458,104),USE(tmp:VatTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
QueryTitle               DETAIL,USE(?QueryTitle)
                           STRING('QUERY REPAIR DETAILS'),AT(156,52),USE(?String56),TRN,FONT('Arial',9,,FONT:bold)
                           LINE,AT(156,417,7135,0),USE(?Line1:3),COLOR(COLOR:Black)
                           STRING('Job No'),AT(156,260),USE(?String57),TRN,FONT('Arial',8,,FONT:bold)
                           STRING('Job Value (Net)'),AT(1146,260),USE(?String57:2),TRN,FONT('Arial',8,,FONT:bold)
                           STRING('Failure Reason'),AT(2292,260),USE(?String57:3),TRN,FONT('Arial',8,,FONT:bold)
                         END
Query                    DETAIL,AT(,,,427),USE(?Query)
                           TEXT,AT(2292,0,5052,365),USE(job:InvoiceQuery),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s15),AT(156,0),USE(tmp:Ref_Number),TRN,RIGHT(1),FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(1146,0),USE(job:Sub_Total),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
QueryTotal               DETAIL,AT(,,,583),USE(?QueryTotal)
                           LINE,AT(156,52,1823,0),USE(?Line7),COLOR(COLOR:Black)
                           STRING('Totals:'),AT(156,104),USE(?String57:5),TRN,FONT('Arial',8,,FONT:bold)
                           STRING(@n14.2),AT(1146,104),USE(tmp:ValueTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(573,104),USE(tmp:QueryCount),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
                         FOOTER,AT(198,9000,,1198),USE(?unnamed:2),ABSOLUTE
                           STRING('Line Total:'),AT(5000,260),USE(?String55),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6458,260),USE(tmp:LineTotal,,?tmp:LineTotal:2),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING('V.A.T. Total:'),AT(5000,469),USE(?String55:2),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6458,469),USE(tmp:VatTotal,,?tmp:VatTotal:2),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(5000,677,2292,0),USE(?Line9),COLOR(COLOR:Black)
                           STRING(@n14.2),AT(6458,729),USE(tmp:InvoiceTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING('<128>'),AT(6406,729),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                           STRING('Proforma Inv Total:'),AT(5000,729),USE(?String55:3),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(385,10323,7521,1156),USE(?unnamed:4)
                         LINE,AT(156,52,7240,0),USE(?Line8),COLOR(COLOR:Black)
                         TEXT,AT(156,156,7240,781),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVSIM.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('BATCH INVOICE'),AT(5885,104),USE(?String49),TRN,FONT('Arial',14,,FONT:bold)
                         STRING('PROFORMA INVOICE TOTAL'),AT(4740,8490),USE(?String56:2),TRN,FONT('Arial',9,,FONT:bold)
                         STRING('Qty'),AT(417,3177),USE(?String47),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Job Type'),AT(833,3177),USE(?String47:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Repair Type'),AT(2760,3177),USE(?String47:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Price'),AT(5938,3177),USE(?String47:5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('V.A.T. Amount'),AT(6573,3177),USE(?String47:6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Chargeable_Batch_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Chargeable_Batch_Invoice')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:PROINV.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:INVOICE.UseFile
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Alternative Invoice Address
        If def:use_invoice_address = 'YES'
            Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
            Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
            Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
            Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
            Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
            Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
            Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
            Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
        Else!If def:use_invoice_address = 'YES'
            Default_Invoice_Company_Name_Temp       = DEF:User_Name
            Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
            Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
            Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
            Default_Invoice_Postcode_Temp           = DEF:Postcode
            Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
            Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
            Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
        End!If def:use_invoice_address = 'YES'
        Set(DEFAULT2)
        Access:DEFAULT2.Next()
        
        access:invoice.clearkey(inv:invoice_number_key)
        inv:invoice_number  = glo:select1
        If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
            Case inv:accounttype
                Of 'MAI'
                    access:tradeacc.clearkey(tra:account_number_key)
                    tra:account_number  = inv:Account_Number
                    If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                        Invoice_Name_Temp   = tra:company_name
                        Address_Line1_Temp  = tra:address_line1
                        Address_Line2_Temp  = tra:address_line2
                        Address_Line3_Temp  = tra:address_line3
                        Address_Line4_Temp  = tra:postcode
                    End!If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
        
                Of 'SUB'
                    access:subtracc.clearkey(sub:account_number_key)
                    sub:account_number  = inv:Account_Number
                    If access:subtracc.tryfetch(tra:account_number_key) = Level:Benign
                        Invoice_Name_Temp   = sub:company_name
                        Address_Line1_Temp  = sub:address_line1
                        Address_Line2_Temp  = sub:address_line2
                        Address_Line3_Temp  = sub:address_line3
                        Address_Line4_Temp  = sub:postcode
                    End!If access:subtracc.tryfetch(tra:account_number_key) = Level:Benign
        
            End!Case inv:accounttype
        
            save_job_id = access:jobs.savefile()
            access:jobs.clearkey(job:batchstatuskey)
            job:invoiceaccount = inv:Account_Number
            job:invoicebatch   = inv:Batch_Number
            set(job:batchstatuskey,job:batchstatuskey)
            loop
                if access:jobs.next()
                   break
                end !if
                if job:invoiceaccount <> inv:Account_Number      |
                or job:invoicebatch   <> inv:Batch_Number      |
                    then break.  ! end if
                !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
                ! 26 Aug 2002 John
                !
                tmp:Ref_number = job:Ref_number
                if glo:WebJob then
                    !Change the tmp ref number if you
                    !can Look up from the wob file
                    access:Webjob.clearkey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_number
                    if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                        tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
                    END
                END
                !--------------------------------------------------------------
                tmp:AuthCount += 1
                Sort(tmp:AuthorisedQueue,autque:AuthJobType,autque:AuthRepairType)
                autque:authrepairtype   = job:repair_type
                Get(tmp:AuthorisedQueue,autque:AuthRepairType)
                If ~Error()
                    autque:Authitemprice += ''
                    autque:AuthLinePrice += job:invoice_sub_total
                    autque:AuthVatAmount   += Round(job:invoice_labour_cost * inv:vat_rate_labour/100,.01) + |
                                                Round(job:invoice_parts_cost * inv:vat_rate_parts/100,.01) + |
                                                Round(job:invoice_courier_cost * inv:vat_rate_labour/100,.01)
                    autque:Authqty += 1
                    Put(tmp:AuthorisedQueue)
                Else!If Error()
                    autque:AuthQty          = 1
                    autque:AuthJobType      = job:charge_type
                    autque:AuthRepairType   = job:repair_type
                    autque:AuthItemPrice    = ''
                    autque:AuthLinePrice    = job:invoice_sub_total
                    autque:AuthVatAmount    = Round(job:invoice_labour_cost * inv:vat_rate_labour/100,.01) + |
                                                Round(job:invoice_parts_cost * inv:vat_rate_parts/100,.01) + |
                                                Round(job:invoice_courier_cost * inv:vat_rate_labour/100,.01)
                    Add(tmp:AuthorisedQueue)
                End!If Error()
                tmp:LineTotal += job:invoice_sub_total
                tmp:VatTotal  += Round(job:invoice_labour_cost * inv:vat_rate_labour/100,.01) + |
                                                Round(job:invoice_parts_cost * inv:vat_rate_parts/100,.01) + |
                                                Round(job:invoice_courier_cost * inv:vat_rate_labour/100,.01)
            end !loop
            access:jobs.restorefile(save_job_id)
        
            If Records(tmp:AuthorisedQueue)
                Loop x# = 1 To Records(tmp:AuthorisedQueue)
                    Get(tmp:AuthorisedQueue,x#)
                    Print(rpt:detail)
                End!Loop x# = 1 To Records(tmp:AuthorisedQueue)
                tmp:InvoiceTotal    = tmp:LineTotal + tmp:VatTotal
                Print(rpt:AuthTotal)
            End!If Records(tmp:AuthorisedQueue)
        End!If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Chargeable_Batch_Invoice')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:PROINV.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE - PROFORMA')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE - PROFORMA'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !--- Webify SETUP ClarioNET:UseReportPreview(0) tmp:BranchIdentification ---------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      access:tradeacc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = ClarioNET:Global.Param2
      IF Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
          tmp:BranchIdentification = tra:BranchIdentification
      ELSE
          tmp:BranchIdentification = '++'
      END !IF
  
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  
  END
  !---------------------------------------------------------------------------------------
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE - PROFORMA'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE - PROFORMA'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Chargeable_Batch_Invoice'
  END
  report{Prop:Preview} = PrintPreviewImage













DespatchNoteMultiple PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
save_jea_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
save_jpt_id          USHORT,AUTO
print_group          QUEUE,PRE(prngrp)
esn                  STRING(30)
msn                  STRING(30)
job_number           REAL
                     END
Invoice_Address_Group GROUP,PRE(INVGRP)
Postcode             STRING(15)
Company_Name         STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Delivery_Address_Group GROUP,PRE(DELGRP)
Postcode             STRING(15)
Company_Name         STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Unit_Details_Group   GROUP,PRE(UNIGRP)
Model_Number         STRING(30)
Manufacturer         STRING(30)
ESN                  STRING(16)
MSN                  STRING(16)
CRepairType          STRING(30)
WRepairType          STRING(30)
                     END
Unit_Ref_Number_Temp REAL
Despatch_Type_Temp   STRING(3)
Invoice_Account_Number_Temp STRING(15)
Delivery_Account_Number_Temp STRING(15)
count_temp           REAL
model_number_temp    STRING(50)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:DefaultEmail     STRING(255)
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
BranchIdentification LIKE(tra:BranchIdentification)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(SUBTRACC)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,3865,7521,6490),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,823,7521,2542),USE(?unnamed)
                         STRING('Courier:'),AT(4896,104),USE(?String23),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,104),USE(GLO:Select4),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Consignment No:'),AT(4896,260),USE(?String27),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,260),USE(GLO:Select2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Despatch Date:'),AT(4896,417),USE(?String27:2),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6042,417),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatch Batch No:'),AT(4896,729,990,208),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,729),USE(GLO:Select3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4896,885),USE(?String62),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@n3),AT(6042,885),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(4073,1354),USE(INVGRP:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1510),USE(INVGRP:Address_Line1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Account No:'),AT(208,2292),USE(?String38),TRN,FONT(,8,,)
                         STRING(@s15),AT(885,2292),USE(GLO:Select1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(4073,2135),USE(?String36:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Fax:'),AT(5521,2135),USE(?String37:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4375,2135),USE(INVGRP:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4688,2292),USE(GLO:Select1,,?glo:select1:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Account No:'),AT(4073,2292),USE(?String38:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,2135),USE(INVGRP:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1667),USE(INVGRP:Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1823),USE(INVGRP:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2135),USE(?String36),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Fax:'),AT(1719,2135),USE(?String37),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4073,1979),USE(INVGRP:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,146),USE(?detailband)
                           STRING(@s30),AT(156,0),USE(prngrp:esn),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(3021,0),USE(UNIGRP:CRepairType),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(1042,0),USE(prngrp:msn),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s25),AT(6302,0,1198,156),USE(job:Order_Number),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s20),AT(4115,0),USE(UNIGRP:WRepairType),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s15),AT(5208,0),USE(tmp:Ref_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s3),AT(6042,0),USE(Despatch_Type_Temp),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(1927,0),USE(UNIGRP:Model_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(,,,354),USE(?Totals)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,104,677,156),USE(?String59),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(1042,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(365,10344,7521,854),USE(?unnamed:2)
                         LINE,AT(208,52,7135,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(208,104,7135,677),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(385,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('MULTIPLE DESPATCH NOTE'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3542,208),USE(tmp:DefaultEmail),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?telephone),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(156,1198),USE(?fax),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('INVOICE ADDRESS'),AT(208,1510),USE(?String29),TRN,FONT(,9,,FONT:bold)
                         STRING('Email:'),AT(156,1354),USE(?Email),TRN,FONT(,9,,)
                         STRING('DELIVERY ADDRESS'),AT(4063,1510),USE(?String29:2),TRN,FONT(,9,,FONT:bold)
                         STRING('Model Number'),AT(1938,3177),USE(?string44),TRN,FONT(,7,,FONT:bold)
                         STRING('I.M.E.I. Number.'),AT(156,3177),USE(?string25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('M.S.N.'),AT(1052,3177),USE(?string25:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Type'),AT(6052,3177),USE(?string25:3),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Order Number'),AT(6302,3177,1042,156),USE(?string25:5),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('War Repair Type'),AT(4125,3177),USE(?string44:3),TRN,FONT(,7,,FONT:bold)
                         STRING('Char Repair Type'),AT(3031,3177),USE(?string44:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Job No'),AT(5219,3177),USE(?string25:4),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('DespatchNoteMultiple')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','DespatchNoteMultiple')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:SUBTRACC.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:LOAN.UseFile
  
  
  RecordsToProcess = RECORDS(SUBTRACC)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(SUBTRACC,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      recordstoprocess    = Records(glo:q_jobnumber) * 2
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Free(print_group)
        Clear(print_group)
        setcursor(cursor:wait)
        
        Loop x# = 1 To Records(glo:q_jobnumber)
            Get(glo:q_jobnumber,x#)
            recordsprocessed += 1
            Do DisplayProgress
        
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = glo:job_number_pointer
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
                prngrp:esn  = job:esn
                prngrp:msn  = job:msn
                prngrp:job_number = job:ref_number
                Add(print_group)
            End!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        End!Loop x# = 1 To Records(glo:q_jobnumber)
        
        Sort(print_group,prngrp:esn,prngrp:msn)
        
        Loop x# = 1 To Records(print_group)
            Get(print_group,x#)
            recordsprocessed += 1
            Do DisplayProgress
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = prngrp:job_number
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
        
                If job:Chargeable_Job = 'YES'
                    unigrp:CRepairType = job:Repair_Type
                Else !If job:Chargeable_Job = 'YES'
                    unigrp:CRepairType  = ''
                End !If job:Chargeable_Job = 'YES'
        
                If job:Warranty_job = 'YES'
                    unigrp:WRepairType = job:Repair_Type_Warranty
                Else !If job:Warranty_job = 'YES'
                    unigrp:WRepairType = ''
                End !If job:Warranty_job = 'YES'
        
                !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
                ! 26 Aug 2002 John
                !
                tmp:Ref_number = job:Ref_number
                if glo:WebJob then
                    !Change the tmp ref number if you
                    !can Look up from the wob file
                    access:Webjob.clearkey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_number
                    if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                        tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
                    END
                END
                !--------------------------------------------------------------
                If JOB:Consignment_Number = glo:select2
                    INVGRP:Postcode         = job:postcode
                    INVGRP:Company_Name     = job:company_name
                    INVGRP:Address_Line1    = job:address_line1
                    INVGRP:Address_Line2    = job:address_line2
                    INVGRP:Address_Line3    = job:address_line3
                    INVGRP:Telephone_Number = job:telephone_number
                    INVGRP:Fax_Number       = job:fax_number
                    UNIGRP:Model_Number     = job:Model_Number
                    UNIGRP:Manufacturer     = job:manufacturer
                    UNIGRP:ESN              = job:esn
                    UNIGRP:MSN              = job:msn
                    despatch_Type_temp  = 'JOB'
                End!If JOB:Incoming_Consignment_Number = glo:select2
                If JOB:Exchange_Consignment_Number = glo:select2
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number = job:exchange_unit_number
                    if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                        UNIGRP:Model_Number = XCH:Model_Number
                        UNIGRP:Manufacturer = xch:manufacturer
                        UNIGRP:ESN          = xch:esn
                        UNIGRP:MSN          = xch:msn
                    end
                    despatch_Type_temp  = 'EXC'
                End!If JOB:Exchange_Consignment_Number = glo:select2
                If JOB:Loan_Consignment_Number = glo:select2
                    access:loan.clearkey(loa:ref_number_key)
                    loa:ref_number = job:loan_unit_number
                    if access:loan.fetch(loa:ref_number_key) = Level:Benign
                        UNIGRP:Model_Number = loa:Model_Number
                        UNIGRP:Manufacturer = loa:manufacturer
                        UNIGRP:ESN          = loa:esn
                        UNIGRP:MSN          = loa:msn
                    end
                    despatch_Type_temp  = 'LOA'
                End!If JOB:Exchange_Consignment_Number = glo:select2
                Print(Rpt:detail)
            end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        
        End!Loop x# = 1 To Records(print_group)
        count_temp = Records(glo:q_jobnumber)
        Print(rpt:totals)
        
        setcursor()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(SUBTRACC,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','DespatchNoteMultiple')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','DESPATCH NOTE - MULTIPLE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'DESPATCH NOTE - MULTIPLE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !printed by
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  !Hide Despatch Address
  If HideDespAdd# = 1
      Settarget(Report)
      ?def:User_Name{prop:Hide} = 1
      ?def:Address_Line1{prop:Hide} = 1
      ?def:Address_Line2{prop:Hide} = 1
      ?def:Address_Line3{prop:Hide} = 1
      ?def:Postcode{prop:hide} = 1
      ?tmp:DefaultTelephone{prop:hide} = 1
      ?tmp:DefaultFax{prop:hide} = 1
      ?telephone{prop:hide} = 1
      ?fax{prop:hide} = 1
      ?email{prop:hide} = 1
      ?tmp:DefaultEmail{prop:Hide} = 1
      Settarget()
  End!If HideDespAdd# = 1
  !--- Webify SETUP ClarioNET:UseReportPreview(0) tmp:BranchIdentification ---------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      access:tradeacc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = ClarioNET:Global.Param2
      IF Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
          tmp:BranchIdentification = tra:BranchIdentification
      ELSE
          tmp:BranchIdentification = ''
      END !IF
  
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  
  END
  !---------------------------------------------------------------------------------------
  HideDespAdd# = 0
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = glo:select1
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          if tra:invoice_sub_accounts = 'YES'
              If sub:HideDespAdd = 1
                  HideDespAdd# = 1
              End!If sub:HideDespAdd = 1
          Else
              If tra:HideDespAdd = 1
                  HideDespAdd# = 1
              End!If sub:HideDespAdd = 1
          End
          if tra:use_sub_accounts = 'YES'
              DELGRP:Postcode           = sub:postcode
              DELGRP:Company_Name       = sub:company_name
              DELGRP:Address_Line1      = sub:address_line1
              DELGRP:Address_Line2      = sub:address_line2
              DELGRP:Address_Line3      = sub:address_line3
              DELGRP:Telephone_Number   = sub:telephone_number
              DELGRP:Fax_Number         = sub:fax_number
              If tra:invoice_sub_accounts = 'YES'
                  INVGRP:Postcode           = sub:Postcode
                  INVGRP:Company_Name       = sub:Company_Name
                  INVGRP:Address_Line1      = sub:Address_Line1
                  INVGRP:Address_Line2      = sub:Address_Line2
                  INVGRP:Address_Line3      = sub:Address_Line3
                  INVGRP:Telephone_Number   = sub:Telephone_Number
                  INVGRP:Fax_Number         = sub:Fax_Number
              Else
                  INVGRP:Postcode           = tra:Postcode
                  INVGRP:Company_Name       = tra:Company_Name
                  INVGRP:Address_Line1      = tra:Address_Line1
                  INVGRP:Address_Line2      = tra:Address_Line2
                  INVGRP:Address_Line3      = tra:Address_Line3
                  INVGRP:Telephone_Number   = tra:Telephone_Number
                  INVGRP:Fax_Number         = tra:Fax_Number
              End!If tra:invoice_sub_accounts = 'YES'
          else!if tra:use_sub_accounts = 'YES'
              INVGRP:Postcode           = tra:Postcode
              INVGRP:Company_Name       = tra:Company_Name
              INVGRP:Address_Line1      = tra:Address_Line1
              INVGRP:Address_Line2      = tra:Address_Line2
              INVGRP:Address_Line3      = tra:Address_Line3
              INVGRP:Telephone_Number   = tra:Telephone_Number
              INVGRP:Fax_Number         = tra:Fax_Number
              DELGRP:Postcode           = tra:postcode
              DELGRP:Company_Name       = tra:company_name
              DELGRP:Address_Line1      = tra:address_line1
              DELGRP:Address_Line2      = tra:address_line2
              DELGRP:Address_Line3      = tra:address_line3
              DELGRP:Telephone_Number   = tra:telephone_number
              DELGRP:Fax_Number         = tra:fax_number
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE'
  access:stantext.fetch(stt:description_key)
  
  !Hide Despatch Address
  If HideDespAdd# = 1
      Settarget(Report)
      ?def:User_Name{prop:Hide} = 1
      ?def:Address_Line1{prop:Hide} = 1
      ?def:Address_Line2{prop:Hide} = 1
      ?def:Address_Line3{prop:Hide} = 1
      ?def:Postcode{prop:hide} = 1
      ?tmp:DefaultTelephone{prop:hide} = 1
      ?tmp:DefaultFax{prop:hide} = 1
      ?telephone{prop:hide} = 1
      ?fax{prop:hide} = 1
      ?email{prop:hide} = 1
      ?tmp:DefaultEmail{prop:Hide} = 1
      Settarget()
  End!If HideDespAdd# = 1
  
  
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE - MULTIPLE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'DESPATCH NOTE - MULTIPLE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  !Alternative Contact Numbers
  Case UseAlternativeContactNos(glo:Select1,0)
      Of 1
          tmp:DefaultTelephone    = tra:AltTelephoneNumber
          tmp:DefaultFax          = tra:AltFaxNumber
          tmp:DefaultEmail        = tra:AltEmailAddress
      Of 2
          tmp:DefaultTelephone    = sub:AltTelephoneNumber
          tmp:DefaultFax          = sub:AltFaxNumber
          tmp:DefaultEmail        = sub:AltEmailAddress
      Else
          tmp:DefaultEmail        = def:EmailAddress
  End !UseAlternativeContactNos(glo:Select1)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='DespatchNoteMultiple'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













SingleSummaryDespatchNote PROCEDURE(func:JobNumber,func:DespatchType)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
save_jea_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
save_jpt_id          USHORT,AUTO
print_group          QUEUE,PRE(prngrp)
esn                  STRING(30)
msn                  STRING(30)
job_number           REAL
                     END
Invoice_Address_Group GROUP,PRE(INVGRP)
Postcode             STRING(15)
Company_Name         STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Delivery_Address_Group GROUP,PRE(DELGRP)
Postcode             STRING(15)
Company_Name         STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Unit_Details_Group   GROUP,PRE(UNIGRP)
Model_Number         STRING(30)
Manufacturer         STRING(30)
ESN                  STRING(16)
MSN                  STRING(16)
CRepairType          STRING(30)
WRepairType          STRING(30)
                     END
Unit_Ref_Number_Temp REAL
Despatch_Type_Temp   STRING(3)
Invoice_Account_Number_Temp STRING(15)
Delivery_Account_Number_Temp STRING(15)
count_temp           REAL
model_number_temp    STRING(50)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:DefaultEmail     STRING(255)
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
BranchIdentification LIKE(tra:BranchIdentification)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Order_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,3865,7521,6490),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,823,7521,2542),USE(?unnamed)
                         STRING('Courier:'),AT(4896,104),USE(?String23),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,104),USE(GLO:Select4),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Consignment No:'),AT(4896,260),USE(?String27),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,260),USE(GLO:Select2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Despatch Date:'),AT(4896,417),USE(?String27:2),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6042,417),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatch Batch No:'),AT(4896,729,990,208),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,729),USE(GLO:Select3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4896,885),USE(?String62),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@n3),AT(6042,885),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(4073,1354),USE(INVGRP:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1510),USE(INVGRP:Address_Line1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Account No:'),AT(208,2292),USE(?String38),TRN,FONT(,8,,)
                         STRING(@s15),AT(885,2292),USE(GLO:Select1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(4073,2135),USE(?String36:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Fax:'),AT(5521,2135),USE(?String37:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4375,2135),USE(INVGRP:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4688,2292),USE(GLO:Select1,,?glo:select1:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Account No:'),AT(4073,2292),USE(?String38:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,2135),USE(INVGRP:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1667),USE(INVGRP:Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1823),USE(INVGRP:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2135),USE(?String36),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Fax:'),AT(1719,2135),USE(?String37),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4073,1979),USE(INVGRP:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,146),USE(?detailband)
                           STRING(@s30),AT(156,0),USE(prngrp:esn),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(3021,0),USE(UNIGRP:CRepairType),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(1042,0),USE(prngrp:msn),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s3),AT(6042,0),USE(func:DespatchType),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s25),AT(6292,0,1250,156),USE(job:Order_Number),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s20),AT(4115,0),USE(UNIGRP:WRepairType),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(1927,0),USE(UNIGRP:Model_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s15),AT(5208,0),USE(tmp:Ref_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(,,,354),USE(?Totals)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,104,677,156),USE(?String59),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(1042,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(365,10344,7521,854),USE(?unnamed:2)
                         LINE,AT(208,52,7135,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(208,104,7135,677),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(385,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('MULTIPLE DESPATCH NOTE'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3542,208),USE(tmp:DefaultEmail),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?telephone),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(156,1198),USE(?fax),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('INVOICE ADDRESS'),AT(208,1510),USE(?String29),TRN,FONT(,9,,FONT:bold)
                         STRING('Email:'),AT(156,1354),USE(?Email),TRN,FONT(,9,,)
                         STRING('DELIVERY ADDRESS'),AT(4063,1510),USE(?String29:2),TRN,FONT(,9,,FONT:bold)
                         STRING('Model Number'),AT(1938,3177),USE(?string44),TRN,FONT(,7,,FONT:bold)
                         STRING('I.M.E.I. Number.'),AT(156,3177),USE(?string25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('M.S.N.'),AT(1052,3177),USE(?string25:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Type'),AT(6052,3177),USE(?string25:3),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Order Number'),AT(6302,3177),USE(?string25:5),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('War Repair Type'),AT(4125,3177),USE(?string44:3),TRN,FONT(,7,,FONT:bold)
                         STRING('Char Repair Type'),AT(3031,3177),USE(?string44:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Job No'),AT(5219,3177),USE(?string25:4),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('SingleSummaryDespatchNote')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','SingleSummaryDespatchNote')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:LOAN.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      recordstoprocess    = Records(glo:q_jobnumber) * 2
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        access:jobs.clearkey(job:ref_number_key)
        job:ref_number = func:JobNumber
        if access:jobs.fetch(job:ref_number_key) = Level:Benign
        
            If job:Chargeable_Job = 'YES'
                unigrp:CRepairType = job:Repair_Type
            Else !If job:Chargeable_Job = 'YES'
                unigrp:CRepairType  = ''
            End !If job:Chargeable_Job = 'YES'
        
            If job:Warranty_job = 'YES'
                unigrp:WRepairType = job:Repair_Type_Warranty
            Else !If job:Warranty_job = 'YES'
                unigrp:WRepairType = ''
            End !If job:Warranty_job = 'YES'
            !--------------------------------------------------------------
            !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
            ! 26 Aug 2002 John
            !
            tmp:Ref_number = job:Ref_number
            if glo:WebJob then
                !Change the tmp ref number if you
                !can Look up from the wob file
                access:Webjob.clearkey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_number
                if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                    tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
                END
            END
            !--------------------------------------------------------------
            Case func:DespatchType
                Of 'JOB'
                    INVGRP:Postcode         = job:postcode
                    INVGRP:Company_Name     = job:company_name
                    INVGRP:Address_Line1    = job:address_line1
                    INVGRP:Address_Line2    = job:address_line2
                    INVGRP:Address_Line3    = job:address_line3
                    INVGRP:Telephone_Number = job:telephone_number
                    INVGRP:Fax_Number       = job:fax_number
                    UNIGRP:Model_Number     = job:Model_Number
                    UNIGRP:Manufacturer     = job:manufacturer
                    UNIGRP:ESN              = job:esn
                    UNIGRP:MSN              = job:msn
                Of 'EXC'
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number = job:exchange_unit_number
                    if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                        UNIGRP:Model_Number = XCH:Model_Number
                        UNIGRP:Manufacturer = xch:manufacturer
                        UNIGRP:ESN          = xch:esn
                        UNIGRP:MSN          = xch:msn
                    end
                Of 'LOA'
                    access:loan.clearkey(loa:ref_number_key)
                    loa:ref_number = job:loan_unit_number
                    if access:loan.fetch(loa:ref_number_key) = Level:Benign
                        UNIGRP:Model_Number = loa:Model_Number
                        UNIGRP:Manufacturer = loa:manufacturer
                        UNIGRP:ESN          = loa:esn
                        UNIGRP:MSN          = loa:msn
                    end
            End !Case func:DespatchType
            Print(Rpt:detail)
        end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        Print(rpt:totals)
        
        setcursor()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','SingleSummaryDespatchNote')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE - MULTIPLE'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','DESPATCH NOTE - MULTIPLE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'DESPATCH NOTE - MULTIPLE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !--- Webify SETUP ClarioNET:UseReportPreview(0) tmp:BranchIdentification ---------------
  ! 26 Aug 2002 John
  !
  if glo:WebJob then
      access:tradeacc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = ClarioNET:Global.Param2
      IF Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
          tmp:BranchIdentification = tra:BranchIdentification
      ELSE
          tmp:BranchIdentification = '=='
      END !IF
  
      !Set up no preview on client
      ClarioNET:UseReportPreview(0)
  
  END
  !---------------------------------------------------------------------------------------
  !printed by
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:password_key)
      use:password = glo:password
  access:users.fetch(use:password_key)
  
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  !Hide Despatch Address
  If HideDespAdd# = 1
      Settarget(Report)
      ?def:User_Name{prop:Hide} = 1
      ?def:Address_Line1{prop:Hide} = 1
      ?def:Address_Line2{prop:Hide} = 1
      ?def:Address_Line3{prop:Hide} = 1
      ?def:Postcode{prop:hide} = 1
      ?tmp:DefaultTelephone{prop:hide} = 1
      ?tmp:DefaultFax{prop:hide} = 1
      ?telephone{prop:hide} = 1
      ?fax{prop:hide} = 1
      ?email{prop:hide} = 1
      ?tmp:DefaultEmail{prop:Hide} = 1
      Settarget()
  End!If HideDespAdd# = 1
  HideDespAdd# = 0
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = glo:select1
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          if tra:invoice_sub_accounts = 'YES'
              If sub:HideDespAdd = 1
                  HideDespAdd# = 1
              End!If sub:HideDespAdd = 1
          Else
              If tra:HideDespAdd = 1
                  HideDespAdd# = 1
              End!If sub:HideDespAdd = 1
          End
          if tra:use_sub_accounts = 'YES'
              DELGRP:Postcode           = sub:postcode
              DELGRP:Company_Name       = sub:company_name
              DELGRP:Address_Line1      = sub:address_line1
              DELGRP:Address_Line2      = sub:address_line2
              DELGRP:Address_Line3      = sub:address_line3
              DELGRP:Telephone_Number   = sub:telephone_number
              DELGRP:Fax_Number         = sub:fax_number
              If tra:invoice_sub_accounts = 'YES'
                  INVGRP:Postcode           = sub:Postcode
                  INVGRP:Company_Name       = sub:Company_Name
                  INVGRP:Address_Line1      = sub:Address_Line1
                  INVGRP:Address_Line2      = sub:Address_Line2
                  INVGRP:Address_Line3      = sub:Address_Line3
                  INVGRP:Telephone_Number   = sub:Telephone_Number
                  INVGRP:Fax_Number         = sub:Fax_Number
              Else
                  INVGRP:Postcode           = tra:Postcode
                  INVGRP:Company_Name       = tra:Company_Name
                  INVGRP:Address_Line1      = tra:Address_Line1
                  INVGRP:Address_Line2      = tra:Address_Line2
                  INVGRP:Address_Line3      = tra:Address_Line3
                  INVGRP:Telephone_Number   = tra:Telephone_Number
                  INVGRP:Fax_Number         = tra:Fax_Number
              End!If tra:invoice_sub_accounts = 'YES'
          else!if tra:use_sub_accounts = 'YES'
              INVGRP:Postcode           = tra:Postcode
              INVGRP:Company_Name       = tra:Company_Name
              INVGRP:Address_Line1      = tra:Address_Line1
              INVGRP:Address_Line2      = tra:Address_Line2
              INVGRP:Address_Line3      = tra:Address_Line3
              INVGRP:Telephone_Number   = tra:Telephone_Number
              INVGRP:Fax_Number         = tra:Fax_Number
              DELGRP:Postcode           = tra:postcode
              DELGRP:Company_Name       = tra:company_name
              DELGRP:Address_Line1      = tra:address_line1
              DELGRP:Address_Line2      = tra:address_line2
              DELGRP:Address_Line3      = tra:address_line3
              DELGRP:Telephone_Number   = tra:telephone_number
              DELGRP:Fax_Number         = tra:fax_number
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE'
  access:stantext.fetch(stt:description_key)
  
  !Hide Despatch Address
  If HideDespAdd# = 1
      Settarget(Report)
      ?def:User_Name{prop:Hide} = 1
      ?def:Address_Line1{prop:Hide} = 1
      ?def:Address_Line2{prop:Hide} = 1
      ?def:Address_Line3{prop:Hide} = 1
      ?def:Postcode{prop:hide} = 1
      ?tmp:DefaultTelephone{prop:hide} = 1
      ?tmp:DefaultFax{prop:hide} = 1
      ?telephone{prop:hide} = 1
      ?fax{prop:hide} = 1
      ?email{prop:hide} = 1
      ?tmp:DefaultEmail{prop:Hide} = 1
      Settarget()
  End!If HideDespAdd# = 1
  
  
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE - MULTIPLE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'DESPATCH NOTE - MULTIPLE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  !Alternative Contact Numbers
  Case UseAlternativeContactNos(glo:Select1,0)
      Of 1
          tmp:DefaultTelephone    = tra:AltTelephoneNumber
          tmp:DefaultFax          = tra:AltFaxNumber
          tmp:DefaultEmail        = tra:AltEmailAddress
      Of 2
          tmp:DefaultTelephone    = sub:AltTelephoneNumber
          tmp:DefaultFax          = sub:AltFaxNumber
          tmp:DefaultEmail        = sub:AltEmailAddress
      Else
          tmp:DefaultEmail        = def:EmailAddress
  End !UseAlternativeContactNos(glo:Select1)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='SingleSummaryDespatchNote'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













WayBillDespatch PROCEDURE(func:FromAccountNumber,func:FromType,func:ToAccountNumber,func:ToType,func:ConsignmentNumber,func:Courier)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
DespatchType         STRING(3)
save_jea_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_xca_id          USHORT,AUTO
save_waj_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
save_jpt_id          USHORT,AUTO
print_group          QUEUE,PRE(prngrp)
esn                  STRING(30)
msn                  STRING(30)
job_number           REAL
                     END
Unit_Ref_Number_Temp REAL
Despatch_Type_Temp   STRING(3)
Invoice_Account_Number_Temp STRING(15)
Delivery_Account_Number_Temp STRING(15)
count_temp           REAL
model_number_temp    STRING(50)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:DefaultEmailAddress STRING(255)
tmp:Courier          STRING(30)
tmp:ConsignmentNumber STRING(30)
tmp:DespatchBatchNumber STRING(30)
tmp:IMEI             STRING(30)
tmp:Accessories      STRING(255)
FromAddressGroup     GROUP,PRE(from)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
TelephoneNumber      STRING(30)
ContactName          STRING(60)
EmailAddress         STRING(255)
                     END
ToAddressGroup       GROUP,PRE(to)
AccountNumber        STRING(30)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
TelephoneNumber      STRING(30)
ContactName          STRING(30)
EmailAddress         STRING(255)
                     END
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(31)
Bar_Code_Temp        CSTRING(31)
Webmaster_Group      GROUP,PRE(tmp)
Ref_Number           STRING(20)
BranchIdentification LIKE(tra:BranchIdentification)
                     END
tmp:Comments         STRING(255)
tmp:SecurityPackNumber STRING(30)
tmp:CustomerAddress  STRING(3)
tmp:WayBillID        LONG
tmp:Exchanged        STRING(1)
tmp:DateDespatched   DATE
tmp:TimeDespatched   TIME
tmp:HeadAccountNo    STRING(15)
locUseOldProcedure   BYTE(0)
!-----------------------------------------------------------------------------
Process:View         VIEW(SUBTRACC)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(448,4063,7771,5406),PAPER(PAPER:LETTER),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,469,7802,3219),USE(?unnamed)
                         STRING('Courier:'),AT(4896,781),USE(?String23),TRN,FONT(,8,,)
                         STRING(@s30),AT(6042,781),USE(tmp:Courier),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Despatch Date:'),AT(4896,365),USE(?DespatchDate),TRN,FONT(,8,,)
                         STRING(@d6),AT(6042,365),USE(tmp:DateDespatched),TRN,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6042,573),USE(tmp:TimeDespatched),TRN,FONT(,8,,FONT:bold)
                         STRING('From Sender:'),AT(104,104),USE(?String16),TRN,FONT(,8,,)
                         STRING('WAYBILL REJECTION'),AT(5365,52),USE(?WaybillRejection),TRN,HIDE,FONT(,16,,FONT:bold)
                         STRING('Deliver To:'),AT(104,1667),USE(?String16:5),TRN,FONT(,8,,)
                         STRING('Tel No:'),AT(104,1042),USE(?String16:2),TRN,FONT(,8,,)
                         STRING('?PP?'),AT(6563,990,375,208),USE(?CPCSPgOfPgStr),FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6354,990),USE(?String48),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(4896,990),USE(?String62),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@n3),AT(6042,990),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(938,1042),USE(from:TelephoneNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name:'),AT(104,2813),USE(?String16:7),TRN,FONT(,8,,)
                         STRING(@s60),AT(938,1198),USE(from:ContactName),TRN,FONT(,8,,FONT:bold)
                         STRING(@s255),AT(938,1354),USE(from:EmailAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('Email:'),AT(104,1354),USE(?String16:4),TRN,FONT(,8,,)
                         STRING('WAYBILL NUMBER'),AT(3490,1667,3906,260),USE(?String39),TRN,CENTER,FONT(,12,,FONT:bold)
                         STRING('Contact Name:'),AT(104,1198),USE(?String16:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,260),USE(from:CompanyName),TRN,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(3490,1979,3906,260),USE(Bar_Code_Temp),CENTER,FONT('C39 High 12pt LJ3',12,,,CHARSET:ANSI),COLOR(COLOR:White)
                         STRING(@s30),AT(104,469),USE(from:AddressLine1),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,625),USE(from:AddressLine2),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number:'),AT(104,1875),USE(?AccountNumber),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,1875,1927,188),USE(to:AccountNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Company Name:'),AT(104,2031),USE(?CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2031),USE(to:CompanyName),TRN,FONT(,8,,FONT:bold)
                         STRING('Address 1:'),AT(104,2188),USE(?Address1),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2188),USE(to:AddressLine1),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(52,2031,3177,156),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Address 2:'),AT(104,2344),USE(?Address2),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2344),USE(to:AddressLine2),TRN,FONT(,8,,FONT:bold)
                         STRING('Suburb:'),AT(104,2500),USE(?Suburb),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2500),USE(to:AddressLine3),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name:'),AT(104,2813),USE(?ContactName),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2813),USE(to:ContactName),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(990,1875,0,1250),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2969,3177,156),USE(?Box1:8),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2656,3177,156),USE(?Box1:6),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Contact Number:'),AT(104,2656),USE(?ContactNumber),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2656),USE(to:TelephoneNumber),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(52,2813,3177,156),USE(?Box1:7),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2188,3177,156),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Email:'),AT(104,2969),USE(?Email),TRN,FONT(,8,,)
                         STRING(@s255),AT(1042,2969),USE(to:EmailAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatch Time:'),AT(4896,573),USE(?DespatchTime),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,781),USE(from:AddressLine3),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(3490,2292,3906,260),USE(tmp:ConsignmentNumber),TRN,CENTER,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                         STRING(@s255),AT(4323,2969,3385,208),USE(glo:ErrorText),TRN,RIGHT,FONT(,8,,FONT:bold)
                         BOX,AT(52,2500,3177,156),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2344,3177,156),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,1875,3177,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,240),USE(?detailband)
                           STRING(@s20),AT(104,0),USE(tmp:Ref_Number,,?job:Ref_Number:2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s18),AT(1370,0),USE(tmp:IMEI),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(5417,0,1875,156),USE(tmp:Accessories),TRN,FONT(,7,,),RESIZE
                           STRING(@s1),AT(7396,0),USE(tmp:Exchanged),TRN,FONT(,8,,)
                           STRING(@s15),AT(4375,0),USE(tmp:SecurityPackNumber),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s30),AT(2552,0,1771,156),USE(job:Order_Number),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(,,,385),USE(?Totals)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number of Items On Waybill :'),AT(260,104),USE(?String59),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(2344,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
SundryDetail             DETAIL,AT(,,,219),USE(?SundryDetail)
                           STRING(@s30),AT(146,0),USE(was:Description),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(2344,0),USE(was:Quantity),TRN,FONT(,8,,)
                         END
CNRDetail                DETAIL,AT(,,,219),USE(?CNRDetail)
                           STRING(@s30),AT(146,0),USE(wcr:PartNumber),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(2344,0),USE(wcr:Description),TRN,FONT(,8,,)
                           STRING(@n_8),AT(4427,0),USE(wcr:Quantity),TRN,FONT(,8,,)
                         END
SundryNotes              DETAIL,AT(,,,1188),USE(?SundryNotes)
                           TEXT,AT(156,52,3594,729),USE(way:UserNotes),TRN,FONT(,8,,),RESIZE
                           STRING(@s30),AT(3844,52),USE(way:SecurityPackNumber),TRN,LEFT,FONT(,8,,)
                           LINE,AT(104,833,7292,0),USE(?Line6:3),COLOR(COLOR:Black)
                           STRING('Description'),AT(146,885),USE(?Description),TRN,FONT(,7,,)
                           STRING('Quantity'),AT(2344,885),USE(?Quantity),TRN,FONT(,7,,)
                           LINE,AT(104,1094,7292,0),USE(?Line6:4),COLOR(COLOR:Black)
                         END
                       END
                       FOOTER,AT(385,9479,7781,1229),USE(?unnamed:2)
                         LINE,AT(208,52,7135,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(208,104,7135,1042),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(385,448,7792,10521),USE(?unnamed:3)
                         STRING('Part Number'),AT(208,3333),USE(?crnPartNumber),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(2406,3333),USE(?crnDescription),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Job No'),AT(156,3333,,156),USE(?JobNo),TRN,FONT(,7,,)
                         STRING('I.M.E.I. No'),AT(1370,3333),USE(?IMEINo),TRN,FONT(,7,,)
                         STRING('Order No'),AT(2615,3333),USE(?OrderNo),TRN,FONT(,7,,)
                         STRING('Accessories'),AT(5479,3333),USE(?Accessories),TRN,FONT(,7,,)
                         STRING('Quantity'),AT(4385,3333),USE(?crnQuantity),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Security Pack No'),AT(4438,3333),USE(?SecurityPackNo),TRN,FONT(,7,,)
                         LINE,AT(104,3281,7292,0),USE(?Line6),COLOR(COLOR:Black)
                         LINE,AT(104,3542,7292,0),USE(?Line6:2),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('WayBillDespatch')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','WayBillDespatch')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'Waybill'
  If ClarioNETServer:Active() = True
      If PrintOption(PreviewReq,glo:ExportReport,'Waybill') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  End ! If ClarioNETServer:Active() = True
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:SUBTRACC.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHACC.Open
  Relate:STANTEXT.Open
  Relate:WAYBILLJ.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:EXCHANGE.UseFile
  Access:LOAN.UseFile
  Access:USERS.UseFile
  Access:JOBSE.UseFile
  Access:WAYBILLS.UseFile
  Access:JOBACC.UseFile
  Access:WAYSUND.UseFile
  Access:WAYCNR.UseFile
  
  
  RecordsToProcess = RECORDS(SUBTRACC)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(SUBTRACC,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      recordstoprocess    = Records(glo:q_jobnumber)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        CASE way:WaybillID !============================================
        OF 400 OROF 401 OROF 402! Credit Note Request
            SetTarget(Report)
            ?JobNo{prop:Hide} = 1
            ?IMEINo{prop:Hide} = 1
            ?OrderNo{prop:Hide} = 1
            ?Accessories{prop:Hide} = 1
            ?SecurityPackNo{prop:Hide} = 1
            ?crnPartNumber{prop:Hide} = 0
            ?crnDescription{prop:Hide} = 0
            ?crnQuantity{prop:Hide} = 0
            tmp:DateDespatched = way:TheDate
            tmp:TimeDespatched = way:TheTime
            tmp:Courier        = way:Courier
            IF (way:WaybillID = 401 OR way:WaybillID = 402)
                ?crnPartNumber{prop:Text} = 'Model Number'
                ?crnDescription{prop:Text} = 'IMEI Number'
                ?crnQuantity{prop:Hide} = 1
                ?wcr:Quantity{prop:Hide} = 1
            END
            SetTarget()
        
            first# = 1
            Access:WAYCNR.Clearkey(wcr:EnteredKey)
            wcr:WAYBILLSRecordNumber = way:RecordNumber
            Set(wcr:EnteredKey,wcr:EnteredKey)
            Loop ! Begin Loop
                If Access:WAYCNR.Next()
                    Break
                End ! If Access:WAYSUND.Next()
                If wcr:WAYBILLSRecordNumber <> way:RecordNumber
                    Break
                End ! If was:WAYBILLSRecordNumber <> way:RecordNumber
                Print(rpt:CNRDetail)
                count_temp += 1
            End ! Loop
        OF 300 !Sunry Waybill
            SetTarget(Report)
            ?JobNo{prop:Text} = 'User Notes'
            ?IMEINo{prop:Hide} = True
            ?OrderNo{prop:Hide} = True
            ?Accessories{prop:Hide} = True
            tmp:DateDespatched = way:TheDate
            tmp:TimeDespatched = way:TheTime
            tmp:Courier        = way:Courier
            SetTarget()
            Print(rpt:SundryNotes)
        
            Access:WAYSUND.Clearkey(was:EnteredKey)
            was:WAYBILLSRecordNumber = way:RecordNumber
            Set(was:EnteredKey,was:EnteredKey)
            Loop ! Begin Loop
                If Access:WAYSUND.Next()
                    Break
                End ! If Access:WAYSUND.Next()
                If was:WAYBILLSRecordNumber <> way:RecordNumber
                    Break
                End ! If was:WAYBILLSRecordNumber <> way:RecordNumber
                Print(rpt:SundryDetail)
            End ! Loop
        
        ELSE
            !New WayBill Procedure
            locUseOldProcedure = 1
            Save_waj_ID = Access:WAYBILLJ.SaveFile()
            Access:WAYBILLJ.ClearKey(waj:JobNumberKey)
            waj:WayBillNumber = func:ConsignmentNumber
            Set(waj:JobNumberKey,waj:JobNumberKey)
            Loop
                If Access:WAYBILLJ.NEXT()
                   Break
                End !If
                If waj:WayBillNumber <> func:ConsignmentNumber      |
                    Then Break.  ! End If
                !Write the Date Despatched on the report - L879 (DBH: 17-07-2003)
        !                Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        !                way:WayBillNumber = func:ConsignmentNumber
        !                If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
                    !Found
                    tmp:DateDespatched  = way:TheDate
                    tmp:TimeDespatched  = way:TheTime
        !                Else !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
        !                    !Error
        !                End !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
        
                tmp:Exchanged = ''
                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_Number  = waj:JobNumber
                If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Found
                    Access:WEBJOB.Clearkey(wob:RefNumberKey)
                    wob:RefNumber   = job:Ref_Number
                    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        !Found
        
                    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = wob:HeadAccountNumber
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Found
        
                    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Error
                    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
                    tmp:Ref_Number  = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
        
                    ! -----------------------------------------------------------------------------
                    ! VP115 / VP120  21st NOV 02 - must display correct IMEI number
                    case waj:WayBillNumber
                        of job:Exchange_Consignment_Number
                            ! Exchange waybill
                            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                            xch:Ref_Number  = job:Exchange_Unit_Number
                            if Access:EXCHANGE.Fetch(xch:Ref_Number_Key)
                                tmp:IMEI = waj:IMEINumber
                            else
                                ! Display exchange unit IMEI number
                                tmp:IMEI = xch:ESN
                            end
                        else
                            tmp:IMEI = waj:IMEINumber
                            If job:Exchange_Unit_Number <> 0
                                tmp:Exchanged = 'E'
                            End !If job:Exchange_Unit_Number <> 0
                    end
                    ! -----------------------------------------------------------------------------
        
                    !tmp:IMEI        = waj:IMEINumber
                    tmp:SecurityPackNumber  = waj:SecurityPackNumber
                    tmp:Accessories = ''
                    !Only print accessories, if it's a job, and there is no accessories attached
                    !Or it's an exchange unit being despatched
        
                    If job:Exchange_Unit_Number = 0 Or way:WaybillID = 3 Or way:WaybillID = 6 Or way:WayBillID = 8 Or |
                        way:WaybillID = 10 Or way:WaybillID = 11 Or way:WaybillID = 12
                        Save_jac_ID = Access:JOBACC.SaveFile()
                        Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                        jac:Ref_Number = job:Ref_Number
                        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                        Loop
                            If Access:JOBACC.NEXT()
                               Break
                            End !If
                            If jac:Ref_Number <> job:Ref_Number      |
                                Then Break.  ! End If
                            ! Inserting (DBH 08/03/2007) # 8703 - Show all accessories if the isn't a 0 (to ARC) or 1 (to RRC)
                            If way:WaybillType = 0 Or way:WaybillType = 1
                            ! End (DBH 08/03/2007) #8703
                                !Only show accessories that were sent to ARC - 4285 (DBH: 26-05-2004)
                                If jac:Attached <> True
                                    Cycle
                                End !If jac:Attached <> True
                            End ! If way:WaybillType <> 0 And way:WaybillType <> 1
        
                            If tmp:Accessories = ''
                                tmp:Accessories = Clip(jac:Accessory)
                            Else !If tmp:Accessories = ''
                                tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(jac:Accessory)
                            End !If tmp:Accessories = ''
                        End !Loop
                        Access:JOBACC.RestoreFile(Save_jac_ID)
        
                    End !If waj:JobType = 'JOB' And job:Exchange_Unit_Number = 0
                    !added by Paul 26/04/2010 - log no 10546
                    If clip(glo:Notes_Global) = 'OBF' then
                        !need to check if the new OBF address is filled in
                        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                        tra:Account_Number  = func:ToAccountNumber
                        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                            If clip(tra:OBFAddress1) <> '' then
                                !address is filled in
                                SetTarget(Report)
                                to:CompanyName      = clip(tra:OBFCompanyName)
                                to:AddressLine1     = clip(tra:OBFAddress1)
                                to:AddressLine2     = clip(tra:OBFAddress2)
                                to:AddressLine3     = clip(tra:OBFSuburb)
                                to:TelephoneNumber  = clip(tra:OBFContactNumber)
                                to:ContactName      = clip(tra:OBFContactName)
                                to:EmailAddress     = clip(tra:OBFEmailAddress)
                                Settarget()
                            End
                        End
                    End
                    Print(rpt:Detail)
                    locUseOldProcedure = 0
                Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            End !Loop
            Access:WAYBILLJ.RestoreFile(Save_waj_ID)
        END   !============================================
            If locUseOldProcedure = 1
            ! Don't really know the despatch date with any accuracy.
            ! So will show date printed on the report instead - L879 (DBH: 17-07-2003)
                tmp:DateDespatched = Today()
                tmp:TimeDespatched = Clock()
                SetTarget(Report)
                ?DespatchDate{prop:Text} = 'Date Printed:'
                ?DespatchTime{prop:Text} = 'Time Printed:'
                SetTarget()
        
                Free(print_group)
                Clear(print_group)
                setcursor(cursor:wait)
        
                If tmp:WaybillID <> 0
                    Loop x# = 1 To Records(glo:q_JobNumber)
                        Get(glo:Q_JobNumber, x#)
                        RecordsProcessed += 1
                        Do DisplayProgress
        
                        Access:JOBS.Clearkey(job:Ref_Number_Key)
                        job:Ref_Number  = glo:Job_Number_Pointer
                        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            ! Found
                            Access:JOBSE.Clearkey(jobe:RefNumberKey)
                            jobe:RefNumber  = job:Ref_Number
                            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            ! Found
        
                            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            ! Error
                            End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
                            Access:WEBJOB.Clearkey(wob:RefNumberKey)
                            wob:RefNumber   = job:Ref_Number
                            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            ! Found
        
                            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            ! Error
                            End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
                            tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
        
                            ! What is being despatched?
                            job# = 0
                            exc# = 0
                            loa# = 0
        
                            Case func:ConsignmentNumber
                            Of job:Incoming_Consignment_Number
                                job#                   = 1
                                tmp:SecurityPackNumber = jobe:InSecurityPackNo
                            Of job:Exchange_Consignment_Number
                                exc#                   = 1
                                tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                            Of job:Loan_Consignment_Number
                                loa#                   = 1
                                tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                            Of job:Consignment_Number
                                job#                   = 1
                                tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                            Of wob:ExcWaybillNumber
                                exc#                   = 1
                                tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                            Of wob:LoaWayBillNumber
                                loa#                   = 1
                                tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                            Of wob:JobWayBillNumber
                                job#                   = 1
                                tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                            End ! Case func:ConsignmentNumber
        
                            If job#
                                tmp:Accessories = ''
                                Save_jac_ID     = Access:JOBACC.SaveFile()
                                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                                jac:Ref_Number = job:Ref_Number
                                Set(jac:Ref_Number_Key, jac:Ref_Number_Key)
                                Loop
                                    If Access:JOBACC.NEXT()
                                        Break
                                    End ! If
                                    If jac:Ref_Number <> job:Ref_Number      |
                                        Then Break   ! End If
                                    End ! If
                                    If tmp:Accessories = ''
                                        tmp:Accessories = Clip(jac:Accessory)
                                    Else ! If tmp:Accessories = ''
                                        tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(jac:Accessory)
                                    End ! If tmp:Accessories = ''
                                End ! Loop
                                Access:JOBACC.RestoreFile(Save_jac_ID)
                                tmp:IMEI    = job:ESN
        
                            End ! If job#
        
                            If exc#
                                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                xch:Ref_Number  = job:Exchange_Unit_Number
                                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                    ! Found
                                    Save_xca_ID = Access:EXCHACC.SaveFile()
                                    Access:EXCHACC.ClearKey(xca:Ref_Number_Key)
                                    xca:Ref_Number = xch:Ref_Number
                                    Set(xca:Ref_Number_Key, xca:Ref_Number_Key)
                                    Loop
                                        If Access:EXCHACC.NEXT()
                                            Break
                                        End ! If
                                        If xca:Ref_Number <> xch:Ref_Number      |
                                            Then Break   ! End If
                                        End ! If
                                        If tmp:Accessories = ''
                                            tmp:Accessories = Clip(xca:Accessory)
                                        Else ! If tmp:Accessories = ''
                                            tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(xca:Accessory)
                                        End ! If tmp:Accessories = ''
        
                                    End ! Loop
                                    Access:EXCHACC.RestoreFile(Save_xca_ID)
                                Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                ! Error
        
                                End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                tmp:IMEI    = xch:ESN
        
                            End ! If exc#
        
                            If loa#
                                Access:LOAN.Clearkey(loa:Ref_Number_Key)
                                loa:Ref_Number  = job:Loan_Unit_Number
                                If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                    ! Found
                                    tmp:IMEI    = loa:ESN
                                Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                ! Error
                                End ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                            End ! If loa#
                            Print(rpt:Detail)
                        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        ! Error
                        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    End ! Loop x# = 1 To Records(glo:q_JobNumber)
                Else ! tmp:WaybillID <> 0
        
                    Loop x# = 1 To Records(glo:q_jobnumber)
                        Get(glo:q_jobnumber, x#)
                        recordsprocessed += 1
                        Do DisplayProgress
        
        
                        access:jobs.clearkey(job:ref_number_key)
                        job:ref_number = glo:job_number_pointer
                        if access:jobs.fetch(job:ref_number_key) = Level:Benign
                            Access:JOBSE.Clearkey(jobe:RefNumberKey)
                            jobe:RefNumber  = job:Ref_Number
                            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            ! Found
                            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            ! Error
                            End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
        
                        !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
                        ! 26 Aug 2002 John
                        !
                            tmp:Ref_number = job:Ref_number
                            if glo:WebJob then
        
                                DespatchType = jobe:DespatchType
        
                            ! Change the tmp ref number if you
                            ! can Look up from the wob file
                                access:Webjob.clearkey(wob:RefNumberKey)
                                wob:RefNumber = job:Ref_number
                                if access:Webjob.fetch(wob:refNumberKey) = level:benign then
                                    tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
                                end ! if
        
                                If func:ToType = 'CUS'
                                    SetTarget(Report)
                                    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                                    sub:Account_Number  = job:Account_Number
                                    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                        ! Found
                                        to:CompanyName     = sub:Company_Name
                                        to:AddressLine1    = sub:Address_Line1
                                        to:AddressLine2    = sub:Address_Line2
                                        to:AddressLine3    = sub:Address_Line3
                                        to:TelephoneNumber = sub:Telephone_Number
                                        to:ContactName     = sub:Contact_Name
                                        to:EmailAddress    = sub:EmailAddress
                                        if sub:UseCustDespAdd = 'YES'
                                            to:CompanyName     = job:Company_Name
                                            to:AddressLine1    = job:Address_Line1
                                            to:AddressLine2    = job:Address_Line2
                                            to:AddressLine3    = job:Address_Line3
                                            to:TelephoneNumber = job:Telephone_Number
                                            to:ContactName     = ''
                                            to:EmailAddress    = jobe:EndUserEmailAddress
                                        else
                                            to:CompanyName     = job:Company_Name_Delivery
                                            to:AddressLine1    = job:Address_Line1_Delivery
                                            to:AddressLine2    = job:Address_Line2_Delivery
                                            to:AddressLine3    = job:Address_Line3_Delivery
                                            to:TelephoneNumber = job:Telephone_Delivery
                                            to:ContactName     = ''
                                            to:EmailAddress    = jobe:EndUserEmailAddress
                                        end ! if
                                    Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                    ! Error
                                    End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        
        
                                    settarget()
                                End ! If
        
                            ELSE
                                Despatchtype = job:Despatch_type
        
                                ! Check to see if it *IS* an RRC!
                                Access:SubTracc.ClearKey(sub:Account_Number_Key)
                                sub:Account_Number = Func:ToAccountNumber
                                IF Access:SubTracc.Fetch(sub:Account_Number_Key)
                            ! Error!
                            ! Don't need to change location here, it should be hanled by the despatch procedures
                            ! LocationChange(Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
                                    if Records(glo:q_jobnumber) = 1 ! Individual despatch
                                        settarget(Report)
                                        case tmp:CustomerAddress
                                        of 'CUS' ! Customer address
                                            to:CompanyName     = job:Company_Name
                                            to:AddressLine1    = job:Address_Line1
                                            to:AddressLine2    = job:Address_Line2
                                            to:AddressLine3    = job:Address_Line3
                                            to:TelephoneNumber = job:Telephone_Number
                                            to:ContactName     = ''
                                            to:EmailAddress    = jobe:EndUserEmailAddress
                                        of 'DEL' ! Delivery address
                                            to:CompanyName     = job:Company_Name_Delivery
                                            to:AddressLine1    = job:Address_Line1_Delivery
                                            to:AddressLine2    = job:Address_Line2_Delivery
                                            to:AddressLine3    = job:Address_Line3_Delivery
                                            to:TelephoneNumber = job:Telephone_Delivery
                                            to:ContactName     = ''
                                            to:EmailAddress    = jobe:EndUserEmailAddress
                                        end ! case
                                        settarget()
                                    end ! if
                                ELSE
                                    Access:TradeAcc.ClearKey(tra:Account_Number_Key)
                                    tra:Account_Number = sub:Main_Account_Number
                                    IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                                        ! Error!
                                        if despatchType = 'JOB' then
                                            If glo:Select21 <> 'REPRINT'
                                                LocationChange(Clip(GETINI('RRC', 'DespatchToCustomer',, CLIP(PATH()) & '\SB2KDEF.INI')))
                                            End ! If glo:Select21 <> 'REPRINT'
        
                                            if Records(glo:q_jobnumber) = 1 and tmp:WaybillID = 0! Individual despatch
                                                settarget(Report)
                                                case tmp:CustomerAddress
                                                of 'CUS' ! Customer address
                                                    to:CompanyName     = job:Company_Name
                                                    to:AddressLine1    = job:Address_Line1
                                                    to:AddressLine2    = job:Address_Line2
                                                    to:AddressLine3    = job:Address_Line3
                                                    to:TelephoneNumber = job:Telephone_Number
                                                    to:ContactName     = ''
                                                    to:EmailAddress    = jobe:EndUserEmailAddress
                                                of 'DEL' ! Delivery address
                                                    to:CompanyName     = job:Company_Name_Delivery
                                                    to:AddressLine1    = job:Address_Line1_Delivery
                                                    to:AddressLine2    = job:Address_Line2_Delivery
                                                    to:AddressLine3    = job:Address_Line3_Delivery
                                                    to:TelephoneNumber = job:Telephone_Delivery
                                                    to:ContactName     = ''
                                                    to:EmailAddress    = jobe:EndUserEmailAddress
                                                end ! case
                                                settarget()
                                            end ! if
                                        end ! if
                                    ELSE
                                        IF tra:RemoteRepairCentre = TRUE
                                            if despatchType = 'JOB' And glo:Select21 <> 'REPRINT' then
                                                LocationChange(Clip(GETINI('RRC', 'InTransitRRC',, CLIP(PATH()) & '\SB2KDEF.INI')))
                                            end ! if
                                        ELSE
                                            if despatchType = 'JOB' then
                                                If glo:Select21 <> 'REPRINT'
                                                    LocationChange(Clip(GETINI('RRC', 'DespatchToCustomer',, CLIP(PATH()) & '\SB2KDEF.INI')))
                                                End ! If glo:Select21 <> 'REPRINT'
                                                if Records(glo:q_jobnumber) = 1 and tmp:WaybillID = 0! Individual despatch
                                                    settarget(Report)
                                                    case tmp:CustomerAddress
                                                    of 'CUS' ! Customer address
                                                        to:CompanyName     = job:Company_Name
                                                        to:AddressLine1    = job:Address_Line1
                                                        to:AddressLine2    = job:Address_Line2
                                                        to:AddressLine3    = job:Address_Line3
                                                        to:TelephoneNumber = job:Telephone_Number
                                                        to:ContactName     = ''
                                                        to:EmailAddress    = jobe:EndUserEmailAddress
                                                    of 'DEL' ! Delivery address
                                                        to:CompanyName     = job:Company_Name_Delivery
                                                        to:AddressLine1    = job:Address_Line1_Delivery
                                                        to:AddressLine2    = job:Address_Line2_Delivery
                                                        to:AddressLine3    = job:Address_Line3_Delivery
                                                        to:TelephoneNumber = job:Telephone_Delivery
                                                        to:ContactName     = ''
                                                        to:EmailAddress    = jobe:EndUserEmailAddress
                                                    end ! case
                                                    settarget()
                                                end ! if
                                            end ! if
                                        END ! IF
                                    END ! IF
                                END ! IF
                                Access:Jobs.Update()
                            end ! if
                            !--------------------------------------------------------------
        
                            Case DespatchType
                            Of 'EXC'
                                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                xch:Ref_Number  = job:Exchange_Unit_Number
                                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                    ! Found
                                    Save_xca_ID = Access:EXCHACC.SaveFile()
                                    Access:EXCHACC.ClearKey(xca:Ref_Number_Key)
                                    xca:Ref_Number = xch:Ref_Number
                                    Set(xca:Ref_Number_Key, xca:Ref_Number_Key)
                                    Loop
                                        If Access:EXCHACC.NEXT()
                                            Break
                                        End ! If
                                        If xca:Ref_Number <> xch:Ref_Number      |
                                            Then Break   ! End If
                                        End ! If
                                        If tmp:Accessories = ''
                                            tmp:Accessories = Clip(xca:Accessory)
                                        Else ! If tmp:Accessories = ''
                                            tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(xca:Accessory)
                                        End ! If tmp:Accessories = ''
        
                                    End ! Loop
                                    Access:EXCHACC.RestoreFile(Save_xca_ID)
                                Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                ! Error
        
                                End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                tmp:IMEI    = xch:ESN
                            Else
                                tmp:Accessories = ''
                                Save_jac_ID     = Access:JOBACC.SaveFile()
                                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                                jac:Ref_Number = job:Ref_Number
                                Set(jac:Ref_Number_Key, jac:Ref_Number_Key)
                                Loop
                                    If Access:JOBACC.NEXT()
                                        Break
                                    End ! If
                                    If jac:Ref_Number <> job:Ref_Number      |
                                        Then Break   ! End If
                                    End ! If
                                    If tmp:Accessories = ''
                                        tmp:Accessories = Clip(jac:Accessory)
                                    Else ! If tmp:Accessories = ''
                                        tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(jac:Accessory)
                                    End ! If tmp:Accessories = ''
                                End ! Loop
                                Access:JOBACC.RestoreFile(Save_jac_ID)
                                tmp:IMEI    = job:ESN
        
                            End ! Case job:Despatch_Type
                            !--------------------------------------------------------------
                            If DespatchType = 'LOA'
        
                                Access:LOAN.Clearkey(loa:Ref_Number_Key)
                                loa:Ref_Number  = job:Loan_Unit_Number
                                If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                    ! Found
                                    tmp:IMEI    = loa:ESN
        
                                Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                ! Error
                                End ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                            End ! If job:Despatch_Type = 'LOA'
                            !--------------------------------------------------------------
                            If job:Despatch_Type <> ''
                                Despatch_Type_Temp = job:Despatch_Type
                            Else ! If job:Despatch_Type <> ''
                                Despatch_Type_Temp = 'JOB'
                            End ! If job:Despatch_Type <> ''
                            !--------------------------------------------------------------
        
        
                            ! Security Pack Number
                            If glo:WebJob
                                ! Waybill Generation
                                Case glo:Select20
                                Of 'WAYREPRINTJTOARC'
                                    tmp:SecurityPackNumber = jobe:InSecurityPackNo
                                Of 'WAYREPRINTJTOCUST'
                                    tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                                Of 'WAYREPRINTE'
                                    tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                                Else
                                    ! j - change this if it is a despatch from the RRC
                                    Case jobe:DespatchType
                                    Of 'EXC'
                                        tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                                    Of 'LOA'
                                        tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                                    Of 'JOB'
                                        tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                                    Else
                                        tmp:SecurityPackNumber = jobe:InSecurityPackNo
                                    End ! Case job:Despatch_Type
                                End ! Case glo:Select21
        
                            Else ! If glo:WebJob
                                ! Waybill Generation
                                Case glo:Select20
                                Of 'WAYREPRINTJTORRC'
                                    tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                                Of 'WAYREPRINTE'
                                    tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                                Of 'WAYREPRINTJTOCUST'
                                    tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                                Else
        
                                    Case job:Despatch_Type
                                    Of 'EXC'
                                        tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                                    Of 'LOA'
                                        tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                                    Else
                                        tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                                    End ! Case job:Despatch_Type
                                End ! Case glo:Select21
        
                            End ! If glo:WebJob
                            Print(rpt:Detail)
                        !--------------------------------------------------------------
                        end! if access:jobs.fetch(job:ref_number_key) = Level:Benign
                    End! Loop x# = 1 To Records(glo:q_jobnumber)
        
                End ! tmp:WaybillID <> 0
                count_temp = Records(glo:q_jobnumber)
                Print(rpt:totals)
        
                setcursor()
            End ! NewProcedure# = 0
        
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(SUBTRACC,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','WayBillDespatch')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHACC.Close
    Relate:JOBACC.Close
    Relate:STANTEXT.Close
    Relate:WAYBILLJ.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !pass   func:FromAccountNumber - The Account number to appear under "From Sender"
  !       func:FromType          - Is the account number of the Head "TRA" or Sub "SUB" account
  !       func:ToACcountNumber   - The Account number to appear under "Deliver To"
  !       func:ToType            - Is the account number of the Head "TRA" or Sub "SUB" account
  !       func:ConsignmentNUmber - Waybill number
  !       func:Courier           - Despatch Courier
  
  
  !! Changing (DBH 04/08/2006) # 6643 - Change the format of the Waybill number
  !!tmp:ConsignmentNumber = func:ConsignmentNumber
  !! to (DBH 04/08/2006) # 6643
  !! Changing (DBH 20/03/2007) # 8865 - Allow to alter the prefix
  !!tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
  !! to (DBH 20/03/2007) # 8865
  !If GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
  !    tmp:ConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(func:ConsignmentNumber,@n07)
  !Else ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
  !    tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
  !End ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
  !! End (DBH 20/03/2007) #8865
  !! End (DBH 04/08/2006) #6643
  !tmp:Courier = func:Courier
  !
  !!Changed by Paul - 02/09/2009 Log No 11042
  !!check if the RRC has a prefic filled in
  !    Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
  !    way:WaybillNumber = func:ConsignmentNumber
  !    If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
  !        !Found
  !        !now look up the trade account
  !        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  !        tra:Account_Number  = way:AccountNumber
  !        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !            !Found
  !            !now see if the RRC waybill prefix has been filled in
  !            If clip(tra:RRCWaybillPrefix) <> '' then
  !                !Prefix filled in - so use it
  !                tmp:ConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(func:ConsignmentNumber,@n07)
  !            Else
  !                !Changed By Paul 17/09/09 - log no 11042
  !                tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
  !            End !If clip(tra:RRCWaybillPrefix) <> '' then
  !        End !If Ac
  !    End ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
  !
  !!Change End
  
  
  !Complete change to waybill prefix
  !change made by Paul 29/09/2009 - log no 11042
  If GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI') = 1
      tmp:HeadAccountNo = clip(GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI'))
  Else
      !cheat and manually set to 'AA20'
      tmp:HeadAccountNo = 'AA20'
  End !If GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI') = 1
  tmp:Courier = func:Courier
  
  
  !now look up the waybill and check if it was procduced from the head account or not
  Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
  way:WaybillNumber = func:ConsignmentNumber
  If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
      !Found
      If way:FromAccount = tmp:HeadAccountNo then
          !its been produced from the head account - so attach prefix accordingly
          If GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
              tmp:ConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(func:ConsignmentNumber,@n07)
          Else ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
              tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
          End ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
      Else
          !must have been produced from an RRC account - so change the prefix
          !now look up the trade account
  
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = way:FromAccount
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              !now see if the RRC waybill prefix has been filled in
              If clip(tra:RRCWaybillPrefix) <> '' then
                  !Prefix filled in - so use it
                  tmp:ConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(func:ConsignmentNumber,@n07)
              Else
                  !Changed By Paul 17/09/09 - log no 11042
                  tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
              End !If clip(tra:RRCWaybillPrefix) <> '' then
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          
      End !If way:AccountNumber = tmp:HeadAccountNo then
  Else
      !error
      ! #13545 Try and trap the creation of blank sundry waybills (DBH: 15/10/2015)
      AddToLog('Debug',ERROR(),'Unable to get waybill ' & func:ConsignmentNumber)
      BEEP(BEEP:SystemHand)  ;  YIELD()
      CASE Missive('An error occurred retrieving the waybill details. Please try again.','ServiceBase',|
                     'mstop.jpg','/&OK') 
      OF 1 ! &OK Button
      END!CASE MESSAGE
      POST(EVENT:CloseWindow)
  End ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
  
  
  
  !Change End
  
  
  
  
  
  
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','WAYBILL')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'WAYBILL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
  way:WaybillNumber = func:ConsignmentNumber
  If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
      !Found
  Else ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
      !Error
  End ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
  
  !printed by
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  !Barcode Bit
  ! Changing (DBH 04/08/2006) # 6643 - I think Code39 = 1
  !code_temp            = 3
  ! to (DBH 04/08/2006) # 6643
  Code_Temp = 1
  ! End (DBH 04/08/2006) #6643
  option_temp          = 0
  
  Bar_Code_String_Temp = CLIP(tmp:ConsignmentNumber)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  !Work out addresses
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'WAYBILL'
  access:stantext.fetch(stt:description_key)
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  
  Case func:FromType
      Of 'TRA'
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = func:FromAccountNumber
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              from:CompanyName        = tra:Company_Name
              from:AddressLine1       = tra:Address_Line1
              from:AddressLine2       = tra:Address_Line2
              from:AddressLine3       = tra:Address_Line3
              from:TelephoneNumber    = tra:Telephone_Number
              from:ContactName        = tra:Contact_Name
              from:EmailAddress       = tra:EmailAddress
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Of 'SUB'
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number  = func:FromAccountNumber
          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Found
              from:CompanyName        = sub:Company_Name
              from:AddressLine1       = sub:Address_Line1
              from:AddressLine2       = sub:Address_Line2
              from:AddressLine3       = sub:Address_Line3
              from:TelephoneNumber    = sub:Telephone_Number
              from:ContactName        = sub:Contact_Name
              from:EmailAddress       = sub:EmailAddress
          Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      Of 'DEF'
          from:CompanyName        = def:User_Name
          from:AddressLine1       = def:Address_Line1
          from:AddressLine2       = def:Address_Line2
          from:AddressLine3       = def:Address_Line3
          from:TelephoneNumber    = def:Telephone_Number
          from:ContactName        = ''
          from:EmailAddress       = def:EmailAddress
  
  End !func:FromType
  
  Case func:ToType
      Of 'TRA'
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = func:ToAccountNumber
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              to:AccountNumber      = tra:Account_Number
              to:CompanyName        = tra:Company_Name
              to:AddressLine1       = tra:Address_Line1
              to:AddressLine2       = tra:Address_Line2
              to:AddressLine3       = tra:Address_Line3
              to:TelephoneNumber    = tra:Telephone_Number
              to:ContactName        = tra:Contact_Name
              to:EmailAddress       = tra:EmailAddress
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      Of 'SUB'
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number  = func:ToAccountNumber
          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Found
              to:AccountNumber      = sub:Account_Number
              to:CompanyName        = sub:Company_Name
              to:AddressLine1       = sub:Address_Line1
              to:AddressLine2       = sub:Address_Line2
              to:AddressLine3       = sub:Address_Line3
              to:TelephoneNumber    = sub:Telephone_Number
              to:ContactName        = sub:Contact_Name
              to:EmailAddress       = sub:EmailAddress
              if sub:UseCustDespAdd = 'YES'
                  tmp:CustomerAddress = 'CUS'
              else
                  tmp:CustomerAddress = 'DEL'
              end
          Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  
  End !func:ToType
  
      !New way of working out addressess
      tmp:WayBillID = 0
      Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
      way:WayBillNumber = func:ConsignmentNumber
      If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
          !Found
          If way:WaybillID <> 0
  
              !Is this a rejection. If so, show title - 4285 (DBH: 14-06-2004)
              IF way:WaybillID = 13
                  SetTarget(Report)
                  ?WaybillRejection{prop:Hide} = False
                  SetTarget()
              End !IF way:WaybillID = 13
              tmp:WayBillID = way:WaybillID
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number  = way:FromAccount
              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Found
                  from:CompanyName    = tra:Company_Name
                  from:AddressLine1   = tra:Address_Line1
                  from:AddressLine2   = tra:Address_Line2
                  from:AddressLine3   = tra:Address_Line3
                  from:TelephoneNumber= tra:Telephone_Number
                  from:ContactName    = tra:Contact_Name
                  from:EmailAddress   = tra:EmailAddress
              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
              ! Inserting (DBH 07/07/2006) # 7149 - PUP to RRC Waybill. Pickup the address from the job
              If way:WayBillID = 20
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number  = way:FromAccount
                  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      from:CompanyName    = sub:Company_Name
                      from:AddressLine1   = sub:Address_Line1
                      from:AddressLine2   = sub:Address_Line2
                      from:AddressLine3   = sub:Address_Line3
                      from:TelephoneNumber= sub:Telephone_Number
                      from:ContactName    = sub:Contact_Name
                      from:EmailAddress   = sub:EmailAddress
                  Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                  ! Insert --- Use the prefix based on the account of the job (DBH: 11/08/2009) #11005
                  if (sub:VCPWaybillPRefix <> '')
                      settarget(report)
                      tmp:ConsignmentNumber = clip(sub:VCPWaybillPrefix) & Format(func:ConsignmentNumber,@n07)
                      settarget()
                  else ! if (sub:VCPWaybillPRefix <> '')
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number    = sub:Main_Account_Number
                      if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                          ! Found
                          if (tra:VCPWaybillPrefix <> '')
                              settarget(report)
                              tmp:ConsignmentNumber = clip(tra:VCPWaybillPrefix) & Format(func:ConsignmentNumber,@n07)
                              settarget()
                          end ! if (tra:VCPWaybillPrefix <> '')
                      else ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)
                          ! Error
                      end ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  end ! if (sub:VCPWaybillPRefix <> '')
                  ! end --- (DBH: 11/08/2009) #11005
                  
              End ! If way:WayBillID = 20
              ! End (DBH 07/07/2006) #7149
  
              Case way:WayBillID
              Of 1 Orof 5 Orof 6 Orof 11 Orof 12 Orof 13 Orof 20 Orof 23
                  ! Inserting (DBH 19/10/2007) # 9451 - Make allowance for PUP Waybills going back to the customer
                  If way:ToAccount = 'CUSTOMER'
                      !added by Paul 28/04/2010 - Log no 11419
                      !lookup sub account to try and get the contact name
                      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                      sub:Account_Number  = way:ToAccount
                      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:notify then
                        !lookup the head account
                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                          tra:Account_Number  = way:ToAccount
                          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign then
                              If tra:Use_Delivery_Address = 'YES' then
                                  to:ContactName     = clip(tra:Contact_Name)
                              Else
                                  to:ContactName     = ''
                              End
                          Else
                              to:ContactName     = ''
                          End
                      Else
                          If sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                      End
  
                      to:AccountNumber   = job:Account_Number
                      to:CompanyName     = job:Company_Name
                      to:AddressLine1    = job:Address_Line1
                      to:AddressLine2    = job:Address_Line2
                      to:AddressLine3    = job:Address_Line3
                      to:TelephoneNumber = job:Telephone_Number
  
                      to:EmailAddress    = jobe:EndUserEmailAddress
                  Else ! If way:ToAccount = 'CUSTOMER'
                  ! End (DBH 19/10/2007) #9451
  
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number  = way:ToAccount
                      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Found
                          to:AccountNumber  = tra:Account_Number
                          to:CompanyName    = tra:Company_Name
                          to:AddressLine1   = tra:Address_Line1
                          to:AddressLine2   = tra:Address_Line2
                          to:AddressLine3   = tra:Address_Line3
                          to:TelephoneNumber= tra:Telephone_Number
                          to:ContactName    = tra:Contact_Name
                          to:EmailAddress   = tra:EmailAddress
                      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Error
                      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  End ! If way:ToAccount = 'CUSTOMER'
              ! Inserting (DBH 07/07/2006) # 7149 - RRC to PUP Address from the job account
              Of 21 Orof 22 !RRC To PUP
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number  = way:ToAccount
                  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      to:AccountNumber  = sub:Account_Number
                      to:CompanyName    = sub:Company_Name
                      to:AddressLine1   = sub:Address_Line1
                      to:AddressLine2   = sub:Address_Line2
                      to:AddressLine3   = sub:Address_Line3
                      to:TelephoneNumber= sub:Telephone_Number
                      to:ContactName    = sub:Contact_Name
                      to:EmailAddress   = sub:EmailAddress
                  Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                  ! End (DBH 07/07/2006) #7149
              ! Inserting (DBH 05/09/2006) # 7995 - Use default addresses for sundry waybill
              Of 300 !Sundry Waybill. Address covered above
                  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                  tra:Account_Number = way:FromAccount
                  If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                      !Found
                      from:CompanyName    = tra:Company_Name
                      from:AddressLine1   = tra:Address_Line1
                      from:AddressLine2   = tra:Address_Line2
                      from:AddressLine3   = tra:Address_Line3
                      from:TelephoneNumber    = tra:Telephone_Number
                      from:ContactName    = tra:Contact_Name
                      from:EmailAddress   = tra:EmailAddress
                  Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  If way:ToAccount = 'OTHER'
                      to:AccountNumber = way:OtherAccountNumber
                      to:CompanyName  = way:OtherCompanyName
                      to:AddressLine1 = way:OtherAddress1
                      to:AddressLine2 = way:OtherAddress2
                      to:AddressLine3 = way:OtherAddress3
                      to:TelephoneNumber = way:OtherTelephoneNO
                      to:ContactName  = way:OtherContactName
                      to:EmailAddress = way:OtherEmailAddress
                  Else ! If way:ToAccount = 'OTHER'
                      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                      tra:Account_Number = way:ToAccount
                      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                          !Found
                          to:AccountNumber      = tra:Account_Number
                          to:CompanyName        = tra:Company_Name
                          to:AddressLine1       = tra:Address_Line1
                          to:AddressLine2       = tra:Address_Line2
                          to:AddressLine3       = tra:Address_Line3
                          to:TelephoneNumber    = tra:Telephone_Number
                          to:ContactName        = tra:Contact_Name
                          to:EmailAddress       = tra:EmailAddress
                      Else ! If Access:TRADEAC.TryFetch(tra:Account_Number_Key) = Level:Benign
                          !Error
                      End ! If Access:TRADEAC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  End ! If way:ToAccount = 'OTHER'
              ! End (DBH 05/09/2006) #7995
              OF 402 ! Loan Waybill Rejection. Return To RRC
                  IF (way:WaybillType = 23) ! #12532 Use the parts order default address (DBH: 10/04/2012)
                      from:CompanyName        = def:OrderCompanyName
                      from:AddressLine1       = def:OrderAddressLine1
                      from:AddressLine2       = def:OrderAddressLine2
                      from:AddressLine3       = def:OrderAddressLine3
                      from:TelephoneNumber    = def:OrderTelephoneNumber
                      from:ContactName        = ''
                      from:EmailAddress       = def:OrderEmailAddress
                  END ! IF
              Else
  
                  Get(glo:q_JobNumber,1)
  
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = glo:Job_Number_Pointer
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
  
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                  sub:Account_Number  = way:ToAccount
                  If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      if sub:UseCustDespAdd = 'YES'
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name
                          to:AddressLine1    = job:Address_Line1
                          to:AddressLine2    = job:Address_Line2
                          to:AddressLine3    = job:Address_Line3
                          to:TelephoneNumber = job:Telephone_Number
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress
                      else
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name_Delivery
                          to:AddressLine1    = job:Address_Line1_Delivery
                          to:AddressLine2    = job:Address_Line2_Delivery
                          to:AddressLine3    = job:Address_Line3_Delivery
                          to:TelephoneNumber = job:Telephone_Delivery
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress
                      end
                  Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  
              End ! Case way:WayBillID
          End !If way:WaybillID <> 0
      Else!If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'WAYBILL'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













WayBillExchange PROCEDURE(func:FromAccountNumber,func:FromType,func:ToAccountNumber,func:ToType,func:ConsignmentNumber,func:Courier,func:48Hour)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
save_jea_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_xca_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
save_jpt_id          USHORT,AUTO
print_group          QUEUE,PRE(prngrp)
esn                  STRING(30)
msn                  STRING(30)
job_number           REAL
                     END
Unit_Ref_Number_Temp REAL
Despatch_Type_Temp   STRING(3)
Invoice_Account_Number_Temp STRING(15)
Delivery_Account_Number_Temp STRING(15)
count_temp           REAL
model_number_temp    STRING(50)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:DefaultEmailAddress STRING(255)
tmp:Courier          STRING(30)
tmp:ConsignmentNumber STRING(30)
tmp:DespatchBatchNumber STRING(30)
tmp:IMEI             STRING(30)
tmp:Accessories      STRING(255)
FromAddressGroup     GROUP,PRE(from)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
TelephoneNumber      STRING(30)
ContactName          STRING(60)
EmailAddress         STRING(255)
                     END
ToAddressGroup       GROUP,PRE(to)
AccountNumber        STRING(30)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
TelephoneNumber      STRING(30)
ContactName          STRING(30)
EmailAddress         STRING(255)
                     END
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(31)
Bar_Code_Temp        CSTRING(31)
tmp:HeadAccountNo    STRING(15)
!-----------------------------------------------------------------------------
Process:View         VIEW(SUBTRACC)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,4021,7771,5719),PAPER(PAPER:LETTER),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,469,7802,3208),USE(?unnamed)
                         BOX,AT(52,2031,3177,156),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2969,3177,156),USE(?Box1:8),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2656,3177,156),USE(?Box1:6),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2813,3177,156),USE(?Box1:7),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2188,3177,156),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2500,3177,156),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2344,3177,156),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,1875,3177,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(990,1875,0,1250),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Courier:'),AT(4896,781),USE(?String23),TRN,FONT(,8,,)
                         STRING(@s30),AT(6042,781),USE(tmp:Courier),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(104,625),USE(from:AddressLine2),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatch Date:'),AT(4896,365),USE(?String27:2),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6042,365),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@t1),AT(6042,573),USE(ReportRunTime),TRN,FONT(,8,,FONT:bold)
                         STRING('From Sender:'),AT(104,104),USE(?String16),TRN,FONT(,8,,)
                         STRING('Deliver To:'),AT(104,1667),USE(?String16:5),TRN,FONT(,8,,)
                         STRING('Tel No:'),AT(104,1042),USE(?String16:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(885,1042),USE(from:TelephoneNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6563,990,375,208),USE(?CPCSPgOfPgStr),FONT(,8,,FONT:bold)
                         STRING(@s60),AT(885,1198),USE(from:ContactName),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6354,990),USE(?String48),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(4896,990),USE(?String62),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@n3),AT(6042,990),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,781),USE(from:AddressLine3),TRN,FONT(,8,,FONT:bold)
                         STRING('Email:'),AT(104,1354),USE(?String16:4),TRN,FONT(,8,,)
                         STRING(@s255),AT(885,1354),USE(from:EmailAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('WAYBILL NUMBER'),AT(3490,1667,3906,260),USE(?String39),TRN,CENTER,FONT(,12,,FONT:bold)
                         STRING('Contact Name:'),AT(104,1198),USE(?String16:3),TRN,HIDE,FONT(,8,,)
                         STRING(@s30),AT(3490,1979,3906,260),USE(Bar_Code_Temp),TRN,CENTER,FONT('C39 High 12pt LJ3',12,,,CHARSET:ANSI)
                         STRING('Account Number:'),AT(104,1875),USE(?AccountNumber),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,1875),USE(to:AccountNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Company Name:'),AT(104,2031),USE(?CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2031),USE(to:CompanyName),TRN,FONT(,8,,FONT:bold)
                         STRING('Address 1:'),AT(104,2188),USE(?Address1),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2188),USE(to:AddressLine1),TRN,FONT(,8,,FONT:bold)
                         STRING('Address 2:'),AT(104,2344),USE(?Address2),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2344),USE(to:AddressLine2),TRN,FONT(,8,,FONT:bold)
                         STRING('Suburb:'),AT(104,2500),USE(?Suburb),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2500),USE(to:AddressLine3),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name:'),AT(104,2813),USE(?ContactName),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2813),USE(to:ContactName),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Number:'),AT(104,2656),USE(?ContactNumber),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2656),USE(to:TelephoneNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Email:'),AT(104,2969),USE(?Email),TRN,FONT(,8,,)
                         STRING(@s255),AT(1042,2969),USE(to:EmailAddress),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,260),USE(from:CompanyName),TRN,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(104,469),USE(from:AddressLine1),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatch Time:'),AT(4896,573),USE(?String27:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(3490,2292,3906,260),USE(tmp:ConsignmentNumber),TRN,CENTER,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,240),USE(?detailband)
                           STRING(@s25),AT(156,0),USE(xch:Manufacturer),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s25),AT(1563,0),USE(xch:Model_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(4010,0),USE(xch:MSN),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n_8b),AT(6823,0),USE(wai:JobNumber48Hour),TRN,HIDE,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s30),AT(5156,0),USE(xch:Stock_Type),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s18),AT(2969,0),USE(xch:ESN),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(,,,385),USE(?Totals)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number of Items On Waybill :'),AT(260,104),USE(?String59),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(2344,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(406,9615,7781,1229),USE(?unnamed:2)
                         LINE,AT(208,52,7135,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(208,104,7135,1042),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(385,479,7792,10521),USE(?unnamed:3)
                         STRING('Manufacturer'),AT(156,3281),USE(?String41),TRN,FONT(,7,,)
                         STRING('Model'),AT(1573,3281),USE(?String42),TRN,FONT(,7,,)
                         STRING('IMEI'),AT(2979,3281),USE(?String42:2),TRN,FONT(,7,,)
                         STRING('MSN'),AT(4021,3281),USE(?String42:3),TRN,FONT(,7,,)
                         LINE,AT(104,3229,7292,0),USE(?Line6),COLOR(COLOR:Black)
                         LINE,AT(104,3490,7292,0),USE(?Line6:2),COLOR(COLOR:Black)
                         STRING('Job No'),AT(6833,3281),USE(?JobNumber),TRN,HIDE,FONT(,7,,)
                         STRING('Stock Type'),AT(5167,3281),USE(?String42:4),TRN,FONT(,7,,)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('WayBillExchange')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','WayBillExchange')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'Waybill'
  If ClarioNETServer:Active() = True
      If PrintOption(PreviewReq,glo:ExportReport,'Waybill') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  End ! If ClarioNETServer:Active() = True
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:SUBTRACC.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHACC.Open
  Relate:STANTEXT.Open
  Relate:WAYITEMS.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:EXCHANGE.UseFile
  Access:LOAN.UseFile
  
  
  RecordsToProcess = RECORDS(SUBTRACC)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(SUBTRACC,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      recordstoprocess = 0
      !recordstoprocess    = Records(glo:q_jobnumber)
      
      Access:WAYITEMS.ClearKey(wai:WayBillNumberKey)
      wai:WayBillNumber = clip(func:ConsignmentNumber)
      set(wai:WayBillNumberKey, wai:WayBillNumberKey)
      loop until Access:WAYITEMS.Next()
          if wai:WayBillNumber <> clip(func:ConsignmentNumber) then break.
          recordstoprocess += 1
      end
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Free(print_group)
        Clear(print_group)
        setcursor(cursor:wait)
        
        count_temp = 0
        
        Access:WAYITEMS.ClearKey(wai:WayBillNumberKey)
        wai:WayBillNumber = clip(func:ConsignmentNumber)
        set(wai:WayBillNumberKey, wai:WayBillNumberKey)
        loop until Access:WAYITEMS.Next()
            if wai:WayBillNumber <> clip(func:ConsignmentNumber) then break.
        
        !Loop x# = 1 To Records(glo:q_jobnumber)
        !    Get(glo:q_jobnumber,x#)
            recordsprocessed += 1
            Do DisplayProgress
        
            access:EXCHANGE.clearkey(xch:Ref_Number_Key)
            !xch:Ref_Number = glo:job_number_pointer
            xch:Ref_Number = wai:Ref_Number
            if access:EXCHANGE.fetch(xch:Ref_Number_Key) = Level:Benign
                !Show the job number, if it's a 48 hour exchange - 3911 (DBH: 24-02-2004)
                If wai:JobNumber48Hour <> 0
                    ?wai:JobNumber48Hour{prop:Hide} = 0
                    ?JobNumber{prop:Hide} = 0
                Else
                    Settarget(Report)
                    ?wai:JobNumber48Hour{prop:Hide} = 1
                    Settarget()
                End !If ?wai:JobNumber48Hour <> 0
        
                Print(rpt:Detail)
                count_temp += 1
            End
        
        End!Loop x# = 1 To Records(glo:q_jobnumber)
        !count_temp = Records(glo:q_jobnumber)
        
        Print(rpt:totals)
        
        setcursor()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(SUBTRACC,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','WayBillExchange')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHACC.Close
    Relate:JOBS.Close
    Relate:STANTEXT.Close
    Relate:WAYITEMS.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !pass   func:FromAccountNumber - The Account number to appear under "From Sender"
  !       func:FromType          - Is the account number of the Head "TRA" or Sub "SUB" account
  !       func:ToACcountNumber   - The Account number to appear under "Deliver To"
  !       func:ToType            - Is the account number of the Head "TRA" or Sub "SUB" account
  !       func:ConsignmentNUmber - Waybill number
  !       func:Courier           - Despatch Courier
  
  
  !! Changing (DBH 04/08/2006) # 6643 - Change the format of the Waybill Number
  !!tmp:ConsignmentNumber = func:ConsignmentNumber
  !! to (DBH 04/08/2006) # 6643
  !! Changing (DBH 20/03/2007) # 8865 - Allow to alter the prefix
  !!tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
  !! to (DBH 20/03/2007) # 8865
  !If GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
  !    tmp:ConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(func:ConsignmentNumber,@n07)
  !Else ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
  !    tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
  !End ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
  !! End (DBH 20/03/2007) #8865
  !
  !! End (DBH 04/08/2006) #6643
  !tmp:Courier = func:Courier
  !
  !!Changed by Paul - 02/09/2009 Log No 11042
  !!check if the RRC has a prefic filled in
  !    Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
  !    way:WaybillNumber = func:ConsignmentNumber
  !    If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
  !        !Found
  !        !now look up the trade account
  !        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  !        tra:Account_Number  = way:AccountNumber
  !        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !            !Found
  !            !now see if the RRC waybill prefix has been filled in
  !            If clip(tra:RRCWaybillPrefix) <> '' then
  !                !Prefix filled in - so use it
  !                tmp:ConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(func:ConsignmentNumber,@n07)
  !            Else
  !                !Changed By Paul 17/09/09 - log no 11042
  !                tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
  !            End !If clip(tra:RRCWaybillPrefix) <> '' then
  !        End !If Ac
  !    End ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
  !
  !!Change End
  
  
  !Complete change to waybill prefix
  !change made by Paul 29/09/2009 - log no 11042
  If GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI') = 1
      tmp:HeadAccountNo = clip(GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI'))
  Else
      !cheat and manually set to 'AA20'
      tmp:HeadAccountNo = 'AA20'
  End !If GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI') = 1
  tmp:Courier = func:Courier
  
  !now look up the waybill and check if it was procduced from the head account or not
  Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
  way:WaybillNumber = func:ConsignmentNumber
  If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
      !Found
  
      If way:AccountNumber = tmp:HeadAccountNo then
          !its been produced from the head account - so attach prefix accordingly
          If GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
              tmp:ConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(func:ConsignmentNumber,@n07)
          Else ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
              tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
          End ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
      Else
          !must have been produced from an RRC account - so change the prefix
          !now look up the trade account
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = way:AccountNumber
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              !now see if the RRC waybill prefix has been filled in
              If clip(tra:RRCWaybillPrefix) <> '' then
                  !Prefix filled in - so use it
                  tmp:ConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(func:ConsignmentNumber,@n07)
              Else
                  !Changed By Paul 17/09/09 - log no 11042
                  tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
              End !If clip(tra:RRCWaybillPrefix) <> '' then
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      End !If way:AccountNumber = tmp:HeadAccountNo then
  End ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
  
  !Change End
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','WAYBILL')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'WAYBILL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !printed by
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  !Barcode Bit
  ! Changing (DBH 04/08/2006) # 6643 - Code 39 = 1
  !code_temp            = 3
  ! to (DBH 04/08/2006) # 6643
  Code_Temp = 1
  ! End (DBH 04/08/2006) #6643
  option_temp          = 0
  
  Bar_Code_String_Temp = CLIP(tmp:ConsignmentNumber)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  If func:48Hour
      Settarget(Report)
      ?wai:JobNumber48Hour{prop:Hide} = 0
      ?JobNumber{prop:Hide} = 0
      Settarget()
  End !func:48Hour
  !Set Addresses
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'WAYBILL'
  access:stantext.fetch(stt:description_key)
  
  !If ToType = 'DEF' then this is a from
  !Rapid Return To National Stores -  (DBH: 11-11-2003)
  If func:ToType = 'DEF'
      !From ARC to National Stores -  (DBH: 11-11-2003)
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = func:ToAccountNumber
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          from:CompanyName        = tra:Company_Name
          from:AddressLine1       = tra:Address_Line1
          from:AddressLine2       = tra:Address_Line2
          from:AddressLine3       = tra:Address_Line3
          from:TelephoneNumber    = tra:Telephone_Number
          from:ContactName        = tra:Contact_Name
          from:EmailAddress       = tra:EmailAddress
  
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      to:AccountNumber      = ''
      to:CompanyName        = def:OrderCompanyName
      to:AddressLine1       = def:OrderAddressLine1
      to:AddressLine2       = def:OrderAddressLine2
      to:AddressLine3       = def:OrderAddressLine3
      to:TelephoneNumber    = def:OrderTelephoneNumber
      to:ContactName        = ''
      to:EmailAddress       = def:OrderEmailAddress
  
  Else !func:ToType = 'DEF'
      !As per request, the "From" address is always the default "parts order" address
      from:CompanyName        = def:OrderCompanyName
      from:AddressLine1       = def:OrderAddressLine1
      from:AddressLine2       = def:OrderAddressLine2
      from:AddressLine3       = def:OrderAddressLine3
      from:TelephoneNumber    = def:OrderTelephoneNumber
      from:ContactName        = ''
      from:EmailAddress       = def:OrderEmailAddress
  
      Case func:ToType
          Of 'TRA'
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number  = func:ToAccountNumber
              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Found
                  to:AccountNumber      = tra:Account_Number
                  to:CompanyName        = tra:Company_Name
                  to:AddressLine1       = tra:Address_Line1
                  to:AddressLine2       = tra:Address_Line2
                  to:AddressLine3       = tra:Address_Line3
                  to:TelephoneNumber    = tra:Telephone_Number
                  to:ContactName        = tra:Contact_Name
                  to:EmailAddress       = tra:EmailAddress
              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          Of 'SUB'
              Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
              sub:Account_Number  = func:ToAccountNumber
              If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                  !Found
                  to:AccountNumber      = sub:Account_Number
                  to:CompanyName        = sub:Company_Name
                  to:AddressLine1       = sub:Address_Line1
                  to:AddressLine2       = sub:Address_Line2
                  to:AddressLine3       = sub:Address_Line3
                  to:TelephoneNumber    = sub:Telephone_Number
                  to:ContactName        = sub:Contact_Name
                  to:EmailAddress       = sub:EmailAddress
              Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      End !func:ToType
  End !func:ToType = 'DEF'
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'WAYBILL'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='WayBillExchange'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













WayBillLoan PROCEDURE(func:FromAccountNumber,func:FromType,func:ToAccountNumber,func:ToType,func:ConsignmentNumber,func:Courier)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
save_jea_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_xca_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
save_jpt_id          USHORT,AUTO
print_group          QUEUE,PRE(prngrp)
esn                  STRING(30)
msn                  STRING(30)
job_number           REAL
                     END
Unit_Ref_Number_Temp REAL
Despatch_Type_Temp   STRING(3)
Invoice_Account_Number_Temp STRING(15)
Delivery_Account_Number_Temp STRING(15)
count_temp           REAL
model_number_temp    STRING(50)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:DefaultEmailAddress STRING(255)
tmp:Courier          STRING(30)
tmp:ConsignmentNumber STRING(30)
tmp:DespatchBatchNumber STRING(30)
tmp:IMEI             STRING(30)
tmp:Accessories      STRING(255)
FromAddressGroup     GROUP,PRE(from)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
TelephoneNumber      STRING(30)
ContactName          STRING(60)
EmailAddress         STRING(255)
                     END
ToAddressGroup       GROUP,PRE(to)
AccountNumber        STRING(30)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
TelephoneNumber      STRING(30)
ContactName          STRING(30)
EmailAddress         STRING(255)
                     END
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(31)
Bar_Code_Temp        CSTRING(31)
tmp:HeadAccountNo    STRING(15)
!-----------------------------------------------------------------------------
Process:View         VIEW(SUBTRACC)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,3979,7771,5781),PAPER(PAPER:LETTER),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,469,7802,3167),USE(?unnamed)
                         BOX,AT(52,2031,3177,156),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2969,3177,156),USE(?Box1:8),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2656,3177,156),USE(?Box1:6),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2813,3177,156),USE(?Box1:7),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2188,3177,156),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2500,3177,156),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2344,3177,156),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,1875,3177,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(990,1875,0,1250),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Courier:'),AT(4896,781),USE(?String23),TRN,FONT(,8,,)
                         STRING(@s30),AT(6042,781),USE(tmp:Courier),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(104,625),USE(from:AddressLine2),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatch Date:'),AT(4896,365),USE(?String27:2),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6042,365),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@t1),AT(6042,573),USE(ReportRunTime),TRN,FONT(,8,,FONT:bold)
                         STRING('From Sender:'),AT(104,104),USE(?String16),TRN,FONT(,8,,)
                         STRING('Deliver To:'),AT(104,1667),USE(?String16:5),TRN,FONT(,8,,)
                         STRING('Tel No:'),AT(104,1042),USE(?String16:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(885,1042),USE(from:TelephoneNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6563,990,375,208),USE(?CPCSPgOfPgStr),FONT(,8,,FONT:bold)
                         STRING(@s60),AT(885,1198),USE(from:ContactName),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6354,990),USE(?String48),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(4896,990),USE(?String62),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@n3),AT(6042,990),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,781),USE(from:AddressLine3),TRN,FONT(,8,,FONT:bold)
                         STRING('Email:'),AT(104,1354),USE(?String16:4),TRN,FONT(,8,,)
                         STRING(@s255),AT(885,1354),USE(from:EmailAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('WAYBILL NUMBER'),AT(3490,1667,3906,260),USE(?String39),TRN,CENTER,FONT(,12,,FONT:bold)
                         STRING('Account Number:'),AT(104,1875),USE(?AccountNumber),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,1875),USE(to:AccountNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Company Name:'),AT(104,2031),USE(?CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2031),USE(to:CompanyName),TRN,FONT(,8,,FONT:bold)
                         STRING('Address 1:'),AT(104,2188),USE(?Address1),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2188),USE(to:AddressLine1),TRN,FONT(,8,,FONT:bold)
                         STRING('Address 2:'),AT(104,2344),USE(?Address2),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2344),USE(to:AddressLine2),TRN,FONT(,8,,FONT:bold)
                         STRING('Suburb:'),AT(104,2500),USE(?Suburb),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2500),USE(to:AddressLine3),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name:'),AT(104,2813),USE(?ContactName),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2813),USE(to:ContactName),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Number:'),AT(104,2656),USE(?ContactNumber),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2656),USE(to:TelephoneNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Email:'),AT(104,2969),USE(?Email),TRN,FONT(,8,,)
                         STRING(@s255),AT(1042,2969),USE(to:EmailAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name:'),AT(104,1198),USE(?String16:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(3490,1979,3906,260),USE(Bar_Code_Temp),TRN,CENTER,FONT('C39 High 12pt LJ3',12,,,CHARSET:ANSI)
                         STRING(@s30),AT(104,260),USE(from:CompanyName),TRN,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(104,469),USE(from:AddressLine1),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatch Time:'),AT(4896,573),USE(?String27:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(3490,2292,3906,260),USE(tmp:ConsignmentNumber),TRN,CENTER,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,240),USE(?detailband)
                           STRING(@s30),AT(156,0),USE(loa:Manufacturer),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(2115,0),USE(loa:Model_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(5771,0),USE(loa:MSN),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(3865,0),USE(loa:ESN),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(,,,385),USE(?Totals)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number of Items On Waybill :'),AT(260,104),USE(?String59),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(2344,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(406,9604,7781,1229),USE(?unnamed:2)
                         LINE,AT(208,52,7135,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(208,104,7135,1042),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(375,469,7813,10521),USE(?unnamed:3)
                         STRING('Manufacturer'),AT(156,3229),USE(?String41),TRN,FONT(,7,,)
                         STRING('Model'),AT(2125,3229),USE(?String42),TRN,FONT(,7,,)
                         STRING('IMEI'),AT(3875,3229),USE(?String42:2),TRN,FONT(,7,,)
                         STRING('MSN'),AT(5781,3229),USE(?String42:3),TRN,FONT(,7,,)
                         LINE,AT(104,3177,7292,0),USE(?Line6),COLOR(COLOR:Black)
                         LINE,AT(104,3438,7292,0),USE(?Line6:2),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('WayBillLoan')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','WayBillLoan')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'Waybill'
  If ClarioNETServer:Active() = True
      If PrintOption(PreviewReq,glo:ExportReport,'Waybill') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  End ! If ClarioNETServer:Active() = True
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:SUBTRACC.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:LOAN.Open
  Relate:STANTEXT.Open
  Relate:WAYITEMS.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:LOANACC.UseFile
  
  
  RecordsToProcess = RECORDS(SUBTRACC)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(SUBTRACC,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      recordstoprocess = 0
      !recordstoprocess    = Records(glo:q_jobnumber)
      
      Access:WAYITEMS.ClearKey(wai:WayBillNumberKey)
      wai:WayBillNumber = clip(func:ConsignmentNumber)
      set(wai:WayBillNumberKey, wai:WayBillNumberKey)
      loop until Access:WAYITEMS.Next()
          if wai:WayBillNumber <> clip(tmp:ConsignmentNumber) then break.
          recordstoprocess += 1
      end
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Free(print_group)
        Clear(print_group)
        setcursor(cursor:wait)
        
        count_temp = 0
        
        Access:WAYITEMS.ClearKey(wai:WayBillNumberKey)
        wai:WayBillNumber = clip(func:ConsignmentNumber)
        set(wai:WayBillNumberKey, wai:WayBillNumberKey)
        loop until Access:WAYITEMS.Next()
            if wai:WayBillNumber <> clip(func:ConsignmentNumber) then break.
        
        !Loop x# = 1 To Records(glo:q_jobnumber)
        !    Get(glo:q_jobnumber,x#)
            recordsprocessed += 1
            Do DisplayProgress
        
            access:LOAN.clearkey(loa:Ref_Number_Key)
            !loa:Ref_Number = glo:job_number_pointer
            loa:Ref_Number = wai:Ref_Number
            if access:LOAN.fetch(loa:Ref_Number_Key) = Level:Benign
                Print(rpt:Detail)
                count_temp += 1
            End
        
        End!Loop x# = 1 To Records(glo:q_jobnumber)
        !count_temp = Records(glo:q_jobnumber)
        Print(rpt:totals)
        
        setcursor()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(SUBTRACC,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','WayBillLoan')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBS.Close
    Relate:LOAN.Close
    Relate:STANTEXT.Close
    Relate:WAYITEMS.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !pass   func:FromAccountNumber - The Account number to appear under "From Sender"
  !       func:FromType          - Is the account number of the Head "TRA" or Sub "SUB" account
  !       func:ToAccountNumber   - The Account number to appear under "Deliver To"
  !       func:ToType            - Is the account number of the Head "TRA" or Sub "SUB" account
  !       func:ConsignmentNumber - Waybill number
  !       func:Courier           - Despatch Courier
  
  !! Changing (DBH 04/08/2006) # 6643 - Change the format of the Waybill Number
  !!tmp:ConsignmentNumber = func:ConsignmentNumber
  !! to (DBH 04/08/2006) # 6643
  !! Changing (DBH 20/03/2007) # 8865 - Allow to alter the prefix
  !!tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
  !! to (DBH 20/03/2007) # 8865
  !If GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
  !    tmp:ConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(func:ConsignmentNumber,@n07)
  !Else ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
  !    tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
  !End ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
  !! End (DBH 20/03/2007) #8865
  !
  !! End (DBH 04/08/2006) #6643
  !
  !tmp:Courier = func:Courier
  !
  !!Changed by Paul - 02/09/2009 Log No 11042
  !!check if the RRC has a prefic filled in
  !    Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
  !    way:WaybillNumber = func:ConsignmentNumber
  !    If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
  !        !Found
  !        !now look up the trade account
  !        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  !        tra:Account_Number  = way:AccountNumber
  !        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !            !Found
  !            !now see if the RRC waybill prefix has been filled in
  !            If clip(tra:RRCWaybillPrefix) <> '' then
  !                !Prefix filled in - so use it
  !                tmp:ConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(func:ConsignmentNumber,@n07)
  !            Else
  !                !Changed By Paul 17/09/09 - log no 11042
  !                tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
  !            End !If clip(tra:RRCWaybillPrefix) <> '' then
  !        End !If Ac
  !    End ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
  !
  !!Change End
  
  !Complete change to waybill prefix
  !change made by Paul 29/09/2009 - log no 11042
  If GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI') = 1
      tmp:HeadAccountNo = clip(GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI'))
  Else
      !cheat and manually set to 'AA20'
      tmp:HeadAccountNo = 'AA20'
  End !If GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI') = 1
  tmp:Courier = func:Courier
  
  !now look up the waybill and check if it was procduced from the head account or not
  Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
  way:WaybillNumber = func:ConsignmentNumber
  If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
      !Found
  
      If way:AccountNumber = tmp:HeadAccountNo then
          !its been produced from the head account - so attach prefix accordingly
          If GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
              tmp:ConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(func:ConsignmentNumber,@n07)
          Else ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
              tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
          End ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
      Else
          !must have been produced from an RRC account - so change the prefix
          !now look up the trade account
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = way:AccountNumber
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              !now see if the RRC waybill prefix has been filled in
              If clip(tra:RRCWaybillPrefix) <> '' then
                  !Prefix filled in - so use it
                  tmp:ConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(func:ConsignmentNumber,@n07)
              Else
                  !Changed By Paul 17/09/09 - log no 11042
                  tmp:ConsignmentNumber = 'VDC' & Format(func:ConsignmentNumber,@n07)
              End !If clip(tra:RRCWaybillPrefix) <> '' then
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      End !If way:AccountNumber = tmp:HeadAccountNo then
  End ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
  
  !Change End
  
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','WAYBILL')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'WAYBILL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !printed by
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  !Barcode Bit
  ! Changing (DBH 04/08/2006) # 6643 - Code 39 = 1
  !code_temp = 3
  ! to (DBH 04/08/2006) # 6643
  Code_Temp = 1
  ! End (DBH 04/08/2006) #6643
  option_temp = 0
  Bar_Code_String_Temp   = tmp:ConsignmentNumber
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'WAYBILL'
  access:stantext.fetch(stt:description_key)
  
  Case func:FromType
      Of 'TRA'
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = func:FromAccountNumber
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              from:CompanyName        = tra:Company_Name
              from:AddressLine1       = tra:Address_Line1
              from:AddressLine2       = tra:Address_Line2
              from:AddressLine3       = tra:Address_Line3
              from:TelephoneNumber    = tra:Telephone_Number
              from:ContactName        = tra:Contact_Name
              from:EmailAddress       = tra:EmailAddress
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Of 'SUB'
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number  = func:FromAccountNumber
          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Found
              from:CompanyName        = sub:Company_Name
              from:AddressLine1       = sub:Address_Line1
              from:AddressLine2       = sub:Address_Line2
              from:AddressLine3       = sub:Address_Line3
              from:TelephoneNumber    = sub:Telephone_Number
              from:ContactName        = sub:Contact_Name
              from:EmailAddress       = sub:EmailAddress
          Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  End !func:FromType
  
  Case func:ToType
      Of 'TRA'
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = func:ToAccountNumber
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              to:AccountNumber      = tra:Account_Number
              to:CompanyName        = tra:Company_Name
              to:AddressLine1       = tra:Address_Line1
              to:AddressLine2       = tra:Address_Line2
              to:AddressLine3       = tra:Address_Line3
              to:TelephoneNumber    = tra:Telephone_Number
              to:ContactName        = tra:Contact_Name
              to:EmailAddress       = tra:EmailAddress
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Of 'SUB'
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number  = func:ToAccountNumber
          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Found
              to:AccountNumber      = sub:Account_Number
              to:CompanyName        = sub:Company_Name
              to:AddressLine1       = sub:Address_Line1
              to:AddressLine2       = sub:Address_Line2
              to:AddressLine3       = sub:Address_Line3
              to:TelephoneNumber    = sub:Telephone_Number
              to:ContactName        = sub:Contact_Name
              to:EmailAddress       = sub:EmailAddress
          Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  End !func:ToType
  !This waybill is only produced from Rapid Loan Transfer
  !therefore the from address is always National Stores - 4002 (DBH: 04-03-2004)
  from:CompanyName        = def:OrderCompanyName
  from:AddressLine1       = def:OrderAddressLine1
  from:AddressLine2       = def:OrderAddressLine2
  from:AddressLine3       = def:OrderAddressLine3
  from:TelephoneNumber    = def:OrderTelephoneNumber
  from:ContactName        = ''
  from:EmailAddress       = def:OrderEmailAddress
  
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'WAYBILL'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='WayBillLoan'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













Despatch_Note_With_Pricing PROCEDURE
RejectRecord         LONG,AUTO
tmp:Ref_number       STRING(20)
tmp:PrintedBy        STRING(255)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_jobs_id         USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(30)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
customer_name_temp   STRING(60)
sub_total_temp       REAL
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
tmp:LoanExchangeUnit STRING(100)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:accessories      STRING(255)
tmp:OriginalIMEI     STRING(30)
tmp:FinalIMEI        STRING(20)
tmp:DefaultEmailAddress STRING(255)
tmp:EngineerReport   STRING(255)
OutFaults_Q          QUEUE,PRE(ofq)
OutFaultsDescription STRING(60)
                     END
DeliveryAddress      GROUP,PRE(delivery)
CustomerName         STRING(30)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
endUserTelNo         STRING(15)
printCopies          LONG
clientName           STRING(65)
tmp:ReplacementValue REAL
tmp:Terms            STRING(1000)
tmp:IDNumber         STRING(13)
DefaultAddress       GROUP,PRE(address)
SiteName             STRING(40)
Name                 STRING(40)
Name2                STRING(40)
Location             STRING(40)
AddressLine1         STRING(40)
AddressLine2         STRING(40)
AddressLine3         STRING(40)
AddressLine4         STRING(40)
Telephone            STRING(40)
RegistrationNo       STRING(40)
Fax                  STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
locTermsText         STRING(255)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Account_Number)
                       PROJECT(job_ali:Authority_Number)
                       PROJECT(job_ali:Consignment_Number)
                       PROJECT(job_ali:Courier)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:Despatch_Number)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Mobile_Number)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Order_Number)
                       PROJECT(job_ali:Time_Completed)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:date_booked)
                       PROJECT(job_ali:time_booked)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(375,6719,7521,1240),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,448,7542,8094),USE(?unnamed)
                         STRING('Job Number: '),AT(4917,396),USE(?String25),TRN,FONT(,8,,)
                         STRING(@s20),AT(6156,396),USE(tmp:Ref_number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Booked: '),AT(4917,719),USE(?String58),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6156,719),USE(job_ali:date_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6760,719),USE(job_ali:time_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Date Completed:'),AT(4917,875),USE(?String85),TRN,FONT(,8,,)
                         STRING('Despatch Batch No:'),AT(4917,563),USE(?String86),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6156,875,635,198),USE(job_ali:Date_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6760,875),USE(job_ali:Time_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@s10b),AT(6156,563),USE(job_ali:Despatch_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s22),AT(1604,3240),USE(job_ali:Order_Number,,?job_ali:Order_Number:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4531,3229),USE(charge_type_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3156,3240),USE(job_ali:Authority_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(6000,3240),USE(repair_type_temp),TRN,FONT(,8,,)
                         STRING(@s20),AT(156,3906,1000,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(5052,3906,1094,156),USE(tmp:FinalIMEI),TRN,HIDE,LEFT,FONT(,8,,)
                         STRING(@s20),AT(1250,3906,1323,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(2604,3906,1396,156),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3906,3906,1094,156),USE(tmp:OriginalIMEI),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(6198,3906),USE(job_ali:MSN),TRN,FONT(,8,,)
                         STRING('REPORTED FAULT'),AT(156,4167),USE(?String64),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,4167,5573,417),USE(jbn_ali:Fault_Description),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('ENGINEER REPORT'),AT(156,4635),USE(?String88),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,4635,5573,677),USE(tmp:EngineerReport),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('EXCHANGE UNIT'),AT(156,5625),USE(?Exchange_Unit),TRN,HIDE,FONT(,9,,FONT:bold)
                         STRING(@s100),AT(1615,5625,5781,208),USE(tmp:LoanExchangeUnit),TRN,HIDE,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s13),AT(1615,5833),USE(tmp:IDNumber),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('ID NUMBER'),AT(156,5833),USE(?IDNumber),TRN,FONT(,9,,FONT:bold)
                         STRING('ACCESSORIES'),AT(156,5365),USE(?String105),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,5365,5573,260),USE(tmp:accessories),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('PARTS USED'),AT(156,6094),USE(?Parts_Used),TRN,FONT(,9,,FONT:bold)
                         STRING('Qty'),AT(1521,6094),USE(?Qty),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1917,6094),USE(?Part_Number),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3698,6094),USE(?description),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Cost'),AT(5719,6094),USE(?Unit_Cost),TRN,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(6677,6094),USE(?Line_Cost),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1406,6250,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         TEXT,AT(156,7500,5885,260),USE(locTermsText),TRN,FONT(,7,,)
                         STRING('Customer Signature'),AT(156,7813),USE(?Terms2),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date: {13}/ {14}/'),AT(5052,7813),USE(?Terms3),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1406,7969,5313,0),USE(?SigLine),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4115,2344),USE(?Stringdeltel),TRN,FONT(,8,,)
                         STRING(@s30),AT(4323,2344),USE(delivery:TelephoneNumber),FONT(,8,,)
                         STRING(@s65),AT(4083,2813,2865,156),USE(clientName),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(2781,2188),USE(job_ali:Mobile_Number),TRN,LEFT,FONT(,8,,)
                         STRING('Mobile:'),AT(2229,2188),USE(?String35:3),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(2229,1875),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2781,1875),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(2229,2031),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2781,2031),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3240),USE(job_ali:Account_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1563),USE(delivery:CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1719,1917,156),USE(delivery:AddressLine1),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1875),USE(delivery:AddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2031),USE(delivery:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2188),USE(delivery:Postcode),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,115),USE(?DetailBand)
                           STRING(@n8b),AT(1198,0),USE(Quantity_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s25),AT(1917,0),USE(part_number_temp),TRN,FONT(,7,,)
                           STRING(@s25),AT(3677,0),USE(Description_temp),TRN,FONT(,7,,)
                           STRING(@n14.2b),AT(5531,0),USE(Cost_Temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n14.2b),AT(6521,0),USE(line_cost_temp),TRN,RIGHT,FONT(,7,,)
                         END
                         FOOTER,AT(396,8802,,1479),USE(?unnamed:2),TOGETHER,ABSOLUTE
                           STRING('Despatch Date:'),AT(156,83),USE(?String66),TRN,FONT(,8,,)
                           STRING(@d6b),AT(1250,83),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING('Courier: '),AT(156,240),USE(?String67),TRN,FONT(,8,,)
                           STRING(@s20),AT(1250,240),USE(job_ali:Courier),TRN,FONT(,8,,FONT:bold)
                           STRING('Labour:'),AT(4844,83),USE(?labour_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,83),USE(labour_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Consignment No:'),AT(156,396),USE(?String70),TRN,FONT(,8,,)
                           STRING(@s20),AT(1250,417),USE(job_ali:Consignment_Number),TRN,FONT(,8,,FONT:bold)
                           STRING(@s20),AT(3125,281),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(2875,83,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Carriage:'),AT(4844,417),USE(?carriage_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,240),USE(parts_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(4844,563),USE(?vat_String),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,396),USE(courier_cost_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Total:'),AT(4844,729),USE(?total_string),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(6281,719,1000,0),USE(?line),COLOR(COLOR:Black)
                           STRING(@n14.2b),AT(6396,729),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('Vodacom Repairs Loan Phone Terms And Conditions'),AT(156,1094),USE(?LoanTermsText),TRN,HIDE,FONT(,7,,FONT:bold+FONT:underline)
                           STRING(@s16),AT(2865,521,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@n14.2b),AT(6396,563),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Loan Replace Value:'),AT(156,625),USE(?LoanValueText),TRN,HIDE,FONT(,8,,)
                           STRING(@n14.2),AT(1250,625),USE(tmp:ReplacementValue),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('<128>'),AT(6250,729,156,208),USE(?Euro),TRN,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(2875,719,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Parts:'),AT(4844,240),USE(?parts_string),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(365,10479,7510,1156),USE(?unnamed:3)
                         TEXT,AT(104,52,7344,990),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(375,448,7542,11198),USE(?unnamed:4)
                         IMAGE('RINVDET.GIF'),AT(0,0,7552,10781),USE(?Image1)
                         STRING('DESPATCH NOTE'),AT(3750,0,3698,260),USE(?Title),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,365,3073,208),USE(address:Name2),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,521,2760,208),USE(address:Location),TRN,FONT(,8,,FONT:bold)
                         STRING('REG NO:'),AT(104,677),USE(?stringREGNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(625,677,1667,146),USE(address:RegistrationNo),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('VAT NO: '),AT(104,781),USE(?stringVATNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s20),AT(625,781,1771,156),USE(address:VatNumber),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,885,2240,156),USE(address:AddressLine1),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,990,2240,156),USE(address:AddressLine2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1094,2240,156),USE(address:AddressLine3),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1198),USE(address:AddressLine4),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('TEL:'),AT(2385,885),USE(?stringTEL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2625,885,1458,208),USE(address:Telephone),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('FAX:'),AT(2385,990,313,208),USE(?stringFAX),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2625,990,1510,188),USE(address:Fax),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('EMAIL:'),AT(104,1323,417,208),USE(?stringEMAIL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s255),AT(625,1313,3333,208),USE(address:EmailAddress),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),TRN,FONT(,9,,FONT:bold)
                         STRING('CUSTOMER ADDRESS'),AT(156,1458),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1458,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING('GENERAL DETAILS'),AT(156,2865),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY DETAILS'),AT(156,8177),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4792,8177),USE(?charge_details),TRN,FONT(,9,,FONT:bold)
                         STRING('Model'),AT(156,3750),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(156,3073),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(1604,3073),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Authority Number'),AT(3083,3073),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(4531,3073),USE(?Chargeable_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(6000,3073),USE(?Repair_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1250,3750),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Exch I.M.E.I. No'),AT(5052,3750),USE(?IMEINo:2),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(2604,3750),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(3906,3750),USE(?IMEINo),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6198,3750),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3490),USE(?String50),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Despatch_Note_With_Pricing')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Despatch_Note_With_Pricing')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'DespatchNote'
  If ClarioNETServer:Active() = True
      If PrintOption(PreviewReq,glo:ExportReport,'Despatch Note') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  End ! If ClarioNETServer:Active() = True
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:STANTEXT.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:JOBACC.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  Access:STOCK.UseFile
  Access:WARPARTS.UseFile
  Access:LOAN.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBOUTFL.UseFile
  Access:JOBSE.UseFile
  Access:MANFAULO.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAUPA.UseFile
  Access:MANFPALO.UseFile
  Access:JOBS.UseFile
  Access:JOBSE2.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        loop    ! Loop used to print number of copies (prop:Copies does not work in ClarioNet)
        
        If job_ali:exchange_unit_number <> ''
            part_number_temp = ''
            Print(rpt:detail)
        Else!If job_ali:exchange_unit_number <> ''
            count# = 0
        
            If job_ali:warranty_job = 'YES'
                setcursor(cursor:wait)
                save_wpr_id = access:warparts.savefile()
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = job_ali:ref_number
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> job_ali:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    part_number_temp = wpr:part_number
                    description_temp = wpr:description
                    quantity_temp = wpr:quantity
                    cost_temp = wpr:purchase_cost
                    line_cost_temp = wpr:quantity * wpr:purchase_cost
                    Print(rpt:detail)
                end !loop
                access:warparts.restorefile(save_wpr_id)
                setcursor()
        
            Else!If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
                setcursor(cursor:wait)
                save_par_id = access:parts.savefile()
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = job_ali:ref_number
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                       break
                    end !if
                    if par:ref_number  <> job_ali:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    description_temp = par:description
                    quantity_temp = par:quantity
                    cost_temp = par:sale_cost
                    part_number_temp = par:part_number
                    line_cost_temp = par:quantity * par:sale_cost
                    Print(rpt:detail)
                end !loop
                access:parts.restorefile(save_par_id)
                setcursor()
            End!If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
            If count# = 0
                part_number_temp = ''
                Print(rpt:detail)
            End!If count# = 0
        End!If job_ali:exchange_unit_number <> ''
        
        printCopies -= 1
        
        if clarionetserver:active() and printCopies > 0
            endpage(Report)
            cycle
        else
            break
        end
        
        end
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','Despatch_Note_With_Pricing')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  
  printCopies = 0
  If glo:Select2 >= 1  ! Was ( > 1 )
      Printer{PropPrint:Copies} = glo:Select2
      printCopies = glo:Select2
  End!If glo:Select2 > 1
  !Printer{PropPrint:Paper} = Paper:Letter
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !Use Alternative Contact Nos
  Case UseAlternativeContactNos(job_ali:Account_Number,0)
      Of 1
          tmp:DefaultEmailAddress = tra:AltEmailAddress
          tmp:DefaultTelephone    = tra:AltTelephoneNumber
          tmp:DefaultFax          = tra:AltFaxNumber
      Of 2
          tmp:DefaultEmailAddress = sub:AltEmailAddress
          tmp:DefaultTelephone    = sub:AltTelephoneNumber
          tmp:DefaultFax          = sub:AltFaxNumber
      Else
          tmp:DefaultEmailAddress = def:EmailAddress
  End !UseAlternativeContactNos(job_ali:Account_Number)
  ! Set Report Layout (DBH: 21-06-2006)
  
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
      Settarget(Report)
      ?chargeable_type{prop:text} = 'Warranty Type'
      ?repair_type{prop:text}     = 'Warranty Repair Type'
      ! Hide(?labour_string)
      ! Hide(?parts_string)
      ! Hide(?Carriage_string)
      ! Hide(?vat_string)
      ! Hide(?total_string)
      ! Hide(?line)
      Settarget()
      charge_type_temp = job_ali:warranty_charge_type
      repair_type_temp = job_ali:repair_type_warranty
  Else
      charge_type_temp = job_ali:charge_type
      repair_type_temp = job_ali:repair_type
  End! If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job_ali:account_number
  IF access:subtracc.fetch(sub:account_number_key)
    ! Error!
  END ! IF
  access:tradeacc.clearkey(tra:account_number_key)
  tra:account_number = sub:main_account_number
  IF access:tradeacc.fetch(tra:account_number_key)
    ! Error!
  END ! IF
  If tra:price_despatch <> 'YES'
      Settarget(report)
      hide(?unit_Cost)
      hide(?line_cost)
      hide(?cost_temp)
      hide(?line_cost_temp)
      hide(?labour_string)
      hide(?parts_string)
      hide(?carriage_String)
      hide(?vat_string)
      hide(?total_string)
      hide(?labour_temp)
      hide(?parts_temp)
      hide(?courier_cost_temp)
      hide(?vat_temp)
      hide(?total_temp)
      hide(?charge_details)
      ?Euro{prop:Hide} = 1
      Settarget()
  End! If tra:price_despatch <> 'YES'
  IF sub:PriceDespatchNotes = TRUE
      Settarget(report)
      unhide(?unit_Cost)
      unhide(?line_cost)
      unhide(?cost_temp)
      unhide(?line_cost_temp)
      unhide(?labour_string)
      unhide(?parts_string)
      unhide(?carriage_String)
      unhide(?vat_string)
      unhide(?total_string)
      unhide(?labour_temp)
      unhide(?parts_temp)
      unhide(?courier_cost_temp)
      unhide(?vat_temp)
      unhide(?total_temp)
      unhide(?charge_details)
      Settarget()
  END ! IF
  
  !*************************set the display address to the head account if booked on as a web job***********************
  if glo:webjob then
      def:User_Name        = tra:Company_Name
      def:Address_Line1    = tra:Address_Line1
      def:Address_Line2    = tra:Address_Line2
      def:Address_Line3    = tra:Address_Line3
      def:Postcode         = tra:Postcode
      tmp:DefaultTelephone = tra:Telephone_Number
      tmp:DefaultFax       = tra:Fax_Number
      def:EmailAddress     = tra:EmailAddress
  end ! if
  !*********************************************************************************************************************
  
  HideDespAdd# = 0
  if tra:invoice_sub_accounts = 'YES'
      If sub:HideDespAdd = 1
          HideDespAdd# = 1
      End! If sub:HideDespAdd = 1
  
      If SUB:UseCustDespAdd = 'YES'
          invoice_address1_temp = job_ali:address_line1
          invoice_address2_temp = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp = job_ali:address_line3
              invoice_address4_temp = job_ali:postcode
          End ! If
          invoice_company_name_temp     = job_ali:company_name
          invoice_telephone_number_temp = job_ali:telephone_number
          invoice_fax_number_temp       = job_ali:fax_number
      Else! If sub:invoice_customer_address = 'YES'
          invoice_address1_temp = sub:address_line1
          invoice_address2_temp = sub:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = sub:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp = sub:address_line3
              invoice_address4_temp = sub:postcode
          End ! If
          invoice_company_name_temp     = sub:company_name
          invoice_telephone_number_temp = sub:telephone_number
          invoice_fax_number_temp       = sub:fax_number
  
  
      End! If sub:invoice_customer_address = 'YES'
  
      else! if tra:use_sub_accounts = 'YES'
      If tra:HideDespAdd = 1
          HideDespAdd# = 1
      End! If tra:HideDespAdd = 'YES'
  
      If TRA:UseCustDespAdd = 'YES'
          invoice_address1_temp = job_ali:address_line1
          invoice_address2_temp = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp = job_ali:address_line3
              invoice_address4_temp = job_ali:postcode
          End ! If
          invoice_company_name_temp     = job_ali:company_name
          invoice_telephone_number_temp = job_ali:telephone_number
          invoice_fax_number_temp       = job_ali:fax_number
  
      Else! If tra:invoice_customer_address = 'YES'
          invoice_address1_temp = tra:address_line1
          invoice_address2_temp = tra:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = tra:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp = tra:address_line3
              invoice_address4_temp = tra:postcode
          End ! If
          invoice_company_name_temp     = tra:company_name
          invoice_telephone_number_temp = tra:telephone_number
          invoice_fax_number_temp       = tra:fax_number
  
      End! If tra:invoice_customer_address = 'YES'
  end!!if tra:use_sub_accounts = 'YES'
  
  ! Hide Despatch Address
  ! #12079 Seems to be trying to remove the wrong address.
  ! Fixed Now. (Bryan: 14/04/2011)
  If HideDespAdd# = 1
      Settarget(Report)
      ?delivery:CompanyName{prop:Hide}         = 1
      ?delivery:AddressLine1{prop:Hide} = 1
      ?delivery:AddressLine2{prop:Hide} = 1
      ?delivery:AddressLine3{prop:Hide} = 1
      ?delivery:Postcode{prop:hide}     = 1
      ?delivery:TelephoneNumber{prop:hide}    = 1
      ?stringDelTel{prop:hide}          = 1
  !    ?telephone{prop:hide}            = 1
  !    ?fax{prop:hide}                  = 1
  !    ?email{prop:hide}                = 1
  !    ?address:EmailAddress{prop:Hide} = 1
      Settarget()
  End! If HideDespAdd# = 1
  
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job_ali:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Found
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  ! Inserting (DBH 10/04/2006) #7305 - Get JOBSE2 record
  Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
  jobe2:RefNumber = job_ali:Ref_Number
  If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Found
        tmp:IDNumber = jobe2:IDNumber
      Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
  ! End (DBH 10/04/2006) #7305
  
  
        !------------------------------------------------------------------
  IF CLIP(job_ali:title) = ''
      customer_name_temp = job_ali:initial
  ELSIF CLIP(job_ali:initial) = ''
      customer_name_temp = job_ali:title
  ELSE
      customer_name_temp = CLIP(job_ali:title) & ' ' & CLIP(job_ali:initial)
  END ! IF
        !------------------------------------------------------------------
  IF CLIP(customer_name_temp) = ''
      customer_name_temp = job_ali:surname
  ELSIF CLIP(job_ali:surname) = ''
            ! customer_name_temp = customer_name_temp ! tautology
  ELSE
      customer_name_temp = CLIP(customer_name_temp) & ' ' & CLIP(job_ali:surname)
  END ! IF
        !------------------------------------------------------------------
  
  endUserTelNo = ''
  
  if jobe:EndUserTelNo <> ''
      endUserTelNo = clip(jobe:EndUserTelNo)
  end ! if
  
  if customer_name_temp <> ''
      clientName = 'Client: ' & clip(customer_name_temp) & '  Tel: ' & clip(endUserTelNo)
  else
      if endUserTelNo <> ''
          clientName = 'Tel: ' & clip(endUserTelNo)
      end ! if
  end ! if
  
  delivery:TelephoneNumber = job_ali:Telephone_Delivery
  delivery:CompanyName     = job_ali:Company_Name_Delivery
  delivery:AddressLine1    = job_ali:address_line1_delivery
  delivery:AddressLine2    = job_ali:address_line2_delivery
  If job_ali:address_line3_delivery   = ''
      delivery:AddressLine3 = job_ali:postcode_delivery
      delivery:Postcode     = ''
  Else
      delivery:AddressLine3 = job_ali:address_line3_delivery
      delivery:Postcode     = job_ali:postcode_delivery
  End ! If
  
  if job_ali:title = '' and job_ali:initial = ''
      delivery:CustomerName = clip(job_ali:surname)
  elsif job_ali:title = '' and job_ali:initial <> ''
      delivery:CustomerName = clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial = ''
      delivery:CustomerName = clip(job_ali:title) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial <> ''
      delivery:CustomerName = clip(job_ali:title) & ' ' & clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  else
      delivery:CustomerName = ''
  end ! if
  
  
  ! Change the delivery address to the RRC, if it's at the ARC
  ! going back to the RRC, otherwise its the delivery address on the job
  
  If ~glo:WebJob And jobe:WebJob
      Access:WEBJOB.Clearkey(wob:RefNumberKey)
      wob:RefNumber   = job_ali:Ref_Number
      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          ! Found
          Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
          tra_ali:Account_Number  = wob:HeadAccountNumber
          If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
              ! Found
              delivery:CustomerName    = tra_ali:Account_Number
              delivery:CompanyName     = tra_ali:Address_Line1
              delivery:AddressLine1    = tra_ali:Address_Line2
              delivery:AddressLine2    = tra_ali:Address_Line3
              delivery:AddressLine3    = tra_ali:Postcode
              delivery:Postcode        = ''
              delivery:TelephoneNumber = tra_ali:Telephone_Number
  
          Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
          ! Error
          End ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      ! Error
      End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      Else ! glo:WebJob And jobe:WebJob
  
  End ! glo:WebJob And jobe:WebJob
  
  labour_temp       = ''
  parts_temp        = ''
  courier_cost_temp = ''
  vat_temp          = ''
  total_temp        = ''
  If job_ali:chargeable_job = 'YES'
  
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job_ali:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Error
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      If job_ali:invoice_number <> ''
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job_ali:invoice_number
          access:invoice.fetch(inv:invoice_number_key)
          If glo:WebJob
              If inv:ExportedRRCOracle
                  Labour_Temp       = jobe:InvRRCCLabourCost
                  Parts_Temp        = jobe:InvRRCCPartsCost
                  Courier_Cost_Temp = job_ali:Invoice_Courier_Cost
                  Vat_Temp          = (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100) + |
                  (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100) + |
                  (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
              Else ! If inv:ExportedRRCOracle
                  Labour_Temp       = jobe:RRCCLabourCost
                  Parts_Temp        = jobe:RRCCPartsCost
                  Courier_Cost_Temp = job_ali:Courier_Cost
                  Vat_Temp          = (jobe:RRCCLabourCost * VatRate(job_ali:Account_Number, 'L') / 100) + |
                  (jobe:RRCCPartsCost * VatRate(job_ali:Account_Number, 'P') / 100) + |
                  (job:Courier_Cost * VatRate(job_ali:Account_Number, 'L') / 100)
              End ! If inv:ExportedRRCOracle
          Else ! If glo:WebJob
              labour_temp       = job_ali:invoice_labour_cost
              parts_temp        = job_ali:invoice_parts_cost
              courier_cost_temp = job_ali:invoice_courier_cost
              Vat_Temp          = (job_ali:Invoice_Labour_Cost * inv:Vat_Rate_Labour / 100) + |
              (job_ali:Invoice_Parts_Cost * inv:Vat_Rate_Parts / 100) + |
              (job_ali:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
          End ! If glo:WebJob
  
      Else! If job_ali:invoice_number <> ''
          If glo:WebJob
              Labour_Temp       = jobe:RRCCLabourCost
              Parts_Temp        = jobe:RRCCPartsCost
              Courier_Cost_Temp = job_ali:Courier_Cost
              Vat_Temp          = (jobe:RRCCLabourCost * VatRate(job_ali:Account_Number, 'L') / 100) + |
              (jobe:RRCCPartsCost * VatRate(job_ali:Account_Number, 'P') / 100) + |
              (job:Courier_Cost * VatRate(job_ali:Account_Number, 'L') / 100)
  
          Else ! If glo:WebJob
              Labour_Temp       = job_ali:Labour_Cost
              Parts_temp        = job_ali:Parts_Cost
              Courier_Cost_temp = job_ali:Courier_Cost
              Vat_Temp          = (job_ali:Labour_Cost * VatRate(job_ali:Account_Number, 'L') / 100) + |
              (job_ali:Parts_Cost * VatRate(job_ali:Account_Number, 'P') / 100) + |
              (job_ali:Courier_Cost * VatRate(job_ali:Account_Number, 'L') / 100)
          End ! If glo:WebJob
  
      End! If job_ali:invoice_number <> ''
      total_temp = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
  End! If job_ali:chargeable_job <> 'YES'
  
  If job_ali:Warranty_Job = 'YES'
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job_ali:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Error
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      If job_ali:invoice_number_warranty <> ''
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job_ali:invoice_number_Warranty
          access:invoice.fetch(inv:invoice_number_key)
          If glo:WebJob
              Labour_Temp       = jobe:InvRRCWLabourCost
              Parts_Temp        = jobe:InvRRCWPartsCost
              Courier_Cost_Temp = job_ali:WInvoice_Courier_Cost
              Vat_Temp          = (jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour / 100) + |
              (jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts / 100) + |
              (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
          Else ! If glo:WebJob
              labour_temp       = jobe:InvoiceClaimValue
              parts_temp        = job_ali:Winvoice_parts_cost
              courier_cost_temp = job_ali:Winvoice_courier_cost
              Vat_Temp          = (jobe:InvoiceClaimValue * inv:Vat_Rate_Labour / 100) + |
              (job_ali:WInvoice_Parts_Cost * inv:Vat_Rate_Parts / 100) +                 |
              (job_ali:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
          End ! If glo:WebJob
  
      Else! If job_ali:invoice_number <> ''
          If glo:WebJob
              Labour_Temp       = jobe:RRCWLabourCost
              Parts_Temp        = jobe:RRCWPartsCost
              Courier_Cost_Temp = job_ali:Courier_Cost_Warranty
              Vat_Temp          = (jobe:RRCWLabourCost * VatRate(job_ali:Account_Number, 'L') / 100) + |
              (jobe:RRCWPartsCost * VatRate(job_ali:Account_Number, 'P') / 100) + |
              (job:Courier_Cost_Warranty * VatRate(job_ali:Account_Number, 'L') / 100)
  
          Else ! If glo:WebJob
              Labour_Temp       = jobe:ClaimValue
              Parts_temp        = job_ali:Parts_Cost_Warranty
              Courier_Cost_temp = job_ali:Courier_Cost_Warranty
              Vat_Temp          = (jobe:ClaimValue * VatRate(job_ali:Account_Number, 'L') / 100) + |
              (job_ali:Parts_Cost_Warranty * VatRate(job_ali:Account_Number, 'P') / 100) +         |
              (job_ali:Courier_Cost_Warranty * VatRate(job_ali:Account_Number, 'L') / 100)
          End ! If glo:WebJob
  
      End! If job_ali:invoice_number <> ''
      total_temp = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
  End ! job_ali:Warranty_Job = 'YES'
  
  access:users.clearkey(use:user_code_key)
  use:user_code = job_ali:despatch_user
  If access:users.fetch(use:user_code_key) = Level:Benign
      despatched_user_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  End! If access:users.fetch(use:user_code_key) = Level:Benign
  
  setcursor(cursor:wait)
  tmp:accessories = ''
  save_jac_id     = access:jobacc.savefile()
  access:jobacc.clearkey(jac:ref_number_key)
  jac:ref_number = job_ali:ref_number
  set(jac:ref_number_key, jac:ref_number_key)
  loop
      if access:jobacc.next()
          break
      end ! if
      if jac:ref_number <> job_ali:ref_number      |
          then break   ! end if
      end ! if
      yldcnt# += 1
      if yldcnt# > 25
          yield() 
          yldcnt# = 0
      end ! if
      If tmp:accessories = ''
          tmp:accessories = jac:accessory
      Else! If tmp:accessories = ''
          tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
      End! If tmp:accessories = ''
  end ! loop
  access:jobacc.restorefile(save_jac_id)
  setcursor()
  
  
  Access:JOBOUTFL.ClearKey(joo:LevelKey) ! Added by Gary (28th August 2002)
  joo:JobNumber = job_ali:ref_number
  set(joo:LevelKey, joo:LevelKey)
  loop until Access:JOBOUTFL.Next()
      if joo:JobNumber <> job_ali:ref_number then break.
      OutFaults_Q.ofq:OutFaultsDescription = joo:Description
      get(OutFaults_Q, OutFaults_Q.ofq:OutFaultsDescription) ! Queue is used to prevent duplicates
      if error()
          tmp:EngineerReport                   = clip(tmp:EngineerReport) & clip(joo:Description) & '<13,10>'
          OutFaults_Q.ofq:OutFaultsDescription = joo:Description
          add(OutFaults_Q, OutFaults_Q.ofq:OutFaultsDescription)
      end ! if
  end ! loop
  
  exchange_note# = 0
  
  ! Exchange Loan
  If job_ali:exchange_unit_number <> ''
      Exchange_Note# = 1
  End! If job_ali:exchange_unit_number <> ''
  
  ! Check Chargeable Parts for an "Exchange" part
  
  save_par_id = access:parts.savefile()
  access:parts.clearkey(par:part_number_key)
  par:ref_number  = job_ali:ref_number
  set(par:part_number_key, par:part_number_key)
  loop
      if access:parts.next()
          break
      end ! if
      if par:ref_number  <> job_ali:ref_number      |
          then break   ! end if
      end ! if
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number = par:part_ref_number
      if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
          If sto:ExchangeUnit = 'YES'
              exchange_note# = 1
              Break
          End! If sto:ExchangeUnit = 'YES'
      end! if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
  end ! loop
  access:parts.restorefile(save_par_id)
  
  If exchange_note# = 0
  ! Check Warranty Parts for an "Exchange" part
      save_wpr_id = access:warparts.savefile()
      access:warparts.clearkey(wpr:part_number_key)
      wpr:ref_number  = job_ali:ref_number
      set(wpr:part_number_key, wpr:part_number_key)
      loop
          if access:warparts.next()
              break
          end ! if
          if wpr:ref_number  <> job_ali:ref_number      |
              then break   ! end if
          end ! if
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = wpr:part_ref_number
          if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
              If sto:ExchangeUnit = 'YES'
                  exchange_note# = 1
                  Break
              End! If sto:ExchangeUnit = 'YES'
          end! if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
      end ! loop
      access:warparts.restorefile(save_wpr_id)
  End! If exchange_note# = 0
  
  If Exchange_Note# = 1
      ! Do not put Exchange Unit Repair on the
      ! Despatch note heading if it's a failed assessment -  (DBH: 07-11-2003)
      If jobe:WebJob And ~glo:WebJob And jobe:Engineer48HourOption = 1
          save_JOBS_id = Access:JOBS.SaveFile()
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number  = job_ali:Ref_Number
          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              ! Found
              If PassExchangeAssessment() = False
                  Exchange_Note# = 0
              End ! If PassExchangeAssessment() = False
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          ! Error
          End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          Access:JOBS.RestoreFile(save_JOBS_id)
      End ! If jobe:HubRepair And ~glo:WebJob And jobe:Exchange48HourOption = 1
  End ! Exchange_Note# = 1
  
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  
  
  If job_ali:loan_unit_number <> 0        !was ''
      Settarget(report)
      ?exchange_unit{prop:text} = 'LOAN UNIT'
      ! Changing (DBH 21/06/2006) #7878 - Check if being despatched from Webmaster
      ! If job_ali:despatch_Type = 'LOA'
      ! to (DBH 21/06/2006) #7878
      !If (glo:WebJob = True And jobe:DespatchType = 'LOA') Or (glo:WebJob = False And job_ali:Despatch_Type = 'LOA')
      ! End (DBH 21/06/2006) #7878
          tmp:IDNumber = jobe2:LoanIDNumber
          ?title{prop:text} = 'LOAN DESPATCH NOTE'
      !End! If job_ali:despatch_Type = 'EXC'
      Unhide(?exchange_unit)
      Unhide(?tmp:LoanExchangeUnit)
      access:loan.clearkey(loa:ref_number_key)
      loa:ref_number = job_ali:loan_unit_number
      if access:loan.tryfetch(loa:ref_number_key) = Level:Benign
          tmp:LoanExchangeUnit = '(' & Clip(loa:Ref_number) & ') ' & CLip(loa:manufacturer) & ' ' & CLip(loa:model_number) & |
          ' - ' & CLip(loa:esn)
      end ! if
      Settarget()
  
      ! #12084 New standard texts (Bryan: 17/05/2011)
      Access:STANTEXT.Clearkey(stt:Description_Key)
      stt:Description = 'LOAN DESPATCH NOTE'
      IF (Access:STANTEXT.TryFetch(stt:Description_Key))
      END
  
  End! If job_ali:exchange_unit_number <> ''
  
  tmp:OriginalIMEI = job_ali:ESN
  
  ! Was the unit exchanged at Third Party?
  If job_ali:Third_Party_Site <> ''
      IMEIError# = 0
      If job_ali:Third_Party_Site <> ''
          Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
          jot:RefNumber = job_ali:Ref_Number
          Set(jot:RefNumberKey, jot:RefNumberKey)
          If Access:JOBTHIRD.NEXT()
              IMEIError# = 1
          Else ! If Access:JOBTHIRD.NEXT()
              If jot:RefNumber <> job_ali:Ref_Number
                  IMEIError# = 1
              Else ! If jot:RefNumber <> job_ali:Ref_Number
                  If jot:OriginalIMEI <> job_ali:esn
                      IMEIError# = 0
                  Else
                      IMEIError# = 1
                  End ! If jot:OriginalIMEI <> job_ali:esn
  
              End ! If jot:RefNumber <> job_ali:Ref_Number
          End ! If Access:JOBTHIRD.NEXT()
      Else ! job_ali:Third_Party_Site <> ''
          IMEIError# = 1
      End ! job_ali:Third_Party_Site <> ''
  
      If IMEIError# = 1
          tmp:OriginalIMEI     = job_ali:esn
      Else ! IMEIError# = 1
          Settarget(Report)
          tmp:OriginalIMEI          = jot:OriginalIMEI
          tmp:FinalIMEI             = job_ali:esn
          ?IMEINo{prop:text}        = 'Orig I.M.E.I. No'
          ?IMEINo:2{prop:hide}      = 0
          ?tmp:FinalIMEI{prop:hide} = 0
          Settarget()
      End ! IMEIError# = 1
  End ! job_ali:Third_Party_Site <> ''
  If exchange_note# = 1
      Settarget(report)
      ?title{prop:text} = 'EXCHANGE DESPATCH NOTE'
      Unhide(?exchange_unit)
      Unhide(?tmp:LoanExchangeUnit)
      Hide(?parts_used)
      Hide(?qty)
      Hide(?part_number)
      Hide(?Description)
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number = job_ali:exchange_unit_number
      if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
          tmp:LoanExchangeUnit = '(' & Clip(xch:Ref_number) & ') ' & CLip(xch:manufacturer) & ' ' & CLip(xch:model_number) & |
          ' - ' & CLip(xch:esn)
          tmp:OriginalIMEI          = job_ali:ESN
          tmp:FinalIMEI             = xch:ESN
          ?tmp:FinalIMEI{prop:Hide} = 0
          ?IMEINo:2{prop:Hide}      = 0
      end ! if
      Settarget()
  End! If exchange_note# = 1
  
  ! Get Job Notes!
  Access:JobNotes_Alias.ClearKey(jbn_ali:RefNumberKey)
  jbn_ali:RefNumber = job_ali:Ref_Number
  IF Access:JobNotes_Alias.Fetch(jbn_ali:RefNumberKey)
    ! Error!
  END ! IF
  
  
  ! If loan attached, show replacement value
  tmp:Terms = stt:Text
  If job_ali:Loan_Unit_Number <> '' and job_ali:Exchange_Unit_Number = 0
      SetTarget(Report)
      ?tmp:ReplacementValue{prop:Hide} = 0
      ?LoanValueText{prop:Hide}        = 0
      tmp:ReplacementValue             = jobe:LoanReplacementValue
      ?LoanTermsText{prop:Hide}        = 0
      !?Terms1{prop:Hide}               = 0
      ?Terms2{prop:Hide}               = 0
      ?Terms3{prop:Hide}               = 0
      ?SigLine{prop:Hide}              = 0
      SetTarget()
      tmp:Terms = '1) The loan phone and its accessories remain the property of Vodacom Service Provider Company and is given to the customer to use while his / her phone is being repaired. It is the customers responsibility to return the loan phone and its accessories in proper working condition.<13,10>'
      tmp:Terms = Clip(tmp:Terms) & '2) In the event of damage to the loan phone and/or its accessories, the customer will be liable for any costs incurred to repair or replace the loan phone and/or its accessories. <13,10>'
      tmp:Terms = Clip(tmp:Terms) & '3) In the event of loss or theft of the loan phone and/or its accessories the customer will be liable for the replacement cost of the loan phone and/or its accessories or he/she may replace the lost/stolen phone and/or its accessories with new ones of the same or similar make and model, or same replacement value.<13,10>'
      tmp:Terms = Clip(tmp:Terms) & '4) Any phone replaced by the customer must be able to operate on the Vodacom network. <13,10>'
      tmp:Terms = Clip(tmp:Terms) & '5) The customers repaired phone will not be returned until the loan phone and its accessories has been returned, repaired or replaced. '
  
  End ! job_ali:Loan_Unit_Number <> ''
  !*********CHANGE LICENSE ADDRESS*********
  set(defaults)
  access:defaults.next()
  Access:WEBJOB.Clearkey(wob:RefNumberKey)
  wob:RefNumber   = job_ali:Ref_Number
  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = wob:HeadAccountNumber
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          ! Found
  
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          ! Error
      End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      tmp:Ref_Number = job_ali:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
  
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      If (glo:WebJob = 1)
          tra:Account_Number = wob:HeadAccountNumber
      ELSE ! If (glo:WebJob = 1)
          tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      END ! If (glo:WebJob = 1)
      If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          address:SiteName        = tra:Company_Name
          address:Name            = tra:coTradingName
          address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
          address:Location        = tra:coLocation
          address:RegistrationNo  = tra:coRegistrationNo
          address:AddressLine1    = tra:coAddressLine1
          address:AddressLine2    = tra:coAddressLine2
          address:AddressLine3    = tra:coAddressLine3
          address:AddressLine4    = tra:coAddressLine4
          address:Telephone       = tra:coTelephoneNumber
          address:Fax             = tra:coFaxNumber
          address:EmailAddress    = tra:coEmailAddress
          address:VatNumber       = tra:coVATNumber
      END
  
  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !Error
      tmp:Ref_Number = job_ali:Ref_Number
  End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !Barcode bit
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(tmp:Ref_number)
  job_number_temp = 'Job No: ' & Clip(tmp:Ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'I.M.E.I. No.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  locTermsText = GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI')   ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Despatch_Note_With_Pricing'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END







