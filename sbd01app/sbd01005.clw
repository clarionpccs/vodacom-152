

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01005.INC'),ONCE        !Local module procedure declarations
                     END


JB:ComputerName CSTRING(255)
JB:ComputerLen  ULONG(255)






VodacomSingleInvoice PROCEDURE(f:InvoiceNumber,f:JobNumber,f:Prefix,f:Suffix)
Default_Invoice_Company_Name_Temp STRING(30)
locInvoiceNumber     LONG
locInvoiceDate       DATE
save_tradeacc_id     USHORT,AUTO
save_invoice_id      USHORT,AUTO
save_jobs_id         USHORT,AUTO
save_jobse_id        USHORT,AUTO
save_audit_id        USHORT,AUTO
save_aud_id          USHORT,AUTO
tmp:PrintA5Invoice   BYTE(0)
tmp:Ref_Number       STRING(20)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
RejectRecord         LONG,AUTO
save_par_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
customer_name_temp   STRING(60)
sub_total_temp       REAL
payment_date_temp    DATE,DIM(4)
other_amount_temp    REAL
total_paid_temp      REAL
payment_type_temp    STRING(20),DIM(4)
payment_amount_temp  REAL,DIM(4)
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
DefaultAddress       GROUP,PRE(address)
SiteName             STRING(40)
Name                 STRING(40)
Name2                STRING(40)
Location             STRING(40)
AddressLine1         STRING(40)
AddressLine2         STRING(40)
AddressLine3         STRING(40)
AddressLine4         STRING(40)
Telephone            STRING(40)
RegistrationNo       STRING(40)
Fax                  STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
tmp:LabourCost       REAL
tmp:PartsCost        REAL
tmp:CourierCost      REAL
tmp:InvoiceNumber    STRING(30)
InvoiceAddressGroup  GROUP,PRE(ia)
CustomerName         STRING(60)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
AddressLine4         STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
DeliveryAddressGroup GROUP,PRE(da)
CustomerName         STRING(60)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
AddressLine4         STRING(30)
TelephoneNumber      STRING(30)
                     END
locFranchiseAccountNumber STRING(20)
locDateIn            DATE
locDateOut           DATE
locTimeIn            TIME
locTimeOut           TIME
locMainOutFault      STRING(1000)
locAuditNotes        STRING(255)
XMLType              STRING(3)
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(385,6646,7521,2260),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,2240,7521,6688),USE(?unnamed)
                         STRING(@s20),AT(6140,1615),USE(tmp:Ref_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s10),AT(3906,1615),USE(wob:HeadAccountNumber),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(2917,1615),USE(tmp:InvoiceNumber),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4854,1615),USE(job:Order_Number),TRN,CENTER,FONT(,8,,)
                         STRING(@s60),AT(156,52),USE(ia:CustomerName),TRN,FONT(,8,,)
                         STRING(@s60),AT(4115,52),USE(da:CustomerName),TRN,FONT(,8,,)
                         STRING(@d6b),AT(156,1615),USE(locInvoiceDate),TRN,RIGHT,FONT(,8,,)
                         STRING(@s25),AT(1458,2135,1000,156),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(156,2135,1323,156),USE(job:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s16),AT(5000,2135,990,156),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                         STRING(@s16),AT(2917,2135),USE(job:MSN),TRN,FONT(,8,,)
                         STRING('REPORTED FAULT'),AT(104,2552),USE(?String64),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,2552,5313,688),USE(jbn:Fault_Description),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(1615,3281,5313,885),USE(locMainOutFault),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('ENGINEERS REPORT'),AT(104,3281),USE(?String88),TRN,FONT(,9,,FONT:bold)
                         STRING('PARTS USED'),AT(104,4219),USE(?String79),TRN,FONT(,9,,FONT:bold)
                         STRING('Qty'),AT(1521,4219),USE(?String80),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1917,4219),USE(?String81),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3677,4219),USE(?String82),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Cost'),AT(5719,4219),USE(?String83),TRN,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(6677,4219),USE(?String89),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1354,4375,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,833),USE(ia:AddressLine4),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4115,990),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4323,990),USE(da:TelephoneNumber),FONT(,8,,)
                         STRING('Vat No:'),AT(156,990),USE(?String34:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(625,990,1406,208),USE(ia:VatNumber),TRN,LEFT,FONT(,8,,)
                         STRING('Tel:'),AT(2083,677),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(2344,677,1406,208),USE(ia:TelephoneNumber),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(2083,833),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(2344,833,1094,208),USE(ia:FaxNumber),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(4115,208),USE(da:CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,365,1917,156),USE(da:AddressLine1),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,521),USE(da:AddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,677),USE(da:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,833),USE(da:AddressLine4),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,208),USE(ia:CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,365),USE(ia:AddressLine1),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,677),USE(ia:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,521),USE(ia:AddressLine2),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@n8b),AT(1156,0),USE(par:Quantity),TRN,RIGHT,FONT(,8,,)
                           STRING(@s25),AT(1917,0),USE(part_number_temp),TRN,FONT(,8,,)
                           STRING(@s25b),AT(3677,0),USE(par:Description),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(5396,0),USE(par:Sale_Cost),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,0),USE(line_cost_temp),TRN,RIGHT,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(385,9104,7521,2406),USE(?unnamed:4)
                         STRING('Labour:'),AT(4844,104),USE(?String90),TRN,FONT(,8,,)
                         STRING(@n-14.2),AT(6417,104),USE(tmp:LabourCost),TRN,RIGHT,FONT(,8,,)
                         STRING('Time In:'),AT(1615,156),USE(?String90:3),TRN,FONT(,8,,)
                         STRING(@d17b),AT(781,573),USE(locDateOut),TRN,FONT(,8,,)
                         STRING('Other:'),AT(4844,417),USE(?String93),TRN,FONT(,8,,)
                         STRING(@n-14.2),AT(6417,260),USE(tmp:PartsCost),TRN,RIGHT,FONT(,8,,)
                         STRING(@t7b),AT(2240,573),USE(locTimeOut),TRN,FONT(,8,,)
                         STRING(@t7b),AT(2240,156),USE(locTimeIn),TRN,FONT(,8,,)
                         STRING('Date Out:'),AT(156,573),USE(?String90:4),TRN,FONT(,8,,)
                         STRING('V.A.T.'),AT(4844,729),USE(?String94),TRN,FONT(,8,,)
                         STRING(@n-14.2),AT(6417,417),USE(tmp:CourierCost),TRN,RIGHT,FONT(,8,,)
                         STRING('Technician:'),AT(156,990),USE(?String90:6),TRN,FONT(,8,,)
                         STRING(@s30),AT(833,990),USE(job:Engineer),TRN,FONT(,8,,)
                         STRING('Time Out:'),AT(1615,573),USE(?String90:5),TRN,FONT(,8,,)
                         STRING('Date In:'),AT(156,156),USE(?String90:2),TRN,FONT(,8,,)
                         STRING('Total:'),AT(4844,990),USE(?String95),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(6302,938,1000,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING(@n-14.2),AT(6417,990),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Received By: '),AT(156,1510),USE(?String90:7),TRN,FONT(,10,,)
                         STRING('Signature: '),AT(3021,1510),USE(?String90:8),TRN,FONT(,10,,)
                         STRING('Date:'),AT(5885,1510),USE(?String90:9),TRN,FONT(,10,,)
                         LINE,AT(990,1719,2031,0),USE(?Line9),COLOR(COLOR:Black)
                         LINE,AT(3646,1719,2240,0),USE(?Line9:2),COLOR(COLOR:Black)
                         LINE,AT(6198,1719,1250,0),USE(?Line9:3),COLOR(COLOR:Black)
                         STRING(@d17b),AT(781,156),USE(locDateIn),TRN,FONT(,8,,)
                         LINE,AT(6302,573,1000,0),USE(?Line2:2),COLOR(COLOR:Black)
                         STRING(@n14.2),AT(6396,573),USE(sub_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('Sub Total:'),AT(4844,573),USE(?String106),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6396,729),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('<128>'),AT(6344,990),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Parts:'),AT(4844,260),USE(?String92),TRN,FONT(,8,,)
                       END
                       FORM,AT(385,406,7521,11198),USE(?unnamed:3)
                         BOX,AT(52,3125,7396,260),USE(?Box2),ROUND,COLOR(COLOR:Black),FILL(0ECECECH),LINEWIDTH(1)
                         BOX,AT(52,3646,7396,260),USE(?Box2:3),ROUND,COLOR(COLOR:Black),FILL(0ECECECH),LINEWIDTH(1)
                         STRING('TAX INVOICE'),AT(4167,0,3281,),USE(?Text:Invoice),TRN,RIGHT,FONT(,18,,FONT:bold)
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,417,3073,208),USE(address:Name2),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,260,3073,208),USE(address:Name),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,573,2760,208),USE(address:Location),TRN,FONT(,8,,FONT:bold)
                         STRING('REG NO:'),AT(104,729),USE(?stringREGNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(625,729,1667,208),USE(address:RegistrationNo),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('VAT NO: '),AT(104,833),USE(?stringVATNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s20),AT(625,833,1771,156),USE(address:VatNumber),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine1),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1146,2240,156),USE(address:AddressLine3),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1250),USE(address:AddressLine4),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('TEL:'),AT(104,1406),USE(?stringTEL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(521,1406,1458,208),USE(address:Telephone),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('FAX:'),AT(2083,1406,313,208),USE(?stringFAX),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2396,1406,1510,188),USE(address:Fax),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('EMAIL:'),AT(104,1510,417,208),USE(?stringEMAIL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s255),AT(521,1510,3333,208),USE(address:EmailAddress),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('INVOICE ADDRESS'),AT(104,1667),USE(?String119),TRN,FONT(,8,,FONT:bold)
                         TEXT,AT(4271,313,3177,1323),USE(de2:GlobalPrintText),TRN,RIGHT,FONT(,8,,)
                         STRING('Make'),AT(156,3698),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Date'),AT(156,3177),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1354,3125,0,1042),USE(?Line5),COLOR(COLOR:Black)
                         STRING('Our Ref'),AT(1458,3177),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Invoice No'),AT(2917,3177),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Your Order No'),AT(5000,3177),USE(?String40:5),TRN,FONT(,8,,FONT:bold)
                         STRING('Job No'),AT(6156,3177),USE(?String40:6),TRN,FONT(,8,,FONT:bold)
                         STRING('Account No'),AT(3906,3177),USE(?String40:7),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(52,3385,7396,260),USE(?Box2:2),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(6094,3125,0,521),USE(?Line5:4),COLOR(COLOR:Black)
                         LINE,AT(2813,3125,0,1042),USE(?Line5:2),COLOR(COLOR:Black)
                         LINE,AT(3802,3125,0,521),USE(?Line5:8),COLOR(COLOR:Black)
                         BOX,AT(52,3906,7396,260),USE(?Box2:4),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Model'),AT(1458,3698),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Serial Number'),AT(2917,3698),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(5000,3698),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(52,8698,2760,1302),USE(?Box4),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(4688,8698,2760,1302),USE(?Box5),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,4219,7396,4323),USE(?Box3),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(4890,3125,0,1042),USE(?Line5:3),COLOR(COLOR:Black)
                         BOX,AT(52,1823,3438,1250),USE(?Box1),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(4010,1823,3438,1250),USE(?Box6),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('DELIVERY ADDRESS'),AT(4063,1667),USE(?String119:2),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('VodacomSingleInvoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  !Print A5 Invoice
  
      IF (GETINI('PRINTING','PrintA5Invoice',,CLIP(PATH())&'\SB2KDEF.INI') = 1)
          IF (glo:WebJob = 1)
              Vodacom_Delivery_Note_Web(f:JobNumber,f:Prefix,f:Suffix)
          ELSE
              Vodacom_Delivery_Note(f:JobNumber)
          END
          
          RETURN
      END
  locInvoiceNumber = f:InvoiceNumber
      ! Save Window Name
   AddToLog('Report','Open','VodacomSingleInvoice')
  ! After Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  ! Before Embed Point: %AfterPreviewReq) DESC(After Setting Preview Request (PreviewReq)) ARG()
  END                                                 !---ClarioNET 83
  Omit('**override printoption**')
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'Invoice'
  If PrintOption(PreviewReq,glo:ExportReport,'Invoice') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  **override printoption**
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  If f:Prefix <> ''
      glo:ReportName = 'Credit'
      If PrintOption(PreviewReq,glo:ExportReport,'Credit') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  
  Else ! If f:Prefix <> ''
      glo:ReportName = 'Invoice'
      If PrintOption(PreviewReq,glo:ExportReport,'Invoice') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  End ! If f:Prefix <> ''
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  ! After Embed Point: %AfterPreviewReq) DESC(After Setting Preview Request (PreviewReq)) ARG()
  BIND('locInvoiceNumber',locInvoiceNumber)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBPAYMT.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:JOBACC.UseFile
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSE.UseFile
  Access:AUDIT.UseFile
  Access:JOBSINV.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:AUDSTATS.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBOUTFL.UseFile
  Access:AUDIT2.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = locInvoiceNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        count# = 0
        setcursor(cursor:wait)
        save_par_id = access:parts.savefile()
        access:parts.clearkey(par:part_number_key)
        par:ref_number  = job:ref_number
        set(par:part_number_key,par:part_number_key)
        loop
            if access:parts.next()
               break
            end !if
            if par:ref_number  <> job:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            count# += 1
            part_number_temp = par:part_number
            line_cost_temp = par:quantity * par:sale_cost
            Print(rpt:detail)
        end !loop
        access:parts.restorefile(save_par_id)
        setcursor()
        If count# = 0
            part_number_temp = 'No Parts Attached'
            Print(rpt:detail)
        End!If count# = 0
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
  ! Before Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  !clarionet stuff
  if glo:webjob then
      ClarioNET:CallClientProcedure('SetClientPrinter','Default')
  END !If this was a web call
      ! Save Window Name
   AddToLog('Report','Close','VodacomSingleInvoice')
  ! After Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBPAYMT.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  ! Get Records
  Access:JOBS.Clearkey(job:InvoiceNumberKey)
  job:Invoice_Number = inv:Invoice_Number
  IF (Access:JOBS.TryFetch(job:InvoiceNumberKey))
  END
  
  Access:WEBJOB.Clearkey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
  END
  
  Access:JOBSE.TryFetch(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
  END
  
  Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey))
  END
  
  SET(DEFAULT2,0) ! #12079 Pick up new "Global Text" (Bryan: 13/04/2011)
  Access:DEFAULT2.Next()
  
  If f:Suffix <> '' And f:Prefix = ''
      ! Printing an invoice (DBH: 27/07/2007)
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = job:Ref_Number
      jov:Type = 'I'
      jov:Suffix = f:Suffix
      If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Found
      Else ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Error
      End ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
  End ! If f:Suffix <> ''
  If f:Prefix <> ''
      ! Printing a Credit Note (DBH: 27/07/2007)
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = job:Ref_Number
      jov:Type = 'C'
      jov:Suffix = f:Suffix
      If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Found
      Else ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Error
      End ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
  End ! If f:Prefix <> ''
  ! Set Default Address
      ! Set Job Number / Invoice Number
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          tmp:Ref_Number = job:Ref_Number & '-' & Clip(tra:BranchIdentification) & wob:JobNumber
          tmp:InvoiceNumber = CLIP(job:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
      END
  
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      If (glo:WebJob = 1)
          tra:Account_Number = wob:HeadAccountNumber
      ELSE ! If (glo:WebJob = 1)
          tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      END ! If (glo:WebJob = 1)
      If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          address:SiteName        = tra:Company_Name
          address:Name            = tra:coTradingName
          address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
          address:Location        = tra:coLocation
          address:RegistrationNo  = tra:coRegistrationNo
          address:AddressLine1    = tra:coAddressLine1
          address:AddressLine2    = tra:coAddressLine2
          address:AddressLine3    = tra:coAddressLine3
          address:AddressLine4    = tra:coAddressLine4
          address:Telephone       = tra:coTelephoneNumber
          address:Fax             = tra:coFaxNumber
          address:EmailAddress    = tra:coEmailAddress
          address:VatNumber       = tra:coVATNumber
  
          foundPrinted# = 0
          Access:AUDIT.Clearkey(aud:Action_Key)
          aud:Ref_Number = job:Ref_Number
          aud:Action = 'INVOICE PRINTED'
          SET(aud:Action_Key,aud:Action_Key)
          LOOP UNTIL Access:AUDIT.Next()
              IF (aud:Ref_Number <> job:Ref_Number OR |
                  aud:Action <> 'INVOICE PRINTED')
                  BREAK
              END
                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                aud2:AUDRecordNumber = aud:Record_Number
                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                  IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & | 
                      CLIP(tra:BranchIdentification),aud2:Notes,1,1))
                      foundPrinted# = 1
                      BREAK
                  END
  
                END ! IF
          END
          If (foundPrinted# = 1)
             
              SetTarget(Report)
              ?Text:Invoice{prop:Text} = 'COPY TAX INVOICE'
              SetTarget()
          END
  
          DO CreateARCRRCInvoices
  
      END
  
  ! Set Invoice  / Delivery Address
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number = job:Account_Number
      IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign)
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number = sub:Main_Account_Number
          IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
          END
      END
      CLEAR(InvoiceAddressGroup)
      
      IF (sub:OverrideHeadVATNo = 1)
          ia:VatNumber = sub:Vat_Number    
      ELSE
          ia:VatNumber = tra:Vat_Number
      END
      
      IF (tra:Invoice_Sub_Accounts = 'YES')
          IF (sub:Invoice_Customer_Address <> 'YES')
              ia:CompanyName = sub:Company_Name
              ia:AddressLine1 = sub:Address_Line1
              ia:AddressLine2 = sub:Address_Line2
              ia:AddressLine3 = sub:Address_Line3
              ia:AddressLine4 = sub:Postcode
              ia:TelephoneNumber = sub:Telephone_Number
              ia:FaxNumber = sub:Fax_Number  
              ia:CustomerName = sub:Contact_Name
          END
          IF (ia:VatNumber = '')
              ia:VatNumber = jobe:VatNumber
          END
      ELSE
          IF (tra:Invoice_Customer_Address <> 'YES')
              ia:CompanyName = tra:Company_Name
              ia:AddressLine1 = tra:Address_Line1
              ia:AddressLine2 = tra:Address_Line2
              ia:AddressLine3 = tra:Address_Line3
              ia:AddressLine4 = tra:Postcode
              ia:TelephoneNumber = tra:Telephone_Number
              ia:FaxNumber = tra:Fax_Number  
              ia:CustomerName = tra:Contact_Name
          END
          
      END
      IF (ia:CompanyName) = ''
          IF (job:Surname <> '')
              IF (job:Title = '')
                  ia:CustomerName = job:Surname
              ELSE
                  IF (job:Initial <> '')
                      ia:CustomerName = CLIP(job:Title) & ' ' & CLIP(job:Initial) & ' ' & CLIP(job:Surname)
                  ELSE
                      ia:CustomerName = CLIP(job:Title) & ' ' & CLIP(job:Surname)
                  END
              END
          END
              
          ia:CompanyName = job:Company_Name
          ia:AddressLine1 = job:Address_Line1
          ia:AddressLine2 = job:Address_Line2
          ia:AddressLine3 = job:Address_Line3
          ia:AddressLine4 = job:Postcode
          ia:TelephoneNumber = job:Telephone_Number
          ia:FaxNumber = job:Fax_Number
      END
  
      da:CompanyName = job:Company_Name_Delivery
      da:AddressLine1 = job:Address_Line1_Delivery
      da:AddressLine2 = job:Address_Line2_Delivery
      da:AddressLine3 = job:Address_Line3_Delivery
      da:AddressLine4 = job:Postcode_Delivery
      da:TelephoneNumber = job:Telephone_Delivery
  
  
      IF (glo:WebJob <> 1)
          IF (jobe:WebJob = 1)
              ! RRC to ARC job.
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number = wob:HeadAccountNumber
              IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
                  da:CompanyName  = tra:Company_Name
                  da:AddressLine1 = tra:Address_Line1
                  da:AddressLine2 = tra:Address_Line2
                  da:AddressLine3 = tra:Address_Line3
                  da:AddressLine4 = tra:Postcode
                  da:TelephoneNumber = tra:Telephone_Number
                  
                  ia:CompanyName  = tra:Company_Name
                  ia:AddressLine1 = tra:Address_Line1
                  ia:AddressLine2 = tra:Address_Line2
                  ia:AddressLine3 = tra:Address_Line3
                  ia:AddressLine4 = tra:Postcode
                  ia:TelephoneNumber = tra:Telephone_Number
                  ia:VatNumber = tra:Vat_Number
                  
              END
          ELSE ! ! IF (jobe:WebJob = 1)
              
                  
              
          END ! IF (jobe:WebJob = 1)
  
      END ! IF (glo:WebJob = 1)
  
          
  ! Lookups
  ! OutFault
  locMainOutFault = ''
  ! #12091 Display outfaults list like the old A5 invoice. (Bryan: 09/05/2011)
  line# = 0
  Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
  joo:JobNumber = job:Ref_Number
  Set(joo:JobNumberKey,joo:JobNumberKey)
  Loop ! Begin Loop
      If Access:JOBOUTFL.Next()
          Break
      End ! If Access:JOBOUTFL.Next()
      If joo:JobNumber <> job:Ref_Number
          Break
      End ! If joo:JobNumber <> job:Ref_Number
      If Instring('IMEI VALIDATION',joo:Description,1,1)
          Cycle
      End ! If Instring('IMEI VALIDATION',joo:Description,1,1)
      line# += 1
      locMainOutFault = Clip(locMainoutfault) & '<13,10>' & Format(line#,@n2) & ' - ' & Clip(joo:Description)
  End ! Loop
  locMainOutFault = CLIP(Sub(locMainOutFault,3,1000))
  
  !Costs
  Total_Price('I',vat",total",balance")
  vat_temp = vat"
  total_temp = total"
  If (glo:WebJob = 1)
      tmp:LabourCost = jobe:InvRRCCLabourCost
      tmp:PartsCost = jobe:InvRRCCPartsCost
      tmp:CourierCost = job:Invoice_Courier_Cost
  ELSE
      tmp:LabourCost = job:Invoice_Labour_Cost
      tmp:PartsCost = job:Invoice_Parts_Cost
      tmp:CourierCost = job:Invoice_Courier_Cost
  END
  Sub_Total_Temp = tmp:LabourCost + tmp:PartsCost + tmp:CourierCost
  
  
  If f:Suffix <> ''
      total_temp = jov:NewTotalCost
  End ! If f:Suffix <> ''
  If f:Prefix <> ''
      total_temp = -jov:CreditAmount
  End ! If f:Prefix <> ''
  ! Find Date In/Out
      
      locDateIn = job:Date_Booked
      locTimeIn = job:Time_Booked
      ! #11881 Use booking date unless RRC job AT ARC. Then use the "date received at ARC".
      ! (Note: Vodacom didn't specifically mention this, but i've assumed
      ! for VCP jobs to use the "received at RRC" date, rather than the booking date (Bryan: 16/03/2011)
      If (glo:WebJob = 1)
          IF (job:Who_Booked = 'WEB')
              Access:AUDIT.Clearkey(aud:TypeActionKey)
              aud:Ref_Number = job:Ref_Number
              aud:Type = 'JOB'
              aud:Action = 'UNIT RECEIVED AT RRC FROM PUP'
              SET(aud:TypeActionKey,aud:TypeActionKey)
              LOOP UNTIL Access:AUDIT.Next()
                  IF (aud:Ref_Number <> job:Ref_Number OR |
                      aud:Type <> 'JOB' OR |
                      aud:Action <> 'UNIT RECEIVED AT RRC FROM PUP')
                      BREAK
                  END
                  locDateIn = aud:Date
                  locTimeIn = aud:Time
                  BREAK
              END
              
          END ! IF (job:Who_Booked = 'WEB')
          
      ELSE ! If (glo:WebJob = 1)
          IF (jobe:WebJob = 1)
              ! RRC Job
              foundDateIn# = 0
              Access:AUDSTATS.Clearkey(aus:DateChangedKey)
              aus:RefNumber = job:Ref_Number
              aus:Type = 'JOB'
              SET(aus:DateChangedKey,aus:DateChangedKey)
              LOOP UNTIL Access:AUDSTATS.Next()
                  IF (aus:RefNumber <> job:Ref_Number OR |
                      aus:TYPE <> 'JOB')
                      BREAK
                  END
                  IF (SUB(UPPER(aus:NewStatus),1,3) = '452')
                      locDateIn = aus:DateChanged
                      locTimeIn = aus:TimeChanged
                      foundDateIn# = 1
                      BREAK
                  END
              END
              IF (foundDateIn# = 0)
                  Access:LOCATLOG.Clearkey(lot:DateKey)
                  lot:RefNumber = job:Ref_Number
                  SET(lot:DateKey,lot:DateKey)
                  LOOP UNTIL Access:LOCATLOG.Next()
                      IF (lot:RefNumber <> job:Ref_Number)
                          BREAK
                      END
                      IF (lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
                          locDateIn = lot:TheDate
                          locTimeIn = lot:TheTime
                          BREAK
                      END
                  END
              END
          END
      END ! If (glo:WebJob = 1)
  
  
      ! Use despatch to customer date (use last occurance), unless ARC to RRC job
      locDateOut = 0
      locTimeOut = 0
      Access:LOCATLOG.Clearkey(lot:DateKey)
      lot:RefNumber = job:Ref_Number
      SET(lot:DateKey,lot:DateKey)
      LOOP UNTIL Access:LOCATLOG.Next()
          IF (lot:RefNumber <> job:Ref_Number)
              BREAK
          END
          IF (lot:NewLocation = Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
              locDateOut = lot:TheDate
              locTimeOut = lot:TheTime
  !            BREAK
          END
      END
  
      If (glo:WebJob <> 1)
          IF (jobe:WebJob = 1)
              ! RRC Job
              Access:LOCATLOG.Clearkey(lot:DateKey)
              lot:RefNumber = job:Ref_Number
              SET(lot:DateKey,lot:DateKey)
              LOOP UNTIL Access:LOCATLOG.Next()
                  IF (lot:RefNumber <> job:Ref_Number)
                      BREAK
                  END
                  IF (lot:NewLocation = Clip(GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI')))
                      locDateOut = lot:TheDate
                      locTimeOut = lot:TheTime
                      BREAK
                  END
              END
          END
      END ! If (glo:WebJob = 1)
  ! Credit Note Details
  ! Inserting (DBH 24/07/2007) # 9141 - Show suffix on Invoice Number
  If f:Suffix <> '' And f:Prefix = ''
      ! Printing an invoice (DBH: 27/07/2007)
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = job:Ref_Number
      jov:Type = 'I'
      jov:Suffix = f:Suffix
      If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Found
      Else ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Error
      End ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
      tmp:InvoiceNumber = Clip(tmp:InvoiceNumber) & Clip(f:Suffix)
  End ! If f:Suffix <> ''
  If f:Prefix <> ''
      ! Printing a Credit Note (DBH: 27/07/2007)
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = job:Ref_Number
      jov:Type = 'C'
      jov:Suffix = f:Suffix
      If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Found
      Else ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
          !Error
      End ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
      tmp:InvoiceNumber = 'CN' & Clip(tmp:InvoiceNumber) & Clip(f:Suffix)
  End ! If f:Prefix <> ''
  
  
  If f:Suffix <> ''
      Total_Temp = jov:NewTotalCost
  End ! If f:Suffix <> ''
  If f:Prefix <> ''
      Total_Temp = -jov:CreditAmount
      SetTarget(Report)
      ?Text:Invoice{prop:Text} = 'CREDIT NOTE'
      SetTarget()
  End ! If f:Prefix <> ''
  !End (DBH 24/07/2007) #9141
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='VodacomSingleInvoice'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


! Before Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()
CreateARCRRCInvoices        Routine
    If (glo:WebJob = 1)
        IF (inv:RRCInvoiceDate = 0)
            ! RRC Invoice has not been created
            inv:RRCInvoiceDate = TODAY()
            IF (inv:ExportedRRCOracle = 0)
                jobe:InvRRCCLabourCost  = jobe:RRCCLabourCost
                jobe:InvRRCCPartsCost   = jobe:RRCCPartsCost
                jobe:InvRRCCSubTotal    = jobe:RRCCSubTotal
                jobe:InvoiceHandlingFee = jobe:HandlingFee
                jobe:InvoiceExchangeRate= jobe:ExchangeRate
                Access:JOBSE.TryUpdate()
                
                inv:ExportedRRCOracle = 1
                Access:INVOICE.TryUpdate()
                
                Line500_XML('RIV')
                !Invoice logging requested by harvey - 07/05/15
                MakeInvoiceLog('RIV')

            END
            
            foundInvoiceCreated# = 0
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = job:Ref_Number
            aud:Action = 'INVOICE CREATED'
            SET(aud:Action_Key,aud:Action_Key)
            LOOP UNTIL Access:AUDIT.Next()
                IF (aud:Ref_Number <> job:Ref_Number OR |
                    aud:Action <> 'INVOICE CREATED')
                    BREAK
                END
                IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & | 
                    CLIP(tra:BranchIdentification),1,1))
                    foundInvoiceCreated# = 1
                    BREAK
                END
            END
            
            IF (foundInvoiceCreated# = 0)
                locAuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                IF (LoanAttachedToJob(job:Ref_Number))
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(job:Invoice_Courier_Cost,@n14.2)
                END
                IF (jobe:Engineer48HourOption = 1)
                    IF (jobe:ExcReplcamentCharge)
                        locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(job:Invoice_Courier_Cost,@n14.2)
                    END
                END
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(jobe:InvRRCCPartsCost,@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(jobe:InvRRCCLabourCost,@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(jobe:InvRRCCSubTotal,@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((jobe:InvRRCCPartsCOst * inv:RRCVatRateParts/100) + |
                    (jobe:InvRRCCLabourCost * inv:RRCVatRateLabour/100) + |
                    (job:Invoice_Courier_Cost * inv:RRCVatRateLabour/100),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(jobe:InvRRCCSubTotal + (jobe:InvRRCCPartsCost * inv:RRCVatRateParts/100) + |
                    (jobe:InvRRCCLabourCost * inv:RRCVatRateLabour/100) + |
                    (job:Invoice_Courier_Cost * inv:RRCVatRateLabour/100),@n14.2)
                
                IF (AddToAudit(job:Ref_Number,'JOB','INVOICE CREATED',CLIP(locAuditNotes)))
                END
            END

        ELSE 
                
        END
            If AddToAudit(job:Ref_Number,'JOB','INVOICE PRINTED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06) )
            End ! '<13,10>INVOICE DATE: ' & FOrmat(inv:Date_Created,@d06)

        locInvoiceDate = inv:RRCInvoiceDate
    ELSE ! If (glo:WebJob = 1)
        IF (inv:ARCInvoiceDate = 0)
            inv:ARCInvoiceDate = TODAY()
            job:Invoice_Labour_Cost = job:Labour_Cost
            job:Invoice_Courier_Cost = job:Courier_Cost
            job:Invoice_Parts_Cost = job:Parts_Cost
            job:Invoice_Sub_Total = job:Invoice_Labour_Cost + |
                job:Invoice_Parts_Cost + | 
                job:Invoice_Courier_Cost
            Access:JOBS.TryUpdate()
            
            inv:ExportedARCOracle = 1
            Access:INVOICe.TryUpdate()
            
            Line500_XML('AOW')
                !Invoice logging requested by harvey - 07/05/15
                MakeInvoiceLog('AOW')

            
            foundInvoiceCreated# = 0
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = job:Ref_Number
            aud:Action = 'INVOICE CREATED'
            SET(aud:Action_Key,aud:Action_Key)
            LOOP UNTIL Access:AUDIT.Next()
                IF (aud:Ref_Number <> job:Ref_Number OR |
                    aud:Action <> 'INVOICE CREATED')
                    BREAK
                END
                IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & | 
                    CLIP(tra:BranchIdentification),1,1))
                    foundInvoiceCreated# = 1
                    BREAK
                END
            END
            
            IF (foundInvoiceCreated# = 0)
                locAuditNotes   = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                If LoanAttachedToJob(job:Ref_Number)
                    ! Changing (DBH 27/06/2006) #7584 - If loan attached, then invoice number is not being written to the audit trail
                    ! locAuditNotes   = '<13,10>LOST LOAN FEE: ' & Format(job:Invoice_Courier_Cost,@n14.2)
                    ! to (DBH 27/06/2006) #7584
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(job:Invoice_Courier_Cost,@n14.2)
                    ! End (DBH 27/06/2006) #7584
    
                End !If LoanAttachedToJob(job:Ref_Number)
                If jobe:Engineer48HourOption = 1
                    If jobe:ExcReplcamentCharge
                        locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(job:Invoice_Courier_Cost,@n14.2)
                    End !If jobe:ExcReplcamentCharge
                End !If jobe:Engineer48HourOption = 1
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(job:Invoice_Parts_Cost,@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(job:Invoice_Labour_Cost,@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(job:Invoice_Sub_Total,@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                    (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                    (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(job:Invoice_Sub_Total + (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                    (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                    (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100),@n14.2)
                IF (AddToAudit(job:Ref_Number,'JOB','INVOICE CREATED',CLIP(locAuditNotes)))
                END
            END

        ELSE ! IF (inv:ARCInvoiceDate = 0)
        END ! IF (inv:ARCInvoiceDate = 0)
            If AddToAudit(job:Ref_Number,'JOB','INVOICE PRINTED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))
            End ! '<13,10>INVOICE DATE: ' & FOrmat(inv:Date_Created,@d06)

        locInvoiceDate = inv:ARCInvoiceDate
    END !If (glo:WebJob = 1)


! After Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()





MakeInvoiceLog       PROCEDURE  (SentXMLType)         ! Declare Procedure
ComputerName         CSTRING(100)
ComputerUser         CSTRING(100)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
               COMPILE('****',_width32_)
     IF WNetGetUser(GWUN:Local,GWUN:User,GWUN:FieldSize)
               ****
               OMIT('****',_width32_)
     IF WNetGetUser(GWUN:User,GWUN:FieldSize)       ! 0 = OK
               ****
        ComputerUser='Invalid' ! Get User Failed
     ELSE                                 ! 0 = OK
        ComputerUser=GWUN:User ! Get User Succeeded
     .
     IF GetComputerName(JB:ComputerName,JB:ComputerLen).
     ComputerName=JB:ComputerName
!prototype SentXMLType
!written for Harvey request to log all XML generation
!J - 07/05/15

    lineprint('New Invoice XML Generated -------------------- '                  ,CLIP(path())&'\invoicexml.log' )
    lineprint('Invoice No : '&clip(inv:Invoice_Number)                           ,CLIP(path())&'\invoicexml.log' )
    lineprint('XML type   : '&clip(SentXMLType)                                  ,CLIP(path())&'\invoicexml.log' )
    lineprint('Generated  : '&format(Today(),@d06)& ' ' & format(clock(),@t04)   ,CLIP(path())&'\invoicexml.log' )
    lineprint('Computer   : '&clip(ComputerName)                                 ,CLIP(path())&'\invoicexml.log' )
    lineprint('UserName   : '&clip(ComputerUser)                                 ,CLIP(path())&'\invoicexml.log' )
    if clip(glo:ErrorText) <> '' then
        lineprint('Error      : '&cliP(glo:ErrorText)                            ,clip(path())&'\invoicexml.log' )
    ELSE
        Lineprint('Error      : None'                                            ,clip(path())&'\invoicexml.log' )
    END
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
