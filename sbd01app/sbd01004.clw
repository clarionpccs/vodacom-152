

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD01004.INC'),ONCE        !Local module procedure declarations
                     END


Despatch_Note        PROCEDURE                        ! Declare Procedure
tmp:DoNotPrintChargeableDespatchNotes BYTE(0)
webjob_id            USHORT,AUTO
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
isPriceDespatch         byte
isFirstCopyUnpriced     byte
isPrintable             byte
noOfCopies              long
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:TRADEACC.Open
   Relate:SUBTRACC.Open
   Relate:JOBS_ALIAS.Open
    ! Print despatch note
    isPriceDespatch = false
    isFirstCopyUnpriced = false
    isPrintable = true

    noOfCopies = GLO:Select2

    tmp:DoNotPrintChargeableDespatchNotes = GETINI('PRINTING','NoChargeableDespatchNotes',,CLIP(PATH())&'\SB2KDEF.INI')

    Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = GLO:Select1
    if not Access:JOBS_ALIAS.Fetch(job_ali:Ref_Number_Key)

        If job_ali:Exchange_Unit_Number = 0 And job_ali:Loan_Unit_Number = 0
            !Does not include Exchange/Loan Despatch Notes
            if tmp:DoNotPrintChargeableDespatchNotes And job:Warranty_Job <> 'YES'
!                if job_ali:Chargeabe_Job = 'YES'
!                    Case MessageEx('You cannot print despatch notes for chargeable jobs.', |
!                           'ServiceBase 2000',|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
!                        Of 1 ! &OK Button
!                    End!Case MessageEx
                isPrintable = false
!                end
            end

        End !If job_ali:Exchange_Unit_Number = 0 And job_ali:Loan_Unit_Number = 0

        if isPrintable

! Changing (DBH 26/11/2007) # 8919 - Take the despatch defaults for the account of the ccurrent location, e.g. ARC, or RRC
!            Access:SUBTRACC.ClearKey(sub:account_number_key)
!            sub:account_number = job_ali:account_number
!            if not access:SUBTRACC.Fetch(sub:account_number_key)
!                Access:TRADEACC.ClearKey(tra:account_number_key)
!                tra:account_number = sub:main_account_number
!                Access:TRADEACC.Fetch(tra:account_number_key)
!            end
! to (DBH 26/11/2007) # 8919
            If glo:WebJob
                ! If at RRC, use the RRC account's despatch defaults (DBH: 26/11/2007)
                webjob_id = Access:WEBJOB.SaveFile()
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber = job_ali:Ref_Number
                If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number = wob:HeadAccountNumber
                    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

                    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                Access:WEBJOB.RestoreFile(webjob_id)
            Else ! If glo:WebJob
                ! If at ARC, use the account's despatch defaults (DBH: 26/11/2007)
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                If Access:TRADEACC.TryFetch(tra:ACcount_Number_Key) = Level:Benign

                End ! If Access:TRADEACC.TryFetch(tra:ACcount_Number_Key) = Level:Benign
            End ! If glo:WebJob
! End (DBH 26/11/2007) #8919

            ! Trade account settings
            if tra:Num_Despatch_Note_Copies <> 0
                noOfCopies = tra:Num_Despatch_Note_Copies
            end

            if tra:price_despatch = 'YES'
                isPriceDespatch = true
                if tra:Price_First_Copy_Only = true             ! First copy un-priced (field name is wrong way round, due to change in spec.)
                    isFirstCopyUnpriced = true
                end
            end

! Deleting (DBH 26/11/2007) # 8919 - Ignore sub account defaults for despatch notes
!            ! Sub account settings
!            if sub:Num_Despatch_Note_Copies <> 0
!                noOfCopies = sub:Num_Despatch_Note_Copies
!            end
!            if sub:PriceDespatchNotes = true
!                isPriceDespatch = true
!                if sub:Price_First_Copy_Only = true
!                    isFirstCopyUnpriced = true
!                end
!            end
! End (DBH 26/11/2007) #8919

            GLO:Select2 = noOfCopies

            if isPriceDespatch                                  ! Price despatch enabled?
                if isFirstCopyUnpriced                          ! First copy un-priced?
                    GLO:Select2 = 1                             ! Print a single copy
                    Despatch_Note_Without_Pricing()
                    noOfCopies -= 1
                    if noOfCopies < 1 then noOfCopies = 1.
                    GLO:Select2 = noOfCopies
                    Despatch_Note_With_Pricing()
                else                                            ! Price all copies
                    Despatch_Note_With_Pricing()
                end
            else                                                ! Price despatch disabled
                Despatch_Note_Without_Pricing()
            end

        end
    end
   Relate:TRADEACC.Close
   Relate:SUBTRACC.Close
   Relate:JOBS_ALIAS.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()






LoanCollectionNote PROCEDURE
RejectRecord         LONG,AUTO
tmp:Ref_number       STRING(20)
tmp:PrintedBy        STRING(255)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(30)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
customer_name_temp   STRING(60)
sub_total_temp       REAL
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
tmp:LoanExchangeUnit STRING(100)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:accessories      STRING(255)
tmp:OriginalIMEI     STRING(30)
tmp:FinalIMEI        STRING(20)
tmp:DefaultEmailAddress STRING(255)
tmp:EngineerReport   STRING(255)
OutFaults_Q          QUEUE,PRE(ofq)
OutFaultsDescription STRING(60)
                     END
DeliveryAddress      GROUP,PRE(delivery)
CustomerName         STRING(30)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
                     END
endUserTelNo         STRING(15)
clientName           STRING(65)
tmp:Terms            STRING(1000)
DefaultAddress       GROUP,PRE(address)
SiteName             STRING(40)
Name                 STRING(40)
Name2                STRING(40)
Location             STRING(40)
AddressLine1         STRING(40)
AddressLine2         STRING(40)
AddressLine3         STRING(40)
AddressLine4         STRING(40)
Telephone            STRING(40)
RegistrationNo       STRING(40)
Fax                  STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
locTermsText         STRING(255)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Account_Number)
                       PROJECT(job_ali:Authority_Number)
                       PROJECT(job_ali:Consignment_Number)
                       PROJECT(job_ali:Courier)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:Despatch_Number)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Mobile_Number)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Order_Number)
                       PROJECT(job_ali:Time_Completed)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:date_booked)
                       PROJECT(job_ali:time_booked)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,6875,7521,1188),PAPER(PAPER:LETTER),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(417,333,7677,9240),USE(?unnamed)
                         STRING('Job Number: '),AT(4917,396),USE(?String25),TRN,FONT(,8,,)
                         STRING(@s20),AT(6156,396),USE(tmp:Ref_number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Booked: '),AT(4917,719),USE(?String58),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6156,719),USE(job_ali:date_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6760,719),USE(job_ali:time_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Date Completed:'),AT(4917,875),USE(?String85),TRN,FONT(,8,,)
                         STRING('Despatch Batch No:'),AT(4917,563),USE(?String86),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6156,875,635,198),USE(job_ali:Date_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6760,875),USE(job_ali:Time_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@s10b),AT(6156,563),USE(job_ali:Despatch_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s22),AT(1604,3240),USE(job_ali:Order_Number,,?job_ali:Order_Number:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4531,3229),USE(charge_type_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3156,3240),USE(job_ali:Authority_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(6000,3240),USE(repair_type_temp),TRN,FONT(,8,,)
                         STRING(@s20),AT(156,3906,1000,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(5052,3906,1094,156),USE(tmp:FinalIMEI),TRN,HIDE,LEFT,FONT(,8,,)
                         STRING(@s20),AT(1250,3906,1323,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(2604,3906,1396,156),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3906,3906,1094,156),USE(tmp:OriginalIMEI),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(6198,3906),USE(job_ali:MSN),TRN,FONT(,8,,)
                         STRING('REPORTED FAULT'),AT(156,4167),USE(?String64),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,4167,5573,417),USE(jbn_ali:Fault_Description),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('ENGINEER REPORT'),AT(156,4688),USE(?String88),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,4688,5573,781),USE(tmp:EngineerReport),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('EXCHANGE UNIT'),AT(156,5885),USE(?Exchange_Unit),TRN,HIDE,FONT(,9,,FONT:bold)
                         STRING(@s100),AT(1615,5885,5781,208),USE(tmp:LoanExchangeUnit),TRN,HIDE,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('ACCESSORIES'),AT(156,5583),USE(?String105),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,5583,5573,260),USE(tmp:accessories),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('PARTS USED'),AT(156,6094),USE(?Parts_Used),TRN,FONT(,9,,FONT:bold)
                         STRING('Qty'),AT(1521,6094),USE(?Qty),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1917,6094),USE(?Part_Number),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3698,6094),USE(?description),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Cost'),AT(5719,6094),USE(?Unit_Cost),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(6677,6094),USE(?Line_Cost),TRN,HIDE,FONT(,8,,FONT:bold)
                         LINE,AT(1406,6250,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         TEXT,AT(156,7448,5885,260),USE(locTermsText),TRN,FONT(,7,,)
                         STRING('Customer Signature'),AT(156,7813),USE(?Terms2),TRN,FONT(,8,,FONT:bold)
                         STRING('Date: {13}/ {14}/'),AT(5104,7813),USE(?Terms3),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1406,7969,5313,0),USE(?SigLine),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,2188),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4115,2344),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4323,2344),USE(delivery:TelephoneNumber),FONT(,8,,)
                         STRING(@s65),AT(4115,2813,2865,156),USE(clientName),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(2781,2031),USE(job_ali:Mobile_Number),TRN,LEFT,FONT(,8,,)
                         STRING('Mobile:'),AT(2229,2031),USE(?String35:3),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(2229,1719),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2781,1719),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(2229,1875),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2781,1875),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3240),USE(job_ali:Account_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1563),USE(delivery:CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1719,1917,156),USE(delivery:AddressLine1),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1875),USE(delivery:AddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2031),USE(delivery:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2188),USE(delivery:Postcode),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1563),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,115),USE(?DetailBand)
                           STRING(@n8b),AT(1198,0),USE(Quantity_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s25),AT(1917,0),USE(part_number_temp),TRN,FONT(,7,,)
                           STRING(@s25),AT(3677,0),USE(Description_temp),TRN,FONT(,7,,)
                           STRING(@n14.2b),AT(5531,0),USE(Cost_Temp),TRN,HIDE,RIGHT,FONT(,7,,)
                           STRING(@n14.2b),AT(6521,0),USE(line_cost_temp),TRN,HIDE,RIGHT,FONT(,7,,)
                         END
                         FOOTER,AT(396,8802,,1479),USE(?unnamed:2),TOGETHER,ABSOLUTE
                           STRING('Despatch Date:'),AT(156,83),USE(?String66),TRN,FONT(,8,,)
                           STRING(@d6b),AT(1156,83),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING('Courier: '),AT(156,240),USE(?String67),TRN,FONT(,8,,)
                           STRING(@s20),AT(1156,240),USE(job_ali:Courier),TRN,FONT(,8,,FONT:bold)
                           STRING('Labour:'),AT(4844,83),USE(?labour_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,83),USE(labour_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Consignment No:'),AT(156,396),USE(?String70),TRN,FONT(,8,,)
                           STRING(@s20),AT(1156,396),USE(job_ali:Consignment_Number),TRN,FONT(,8,,FONT:bold)
                           STRING(@s20),AT(3125,281),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(2875,83,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Carriage:'),AT(4844,396),USE(?carriage_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,240),USE(parts_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(4844,563),USE(?vat_String),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,396),USE(courier_cost_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s13),AT(1146,552),USE(jobe2:LoanIDNumber),TRN,FONT(,8,,FONT:bold)
                           STRING('ID Number:'),AT(156,552),USE(?String70:2),TRN,FONT(,8,,)
                           STRING('Total:'),AT(4844,729),USE(?total_string),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(6281,719,1000,0),USE(?line),COLOR(COLOR:Black)
                           STRING(@n14.2b),AT(6396,729),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@s16),AT(2865,521,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@n14.2b),AT(6396,563),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('<128>'),AT(6250,729,156,208),USE(?Euro),TRN,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(2875,719,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Parts:'),AT(4844,240),USE(?parts_string),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,9833,7521,1156),USE(?unnamed:3)
                         STRING('Vodacom Repairs Loan Phone Terms And Conditions'),AT(208,0),USE(?String108),TRN,FONT(,7,,FONT:bold+FONT:underline)
                         TEXT,AT(208,156,7031,885),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(396,292,7510,11375),USE(?unnamed:4)
                         IMAGE('RINVDET.GIF'),AT(0,0,7552,10781),USE(?Image1)
                         STRING('LOAN COLLECTION NOTE'),AT(4531,0,2865,260),USE(?Title),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,0,4469,260),USE(address:SiteName),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,365,3073,208),USE(address:Name2),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(104,521,2760,208),USE(address:Location),TRN,FONT(,8,,FONT:bold)
                         STRING('REG NO:'),AT(104,677),USE(?stringREGNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(625,677,1667,146),USE(address:RegistrationNo),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('VAT NO: '),AT(104,781),USE(?stringVATNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s20),AT(625,781,1771,156),USE(address:VatNumber),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,885,2240,156),USE(address:AddressLine1),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,990,2240,156),USE(address:AddressLine2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1094,2240,156),USE(address:AddressLine3),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1198),USE(address:AddressLine4),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('TEL:'),AT(2385,885),USE(?stringTEL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2625,885,1458,208),USE(address:Telephone),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('FAX:'),AT(2385,990,313,208),USE(?stringFAX),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2625,990,1510,188),USE(address:Fax),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('EMAIL:'),AT(104,1323,417,208),USE(?stringEMAIL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s255),AT(625,1313,3333,208),USE(address:EmailAddress),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),TRN,FONT(,9,,FONT:bold)
                         STRING('CUSTOMER ADDRESS'),AT(156,1458),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1458,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING('GENERAL DETAILS'),AT(156,2865),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY DETAILS'),AT(156,8177),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4792,8177),USE(?charge_details),TRN,FONT(,9,,FONT:bold)
                         STRING('Model'),AT(156,3750),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(156,3073),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(1604,3073),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Authority Number'),AT(3083,3073),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(4531,3073),USE(?Chargeable_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(6000,3073),USE(?Repair_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1250,3750),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Exch I.M.E.I. No'),AT(5052,3750),USE(?IMEINo:2),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(2604,3750),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(3906,3750),USE(?IMEINo),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6198,3750),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3490),USE(?String50),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('LoanCollectionNote')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','LoanCollectionNote')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'LoanCollNote'
  If PrintOption(PreviewReq,glo:ExportReport,'Loan Collection Note') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:STANTEXT.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:JOBACC.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  Access:STOCK.UseFile
  Access:WARPARTS.UseFile
  Access:LOAN.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBOUTFL.UseFile
  Access:JOBSE.UseFile
  Access:JOBSE2.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        If job_ali:exchange_unit_number <> ''
            part_number_temp = ''
            Print(rpt:detail)
        Else!If job_ali:exchange_unit_number <> ''
            count# = 0
        
            If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
                setcursor(cursor:wait)
                save_wpr_id = access:warparts.savefile()
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = job_ali:ref_number
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> job_ali:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    part_number_temp = wpr:part_number
                    description_temp = wpr:description
                    quantity_temp = wpr:quantity
                    cost_temp = wpr:purchase_cost
                    line_cost_temp = wpr:quantity * wpr:purchase_cost
                    Print(rpt:detail)
                end !loop
                access:warparts.restorefile(save_wpr_id)
                setcursor()
        
            Else!If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
                setcursor(cursor:wait)
                save_par_id = access:parts.savefile()
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = job_ali:ref_number
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                       break
                    end !if
                    if par:ref_number  <> job_ali:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    description_temp = par:description
                    quantity_temp = par:quantity
                    cost_temp = par:sale_cost
                    part_number_temp = par:part_number
                    line_cost_temp = par:quantity * par:sale_cost
                    Print(rpt:detail)
                end !loop
                access:parts.restorefile(save_par_id)
                setcursor()
            End!If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
            If count# = 0
                part_number_temp = ''
                Print(rpt:detail)
            End!If count# = 0
        End!If job_ali:exchange_unit_number <> ''
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','LoanCollectionNote')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  If glo:Select2 >= 1  ! Was ( > 1 )
      Printer{PropPrint:Copies} = glo:Select2
  End!If glo:Select2 > 1
  !Printer{PropPrint:Paper} = Paper:Letter
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !********************Code added by Paul*****************
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  access:webjob.clearkey(wob:RefNumberKey)
  wob:refnumber = job_ali:Ref_Number
  if access:webjob.fetch(wob:RefNumberKey)
      tmp:ref_number = job_ali:Ref_Number
  else
      access:tradeacc.clearkey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      access:tradeacc.fetch(tra:Account_Number_Key)
      tmp:ref_number = job_ali:ref_number & '-' &tra:BranchIdentification &wob:jobnumber
  
  end
  
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      If (glo:WebJob = 1)
          tra:Account_Number = wob:HeadAccountNumber
      ELSE ! If (glo:WebJob = 1)
          tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      END ! If (glo:WebJob = 1)
      If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          address:SiteName        = tra:Company_Name
          address:Name            = tra:coTradingName
          address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
          address:Location        = tra:coLocation
          address:RegistrationNo  = tra:coRegistrationNo
          address:AddressLine1    = tra:coAddressLine1
          address:AddressLine2    = tra:coAddressLine2
          address:AddressLine3    = tra:coAddressLine3
          address:AddressLine4    = tra:coAddressLine4
          address:Telephone       = tra:coTelephoneNumber
          address:Fax             = tra:coFaxNumber
          address:EmailAddress    = tra:coEmailAddress
          address:VatNumber       = tra:coVATNumber
      END
  
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  !Barcode bit
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(tmp:Ref_number)
  job_number_temp = 'Job No: ' & Clip(tmp:Ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'I.M.E.I. No.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  ! Inserting (DBH 08/06/2006) #7305 - Show the Loan ID Number
  Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
  jobe2:RefNumber = job_ali:Ref_Number
  If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Found
  
  Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
  ! End (DBH 08/06/2006) #7305
  
  Settarget(Report)
  Hide(?labour_string)
  Hide(?labour_temp)
  Hide(?Parts_temp)
  Hide(?parts_string)
  Hide(?Carriage_string)
  Hide(?vat_string)
  Hide(?total_string)
  Hide(?line)
  hide(?unit_Cost)
  hide(?line_cost)
  hide(?cost_temp)
  hide(?line_cost_temp)
  hide(?labour_string)
  hide(?parts_string)
  hide(?courier_cost_temp)
  hide(?vat_temp)
  hide(?total_temp)
  hide(?charge_details)
  ?Euro{prop:Hide} = 1
  Settarget()
  
  If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
      Settarget(Report)
  !    ?Title{Prop:Text} = 'WARRANTY DESPATCH NOTE'
      ?chargeable_type{prop:text} = 'Warranty Type'
      ?repair_type{prop:text} = 'Warranty Repair Type'
  !    Hide(?labour_string)
  !    Hide(?parts_string)
  !    Hide(?Carriage_string)
  !    Hide(?vat_string)
  !    Hide(?total_string)
  !    Hide(?line)
      Settarget()
      charge_type_temp = job_ali:warranty_charge_type
      repair_type_temp = job_ali:repair_type_warranty
  Else
      Settarget(Report)
  !    ?Title{Prop:Text} = 'CHARGEABLE DESPATCH NOTE'
      settarget()
      charge_type_temp = job_ali:charge_type
      repair_type_temp = job_ali:repair_type
  End!If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job_ali:account_number
  IF access:subtracc.fetch(sub:account_number_key)
    !Error!
  END
  access:tradeacc.clearkey(tra:account_number_key)
  tra:account_number = sub:main_account_number
  IF access:tradeacc.fetch(tra:account_number_key)
    !Error!
  END
  !If tra:price_despatch <> 'YES'
  !    Settarget(report)
  !    hide(?unit_Cost)
  !    hide(?line_cost)
  !    hide(?cost_temp)
  !    hide(?line_cost_temp)
  !    hide(?labour_string)
  !    hide(?parts_string)
  !    hide(?carriage_String)
  !    hide(?vat_string)
  !    hide(?total_string)
  !    hide(?labour_temp)
  !    hide(?parts_temp)
  !    hide(?courier_cost_temp)
  !    hide(?vat_temp)
  !    hide(?total_temp)
  !    hide(?charge_details)
  !    ?Euro{prop:Hide} = 1
  !    Settarget()
  !End!If tra:price_despatch <> 'YES'
  !IF sub:PriceDespatchNotes = TRUE
  !  Settarget(report)
  !  unhide(?unit_Cost)
  !  unhide(?line_cost)
  !  unhide(?cost_temp)
  !  unhide(?line_cost_temp)
  !  unhide(?labour_string)
  !  unhide(?parts_string)
  !  unhide(?carriage_String)
  !  unhide(?vat_string)
  !  unhide(?total_string)
  !  unhide(?labour_temp)
  !  unhide(?parts_temp)
  !  unhide(?courier_cost_temp)
  !  unhide(?vat_temp)
  !  unhide(?total_temp)
  !  unhide(?charge_details)
  !  Settarget()
  !END
  
  !*************************set the display address to the head account if booked on as a web job***********************
  if glo:webjob then
      def:User_Name           = tra:Company_Name
      def:Address_Line1       = tra:Address_Line1
      def:Address_Line2       = tra:Address_Line2
      def:Address_Line3       = tra:Address_Line3
      def:Postcode            = tra:Postcode
      tmp:DefaultTelephone    = tra:Telephone_Number
      tmp:DefaultFax          = tra:Fax_Number
      def:EmailAddress        = tra:EmailAddress
  end
  !*********************************************************************************************************************
  
  HideDespAdd# = 0
  if tra:invoice_sub_accounts = 'YES'
      If sub:HideDespAdd = 1
          HideDespAdd# = 1
      End!If sub:HideDespAdd = 1
  
      If SUB:UseCustDespAdd = 'YES'
          invoice_address1_temp     = job_ali:address_line1
          invoice_address2_temp     = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job_ali:address_line3
              invoice_address4_temp  = job_ali:postcode
          End
          invoice_company_name_temp   = job_ali:company_name
          invoice_telephone_number_temp   = job_ali:telephone_number
          invoice_fax_number_temp = job_ali:fax_number
      Else!If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = sub:address_line1
          invoice_address2_temp     = sub:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = sub:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = sub:address_line3
              invoice_address4_temp  = sub:postcode
          End
          invoice_company_name_temp   = sub:company_name
          invoice_telephone_number_temp   = sub:telephone_number
          invoice_fax_number_temp = sub:fax_number
  
  
      End!If sub:invoice_customer_address = 'YES'
  
  else!if tra:use_sub_accounts = 'YES'
      If tra:HideDespAdd = 1
          HideDespAdd# = 1
      End!If tra:HideDespAdd = 'YES'
  
      If TRA:UseCustDespAdd = 'YES'
          invoice_address1_temp     = job_ali:address_line1
          invoice_address2_temp     = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job_ali:address_line3
              invoice_address4_temp  = job_ali:postcode
          End
          invoice_company_name_temp   = job_ali:company_name
          invoice_telephone_number_temp   = job_ali:telephone_number
          invoice_fax_number_temp = job_ali:fax_number
  
      Else!If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = tra:address_line1
          invoice_address2_temp     = tra:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = tra:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = tra:address_line3
              invoice_address4_temp  = tra:postcode
          End
          invoice_company_name_temp   = tra:company_name
          invoice_telephone_number_temp   = tra:telephone_number
          invoice_fax_number_temp = tra:fax_number
  
      End!If tra:invoice_customer_address = 'YES'
  End!!if tra:use_sub_accounts = 'YES'
  
  !!Hide Despatch Address
  !If HideDespAdd# = 1
  !    Settarget(Report)
  !    ?def:User_Name{prop:Hide} = 1
  !    ?def:Address_Line1{prop:Hide} = 1
  !    ?def:Address_Line2{prop:Hide} = 1
  !    ?def:Address_Line3{prop:Hide} = 1
  !    ?def:Postcode{prop:hide} = 1
  !    ?tmp:DefaultTelephone{prop:hide} = 1
  !    ?tmp:DefaultFax{prop:hide} = 1
  !    ?telephone{prop:hide} = 1
  !    ?fax{prop:hide} = 1
  !    ?email{prop:hide} = 1
  !    ?tmp:DefaultEmailAddress{prop:Hide} = 1
  !    Settarget()
  !End!If HideDespAdd# = 1
  
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job_ali:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
        !------------------------------------------------------------------
        IF CLIP(job_ali:title) = ''
            customer_name_temp = job_ali:initial
        ELSIF CLIP(job_ali:initial) = ''
            customer_name_temp = job_ali:title
        ELSE
            customer_name_temp = CLIP(job_ali:title) & ' ' & CLIP(job_ali:initial)
        END ! IF
        !------------------------------------------------------------------
        IF CLIP(customer_name_temp) = ''
            customer_name_temp = job_ali:surname
        ELSIF CLIP(job_ali:surname) = ''
            !customer_name_temp = customer_name_temp ! tautology
        ELSE
            customer_name_temp = CLIP(customer_name_temp) & ' ' & CLIP(job_ali:surname)
        END ! IF
        !------------------------------------------------------------------
  
      endUserTelNo = ''
  
      if jobe:EndUserTelNo <> ''
          endUserTelNo = clip(jobe:EndUserTelNo)
      end
  
      if customer_name_temp <> ''
          clientName = 'Client: ' & clip(customer_name_temp) & '  Tel: ' & clip(endUserTelNo)
      else
          if endUserTelNo <> ''
              clientName = 'Tel: ' & clip(endUserTelNo)
          end
      end
  
  delivery:TelephoneNumber    = job_ali:Telephone_Delivery
  delivery:CompanyName        = job_ali:Company_Name_Delivery
  delivery:AddressLine1     = job_ali:address_line1_delivery
  delivery:AddressLine2     = job_ali:address_line2_delivery
  If job_ali:address_line3_delivery   = ''
      delivery:AddressLine3 = job_ali:postcode_delivery
      delivery:Postcode = ''
  Else
      delivery:AddressLine3  = job_ali:address_line3_delivery
      delivery:Postcode  = job_ali:postcode_delivery
  End
  
  if job_ali:title = '' and job_ali:initial = ''
      delivery:CustomerName = clip(job_ali:surname)
  elsif job_ali:title = '' and job_ali:initial <> ''
      delivery:CustomerName = clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial = ''
      delivery:CustomerName = clip(job_ali:title) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial <> ''
      delivery:CustomerName = clip(job_ali:title) & ' ' & clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  else
      delivery:CustomerName = ''
  end
  
  
  !Change the delivery address to the RRC, if it's at the ARC
  !going back to the RRC, otherwise its the delivery address on the job
  If ~glo:WebJob And jobe:WebJob
      Access:WEBJOB.Clearkey(wob:RefNumberKey)
      wob:RefNumber   = job_ali:Ref_Number
      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          !Found
          Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
          tra_ali:Account_Number  = wob:HeadAccountNumber
          If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
              !Found
              delivery:CustomerName   = tra_ali:Account_Number
              delivery:CompanyName    = tra_ali:Address_Line1
              delivery:AddressLine1   = tra_ali:Address_Line2
              delivery:AddressLine2   = tra_ali:Address_Line3
              delivery:AddressLine3   = tra_ali:Postcode
              delivery:Postcode       = ''
              delivery:TelephoneNumber= tra_ali:Telephone_Number
  
          Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          !Error
      End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  Else !glo:WebJob And jobe:WebJob
  
  End !glo:WebJob And jobe:WebJob
  
  
  
  
  labour_temp = ''
  parts_temp = ''
  courier_cost_temp = ''
  vat_temp = ''
  total_temp = ''
  If job_ali:chargeable_job = 'YES'
  
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job_ali:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      If job_ali:invoice_number <> ''
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job_ali:invoice_number
          access:invoice.fetch(inv:invoice_number_key)
          If glo:WebJob
              If inv:ExportedRRCOracle
                  Labour_Temp = jobe:InvRRCCLabourCost
                  Parts_Temp  = jobe:InvRRCCPartsCost
                  Courier_Cost_Temp = job_ali:Invoice_Courier_Cost
                  Vat_Temp    = (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour/100) + |
                                  (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts/100) + |
                                  (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100)
              Else !If inv:ExportedRRCOracle
                  Labour_Temp = jobe:RRCCLabourCost
                  Parts_Temp  = jobe:RRCCPartsCost
                  Courier_Cost_Temp = job_ali:Courier_Cost
                  Vat_Temp    = (jobe:RRCCLabourCost * VatRate(job_ali:Account_Number,'L')/100) + |
                                  (jobe:RRCCPartsCost * VatRate(job_ali:Account_Number,'P')/100) + |
                                  (job:Courier_Cost * VatRate(job_ali:Account_Number,'L')/100)
              End !If inv:ExportedRRCOracle
          Else !If glo:WebJob
              labour_temp       = job_ali:invoice_labour_cost
              parts_temp        = job_ali:invoice_parts_cost
              courier_cost_temp = job_ali:invoice_courier_cost
              Vat_Temp    = (job_ali:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                                  (job_ali:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                                  (job_ali:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100)
          End !If glo:WebJob
  
      Else!If job_ali:invoice_number <> ''
          If glo:WebJob
              Labour_Temp = jobe:RRCCLabourCost
              Parts_Temp  = jobe:RRCCPartsCost
              Courier_Cost_Temp = job_ali:Courier_Cost
              Vat_Temp    = (jobe:RRCCLabourCost * VatRate(job_ali:Account_Number,'L')/100) + |
                              (jobe:RRCCPartsCost * VatRate(job_ali:Account_Number,'P')/100) + |
                              (job:Courier_Cost * VatRate(job_ali:Account_Number,'L')/100)
  
          Else !If glo:WebJob
              Labour_Temp = job_ali:Labour_Cost
              Parts_temp  = job_ali:Parts_Cost
              Courier_Cost_temp    = job_ali:Courier_Cost
              Vat_Temp    = (job_ali:Labour_Cost * VatRate(job_ali:Account_Number,'L')/100) + |
                              (job_ali:Parts_Cost * VatRate(job_ali:Account_Number,'P')/100) + |
                              (job_ali:Courier_Cost * VatRate(job_ali:Account_Number,'L')/100)
          End !If glo:WebJob
  
      End!If job_ali:invoice_number <> ''
      total_temp = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
  End!If job_ali:chargeable_job <> 'YES'
  
  If job_ali:Warranty_Job = 'YES'
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job_ali:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      If job_ali:invoice_number_warranty <> ''
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job_ali:invoice_number_Warranty
          access:invoice.fetch(inv:invoice_number_key)
          If glo:WebJob
              Labour_Temp = jobe:InvRRCWLabourCost
              Parts_Temp  = jobe:InvRRCWPartsCost
              Courier_Cost_Temp = job_ali:WInvoice_Courier_Cost
              Vat_Temp    = (jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour/100) + |
                              (jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts/100) + |
                              (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100)
          Else !If glo:WebJob
              labour_temp       = jobe:InvoiceClaimValue
              parts_temp        = job_ali:Winvoice_parts_cost
              courier_cost_temp = job_ali:Winvoice_courier_cost
              Vat_Temp    = (jobe:InvoiceClaimValue * inv:Vat_Rate_Labour/100) + |
                                  (job_ali:WInvoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                                  (job_ali:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100)
          End !If glo:WebJob
  
      Else!If job_ali:invoice_number <> ''
          If glo:WebJob
              Labour_Temp = jobe:RRCWLabourCost
              Parts_Temp  = jobe:RRCWPartsCost
              Courier_Cost_Temp = job_ali:Courier_Cost_Warranty
              Vat_Temp    = (jobe:RRCWLabourCost * VatRate(job_ali:Account_Number,'L')/100) + |
                              (jobe:RRCWPartsCost * VatRate(job_ali:Account_Number,'P')/100) + |
                              (job:Courier_Cost_Warranty * VatRate(job_ali:Account_Number,'L')/100)
  
          Else !If glo:WebJob
              Labour_Temp = jobe:ClaimValue
              Parts_temp  = job_ali:Parts_Cost_Warranty
              Courier_Cost_temp    = job_ali:Courier_Cost_Warranty
              Vat_Temp    = (jobe:ClaimValue * VatRate(job_ali:Account_Number,'L')/100) + |
                              (job_ali:Parts_Cost_Warranty * VatRate(job_ali:Account_Number,'P')/100) + |
                              (job_ali:Courier_Cost_Warranty * VatRate(job_ali:Account_Number,'L')/100)
          End !If glo:WebJob
  
      End!If job_ali:invoice_number <> ''
      total_temp = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
  End !job_ali:Warranty_Job = 'YES'
  
  access:users.clearkey(use:user_code_key)
  use:user_code = job_ali:despatch_user
  If access:users.fetch(use:user_code_key) = Level:Benign
      despatched_user_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  End!If access:users.fetch(use:user_code_key) = Level:Benign
  
  setcursor(cursor:wait)
  tmp:accessories= ''
  save_jac_id = access:jobacc.savefile()
  access:jobacc.clearkey(jac:ref_number_key)
  jac:ref_number = job_ali:ref_number
  set(jac:ref_number_key,jac:ref_number_key)
  loop
      if access:jobacc.next()
         break
      end !if
      if jac:ref_number <> job_ali:ref_number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      If tmp:accessories = ''
          tmp:accessories = jac:accessory
      Else!If tmp:accessories = ''
          tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
      End!If tmp:accessories = ''
  end !loop
  access:jobacc.restorefile(save_jac_id)
  setcursor()
  
  
  Access:JOBOUTFL.ClearKey(joo:LevelKey) ! Added by Gary (28th August 2002)
  joo:JobNumber = job_ali:ref_number
  set(joo:LevelKey,joo:LevelKey)
  loop until Access:JOBOUTFL.Next()
      if joo:JobNumber <> job_ali:ref_number then break.
      OutFaults_Q.ofq:OutFaultsDescription = joo:Description
      get(OutFaults_Q,OutFaults_Q.ofq:OutFaultsDescription) ! Queue is used to prevent duplicates
      if error()
          tmp:EngineerReport = clip(tmp:EngineerReport) & clip(joo:Description) & '<13,10>'
          OutFaults_Q.ofq:OutFaultsDescription = joo:Description
          add(OutFaults_Q,OutFaults_Q.ofq:OutFaultsDescription)
      end
  end
  
  exchange_note# = 0
  
  !Exchange Loan
  If job_ali:exchange_unit_number <> ''
      exchange_note# = 1
  End!If job_ali:exchange_unit_number <> ''
  
  !Check Chargeable Parts for an "Exchange" part
  
  save_par_id = access:parts.savefile()
  access:parts.clearkey(par:part_number_key)
  par:ref_number  = job_ali:ref_number
  set(par:part_number_key,par:part_number_key)
  loop
      if access:parts.next()
         break
      end !if
      if par:ref_number  <> job_ali:ref_number      |
          then break.  ! end if
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number = par:part_ref_number
      if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
          If sto:ExchangeUnit = 'YES'
              exchange_note# = 1
              Break
          End!If sto:ExchangeUnit = 'YES'
      end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
  end !loop
  access:parts.restorefile(save_par_id)
  
  If exchange_note# = 0
  !Check Warranty Parts for an "Exchange" part
      save_wpr_id = access:warparts.savefile()
      access:warparts.clearkey(wpr:part_number_key)
      wpr:ref_number  = job_ali:ref_number
      set(wpr:part_number_key,wpr:part_number_key)
      loop
          if access:warparts.next()
             break
          end !if
          if wpr:ref_number  <> job_ali:ref_number      |
              then break.  ! end if
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = wpr:part_ref_number
          if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
              If sto:ExchangeUnit = 'YES'
                  exchange_note# = 1
                  Break
              End!If sto:ExchangeUnit = 'YES'
          end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
      end !loop
      access:warparts.restorefile(save_wpr_id)
  End!If exchange_note# = 0
  
  If job_ali:loan_unit_number <> ''
      Settarget(report)
      ?exchange_unit{prop:text} = 'LOAN UNIT'
      If job_ali:despatch_Type = 'LOA'
   !       ?title{prop:text} = 'LOAN DESPATCH NOTE'
      End!If job_ali:despatch_Type = 'EXC'
      Unhide(?exchange_unit)
      Unhide(?tmp:LoanExchangeUnit)
      access:loan.clearkey(loa:ref_number_key)
      loa:ref_number = job_ali:loan_unit_number
      if access:loan.tryfetch(loa:ref_number_key) = Level:Benign
          tmp:LoanExchangeUnit = '(' & Clip(loa:Ref_number) & ') ' & CLip(loa:manufacturer) & ' ' & CLip(loa:model_number) &|
                                 ' - ' & CLip(loa:esn)
      end
      Settarget()
  
  End!If job_ali:exchange_unit_number <> ''
  
  tmp:OriginalIMEI = job_ali:ESN
  
  !Was the unit exchanged at Third Party?
  If job_ali:Third_Party_Site <> ''
      IMEIError# = 0
      If job_ali:Third_Party_Site <> ''
          Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
          jot:RefNumber = job_ali:Ref_Number
          Set(jot:RefNumberKey,jot:RefNumberKey)
          If Access:JOBTHIRD.NEXT()
              IMEIError# = 1
          Else !If Access:JOBTHIRD.NEXT()
              If jot:RefNumber <> job_ali:Ref_Number
                  IMEIError# = 1
              Else !If jot:RefNumber <> job_ali:Ref_Number
                  If jot:OriginalIMEI <> job_ali:esn
                      IMEIError# = 0
                  Else
                      IMEIError# = 1
                  End !If jot:OriginalIMEI <> job_ali:esn
  
              End !If jot:RefNumber <> job_ali:Ref_Number
          End !If Access:JOBTHIRD.NEXT()
      Else !job_ali:Third_Party_Site <> ''
          IMEIError# = 1
      End !job_ali:Third_Party_Site <> ''
  
      If IMEIError# = 1
          tmp:OriginalIMEI     = job_ali:esn
      Else !IMEIError# = 1
          Settarget(Report)
          tmp:OriginalIMEI     = jot:OriginalIMEI
          tmp:FinalIMEI        = job_ali:esn
          ?IMEINo{prop:text}   = 'Orig I.M.E.I. No'
          ?IMEINo:2{prop:hide} = 0
          ?tmp:FinalIMEI{prop:hide} = 0
          Settarget()
      End !IMEIError# = 1
  End !job_ali:Third_Party_Site <> ''
  
  If exchange_note# = 1
      Settarget(report)
  !    ?title{prop:text} = 'EXCHANGE DESPATCH NOTE'
      Unhide(?exchange_unit)
      Unhide(?tmp:LoanExchangeUnit)
      Hide(?parts_used)
      Hide(?qty)
      Hide(?part_number)
      Hide(?Description)
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number = job_ali:exchange_unit_number
      if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
          tmp:LoanExchangeUnit = '(' & Clip(xch:Ref_number) & ') ' & CLip(xch:manufacturer) & ' ' & CLip(xch:model_number) &|
                                  ' - ' & CLip(xch:esn)
          tmp:OriginalIMEI    = job_ali:ESN
          tmp:FinalIMEI       = xch:ESN
          ?tmp:FinalIMEI{prop:Hide} = 0
          ?IMEINo:2{prop:Hide} = 0
      end
      Settarget()
  End!If exchange_note# = 1
  
  !Get Job Notes!
  Access:JobNotes_Alias.ClearKey(jbn_ali:RefNumberKey)
  jbn_ali:RefNumber = job_ali:Ref_Number
  IF Access:JobNotes_Alias.Fetch(jbn_ali:RefNumberKey)
    !Error!
  END
  !Use Alternative Contact Nos
  Case UseAlternativeContactNos(job_ali:Account_Number,0)
      Of 1
          tmp:DefaultEmailAddress = tra:AltEmailAddress
          tmp:DefaultTelephone    = tra:AltTelephoneNumber
          tmp:DefaultFax          = tra:AltFaxNumber
      Of 2
          tmp:DefaultEmailAddress = sub:AltEmailAddress
          tmp:DefaultTelephone    = sub:AltTelephoneNumber
          tmp:DefaultFax          = sub:AltFaxNumber
      Else
          tmp:DefaultEmailAddress = def:EmailAddress
  End !UseAlternativeContactNos(job_ali:Account_Number)
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'LOAN COLLECTION NOTE'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  
  !tmp:Terms = '1) The loan phone and its accessories remain the property of Vodacom Service Provider Company and is given to the customer to use while his / her phone is being repaired. It is the customers responsibility to return the loan phone and its accessories in proper working condition.<13,10>'
  !tmp:Terms = Clip(tmp:Terms) & '2) In the event of damage to the loan phone and/or its accessories, the customer will be liable for any costs incurred to repair or replace the loan phone and/or its accessories. <13,10>'
  !tmp:Terms = Clip(tmp:Terms) & '3) In the event of loss or theft of the loan phone and/or its accessories the customer will be liable for the replacement cost of the loan phone and/or its accessories or he/she may replace the lost/stolen phone and/or its accessories with new ones of the same or similar make and model, or same replacement value.<13,10>'
  !tmp:Terms = Clip(tmp:Terms) & '4) Any phone replaced by the customer must be able to operate on the Vodacom network. <13,10>'
  !tmp:Terms = Clip(tmp:Terms) & '5) The customers repaired phone will not be returned until the loan phone and its accessories has been returned, repaired or replaced. '
  
  locTermsText = GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI')   ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='LoanCollectionNote'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END













RRCWarrantyInvoice PROCEDURE(func:InvoiceNumber)
save_wob_id          USHORT,AUTO
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
DefaultAddress       GROUP,PRE(address)
Name                 STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
Telephone            STRING(30)
Fax                  STRING(30)
EmailAddress         STRING(255)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:BatchNumber      LONG
tmp:Manufacturer     STRING(30)
tmp:Claims           LONG
tmp:Labour           REAL
tmp:Parts            REAL
tmp:Total            REAL
tmp:JobNumber        STRING(20)
tmp:PartsTotal       REAL
tmp:LabourTotal      REAL
tmp:SubTotalTotal    REAL
tmp:InvoiceNumber    LONG
tmp:JobsCount        LONG
tmp:ReportInvoiceNumber STRING(30)
tmp:TradeID          STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:VAT              REAL
tmp:GrandTotal       REAL
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Labour_Paid)
                       PROJECT(inv:Parts_Paid)
                       PROJECT(inv:Reconciled_Date)
                       PROJECT(inv:Total)
                       PROJECT(inv:jobs_count)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Invoice No:'),AT(5000,104),USE(?String26),TRN,FONT(,10,,)
                         STRING(@s30),AT(6042,104),USE(tmp:ReportInvoiceNumber),TRN,LEFT,FONT(,10,,FONT:bold)
                         STRING('Reconciled Date:'),AT(5000,365),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6042,365),USE(inv:Reconciled_Date),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s8),AT(6042,521),USE(inv:jobs_count),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Claims On Invoice:'),AT(5000,521),USE(?string22:2),TRN,FONT(,8,,)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,135),USE(?detailband)
                           STRING(@n10.2),AT(6719,0),USE(jobe:RRCWSubTotal),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s15),AT(1250,0),USE(job:Manufacturer),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s20),AT(2240,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s16),AT(156,0),USE(tmp:JobNumber),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(3542,0),USE(job:ESN),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@d6),AT(4844,0),USE(job:Date_Completed),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(5521,0),USE(jobe:InvRRCWPartsCost),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(6146,0),USE(jobe:InvRRCWLabourCost),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,729),USE(?unnamed:2)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Claims:'),AT(156,104),USE(?String38),TRN,FONT(,8,,FONT:bold)
                           STRING(@n10.2),AT(5521,104),USE(inv:Parts_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING(@n10.2),AT(6146,104),USE(inv:Labour_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING(@n10.2),AT(6719,104),USE(inv:Total),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.:'),AT(6042,313),USE(?String41),TRN,FONT(,8,,)
                           STRING(@n10.2),AT(6719,313),USE(tmp:VAT),TRN,RIGHT,FONT(,8,,)
                           LINE,AT(6510,469,885,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING(@n14.2),AT(6500,521),USE(tmp:GrandTotal),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('Total:'),AT(6042,521),USE(?String41:2),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(990,104),USE(tmp:JobsCount),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(address:Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('WARRANTY CLAIM PAYMENT '),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,10,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(address:AddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(address:AddressLine2),TRN,FONT(,9,,)
                         STRING('RECONCILIATION DOCUMENT'),AT(5500,156),USE(?String29),TRN,RIGHT,FONT(,10,,FONT:bold)
                         STRING(@s30),AT(156,573,2240,156),USE(address:AddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s30),AT(542,1042),USE(address:Telephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s30),AT(542,1198),USE(address:Fax),TRN,FONT(,9,,)
                         STRING(@s255),AT(542,1354),USE(address:EmailAddress),TRN,FONT(,9,,)
                         STRING('Job No'),AT(156,2031),USE(?String30),TRN,FONT(,8,,FONT:bold)
                         STRING('Manufacturer'),AT(1250,2031),USE(?String30:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(2240,2031),USE(?String30:3),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(3542,2031),USE(?String30:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Completed'),AT(4823,2031),USE(?String30:5),TRN,FONT(,8,,FONT:bold)
                         STRING('Parts'),AT(5833,2031),USE(?String30:6),TRN,FONT(,8,,FONT:bold)
                         STRING('Labour'),AT(6354,2031),USE(?String30:7),TRN,FONT(,8,,FONT:bold)
                         STRING('Sub Total'),AT(6802,2031),USE(?String30:8),TRN,FONT(,8,,FONT:bold)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('RRCWarrantyInvoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  tmp:InvoiceNumber   = func:InvoiceNumber
      ! Save Window Name
   AddToLog('Report','Open','RRCWarrantyInvoice')
  ! After Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'WarrInvoice'
  If PrintOption(PreviewReq,glo:ExportReport,'Warranty Invoice') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('tmp:InvoiceNumber',tmp:InvoiceNumber)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EDIBATCH.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = tmp:InvoiceNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        Save_wob_ID = Access:WEBJOB.SaveFile()
        Access:WEBJOB.ClearKey(wob:RRCWInvoiceNumberKey)
        wob:RRCWInvoiceNumber = inv:Invoice_Number
        Set(wob:RRCWInvoiceNumberKey,wob:RRCWInvoiceNumberKey)
        Loop
            If Access:WEBJOB.NEXT()
               Break
            End !If
            If wob:RRCWInvoiceNumber <> inv:Invoice_Number      |
                Then Break.  ! End If
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = wob:RefNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
        
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber = wob:RefNumber
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
        
            Else !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            tmp:JobNumber = Clip(wob:RefNumber) & '-' & Clip(tmp:TradeID) & Clip(wob:JobNumber)
            tmp:JobsCount += 1
            Print(rpt:Detail)
        End !Loop
        Access:WEBJOB.RestoreFile(Save_wob_ID)
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','RRCWarrantyInvoice')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EDIBATCH.Close
    Relate:INVOICE.Close
    Relate:WEBJOB.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !*********CHANGE LICENSE ADDRESS*********
  access:tradeacc.clearkey(tra:account_number_key)
  tra:account_number = inv:Account_Number
  IF access:tradeacc.fetch(tra:account_number_key)
    !Error!
  END
  address:Name            = tra:Company_Name
  address:AddressLine1    = tra:Address_Line1
  address:AddressLine2    = tra:Address_Line2
  address:AddressLine3    = tra:Address_Line3
  address:Postcode        = tra:Postcode
  address:Telephone       = tra:Telephone_Number
  address:Fax             = tra:Fax_Number
  address:EmailAddress    = tra:EmailAddress
  
  tmp:ReportInvoiceNumber = Clip(inv:Invoice_Number) & '/' & Clip(tra:BranchIdentification)
  tmp:TradeID = tra:BranchIdentification
  
  tmp:VAT = Round(inv:Labour_Paid * inv:Vat_Rate_Labour/100 +|
              inv:Parts_Paid * inv:Vat_Rate_Parts/100,.01)
  tmp:GrandTotal = Round(inv:Total + tmp:VAT,.01)
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='RRCWarrantyInvoice'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END







Warranty_Delivery_Note PROCEDURE                      !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
Tmp:DefaultHeadAccount STRING(30)
save_subtracc_id     USHORT,AUTO
save_tradeacc_id     USHORT,AUTO
save_jobse_id        USHORT,AUTO
Parameter_Group      GROUP,PRE(param)
JobNumber            LONG
Username             STRING(3)
                     END
Local_Group          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase 2000')
BatchNumber          LONG
CommentText          STRING(100)
DesktopPath          STRING(255)
FileName             STRING(255)
Path                 STRING(255)
ProgramName          STRING(100)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED !OLE Automation holder
excel:ColumnName     STRING(100)
excel:ColumnWidth    REAL
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
Excel:Visible        BYTE(0)
                     END
Misc_Group           GROUP,PRE()
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
debug:Count          LONG
debug:Active         LONG
OPTION1              SHORT
RecordCount          LONG
Result               BYTE
Save_Job_ID          ULONG,AUTO
SAVEPATH             STRING(255)
TotalCharacters      LONG
                     END
LOC:HeadAccountNumber STRING(15)
HeadAccountQueue     QUEUE,PRE(hq)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
JobsBooked           LONG
                     END
LOC:SubAccountNumber STRING(15)
SubAccountQueue      QUEUE,PRE(sq)
HeadAccountNumber    STRING(15)
SubAccountNumber     STRING(15)
SubAccountName       STRING(30)
JobsBooked           LONG
                     END
Progress_Group       GROUP,PRE(progress)
Text                 STRING(100)
NextCancelCheck      TIME
Count                LONG
CancelPressed        LONG
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('E')
DataLastCol          STRING('S')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(2048)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
HTML_Queue           QUEUE,PRE(html)
CharPos              LONG
Length               LONG
LINE                 STRING(255)
                     END
NetTalk_Group        GROUP,PRE()
EmailServer          STRING(80)
EmailPort            USHORT
EmailFrom            STRING(255)
EmailTo              STRING(1024)
EmailSubject         STRING(255)
EmailCC              STRING(1024)
EmailFileList        STRING(1024)
EmailBCC             STRING(1024)
EmailMessageText     STRING(16384)
                     END
PrinterinfoGrp       GROUP(PRINTER_INFO_5),PRE()
                     END
DocInfoGrp           GROUP(DOC_INFO_1),PRE()
                     END
Printer_Group        GROUP,PRE(prn)
PageLength           LONG(48)
DetailRows           LONG(14)
Local_Printer_Name   STRING(80)
hPrinter             UNSIGNED
szPrinterName        CSTRING(260)
dwBytesCopied        LONG
dwWritten            LONG
szBuffer             CSTRING(30000)
szDocName            CSTRING(80)
szDataType           CSTRING(80)
                     END
BlankLine            STRING(80)
AddressLine          GROUP,PRE(address),OVER(BlankLine)
Delivery             STRING(30)
Filler1              STRING(4)
Business             STRING(30)
                     END
OrderLine            GROUP,PRE(order),OVER(BlankLine)
DespatchDate         STRING(10)
Filler1              STRING(4)
Filler2              STRING(7)
InvoiceNumber        STRING(10)
Filler4              STRING(1)
AccountNumber        STRING(12)
Filler3              STRING(1)
YourOrderNumber      STRING(20)
                     END
IMEILine             GROUP,PRE(imeitmp),OVER(BlankLine)
Make                 STRING(14)
Model                STRING(15)
SerialNumber         STRING(23)
IMEINumber           STRING(20)
                     END
NarrativeLine        GROUP,PRE(narrative),OVER(BlankLine)
Text1                STRING(55)
Filler1              STRING(10)
Amount               STRING(10)
                     END
DateLine             GROUP,PRE(date),OVER(BlankLine)
Filler0              STRING(10)
DateOut              STRING(10)
Filler1              STRING(22)
TimeOut              STRING(10)
Filler2              STRING(5)
Engineer             STRING(3)
                     END
Detail_Queue         QUEUE,PRE(detail)
Line                 STRING(56)
Filler1              STRING(10)
Amount               STRING(10)
                     END
Address_Group        GROUP,PRE(address)
User_Name            STRING('''INSERT USER NAME'' {12}')
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Postcode             STRING(15)
Telephone_Number     STRING(15)
VatNumber            STRING(30)
                     END
AddressArray         STRING(30),DIM(2,8)
szHeader             CSTRING(5000)
szFooter             CSTRING(5000)
RowNum               LONG
tmp:VatNumber        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ProgressWindow       WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,HIDE,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Printing Delivery Note'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Printing Delivery Note'),USE(?Tab1)
                           PROGRESS,USE(?progress:thermometer),HIDE,AT(248,222,184,10),RANGE(0,100)
                           STRING('Printing Delivery Note'),AT(260,194,160,20),USE(?progress:userstring),CENTER,FONT('Tahoma',14,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(368,258),USE(?ProgressCancel),TRN,FLAT,LEFT,TIP('Click to close window'),ICON('cancelp.jpg')
                       STRING(''),AT(248,266,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
                     END

InvoiceAddressVector    EQUATE(1)
DeliveryAddressVector   EQUATE(2)

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
AddAddress      PROCEDURE()
AddAddressLine  PROCEDURE( LONG, STRING )
AddDateInLine PROCEDURE()
AddDateOutLine     PROCEDURE()
AddIMEILine     PROCEDURE()
AddLine         PROCEDURE()
AddNarrativeLine PROCEDURE()

AddOrderLine    PROCEDURE()
After   PROCEDURE ( STRING, STRING ), STRING
AppendString    PROCEDURE(STRING, STRING, STRING), STRING
Before                    PROCEDURE ( STRING, STRING ), STRING
CalcLabour          PROCEDURE(), REAL
DateToString PROCEDURE(DATE), STRING
GetCustomerName PROCEDURE(), STRING
GetDeliveryTime PROCEDURE(), STRING
GetExpectedShipDate PROCEDURE(), STRING!(2)
GetFaultDescriptionLine PROCEDURE( STRING, *STRING, *STRING )
GetParameters       PROCEDURE(), LONG ! BOOL
GetPrinterName PROCEDURE(), LONG ! BOOL
GetUserName  PROCEDURE( STRING ), LONG ! BOOL
InsertAmountToDetailQueue       PROCEDURE( LONG, REAL )
IsStockPart PROCEDURE( LONG ), LONG ! BOOL
IsWebJob    PROCEDURE(), LONG ! BOOL
LoadINVOICE PROCEDURE( LONG ), LONG ! BOOL
LoadJobNotes PROCEDURE( LONG ), LONG ! BOOL
LoadStock PROCEDURE( LONG ), LONG ! BOOL
LoadSUBTRACC        PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC        PROCEDURE( STRING ), LONG ! BOOL
OpenPrinter                 PROCEDURE(), STRING ! WHERE '' = SUCCESS, ERROR MESSAGE OTHERWISE
ClosePrinter PROCEDURE()
ReplaceString               PROCEDURE ( STRING, STRING, STRING ), STRING
SetupInvoiceAddress     PROCEDURE()
UpdateScreen PROCEDURE( STRING ), LONG, PROC ! BOOL
WriteDebug  PROCEDURE( STRING )
    END ! MAP
! ProgressWindow Declarations

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
!progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
!       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
!       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
!       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
!       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
!     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!-----------------------------------------------
PrintDeliveryNote                     ROUTINE
    DATA
Temp      STRING(255)
    CODE
        !------------------------------------------------------------------
        debug:Active = False

        SetupInvoiceAddress()
        !------------------------------------------------------------------
        IF NOT GetPrinterName()
            Case Missive('Unable to find Delivery Note printer name.' & CLIP(Temp),'ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)
            EXIT
        END !IF

        Temp = OpenPrinter()
            !------------------------------------------------------------------
            IF CLIP(Temp) <> ''
                Case Missive('Error Opening Printer.<13,10,13,10>' & CLIP(Temp),'ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive

                POST(Event:CloseWindow)
                EXIT
            END !IF
            !------------------------------------------------------------------
            DO CreateHeader
            DO CreateFooter
            DO CreatePartsList ! Fill Detail_Queue with 1. text lines, 2. parts lines, 3. out fault lines
            DO InsertAmounts   ! Fill Detail_Queue amount column

            WriteDebug('RECORDS(Detail_Queue)=' & RECORDS(Detail_Queue))

            Loop x# = 1 TO RECORDS(Detail_Queue)
                !--------------------------------------------------------------
                GET(Detail_Queue, x#)

                AddNarrativeLine()
                !--------------------------------------------------------------
                IF (X# % prn:DetailRows) = 0
                    WriteDebug('DO PrintJob(' & x# & ')')
                    DO PrintJob
                END !IF
                !--------------------------------------------------------------
            END !LOOP
            !------------------------------------------------------------------
        ClosePrinter()
        Case Missive('Printing Complete.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
!-----------------------------------------------
CreateHeader        ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        !Dec.     Hex.    Mnemonic         Function
        !
        ! 0       00      NUL              Command terminator
        ! 7       07      BEL              Beeper
        ! 8       08      BS               Backspace
        ! 9       09      HT               Horizontal Tab
        !10       0A      LF               Line Feed
        !11       0B      VT               Vertical Tab
        !12       0C      FF               Form Feed
        !
        !Escape Sequence Codes:
        !Dec.             Hex.           Mnemonic      Function
        !27 67 n          1B 43 n        ESC C n       Set Page Length in Lines
        !
        prn:szBuffer = CHR(27) & CHR(67) & CHR(prn:PageLength)
            AddLine()
            AddLine()
            AddLine()
            AddLine()
            AddLine()
            AddLine()
            AddLine()

        AddAddress()
            AddLine()
            AddLine()
            AddLine()
            AddLine()

        AddOrderLine()
            AddLine()
            AddLine()
            AddLine()
            AddLine()

        AddIMEILine()
            AddLine()

        !------------------------------------------------------------------
        szHeader = CLIP(prn:szBuffer)
        prn:szBuffer = ''
        !------------------------------------------------------------------
    EXIT
CreateFooter        ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        prn:szBuffer = ''
            AddLine()
            AddDateInLine()
            AddLine()
            AddDateOutLine()

        szFooter = CLIP(prn:szBuffer) & CHR(12) ! FORM FEED
        prn:szBuffer = ''
        !------------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreatePartsList             ROUTINE
    DATA
Temp1      STRING(255)
Temp2      STRING(255)
    CODE
        !-----------------------------------------------------------------
        ! 14 rows
        !-----------------------------------------------------------------
        FREE( Detail_Queue)
        CLEAR(Detail_Queue)

        !Show Warranty Status -  (DBH: 02-12-2003)
        detail:Line = 'WARRANTY STATUS: ' & Left(job:Warranty_Charge_Type)
        detail:Amount = ''
        Add(Detail_Queue)

        IF LoadJobNotes(job:Ref_Number) = True
            GetFaultDescriptionLine ( jbn:Fault_Description, Temp1, Temp2 )
                detail:Line   = Temp1
                detail:Amount = ''
                ADD(Detail_Queue)

                detail:Line   = Temp2
                detail:Amount = ''
                ADD(Detail_Queue)
        ELSE
            detail:Line   = '' ! CreatePartsList Debug Fault description 1'
            detail:Amount = ''
            ADD(Detail_Queue)

            detail:Line   = '' ! CreatePartsList Debug Fault description 2'
            detail:Amount = ''
            ADD(Detail_Queue)
        END !IF
        !-----------------------------------------------------------------
        !               |-------------------------------------------------------|
        !               |----Reference-------------------QTY------Cost----Amount
        !               |1234*23456789012345678901234567*234*234567890*234567890
        ! detail:Line   = ' NO REFERENCE                                          ' ! See change request 0232 below
        !etail:Line   = ' ## REFERENCE               QUANTITY      COST    AMOUNT'
        !detail:Amount = ''
        !ADD(Detail_Queue)
        !-----------------------------------------------------------------
        detail:Line   = ALL('-', SIZE(narrative:Text1))
        detail:Amount = ''
        ADD(Detail_Queue)
        !-----------------------------------------------------------------
        DO AddParts
!        !-----------------------------------------------------------------
!        ! 0239 4a)Require a line to separate the fault description and the out fault text
!        !
!        detail:Line   = ''
!        detail:Amount = ''
!        ADD(Detail_Queue)
!        !-----------------------------------------------------------------
        DO AddOutFaults
        !-----------------------------------------------------------------
        ! Force the number of entries in the Detail_Queue to use the
        !   full number of lines used for narrative entries.
        !
        detail:Line   = ''
        detail:Amount = ''

        LOOP WHILE 1 = 1
            IF (RECORDS(Detail_Queue) % prn:DetailRows) = 0
                BREAK
            END !IF

            ADD(Detail_Queue)
        END !IF
        !-----------------------------------------------------------------
    EXIT

!-------------------------------------------------------------------------
! 27 Aug 2002, John, Vodacom change request 0232, reported by Harvey, The  list should show "NO" and "REFERENCE" only.
! CHANGE FROM
!         detail:Line   = ' NO REFERENCE                   QTY      COST    AMOUNT'
! CHANGE TO
!         detail:Line   = ' NO REFERENCE                                          '
!-------------------------------------------------------------------------
AddParts        ROUTINE
    DATA
Temp       STRING(255)
Amount     DECIMAL(10,2)
    CODE
        !-----------------------------------------------------------------
        ! Order_Number_Key KEY( par:Ref_Number, par:Order_Number       ),DUP,NOCASE
        ! Order_Part_Key   KEY( par:Ref_Number, par:Order_Part_Number  ),DUP,NOCASE
        ! Part_Number_Key  KEY( par:Ref_Number, par:Part_Number        ),DUP,NOCASE
        ! RefPartRefNoKey  KEY( par:Ref_Number, par:Part_Ref_Number    ),DUP,NOCASE
        ! PendingRefNoKey  KEY( par:Ref_Number, par:Pending_Ref_Number ),DUP,NOCASE
        !
        Access:WARPARTS.ClearKey(wpr:Order_Number_Key)
            wpr:Ref_Number = job:Ref_Number
        SET(wpr:Order_Number_Key, wpr:Order_Number_Key)
        LOOP WHILE Access:WARPARTS.NEXT() = Level:Benign
            !-------------------------------------------------------------
            IF NOT wpr:Ref_Number = job:Ref_Number
                BREAK
            END !IF

!            IF NOT IsStockPart(par:Part_Ref_Number)
!                CYCLE
!            END !IF
            !-------------------------------------------------------------
            RowNum += 1

            Amount = wpr:Quantity * wpr:Purchase_Cost
        !               |----Reference-------------------QTY------Cost----Amount
            Temp = ' ' & FORMAT(RowNum, @n2) & ' ' & LEFT(wpr:Description, 27) !& FORMAT(par:Quantity, @N_4) & FORMAT(par:Sale_Cost, @N_10.2) & FORMAT(Amount, @n_10.2)

            detail:Line   = Temp
            detail:Amount = ''
            ADD(Detail_Queue)
            !-------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
    EXIT
AddOutFaults        ROUTINE
    DATA
Temp       STRING(255)
    CODE
        !-----------------------------------------------------------------
        Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
            joo:JobNumber = job:Ref_Number
        SET(joo:JobNumberKey, joo:JobNumberKey)
        LOOP WHILE Access:JOBOUTFL.NEXT() = Level:Benign
            !-------------------------------------------------------------
            IF NOT joo:JobNumber = job:Ref_Number
                BREAK
            END !IF
            !-------------------------------------------------------------
            ! Inserting (DBH 16/10/2006) # 8059 - Do not show the IMEI Validation text on the paperwork
            If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                Cycle
            End ! If Instring('IMEI VALIDATION: ',joo:Description,1,1)
            ! End (DBH 16/10/2006) #8059
            RowNum += 1

            Temp = ' ' & FORMAT(RowNum, @n2) & ' ' & LEFT(joo:Description)

            detail:Line   = Temp
            detail:Amount = ''
            ADD(Detail_Queue)
            !-------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
    EXIT
InsertAmounts       ROUTINE
    DATA
VAT       REAL
Total_Due REAL
    CODE
        !------------------------------------------------------------------
!        IF LoadINVOICE(job:Invoice_Number)
!            If inv:ARCInvoiceDate = 0
!                inv:ARCInvoiceDate = Today()
!                job:Invoice_Labour_Cost  = job:Labour_Cost
!                job:Invoice_Courier_Cost = job:Courier_Cost
!                job:Invoice_Parts_Cost   = job:Parts_Cost
!
!
!                !Total_Due = job:Invoice_Sub_Total + VAT
!                job:Invoice_Sub_total = (job:Invoice_Labour_Cost+job:Invoice_Parts_Cost)+job:Invoice_Courier_Cost
!                Access:JOBS.Update()
!
!                inv:ExportedARCOracle = TRUE
!                Access:Invoice.TryUpdate()
!            End !If inv:RRCInvoiceDate = 0
!            VAT       = (inv:Vat_Rate_Labour*job:Invoice_Labour_Cost/100) + (inv:VAT_Rate_Parts*job:Invoice_Parts_Cost/100) + (inv:Vat_Rate_Labour*job:Invoice_Courier_Cost/100)
!            Total_Due = (job:Invoice_Labour_Cost+job:Invoice_Parts_Cost)+ job:Invoice_Courier_Cost + VAT
!        ELSE
!            VAT       = 0.00
!            Total_Due = 0.00
!        END !IF

        VAT = (VatRate(job:Account_Number,'L') * job:Labour_Cost_Warranty/100) + |
                (VatRate(job:Account_Number,'P') * job:Parts_Cost_Warranty/100) + |
                (VatRate(job:Account_Number,'L') * job:Courier_Cost_Warranty/100)
        Total_Due = job:Labour_Cost_Warranty + job:Parts_Cost_Warranty + job:Courier_Cost_Warranty + VAT

        LOOP x# = 1 TO RECORDS(Detail_Queue)
            CASE x# % prn:DetailRows
                OF 01 ! labour
                    InsertAmountToDetailQueue(x#,  job:Labour_Cost_Warranty)
                OF 03 ! Parts
                    InsertAmountToDetailQueue(x#,   job:Parts_Cost_Warranty)
                OF 05 ! Other
                    InsertAmountToDetailQueue(x#, job:Courier_Cost_Warranty) !Courier Cost
                OF 10 ! Sub Total
                    InsertAmountToDetailQueue(x#,    job:Labour_Cost_Warranty + job:Parts_Cost_Warranty + job:Courier_Cost_warranty)
                OF 12 ! VAT
                    InsertAmountToDetailQueue(x#,                      VAT)
                OF 0 ! Total Due
                    InsertAmountToDetailQueue(x#,                Total_Due)
            END !CASE
        END !IF

        GET(Detail_Queue, 01) ! Reset
        !------------------------------------------------------------------
    EXIT
!-----------------------------------------------
PrintJob                    ROUTINE
    DATA
NumBytes LONG
    CODE
        !------------------------------------------------------------------
        prn:szBuffer = CLIP(szHeader) & CLIP(prn:szBuffer) & CLIP(szFooter)

        IF NOT StartPagePrinter( prn:hPrinter ) THEN
            Case Missive('Failure reported by "StartPagePrinter()", attempting to continue.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive

            EXIT
        END !IF


        IF NOT WritePrinter( prn:hPrinter, ADDRESS(prn:szBuffer), LEN(prn:szBuffer), NumBytes ) THEN
            Case Missive('Failure reported by "WritePrinter()", attempting to continue.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            EXIT
        END !IF

        EndPagePrinter( prn:hPrinter )

        prn:szBuffer = ''
        !------------------------------------------------------------------
    EXIT
!-----------------------------------------------
CancelCheck                     routine
    cancel#    = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                IF progress:CancelPressed = False
                    cancel# = 1
                END !IF

                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
                tmp:cancel = 1
                progress:CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1
getnextrecord2      routine
    recordsprocessed += 1
!    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess) * 100
      if percentprogress > 100
        percentprogress = 100
      ELSIF  percentprogress > 80
        RecordsToProcess = (RecordsToProcess * 2)
        percentprogress = (recordsprocessed / recordstoprocess) * 100
      end
    end

    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
    end

    !Display()

endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
!    close(progresswindow)
!    display()
RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF ProgressWindow{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
!-----------------------------------------------
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, ProgressWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  ! #11881 Print A5 Despatch Note?  (Bryan: 18/03/2011)
      IF (GETINI('PRINTING','PrintA5Invoice',,CLIP(PATH())&'\SB2KDEF.INI') <> 1)
          Despatch_Note
          RETURN RequestCompleted
      END
  GlobalErrors.SetProcedureName('Warranty_Delivery_Note')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:DESBATCH.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:JOBNOTES.UseFile
  Access:PARTS.UseFile
  Access:STOCK.UseFile
  Access:JOBOUTFL.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:INVOICE.UseFile
  Access:JOBS.UseFile
  Access:WARPARTS.UseFile
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Warranty_Delivery_Note')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
      LOC:ProgramName           = 'Vodacom Delivery Note'               ! Job=2034      Cust=    Date=20 Aug 2002 John
      ProgressWindow{PROP:Text} = CLIP(LOC:ProgramName)
      ?Tab1{PROP:Text}          = CLIP(LOC:ProgramName) & ' Printing'
  
  !    IF IsWebJob()
  !        Case MessageEx('Unable To print Delivery Note<13,10>   This is a web job'
  !                        ,LOC:ApplicationName,|
  !                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,100)
  !            Of 1 ! &OK Button
  !        End!Case MessageEx
  !
  !        POST(Event:CloseWindow)
  !    END !IF
  
  !    IF NOT GetParameters()
  !        POST(Event:CloseWindow)
  !    END !IF
  
  
      DO PrintDeliveryNote
  
  !    IF GetUserName(param:Username)
  !        LOC:UserName = use:Forename
  !
  !        IF CLIP(LOC:UserName) = ''
  !            LOC:UserName = use:Surname
  !        ELSE
  !            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname
  !        END !IF
  !
  !        IF CLIP(LOC:UserName) = ''
  !            LOC:UserName = '<' & use:User_Code & '>'
  !        END !IF
  !
  !        DO OKButtonPressed
  !    ELSE
  !        POST(Event:CloseWindow)
  !    END !IF
  
      POST(Event:CloseWindow)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:DESBATCH.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Warranty_Delivery_Note')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ProgressCancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProgressCancel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProgressCancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
!-----------------------------------------------
AddAddress PROCEDURE()
    CODE
        !------------------------------------------------------------------
        CLEAR(AddressArray)

        Access:MANUFACT.Clearkey(man:Manufacturer_Key)
        man:Manufacturer    = job:Manufacturer
        If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
            !Found
            AddAddressLine( DeliveryAddressVector, man:Manufacturer )
            AddAddressLine( DeliveryAddressVector, man:Address_Line1 )
            AddAddressLine( DeliveryAddressVector, man:Address_Line2 )
            AddAddressLine( DeliveryAddressVector, man:Address_Line3 )
            AddAddressLine( DeliveryAddressVector, man:Postcode      )
            AddAddressLine( DeliveryAddressVector, man:Telephone_Number )
            ! Display the Manufacturer's Vat Number - TrkBs: 5364 (DBH: 21-04-2005)
            AddAddressLine( DeliveryAddressVector, man:VatNumber)
        Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
            !Error
        End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign

!
!
!        !Add the vat number of the "to" account -  (DBH: 01-12-2003)
!        save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
!        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
!        sub:Account_Number  = job:Account_Number
!        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!            !Found
!            save_TRADEACC_id = Access:TRADEACC.SaveFile()
!            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!            tra:Account_Number  = sub:Main_Account_Number
!            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!                !Found
!                If tra:Use_Sub_Accounts <> 'YES'
!                    AddAddressLine( DeliveryAddressVector, tra:VAT_Number    )
!                Else !If sub:Use_Sub_Accounts <> 'YES'
!                    AddAddressLine( DeliveryAddressVector, sub:VAT_Number    )
!                End !If sub:Use_Sub_Accounts <> 'YES'
!
!            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!                !Error
!            End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!            Access:TRADEACC.RestoreFile(save_TRADEACC_id)
!        Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!            !Error
!        End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!        Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)

        AddAddressLine( InvoiceAddressVector, address:User_Name        )
        AddAddressLine( InvoiceAddressVector, address:Address_Line1    )
        AddAddressLine( InvoiceAddressVector, address:Address_Line2    )
        AddAddressLine( InvoiceAddressVector, address:Address_Line3    )
        AddAddressLine( InvoiceAddressVector, address:Postcode         )
        AddAddressLine( InvoiceAddressVector, address:Telephone_Number )
        AddAddressLine( InvoiceAddressVector, address:VATNumber )
        !------------------------------------------------------------------
        ! force job number to last line
        !
        AddressArray[ InvoiceAddressVector,  8 ] = 'Job Number ' & job:Ref_Number
        !------------------------------------------------------------------
        LOOP x# = 1 TO 8  ! Lines 1..7 for addrsss, 8 = new line added for job number
            CLEAR(BlankLine)

            address:Delivery = AddressArray[ DeliveryAddressVector, x# ]
            address:Business = AddressArray[ InvoiceAddressVector,  x# ]

            prn:szBuffer = CLIP(prn:szBuffer) & CLIP(AddressLine)
            prn:szBuffer = CLIP(prn:szBuffer) & CHR(13) & CHR(10)
            RecordCount += 1
        END !LOOP
        !------------------------------------------------------------------
AddAddressLine PROCEDURE( IN:Vector, IN:Value )
    CODE
        !------------------------------------------------------------------
        ! This procedure is to add the in:value at the next available slot in AddressArray[]
        !   ie remove blank lines in addresses.
        !------------------------------------------------------------------
        LOOP x# = 1 TO 7
            IF AddressArray[IN:Vector, x#] = ''
                AddressArray[IN:Vector, x#] = IN:Value
                BREAK
            END !IF
        END !LOOP

        RecordCount += 1
        !------------------------------------------------------------------
AddDateInLine PROCEDURE()
    CODE
        !------------------------------------------------------------------
        CLEAR(BlankLine)

        date:DateOut  = DateToString(job:date_booked)
        date:TimeOut  = FORMAT(job:time_booked, @T1)
        date:Engineer = ''

        prn:szBuffer = CLIP(prn:szBuffer) & CLIP(DateLine) & CHR(13) & CHR(10)

        RecordCount += 1
        !------------------------------------------------------------------
AddDateOutLine     PROCEDURE()
    CODE
        !------------------------------------------------------------------
        CLEAR(BlankLine)
        !------------------------------------------------------------------
        ! 0239 5) Date Out - the date out is not filled unless the invoice is pre printed later after
        !   it has been despatched (obviously).
        ! However most of the time the invoice will be printed before despatch.
        ! Can youplease fill date [being] printed in here if there is no despatch date.
        !
        IF job:Date_Despatched = 0
            date:DateOut = DateToString(TODAY())
        ELSE
            date:DateOut = DateToString(job:Date_Despatched)
        END !IF
        !------------------------------------------------------------------
        date:TimeOut  = GetDeliveryTime()
        date:Engineer = job:Engineer

        prn:szBuffer = CLIP(prn:szBuffer) & CLIP(DateLine) & CHR(13) & CHR(10)

        RecordCount += 1
        !------------------------------------------------------------------
AddIMEILine     PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        CLEAR(BlankLine)

        imeitmp:Make         = job:Manufacturer
        imeitmp:Model        = job:Model_Number !job_ali:Unit_Type
        imeitmp:SerialNumber = job:MSN
        imeitmp:IMEINumber   = job:ESN

        prn:szBuffer = CLIP(prn:szBuffer) & CLIP(IMEILine) & CHR(13) & CHR(10)

        RecordCount += 1
        !-----------------------------------------------------------------
AddLine PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        prn:szBuffer = CLIP(prn:szBuffer) & CHR(13) & CHR(10)

        RecordCount += 1
        !-----------------------------------------------------------------
AddNarrativeLine PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        CLEAR(BlankLine)
        ! CLEAR(BlankLine)! Bryan: NarrativeLine is blank, hence no parts or outfaults or costs on the report

        narrative:Text1  = detail:Line
        narrative:Amount = detail:Amount

        prn:szBuffer = CLIP(prn:szBuffer) & CLIP(NarrativeLine)
        prn:szBuffer = CLIP(prn:szBuffer) & CHR(13) & CHR(10)

        WriteDebug(CLIP(NarrativeLine))

        RecordCount += 1
        !-----------------------------------------------------------------
AddOrderLine    PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        CLEAR(BlankLine)

        order:DespatchDate    = DateToString(TODAY())
        order:InvoiceNumber   = job:Ref_Number
        order:AccountNumber   = job:Account_Number
        order:YourOrderNumber = job:Order_Number

        prn:szBuffer = CLIP(prn:szBuffer) & CLIP(OrderLine) & CHR(13) & CHR(10)

        RecordCount += 1
        !-----------------------------------------------------------------
!-----------------------------------------------
After   PROCEDURE (LookFor, LookIn)! STRING
FirstChar     LONG
    CODE
        !-----------------------------------------------------------------
        FirstChar = INSTRING( LookFor, LookIn, 1, 1 )
        IF FirstChar = 0
            RETURN ''
        END !IF

        FirstChar += LEN(LookFor)

        RETURN SUB(LookIn, FirstChar, 255)
        !-----------------------------------------------------------------
AppendString    PROCEDURE(IN:First, IN:Second, IN:Separator)!STRING
    CODE
        !------------------------------------------------------------------
        IF CLIP(IN:First) = ''
            RETURN IN:Second

        ELSIF CLIP(IN:Second) = ''
            RETURN IN:First
        END ! IF

        RETURN CLIP(IN:First) & IN:Separator & CLIP(IN:Second)
        !------------------------------------------------------------------
Before   PROCEDURE (LookFor, LookIn)! STRING
FirstChar     LONG
    CODE
        !-----------------------------------------------------------------
        FirstChar = INSTRING( CLIP(LEFT(LookFor)), LookIn, 1, 1 )
        IF FirstChar = 0
            RETURN ''
        END !IF

        RETURN SUB(LookIn, 1, FirstChar - 1)
        !-----------------------------------------------------------------
CalcLabour          PROCEDURE()! REAL
LabourAmount REAL
    CODE
        !-----------------------------------------------------------------
        LabourAmount    = 0

        IF job:chargeable_job <> 'YES'
            RETURN LabourAmount
        END ! IF

        IF job:ignore_chargeable_charges = 'YES'
            RETURN LabourAmount
        END ! IF

        Access:CHARTYPE.ClearKey(cha:charge_type_key)
        IF job:warranty_job = 'YES'
            cha:charge_type = job:warranty_charge_type
        ELSE
            cha:charge_type = job:charge_type
        END !IF

        If Access:CHARTYPE.Fetch(cha:charge_type_key)
            RETURN LabourAmount
        End !If access:chartype.clearkey(cha:charge_type_key)

        If cha:no_charge = 'YES'
            RETURN LabourAmount
        End !If cha:no_charge = 'YES'

        IF NOT LoadSUBTRACC( job:Account_Number )
            GOTO Final
        END !IF

        IF NOT LoadTRADEACC( sub:Main_Account_Number )
            GOTO Final
        END !IF

        If     tra:invoice_sub_accounts = 'YES'
            Access:SUBCHRGE.ClearKey(suc:model_repair_type_key)
            suc:account_number = job:account_number
            suc:model_number   = job:model_number
            suc:charge_type    = job:charge_type
            suc:unit_type      = job:unit_type
            suc:repair_type    = job:repair_type
            if Access:SUBCHRGE.Fetch(suc:model_repair_type_key)
                access:trachrge.clearkey(trc:account_charge_key)
                trc:account_number = sub:main_account_number
                trc:model_number   = job:model_number
                trc:charge_type    = job:charge_type
                trc:unit_type      = job:unit_type
                trc:repair_type    = job:repair_type
                if Access:TRACHRGE.Fetch(trc:account_charge_key) = Level:Benign
                    LabourAmount = trc:cost

                    RETURN LabourAmount
                End!if access:trachrge.fetch(trc:account_charge_key)
            Else
                LabourAmount = suc:cost

                RETURN LabourAmount
            End!if access:subchrge.fetch(suc:model_repair_type_key)

        Else!If tra:invoice_sub_accounts = 'YES'
            Access:TRACHRGE.clearkey(trc:account_charge_key)
            trc:account_number = sub:main_account_number
            trc:model_number   = job:model_number
            trc:charge_type    = job:charge_type
            trc:unit_type      = job:unit_type
            trc:repair_type    = job:repair_type
            if Access:TRACHRGE.fetch(trc:account_charge_key) = Level:Benign
                LabourAmount = trc:cost

                RETURN LabourAmount
            End!if access:trachrge.fetch(trc:account_charge_key)

        End!If tra:invoice_sub_accounts = 'YES'

Final   Access:STDCHRGE.clearkey(sta:model_number_charge_key)
        sta:model_number = job:model_number
        IF job:warranty_job = 'YES'
            sta:charge_type  = job:warranty_charge_type
        ELSE
            sta:charge_type  = job:charge_type
        END !IF

        sta:unit_type    = job:unit_type
        sta:repair_type  = job:repair_type
        IF Access:STDCHRGE.fetch(sta:model_number_charge_key) = Level:Benign
            LabourAmount = sta:cost
        END !IF access:stdchrge.fetch(sta:model_number_charge_key)
        !-----------------------------------------------------------------
        RETURN LabourAmount
        !-----------------------------------------------------------------

DateToString PROCEDURE(IN:Date)!STRING
    CODE
        !------------------------------------------------------------------
        ! Original
        !
        !RETURN LEFT(FORMAT(IN:Date, @D8)) ! DD MM YYYY
        !------------------------------------------------------------------
        ! 2 Sep 2002 0269 Issues identified on the Vodacom Invoices are:
        !   1. Dates showing in wrong format
        !
        !RETURN LEFT(FORMAT(IN:Date, @D17)) ! Windows short date format
        !------------------------------------------------------------------
        ! 4 Sep 2002 Dates - show in UK format i.e. DD/MM/YYYY Still showing in the wrong format - showing as mm/dd/yyyy
        !
        RETURN LEFT(FORMAT(IN:Date, @D6)) ! DD/MM/YYYY
        !------------------------------------------------------------------
GetCustomerName PROCEDURE()!STRING
local:TEMP STRING(100)
    CODE
        !------------------------------------------------------------------
        ! STRING(30)!(job:Title+job:Initials+job:Surname) / job:Company_Name
        !------------------------------------------------------------------
        local:TEMP = AppendString( job:Title, job:Initial, ' ')
        local:TEMP = AppendString(      local:TEMP, job:Surname, ' ')

        IF CLIP(local:TEMP) = ''
            RETURN job:Company_Name
        END !IF

        RETURN CLIP(local:TEMP)
        !------------------------------------------------------------------
GetDeliveryTime PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        ! 2 Sep 2002 0269 Issues identified on the Vodacom Invoices are:
        !   2. Time Out missing - show time printed if not despatched (as do with date).
        !       If the job HAS been despatched then show the time despatched.
        !
        !------------------------------------------------------------------
        IF job:Despatch_Number = 0
            RETURN FORMAT(CLOCK(), @T1)
        END !IF

        Access:DESBATCH.ClearKey(dbt:Batch_Number_Key)
            dbt:Batch_Number = job:Despatch_Number
        SET(dbt:Batch_Number_Key, dbt:Batch_Number_Key)

        IF Access:DESBATCH.NEXT() = Level:Benign
            RETURN FORMAT(dbt:Time, @T1)
        ELSE
            RETURN FORMAT(CLOCK(), @T1)
        END !IF
        !------------------------------------------------------------------
GetExpectedShipDate PROCEDURE()!STRING(2)
    CODE
        !------------------------------------------------------------------
        ! STRING('Delivery Not Known')!put "Delivery Not Known" if job:Current_Status IN("Awaiting Spares", "Spares Requested")
        !
        CASE job:Current_Status
        OF 'Awaiting Spares' OROF 'Spares Requested'
            RETURN 'Delivery Not Known'
        ELSE
            RETURN ''
        END !CASE
        !------------------------------------------------------------------
GetFaultDescriptionLine PROCEDURE( IN:String, OUT:Line1, OUT:Line2 )
LookFor STRING(1)
Pos     LONG
    CODE
        !-----------------------------------------------------------------
        !LookFor = CHR(13) & ''
        Pos     = INSTRING('<13,10>', IN:String, 1, 1)

        IF Pos = 0
            Pos = 55!SIZE(narrative:Text1)

            OUT:line1 = SUB(in:String,     1, Pos)
            OUT:line2 = SUB(in:String, Pos+1, 255)
        ELSE
            OUT:line1 = SUB(in:String,     1, Pos-1)
            OUT:line2 = SUB(in:String, Pos+2,   255)

            Pos = INSTRING('<13,10>', OUT:line2, 1, 1)
            IF Pos <> 0
                OUT:line2 = SUB(OUT:line2, 1, Pos-1)
            END !IF
        END !IF

        RETURN
        !-----------------------------------------------------------------
GetParameters       PROCEDURE()! LONG ! BOOL
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('Attempting to use ' & Clip(loc:ProgramName) & ' without using ' & Clip(loc:ApplicationName) & '.' & |
              '<13,10>'&|
              '<13,10>Start ' & Clip(loc:ApplicationName) & ' and run the report from there.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive

           RETURN False
        END !IF tmpPos
        !-----------------------------------------------
!        tmpPos = INSTRING('Job(', CommandLine)
!        IF NOT tmpPos
!            Case MessageEx('Attempting to use ' & CLIP(LOC:ProgramName) & '<10,13>'            & |
!                '   without passing Job Number.'                                               & |
!                LOC:ApplicationName,                                                             |
!                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!            Of 1 ! &OK Button
!            END!Case MessageEx
!
!           RETURN False
!        END !IF tmpPos
        !-----------------------------------------------
        param:Username     = CLIP(After('%', CommandLine))
        !param:JobNumber    = 247 !9602
        param:JobNumber    = Before(')', After('Job(', CommandLine))

        RETURN True
        !-----------------------------------------------

GetPrinterName PROCEDURE()! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:DEFPRINT.ClearKey(dep:Printer_Name_Key)
            dep:Printer_Name = CLIP(LOC:ProgramName)
        IF Access:DEFPRINT.TryFetch(dep:Printer_Name_Key) = Level:Benign
            prn:Local_Printer_Name = dep:Printer_Path
            IF CLIP(prn:Local_Printer_Name) <> ''
                RETURN True
            END !IF

        END !IF

        RETURN False
        !-----------------------------------------------
GetUserName  PROCEDURE( IN:Username )!LONG ! BOOL
    CODE
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = IN:Username ! CLIP(SUB(CommandLine, tmpPos + 1, 30))

        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Unable to find your logged in user details.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive


           RETURN False
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        RETURN True
        !-----------------------------------------------
InsertAmountToDetailQueue       PROCEDURE( IN:Row, IN:Amount )
    CODE
        !------------------------------------------------------------------
        GET(Detail_Queue, IN:Row) ! labour
            detail:Amount = FORMAT( IN:Amount, @n_10.2)
            PUT(Detail_Queue)
        !------------------------------------------------------------------
IsStockPart PROCEDURE( IN:PartNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------------------------
        IF NOT LoadStock( par:Part_Ref_Number )
            RETURN False
        END !IF

        IF sto:Accessory = 'YES'
            RETURN False
        ELSE
            RETURN True
        END !IF
        !-----------------------------------------------------------------
IsWebJob    PROCEDURE()! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF glo:WebJob then
            RETURN True
        ELSE
            RETURN False
        END
        !------------------------------------------------------------------
!
! Taken from
!        !------------------------------------------------------------------
!        ! Webify tmp:Ref_Number
!        !
!        tmp:Ref_number = job:Ref_number
!        if glo:WebJob then
!            !Change the tmp ref number if you
!            !can Look up from the wob file
!            access:Webjob.clearkey(wob:RefNumberKey)
!            wob:RefNumber = job:Ref_number
!            if access:Webjob.fetch(wob:refNumberKey)=level:benign then
!                access:tradeacc.clearkey(tra:Account_Number_Key)
!                    tra:Account_Number = ClarioNET:Global.Param2
!                access:tradeacc.fetch(tra:Account_Number_Key)
!
!                tmp:Ref_Number = Job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
!            END
!            !Set up no preview on client
!            ClarioNET:UseReportPreview(0)
!        END
!        !------------------------------------------------------------------
LoadINVOICE PROCEDURE( IN:InvoiceNumber )! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        ! RefNumberKey KEY( jbn:RefNumber ),NOCASE,PRIMARY
        !
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = IN:InvoiceNumber
        Set(inv:Invoice_Number_Key, inv:Invoice_Number_Key)

        IF Access:INVOICE.NEXT() <> Level:Benign
            RETURN False
        End !IF

        IF NOT inv:Invoice_Number = IN:InvoiceNumber
            RETURN False
        End !IF

        RETURN True
        !------------------------------------------------------------------
LoadJobNotes PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        ! RefNumberKey KEY( jbn:RefNumber ),NOCASE,PRIMARY
        !
        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = IN:JobNumber
        Set(jbn:RefNumberKey, jbn:RefNumberKey)

        IF Access:JOBNOTES.NEXT() !<> Level:Benign
            RETURN False
        End !IF

        IF NOT jbn:RefNumber = IN:JobNumber
            RETURN False
        End !IF

        RETURN True
        !------------------------------------------------------------------
LoadStock PROCEDURE( IN:StockNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !
        ! Manufacturer_Accessory_Key    KEY(sto:Location,sto:Accessory,sto:Manufacturer,sto:Part_Number                   ),DUP,NOCASE
        ! Shelf_Location_Accessory_Key  KEY(sto:Location,sto:Accessory,                 sto:Shelf_Location                ),DUP,NOCASE
        ! Location_Part_Description_Key KEY(sto:Location,                               sto:Part_Number,   sto:Description),DUP,NOCASE
        ! Description_Accessory_Key     KEY(sto:Location,sto:Accessory,                                    sto:Description),DUP,NOCASE
        ! Part_Number_Accessory_Key     KEY(sto:Location,sto:Accessory,                 sto:Part_Number                   ),DUP,NOCASE
        ! Location_Manufacturer_Key     KEY(sto:Location,              sto:Manufacturer,sto:Part_Number                   ),DUP,NOCASE
        ! Supplier_Accessory_Key        KEY(sto:Location,sto:Accessory,                 sto:Shelf_Location                ),DUP,NOCASE
        ! Description_Key               KEY(sto:Location,                                                  sto:Description),DUP,NOCASE
        ! Location_Key                  KEY(sto:Location,                               sto:Part_Number                   ),DUP,NOCASE
        ! Ref_Part_Description_Key      KEY(sto:Location,sto:Ref_Number,                sto:Part_Number,   sto:Description),DUP,NOCASE
        ! Shelf_Location_Key            KEY(sto:Location,                               sto:Shelf_Location                ),DUP,NOCASE
        ! SecondLocKey                  KEY(sto:Location,                               sto:Shelf_Location,sto:Second_Location,sto:Part_Number),DUP,NOCASE
        ! Supplier_Key                  KEY(sto:Location,sto:Supplier),DUP,NOCASE

        ! Ref_Part_Description2_Key     KEY(sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE
        ! ExchangeAccDescKey            KEY(sto:ExchangeUnit,sto:Location,sto:Description),DUP,NOCASE
        ! ExchangeAccPartKey            KEY(sto:ExchangeUnit,sto:Location,sto:Part_Number),DUP,NOCASE

        ! Manufacturer_Key              KEY(sto:Manufacturer,sto:Part_Number),DUP,NOCASE
        ! Minimum_Description_Key       KEY(sto:Minimum_Stock,sto:Location,sto:Description),DUP,NOCASE
        ! Minimum_Part_Number_Key       KEY(sto:Minimum_Stock,sto:Location,sto:Part_Number),DUP,NOCASE
        ! RequestedKey                  KEY(sto:QuantityRequested,sto:Part_Number),DUP,NOCASE
        ! Ref_Number_Key                KEY(sto:Ref_Number),NOCASE,PRIMARY
        ! Sundry_Item_Key               KEY(sto:Sundry_Item),DUP,NOCASE
        !
        !-----------------------------------------------
        Access:STOCK.ClearKey( sto:Ref_Number_Key )
            sto:Ref_Number = IN:StockNumber
        SET(sto:Ref_Number_Key, sto:Ref_Number_Key)

        IF Access:STOCK.Next() <> Level:Benign
            RETURN False
        END !IF

        IF sto:Ref_Number <> IN:StockNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadSUBTRACC        PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        IF sub:Account_Number = IN:AccountNumber
            ! Only load if not already at correct record
            RETURN True
        END !IF
        !-----------------------------------------------
        ! Account_Number_Key       KEY(sub:Account_Number),NOCASE
        ! Branch_Key               KEY(sub:Branch),DUP,NOCASE
        ! Company_Name_Key         KEY(sub:Company_Name),DUP,NOCASE
        ! Main_Account_Key         KEY(sub:Main_Account_Number,sub:Account_Number),NOCASE,PRIMARY
        ! Main_Branch_Key          KEY(sub:Main_Account_Number,sub:Branch),DUP,NOCASE
        ! Main_Name_Key            KEY(sub:Main_Account_Number,sub:Company_Name),DUP,NOCASE
        !
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = IN:AccountNumber
        SET(sub:Account_Number_Key, sub:Account_Number_Key)

        IF Access:SUBTRACC.NEXT() ! <> Level:Benign
            sub:Account_Number = ''

            RETURN False
        END !IF

        IF NOT sub:Account_Number = IN:AccountNumber
            sub:Account_Number = ''

            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC        PROCEDURE( IN:MainAccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        IF tra:Account_Number = IN:MainAccountNumber
            ! Only load if not already at correct record
            RETURN True
        END !IF

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:MainAccountNumber
        SET(tra:Account_Number_Key, tra:Account_Number_Key)

        IF Access:TRADEACC.NEXT() <> Level:Benign
            tra:Account_Number = ''

            RETURN False
        END !IF

        IF NOT tra:Account_Number = IN:MainAccountNumber
            tra:Account_Number = ''

            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
!-----------------------------------------------
OpenPrinter                 PROCEDURE()! STRING WHERE '' = SUCCESS, ERROR MESSAGE OTHERWISE
NumBytes LONG
    CODE
        !------------------------------------------------------------------
        prn:szPrintername = CLIP(prn:Local_Printer_Name)

        IF NOT OpenPrinter(prn:szPrinterName, prn:hPrinter, 0) THEN
            RETURN 'Unable to open printer "' & prn:szPrinterName & '"'
        END !IF

        CLEAR(DocInfoGrp)

        prn:szDocName  = CLIP(LOC:ProgramName) ! 'Delivery Note'
        prn:szDataType = 'RAW'

        DocInfoGrp.pDocName    = ADDRESS(prn:szDocName)
        DocInfoGrp.pOutputFile = 0
        DocInfoGrp.pDataType   = ADDRESS(prn:szDataType)

        IF NOT StartDocPrinter( prn:hPrinter, 1, ADDRESS(DocInfoGrp) ) THEN
            RETURN 'Unable to StartDocPrinter()'
        END !IF
        !------------------------------------------------------------------
        RETURN ''
        !------------------------------------------------------------------
ClosePrinter        PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        IF prn:hPrinter <> 0
            ClosePrinter( prn:hPrinter )
        END !IF
        !-----------------------------------------------------------------
!-----------------------------------------------
ReplaceString               PROCEDURE (IN:String, IN:LookFor, IN:ReplaceWith)! STRING
FirstChar     LONG
LEN_IN_String LONG
LEN_LookFor   LONG
    CODE
        !-----------------------------------------------------------------
        FirstChar = INSTRING( CLIP(IN:LookFor), IN:String )
        IF FirstChar = 0
            ! Not in string
            RETURN IN:String
        END !IF

        LEN_IN_String = LEN(CLIP( IN:String))
        LEN_LookFor   = LEN(CLIP(IN:LookFor))

        IF FirstChar = 1
            ! First characters in string
            RETURN IN:ReplaceWith & ReplaceString(SUB(IN:String, FirstChar+LEN_LookFor, LEN_IN_String - LEN_LookFor), IN:LookFor, IN:ReplaceWith)
        END !IF

        IF (FirstChar + LEN_LookFor) > LEN_IN_String
            ! Last characters in string
            RETURN SUB(IN:String, 1, FirstChar - 1)
        END !IF

       RETURN SUB(IN:String, 1, FirstChar - 1) & IN:ReplaceWith & ReplaceString( After(IN:LookFor, IN:String), IN:LookFor, IN:ReplaceWith )
        !-----------------------------------------------------------------
SetupInvoiceAddress         PROCEDURE()
    CODE
     !------------------------------------------------------------------
        SET(DEFAULTS, 0)
        Access:DEFAULTS.NEXT()

        ! Default values
        address:User_Name        = def:User_Name
        address:Address_Line1    = def:Address_Line1
        address:Address_Line2    = def:Address_Line2
        address:Address_Line3    = def:Address_Line3
        address:Postcode         = def:Postcode
        address:Telephone_Number = def:Telephone_Number
!  return

        !Amended by Neil T036 21/09/02

        tmp:DefaultHeadAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
        IF NOT LoadTRADEACC(tmp:DefaultHeadAccount)
          RETURN
        END !IF

        If jobe:WebJob
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = wob:HeadAccountNumber
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                address:User_Name        = tra:company_name
                address:Address_Line1    = tra:address_line1
                address:Address_Line2    = tra:address_line2
                address:Address_Line3    = tra:address_line3
                address:Postcode         = tra:postcode
                address:Telephone_Number = tra:telephone_number
                address:VATNumber        = tra:VAT_Number

            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Else !If jobe:HubRepair
            IF NOT LoadSUBTRACC(job:Account_Number)
                RETURN
            END !IF

            IF NOT LoadTRADEACC(sub:Main_Account_Number)
                RETURN
            END !IF
            !------------------------------------------------------------------
            if tra:invoice_sub_accounts = 'YES'
                If sub:invoice_customer_address = 'YES'
                    address:User_Name        = job:company_name
                    address:Address_Line1    = job:address_line1
                    address:Address_Line2    = job:address_line2
                    address:Address_Line3    = job:address_line3
                    address:Postcode         = job:postcode
                    address:Telephone_Number = job:telephone_number

                Else!If sub:invoice_customer_address = 'YES'
                    address:User_Name        = sub:company_name
                    address:Address_Line1    = sub:address_line1
                    address:Address_Line2    = sub:address_line2
                    address:Address_Line3    = sub:address_line3
                    address:Postcode         = sub:postcode
                    address:Telephone_Number = sub:telephone_number
                    address:VatNumber        = sub:Vat_Number

                End!If sub:invoice_customer_address = 'YES'

                RETURN
            End!!if tra:use_sub_accounts = 'YES'
            !------------------------------------------------------------------
            If tra:invoice_customer_address = 'YES'
                address:User_Name        = job:company_name
                address:Address_Line1    = job:address_line1
                address:Address_Line2    = job:address_line2
                address:Address_Line3    = job:address_line3
                address:Postcode         = job:postcode
                address:Telephone_Number = job:telephone_number

                RETURN
            End!If tra:invoice_customer_address = 'YES'
            !------------------------------------------------------------------
            address:User_Name        = tra:company_name
            address:Address_Line1    = tra:address_line1
            address:Address_Line2    = tra:address_line2
            address:Address_Line3    = tra:address_line3
            address:Postcode         = tra:postcode
            address:Telephone_Number = tra:telephone_number
            address:VatNumber        = tra:Vat_Number
            !------------------------------------------------------------------

        End !If jobe:HubRepair
UpdateScreen PROCEDURE( IN:Message ) ! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        ?progress:userstring{prop:text} = CLIP(IN:Message)

        DO getnextrecord2
        DO CancelCheck

        IF progress:CancelPressed = True
            RETURN True
        ELSE
            RETURN False
        END !IF
        !------------------------------------------------------------------
WriteDebug  PROCEDURE( IN:Message )
    CODE
        !------------------------------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'c:\Debug.ini')
        !------------------------------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Warranty_Delivery_Note_Web PROCEDURE                  !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
Tmp:DefaultHeadAccount STRING(30)
Parameter_Group      GROUP,PRE(param)
JobNumber            LONG
Username             STRING(3)
                     END
Local_Group          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase 2000')
BatchNumber          LONG
CommentText          STRING(100)
DesktopPath          STRING(255)
FileName             STRING(255)
Path                 STRING(255)
ProgramName          STRING(100)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED !OLE Automation holder
excel:ColumnName     STRING(100)
excel:ColumnWidth    REAL
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
Excel:Visible        BYTE(0)
                     END
Misc_Group           GROUP,PRE()
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
debug:Count          LONG
debug:Active         LONG
OPTION1              SHORT
RecordCount          LONG
Result               BYTE
Save_Job_ID          ULONG,AUTO
SAVEPATH             STRING(255)
TotalCharacters      LONG
                     END
LOC:HeadAccountNumber STRING(15)
HeadAccountQueue     QUEUE,PRE(hq)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
JobsBooked           LONG
                     END
LOC:SubAccountNumber STRING(15)
SubAccountQueue      QUEUE,PRE(sq)
HeadAccountNumber    STRING(15)
SubAccountNumber     STRING(15)
SubAccountName       STRING(30)
JobsBooked           LONG
                     END
Progress_Group       GROUP,PRE(progress)
Text                 STRING(100)
NextCancelCheck      TIME
Count                LONG
CancelPressed        LONG
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('E')
DataLastCol          STRING('S')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(2048)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
HTML_Queue           QUEUE,PRE(html)
CharPos              LONG
Length               LONG
LINE                 STRING(255)
                     END
NetTalk_Group        GROUP,PRE()
EmailServer          STRING(80)
EmailPort            USHORT
EmailFrom            STRING(255)
EmailTo              STRING(1024)
EmailSubject         STRING(255)
EmailCC              STRING(1024)
EmailFileList        STRING(1024)
EmailBCC             STRING(1024)
EmailMessageText     STRING(16384)
                     END
PrinterinfoGrp       GROUP(PRINTER_INFO_5),PRE()
                     END
DocInfoGrp           GROUP(DOC_INFO_1),PRE()
                     END
Printer_Group        GROUP,PRE(prn)
PageLength           LONG(48)
DetailRows           LONG(14)
Local_Printer_Name   STRING(80)
hPrinter             UNSIGNED
szPrinterName        CSTRING(260)
dwBytesCopied        LONG
dwWritten            LONG
szBuffer             CSTRING(30000)
szDocName            CSTRING(80)
szDataType           CSTRING(80)
                     END
BlankLine            STRING(80)
AddressLine          GROUP,PRE(address),OVER(BlankLine)
Delivery             STRING(30)
Filler1              STRING(4)
Business             STRING(30)
                     END
OrderLine            GROUP,PRE(order),OVER(BlankLine)
DespatchDate         STRING(10)
Filler1              STRING(4)
Filler2              STRING(7)
InvoiceNumber        STRING(10)
Filler4              STRING(1)
AccountNumber        STRING(12)
Filler3              STRING(1)
YourOrderNumber      STRING(20)
                     END
IMEILine             GROUP,PRE(imeitmp),OVER(BlankLine)
Make                 STRING(14)
Model                STRING(15)
SerialNumber         STRING(23)
IMEINumber           STRING(20)
                     END
NarrativeLine        GROUP,PRE(narrative),OVER(BlankLine)
Text1                STRING(55)
Filler1              STRING(10)
Amount               STRING(10)
                     END
DateLine             GROUP,PRE(date),OVER(BlankLine)
Filler0              STRING(10)
DateOut              STRING(10)
Filler1              STRING(22)
TimeOut              STRING(10)
Filler2              STRING(5)
Engineer             STRING(3)
                     END
Detail_Queue         QUEUE,PRE(detail)
Line                 STRING(56)
Filler1              STRING(10)
Amount               STRING(10)
                     END
Address_Group        GROUP,PRE(address)
User_Name            STRING('''INSERT USER NAME'' {12}')
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Postcode             STRING(15)
Telephone_Number     STRING(15)
VatNumber            STRING(30)
                     END
AddressArray         STRING(30),DIM(2,8)
szHeader             CSTRING(5000)
szFooter             CSTRING(5000)
RowNum               LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:sub_total        REAL
Left_Address_Group   GROUP,PRE(left)
CompanyName          STRING(30)
Line1                STRING(30)
Line2                STRING(30)
Line3                STRING(30)
Post_Code            STRING(15)
TelNo                STRING(15)
VatNumber            STRING(30)
                     END
ProgressWindow       WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(632,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Create Delivery Note'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB,USE(?Tab1)
                           STRING('About to create Delivery note'),AT(260,194,160,11),USE(?progress:userstring),CENTER,FONT('Tahoma',10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Are you sure?'),AT(309,214),USE(?String3),FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(''),AT(259,221,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
                         END
                       END
                       BUTTON,AT(300,258),USE(?OKButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?ProgressCancel),TRN,FLAT,LEFT,TIP('Click to close window'),ICON('cancelp.jpg')
                     END

InvoiceAddressVector    EQUATE(1)
DeliveryAddressVector   EQUATE(2)

INFILE FILE,DRIVER('ASCII'),PRE(INF),NAME(LocalGloabinfile),CREATE
INFILERec RECORD
TextLine  STRING(30000)
    end !Record  (Part of infile)
    end !infile
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
AddAddress      PROCEDURE()
AddAddressLine  PROCEDURE( LONG, STRING )
AddDateInLine PROCEDURE()
AddDateOutLine     PROCEDURE()
AddIMEILine     PROCEDURE()
AddLine         PROCEDURE()
AddNarrativeLine PROCEDURE()

AddOrderLine    PROCEDURE()
After   PROCEDURE ( STRING, STRING ), STRING
AppendString    PROCEDURE(STRING, STRING, STRING), STRING
Before                    PROCEDURE ( STRING, STRING ), STRING
CalcLabour          PROCEDURE(), REAL
DateToString PROCEDURE(DATE), STRING
GetCustomerName PROCEDURE(), STRING
GetDeliveryTime PROCEDURE(), STRING
GetExpectedShipDate PROCEDURE(), STRING!(2)
GetFaultDescriptionLine PROCEDURE( STRING, *STRING, *STRING )
GetParameters       PROCEDURE(), LONG ! BOOL
GetPrinterName PROCEDURE(), LONG ! BOOL
GetUserName  PROCEDURE( STRING ), LONG ! BOOL
InsertAmountToDetailQueue       PROCEDURE( LONG, REAL )
IsStockPart PROCEDURE( LONG ), LONG ! BOOL
IsWebJob    PROCEDURE(), LONG ! BOOL
LoadINVOICE PROCEDURE( LONG ), LONG ! BOOL
LoadJobNotes PROCEDURE( LONG ), LONG ! BOOL
LoadStock PROCEDURE( LONG ), LONG ! BOOL
LoadSUBTRACC        PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC        PROCEDURE( STRING ), LONG ! BOOL
ClosePrinter PROCEDURE()
OpenPrinter                 PROCEDURE(), STRING ! WHERE '' = SUCCESS, ERROR MESSAGE OTHERWISE
ReplaceString               PROCEDURE ( STRING, STRING, STRING ), STRING
SetupInvoiceAddress     PROCEDURE()
UpdateScreen PROCEDURE( STRING ), LONG, PROC ! BOOL
WriteDebug  PROCEDURE( STRING )
    END ! MAP
! ProgressWindow Declarations

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
!progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
!       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
!       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
!       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
!       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
!     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!-----------------------------------------------
PrintDeliveryNote                     ROUTINE
    DATA
Temp      STRING(255)
    CODE
        !------------------------------------------------------------------
        debug:Active = False

        SetupInvoiceAddress()
        !------------------------------------------------------------------
        If ~glo:WebJob


          IF NOT GetPrinterName()
              Case Missive('Unable to find Delivery Note printer name.' & CLIP(Temp),'ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive

              POST(Event:CloseWindow)
              EXIT
          END !IF

            Temp = OpenPrinter()
            !------------------------------------------------------------------
            IF CLIP(Temp) <> ''
                Case Missive('Error Opening Printer.<13,10,13,10>' & CLIP(Temp),'ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive

                POST(Event:CloseWindow)
                EXIT
            END !IF
            !------------------------------------------------------------------
        End !If ~glo:WebJob
            DO CreateHeader
            DO CreateFooter
            DO CreatePartsList ! Fill Detail_Queue with 1. text lines, 2. parts lines, 3. out fault lines
            DO InsertAmounts   ! Fill Detail_Queue amount column

            WriteDebug('RECORDS(Detail_Queue)=' & RECORDS(Detail_Queue))

            Loop x# = 1 TO RECORDS(Detail_Queue)
                !--------------------------------------------------------------
                GET(Detail_Queue, x#)

                AddNarrativeLine()
                !--------------------------------------------------------------
                IF (X# % prn:DetailRows) = 0
                    WriteDebug('DO PrintJob(' & x# & ')')
                    DO PrintJob
                END !IF
                !--------------------------------------------------------------
            END !LOOP
            !------------------------------------------------------------------
        If ~glo:webJob
          ClosePrinter()
          Case Missive('Printing Complete.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
        End !If glo:webJOb
!-----------------------------------------------
CreateHeader        ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        !Dec.     Hex.    Mnemonic         Function
        !
        ! 0       00      NUL              Command terminator
        ! 7       07      BEL              Beeper
        ! 8       08      BS               Backspace
        ! 9       09      HT               Horizontal Tab
        !10       0A      LF               Line Feed
        !11       0B      VT               Vertical Tab
        !12       0C      FF               Form Feed
        !
        !Escape Sequence Codes:
        !Dec.             Hex.           Mnemonic      Function
        !27 67 n          1B 43 n        ESC C n       Set Page Length in Lines
        !
        prn:szBuffer = CHR(27) & CHR(67) & CHR(prn:PageLength)
            AddLine()
            AddLine()
            AddLine()
            AddLine()
            AddLine()
            AddLine()
            AddLine()

        AddAddress()
            AddLine()
            AddLine()
            AddLine()
            AddLine()

        AddOrderLine()
            AddLine()
            AddLine()
            AddLine()
            AddLine()

        AddIMEILine()
            AddLine()
        !------------------------------------------------------------------
        szHeader = CLIP(prn:szBuffer)
        prn:szBuffer = ''
        !------------------------------------------------------------------
    EXIT
CreateFooter        ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        prn:szBuffer = ''
            AddLine()
            AddDateInLine()
            AddLine()
            AddDateOutLine()

        szFooter = CLIP(prn:szBuffer) & CHR(12) ! FORM FEED
        prn:szBuffer = ''
        !------------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreatePartsList             ROUTINE
    DATA
Temp1      STRING(255)
Temp2      STRING(255)
    CODE
        !-----------------------------------------------------------------
        ! 14 rows
        !-----------------------------------------------------------------
        FREE( Detail_Queue)
        CLEAR(Detail_Queue)

        !Show Warranty Status -  (DBH: 02-12-2003)
        detail:Line = 'WARRANTY STATUS: ' & Left(job:Warranty_Charge_Type)
        detail:Amount = ''
        Add(Detail_Queue)

        IF LoadJobNotes(job:Ref_Number) = True
            GetFaultDescriptionLine ( jbn:Fault_Description, Temp1, Temp2 )
                detail:Line   = Temp1
                detail:Amount = ''
                ADD(Detail_Queue)

                detail:Line   = Temp2
                detail:Amount = ''
                ADD(Detail_Queue)
        ELSE
            detail:Line   = '' ! CreatePartsList Debug Fault description 1'
            detail:Amount = ''
            ADD(Detail_Queue)

            detail:Line   = '' ! CreatePartsList Debug Fault description 2'
            detail:Amount = ''
            ADD(Detail_Queue)
        END !IF
        !-----------------------------------------------------------------
        !               |-------------------------------------------------------|
        !               |----Reference-------------------QTY------Cost----Amount
        !               |1234*23456789012345678901234567*234*234567890*234567890
        ! detail:Line   = ' NO REFERENCE                                          ' ! See change request 0232 below
        !etail:Line   = ' ## REFERENCE               QUANTITY      COST    AMOUNT'
        !detail:Amount = ''
        !ADD(Detail_Queue)
        !-----------------------------------------------------------------
        detail:Line   = ALL('-', SIZE(narrative:Text1))
        detail:Amount = ''
        ADD(Detail_Queue)
        !-----------------------------------------------------------------
        DO AddParts
!        !-----------------------------------------------------------------
!        ! 0239 4a)Require a line to separate the fault description and the out fault text
!        !
!        detail:Line   = ''
!        detail:Amount = ''
!        ADD(Detail_Queue)
!        !-----------------------------------------------------------------
        DO AddOutFaults
        !-----------------------------------------------------------------
        ! Force the number of entries in the Detail_Queue to use the
        !   full number of lines used for narrative entries.
        !
        detail:Line   = ''
        detail:Amount = ''

        LOOP WHILE 1 = 1
            IF (RECORDS(Detail_Queue) % prn:DetailRows) = 0
                BREAK
            END !IF

            ADD(Detail_Queue)
        END !IF
        !-----------------------------------------------------------------
    EXIT

!-------------------------------------------------------------------------
! 27 Aug 2002, John, Vodacom change request 0232, reported by Harvey, The  list should show "NO" and "REFERENCE" only.
! CHANGE FROM
!         detail:Line   = ' NO REFERENCE                   QTY      COST    AMOUNT'
! CHANGE TO
!         detail:Line   = ' NO REFERENCE                                          '
!-------------------------------------------------------------------------
AddParts        ROUTINE
    DATA
Temp       STRING(255)
Amount     DECIMAL(10,2)
    CODE
        !-----------------------------------------------------------------
        ! Order_Number_Key KEY( par:Ref_Number, par:Order_Number       ),DUP,NOCASE
        ! Order_Part_Key   KEY( par:Ref_Number, par:Order_Part_Number  ),DUP,NOCASE
        ! Part_Number_Key  KEY( par:Ref_Number, par:Part_Number        ),DUP,NOCASE
        ! RefPartRefNoKey  KEY( par:Ref_Number, par:Part_Ref_Number    ),DUP,NOCASE
        ! PendingRefNoKey  KEY( par:Ref_Number, par:Pending_Ref_Number ),DUP,NOCASE
        !
        Access:WARPARTS.ClearKey(wpr:Order_Number_Key)
            wpr:Ref_Number = job:Ref_Number
        SET(wpr:Order_Number_Key, wpr:Order_Number_Key)
        LOOP WHILE Access:WARPARTS.NEXT() = Level:Benign
            !-------------------------------------------------------------
            IF NOT wpr:Ref_Number = job:Ref_Number
                BREAK
            END !IF

!            IF NOT IsStockPart(par:Part_Ref_Number)
!                CYCLE
!            END !IF
            !-------------------------------------------------------------
            RowNum += 1

            Amount = wpr:Quantity * wpr:RRCPurchaseCost
        !               |----Reference-------------------QTY------Cost----Amount
            Temp = ' ' & FORMAT(RowNum, @n2) & ' ' & LEFT(wpr:Description, 27) !& FORMAT(par:Quantity, @N_4) & FORMAT(par:Sale_Cost, @N_10.2) & FORMAT(Amount, @n_10.2)

            detail:Line   = Temp
            detail:Amount = ''
            ADD(Detail_Queue)
            !-------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
    EXIT
AddOutFaults        ROUTINE
    DATA
Temp       STRING(255)
    CODE
        !-----------------------------------------------------------------
        Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
            joo:JobNumber = job:Ref_Number
        SET(joo:JobNumberKey, joo:JobNumberKey)
        LOOP WHILE Access:JOBOUTFL.NEXT() = Level:Benign
            !-------------------------------------------------------------
            IF NOT joo:JobNumber = job:Ref_Number
                BREAK
            END !IF
            !-------------------------------------------------------------
            ! Inserting (DBH 16/10/2006) # 8059 - Do not show the IMEI Validation text on the paperwork
            If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                Cycle
            End ! If Instring('IMEI VALIDATION: ',joo:Description,1,1)
            ! End (DBH 16/10/2006) #8059
            RowNum += 1

            Temp = ' ' & FORMAT(RowNum, @n2) & ' ' & LEFT(joo:Description)

            detail:Line   = Temp
            detail:Amount = ''
            ADD(Detail_Queue)
            !-------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
    EXIT
InsertAmounts       ROUTINE
    DATA
VAT       REAL
Total_Due REAL
local:LabourCost    Real
local:partsCost     Real
local:CourierCost   Real
    CODE
        !------------------------------------------------------------------
        Access:JobSe.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:ref_number
        IF Access:JobSe.Fetch(jobe:RefNumberKey)
          !Error!
        ELSE
          !Got it
        END
!        IF LoadINVOICE(job:Invoice_Number)
!            If inv:RRCInvoiceDate = 0
!                inv:RRCInvoiceDate = Today()
!            End !If inv:RRCInvoiceDate = 0
!            If inv:ExportedRRCOracle = 0
!                jobe:InvRRCCLabourCost  = jobe:RRCCLabourCost
!                jobe:InvRRCCPartsCost   = jobe:RRCCPartsCost
!                jobe:InvRRCCSubTotal    = jobe:RRCCSubTotal
!                jobe:InvoiceHandlingFee = jobe:HandlingFee
!                jobe:InvoiceExchangeRate    = jobe:ExchangeRate
!                Access:JOBSE.TryUpdate()
!
!                inv:ExportedRRCOracle = TRUE
!
!            End !If inv:ExportedRRCOracle = 0
!
!
!            VAT       = (jobe:InvRRCCLabourCost * inv:RRCVatRateLabour/100) + |
!                                    (jobe:InvRRCCPartsCost * inv:RRCVatRateParts/100) + |
!                                    (job:Invoice_Courier_Cost * inv:RRCVatRateLabour/100)
!            !Total_Due = job:Invoice_Sub_Total + VAT
!            !job:Invoice_Sub_total = (job:Invoice_Labour_Cost+job:Invoice_Parts_Cost)+job:Invoice_Courier_Cost
!            tmp:sub_total = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job:Invoice_Courier_Cost
!
!            Total_Due = tmp:sub_total + VAT
!
!            inv:ExportedRRCOracle = TRUE
!
!            Access:Invoice.TryUpdate()
!        ELSE
!            VAT       = 0.00
!            Total_Due = 0.00
!        END !IF



        If SentToHub(job:Ref_Number)
            local:LabourCost = 0
            local:PartsCost = 0
            local:CourierCost = 0
            VAT = 0
            tmp:Sub_Total = 0
            Total_Due = 0
        Else !If SentToHub(job:Ref_Number)
            local:LabourCost = jobe:RRCWLabourCost
            local:PartsCost = jobe:RRCWPartsCost
            local:CourierCost = job:Courier_Cost_Warranty
            VAT = (VatRate(job:Account_Number,'L') * jobe:RRCWLabourCost/100) + |
                    (VatRate(job:Account_Number,'P') * jobe:RRCWPartsCost/100) + |
                    (VatRate(job:Account_Number,'L') * job:Courier_Cost_Warranty/100)
            tmp:Sub_Total   = jobe:RRCWLabourCost + jobe:RRCWPartsCost + job:Courier_Cost_Warranty
            Total_Due = tmp:Sub_Total + VAT
        End !If SentToHub(job:Ref_Number)

        LOOP x# = 1 TO RECORDS(Detail_Queue)
            CASE x# % prn:DetailRows
                OF 01 ! labour
                    InsertAmountToDetailQueue(x#,  local:LabourCost)
                OF 03 ! Parts
                    InsertAmountToDetailQueue(x#,   local:PartsCost)
                OF 05 ! Other
                    InsertAmountToDetailQueue(x#, local:CourierCost)
                OF 10 ! Sub Total
                    InsertAmountToDetailQueue(x#,    tmp:sub_total)
                OF 12 ! VAT
                    InsertAmountToDetailQueue(x#,                      VAT)
                OF 0 ! Total Due
                    InsertAmountToDetailQueue(x#,                Total_Due)
            END !CASE
        END !IF

        GET(Detail_Queue, 01) ! Reset
        !------------------------------------------------------------------
    EXIT
!-----------------------------------------------
PrintJob                    ROUTINE
    DATA
NumBytes LONG
    CODE
        !------------------------------------------------------------------
        prn:szBuffer = CLIP(szHeader) & CLIP(prn:szBuffer) & CLIP(szFooter)

!===============================================================!
!
!   Everything is now in the prn:szbuffer
!
!   Used to dump this to printer -
!   now we write to a file, send file to client
!   set printer on the client
!   and then tell the client to print to print the invoice
!
!===============================================================!


!Write to a file
 !write to file (path()&'\'&clip(ClarioNET:Global.Param2)&'I.TXA')

    If glo:WebJob

        LocalGloabinfile = CLIP(path())&'\'&clip(ClarioNET:Global.Param2)&'I.TXA'
        CREATE(infile)
        EMPTY(infile)
        open(infile)
        inf:textline = prn:szBuffer
        ADD(Infile)
        close(infile)
      !Send that file to client
        SendFileToClient(CLIP(path())&'\'&clip(ClarioNET:Global.Param2)&'I.TXA')

      !set printer on client
        ClarioNET:CallClientProcedure('SetClientPrinter','VODACOM DELIVERY NOTE')

      !Tell client to print
        ClarioNET:CallClientProcedure('PRINTINVOICE')
        Remove(INfile)
    Else !If glo:WebJob
        IF NOT StartPagePrinter( prn:hPrinter ) THEN
            Case Missive('Failure reported by "StartPagePrinter()", attempting to continue.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive

            EXIT
        END !IF

        IF NOT WritePrinter( prn:hPrinter, ADDRESS(prn:szBuffer), LEN(prn:szBuffer), NumBytes ) THEN
            Case Missive('Failure reported by "WritePrinter()", attempting to continue.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive

            EXIT
        END !IF

        EndPagePrinter( prn:hPrinter )

    End !If glo:WebJob

        prn:szBuffer = ''
        !------------------------------------------------------------------
    EXIT
!-----------------------------------------------
CancelCheck                     routine
    cancel#    = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                IF progress:CancelPressed = False
                    cancel# = 1
                END !IF

                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
                tmp:cancel = 1
                progress:CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1
getnextrecord2      routine
    recordsprocessed += 1
!    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess) * 100
      if percentprogress > 100
        percentprogress = 100
      ELSIF  percentprogress > 80
        RecordsToProcess = (RecordsToProcess * 2)
        percentprogress = (recordsprocessed / recordstoprocess) * 100
      end
    end

    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
    end

    !Display()

endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
!    close(progresswindow)
!    display()
RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF ProgressWindow{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
!-----------------------------------------------
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020152'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, ProgressWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  ! #11881 Print A5 Despatch Note?  (Bryan: 18/03/2011)
      IF (GETINI('PRINTING','PrintA5Invoice',,CLIP(PATH())&'\SB2KDEF.INI') <> 1)
          Despatch_Note
          RETURN RequestCompleted
      END
  GlobalErrors.SetProcedureName('Warranty_Delivery_Note_Web')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:DESBATCH.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:JOBNOTES.UseFile
  Access:PARTS.UseFile
  Access:STOCK.UseFile
  Access:JOBOUTFL.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:INVOICE.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:WARPARTS.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Warranty_Delivery_Note_Web')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
      LOC:ProgramName           = 'Vodacom Delivery Note'               ! Job=2034      Cust=    Date=20 Aug 2002 John
      ProgressWindow{PROP:Text} = CLIP(LOC:ProgramName)
      ?Tab1{PROP:Text}          = CLIP(LOC:ProgramName) & ' Printing'
  
  
  
  
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:DESBATCH.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Warranty_Delivery_Note_Web')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020152'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020152'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020152'&'0')
      ***
    OF ?OKButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OKButton, Accepted)
          DO PrintDeliveryNote
      
      
          POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OKButton, Accepted)
    OF ?ProgressCancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProgressCancel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProgressCancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
!-----------------------------------------------
AddAddress PROCEDURE()
    CODE
        !------------------------------------------------------------------
        CLEAR(AddressArray)
        access:Webjob.clearkey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        if access:Webjob.fetch(wob:refNumberKey)=level:benign
          !Ok
        END

!        access:tradeacc.clearkey(tra:Account_Number_Key)
!        tra:Account_Number = job:Account_Number
!        if Access:tradeacc.fetch(tra:Account_Number_Key) then
!           Access:subtracc.clearkey(sub:Account_Number_Key)
!           sub:Account_Number = job:Account_Number
!           if Access:subtracc.fetch(sub:Account_Number_Key) then
!              !error
!           end
!           IF sub:Invoice_Customer_Address = 'YES'
!             IF job:Company_Name = ''
!               left:CompanyName = CLIP(job:Title)&' '&CLIP(job:Initial)&' '&CLIP(job:Surname)
!             ELSE
!               left:CompanyName = job:Company_Name
!             END
!             left:line1 = job:Address_Line1
!             left:line2 = job:Address_Line2
!             left:line3 = job:Address_Line3
!             left:post_code = job:Postcode
!             left:TelNo = job:Telephone_Number
!             left:VatNumber = sub:Vat_Number
!           ELSE
!             left:CompanyName = sub:Company_Name
!             left:line1 = sub:Address_Line1
!             left:line2 = sub:Address_Line2
!             left:line3 = sub:Address_Line3
!             left:post_code = sub:Postcode
!             left:TelNo = sub:Telephone_Number
!             left:VatNumber = sub:Vat_Number
!           END
!
!        else
!            left:CompanyName = tra:Company_Name
!            left:line1 = tra:Address_Line1
!            left:line2 = tra:Address_Line2
!            left:line3 = tra:Address_Line3
!            left:post_code = tra:Postcode
!            left:TelNo = tra:Telephone_Number
!            left:VatNumber = tra:Vat_Number
!        end

        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = GETINI('Booking','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found

        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign


        AddAddressLine( DeliveryAddressVector, tra:Company_Name  )
        AddAddressLine( DeliveryAddressVector, tra:Address_Line1 )
        AddAddressLine( DeliveryAddressVector, tra:Address_Line2 )
        AddAddressLine( DeliveryAddressVector, tra:Address_Line3)
        AddAddressLine( DeliveryAddressVector, tra:Postcode      )
        AddAddressLine( DeliveryAddressVector, tra:Telephone_Number     )
        AddAddressLine( DeliveryAddressVector, tra:VAT_Number     )

        AddAddressLine( InvoiceAddressVector, address:User_Name        )
        AddAddressLine( InvoiceAddressVector, address:Address_Line1    )
        AddAddressLine( InvoiceAddressVector, address:Address_Line2    )
        AddAddressLine( InvoiceAddressVector, address:Address_Line3    )
        AddAddressLine( InvoiceAddressVector, address:Postcode         )
        AddAddressLine( InvoiceAddressVector, address:Telephone_Number )
        AddAddressLine( InvoiceAddressVector, address:VatNumber )
        !------------------------------------------------------------------
        ! force job number to last line
        !
        If glo:WebJob
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = Clarionet:Global.Param2
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !ok!
            END

        Else !If glo:WebJob
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = wob:HeadAccountNumber
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !ok!
            END

        End !If glo:WebJob

        AddressArray[ InvoiceAddressVector,  8 ] = 'Job Number ' & job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
        !------------------------------------------------------------------
        LOOP x# = 1 TO 8  ! Lines 1..7 for addrsss, 8 = new line added for job number
            CLEAR(BlankLine)

            address:Delivery = AddressArray[ DeliveryAddressVector, x# ]
            address:Business = AddressArray[ InvoiceAddressVector,  x# ]

            prn:szBuffer = CLIP(prn:szBuffer) & CLIP(AddressLine)
            prn:szBuffer = CLIP(prn:szBuffer) & CHR(13) & CHR(10)
            RecordCount += 1
        END !LOOP
        !------------------------------------------------------------------
AddAddressLine PROCEDURE( IN:Vector, IN:Value )
    CODE
        !------------------------------------------------------------------
        ! This procedure is to add the in:value at the next available slot in AddressArray[]
        !   ie remove blank lines in addresses.
        !------------------------------------------------------------------
        LOOP x# = 1 TO 7
            IF AddressArray[IN:Vector, x#] = ''
                AddressArray[IN:Vector, x#] = IN:Value
                BREAK
            END !IF
        END !LOOP

        RecordCount += 1
        !------------------------------------------------------------------
AddDateInLine PROCEDURE()
    CODE
        !------------------------------------------------------------------
        CLEAR(BlankLine)

        date:DateOut  = DateToString(job:date_booked)
        date:TimeOut  = FORMAT(job:time_booked, @T1)
        date:Engineer = ''

        prn:szBuffer = CLIP(prn:szBuffer) & CLIP(DateLine) & CHR(13) & CHR(10)

        RecordCount += 1
        !------------------------------------------------------------------
AddDateOutLine     PROCEDURE()
    CODE
        !------------------------------------------------------------------
        CLEAR(BlankLine)
        !------------------------------------------------------------------
        ! 0239 5) Date Out - the date out is not filled unless the invoice is pre printed later after
        !   it has been despatched (obviously).
        ! However most of the time the invoice will be printed before despatch.
        ! Can youplease fill date [being] printed in here if there is no despatch date.
        !
        IF job:Date_Despatched = 0
            date:DateOut = DateToString(TODAY())
        ELSE
            date:DateOut = DateToString(job:Date_Despatched)
        END !IF
        !------------------------------------------------------------------
        date:TimeOut  = GetDeliveryTime()
        date:Engineer = job:Engineer

        prn:szBuffer = CLIP(prn:szBuffer) & CLIP(DateLine) & CHR(13) & CHR(10)

        RecordCount += 1
        !------------------------------------------------------------------
AddIMEILine     PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        CLEAR(BlankLine)

        imeitmp:Make         = job:Manufacturer
        imeitmp:Model        = job:Model_Number !job_ali:Unit_Type
        imeitmp:SerialNumber = job:MSN
        imeitmp:IMEINumber   = job:ESN

        prn:szBuffer = CLIP(prn:szBuffer) & CLIP(IMEILine) & CHR(13) & CHR(10)

        RecordCount += 1
        !-----------------------------------------------------------------
AddLine PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        prn:szBuffer = CLIP(prn:szBuffer) & CHR(13) & CHR(10)

        RecordCount += 1
        !-----------------------------------------------------------------
AddNarrativeLine PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        CLEAR(BlankLine)
        ! CLEAR(BlankLine)! Bryan: NarrativeLine is blank, hence no parts or outfaults or costs on the report

        narrative:Text1  = detail:Line
        narrative:Amount = detail:Amount

        prn:szBuffer = CLIP(prn:szBuffer) & CLIP(NarrativeLine)
        prn:szBuffer = CLIP(prn:szBuffer) & CHR(13) & CHR(10)

        WriteDebug(CLIP(NarrativeLine))

        RecordCount += 1
        !-----------------------------------------------------------------
AddOrderLine    PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        CLEAR(BlankLine)

        order:DespatchDate    = DateToString(TODAY())
        order:InvoiceNumber   = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
        order:AccountNumber   = job:Account_Number
        order:YourOrderNumber = job:Order_Number

        prn:szBuffer = CLIP(prn:szBuffer) & CLIP(OrderLine) & CHR(13) & CHR(10)

        RecordCount += 1
        !-----------------------------------------------------------------
!-----------------------------------------------
After   PROCEDURE (LookFor, LookIn)! STRING
FirstChar     LONG
    CODE
        !-----------------------------------------------------------------
        FirstChar = INSTRING( LookFor, LookIn, 1, 1 )
        IF FirstChar = 0
            RETURN ''
        END !IF

        FirstChar += LEN(LookFor)

        RETURN SUB(LookIn, FirstChar, 255)
        !-----------------------------------------------------------------
AppendString    PROCEDURE(IN:First, IN:Second, IN:Separator)!STRING
    CODE
        !------------------------------------------------------------------
        IF CLIP(IN:First) = ''
            RETURN IN:Second

        ELSIF CLIP(IN:Second) = ''
            RETURN IN:First
        END ! IF

        RETURN CLIP(IN:First) & IN:Separator & CLIP(IN:Second)
        !------------------------------------------------------------------
Before   PROCEDURE (LookFor, LookIn)! STRING
FirstChar     LONG
    CODE
        !-----------------------------------------------------------------
        FirstChar = INSTRING( CLIP(LEFT(LookFor)), LookIn, 1, 1 )
        IF FirstChar = 0
            RETURN ''
        END !IF

        RETURN SUB(LookIn, 1, FirstChar - 1)
        !-----------------------------------------------------------------
CalcLabour          PROCEDURE()! REAL
LabourAmount REAL
    CODE
        !-----------------------------------------------------------------
        LabourAmount    = 0

        IF job:chargeable_job <> 'YES'
            RETURN LabourAmount
        END ! IF

        IF job:ignore_chargeable_charges = 'YES'
            RETURN LabourAmount
        END ! IF

        Access:CHARTYPE.ClearKey(cha:charge_type_key)
        IF job:warranty_job = 'YES'
            cha:charge_type = job:warranty_charge_type
        ELSE
            cha:charge_type = job:charge_type
        END !IF

        If Access:CHARTYPE.Fetch(cha:charge_type_key)
            RETURN LabourAmount
        End !If access:chartype.clearkey(cha:charge_type_key)

        If cha:no_charge = 'YES'
            RETURN LabourAmount
        End !If cha:no_charge = 'YES'

        IF NOT LoadSUBTRACC( job:Account_Number )
            GOTO Final
        END !IF

        IF NOT LoadTRADEACC( sub:Main_Account_Number )
            GOTO Final
        END !IF

        If     tra:invoice_sub_accounts = 'YES'
            Access:SUBCHRGE.ClearKey(suc:model_repair_type_key)
            suc:account_number = job:account_number
            suc:model_number   = job:model_number
            suc:charge_type    = job:charge_type
            suc:unit_type      = job:unit_type
            suc:repair_type    = job:repair_type
            if Access:SUBCHRGE.Fetch(suc:model_repair_type_key)
                access:trachrge.clearkey(trc:account_charge_key)
                trc:account_number = sub:main_account_number
                trc:model_number   = job:model_number
                trc:charge_type    = job:charge_type
                trc:unit_type      = job:unit_type
                trc:repair_type    = job:repair_type
                if Access:TRACHRGE.Fetch(trc:account_charge_key) = Level:Benign
                    LabourAmount = trc:cost

                    RETURN LabourAmount
                End!if access:trachrge.fetch(trc:account_charge_key)
            Else
                LabourAmount = suc:cost

                RETURN LabourAmount
            End!if access:subchrge.fetch(suc:model_repair_type_key)

        Else!If tra:invoice_sub_accounts = 'YES'
            Access:TRACHRGE.clearkey(trc:account_charge_key)
            trc:account_number = sub:main_account_number
            trc:model_number   = job:model_number
            trc:charge_type    = job:charge_type
            trc:unit_type      = job:unit_type
            trc:repair_type    = job:repair_type
            if Access:TRACHRGE.fetch(trc:account_charge_key) = Level:Benign
                LabourAmount = trc:cost

                RETURN LabourAmount
            End!if access:trachrge.fetch(trc:account_charge_key)

        End!If tra:invoice_sub_accounts = 'YES'

Final   Access:STDCHRGE.clearkey(sta:model_number_charge_key)
        sta:model_number = job:model_number
        IF job:warranty_job = 'YES'
            sta:charge_type  = job:warranty_charge_type
        ELSE
            sta:charge_type  = job:charge_type
        END !IF

        sta:unit_type    = job:unit_type
        sta:repair_type  = job:repair_type
        IF Access:STDCHRGE.fetch(sta:model_number_charge_key) = Level:Benign
            LabourAmount = sta:cost
        END !IF access:stdchrge.fetch(sta:model_number_charge_key)
        !-----------------------------------------------------------------
        RETURN LabourAmount
        !-----------------------------------------------------------------

DateToString PROCEDURE(IN:Date)!STRING
    CODE
        !------------------------------------------------------------------
        ! Original
        !
        !RETURN LEFT(FORMAT(IN:Date, @D8)) ! DD MM YYYY
        !------------------------------------------------------------------
        ! 2 Sep 2002 0269 Issues identified on the Vodacom Invoices are:
        !   1. Dates showing in wrong format
        !
        !RETURN LEFT(FORMAT(IN:Date, @D17)) ! Windows short date format
        !------------------------------------------------------------------
        ! 4 Sep 2002 Dates - show in UK format i.e. DD/MM/YYYY Still showing in the wrong format - showing as mm/dd/yyyy
        !
        RETURN LEFT(FORMAT(IN:Date, @D6)) ! DD/MM/YYYY
        !------------------------------------------------------------------
GetCustomerName PROCEDURE()!STRING
local:TEMP STRING(100)
    CODE
        !------------------------------------------------------------------
        ! STRING(30)!(job:Title+job:Initials+job:Surname) / job:Company_Name
        !------------------------------------------------------------------
        local:TEMP = AppendString( job:Title, job:Initial, ' ')
        local:TEMP = AppendString(      local:TEMP, job:Surname, ' ')

        IF CLIP(local:TEMP) = ''
            RETURN job:Company_Name
        END !IF

        RETURN CLIP(local:TEMP)
        !------------------------------------------------------------------
GetDeliveryTime PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        ! 2 Sep 2002 0269 Issues identified on the Vodacom Invoices are:
        !   2. Time Out missing - show time printed if not despatched (as do with date).
        !       If the job HAS been despatched then show the time despatched.
        !
        !------------------------------------------------------------------
        IF job:Despatch_Number = 0
            RETURN FORMAT(CLOCK(), @T1)
        END !IF

        Access:DESBATCH.ClearKey(dbt:Batch_Number_Key)
            dbt:Batch_Number = job:Despatch_Number
        SET(dbt:Batch_Number_Key, dbt:Batch_Number_Key)

        IF Access:DESBATCH.NEXT() = Level:Benign
            RETURN FORMAT(dbt:Time, @T1)
        ELSE
            RETURN FORMAT(CLOCK(), @T1)
        END !IF
        !------------------------------------------------------------------
GetExpectedShipDate PROCEDURE()!STRING(2)
    CODE
        !------------------------------------------------------------------
        ! STRING('Delivery Not Known')!put "Delivery Not Known" if job:Current_Status IN("Awaiting Spares", "Spares Requested")
        !
        CASE job:Current_Status
        OF 'Awaiting Spares' OROF 'Spares Requested'
            RETURN 'Delivery Not Known'
        ELSE
            RETURN ''
        END !CASE
        !------------------------------------------------------------------
GetFaultDescriptionLine PROCEDURE( IN:String, OUT:Line1, OUT:Line2 )
LookFor STRING(1)
Pos     LONG
    CODE
        !-----------------------------------------------------------------
        !LookFor = CHR(13) & ''
        Pos     = INSTRING('<13,10>', IN:String, 1, 1)

        IF Pos = 0
            Pos = 55!SIZE(narrative:Text1)

            OUT:line1 = SUB(in:String,     1, Pos)
            OUT:line2 = SUB(in:String, Pos+1, 255)
        ELSE
            OUT:line1 = SUB(in:String,     1, Pos-1)
            OUT:line2 = SUB(in:String, Pos+2,   255)

            Pos = INSTRING('<13,10>', OUT:line2, 1, 1)
            IF Pos <> 0
                OUT:line2 = SUB(OUT:line2, 1, Pos-1)
            END !IF
        END !IF

        RETURN
        !-----------------------------------------------------------------
GetParameters       PROCEDURE()! LONG ! BOOL
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('Attempting to use ' & Clip(loc:ProgramName) & ' without using ' & Clip(loc:ApplicationName) & '.'&|
              '<13,10>'&|
              '<13,10>Start ' & Clip(loc:ApplicationName) & ' and run the report from there.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive

           RETURN False
        END !IF tmpPos
        !-----------------------------------------------
!        tmpPos = INSTRING('Job(', CommandLine)
!        IF NOT tmpPos
!            Case MessageEx('Attempting to use ' & CLIP(LOC:ProgramName) & '<10,13>'            & |
!                '   without passing Job Number.'                                               & |
!                LOC:ApplicationName,                                                             |
!                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!            Of 1 ! &OK Button
!            END!Case MessageEx
!
!           RETURN False
!        END !IF tmpPos
        !-----------------------------------------------
        param:Username     = CLIP(After('%', CommandLine))
        !param:JobNumber    = 247 !9602
        param:JobNumber    = Before(')', After('Job(', CommandLine))

        RETURN True
        !-----------------------------------------------

GetPrinterName PROCEDURE()! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:DEFPRINT.ClearKey(dep:Printer_Name_Key)
            dep:Printer_Name = CLIP(LOC:ProgramName)
        IF Access:DEFPRINT.TryFetch(dep:Printer_Name_Key) = Level:Benign
            prn:Local_Printer_Name = dep:Printer_Path
            IF CLIP(prn:Local_Printer_Name) <> ''
                RETURN True
            END !IF

        END !IF

        RETURN False
        !-----------------------------------------------
GetUserName  PROCEDURE( IN:Username )!LONG ! BOOL
    CODE
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = IN:Username ! CLIP(SUB(CommandLine, tmpPos + 1, 30))

        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Unable to find your logged in user details.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
           RETURN False
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        RETURN True
        !-----------------------------------------------
InsertAmountToDetailQueue       PROCEDURE( IN:Row, IN:Amount )
    CODE
        !------------------------------------------------------------------
        GET(Detail_Queue, IN:Row) ! labour
            detail:Amount = FORMAT( IN:Amount, @n_10.2)
            PUT(Detail_Queue)
        !------------------------------------------------------------------
IsStockPart PROCEDURE( IN:PartNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------------------------
        IF NOT LoadStock( par:Part_Ref_Number )
            RETURN False
        END !IF

        IF sto:Accessory = 'YES'
            RETURN False
        ELSE
            RETURN True
        END !IF
        !-----------------------------------------------------------------
IsWebJob    PROCEDURE()! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF glo:WebJob then
            RETURN True
        ELSE
            RETURN False
        END
        !------------------------------------------------------------------
!
! Taken from
!        !------------------------------------------------------------------
!        ! Webify tmp:Ref_Number
!        !
!        tmp:Ref_number = job:Ref_number
!        if glo:WebJob then
!            !Change the tmp ref number if you
!            !can Look up from the wob file
!            access:Webjob.clearkey(wob:RefNumberKey)
!            wob:RefNumber = job:Ref_number
!            if access:Webjob.fetch(wob:refNumberKey)=level:benign then
!                access:tradeacc.clearkey(tra:Account_Number_Key)
!                    tra:Account_Number = ClarioNET:Global.Param2
!                access:tradeacc.fetch(tra:Account_Number_Key)
!
!                tmp:Ref_Number = Job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
!            END
!            !Set up no preview on client
!            ClarioNET:UseReportPreview(0)
!        END
!        !------------------------------------------------------------------
LoadINVOICE PROCEDURE( IN:InvoiceNumber )! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        ! RefNumberKey KEY( jbn:RefNumber ),NOCASE,PRIMARY
        !
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = IN:InvoiceNumber
        Set(inv:Invoice_Number_Key, inv:Invoice_Number_Key)

        IF Access:INVOICE.NEXT() <> Level:Benign
            RETURN False
        End !IF

        IF NOT inv:Invoice_Number = IN:InvoiceNumber
            RETURN False
        End !IF

        RETURN True
        !------------------------------------------------------------------
LoadJobNotes PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        ! RefNumberKey KEY( jbn:RefNumber ),NOCASE,PRIMARY
        !
        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = IN:JobNumber
        Set(jbn:RefNumberKey, jbn:RefNumberKey)

        IF Access:JOBNOTES.NEXT() !<> Level:Benign
            RETURN False
        End !IF

        IF NOT jbn:RefNumber = IN:JobNumber
            RETURN False
        End !IF

        RETURN True
        !------------------------------------------------------------------
LoadStock PROCEDURE( IN:StockNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !
        ! Manufacturer_Accessory_Key    KEY(sto:Location,sto:Accessory,sto:Manufacturer,sto:Part_Number                   ),DUP,NOCASE
        ! Shelf_Location_Accessory_Key  KEY(sto:Location,sto:Accessory,                 sto:Shelf_Location                ),DUP,NOCASE
        ! Location_Part_Description_Key KEY(sto:Location,                               sto:Part_Number,   sto:Description),DUP,NOCASE
        ! Description_Accessory_Key     KEY(sto:Location,sto:Accessory,                                    sto:Description),DUP,NOCASE
        ! Part_Number_Accessory_Key     KEY(sto:Location,sto:Accessory,                 sto:Part_Number                   ),DUP,NOCASE
        ! Location_Manufacturer_Key     KEY(sto:Location,              sto:Manufacturer,sto:Part_Number                   ),DUP,NOCASE
        ! Supplier_Accessory_Key        KEY(sto:Location,sto:Accessory,                 sto:Shelf_Location                ),DUP,NOCASE
        ! Description_Key               KEY(sto:Location,                                                  sto:Description),DUP,NOCASE
        ! Location_Key                  KEY(sto:Location,                               sto:Part_Number                   ),DUP,NOCASE
        ! Ref_Part_Description_Key      KEY(sto:Location,sto:Ref_Number,                sto:Part_Number,   sto:Description),DUP,NOCASE
        ! Shelf_Location_Key            KEY(sto:Location,                               sto:Shelf_Location                ),DUP,NOCASE
        ! SecondLocKey                  KEY(sto:Location,                               sto:Shelf_Location,sto:Second_Location,sto:Part_Number),DUP,NOCASE
        ! Supplier_Key                  KEY(sto:Location,sto:Supplier),DUP,NOCASE

        ! Ref_Part_Description2_Key     KEY(sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE
        ! ExchangeAccDescKey            KEY(sto:ExchangeUnit,sto:Location,sto:Description),DUP,NOCASE
        ! ExchangeAccPartKey            KEY(sto:ExchangeUnit,sto:Location,sto:Part_Number),DUP,NOCASE

        ! Manufacturer_Key              KEY(sto:Manufacturer,sto:Part_Number),DUP,NOCASE
        ! Minimum_Description_Key       KEY(sto:Minimum_Stock,sto:Location,sto:Description),DUP,NOCASE
        ! Minimum_Part_Number_Key       KEY(sto:Minimum_Stock,sto:Location,sto:Part_Number),DUP,NOCASE
        ! RequestedKey                  KEY(sto:QuantityRequested,sto:Part_Number),DUP,NOCASE
        ! Ref_Number_Key                KEY(sto:Ref_Number),NOCASE,PRIMARY
        ! Sundry_Item_Key               KEY(sto:Sundry_Item),DUP,NOCASE
        !
        !-----------------------------------------------
        Access:STOCK.ClearKey( sto:Ref_Number_Key )
            sto:Ref_Number = IN:StockNumber
        SET(sto:Ref_Number_Key, sto:Ref_Number_Key)

        IF Access:STOCK.Next() <> Level:Benign
            RETURN False
        END !IF

        IF sto:Ref_Number <> IN:StockNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadSUBTRACC        PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        IF sub:Account_Number = IN:AccountNumber
            ! Only load if not already at correct record
            RETURN True
        END !IF
        !-----------------------------------------------
        ! Account_Number_Key       KEY(sub:Account_Number),NOCASE
        ! Branch_Key               KEY(sub:Branch),DUP,NOCASE
        ! Company_Name_Key         KEY(sub:Company_Name),DUP,NOCASE
        ! Main_Account_Key         KEY(sub:Main_Account_Number,sub:Account_Number),NOCASE,PRIMARY
        ! Main_Branch_Key          KEY(sub:Main_Account_Number,sub:Branch),DUP,NOCASE
        ! Main_Name_Key            KEY(sub:Main_Account_Number,sub:Company_Name),DUP,NOCASE
        !
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = IN:AccountNumber
        SET(sub:Account_Number_Key, sub:Account_Number_Key)

        IF Access:SUBTRACC.NEXT() ! <> Level:Benign
            sub:Account_Number = ''

            RETURN False
        END !IF

        IF NOT sub:Account_Number = IN:AccountNumber
            sub:Account_Number = ''

            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC        PROCEDURE( IN:MainAccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        IF tra:Account_Number = IN:MainAccountNumber
            ! Only load if not already at correct record
            RETURN True
        END !IF

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:MainAccountNumber
        SET(tra:Account_Number_Key, tra:Account_Number_Key)

        IF Access:TRADEACC.NEXT() <> Level:Benign
            tra:Account_Number = ''

            RETURN False
        END !IF

        IF NOT tra:Account_Number = IN:MainAccountNumber
            tra:Account_Number = ''

            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
!-----------------------------------------------
ClosePrinter        PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        IF prn:hPrinter <> 0
            ClosePrinter( prn:hPrinter )
        END !IF
        !-----------------------------------------------------------------
OpenPrinter                 PROCEDURE()! STRING WHERE '' = SUCCESS, ERROR MESSAGE OTHERWISE
NumBytes LONG
    CODE
        !------------------------------------------------------------------
        prn:szPrintername = CLIP(prn:Local_Printer_Name)

        IF NOT OpenPrinter(prn:szPrinterName, prn:hPrinter, 0) THEN
            RETURN 'Unable to open printer "' & prn:szPrinterName & '"'
        END !IF

        CLEAR(DocInfoGrp)

        prn:szDocName  = CLIP(LOC:ProgramName) ! 'Delivery Note'
        prn:szDataType = 'RAW'

        DocInfoGrp.pDocName    = ADDRESS(prn:szDocName)
        DocInfoGrp.pOutputFile = 0
        DocInfoGrp.pDataType   = ADDRESS(prn:szDataType)

        IF NOT StartDocPrinter( prn:hPrinter, 1, ADDRESS(DocInfoGrp) ) THEN
            RETURN 'Unable to StartDocPrinter()'
        END !IF
        !------------------------------------------------------------------
        RETURN ''
        !------------------------------------------------------------------
!-----------------------------------------------
ReplaceString               PROCEDURE (IN:String, IN:LookFor, IN:ReplaceWith)! STRING
FirstChar     LONG
LEN_IN_String LONG
LEN_LookFor   LONG
    CODE
        !-----------------------------------------------------------------
        FirstChar = INSTRING( CLIP(IN:LookFor), IN:String )
        IF FirstChar = 0
            ! Not in string
            RETURN IN:String
        END !IF

        LEN_IN_String = LEN(CLIP( IN:String))
        LEN_LookFor   = LEN(CLIP(IN:LookFor))

        IF FirstChar = 1
            ! First characters in string
            RETURN IN:ReplaceWith & ReplaceString(SUB(IN:String, FirstChar+LEN_LookFor, LEN_IN_String - LEN_LookFor), IN:LookFor, IN:ReplaceWith)
        END !IF

        IF (FirstChar + LEN_LookFor) > LEN_IN_String
            ! Last characters in string
            RETURN SUB(IN:String, 1, FirstChar - 1)
        END !IF

       RETURN SUB(IN:String, 1, FirstChar - 1) & IN:ReplaceWith & ReplaceString( After(IN:LookFor, IN:String), IN:LookFor, IN:ReplaceWith )
        !-----------------------------------------------------------------
SetupInvoiceAddress         PROCEDURE()
    CODE
     !------------------------------------------------------------------
        SET(DEFAULTS, 0)
        Access:DEFAULTS.NEXT()

        IF NOT LoadSUBTRACC(job:Account_Number)
            RETURN
        END !IF

        IF NOT LoadTRADEACC(sub:Main_Account_Number)
            RETURN
        END !IF
        !------------------------------------------------------------------
        if tra:invoice_sub_accounts = 'YES'
            If sub:invoice_customer_address = 'YES'
                address:User_Name        = job:company_name
                address:Address_Line1    = job:address_line1
                address:Address_Line2    = job:address_line2
                address:Address_Line3    = job:address_line3
                address:Postcode         = job:postcode
                address:Telephone_Number = job:telephone_number

            Else!If sub:invoice_customer_address = 'YES'
                address:User_Name        = sub:company_name
                address:Address_Line1    = sub:address_line1
                address:Address_Line2    = sub:address_line2
                address:Address_Line3    = sub:address_line3
                address:Postcode         = sub:postcode
                address:Telephone_Number = sub:telephone_number
                address:VatNumber        = sub:Vat_Number

            End!If sub:invoice_customer_address = 'YES'

            RETURN
        End!!if tra:use_sub_accounts = 'YES'
        !------------------------------------------------------------------
        If tra:invoice_customer_address = 'YES'
            address:User_Name        = job:company_name
            address:Address_Line1    = job:address_line1
            address:Address_Line2    = job:address_line2
            address:Address_Line3    = job:address_line3
            address:Postcode         = job:postcode
            address:Telephone_Number = job:telephone_number

            RETURN
        End!If tra:invoice_customer_address = 'YES'
        !------------------------------------------------------------------
        address:User_Name        = tra:company_name
        address:Address_Line1    = tra:address_line1
        address:Address_Line2    = tra:address_line2
        address:Address_Line3    = tra:address_line3
        address:Postcode         = tra:postcode
        address:Telephone_Number = tra:telephone_number
        address:VatNumber        = tra:Vat_Number
!        !------------------------------------------------------------------
!
!
!        ! Default values
!        address:User_Name        = def:User_Name
!        address:Address_Line1    = def:Address_Line1
!        address:Address_Line2    = def:Address_Line2
!        address:Address_Line3    = def:Address_Line3
!        address:Postcode         = def:Postcode
!        address:Telephone_Number = def:Telephone_Number
!        address:VatNumber        = def:Vat_Number
!!  return
!
!        !Amended by Neil T036 21/09/02
!
!        If glo:WebJob
!            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!            tra:Account_Number  = GETINI('Booking','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
!
!            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!              address:User_Name        = tra:company_name
!              address:Address_Line1    = tra:address_line1
!              address:Address_Line2    = tra:address_line2
!              address:Address_Line3    = tra:address_line3
!              address:Postcode         = tra:postcode
!              address:Telephone_Number = tra:telephone_number
!              address:VatNumber        = tra:Vat_Number
!            END
!
!        Else !If glo:WebJob
!            !Show the address of the booking account on the invoice
!            Access:JOBS.Clearkey(job:InvoiceNumberKey)
!            job:Invoice_Number  = glo:Select1
!            If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
!                !Found
!                Access:WEBJOB.Clearkey(wob:RefNumberKey)
!                wob:RefNumber   = job:Ref_Number
!                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                    !Found
!                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!                    tra:Account_Number  = wob:HeadAccountNumber
!                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!                      address:User_Name        = tra:company_name
!                      address:Address_Line1    = tra:address_line1
!                      address:Address_Line2    = tra:address_line2
!                      address:Address_Line3    = tra:address_line3
!                      address:Postcode         = tra:postcode
!                      address:Telephone_Number = tra:telephone_number
!                      address:VatNumber        = tra:Vat_Number
!                    END
!
!                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                    !Error
!                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!            Else ! If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
!                !Error
!            End !If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
!
!        End !If glo:WebJob
!!        IF NOT LoadSUBTRACC(job:Account_Number)
!!            RETURN
!!        END !IF
!!
!!        IF NOT LoadTRADEACC(sub:Main_Account_Number)
!!            RETURN
!!        END !IF
!!        !------------------------------------------------------------------
!!        if tra:invoice_sub_accounts = 'YES'
!!            If sub:invoice_customer_address = 'YES'
!!                address:User_Name        = job:company_name
!!                address:Address_Line1    = job:address_line1
!!                address:Address_Line2    = job:address_line2
!!                address:Address_Line3    = job:address_line3
!!                address:Postcode         = job:postcode
!!                address:Telephone_Number = job:telephone_number
!!
!!            Else!If sub:invoice_customer_address = 'YES'
!!                address:User_Name        = sub:company_name
!!                address:Address_Line1    = sub:address_line1
!!                address:Address_Line2    = sub:address_line2
!!                address:Address_Line3    = sub:address_line3
!!                address:Postcode         = sub:postcode
!!                address:Telephone_Number = sub:telephone_number
!!
!!            End!If sub:invoice_customer_address = 'YES'
!!
!!            RETURN
!!        End!!if tra:use_sub_accounts = 'YES'
!!        !------------------------------------------------------------------
!!        If tra:invoice_customer_address = 'YES'
!!            address:User_Name        = job:company_name
!!            address:Address_Line1    = job:address_line1
!!            address:Address_Line2    = job:address_line2
!!            address:Address_Line3    = job:address_line3
!!            address:Postcode         = job:postcode
!!            address:Telephone_Number = job:telephone_number
!!
!!            RETURN
!!        End!If tra:invoice_customer_address = 'YES'
!!        !------------------------------------------------------------------
!!        address:User_Name        = tra:company_name
!!        address:Address_Line1    = tra:address_line1
!!        address:Address_Line2    = tra:address_line2
!!        address:Address_Line3    = tra:address_line3
!!        address:Postcode         = tra:postcode
!!        address:Telephone_Number = tra:telephone_number
!!        !------------------------------------------------------------------
UpdateScreen PROCEDURE( IN:Message ) ! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        ?progress:userstring{prop:text} = CLIP(IN:Message)

        DO getnextrecord2
        DO CancelCheck

        IF progress:CancelPressed = True
            RETURN True
        ELSE
            RETURN False
        END !IF
        !------------------------------------------------------------------
WriteDebug  PROCEDURE( IN:Message )
    CODE
        !------------------------------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'c:\Debug.ini')
        !------------------------------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
PageNames            PROCEDURE  (pagenumber)          ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
pagefile file,driver('DOS'),name(NAMEVAR),create
rec record
f1  long
    ..
tmp:DesktopFolder   CString(255)
tmp:TempFilePath    CString(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    a#=a#+1
    If glo:ReportName = 'ESTIMATEALERT'
        ! Save the estimate to default folder and save name (DBH: 18-05-2006)
        tmp:DesktopFolder = Format(Today(), @d012) & Format(Clock(), @n010) & '.wmf'
        If GetTempPathA(255, tmp:TempFilePath)
            If Sub(tmp:TempFilePath, -1, 1) = '\'
                tmp:DesktopFolder = Clip(tmp:TempFilePath) & Clip(tmp:DesktopFolder)
            Else ! If Sub(TempFilePath,-1,1) = '\'
                tmp:DesktopFolder = Clip(tmp:TempFilePath) & '\' & Clip(tmp:DesktopFolder)
            End ! If Sub(tmp:TempFilePath,-1,1) = '\'
        End ! If
        namevar = tmp:DesktopFolder
    Else ! If glo:ReportName = 'ESTIMATEALERT'
        If glo:WebJob
            tmp:DesktopFolder = Format(Today(), @d012) & Format(Clock(), @n010) & Format(pagenumber, @n04) & '.wmf'
            If GetTempPathA(255, tmp:TempFilePath)
                If Sub(tmp:TempFilePath, -1, 1) = '\'
                    tmp:DesktopFolder = Clip(tmp:TempFilePath) & Clip(tmp:DesktopFolder)
                Else ! If Sub(TempFilePath,-1,1) = '\'
                    tmp:DesktopFolder = Clip(tmp:TempFilePath) & '\' & Clip(tmp:DesktopFolder)
                End ! If Sub(tmp:TempFilePath,-1,1) = '\'
            End ! If
            namevar = tmp:DesktopFolder
            flq:FileName = namevar
            Add(FileListQueue)
        Else ! If glo:WebJob
            SHGetSpecialFolderPath( GetDesktopWindow(), tmp:DesktopFolder, 16, FALSE )
            tmp:DesktopFolder = tmp:DesktopFolder & '\ServiceBase Reports'
            If ~Exists(tmp:DesktopFolder)
                If ~MkDir(tmp:DesktopFolder)
                ! Can't create desktop folder
                End ! If ~MkDir(tmp:Desktop)
            End ! If ~Exists(tmp:Desktop)
            tmp:DesktopFolder = tmp:DesktopFolder & '\' & Clip(glo:ReportName)
            If ~Exists(tmp:DesktopFolder)
                If ~MkDir(tmp:DesktopFolder)
                ! Can't create desktop folder
                End ! If ~MkDir(tmp:Desktop)
            End ! If ~Exists(tmp:Desktop)
            ! namevar=path()&'\page'&format(pagenumber,@n04)&'.wmf'
            namevar = tmp:DesktopFolder & '\' & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & format(pagenumber, @n04) & '.wmf'
        End ! If glo:WebJob

    End ! If glo:ReportName = 'ESTIMATEALERT'

    create(pagefile)
    return(namevar)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
PrintOption PROCEDURE (f:Option,f:Export,String f:ReportName) !Generated from procedure template - Window

tmp:CreateExport     BYTE(0)
tmp:ReturnValue      BYTE(0)
window               WINDOW('Print Option'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Print Preview Options'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Button:CancelPrint),TRN,FLAT,ICON('canprnp.jpg')
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Report Name'),AT(172,96,336,14),USE(?Prompt:ReportName),CENTER,FONT(,12,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP('Export And Print Options'),AT(174,112,334,46),USE(?Group4),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Export Report'),AT(180,132),USE(tmp:CreateExport),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Create Export'),TIP('Create Export'),VALUE('1','0')
                             PROMPT('If you select this option a WMF copy of this report will be created on your desk' &|
   'top. '),AT(254,130,248,22),USE(?Prompt:WMF),LEFT,FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           END
                           GROUP('Preview Report'),AT(248,162,184,46),USE(?Group:Preview),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           BUTTON,AT(307,176),USE(?Button:Preview),TRN,FLAT,ICON('prevrepp.jpg'),DEFAULT
                           GROUP('Print Report Without Preview'),AT(248,216,184,46),USE(?Group:Print),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           BUTTON,AT(307,232),USE(?Button:Print),TRN,FLAT,ICON('prnrepp.jpg')
                           GROUP('CSV Export'),AT(248,272,184,46),USE(?GroupCSV),BOXED,TRN,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                             BUTTON,AT(307,284),USE(?ButtonCSV),TRN,FLAT,ICON('CsvExpP.jpg')
                           END
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:ReturnValue)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020647'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('PrintOption')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If glo:WebJob
      ?Button:CancelPrint{prop:Hide} = True
      ?Group:Print{prop:Hide} = True
      ?Button:Print{prop:Hide} = True
  End ! If glo:WebJob
  ?Prompt:ReportName{prop:Text} = f:ReportName
  If SecurityCheck('EXPORT REPORTS')
      ?tmp:CreateExport{prop:Disable} = True
  End ! If SecurityCheck('EXPORT REPORTS')
  if glo:ExportToCSV = '?'
      unhide(?GroupCSV)
  END
      ! Save Window Name
   AddToLog('Window','Open','PrintOption')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','PrintOption')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020647'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020647'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020647'&'0')
      ***
    OF ?Button:CancelPrint
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CancelPrint, Accepted)
      tmp:ReturnValue = False
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CancelPrint, Accepted)
    OF ?Button:Preview
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Preview, Accepted)
      f:Option = 1
      f:Export = tmp:CreateExport
      tmp:ReturnValue = True
      glo:ExportToCSV = ''
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Preview, Accepted)
    OF ?Button:Print
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Print, Accepted)
      f:Option = 0
      f:Export = tmp:CreateExport
      glo:ExportToCSV = ''
      tmp:ReturnValue = True
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Print, Accepted)
    OF ?ButtonCSV
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCSV, Accepted)
      if tmp:CreateExport = true then
          miss# = missive('CSV export is incomplatable with the WMF export.|Please select again','ServiceBase 3g','mexclam.jpg','OK')
      ELSE
          f:Option = 0
          f:Export = 0
          glo:ExportToCSV = 'Y'
          tmp:ReturnValue = True
          Post(Event:CloseWindow)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCSV, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue







WayBillRetail PROCEDURE
RejectRecord         LONG,AUTO
save_retstock_id     USHORT,AUTO
save_waybillj_id     USHORT,AUTO
tmp:RecordsCount     LONG
save_res_id          USHORT,AUTO
tmp:PrintedBy        STRING(60)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Address_Group        GROUP,PRE()
Postcode             STRING(10)
Company_Name         STRING(30)
Building_Name        STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Address_Delivery_Group GROUP,PRE()
Postcode_Delivery    STRING(10)
Company_Name_Delivery STRING(30)
Building_Name_Delivery STRING(30)
Address_Line1_Delivery STRING(30)
Address_Line2_Delivery STRING(30)
Address_Line3_Delivery STRING(30)
Telephone_Delivery   STRING(15)
Fax_Number_Delivery  STRING(15)
                     END
Purchase_Order_Number STRING(30)
Account_Number       STRING(15)
Stock_queue          QUEUE,PRE(STOQUE)
Ref_Number           REAL,NAME('stoqueRef_Number')
Location             STRING(30),NAME('stoqueLocation')
Shelf_Location       STRING(30),NAME('stoqueShelf_Location')
Second_Location      STRING(30),NAME('stoqueSecond_Location')
Quantity             REAL,NAME('stoqueQuantity')
                     END
Delivery_Name_Temp   STRING(30)
Delivery_Address_Line_1_Temp STRING(60)
Invoice_Name_temp    STRING(30)
Invoice_address_Line_1 STRING(60)
items_total_temp     REAL
tmp:Type             STRING(4)
tmp:UserName         STRING(30)
tmp:WayBillBarcode   STRING(40)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(41)
Bar_Code_Temp        CSTRING(41)
Bar_Code_String2_Temp CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
!-----------------------------------------------------------------------------
Process:View         VIEW(WAYBILLS)
                       PROJECT(way:WayBillNumber)
                       JOIN(waj:JobNumberKey,way:WayBillNumber)
                       END
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('ORDERS Report'),AT(208,3750,7750,4052),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),THOUS
                       HEADER,AT(219,573,7750,2740),USE(?unnamed)
                         STRING('Tel:'),AT(4167,2240),USE(?string22:4),TRN,FONT(,8,,)
                         STRING(@s30),AT(4167,1458),USE(Delivery_Name_Temp),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1458),USE(Invoice_Name_temp),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(4167,1615,3438,208),USE(Delivery_Address_Line_1_Temp),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(208,1615,3281,208),USE(Invoice_address_Line_1),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4167,1771),USE(Address_Line2_Delivery),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1771),USE(Address_Line2),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(4490,2240),USE(Telephone_Delivery),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2240),USE(?string22:9),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2240),USE(Telephone_Number),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(4167,2396),USE(?string22:5),TRN,FONT(,8,,)
                         STRING(@s10),AT(4167,2083),USE(Postcode_Delivery),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s10),AT(208,2083),USE(Postcode),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4167,1927),USE(Address_Line3_Delivery),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1927),USE(Address_Line3),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(4490,2396),USE(Fax_Number_Delivery),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(208,2396),USE(?string22:8),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2396),USE(Fax_Number),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                       END
detail1                DETAIL,AT(,,,188),USE(?unnamed:6)
                         STRING(@s16),AT(167,0),USE(sto:Part_Number,,?sto:Part_Number:2),TRN,LEFT,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s25),AT(1250,0,1771,156),USE(sto:Description,,?sto:Description:2),TRN,LEFT,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s4),AT(2969,0),USE(tmp:Type,,?tmp:Type:2),TRN,LEFT,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s6),AT(6771,0),USE(STOQUE:Quantity),TRN,RIGHT,FONT('Tahoma',8,,,CHARSET:ANSI)
                         BOX,AT(7240,0,156,156),USE(?Box1),COLOR(COLOR:Black)
                         STRING(@s14),AT(5781,0),USE(sto:Second_Location),FONT(,8,,)
                         STRING(@s18),AT(3385,0),USE(sto:Location),TRN,LEFT,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s18),AT(4583,0),USE(sto:Shelf_Location),TRN,LEFT,FONT('Tahoma',8,,,CHARSET:ANSI)
                       END
total1                 DETAIL,AT(,,,1031),USE(?unnamed:7)
                         STRING('Invoice Number:'),AT(2344,52),USE(?String73),TRN,RIGHT,FONT(,12,,FONT:bold)
                         STRING(@s8),AT(4063,52),USE(ret:Invoice_Number),TRN,LEFT,FONT(,12,,FONT:bold)
                         STRING(@s20),AT(5000,94),USE(Bar_Code2_Temp),TRN,LEFT,FONT('C39 High 12pt LJ3',12,,FONT:regular,CHARSET:ANSI)
                         STRING('Total No Of Lines Invoiced:'),AT(1958,365),USE(?string26),TRN,RIGHT,FONT(,10,,FONT:bold)
                         STRING(@s8),AT(4063,365),USE(tmp:RecordsCount),TRN,LEFT,FONT(,10,,FONT:bold)
                         STRING('Total No Of Items Invoiced:'),AT(1917,677),USE(?string26:2),TRN,RIGHT,FONT(,10,,FONT:bold)
                         STRING(@s8),AT(4063,677),USE(items_total_temp),TRN,LEFT(14),FONT(,10,,FONT:bold)
                       END
                       FOOTER,AT(208,8208,7740,3198),USE(?unnamed:4)
                         STRING('Name:'),AT(208,104),USE(?String66),TRN
                         STRING(@s60),AT(938,104),USE(tmp:UserName),TRN,FONT(,,,FONT:bold)
                         STRING('Date:'),AT(208,365),USE(?String66:2),TRN
                         STRING(@d6),AT(938,365),USE(ReportRunDate),TRN,LEFT,FONT('Tahoma',8,,FONT:bold)
                         STRING('Time:'),AT(208,625),USE(?String66:3),TRN
                         STRING(@t1),AT(938,625),USE(ReportRunTime),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Signature:'),AT(208,938),USE(?String66:4),TRN
                         STRING('Name:'),AT(4167,104),USE(?String66:6),TRN
                         STRING('Date:'),AT(4167,365),USE(?String66:7),TRN
                         STRING('Time:'),AT(4167,625),USE(?String66:8),TRN
                         STRING('Signature:'),AT(4167,938),USE(?String66:5),TRN
                         STRING('Name:'),AT(208,1771),USE(?String66:10),TRN
                         STRING('Date:'),AT(208,2031),USE(?String66:11),TRN
                         STRING('Time:'),AT(208,2292),USE(?String66:12),TRN
                         STRING('Signature:'),AT(208,2604),USE(?String66:9),TRN
                       END
                       FORM,AT(219,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         BOX,AT(156,1719,3600,1300),USE(?BoxGrey:Address1),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4115,1719,3500,1300),USE(?BoxGrey:Address2),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4115,8021,3500,1300),USE(?BoxGrey:Address2:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:Silver)
                         STRING('DESCRIPTION OF GOODS \ SPECIAL INSTRUCTIONS'),AT(4167,9427),USE(?String65:4),TRN,FONT(,,,FONT:bold)
                         BOX,AT(4115,9688,3500,1300),USE(?BoxGrey:Address2:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:Silver)
                         BOX,AT(156,9688,3600,1300),USE(?BoxGrey:Address1:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:Silver)
                         BOX,AT(156,8021,3600,1300),USE(?BoxGrey:Address1:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:Silver)
                         BOX,AT(156,3125,7452,4531),USE(?BoxGrey:Detail),ROUND,FILL(COLOR:Silver)
                         STRING('FROM SENDER:'),AT(156,7708),USE(?String65),TRN,FONT(,,,FONT:bold)
                         STRING('RAM:'),AT(4167,7708),USE(?String65:2),TRN,FONT(,,,FONT:bold)
                         STRING('TO RECEIVER:'),AT(156,9427),USE(?String65:3),TRN,FONT(,,,FONT:bold)
                         BOX,AT(104,9635,3600,1300),USE(?Box:Address1:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4063,9635,3500,1300),USE(?Box:Address2:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,7969,3600,1300),USE(?Box:Address1:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4063,7969,3500,1300),USE(?Box:Address2:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,1667,3600,1300),USE(?Box:Address1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4063,1667,3500,1300),USE(?Box:Address2),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,3073,7452,4531),USE(?Box:Detail),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         STRING(@s30),AT(156,0,3844,240),USE(def:OrderCompanyName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:OrderAddressLine1),TRN,FONT(,9,,)
                         STRING(@s40),AT(2031,313,5260,208),USE(Bar_Code_Temp),TRN,RIGHT,FONT('C39 High 12pt LJ3',12,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(156,417,3844,156),USE(def:OrderAddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:OrderAddressLine3),TRN,FONT(,9,,)
                         STRING(@s12),AT(6250,573),USE(tmp:WayBillBarcode),TRN,RIGHT,FONT(,10,,FONT:bold)
                         STRING('Waybill: '),AT(5542,573),USE(?String70),TRN,FONT(,10,,)
                         STRING(@s30),AT(156,729,1156,156),USE(def:OrderPostcode),TRN,FONT(,9,,)
                         STRING(@s30),AT(5052,781),USE(Invoice_Name_temp,,?Invoice_Name_temp:2),TRN,RIGHT,FONT(,,,FONT:bold)
                         STRING('Tel:'),AT(156,990),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,990),USE(def:OrderTelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1146),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1146),USE(def:OrderFaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1302,3844,198),USE(def:OrderEmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1302),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('DELIVERY ADDRESS'),AT(4167,1510),USE(?string44:2),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(208,1510),USE(?string44:3),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('WayBillRetail')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','WayBillRetail')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'Waybill'
  If ClarioNETServer:Active() = True
      If PrintOption(PreviewReq,glo:ExportReport,'Retail Waybill') = False
          Do ProcedureReturn
      End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  End ! If ClarioNETServer:Active() = True
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:WAYBILLS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:RETSALES.Open
  Relate:STANTEXT.Open
  Relate:STOCK.Open
  Access:WAYBILLJ.UseFile
  Access:RETSTOCK.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(WAYBILLS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(WAYBILLS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Barcode Setup
        Code_Temp = 1 ! Code39
        Option_Temp = 0
        ! Inserting (DBH 03/11/2006) # 8308 - Prefix the waybill number with VDC
        tmp:WaybillBarCode = 'VDC' & Clip(glo:Select1)
        ! Changing (DBH 20/03/2007) # 8865 - Allow to alter the prefix
        !tmp:WaybillBarCode = 'VDC' & Clip(glo:Select1)
        ! to (DBH 20/03/2007) # 8865
        If GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
            tmp:WaybillBarCode = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Clip(glo:Select1)
        Else ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
            tmp:WaybillBarCode = 'VDC' & Clip(glo:Select1)
        End ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
        ! End (DBH 20/03/2007) #8865
        
        Bar_Code_String_Temp = Clip(tmp:WayBillBarcode)
        ! End (DBH 03/11/2006) #8308
        If Clip(Bar_Code_String_Temp) <> ''
            Sequence2(Code_Temp,Option_Temp,Bar_Code_String_Temp,Bar_Code_Temp)
        End ! If Clip(Bar_Code_String_Temp) <> ''
        
        ! #12249 Show Invoice Barcode (DBH: 17/01/2012)
        Bar_Code_String2_Temp = CLIP(ret:Invoice_Number)
        SEQUENCE2(Code_Temp,Option_Temp,Bar_Code_String2_Temp,Bar_Code2_Temp)
        Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
        way:WaybillNumber    = glo:Select1
        If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
            !Found
            JobNumber# = 0
            Save_WAYBILLJ_ID = Access:WAYBILLJ.SaveFile()
            Access:WAYBILLJ.Clearkey(waj:JobNumberKey)
            waj:WaybillNumber = way:WaybillNumber
            Set(waj:JobNumberKey,waj:JobNumberKey)
            Loop ! Begin Loop
                If Access:WAYBILLJ.Next()
                    Break
                End ! If Access:WAYBILLJ.Next()
                If waj:WaybillNumber <> way:WaybillNumber
                    Break
                End ! If waj:WaybillNumber <> way:WaybillNumber
                ! Inserting (DBH 20/10/2006) # 8381 - Should only be one job number, but use loop just in case
                JobNumber# = waj:JobNumber
                Break
                ! End (DBH 20/10/2006) #8381
            End ! Loop
            Access:WAYBILLJ.RestoreFile(Save_WAYBILLJ_ID)
        
            Access:RETSALES.ClearKey(ret:Ref_Number_Key)
            ret:Ref_Number = JobNumber#
            If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                !Found
                If ret:Payment_Method = 'CAS'
                    Delivery_Name_Temp   = ret:Contact_Name
                    Invoice_Name_Temp    = ret:Contact_Name
                Else ! If ret:Payment_Method = 'CAS'
                    Delivery_Name_Temp   = ret:Company_Name_Delivery
                    Invoice_Name_Temp    = ret:Company_Name
                End ! If ret:Payment_Method = 'CAS'
                If ret:Building_Name = ''
                    Invoice_Address_Line_1    = ret:Address_Line1
                Else ! If ret:Building_Name = ''
                    Invoice_Address_Line_1    = Clip(ret:Building_Name) & ' ' & Clip(ret:Address_Line1)
                End ! If ret:Building_Name = ''
                If ret:Building_Name_Delivery = ''
                    Delivery_Address_Line_1_Temp    = ret:Address_Line1_Delivery
                Else ! If ret:Building_Name_Delivery = ''
                    Delivery_Address_Line_1_Temp    = Clip(ret:Building_Name_Delivery) & ' ' & Clip(ret:Address_Line1_Delivery)
                End ! If ret:Building_Name_Delivery = ''
        
                Postcode    = ret:Postcode
                Company_Name    = ret:Company_Name
                Building_Name    = ret:Building_Name
                Address_Line1    = ret:Address_Line1
                Address_Line2    = ret:Address_Line2
                Address_Line3    = ret:Address_Line3
                Telephone_Number    = ret:Telephone_Number
                Fax_Number        = ret:Fax_Number
        
                Postcode_Delivery    = ret:Postcode_Delivery
                Company_Name_Delivery    = ret:Company_Name_Delivery
                Building_Name_Delivery    = ret:Building_Name_Delivery
                Address_Line1_Delivery    = ret:Address_Line1_Delivery
                Address_Line2_Delivery    = ret:Address_Line2_Delivery
                Address_Line3_Delivery    = ret:Address_Line3_Delivery
                Telephone_Delivery        = ret:Telephone_Delivery
                Fax_Number_Delivery       = ret:Fax_Number_Delivery
                Purchase_Order_Number     = ret:Purchase_Order_Number
                Account_Number            = ret:Account_Number
            Else ! If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                !Error
            End ! If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
        
            Access:USERS.ClearKey(use:Password_Key)
            use:Password    = glo:Password
            If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                !Found
                tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
            Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                !Error
            End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
        
            Save_RETSTOCK_ID = Access:RETSTOCK.SaveFile()
            Access:RETSTOCK.Clearkey(res:Despatched_Only_Key)
            res:Ref_Number  = ret:Ref_Number
            res:Despatched = 'YES'
            Set(res:Despatched_Only_Key,res:Despatched_Only_Key)
            Loop ! Begin Loop
                If Access:RETSTOCK.Next()
                    Break
                End ! If Access:RETSTOCK.Next()
                If res:Ref_Number <> ret:Ref_Number
                    Break
                End ! If res:Ref_Number <> ret:Ref_Number
        
                ! Inserting (DBH 20/10/2006) # 8381 - Don't show back orders
                If res:despatched <> 'YES'
                    Break
                End ! If res:despatched <> 'YES'
                ! End (DBH 20/10/2006) #8381
        
                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = res:Part_Ref_Number
                If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    stoque:Ref_Number    = sto:Ref_Number
                    Get(Stock_Queue,stoque:Ref_Number)
                    If Error()
                        stoque:Ref_Number   = sto:Ref_Number
                        stoque:Location     = sto:Location
                        stoque:Shelf_Location   = sto:Shelf_Location
                        stoque:Second_Location  = sto:Second_Location
                        stoque:Quantity         = res:Quantity
                        Add(Stock_Queue)
                    Else ! If Error
                        stoque:Quantity     += res:Quantity
                        Put(Stock_Queue)
                    End ! If Error
                Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
        
            End ! Loop
            Access:RETSTOCK.RestoreFile(Save_RETSTOCK_ID)
        
            Sort(Stock_queue,'stoquelocation,stoqueshelf_location,stoquesecond_location')
            Loop x# = 1 To Records(Stock_queue)
                Get(stock_queue,x#)
                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number  = stoque:Ref_Number
                If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    Items_Total_Temp += stoque:Quantity
        ! Deleting (DBH 03/11/2006) # 8308 - The individual lines aren't needed now
        !            Print(rpt:Detail1)
        ! End (DBH 03/11/2006) #8308
                Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
            End!Loop x# = 1 To Records(Stock_queue)
            tmp:RecordsCount = Records(Stock_Queue)
            Print(rpt:Total1)
        Else ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
            !Error
        End ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
        
        
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(WAYBILLS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','WayBillRetail')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:RETSALES.Close
    Relate:STANTEXT.Close
    Relate:STOCK.Close
    Relate:WAYBILLS.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 3
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  ! Inserting (DBH 20/10/2006) # 8381 - Allow to set the number of copies used
  If glo:ReportCopies > 0
      printer{propprint:Copies} = glo:ReportCopies
      glo:ReportCopies = 0
  End ! If glo:ReportCopies > 0
  ! End (DBH 20/10/2006) #8381
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='WayBillRetail'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END







Vodacom_Delivery_Note_Web PROCEDURE  (fJobNumber,f:Prefix,f:Suffix) ! Declare Procedure
save_jobs_id         USHORT,AUTO
save_jobse_id        USHORT,AUTO
save_webjob_id       USHORT,AUTO
save_invoice_id      USHORT,AUTO
save_tradeacc_id     USHORT,AUTO
LocalGlobalInFile    STRING(255),STATIC
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
InFile    File,Driver('ASCII'),Pre(inf),Name(LocalGlobalInFile),Create
InFileRec    Record
TextLine         String(30000)
             End
          End ! InFile    File,Driver('ASCII'),Pre(inf),Name(LocalGlobalInFile),Create

    Map
AddAddressLine        Procedure(Long,String)
OpenPrinter           Procedure(),Byte
PRintJob              Procedure(),Byte
    End

tmp:SubTotal        Real()
tmp:VAT             Real()
tmp:Total           Real()

DocInfoGrp    Group(doc_info_1)
              End

Printer_Group    Group,Pre(prn)
PageLength        Long(48)
DetailRows        Long(14)
Local_Printer_Name    String(80)
hPrinter          Unsigned
szPrinterName     CString(260)
dwBytesCopied     Long
dwWritten         Long
szBuffer          CString(30000)
szDocName         CString(80)
szDataType        CString(80)
                End ! Printer_Group    Group,Pre(prn)
BlankLine       String(80)

AddressLine    Group,Pre(Address)
Delivery            String(30)
Filler1             String(4)
Business            String(30)
               End ! AddressLine    Group,Pre(Address)

NarrativeLine  Group,Pre(narrative)
Text1                String(55)
Filler1              String(10)
Amount               String(10)
               End ! NarrativeLine  Group,Pre(narrative)


OrderLine        Group,Pre(Order)
DespatchDate        String(10)
Filler1            String(4)
Filler2            String(7)
InvoiceNumber        String(10)
Filler4            String(1)
AccountNumber        String(12)
Filler3            String(1)
YourOrderNumber    String(20)
                    End ! OrderLine        Group,Pre(Order)

IMEILine        Group,Pre(imeitmp)
Make                String(14)
Model                String(15)
SerialNumber            String(23)
IMEINumber            String(20)
                End ! IMEILine        Group,Pre(imeitmp)

DateLine        Group,Pre(date)
Filler0            String(10)
DateOut            String(10)
Filler1            String(22)
TimeOut            String(10)
Filler2            String(5)
Engineer            String(3)
                End ! DateLine        Group,Pre(date)

Detail_Queue        Queue,Pre(detail)
Line                String(56)
Filler1                String(10)
Amount                String(10)
                    End ! Detail_Queue        Queue,Pre(detail)

Address_Group        Group,Pre(Address)
User_Name            String(30)
Address_Line1        String(30)
Address_Line2        String(30)
Address_Line3        String(30)
Postcode            String(15)
Telephone_Number    String(15)
                    End ! Address_Grou        Group,Pre(Address)

AddressArray        String(30),DIM(2,8)

szHeader            CString(5000)
szFooter            Cstring(5000)

Left_Address_Group        Group,Pre(left)
CompanyName            String(30)
Line1                String(30)
Line2                String(30)
Line3                String(30)
Post_Code            String(15)
TelNo                String(15)
                    End

tmp:VatNumber            String(30)
tmp:AuditNotes    String(255)
tmp:InvoiceTime     Time()
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:AUDIT.Open
   Relate:JOBS.Open
   Relate:WEBJOB.Open
   Relate:JOBSE.Open
   Relate:INVOICE.Open
   Relate:TRADEACC.Open
   Relate:JOBSINV.Open
   Relate:DEFPRINT.Open
   Relate:JOBSE2.Open
    !Print Invoice
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = fJobNumber
    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        !Found
    Else ! If Access:JOBS.TryFetch(jobs:InvoiceNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBS.TryFetch(jobs:InvoiceNumberKey) = Level:Benign

    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign


    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

    !added by Paul 06/10/2010 - Log no 11691
    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber = job:Ref_Number
    If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign then
        !found
    Else
        !Error
    End

    Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
    inv:Invoice_Number = job:Invoice_Number
    If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        !Found
    Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        !Error
    End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign


    If f:Suffix <> '' And f:Prefix = ''
        ! Printing an invoice (DBH: 27/07/2007)
        Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
        jov:RefNumber = job:Ref_Number
        jov:Type = 'I'
        jov:Suffix = f:Suffix
        If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
            !Found
        Else ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
            !Error
        End ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
    End ! If f:Suffix <> ''
    If f:Prefix <> ''
        ! Printing a Credit Note (DBH: 27/07/2007)
        Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
        jov:RefNumber = job:Ref_Number
        jov:Type = 'C'
        jov:Suffix = f:Suffix
        If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
            !Found
        Else ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
            !Error
        End ! If Access:JOBSINV.TryFetch(jov:TypeSuffixKey) = Level:Benign
    End ! If f:Prefix <> ''


    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Found
        address:User_Name     = tra:Company_Name
        address:Address_Line1 = tra:Address_Line1
        address:Address_Line2 = tra:Address_Line2
        address:Address_Line3 = tra:Address_Line3
        address:Postcode      = tra:Postcode
        address:Telephone_Number = tra:Telephone_Number
    Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

    Do CheckCreateInvoice

    If OpenPrinter()
        Return 
    End ! If OpenPrinter()

    prn:szBuffer = CHR(27) & CHR(67) & CHR(prn:PageLength)
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine

    Do AddAddress
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine

    Do AddOrderLine
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine

    Do AddIMEILine
    Do AddLine

    szHeader = CLIP(prn:szBuffer)

    prn:szBuffer = ''

    Do AddLine

    ! Date In (DBH: 30/07/2007)
    Clear(BlankLine)
    date:DateOut = Format(job:Date_Booked,@d06)
    date:TimeOut = Format(job:Time_Booked,@t1)
    date:Engineer = ''
    prn:szBuffer = Clip(prn:szBuffer) & Clip(DateLine) & '<13,10>'

    Do AddLine

    ! Date Out (DBH: 30/07/2007)
    Clear(BlankLine)
    date:DateOut = Format(inv:RRCInvoiceDate,@d06)
    date:TimeOut = Format(tmp:InvoiceTime,@t01)
    If f:Suffix <> '' or f:Prefix <> ''
        date:DateOut = Format(jov:DateCreated,@d06)
        date:TimeOut = Format(jov:TimeCreated,@t01)
    End ! If f:Suffix <> ''


    date:Engineer = job:Engineer
    prn:szBuffer = Clip(prn:szBuffer) & Clip(DateLine) & '<13,10>'

    szFooter = Clip(prn:szBuffer) & '<12>'
    prn:szBuffer = ''

    Do CreatePartsList

    Loop x# = 1 To Records(Detail_Queue)
        Get(Detail_Queue,x#)
        Case x#
        Of 1
            detail:Amount = Format(jobe:invRRCCLabourCost,@n_10.2)
        Of 3
            detail:Amount = Format(jobe:invRRCCPartsCost,@n_10.2)
        Of 5
            detail:Amount = Format(job:invoice_Courier_Cost,@n_10.2)
        Of 10
            detail:Amount = Format(tmp:SubTotal,@n_10.2)
        Of 12
            detail:Amount = Format(tmp:VAT,@n_10.2)
        Of 14
            detail:Amount = Format(tmp:Total,@n-_10.2)
        End ! Case x#

        Put(Detail_Queue)
    End ! Loop x# = 1 To Records(Detail_Queue)

    Get(Detail_Queue,1)

    Loop x# = 1 To Records(Detail_Queue)
        Get(Detail_Queue,x#)
        Clear(BlankLine)
        narrative:Text1 = detail:Line
        narrative:Amount = detail:Amount
        prn:szBuffer = Clip(prn:szBuffer) & Clip(NarrativeLine)
        prn:szBuffer = Clip(prn:szBuffer) & '<13,10>'
    End ! Loop x# = 1 To Records(Detail_Queue)

    If PrintJob()
        If f:Prefix <> ''
            Case Missive('Credit Note Printed.','ServiceBase 3g',|
                           'midea.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        Else ! If f:Prefix <> ''
            Case Missive('Invoice Printed.','ServiceBase 3g',|
                           'midea.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If f:Prefix <> ''
    End ! If PrintJob()

    If ~glo:webJob
        IF prn:hPrinter <> 0
            ClosePrinter( prn:hPrinter )
        END !IF
    End ! If ~glo:webJob
          
   Relate:AUDIT.Close
   Relate:JOBS.Close
   Relate:WEBJOB.Close
   Relate:JOBSE.Close
   Relate:INVOICE.Close
   Relate:TRADEACC.Close
   Relate:JOBSINV.Close
   Relate:DEFPRINT.Close
   Relate:JOBSE2.Close
! Routines
AddIMEILine        Routine
    Clear(BlankLine)
    imeitmp:Make        = job:Manufacturer
    imeitmp:Model       = job:Model_Number
    imeitmp:SerialNumber = job:MSN
    imeitmp:IMEINumber = job:ESN

    prn:szBuffer = Clip(prn:szBuffer) & Clip(IMEILine) & '<13,10>'
AddAddress        Routine
        !------------------------------------------------------------------
        CLEAR(AddressArray)
        access:Webjob.clearkey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        if access:Webjob.fetch(wob:refNumberKey)=level:benign
          !Ok
        END

        save_JOBSE_id = Access:JOBSE.SaveFile()
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        Access:JOBSE.RestoreFile(save_JOBSE_id)


           Access:subtracc.clearkey(sub:Account_Number_Key)
           sub:Account_Number = job:Account_Number
           if Access:subtracc.fetch(sub:Account_Number_Key) then
              !error
                ! #11606 Not getting trade account correctly. HOW LONG has this been wrong?! (DBH: 04/08/2010)
                access:tradeacc.clearkey(tra:Account_Number_Key)
                tra:Account_Number = sub:Main_Account_Number
                if Access:tradeacc.fetch(tra:Account_Number_Key)
                end
           end
            !This report always uses the customer, or sub address - TrkBs: 5364 (DBH: 21-03-2005)

            !Set default vat number, from sub, or head account - TrkBs: 5364 (DBH: 21-03-2005)
            If sub:OverrideHeadVATNo = True
                tmp:VatNumber = sub:Vat_Number
            Else ! If sub:OverrideHeadVATNo = True
                tmp:VatNumber = tra:Vat_Number
            End ! If sub:OverrideHeadVATNo = True

           IF sub:Invoice_Customer_Address = 'YES'
             IF job:Company_Name = ''
               left:CompanyName = CLIP(job:Title)&' '&CLIP(job:Initial)&' '&CLIP(job:Surname)
             ELSE
               left:CompanyName = job:Company_Name
             END
             left:line1 = job:Address_Line1
             left:line2 = job:Address_Line2
             left:line3 = job:Address_Line3
             left:post_code = job:Postcode
             left:TelNo = job:Telephone_Number

             ! Start - If there is no account vat number, try the job one - TrkBs: 5364 (DBH: 22-04-2005)
             If tmp:VatNumber = ''
                tmp:VatNumber = jobe:VatNumber
             End ! If tmp:VatNumber = ''
             ! End   - If there is no account vat number, try the job one - TrkBs: 5364 (DBH: 22-04-2005)
           ELSE
             left:CompanyName = sub:Company_Name
             left:line1 = sub:Address_Line1
             left:line2 = sub:Address_Line2
             left:line3 = sub:Address_Line3
             left:post_code = sub:Postcode
             left:TelNo = sub:Telephone_Number
           END

!        else
!            left:CompanyName = tra:Company_Name
!            left:line1 = tra:Address_Line1
!            left:line2 = tra:Address_Line2
!            left:line3 = tra:Address_Line3
!            left:post_code = tra:Postcode
!            left:TelNo = tra:Telephone_Number
!            !In the impossible event of no subaccount, use the trade vat number (DBH: 21-03-2005)
!            tmp:VatNumber = tra:Vat_Number
!        end


        AddAddressLine( 2, left:CompanyName  )
        AddAddressLine( 2, left:line1 )
        AddAddressLine( 2, left:line2 )
        AddAddressLine( 2, left:line3 )
        AddAddressLine( 2, left:post_code      )
        AddAddressLine( 2, left:TelNo     )
        !Show the VAT number instead of the telephone number twice (DBH: 21-03-2005)
        AddAddressLine( 2, 'VAT No.: ' & tmp:VatNumber     )

        AddAddressLine( 1, address:User_Name        )
        AddAddressLine( 1, address:Address_Line1    )
        AddAddressLine( 1, address:Address_Line2    )
        AddAddressLine( 1, address:Address_Line3    )
        AddAddressLine( 1, address:Postcode         )
        AddAddressLine( 1, address:Telephone_Number )
        !------------------------------------------------------------------
        ! force job number to last line
        !
        If glo:WebJob
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = Clarionet:Global.Param2
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !ok!
            END

        Else !If glo:WebJob
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = wob:HeadAccountNumber
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !ok!
            END

        End !If glo:WebJob

        AddressArray[ 1,  8 ] = 'Job Number ' & job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
        !------------------------------------------------------------------
        LOOP x# = 1 TO 8  ! Lines 1..7 for addrsss, 8 = new line added for job number
            CLEAR(BlankLine)

            address:Delivery = AddressArray[ 2, x# ]
            address:Business = AddressArray[ 1,  x# ]

            prn:szBuffer = CLIP(prn:szBuffer) & CLIP(AddressLine)
            prn:szBuffer = CLIP(prn:szBuffer) & CHR(13) & CHR(10)
        END !LOOP
        !------------------------------------------------------------------
AddLine        Routine
    prn:szBuffer = Clip(prn:szBuffer) & '<13,10>'
AddOrderLine        Routine
    Clear(BlankLine)
    order:DespatchDate = Format(inv:RRCInvoiceDate,@d06)
    order:InvoiceNumber = Clip(f:Prefix) & Clip(job:Invoice_Number) & f:Suffix
    order:AccountNumber = job:Account_Number
    order:YourOrderNumber = job:Order_Number
    prn:szBuffer = Clip(prn:szBuffer) & Clip(OrderLine) & '<13,10>'
CheckCreateInvoice        Routine
Data
local:AuditNotes        String(255)
Code
    If inv:RRCInvoiceDate = 0
        ! Create an RRC Invoice (DBH: 27/07/2007)
        inv:RRCInvoiceDate = Today()

        tmp:InvoiceTime = Clock()

        jobe:InvRRCCLabourCost = jobe:RRCCLabourCost
        jobe:InvRRCCPartsCost = jobe:RRCCPartsCost
        jobe:InvRRCCSubTotal = jobe:RRCCSubTotal
        jobe:InvoiceHandlingFee = jobe:HandlingFee
        jobe:InvoiceExchangeRate = jobe:ExchangeRate
        Access:JOBSE.Update()

        inv:ExportedRRCOracle = 1

        Access:INVOICE.Update()

        If f:Suffix = '' And f:Prefix = ''
            ! Don't export credit notes (DBH: 02/08/2007)
            Line500_XML('RIV')
                !Invoice logging requested by harvey - 07/05/15
                MakeInvoiceLog('RIV')

        End ! If f:Suffix = '' And f:Prefix = ''

        ! Add Relevant Audit Entry (DBH: 27/07/2007)
        tmp:AuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
        If LoanAttachedToJob(job:Ref_Number)
            tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(job:Invoice_Courier_Cost,@n14.2)
        End ! If LoanAttachedToJob(job:Ref_Number)

        ! Show replacement charge (DBH: 27/07/2007)
        If jobe:Engineer48HourOption = 1
            If jobe:ExcReplcamentCharge
                tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>EXC REPL: ' & Format(job:Invoice_Courier_Cost,@n14.2)
            End ! If jobe:ExcReplcamentCharge
        End ! If jobe:Engineer48HourOption = 1

        tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>PARTS: ' & Format(jobe:InvRRCCPartsCost,@n14.2) & |
                                                 '<13,10>LABOUR: ' & Format(jobe:InvRRCCLabourCost,@n14.2) & |
                                                 '<13,10>SUB TOTAL: ' & Format(jobe:InvRRCCSubTotal,@n14.2) & |
                                                 '<13,10>VAT: ' & Format((jobe:InvRRCCPartsCOst * inv:RRCVatRateParts/100) + |
                                                        (jobe:InvRRCCLabourCost * inv:RRCVatRateLabour/100) + |
                                                        (job:Invoice_Courier_Cost * inv:RRCVatRateLabour/100),@n14.2) & |
                                                 '<13,10>TOTAL: ' & Format(jobe:InvRRCCSubTotal + (jobe:InvRRCCPartsCost * inv:RRCVatRateParts/100) + |
                                                        (jobe:InvRRCCLabourCost * inv:RRCVatRateLabour/100) + |
                                                        (job:Invoice_Courier_Cost * inv:RRCVatRateLabour/100),@n14.2)

        If AddToAudit(job:Ref_Number,'JOB','INVOICE CREATED',Clip(tmp:AuditNotes))

        End ! If AddToAudit(job:Ref_Number,'JOB','INVOICE CREATED',Clip(tmp:AuditNotes))
    Else ! If inv:RRCInvoiceDate = 0
        ! Reprint (DBH: 27/07/2007)
        If f:Prefix <> ''
            ! Credit Note (DBH: 27/07/2007)
            tmp:AuditNotes = 'CREDIT NOTE: ' & Clip(f:Prefix) & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(f:Suffix) & |
                            'AMOUNT: ' & Format(jov:CreditAmount,@n14.2)
            If AddToAudit(job:Ref_Number,'JOB','CREDIT NOTE PRINTED',Clip(tmp:AuditNotes))

            End ! If AddToAudit(job:Ref_Number,'JOB','CREDIT NOTE PRINTED',Clip(tmp:AuditNotes))
        Else ! If f:Prefix <> ''
            If f:Suffix <> ''
                ! Credit Invoice (DBH: 27/07/2007)
                tmp:AuditNotes = 'INVOICE NUMBER: ' & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(f:Suffix) & |
                                'AMOUNT: ' & Format(jov:NewTotalCost,@n14.2)
                If AddToAudit(job:Ref_Number,'JOB','CREDITED INVOICE PRINTED',Clip(tmp:AuditNotes))

                End ! If AddToAudit(job:Ref_Number,'JOB','CREDIT NOTE PRINTED',Clip(tmp:AuditNotes))
            Else ! If f:Suffix <> ''
                ! Normal Invoice Reprint (DBH: 27/07/2007)
                If AddToAudit(job:Ref_Number,'JOB','INVOICE REPRINTED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                                                                        '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06) )
                End ! '<13,10>INVOICE DATE: ' & FOrmat(inv:Date_Created,@d06)

                tmp:InvoiceTime = job:Time_Booked
                Access:AUDIT.Clearkey(aud:TypeActionKey)
                aud:Ref_Number = job:Ref_Number
                aud:Type = 'JOB'
                aud:Action = 'INVOICE CREATED'
                aud:Date = inv:RRCInvoiceDate
                Set(aud:TypeActionKey,aud:TypeActionKey)
                Loop ! Begin Loop
                    If Access:AUDIT.Next()
                        Break
                    End ! If Access:AUDIT.Next()
                    If aud:Ref_Number <> job:Ref_Number
                        Break
                    End ! If aud:Ref_Number <> job:Ref_Number
                    If aud:Type <> 'JOB'
                        Break
                    End ! If aud:Type <> 'JOB'
                    If aud:Action <> 'INVOICE CREATED'
                        Break
                    End ! If aud:Action <> 'INVOICE CREATED'
                    If aud:Date <> inv:RRCInvoiceDate
                        Break
                    End ! If aud:Date <> inv:RRCInvoiceDate
                    tmp:InvoiceTime = aud:Time
                    Break
                End ! Loop


            End ! If f:Suffix <> ''
        End ! If f:Prefix <> ''
    End ! If inv:RRCInvoiceDate = 0

    tmp:VAT = (jobe:InvRRCCLabourCost * inv:RRCVatRateLabour/100) + |
                                    (jobe:InvRRCCPartsCost * inv:RRCVatRateParts/100) + |
                                    (job:Invoice_Courier_Cost * inv:RRCVatRateLabour/100)
    tmp:SubTotal = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job:Invoice_Courier_Cost
    tmp:Total = tmp:SubTotal + tmp:VAT

    If f:Suffix <> ''
        tmp:Total = jov:NewTotalCost
    End ! If f:Suffix <> ''
    If f:Prefix <> ''
        tmp:Total = -jov:CreditAmount
    End ! If f:Prefix <> ''
CreatePartsList        Routine
Data
local:FaultDescription  String(255)
Code
    Free(Detail_Queue)
    Clear(Detail_Queue)

    Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
    jbn:RefNumber = job:Ref_Number
    If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign

    local:FaultDescription = Clip(BHStripReplace(jbn:Fault_Description,'<13,10>','. '))

    If Clip(local:FaultDescription) <> ''
        detail:Line = Clip(Sub(local:FaultDescription,1,55))
        detail:Amount = ''
        Add(Detail_Queue)
        detail:Line = Clip(Sub(local:FaultDescription,56,55))
        detail:Amount = ''
        Add(Detail_Queue)
        If jobe2:InvDiscountAmnt > 0 then
            detail:Line = 'OOW Discount ' & Format(jobe2:InvDiscountAmnt,@n_10.2)
            detail:Amount = ''
            Add(Detail_Queue)
        Else
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
        End
    Else ! If Clip(jbn:Fault_Description) <> ''
        If jobe2:InvDiscountAmnt > 0 then
            detail:Line = 'OOW Discount ' & Format(jobe2:InvDiscountAmnt,@n_10.2)
            detail:Amount = ''
            Add(Detail_Queue)
        Else
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
        End
        detail:Line = ''
        detail:Amount = ''
        Add(Detail_Queue)
        detail:Line = ''
        detail:Amount = ''
        Add(Detail_Queue)
    End ! If Clip(jbn:Fault_Description) <> ''

    detail:Line = ALL('-',55)
    detail:Amount = ''
    Add(Detail_Queue)

    ! Show Parts (DBH: 27/07/2007)
    RowNum# = 0
    Access:PARTS.Clearkey(par:Order_Number_Key)
    par:Ref_Number = job:Ref_Number
    Set(par:Order_Number_Key,par:Order_Number_Key)
    Loop ! Begin Loop
        If Access:PARTS.Next()
            Break
        End ! If Access:PARTS.Next()
        If par:Ref_Number <> job:Ref_Number
            Break
        End ! If par:Ref_Number <> job:Ref_Number
        RowNum# += 1
        detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(par:Description,1,50)
        detail:Amount = ''
        Add(Detail_Queue)
    End ! Loop

    ! Show Fault Codes (DBH: 27/07/2007)
    Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
    joo:JobNumber = job:Ref_Number
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop ! Begin Loop
        If Access:JOBOUTFL.Next()
            Break
        End ! If Access:JOBOUTFL.Next()
        If joo:JobNumber <> job:Ref_Number
            Break
        End ! If joo:JobNumber <> job:Ref_Number
        If Instring('IMEI VALIDATION',joo:Description,1,1)
            Cycle
        End ! If Instring('IMEI VALIDATION',joo:Description,1,1)
        RowNum# += 1
        detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(joo:Description,1,50)
        detail:Amount = ''
        Add(Detail_Queue)
    End ! Loop

    ! I guess this is filling in the blank lines (DBH: 27/07/2007)
    detail:Line = ''
    detail:Amount = ''
    Loop
        If Records(Detail_Queue) % prn:DetailRows = 0
            Break
        End ! If Records(Detail_Queue) % prn:DetailRows = 0
        Add(Detail_Queue)
    End ! Loop
! Procedures
AddAddressLine        Procedure(f:Vector,f:Value)
Code
    Loop x# = 1 To 7
        If AddressArray[f:Vector,x#] = ''
            AddressArray[f:Vector,x#] = f:Value
            Break
        End ! If AddressArray[f:Version,x#] = ''
    End ! Loop x# = 1 To &
OpenPrinter            Procedure
Code
    If glo:WebJob
        Return 0
    End ! If glo:WebJob

    Access:DEFPRINT.ClearKey(dep:Printer_Name_Key)
    dep:Printer_Name = 'VODACOM DELIVERY NOTE'
    If Access:DEFPRINT.TryFetch(dep:Printer_Name_Key) = Level:Benign
        !Found
        prn:Local_Printer_Name = dep:Printer_Path
    Else ! If Access:DEFPRINT.TryFetch(dep:Printer_Name_Key) = Level:Benign
        !Error
    End ! If Access:DEFPRINT.TryFetch(dep:Printer_Name_Key) = Level:Benign

    prn:szPrinterName = Clip(prn:Local_Printer_Name)

    If ~OpenPrinter(prn:szPrinterName,prn:hPrinter,0)
        Case Missive('Unable to open printer ' & prn:szPrinterName & '.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Return 1
    End ! If ~OpenPrinter(prn:szPrinterName,prn:hPrinter,0)

    Clear(DocInfoGrp)
    prn:szDocName = Clip('Vodacom Delivery Note')
    prn:szDataType = 'RAW'
    DocInfoGrp.pDocName = Address(prn:szDocName)
    DocInfoGrp.pOutputFile = 0
    DocInfoGrp.pDataType = Address(prn:szDataType)

    If ~StartDocPrinter(prn:hPrinter,1,Address(DocInfoGrp))
        Case Missive('Unable to "StartDocPrinter()".','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Return 1
    End ! If ~StartDocPrinter(prn:hPrinter,1,Address(DocInfoGrp))

    Return 0
PrintJob            Procedure()
NumBytes        Long()
Code
    prn:szBuffer = Clip(szHeader) & Clip(prn:szBuffer) & Clip(szFooter)

    If glo:WebJob
        LocalGlobalInFile = Clip(Path()) & '\' & Clip(wob:HeadAccountNumber) & 'I.TXA'
        Remove(InFile)
        Create(InFile)
        Open(InFile)
        inf:textLine = prn:szBuffer
        Add(InFile)
        Close(InFile)

        ! Send File To Client (DBH: 27/07/2007)
        SendFileToClient(LocalGlobalInFile)

        ! Set Remote Printer (DBH: 27/07/2007)
        ClarioNET:CallClientProcedure('SetClientPrinter','VODACOM DELIVERY NOTE')

        ! Tell Client To Print (DBH: 27/07/2007)
        ClarioNET:CallClientProcedure('PRINTINVOICE')
        Remove(InFile)
    Else ! If glo:WebJob

        IF NOT StartPagePrinter( prn:hPrinter ) THEN
            Case Missive('Failure reported by "StartPagePrinter()", attempting to continue.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive

            Return 0
        END !IF


        IF NOT WritePrinter( prn:hPrinter, ADDRESS(prn:szBuffer), LEN(prn:szBuffer), NumBytes ) THEN
            Case Missive('Failure reported by "WritePrinter()", attempting to continue.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive

            Return 0
        END !IF

        EndPagePrinter( prn:hPrinter )

    End ! If glo:WebJob

    prn:szBuffer = ''
    Return 1

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Vodacom_Delivery_Note PROCEDURE  (fJobNumber)         ! Declare Procedure
save_jobs_id         USHORT,AUTO
save_jobse_id        USHORT,AUTO
save_webjob_id       USHORT,AUTO
save_invoice_id      USHORT,AUTO
save_tradeacc_id     USHORT,AUTO
LocalGlobalInFile    STRING(255),STATIC
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
InFile    File,Driver('ASCII'),Pre(inf),Name(LocalGlobalInFile),Create
InFileRec    Record
TextLine         String(30000)
             End
          End ! InFile    File,Driver('ASCII'),Pre(inf),Name(LocalGlobalInFile),Create

    Map
AddAddressLine        Procedure(Long,String)
OpenPrinter           Procedure(),Byte
PrintJob              Procedure(),Byte
    End

tmp:SubTotal        Real()
tmp:VAT             Real()
tmp:Total           Real()

DocInfoGrp    Group(doc_info_1)
              End

Printer_Group    Group,Pre(prn)
PageLength        Long(48)
DetailRows        Long(14)
Local_Printer_Name    String(80)
hPrinter          Unsigned
szPrinterName     CString(260)
dwBytesCopied     Long
dwWritten         Long
szBuffer          CString(30000)
szDocName         CString(80)
szDataType        CString(80)
                End ! Printer_Group    Group,Pre(prn)
BlankLine       String(80)

AddressLine    Group,Pre(Address)
Delivery            String(30)
Filler1             String(4)
Business            String(30)
               End ! AddressLine    Group,Pre(Address)

NarrativeLine  Group,Pre(narrative)
Text1                String(55)
Filler1              String(10)
Amount               String(10)
               End ! NarrativeLine  Group,Pre(narrative)


OrderLine        Group,Pre(Order)
DespatchDate        String(10)
Filler1            String(4)
Filler2            String(7)
InvoiceNumber        String(10)
Filler4            String(1)
AccountNumber        String(12)
Filler3            String(1)
YourOrderNumber    String(20)
                    End ! OrderLine        Group,Pre(Order)

IMEILine        Group,Pre(imeitmp)
Make                String(14)
Model                String(15)
SerialNumber            String(23)
IMEINumber            String(20)
                End ! IMEILine        Group,Pre(imeitmp)

DateLine        Group,Pre(date)
Filler0            String(10)
DateOut            String(10)
Filler1            String(22)
TimeOut            String(10)
Filler2            String(5)
Engineer            String(3)
                End ! DateLine        Group,Pre(date)

Detail_Queue        Queue,Pre(detail)
Line                String(56)
Filler1                String(10)
Amount                String(10)
                    End ! Detail_Queue        Queue,Pre(detail)

Address_Group        Group,Pre(Address)
User_Name            String(30)
Address_Line1        String(30)
Address_Line2        String(30)
Address_Line3        String(30)
Postcode            String(15)
Telephone_Number    String(15)
                    End ! Address_Grou        Group,Pre(Address)

AddressArray        String(30),DIM(2,8)

szHeader            CString(5000)
szFooter            Cstring(5000)

Left_Address_Group        Group,Pre(left)
CompanyName            String(30)
Line1                String(30)
Line2                String(30)
Line3                String(30)
Post_Code            String(15)
TelNo                String(15)
                    End

tmp:VatNumber            String(30)
tmp:AuditNotes    String(255)
tmp:InvoiceTime     Time()
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:AUDIT.Open
   Relate:JOBS.Open
   Relate:WEBJOB.Open
   Relate:JOBSE.Open
   Relate:INVOICE.Open
   Relate:TRADEACC.Open
   Relate:JOBSINV.Open
   Relate:DEFPRINT.Open
   Relate:JOBSE2.Open
    !Print Invoice
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = fJobNumber
    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        !Found
    Else ! If Access:JOBS.TryFetch(jobs:InvoiceNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBS.TryFetch(jobs:InvoiceNumberKey) = Level:Benign

    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign


    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

    !added by Paul 06/10/2010 - Log no 11691
    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber = job:Ref_Number
    If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign then
        !found
    Else
        !Error
    End

    Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
    inv:Invoice_Number = job:Invoice_Number
    If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        !Found
    Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        !Error
    End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign

    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Found
        address:User_Name     = tra:Company_Name
        address:Address_Line1 = tra:Address_Line1
        address:Address_Line2 = tra:Address_Line2
        address:Address_Line3 = tra:Address_Line3
        address:Postcode      = tra:Postcode
        address:Telephone_Number = tra:Telephone_Number
    Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

    Do CheckCreateInvoice

    If OpenPrinter()
        Return 
    End ! If OpenPrinter()

    prn:szBuffer = CHR(27) & CHR(67) & CHR(prn:PageLength)
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine

    Do AddAddress
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine

    Do AddOrderLine
    Do AddLine
    Do AddLine
    Do AddLine
    Do AddLine

    Do AddIMEILine
    Do AddLine

    szHeader = CLIP(prn:szBuffer)

    prn:szBuffer = ''

    Do AddLine

    ! Date In (DBH: 30/07/2007)
    Clear(BlankLine)
    date:DateOut = Format(job:Date_Booked,@d06)
    date:TimeOut = Format(job:Time_Booked,@t1)
    date:Engineer = ''
    prn:szBuffer = Clip(prn:szBuffer) & Clip(DateLine) & '<13,10>'

    Do AddLine

    ! Date Out (DBH: 30/07/2007)
    Clear(BlankLine)
    date:DateOut = Format(inv:ARCInvoiceDate,@d06)
    date:TimeOut = Format(tmp:InvoiceTime,@t01)

    date:Engineer = job:Engineer
    prn:szBuffer = Clip(prn:szBuffer) & Clip(DateLine) & '<13,10>'

    szFooter = Clip(prn:szBuffer) & '<12>'
    prn:szBuffer = ''

    Do CreatePartsList

    Loop x# = 1 To Records(Detail_Queue)
        Get(Detail_Queue,x#)
        Case x#
        Of 1
            detail:Amount = Format(job:Invoice_Labour_Cost,@n_10.2)
        Of 3
            detail:Amount = Format(job:Invoice_Parts_Cost,@n_10.2)
        Of 5
            detail:Amount = Format(job:invoice_Courier_Cost,@n_10.2)
        Of 10
            detail:Amount = Format(tmp:SubTotal,@n_10.2)
        Of 12
            detail:Amount = Format(tmp:VAT,@n_10.2)
        Of 14
            detail:Amount = Format(tmp:Total,@n-_10.2)
        End ! Case x#

        Put(Detail_Queue)
    End ! Loop x# = 1 To Records(Detail_Queue)

    Get(Detail_Queue,1)

    Loop x# = 1 To Records(Detail_Queue)
        Get(Detail_Queue,x#)
        Clear(BlankLine)
        narrative:Text1 = detail:Line
        narrative:Amount = detail:Amount
        prn:szBuffer = Clip(prn:szBuffer) & Clip(NarrativeLine)
        prn:szBuffer = Clip(prn:szBuffer) & '<13,10>'
    End ! Loop x# = 1 To Records(Detail_Queue)

    If PrintJob()
        Case Missive('Invoice Printed.','ServiceBase 3g',|
                       'midea.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    End ! If PrintJob()

    IF prn:hPrinter <> 0
        ClosePrinter( prn:hPrinter )
    END !IF
          
   Relate:AUDIT.Close
   Relate:JOBS.Close
   Relate:WEBJOB.Close
   Relate:JOBSE.Close
   Relate:INVOICE.Close
   Relate:TRADEACC.Close
   Relate:JOBSINV.Close
   Relate:DEFPRINT.Close
   Relate:JOBSE2.Close
! Routines
AddIMEILine        Routine
    Clear(BlankLine)
    imeitmp:Make        = job:Manufacturer
    imeitmp:Model       = job:Model_Number
    imeitmp:SerialNumber = job:MSN
    imeitmp:IMEINumber = job:ESN

    prn:szBuffer = Clip(prn:szBuffer) & Clip(IMEILine) & '<13,10>'
AddAddress        Routine
    !------------------------------------------------------------------
    CLEAR(AddressArray)
    access:Webjob.clearkey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    if access:Webjob.fetch(wob:refNumberKey)=level:benign
      !Ok
    END


! Inserting (DBH 07/12/2007) # 8218 - If this an RRC-ARC job then show the RRC details as the "delivery"address
    If jobe:WebJob
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = wob:HeadAccountNumber
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

        AddAddressLine( 2, tra:Company_Name)
        AddAddressLine( 2, tra:Address_Line1)
        AddAddressLine( 2, tra:Address_Line2)
        AddAddressLine( 2, tra:Address_Line3)
        AddAddressLine( 2, tra:Postcode)
        AddAddressLine( 2, tra:Telephone_Number)
        AddAddressLine( 2, 'VAT No.: ' & tra:Vat_Number     )

    Else ! If jobe:WebJob
! End (DBH 07/12/2007) #8218
           Access:subtracc.clearkey(sub:Account_Number_Key)
           sub:Account_Number = job:Account_Number
           if Access:subtracc.fetch(sub:Account_Number_Key) then
              !error
                ! #11606 Not getting trade account correctly. HOW LONG has this been wrong?! (DBH: 04/08/2010)
                access:tradeacc.clearkey(tra:Account_Number_Key)
                tra:Account_Number = sub:Main_Account_Number
                if Access:tradeacc.fetch(tra:Account_Number_Key)
                end
           end
            !This report always uses the customer, or sub address - TrkBs: 5364 (DBH: 21-03-2005)

            !Set default vat number, from sub, or head account - TrkBs: 5364 (DBH: 21-03-2005)
            If sub:OverrideHeadVATNo = True
                tmp:VatNumber = sub:Vat_Number
            Else ! If sub:OverrideHeadVATNo = True
                tmp:VatNumber = tra:Vat_Number
            End ! If sub:OverrideHeadVATNo = True

           IF sub:Invoice_Customer_Address = 'YES'
             ! Start - If there is no account vat number, try the job one - TrkBs: 5364 (DBH: 22-04-2005)
             If tmp:VatNumber = ''
                tmp:VatNumber = jobe:VatNumber
             End ! If tmp:VatNumber = ''
             ! End   - If there is no account vat number, try the job one - TrkBs: 5364 (DBH: 22-04-2005)
           END

!        else
!            !In the impossible event of no subaccount, use the trade vat number (DBH: 21-03-2005)
!            tmp:VatNumber = tra:Vat_Number
!        end


        AddAddressLine( 2, job:Company_Name_Delivery)
        AddAddressLine( 2, job:Address_Line1_Delivery )
        AddAddressLine( 2, job:Address_Line2_Delivery )
        AddAddressLine( 2, job:Address_Line3_Delivery )
        AddAddressLine( 2, job:Postcode_Delivery )
        AddAddressLine( 2, job:Telephone_Delivery )
        !Show the VAT number instead of the telephone number twice (DBH: 21-03-2005)
        AddAddressLine( 2, 'VAT No.: ' & tmp:VatNumber     )

    End ! If jobe:WebJob

    AddAddressLine( 1, address:User_Name        )
    AddAddressLine( 1, address:Address_Line1    )
    AddAddressLine( 1, address:Address_Line2    )
    AddAddressLine( 1, address:Address_Line3    )
    AddAddressLine( 1, address:Postcode         )
    AddAddressLine( 1, address:Telephone_Number )
    !------------------------------------------------------------------
    ! force job number to last line
    !



    AddressArray[ 1,  8 ] = 'Job Number ' & job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
    !------------------------------------------------------------------
    LOOP x# = 1 TO 8  ! Lines 1..7 for addrsss, 8 = new line added for job number
        CLEAR(BlankLine)

        address:Delivery = AddressArray[ 2, x# ]
        address:Business = AddressArray[ 1,  x# ]

        prn:szBuffer = CLIP(prn:szBuffer) & CLIP(AddressLine)
        prn:szBuffer = CLIP(prn:szBuffer) & CHR(13) & CHR(10)
    END !LOOP
    !------------------------------------------------------------------
AddLine        Routine
    prn:szBuffer = Clip(prn:szBuffer) & '<13,10>'
AddOrderLine        Routine
    Clear(BlankLine)
    order:DespatchDate = Format(inv:ARCInvoiceDate,@d06)
    order:InvoiceNumber = job:Invoice_Number
    order:AccountNumber = job:Account_Number
    order:YourOrderNumber = job:Order_Number
    prn:szBuffer = Clip(prn:szBuffer) & Clip(OrderLine) & '<13,10>'
CheckCreateInvoice        Routine
Data
local:AuditNotes        String(255)
Code
    If inv:ARCInvoiceDate = 0
        ! Create an RRC Invoice (DBH: 27/07/2007)
        inv:ARCInvoiceDate = Today()

        tmp:InvoiceTime = Clock()

        job:Invoice_Labour_Cost = job:Labour_Cost
        job:Invoice_Courier_Cost = job:Courier_Cost
        job:Invoice_Parts_Cost = job:Parts_Cost
        job:Invoice_Sub_Total = job:Invoice_Labour_Cost + job:Invoice_Parts_Cost + job:Invoice_Courier_Cost
        Access:JOBS.Update()

        inv:ExportedARCOracle = 1

        Access:INVOICE.Update()

        Line500_XML('AOW')
        !Invoice logging requested by harvey - 07/05/15
        MakeInvoiceLog('AOW')


        ! Add Relevant Audit Entry (DBH: 27/07/2007)
        tmp:AuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
        If LoanAttachedToJob(job:Ref_Number)
            tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(job:Invoice_Courier_Cost,@n14.2)
        End ! If LoanAttachedToJob(job:Ref_Number)

        ! Show replacement charge (DBH: 27/07/2007)
        If jobe:Engineer48HourOption = 1
            If jobe:ExcReplcamentCharge
                tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>EXC REPL: ' & Format(job:Invoice_Courier_Cost,@n14.2)
            End ! If jobe:ExcReplcamentCharge
        End ! If jobe:Engineer48HourOption = 1

        tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>PARTS: ' & Format(job:Invoice_Parts_Cost,@n14.2) & |
                                                 '<13,10>LABOUR: ' & Format(job:Invoice_Labour_Cost,@n14.2) & |
                                                 '<13,10>SUB TOTAL: ' & Format(job:Invoice_Sub_Total,@n14.2) & |
                                                 '<13,10>VAT: ' & Format((job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                                                        (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                                                        (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100),@n14.2) & |
                                                 '<13,10>TOTAL: ' & Format(job:Invoice_Sub_Total + (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                                                        (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                                                        (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100),@n14.2)

        If AddToAudit(job:Ref_Number,'JOB','INVOICE CREATED',Clip(tmp:AuditNotes))

        End ! If AddToAudit(job:Ref_Number,'JOB','INVOICE CREATED',Clip(tmp:AuditNotes))
    Else ! If inv:RRCInvoiceDate = 0
        ! Reprint (DBH: 27/07/2007)
        ! Normal Invoice Reprint (DBH: 27/07/2007)
        If AddToAudit(job:Ref_Number,'JOB','INVOICE REPRINTED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                                                                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06) )
        End ! '<13,10>INVOICE DATE: ' & FOrmat(inv:Date_Created,@d06)

        tmp:InvoiceTime = job:Time_Booked
        Access:AUDIT.Clearkey(aud:TypeActionKey)
        aud:Ref_Number = job:Ref_Number
        aud:Type = 'JOB'
        aud:Action = 'INVOICE CREATED'
        aud:Date = inv:ARCInvoiceDate
        Set(aud:TypeActionKey,aud:TypeActionKey)
        Loop ! Begin Loop
            If Access:AUDIT.Next()
                Break
            End ! If Access:AUDIT.Next()
            If aud:Ref_Number <> job:Ref_Number
                Break
            End ! If aud:Ref_Number <> job:Ref_Number
            If aud:Type <> 'JOB'
                Break
            End ! If aud:Type <> 'JOB'
            If aud:Action <> 'INVOICE CREATED'
                Break
            End ! If aud:Action <> 'INVOICE CREATED'
            If aud:Date <> inv:ARCInvoiceDate
                Break
            End ! If aud:Date <> inv:RRCInvoiceDate
            tmp:InvoiceTime = aud:Time
            Break
        End ! Loop
    End ! If inv:RRCInvoiceDate = 0

    tmp:VAT = (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
            (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
            (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100)
    tmp:SubTotal = job:Invoice_Sub_Total
    tmp:Total = tmp:SubTotal + tmp:VAT

CreatePartsList        Routine
Data
local:FaultDescription  String(255)
Code
    Free(Detail_Queue)
    Clear(Detail_Queue)

    Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
    jbn:RefNumber = job:Ref_Number
    If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign

    local:FaultDescription = Clip(BHStripReplace(jbn:Fault_Description,'<13,10>','. '))

    If Clip(local:FaultDescription) <> ''
        detail:Line = Clip(Sub(local:FaultDescription,1,55))
        detail:Amount = ''
        Add(Detail_Queue)
        detail:Line = Clip(Sub(local:FaultDescription,56,55))
        detail:Amount = ''
        Add(Detail_Queue)
        !added by Paul 06/10/2010 - log no 11691
        If jobe2:InvDiscountAmnt > 0 then
            detail:Line = 'OOW Discount ' & Format(jobe2:InvDiscountAmnt,@n_10.2)
            detail:Amount = ''
            Add(Detail_Queue)
        Else
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
        End
    Else ! If Clip(jbn:Fault_Description) <> ''
        If jobe2:InvDiscountAmnt > 0 then
            detail:Line = 'OOW Discount ' & Format(jobe2:InvDiscountAmnt,@n_10.2)
            detail:Amount = ''
            Add(Detail_Queue)
        Else
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
        End
        detail:Line = ''
        detail:Amount = ''
        Add(Detail_Queue)
        detail:Line = ''
        detail:Amount = ''
        Add(Detail_Queue)
    End ! If Clip(jbn:Fault_Description) <> ''

    detail:Line = ALL('-',55)
    detail:Amount = ''
    Add(Detail_Queue)

    ! Show Parts (DBH: 27/07/2007)
    RowNum# = 0
    Access:PARTS.Clearkey(par:Order_Number_Key)
    par:Ref_Number = job:Ref_Number
    Set(par:Order_Number_Key,par:Order_Number_Key)
    Loop ! Begin Loop
        If Access:PARTS.Next()
            Break
        End ! If Access:PARTS.Next()
        If par:Ref_Number <> job:Ref_Number
            Break
        End ! If par:Ref_Number <> job:Ref_Number
        RowNum# += 1
        detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(par:Description,1,50)
        detail:Amount = ''
        Add(Detail_Queue)
    End ! Loop

    ! Show Fault Codes (DBH: 27/07/2007)
    Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
    joo:JobNumber = job:Ref_Number
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop ! Begin Loop
        If Access:JOBOUTFL.Next()
            Break
        End ! If Access:JOBOUTFL.Next()
        If joo:JobNumber <> job:Ref_Number
            Break
        End ! If joo:JobNumber <> job:Ref_Number
        If Instring('IMEI VALIDATION',joo:Description,1,1)
            Cycle
        End ! If Instring('IMEI VALIDATION',joo:Description,1,1)
        RowNum# += 1
        detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(joo:Description,1,50)
        detail:Amount = ''
        Add(Detail_Queue)
    End ! Loop

    ! I guess this is filling in the blank lines (DBH: 27/07/2007)
    detail:Line = ''
    detail:Amount = ''
    Loop
        If Records(Detail_Queue) % prn:DetailRows = 0
            Break
        End ! If Records(Detail_Queue) % prn:DetailRows = 0
        Add(Detail_Queue)
    End ! Loop
! Procedures
AddAddressLine        Procedure(f:Vector,f:Value)
Code
    Loop x# = 1 To 7
        If AddressArray[f:Vector,x#] = ''
            AddressArray[f:Vector,x#] = f:Value
            Break
        End ! If AddressArray[f:Version,x#] = ''
    End ! Loop x# = 1 To &
OpenPrinter            Procedure
Code
    Access:DEFPRINT.ClearKey(dep:Printer_Name_Key)
    dep:Printer_Name = 'VODACOM DELIVERY NOTE'
    If Access:DEFPRINT.TryFetch(dep:Printer_Name_Key) = Level:Benign
        !Found
        prn:Local_Printer_Name = dep:Printer_Path
    Else ! If Access:DEFPRINT.TryFetch(dep:Printer_Name_Key) = Level:Benign
        !Error
    End ! If Access:DEFPRINT.TryFetch(dep:Printer_Name_Key) = Level:Benign

    prn:szPrinterName = Clip(prn:Local_Printer_Name)

    If ~OpenPrinter(prn:szPrinterName,prn:hPrinter,0)
        Case Missive('Unable to open printer ' & prn:szPrinterName & '.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Return 1
    End ! If ~OpenPrinter(prn:szPrinterName,prn:hPrinter,0)

    Clear(DocInfoGrp)
    prn:szDocName = Clip('Vodacom Delivery Note')
    prn:szDataType = 'RAW'
    DocInfoGrp.pDocName = Address(prn:szDocName)
    DocInfoGrp.pOutputFile = 0
    DocInfoGrp.pDataType = Address(prn:szDataType)

    If ~StartDocPrinter(prn:hPrinter,1,Address(DocInfoGrp))
        Case Missive('Unable to "StartDocPrinter()".','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Return 1
    End ! If ~StartDocPrinter(prn:hPrinter,1,Address(DocInfoGrp))

    Return 0
PrintJob            Procedure()
NumBytes        Long()
Code
    prn:szBuffer = Clip(szHeader) & Clip(prn:szBuffer) & Clip(szFooter)

    IF NOT StartPagePrinter( prn:hPrinter ) THEN
        Case Missive('Failure reported by "StartPagePrinter()", attempting to continue.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive

        Return 0
    END !IF


    IF NOT WritePrinter( prn:hPrinter, ADDRESS(prn:szBuffer), LEN(prn:szBuffer), NumBytes ) THEN
        Case Missive('Failure reported by "WritePrinter()", attempting to continue.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive

        Return 0
    END !IF

    EndPagePrinter( prn:hPrinter )

    prn:szBuffer = ''

    Return 1


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
