

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBD03011.INC'),ONCE        !Local module procedure declarations
                     END








LoanOrders PROCEDURE(ModelQueue,<LONG fOrderNumber>)
! Before Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
!ModelQueue  Queue(DefModelQueue)
!            End
! After Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
RejectRecord         LONG,AUTO
LineCost             DECIMAL(10,2)
TotalLineCost        DECIMAL(10,2)
Address:User_Name    STRING(30)
address:Address_Line1 STRING(30)
address:Address_Line2 STRING(30)
address:Address_Line3 STRING(30)
address:Post_Code    STRING(20)
address:Telephone_Number STRING(20)
address:Fax_No       STRING(20)
address:Email        STRING(50)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Total_No_Of_Lines    LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:Manufacturer     STRING(30)
tmp:ModelNumber      STRING(30)
tmp:Quantity         LONG
locPurchasePrice     REAL
!-----------------------------------------------------------------------------
Process:View         VIEW(ORDWEBPR)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(396,2802,7521,7990),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?strOrderNumber:2)
                         STRING(@s30),AT(52,646),USE(address:Address_Line3),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(52,833),USE(address:Post_Code),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(4938,948),USE(?String16),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@D6),AT(6240,948),USE(ReportRunDate),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Email:'),AT(52,1302),USE(?String33),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s50),AT(365,1302),USE(address:Email),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s20),AT(365,990),USE(address:Telephone_Number),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@n_8),AT(6198,625),USE(lno:RecordNumber),TRN,HIDE,RIGHT,FONT('Arial',9,,FONT:bold,CHARSET:ANSI)
                         STRING('Order Number:'),AT(4948,625),USE(?strOrderNumber),TRN,HIDE,FONT('Arial',9,,,CHARSET:ANSI)
                         STRING('Fax:'),AT(52,1146),USE(?String34),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s20),AT(365,1146),USE(address:Fax_No),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(52,990),USE(?String32),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4938,1104),USE(?String16:2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@N3),AT(6240,1104),PAGENO,USE(?PageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6542,1104),USE(?String16:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6708,1104,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(52,271),USE(address:Address_Line1),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,458),USE(address:Address_Line2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,0),USE(Address:User_Name),TRN,FONT('Arial',14,,FONT:bold,CHARSET:ANSI)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s8),AT(4167,0),USE(tmp:Quantity),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(5573,0),USE(locPurchasePrice),TRN,HIDE,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(104,0),USE(tmp:Manufacturer),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(2135,0,,208),USE(tmp:ModelNumber),TRN,FONT('Arial',8,,)
                         END
                         FOOTER,AT(0,0),USE(?unnamed:4)
                           LINE,AT(104,52,2146,0),USE(?Line3),COLOR(COLOR:Black)
                           STRING('Total Quantity Ordered:'),AT(115,115),USE(?String29),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-14),AT(1635,115),USE(Total_No_Of_Lines),TRN,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10833,7521,177),USE(?unnamed:3)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('LOANS ORDERED'),AT(5510,52),USE(?String3),TRN,FONT('Arial',16,,FONT:bold,CHARSET:ANSI)
                         STRING('Quantity'),AT(4198,1979),USE(?String5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Price'),AT(6146,1979),USE(?strPrice),TRN,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Manufacturer'),AT(156,1979),USE(?String6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Model Number'),AT(2167,1979),USE(?String31),TRN,FONT('Arial',8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('LoanOrders')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','LoanOrders')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'LoanOrder'
  If PrintOption(PreviewReq,glo:ExportReport,'Loan Order') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:ORDWEBPR.Open
  Relate:DEFAULTS.Open
  Relate:GENSHORT.Open
  Relate:LOANORNO.Open
  Relate:MODELNUM.Open
  Access:STOAUDIT.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  
  
  RecordsToProcess = BYTES(ORDWEBPR)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(ORDWEBPR,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      !Set Records For Progress Window
      RecordsToProcess = RECORDS(ModelQueue)
      
      !Set Stock File (Stock Type Key)
      
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        IF (glo:WebJob)
            ! #12347 Report seemed to be only designed for webmaster printing. Changed to include fat client (DBH: 19/01/2012)
            tra:Account_Number  = Clarionet:Global.Param2
        ELSE
            tra:Account_Number   = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
        END
        IF Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
           !Found
           address:User_Name = tra:company_name
           address:Address_Line1 = tra:address_line1
           address:Address_Line2 = tra:address_line2
           address:Address_Line3 = tra:address_line3
           address:Post_code = tra:postcode
           address:Telephone_Number = tra:telephone_number
           address:Fax_No = tra:Fax_Number
           address:Email = tra:EmailAddress
        ELSE ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        END !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        SETTARGET()
        !Loop not here any more
        Loop x# = 1 To Records(ModelQueue)
            Get(ModelQueue,x#)
            tmp:Manufacturer    = ModelQueue.Manufacturer
            tmp:ModelNumber     = ModelQueue.ModelNumber
            tmp:Quantity        = ModelQueue.Quantity
            Total_No_Of_Lines += ModelQueue.Quantity
        
            Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
            mod:Manufacturer = ModelQueue.Manufacturer
            mod:Model_Number = ModelQueue.ModelNumber
            IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)
                locPurchasePrice = mod:LoanReplacementValue * ModelQueue.Quantity
            ELSE
                locPurchasePrice = 0
            END
            Print(rpt:Detail)
        End !x# = 1 To Records(ModelQueue)
        LocalResponse = RequestCompleted
        BREAK
        
        
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(ORDWEBPR,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','LoanOrders')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:GENSHORT.Close
    Relate:LOANORNO.Close
    Relate:MODELNUM.Close
    Relate:ORDWEBPR.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !After Opening The Report
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Total_No_Of_Lines = 0
  
  IF (fOrderNumber > 0)
      ! #12347 I don't think this report is used for anything else now
      ! But only show order number when passed to it. (DBH: 19/01/2012)
      Access:LOANORNO.Clearkey(lno:RecordNumberKey)
      lno:RecordNumber = fOrderNumber
      IF (Access:LOANORNO.Tryfetch(lno:RecordNumberKey) = Level:Benign)
          SETTARGET(Report)
          ?strOrderNumber{prop:Hide} = 0
          ?lno:RecordNumber{prop:Hide} = 0
          ?strPrice{prop:Hide} = 0
          ?locPurchasePrice{prop:Hide} = 0
          SETTARGET()
      END ! IF (Access:LOANORNO.Tryfetch(lno:RecordNumberKey) = Level:Benign)
  END !IF (fOrderNumber > 0)
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='LoanOrders'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END













StockValueSummary PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_tradeacc_id     USHORT,AUTO
save_sto_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
stock_queue          QUEUE,PRE(stoque)
site_location        STRING(30)
manufacturer         STRING(30)
total_stock          REAL
purchase_value       REAL
AveragePurchaseCost  REAL
sale_value           REAL
                     END
site_location_temp   STRING(30)
count_temp           STRING(9)
average_price_temp   REAL
count_average_temp   REAL
job_count_temp       REAL
total_jobs_temp      REAL
Manufacturer_Temp    STRING(30)
total_stock_temp     STRING(12)
sale_value_temp      REAL
purchase_value_temp  REAL
purchase_value_total_temp REAL
sale_value_total_temp REAL
total_stock_total_temp STRING(12)
location_temp        STRING(30)
address:UserName     STRING(30)
address:AddressLine1 STRING(30)
address:AddressLine2 STRING(30)
address:AddressLine3 STRING(30)
address:Postcode     STRING(30)
address:TelephoneNumber STRING(30)
address:FaxNumber    STRING(30)
address:EmailAddress STRING(255)
tmp:TotalAveragePurchaseCost REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(STOCK)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

Report               REPORT('Stock Value Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,781,7521,1438),USE(?unnamed)
                         STRING('Inc. Accessories:'),AT(4948,156),USE(?String22),TRN,FONT(,8,,)
                         STRING(@s40),AT(5885,156),USE(GLO:Select2),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(5885,365),USE(GLO:Select3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Suppress Zeros:'),AT(4948,365),USE(?String22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(4948,573),USE(?String27),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(4948,990),USE(?String59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,990),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6146,990),USE(?String34),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6302,990,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(5885,573),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(4948,781),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5885,781),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,229),USE(?DetailBand)
                           STRING(@s30),AT(156,0),USE(site_location_temp),TRN,FONT(,8,,)
                           STRING(@s30),AT(2083,0),USE(Manufacturer_Temp),TRN,FONT(,8,,)
                           STRING(@s8),AT(4010,0),USE(stoque:total_stock),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(4688,0),USE(stoque:purchase_value),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6604,0),USE(stoque:sale_value),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(5677,0),USE(stoque:AveragePurchaseCost),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(,,,458),USE(?totals)
                           LINE,AT(208,52,7083,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(208,104),USE(?String26),TRN,FONT(,8,,FONT:bold)
                           STRING(@s9),AT(3948,104),USE(total_stock_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n-14.2),AT(4708,104),USE(purchase_value_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n-14.2),AT(6625,104),USE(sale_value_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n14.2),AT(5677,104),USE(tmp:TotalAveragePurchaseCost),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@s9),AT(2083,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(52,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(address:UserName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('STOCK VALUE REPORT'),AT(4271,0,3177,260),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(address:AddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(address:AddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(address:AddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1042),USE(address:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(address:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('Manufacturer'),AT(2083,2083),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('Average Purchase'),AT(5792,2021),USE(?String45:5),TRN,FONT(,7,,FONT:bold)
                         STRING('Site Location'),AT(156,2083),USE(?String44:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Stock Items'),AT(3958,2125),USE(?String45),TRN,FONT(,7,,FONT:bold)
                         STRING('In Warranty'),AT(4969,2021),USE(?String45:3),TRN,FONT(,7,,FONT:bold)
                         STRING('Value'),AT(5083,2125),USE(?String24),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Out Warranty'),AT(6813,2021),USE(?String45:4),TRN,FONT(,7,,FONT:bold)
                         STRING('Value'),AT(7010,2125),USE(?String24:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Cost'),AT(6063,2135),USE(?String24:3),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Total'),AT(4104,2021),USE(?String45:2),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('StockValueSummary')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','StockValueSummary')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:STOCK.Open
  Relate:DEFAULTS.Open
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(STOCK)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(STOCK,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        RecordsToProcess    = Records(STOCK)
        
        !Build the manufacturer list - 3277  (DBH: 17-06-2004)
        setcursor(cursor:wait)
        Access:STOCK.ClearKey(sto:Location_Key)
        sto:Location    = glo:Select1
        Set(sto:Location_Key,sto:Location_Key)
        Loop
            If Access:STOCK.NEXT()
               Break
            End !If
            If sto:Location    <> glo:Select1      |
                Then Break.  ! End If
            !Include accessories? - 3277  (DBH: 17-06-2004)
            If glo:Select2 <> 'YES'
                If sto:Accessory = 'YES'
                    Cycle
                End !If sto:Accessory = 'YES'
            End !If glo:Select3 <> 'YES'
        
            !Include zero value parts? - 3277  (DBH: 17-06-2004)
            If glo:Select3 = 'YES' and sto:Quantity_Stock = 0
                Cycle
            End !If glo:Select4 = 'YES' and sto:Quantity_Stock = 0
        
            RecordsProcessed += 1
            Do DisplayProgress
            Sort(stock_queue,stoque:site_location,stoque:manufacturer)
            stoque:site_location    = sto:location
            stoque:manufacturer     = sto:manufacturer
            Get(stock_queue,stoque:site_location,stoque:manufacturer)
            If Error()
                stoque:site_location    = sto:location
                stoque:manufacturer     = sto:manufacturer
                stoque:total_stock      = sto:quantity_stock
                stoque:purchase_value   = Round(sto:purchase_cost * sto:quantity_stock,.01)
                !Add average purchase cost - TrkBs: 3277 (DBH: 16-02-2005)
                stoque:AveragePurchaseCost   = Round(sto:AveragePurchaseCost * sto:quantity_stock,.01)
                stoque:sale_value       = Round(sto:sale_cost * sto:quantity_stock,.01)
                Add(stock_queue)
            Else!If Error()
                stoque:total_stock      += sto:quantity_stock
                stoque:purchase_value   += Round(sto:purchase_cost * sto:quantity_stock,.01)
                stoque:AveragePurchaseCost   += Round(sto:AveragePurchaseCost * sto:quantity_stock,.01)
                stoque:sale_value       += Round(sto:sale_cost * sto:quantity_stock,.01)
                Put(stock_queue)
            End!If Error()
        End !Loop
        setcursor()
        
        !While I'm here, add an accurate progress bar - TrkBs: 3277 (DBH: 16-02-2005)
        Prog.ProgressSetup(Records(Stock_Queue))
        
        If Records(stock_queue)
            count_temp = 0
        
            Sort(stock_queue,stoque:site_location,stoque:manufacturer)
            Loop x# = 1 To Records(stock_queue)
                If Prog.InsideLoop()
        
                End ! If Prog.InsideLoop()
                Get(stock_queue,x#)
                If count_temp = 0
                    location" = stoque:site_location
                    site_location_temp = stoque:site_location
                Else!If count_temp = 0
                    If location" <> stoque:site_location
                        site_location_temp = stoque:site_location
                        location" = stoque:site_location
                    Else!If location" <> stoque:site_locatoin
                        site_location_temp = ''
                    End!If location" <> stoque:site_locatoin
                End!If count_temp = 0
        !        site_location_temp = stoque:site_location
                manufacturer_temp   = stoque:manufacturer
                count_temp += 1
                tmp:RecordsCount += 1
                Print(rpt:detail)
                total_stock_total_temp      += stoque:total_stock
                purchase_value_total_temp   += stoque:purchase_value
                tmp:TotalAveragePurchaseCost += stoque:AveragePurchaseCost
                sale_value_total_temp       += stoque:sale_value
            End!Loop x# = 1 To Records(stock_queue)
            Print(rpt:totals)
        End!If Records(stock_queue)
        Prog.ProgressFinish()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(STOCK,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','StockValueSummary')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:STOCK.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !Display Correct Address - 3277  (DBH: 17-06-2004)
  If glo:WebJob
      save_TRADEACC_id = Access:TRADEACC.SaveFile()
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          address:UserName        = tra:Company_Name
          address:AddressLine1    = tra:Address_Line1
          address:AddressLine2    = tra:Address_Line2
          address:AddressLine3    = tra:Address_Line3
          address:Postcode        = tra:Postcode
          address:TelephoneNumber = tra:Telephone_Number
          address:FaxNumber       = tra:Fax_Number
          address:EmailAddress    = tra:EmailAddress
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Access:TRADEACC.RestoreFile(save_TRADEACC_id)
  Else !glo:WebJob
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      address:UserName        = def:User_Name
      address:AddressLine1    = def:Address_Line1
      address:AddressLine2    = def:Address_Line2
      address:AddressLine3    = def:Address_Line3
      address:Postcode        = def:Postcode
      address:TelephoneNumber = def:Telephone_Number
      address:FaxNumber       = def:Fax_Number
      address:EmailAddress    = def:EmailAddress
  End !glo:WebJob
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='StockValueSummary'
  END
  Report{Prop:Preview} = PrintPreviewImage


Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: UnivAbcReport
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0











HandOverConfirmationReport PROCEDURE(Long f:ReportNumber)
RejectRecord         LONG,AUTO
save_jac_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:telephonenumber  STRING(20)
tmp:faxnumber        STRING(20)
tmp:count            LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Accessories      STRING(255)
tmp:Engineer         STRING(60)
tmp:BranchIdentification STRING(2)
Address:User_Name    STRING(30)
address:Address_Line1 STRING(30)
address:Address_Line2 STRING(30)
address:Address_Line3 STRING(30)
address:Post_Code    STRING(20)
address:Telephone_Number STRING(20)
address:Fax_No       STRING(20)
address:Email        STRING(50)
tmp:IMEINumber       STRING(30)
tmp:JobNumber        LONG
tmp:ModelNumber      STRING(30)
tmp:ReportNumber     LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(HANDOVER)
                       PROJECT(han:RecordNumber)
                       JOIN(haj:JobNumberKey,han:RecordNumber)
                         PROJECT(haj:Accessories)
                         PROJECT(haj:IMEINumber)
                         PROJECT(haj:JobNumber)
                         PROJECT(haj:ModelNumber)
                       END
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,885,7521,1344),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(5885,833),USE(tmp:Engineer),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Report No:'),AT(5000,990),USE(?ReportNo),TRN,FONT(,8,,)
                         STRING(@s8),AT(5885,990),USE(tmp:ReportNumber),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Engineer:'),AT(5000,833),USE(?EngineerText),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(5000,521),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,521),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,521),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,677),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,677),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,677),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,677,375,208),USE(?CPCSPgOfPgStr),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,219),USE(?detailband)
                           STRING(@s8),AT(208,0),USE(haj:JobNumber),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(833,0,1042,156),USE(haj:IMEINumber),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(1927,0,1302,208),USE(haj:ModelNumber),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           TEXT,AT(3281,0,4063,156),USE(haj:Accessories),FONT(,8,,),RESIZE
                         END
                         FOOTER,AT(0,0,,417),USE(?unnamed:2)
                           LINE,AT(302,52,6927,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Jobs:'),AT(313,104),USE(?String32),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(1667,104),USE(tmp:count),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING('Processed By:'),AT(2448,104),USE(?String36),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(3333,260,1615,0),USE(?Line2),COLOR(COLOR:Black)
                           LINE,AT(5729,260,1615,0),USE(?Line2:3),COLOR(COLOR:Black)
                           STRING('Received By:'),AT(5000,104),USE(?String36:2),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(Address:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('HANDOVER CONFIRMATION REPORT'),AT(3542,0,3906,260),USE(?TitleText),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(address:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(address:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(address:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s20),AT(156,729,1156,156),USE(address:Post_Code),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1042),USE(address:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1198),USE(address:Fax_No),TRN,FONT(,9,,)
                         STRING(@s50),AT(573,1354,3844,198),USE(address:Email),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Job No'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(833,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Accessories'),AT(3281,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Model Number'),AT(1927,2083),USE(?string45:2),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('HandOverConfirmationReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 76
      ! Save Window Name
   AddToLog('Report','Open','HandOverConfirmationReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'HandOverConf'
  If PrintOption(PreviewReq,glo:ExportReport,'Handover Confirmation Report') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('tmp:ReportNumber',tmp:ReportNumber)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:HANDOVER.Open
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Access:HANDOJOB.UseFile
  
  
  RecordsToProcess = RECORDS(HANDOVER)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(HANDOVER,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      ! Before Embed Point: %BeforeKeySet) DESC(Before SET() issued) ARG()
      tmp:ReportNumber = f:ReportNumber
      ! After Embed Point: %BeforeKeySet) DESC(Before SET() issued) ARG()
      SET(han:RecordNumberKey)
      Process:View{Prop:Filter} = |
      'han:RecordNumber = tmp:ReportNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        tmp:Count += 1
        If han:HandoverType = 'JOB'
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = haj:JobNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                ! Found
                If job:Exchange_Unit_Number <> 0
                    haj:Accessories = ''
                End ! If job:Exchange_Unit_Number <> 0
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                ! Error
            End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        End ! If f:Type = 0
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(rpt:detail)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(HANDOVER,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','HandOverConfirmationReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:HANDOVER.Close
    Relate:JOBS.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'HANDOVER')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !Before Opening Report
  Access:USERS.Clearkey(use:User_Code_Key)
  use:User_Code   = han:UserCode
  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Found
      tmp:Engineer    = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      ! Found
      tmp:PrintedBy = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      ! Error
  End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  
  printer{propprint:Copies} = GETINI('RAPIDSTATUS','Copies',,CLIP(PATH())&'\SB2KDEF.INI')
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !After Opening Report
  SetTarget(Report)
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  !--- Webify SETUP ClarioNET:UseReportPreview(0) tmp:BranchIdentification ---------------
  ! 26 Aug 2002 John
  ! 02 sep 2002 modified by J to refer to variables acutally used ;-)
  if glo:WebJob Or han:RRCAccountNumber <> '' then
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = han:RRCAccountNumber
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          ! Found
          tmp:BranchIdentification = tra:BranchIdentification
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          ! Error
          tmp:BranchIdentification = ''
      End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      !Set up no preview on client
      !ClarioNET:UseReportPreview(0)
  
      !*************************set the display address to the head account if booked on as a web job***********************
      Address:User_Name=        tra:Company_Name
      address:Address_Line1=    tra:Address_Line1
      address:Address_Line2=    tra:Address_Line2
      address:Address_Line3=    tra:Address_Line3
      address:Post_Code=        tra:Postcode
      address:Telephone_Number= tra:Telephone_Number
      address:Fax_No=           tra:Fax_Number
      address:Email=            tra:EmailAddress
  
      !*********************************************************************************************************************
  Else
      Address:User_Name=def:User_Name
      address:Address_Line1=def:Address_Line1
      address:Address_Line2=def:Address_Line2
      address:Address_Line3=def:Address_Line3
      address:Post_Code=def:Postcode
      address:Telephone_Number=def:telephone_Number
      address:Fax_No=def:Fax_Number
      address:Email=def:EmailAddress
  END
  !---------------------------------------------------------------------------------------
  
  ! Inserting (DBH 06/06/2006) #7793 - Set the report title
  If han:HandoverType = 'JOB'
      ?TitleText{prop:Text} = 'HANDOVER CONFIRMATION REPORT'
  Else ! If han:HandoverType = 0
      ?TitleText{prop:Text} = 'HANDOVER EXCHANGE CONF. REPORT'
  End ! If han:HandoverType = 0
  ! End (DBH 06/06/2006) #7793
  
  SetTarget()
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='HandOverConfirmationReport'
  END
  report{Prop:Preview} = PrintPreviewImage







PageNames            PROCEDURE  (thepagenumber)       ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
pagefile file,driver('DOS'),name(NAMEVAR),create
rec record
f1  long
    ..
tmp:DesktopFolder   CString(255)
tmp:TempFilePath    CString(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    a#=a#+1
    If glo:ReportName = 'ESTIMATEALERT'
        ! Save the estimate to default folder and save name (DBH: 18-05-2006)
        tmp:DesktopFolder = Format(Today(), @d012) & Format(Clock(), @n010) & '.wmf'
        If GetTempPathA(255, tmp:TempFilePath)
            If Sub(tmp:TempFilePath, -1, 1) = '\'
                tmp:DesktopFolder = Clip(tmp:TempFilePath) & Clip(tmp:DesktopFolder)
            Else ! If Sub(TempFilePath,-1,1) = '\'
                tmp:DesktopFolder = Clip(tmp:TempFilePath) & '\' & Clip(tmp:DesktopFolder)
            End ! If Sub(tmp:TempFilePath,-1,1) = '\'
        End ! If
        namevar = tmp:DesktopFolder
    Else ! If glo:ReportName = 'ESTIMATEALERT'
        If glo:WebJob
            tmp:DesktopFolder = Format(Today(), @d012) & Format(Clock(), @n010) & Format(pagenumber, @n04) & '.wmf'
            If GetTempPathA(255, tmp:TempFilePath)
                If Sub(tmp:TempFilePath, -1, 1) = '\'
                    tmp:DesktopFolder = Clip(tmp:TempFilePath) & Clip(tmp:DesktopFolder)
                Else ! If Sub(TempFilePath,-1,1) = '\'
                    tmp:DesktopFolder = Clip(tmp:TempFilePath) & '\' & Clip(tmp:DesktopFolder)
                End ! If Sub(tmp:TempFilePath,-1,1) = '\'
            End ! If
            namevar = tmp:DesktopFolder
            flq:FileName = namevar
            Add(FileListQueue)
        Else ! If glo:WebJob
            SHGetSpecialFolderPath( GetDesktopWindow(), tmp:DesktopFolder, 16, FALSE )
            tmp:DesktopFolder = tmp:DesktopFolder & '\ServiceBase Reports'
            If ~Exists(tmp:DesktopFolder)
                If ~MkDir(tmp:DesktopFolder)
                ! Can't create desktop folder
                End ! If ~MkDir(tmp:Desktop)
            End ! If ~Exists(tmp:Desktop)
            tmp:DesktopFolder = tmp:DesktopFolder & '\' & Clip(glo:ReportName)
            If ~Exists(tmp:DesktopFolder)
                If ~MkDir(tmp:DesktopFolder)
                ! Can't create desktop folder
                End ! If ~MkDir(tmp:Desktop)
            End ! If ~Exists(tmp:Desktop)
            ! namevar=path()&'\page'&format(pagenumber,@n04)&'.wmf'
            namevar = tmp:DesktopFolder & '\' & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & format(pagenumber, @n04) & '.wmf'
        End ! If glo:WebJob

    End ! If glo:ReportName = 'ESTIMATEALERT'
    create(pagefile)
    return(namevar)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()






JobIncomeReport PROCEDURE(func:StartDate,func:EndDate,func:All,func:AlLChargeTypes,func:ReportOrder,func:zero,func:style,func:Paper,func:arc,func:silent)
! Before Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
    MAP
LoadSBOnlineCriteria    PROCEDURE(),LONG
    END
RepQ    RepQClass
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
! After Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
Local                CLASS
ReportCriteria       Procedure(Byte func:Type),Byte
ReportCriteria2      Procedure(Byte func:Type),Byte
                     END
Loan_Issued          STRING(3)
rrc_flag             BYTE
save_job_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
save_inv_id          USHORT,AUTO
save_par_id          USHORT,AUTO
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
AddressGroup         GROUP,PRE(address)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:CChargeType      STRING(30)
tmp:WChargeType      STRING(30)
tmp:All              BYTE(0)
ReportGroup          GROUP,PRE(report)
InvoiceNumber        STRING(16)
InvoiceDate          STRING(30)
JobNumber            STRING(20)
CompanyName          STRING(30)
User                 STRING(3)
Repair               STRING(3)
Exchanged            STRING(3)
ARCCharge            REAL
HandlingFee          REAL
LostLoanCharge       REAL
Labour               REAL
Parts                REAL
PartsSelling         REAL
ARCMarkup            REAL
Vat                  REAL
Total                REAL
CashAmount           REAL
CreditAmount         REAL
AmountPaid           REAL
AmountOutstanding    REAL
                     END
TotalGroup           GROUP,PRE(total)
Lines                LONG
HandlingFee          REAL
LostLoanCharge       REAL
ARCCharge            REAL
Labour               REAL
Parts                REAL
PartsSelling         REAL
ARCMarkup            REAL
VAT                  REAL
Total                REAL
CashAmount           REAL
CreditAmount         REAL
AmountPaid           REAL
AmountOutstanding    REAL
                     END
NumberQueue          QUEUE,PRE(number)
RecordNumber         LONG
RecordNumber2        LONG
                     END
tmp:DateRange        STRING(60)
tmp:ReportOrder      BYTE(0)
tmp:AllChargeTypes   BYTE(0)
tmp:ARCVatTotal      REAL
tmp:ARCLocation      STRING(30)
ShowEmpty            BYTE
TempFilePath         CSTRING(255)
tmp:FixedPrice       BYTE(0)
tmp:LoopCount        LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

report               REPORT('Status Report'),AT(458,2010,10938,5688),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,),LANDSCAPE,THOUS
                       HEADER,AT(427,323,9740,1375),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(address:CompanyName),TRN,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(104,260,3531,198),USE(address:AddressLine1),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(7448,729),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(8438,729),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,365,3531,198),USE(address:AddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,469,3531,198),USE(address:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,573),USE(address:Postcode),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(7448,573,625,208),USE(?String67),TRN,FONT(,8,,)
                         STRING(@s60),AT(8438,573),USE(tmp:printedby),TRN,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(8438,417),USE(tmp:DateRange),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(7448,417,625,208),USE(?String67:2),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(104,729),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,729),USE(address:TelephoneNumber),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(104,833),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,833),USE(address:FaxNumber),TRN,FONT(,8,,)
                         STRING(@s255),AT(573,938,3531,198),USE(address:EmailAddress),TRN,FONT(,8,,)
                         STRING('Email:'),AT(104,938),USE(?String16:2),TRN,FONT(,9,,)
                         STRING('Page:'),AT(7448,885),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@s4),AT(8438,885),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(8802,885),USE(?String72),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(9010,885,313,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
break1                 BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(10,10,10875,167),USE(?DetailBand)
                           STRING(@n10.2),AT(9750,0),USE(report:AmountPaid),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(10260,0,521,167),USE(report:AmountOutstanding),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(5000,0),USE(report:LostLoanCharge),TRN,RIGHT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n-10.2),AT(7146,0),USE(report:ARCMarkup),TRN,RIGHT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s3),AT(3667,0),USE(report:Exchanged),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s3),AT(3448,0),USE(report:Repair),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n10.2),AT(4479,0),USE(report:ARCCharge),TRN,RIGHT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n-10.2),AT(5552,0),USE(report:Parts),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(7667,0),USE(report:Vat),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(8177,0),USE(report:Total),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(8698,0),USE(report:CashAmount),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(9208,0),USE(report:CreditAmount),TRN,RIGHT,FONT(,7,,)
                           STRING(@s15),AT(156,0),USE(report:InvoiceNumber),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(1490,0,1135,167),USE(report:JobNumber),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s12),AT(2563,0),USE(job:Account_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s3),AT(3240,0),USE(job:Engineer),TRN,FONT(,7,,)
                           STRING(@n10.2),AT(3948,0),USE(report:HandlingFee),TRN,RIGHT,FONT(,7,,)
                           STRING(@n-10.2),AT(6615,0),USE(report:Labour),TRN,RIGHT,FONT(,7,,)
                           STRING(@n-10.2),AT(6104,0),USE(report:PartsSelling),TRN,RIGHT,FONT(,7,,)
                           STRING(@s10),AT(948,0),USE(report:InvoiceDate),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:3)
                           LINE,AT(198,52,10635,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,156),USE(?String68),TRN,FONT(,7,,FONT:bold)
                           STRING(@s8),AT(885,156),USE(total:Lines),FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(4406,156),USE(total:ARCCharge),TRN,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                           STRING(@n12.2),AT(3875,156),USE(total:HandlingFee),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n-14.2),AT(5927,156),USE(total:PartsSelling),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n-14.2),AT(6438,156),USE(total:Labour),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(7594,156),USE(total:VAT),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(8104,156),USE(total:Total),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(8625,156),USE(total:CashAmount),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(9135,156),USE(total:CreditAmount),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(9677,156),USE(total:AmountPaid),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(10104,156,677,156),USE(total:AmountOutstanding),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(4927,156),USE(total:LostLoanCharge),TRN,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-14.2),AT(6969,156),USE(total:ARCMarkup),TRN,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-14.2),AT(5375,156),USE(total:Parts,,?total:Parts:2),TRN,RIGHT,FONT(,7,,FONT:bold)
                         END
                       END
NewPage                DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?unnamed:4)
                       END
TitlePage              DETAIL,AT(,,,219),USE(?unnamed:6),ABSOLUTE
                       END
                       FORM,AT(406,313,10969,7448),USE(?unnamed)
                         IMAGE('Rlistlan.gif'),AT(104,0,10938,7448),USE(?Image1)
                         STRING('CHARGEABLE INCOME REPORT'),AT(3177,63,4188,240),USE(?ReportTitle),TRN,RIGHT,FONT(,12,,FONT:bold)
                         STRING('Inv No'),AT(208,1406),USE(?InvoiceNumberText),TRN,FONT(,7,,FONT:bold)
                         STRING('Account No'),AT(2625,1406),USE(?String26:4),TRN,FONT(,7,,FONT:bold)
                         STRING('Coll'),AT(3188,1406),USE(?String26:5),TRN,FONT(,7,,FONT:bold)
                         STRING('Rep'),AT(3448,1406),USE(?String69),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Exch'),AT(3729,1406),USE(?String73),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Hndlng Fee'),AT(4031,1406),USE(?String26:7),TRN,FONT(,7,,FONT:bold)
                         STRING('Lost Loan'),AT(5260,1510),USE(?String77),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Labour'),AT(6875,1510),USE(?String26:8),TRN,FONT(,7,,FONT:bold)
                         STRING('RRC Parts'),AT(6042,1354),USE(?String26:9),TRN,FONT(,7,,FONT:bold)
                         STRING('Outstndng'),AT(10344,1510),USE(?AmountOutstanding),TRN,FONT(,7,,FONT:bold)
                         STRING('RRC'),AT(8385,1354),USE(?String81:4),TRN,FONT(,7,,FONT:bold)
                         STRING('RRC'),AT(6927,1354),USE(?String81:2),TRN,FONT(,7,,FONT:bold)
                         STRING('/Exchange'),AT(4042,1510),USE(?String80),TRN,FONT('Arial',7,,)
                         STRING('Cost'),AT(5917,1510),USE(?String26:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Selling'),AT(6365,1510),USE(?String26:3),TRN,FONT(,7,,FONT:bold)
                         STRING('Parts M/Up'),AT(7344,1510),USE(?String74),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Amount'),AT(10208,1354),USE(?AmountPaid:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Job No'),AT(1552,1406),USE(?JobNumberText),TRN,FONT(,7,,FONT:bold)
                         STRING('Inv Date'),AT(1010,1406),USE(?InvoiceDateText),TRN,FONT(,7,,FONT:bold)
                         STRING('Paid'),AT(10052,1510),USE(?AmountPaid),TRN,FONT(,7,,FONT:bold)
                         STRING('Credit Crd'),AT(9417,1406),USE(?CreditCard),TRN,FONT(,7,,FONT:bold)
                         STRING('RRC'),AT(5417,1354),USE(?String81),TRN,FONT(,7,,FONT:bold)
                         STRING('ARC Charge'),AT(4625,1406),USE(?String70),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('ARC'),AT(7448,1354),USE(?String85),TRN,FONT(,7,,FONT:bold)
                         STRING('RRC'),AT(7917,1354),USE(?String81:3),TRN,FONT(,7,,FONT:bold)
                         STRING('Cash/Chqe'),AT(8844,1406),USE(?CashCheque),TRN,FONT(,7,,FONT:bold)
                         STRING('Total'),AT(8365,1510,260,156),USE(?String26:11),TRN,FONT(,7,,FONT:bold)
                         STRING('V.A.T.'),AT(7917,1510),USE(?String26:10),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('JobIncomeReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','JobIncomeReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULTS.Open
  Relate:JOBPAYMT.Open
  Relate:PAYTYPES.Open
  Relate:SUBTRACC_ALIAS.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBS.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBSE.UseFile
  Access:SUBTRACC.UseFile
  Access:PARTS.UseFile
  Access:AUDIT.UseFile
  Access:AUDIT2.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = Records(INVOICE)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Chargeable Invoices
        
        !added 3/12/02 - L386 - allow export to CSV file
        if ~func:paper then
            !this is an export to csv file - dont show error when paper report is empty
            ShowEmpty=false
        
            !prepare file for export
            If GetTempPathA(255,TempFilePath)
                If Sub(TempFilePath,-1,1) = '\'
                    glo:File_Name = Clip(TempFilePath) & 'Job Income Report VODR0063-' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
                Else !If Sub(TempFilePath,-1,1) = '\'
                    glo:File_Name = Clip(TempFilePath) & '\Job Income Report VODR0063-' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
                End !If Sub(TempFilePath,-1,1) = '\'
        
                Remove(glo:File_Name)
                Access:EXPGEN.Open()
                Access:EXPGEN.UseFile()
        
                !write the title
        
                IF func:style = 'I'
                    Clear(gen:record)
                    Gen:line1   = 'CHARGEABLE INCOME REPORT - INVOICED  (Version: ' & Clip(ProgramVersionNumber()) & ')'
                    access:expgen.insert()
                ELSE
                    Clear(gen:record)
                    Gen:line1   = 'CHARGEABLE INCOME REPORT - NON-INVOICED  (Version: ' & Clip(ProgramVersionNumber()) & ')'
                    access:expgen.insert()
                END
        
                Clear(gen:record)
                Gen:line1   = 'Date Range:,' & tmp:DateRange
                Access:expgen.insert()
                Clear(gen:record)
                Gen:line1   = 'Printed By,' & tmp:printedby
                Access:expgen.insert()
                Clear(gen:record)
                Gen:line1   = 'Date Printed,'&format(Today(),@d6)
                Access:expgen.insert()
        
                Clear(gen:Record)
                !What is the point of this code????
                If tmp:ReportOrder = 1 or func:style <> 'I'
                    Gen:line1 =  'Job No,Branch ID,Franchise Job No,'
                ELSE
                   Gen:line1 =  'Job No,Branch ID,Franchise Job No,'
                End !tmp:ReportOrder = 1
        
                Gen:line1 =  'Job No,IMEI Number,Branch ID,Franchise Job No,'
        
                If func:Style = 'I'
                    gen:Line1   = Clip(gen:Line1) & 'Inv No,Inv Date,'
                End !If func:Type = 'I'
        
                !Add engineer option - 3788 (DBH: 19-04-2004)
                ! Changing (DBH 29/06/2006) # 7583 - Add Waybill Number column
                ! gen:Line1   = Clip(gen:line1)&'Enginer Option,Head Account No,Head Account Name,Account Number,Account Name,Sub Account No,Sub Account Name,Chargeable Charge Type,Completed Date,Chargeable Repair Type,Current Status,Days In Status,Total Days,Engineer,Repair,Exch,Loan,Handling/Exchange Fee,ARC Charge,RRC Lost Loan,RRC Part Cost,RRC Part Sell,RRC Labour,'&|
                !                                       'ARC M/Up,RRC VAT,RRC Total,Cash/Chq,Credit Card,Paid,Outstanding,Coll By'
                ! ! to (DBH 29/06/2006) # 7583
        
                gen:Line1   = Clip(gen:line1)&'Enginer Option,Head Account No,Head Account Name,Account Number,Waybill Number,Account Name,Sub Account No,Sub Account Name,Chargeable Charge Type,Completed Date,Chargeable Repair Type,Current Status,Days In Status,Total Days,Engineer,Repair,Exch,Loan,Handling/Exchange Fee,ARC Charge,RRC Lost Loan,RRC Part Cost,RRC Part Sell,RRC Labour,'&|
                                              'ARC M/Up,RRC VAT,RRC Total,Cash/Chq,Credit Card,Paid,Outstanding,Coll By'
                                              ! End (DBH 29/06/2006) #7583
        
        
                Access:EXPGEN.Insert()
        
            ELSE
                if func:silent = false ! Silent mode - don't display messages
                    if glo:webjob then
                        Case Missive('Unable to source a temporary directory on the web server. Please report this error to the IT Department.'&|
                          '<13,10>'&|
                          '<13,10>A paper report will now be prepared.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    ELSE
                        Case Missive('Unable to souce a temporary directory on your computer. Please setup a temporary directory for Windows to use.'&|
                          '<13,10>'&|
                          '<13,10>A paper report will now be prepared.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    END
                end
                !Can't get temp file - print a paper file
                func:paper = 1
                ShowEmpty=true
            END
        
        ELSE
            ShowEmpty=true
        END !if not paper report
        !end of added 3/12/02 - L386 - allow export to CSV file - more at bottom of proc
        
        
        IF func:style = 'I'
            SetTarget(Report)
            ?ReportTitle{prop:Text} = 'CHARGEABLE INCOME REPORT (INVOICED)'
            !Print(rpt:TitlePage)
            SetTarget()
            Clear(TotalGroup)
        
            Save_inv_ID = Access:INVOICE.SaveFile()
        
            Prog.Init(Records(INVOICE))
        
            Prog.ProgressText('Building Jobs List...')
        
            ! RRC invoice date
            Access:INVOICE.ClearKey(inv:RRCInvoiceDateKey)
            inv:RRCInvoiceDate = tmp:StartDate
            set(inv:RRCInvoiceDateKey, inv:RRCInvoiceDateKey)
            Loop
                If Access:INVOICE.NEXT()
                   Break
                End !If
                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop()
        
                If inv:RRCInvoiceDate > tmp:EndDate
                    Break
                End ! If inv:RRCInvoiceDate > tmp:EndDate
        
                If ~inv:ExportedRRCOracle
                    Cycle
                End !If ~inv:ExportedRRCOracle
        
                !Start - Only pick certain accounts - TrkBs: 4762 (DBH: 28-02-2005)
                If ~tmp:All
                    Sort(glo:Queue,glo:Pointer)
                    glo:Pointer = inv:Account_Number
                    Get(glo:Queue,glo:Pointer)
                    If Error()
                        Cycle
                    End !Error()
                End ! If ~tmp:All
                !End   - Only pick certain accounts - TrkBs: 4762 (DBH: 28-02-2005)
        
                Access:JOBS.Clearkey(job:InvoiceNumberKey)
                job:Invoice_Number  = inv:Invoice_Number
                If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
                    !Found
                    !Start - Only pick certain charge types - TrkBs: 4762 (DBH: 28-02-2005)
                    If ~tmp:AllChargeTypes
                        Sort(glo:Queue2,glo:Pointer2)
                        glo:Pointer2    = job:Charge_Type
                        Get(glo:Queue2,glo:Pointer2)
                        If Error()
                            Cycle
                        End !If Error()
                    End !If ~tmp:AllChargeTypes
                    !End   - Only pick certain charge types - TrkBs: 4762 (DBH: 28-02-2005)
        
                    !TB13343 - J - 04/08/14 - don't show the invoice if the job is no longer chargeable
                    if job:Chargeable_Job <> 'YES' then cycle.
                    !END TB13343
        
                    Access:WEBJOB.Clearkey(wob:RefNumberKey)
                    wob:RefNumber   = job:Ref_Number
                    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        !Found
                    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Found
        
                        If glo:WebJob or func:ARC = True
                            If func:ARC = False
                                If wob:HeadAccountNumber <> Clarionet:Global.Param2
                                    Cycle
                                End !If wob:HeadAccountNumber <> Clarionet:Global.Param2
                            End ! If func:ARC = False
                            If ~jobe:WebJob
                                Cycle
                            End !If ~jobe:WebJob
                        Else ! If glo:WebJob or func:ARC = True
                            If ~SentToHub(job:Ref_Number)
                                Cycle
                            End !If ~SentToHub(job:Ref_Number)
                        End ! If glo:WebJob or func:ARC = True
        
                        number:RecordNumber = inv:Invoice_Number
                        number:RecordNumber2    = job:Ref_Number
                        Add(NumberQueue)
        
                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
               Else ! If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
                    !Error
                    Cycle
               End ! If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
        
            End !Loop
            Access:INVOICE.RestoreFile(Save_inv_ID)
        ELSE
        
            SetTarget(Report)
            ?ReportTitle{prop:Text} = 'CHARGEABLE INCOME REPORT (NON-INVOICED)'
            !Print(rpt:TitlePage)
            SetTarget()
            Clear(TotalGroup)
        
            Prog.Init(Records(JOBS))
        
            Access:Jobs.ClearKey(job:DateCompletedKey)
            job:Date_Completed = tmp:StartDate
            Set(job:DateCompletedKey,job:DateCompletedKey)
            Loop
                If Access:Jobs.NEXT()
                   Break
                End !If
                If job:Date_Completed > tmp:EndDate       |
                    Then Break.  ! End If
        
                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop()
        
                If job:Invoice_Number > 0
                    Cycle
                End !If Local.ReportCriteria(1)
        
                IF job:Chargeable_Job <> 'YES'
                    CYCLE
                END
        
                If Local.ReportCriteria2(2)
                    Cycle
                End !If Local.ReportCriteria(1)
        
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Found
        
                    If Local.ReportCriteria2(3)
                        Cycle
                    End !If Local.ReportCriteria(1)
        
        
                    number:RecordNumber = 0
                    number:RecordNumber2    = job:Ref_Number
                    Add(NumberQueue)
        
        
                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
                ! Exclude units that have been sent to the hub for repair
                if SentToHub(job:Ref_Number) then cycle.
        
                ! Exclude units that have been exchanged at the RRC
                if job:Exchange_Unit_Number <> 0 and jobe:ExchangedATRRC = true
                    cycle
                end
        
            End !Loop
        END
        
        !Prog.ProgressFinish()
        
        
        IF func:Style = 'C'
          tmp:ReportOrder = 1
        END
        
        Case tmp:ReportOrder
            Of 0 !Invoice Number
                Sort(NumberQueue,number:RecordNumber)
            Of 1 !Job Number
                Sort(NumberQueue,number:RecordNumber2)
        End !tmp:ReportOrder
        
        
        Prog.ResetProgress(Records(NumberQueue))
        
        Prog.ProgressText('Jobs Found: ' & Records(NumberQueue) & '.  Building Report..')
        
        RepQ.TotalRecords = RECORDS(NumberQueue)
        
        IF func:Style = 'I'
        
            Loop tmp:LoopCount = 1 To Records(NumberQueue)
                Get(NumberQueue,tmp:LoopCount)
        
                IF (RepQ.SBOReport)
                    IF (RepQ.UpdateProgress())
                        BREAK
                    END ! IF
                END ! IF
        
                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop()
        
                Clear(ReportGroup)
        
                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_Number  = number:RecordNumber2
                If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Found
        
                Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                    Cycle
                End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        
                !Is the charge type fixed price? - L876 (DBH: 16-07-2003)
                tmp:FixedPrice = 0
                Access:CHARTYPE.ClearKey(cha:Warranty_Key)
                cha:Warranty    = 'NO'
                cha:Charge_Type = job:Charge_Type
                If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                    !Found
                    If cha:Zero_Parts_ARC Or cha:Zero_Parts = 'YES'
                        tmp:FixedPrice = 1
                    End !If cha:Zero_Parts_ARC Or cha:Zero_Parts = 'YES'
                Else !If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                    !Error
                End !If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        
        
                Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                inv:Invoice_Number  = number:RecordNumber
                If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                   !Found
        
                Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                        !Error
                   Cycle
                End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        
        
                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = job:Account_Number
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found
                    report:CompanyName = sub:Company_Name
                Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
        
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Found
                    rrc_flag = FALSE
                    If SentToHub(job:Ref_Number)
                        !tmp:ARCVatTotal = 0
                        !report:ARCCharge = 0
                        !tmp:ARCVatTotal = (job:Courier_Cost * (VatRate(job:Account_Number,'L') /100)) + |
                        !                  (job:Parts_Cost * (VatRate(job:Account_Number,'P') /100)) +  |
                        !                  (job:Labour_Cost * (VatRate(job:Account_Number,'L')/100))
                        !report:ARCCharge = (job:Courier_Cost + job:Parts_Cost + job:Labour_Cost + tmp:ARCVatTotal) + (jobe:ARC3rdPartyCost + jobe:ARC3rdPartyVAT)
                        !
        
                        !If job is 48 Hour then the Courier Cost is used for the Exchange Replacement Fee
                        !and should appear under the RRC Parts Column - 3685 (DBH: 11-12-2003)
                        If jobe:Engineer48HourOption = 1
                            tmp:ARCVatTotal    = (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                                                (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100)
                        Else !If jobe:Engineer48HourOption = 1
                            tmp:ARCVatTotal    = (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                                                (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                                                (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100)
        
                        End !If jobe:Engineer48HourOption = 1
        
                        !Handling Fee
                        report:HandlingFee = jobe:InvoiceHandlingFee
                        IF job:Exchange_Unit_Number >0
                            report:HandlingFee +=  jobe:InvoiceExchangeRate
                        END
        
                        !ARC Charge (Total Of ARC Costs Labour/Parts and Lost Loan)
                        !If job is 48 Hour then the Courier Cost is used for the Exchange Replacement Fee
                        !and should appear under the RRC Parts Column - 3685 (DBH: 11-12-2003)
                        If jobe:Engineer48HourOption = 1
                            report:ARCCharge = (job:Invoice_Parts_Cost + job:Invoice_Labour_Cost)
                        Else !If jobe:Engineer48HourOption = 1
                            report:ARCCharge = (job:Invoice_Courier_Cost + job:Invoice_Parts_Cost + job:Invoice_Labour_Cost)
                        End !If jobe:Engineer48HourOption = 1
        
        
        
                        !RRC Lost Loan
                        IF LoanAttachedToJob(job:ref_number) = 1
                            report:LostLoanCharge = job:Invoice_Courier_Cost
                            Loan_Issued = 'YES'
                        ELSE
                            !No Loan ever
                            report:LostLoanCharge = 0
                            Loan_Issued = 'NO'
                        END
        
                        !RRC Part Cost
                        !See Below
        
                        !RRC Part Selling
                        !report:PartsSelling = jobe:InvRRCCPartsCost - job:Invoice_Parts_Cost! - report:ARCMarkup
                        !If 48 Hour option is selected then the Exchange Replacement charge appears under the Parts Selling Column - 3685 (DBH: 11-12-2003)
                        If jobe:Engineer48HourOption = 1
                            report:PartsSelling = jobe:InvRRCCPartsCost + job:Invoice_Courier_Cost
                        Else !If jobe:Engineer48HourOption = 1
                            report:PartsSelling = jobe:InvRRCCPartsCost
                        End !If jobe:Engineer48HourOption = 1
        
        
                        !RRC Labour
                        report:Labour   = jobe:InvRRCCLabourCost - job:Invoice_Labour_Cost
        
                        !ARC M/Up
                        !Include the Exchange Replacement Charge in this column - 3685 (DBH: 11-12-2003)
                        report:ARCMarkup = report:PartsSelling - job:Invoice_Parts_Cost
                        !Do not allow a negative markup - L876 (DBH: 16-07-2003)
                        If report:ARCMarkup < 0
                            report:ARCMarkup = 0
                        End !If report:ARCMarkup < 0
        
                        !If fixed price then there is no markup
                        !(But I cannot know if the job AT THE TIME was fixed price) - L876 (DBH: 16-07-2003)
                        If tmp:FixedPrice
                            report:ARCMarkup = 0
                        End !If tmp:FixedPrice
        
                        !RRC VAT
                        report:Vat      = (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour/100) + |
                                            (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts/100) + |
                                            (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100)
        
                        !RRC Total
                        report:Total    = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job:Invoice_Courier_Cost + report:VAT
                        rrc_flag = TRUE
                    Else !If SentToHub(job:Ref_Number)
                        !Handling Fee
                        report:HandlingFee = 0
        
                        !ARC Charge (Total Of ARC Costs Labour/Parts and Lost Loan)
                        report:ARCCharge = 0
        
                        !RRC Lost Loan
                        IF LoanAttachedToJob(job:ref_number) = 1
                            report:LostLoanCharge = job:Invoice_Courier_Cost
                            Loan_Issued = 'YES'
                        ELSE
                            !No Loan ever
                            report:LostLoanCharge = 0
                            Loan_Issued = 'NO'
                        END
        
                        !RRC Part Cost
                        !See Below
        
                        !RRC Part Selling
                        report:PartsSelling = jobe:InvRRCCPartsCost
        
                        !RRC Labour
                        report:Labour   = jobe:InvRRCCLabourCost
        
                        !ARC M/Up
                        !report:ARCMarkup = jobe:InvRRCCPartsCost
                        !Do not show markup for jobs that were not sent to the hub - L876 (DBH: 16-07-2003)
                        report:ARCMarkup    = 0
        
                        !RRC VAT
                        report:Vat      = (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour/100) + |
                                            (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts/100) + |
                                            (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100)
        
                        !RRC Total
                        report:Total    = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job:Invoice_Courier_Cost + report:VAT
                        rrc_flag = TRUE
                    End !If SentToHub(job:Ref_Number)
        
                    report:CashAmount   = 0
                    report:CreditAmount = 0
                    IF func:Zero = 'Y'
                      IF report:Total = 0
                        CYCLE
                      END
                    END
                    report:User = ''
                    !Work out payments
                    Save_jpt_ID = Access:JOBPAYMT.SaveFile()
                    Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
                    jpt:Ref_Number =  job:Ref_Number
                    Set(jpt:All_Date_Key,jpt:All_Date_Key)
                    Loop
                        If Access:JOBPAYMT.NEXT()
                           Break
                        End !If
                        If jpt:Ref_Number <> job:Ref_Number      |
                            Then Break.  ! End If
                        Access:PAYTYPES.ClearKey(pay:Payment_Type_Key)
                        pay:Payment_Type = jpt:Payment_Type
                        If Access:PAYTYPES.TryFetch(pay:Payment_Type_Key) = Level:Benign
                            !Found
                            If pay:Credit_Card = 'YES'
                                report:CreditAmount += jpt:Amount
                            Else !If pay:Credit_Card = 'YES'
                                report:CashAmount += jpt:Amount
                            End !If pay:Credit_Card = 'YES'
                        Else!If Access:PAYTYPES.TryFetch(pay:Payment_Type_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            report:CashAmount += jpt:Amount
                        End!If Access:PAYTYPES.TryFetch(pay:Payment_Type_Key) = Level:Benign
                        report:User = jpt:User_Code !The last record should be the last user paied
                    End !Loop
                    Access:JOBPAYMT.RestoreFile(Save_jpt_ID)
        
        
                    report:AmountPaid += report:CreditAmount + report:CashAmount
        
                    report:AmountOutstanding = report:Total - report:AmountPaid
                    If report:AmountOutstanding < 0
                        report:AmountOutstanding = 0
                    End !If report:AmountOutstanding < 0
        
                    !Only add RRC Parts Cost to the report *IF* the ARC job was NOT Fixed price
                    report:Parts = 0
                    Save_par_ID = Access:PARTS.SaveFile()
                    Access:PARTS.ClearKey(par:Part_Number_Key)
                    par:Ref_Number  = job:Ref_Number
                    Set(par:Part_Number_Key,par:Part_Number_Key)
                    Loop
                        If Access:PARTS.NEXT()
                           Break
                        End !If
                        If par:Ref_Number  <> job:Ref_Number      |
                            Then Break.  ! End I                    f
                        !Count ARC Parts
                        CountParts# += 1
                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                        sto:Ref_Number  = par:Part_Ref_Number
                        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Location <> tmp:ARCLocation
                                !These are RRC Parts
                                report:Parts += par:RRCAveragePurchaseCost
                                !Don't count RRC Parts
                            End !If sto:Location <> tmp:ARCLocation
                        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        
                    End !Loop
                    Access:PARTS.RestoreFile(Save_par_ID)
        
        
        
        !            If (glo:webJob OR func:arc = TRUE) and SentToHub(job:ref_Number)
        !                report:Parts = 0
        !            End !If glo:webJob and SentToHub(job:ref_Number
        !
        !            If ~glo:WebJob And (jobe:WebJob And ~SentToHub(job:Ref_Number))
        !                IF rrc_flag = FALSE
        !                  report:Parts = 0
        !                END
        !            End !If ~glo:WebJob And (jobe:WebJob And ~SentToHub(job:Ref_Number))
        
                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = wob:HeadAccountNumber
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found
                    Case tmp:ReportOrder
                        Of 0
                            report:InvoiceNumber = Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                            report:InvoiceDate   = Format(inv:RRCInvoiceDate,@d06)
                            report:JobNumber     = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
                        Of 1
                            report:InvoiceNumber     = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
                            report:InvoiceDate = Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                            report:JobNumber   = Format(inv:RRCInvoiceDate,@d06)
                    End !Case tmp:ReportOrder
                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
                if jobe:WebJob AND SentToHub(job:ref_number) = FALSE
                    report:Repair = 'RRC'
                else
                    report:Repair = 'ARC'
                end
        
                if job:Exchange_Unit_Number = 0
                    report:Exchanged = 'NO'
                else
                    if jobe:ExchangedATRRC
                        report:Exchanged = 'RRC'
                    else
                        report:Exchanged = 'ARC'
                    end
                end
        
                !Add totals
                total:Lines             += 1
                total:ARCCharge         += report:ARCCharge
                total:HandlingFee       += report:HandlingFee
                total:LostLoanCharge    += report:LostLoanCharge
                total:Labour            += report:Labour
                total:Parts             += report:Parts
                total:PartsSelling      += report:PartsSelling
                total:ARCMarkup         += report:ARCMarkup
                total:VAT               += report:Vat
                total:Total             += report:Total
                total:CashAmount        += report:CashAmount
                total:CreditAmount      += report:CreditAmount
                total:AmountPaid        += report:AmountPaid
                total:AmountOutstanding += report:AmountOutstanding
        
                if func:paper then
                    Print(rpt:Detail)
                ELSE
                    Do Export
                END !if tmp:paper
            End !Loop x# = 1 To Records(NumberQueue)
        ELSE !IF func:Style = 'I'
            
            Loop tmp:LoopCount = 1 To Records(NumberQueue)
                Get(NumberQueue,tmp:LoopCount)
        
                IF (RepQ.SBOReport)
                    IF (RepQ.UpdateProgress())
                        BREAK
                    END ! IF
                END ! IF
        
                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop()
        
                Clear(ReportGroup)
        
                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_Number  = number:RecordNumber2
                If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Found
        
                Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                    Cycle
                End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        
                !Is the charge type fixed price? - L876 (DBH: 16-07-2003)
                tmp:FixedPrice = 0
                Access:CHARTYPE.ClearKey(cha:Warranty_Key)
                cha:Warranty    = 'NO'
                cha:Charge_Type = job:Charge_Type
                If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                    !Found
                    If cha:Zero_Parts_ARC Or cha:Zero_Parts = 'YES'
                        tmp:FixedPrice = 1
                    End !If cha:Zero_Parts_ARC Or cha:Zero_Parts = 'YES'
                Else !If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                    !Error
                End !If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        
        
                Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                inv:Invoice_Number  = number:RecordNumber
                If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                   !Found
                Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                        !Error
        !           Cycle
                End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        
        
                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = job:Account_Number
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found
                    report:CompanyName = sub:Company_Name
                Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = wob:HeadAccountNumber
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Found
        
                    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Error
                    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Found
        
                    rrc_flag = FALSE
                    If SentToHub(job:Ref_Number)
                        !Why is this here?
        !                if jobe:HandlingFee <> 0 then cycle.
        
                        !Handling Fee
                        report:HandlingFee = jobe:HandlingFee
                        IF job:Exchange_Unit_Number >0
                            report:HandlingFee +=  jobe:ExchangeRate
                        END
        
                        !ARC Charge (Total Of ARC Costs Labour/Parts and Lost Loan)
                        !If job is 48 Hour then the Courier Cost is used for the Exchange Replacement Fee
                        !and should appear under the RRC Parts Column - 3685 (DBH: 11-12-2003)
                        If jobe:Engineer48HourOption = 1
                            report:ARCCharge = (job:Courier_Cost + job:Parts_Cost)
                        Else !If jobe:Engineer48HourOption = 1
                            report:ARCCharge = (job:Courier_Cost + job:Parts_Cost + job:Labour_Cost)
                        End !If jobe:Engineer48HourOption = 1
        
                        !RRC Lost Loan
                        IF LoanAttachedToJob(job:ref_number) = 1
                            report:LostLoanCharge = job:Courier_Cost
                            Loan_Issued = 'YES'
                        ELSE
                            !No Loan ever
                            report:LostLoanCharge = 0
                            Loan_Issued = 'NO'
                        END
        
                        !RRC Part Cost
                        !See Below
        
                        !RRC Part Selling
                        !report:PartsSelling = jobe:RRCCPartsCost! - job:Parts_Cost! - report:ARCMarkup
                        !If 48 Hour option is selected then the Exchange Replacement charge appears under the Parts Selling Column - 3685 (DBH: 11-12-2003)
                        If jobe:Engineer48HourOption = 1
                            report:PartsSelling = jobe:RRCCPartsCost + job:Courier_Cost
                        Else !If jobe:Engineer48HourOption = 1
                            report:PartsSelling = jobe:RRCCPartsCost
                        End !If jobe:Engineer48HourOption = 1
        
        
                        !RRC Labour
                        report:Labour   = jobe:RRCCLabourCost - job:Labour_Cost
        
                        !ARC M/Up
                        report:ARCMarkup = report:PartsSelling - job:Parts_Cost
                        !Do not allow negative markups - L876 (DBH: 16-07-2003)
                        If report:ARCMarkup < 0
                            report:ARCMarkup = 0
                        End !If report:ARCMarkup < 0
                        !If fixed price then there is no markup
                        !(But I cannot know if the job AT THE TIME was fixed price) - L876 (DBH: 16-07-2003)
                        If tmp:FixedPrice
                            report:ARCMarkup = 0
                        End !If tmp:FixedPrice
        
                        !RRC VAT
                        report:Vat      = (jobe:RRCCLabourCost * VatRate(job:Account_Number,'L')/100) + |
                                            (jobe:RRCCPartsCost * VatRate(job:Account_Number,'P')/100) + |
                                            (job:Courier_Cost * VatRate(job:Account_Number,'L')/100)
        
                        !RRC Total
                        report:Total    = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:Courier_Cost + report:VAT
        
                        tmp:ARCVatTotal = 0
                        !If 48 Hour option is selected then the Exchange Replacement charge appears under the Parts Selling Column - 3685 (DBH: 11-12-2003)
                        If jobe:Engineer48HourOption = 1
                            tmp:ARCVatTotal = (job:Courier_Cost * (VatRate(job:Account_Number,'L') /100)) + |
                                              (job:Parts_Cost * (VatRate(job:Account_Number,'P') /100))
        
                        Else !If jobe:Engineer48HourOption = 1
                            tmp:ARCVatTotal = (job:Courier_Cost * (VatRate(job:Account_Number,'L') /100)) + |
                                              (job:Parts_Cost * (VatRate(job:Account_Number,'P') /100)) +  |
                                              (job:Labour_Cost * (VatRate(job:Account_Number,'L')/100))
        
                        End !If jobe:Engineer48HourOption = 1
        
                        ! + (jobe:ARC3rdPartyCost)
                        !report:ARCCharge = (job:Courier_Cost+job:Parts_Cost+job:Labour_Cost + tmp:ARCVatTotal) + (jobe:ARC3rdPartyCost + jobe:ARC3rdPartyVAT)
        
                        !Parts Selling should only show parts that the RRC added
        
                        rrc_flag = TRUE
                    Else !If SentToHub(job:Ref_Number)
                        !Handling Fee
                        report:HandlingFee = 0
        
                        !ARC Charge (Total Of ARC Costs Labour/Parts and Lost Loan)
                        report:ARCCharge = 0
        
                        !RRC Lost Loan
                        IF LoanAttachedToJob(job:ref_number) = 1
                            report:LostLoanCharge = job:Courier_Cost
                            Loan_Issued = 'YES'
                        ELSE
                            !No Loan ever
                            report:LostLoanCharge = 0
                            Loan_Issued = 'NO'
                        END
        
                        !RRC Part Cost
                        !See Below
        
                        !RRC Part Selling
                        report:PartsSelling = jobe:RRCCPartsCost!report:ARCMarkup
        
                        !RRC Labour
                        report:Labour   = jobe:RRCCLabourCost
        
                        !ARC M/Up
                        !report:ARCMarkup = jobe:RRCCPartsCost
                        !Markup is zero if job has not been sent to Hub - L876 (DBH: 16-07-2003)
                        report:ARCMarkup    = 0
        
                        !RRC VAT
                        report:Vat      = (jobe:RRCCLabourCost * VatRate(job:Account_Number,'L')/100) + |
                                            (jobe:RRCCPartsCost * VatRate(job:Account_Number,'P')/100) + |
                                            (job:Courier_Cost * VatRate(job:Account_Number,'L')/100)
        
                        !RRC Total
                        report:Total    = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:Courier_Cost + report:VAT
                        rrc_flag = TRUE
                    End !If SentToHub(job:Ref_Number)
        
                    report:CashAmount   = 0
                    report:CreditAmount = 0
                    IF func:Zero = 'Y'
                      IF report:Total = 0
                        CYCLE
                      END
                    END
                    report:User = ''
                    !Work out payments
                    Save_jpt_ID = Access:JOBPAYMT.SaveFile()
                    Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
                    jpt:Ref_Number =  job:Ref_Number
                    Set(jpt:All_Date_Key,jpt:All_Date_Key)
                    Loop
                        If Access:JOBPAYMT.NEXT()
                           Break
                        End !If
                        If jpt:Ref_Number <> job:Ref_Number      |
                            Then Break.  ! End If
                        Access:PAYTYPES.ClearKey(pay:Payment_Type_Key)
                        pay:Payment_Type = jpt:Payment_Type
                        If Access:PAYTYPES.TryFetch(pay:Payment_Type_Key) = Level:Benign
                            !Found
                            If pay:Credit_Card = 'YES'
                                report:CreditAmount += jpt:Amount
                            Else !If pay:Credit_Card = 'YES'
                                report:CashAmount += jpt:Amount
                            End !If pay:Credit_Card = 'YES'
                        Else!If Access:PAYTYPES.TryFetch(pay:Payment_Type_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            report:CashAmount += jpt:Amount
                        End!If Access:PAYTYPES.TryFetch(pay:Payment_Type_Key) = Level:Benign
                        report:User = jpt:User_Code !The last record should be the last user paied
                    End !Loop
                    Access:JOBPAYMT.RestoreFile(Save_jpt_ID)
        
                    report:Parts = 0
                    Save_par_ID = Access:PARTS.SaveFile()
                    Access:PARTS.ClearKey(par:Part_Number_Key)
                    par:Ref_Number  = job:Ref_Number
                    Set(par:Part_Number_Key,par:Part_Number_Key)
                    Loop
                        If Access:PARTS.NEXT()
                           Break
                        End !If
                        If par:Ref_Number  <> job:Ref_Number      |
                            Then Break.  ! End If
                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                        sto:Ref_Number  = par:Part_Ref_Number
                        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Location <> tmp:ARCLocation
                                !These are RRC Parts
                                report:Parts += par:RRCAveragePurchaseCost
                                !Don't count RRC Parts
                            End !If sto:Location <> tmp:ARCLocation
                        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    End !Loop
                    Access:PARTS.RestoreFile(Save_par_ID)
        
        
                    report:AmountPaid += report:CreditAmount + report:CashAmount
        
                    report:AmountOutstanding = report:Total - report:AmountPaid
                    If report:AmountOutstanding < 0
                        report:AmountOutstanding = 0
                    End !If report:AmountOutstanding < 0
        
        
                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = wob:HeadAccountNumber
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found
                    report:InvoiceNumber = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
                    report:InvoiceDate   = ''
                    report:JobNumber     = ''
        
                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
                if jobe:WebJob AND SentToHub(job:ref_number) = FALSE
                    report:Repair = 'RRC'
                else
                    report:Repair = 'ARC'
                end
        
                if job:Exchange_Unit_Number = 0
                    report:Exchanged = 'NO'
                else
                    if jobe:ExchangedATRRC
                        report:Exchanged = 'RRC'
                    else
                        report:Exchanged = 'ARC'
                    end
                end
        
                !Add totals
                total:Lines             += 1
                total:HandlingFee       += report:HandlingFee
                total:LostLoanCharge    += report:LostLoanCharge
                total:ARCCharge         += report:ARCCharge
                total:Labour            += report:Labour
                total:Parts             += report:Parts
                total:PartsSelling      += report:PartsSelling
                total:ARCMarkup         += report:ARCMarkup
                total:VAT               += report:Vat
                total:Total             += report:Total
                total:CashAmount        += report:CashAmount
                total:CreditAmount      += report:CreditAmount
                total:AmountPaid        += report:AmountPaid
                total:AmountOutstanding += report:AmountOutstanding
        
                if func:paper then
                    Print(rpt:Detail)
                ELSE
                    Do Export
                END !if tmp:paper
            End !Loop x# = 1 To Records(NumberQueue)
        END
        Prog.ProgressFinish()
        
        !added 3/12/02 - L386 - allow export to CSV file
        if func:paper
            !all done and finished
        ELSE
            !Print the final lines
            Clear(gen:Record)
            gen:Line1   = 'TOTALS'
            Access:EXPGEN.Insert()
        
            Clear(gen:Record)
            Gen:line1   =   'Jobs Counted,'&clip(Total:Lines)&',,,,,,,,,,,,,,,,,,,,,'
            !There are two extra columns for the invoiced report
            If func:Style = 'I'
                gen:Line1 = Clip(gen:Line1) & ',,'
            End !Case func:Style
            gen:Line1   = Clip(gen:Line1) & clip(FORMAT(total:HandlingFee,@n_10.2))&','&clip(FORMAT(total:ARCCharge,@n_10.2))&','&|
                            clip(FORMAT(total:LostLoanCharge,@n_10.2))&','&clip(FORMAT(total:Parts,@n_10.2))&','&clip(FORMAT(total:PartsSelling,@n_10.2))&','&|
                            clip(FORMAT(total:Labour,@n_10.2))&','&clip(FORMAT(total:ARCMarkup,@n_10.2))&','&clip(FORMAT(total:Vat,@n_10.2))&','&|
                            clip(FORMAT(total:Total,@n_10.2))&','&clip(FORMAT(total:CashAmount,@n_10.2))&','&clip(FORMAT(total:CreditAmount,@n_10.2))&','&|
                            clip(FORMAT(total:AmountPaid,@n_10.2))&','&clip(FORMAT(total:AmountOutstanding,@n_10.2))
            Access:EXPGEN.Insert()
            Access:EXPGEN.close()
            IF (RepQ.SBOReport)
                IF (RepQ.FinishReport('Chargeable Income Report (' & CHOOSE(func:style = 'I','Invoiced','Non-Invoiced') & ').csv', |
                                    glo:File_Name))
                END ! IF
        
            ELSE ! IF (RepQ.SBOReport)
        
                if Total:lines = 0 then
                    if func:silent = false
                        Case Missive('There are no records that match the selected criteria.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    end
                ELSE
                    if glo:webjob then
                        !send the file to the client
                        SendFileToClient(glo:File_Name)
                    ELSe
                        !copy it to where it is expected
                        copy(glo:file_name,Glo:CSV_SavePath)
                    END
                    if func:silent = false
                        Case Missive('Chargeable Income Report Created.','ServiceBase 3g',|
                                       'midea.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    end
                END !If totalLines = 0
            END ! IF (RepQ.SBOReport)
        
            !remove the file
            
            Remove(glo:File_name)
        
        END!if tmp:paper
        !end of - added 3/12/02 - L386 - allow export to CSV file
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF ShowEmpty = TRUE
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'c:\REPORT.TXT'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
        IF ShowEmpty = TRUE
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF ShowEmpty = TRUE
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
        IF ShowEmpty = TRUE
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','JobIncomeReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:JOBPAYMT.Close
    Relate:PAYTYPES.Close
    Relate:SUBTRACC_ALIAS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Before Embed Point: %EndOfProcedure) DESC(End of Procedure) ARG()
  If func:Paper
  
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  End ! func:Paper
  ! After Embed Point: %EndOfProcedure) DESC(End of Procedure) ARG()
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  IF (RepQ.Init(COMMAND()) = Level:Benign)
      IF (LoadSBOnlineCriteria())
          RETURN 
      END ! IF
  END ! IF
  
  
  
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
  tmp:All         = func:All
  tmp:AllChargeTypes  = func:AllChargeTypes
  tmp:ReportOrder = func:ReportOrder
  
  tmp:DateRange   = Format(tmp:StartDate,@d6) & ' to ' & Format(tmp:EndDate,@d6)
  
  If ~glo:WebJob
      address:CompanyName     = def:User_Name
      address:AddressLine1    = def:Address_Line1
      address:AddressLine2    = def:Address_Line2
      address:AddressLine3    = def:Address_Line3
      address:Postcode        = def:Postcode
      address:TelephoneNumber = def:Telephone_Number
      address:FaxNumber       = def:Fax_Number
      address:EmailAddress    = def:EmailAddress
  Else !glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          address:CompanyName     = tra:Company_Name
          address:AddressLine1    = tra:Address_Line1
          address:AddressLine2    = tra:Address_Line2
          address:AddressLine3    = tra:Address_Line3
          address:Postcode        = tra:Postcode
          address:TelephoneNumber = tra:Telephone_Number
          address:FaxNumber       = tra:Fax_Number
          address:EmailAddress    = tra:EmailAddress
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  End !glo:WebJob
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
    IF (RepQ.SBOReport)
        0{prop:Hide} = 1
    END !IF
  
  SetTarget(Report)
  If func:style <> 'I'
      ?InvoiceNumberText{prop:Text} = 'Job No'
      ?InvoiceDateText{prop:Text} = ''
      ?JobNumberText{prop:Text} = ''
  Else !func:style <> 'I'
      If tmp:ReportOrder = 1
          ?InvoiceNumberText{prop:Text} = 'Job No'
          ?InvoiceDateText{prop:Text} = LEFT('Inv No')
          ?JobNumberText{prop:Text} = RIGHT('Inv Date')
      End !tmp:ReportOrder = 1
  
  End !func:style <> 'I'
  
  SetTarget()
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation  = tra:SiteLocation
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='JobIncomeReport'
  END
  report{Prop:Preview} = PrintPreviewImage


! Before Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()
Export      Routine
Data
local:EngineerOption    String(3)
Code
            Clear(gen:record)
            !Job Number
            gen:Line1   = Clip(job:Ref_Number)
            !IMEI Number
            gen:Line1   = Clip(gen:Line1) & ',' & Clip(job:ESN)
            !Branch ID
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(tra:BranchIdentification)
            !Webjob Number
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(wob:JobNumber)
            !Invoice Number/ Date (depending on paper order)
            If func:Style = 'I'
                Case tmp:ReportOrder
                    Of 0
                        gen:Line1   = Clip(gen:Line1) & ',' & StripComma(report:InvoiceNumber)
                        gen:Line1   = Clip(gen:Line1) & ',' & StripComma(report:InvoiceDate)
                    Of 1
                        gen:Line1   = Clip(gen:Line1) & ',' & StripComma(report:InvoiceDate)
                        gen:Line1   = Clip(gen:Line1) & ',' & StripComma(report:JobNumber)
                End !Case tmp:ReportOrder
            End !If func:Style = 'I'
            !Add column to show Engineer Option - 3788 (DBH: 15-04-2004)
            Case jobe:Engineer48HourOption
                Of 1
                    local:EngineerOption = '48H'
                Of 2
                    local:EngineerOption = 'ARC'
                Of 3
                    local:EngineerOption = '7DT'
                Of 4
                    local:EngineerOption = 'STD'
                Else
                    local:EngineerOption = ''
            End !Case jobe:Engineer48HourOption
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(local:EngineerOption)
            !Head Acccount Number
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(tra:Account_Number)
            !Head Account Name
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(tra:Company_Name)
            !Account Number (Generic)
            !Account Name (Genric)
            !WayBill Number
            Access:SUBTRACC_ALIAS.ClearKey(sub_ali:Account_Number_Key)
            sub_ali:Account_Number = job:Account_Number
            If Access:SUBTRACC_ALIAS.TryFetch(sub_ali:Account_Number_Key) = Level:Benign
                !Found
                If sub_ali:Generic_Account
                    Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
                    tra_ali:Account_Number  = sub_ali:Main_Account_Number
                    If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
                        !Found
                        gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(tra_ali:Account_Number)
                        ! Inserting (DBH 29/06/2006) # 7583 - Add column for waybill
                        If Sub(tra_ali:Account_Number,1,7) = 'AA20-85'
                            Found# = 0
                            If job:Exchange_Unit_Number <> ''
                                Access:AUDIT.ClearKey(aud:TypeActionKey)
                                aud:Ref_Number = job:Ref_Number
                                aud:Type       = 'EXC'
                                aud:Action     = 'DESPATCH FROM RRC'
                                Set(aud:TypeActionKey,aud:TypeActionKey)
                                Loop
                                    If Access:AUDIT.NEXT()
                                       Break
                                    End !If
                                    If aud:Ref_Number <> job:Ref_Number      |
                                    Or aud:Type       <> 'EXC'      |
                                    Or aud:Action     <> 'DESPATCH FROM RRC'      |
                                        Then Break.  ! End If

                                    Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                                    aud2:AUDRecordNumber = aud:Record_Number
                                    IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                                        x# = Instring('WAYBILL NO:',aud2:Notes,1,1)
                                        If x# > 0
                                            Found# = 1
                                            Loop y# = x# To Len(Clip(aud2:Notes))
                                                If Sub(aud2:Notes,y#,1) = '<13>'
                                                    Break
                                                End ! If Sub(aud:Notes,y#,1) = '<13>'
                                            End ! Loop y# = x# To 255
                                            gen:Line1 = Clip(gen:Line1) & ',' & StripComma(Sub(aud2:Notes,x# + 12,y# - (x# + 12)))
                                            Break
                                        End ! If x# > 0

                                    END ! IF
                                End !Loop
                            Else ! If job:Exchange_Unit_Number <> ''
                                Access:AUDIT.ClearKey(aud:TypeActionKey)
                                aud:Ref_Number = job:Ref_Number
                                aud:Type       = 'JOB'
                                aud:Action     = 'DESPATCH FROM RRC'
                                Set(aud:TypeActionKey,aud:TypeActionKey)
                                Loop
                                    If Access:AUDIT.NEXT()
                                       Break
                                    End !If
                                    If aud:Ref_Number <> job:Ref_Number      |
                                    Or aud:Type       <> 'JOB'      |
                                    Or aud:Action     <> 'DESPATCH FROM RRC'      |
                                        Then Break.  ! End If
                                    Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                                    aud2:AUDRecordNumber = aud:Record_Number
                                    IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                                        x# = Instring('WAYBILL NO:',aud2:Notes,1,1)
                                        If x# > 0
                                            Found# = 1
                                            Loop y# = x# To Len(Clip(aud2:Notes))
                                                If Sub(aud2:Notes,y#,1) = '<13>'
                                                    Break
                                                End ! If Sub(aud:Notes,y#,1) = '<13>'
                                            End ! Loop y# = x# To 255
                                            gen:Line1 = Clip(gen:Line1) & ',' & StripComma(Sub(aud2:Notes,x# + 12,y# - (x# + 12)))
                                            Break
                                        End ! If x# > 0

                                    END ! IF
                                End !Loop
                            End ! If job:Exchange_Unit_Number <> ''
                            If Found# = 0
                                gen:Line1 = Clip(gen:Line1) & ','
                            End ! If Found# = 0
                        Else ! If Sub(job:Account_Number,1,7) = 'AA20-85'
                            gen:Line1   = Clip(gen:Line1) & ','
                        End ! If Sub(job:Account_Number,1,7) = 'AA20-85'
                        ! End (DBH 29/06/2006) #7583
                        gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(tra_ali:Company_Name)
                    Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
                        !Error
                        gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma('')
                        gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma('')
                        gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma('')
                    End !If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
                Else
                    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma('')
                    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma('')
                    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma('')
                End !If sub_ali:Generic_Account
            Else!If Access:SUBTRACC_ALIAS.TryFetch(sub_ali:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma('')
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma('')
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma('')
            End!If Access:SUBTRACC_ALIAS.TryFetch(sub_ali:Account_Number_Key) = Level:Benign
            !Sub Account Number
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Account_Number)
            !Sub Account Name
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Company_Name)
            !Chargeable Charge Type
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Charge_Type)
            !Completed Date
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(Format(job:Date_Completed,@d06b))
            !Chargeable Repair Type
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Repair_Type)
            !Current Status
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Current_Status)
            !Days In Status
            statusdays" = today() - wob:Current_Status_Date
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(StatusDays")
            !Total Days
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(Today() - job:Date_Booked)
            !Engineer
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = job:Engineer
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Found
                gen:Line1   = Clip(gen:Line1) & ',' & StripComma(Clip(use:Forename) & ' ' & Clip(use:Surname))
            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Error
                gen:Line1   = Clip(gen:Line1) & ',' & StripComma('')
            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Repair
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(report:Repair)
            !Exch
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(report:Exchanged)
            !Loan
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(Loan_Issued)
            !Handling Fee
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:HandlingFee,@n-_14.2))
            !ARC Charge
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:ARCCharge,@n-_14.2))
            !Lost Loan
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:LostLoanCharge,@n-_14.2))
            !Parts Cost
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:Parts,@n-_14.2))
            !Parts Selling
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:PartsSelling,@n-_14.2))
            !Labour
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:Labour,@n-_14.2))
            !ARC Markup
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:ARCMarkup,@n-_14.2))
            !VAT
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:Vat,@n-_14.2))
            !Total
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:Total,@n-_14.2))
            !Cash/Chq
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:CashAmount,@n-_14.2))
            !Credit Card
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:CreditAmount,@n-_14.2))
            !Paid
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:AmountPaid,@n-_14.2))
            !Outstanding
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(FORMAT(report:AmountOutstanding,@n-_14.2))
            !Coll By
            gen:Line1   = Clip(gen:Line1) & ',' & StripComma(report:User)
            Access:expgen.insert()
! After Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()
! Before Embed Point: %DBHProcedureSection) DESC(Procedure Local * (Place Local Procedure Code Here When Using Bryan's Progress Bar) *) ARG()
Local.ReportCriteria2        Procedure(Byte func:Type)
Code
    Case func:Type
        Of 1
!            !Had the web invoice been created?
!            If glo:WebJob
!                If ~inv:ExportedRRCOracle
!                    Return Level:Fatal
!                End !If inv:ExportedRRCOracle
!            End !If glo:WebJob
!
!
!            If inv:Invoice_Type = 'WAR' Or inv:Invoice_Type = 'RET'
!                Return Level:Fatal
!            End !If inv:Invoice_Type = 'WAR' Or inv:Invoice_Type = 'RET'
        Of 2
            If ~tmp:All
                Sort(glo:Queue,glo:Pointer)
                glo:Pointer = job:Account_Number
                Get(glo:Queue,glo:Pointer)
                If Error()
                    Return Level:Fatal
                End !Error()
            End !tmp:All

            If ~tmp:AllChargeTypes
                Sort(glo:Queue2,glo:Pointer2)
                glo:Pointer2    = job:Charge_Type
                Get(glo:Queue2,glo:Pointer2)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If ~tmp:AllChargeTypes
        Of 3
            If glo:WebJob OR func:arc = TRUE
                IF func:arc = FALSE
                  If wob:HeadAccountNumber <> Clarionet:Global.Param2
                      Return Level:Fatal
                  End !If wob:HeadAccountNumber <> Clarionet:Global.Param2
                END
                If ~jobe:WebJob
                    Return Level:Fatal
                End !If ~jobe:WebJob
!                If ~inv:ExportedRRCOracle
!                    Return Level:Fatal
!                End !If ~inv:ExportedRRCOracle

            Else !If glo:WebJob
                If ~SentToHub(job:Ref_Number)
                    Return Level:Fatal
                End !If SentToHub(job:Ref_Number)
            End !If glo:WebJob
    End !Case func:Type

    Return Level:Benign
Local.ReportCriteria        Procedure(Byte func:Type)
Code
    Case func:Type
        Of 1
            !Had the web invoice been created?
            If glo:WebJob OR func:arc = TRUE
                If ~inv:ExportedRRCOracle
                    Return Level:Fatal
                End !If inv:ExportedRRCOracle
            End !If glo:WebJob

            If ~tmp:All
                Sort(glo:Queue,glo:Pointer)
                glo:Pointer = inv:Account_Number
                Get(glo:Queue,glo:Pointer)
                If Error()
                    Return Level:Fatal
                End !Error()
            End !tmp:All

            If inv:Invoice_Type = 'WAR' Or inv:Invoice_Type = 'RET'
                Return Level:Fatal
            End !If inv:Invoice_Type = 'WAR' Or inv:Invoice_Type = 'RET'
        Of 2
            If ~tmp:AllChargeTypes
                Sort(glo:Queue2,glo:Pointer2)
                glo:Pointer2    = job:Charge_Type
                Get(glo:Queue2,glo:Pointer2)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If ~tmp:AllChargeTypes
        Of 3
            If glo:WebJob OR func:arc = TRUE
                IF func:arc = FALSE
                  If wob:HeadAccountNumber <> Clarionet:Global.Param2
                      Return Level:Fatal
                  End !If wob:HeadAccountNumber <> Clarionet:Global.Param2
                END
                If ~jobe:WebJob
                    Return Level:Fatal
                End !If ~jobe:WebJob
                If ~inv:ExportedRRCOracle
                    Return Level:Fatal
                End !If ~inv:ExportedRRCOracle

            Else !If glo:WebJob
                If ~SentToHub(job:Ref_Number)
                    Return Level:Fatal
                End !If SentToHub(job:Ref_Number)
            End !If glo:WebJob
    End !Case func:Type

    Return Level:Benign
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: UnivAbcReport
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE
! Before Embed Point: %DBHProgressSetupCode1) DESC(Progress Bar Setup Code Section) ARG()
    IF (RepQ.SBOReport)
        RETURN
    END !IF
! After Embed Point: %DBHProgressSetupCode1) DESC(Progress Bar Setup Code Section) ARG()

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE
! Before Embed Point: %DBHProgressLoopCode1) DESC(Progress Bar Inside Loop Code Section) ARG()
    IF (RepQ.SBOReport)
        RETURN 0
    END !IF
! After Embed Point: %DBHProgressLoopCode1) DESC(Progress Bar Inside Loop Code Section) ARG()

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 150
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE
! Before Embed Point: %DBHProgressTextCode1) DESC(Progress Bar Progress Text Code Section) ARG()
    IF (RepQ.SBOReport)
        RETURN
    END !IF
! After Embed Point: %DBHProgressTextCode1) DESC(Progress Bar Progress Text Code Section) ARG()

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE
! Before Embed Point: %DBHProgressFinishCode1) DESC(Progress Bar Progress Finish Code Section) ARG()
    IF (RepQ.SBOReport)
        RETURN
    END !IF
! After Embed Point: %DBHProgressFinishCode1) DESC(Progress Bar Progress Finish Code Section) ARG()

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
LoadSBOnlineCriteria        PROCEDURE()!,LONG
RetValue                        LONG(Level:Benign)
i                               LONG
qXML                            QUEUE(),PRE(qXml)
ReportOrder                         CSTRING(10)
ReportType                          CSTRING(10)
StartDate                           CSTRING(10)
EndDate                             CSTRING(10)
ReportStyle                         CSTRING(10)
GenericAccounts                     CSTRING(10)
AllAccounts                         CSTRING(10)
AllChargeTypes                      CSTRING(10)
ZeroSuppression                     CSTRING(10)
                                END! QUEUE
qAccounts QUEUE(),PRE(qAccounts) 
AccountNumber CSTRING(31)
        END!  QUEUE                               
qChargeTypes QUEUE(),PRE(qChargeTypes)        
ChargeType      CSTRING(31)
             END! QUEUE
    
    CODE
        LOOP 1 TIMES
            glo:WebJob = 1
            
            IF (XML:LoadFromFile(RepQ.FullCriteriaFilename))
                RetValue = Level:Fatal
                BREAK
            END ! IF
            XML:GotoTop

            IF (~XML:FindNextNode('Defaults'))
                recs# = XML:LoadQueue(qXML,1,1)
                IF (RECORDS(qXML) = 0)
                    RetValue = Level:Fatal
                    BREAK
                END ! IF
                GET(qXML, 1)
                !XML:DebugMyQueue(qXML,'qXML')
                func:StartDate = qXML.StartDate
                func:EndDate = qXML.EndDate
                func:Style = qXML.ReportStyle
                func:AllChargeTypes = qXML.AllChargeTypes
                func:ReportOrder = qXML.ReportOrder
                func:All = qXML.AllAccounts
                func:Zero = qXML.ZeroSuppression
                func:Paper = 0
                func:Silent = 1

                IF (qXML.AllAccounts <> 1)
                    XML:GotoTop()
                    IF (~XML:FindNextNode('Accounts'))
                        recs# = XML:LoadQueue(qAccounts)
                        IF (RECORDS(qAccounts) > 0)
                            LOOP i = 1 TO RECORDS(qAccounts)
                                GET(qAccounts, i)
                                glo:Pointer = qAccounts.AccountNumber
                                ADD(glo:Queue)
                            END ! LOOP
                        END !I F
                    END ! !IF
                END ! IF
                IF (qXML.AllChargeTypes <> 1)
                    XML:GotoTop()
                    IF (~XML:FindNextNode('ChargeTypes'))
                        recs# = XML:LoadQueue(qChargeTypes)
                        IF (RECORDS(qChargeTypes) > 0)
                            LOOP i = 1 TO RECORDS(qChargeTypes)
                                GET(qChargeTypes,i)
                                glo:Pointer2 = qChargeTypes.ChargeType
                                ADD(glo:Queue2)
                            END!  LOOP
                        END ! IF
                    END ! IF
                END ! IF
                
                XML:Free()
            END ! IF
            
            

            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = RepQ.UserCode
            Access:USERS.TryFetch(use:User_Code_Key)
            Clarionet:Global.Param2 = rpq:AccountNumber
            tmp:PrintedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)

            RepQ.UpdateStatus('Running')

            !DO Export

        END ! BREAK LOOP

        IF (RetValue = Level:Fatal)
            RepQ.Failed()
        END !I F

        RETURN RetValue

! After Embed Point: %DBHProcedureSection) DESC(Procedure Local * (Place Local Procedure Code Here When Using Bryan's Progress Bar) *) ARG()





JobIncomeReportCriteria PROCEDURE (func:AutoRun)      !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::10:TAGDISPSTATUS   BYTE(0)
DASBRW::10:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_sub_id          USHORT,AUTO
tmp:AccountTag       STRING(1)
tmp:PaymentTag       STRING(1)
tmp:ChargeTypeTag    STRING(1)
tmp:ChargeType       STRING(30)
tmp:WarrantyChargeType STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:AllAccounts      BYTE(1)
tmp:MainAccount      STRING(30)
tmp:True             BYTE(1)
tmp:AllChargeTypes   BYTE(1)
tmp:ReportOrder      BYTE(0)
tmp:No               STRING('NO {28}')
Tmp:Zero_Sup         STRING(1)
tmp:Report_Style     STRING(1)
tmp:Paper            BYTE(1)
DOSDialogHeader      CSTRING(40)
DOSExtParameter      CSTRING(255)
DOSTargetVariable    CSTRING(1024)
tmp:arc              BYTE
BRW5::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                       PROJECT(sub:Generic_Account)
                       PROJECT(sub:Main_Account_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:AccountTag         LIKE(tmp:AccountTag)           !List box control field - type derived from local data
tmp:AccountTag_Icon    LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
sub:Generic_Account    LIKE(sub:Generic_Account)      !Browse key field - type derived from field
sub:Main_Account_Number LIKE(sub:Main_Account_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:ChargeTypeTag      LIKE(tmp:ChargeTypeTag)        !List box control field - type derived from local data
tmp:ChargeTypeTag_Icon LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Chargeable Income Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(168,332),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(236,332),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Tab1'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select the Trade Accounts you wish to include'),AT(252,126),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All Accounts'),AT(433,126),USE(tmp:AllAccounts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Accounts'),TIP('All Accounts'),VALUE('1','0')
                           SHEET,AT(168,136,344,190),USE(?Sheet2),SPREAD
                             TAB('By Account Number'),USE(?Tab4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                             TAB('Generic Accounts'),USE(?Tab5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                           END
                           LIST,AT(172,152,236,170),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@67L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(249,152,5,5),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(249,174,5,5),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(424,188),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(424,218),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(424,250),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 6'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Charge Types To Include'),AT(252,126),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All Charge Types'),AT(388,126),USE(tmp:AllChargeTypes),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Charge Types'),TIP('All Charge Types'),VALUE('1','0')
                           LIST,AT(252,140,180,134),USE(?List:2),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(289,176,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(293,208,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(240,278),USE(?DASTAG:2),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,278),USE(?DASTAGAll:2),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,278),USE(?DASUNTAGALL:2),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 3'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Invoice Date Range'),AT(291,126),USE(?Prompt4),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Start Date'),AT(252,138),USE(?tmp:StartDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(340,138,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(408,134),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(252,158),USE(?tmp:EndDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(340,158,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(408,154),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           OPTION('Report Type'),AT(234,252,212,24),USE(tmp:Paper),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Paper'),AT(238,260),USE(?tmp:Paper:Radio7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Electronic'),AT(346,260),USE(?tmp:Paper:Radio8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                           END
                           PROMPT('Path to save file'),AT(248,280),USE(?SavePathPrompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(248,290,191,11),USE(glo:CSV_SavePath),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(444,286),USE(?LookupFile),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           OPTION('Report Style'),AT(234,174,212,24),USE(tmp:Report_Style),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Chargeable Non-Invoiced'),AT(238,184),USE(?Option2:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('C')
                             RADIO('Invoiced Only'),AT(346,184),USE(?Option2:Radio4),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('I')
                           END
                           OPTION('Zero Suppression'),AT(234,198,212,24),USE(Tmp:Zero_Sup),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Suppress Zeros'),AT(238,208),USE(?Option3:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y')
                             RADIO('Show Zeros'),AT(346,208),USE(?Option3:Radio6),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('N')
                           END
                           OPTION('Report Order'),AT(234,224,212,24),USE(tmp:ReportOrder),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('By Invoice Number'),AT(238,234),USE(?tmp:ReportOrder:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('By Job Number'),AT(346,234),USE(?tmp:ReportOrder:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Chargeable Income Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

Wizard7         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet2)  = 2
BRW9                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
FileLookup11         SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
tmp:DesktopFolder       CString(255)

RepQ    RepQClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue.Pointer = sub:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:AccountTag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:AccountTag = ''
  END
    Queue:Browse.tmp:AccountTag = tmp:AccountTag
  IF (tmp:Accounttag = '*')
    Queue:Browse.tmp:AccountTag_Icon = 2
  ELSE
    Queue:Browse.tmp:AccountTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::6:QUEUE = glo:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sub:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::10:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW9.UpdateBuffer
   glo:Queue2.Pointer2 = cha:Charge_Type
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = cha:Charge_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:ChargeTypeTag = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:ChargeTypeTag = ''
  END
    Queue:Browse:1.tmp:ChargeTypeTag = tmp:ChargeTypeTag
  IF (tmp:ChargeTypeTag = '*')
    Queue:Browse:1.tmp:ChargeTypeTag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:ChargeTypeTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = cha:Charge_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::10:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::10:QUEUE = glo:Queue2
    ADD(DASBRW::10:QUEUE)
  END
  FREE(glo:Queue2)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::10:QUEUE.Pointer2 = cha:Charge_Type
     GET(DASBRW::10:QUEUE,DASBRW::10:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = cha:Charge_Type
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASSHOWTAG Routine
   CASE DASBRW::10:TAGDISPSTATUS
   OF 0
      DASBRW::10:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::10:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::10:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020191'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('JobIncomeReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?VSBackButton
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:REPSCHCR.Open
  Access:REPSCHCT.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLG.UseFile
  SELF.FilesOpened = True
  tmp:StartDate = Today()
  tmp:EndDate   = Today()
  
  
  If glo:WebJob
      tmp:MainAccount = Clarionet:Global.Param2
  Else !glo:WebJob
      tmp:MainAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  End !glo:WebJob
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  BRW9.Init(?List:2,Queue:Browse:1.ViewPosition,BRW9::View:Browse,Queue:Browse:1,Relate:CHARTYPE,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (RepQ.Init(COMMAND()) = Level:Benign)
     JobIncomeReport(0,0,0,0,0,'0','0',0,0,0)
     POST(Event:CloseWindow)
     RETURN RequestCompleted
  END!  IF
  
  If glo:WebJob
      ?LookupStartDate{prop:Hide} = 1
      ?LookupEndDate{prop:Hide} = 1
  ELSE
      HIDE(?tab1)
      tmp:arc = TRUE
  End !glo:WebJob
  set(defaults)
  access:defaults.next()
  Glo:CSV_SavePath = def:ExportPath
  if Glo:CSV_SavePath[len(clip(Glo:CSV_SavePath))] <> '\' then
      Glo:CSV_SavePath = clip(Glo:CSV_SavePath)&'\'
  END
  
  if func:AutoRun = true
      ! Auto-run wizard
      tmp:StartDate = date(month(today()), 1, year(today()))
      tmp:EndDate = today()
      tmp:AllAccounts = 1            ! All trade accounts
      tmp:AllChargeTypes = 1         ! All charge types
      tmp:ReportOrder = 0            ! By invoice number
      Tmp:Zero_Sup = 'Y'             ! Zero suppression
      tmp:Report_Style = 'I'         ! Chargeable non-invoiced (I - Invoiced only)
      tmp:paper = 0                  ! Export
      tmp:arc = 1                    ! ARC
  
      post(event:Accepted, ?Finish)
  end
  
  
      ! Save Window Name
   AddToLog('Window','Open','JobIncomeReportCriteria')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard7.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?cANCEL, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,sub:GenericAccountKey)
  BRW5.AddRange(sub:Generic_Account,tmp:True)
  BRW5.AddLocator(BRW5::Sort1:Locator)
  BRW5::Sort1:Locator.Init(,sub:Account_Number,1,BRW5)
  BRW5.AddSortOrder(,sub:Main_Account_Key)
  BRW5.AddRange(sub:Main_Account_Number,tmp:MainAccount)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,sub:Account_Number,1,BRW5)
  BIND('tmp:AccountTag',tmp:AccountTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(tmp:AccountTag,BRW5.Q.tmp:AccountTag)
  BRW5.AddField(sub:Account_Number,BRW5.Q.sub:Account_Number)
  BRW5.AddField(sub:Company_Name,BRW5.Q.sub:Company_Name)
  BRW5.AddField(sub:RecordNumber,BRW5.Q.sub:RecordNumber)
  BRW5.AddField(sub:Generic_Account,BRW5.Q.sub:Generic_Account)
  BRW5.AddField(sub:Main_Account_Number,BRW5.Q.sub:Main_Account_Number)
  BRW9.Q &= Queue:Browse:1
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,cha:Warranty_Key)
  BRW9.AddRange(cha:Warranty,tmp:No)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,cha:Charge_Type,1,BRW9)
  BIND('tmp:ChargeTypeTag',tmp:ChargeTypeTag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:ChargeTypeTag,BRW9.Q.tmp:ChargeTypeTag)
  BRW9.AddField(cha:Charge_Type,BRW9.Q.cha:Charge_Type)
  BRW9.AddField(cha:Warranty,BRW9.Q.cha:Warranty)
  IF ?tmp:AllAccounts{Prop:Checked} = True
    DISABLE(?List)
  END
  IF ?tmp:AllAccounts{Prop:Checked} = False
    ENABLE(?List)
  END
  IF ?tmp:AllChargeTypes{Prop:Checked} = True
    DISABLE(?List:2)
  END
  IF ?tmp:AllChargeTypes{Prop:Checked} = False
    ENABLE(?List:2)
  END
  FileLookup11.Init
  FileLookup11.Flags=BOR(FileLookup11.Flags,FILE:LongName)
  FileLookup11.Flags=BOR(FileLookup11.Flags,FILE:Directory)
  FileLookup11.SetMask('All Files','*.*')
  FileLookup11.DefaultDirectory='Glo:CSV_SavePath'
  BRW5.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
              tmp:AllChargeTypes = rpc:AllChargeTypes
  
              Access:REPSCHCT.Clearkey(rpr:ChargeTypeKey)
              rpr:REPSCHCRRecordNumber = rpc:RecordNumber
              Set(rpr:ChargeTypeKey,rpr:ChargeTypeKey)
              Loop ! Begin Loop
                  If Access:REPSCHCT.Next()
                      Break
                  End ! If Access:REPSCHCT.Next()
                  If rpr:REPSCHCRRecordNumber <> rpc:RecordNumber
                      Break
                  End ! If rpr:REPSCHCRRecordNumber <> rpc:RecordNumber
                  glo:Pointer2 = rpr:ChargeType
                  Add(glo:Queue2)
              End ! Loop
  
              If rpc:CharIncReportStyle
                  tmp:Report_Style = 'I'
              Else
                  tmp:Report_Style = 'C'
              End ! If rpc:CharIncReportStyle = 1
  
              If rpc:CharIncZeroSuppression
                  tmp:Zero_Sup = 'N'
              Else ! If rpc:CharIncZeroSuppression
                  tmp:Zero_Sup = 'Y'
              End ! If rpc:CharIncZeroSuppression
  
              tmp:ReportOrder =  rpc:ChaIncReportOrder
  
              tmp:Paper = 0
  
              SHGetSpecialFolderPath( GetDesktopWindow(), tmp:DesktopFolder, 16, FALSE )
              tmp:DesktopFolder = tmp:DesktopFolder & '\ServiceBase Export'
              If ~Exists(tmp:DesktopFolder)
                  If ~MkDir(tmp:DesktopFolder)
                      ! Can't create desktop folder
                  End ! If ~MkDir(tmp:Desktop)
              End ! If ~Exists(tmp:Desktop)
              glo:CSV_SavePath = tmp:DesktopFolder
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  tmp:StartDate = Today()
                  tmp:EndDate = Today()
              Of 2 ! 1st Of Month
                  tmp:StartDate = Date(Month(Today()),1,Year(Today()))
                  tmp:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Does not account for a change of year
  !                loc:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  ! to (DBH 31/01/2008) # 9711
                  tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  tmp:StartDate = Date(Month(tmp:EndDate),1,Year(tmp:EndDate))
  ! End (DBH 31/01/2008) #9711
  
              End ! Case rpc:DateRangeType
              func:AutoRun = True
              Post(Event:Accepted,?Finish)
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:REPSCHCR.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','JobIncomeReportCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard7.Validate()
        DISABLE(Wizard7.NextControl())
     ELSE
        ENABLE(Wizard7.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:Paper
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Paper, Accepted)
      if tmp:Paper then
          hide(?Glo:CSV_SavePath)
          hide(?LookupFile)
          hide(?SavePathPrompt)
      
      ELSE
          if glo:webjob
              !ignore - not relevant to thin client
          ELSe
              unhide(?Glo:CSV_SavePath)
              unhide(?LookupFile)
              unhide(?SavePathPrompt)
          END !if glo:webjob
      END !if tmp:paper
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Paper, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?VSBackButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?tmp:AllAccounts
      IF ?tmp:AllAccounts{Prop:Checked} = True
        DISABLE(?List)
      END
      IF ?tmp:AllAccounts{Prop:Checked} = False
        ENABLE(?List)
      END
      ThisWindow.Reset
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllChargeTypes
      IF ?tmp:AllChargeTypes{Prop:Checked} = True
        DISABLE(?List:2)
      END
      IF ?tmp:AllChargeTypes{Prop:Checked} = False
        ENABLE(?List:2)
      END
      ThisWindow.Reset
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupFile
      ThisWindow.Update
      glo:CSV_SavePath = Upper(FileLookup11.Ask(1)  )
      DISPLAY
    OF ?tmp:Report_Style
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Report_Style, Accepted)
      Case tmp:Report_Style
          Of 'C'
              ?tmp:ReportOrder:Radio1{prop:Disable} = 1
              tmp:ReportOrder = 1
          Of 'I'
              ?tmp:ReportOrder:Radio1{prop:Disable} = 0
      End !tmp:Report_Style
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Report_Style, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020191'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020191'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020191'&'0')
      ***
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      ! Inserting (DBH 14/02/2008) # 9752 - Check for valid dates
      If func:AutoRun <> 1 And ~Command('/SCHEDULE')
          If IsDateInValid(tmp:StartDate)
              Beep(Beep:SystemHand);  Yield()
             Case Missive('Invalid Start Date.','ServiceBase 3g',|
                            'mstop.jpg','/OK')
                 Of 1 ! OK Button
             End ! Case Missive
             tmp:StartDate = ''
             Select(?tmp:StartDate)
             Cycle
          End ! If IsDateInValid(tmp:StartDate)
          If IsDateInValid(tmp:EndDate)
              Beep(Beep:SystemHand);  Yield()
             Case Missive('Invalid End Date.','ServiceBase 3g',|
                            'mstop.jpg','/OK')
                 Of 1 ! OK Button
             End ! Case Missive
             tmp:EndDate = ''
             Select(?tmp:EndDate)
             Cycle
          End ! If IsDateInValid(tmp:StartDate)
      
          Beep(Beep:SystemQuestion);  Yield()
          Case Missive('Confirm Date Range:'&|
              '|    From:   ' & Format(tmp:StartDate,@d18) & |
              '|    To:        ' & Format(tmp:EndDate,@d18) & |
              '||Run Report?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
              Of 1 ! No Button
                  Display()
                  Cycle
          End ! Case Missive
      End ! If func:AutoRun <> 1 And ~Command('/SCHEDULE')
      ! End (DBH 14/02/2008) #9752
      
      JobIncomeReport(tmp:StartDate,tmp:EndDate,tmp:AllAccounts,tmp:AllChargeTypes,tmp:ReportOrder,Tmp:Zero_Sup,tmp:Report_Style,tmp:paper,tmp:arc, func:AutoRun)
      
      if func:AutoRun = true
          ! If this report is running automatically then close the window
          post(event:CloseWindow)
      end
      
      If Command('/SCHEDULE')
          If Access:REPSCHLG.PrimeRecord() = Level:Benign
              rlg:REPSCHEDRecordNumber = rpd:RecordNumber
              rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                '<13,10>Report Finished'
              If Access:REPSCHLG.TryInsert() = Level:Benign
                  !Insert
              Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                  Access:REPSCHLG.CancelAutoInc()
              End ! If Access:REPSCHLG.TryInsert() = Level:Benign
          End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
      If Keycode() = MouseLeft2
          Post(Event:Accepted,?DasTag:2)
      End ! If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2)  = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:AccountTag = ''
    ELSE
      tmp:AccountTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Accounttag = '*')
    SELF.Q.tmp:AccountTag_Icon = 2
  ELSE
    SELF.Q.tmp:AccountTag_Icon = 1
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = cha:Charge_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:ChargeTypeTag = ''
    ELSE
      tmp:ChargeTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:ChargeTypeTag = '*')
    SELF.Q.tmp:ChargeTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:ChargeTypeTag_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = cha:Charge_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::10:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue

Wizard7.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW5.Q &= NULL) ! Has Browse Object been initialized?
       BRW5.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW9.Q &= NULL) ! Has Browse Object been initialized?
       BRW9.ResetQueue(Reset:Queue)
    END

Wizard7.TakeBackEmbed PROCEDURE
   CODE

Wizard7.TakeNextEmbed PROCEDURE
   CODE

Wizard7.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False






WarrantyIncomeReport PROCEDURE(func:StartDate,func:EndDate,func:AllAccounts,func:AllChargeTypes,func:AllManufacturers,func:ReportOrder,func:includearc,func:AllStatus,func:Sub,func:ReSub,func:Rej,func:RejAck,func:Rec,func:Paper,func:arc, func:arcjobs, func:silent,f:DateRangeType,fExc)
! Before Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
local   CLASS
LoadSBOnlineCriteria    PROCEDURE(),LONG
        END
RepQ    RepQClass
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
! After Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
save_job_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
save_inv_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
RejectRecord         LONG,AUTO
ShowEmpty            BYTE
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
AddressGroup         GROUP,PRE(address)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:CChargeType      STRING(30)
tmp:WChargeType      STRING(30)
tmp:All              BYTE(0)
tmp:Paper            BYTE
ReportGroup          GROUP,PRE(report)
JobNumber            STRING(20)
CompletedDate        DATE
CompanyName          STRING(30)
Manufacturer         STRING(30)
ModelNumber          STRING(30)
ChargeType           STRING(30)
Repair               STRING(3)
Exchanged            STRING(3)
WarrantyStatus       STRING(20)
RejectionBy          STRING(60)
Engineer             STRING(60)
RepairType           STRING(30)
HandlingFee          REAL
Parts                REAL
PartsSelling         REAL
Labour               REAL
Vat                  REAL
Total                REAL
                     END
TotalGroup           GROUP,PRE(total)
Lines                LONG
HandlingFee          REAL
Labour               REAL
Parts                REAL
PartsSelling         REAL
SubTotal             REAL
VAT                  REAL
Total                REAL
                     END
NumberQueue          QUEUE,PRE(number)
Manufacturer         STRING(30)
ModelNumber          STRING(30)
JobNumber            LONG
DateCompleted        DATE
                     END
tmp:DateRange        STRING(60)
tmp:ReportOrder      BYTE(0)
tmp:AllChargeTypes   BYTE(0)
tmp:AllManufacturers BYTE(0)
tmp:AllAccounts      BYTE(0)
TempFilePath         CSTRING(255)
tmp:EngineerOption   STRING(3)
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

report               REPORT('Status Report'),AT(458,2010,10938,5688),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,),LANDSCAPE,THOUS
                       HEADER,AT(427,323,9740,1375),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(address:CompanyName),TRN,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(104,260,3531,198),USE(address:AddressLine1),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(7448,729),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(8438,729),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,365,3531,198),USE(address:AddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,469,3531,198),USE(address:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,573),USE(address:Postcode),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(7448,573,625,208),USE(?String67),TRN,FONT(,8,,)
                         STRING(@s60),AT(8438,573),USE(tmp:printedby),TRN,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(8438,417),USE(tmp:DateRange),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(7448,417,625,208),USE(?String67:2),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(104,729),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,729),USE(address:TelephoneNumber),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(104,833),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,833),USE(address:FaxNumber),TRN,FONT(,8,,)
                         STRING(@s255),AT(573,938,3531,198),USE(address:EmailAddress),TRN,FONT(,8,,)
                         STRING('Email:'),AT(104,938),USE(?String16:2),TRN,FONT(,9,,)
                         STRING('Page:'),AT(7448,885),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@s4),AT(8438,885),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(8802,885),USE(?String72),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(9010,885,313,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
break1                 BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(10,10,10875,167),USE(?DetailBand)
                           STRING(@s25),AT(4677,0,1146,156),USE(report:ChargeType),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n10.2),AT(8010,0),USE(report:Parts),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(9688,0),USE(report:Vat),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(10260,0),USE(report:Total),TRN,RIGHT,FONT(,7,,)
                           STRING(@s3),AT(7240,0),USE(report:Exchanged),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(5844,0),USE(report:WarrantyStatus),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s3),AT(6938,0),USE(report:Repair),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(3625,0,1146,156),USE(report:ModelNumber),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(2573,0,,156),USE(report:Manufacturer),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(146,0,1125,167),USE(report:JobNumber),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s15),AT(1760,0),USE(job:Account_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n10.2),AT(7438,0),USE(report:HandlingFee),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(9104,0),USE(report:Labour),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(8563,0),USE(report:PartsSelling),TRN,RIGHT,FONT(,7,,)
                           STRING(@d6),AT(1208,0),USE(report:CompletedDate),TRN,RIGHT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:3)
                           LINE,AT(198,52,10583,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,156),USE(?String68),TRN,FONT(,7,,FONT:bold)
                           STRING(@s8),AT(885,156),USE(total:Lines),FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(7365,156),USE(total:HandlingFee),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n14.2),AT(8510,156),USE(total:PartsSelling,,?total:PartsSelling:2),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n14.2),AT(9031,156),USE(total:Labour,,?total:Labour:2),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(9604,156),USE(total:VAT),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(10177,156),USE(total:Total),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n14.2),AT(7938,156),USE(total:Parts),TRN,RIGHT,FONT(,7,,FONT:bold)
                         END
                       END
NewPage                DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?unnamed:4)
                       END
TitlePage              DETAIL,AT(,,,219),USE(?unnamed:6),ABSOLUTE
                       END
                       FORM,AT(406,313,10969,7448),USE(?unnamed)
                         IMAGE('Rlistlan.gif'),AT(52,0,10938,7448),USE(?Image1)
                         STRING('WARRANTY INCOME REPORT'),AT(4927,83),USE(?ReportTitle),TRN,FONT(,12,,FONT:bold)
                         STRING('Job No'),AT(208,1406),USE(?InvoiceNumberText),TRN,FONT(,7,,)
                         STRING('Account No'),AT(1823,1406),USE(?String26:4),TRN,FONT(,7,,FONT:bold)
                         STRING('Handling/'),AT(7760,1406),USE(?String26:7),TRN,FONT(,7,,FONT:bold)
                         STRING('Labour'),AT(9479,1458),USE(?String26:8),TRN,FONT(,7,,FONT:bold)
                         STRING('Parts'),AT(8750,1406),USE(?String26:9),TRN,FONT(,7,,FONT:bold)
                         STRING('Repair'),AT(6917,1406),USE(?String58),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Exchange'),AT(7260,1406),USE(?String61),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Cost'),AT(8490,1510),USE(?String26:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Selling'),AT(8906,1510),USE(?String26:3),TRN,FONT(,7,,FONT:bold)
                         STRING('Swap Fee'),AT(7760,1510),USE(?String26:14),TRN,FONT(,7,,FONT:bold)
                         STRING('Completed'),AT(1323,1406),USE(?InvoiceDateText),TRN,FONT(,7,,)
                         STRING('Model Number'),AT(3688,1406),USE(?String26:12),TRN,FONT(,7,,FONT:bold)
                         STRING('Manufacturer'),AT(2635,1406),USE(?String26:5),TRN,FONT(,7,,FONT:bold)
                         STRING('Total'),AT(10625,1458,260,156),USE(?String26:11),TRN,FONT(,7,,FONT:bold)
                         STRING('Charge Type'),AT(4740,1406),USE(?String26:13),TRN,FONT(,7,,FONT:bold)
                         STRING('Warranty Status'),AT(5906,1406),USE(?String57),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('V.A.T.'),AT(10104,1458),USE(?String26:10),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('WarrantyIncomeReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','WarrantyIncomeReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULTS.Open
  Relate:JOBPAYMT.Open
  Relate:PAYTYPES.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBS.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBSE.UseFile
  Access:SUBTRACC.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:AUDIT.UseFile
  Access:JOBSWARR.UseFile
  Access:REPTYDEF.UseFile
  Access:AUDIT2.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = Records(JOBS)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !added 3/12/02 - L386 - allow export to CSV file
        if ~tmp:paper then
            
            !this is an export to csv file - dont show error when paper report is empty
            ShowEmpty=false
        
            !prepare file for export
            If GetTempPathA(255,TempFilePath)
                If Sub(TempFilePath,-1,1) = '\'
                    glo:File_Name = Clip(TempFilePath) & 'Warranty Claim Status Report VODR0064-' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
                Else !If Sub(TempFilePath,-1,1) = '\'
                    glo:File_Name = Clip(TempFilePath) & '\Warranty Claim Status Report VODR0064-' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
                End !If Sub(TempFilePath,-1,1) = '\'
        
                Remove(glo:File_Name)
                Access:EXPGEN.Open()
                Access:EXPGEN.UseFile()
                !write the title
                Clear(gen:record)
                if (glo:webJob = 1)
                    Gen:line1   = 'RRC WARRANTY INCOME REPORT  (Version: ' & Clip(ProgramVersionNumber()) & ')'
                else ! if (glo:WebJob)
                    Gen:line1   = 'WARRANTY INCOME REPORT  (Version: ' & Clip(ProgramVersionNumber()) & ')'
                end ! if (glo:WebJob)
        
                access:expgen.insert()
                Clear(gen:record)
                case f:DateRangeType
                of 1
                    Gen:line1   = 'Submitted Date Range:,' & tmp:DateRange
                of 2
                    Gen:line1   = 'Accepted Date Range:,' & tmp:DateRange
                else
                    Gen:line1   = 'Completed Date Range:,' & tmp:DateRange
                end ! case f:DateRangeType
        
                Access:expgen.insert()
                Clear(gen:record)
                Gen:line1   = 'Printed By,' & tmp:printedby
                Access:expgen.insert()
                Clear(gen:record)
                Gen:line1   = 'Date Printed,'&format(Today(),@d6)
                Access:expgen.insert()
        
                Clear(gen:Record)
                gen:Line1   = '"SB Job No'
                gen:Line1    = Clip(gen:Line1) & '","Franchise Branch ID'
                gen:Line1    = Clip(gen:Line1) & '","Franchise Job No'
                gen:Line1    = Clip(gen:Line1) & '","Completed'
                gen:Line1    = Clip(gen:Line1) & '","Head Account No'
                gen:Line1    = Clip(gen:Line1) & '","Head Account Name'
                gen:Line1    = Clip(gen:Line1) & '","Account No'
                gen:Line1    = Clip(gen:Line1) & '","Account Name'
                gen:Line1    = Clip(gen:Line1) & '","Manufacturer'
                gen:Line1    = Clip(gen:Line1) & '","Model Number'
                gen:Line1    = Clip(gen:Line1) & '","Warranty Charge Type'
                gen:Line1    = Clip(gen:Line1) & '","Warranty Repair Type'
                gen:Line1    = Clip(gen:Line1) & '","Engineer'
                gen:Line1    = Clip(gen:Line1) & '","Repair'
                gen:Line1    = Clip(gen:Line1) & '","Exchange'
                gen:Line1    = Clip(gen:Line1) & '","Handling/Swap Fee'
                gen:Line1    = Clip(gen:Line1) & '","Parts Cost'
                gen:Line1    = Clip(gen:Line1) & '","Part Sale'
                gen:Line1    = Clip(gen:Line1) & '","Labour'
                gen:Line1    = Clip(gen:Line1) & '","Sub Total'
                gen:Line1    = Clip(gen:Line1) & '","VAT'
                gen:Line1    = Clip(gen:Line1) & '","Total'
                gen:Line1    = Clip(gen:Line1) & '","Warranty Status'
                gen:Line1    = Clip(gen:Line1) & '","Rejection Reason 1'
                gen:Line1    = Clip(gen:Line1) & '","Rejection Reason 2'
                gen:Line1    = Clip(gen:Line1) & '","Rejection Reason 3'
                gen:Line1    = Clip(gen:Line1) & '","Rejection Accepted By'
                gen:Line1    = Clip(gen:Line1) & '","Claim Submitted"'
                Access:EXPGEN.Insert()
        
            ELSE
                if func:silent = false ! Silent mode - don't display messages
                    if glo:webjob then
                        Case Missive('Unable to source a temporary directory on the web server. Please report this error to the IT Department.'&|
                          '<13,10>'&|
                          '<13,10>A paper report will now be prepared.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    ELSE
                        Case Missive('Unable to souce a temporary directory on your computer. Please setup a temporary directory for Windows to use.'&|
                          '<13,10>'&|
                          '<13,10>A paper report will now be prepared.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    END
                end
                !Can't get temp file - print a paper file
                tmp:paper = 1
                ShowEmpty = true
            END
        ELSE
            ShowEmpty=true
        END !if not paper report
        !end of added 3/12/02 - L386 - allow export to CSV file - more at bottom of proc
        
        Clear(TotalGroup)
        
        Prog.Init(1000) ! #13150 Pick a random value (DBH: 29/08/2013)
        Prog.ProgressText('Checking Jobs..')
        
        ! Change --- Add "accepted" option (DBH: 02/04/2009) #10760
        !If f:DateRangeType = 1
        ! To --- (DBH: 02/04/2009) #10760
        if (f:DateRangeType = 1 or f:DateRangeType = 2)
        ! end --- (DBH: 02/04/2009) #10760
            If glo:WebJob
                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = Clarionet:Global.Param2
                If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                    !Found
                Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        
                if (f:DateRangeType = 1)
                    Access:JOBSWARR.Clearkey(jow:SubmittedBranchKey)
                    jow:BranchID = tra:BranchIdentification
                    jow:ClaimSubmitted = tmp:StartDate
                    Set(jow:SubmittedBranchKey,jow:SubmittedBranchKey)
                elsif (f:DateRangeType = 2)
                    Access:JOBSWARR.Clearkey(jow:AcceptedBranchKey)
                    jow:BranchID = tra:BranchIdentification
                    jow:DateAccepted = tmp:StartDate
                    Set(jow:AcceptedBranchKey,jow:AcceptedBranchKey)
                end ! if (f:DateRangeType = 1)
            Else ! If glo:WebJob
                if (f:DateRangeType = 1)
                    Access:JOBSWARR.Clearkey(jow:ClaimSubmittedKey)
                    jow:ClaimSubmitted = tmp:StartDate
                    Set(jow:ClaimSubmittedKey,jow:ClaimSubmittedKey)
                elsif (f:DateRangeType = 2)
                    Access:JOBSWARR.Clearkey(jow:DateAcceptedKey)
                    jow:DateAccepted = tmp:StartDate
                    Set(jow:DateAcceptedKey,jow:DateAcceptedKey)
                end ! if (f:DateRangeType = 1)
            End ! If glo:WebJob
            Loop ! Begin Loop
                If Access:JOBSWARR.Next()
                    Break
                End ! If Access:JOBSWARR.Next()
                If jow:ClaimSubmitted > tmp:EndDate
                    Break
                End ! If jow:ClaimSubmitted <> tmp:StartDate
                If glo:WebJob
                    If jow:BranchID <> tra:BranchIdentification
                        Break
                    End ! If jow:BranchID <> tra:BranchIdentification
                End ! If glo:WebJob
        
                IF (Prog.InsideLoop())
                    BREAK
                END ! IF
        
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = jow:RefNumber
                If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                    !Found
                Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                    ! Insert --- Make sure the job exists (DBH: 17/08/2009) #11033
                    cycle
                    ! end --- (DBH: 17/08/2009) #11033
                End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        
                If job:Warranty_Job <> 'YES'
                    Cycle
                End !If job:Warranty_Job <> 'YES'
                ! Insert --- Make sure the job exists (DBH: 17/08/2009) #11033
                if (job:Cancelled = 'YES')
                    cycle
                end ! if (job:Cancelled = 'YES')
                ! end --- (DBH: 17/08/2009) #11033
        
                If ~tmp:AllAccounts
                    Sort(glo:Queue,glo:Pointer)
                    glo:Pointer = job:Account_Number
                    Get(glo:Queue,glo:Pointer)
                    If Error()
                        Cycle
                    End !If Error()
                End !If ~tmp:AllAccounts
        
                If ~tmp:AllChargeTypes
                    Sort(glo:Queue2,glo:Pointer2)
                    glo:Pointer2    = job:Warranty_Charge_Type
                    Get(glo:Queue2,glo:Pointer2)
                    If Error()
                        Cycle
                    End !If Error()
                End !If ~tmp:AllChargeTypes
        
                If ~tmp:AllManufacturers
                    Sort(glo:Queue3,glo:Pointer3)
                    glo:Pointer3    = job:Manufacturer
                    Get(glo:Queue3,glo:Pointer3)
                    If Error()
                        Cycle
                    End !If Error()
                End !If ~tmp:AllManufacturers
        
                If ~func:AllStatus
                    Case jobe:WarrantyClaimStatus
                        Of 'SUBMITTED'
                            If ~func:Sub
                                Cycle
                            End !If ~func:Sub
        
                        Of 'RESUBMITTED'
                            If ~func:Resub
                                Cycle
                            End !If ~func:Resub
        
                        Of 'REJECTED'
                            If ~func:Rej
                                Cycle
                            End !If ~func:Rej
        
                        Of 'REJECTION ACKNOWLEDGED' OrOf 'FINAL REJECTION'
                            If ~func:RejAck
                                Cycle
                            End !If ~func:RejAck
        
                        Of 'RECONCILED'
                            If ~func:Rec
                                Cycle
                            End !If ~func:Rec
        
                    End !Case jobe:WarrantyClaimStatus
        
                End !If ~func:AllStatus
        
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    If glo:WebJob
                        If wob:HeadAccountNumber <> Clarionet:Global.Param2
                            Cycle
                        End !If wob:HeadAccountNumber <> Clarionet:Global.Param2
        
                    Else !If glo:WebJob
                        If ~func:ARCJobs
                            If wob:HeadAccountNumber = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                                Cycle
                            End !If wob:HeadAccountNumber <> GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                        End ! If ~func:ARCJobs
                    End !If glo:WebJob
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                    ! Insert --- Make sure the job exists (DBH: 17/08/2009) #11033
                    cycle
                    ! end --- (DBH: 17/08/2009) #11033
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
                number:Manufacturer = job:Manufacturer
                number:ModelNumber  = job:Model_Number
                number:JobNumber    = job:Ref_Number
                number:DateCompleted    = job:Date_Completed
                Add(NumberQueue)
        
        
            End ! Loop
        
        Else ! If f:DateRangeType = 1
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:DateCompletedKey)
            job:Date_Completed = tmp:StartDate
            Set(job:DateCompletedKey,job:DateCompletedKey)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Date_Completed > tmp:EndDate      |
                    Then Break.  ! End If
                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop()
        
                If job:Warranty_Job <> 'YES'
                    Cycle
                End !If job:Warranty_Job <> 'YES'
                ! Insert --- Make sure the job exists (DBH: 17/08/2009) #11033
                if (job:Cancelled = 'YES')
                    cycle
                end ! if (job:Cancelled = 'YES')
                ! end --- (DBH: 17/08/2009) #11033
        
        
                If ~tmp:AllAccounts
                    Sort(glo:Queue,glo:Pointer)
                    glo:Pointer = job:Account_Number
                    Get(glo:Queue,glo:Pointer)
                    If Error()
                        Cycle
                    End !If Error()
                End !If ~tmp:AllAccounts
        
                If ~tmp:AllChargeTypes
                    Sort(glo:Queue2,glo:Pointer2)
                    glo:Pointer2    = job:Warranty_Charge_Type
                    Get(glo:Queue2,glo:Pointer2)
                    If Error()
                        Cycle
                    End !If Error()
                End !If ~tmp:AllChargeTypes
        
                If ~tmp:AllManufacturers
                    Sort(glo:Queue3,glo:Pointer3)
                    glo:Pointer3    = job:Manufacturer
                    Get(glo:Queue3,glo:Pointer3)
                    If Error()
                        Cycle
                    End !If Error()
                End !If ~tmp:AllManufacturers
        
                If ~func:AllStatus
                    Case jobe:WarrantyClaimStatus
                        Of 'SUBMITTED'
                            If ~func:Sub
                                Cycle
                            End !If ~func:Sub
        
                        Of 'RESUBMITTED'
                            If ~func:Resub
                                Cycle
                            End !If ~func:Resub
        
                        Of 'REJECTED'
                            If ~func:Rej
                                Cycle
                            End !If ~func:Rej
        
                        Of 'REJECTION ACKNOWLEDGED' OrOf 'FINAL REJECTION'
                            If ~func:RejAck
                                Cycle
                            End !If ~func:RejAck
        
                        Of 'RECONCILED'
                            If ~func:Rec
                                Cycle
                            End !If ~func:Rec
        
                    End !Case jobe:WarrantyClaimStatus
        
                End !If ~func:AllStatus
        
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    If glo:WebJob
                        If wob:HeadAccountNumber <> Clarionet:Global.Param2
                            Cycle
                        End !If wob:HeadAccountNumber <> Clarionet:Global.Param2
        
                    Else !If glo:WebJob
                        If ~func:ARCJobs
                            If wob:HeadAccountNumber = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                                Cycle
                            End !If wob:HeadAccountNumber <> GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                        End ! If ~func:ARCJobs
                    End !If glo:WebJob
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                    ! Insert --- Make sure the job exists (DBH: 17/08/2009) #11033
                    cycle
                    ! end --- (DBH: 17/08/2009) #11033
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
                if (fExc)
                    ! #11505 Only include excluded jobs. Bouncers and exluced repair types only (DBH: 03/08/2010)
        
                    if (job:Bouncer = 'X')
                        ! Include Job
                    else
                        Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer = job:Manufacturer
                        rtd:Warranty = 'YES'
                        rtd:Repair_Type = job:Repair_Type_Warranty
                        If (Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey))
                            Cycle
                        Else
                            if (rtd:BER = 7)
                                ! Include Job
                            Else
                                Cycle
                            end
                        end
        
                    end
                End
        
                number:Manufacturer = job:Manufacturer
                number:ModelNumber  = job:Model_Number
                number:JobNumber    = job:Ref_Number
                number:DateCompleted    = job:Date_Completed
                Add(NumberQueue)
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
        
        End ! If f:DateRangeType = 1
        
        if (f:DateRangeType <> 2 and fExc = 0)
            ! #11422 Don't include submitted jobs in the "Accepted" date range is used. (DBH: 17/06/2010)
        
            ! Inserting (DBH 07/09/2007) # 8708 - Find resbumitted jobs in the date range
            Prog.ProgressText('Checking Resubmitted Jobs..')
            Access:AUDIT.Clearkey(aud:ActionOnlyKey)
            aud:Action = 'WARRANTY CLAIM RESUBMITTED'
            aud:Date = tmp:StartDate
            Set(aud:ActionOnlyKey,aud:ActionOnlyKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Action <> 'WARRANTY CLAIM RESUBMITTED'
                    Break
                End ! If aud:Action <> 'WARRANTY CLAIM RESUBMITTED'
                If aud:Date > tmp:EndDate
                    Break
                End ! If aud:Date > tmp:EndDate
        
                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop()
        
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = aud:Ref_Number
                If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                    !Found
                Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                    Cycle
                End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                ! Insert --- Make sure the job exists (DBH: 17/08/2009) #11033
                if (job:Cancelled = 'YES')
                    cycle
                end ! if (job:Cancelled = 'YES')
                ! end --- (DBH: 17/08/2009) #11033
        
        
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    If glo:WebJob
                        If wob:HeadAccountNumber <> Clarionet:Global.Param2
                            Cycle
                        End !If wob:HeadAccountNumber <> Clarionet:Global.Param2
                    Else !If glo:WebJob
                        If wob:HeadAccountNumber = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                            Cycle
                        End !If wob:HeadAccountNumber <> GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
            !            If wob:HeadAccountNumber <> GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
            !                Cycle
            !            End !If wob:HeadAccountNumber <> GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                    End !If glo:WebJob
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                    Cycle
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                    !Found
                Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                    Cycle
                End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        
                If job:Warranty_Job <> 'YES'
                    Cycle
                End ! If job:Warranty_Job <> 'YES'
        
                If ~tmp:AllAccounts
                    Sort(glo:Queue,glo:Pointer)
                    glo:Pointer = job:Account_Number
                    Get(glo:Queue,glo:Pointer)
                    If Error()
                        Cycle
                    End !If Error()
                End !If ~tmp:AllAccounts
        
                If ~tmp:AllChargeTypes
                    Sort(glo:Queue2,glo:Pointer2)
                    glo:Pointer2    = job:Warranty_Charge_Type
                    Get(glo:Queue2,glo:Pointer2)
                    If Error()
                        Cycle
                    End !If Error()
                End !If ~tmp:AllChargeTypes
        
                If ~tmp:AllManufacturers
                    Sort(glo:Queue3,glo:Pointer3)
                    glo:Pointer3    = job:Manufacturer
                    Get(glo:Queue3,glo:Pointer3)
                    If Error()
                        Cycle
                    End !If Error()
                End !If ~tmp:AllManufacturers
        
                If ~func:AllStatus
                    Case jobe:WarrantyClaimStatus
                        Of 'SUBMITTED'
                            If ~func:Sub
                                Cycle
                            End !If ~func:Sub
        
                        Of 'RESUBMITTED'
                            If ~func:Resub
                                Cycle
                            End !If ~func:Resub
        
                        Of 'REJECTED'
                            If ~func:Rej
                                Cycle
                            End !If ~func:Rej
        
                        Of 'REJECTION ACKNOWLEDGED' OrOf 'FINAL REJECTION'
                            If ~func:RejAck
                                Cycle
                            End !If ~func:RejAck
        
                        Of 'RECONCILED'
                            If ~func:Rec
                                Cycle
                            End !If ~func:Rec
        
                    End !Case jobe:WarrantyClaimStatus
        
                End !If ~func:AllStatus
        
                ! Add job number if it already hasn't been found (DBH: 07/09/2007)
                number:JobNumber        = job:Ref_Number
                Get(NumberQueue,number:JobNumber)
                If Error()
                    number:Manufacturer     = job:Manufacturer
                    number:ModelNumber      = job:Model_Number
                    number:JobNumber        = job:Ref_Number
                    number:DateCompleted    = job:Date_Completed
                    Add(NumberQueue)
                End ! If Error()
            End ! Loop
            ! End (DBH 07/09/2007) #8708
        end !if (f:DateRangeType <> 2)
        
        Prog.ProgressFinish()
        
        Prog.Init(Records(NumberQueue))
        Prog.ProgressText('Building Report..')
        
        
        Case tmp:ReportOrder
            Of 0
                Sort(NumberQueue,number:Manufacturer,number:ModelNumber,number:JobNumber)
            Of 1
                Sort(NumberQueue,number:JobNumber)
            Of 2
                Sort(NumberQueue,number:DateCompleted)
        End !tmp:ReportType
        
        RepQ.TotalRecords = RECORDS(NumberQueue)
        
        Loop x# = 1 To Records(NumberQueue)
            Get(NumberQueue,x#)
        
            IF (RepQ.SBOReport)
                IF (RepQ.UpdateProgress())
                    BREAK
                END!  IF
            END ! IF
        
            If Prog.InsideLoop()
                Break
            End !If Prog.InsideLoop()
        
            Clear(ReportGroup)
        
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = number:JobNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
        
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
        
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
            Access:JOBSWARR.ClearKey(jow:RefNumberKey)
            jow:RefNumber = job:Ref_Number
            If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                !Error
            End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
        
        
            !Filter
            If func:IncludeARC = False
                If jobe:WebJob And SentToHub(job:Ref_Number) = False
        
                Else ! If jobe:WebJob And SentToHub(job:Ref_Number) = False
                    Cycle
                End ! If jobe:WebJob And SentToHub(job:Ref_Number) = False
            End ! If func:IncludeARC = False
        
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found
        
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number  = job:Account_Number
            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Found
                report:CompanyName = sub:Company_Name
            Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        
            If glo:WebJob OR func:arc = TRUE
                If job:Exchange_Unit_Number <> 0
                    If jobe:ExchangedAtRRC
                        report:HandlingFee  = jobe:ExchangeRate
                    End !If jobe:ExchangedAtRRC
                End !If job:Exchange_Unit_Number <> 0
                If ~SentToHub(job:Ref_Number)
                !Changed by Paul - 27/07/2009 Log No 10948
        !            If job:Invoice_Number_Warranty <> 0
        !                Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
        !                inv:Invoice_Number  = job:Invoice_Number_Warranty
        !                If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        !                    !Found
        !                    report:PartsSelling    = jobe:InvRRCWPartsCost
        !                    report:Labour   = jobe:InvRRCWLabourCost
        !                    report:VAT      = jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts/100 + |
        !                                        jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour/100 +|
        !                                        job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100
        !                    report:Total    = report:PartsSelling + report:Labour + report:VAT + job:WInvoice_Courier_Cost
        !                Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        !                    !Error
        !                    Cycle
        !                End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        !
        !            Else !If job:Invoice_Number_Warranty <> 0
                !end of change
        
                        report:PartsSelling    = jobe:RRCWPartsCost
                        report:Labour   = jobe:RRCWLabourCost
                        report:VAT      = jobe:RRCWPartsCost * VatRate(job:Account_Number,'L')/100 + |
                                            jobe:RRCWLabourCost * VatRate(job:Account_Number,'P')/100 + |
                                            job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100
                        report:Total    = report:PartsSelling + report:Labour + Report:VAT + job:Courier_Cost_Warranty
        !            End !If job:Invoice_Number_Warranty <> 0
                Else
                    report:HandlingFee += jobe:HandlingFee
                End !If ~SentToHub(job:Ref_Number)
            Else !If glo:WebJob
                If job:Invoice_Number_Warranty <> 0
                    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                    inv:Invoice_Number  = job:Invoice_Number_Warranty
                    If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                        !Found
                        report:PartsSelling   = job:WInvoice_Parts_Cost
                        report:Labour   = jobe:InvoiceClaimValue !job:WInvoice_Labour_Cost
                        report:VAT      = job:WInvoice_Parts_Cost * inv:Vat_Rate_Labour/100 + |
                                            jobe:InvoiceClaimValue * inv:Vat_Rate_Parts/100 + |
                                            job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100
                        report:Total    = report:PartsSelling + report:Labour + report:VAT + job:WInvoice_Courier_Cost
                    Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                        !Error
                    End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                Else !If job:Invoice_Number_Warranty <> 0
                    report:PartsSelling    = job:Parts_Cost_Warranty
                    report:Labour   = jobe:ClaimValue
                    report:VAT      = job:Parts_Cost_Warranty * VatRate(job:Account_Number,'L')/100 + |
                                        jobe:ClaimValue * VatRate(job:Account_Number,'P')/100 + |
                                        job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100
                    report:Total    = report:PartsSelling + report:Labour + report:VAT + job:Courier_Cost_Warranty
                End !If job:Invoice_Number_Warranty <> 0
            End !If glo:WebJob
        
            report:Parts = 0
            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_Number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                ! Inserting (DBH 03/04/2007) # 8883 - Do not cost the part if it is a correction
                If wpr:Correction
                    Cycle
                End ! If wpr:Correction
                ! End (DBH 03/04/2007) #8883
        
                If glo:WebJOb OR func:arc = TRUE
                   IF ~SenttoHub(job:Ref_Number)
        ! Changing (DBH 08/08/2006) # 8091 - We are not talking the quantity of parts into account
        !              report:Parts += wpr:RRCAveragePurchaseCost !wpr:RRCPurchaseCost
        ! to (DBH 08/08/2006) # 8091
                        report:Parts += (wpr:RRCAveragePurchaseCost * wpr:Quantity)
        ! End (DBH 08/08/2006) #8091
                   END
                Else !If glo:WebJOb
        ! Changing (DBH 08/08/2006) # 8091 - We are not taking the quantity of parts into account
        !            report:Parts += wpr:AveragePurchaseCost
        ! to (DBH 08/08/2006) # 8091
                    report:Parts += (wpr:AveragePurchaseCost * wpr:Quantity)
        ! End (DBH 08/08/2006) #8091
                End !If glo:WebJOb
            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)
        
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = wob:HeadAccountNumber
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                 report:JobNumber     = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
            if jobe:WebJob AND sentToHub(job:ref_number) = FALSE
                report:Repair = 'RRC'
            else
                report:Repair = 'ARC'
            end
        
            if job:Exchange_Unit_Number = 0
                report:Exchanged = ''
            else
                if jobe:ExchangedATRRC
                    report:Exchanged = 'RRC'
                else
                    report:Exchanged = 'ARC'
                end
            end
        
            report:WarrantyStatus = ''
            report:RejectionBy = ''
        
            case wob:EDI
                of 'XXX'
                    report:WarrantyStatus = 'EXCLUDED'
                of 'NO'
                    report:WarrantyStatus = 'PENDING'
                of 'YES'
                    report:WarrantyStatus = 'IN PROGRESS'
                of 'EXC'
                    report:WarrantyStatus = 'REJECTED'
                of 'PAY'
                    report:WarrantyStatus = 'PAID'
                of 'APP'
                    report:WarrantyStatus = 'APPROVED'
                of 'AAJ'
                    ! Changing (DBH 15/02/2006) #7158 - Change text
                    ! report:WarrantyStatus = 'ACCEPTED-REJECTED'
                    ! to (DBH 15/02/2006) #7158
                    report:WarrantyStatus = 'ACCEPTED-REJECTION'
                    ! End (DBH 15/02/2006) #7158
                    Save_aud_ID = Access:AUDIT.SaveFile()
                    Access:AUDIT.ClearKey(aud:TypeActionKey)
                    aud:Ref_Number = job:ref_Number
                    aud:Type       = 'JOB'
                    aud:Action     = 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
                    Set(aud:TypeActionKey,aud:TypeActionKey)
                    Loop
                        If Access:AUDIT.NEXT()
                           Break
                        End !If
                        If aud:Ref_Number <> job:Ref_Number      |
                        Or aud:Type       <> 'JOB'      |
                        Or aud:Action     <> 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'      |
                            Then Break.  ! End If
                        Access:USERS.Clearkey(use:user_code_Key)
                        use:User_Code   = aud:User
                        If Access:USERS.Tryfetch(use:user_code_Key) = Level:Benign
                            !Found
                            report:RejectionBy = Clip(use:Forename) & ' ' & Clip(use:Surname)
                            Break
                        Else ! If Access:USERS.Tryfetch(use:user_code_Key) = Level:Benign
                            !Error
                        End !If Access:USERS.Tryfetch(use:user_code_Key) = Level:Benign
                    End !Loop
                    Access:AUDIT.RestoreFile(Save_aud_ID)
        
                Of 'REJ'
                    report:WarrantyStatus = 'FINAL REJECTION'
            end
        
        
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_code   = job:engineer
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Found
                report:Engineer = Clip(use:Forename) & ' '  & Clip(use:Surname)
            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Error
                report:Engineer = ''
            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        
            !Suppress Zeros
            If glo:Select21 = 1
                If report:Total = 0
                    Cycle
                End !If report:Total = 0
            End !If glo:Select21 = 1
        
            report:Manufacturer = job:Manufacturer
            report:ModelNumber  = job:Model_Number
            report:ChargeType   = job:Warranty_Charge_Type
            report:CompletedDate    = job:Date_Completed
        
            !Add totals
            total:Lines             += 1
            total:HandlingFee       += report:HandlingFee
            total:Labour            += report:Labour
            total:Parts             += report:Parts
            total:PartsSelling      += report:PartsSelling
            total:SubTotal          += (report:PartsSelling + report:Labour)
            total:VAT               += report:Vat
            total:Total             += report:Total
        
            !added 3/12/02 - L386 - allow export to CSV file
            !Output all calculations - was just print(rpt:detail)
            if tmp:Paper then
                Print(rpt:Detail)
            ELSE
                !Export to file
                Clear(gen:Record)
                !Add Engineer Option - 3788 (DBH: 19-04-2004)
                Case jobe:Engineer48HourOption
                    Of 1
                        tmp:EngineerOption = '48H'
                    Of 2
                        tmp:EngineerOption = 'ARC'
                    Of 3
                        tmp:EngineerOption = '7DT'
                    Of 4
                        tmp:EngineerOption = 'STD'
                    Else
                        tmp:EngineerOption = ''
                End !Case jobe:Engineer48HourOption
                !Job Number
                gen:Line1   = clip(job:ref_number)
                ! Deleted (DBH 15/02/2006) #7158 - Remove column
                ! !Engineer Option
                !         gen:Line1   = Clip(gen:Line1) & ',' & clip(tmp:EngineerOption)
                ! End (DBH 15/02/2006) #7158
                !Branch ID
                gen:Line1   = Clip(gen:Line1) & ',' & clip(tra:BranchIdentification)
                !Webjob Number
                gen:Line1   = Clip(gen:Line1) & ',' & clip(wob:JobNumber)
                !Completed Date
                gen:Line1   = Clip(gen:Line1) & ',' & clip(format(report:CompletedDate,@d6))
                !Account Number
                gen:Line1   = Clip(gen:Line1) & ',' & clip(tra:Account_Number)
                !Account Name
                gen:Line1   = Clip(gen:Line1) & ',' & clip(tra:Company_Name)
                !Account Number
                gen:Line1   = Clip(gen:Line1) & ',' & clip(job:Account_Number)
                !Company Name
                gen:Line1   = Clip(gen:Line1) & ',' & clip(StripComma(job:Company_Name))
                !Manufacturer
                gen:Line1   = Clip(gen:Line1) & ',' & clip(report:Manufacturer)
                !Model Number
                gen:Line1   = Clip(gen:Line1) & ',' & clip(report:ModelNumber)
                !Charge TYpe
                gen:Line1   = Clip(gen:Line1) & ',' & clip(report:ChargeType)
                !Repair Type
                gen:Line1   = Clip(gen:Line1) & ',' & clip(job:Repair_Type_Warranty)
                ! Deleted (DBH 15/02/2006) #7158 - Moved to end
                ! !Warranty Status
                !         gen:Line1   = Clip(gen:Line1) & ',' & clip(report:WarrantyStatus)
                !         !Rejection By
                !         gen:Line1   = Clip(gen:Line1) & ',' & Clip(report:RejectionBy)
                ! End (DBH 15/02/2006) #7158
                !Engineer
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(report:Engineer)
                !Repair
                gen:Line1   = Clip(gen:Line1) & ',' & clip(report:Repair)
                !Exchanged
                gen:Line1   = Clip(gen:Line1) & ',' & clip(report:Exchanged)
                !Handling Fee
                gen:Line1   = Clip(gen:Line1) & ',' & clip(Format(report:HandlingFee,@n_10.2))
                !Parts Cost
                gen:Line1   = Clip(gen:Line1) & ',' & clip(Format(report:Parts,@n_10.2))
                !Parts Selling Cost
                gen:Line1   = Clip(gen:Line1) & ',' & clip(Format(report:PartsSelling,@n_10.2))
                !Labour Cost
                gen:Line1   = Clip(gen:Line1) & ',' & clip(Format(report:Labour,@n_10.2))
                ! Inserting (DBH 15/02/2006) #7158 - New Column
                !Sub Total
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(Format(report:Labour + report:PartsSelling,@n_10.2))
                ! End (DBH 15/02/2006) #7158
                !VAT
                gen:Line1   = Clip(gen:Line1) & ',' & clip(FORMAT(report:Vat,@n_10.2))
                !Total
                gen:Line1   = Clip(gen:Line1) & ',' & clip(FORMAT(report:Total,@n_10.2))
                ! Inserting (DBH 15/02/2006) #7158 - New columns
                !Warranty Status
                gen:Line1   = Clip(gen:Line1) & ',' & clip(report:WarrantyStatus)
        
                !Rejection Reasons
                CountReasons# = 0
                Save_aud_ID = Access:AUDIT.SaveFile()
                Access:AUDIT.ClearKey(aud:TypeActionKey)
                aud:Ref_Number = job:ref_Number
                aud:Type       = 'JOB'
                aud:Action     = 'WARRANTY CLAIM REJECTED'
                Set(aud:TypeActionKey,aud:TypeActionKey)
                Loop
                    If Access:AUDIT.NEXT()
                       Break
                    End !If
                    If aud:Ref_Number <> job:Ref_Number      |
                    Or aud:Type       <> 'JOB'      |
                    Or aud:Action     <> 'WARRANTY CLAIM REJECTED'      |
                        Then Break.  ! End If
                    CountReasons# += 1
                    If CountReasons# > 3
                        Break
                    End ! If CountReasons# >=3
                    Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                    aud2:AUDRecordNumber = aud:Record_Number
                    IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                        gen:Line1 = Clip(gen:Line1) & ',' & Clip(BHStripNonAlphaNum(BHStripReplace(Sub(aud2:Notes,9,255),',',''),' '))
                    END ! IF
                    
                End !Loop
                Access:AUDIT.RestoreFile(Save_aud_ID)
        
                CountReasons# += 1
                If CountReasons# <= 3
                    Loop loop# = CountReasons# To 3
                        gen:Line1 = Clip(gen:Line1) & ','
                    End ! Loop loop# = CountReasons# To 3
                End ! If CountReasons# < 3
        
                !Rejection By
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(report:RejectionBy)
                ! End (DBH 15/02/2006) #7158
                gen:Line1 = Clip(gen:Line1) & ',' & Format(jow:ClaimSubmitted,@d06)
        
                Access:EXPGEN.Insert()
            END
            !end of - added 3/12/02 - L386 - allow export to CSV file
        End !x# = 1 To Records(NumberQueue)
        
        Prog.ProgressFinish()
        
        
        !added 3/12/02 - L386 - allow export to CSV file
        if tmp:paper
            !all done and finished
        ELSE
            !Print the final lines
            Clear(gen:Record)
            gen:Line1   = 'TOTALS'
            Access:EXPGEN.Insert()
        
            Clear(gen:Record)
            gen:Line1   = 'Jobs Counted,'&clip(Total:lines)&',,,,,,,,,,,,,,'&clip(Format(Total:handlingFee,@n_10.2))&','&clip(Format(total:parts,@n_10.2))&','&|
                          clip(Format(total:partsSelling,@n_10.2))&','&clip(Format(Total:labour,@n_10.2))&',' & Clip(Format(total:SubTotal,@n_10.2)) & ',' & clip(Format(Total:vat,@n_10.2))&','&clip(Format(total:total,@n_10.2))
            Access:EXPGEN.Insert()
        
            Access:EXPGEN.close()
            IF (RepQ.SBOReport)
                IF (RepQ.FinishReport('Warranty Income Report.csv',glo:File_Name))
                END ! IF
            
            ELSE ! IF (RepQ.SBOReport)
        
                if total:lines = 0 then
                    if func:silent = false
                        Case Missive('There are no records that match the selected criteria.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    end
                Else
        
                        
                    if glo:webjob then
                         !send the file to the client
                         SendFileToClient(glo:File_Name)
                    ELSe
                         !copy it to where it is expected
                         copy(glo:file_name,Glo:CSV_SavePath)
                    END
                    if func:silent = false
                        Case Missive('Warranty Income Report Created.','ServiceBase 3g',|
                                       'midea.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    end
                END ! IF (RepQ.SBOReport)
            END !If total:lines = 0
        
            !remove the file
            
            Remove(glo:File_name)
        
        END!if tmp:paper
        !end of - added 3/12/02 - L386 - allow export to CSV file
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF ShowEmpty = TRUE
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'c:\REPORT.TXT'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
        IF ShowEmpty = TRUE
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF ShowEmpty = TRUE
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
        IF ShowEmpty = TRUE
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','WarrantyIncomeReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:JOBPAYMT.Close
    Relate:PAYTYPES.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Before Embed Point: %EndOfProcedure) DESC(End of Procedure) ARG()
  If tmp:Paper
  
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  End ! tmp:Paper
  ! After Embed Point: %EndOfProcedure) DESC(End of Procedure) ARG()
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
    IF (RepQ.Init(COMMAND()) = Level:Benign)
        IF (local.LoadSBOnlineCriteria())
            RETURN
        END ! IF
        0{prop:Hide} = 1
    END ! IF
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
  tmp:AllAccounts = func:AllAccounts
  tmp:AllChargeTypes  = func:AllChargeTypes
  tmp:AllManufacturers    = func:AllManufacturers
  tmp:ReportOrder = func:ReportOrder
  tmp:Paper       = func:paper
  
  tmp:DateRange   = Format(tmp:StartDate,@d6) & ' to ' & Format(tmp:EndDate,@d6)
  
  If ~glo:WebJob
      address:CompanyName     = def:User_Name
      address:AddressLine1    = def:Address_Line1
      address:AddressLine2    = def:Address_Line2
      address:AddressLine3    = def:Address_Line3
      address:Postcode        = def:Postcode
      address:TelephoneNumber = def:Telephone_Number
      address:FaxNumber       = def:Fax_Number
      address:EmailAddress    = def:EmailAddress
  Else !glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          address:CompanyName     = tra:Company_Name
          address:AddressLine1    = tra:Address_Line1
          address:AddressLine2    = tra:Address_Line2
          address:AddressLine3    = tra:Address_Line3
          address:Postcode        = tra:Postcode
          address:TelephoneNumber = tra:Telephone_Number
          address:FaxNumber       = tra:Fax_Number
          address:EmailAddress    = tra:EmailAddress
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  End !glo:WebJob
  
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='WarrantyIncomeReport'
  END
  report{Prop:Preview} = PrintPreviewImage


! Before Embed Point: %DBHProcedureSection) DESC(Procedure Local * (Place Local Procedure Code Here When Using Bryan's Progress Bar) *) ARG()
local.LoadSBOnlineCriteria        PROCEDURE()!,LONG
RetValue                        LONG(Level:Benign)
i                               LONG
qXML                            QUEUE(),PRE(qXml)
ReportOrder                         CSTRING(3)
ReportType                          CSTRING(3)
GenericAccounts                     CSTRING(3)
AllAccounts                         CSTRING(3)
AllChargeTypes                      CSTRING(3)
AllManufacturers                    CSTRING(3)
StartDate                           CSTRING(20)
EndDate                             CSTRING(20)
ZeroSuppression                     CSTRING(3)
AllWarrantyStatus                   CSTRING(3)
Submitted                           CSTRING(3)
Resubmitted                         CSTRING(3)
Rejected                            CSTRING(3)
RejectionAcknowledged               CSTRING(3)
Reconciled                          CSTRING(3)
ExcludedOnly                        CSTRING(3)
DateRangeType                       CSTRING(3)
IncludeARCRepairedJobs              CSTRING(3)
                                END! QUEUE
qAccounts       QUEUE(),PRE(qAccounts) 
AccountNumber       CSTRING(31)
                END!  QUEUE                               
qChargeTypes    QUEUE(),PRE(qChargeTypes)        
ChargeType          CSTRING(31)
                END! QUEUE
qManufacturers  QUEUE(),PRE(qManufacturers)    
Manufacturer        CSTRING(31)
                END ! QUEUE
    CODE
        LOOP 1 TIMES
            glo:WebJob = 1
            
            IF (XML:LoadFromFile(RepQ.FullCriteriaFilename))
                RetValue = Level:Fatal
                BREAK
            END ! IF
            XML:GotoTop

            IF (~XML:FindNextNode('Defaults'))
                recs# = XML:LoadQueue(qXML,1,1)
                IF (RECORDS(qXML) = 0)
                    RetValue = Level:Fatal
                    BREAK
                END ! IF
                GET(qXML, 1)
                !XML:DebugMyQueue(qXML,'qXML')
                func:StartDate = qXML.StartDate
                func:EndDate = qXML.EndDate
                func:AllAccounts = qXML.AllAccounts
                func:AllChargeTypes = qXML.AllChargeTypes
                func:AllManufacturers = qXML.AllManufacturers
                func:ReportOrder = qXML.ReportOrder
                func:Paper = 0
                f:DateRangeType = qXML.DateRangeType
                func:AllStatus = qXML.AllWarrantyStatus
                func:Sub = qXML.Submitted
                func:Resub = qXML.Resubmitted
                func:Rej = qXML.Rejected
                func:Rejack = qXML.RejectionAcknowledged
                func:Rec = qXML.Reconciled
                func:ARCJobs = qXML.IncludeARCRepairedJobs
                fExc = qXML.ExcludedOnly
                func:Silent = 1


                IF (qXML.AllAccounts <> 1)
                    recs# = XML:LoadQueue(qAccounts)
                    IF (RECORDS(qAccounts) > 0)
                        LOOP i = 1 TO RECORDS(qAccounts)
                            GET(qAccounts, i)
                            glo:Pointer = qAccounts.AccountNumber
                            ADD(glo:Queue)
                        END ! LOOP
                    END !I F
                END ! IF
                
                XML:Free()
            END ! IF
            
            

            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = RepQ.UserCode
            Access:USERS.TryFetch(use:User_Code_Key)
            Clarionet:Global.Param2 = RepQ.AccountNumber

            RepQ.UpdateStatus('Running')

!            DO Export

        END ! BREAK LOOP

        IF (RetValue = Level:Fatal)
            RepQ.Failed()
        END !I F

        RETURN RetValue
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: UnivAbcReport
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE
! Before Embed Point: %DBHProgressSetupCode1) DESC(Progress Bar Setup Code Section) ARG()
    IF (RepQ.SBOReport)
        RETURN
    END ! IF
! After Embed Point: %DBHProgressSetupCode1) DESC(Progress Bar Setup Code Section) ARG()

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE
! Before Embed Point: %DBHProgressLoopCode1) DESC(Progress Bar Inside Loop Code Section) ARG()
    IF (RepQ.SBOReport)
        RETURN 0
    END ! IF
! After Embed Point: %DBHProgressLoopCode1) DESC(Progress Bar Inside Loop Code Section) ARG()

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 150
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE
! Before Embed Point: %DBHProgressFinishCode1) DESC(Progress Bar Progress Finish Code Section) ARG()
    IF (RepQ.SBOReport)
        RETURN
    END ! IF
! After Embed Point: %DBHProgressFinishCode1) DESC(Progress Bar Progress Finish Code Section) ARG()

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
! After Embed Point: %DBHProcedureSection) DESC(Procedure Local * (Place Local Procedure Code Here When Using Bryan's Progress Bar) *) ARG()





WarrantyIncomeReportCriteria PROCEDURE (func:AutoRun) !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::10:TAGDISPSTATUS   BYTE(0)
DASBRW::10:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer3                      LIKE(glo:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_sub_id          USHORT,AUTO
tmp:arc              BYTE
tmp:AccountTag       STRING(1)
tmp:PaymentTag       STRING(1)
tmp:ChargeTypeTag    STRING(1)
tmp:ChargeType       STRING(30)
tmp:WarrantyChargeType STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:includeARC       BYTE
tmp:AllAccounts      BYTE(1)
tmp:MainAccount      STRING(30)
tmp:True             BYTE(1)
tmp:AllChargeTypes   BYTE(1)
tmp:ReportOrder      BYTE(0)
tmp:No               STRING('NO {28}')
tmp:Yes              STRING('YES')
tmp:ManufacturerTag  STRING(1)
tmp:AllManufacturer  BYTE(1)
tmp:AllStatus        BYTE(1)
StatusTypes          GROUP,PRE()
tmp:Submitted        BYTE(0)
tmp:Resubmitted      BYTE(0)
tmp:Rejected         BYTE(0)
tmp:RejectionAcknowledged BYTE(0)
tmp:Reconciled       BYTE(0)
                     END
tmp:PaperReport      BYTE(1)
DOSDialogHeader      CSTRING(40)
DOSExtParameter      CSTRING(255)
DOSTargetVariable    CSTRING(1024)
tmp:SuppressZeros    BYTE(1)
tmp:IncludeARCJobs   BYTE(0)
tmp:DateRangeType    BYTE(0)
locExcludedOnly      BYTE
BRW5::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                       PROJECT(sub:Generic_Account)
                       PROJECT(sub:Main_Account_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:AccountTag         LIKE(tmp:AccountTag)           !List box control field - type derived from local data
tmp:AccountTag_Icon    LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
sub:Generic_Account    LIKE(sub:Generic_Account)      !Browse key field - type derived from field
sub:Main_Account_Number LIKE(sub:Main_Account_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:ChargeTypeTag      LIKE(tmp:ChargeTypeTag)        !List box control field - type derived from local data
tmp:ChargeTypeTag_Icon LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:ManufacturerTag    LIKE(tmp:ManufacturerTag)      !List box control field - type derived from local data
tmp:ManufacturerTag_Icon LONG                         !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Warranty Income Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Tab1'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select the Trade Accounts you wish to include'),AT(252,126),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All Accounts'),AT(433,126),USE(tmp:AllAccounts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Accounts'),TIP('All Accounts'),VALUE('1','0')
                           SHEET,AT(168,138,344,188),USE(?Sheet2),SPREAD
                             TAB('By Account Number'),USE(?Tab4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                             TAB('Generic Accounts'),USE(?Tab5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                           END
                           LIST,AT(172,154,236,168),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@67L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(249,156,5,5),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(249,176,5,5),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(428,188),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(428,220),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(428,250),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 6'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Charge Types To Include'),AT(233,126),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All Charge Types'),AT(369,126),USE(tmp:AllChargeTypes),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Charge Types'),TIP('All Charge Types'),VALUE('1','0')
                           LIST,AT(249,140,182,134),USE(?List:2),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(289,184,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(293,216,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(240,278),USE(?DASTAG:2),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,278),USE(?DASTAGAll:2),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,278),USE(?DASUNTAGALL:2),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 7'),USE(?Tab7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Manufacturer To Include'),AT(229,126),USE(?Prompt7),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All Manufacturers'),AT(369,126),USE(tmp:AllManufacturer),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Manufacturers'),TIP('All Manufacturers'),VALUE('1','0')
                           LIST,AT(252,138,180,134),USE(?List:3),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Rev tags'),AT(299,216,50,13),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(307,232,70,13),USE(?DASSHOWTAG:3),HIDE
                           BUTTON,AT(240,278),USE(?DASTAG:3),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,278),USE(?DASTAGAll:3),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,278),USE(?DASUNTAGALL:3),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 3'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Warranty Status'),AT(252,124),USE(?Prompt8),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All'),AT(252,136),USE(tmp:AllStatus),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All'),TIP('All'),VALUE('1','0')
                           OPTION('Report Type'),AT(248,264,256,20),USE(tmp:PaperReport),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Paper'),AT(252,272),USE(?tmp:PaperReport:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Electronic'),AT(340,272),USE(?tmp:PaperReport:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                           END
                           PROMPT('Path to Save File'),AT(172,292),USE(?SavePathPrompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(464,288),USE(?LookupFile),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(248,292,212,10),USE(glo:CSV_SavePath),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           GROUP,AT(280,132,200,30),USE(?StatusGroup),BOXED
                             CHECK('Submitted'),AT(288,138),USE(tmp:Submitted),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Submitted'),TIP('Submitted'),VALUE('1','0')
                             CHECK('Resubmitted'),AT(352,138),USE(tmp:Resubmitted),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Resubmitted'),TIP('Resubmitted'),VALUE('1','0')
                             CHECK('Rejected'),AT(420,138),USE(tmp:Rejected),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Rejected'),TIP('Rejected'),VALUE('1','0')
                             CHECK('Rejection Acknowledged'),AT(288,150),USE(tmp:RejectionAcknowledged),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Rejection Acknowledged'),TIP('Rejection Acknowledged'),VALUE('1','0')
                             CHECK('Reconciled'),AT(420,150),USE(tmp:Reconciled),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Reconciled'),TIP('Reconciled'),VALUE('1','0')
                           END
                           PROMPT('Select Date Range'),AT(252,166),USE(?Prompt4),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           OPTION('Date Range Type'),AT(168,164,76,76),USE(tmp:DateRangeType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Completed'),AT(172,178),USE(?tmp:DateRangeType:Radio6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Submitted'),AT(172,192),USE(?tmp:DateRangeType:Radio7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Accepted'),AT(172,206),USE(?tmp:DateRangeType:Radio8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           PROMPT('Start Date'),AT(252,178),USE(?tmp:StartDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(340,178,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(408,174),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(252,194),USE(?tmp:EndDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(340,194,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(408,190),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           CHECK(' Include ARC Repaired RRC Jobs'),AT(252,208),USE(tmp:includeARC),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Include ARC Jobs'),AT(424,208),USE(tmp:IncludeARCJobs),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Include ARC Jobs'),TIP('Include ARC Jobs'),VALUE('1','0')
                           OPTION('Zero Suppresion'),AT(248,220,256,20),USE(tmp:SuppressZeros),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Suppress Zeros'),AT(252,228),USE(?Option3:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Show Zeros'),AT(412,228),USE(?Option3:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                           END
                           CHECK('Excluded Only'),AT(172,224),USE(locExcludedOnly),FONT(,,,FONT:bold),VALUE('1','0')
                           OPTION('Report Order'),AT(248,242,256,20),USE(tmp:ReportOrder),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('By Manufacturer'),AT(252,250),USE(?tmp:ReportOrder:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('By Job Number'),AT(340,250),USE(?tmp:ReportOrder:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('By Date Completed'),AT(412,250),USE(?tmp:ReportOrder:Radio2:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                         END
                       END
                       BUTTON,AT(168,332),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(232,332),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Warranty Income Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

! Clarionet Moving Bar
CN:recordstoprocess     long,auto
CN:recordsprocessed     long,auto
CN:recordspercycle      long,auto
CN:recordsthiscycle     long,auto
CN:percentprogress      byte
CN:recordstatus         byte,auto

CN:progress:thermometer byte
CN:progresswindow WINDOW('Please Wait...'),AT(,,164,22),FONT('Arial',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(CN:progress:thermometer),AT(4,4,156,12),RANGE(0,100)
     END
tmp:DesktopFolder       CString(255)
RepQ       RepQClass
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

Wizard7         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet2)  = 2
BRW9                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW11                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                 !Default Locator
FileLookup16         SelectFileClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue.Pointer = sub:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:AccountTag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:AccountTag = ''
  END
    Queue:Browse.tmp:AccountTag = tmp:AccountTag
  IF (tmp:Accounttag = '*')
    Queue:Browse.tmp:AccountTag_Icon = 2
  ELSE
    Queue:Browse.tmp:AccountTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::6:QUEUE = glo:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sub:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::10:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW9.UpdateBuffer
   glo:Queue2.Pointer2 = cha:Charge_Type
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = cha:Charge_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:ChargeTypeTag = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:ChargeTypeTag = ''
  END
    Queue:Browse:1.tmp:ChargeTypeTag = tmp:ChargeTypeTag
  IF (tmp:ChargeTypeTag = '*')
    Queue:Browse:1.tmp:ChargeTypeTag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:ChargeTypeTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = cha:Charge_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::10:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::10:QUEUE = glo:Queue2
    ADD(DASBRW::10:QUEUE)
  END
  FREE(glo:Queue2)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::10:QUEUE.Pointer2 = cha:Charge_Type
     GET(DASBRW::10:QUEUE,DASBRW::10:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = cha:Charge_Type
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASSHOWTAG Routine
   CASE DASBRW::10:TAGDISPSTATUS
   OF 0
      DASBRW::10:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::10:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::10:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW11.UpdateBuffer
   glo:Queue3.Pointer3 = man:Manufacturer
   GET(glo:Queue3,glo:Queue3.Pointer3)
  IF ERRORCODE()
     glo:Queue3.Pointer3 = man:Manufacturer
     ADD(glo:Queue3,glo:Queue3.Pointer3)
    tmp:ManufacturerTag = '*'
  ELSE
    DELETE(glo:Queue3)
    tmp:ManufacturerTag = ''
  END
    Queue:Browse:2.tmp:ManufacturerTag = tmp:ManufacturerTag
  IF (tmp:Manufacturertag = '*')
    Queue:Browse:2.tmp:ManufacturerTag_Icon = 2
  ELSE
    Queue:Browse:2.tmp:ManufacturerTag_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW11.Reset
  FREE(glo:Queue3)
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue3.Pointer3 = man:Manufacturer
     ADD(glo:Queue3,glo:Queue3.Pointer3)
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue3)
  BRW11.Reset
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue3)
    GET(glo:Queue3,QR#)
    DASBRW::12:QUEUE = glo:Queue3
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue3)
  BRW11.Reset
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer3 = man:Manufacturer
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer3)
    IF ERRORCODE()
       glo:Queue3.Pointer3 = man:Manufacturer
       ADD(glo:Queue3,glo:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW11.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
CN:getnextrecord      routine
    CN:recordsprocessed += 1
    CN:recordsthiscycle += 1
    if CN:percentprogress < 100
      CN:percentprogress = (CN:recordsprocessed / CN:recordstoprocess)*100
      if CN:percentprogress > 100
        CN:percentprogress = 100
      end
      if CN:percentprogress <> CN:progress:thermometer then
        CN:progress:thermometer = CN:percentprogress
      end
    end

CN:endprintrun         routine
    CN:progress:thermometer = 100
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020194'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('WarrantyIncomeReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:REPSCHCR.Open
  Access:REPSCHCT.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHMA.UseFile
  Access:REPSCHLG.UseFile
  SELF.FilesOpened = True
  tmp:StartDate = Today()
  tmp:EndDate   = Today()
  
  
  If glo:WebJob
      tmp:MainAccount = Clarionet:Global.Param2
  Else !glo:WebJob
      tmp:MainAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  End !glo:WebJob
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  BRW9.Init(?List:2,Queue:Browse:1.ViewPosition,BRW9::View:Browse,Queue:Browse:1,Relate:CHARTYPE,SELF)
  BRW11.Init(?List:3,Queue:Browse:2.ViewPosition,BRW11::View:Browse,Queue:Browse:2,Relate:MANUFACT,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (RepQ.Init(COMMAND()) = Level:Benign)
     WarrantyIncomeReport(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
     RETURN RequestCompleted
  END!  IF
  If glo:WebJob
      ?LookupStartDate{prop:Hide} = 1
      ?LookupEndDate{prop:Hide} = 1
  ELSE
     HIDE(?Tab1)
     tmp:arc = TRUE
  End !glo:WebJob
  
  set(defaults)
  access:defaults.next()
  Glo:CSV_SavePath = def:ExportPath
  if Glo:CSV_SavePath[len(clip(Glo:CSV_SavePath))] <> '\' then
      Glo:CSV_SavePath = clip(Glo:CSV_SavePath)&'\'
  END
  
  if func:AutoRun = true
      ! Auto-run wizard
      tmp:StartDate = date(month(today()), 1, year(today()))
      tmp:EndDate = today()
      tmp:AllAccounts = 1             ! All trade accounts
      tmp:AllChargeTypes = 1          ! All charge types
      tmp:AllManufacturer = 1         ! All manufacturers
      tmp:ReportOrder = 0             ! By manufacturer
      tmp:includearc = 1              ! Include ARC jobs
      tmp:AllStatus = 1               ! All status types
      tmp:Submitted = 0
      tmp:Resubmitted = 0
      tmp:Rejected = 0
      tmp:RejectionAcknowledged = 0
      tmp:Reconciled = 0
      tmp:PaperReport = 0             ! Export
      tmp:arc = 1                     ! ARC
  
      post(event:Accepted, ?Finish)
  end
      ! Save Window Name
   AddToLog('Window','Open','WarrantyIncomeReportCriteria')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard7.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Cancel, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     0, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,sub:GenericAccountKey)
  BRW5.AddRange(sub:Generic_Account,tmp:True)
  BRW5.AddLocator(BRW5::Sort1:Locator)
  BRW5::Sort1:Locator.Init(,sub:Account_Number,1,BRW5)
  BRW5.AddSortOrder(,sub:Main_Account_Key)
  BRW5.AddRange(sub:Main_Account_Number,tmp:MainAccount)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,sub:Account_Number,1,BRW5)
  BIND('tmp:AccountTag',tmp:AccountTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(tmp:AccountTag,BRW5.Q.tmp:AccountTag)
  BRW5.AddField(sub:Account_Number,BRW5.Q.sub:Account_Number)
  BRW5.AddField(sub:Company_Name,BRW5.Q.sub:Company_Name)
  BRW5.AddField(sub:RecordNumber,BRW5.Q.sub:RecordNumber)
  BRW5.AddField(sub:Generic_Account,BRW5.Q.sub:Generic_Account)
  BRW5.AddField(sub:Main_Account_Number,BRW5.Q.sub:Main_Account_Number)
  BRW9.Q &= Queue:Browse:1
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,cha:Warranty_Key)
  BRW9.AddRange(cha:Warranty,tmp:Yes)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,cha:Charge_Type,1,BRW9)
  BIND('tmp:ChargeTypeTag',tmp:ChargeTypeTag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:ChargeTypeTag,BRW9.Q.tmp:ChargeTypeTag)
  BRW9.AddField(cha:Charge_Type,BRW9.Q.cha:Charge_Type)
  BRW9.AddField(cha:Warranty,BRW9.Q.cha:Warranty)
  BRW11.Q &= Queue:Browse:2
  BRW11.RetainRow = 0
  BRW11.AddSortOrder(,man:Manufacturer_Key)
  BRW11.AddLocator(BRW11::Sort0:Locator)
  BRW11::Sort0:Locator.Init(,man:Manufacturer,1,BRW11)
  BIND('tmp:ManufacturerTag',tmp:ManufacturerTag)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW11.AddField(tmp:ManufacturerTag,BRW11.Q.tmp:ManufacturerTag)
  BRW11.AddField(man:Manufacturer,BRW11.Q.man:Manufacturer)
  BRW11.AddField(man:RecordNumber,BRW11.Q.man:RecordNumber)
  IF ?tmp:AllAccounts{Prop:Checked} = True
    DISABLE(?List)
  END
  IF ?tmp:AllAccounts{Prop:Checked} = False
    ENABLE(?List)
  END
  IF ?tmp:AllChargeTypes{Prop:Checked} = True
    DISABLE(?List:2)
  END
  IF ?tmp:AllChargeTypes{Prop:Checked} = False
    ENABLE(?List:2)
  END
  IF ?tmp:AllManufacturer{Prop:Checked} = True
    DISABLE(?List:3)
  END
  IF ?tmp:AllManufacturer{Prop:Checked} = False
    ENABLE(?List:3)
  END
  IF ?tmp:AllStatus{Prop:Checked} = True
    DISABLE(?StatusGroup)
  END
  IF ?tmp:AllStatus{Prop:Checked} = False
    ENABLE(?StatusGroup)
  END
  FileLookup16.Init
  FileLookup16.Flags=BOR(FileLookup16.Flags,FILE:LongName)
  FileLookup16.Flags=BOR(FileLookup16.Flags,FILE:Directory)
  FileLookup16.SetMask('All Files','*.*')
  FileLookup16.DefaultDirectory='Glo:CSV_SavePath'
  FileLookup16.WindowTitle='Choose A Directory'
  BRW5.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  BRW11.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW11.AskProcedure = 0
      CLEAR(BRW11.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
  
              tmp:AllChargeTypes = rpc:AllChargeTypes
  
              Access:REPSCHCT.Clearkey(rpr:ChargeTypeKey)
              rpr:REPSCHCRRecordNumber = rpc:RecordNumber
              Set(rpr:ChargeTypeKey,rpr:ChargeTypeKey)
              Loop ! Begin Loop
                  If Access:REPSCHCT.Next()
                      Break
                  End ! If Access:REPSCHCT.Next()
                  If rpr:REPSCHCRRecordNumber <> rpc:RecordNumber
                      Break
                  End ! If rpr:REPSCHCRRecordNumber <> rpc:RecordNumber
                  glo:Pointer2 = rpr:ChargeType
                  Add(glo:Queue2)
              End ! Loop
  
              tmp:AllManufacturer = rpc:AllManufacturers
  
              Access:REPSCHMA.Clearkey(rpu:ManufacturerKey)
              rpu:REPSCHCRRecordNumber = rpc:RecordNumber
              Set(rpu:ManufacturerKey,rpu:ManufacturerKey)
              Loop ! Begin Loop
                  If Access:REPSCHMA.Next()
                      Break
                  End ! If Access:REPSCHMA.Next()
                  If rpu:REPSCHCRRecordNumber <> rpc:RecordNumber
                      Break
                  End ! If rpu:REPSCHCRRecordNumber <> rpc:RecordNumber
                  glo:Pointer3 = rpu:Manufacturer
                  Add(glo:Queue3)
              End ! Loop
  
              tmp:AllStatus = rpc:WarIncAll
              tmp:Submitted = rpc:WarIncSubmitted
              tmp:Resubmitted = rpc:WarIncReSubmitted
              tmp:Rejected = rpc:WarIncRejected
              tmp:RejectionAcknowledged = rpc:WarIncRejectionAcknowledged
              tmp:Reconciled = rpc:WarIncReconciled
              tmp:IncludeARCJobs = rpc:WarIncIncludeARCRepairedJobs
              tmp:IncludeARC =  rpc:IncludeARCREpairedRRCJobs
              tmp:SuppressZeros = rpc:WarIncSuppressZeros
              tmp:ReportOrder = rpc:WarIncReportOrder
  
              tmp:PaperReport = 0
  
              SHGetSpecialFolderPath( GetDesktopWindow(), tmp:DesktopFolder, 16, FALSE )
              tmp:DesktopFolder = tmp:DesktopFolder & '\ServiceBase Export'
              If ~Exists(tmp:DesktopFolder)
                  If ~MkDir(tmp:DesktopFolder)
                      ! Can't create desktop folder
                  End ! If ~MkDir(tmp:Desktop)
              End ! If ~Exists(tmp:Desktop)
              glo:CSV_SavePath = tmp:DesktopFolder
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  tmp:StartDate = Today()
                  tmp:EndDate = Today()
              Of 2 ! 1st Of Month
                  tmp:StartDate = Date(Month(Today()),1,Year(Today()))
                  tmp:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Does not account for a change of year
  !                loc:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  ! to (DBH 31/01/2008) # 9711
                  tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  tmp:StartDate = Date(Month(tmp:EndDate),1,Year(tmp:EndDate))
  ! End (DBH 31/01/2008) #9711
  
              End ! Case rpc:DateRangeType
              func:AutoRun = True
              Post(Event:Accepted,?Finish)
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:REPSCHCR.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','WarrantyIncomeReportCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard7.Validate()
        DISABLE(Wizard7.NextControl())
     ELSE
        ENABLE(Wizard7.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:PaperReport
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PaperReport, Accepted)
      if tmp:PaperReport then
          !Hide everything
          hide(?SavePathPrompt)
          hide(?Glo:CSV_SavePath)
          hide(?LookupFile)
      ELSE
          if glo:webjob then
              !ignore - not relevant to thin client
          ELSE
              unhide(?SavePathPrompt)
              unhide(?Glo:CSV_SavePath)
              unhide(?LookupFile)
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PaperReport, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:AllAccounts
      IF ?tmp:AllAccounts{Prop:Checked} = True
        DISABLE(?List)
      END
      IF ?tmp:AllAccounts{Prop:Checked} = False
        ENABLE(?List)
      END
      ThisWindow.Reset
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllChargeTypes
      IF ?tmp:AllChargeTypes{Prop:Checked} = True
        DISABLE(?List:2)
      END
      IF ?tmp:AllChargeTypes{Prop:Checked} = False
        ENABLE(?List:2)
      END
      ThisWindow.Reset
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllManufacturer
      IF ?tmp:AllManufacturer{Prop:Checked} = True
        DISABLE(?List:3)
      END
      IF ?tmp:AllManufacturer{Prop:Checked} = False
        ENABLE(?List:3)
      END
      ThisWindow.Reset
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllStatus
      IF ?tmp:AllStatus{Prop:Checked} = True
        DISABLE(?StatusGroup)
      END
      IF ?tmp:AllStatus{Prop:Checked} = False
        ENABLE(?StatusGroup)
      END
      ThisWindow.Reset
    OF ?LookupFile
      ThisWindow.Update
      glo:CSV_SavePath = FileLookup16.Ask(1)
      DISPLAY
    OF ?tmp:DateRangeType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:DateRangeType, Accepted)
      if (tmp:DateRangeType <> 0)
          ?locExcludedOnly{prop:Disable} = 1
      else
          ?locExcludedOnly{prop:Disable} = 0
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:DateRangeType, Accepted)
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?VSBackButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020194'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020194'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020194'&'0')
      ***
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      ! Inserting (DBH 14/02/2008) # 9752 - Check for valid dates
      If func:AutoRun <> 1 And ~Command('/SCHEDULE')
          If IsDateInValid(tmp:StartDate)
              Beep(Beep:SystemHand);  Yield()
             Case Missive('Invalid Start Date.','ServiceBase 3g',|
                            'mstop.jpg','/OK')
                 Of 1 ! OK Button
             End ! Case Missive
             tmp:StartDate = ''
             Select(?tmp:StartDate)
             Cycle
          End ! If IsDateInValid(tmp:StartDate)
          If IsDateInValid(tmp:EndDate)
              Beep(Beep:SystemHand);  Yield()
             Case Missive('Invalid End Date.','ServiceBase 3g',|
                            'mstop.jpg','/OK')
                 Of 1 ! OK Button
             End ! Case Missive
             tmp:EndDate = ''
             Select(?tmp:EndDate)
             Cycle
          End ! If IsDateInValid(tmp:StartDate)
      
          Beep(Beep:SystemQuestion);  Yield()
          Case Missive('Confirm Date Range:'&|
              '|    From:   ' & Format(tmp:StartDate,@d18) & |
              '|    To:        ' & Format(tmp:EndDate,@d18) & |
              '||Run Report?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
              Of 1 ! No Button
                  Display()
                  Cycle
          End ! Case Missive
      End ! If func:AutoRun <> 1 And ~Command('/SCHEDULE')
      ! End (DBH 14/02/2008) #9752
      
      !Will use a global to prevent change the Protype and all the stupid
      !hassles that will cause, because of them using an EXE kicker program.
      glo:Select21 = tmp:SuppressZeros
      WarrantyIncomeReport(tmp:StartDate,tmp:EndDate,tmp:AllAccounts,tmp:AllChargeTypes,|
                          tmp:AllManufacturer,tmp:ReportOrder,tmp:includearc,|
                          tmp:AllStatus,tmp:Submitted,tmp:Resubmitted,tmp:Rejected,tmp:RejectionAcknowledged,|
                          tmp:Reconciled,tmp:PaperReport,tmp:arc, tmp:IncludeARCJobs,func:AutoRun,tmp:DateRangeType,locExcludedOnly)
      glo:Select21 = ''
      
      !Final tmp:paperReport added 3/12/02 - l368 - allow choice of CSV output
      
      if func:AutoRun = true
          ! If this report is running automatically then close the window
          post(event:CloseWindow)
      end
      If Command('/SCHEDULE')
          If Access:REPSCHLG.PrimeRecord() = Level:Benign
              rlg:REPSCHEDRecordNumber = rpd:RecordNumber
              rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                '<13,10>Report Finished'
              If Access:REPSCHLG.TryInsert() = Level:Benign
                  !Insert
              Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                  Access:REPSCHLG.CancelAutoInc()
              End ! If Access:REPSCHLG.TryInsert() = Level:Benign
          End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag:2)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
    END
  OF ?List:3
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:3, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag:3)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:3, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2)  = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:AccountTag = ''
    ELSE
      tmp:AccountTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Accounttag = '*')
    SELF.Q.tmp:AccountTag_Icon = 2
  ELSE
    SELF.Q.tmp:AccountTag_Icon = 1
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = cha:Charge_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:ChargeTypeTag = ''
    ELSE
      tmp:ChargeTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:ChargeTypeTag = '*')
    SELF.Q.tmp:ChargeTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:ChargeTypeTag_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = cha:Charge_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::10:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue


BRW11.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = man:Manufacturer
     GET(glo:Queue3,glo:Queue3.Pointer3)
    IF ERRORCODE()
      tmp:ManufacturerTag = ''
    ELSE
      tmp:ManufacturerTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Manufacturertag = '*')
    SELF.Q.tmp:ManufacturerTag_Icon = 2
  ELSE
    SELF.Q.tmp:ManufacturerTag_Icon = 1
  END


BRW11.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW11.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW11::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW11::RecordStatus=ReturnValue
  IF BRW11::RecordStatus NOT=Record:OK THEN RETURN BRW11::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = man:Manufacturer
     GET(glo:Queue3,glo:Queue3.Pointer3)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW11::RecordStatus
  RETURN ReturnValue

Wizard7.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW5.Q &= NULL) ! Has Browse Object been initialized?
       BRW5.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW9.Q &= NULL) ! Has Browse Object been initialized?
       BRW9.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW11.Q &= NULL) ! Has Browse Object been initialized?
       BRW11.ResetQueue(Reset:Queue)
    END

Wizard7.TakeBackEmbed PROCEDURE
   CODE

Wizard7.TakeNextEmbed PROCEDURE
   CODE

Wizard7.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
HandlingExchangeReportCriteria PROCEDURE              !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::10:TAGDISPSTATUS   BYTE(0)
DASBRW::10:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer3                      LIKE(glo:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_sub_id          USHORT,AUTO
tmp:AccountTag       STRING(1)
tmp:PaymentTag       STRING(1)
tmp:ChargeTypeTag    STRING(1)
tmp:ChargeType       STRING(30)
tmp:WarrantyChargeType STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:includeARC       BYTE
tmp:AllAccounts      BYTE(1)
tmp:MainAccount      STRING(30)
tmp:True             BYTE(1)
tmp:AllChargeTypes   BYTE(1)
tmp:ReportOrder      BYTE(0)
tmp:No               STRING('NO {28}')
tmp:Yes              STRING('YES')
tmp:ManufacturerTag  STRING(1)
tmp:AllManufacturer  BYTE(1)
tmp:Paper            BYTE(1)
tmp:arc              BYTE
BRW5::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                       PROJECT(sub:Generic_Account)
                       PROJECT(sub:Main_Account_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:AccountTag         LIKE(tmp:AccountTag)           !List box control field - type derived from local data
tmp:AccountTag_Icon    LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
sub:Generic_Account    LIKE(sub:Generic_Account)      !Browse key field - type derived from field
sub:Main_Account_Number LIKE(sub:Main_Account_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:ChargeTypeTag      LIKE(tmp:ChargeTypeTag)        !List box control field - type derived from local data
tmp:ChargeTypeTag_Icon LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:ManufacturerTag    LIKE(tmp:ManufacturerTag)      !List box control field - type derived from local data
tmp:ManufacturerTag_Icon LONG                         !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Handling & Exchange Fee Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(168,332),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(236,332),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Tab1'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select the Trade Accounts you wish to include'),AT(219,124),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All Accounts'),AT(400,124),USE(tmp:AllAccounts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Accounts'),TIP('All Accounts'),VALUE('1','0')
                           SHEET,AT(168,137,344,191),USE(?Sheet2),SPREAD
                             TAB('By Account Number'),USE(?Tab4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                             TAB('Generic Accounts'),USE(?Tab5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                           END
                           LIST,AT(172,153,236,171),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@67L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(249,164,5,5),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(249,185,5,5),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(420,177),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(420,209),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(420,247),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 6'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Charge Types To Include'),AT(239,124),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All Charge Types'),AT(363,124),USE(tmp:AllChargeTypes),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Charge Types'),TIP('All Charge Types'),VALUE('1','0')
                           LIST,AT(250,137,182,134),USE(?List:2),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(289,191,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(293,223,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(240,276),USE(?DASTAG:2),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,276),USE(?DASTAGAll:2),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,276),USE(?DASUNTAGALL:2),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 7'),USE(?Tab7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Manufacturer To Include'),AT(235,124),USE(?Prompt7),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All Manufacturers'),AT(363,124),USE(tmp:AllManufacturer),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Manufacturers'),TIP('All Manufacturers'),VALUE('1','0')
                           LIST,AT(252,137,180,134),USE(?List:3),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Rev tags'),AT(299,223,50,13),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(307,239,70,13),USE(?DASSHOWTAG:3),HIDE
                           BUTTON,AT(244,276),USE(?DASTAG:3),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,276),USE(?DASTAGAll:3),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(372,276),USE(?DASUNTAGALL:3),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 3'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Action Date Range'),AT(218,126),USE(?Prompt4),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Start Date'),AT(217,144),USE(?tmp:StartDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(305,144,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(373,140),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(217,164),USE(?tmp:EndDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(305,164,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(373,160),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           CHECK(' Include ARC Repaired Jobs'),AT(217,180),USE(tmp:includeARC),DISABLE,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           OPTION('Report Order'),AT(213,196,244,28),USE(tmp:ReportOrder),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('By Manufacturer'),AT(373,209),USE(?tmp:ReportOrder:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('By Job Number'),AT(221,209),USE(?tmp:ReportOrder:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('By Action Date'),AT(297,209),USE(?tmp:ReportOrder:Radio2:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           OPTION('Report Type'),AT(213,232,244,28),USE(tmp:Paper),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Paper'),AT(221,242),USE(?tmp:Paper:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Electronic'),AT(297,242),USE(?tmp:Paper:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                           END
                           PROMPT('Path to Save File'),AT(214,274),USE(?SavePathPrompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(214,286,223,11),USE(glo:CSV_SavePath),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           BUTTON,AT(441,282),USE(?LookupFile),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Handling && Exchange Fee Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

! Clarionet Moving Bar
CN:recordstoprocess     long,auto
CN:recordsprocessed     long,auto
CN:recordspercycle      long,auto
CN:recordsthiscycle     long,auto
CN:percentprogress      byte
CN:recordstatus         byte,auto

CN:progress:thermometer byte
CN:progresswindow WINDOW('Please Wait...'),AT(,,164,22),FONT('Arial',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(CN:progress:thermometer),AT(4,4,156,12),RANGE(0,100)
     END


tmp:DesktopFolder   CString(255)
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

Wizard7         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet2)  = 2
BRW9                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW11                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                 !Default Locator
FileLookup16         SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
RepQ    RepQClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue.Pointer = sub:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:AccountTag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:AccountTag = ''
  END
    Queue:Browse.tmp:AccountTag = tmp:AccountTag
  IF (tmp:Accounttag = '*')
    Queue:Browse.tmp:AccountTag_Icon = 2
  ELSE
    Queue:Browse.tmp:AccountTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::6:QUEUE = glo:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sub:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::10:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW9.UpdateBuffer
   glo:Queue2.Pointer2 = cha:Charge_Type
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = cha:Charge_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:ChargeTypeTag = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:ChargeTypeTag = ''
  END
    Queue:Browse:1.tmp:ChargeTypeTag = tmp:ChargeTypeTag
  IF (tmp:ChargeTypeTag = '*')
    Queue:Browse:1.tmp:ChargeTypeTag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:ChargeTypeTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = cha:Charge_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::10:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::10:QUEUE = glo:Queue2
    ADD(DASBRW::10:QUEUE)
  END
  FREE(glo:Queue2)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::10:QUEUE.Pointer2 = cha:Charge_Type
     GET(DASBRW::10:QUEUE,DASBRW::10:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = cha:Charge_Type
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASSHOWTAG Routine
   CASE DASBRW::10:TAGDISPSTATUS
   OF 0
      DASBRW::10:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::10:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::10:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW11.UpdateBuffer
   glo:Queue3.Pointer3 = man:Manufacturer
   GET(glo:Queue3,glo:Queue3.Pointer3)
  IF ERRORCODE()
     glo:Queue3.Pointer3 = man:Manufacturer
     ADD(glo:Queue3,glo:Queue3.Pointer3)
    tmp:ManufacturerTag = '*'
  ELSE
    DELETE(glo:Queue3)
    tmp:ManufacturerTag = ''
  END
    Queue:Browse:2.tmp:ManufacturerTag = tmp:ManufacturerTag
  IF (tmp:Manufacturertag = '*')
    Queue:Browse:2.tmp:ManufacturerTag_Icon = 2
  ELSE
    Queue:Browse:2.tmp:ManufacturerTag_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW11.Reset
  FREE(glo:Queue3)
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue3.Pointer3 = man:Manufacturer
     ADD(glo:Queue3,glo:Queue3.Pointer3)
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue3)
  BRW11.Reset
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue3)
    GET(glo:Queue3,QR#)
    DASBRW::12:QUEUE = glo:Queue3
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue3)
  BRW11.Reset
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer3 = man:Manufacturer
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer3)
    IF ERRORCODE()
       glo:Queue3.Pointer3 = man:Manufacturer
       ADD(glo:Queue3,glo:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW11.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
CN:getnextrecord      routine
    CN:recordsprocessed += 1
    CN:recordsthiscycle += 1
    if CN:percentprogress < 100
      CN:percentprogress = (CN:recordsprocessed / CN:recordstoprocess)*100
      if CN:percentprogress > 100
        CN:percentprogress = 100
      end
      if CN:percentprogress <> CN:progress:thermometer then
        CN:progress:thermometer = CN:percentprogress
      end
    end

CN:endprintrun         routine
    CN:progress:thermometer = 100
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020190'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('HandlingExchangeReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?VSBackButton
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:REPSCHCR.Open
  Access:REPSCHCT.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLG.UseFile
  Access:REPSCHMA.UseFile
  SELF.FilesOpened = True
  tmp:StartDate = Today()
  tmp:EndDate   = Today()
  
  If glo:WebJob
      tmp:MainAccount = Clarionet:Global.Param2
  Else !glo:WebJob
      tmp:MainAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  End !glo:WebJob
  
  
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  BRW9.Init(?List:2,Queue:Browse:1.ViewPosition,BRW9::View:Browse,Queue:Browse:1,Relate:CHARTYPE,SELF)
  BRW11.Init(?List:3,Queue:Browse:2.ViewPosition,BRW11::View:Browse,Queue:Browse:2,Relate:MANUFACT,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (RepQ.Init(COMMAND()) = Level:Benign)
     HandlingExchangeFeeReport(0,0,0,0,0,0,0,0,0)
     RETURN RequestCompleted
  END!  IF
  
  If glo:WebJob
      ?LookupStartDate{prop:Hide} = 1
      ?LookupEndDate{prop:Hide} = 1
  ELSE
      HIDE(?Tab1)
      tmp:arc = TRUE
  End !glo:WebJob
  set(defaults)
  access:defaults.next()
  Glo:CSV_SavePath = def:ExportPath
  if Glo:CSV_SavePath[len(clip(Glo:CSV_SavePath))] <> '\' then
      Glo:CSV_SavePath = clip(Glo:CSV_SavePath)&'\'
  END
      ! Save Window Name
   AddToLog('Window','Open','HandlingExchangeReportCriteria')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard7.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?cANCEL, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,sub:GenericAccountKey)
  BRW5.AddRange(sub:Generic_Account,tmp:True)
  BRW5.AddLocator(BRW5::Sort1:Locator)
  BRW5::Sort1:Locator.Init(,sub:Account_Number,1,BRW5)
  BRW5.AddSortOrder(,sub:Main_Account_Key)
  BRW5.AddRange(sub:Main_Account_Number,tmp:MainAccount)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,sub:Account_Number,1,BRW5)
  BIND('tmp:AccountTag',tmp:AccountTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(tmp:AccountTag,BRW5.Q.tmp:AccountTag)
  BRW5.AddField(sub:Account_Number,BRW5.Q.sub:Account_Number)
  BRW5.AddField(sub:Company_Name,BRW5.Q.sub:Company_Name)
  BRW5.AddField(sub:RecordNumber,BRW5.Q.sub:RecordNumber)
  BRW5.AddField(sub:Generic_Account,BRW5.Q.sub:Generic_Account)
  BRW5.AddField(sub:Main_Account_Number,BRW5.Q.sub:Main_Account_Number)
  BRW9.Q &= Queue:Browse:1
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,cha:Charge_Type_Key)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,cha:Charge_Type,1,BRW9)
  BIND('tmp:ChargeTypeTag',tmp:ChargeTypeTag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:ChargeTypeTag,BRW9.Q.tmp:ChargeTypeTag)
  BRW9.AddField(cha:Charge_Type,BRW9.Q.cha:Charge_Type)
  BRW11.Q &= Queue:Browse:2
  BRW11.RetainRow = 0
  BRW11.AddSortOrder(,man:Manufacturer_Key)
  BRW11.AddLocator(BRW11::Sort0:Locator)
  BRW11::Sort0:Locator.Init(,man:Manufacturer,1,BRW11)
  BIND('tmp:ManufacturerTag',tmp:ManufacturerTag)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW11.AddField(tmp:ManufacturerTag,BRW11.Q.tmp:ManufacturerTag)
  BRW11.AddField(man:Manufacturer,BRW11.Q.man:Manufacturer)
  BRW11.AddField(man:RecordNumber,BRW11.Q.man:RecordNumber)
  IF ?tmp:AllAccounts{Prop:Checked} = True
    DISABLE(?List)
  END
  IF ?tmp:AllAccounts{Prop:Checked} = False
    ENABLE(?List)
  END
  IF ?tmp:AllChargeTypes{Prop:Checked} = True
    DISABLE(?List:2)
  END
  IF ?tmp:AllChargeTypes{Prop:Checked} = False
    ENABLE(?List:2)
  END
  IF ?tmp:AllManufacturer{Prop:Checked} = True
    DISABLE(?List:3)
  END
  IF ?tmp:AllManufacturer{Prop:Checked} = False
    ENABLE(?List:3)
  END
  FileLookup16.Init
  FileLookup16.Flags=BOR(FileLookup16.Flags,FILE:LongName)
  FileLookup16.Flags=BOR(FileLookup16.Flags,FILE:Directory)
  FileLookup16.SetMask('All Files','*.*')
  FileLookup16.DefaultDirectory='Glo:CSV_SavePath'
  BRW5.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  BRW11.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW11.AskProcedure = 0
      CLEAR(BRW11.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
  
              tmp:AllChargeTypes = rpc:AllChargeTypes
              tmp:AllManufacturer = rpc:AllManufacturers
  
              Access:REPSCHCT.Clearkey(rpr:ChargeTypeKey)
              rpr:REPSCHCRRecordNumber = rpc:RecordNumber
              Set(rpr:ChargeTypeKey,rpr:ChargeTypeKey)
              Loop ! Begin Loop
                  If Access:REPSCHCT.Next()
                      Break
                  End ! If Access:REPSCHCT.Next()
                  If rpr:REPSCHCRRecordNumber <> rpc:RecordNumber
                      Break
                  End ! If rpr:REPSCHCRRecordNumber <> rpc:RecordNumber
                  glo:Queue2 = rpr:ChargeType
                  Add(glo:Queue2)
              End ! Loop
  
              Access:REPSCHMA.Clearkey(rpu:ManufacturerKey)
              rpu:REPSCHCRRecordNumber = rpc:RecordNumber
              Set(rpu:ManufacturerKey,rpu:ManufacturerKey)
              Loop ! Begin Loop
                  If Access:REPSCHMA.Next()
                      Break
                  End ! If Access:REPSCHMA.Next()
                  If rpu:REPSCHCRRecordNumber <> rpc:RecordNumber
                      Break
                  End ! If rpu:REPSCHCRRecordNumber <> rpc:RecordNumber
                  glo:Queue3 = rpu:Manufacturer
                  Add(glo:Queue3)
              End ! Loop
  
              tmp:ReportOrder = rpc:WarIncReportOrder - 1
              tmp:Paper = 0
  
              SHGetSpecialFolderPath( GetDesktopWindow(), tmp:DesktopFolder, 16, FALSE )
              tmp:DesktopFolder = tmp:DesktopFolder & '\ServiceBase Export'
              If ~Exists(tmp:DesktopFolder)
                  If ~MkDir(tmp:DesktopFolder)
                      ! Can't create desktop folder
                  End ! If ~MkDir(tmp:Desktop)
              End ! If ~Exists(tmp:Desktop)
              glo:CSV_SavePath = tmp:DesktopFolder
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  tmp:StartDate = Today()
                  tmp:EndDate = Today()
              Of 2 ! 1st Of Month
                  tmp:StartDate = Date(Month(Today()),1,Year(Today()))
                  tmp:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Does not account for a change of year
  !                loc:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  ! to (DBH 31/01/2008) # 9711
                  tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  tmp:StartDate = Date(Month(tmp:EndDate),1,Year(tmp:EndDate))
  ! End (DBH 31/01/2008) #9711
  
              End ! Case rpc:DateRangeType
  
              Post(Event:Accepted,?Finish)
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:REPSCHCR.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','HandlingExchangeReportCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard7.Validate()
        DISABLE(Wizard7.NextControl())
     ELSE
        ENABLE(Wizard7.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:Paper
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Paper, Accepted)
      if tmp:paper then
          hide(?SavePathPrompt)
          hide(Glo:Csv_savePath)
          hide(?LookupFile)
      ELSE
          if glo:webjob then
              !ignore - not applicable to thin client
          ELSE
              unhide(?SavePathPrompt)
              unhide(?Glo:CSV_SavePath)
              unhide(?lookupfile)
          END !if glo:webjob
      END !If tmp:paper
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Paper, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?VSBackButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?tmp:AllAccounts
      IF ?tmp:AllAccounts{Prop:Checked} = True
        DISABLE(?List)
      END
      IF ?tmp:AllAccounts{Prop:Checked} = False
        ENABLE(?List)
      END
      ThisWindow.Reset
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllChargeTypes
      IF ?tmp:AllChargeTypes{Prop:Checked} = True
        DISABLE(?List:2)
      END
      IF ?tmp:AllChargeTypes{Prop:Checked} = False
        ENABLE(?List:2)
      END
      ThisWindow.Reset
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllManufacturer
      IF ?tmp:AllManufacturer{Prop:Checked} = True
        DISABLE(?List:3)
      END
      IF ?tmp:AllManufacturer{Prop:Checked} = False
        ENABLE(?List:3)
      END
      ThisWindow.Reset
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupFile
      ThisWindow.Update
      glo:CSV_SavePath = FileLookup16.Ask(1)
      DISPLAY
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020190'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020190'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020190'&'0')
      ***
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      ! Inserting (DBH 14/02/2008) # 9752 - Check for valid dates
      If ~Command('/SCHEDULE')
          If IsDateInValid(tmp:StartDate)
              Beep(Beep:SystemHand);  Yield()
             Case Missive('Invalid Start Date.','ServiceBase 3g',|
                            'mstop.jpg','/OK')
                 Of 1 ! OK Button
             End ! Case Missive
             tmp:StartDate = ''
             Select(?tmp:StartDate)
             Cycle
          End ! If IsDateInValid(tmp:StartDate)
          If IsDateInValid(tmp:EndDate)
              Beep(Beep:SystemHand);  Yield()
             Case Missive('Invalid End Date.','ServiceBase 3g',|
                            'mstop.jpg','/OK')
                 Of 1 ! OK Button
             End ! Case Missive
             tmp:EndDate = ''
             Select(?tmp:EndDate)
             Cycle
          End ! If IsDateInValid(tmp:StartDate)
      
          Beep(Beep:SystemQuestion);  Yield()
          Case Missive('Confirm Date Range:'&|
              '|    From:   ' & Format(tmp:StartDate,@d18) & |
              '|    To:        ' & Format(tmp:EndDate,@d18) & |
              '||Run Report?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
              Of 1 ! No Button
                  Display()
                  Cycle
          End ! Case Missive
      End ! If func:AutoRun <> 1 And ~Command('/SCHEDULE')
      ! End (DBH 14/02/2008) #9752
      
      HandlingExchangeFeeReport(tmp:StartDate,tmp:EndDate,tmp:AllAccounts,tmp:AllChargeTypes,tmp:AllManufacturer,tmp:ReportOrder,tmp:includearc,tmp:paper,tmp:arc)
      If Command('/SCHEDULE')
          If Access:REPSCHLG.PrimeRecord() = Level:Benign
              rlg:REPSCHEDRecordNumber = rpd:RecordNumber
              rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                '<13,10>Report Finished'
              If Access:REPSCHLG.TryInsert() = Level:Benign
                  !Insert
              Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                  Access:REPSCHLG.CancelAutoInc()
              End ! If Access:REPSCHLG.TryInsert() = Level:Benign
          End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
      End
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If Keycode() = MouseLeft2
          Post(Event:Accepted,?DasTag)
      End ! If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag:2)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
    END
  OF ?List:3
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:3, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag:3)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:3, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2)  = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:AccountTag = ''
    ELSE
      tmp:AccountTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Accounttag = '*')
    SELF.Q.tmp:AccountTag_Icon = 2
  ELSE
    SELF.Q.tmp:AccountTag_Icon = 1
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = cha:Charge_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:ChargeTypeTag = ''
    ELSE
      tmp:ChargeTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:ChargeTypeTag = '*')
    SELF.Q.tmp:ChargeTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:ChargeTypeTag_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = cha:Charge_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::10:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue


BRW11.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = man:Manufacturer
     GET(glo:Queue3,glo:Queue3.Pointer3)
    IF ERRORCODE()
      tmp:ManufacturerTag = ''
    ELSE
      tmp:ManufacturerTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Manufacturertag = '*')
    SELF.Q.tmp:ManufacturerTag_Icon = 2
  ELSE
    SELF.Q.tmp:ManufacturerTag_Icon = 1
  END


BRW11.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW11.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW11::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW11::RecordStatus=ReturnValue
  IF BRW11::RecordStatus NOT=Record:OK THEN RETURN BRW11::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = man:Manufacturer
     GET(glo:Queue3,glo:Queue3.Pointer3)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW11::RecordStatus
  RETURN ReturnValue

Wizard7.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW5.Q &= NULL) ! Has Browse Object been initialized?
       BRW5.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW9.Q &= NULL) ! Has Browse Object been initialized?
       BRW9.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW11.Q &= NULL) ! Has Browse Object been initialized?
       BRW11.ResetQueue(Reset:Queue)
    END

Wizard7.TakeBackEmbed PROCEDURE
   CODE

Wizard7.TakeNextEmbed PROCEDURE
   CODE

Wizard7.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0






HandlingExchangeFeeReport PROCEDURE(func:StartDate,func:EndDate,func:AllAccounts,func:AllChargeTypes,func:AllManufacturers,func:ReportOrder,func:includearc,func:paper,func:arc)
! Before Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
!    MAP

!    END
RepQ    RepQClass
local       CLASS
Criteria        Procedure(),Byte
LoadSBOnlineCriteria    PROCEDURE(),LONG
            End ! local       CLASS
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
! After Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
save_job_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
save_inv_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
save_wob_id          USHORT,AUTO
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
AddressGroup         GROUP,PRE(address)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:CChargeType      STRING(30)
tmp:WChargeType      STRING(30)
tmp:All              BYTE(0)
ReportGroup          GROUP,PRE(report)
JobNumber            STRING(20)
ActionDate           DATE
CompanyName          STRING(30)
Manufacturer         STRING(30)
ModelNumber          STRING(30)
ChargeType           STRING(30)
Repair               STRING(3)
Exchanged            STRING(3)
Type                 STRING(20)
HandlingFee          REAL
Parts                REAL
PartsSelling         REAL
Labour               REAL
Vat                  REAL
Total                REAL
                     END
TotalGroup           GROUP,PRE(total)
Lines                LONG
HandlingFee          REAL
Labour               REAL
Parts                REAL
PartsSelling         REAL
VAT                  REAL
Total                REAL
                     END
NumberQueue          QUEUE,PRE(number)
Manufacturer         STRING(30)
ModelNumber          STRING(30)
JobNumber            LONG
ActionDate           DATE
RepairType           STRING(1)
                     END
tmp:DateRange        STRING(60)
tmp:ReportOrder      BYTE(0)
tmp:AllChargeTypes   BYTE(0)
tmp:AllManufacturers BYTE(0)
tmp:AllAccounts      BYTE(0)
tmp:ExchangeDate     DATE
ShowEmpty            BYTE
tempFilePath         CSTRING(255)
tmp:EngineerOption   STRING(3)
ExchangeIMEI         STRING(20)
ExchangeDate         STRING(10)
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

report               REPORT('Handling/Exchange Fee Report'),AT(458,2010,10938,5688),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,),LANDSCAPE,THOUS
                       HEADER,AT(427,323,9740,1375),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(address:CompanyName),TRN,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(104,260,3531,198),USE(address:AddressLine1),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(7448,729),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(8438,729),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,365,3531,198),USE(address:AddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,469,3531,198),USE(address:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,573),USE(address:Postcode),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(7448,573,625,208),USE(?String67),TRN,FONT(,8,,)
                         STRING(@s60),AT(8438,573),USE(tmp:printedby),TRN,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(8438,417),USE(tmp:DateRange),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(7448,417,625,208),USE(?String67:2),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(104,729),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,729),USE(address:TelephoneNumber),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(104,833),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,833),USE(address:FaxNumber),TRN,FONT(,8,,)
                         STRING(@s255),AT(573,938,3531,198),USE(address:EmailAddress),TRN,FONT(,8,,)
                         STRING('Email:'),AT(104,938),USE(?String16:2),TRN,FONT(,9,,)
                         STRING('Page:'),AT(7448,885),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@s4),AT(8438,885),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(8802,885),USE(?String72),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(9010,885,313,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
break1                 BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(10,10,10875,167),USE(?DetailBand)
                           STRING(@s25),AT(6813,0,1146,156),USE(report:ChargeType),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s3),AT(8979,0),USE(report:Repair),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(5573,0,1146,156),USE(report:ModelNumber),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(4417,0,,156),USE(report:Manufacturer),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(156,0,1146,167),USE(report:JobNumber),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s15),AT(1896,0,802,167),USE(job:Account_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(2760,0),USE(report:CompanyName),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n10.2),AT(9656,0),USE(report:HandlingFee),TRN,RIGHT,FONT(,7,,)
                           STRING(@s10),AT(8177,0),USE(report:Type),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@d6),AT(1313,0),USE(report:ActionDate),TRN,RIGHT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:3)
                           LINE,AT(198,52,10063,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,156),USE(?String68),TRN,FONT(,7,,FONT:bold)
                           STRING(@s8),AT(885,156),USE(total:Lines),FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(9583,156),USE(total:HandlingFee),TRN,RIGHT,FONT(,7,,FONT:bold)
                         END
                       END
NewPage                DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?unnamed:4)
                       END
TitlePage              DETAIL,AT(,,,219),USE(?unnamed:6),ABSOLUTE
                       END
                       FORM,AT(406,313,10969,7448),USE(?unnamed)
                         IMAGE('Rlistlan.gif'),AT(52,0,10938,7448),USE(?Image1)
                         STRING('HANDLING & EXCHANGE FEE REPORT'),AT(4156,52),USE(?ReportTitle),TRN,FONT(,12,,FONT:bold)
                         STRING('Job No'),AT(208,1406),USE(?InvoiceNumberText),TRN,FONT(,7,,)
                         STRING('Account No'),AT(1958,1406),USE(?String26:4),TRN,FONT(,7,,FONT:bold)
                         STRING('Hand/Exch'),AT(9729,1406),USE(?String26:8),TRN,FONT(,7,,FONT:bold)
                         STRING('Location'),AT(9042,1406),USE(?String58),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Action Date'),AT(1375,1406),USE(?InvoiceDateText),TRN,FONT(,7,,)
                         STRING('Model Number'),AT(5635,1406),USE(?String26:12),TRN,FONT(,7,,FONT:bold)
                         STRING('Manufacturer'),AT(4479,1406),USE(?String26:5),TRN,FONT(,7,,FONT:bold)
                         STRING('Account Name'),AT(2823,1406,,156),USE(?String26:11),TRN,FONT(,7,,FONT:bold)
                         STRING('Charge Type'),AT(6875,1406),USE(?String26:13),TRN,FONT(,7,,FONT:bold)
                         STRING('Type'),AT(8240,1406),USE(?String57),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('HandlingExchangeFeeReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','HandlingExchangeFeeReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULTS.Open
  Relate:DISCOUNT.Open
  Relate:EXCHANGE.Open
  Relate:JOBPAYMT.Open
  Relate:PAYTYPES.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBS.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBSE.UseFile
  Access:SUBTRACC.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:AUDIT.UseFile
  Access:TRACHAR.UseFile
  Access:CHARTYPE.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:STOCK.UseFile
  Access:AUDIT2.UseFile
  Access:JOBTHIRD.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = Records(JOBS)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Export/Report

        ! Inserting (DBH 14/06/2006) #7626 - Speed up report by using Audit Trail
        if ~func:paper then
            !this is an export to csv file - dont show error when paper report is empty
            ShowEmpty=false
        
            !prepare file for export
            If GetTempPathA(255,TempFilePath)
                If Sub(TempFilePath,-1,1) = '\'
                    glo:File_Name = Clip(TempFilePath) & 'Exchange Handling Fee Report ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
                Else !If Sub(TempFilePath,-1,1) = '\'
                    glo:File_Name = Clip(TempFilePath) & '\Exchange Handling Fee Report ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
                End !If Sub(TempFilePath,-1,1) = '\'
        
                Remove(glo:File_Name)
                Access:EXPGEN.Open()
                Access:EXPGEN.UseFile()
        
                !write the title
                Gen:line1   = 'HANDLING AND EXCHANGE FEE REPORT (Version: ' & Clip(ProgramVersionNumber()) & ')'
                access:expgen.insert()
        
                Clear(gen:record)
                Gen:line1   = 'Date Range:,' & tmp:DateRange
                Access:expgen.insert()
                Clear(gen:record)
                Gen:line1   = 'Printed By,' & tmp:printedby
                Access:expgen.insert()
                Clear(gen:record)
                Gen:line1   = 'Date Printed,'&format(Today(),@d6)
                Access:expgen.insert()
        
                Clear(gen:record)
        
                !Add Engineer Option - 3788 (DBH: 19-04-2004)
        !J - 18/09/14 - TB13293 adding yet more columns
            ! Changing (DBH 01/08/2006) # 7962 - New columns
            !        gen:line1 = 'Job No,Engineer Option,Branch ID,Franchise Job No,Action Date,Account No,Account Name,Manufacturer,Model Number,Charge Type,Type,Location,Hand/Exch'
            ! to (DBH 01/08/2006) # 7962
            !        gen:line1 = 'Job No,Engineer Option,Branch ID,Franchise Job No,Action Date,Account No,Account Name,Manufacturer,Model Number,Charge Type,RRC Waybill No,RRC Waybill Date,ARC Waybill Receipt Date,Type,Location,Hand/Exch'
            ! End (DBH 01/08/2006) #7962
        !J - 18/09/14 - TB13293 adding yet more columns
               gen:line1 = 'Job No,Engineer Option,Branch ID,Franchise Job No,Action Date,Account No,Account Name,Manufacturer,Model Number,'&|
                           'Charge Type,RRC Waybill No,RRC Waybill Date,ARC Waybill Receipt Date,Type,Location,Hand/Exch,Original IMEI,Boking Date,Exchange IMEI,Exchange Date'
        !!Uncomment things with ! in the first column - comment the other gen:line1 line
        !!End J - 18/09/14
                Access:EXPGEN.Insert()
        
            ELSE
                If ~Command('/SCHEDULE')
                    if glo:webjob then
                        Case Missive('Unable to source a temporary directory on the web server. Please report this error to the IT Department.'&|
                          '<13,10>'&|
                          '<13,10>A paper report will now be prepared.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    ELSE
                        Case Missive('Unable to souce a temporary directory on your computer. Please setup a temporary directory for Windows to use.'&|
                          '<13,10>'&|
                          '<13,10>A paper report will now be prepared.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    END
                    Return
                End ! If Command('/SCHEDULE')
                !Can't get temp file - print a paper file
                showEmpty = true
                func:paper = 1
            END
        ELSE
            ShowEmpty=true
        END !if not paper report
        
        prog.Init(1000)
        
        recordsRead# = 0 ! Used so the f!!! progress bar will update enough to keep the plebs happy
        
        
        ! Inserting (DBH 31/10/2006) # 8409 - Put this back in for Webmaster only
        If glo:WebJob
            Access:AUDIT.Clearkey(aud:ActionOnlyKey)
            aud:Action = 'O.B.F. VALIDATION ACCEPTED'
            aud:Date = tmp:StartDate
            Set(aud:ActionOnlyKey,aud:ActionOnlyKey)
            Loop ! Begin AUDIT Loop
                If Access:AUDIT.Next()
                    Break
                End ! If !Access
                If aud:Action <> 'O.B.F. VALIDATION ACCEPTED'
                    Break
                End ! If
                If aud:Date > tmp:EndDate
                    Break
                End ! If
                recordsRead# += 1
                If Prog.InsideLoop('Checking OBF Validation...(' & recordsRead# & ')')
                    Break
                End ! If Prog.InsideLoop()
        
                If local.Criteria() = False
                    Cycle
                End ! If local.Criteria() = False
        
                number:Manufacturer = job:Manufacturer
                number:ModelNumber  = job:Model_Number
                number:JobNumber    = job:Ref_Number
                number:RepairType   = 'O'
                number:ActionDate   = aud:Date
                Add(NumberQueue)
            End ! End AUDIT Loop
        End ! If glo:WebJob
        ! End (DBH 31/10/2006) #8409
        
        Access:AUDIT.Clearkey(aud:ActionOnlyKey)
        aud:Action = 'EXCHANGE UNIT ATTACHED TO JOB'
        aud:Date = tmp:StartDate
        Set(aud:ActionOnlyKey,aud:ActionOnlyKey)
        Loop ! Begin AUDIT Loop
            If Access:AUDIT.Next()
                Break
            End ! If !Access
            If aud:Action <> 'EXCHANGE UNIT ATTACHED TO JOB'
                Break
            End ! If
            If aud:Date > tmp:EndDate
                Break
            End ! If
            recordsRead#+= 1
            If Prog.InsideLoop('Checking Exchange Units...(' & recordsRead# & ')')
                Break
            End ! If Prog.InsideLoop()
        
        
            If local.Criteria() = False
                Cycle
            End ! If local.Criteria() = False
        
            ! Inserting (DBH 02/08/2006) # 7962 - Make sure this entry refers to the unit actually attached to the job
            Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
            aud2:AUDRecordNumber = aud:Record_Number
            IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                If ~Instring('UNIT NUMBER: ' & job:Exchange_Unit_Number,aud2:Notes,1,1)
                    Cycle
                End ! If ~Instring('UNIT NUMBER: ' & job:Exchange_Unit_Number,aud:Notes,1,1)
            END ! IF
            ! End (DBH 02/08/2006) #7962
        
            If jobe:ExchangedATRRC <> True
                Cycle
            End ! If jobe:ExchangedATRRC = True
        
            ! Inserting (DBH 01/08/2006) # 7962 - Only count jobs that have arrived at the ARC.
            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
            lot:RefNumber   = aud:Ref_Number
            lot:NewLocation = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
            If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
                ! Job never went to ARC location (DBH: 01/08/2006)
                Cycle
            End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
            ! End (DBH 01/08/2006) #7962
        
            ! Inserting (DBH 02/08/2006) # 7962 - Exclude 48 Hour Exchanges
        ! Changing (DBH 31/10/2006) # 8409 - Include 48 hour jobs for webmaster
        !    If jobe:Engineer48HourOption = 1
        ! to (DBH 31/10/2006) # 8409
            If jobe:Engineer48HourOption = 1 And ~glo:WebJob
        ! End (DBH 31/10/2006) #8409
                Cycle
            End ! If jobe:Engineer48HourOption = 1
            ! End (DBH 02/08/2006) #7962
        
            number:Manufacturer = job:Manufacturer
            number:ModelNumber  = job:Model_Number
            number:JobNumber    = job:Ref_Number
            number:RepairType   = 'E'
            number:ActionDate   = aud:Date
            Add(NumberQueue)
        End ! End AUDIT Loop
        
        Access:AUDIT.Clearkey(aud:ActionOnlyKey)
        aud:Action = 'JOB UPDATED'
        aud:Date = tmp:StartDate
        Set(aud:ActionOnlyKey,aud:ActionOnlyKey)
        Loop ! Begin AUDIT Loop
            If Access:AUDIT.Next()
                Break
            End ! If !Access
            If aud:Action <> 'JOB UPDATED'
                Break
            End ! If
            If aud:Date > tmp:EndDate
                Break
            End ! If
            recordsRead# += 1
            If prog.InsideLoop('Checking Jobs...(' & recordsRead# & ')')
                Break
            End ! If prog.InsideLoop()
        
            Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
            aud2:AUDRecordNumber = aud:Record_Number
            IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                If ~Instring('HUB REPAIR SELECTED',aud2:Notes,1,1)
                    Cycle
                End ! If ~Instring('HUB REPAIR SELECTED',aud:Notes,1,1)
            END ! IF
        
            If local.Criteria() = False
                Cycle
            End ! If local.Criteria() = False

! #13614 Remove date check. The location check should be enough (DBH: 12/10/2015)
!            If jobe:HubRepairDate = ''
!                ! Hub Repair may of been selected, but it never went anywhere (DBH: 14-06-2006)
!                Cycle
!            End ! If jobe:HubRepairDate = ''
        
            ! Inserting (DBH 01/08/2006) # 7962 - Make sure the job DID go to the ARC.
            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
            lot:RefNumber   = aud:Ref_Number
            lot:NewLocation = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
            If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
                ! Job never went to ARC location (DBH: 01/08/2006)
                Cycle
            End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
            ! End (DBH 01/08/2006) #7962
        
            number:Manufacturer = job:Manufacturer
            number:ModelNumber  = job:Model_Number
            number:JobNumber    = job:Ref_Number
            number:RepairType   = 'R'
            number:ActionDate   = aud:Date
            Add(NumberQueue)
        End ! End AUDIT Loop
        
        
        Prog.ProgressFinish()
        
        RecordsToProcess = Records(NumberQueue)
        RecordsProcessed = 0
        
        Prog.Init(Records(NumberQueue))

        RepQ.TotalRecords = RECORDS(NumberQueue)
        
        Case tmp:ReportOrder
            Of 0
                Sort(NumberQueue,number:JobNumber)
            Of 1
                Sort(NumberQueue,number:ActionDate)
            Of 2
                Sort(NumberQueue,number:Manufacturer,number:ModelNumber,number:JobNumber)
        End !tmp:ReportType
        
        Loop x# = 1 To Records(NumberQueue)
            Get(NumberQueue,x#)
            Yield()
            RecordsProcessed += 1
            recordsRead# += 1

            IF (RepQ.SBOReport)
                IF (RepQ.UpdateProgress())
                    BREAK
                END ! IF
            END ! IF

            If Prog.InsideLoop('Exporting...(' & recordsRead# & ')')
                Break
            End !If Prog.InsideLoop()
        
            Clear(ReportGroup)
        
            report:ActionDate = number:ActionDate
        
            case number:RepairType
                of 'R'
                    report:Type = 'REPAIR'
                of 'E'
                    report:Type = 'EXCH'
        ! Inserting (DBH 31/10/2006) # 8409 - Reinclude OBF jobs (just for webmaster)
                of 'O'
                    report:Type = 'OBF'
        ! End (DBH 31/10/2006) #8409
        
            end
        
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = number:JobNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
        
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        
        
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
        
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found
        
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number  = job:Account_Number
            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Found
            Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            report:CompanyName = Clip(job:Company_Name)
        
            report:Manufacturer = job:Manufacturer
            report:ModelNumber  = job:Model_Number
        
            if job:Warranty_Job = 'YES' and job:Chargeable_Job = 'YES'
                report:ChargeType = clip(job:Warranty_Charge_Type) & '/' & clip(job:Charge_Type)
            else
                !Add new prototype - L945 (DBH: 04-09-2003)
                JobPricingRoutine(0)
        
                if job:Warranty_Job = 'YES'
                    report:ChargeType   = job:Warranty_Charge_Type
                end
                if job:Chargeable_Job = 'YES'
                    report:ChargeType  = job:Charge_Type
                end
            end
        
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = wob:HeadAccountNumber
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                 report:JobNumber     = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
        
            report:HandlingFee = jobe:HandlingFee + jobe:ExchangeRate
        
            case number:RepairType
                of 'R'
                    if job:Exchange_Unit_Number <> 0
                        report:Repair = 'ARC'
                    else
                        if jobe:WebJob AND sentToHub(job:ref_number) = FALSE
                            report:Repair = 'RRC'
                        else
                            report:Repair = 'ARC'
                        end
                    end
                of 'E'
                    report:Repair = 'RRC'
            end
        
            !Add totals
            total:Lines             += 1
            total:HandlingFee       += report:HandlingFee
        
            if func:paper
                Print(rpt:Detail)
            ELSE
                Clear(gen:record)
                !Job Number
                gen:Line1   = Stripcomma(job:Ref_Number)
                !Add Engineer Option - 3788 (DBH: 19-04-2004)
                Case jobe:Engineer48HourOption
                    Of 1
                        tmp:EngineerOption = '48H'
                    Of 2
                        tmp:EngineerOption = 'ARC'
                    Of 3
                        tmp:EngineerOption = '7DT'
                    Of 4
                        tmp:EngineerOption = 'STD'
                    Else
                        tmp:EngineerOption = ''
                End !Case jobe:Engineer48HourOption
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(tmp:EngineerOption)
                !Trade Branch ID
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(tra:BranchIdentification)
                !Webjob Number
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(wob:JobNumber)
                !Action Date
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(format(report:ActionDate,@d6))
                !Account Number
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(job:Account_Number)
                !Company Name
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(report:CompanyName)
                !Manufacturer
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(report:Manufacturer)
                !Model Number
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(report:ModelNumber)
                !Charge Type
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(report:ChargeType)
                !RRC Waybill No
                !RRC Waybill Date
                Found# = False
                Access:JOBSCONS.ClearKey(joc:DateKey)
                joc:RefNumber = job:Ref_Number
                Set(joc:DateKey,joc:DateKey)
                Loop
                    If Access:JOBSCONS.NEXT()
                       Break
                    End !If
                    If joc:RefNumber <> job:Ref_Number      |
                        Then Break.  ! End If
                    If joc:DespatchFrom = 'RRC' And joc:DespatchTo = 'ARC'
                        gen:Line1   = Clip(gen:Line1) & ',' & StripComma(joc:ConsignmentNumber)
                        gen:Line1   = Clip(gen:Line1) & ',' & Format(joc:TheDate,@d06)
                        Found# = True
                        Break
                    End ! If joc:DespatchFrom = 'RRC' And joc:DespatchTo = 'ARC'
                End !Loop
                If Found# = False
                    gen:Line1 = Clip(gen:Line1) & ','
                    gen:Line1 = Clip(gen:Line1) & ','
                End ! If Found# = False
        
                !ARC Waybill Receipt Date
                Access:LOCATLOG.ClearKey(lot:NewLocationKey)
                lot:RefNumber   = job:Ref_Number
                lot:NewLocation = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
                If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                    ! Job never went to ARC location (DBH: 01/08/2006)
                    gen:Line1 = Clip(gen:Line1) & ',' & Format(lot:TheDate,@d06)
                Else !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
                    gen:Line1 = Clip(gen:Line1) & ','
                End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
                !Type
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(report:Type)
                !Repair
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(report:Repair)
                !Handling Fee
                gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Format(report:HandlingFee,@n_14.2))
        
                !TB13293 - J - 18/09/14 - add columns of  OriginalIMEI, BookingDate, ExchangeIMEI and ExchangeDate
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.Next() = Level:Benign
                    If jot:RefNumber = job:Ref_Number
                        gen:Line1   = Clip(gen:Line1) & ',''' & StripComma(jot:OriginalIMEI)
                    Else !If jot:RefNumber = job:Ref_Number
                        gen:Line1   = Clip(gen:Line1) & ',''' & StripComma(job:ESN)
                    End !If jot:RefNumber = job:Ref_Number
                Else !If Access:JOBTHIRD.Next() = Level:Benign
                    gen:Line1   = Clip(gen:Line1) & ',''' & StripComma(job:ESN)
                End !If Access:JOBTHIRD.Next() = Level:Benign
        
                gen:Line1   = Clip(gen:Line1) & ',' & format(job:date_booked,@d06)
        
                !if job:Exchange_Issued_Date > 0 and
                if job:Exchange_Unit_Number > 0 then
                    Access:Exchange.clearkey(xch:Ref_Number_Key)
                    xch:Ref_Number = job:Exchange_Unit_Number
                    if access:Exchange.fetch(xch:Ref_Number_Key)
                        !not found - show a blank
                        xch:ESN = ''
                    END
                    gen:Line1   = Clip(gen:Line1) & ',''' & StripComma(xch:ESN)
        
                    !J - 18/09/14 - I need to look up the allocation date from the audit trail job:Exchange_Issued_Date is not being filled in!!
                    ExchangeDate = ''
                    Access:Audit.clearkey(aud:Ref_Number_Key)
                    aud:Ref_Number = job:ref_number
                    aud:Date = today()
                    set(aud:Ref_Number_Key,aud:Ref_Number_Key)
                    Loop
                        if access:Audit.next() then break.
                        if aud:Ref_Number <> job:ref_number then break.
                        if aud:Action = 'EXCHANGE UNIT ATTACHED TO JOB' then
                            ExchangeDate = format(aud:Date,@d06)
                            break
                        END !if exchange unit
        
                    End !Audit loop
                    gen:Line1   = Clip(gen:Line1) & ',' & clip(ExchangeDate)
        
                !ELSE - no exchange - leave IMEI and date blank
                END !if job:Exchange_Issued_Date exists
        
                !END TB13293
                Access:EXPGEN.Insert()
            END
        
        End !x# = 1 To Records(NumberQueue)
        
        Prog.ProgressFinish()

    
        
        !added 3/12/02 - L386 - allow export to CSV file
        if func:paper
            !all done and finished
        ELSE
            !Print the final lines
            Clear(gen:Record)
            gen:Line1   = 'TOTALS'
            Access:EXPGEN.Insert()
        
            Clear(gen:Record)
            Gen:line1   =   'Jobs Counted,'&clip(Total:Lines)&',,,,,,,,,,,,,,'&clip(Format(total:HandlingFee,@n_14.2))
            Access:EXPGEN.Insert()
            Access:EXPGEN.close()

            IF (RepQ.SBOReport = TRUE)
                IF (RepQ.FinishReport('Handling And Exchange Fee Export.csv',glo:File_Name))
                END ! IF
                POST(Event:CloseWindow)
            ELSE ! IF
        
                if Total:lines = 0 then
                    If ~Command('/SCHEDULE')
                        Case Missive('There are no records that match the selected criteria.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    End ! If ~Command('/SCHEDULE')
                ELSE
                    if glo:webjob then
                        !send the file to the client
                        SendFileToClient(glo:File_Name)
                    ELSe
                        !copy it to where it is expected
                        copy(glo:file_name,Glo:CSV_SavePath)
                    END
            
                    If ~Command('/SCHEDULE')
                        Case Missive('Handling / Exchange Fee Report Created.','ServiceBase 3g',|
                                       'midea.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    End ! If ~Command('/SCHEDULE')
                END !if total:lines = 0

            END ! IF
            !remove the file
            
            Remove(glo:File_name)
        
        END!if tmp:paper
        ! End (DBH 14/06/2006) #7626
        
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF ShowEmpty = TRUE
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'c:\REPORT.TXT'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
        IF ShowEmpty = TRUE
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF ShowEmpty = TRUE
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
        IF ShowEmpty = TRUE
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','HandlingExchangeFeeReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DISCOUNT.Close
    Relate:EXCHANGE.Close
    Relate:JOBPAYMT.Close
    Relate:PAYTYPES.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Before Embed Point: %EndOfProcedure) DESC(End of Procedure) ARG()
  If func:Paper
  
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  End ! func:Paper
  ! After Embed Point: %EndOfProcedure) DESC(End of Procedure) ARG()
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF (RepQ.Init(COMMAND()) = Level:Benign)
      IF (local.LoadSBOnlineCriteria())
          RETURN
      ELSE
          0{prop:Hide} = 1
      END ! IF
  END ! IF
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
  tmp:AllAccounts = func:AllAccounts
  tmp:AllChargeTypes  = func:AllChargeTypes
  tmp:AllManufacturers    = func:AllManufacturers
  tmp:ReportOrder = func:ReportOrder
  
  tmp:DateRange   = Format(tmp:StartDate,@d6) & ' to ' & Format(tmp:EndDate,@d6)
  
  If ~glo:WebJob
      address:CompanyName     = def:User_Name
      address:AddressLine1    = def:Address_Line1
      address:AddressLine2    = def:Address_Line2
      address:AddressLine3    = def:Address_Line3
      address:Postcode        = def:Postcode
      address:TelephoneNumber = def:Telephone_Number
      address:FaxNumber       = def:Fax_Number
      address:EmailAddress    = def:EmailAddress
  Else !glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          address:CompanyName     = tra:Company_Name
          address:AddressLine1    = tra:Address_Line1
          address:AddressLine2    = tra:Address_Line2
          address:AddressLine3    = tra:Address_Line3
          address:Postcode        = tra:Postcode
          address:TelephoneNumber = tra:Telephone_Number
          address:FaxNumber       = tra:Fax_Number
          address:EmailAddress    = tra:EmailAddress
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  End !glo:WebJob
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='HandlingExchangeFeeReport'
  END
  report{Prop:Preview} = PrintPreviewImage


! Before Embed Point: %DBHProcedureSection) DESC(Procedure Local * (Place Local Procedure Code Here When Using Bryan's Progress Bar) *) ARG()
local.Criteria      Procedure()
Code
    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number  = aud:Ref_Number
    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        ! Found

    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        ! Error
        Return False
    End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

    Clear(NumberQueue)
    number:JobNumber = job:Ref_Number
    Get(NumberQueue,number:JobNumber)
    If ~Error()
        Return False
    End ! If ~Error()

    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
        Return False
    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Error
        Return False
    End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    If glo:WebJob
        If wob:HeadAccountNumber <> Clarionet:Global.Param2
            Return False
        End ! If wob:HeadAccountNumber <> Clarionet:Global.Param2
    Else ! If glo:WebJob
        If wob:HeadAccountNumber = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
            Return False
        End ! If wob:HeadAccountNumber = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
    End ! If glo:WebJob

    If ~tmp:AllAccounts
        Sort(glo:Queue,glo:Pointer)
        glo:Pointer = job:Account_Number
        Get(glo:Queue,glo:Pointer)
        If Error()
            Return False
        End !If Error()
    End !If ~tmp:AllAccounts

    If ~tmp:AllChargeTypes
        Sort(glo:Queue2,glo:Pointer2)
        glo:Pointer2    = job:Warranty_Charge_Type
        Get(glo:Queue2,glo:Pointer2)
        If Error()
            Return False
        End !If Error()
    End !If ~tmp:AllChargeTypes

    If ~tmp:AllManufacturers
        Sort(glo:Queue3,glo:Pointer3)
        glo:Pointer3    = job:Manufacturer
        Get(glo:Queue3,glo:Pointer3)
        If Error()
            Return False
        End !If Error()
    End !If ~tmp:AllManufacturers

    Return True
local.LoadSBOnlineCriteria    PROCEDURE()!,LONG
RetValue                    LONG(Level:Benign)
i                           LONG
qXML                        QUEUE(),PRE(qXml)
AllAccounts                     CSTRING(3)
GenericAccounts                 CSTRING(3)
AllChargeTypes                  CSTRING(3)
AllManufacturers                CSTRING(3) 
StartDate                       CSTRING(20)
EndDate                         CSTRING(20)
ReportOrder                     CSTRING(3) ! 0-Job, 1-Action, 2-Manufacturer
                                END! QUEUE
qAccounts       QUEUE(),PRE(qAccounts) 
AccountNumber       CSTRING(31)
                END!  QUEUE                               
qChargeTypes    QUEUE(),PRE(qChargeTypes)        
ChargeType          CSTRING(31)
                END! QUEUE
qManufacturers  QUEUE(),PRE(qManufacturers)    
Manufacturer        CSTRING(31)
                END ! QUEUE
    CODE
        LOOP 1 TIMES
            glo:WebJob = 1
            
            IF (XML:LoadFromFile(RepQ.FullCriteriaFilename))
                RetValue = Level:Fatal
                BREAK
            END ! IF
            XML:GotoTop

            IF (~XML:FindNextNode('Defaults'))
                recs# = XML:LoadQueue(qXML,1,1)
                IF (RECORDS(qXML) = 0)
                    RetValue = Level:Fatal
                    BREAK
                END ! IF
                GET(qXML, 1)
                !XML:DebugMyQueue(qXML,'qXML')
                func:StartDate = qXML.StartDate
                func:EndDate = qXML.EndDate
                func:Paper = 0
                func:AllAccounts = qXML.AllAccounts
                func:AllChargeTypes = qXML.AllChargeTypes
                func:AllManufacturers = qXML.AllManufacturers
                
                IF (qXML.AllAccounts <> 1)
                    XML:GotoTop()
                    recs# = XML:LoadQueue(qAccounts)
                    IF (RECORDS(qAccounts) > 0)
                        LOOP i = 1 TO RECORDS(qAccounts)
                            GET(qAccounts, i)
                            glo:Pointer = qAccounts.AccountNumber
                            ADD(glo:Queue)
                        END ! LOOP
                    END !I F
                END ! IF
                IF (qXML.AllChargeTypes <> 1)
                    XML:GotoTop()
                    recs# = XML:LoadQueue(qChargeTypes)
                    IF (RECORDS(qChargeTypes) > 0)
                        LOOP i = 1 TO RECORDS(qChargeTypes)
                            GET(qChargeTypes, i)
                            glo:Pointer2 = qChargeTypes.ChargeType
                            ADD(glo:Queue2)
                        END ! LOOP
                    END !I F
                END ! IF
                IF (qXML.AllManufacturers <> 1)
                    XML:GotoTop()
                    recs# = XML:LoadQueue(qManufacturers)
                    IF (RECORDS(qManufacturers) > 0)
                        LOOP i = 1 TO RECORDS(qManufacturers)
                            GET(qManufacturers, i)
                            glo:Pointer = qManufacturers.Manufacturer
                            ADD(glo:Queue3)
                        END ! LOOP
                    END !I F
                END ! IF                
                XML:Free()
            END ! IF
            
            

            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = RepQ.UserCode
            Access:USERS.TryFetch(use:User_Code_Key)
            Access:TRADEACC.ClearKey(tra:SiteLocationKey)
            tra:SiteLocation = use:Location
            Access:TRADEACC.TrYfetch(tra:SiteLocationKey)
            Clarionet:Global.Param2 = tra:Account_Number

            RepQ.UpdateStatus('Running')

        END ! BREAK LOOP

        IF (RetValue = Level:Fatal)
            RepQ.Failed()
        END !I F

        RETURN RetValue
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: UnivAbcReport
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE
! Before Embed Point: %DBHProgressSetupCode1) DESC(Progress Bar Setup Code Section) ARG()
        IF (RepQ.SBOReport)
            RETURN
        END ! IF
! After Embed Point: %DBHProgressSetupCode1) DESC(Progress Bar Setup Code Section) ARG()

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = ''
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE
! Before Embed Point: %DBHProgressLoopCode1) DESC(Progress Bar Inside Loop Code Section) ARG()
    IF (RepQ.SBOReport)
            RETURN 0
        END ! IF
! After Embed Point: %DBHProgressLoopCode1) DESC(Progress Bar Inside Loop Code Section) ARG()

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 100
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE
! Before Embed Point: %DBHProgressFinishCode1) DESC(Progress Bar Progress Finish Code Section) ARG()
           IF (RepQ.SBOReport)
            RETURN
        END ! IF
! After Embed Point: %DBHProgressFinishCode1) DESC(Progress Bar Progress Finish Code Section) ARG()

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
! After Embed Point: %DBHProcedureSection) DESC(Procedure Local * (Place Local Procedure Code Here When Using Bryan's Progress Bar) *) ARG()





