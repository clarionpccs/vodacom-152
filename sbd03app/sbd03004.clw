

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03004.INC'),ONCE        !Local module procedure declarations
                     END








Third_Party_Returns_Report PROCEDURE(func:Type)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_trb_id          USHORT,AUTO
tmp:batchqueue       QUEUE,PRE(trdque)
RecordNumber         LONG
Exchanged            STRING(3)
ModelNumber          STRING(30)
                     END
count_temp           LONG
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Accessories      STRING(255)
!-----------------------------------------------------------------------------
Process:View         VIEW(TRDBATCH)
                       PROJECT(trb:ESN)
                       PROJECT(trb:Ref_Number)
                       JOIN(job:Ref_Number_Key,trb:Ref_Number)
                         PROJECT(job:Current_Status)
                         PROJECT(job:MSN)
                         PROJECT(job:Manufacturer)
                         PROJECT(job:Model_Number)
                         PROJECT(job:Unit_Type)
                       END
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,865,7521,1396),USE(?unnamed)
                         STRING('3rd Party Agent:'),AT(5000,156),USE(?ThirdPartyAgent),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(GLO:Select3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(5000,365),USE(?DateRange),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5885,365),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Printed By:'),AT(5000,573),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,573),USE(tmp:PrintedBy),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,833),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,833),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,833),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@d6b),AT(6719,365),USE(GLO:Select2),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,1042),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,1042),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,1042),USE(?String26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,1042,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('To'),AT(6563,365,156,208),USE(?DateRangeTo),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,458),USE(?detailband),FONT('Arial',7,,)
                           STRING(@s20),AT(1708,0),USE(job:Manufacturer),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s16),AT(208,0),USE(trb:ESN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s11),AT(1094,0),USE(job:MSN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(2813,0),USE(job:Model_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(3906,0),USE(job:Unit_Type),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s8),AT(5510,0),USE(trb:Ref_Number),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s25),AT(6021,0),USE(job:Current_Status),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING('Accessories:'),AT(2188,156),USE(?String48),TRN,LEFT,FONT(,7,,)
                           TEXT,AT(2813,156,4531,156),USE(tmp:Accessories),TRN,RESIZE
                           LINE,AT(260,365,6979,0),USE(?Line1:2),COLOR(COLOR:Black)
                           STRING(@d6),AT(4969,0),USE(jot:DateIn),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(260,52,6979,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(260,104),USE(?String38),TRN,FONT(,8,,FONT:bold)
                           STRING(@n-14),AT(1667,104),USE(count_temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
CustomerTitle          DETAIL,AT(,,,344),USE(?CustomerTitle)
                         STRING('CUSTOMER UNITS'),AT(260,0),USE(?String40),TRN,FONT(,9,,FONT:bold)
                       END
ExchangedTitle         DETAIL,AT(,,,365),USE(?ExchangeTitle)
                         STRING('EXCHANGED UNITS'),AT(260,0),USE(?String40:2),TRN,FONT(,9,,FONT:bold)
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('3RD PARTY RETURNS NOTE'),AT(4063,52,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING('Unit Type'),AT(3906,2083),USE(?string25:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Rcvd'),AT(4969,2083),USE(?string25:3),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Job No'),AT(5625,2083),USE(?string25:4),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Current Status'),AT(6021,2083),USE(?String46),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('I.M.E.I. No'),AT(208,2083),USE(?string44),TRN,FONT(,7,,FONT:bold)
                         STRING('M.S.N.'),AT(1094,2083),USE(?string45),TRN,FONT(,7,,FONT:bold)
                         STRING('Manufacturer'),AT(1708,2083),USE(?string24),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Model Number'),AT(2813,2083),USE(?string25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Third_Party_Returns_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Third_Party_Returns_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = '3rdPartyRet'
  If PrintOption(PreviewReq,glo:ExportReport,'Third Party Returns Report') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select2',GLO:Select2)
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:TRDBATCH.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Access:JOBS.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBACC.UseFile
  Access:TRDACC.UseFile
  
  
  RecordsToProcess = RECORDS(TRDBATCH)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(TRDBATCH,'QUICKSCAN=on')
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess    = Records(trdbatch)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        If func:Type = 0
            !Add all the models into a queue
            Save_trb_ID = Access:TRDBATCH.SaveFile()
            Access:TRDBATCH.ClearKey(trb:CompanyDateKey)
            trb:Company_Name = glo:Select3
            trb:Status       = 'IN'
            trb:DateReturn   = glo:Select1
            Set(trb:CompanyDateKey,trb:CompanyDateKey)
            Loop
                If Access:TRDBATCH.NEXT()
                   Break
                End !If
                If trb:Company_Name <> glo:Select3      |
                Or trb:Status       <> 'IN'      |
                Or trb:DateReturn    > glo:Select2      |
                    Then Break.  ! End If
                RecordsProcessed    += 1
                Do DisplayProgress
                trdque:RecordNumber = trb:RecordNumber
                trdque:Exchanged    = trb:Exchanged
        
                access:JOBS.clearkey(job:Ref_Number_Key)
                job:Ref_Number  = trb:Ref_Number
                If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Found
                    trdque:ModelNumber  = job:Model_Number
                    Add(tmp:BatchQueue)
                Else! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                End! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
        
            End !Loop
            Access:TRDBATCH.RestoreFile(Save_trb_ID)
        Else !func:Type = 0
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                Access:TRDBATCH.Clearkey(trb:RecordNumberKey)
                trb:RecordNumber    = glo:Pointer
                If Access:TRDBATCH.Tryfetch(trb:RecordNumberKey) = Level:Benign
                    !Found
                    trdque:RecordNumber = trb:RecordNumber
                    trdque:Exchanged    = trb:Exchanged
                    access:JOBS.clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = trb:Ref_Number
                    If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                        trdque:ModelNumber  = job:Model_Number
                        Add(tmp:BatchQueue)
                    Else! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                    End! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
        
                Else ! If Access:TRDBATCH.Tryfetch(trd:RecordNumberKey) = Level:Benign
                    !Error
                End !If Access:TRDBATCH.Tryfetch(trd:RecordNumberKey) = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)
        End !func:Type = 0
        
        !Resort the queue into Exchanged/Model Number order
        
        Sort(tmp:BatchQueue,trdque:Exchanged,trdque:Modelnumber)
        first_cust# = 1
        first_exch# = 1
        
        RecordsToProcess    = Records(tmp:BatchQueue)
        
        Loop x# = 1 To Records(tmp:BatchQueue)
            Get(tmp:BatchQueue,x#)
            RecordsProcessed    += 1
            Do DisplayProgress
            access:TRDBATCH.clearkey(trb:RecordNumberKey)
            trb:RecordNumber    = trdque:RecordNumber
            If access:TRDBATCH.tryfetch(trb:RecordNumberKey) = Level:Benign
                !Found
                access:JOBS.clearkey(job:Ref_Number_Key)
                job:Ref_Number  = trb:Ref_Number
                If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Found
                Else! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                End! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
        
                access:JOBTHIRD.clearkey(jot:ThirdPartyKey)
                jot:ThirdPartyNumber    = trb:RecordNumber
                If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
                    !Found
        
                    ! Display the accessories on the report - TrkBs: 6264 (DBH: 08-09-2005)
                    tmp:Accessories = ''
                    Access:TRDACC.ClearKey(trr:AccessoryKey)
                    trr:RefNumber = trb:RecordNumber
                    Set(trr:AccessoryKey,trr:AccessoryKey)
                    Loop
                        If Access:TRDACC.NEXT()
                           Break
                        End !If
                        If trr:RefNumber <> trb:RecordNumber      |
                            Then Break.  ! End If
                        tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(trr:Accessory)
                    End !Loop
                    tmp:Accessories = Clip(Sub(tmp:Accessories,3,255))
                    ! End   - Display the accessories on the report - TrkBs: 6264 (DBH: 08-09-2005)
        
                    If trdque:exchanged = 'NO'
                        If first_cust# = 1
                            Print(rpt:customertitle)
                            count_temp = 0
                            first_cust# = 0
                        End!If first# = 1
                        count_temp += 1
                        Print(rpt:detail)
                    End!If trdque:exchanged = 'NO'
                    If trdque:exchanged = 'YES'
                        If first_exch# = 1
                            Print(rpt:exchangedtitle)
                            count_temp = 0
                            first_exch# = 0
                        End!If first_exch# = 1
                        count_temp += 1
                        Print(rpt:detail)
                    End!If trdque:exchanged = 'YES'
                Else! If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
                    !Error
                End! If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
            Else! If access:TRDBATCH.tryfetch(trb:RecordNumberKey) = Level:Benign
                !Error
            End! If access:TRDBATCH.tryfetch(trb:RecordNumberKey) = Level:Benign
        End!Loop x# = 1 To Records(tmp:batchqueue)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(TRDBATCH,'QUICKSCAN=off').
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','Third_Party_Returns_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBACC.Close
    Relate:STANTEXT.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  Settarget(Report)
  If func:Type = 1
      ?glo:Select1{prop:Hide} = 1
      ?glo:Select2{prop:Hide} = 1
      ?DateRange{prop:Hide} = 1
      ?DateRangeTo{prop:Hide} = 1
      ?glo:Select3{prop:Hide} = 1
      ?ThirdPartyAgent{prop:Hide} = 1
  End !func:Type = 1
  Settarget()
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Third_Party_Returns_Report'
  END
  report{Prop:Preview} = PrintPreviewImage







Stock_Value_Detailed_Criteria PROCEDURE               !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::4:TAGDISPSTATUS    BYTE(0)
DASBRW::4:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_sto_id          USHORT,AUTO
save_stm_id          USHORT,AUTO
Location_Temp        STRING(30)
tag_temp             STRING(1)
tmp:ReportType       BYTE(0)
tmp:OutputType       BYTE(0)
tmp:ExportFile       STRING(255)
tmp:ExportFilename   STRING(255),STATIC
stock_queue          QUEUE,PRE(stoque)
site_location        STRING(30)
manufacturer         STRING(30)
total_stock          REAL
purchase_value       REAL
AveragePurchaseCost  REAL
sale_value           REAL
                     END
stock_queue_temp     QUEUE,PRE(stoque)
ref_number           LONG
part_number          STRING(30)
description          STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
BRW3::View:Browse    VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:RecordNumber)
                       PROJECT(los:Site_Location)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:RecordNumber       LIKE(los:RecordNumber)         !Primary key field - type derived from field
los:Site_Location      LIKE(los:Site_Location)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Stock Value Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Site Location'),AT(68,112),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s30),AT(144,112,124,10),USE(GLO:Select1),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       LIST,AT(144,158,146,160),USE(?List),IMM,DISABLE,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@120L(2)|M~Shelf Location~@s30@'),FROM(Queue:Browse)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,54,552,310),USE(?Panel6),FILL(09A6A7CH)
                       OPTION('Report Type'),AT(144,126,148,28),USE(tmp:ReportType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('Summary'),AT(153,139),USE(?tmp:ReportType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                         RADIO('Detailed'),AT(240,139),USE(?tmp:ReportType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                       END
                       BUTTON('&Rev tags'),AT(200,182,1,1),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(200,198,1,1),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(120,322),USE(?DASTAG),DISABLE,TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                       BUTTON,AT(188,322),USE(?DASTAGAll),DISABLE,TRN,FLAT,LEFT,ICON('tagallp.jpg')
                       BUTTON,AT(256,322),USE(?DASUNTAGALL),DISABLE,TRN,FLAT,LEFT,ICON('untagalp.jpg')
                       CHECK('Include Accessories In Report'),AT(416,178),USE(GLO:Select2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Value Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PROMPT('Export Path'),AT(384,274),USE(?tmp:ExportFile:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s255),AT(384,286,184,10),USE(tmp:ExportFile),SKIP,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Export Path'),TIP('Export Path'),UPR,READONLY
                       BUTTON,AT(572,282),USE(?LookupFile),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                       CHECK('Suppress Zeros'),AT(416,194),USE(GLO:Select3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       OPTION('Report Output Type'),AT(384,220,184,28),USE(tmp:OutputType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('Paper Report'),AT(392,233),USE(?tmp:OutputType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                         RADIO('Export File'),AT(504,233),USE(?tmp:OutputType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                       END
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
FileLookup10         SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!RepQ Definitions
    MAP
LoadSBOnlineCriteria    PROCEDURE(),LONG
    END
RepQ    RepQClass
ExportFile    File,Driver('ASCII'),Pre(expfil),Name(tmp:ExportFileName),Create,Bindable,Thread
Record                  Record
Line                    String(1000)
                        End
                    End
TempFilePath         CSTRING(255)
tmp:DesktopFolder    Cstring(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::4:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW3.UpdateBuffer
   glo:Queue.Pointer = los:Shelf_Location
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = los:Shelf_Location
     ADD(glo:Queue,glo:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(glo:Queue)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = los:Shelf_Location
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::4:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::4:QUEUE = glo:Queue
    ADD(DASBRW::4:QUEUE)
  END
  FREE(glo:Queue)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::4:QUEUE.Pointer = los:Shelf_Location
     GET(DASBRW::4:QUEUE,DASBRW::4:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = los:Shelf_Location
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASSHOWTAG Routine
   CASE DASBRW::4:TAGDISPSTATUS
   OF 0
      DASBRW::4:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::4:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::4:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
SummaryExport       Routine
Data
local:SiteLocation      String(30)
local:TotalLines        Long
local:TotalTotalStockItems  Long
local:TotalPurchaseValue    Real
!Add Average Purchase Cost to export - TrkBs: 3277 (DBH: 16-02-2005)
local:TotalAveragePurchaseCost  Real
local:TotalSaleValue        Real
local:FileName          String(255)

Code
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            tmp:ExportFileName = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            tmp:ExportFileName = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
        tmp:ExportFileName = Clip(tmp:ExportFileName) & 'STOCKVAL' & Today() & Clock() & '.TMP'

        Remove(tmp:ExportFileName)

        Create(ExportFile)
        Open(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"STOCK VALUE REPORT (SUMMARY)"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Site Location","' & Clip(glo:Select1) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Include Accessories","' & Clip(glo:Select2) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Suppress Zeros","' & Clip(glo:Select3) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Printed By","' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Date Printed","' & Format(Today(),@d6) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"' & Clip('Site Location')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Manufacturer')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Total Stock Items')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('In Warranty Value')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Average Purchase Cost')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Out Warranty Value') & '"'
        Add(ExportFile)

        Prog.Init(Records(STOCK))
        !Build the manufacturer list - 3277  (DBH: 17-06-2004)
        Access:STOCK.ClearKey(sto:Location_Key)
        sto:Location    = glo:Select1
        Set(sto:Location_Key,sto:Location_Key)
        Loop
            If Access:STOCK.NEXT()
               Break
            End !If
            If sto:Location    <> glo:Select1      |
                Then Break.  ! End If
            If Prog.InsideLoop()
                Break
            End !If Prog.InsideLoop()
            if (sto:Suspend)
                Cycle
            end
            !Include accessories? - 3277  (DBH: 17-06-2004)
            If glo:Select2 <> 'YES'
                If sto:Accessory = 'YES'
                    Cycle
                End !If sto:Accessory = 'YES'
            End !If glo:Select3 <> 'YES'

            !Include zero value parts? - 3277  (DBH: 17-06-2004)
            If glo:Select3 = 'YES' and sto:Quantity_Stock = 0
                Cycle
            End !If glo:Select4 = 'YES' and sto:Quantity_Stock = 0

            Sort(stock_queue,stoque:site_location,stoque:manufacturer)
            stoque:site_location    = sto:location
            stoque:manufacturer     = sto:manufacturer
            Get(stock_queue,stoque:site_location,stoque:manufacturer)
            If Error()
                stoque:site_location    = sto:location
                stoque:manufacturer     = sto:manufacturer
                stoque:total_stock      = sto:quantity_stock
                stoque:purchase_value   = Round(sto:purchase_cost * sto:quantity_stock,.01)
                stoque:AveragePurchaseCost = Round(sto:AveragePurchaseCost * sto:Quantity_Stock,.01)
                stoque:sale_value       = Round(sto:sale_cost * sto:quantity_stock,.01)
                Add(stock_queue)
            Else!If Error()
                stoque:total_stock      += sto:quantity_stock
                stoque:purchase_value   += Round(sto:purchase_cost * sto:quantity_stock,.01)
                stoque:AveragePurchaseCost += Round(sto:AveragePurchaseCost * sto:Quantity_Stock,.01)
                stoque:sale_value       += Round(sto:sale_cost * sto:quantity_stock,.01)
                Put(stock_queue)
            End!If Error()
        End !Loop

        Prog.ProgressFinish()
        Prog.Init(Records(Stock_Queue))

        RepQ.TotalRecords = RECORDS(Stock_Queue)

        If Records(stock_queue)
            count# = 0

            Sort(stock_queue,stoque:site_location,stoque:manufacturer)
            Loop x# = 1 To Records(stock_queue)
                Get(stock_queue,x#)

                IF (RepQ.SBOReport)
                    IF (RepQ.UpdateProgress())
                        BREAK
                    END ! IF
                END ! IF

                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop()

                If count# = 0
                    location" = stoque:site_location
                    local:SiteLocation = stoque:site_location
                Else!If count_temp = 0
                    If location" <> stoque:site_location
                        local:SiteLocation = stoque:site_location
                        location" = stoque:site_location
                    Else!If location" <> stoque:site_locatoin
                        local:SiteLocation = ''
                    End!If location" <> stoque:site_locatoin
                End!If count_temp = 0

                count# += 1

                Clear(expfil:Record)
                expfil:Line = '"' & Clip(local:SiteLocation)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(stoque:Manufacturer)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(stoque:Total_Stock)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(stoque:Purchase_Value,@n_14.2))
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(stoque:AveragePurchaseCost,@n_14.2))
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(stoque:Sale_Value,@n_14.2)) & '"'
                Add(ExportFile)

                local:TotalLines += 1
                local:TotalTotalStockItems  += stoque:Total_Stock
                local:TotalPurchaseValue    += stoque:Purchase_Value
                local:TotalAveragePurchaseCost += stoque:AveragePurchaseCost
                local:TotalSaleValue        += stoque:Sale_Value
            End!Loop x# = 1 To Records(stock_queue)

            Clear(expfil:Record)
            expfil:Line = '"' & Clip('Total Number Of Lines')
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:TotalLines)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:TotalTotalStockItems)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(local:TotalPurchaseValue,@n_14.2))
            expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(local:TotalAveragePurchaseCost,@n_14.2))
            expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(local:TotalSaleValue,@n_14.2)) & '"'
            Add(ExportFile)
        End!If Records(stock_queue)

        Prog.ProgressFinish()
        Close(ExportFile)

        local:FileName = 'STOCK VALUE REPORT (SUMMARY) ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'

        !Make sure the is only one trailing backslash - TrkBs: 3277 (DBH: 21-02-2005)
        If Sub(tmp:ExportFile,-1,1) = '\'
        Else !If Sub(TempFilePath,-1,1) = '\'
            tmp:ExportFile = Clip(tmp:ExportFile) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'

        IF (RepQ.SBOReport = TRUE)
            IF (RepQ.FinishReport('Stock Value Report (Summary).csv',tmp:ExportFileName))
            END ! IF
            REMOVE(tmp:ExportFileName)
            POST(EVent:CloseWindow)
        ELSE ! IF

            If glo:WebJob
                Remove(Clip(TempFilePath) & CLip(local:FileName))
                Rename(ExportFile,Clip(TempFilePath) & Clip(local:FileName))
                SendFileToClient(Clip(TempFilePath) & Clip(local:FileName))
            Else !If glo:WebJob
                Remove(Clip(tmp:ExportFile) & Clip(local:FileName))
                Copy(ExportFile,Clip(tmp:ExportFile) & Clip(local:FileName))
            End !If glo:WebJob
            !Remove the temp file - 3277  (DBH: 18-06-2004)
            Remove(Clip(TempFilePath) & Clip(local:FileName))

            If ~Command('/SCHEDULE')
                Case Missive('Export Completed.','ServiceBase 3g',|
                               'midea.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            End ! If ~Command('/SCHEDULE')
        END ! IF

    End


DetailedExport       Routine
Data
local:LineCost      Real
local:FirstModel    String(30)
local:NumberOfLines Long
local:TotalLineCost Real
local:FileName      String(255)
Code
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            tmp:ExportFileName = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            tmp:ExportFileName = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
        tmp:ExportFileName = Clip(tmp:ExportFileName) & 'STOCKVAL' & Today() & Clock() & '.TMP'

        Remove(tmp:ExportFileName)

        Create(ExportFile)
        Open(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"STOCK VALUE REPORT (DETAILED)"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Site Location","' & Clip(glo:Select1) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Include Accessories","' & Clip(glo:Select2) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Suppress Zeros","' & Clip(glo:Select3) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Printed By","' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Date Printed","' & Format(Today(),@d6) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"' & Clip('Part Number')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Description')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('First Model Number')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Order If Below')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Out Warranty Value')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Qty In Stock')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('In Warranty Value')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Average Purchase Cost')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Line Cost') &'"'
        Add(ExportFile)

        Prog.Init(Records(glo:Queue))

        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            save_sto_id = access:stock.savefile()
            access:stock.clearkey(sto:shelf_location_key)
            sto:location       = glo:select1
            sto:shelf_location = glo:pointer
            set(sto:shelf_location_key,sto:shelf_location_key)
            loop
                if access:stock.next()
                   break
                end !if
                if sto:location       <> glo:select1      |
                or sto:shelf_location <> glo:pointer      |
                    then break.  ! end if
                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop()
                if (sto:Suspend)
                    Cycle
                end

                If glo:select2 <> 'YES'
                    If sto:accessory = 'YES'
                        Cycle
                    End!If sto:accessory = 'YES'
                End!If glo:select2 = 'YES'
                If glo:select3 = 'YES'
                    If sto:quantity_stock = 0
                        Cycle
                    End!If sto:quantity_stock = 0
                End!If glo:select3 = 'YES'
                stoque:ref_number   = sto:ref_number
                stoque:part_number  = sto:part_number
                stoque:description  = sto:description
                add(stock_queue_temp)
            end !loop
            access:stock.restorefile(save_sto_id)
        End!Loop x# = 1 To Records(glo:Queue)

        Prog.ProgressFinish()
        Prog.Init(Records(Stock_Queue_Temp))

        RepQ.TotalRecords = RECORDS(Stock_Queue_Temp)

        Sort(stock_queue_temp,stoque:part_number,stoque:description)
        Loop x# = 1 To Records(stock_queue_temp)
            Get(stock_queue_temp,x#)

            IF (RepQ.SBOReport)
                IF (RepQ.UpdateProgress())
                    BREAK
                END ! IF
            END ! IF

            If Prog.InsideLoop()
                Break
            End !If Prog.InsideLoop()

            local:LineCost = 0
            access:stock.clearkey(sto:ref_number_key)
            sto:ref_number = stoque:ref_number
            if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                !Line cost is worked out from avg purchase cost - TrkBs: 3277 (DBH: 16-02-2005)
                local:LineCost = Round(sto:AveragePurchaseCost * sto:Quantity_Stock,.01)

        !Get First Model
                local:FirstModel = ''
                save_stm_id = access:stomodel.savefile()
                access:stomodel.clearkey(stm:model_number_key)
                stm:ref_number   = sto:ref_number
                stm:manufacturer = sto:manufacturer
                set(stm:model_number_key,stm:model_number_key)
                loop
                    if access:stomodel.next()
                       break
                    end !if
                    if stm:ref_number   <> sto:ref_number      |
                    or stm:manufacturer <> sto:manufacturer      |
                        then break.  ! end if
                    local:FirstModel = stm:model_number
                    Break
                end !loop
                access:stomodel.restorefile(save_stm_id)

                Clear(expfil:Record)
                expfil:Line    = '"''' & Clip(sto:Part_Number)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(sto:Description)
                expfil:Line = Clip(expfil:Line) & '","''' & Clip(local:FirstModel)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(sto:Minimum_Level)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(sto:Sale_Cost,@n_14.2))
                expfil:Line = Clip(expfil:Line) & '","' & Clip(sto:Quantity_Stock)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(sto:Purchase_Cost,@n_14.2))
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(sto:AveragePurchaseCost,@n_14.2))
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(local:LineCost,@n_14.2)) & '"'
                Add(ExportFile)
                local:NumberOfLines += sto:Quantity_Stock
                local:TotalLineCost += local:LineCost
            end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
        End!Loop x# = 1 To Records(stock_queue_temp)

        Clear(expfil:Record)
        expfil:Line = '"Total Number Of Lines","","","","","' & Clip(local:NumberOfLines) & '","","","' & |
                    Format(local:TotalLineCost,@n_14.2) & '"'
        Add(ExportFile)

        Prog.ProgressFinish()
        Close(ExportFile)

        local:FileName = 'STOCK VALUE REPORT (DETAILED) ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'

        !Make sure the is only one trailing backslash - TrkBs: 3277 (DBH: 21-02-2005)
        If Sub(tmp:ExportFile,-1,1) = '\'
        Else !If Sub(TempFilePath,-1,1) = '\'
            tmp:ExportFile = Clip(tmp:ExportFile) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
        IF (RepQ.SBOReport = TRUE)
            IF (RepQ.FinishReport('Stock Value Report (Detailed).csv',tmp:ExportFileName))
            END ! IF
            REMOVE(tmp:ExportFileName)
            POST(EVent:CloseWindow)
        ELSE ! IF
            If glo:WebJob
                Remove(Clip(TempFilePath) & CLip(local:FileName))
                Rename(ExportFile,Clip(TempFilePath) & Clip(local:FileName))
                SendFileToClient(Clip(TempFilePath) & Clip(local:FileName))
            Else !If glo:WebJob
                Remove(Clip(tmp:ExportFile) & Clip(local:FileName))
                Copy(ExportFile,Clip(tmp:ExportFile) & Clip(local:FileName))
            End !If glo:WebJob
            !Remove the temp file - 3277  (DBH: 18-06-2004)
            Remove(Clip(TempFilePath) & Clip(local:FileName))

            If ~Command('/SCHEDULE')
                Case Missive('Export Completed.','ServiceBase 3g',|
                               'midea.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            End ! If ~Command('/SCHEDULE')
        END ! IF
    End


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020184'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Stock_Value_Detailed_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Relate:REPSCHCR.Open
  Access:TRADEACC.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLG.UseFile
  Access:REPSCHSL.UseFile
  SELF.FilesOpened = True
  glo:select1 = ''
  glo:select2 = 'NO'
  glo:select3 = 'NO'
  
  !Autofill the Site Location when run from Webmaster - 3277 (DBH: 16-06-2004)
  If glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  =  Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          glo:Select1 = tra:SiteLocation
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  End !glo:WebJob
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  tmp:ExportFile = def:ExportPath
  If tmp:ExportFile[Len(Clip(tmp:ExportFile))] <> '\'
      tmp:ExportFile = Clip(tmp:ExportFile) & '\'
  End !tmp:ExportPath[Len(Clip(tmp:ExportPath))] <> '\'
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:LOCSHELF,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (RepQ.Init(COMMAND()) = Level:Benign)
     0{prop:Hide} = 1
  END!  IF
  !Do not allow the RRC to change the Site Location - 3277 (DBH: 16-06-2004)
  If glo:WebJob
      ?glo:Select1{prop:Disable} = True
  End !glo:WebJob
      ! Save Window Name
   AddToLog('Window','Open','Stock_Value_Detailed_Criteria')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW3.Q &= Queue:Browse
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,los:Shelf_Location_Key)
  BRW3.AddRange(los:Site_Location,GLO:Select1)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,los:Shelf_Location,1,BRW3)
  BIND('tag_temp',tag_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(tag_temp,BRW3.Q.tag_temp)
  BRW3.AddField(los:Shelf_Location,BRW3.Q.los:Shelf_Location)
  BRW3.AddField(los:RecordNumber,BRW3.Q.los:RecordNumber)
  BRW3.AddField(los:Site_Location,BRW3.Q.los:Site_Location)
  FDCB2.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(loc:Location_Key)
  FDCB2.AddField(loc:Location,FDCB2.Q.loc:Location)
  FDCB2.AddField(loc:RecordNumber,FDCB2.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FileLookup10.Init
  FileLookup10.Flags=BOR(FileLookup10.Flags,FILE:LongName)
  FileLookup10.Flags=BOR(FileLookup10.Flags,FILE:Directory)
  FileLookup10.SetMask('All Files','*.*')
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
              ! Inserting (DBH 23/01/2008) # 9711 - Add "detailed" version of report
              If rpd:ReportName = 'Stock Value Detailed Report'
                  tmp:ReportType = 2
                  Access:REPSCHSL.Clearkey(rpf:LocationKey)
                  rpf:REPSCHCRRecordNumber = rpc:RecordNumber
                  Set(rpf:LocationKey,rpf:LocationKey)
                  Loop
                      If Access:REPSCHSL.Next()
                          Break
                      End ! If Access:REPSCHSL.Next()
                      If rpf:REPSCHCRRecordNumber <> rpc:RecordNumber
                          Break
                      End ! If rpf:REPSCHCRRecordNumber <> rpc:RecordNumber
                      glo:Pointer = rpf:ShelfLocation
                      Add(glo:Queue)
                  End ! Loop
                  Brw3.ResetSort(1)
              Else ! If rpd:ReportName = 'Stock Value Detailed Report'
                  ! End (DBH 23/01/2008) #9711
                  tmp:ReportType = 1
              End ! If rpd:ReportName = 'Stock Value Detailed Report'
  
              glo:Select1 = rpc:SiteLocation
  
              If rpc:IncludeAccessories
                  glo:Select2 = 'YES'
              Else ! If rpc:IncludeAccessories
                  glo:Select2 = 'NO'
              End ! If rpc:IncludeAccessories
  
              If rpc:WarIncSuppressZeros
                  glo:Select3 = 'YES'
              Else ! If rpc:WarIncSuppressZeros
                  glo:Select3 = 'NO'
              End ! If rpc:WarIncSuppressZeros
  
              tmp:OutputType = 2
              SHGetSpecialFolderPath( GetDesktopWindow(), tmp:DesktopFolder, 16, FALSE )
              tmp:DesktopFolder = tmp:DesktopFolder & '\ServiceBase Export'
              If ~Exists(tmp:DesktopFolder)
                  If ~MkDir(tmp:DesktopFolder)
                      ! Can't create desktop folder
                  End ! If ~MkDir(tmp:DesktopFolder)
              End ! If ~Exists(tmp:DesktopFolder)
  
              tmp:ExportFile = tmp:DesktopFolder
              Post(Event:Accepted,?OKButton)
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  IF (RepQ.SBOReport = TRUE)
      IF (LoadSBOnlineCriteria())
          RETURN RequestCancelled
      END ! IF
  END ! IF
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  glo:select1 = ''
  glo:select2 = ''
  glo:select3 = ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
    Relate:REPSCHCR.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Stock_Value_Detailed_Criteria')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?GLO:Select1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
      BRW3.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
    OF ?tmp:ReportType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ReportType, Accepted)
      Case tmp:ReportType
          Of 1
              ?List{prop:Disable} = True
              ?Dastag{prop:Disable} = True
              ?Dastagall{prop:Disable} = True
              ?DasUntagall{prop:Disable} = True
          Else
              ?List{prop:Disable} = False
              ?Dastag{prop:Disable} = False
              ?Dastagall{prop:Disable} = False
              ?DasUntagall{prop:Disable} = False
      End !tmp:ReportType
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ReportType, Accepted)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020184'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020184'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020184'&'0')
      ***
    OF ?LookupFile
      ThisWindow.Update
      tmp:ExportFile = Upper(FileLookup10.Ask(1)  )
      DISPLAY
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      !Add error checking - 3277 (DBH: 17-06-2004)
      IF (RepQ.SBOReport <> TRUE)
          If glo:Select1 = ''
              Case Missive('You must select a Site Location.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !glo:Select1 = ''
      
          IF tmp:ReportType = 0
              Case Missive('You must select a Report Type.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !tmp:ReportType = 0
      
          If tmp:OutputType = 0
              Case Missive('You must select a Report Output Type.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !tmp:ExportType = 0
      
          If tmp:ReportType = 2
              If ~Records(glo:Queue)
                  Case Missive('You must select at least one Site Location.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              End !If ~Records(glo:Queue)
          End !tmp:ReportType = 2
      END ! IF
      
      Case tmp:ReportType
          Of 1 !Summary
              Case tmp:OutputType
                  Of 1 !Paper
                      StockValueSummary
                  Of 2 !Export
                      Do SummaryExport
              End !Case tmp:OutputType
          Of 2 !Detailed
              Case tmp:OutputType
                  Of 1 !Paper
                      Stock_Value_Report
                  Of 2 !Export
                      Do DetailedExport
              End !Case tmp:OutputType
      End !tmp:ReportType
      
      !Stock_Value_Report
          If Command('/SCHEDULE')
              If Access:REPSCHLG.PrimeRecord() = Level:Benign
                  rlg:REPSCHEDRecordNumber = rpd:RecordNumber
                  rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                    '<13,10>Report Finished'
                  If Access:REPSCHLG.TryInsert() = Level:Benign
                      !Insert
                  Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                      Access:REPSCHLG.CancelAutoInc()
                  End ! If Access:REPSCHLG.TryInsert() = Level:Benign
              End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
          End
      IF (RepQ.SBOReport = TRUE)
          POST(EVENT:CloseWindow)
      END! I F
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?tmp:OutputType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:OutputType, Accepted)
      !Show Hide Export Path - 3277  (DBH: 18-06-2004)
      If ~glo:WebJob
          Case tmp:OutputType
              Of 2
                  ?tmp:ExportFile{prop:Hide} = False
                  ?tmp:ExportFile:Prompt{prop:Hide} = False
                  ?LookupFile{prop:Hide} = False
              Else
                  ?tmp:ExportFile{prop:Hide} = True
                  ?tmp:ExportFile:Prompt{prop:Hide} = True
                  ?LookupFile{prop:Hide} = True
          End !tmp:OutputType
      End !glo:WebJob
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:OutputType, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?Dastag)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      !Do DASBRW::4:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
!Load RepQ Criteria
LoadSBOnlineCriteria    PROCEDURE()!,LONG
RetValue                    LONG(Level:Benign)
i                           LONG
qXML                        QUEUE(),PRE(qXml)
ReportType                      CSTRING(3)
AllShelfLocations               CSTRING(3)
IncludeAccessories              CSTRING(3)
SuppressZeros                   CSTRING(3)
                            END! QUEUE
qShelfLocations             QUEUE(),PRE(qShelfLocations)
ShelfLocation                   CSTRING(31)
                            END ! QUEUE
    CODE
        LOOP 1 TIMES
            glo:WebJob = 1
            
            IF (XML:LoadFromFile(RepQ.FullCriteriaFilename))
                RetValue = Level:Fatal
                BREAK
            END ! IF

            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = RepQ.UserCode
            Access:USERS.TryFetch(use:User_Code_Key)
            Clarionet:Global.Param2 = repq:AccountNumber
            glo:Select1 = use:Location

            XML:GotoTop

            IF (~XML:FindNextNode('Defaults'))
                recs# = XML:LoadQueue(qXML,1,1)
                IF (RECORDS(qXML) = 0)
                    RetValue = Level:Fatal
                    BREAK
                END ! IF
                GET(qXML, 1)
                !XML:DebugMyQueue(qXML,'qXML')
                tmp:ReportType = CHOOSE(qXML.ReportType = 1, 2, 1)
                glo:Select2 = qXML.IncludeAccessories
                glo:Select3 = qXML.SuppressZeros
                
                IF (qXML.AllShelfLocations = 1)
                    Access:LOCSHELF.ClearKey(los:Shelf_Location_Key)
                    los:Site_Location = glo:Select1
                    SET(los:Shelf_Location_Key,los:Shelf_Location_Key)
                    LOOP UNTIL Access:LOCSHELF.Next() <> Level:Benign
                        IF (los:Site_Location <> glo:Select1)
                            BREAK
                        END!  IF
                        glo:Pointer = los:Shelf_Location
                        ADD(glo:Queue)
                    END !LOOP
                ELSE
                    XML:GotoTop()
                    IF (~XML:FindNextNode('ShelfLocations'))
                        recs# = XML:LoadQueue(qShelfLocations)
                        IF (RECORDS(qShelfLocations) > 0)
                            LOOP i = 1 TO RECORDS(qShelfLocations)
                                GET(qShelfLocations, i)
                                glo:Pointer = qShelfLocations.ShelfLocation
                                ADD(glo:Queue)
                            END ! LOOP
                        END !I F
                    END ! IF
                END ! IF
 
                XML:Free()
            END ! IF


            RepQ.UpdateStatus('Running')

            IF (tmp:reportType = 2)
                DO DetailedExport
            ELSE
                DO SummaryExport
            END!  IF
            !DO Export

        END ! BREAK LOOP

        IF (RetValue = Level:Fatal)
            RepQ.UpdateStatus('Failed',,1)
        END !I F

        RETURN RetValue
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

FDCB2.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1


BRW3.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = los:Shelf_Location
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = los:Shelf_Location
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::4:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE
! Before Embed Point: %DBHProgressSetupCode1) DESC(Progress Bar Setup Code Section) ARG()
    IF (RepQ.SBOReport)
        RETURN
    END ! IF
! After Embed Point: %DBHProgressSetupCode1) DESC(Progress Bar Setup Code Section) ARG()

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE
! Before Embed Point: %DBHProgressLoopCode1) DESC(Progress Bar Inside Loop Code Section) ARG()
    IF (RepQ.SBOReport)
        RETURN 0
    END ! IF
! After Embed Point: %DBHProgressLoopCode1) DESC(Progress Bar Inside Loop Code Section) ARG()

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 100
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE
! Before Embed Point: %DBHProgressFinishCode1) DESC(Progress Bar Progress Finish Code Section) ARG()
    IF (RepQ.SBOReport)
        RETURN
    END ! IF
! After Embed Point: %DBHProgressFinishCode1) DESC(Progress Bar Progress Finish Code Section) ARG()

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()






Stock_Value_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
save_stm_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
save_tradeacc_id     USHORT,AUTO
tmp:DefaultFax       STRING(20)
save_sto_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
stock_queue_temp     QUEUE,PRE(stoque)
ref_number           LONG
part_number          STRING(30)
description          STRING(30)
                     END
site_location_temp   STRING(30)
count_temp           STRING(9)
average_price_temp   REAL
count_average_temp   REAL
job_count_temp       REAL
total_jobs_temp      REAL
Manufacturer_Temp    STRING(30)
total_stock_temp     STRING(12)
sale_value_temp      REAL
purchase_value_temp  REAL
purchase_value_total_temp REAL
sale_value_total_temp REAL
total_stock_total_temp STRING(12)
location_temp        STRING(30)
tmp:PrintedBy        STRING(60)
line_cost_temp       REAL
line_cost_total_temp REAL
unit_cost_total_temp REAL
quantity_total_temp  LONG
tmp:firstmodel       STRING(30)
address:UserName     STRING(30)
address:AddressLine1 STRING(30)
address:AddressLine2 STRING(30)
address:AddressLine3 STRING(30)
address:Postcode     STRING(30)
address:TelephoneNumber STRING(30)
address:FaxNumber    STRING(30)
address:EmailAddress STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(LOCATION)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

Report               REPORT('Stock Value Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,781,7521,2094),USE(?unnamed)
                         STRING('Inc. Accessories:'),AT(4948,313),USE(?String22),TRN,FONT(,8,,)
                         STRING(@s40),AT(5885,313),USE(GLO:Select2),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(5885,469),USE(GLO:Select3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Suppress Zeros:'),AT(4948,469),USE(?String22:3),TRN,FONT(,8,,)
                         STRING(@s40),AT(5885,156),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Site Location:'),AT(4948,156),USE(?String22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(4948,833),USE(?String27),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(4948,1250),USE(?String59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,1250),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6198,1250),USE(?String34),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6458,1250,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(5885,833),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(4948,1042),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5885,1042),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,229),USE(?DetailBand)
                           STRING(@s20),AT(156,0,1198,156),USE(sto:Part_Number),TRN,FONT(,7,,)
                           STRING(@s30),AT(1250,0),USE(sto:Description),TRN,FONT(,7,,)
                           STRING(@s15),AT(3073,0,781,156),USE(tmp:firstmodel),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@S6),AT(3875,0),USE(sto:Minimum_Level),TRN,RIGHT,FONT(,7,,)
                           STRING(@N10.2),AT(4427,0),USE(sto:Sale_Cost),TRN,RIGHT,FONT(,7,,)
                           STRING(@S8),AT(5104,0),USE(sto:Quantity_Stock),TRN,RIGHT,FONT(,7,,)
                           STRING(@N10.2),AT(5698,0),USE(sto:Purchase_Cost),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(6844,0),USE(line_cost_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(6281,0),USE(sto:AveragePurchaseCost),TRN,RIGHT,FONT(,7,,)
                         END
Totals                   DETAIL,AT(,,,302),USE(?totals)
                           LINE,AT(208,52,7083,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(208,104),USE(?String26),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(5010,104),USE(quantity_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n10.2),AT(6750,104),USE(line_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@s9),AT(2083,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?IMage1)
                         STRING(@s30),AT(156,0,3844,240),USE(address:UserName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('STOCK VALUE REPORT'),AT(4271,0,3177,260),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(address:AddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(address:AddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(address:AddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1042),USE(address:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354),USE(address:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('Description'),AT(1250,2083),USE(?String44),TRN,FONT(,7,,FONT:bold)
                         STRING('Out Warranty'),AT(4490,2021),USE(?String44:3),TRN,FONT(,7,,FONT:bold)
                         STRING('If Below'),AT(3958,2135),USE(?String24:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING(' Cost '),AT(5885,2135),USE(?String44:4),TRN,FONT(,7,,FONT:bold)
                         STRING(' Cost '),AT(6479,2135),USE(?String44:10),TRN,FONT(,7,,FONT:bold)
                         STRING(' Cost '),AT(4646,2135),USE(?String44:9),TRN,FONT(,7,,FONT:bold)
                         STRING('Purchase'),AT(6417,2021),USE(?String44:8),TRN,FONT(,7,,FONT:bold)
                         STRING('Qty In'),AT(5313,2021),USE(?String45:2),TRN,FONT(,7,,FONT:bold)
                         STRING('First Model No'),AT(3073,2083),USE(?String44:7),TRN,FONT(,7,,FONT:bold)
                         STRING('In Warranty'),AT(5750,2021),USE(?String44:6),TRN,FONT(,7,,FONT:bold)
                         STRING('Stock'),AT(5313,2135),USE(?String44:5),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(156,2083),USE(?String44:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Order'),AT(4010,2031),USE(?String45),TRN,FONT(,7,,FONT:bold)
                         STRING('Line Cost'),AT(6927,2083),USE(?String25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Stock_Value_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Stock_Value_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:LOCATION.Open
  Relate:DEFAULTS.Open
  Access:STOCK.UseFile
  Access:USERS.UseFile
  Access:STOMODEL.UseFile
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(LOCATION)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(LOCATION,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = Records(glo:Queue)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        setcursor(cursor:wait)
        
        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            save_sto_id = access:stock.savefile()
            access:stock.clearkey(sto:shelf_location_key)
            sto:location       = glo:select1
            sto:shelf_location = glo:pointer
            set(sto:shelf_location_key,sto:shelf_location_key)
            loop
                if access:stock.next()
                   break
                end !if
                if sto:location       <> glo:select1      |
                or sto:shelf_location <> glo:pointer      |
                    then break.  ! end if
                RecordsProcessed += 1
                Do DisplayProgress
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                If glo:select2 <> 'YES'
                    If sto:accessory = 'YES'
                        Cycle
                    End!If sto:accessory = 'YES'
                End!If glo:select2 = 'YES'
                If glo:select3 = 'YES'
                    If sto:quantity_stock = 0
                        Cycle
                    End!If sto:quantity_stock = 0
                End!If glo:select3 = 'YES'
                stoque:ref_number   = sto:ref_number
                stoque:part_number  = sto:part_number
                stoque:description  = sto:description
                add(stock_queue_temp)
            end !loop
            access:stock.restorefile(save_sto_id)
        End!Loop x# = 1 To Records(glo:Queue)
        
        setcursor()
        line_cost_total_temp = 0
        unit_cost_total_temp = 0
        quantity_total_temp = 0
        
        !While I'm in here, add an accurate progress bar - TrkBs: 3277 (DBH: 16-02-2005)
        Prog.ProgressSetup(Records(Stock_Queue_Temp))
        
        Sort(stock_queue_temp,stoque:part_number,stoque:description)
        Loop x# = 1 To Records(stock_queue_temp)
            Get(stock_queue_temp,x#)
            If Prog.InsideLoop()
                !Can't cancel
            End ! If Prog.InsideLoop()
            line_cost_temp = 0
            access:stock.clearkey(sto:ref_number_key)
            sto:ref_number = stoque:ref_number
            if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                !Line cost = Average Purchase Cost * Quantity - TrkBs: 3277 (DBH: 15-02-2005)
                line_cost_temp = Round(sto:AveragePurchaseCost * sto:quantity_stock,.01)
                line_cost_total_temp += line_cost_temp
                unit_cost_total_temp += sto:AveragePurchaseCost
                quantity_total_temp += sto:quantity_stock
        
        !Get First Model
                tmp:FirstModel = ''
                save_stm_id = access:stomodel.savefile()
                access:stomodel.clearkey(stm:model_number_key)
                stm:ref_number   = sto:ref_number
                stm:manufacturer = sto:manufacturer
                set(stm:model_number_key,stm:model_number_key)
                loop
                    if access:stomodel.next()
                       break
                    end !if
                    if stm:ref_number   <> sto:ref_number      |
                    or stm:manufacturer <> sto:manufacturer      |
                        then break.  ! end if
                    tmp:FirstModel = stm:model_number
                    Break
                end !loop
                access:stomodel.restorefile(save_stm_id)
        
                Print(rpt:detail)
            end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
        End!Loop x# = 1 To Records(stock_queue_temp)
        If records(stock_queue_temp)
            !Show the line count - TrkBs: 3277 (DBH: 16-02-2005)
            Count_Temp = Records(Stock_Queue_Temp)
            Print(rpt:totals)
        End!If records(stock_queue_temp)
        
        Prog.ProgressFinish()
        
        
        
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(LOCATION,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Stock_Value_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !Display Correct Address - 3277  (DBH: 17-06-2004)
  If glo:WebJob
      save_TRADEACC_id = Access:TRADEACC.SaveFile()
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          address:UserName        = tra:Company_Name
          address:AddressLine1    = tra:Address_Line1
          address:AddressLine2    = tra:Address_Line2
          address:AddressLine3    = tra:Address_Line3
          address:Postcode        = tra:Postcode
          address:TelephoneNumber = tra:Telephone_Number
          address:FaxNumber       = tra:Fax_Number
          address:EmailAddress    = tra:EmailAddress
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Access:TRADEACC.RestoreFile(save_TRADEACC_id)
  Else !glo:WebJob
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      address:UserName        = def:User_Name
      address:AddressLine1    = def:Address_Line1
      address:AddressLine2    = def:Address_Line2
      address:AddressLine3    = def:Address_Line3
      address:Postcode        = def:Postcode
      address:TelephoneNumber = def:Telephone_Number
      address:FaxNumber       = def:Fax_Number
      address:EmailAddress    = def:EmailAddress
  End !glo:WebJob
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Stock_Value_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage


Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: UnivAbcReport
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0











Stock_Check_Report PROCEDURE(func:Type)
! Before Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
    MAP
BuildQ      PROCEDURE(LONG pType)
    END ! MAP

qStock      QUEUE(),PRE(qStock)
RefNumber        LONG()
            END ! QUEUE

i LONG()
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
! After Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
RejectRecord         LONG,AUTO
Local                CLASS
Validation           Procedure(),Byte
                     END
used_temp            LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
line_temp            LONG
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
quantity_total_temp  LONG
count_temp           LONG
stock_queue_temp     QUEUE,PRE(stoque)
ref_number           LONG
part_number          STRING(30)
description          STRING(30)
second_location      STRING(30)
                     END
tmp:PrintedBy        STRING(60)
code_temp            BYTE
option_temp          BYTE
bar_code_string_temp CSTRING(21)
bar_code_temp        CSTRING(21)
tmp:FirstModel       STRING(20)
Address:User_Name    STRING(30)
address:Address_Line1 STRING(30)
address:Address_Line2 STRING(30)
address:Address_Line3 STRING(30)
address:Post_Code    STRING(20)
Address:Telephone_Number STRING(20)
address:FaxNumber    STRING(30)
address:Email        STRING(255)
ExportFilename       STRING(255)
ExportDirectory      CSTRING(255)
DisplayStockQty      STRING(20)
WinPrint             CLASS(CWin32file)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(STOCK)
                       PROJECT(sto:Description)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Purchase_Cost)
                       PROJECT(sto:Quantity_Stock)
                       PROJECT(sto:Second_Location)
                       PROJECT(sto:Shelf_Location)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

Report               REPORT('Stock Value Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,781,7521,2094),USE(?unnamed:2)
                         STRING('Inc. Accessories:'),AT(4948,469),USE(?IncludeAccessories),TRN,FONT(,8,,)
                         STRING(@s40),AT(5885,469),USE(GLO:Select2),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(5885,625),USE(GLO:Select3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Price Band:'),AT(4948,792),USE(?PriceBand),TRN,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s40),AT(5885,792),USE(GLO:Select9),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Suppress Zeros:'),AT(4948,625),USE(?SuppressZeros),TRN,FONT(,8,,)
                         STRING(@s40),AT(5885,156),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5885,313),USE(GLO:Select4),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Manufacturer:'),AT(4948,313),USE(?Manufacturer),TRN,FONT(,8,,)
                         STRING('Site Location:'),AT(4948,156),USE(?String22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(4948,948),USE(?String27),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(4948,1260),USE(?String59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,1260),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,1260,156,208),USE(?String38),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6354,1260,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(5885,948),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Report Date:'),AT(4948,1104),USE(?ReportDatePrompt),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@d6b),AT(5885,1104),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed)
DETAIL                   DETAIL,AT(,,,177),USE(?DetailBand)
                           STRING(@s15),AT(2344,0,1354,208),USE(bar_code_temp),LEFT,FONT('C128 High 12pt LJ3',10,,,CHARSET:ANSI)
                           STRING(@s12),AT(156,0),USE(sto:Shelf_Location),TRN,FONT(,7,,)
                           STRING(@s12),AT(833,0),USE(sto:Second_Location),TRN,FONT(,7,,)
                           STRING(@s15),AT(1510,0),USE(sto:Part_Number),TRN,FONT(,7,,)
                           STRING(@s20),AT(4531,0),USE(sto:Description),TRN,FONT(,7,,)
                           STRING(@s6),AT(6563,0),USE(sto:Quantity_Stock),TRN,RIGHT,FONT(,7,,)
                           BOX,AT(6979,0,156,150),USE(?Box1),COLOR(COLOR:Black)
                           STRING(@n10.2),AT(5625,0,521,104),USE(sto:Purchase_Cost),TRN,RIGHT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s5),AT(6198,0),USE(used_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s14),AT(3750,0),USE(tmp:FirstModel),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           LINE,AT(7188,104,208,0),USE(?Line2),COLOR(COLOR:Black)
                         END
Totals                   DETAIL,AT(,,,458),USE(?totals)
                           LINE,AT(208,52,7083,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(208,104),USE(?String26),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(6365,104),USE(quantity_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('Total Items:'),AT(5573,104),USE(?totalitems),TRN,FONT(,8,,FONT:bold)
                           STRING(@s9),AT(2083,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(10,31,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(Address:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('STOCK CHECK REPORT'),AT(4271,0,3177,260),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(address:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(address:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(address:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s20),AT(156,729,1156,156),USE(address:Post_Code),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(Address:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(address:Email),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('2nd Loc'),AT(833,2083),USE(?String44),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(1510,2083),USE(?String44:3),TRN,FONT(,7,,FONT:bold)
                         STRING('Description'),AT(4531,2083),USE(?String44:4),TRN,FONT(,7,,FONT:bold)
                         STRING('Shelf Loc'),AT(156,2083),USE(?String44:2),TRN,FONT(,7,,FONT:bold)
                         STRING('In Stock'),AT(6667,2083),USE(?Instock),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Count'),AT(7083,2083),USE(?String25:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Purch. Cost'),AT(5583,2083),USE(?String44:7),TRN,FONT(,7,,FONT:bold)
                         STRING('Usage'),AT(6302,2083),USE(?Instock:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Part Number'),AT(2344,2083),USE(?String44:6),TRN,FONT(,7,,FONT:bold)
                         STRING('1st Model'),AT(3750,2083),USE(?String44:5),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Stock_Check_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Stock_Check_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:STOCK.Open
  Relate:DEFAULTS.Open
  Relate:PRIBAND.Open
  Access:STOMODEL.UseFile
  Access:TRADEACC.UseFile
  Access:LOCATION.UseFile
  ! Before Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  If glo:WebJob
  
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          address:User_Name        = tra:Company_Name
          address:Address_Line1    = tra:Address_Line1
          address:Address_Line2    = tra:Address_Line2
          address:Address_Line3    = tra:Address_Line3
          address:Post_code        = tra:Postcode
          address:Telephone_Number = tra:Telephone_Number
          address:FaxNumber        = tra:Fax_Number
          address:Email            = tra:EmailAddress
      Else ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  Else !glo:WebJob
  
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      address:User_Name         = def:User_Name
      address:Address_Line1     = def:Address_Line1
      address:Address_Line2     = def:Address_Line2
      address:Address_Line3     = def:Address_Line3
      address:Post_code         = def:Postcode
      address:Telephone_Number  = def:Telephone_Number
      address:FaxNumber         = def:Fax_Number
      address:Email             = def:EmailAddress
  
  
  End !glo:WebJob
  
  ExportFilename = SetReportsFolder('ServiceBase Export','Stock Check Report',glo:WebJob) & 'Stock_Check_'&format(Today(),@d12)&'_'&format(Clock(),@t5)&'.csv'
  
      BuildQ(glo:Select5)
  
      IF (glo:Select12 <> 'Print')
          ! #978 Moved the export process here. It seems if any Clarionet code was called it would crash WM. (DBH: 20/08/2013)
          DO Printing
  
          RETURN
      END ! IF
  
      
  ! After Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  
  
  RecordsToProcess = RECORDS(STOCK)
  RecordsPerCycle = 10
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(STOCK,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess    = RECORDS(qStock)
      
      !look up the price band
      if clip(GLO:Select9) = '' then
          !leave this alone
      ELSE
          Access:priband.clearkey(prb:KeyBandName)
          prb:BandName = Glo:Select9
          if access:Priband.fetch(prb:KeyBandName)
              !error ??
              Glo:Select9 = ''
          END
      END
      
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !change the report
        IF (glo:Select12 = 'Print')
            DO Printing
        END
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(STOCK,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        OMIT('**End Omit Nothing Message**')
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
        OMIT('**End Omit Nothing Message**')
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        OMIT('**End Omit Nothing Message**')
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
        OMIT('**End Omit Nothing Message**')
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Stock_Check_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
    Relate:PRIBAND.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Stock Check'
  END
  Report{Prop:Preview} = PrintPreviewImage


! Before Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()
Printing        ROUTINE
    IF (glo:Select12 = 'Print')

        Settarget(Report)

            If glo:select6 = 0        !don't show stock quanity on report or CSV
                    Hide(?instock)
                    Hide(?sto:quantity_stock)
                    Hide(?totalitems)
                    HIde(?quantity_total_temp)
            End!If glo:select6 = 0


        !07/01/13 - hide stock quanities for both sets of calls - so only thing to do is above this line

        !    If func:Type = 0
        !        If glo:select6 = 0
        !                Hide(?instock)
        !                Hide(?sto:quantity_stock)
        !                Hide(?totalitems)
        !                HIde(?quantity_total_temp)
        !        End!If glo:select6 = 0
        !    Else
        !        !audit types    Removed this hide on 3/1/13
        !        !?Manufacturer{prop:Hide} = 1
        !        !?IncludeAccessories{prop:Hide} = 1
        !        !?SuppressZeros{prop:Hide} = 1
        !        !?glo:Select4{prop:Hide} = 1
        !        !?glo:Select2{prop:Hide} = 1
        !        !?glo:Select3{prop:Hide} = 1
        !        !?GLO:Select9{prop:Hide} = 1
        !        !?PriceBand{prop:Hide} = 1
        !    End !func:Type = 0

            if GLO:Select11 = 'HIDE' then
                hide(?used_temp)
                hide(?Instock:2)
            END

        Settarget()
    END ! IF


    if glo:Select12 <> 'Print'
        prog.ProgressSetup(RECORDS(qStock))
        prog.ProgressText('Exporting...')

        WinPrint.init(ExportFilename,Append_mode)

        WinPrint.write('"'&clip(Address:User_Name)               &'",,,,"'& 'Site Location'   &'","'&  clip(Glo:select1)    &'"<13,10>'&|
                       '"'&clip(address:Address_Line1)           &'",,,,"'& 'Manufacturer'    &'","'&  clip(Glo:Select4)    &'"<13,10>'&|
                       '"'&clip(address:Address_Line2)           &'",,,,"'& 'Inc Accessories' &'","'&  clip(Glo:Select2)    &'"<13,10>'&|
                       '"'&clip(address:Address_Line3)           &'",,,,"'& 'Suppress Zeros'  &'","'&  clip(Glo:select3)    &'"<13,10>'&|
                       '"'&clip(address:Post_Code)               &'",,,,"'& 'Price Band'      &'","'&  clip(Glo:Select9)    &'"<13,10>'&|
                       '"TEL: '  &clip(Address:Telephone_Number) &'",,,,"'& 'Printed By'      &'","'&  clip(tmp:PrintedBy)  &'"<13,10>'&|
                       '"FAX: '  &clip(address:FaxNumber)        &'",,,,"'& 'Report Date'     &'","'&  format(Today(),@d06) &'"<13,10>'&|
                       '"EMAIL: '&clip(address:Email)            &'"<13,10,13,10>')

        IF GLO:Select6 = 1 then     !show stock quanitity in export
            DisplayStockQty = ',"In Stock"'
        ELSE
            DisplayStockQty = ''
        END
        if GLO:Select11 = 'HIDE' then
            Winprint.write('"Shelf Location","2nd Location","Part Number","1st Model","Description","Purch Cost"'&clip(DisplayStockQty)&'<13,10>')
        ELSE
            Winprint.write('"Shelf Location","2nd Location","Part Number","1st Model","Description","Purch Cost","Usage"'&clip(DisplayStockQty)&'<13,10>')
        END !hiding usage

    END !if not printing

    code_temp = 3
    option_temp = 0
    quantity_total_temp = 0
    count_temp = 0

        LOOP i = 1 TO RECORDS(qStock)
            GET(qStock,i)
            Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number = qStock.RefNumber
            IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
                CYCLE
            END ! IF

            IF (glo:Select12 = 'Print')
                RecordsProcessed += 1
                DO DisplayProgress
            ELSE
                IF (prog.InsideLoop())
                    BREAK
                END ! IF
            END !IF

            !Get the first model attached to this part, 
            !code moved here before print(detail) or winprint.write() - TB13160 - JC - 30/09/13 to get right "first model"
            tmp:FirstModel = ''
            access:stomodel.clearkey(stm:model_number_key)
            stm:ref_number   = sto:ref_number
            stm:manufacturer = sto:manufacturer
            set(stm:model_number_key,stm:model_number_key)
            loop
                if access:stomodel.next() then break.
                if stm:ref_number   <> sto:ref_number then break.
                if stm:manufacturer <> sto:manufacturer then break.
                !got a matching model
                tmp:FirstModel = stm:model_number
                Break
            end !loop

            quantity_total_temp += sto:quantity_stock
            count_temp += 1
            tmp:RecordsCount += 1
            if glo:Select12 = 'Print'
                bar_code_string_temp = Clip(sto:part_number)
                sequence2(code_temp,option_temp,bar_code_string_temp,bar_code_temp)
                Print(rpt:detail)
            ELSE
                If glo:select6 = 1        !show stock quanity on report or CSV
                    DisplayStockQty = clip(sto:Quantity_Stock)
                ELSE
                    DisplayStockQty = ''
                END



                if GLO:Select11 = 'HIDE' then
                    Winprint.write('"'&Clip(sto:Shelf_Location) &'","'& |
                               Clip(sto:Second_Location)        &'","'& |
                               Clip(sto:Part_Number)            &'","'& |
                               Clip(tmp:FirstModel)             &'","'& |
                               Clip(sto:Description)            &'","'& |
                               Format(sto:Purchase_Cost,@n10.2) &'","'& |          !change from sto:AveragePurchaseCost - TB12462 JC 3/1/13
                               Clip(DisplayStockQty)&'"<13,10>' )
                ELSE
                    Winprint.write('"'&Clip(sto:Shelf_Location) &'","'& |
                               Clip(sto:Second_Location)        &'","'& |
                               Clip(sto:Part_Number)            &'","'& |
                               Clip(tmp:FirstModel)             &'","'& |
                               Clip(sto:Description)            &'","'& |
                               Format(sto:Purchase_Cost,@n10.2) &'","'& |          !change from sto:AveragePurchaseCost - TB12462 JC 3/1/13
                               Clip(used_temp)                  &'","'& |
                               Clip(DisplayStockQty)&'"<13,10>' )

                END !hiding usage

            END
            
            
        END ! LOOP

    Winprint.close()

    if glo:Select12 = 'Print'
        If count_temp <> 0
            Print(rpt:totals)
        End !If records(stock_queue_temp)
    ELSE
        !not bring printed - and at the end
        prog.ProgressFinish()
        
        if glo:WebJob then
            SendFileToClient(ExportFilename)
            Remove(ExportFilename)
            Miss# = missive('CSV Export Complete','ServiceBase 3g','Mexclam.jpg','OK') ! #978 Don't know the final path in WM (DBH: 20/08/2013)
        ELSE
            Miss# = missive('CSV Export Complete : |'&clip(ExportFilename),'ServiceBase 3g','Mexclam.jpg','OK')
        END
    END !if glo:Select12 = 'Print'
! After Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()
! Before Embed Point: %DBHProcedureSection) DESC(Procedure Local * (Place Local Procedure Code Here When Using Bryan's Progress Bar) *) ARG()
Local.Validation      Procedure()
code

    !Include suspended stock
    if clip(glo:Select10) <> 'YES'
        !exclude stock
        if sto:Suspend = true  then
            return(level:fatal)
        END !if stock suspended
    END

    If glo:select2 <> 'YES'
        If sto:accessory = 'YES'
            Return Level:Fatal
        End!If sto:accessory = 'YES'
    End!If glo:select2 = 'YES'

    !include zero quanitiry?
    If glo:select3 = 'YES'
        If sto:quantity_stock = 0
            Return Level:Fatal
        End!If sto:quantity_stock = 0
    End!If glo:select3 = 'YES'

    !select manufacturer
    If glo:select4 <> ''
        If sto:manufacturer <> glo:select4
            Return Level:Fatal
        End!If sto:manufacturer <> glo:select4
    End!If glo:select4 <> ''

    !pricebanding added
    if clip(glo:Select9) <> '' then
        !if sto:AveragePurchaseCost < prb:MinPrice or sto:AveragePurchaseCost > prb:MaxPrice then    TB13019 should have been purchase_Cost J 5/03/13
        if sto:Purchase_Cost  < prb:MinPrice or sto:Purchase_Cost > prb:MaxPrice then
            !outisde the banding system
            return(level:Fatal)
        END
    END

!    !Get the first model attached to this part
!not wanted here - this only validates the job
!    tmp:FirstModel = ''
!    access:stomodel.clearkey(stm:model_number_key)
!    stm:ref_number   = sto:ref_number
!    stm:manufacturer = sto:manufacturer
!    set(stm:model_number_key,stm:model_number_key)
!    loop
!        if access:stomodel.next()
!           break
!        end !if
!        if stm:ref_number   <> sto:ref_number      |
!        or stm:manufacturer <> sto:manufacturer      |
!            then break.  ! end if
!        tmp:FirstModel = stm:model_number
!        Break
!    end !loop

    !Get Usage only if they want it TB12462
    if GLO:Select11 = 'SHOW' then

        used_temp = 0
        !save_shi_id = access:stohist.savefile()
        access:stohist.clearkey(shi:ref_number_key)
        shi:ref_number = sto:ref_number
        shi:date       = glo:select7
        set(shi:ref_number_key,shi:ref_number_key)
        loop
            if access:stohist.next()
               break
            end !if
            if shi:ref_number <> sto:ref_number      |
            or shi:date       > glo:select8      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            If shi:job_number = ''
                Cycle
            End
            If shi:notes = 'STOCK READJUSTED FROM SERVICEBASE DOS'
                Cycle
            End!If shi:action = 'STOCK READJUSTED FROM SERVICEBASE DOS'
            If shi:notes = 'STOCK DECREMENTED' and shi:job_number = ''
                Cycle
            End!If shi:notes = 'STOCK DECREMENTED'
            If shi:transaction_type = 'DEC'
                Used_Temp += shi:quantity
            End!If shi:transaction_type = 'DEC'
            If shi:transaction_type = 'REC'
                Used_Temp -= shi:quantity
            End!If shi:transaction_type = 'REC'
        end !loop
    END !if GLO:Select11 = 'SHOW' then

    Return Level:Benign

BuildQ      PROCEDURE(LONG pType) 
    CODE
        prog.ProgressSetup(1000)
        prog.ProgressText('Building List Of Parts...')
        
        IF (pType = 1) ! Location Order
            LOOP i = 1 TO RECORDS(glo:Queue)
                GET(glo:Queue,i)
                            
                Access:STOCK.ClearKey(sto:SecondLocKey)
                sto:Location = glo:Select1
                sto:Shelf_Location = glo:Pointer
                SET(sto:SecondLocKey,sto:SecondLocKey)
                LOOP UNTIL Access:STOCK.Next() <> Level:Benign
                    IF (sto:Location <> glo:Select1 OR |
                        sto:Shelf_Location <> glo:Pointer)
                        BREAK
                    END ! IF
                    
                    IF (prog.InsideLoop())
                        BREAK
                    END ! IF
                    
                    IF (Local.Validation() = Level:Benign)
                        qStock.RefNumber = sto:Ref_Number
                        ADD(qStock)
                    END ! IF
                END ! LOOP
            END ! LOOP
        END ! 
        
        IF (pType = 2) ! Description Order
            Access:STOCK.ClearKey(sto:Description_Key)
            sto:Location     = glo:Select1
            SET(sto:Description_Key,sto:Description_Key)
            LOOP UNTIL Access:STOCK.Next() <> Level:Benign
                IF (sto:Location <> glo:Select1)
                    BREAK
                END ! IF
                
                IF (prog.InsideLoop())
                    BREAK
                END ! IF
                    
                glo:Pointer = sto:Shelf_Location
                GET(glo:Queue,glo:Pointer)
                IF (ERROR())
                    CYCLE
                END ! IF
                
                IF (Local.Validation() = Level:Benign)
                    qStock.RefNumber = sto:Ref_Number
                    ADD(qStock)
                END ! IF
            END ! LOOP
        END ! IF
        
        prog.ProgressFinish()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: UnivAbcReport
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = ''
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 500
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            return 1
        Of Button:No
        End !CASE
    End!If cancel# = 1

    return 0
! After Embed Point: %DBHProcedureSection) DESC(Procedure Local * (Place Local Procedure Code Here When Using Bryan's Progress Bar) *) ARG()





Stock_Check_Criteria PROCEDURE (SentType)             !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::4:TAGFLAG          BYTE(0)
DASBRW::4:TAGMOUSE         BYTE(0)
DASBRW::4:TAGDISPSTATUS    BYTE(0)
DASBRW::4:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
Location_Temp        STRING(30)
SaveFileName         STRING(255),STATIC
Tmp:Count            LONG
tag_temp             STRING(1)
tmp:showquantity     BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
AllMan               BYTE
AllPrb               BYTE
locTRUE              BYTE(1)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:Active             LIKE(loc:Active)               !List box control field - type derived from field
loc:Active_NormalFG    LONG                           !Normal forground color
loc:Active_NormalBG    LONG                           !Normal background color
loc:Active_SelectedFG  LONG                           !Selected forground color
loc:Active_SelectedBG  LONG                           !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select4
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:Active)
                       PROJECT(loc:RecordNumber)
                     END
FDCB8::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
BRW3::View:Browse    VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:RecordNumber)
                       PROJECT(los:Site_Location)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:RecordNumber       LIKE(los:RecordNumber)         !Primary key field - type derived from field
los:Site_Location      LIKE(los:Site_Location)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB15::View:FileDrop VIEW(PRIBAND)
                       PROJECT(prb:BandName)
                       PROJECT(prb:MinPrice)
                       PROJECT(prb:RecordNumber)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select9
prb:BandName           LIKE(prb:BandName)             !List box control field - type derived from field
prb:MinPrice           LIKE(prb:MinPrice)             !List box control field - type derived from field
prb:RecordNumber       LIKE(prb:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Stock Check Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,54,552,310),USE(?Panel6),FILL(09A6A7CH)
                       PROMPT('Pick Site/Shelf Locations'),AT(68,82),USE(?Prompt7),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Pick Manufacturer or Price Band and Date Range'),AT(384,82),USE(?Prompt9),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s30),AT(144,102,124,10),USE(GLO:Select1),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M*@s30@0L(2)|M*@n1@'),DROP(10),FROM(Queue:FileDropCombo)
                       LINE,AT(338,126,0,214),USE(?Line1),COLOR(COLOR:White),LINEWIDTH(2)
                       PROMPT('Site Location'),AT(68,102),USE(?Prompt8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       LIST,AT(144,118,124,204),USE(?List),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Shelf Location~@s30@'),FROM(Queue:Browse)
                       PROMPT('Price Band'),AT(384,118),USE(?Prompt10),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       LIST,AT(456,118,124,10),USE(GLO:Select9),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('80L(2)|M@s30@56R(2)|M@n-14.2@'),DROP(5),FROM(Queue:FileDrop)
                       CHECK('All'),AT(584,118),USE(AllPrb),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                       PROMPT('Manufacturer'),AT(384,102),USE(?Prompt1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s40),AT(456,102,124,10),USE(GLO:Select4),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                       CHECK('All'),AT(584,102),USE(AllMan),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                       CHECK('Include Accessories'),AT(456,134),USE(GLO:Select2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                       BUTTON('&Rev tags'),AT(280,216,20,14),USE(?DASREVTAG),HIDE
                       CHECK('Suppress Zeros on Report'),AT(456,146),USE(GLO:Select3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                       CHECK('Show Stock Quantity on Report'),AT(456,158),USE(GLO:Select6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                       BUTTON('sho&W tags'),AT(280,234,20,14),USE(?DASSHOWTAG),HIDE
                       CHECK('Include Suspended Parts'),AT(456,170),USE(GLO:Select10),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                       CHECK('Show Usage Data'),AT(384,204),USE(GLO:Select11),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('SHOW','HIDE')
                       GROUP,AT(360,186,236,150),USE(?GroupReport),TRN
                         GROUP('Usage Date Range On Report'),AT(376,190,200,76),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         PROMPT('Start Date'),AT(384,220),USE(?GLO:Select7:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6b),AT(456,220,64,10),USE(GLO:Select7),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         BUTTON,AT(532,218),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         PROMPT('End Date'),AT(384,244),USE(?GLO:Select8:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6b),AT(456,244,64,10),USE(GLO:Select8),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         BUTTON,AT(532,239),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         OPTION('Report Order'),AT(376,268,200,28),USE(GLO:Select5),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           RADIO('By Location'),AT(384,281),USE(?glo:select5:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           RADIO('By Description'),AT(456,281),USE(?glo:select5:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                         END
                         OPTION('Report Type'),AT(377,298,200,28),USE(GLO:Select12),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           RADIO('Printed'),AT(384,311),USE(?GLO:Select12:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Print')
                           RADIO('CSV Export'),AT(456,311),USE(?GLO:Select12:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('EXPORT')
                         END
                       END
                       BUTTON,AT(108,331),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                       BUTTON,AT(175,331),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                       BUTTON,AT(240,331),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Stock Check Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(484,366),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(552,366),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(412,366),USE(?ButtonStockcheckReport),TRN,FLAT,HIDE,ICON('stochkp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
FDB15                CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
SaveFile    File,Driver('ASCII'),Pre(SavFil),Name(SaveFileName),Create,Bindable,Thread
Record                  Record
Line                    String(1000)
                        End
                    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::4:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW3.UpdateBuffer
   glo:Queue.Pointer = los:Shelf_Location
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = los:Shelf_Location
     ADD(glo:Queue,glo:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(glo:Queue)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = los:Shelf_Location
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::4:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::4:QUEUE = glo:Queue
    ADD(DASBRW::4:QUEUE)
  END
  FREE(glo:Queue)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::4:QUEUE.Pointer = los:Shelf_Location
     GET(DASBRW::4:QUEUE,DASBRW::4:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = los:Shelf_Location
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASSHOWTAG Routine
   CASE DASBRW::4:TAGDISPSTATUS
   OF 0
      DASBRW::4:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::4:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::4:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!subroutines to load and save local variables to ini file


!SaveFile    File,Driver('ASCII'),Pre(SavFil),Name(SaveFileName),Create,Bindable,Thread
!Record                  Record
!Line                    String(1000)
!                        End
!                    End

SaveVariables           Routine

    if exists(SaveFileName) then remove(SaveFileName).


    !just to be sure these are actioned
    if AllMan = 1 then
        glo:Select4 = ''
    END
    if AllPrb = 1 then
        GLO:Select9 = ''
    END

    !standard order for writing will be used for reading
    Create(SaveFile)
    Open(SaveFile)

    SavFil:Line = Glo:Select1
    Add(SaveFile)
    SavFil:Line = Glo:Select2
    Add(SaveFile)
    SavFil:Line = Glo:Select3
    Add(SaveFile)
    SavFil:Line = Glo:Select4
    Add(SaveFile)
    SavFil:Line = Glo:Select5
    Add(SaveFile)
    SavFil:Line = Glo:Select6
    Add(SaveFile)
    SavFil:Line = Glo:Select7
    Add(SaveFile)
    SavFil:Line = Glo:Select8
    Add(SaveFile)
    SavFil:Line = Glo:Select9
    Add(SaveFile)
    SavFil:Line = Glo:Select10
    Add(SaveFile)
    SavFil:Line = Glo:Select11
    Add(SaveFile)
    SavFil:Line = Glo:Select12
    Add(SaveFile)

    loop tmp:Count = 1 to records(GLO:Queue)
        Get(GLO:Queue,Tmp:count)
        if error() then break.
        SavFil:Line = GLO:Queue.Pointer
        Add(SaveFile)
    END !loop through glo:Queue

    Close(SaveFile)

    EXIT



LoadVariables           Routine

    Free(GLO:Queue)
    clear(GLO:Queue)

    if exists(SaveFileName)

        Tmp:Count = 0

        Open(SaveFile)
        if error() then
            EXIT
        END

        Set(SaveFile,0)
        Loop

            Next(SaveFile)
            if error() then break.

            tmp:Count += 1
            Case Tmp:Count
                of 1
                    Glo:Select1 = SavFil:Line
                of 2
                    Glo:Select2 = SavFil:Line
                of 3
                    Glo:Select3 = SavFil:Line
                of 4
                    Glo:Select4 = SavFil:Line
                of 5
                    Glo:Select5 = SavFil:Line
                of 6
                    Glo:Select6 = SavFil:Line
                of 7
                    Glo:Select7 = SavFil:Line
                of 8
                    Glo:Select8 = SavFil:Line
                of 9
                    Glo:Select9 = SavFil:Line
                of 10
                    Glo:Select10 = SavFil:Line
                of 11
                    Glo:Select11 = SavFil:Line
                of 12
                    Glo:Select12 = SavFil:Line
                ELSE
                    !Add loc thing to queue
                    if clip(SavFil:line) <> '' then
                        GLO:Queue.Pointer = clip(SavFil:Line)
                        Add(Glo:Queue)
                    END !if line empty
                !End possilbe cases
            END !Case tmpcount
        END !loop through SaveFile

        !that is it
        Close(SaveFile)

        !if this is an audit call remove the dates

        !tidy up the display
        display()
        BRW3.ResetSort(1)
        if GLO:Select9 = '' then
            AllPrb = 1
            disable(?Glo:Select9)
        END
        if GLO:Select4 = '' then
            AllMan = 1
            disable(?Glo:Select4)
        END

    END !if file exists

    EXIT


!Previous trials used ini file
!    putini('AUDIT','Location',       GLO:Select1,   AuditSetIni)           !Location for audit
!    putini('AUDIT','Inc_Accessories',GLO:Select2,   AuditSetIni)           !Include Accessories YES or NO
!    putini('AUDIT','SupressZero',    GLO:Select3,   AuditSetIni)           !SupressZeros YES or No
!    putini('AUDIT','Manufacturer',   GLO:Select4,   AuditSetIni)           !Manufacturer
!    putini('AUDIT','ReportOrder',    GLO:Select5,   AuditSetIni)           !ReportOrder
!    putini('AUDIT','ShowStockQty',   GLO:Select6,   AuditSetIni)           !Show Stock Quantity    1 or 0
!    putini('AUDIT','StartDate',      GLO:Select7,   AuditSetIni)           !StartDate
!    putini('AUDIT','EndDate',        GLO:Select8,   AuditSetIni)           !ENDDate
!    putini('AUDIT','PriceBand',      GLO:Select9,   AuditSetIni)           !Price Band Name
!    putini('AUDIT','IncludeSusp',    GLO:Select10,  AuditSetIni)           !Include suspended parts yes or no


!    GLO:Select1 = getini('AUDIT','Location',        ''  ,AuditSetIni)           !Location for audit
!    GLO:Select2 = getini('AUDIT','Inc_Accessories', 'NO',AuditSetIni)           !Include Accessories YES or NO
!    GLO:Select3 = getini('AUDIT','SupressZero',     'NO',AuditSetIni)           !SupressZeros YES or No
!    GLO:Select4 = getini('AUDIT','Manufacturer',    ''  ,AuditSetIni)           !Manufacturer
!    GLO:Select5 = getini('AUDIT','ReportOrder',     ''  ,AuditSetIni)           !ReportOrder
!    GLO:Select6 = getini('AUDIT','ShowStockQty',    '1' ,AuditSetIni)           !Show Stock Quantity    1 or 0
!    GLO:Select7 = getini('AUDIT','StartDate',       ''  ,AuditSetIni)           !StartDate
!    GLO:Select8 = getini('AUDIT','EndDate',         ''  ,AuditSetIni)           !ENDDate
!    GLO:Select9 = getini('AUDIT','PriceBand',       ''  ,AuditSetIni)           !Price Band Name
!    GLO:Select10= getini('AUDIT','IncludeSusp',     'NO',AuditSetIni)           !Include suspended parts yes or no
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020180'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Stock_Check_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:PRIBAND.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:LOCSHELF,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Stock_Check_Criteria')
  ?List{prop:vcr} = TRUE
  ?GLO:Select9{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?GLO:Select7{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select8{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  !(SentType) is passed onto the report - says if this is audit (1) or ordinary(0)
  !this hides a few header lists and allows the report to be limited by manufacturer etc
  !12462 Stock and exchange audit adds a third use - Audit preparation criteria with a value of 2 - never passed to report
  
  !!preset the global selects with default values - these may be replaced from previous loads
  glo:select1 = ''        !location
  glo:select2 = 'NO'      !include accessories
  glo:select3 = 'NO'      !Supress zeros
  glo:select4 = ''        !manufacturer
  glo:select5 = 1         !by description or locatioin
  glo:select6 = 0         !show stock quanitity
  glo:select7 = Deformat('1/1/1990',@d6)     !start date
  glo:select8 = Today()                      !end date
  glo:select9 = ''        !priceband
  glo:Select10 = 'NO'     !include suspended parts
  GLO:Select11 = 'SHOW'   !Show
  
  !may be replaced with saved variables - from the account specific set
  SaveFileName = clip(path())&'\'&clip(glo:UserAccountNumber)&'AuditSet.txa'
  
  if glo:Webjob then
      hide(?LookupStartDate)
      hide(?LookupEndDate)
  END
  
  if SentType > 0 then     
      !preselect the location into GLO:Select1
      if glo:RelocateStore then
          glo:Select1 = 'MAIN STORE'
      ELSE
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.fetch(use:Password_Key) = Level:Benign
              !Found
              glo:Select1 = use:Location
          End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      END
  
      !TB13114 - do not let webmaster user change the location - JC - 03/09/13
      if glo:Webjob then disable(?Glo:Select1).
  
      if SentType = 2 then
        !change a few things - this is the audit criteria
        ?WindowTitle{prop:text} = 'Audit Criteria Selection'
        ?Prompt9{prop:Text}= 'Pick Manufacturer or Price Band'
        !hide(?GroupReport)
        !hide(?GLO:Select3)   unhidden 3/1/13
        !hide(?GLO:Select6)
        unhide(?ButtonStockcheckReport)
      END !if autidt criteria
  
  END
  
  BRW3.Q &= Queue:Browse
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,los:Shelf_Location_Key)
  BRW3.AddRange(los:Site_Location,GLO:Select1)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,los:Shelf_Location,1,BRW3)
  BIND('tag_temp',tag_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(tag_temp,BRW3.Q.tag_temp)
  BRW3.AddField(los:Shelf_Location,BRW3.Q.los:Shelf_Location)
  BRW3.AddField(los:RecordNumber,BRW3.Q.los:RecordNumber)
  BRW3.AddField(los:Site_Location,BRW3.Q.los:Site_Location)
  FDCB2.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,0,0)
  FDCB2.EntryCompletion=False
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(loc:ActiveLocationKey)
  FDCB2.AddRange(loc:Active,locTRUE)
  FDCB2.AddField(loc:Location,FDCB2.Q.loc:Location)
  FDCB2.AddField(loc:Active,FDCB2.Q.loc:Active)
  FDCB2.AddField(loc:RecordNumber,FDCB2.Q.loc:RecordNumber)
  FDCB2.AddUpdateField(loc:Location,GLO:Select1)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB8.Init(GLO:Select4,?GLO:Select4,Queue:FileDropCombo:1.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MANUFACT,ThisWindow,GlobalErrors,0,0,0)
  FDCB8.EntryCompletion=False
  FDCB8.Q &= Queue:FileDropCombo:1
  FDCB8.AddSortOrder(man:Manufacturer_Key)
  FDCB8.AddField(man:Manufacturer,FDCB8.Q.man:Manufacturer)
  FDCB8.AddField(man:RecordNumber,FDCB8.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  FDB15.Init(?GLO:Select9,Queue:FileDrop.ViewPosition,FDB15::View:FileDrop,Queue:FileDrop,Relate:PRIBAND,ThisWindow)
  FDB15.Q &= Queue:FileDrop
  FDB15.AddSortOrder(prb:KeyMinPrice)
  FDB15.AddField(prb:BandName,FDB15.Q.prb:BandName)
  FDB15.AddField(prb:MinPrice,FDB15.Q.prb:MinPrice)
  FDB15.AddField(prb:RecordNumber,FDB15.Q.prb:RecordNumber)
  FDB15.AddUpdateField(prb:BandName,GLO:Select9)
  ThisWindow.AddItem(FDB15.WindowComponent)
  FDB15.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  Do LoadVariables
  !message('Back after loadvariables - init')
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset the global selects used by this procedure
  !unless they are going into audit
  if SentType < 2 then
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      glo:select4 = ''
      glo:select5 = ''
      glo:Select6 = ''
      glo:select7 = ''
      glo:select8 = ''
      glo:select9 = ''
      glo:select10 = ''
  END
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOCATION.Close
    Relate:PRIBAND.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Stock_Check_Criteria')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Close
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
      !need to say if this was cancelled
      glo:ErrorText = 'CANCELLED'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
    OF ?ButtonStockcheckReport
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonStockcheckReport, Accepted)
      error# = 0
      If glo:select1 = ''
          Select(?glo:select1)
          error# = 1
      End!If glo:select1 = ''
      If error# = 0 And ~Records(glo:Queue)
          Case Missive('You must select at least ONE Shelf Location.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          error# = 1
      End!If error# = 0 And ~Records(glo:Queue)
      if clip(GLO:Select12) = '' then
         miss# = missive('You must select the type of report you want.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
         error# = 1
      END
      !just to be sure these are actioned
      if AllMan = 1 then
          glo:Select4 = ''
      END
      if AllPrb = 1 then
          GLO:Select9 = ''
      END
      
      If error# = 0
         !print the report
         Stock_Check_Report(1)     !non-audit report = 0 Audit = 1
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonStockcheckReport, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?GLO:Select1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
      BRW3.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
    OF ?AllPrb
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllPrb, Accepted)
      !this controls GLO:Select9
      if AllPrb = 1 then
          GLO:Select9 = ''
          disable(?Glo:Select9)
      ELSE
          enable(?glo:Select9)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllPrb, Accepted)
    OF ?AllMan
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllMan, Accepted)
      !this controls glo:Select4
      if AllMan = 1 then
          glo:Select4 = ''
          disable(?Glo:Select4)
      ELSE
          enable(?glo:Select4)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllMan, Accepted)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select7 = TINCALENDARStyle1(GLO:Select7)
          Display(?GLO:Select7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select8 = TINCALENDARStyle1(GLO:Select8)
          Display(?GLO:Select8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020180'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020180'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020180'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If glo:select1 = ''
          Select(?glo:select1)
          error# = 1
      End!If glo:select1 = ''
      
      If error# = 0 And ~Records(glo:Queue)
          Case Missive('You must select at least ONE Shelf Location.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          error# = 1
      End!If error# = 0 And ~Records(glo:Queue)
      
      !just to be sure these are actioned
      if AllMan = 1 then
          glo:Select4 = ''
      END
      
      if AllPrb = 1 then
          GLO:Select9 = ''
      END
      
      If error# = 0
          Do SaveVariables
          !need to say what is shown on the report by changing the variable sent...
          if SentType = 2 then
              Post(Event:Closewindow) !this is now finished
          ELSE
            !print the report
            Stock_Check_Report(SentType)     !non-audit report = 0 Audit = 1
          END
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?GLO:Select7
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?GLO:Select8
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::4:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

FDCB2.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1
  SELF.Q.loc:Active_NormalFG = -1
  SELF.Q.loc:Active_NormalBG = -1
  SELF.Q.loc:Active_SelectedFG = -1
  SELF.Q.loc:Active_SelectedBG = -1


FDCB8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(8, ValidateRecord, (),BYTE)
      !IF (man:Notes[1:8] = 'INACTIVE')
      !TB13214 - change to using field for inactive  - J 05/02/14
      if man:Inactive = 1 then 
          RETURN Record:Filtered
      END ! IF
  ReturnValue = PARENT.ValidateRecord()
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(8, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW3.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = los:Shelf_Location
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = los:Shelf_Location
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::4:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue







Third_Party_Failed_Returns PROCEDURE
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
count_temp           LONG
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(TRDBATCH)
                       PROJECT(trb:ESN)
                       PROJECT(trb:Ref_Number)
                       JOIN(job:Ref_Number_Key,trb:Ref_Number)
                         PROJECT(job:Account_Number)
                         PROJECT(job:MSN)
                         PROJECT(job:Manufacturer)
                         PROJECT(job:Model_Number)
                         PROJECT(job:Unit_Type)
                       END
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,865,7521,1396),USE(?unnamed)
                         STRING('3rd Party Agent:'),AT(5000,0),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,0),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(5885,208),USE(GLO:Select2),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Report Type:'),AT(5000,417),USE(?string22:3),TRN,FONT(,8,,)
                         STRING('Report Type'),AT(5885,417),USE(?Report_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Exchange Type'),AT(5885,625),USE(?Exchanged_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Exchanged Type:'),AT(5000,625),USE(?string22:4),TRN,FONT(,8,,)
                         STRING('Batch No:'),AT(5000,208),USE(?string22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,833),USE(?ExchangeType:2),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,833),USE(tmp:PrintedBy),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,1042),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,1042),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,1042),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,1250),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,1250),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,1250),USE(?String26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,1250,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,188),USE(?detailband)
                           STRING(@s15),AT(5208,0),USE(job:Account_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(1823,0),USE(job:Manufacturer),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s16),AT(208,0),USE(trb:ESN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s11),AT(1146,0),USE(job:MSN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(2969,0),USE(job:Model_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(4115,0),USE(job:Unit_Type),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s8),AT(6927,0),USE(trb:Ref_Number),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@d6),AT(6354,0),USE(jot:DateDespatched),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(260,52,6979,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(260,104),USE(?String38),TRN,FONT(,8,,FONT:bold)
                           STRING(@n-14),AT(1667,104),USE(count_temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('3RD PARTY FAILED RETURNS'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING('Unit Type'),AT(4115,2083),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Sent'),AT(6333,2083),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Job No'),AT(6990,2083),USE(?string25:4),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Account No'),AT(5208,2083),USE(?string25:5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('I.M.E.I. Number'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(1146,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Manufacturer'),AT(1823,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Model Number'),AT(2969,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Third_Party_Failed_Returns')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Third_Party_Failed_Returns')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = '3rdPartyFail'
  If PrintOption(PreviewReq,glo:ExportReport,'Third Party Failed Returns') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:TRDBATCH.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Access:JOBS.UseFile
  Access:TRDPARTY.UseFile
  Access:JOBTHIRD.UseFile
  
  
  RecordsToProcess = RECORDS(TRDBATCH)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(TRDBATCH,'QUICKSCAN=on')
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(trb:Company_Batch_ESN_Key)
      Process:View{Prop:Filter} = |
      'UPPER(trb:Company_Name) = UPPER(GLO:Select1) AND (Upper(trb:status) = ' & |
      '''OUT'')'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        print# = 1
        
        Access:JOBTHIRD.Clearkey(jot:ThirdPartyKey)
        jot:ThirdPartyNumber    = trb:RecordNumber
        If Access:JOBTHIRD.Tryfetch(jot:ThirdPartyKey) = Level:Benign
            !Found
        Else! If Access:JOBTHIRD.Tryfetch(jot:ThirdPartyKey) = Level:Benign
            !Error
            Assert(0,'<13,10>Cannot Find JOBTHIRD Record.<13,10>')
        End! If Access:.Tryfetch(jot:ThirdPartyKey) = Level:Benign
        
        IF glo:select3 <> 'ALL'
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = trb:ref_number
            if access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                If job:ThirdPartyDateDesp <> ''
                    If trb:turntime <> ''
                        If job:ThirdPartyDateDesp + trb:turntime => Today()
                            print# = 0
                        End!If JOB:Third_Party_Despatch_Date + trd:turnaround_time < Today()
                    Else!If trb:turntime <> ''
                        If job:ThirdPartyDateDesp+ trd:turnaround_time => Today()
                            print# = 0
                        End!If JOB:Third_Party_Despatch_Date + trd:turnaround_time < Today()
                    End!If trb:turntime <> ''
                End!If job:third_party_despatch_date <> ''
            end!if access:jobs.tryfetch(job:ref_number_key) = Level:Benign
        End!IF glo:select3 = 'ALL'
        
        Case glo:select4
            Of 1 !Exchanged Only
                If trb:exchanged <> 'YES'
                    print# = 0
                End!If trb:exchanged <> 'YES'
            Of 2 !Not Exchange Only
                If trb:exchanged = 'YES'
                    print# = 0
                End!If trb:exchanged = 'YES'
            Of 3
        End!Case glo:select4
        
        If print# = 1
            count_temp += 1
            Print(rpt:detail)
        End!If print# = 1
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(TRDBATCH,'QUICKSCAN=off').
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','Third_Party_Failed_Returns')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBTHIRD.Close
    Relate:STANTEXT.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'TRDBATCH')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  access:trdparty.clearkey(trd:company_name_key)
  trd:company_name = glo:select1
  access:trdparty.tryfetch(trd:company_name_key)
  
  
  Settarget(Report)
  If glo:select3   = 'ALL'
      ?report_type{prop:text} = 'All Outstanding Units'
  Else!If glo:select3   = 'ALL'
      ?report_type{prop:text} = 'Overdue Returns'
  End!If glo:select3   = 'ALL'
  Case glo:select4
      Of 1
          ?exchanged_type{prop:text}  = 'Exchange Units Only'
      Of 2
          ?Exchanged_type{prop:text}  = 'Not Exchanged Units Only'
      Of 3
          ?Exchanged_type{prop:text}  = 'All Units'
  End!Case glo:select4
  Settarget()
  
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Third_Party_Failed_Returns'
  END
  report{Prop:Preview} = PrintPreviewImage







Third_Party_Report_Criteria PROCEDURE                 !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select3
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
window               WINDOW('3rd Party Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Third Party Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           COMBO(@s30),AT(314,166,124,10),USE(GLO:Select3),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Start Date'),AT(242,188),USE(?glo:select1:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(314,188,64,10),USE(GLO:Select1),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(382,183),USE(?PopCalendar),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(242,210),USE(?glo:select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(314,210,64,10),USE(GLO:Select2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(382,206),USE(?PopCalendar:2),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('3rd Party Agent'),AT(242,166),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020186'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Third_Party_Report_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:TRDPARTY.Open
  SELF.FilesOpened = True
  glo:select1  = Today()
  glo:select2  = Today()
  glo:select3  = ''
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Third_Party_Report_Criteria')
  Bryan.CompFieldColour()
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
  ?glo:select2{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB3.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(trd:Company_Name_Key)
  FDCB3.AddField(trd:Company_Name,FDCB3.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  glo:select1  = ''
  glo:select2  = ''
  glo:select3  = ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRDPARTY.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Third_Party_Report_Criteria')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020186'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020186'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020186'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?glo:select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If glo:select1 = ''
          Select(?glo:select1)
          error# = 1
      End!If glo:select1 = ''
      If glo:select2 = '' and error# = 0
          Select(?glo:select2)
          error# = 1
      End!If glo:select2 = '' and error# = 0
      
      If error# = 0
          Third_Party_Returns_Report(0)
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?GLO:Select1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






Income_Report PROCEDURE(func:all,func:AccountType)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_job_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
invoice_type_temp    STRING(10)
courier_temp         REAL
labour_temp          REAL
parts_temp           REAL
vat_temp             REAL
line_total_temp      REAL
line_total_total_temp REAL
vat_total_temp       REAL
parts_total_temp     REAL
labour_total_temp    REAL
courier_total_temp   REAL
count_temp           REAL
account_number_temp  STRING(15)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:RenameCourierCost BYTE(0)
tmp:CourierCostName  STRING(30)
tmp:JobNo            STRING(8)
tmp:InvoiceNumber    STRING(30)
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Account_Number)
                       PROJECT(inv:Date_Created)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT('Income Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(417,875,7521,1281),USE(?unnamed)
                         STRING('Date Range:'),AT(5000,417),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5833,417),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(6510,417,615,188),USE(GLO:Select2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Account No:'),AT(5000,208),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,208),USE(account_number_temp),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@d6),AT(5833,833),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(5833,625),USE(tmp:PrintedBy),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,833),USE(?String43),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,625),USE(?String42),TRN,FONT(,8,,)
                         STRING(@s3),AT(5833,1042),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6094,1042),USE(?String47),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6302,1042,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,1042),USE(?string59),TRN,FONT(,8,,)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@n10.2),AT(5208,0),USE(parts_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s11),AT(10,0,781,208),USE(tmp:InvoiceNumber),TRN,RIGHT,FONT(,8,,)
                           STRING(@s15),AT(2604,0),USE(inv:Account_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@n10.2),AT(3698,0),USE(courier_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6510,0),USE(line_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n10.2),AT(5885,0),USE(vat_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(4479,0),USE(labour_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s3),AT(2229,0),USE(invoice_type_temp),TRN,FONT(,8,,)
                           STRING(@d6b),AT(938,0),USE(inv:Date_Created),TRN,RIGHT,FONT(,8,,)
                           STRING(@s8),AT(1677,0),USE(tmp:JobNo),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           STRING(@n12.2),AT(4385,104),USE(labour_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           LINE,AT(156,52,7083,0),USE(?line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(208,104),USE(?string26),TRN,FONT(,8,,FONT:bold)
                           STRING(@n14.2),AT(6510,104),USE(line_total_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(990,104),USE(count_temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n12.2),AT(3604,104),USE(courier_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n12.2),AT(5115,104),USE(parts_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n12.2),AT(5792,104),USE(vat_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('INCOME REPORT'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Total'),AT(7031,2083,313,208),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Client'),AT(2604,2083),USE(?string24:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Invoice No'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Issued'),AT(938,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Job No.'),AT(1677,2083),USE(?String51),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Type'),AT(2229,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('V.A.T.'),AT(6167,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Courier'),AT(3594,2083),USE(?string24:5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Parts'),AT(5521,2083),USE(?string24:4),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Labour'),AT(4688,2083),USE(?string24:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Income_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Income_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULTS.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSE.UseFile
  Access:TRADEACC.UseFile
  Access:LOCATLOG.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(inv:Date_Created_Key)
      Process:View{Prop:Filter} = |
      'inv:Date_Created >= GLO:Select1 AND inv:Date_Created <<= GLO:Select2'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        courier_temp    = 0
        labour_temp     = 0
        parts_temp      = 0
        vat_temp        = 0
        line_total_temp = 0
        tmp:InvoiceNumber   = inv:Invoice_Number & '-' & tra:BranchIdentification
        
        error# = 0
        
        If glo:WebJob
            If ~inv:ExportedRRCOracle
                Error# = 1
            End !If ~inv:ExportedRRCOracle
        Else
        End !glo:WebJob
        
        
        If func:All
            Case func:AccountType
                Of 0 !Head Account
        
                    !If the invoice is marked as account_type = 'MAI', then
                    !the invoice account number is a "Header" account number
                    !otherwise it's a "Sub" Account Number
                    If glo:Select6 <> ''
                        If inv:AccountType = 'MAI'
                            If inv:Account_Number <> glo:Select3
                                Error# = 1
                            End !If inv:Account_Number <> glo:Select3
                        Else !If inv:Account_Type = 'MAI'
        
                            !You have selected a "Header" Account Number
                            !The invoice has a "Sub" Account Number
                            !Does the Sub match the Header?
                            Access:SUBTRACC.Clearkey(sub:Main_Account_Key)
                            sub:Main_Account_Number = glo:Select6
                            sub:Account_Number  = inv:Account_Number
                            Set(sub:Main_Account_Key,sub:Main_Account_Key)
                            If Access:SUBTRACC.Next() = Level:Benign
                                If sub:Main_Account_Number <> glo:Select6 Or |
                                    sub:Account_Number  <> inv:Account_Number
                                    Error# = 1
                                End !sub:Account_Number  = inv:Account_Number
                            Else!If Access:SUBTRACC.Next() = Level:Benign
                                Error# = 1
                            End !If Access:SUBTRACC.Next() = Level:Benign
        
                        End !If inv:Account_Type = 'MAI'
        
                    End !If glo:Select6 <> ''
                Of 1 !Sub Account
                    If glo:select3 <> ''
                        If inv:AccountType = 'MAI'
                            !You have selected a "Sub" Account Number
                            !The invoice has a "Header" Account Number
                            !Does the Header account number match the sub?
                            Access:SUBTRACC.Clearkey(sub:Main_Account_Key)
                            sub:Main_Account_Number = inv:Account_Number
                            sub:Account_Number  = glo:Select3
                            Set(sub:Main_Account_Key,sub:Main_Account_Key)
                            If Access:SUBTRACC.Next() = Level:Benign
                                If sub:Main_Account_Number <> inv:Account_Number Or |
                                    sub:Account_Number <> glo:Select3
                                    Error# = 1
                                End
                            Else !If Access:SUBTRACC.Next() = Level:Benign
                                Error# = 1
                            End !If Access:SUBTRACC.Next() = Level:Benign
                        Else !If inv:Account_Type = 'MAI'
                            If inv:account_number <> glo:select3
                                error# = 1
                            End!If inv:account_number <> glo:select3
        
                        End !If inv:Account_Type = 'MAI'
        
                    End!If glo:select3 <> ''
            End !func:AccountType
        
        End !func:All
        IF error# = 0 And glo:select4 <> 'YES'
            If inv:invoice_type = 'WAR' or inv:invoice_Type = 'CHA' or inv:invoice_type = 'SIN'
                error# = 1
            End!If inv:invoice_type = 'WAR' or inv:invoice_Type = 'CHA' or inv:invoice_type = 'SIN'
        End!IF error# = 0 And glo:select4 <> 'YES'
        If error# = 0 And glo:select5 <> 'YES'
            If inv:invoice_type = 'RET'
                error# = 1
            End!If inv:invoice_type = 'RET'
        End!If error# = 0 And glo:select5 <> 'YES'
        
        If error# = 0
            CountJobs# = 0
        
            Case inv:invoice_type
                Of 'WAR'
                    invoice_type_temp   = 'WAR'
                    tmp:JobNo           = ''
                    courier_temp    += INV:Courier_Paid
                    labour_temp     += INV:Labour_Paid
                    parts_temp      += INV:Parts_Paid
                    vat_temp        += (Round(INV:Courier_Paid * (INV:Vat_Rate_Labour/100),.01)) + |
                                        (Round(INV:Labour_Paid * (INV:Vat_Rate_Labour/100),.01)) + |
                                        (Round(INV:Parts_Paid * (INV:Vat_Rate_Parts/100),.01))
        
                Of 'RET'
                    invoice_type_temp   = 'RET'
                    tmp:JobNo           = ''
                    courier_temp    += INV:Courier_Paid
                    labour_temp     += INV:Labour_Paid
                    parts_temp      += INV:Parts_Paid
                    vat_temp        += (Round(INV:Courier_Paid * (INV:Vat_Rate_retail/100),.01)) + |
                                        (Round(INV:Labour_Paid * (INV:Vat_Rate_Retail/100),.01)) + |
                                        (Round(INV:Parts_Paid * (INV:Vat_Rate_retail/100),.01))
                Else
                    invoice_type_temp = 'CHG'
                    tmp:JobNo         = ''
        
                    CountJobs# = 0
                    save_job_id = access:jobs.savefile()
                    access:jobs.clearkey(job:InvoiceNumberKey)
                    job:invoice_number = inv:invoice_number
                    set(job:InvoiceNumberKey,job:InvoiceNumberKey)
                    loop
                        if access:jobs.next()
                           break
                        end !if
                        if job:invoice_number <> inv:invoice_number      |
                            then break.  ! end if
                        if tmp:JobNo = ''
                            tmp:JobNo = job:Ref_Number
                        else
                            tmp:JobNo = 'MULT'
                        end
                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = job:Ref_Number
                        Access:JOBSE.Fetch(jobe:RefNumberKey)
                        If ~glo:WebJob
                            !If it's being printed at the ARC
                            !only include if the job was sent the ARC from an RRC
                            If jobe:WebJob
                                If ~SentToHub(job:Ref_Number)
                                    Cycle
                                End !If SentToHub(job:Ref_Number)
                            End !If jobe:WebJob
        
                        End !glo:WebJob
        
                        yldcnt# += 1
                        if yldcnt# > 25
                           yield() ; yldcnt# = 0
                        end !if
        
                        if ~glo:WebJob
                            courier_temp    += JOB:Invoice_Courier_Cost
                            labour_temp     += JOB:Invoice_Labour_Cost
                            parts_temp      += JOB:Invoice_Parts_Cost
                            vat_temp        += (Round(JOB:Invoice_Courier_Cost * (inv:vat_rate_labour/100),.01)) + |
                                                (Round(JOB:Invoice_Labour_Cost * (inv:vat_rate_labour/100),.01)) + |
                                                (Round(JOB:Invoice_Parts_Cost * (inv:vat_rate_parts/100),.01))
                        else
                            courier_temp  = job:Invoice_Courier_cost
                            labour_temp   = jobe:InvRRCCLabourCost
                            parts_temp    = jobe:InvRRCCPartsCost
                            vat_temp        += (Round(JOB:Invoice_Courier_Cost * (inv:vat_rate_labour/100),.01)) + |
                                                (Round(jobe:InvRRCCLabourCost * (inv:vat_rate_labour/100),.01)) + |
                                                (Round(jobe:InvRRCCPartsCost * (inv:vat_rate_parts/100),.01))
        
                            !courier_temp    += JOB:Invoice_Courier_Cost
                            !labour_temp     += jobe:RRCCLabourCost
                            !parts_temp      += jobe:RRCCPartsCost
                            !vat_temp        += (Round(JOB:Invoice_Courier_Cost * (inv:RRCVatRateLabour/100),.01)) + |
                            !                    (Round(jobe:RRCCLabourCost * (inv:RRCVatRateLabour/100),.01)) + |
                            !                    (Round(jobe:RRCCPartsCost * (inv:RRCVatRateParts/100),.01))
        
                        end
                        CountJobs# += 1
                    end !loop
                    access:jobs.restorefile(save_job_id)
            End!If inv:invoice_type = 'WAR'
        
            If CountJobs# = 1
                !Incase a single invoice is for the RRC only
                !then it shouldn't be included on the ARC report
                courier_total_temp      += courier_temp
                labour_total_temp       += labour_temp
                parts_total_temp        += parts_temp
                vat_total_temp          += vat_temp
                line_total_temp         += Round(courier_temp,.01) + Round(labour_temp,.01) + Round(parts_temp,.01) + Round(vat_temp,.01)
                line_total_total_temp   += line_total_temp
                count_temp              += 1
                Print(rpt:detail)
                tmp:RecordsCount += 1
            End !If CountJobs# = 1
        
        
        End!If error# = 0
        
            !*************************set the display address to the head account if booked on as a web job***********************
            if glo:webjob then
                access:tradeacc.clearkey(tra:account_number_key)
                tra:account_number = clarionet:global.param2
                IF access:tradeacc.fetch(tra:account_number_key)
                  !Error!
                END
                def:User_Name           = tra:Company_Name
                def:Address_Line1       = tra:Address_Line1
                def:Address_Line2       = tra:Address_Line2
                def:Address_Line3       = tra:Address_Line3
                def:Postcode            = tra:Postcode
                tmp:DefaultTelephone    = tra:Telephone_Number
                tmp:DefaultFax          = tra:Fax_Number
                def:EmailAddress        = tra:EmailAddress
        
            end
            !*********************************************************************************************************************
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Income_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  If func:all
      Case func:AccountType
          Of 0
              Account_Number_Temp = glo:Select6
          Of 1
              Account_Number_Temp = glo:Select3
      End !Case func:AccountType
  Else!If glo:select3 <> ''
      account_number_temp = 'ALL'
  End!If glo:select3 <> ''
  
  tmp:RenameCourierCost = Clip(GETINI('RENAME','RenameCourierCost',,CLIP(PATH())&'\SB2KDEF.INI'))
  
  if tmp:RenameCourierCost
      tmp:CourierCostName = Clip(GETINI('RENAME','CourierCostName',,CLIP(PATH())&'\SB2KDEF.INI'))
  end
  
  if tmp:CourierCostName <> ''
      settarget(Report)
      ?string24:5{prop:Text} = clip(tmp:CourierCostName)                       ! Rename courier cost prompt
      settarget()
  end
  
  
  If glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
  
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  Else !glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
  
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  End !glo:WebJob
  
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Income_Report'
  END
  report{Prop:Preview} = PrintPreviewImage













Job_Pending_Parts_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_ope_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
save_orp_id          USHORT,AUTO
save_WPR_id          USHORT,AUTO
save_par_id          USHORT,AUTO
ordpend_queue        QUEUE,PRE(ordque)
Ref_Number           REAL
Part_Number          STRING(30)
Description          STRING(30)
Model_Number         STRING(30)
number_of_jobs       REAL
quantity             REAL
quantity_on_order    REAL
                     END
count_temp           REAL
account_number_temp  STRING(15)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(ORDPEND)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Account No:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,156),USE(account_number_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@s60),AT(5833,365),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Report Date:'),AT(5000,573),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5833,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s25),AT(208,0),USE(ordque:Model_Number),TRN,FONT(,8,,)
                           STRING(@s25),AT(1823,0),USE(ordque:Part_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s25),AT(3490,0),USE(ordque:Description),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s8),AT(6823,0),USE(ordque:quantity_on_order),TRN,RIGHT,FONT(,8,,)
                           STRING(@s8),AT(6042,0),USE(ordque:quantity),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s8),AT(5313,0),USE(ordque:number_of_jobs),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
totals                   DETAIL,AT(,,,458),USE(?totals)
                           LINE,AT(208,52,7083,0),USE(?line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines'),AT(208,104),USE(?string26),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(1563,104),USE(count_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('JOB PENDING PARTS REPORT'),AT(3958,0,3490,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Model Number'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1823,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3490,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Jobs'),AT(5521,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('No Required'),AT(6042,2083),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('On Order'),AT(6875,2083),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Job_Pending_Parts_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Job_Pending_Parts_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:ORDPEND.Open
  Relate:DEFAULTS.Open
  Access:JOBS.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:ORDPARTS.UseFile
  
  
  RecordsToProcess = RECORDS(ORDPEND)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(ORDPEND,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = Records(ordpend)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Go through the ordpend file. If it has a job number, it is a job order.
        !See if the account number on that job is the account number selected.
        !There shouldn't be that many pending orders, so this is probably, the
        !quickest way to do it.
        
        !Build a queue of ordpend records, model number (from the job) and part number
        !so the report will be in model/part number order.
        save_ope_id = access:ordpend.savefile()
        set(ope:ref_number_key)
        loop
            if access:ordpend.next()
               break
            end !if
        
            If ope:job_number <> ''
                access:jobs.clearkey(job:ref_number_key)
                job:ref_number  = ope:job_number
                If access:jobs.fetch(job:ref_number_key) = Level:Benign
                    print# = 1
                    If glo:select1 <> ''
                        If job:account_number <> glo:select1
                            print# = 0
                        End!If job:account_number <> glo:select1
                    End!If glo:select1 <> ''
                    If print# = 1
                        Clear(glo:Q_ModelNumber)
                        glo:model_number_pointer    = job:model_number
                        Get(glo:Q_ModelNumber,glo:model_number_pointer)
                        IF error()
                            print# = 0
                        End!IF error()
                    End!If print# = 1
                    If print# = 1
                        quantity$ = 0
                        description" = 0
                        Case ope:part_type                                                              !Find the part on the job, and
                            Of 'JOB'                                                                    !get the quantity
                                setcursor(cursor:wait)
                                save_par_id = access:parts.savefile()
                                access:parts.clearkey(par:PendingRefNoKey)
                                par:ref_number         = job:ref_number
                                par:pending_ref_number = ope:ref_number
                                set(par:PendingRefNoKey,par:PendingRefNoKey)
                                loop
                                    if access:parts.next()
                                       break
                                    end !if
                                    if par:ref_number         <> job:ref_number      |
                                    or par:pending_ref_number <> ope:Ref_number      |
                                        then break.  ! end if
                                    If par:part_number  = ope:part_number
                                        quantity$   = par:quantity
                                        description"    = par:description
                                        Break
                                    End!If par:part_number  = ope:part_number
                                end !loop
                                access:parts.restorefile(save_par_id)
                                setcursor()
                            Of 'WAR'
                                setcursor(cursor:wait)
                                save_wpr_id = access:warparts.savefile()
                                access:warparts.clearkey(wpr:PendingRefNoKey)
                                wpr:ref_number         = job:ref_number
                                wpr:pending_ref_number = ope:ref_number
                                set(wpr:PendingRefNoKey,wpr:PendingRefNoKey)
                                loop
                                    if access:warparts.next()
                                       break
                                    end !if
                                    if wpr:ref_number         <> job:ref_number      |
                                    or wpr:pending_ref_number <> ope:Ref_number      |
                                        then break.  ! end if
                                    If wpr:part_number  = ope:part_number
                                        quantity$   = wpr:quantity
                                        description"    = wpr:description
                                        Break
                                    End!If wpr:part_number  = ope:part_number
                                end !loop
                                access:warparts.restorefile(save_wpr_id)
                                setcursor()
                        End!Case ope:part_type
                        quantity_on_order$ = 0
                        save_orp_id = access:ordparts.savefile()
                        access:ordparts.clearkey(orp:received_part_number_key)
                        orp:all_received = 'NO'
                        orp:part_number  = ope:part_number
                        set(orp:received_part_number_key,orp:received_part_number_key)
                        loop
                            if access:ordparts.next()
                               break
                            end !if
                            if orp:all_received <> 'NO'      |
                            or orp:part_number  <> ope:part_number      |
                                then break.  ! end if
                            quantity_on_order$  += orp:quantity
                        end !loop
                        access:ordparts.restorefile(save_orp_id)
        
                        ordque:part_number  = ope:part_number
                        ordque:model_number = job:model_number
                        Get(ordpend_queue,ordque:part_number,ordque:model_number)
                        If error()
                            ordque:ref_number   = ope:ref_number
                            ordque:part_number  = ope:part_number
                            ordque:model_Number = job:model_number
                            ordque:number_of_jobs   = 1
                            ordque:quantity     = quantity$
                            ordque:quantity_on_order   = quantity_on_order$
                            ordque:description  = description"
                            Add(ordpend_queue)
                        Else!If error
                            ordque:number_of_jobs   += 1
                            ordque:quantity += quantity$
                            ordque:quantity_on_order += quantity_on_order$
                            Put(ordpend_queue)
                        End!If error
        
                    End!If print# = 1
                End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
            End!If ope:job_number <> ''
        end !loop
        access:ordpend.restorefile(save_ope_id)
        
        RecordsToPRocess    = Records(ordpend_queue)
        !Print
        If Records(ordpend_queue)
        
            count_temp = 0
            Clear(ordpend_queue)
            Sort(ordpend_queue,ordque:Model_Number,ordque:Part_Number)
            Loop x# = 1 To Records(ordpend_queue)
                Get(ordpend_queue,x#)
                RecordsProcessed += 1
                Do DisplayProgress
                access:ordpend.clearkey(ope:ref_number_key)
                ope:ref_number = ordque:ref_number
                if access:ordpend.fetch(ope:ref_number_key) = Level:Benign
                    count_temp += 1
                    tmp:RecordsCount += 1
                    Print(rpt:detail)
                End!if access:ordpend.fetch(ope:ref_number_key)
            End!Loop x# = 1 To Records(ordpend_queue)
            Print(rpt:totals)
        
        End!If Records(ordpend_queue)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(ORDPEND,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Job_Pending_Parts_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  If glo:select1 = ''
      account_number_temp  = 'ALL ACCOUNTS'
  Else!If glo:select1 = ''
      account_number_temp = glo:select1
  End!If glo:select1 = ''
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Job_Pending_Parts_Report'
  END
  report{Prop:Preview} = PrintPreviewImage













Nokia_accessories_claim_report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
parts_rate_temp      REAL
job_count_temp       REAL
save_wpr_id          USHORT,AUTO
total_jobs_temp      REAL
raised_by_temp       STRING(60)
total_count_temp     REAL
total_cost_temp      REAL
vat_temp             REAL
claim_value_temp     REAL
line_cost_temp       REAL
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Accessories Claim Report'),AT(396,2760,7521,8500),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1625),USE(?unnamed)
                         STRING(@s3),AT(5781,1094),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6094,1094),USE(?String48),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6354,1094,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,52),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5781,52),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING('Raised By:'),AT(5000,313),USE(?String31),TRN,FONT(,8,,)
                         STRING(@s60),AT(5781,323),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Manufacturer:'),AT(5000,573),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s40),AT(5781,573),USE(GLO:Select3),TRN,FONT(,8,,FONT:bold)
                         STRING('Batch Number:'),AT(5000,823),USE(?String46),TRN,FONT(,8,,)
                         STRING(@s10),AT(5781,833),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,1094),USE(?String59),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s8),AT(52,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,7,,)
                           STRING(@s8),AT(677,0),USE(wpr:Quantity),TRN,RIGHT,FONT(,7,,)
                           STRING(@s20),AT(1302,0),USE(wpr:Part_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s20),AT(2448,0),USE(wpr:Description),TRN,FONT(,7,,)
                           STRING(@s20),AT(3594,0),USE(job:Model_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s16),AT(4740,0),USE(job:ESN),TRN,LEFT,FONT(,7,,)
                           STRING(@n-14.2),AT(6656,0),USE(line_cost_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s16),AT(5677,0),USE(job:MSN),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,2490),USE(?unnamed:2)
                           LINE,AT(167,52,7188,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Quantity: '),AT(208,156),USE(?String34),TRN,FONT(,8,,)
                           STRING(@s9),AT(1146,156),USE(total_count_temp),TRN,LEFT,FONT(,8,,)
                           STRING('Total:'),AT(5469,156),USE(?Total),TRN,FONT(,8,,)
                           STRING(@n-14.2),AT(6583,156),USE(total_cost_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(5469,365),USE(?VAT),TRN,FONT(,8,,)
                           STRING(@n-14.2),AT(6583,365),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                           LINE,AT(5417,573,1979,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING('Claim Value:'),AT(5469,625),USE(?ClaimValue),TRN,FONT(,8,,)
                           STRING(@n-14.2),AT(6583,625),USE(claim_value_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('I confirm that the accessories listed above were replaced under the terms of the' &|
   ' manufacturer''s warranty '),AT(281,1344),USE(?String41),TRN,FONT(,10,,FONT:bold)
                           STRING('claim process.'),AT(281,1552),USE(?String42),TRN,FONT(,10,,FONT:bold)
                           STRING('Signed By'),AT(281,2177),USE(?String43),TRN,FONT(,,,FONT:bold)
                           STRING('Write Name'),AT(3250,2177),USE(?String44),TRN,FONT(,10,,FONT:bold)
                           STRING('Date'),AT(5906,2177),USE(?String45),TRN,FONT(,10,,FONT:bold)
                           LINE,AT(260,2396,7083,0),USE(?Line3),COLOR(COLOR:Black)
                         END
                       END
                       FOOTER,AT(406,11417,7521,240),USE(?unnamed:4)
                       END
                       FORM,AT(396,469,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('WARRANTY ACCESSORIES CLAIM REPORT'),AT(3906,52,3594,260),USE(?String20),TRN,RIGHT,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,240),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,240),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,240),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1094),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1094),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1250),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1250),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1406,3844,240),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1406),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('Job No'),AT(156,2083),USE(?String24),TRN,FONT(,8,,FONT:bold)
                         STRING('Qty'),AT(781,2083),USE(?String25),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1302,2083),USE(?String26),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(2448,2083),USE(?String27),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(3594,2083),USE(?String28),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. No.'),AT(4740,2083),USE(?String29),TRN,FONT(,8,,FONT:bold)
                         STRING('Cost'),AT(7135,2083),USE(?Cost),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(5677,2083),USE(?MSN),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Nokia_accessories_claim_report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Nokia_accessories_claim_report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select3',GLO:Select3)
  BIND('GLO:Select3',GLO:Select3)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:VATCODE.Open
  Access:WARPARTS.UseFile
  Access:STOCK.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job:EDI_Key)
      Process:View{Prop:Filter} = |
      'UPPER(job:Manufacturer) = UPPER(GLO:Select3) AND (upper(job:edi) = ''YE' & |
      'S'' And upper(job:edi_batch_number) = glo:select1)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                if tra:invoice_sub_accounts = 'YES'
                    access:vatcode.clearkey(vat:vat_code_key)
                    vat:vat_code = sub:parts_vat_code
                    if access:vatcode.fetch(vat:vat_code_key) = level:benign
                        parts_rate_temp = vat:vat_rate
                    end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                else!if tra:use_sub_accounts = 'YES'
                    access:vatcode.clearkey(vat:vat_code_key)
                    vat:vat_code = tra:parts_vat_code
                    if access:vatcode.fetch(vat:vat_code_key) = level:benign
                        parts_rate_temp = vat:vat_rate
                    end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                end!if tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign
        
        
        setcursor(cursor:wait)
        
        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number  <> job:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            access:stock.clearkey(sto:ref_number_key)
            sto:ref_number = wpr:part_ref_number
            if access:stock.fetch(sto:ref_number_key) = Level:Benign
                If sto:accessory = 'YES'
                    total_count_temp += 1
                    line_cost_temp = wpr:quantity * wpr:purchase_cost
                    total_cost_temp += line_cost_temp
                    vat_temp += (line_cost_temp * parts_rate_temp/100)
                    claim_value_temp += line_cost_temp + (line_cost_temp * parts_rate_temp/100)
                    tmp:RecordsCount += 1
                    Print(rpt:detail)
                End
            end
        end !loop
        access:warparts.restorefile(save_wpr_id)
        setcursor()
        
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        OMIT('**End Omit Nothing Message**')
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
        OMIT('**End Omit Nothing Message**')
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        OMIT('**End Omit Nothing Message**')
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
        OMIT('**End Omit Nothing Message**')
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Nokia_accessories_claim_report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:VATCODE.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = glo:Select3
  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Found
      If man:RemAccCosts
          Settarget(Report)
          ?line_cost_temp{prop:hide} = 1
          ?Total{prop:hide} = 1
          ?VAT{prop:hide} = 1
          ?ClaimValue{prop:hide} = 1
          ?Total_Cost_Temp{prop:hide} = 1
          ?Vat_Temp{prop:hide} = 1
          ?Claim_Value_Temp{prop:hide} = 1
          ?Line2{prop:hide} = 1
          ?Cost{prop:hide} = 1
          Settarget()
      End!If man:RemAccCosts
  Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Nokia_accessories_claim_report'
  END
  Report{Prop:Preview} = PrintPreviewImage







