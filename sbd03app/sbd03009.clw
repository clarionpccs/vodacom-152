

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03009.INC'),ONCE        !Local module procedure declarations
                     END








ToteReport PROCEDURE(func:StartDate,func:EndDate,func:Detailed,func:ExportPath)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:ExportPath       STRING(255),STATIC
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
save_cou_id          USHORT,AUTO
save_job_id          USHORT,AUTO
AccountQueue         QUEUE,PRE(accque)
AccountNumber        STRING(30)
Count                LONG
                     END
JobNumberQueue       QUEUE,PRE(jobque)
AccountNumber        STRING(30),NAME('jobque:AccountNumber')
JobNumber            LONG,NAME('jobque:JobNumber')
                     END
tmp:Pouches          LONG
tmp:JobNumbers       STRING(5000)
tmp:TotalPouches     LONG
tmp:ShowPreview      BYTE(0)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
! Before Embed Point: %DataSectionBeforeReport) DESC(Data Section, Before Report Declaration) ARG()
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
ToteExport    File,Driver('BASIC'),Pre(tote),Name(tmp:ExportPath),Create,Bindable,Thread
Record                  Record
AccountNumber               String(30)
JobNumber                   String(30)
                        End
                    End

! After Embed Point: %DataSectionBeforeReport) DESC(Data Section, Before Report Declaration) ARG()
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1104,7521,1000)
                         STRING('Date From'),AT(5000,0),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6),AT(5833,0),USE(func:StartDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date To'),AT(5000,156),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5833,156),USE(func:EndDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,313),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5833,313),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,469),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5833,469),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,469),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,625),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5833,625),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6094,625),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6302,625,375,208),USE(?CPCSPgOfPgStr),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,146),USE(?detailband)
                           STRING(@s30),AT(208,0),USE(accque:AccountNumber),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s8),AT(2865,0),USE(tmp:Pouches),FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,813),USE(?unnamed:2)
                           STRING('Total Number Of Pouches:'),AT(208,104),USE(?String29),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(198,52,7135,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING(@s8),AT(2865,104),USE(tmp:TotalPouches),FONT(,8,,FONT:bold)
                           STRING('Collected By (Signed)'),AT(208,521),USE(?CollectedBy),TRN,FONT(,10,,)
                           LINE,AT(1563,677,5677,0),USE(?Line1),COLOR(COLOR:Black)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7500,11198),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('TOTE REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s15),AT(625,1042),USE(def:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s15),AT(625,1198),USE(def:Fax_Number),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING(@s255),AT(625,1354,3750,208),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Store Code'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Number Of Pouches'),AT(2760,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('ToteReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','ToteReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Access:COURIER.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = BYTES(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = 0
      Save_cou_ID = Access:COURIER.SaveFile()
      Access:COURIER.ClearKey(cou:Courier_Type_Key)
      cou:Courier_Type = 'TOTE'
      Set(cou:Courier_Type_Key,cou:Courier_Type_Key)
      Loop
          If Access:COURIER.NEXT()
             Break
          End !If
          If cou:Courier_Type <> 'TOTE'      |
              Then Break.  ! End If
          RecordsToProcess += 1
      End !Loop
      Access:COURIER.RestoreFile(Save_cou_ID)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Clear(AccountQueue)
        Free(AccountQueue)
        !Go through all the TOTE couriers and find any despatches (loan/exchange/job)
        !in the date range.
        
        !Show the "no records" message, only if printing report.
        If func:Detailed = 0
            tmp:ShowPreview = 1
        End !tmp:Detailed = 0
        
        tmp:TotalPouches = 0
        Save_cou_ID = Access:COURIER.SaveFile()
        Access:COURIER.ClearKey(cou:Courier_Type_Key)
        cou:Courier_Type = 'TOTE'
        Set(cou:Courier_Type_Key,cou:Courier_Type_Key)
        Loop
            If Access:COURIER.NEXT()
               Break
            End !If
            If cou:Courier_Type <> 'TOTE'      |
                Then Break.  ! End If
            RecordsProcessed += 1
            Do DisplayProgress
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:DateDespatchKey)
            job:Courier         = cou:Courier
            job:Date_Despatched = func:StartDate
            Set(job:DateDespatchKey,job:DateDespatchKey)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Courier         <> cou:Courier      |
                Or job:Date_Despatched > func:EndDate      |
                    Then Break.  ! End If
                !Only allow one occurance of each Account Number in the queue,
                !and add up the number of jobs for each Account.
        
                accque:AccountNumber    = job:Account_Number
                Get(AccountQueue,accque:AccountNumber)
                If Error()
                    accque:AccountNumber    = job:Account_Number
                    accque:Count            = 1
                    Add(AccountQueue)
                Else !If Error()
                    accque:Count += 1
                    Put(AccountQueue)
                End !If Error()
        
                !Put all the job numbers into a Queue for the Detailed Report.
        
                If func:Detailed
                    jobque:AccountNumber    = job:Account_Number
                    jobque:JobNumber        = job:Ref_Number
                    Add(JobNumberQueue)
                End !If func:Detailed
        
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
        
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:DateDespLoaKey)
            job:Loan_Courier    = cou:Courier
            job:Loan_Despatched = func:StartDate
            Set(job:DateDespLoaKey,job:DateDespLoaKey)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Loan_Courier    <> cou:Courier      |
                Or job:Loan_Despatched > func:EndDate      |
                    Then Break.  ! End If
                accque:AccountNumber    = job:Account_Number
                Get(AccountQueue,accque:AccountNumber)
                If Error()
                    accque:AccountNumber    = job:Account_Number
                    accque:Count            = 1
                    Add(AccountQueue)
                Else !If Error()
                    accque:Count += 1
                    Put(AccountQueue)
                End !If Error()
        
                If func:Detailed
                    jobque:AccountNumber    = job:Account_Number
                    jobque:JobNumber        = job:Ref_Number
                    Add(JobNumberQueue)
                End !If func:Detailed
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
        
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:DateDespExcKey)
            job:Exchange_Courier    = cou:Courier
            job:Exchange_Despatched = func:StartDate
            Set(job:DateDespExcKey,job:DateDespExcKey)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Exchange_Courier    <> cou:Courier      |
                Or job:Exchange_Despatched > func:EndDate       |
                Then Break.  ! End If
                accque:AccountNumber    = job:Account_Number
                Get(AccountQueue,accque:AccountNumber)
                If Error()
                    accque:AccountNumber    = job:Account_Number
                    accque:Count            = 1
                    Add(AccountQueue)
                Else !If Error()
                    accque:Count += 1
                    Put(AccountQueue)
                End !If Error()
        
                If func:Detailed
                    jobque:AccountNumber    = job:Account_Number
                    jobque:JobNumber        = job:Ref_Number
                    Add(JobNumberQueue)
                End !If func:Detailed
        
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
        End !Loop
        Access:COURIER.RestoreFile(Save_cou_ID)
        
        RecordsToProcess = Records(AccountQueue)
        
        Sort(AccountQueue,accque:AccountNumber)
        Loop x# = 1 To Records(AccountQueue)
            Get(AccountQueue,x#)
            tmp:Pouches = INT(accque:Count/5)
            If tmp:Pouches < accque:Count
                tmp:Pouches += 1
            End !If tmp:Pouches < accque:Count
        
            tmp:TotalPouches += tmp:Pouches
        
            RecordsProcessed += 1
            Do DisplayProgress
        
            tmp:JobNumbers = ''
            If func:Detailed <> 0
                Sort(JobNumberQueue,'jobque:AccountNumber,jobque:JobNumber')
                Loop y# = 1 To Records(JobNumberQueue)
                    Get(JobNumberqueue,y#)
                    If jobque:AccountNumber = accque:AccountNumber
                        Clear(tote:Record)
                        tote:AccountNumber = accque:AccountNumber
                        tote:JobNumber     = jobque:JobNumber
                        Add(ToteExport)
                    End !If jobque:AccountNumber = accque:AccountNumber
                End !Loop y# = 1 To Records(JobNumberQueue)
            End
            If func:Detailed = 0 or func:Detailed = 2
                Print(rpt:Detail)
            End !If func:Detailed
        End !x# = 1 To Records(AccountQueue)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF tmp:ShowPreview = TRUE
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
        IF tmp:ShowPreview = TRUE
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF tmp:ShowPreview = TRUE
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
        IF tmp:ShowPreview = TRUE
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  ! Before Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  If func:Detailed
      Close(ToteExport)
  End !func:Detailed
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','ToteReport')
  ! After Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !Open Export File
  If func:Detailed <> 0
      If Sub(Clip(func:ExportPath),-1,1) = '\'
          tmp:ExportPath   = Clip(func:ExportPath) & 'TOTEEXP.CSV'
      Else !If Sub(Clip(func:ExportPath),-1,1) = '\'
          tmp:ExportPath   = Clip(func:ExportPath) & '\TOTEEXP.CSV'
      End !If Sub(Clip(func:ExportPath),-1,1) = '\'
      Remove(ToteExport)
  
      Open(ToteExport)
      If Error()
          Create(ToteExport)
          Open(ToteExport)
          If Error()
              Stop(Error())
          End !If Error()
      End !If Error()
  
      Clear(tote:Record)
      tote:AccountNumber  = 'Account Number'
      tote:JobNumber      = 'Job Number'
      Add(ToteExport)
  End !func:Detailed <> 0
  
  
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Tote Report'
  END
  report{Prop:Preview} = PrintPreviewImage







SparesReceivedReportCriteria PROCEDURE                !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Team             STRING(30)
tmp:Engineer         STRING(60)
tmp:AccountNumber    STRING(30)
tmp:UserCode         STRING(3)
window               WINDOW('Spares Received Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Spares Received Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           PROMPT('Team'),AT(225,166),USE(?tmp:Team:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(301,166,124,10),USE(tmp:Team),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Team'),TIP('Team'),ALRT(EnterKey),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                           BUTTON,AT(428,162),USE(?LookupTeam),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Engineer'),AT(225,187),USE(?tmp:Engineer:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(301,187,124,10),USE(tmp:Engineer),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Engineer'),TIP('Engineer'),REQ,UPR,READONLY
                           BUTTON,AT(429,184),USE(?LookupEngineer),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Account Number'),AT(225,210),USE(?tmp:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(301,210,124,10),USE(tmp:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Account Number'),TIP('Account Number'),ALRT(EnterKey),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                           BUTTON,AT(429,206),USE(?LookupAccountNumber),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:Team                Like(tmp:Team)
look:tmp:AccountNumber                Like(tmp:AccountNumber)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020766'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SparesReceivedReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:SUBTRACC.Open
  Access:TEAMS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','SparesReceivedReportCriteria')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Team{Prop:Tip} AND ~?LookupTeam{Prop:Tip}
     ?LookupTeam{Prop:Tip} = 'Select ' & ?tmp:Team{Prop:Tip}
  END
  IF ?tmp:Team{Prop:Msg} AND ~?LookupTeam{Prop:Msg}
     ?LookupTeam{Prop:Msg} = 'Select ' & ?tmp:Team{Prop:Msg}
  END
  IF ?tmp:AccountNumber{Prop:Tip} AND ~?LookupAccountNumber{Prop:Tip}
     ?LookupAccountNumber{Prop:Tip} = 'Select ' & ?tmp:AccountNumber{Prop:Tip}
  END
  IF ?tmp:AccountNumber{Prop:Msg} AND ~?LookupAccountNumber{Prop:Msg}
     ?LookupAccountNumber{Prop:Msg} = 'Select ' & ?tmp:AccountNumber{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SUBTRACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','SparesReceivedReportCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickTeams
      PickSubAccounts
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020766'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020766'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020766'&'0')
      ***
    OF ?tmp:Team
      IF tmp:Team OR ?tmp:Team{Prop:Req}
        tea:Team = tmp:Team
        !Save Lookup Field Incase Of error
        look:tmp:Team        = tmp:Team
        IF Access:TEAMS.TryFetch(tea:Team_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Team = tea:Team
          ELSE
            !Restore Lookup On Error
            tmp:Team = look:tmp:Team
            SELECT(?tmp:Team)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupTeam
      ThisWindow.Update
      tea:Team = tmp:Team
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Team = tea:Team
          Select(?+1)
      ELSE
          Select(?tmp:Team)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Team)
    OF ?LookupEngineer
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickEngineers
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupEngineer, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              tmp:UserCode = use:User_Code
              tmp:Engineer    = Clip(use:Forename) & ' ' & Clip(use:Surname)
              Select(?+2)
          Of Requestcancelled
      !        tmp:UserCode = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?tmp:Engineer)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupEngineer, Accepted)
    OF ?tmp:AccountNumber
      IF tmp:AccountNumber OR ?tmp:AccountNumber{Prop:Req}
        sub:Account_Number = tmp:AccountNumber
        !Save Lookup Field Incase Of error
        look:tmp:AccountNumber        = tmp:AccountNumber
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:AccountNumber = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            tmp:AccountNumber = look:tmp:AccountNumber
            SELECT(?tmp:AccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupAccountNumber
      ThisWindow.Update
      sub:Account_Number = tmp:AccountNumber
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:AccountNumber = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?tmp:AccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:AccountNumber)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      SparesReceivedReport(tmp:Team,tmp:UserCode,tmp:AccountNumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:Team
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Team, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Team, AlertKey)
    END
  OF ?tmp:AccountNumber
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountNumber, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountNumber, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






SparesReceivedReport PROCEDURE(func:Team,func:Engineer,func:AccountNumber)
RejectRecord         LONG,AUTO
save_job_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
local:FileName       STRING(255),STATIC
TempFilePath         CSTRING(20)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:FirstEngineer    STRING(3)
BreaksStarted        BYTE(0)
tmp:Quantity         LONG
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
tmp:ShowTeam         STRING(30)
tmp:ShowEngineer     STRING(60)
tmp:ShowAccountNumber STRING(30)
tmp:CurrentEngineer  STRING(80)
tmp:TotalParts       LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                     END
! Before Embed Point: %DataSectionBeforeReport) DESC(Data Section, Before Report Declaration) ARG()
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
SparesReceived    File,Driver('TOPSPEED'),Pre(sparec),Name(local:FileName),Create,Bindable,Thread
UserCodeKey         Key(sparec:UserCode,sparec:ShelfLocation,sparec:PartNumber),DUP
Record                  Record
UserCode                String(3)
JobNumber               Long
Team                    String(30)
ShelfLocation           String(30)
PartNumber              String(30)
PartRecordNumber        Long
ChargeableJob           Byte
                        End
                    End
! After Embed Point: %DataSectionBeforeReport) DESC(Data Section, Before Report Declaration) ARG()
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,927,7521,1250),USE(?unnamed)
                         STRING('Team:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5938,156),USE(tmp:ShowTeam),TRN,LEFT,FONT('Arial',8,,956,CHARSET:ANSI)
                         STRING('Engineer:'),AT(5000,365),USE(?string22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,781),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5938,781),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(5938,365),USE(tmp:ShowEngineer),TRN,LEFT,FONT('Arial',8,,956,CHARSET:ANSI)
                         STRING('Account Number:'),AT(5000,573),USE(?string22:3),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(5000,990),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5938,990),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6615,990),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5938,573),USE(tmp:ShowAccountNumber),TRN,LEFT,FONT('Arial',8,,956,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s18),AT(208,0),USE(sto:Shelf_Location),TRN,FONT(,8,,)
                           STRING(@s18),AT(1406,0),USE(sto:Second_Location),TRN,FONT(,8,,)
                           STRING(@s20),AT(2604,0),USE(sto:Part_Number),TRN,FONT(,8,,)
                           STRING(@s25),AT(3906,0),USE(sto:Description),TRN,FONT(,8,,)
                           STRING(@n3),AT(5573,0),USE(tmp:Quantity),TRN,FONT(,8,,)
                           STRING(@s8),AT(5885,0),USE(job:Ref_Number),TRN,RIGHT(1),FONT(,8,,)
                           STRING(@s8),AT(6510,0),USE(Bar_Code_Temp),TRN,LEFT,FONT('C128 High 12pt LJ3',12,,FONT:regular,CHARSET:ANSI)
                         END
                       END
NewPage                DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?NewPage)
                       END
Title                  DETAIL,AT(,,,333),USE(?Title)
                         STRING('Engineer:'),AT(156,52),USE(?String35),TRN,FONT(,8,,)
                         STRING(@s80),AT(677,52),USE(tmp:CurrentEngineer),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(156,260,3021,0),USE(?Line1),COLOR(COLOR:Black)
                       END
Totals                 DETAIL,AT(,,,365),USE(?Totals)
                         LINE,AT(167,52,7188,0),USE(?Line1:2),COLOR(COLOR:Black)
                         STRING('Total Parts For Engineer: '),AT(156,94),USE(?String37),TRN,FONT(,8,,)
                         STRING(@s8),AT(1458,94),USE(tmp:TotalParts),FONT(,8,,FONT:bold)
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7500,11198),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:user_name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('SPARES RECEIVED REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:address_line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:address_line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:address_line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:FaxNumber),TRN,FONT(,9,,)
                         STRING('Shelf Location'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Second Location'),AT(1406,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(2604,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Job No'),AT(6052,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Qty'),AT(5625,2083),USE(?string24:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Description'),AT(3906,2083),USE(?string24:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('SparesReceivedReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 76
      ! Save Window Name
   AddToLog('Report','Open','SparesReceivedReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:STOCK.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = RECORDS(JOBS)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Error# = 0
        If GetTempPathA(255,TempFilePath)
            local:FileName = Clip(TempFilePath) & '\SPARECREP' & Clock() & '.TMP'
        Else
            Error# = 1
        End

        Remove(local:FileName)
        Open(SparesReceived)
        If Error()
            Create(SparesReceived)
            Open(SparesReceived)
            If Error()
                Error# = 1
                Stop(Error())
            End !If Error()
        End !Error()

        If Error# = 0

            !Go through all jobs that have the status,
            !Spares Recevied, and put the "received" parts into a file
            !to print.
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:By_Status)
            job:Current_Status = '345 SPARES RECEIVED'
            Set(job:By_Status,job:By_Status)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Current_Status <> '345 SPARES RECEIVED'      |
                    Then Break.  ! End If
                RecordsProcessed += 1
                Do DisplayProgress
                !Get User Details
                If func:AccountNumber <> ''
                    If job:Account_Number <> func:AccountNumber
                        Cycle
                    End !If job:Account_Number <> func:AccountNumber
                End !If func:AccountNumber <> ''
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code   = job:Engineer
                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If func:Team <> ''
                        If use:Team <> func:Team
                            Cycle
                        End !If use:Team <> func:Team
                    End !If func:Team <> ''
                    If func:Engineer <> ''
                        If use:User_Code <> func:Engineer
                            Cycle
                        End !If use:User_Code <> func:Engineer
                    End !If func:Engineer <> ''
                Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

                If job:Chargeable_Job = 'YES'
                    Save_par_ID = Access:PARTS.SaveFile()
                    Access:PARTS.ClearKey(par:Part_Number_Key)
                    par:Ref_Number  = job:Ref_Number
                    Set(par:Part_Number_Key,par:Part_Number_Key)
                    Loop
                        If Access:PARTS.NEXT()
                           Break
                        End !If
                        If par:Ref_Number  <> job:Ref_Number      |
                            Then Break.  ! End If
                        If par:Date_Received <> '' and par:Order_Number <> ''
                            !Get stock details
                            Clear(sparec:Record)

                            Access:STOCK.Clearkey(sto:Ref_Number_Key)
                            sto:Ref_Number  = par:Part_Ref_Number
                            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                !Found
                                sparec:ShelfLocation    = sto:Shelf_Location
                            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                                sparec:ShelfLocation    = ''
                            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                            sparec:UserCode             = job:Engineer
                            sparec:JobNumber            = job:Ref_Number
                            sparec:Team                 = use:Team
                            sparec:PartNumber           = par:Part_Number
                            sparec:PartRecordNumber     = par:Record_Number
                            sparec:ChargeableJob        = 1
                            Add(SparesReceived)
                        End !If par:Date_Received <> '' and par:Order_Number <> ''
                    End !Loop
                    Access:PARTS.RestoreFile(Save_par_ID)
                End !If job:Chargeable_Job = 'YES'

                If job:Warranty_Job = 'YES'
                    Save_wpr_ID = Access:WARPARTS.SaveFile()
                    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                    wpr:Ref_Number  = job:Ref_Number
                    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                    Loop
                        If Access:WARPARTS.NEXT()
                           Break
                        End !If
                        If wpr:Ref_Number  <> job:Ref_Number      |
                            Then Break.  ! End If
                        If wpr:Date_Received <> '' and wpr:Order_Number <> ''
                            !Get stock details
                            Clear(sparec:Record)

                            Access:STOCK.Clearkey(sto:Ref_Number_Key)
                            sto:Ref_Number  = wpr:Part_Ref_Number
                            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                !Found
                                sparec:ShelfLocation    = sto:Shelf_Location
                            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                                sparec:ShelfLocation    = ''
                            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                            sparec:UserCode             = job:Engineer
                            sparec:JobNumber            = job:Ref_Number
                            sparec:Team                 = use:Team
                            sparec:PartNumber           = wpr:Part_Number
                            sparec:PartRecordNumber     = wpr:Record_Number
                            sparec:ChargeableJob        = 0
                            Add(SparesReceived)
                        End !If par:Date_Received <> '' and par:Order_Number <> ''
                    End !Loop
                    Access:WARPARTS.RestoreFile(Save_wpr_ID)
                End !If job:Chargeable_Job = 'YES'

            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
        End !Error# = 0


        tmp:TotalParts = 0
        tmp:FirstEngineer = ''
        Clear(sparec:Record)
        Set(sparec:UserCodeKey,sparec:UserCodeKey)
        Loop
            Next(SparesReceived)
            If Error()
                Break
            End !If Error
            RecordsProcessed += 1
            Do DisplayProgress

            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = sparec:UserCode
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Found
            Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign


            If tmp:FirstEngineer = ''
                tmp:FirstEngineer = sparec:UserCode
                tmp:CurrentEngineer = Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Team)
                Print(rpt:Title)
            End !If tmp:FirstEngineer = ''

            If tmp:FirstEngineer <> sparec:UserCode
                Print(rpt:Totals)
                tmp:TotalParts = 0
                Print(rpt:NewPage)
                tmp:CurrentEngineer = Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Team)
                Print(rpt:Title)
                tmp:FirstEngineer = sparec:UserCode
            End !If tmp:FirstEngineer <> sparec:UserCode


            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = sparec:JobNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found

            Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

            If sparec:ChargeableJob
                Access:PARTS.ClearKey(par:recordnumberkey)
                par:Record_Number = sparec:PartRecordNumber
                If Access:PARTS.TryFetch(par:recordnumberkey) = Level:Benign
                    !Found
                    tmp:Quantity    = par:Quantity
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found

                    Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                Else!If Access:PARTS.TryFetch(par:recordnumberkey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:PARTS.TryFetch(par:recordnumberkey) = Level:Benign

            Else !If sparec:ChargeableJob
                Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                wpr:Record_Number   = sparec:PartRecordNumber
                If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                    !Found
                    tmp:Quantity    = wpr:Quantity
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found

                    Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                Else! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign

            End !If sparec:ChargeableJob


            !Barcode Bit
            code_temp = 3
            option_temp = 0
            Bar_Code_String_Temp   = job:Ref_Number
            SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)


            tmp:TotalParts += tmp:Quantity
            Print(Rpt:Detail)
!            If tmp:FirstEngineer <> sparec:UserCode
!
!            End !If tmp:FirstEngineer <> sparec:UserCode

        End
        Print(rpt:Totals)

        Close(SparesReceived)
        Remove(SparesReceived)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          DO AsciiOutputRoutine
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        DO AsciiOutputRoutine
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','SparesReceivedReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:JOBS.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN

AsciiOutputRoutine    ROUTINE
IF RECORDS(PrintPreviewQueue)
  Wmf2AsciiName = 'C:\REPORT.TXT'
  IF FileDialog(CPCS:AsciiOutTitle,Wmf2AsciiName,CPCS:AsciiOutMasks,1)
    Wmf2Ascii(PrintPreviewQueue, Wmf2AsciiName, AsciiLineOption)
  END
END

GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  If func:Team <> ''
      tmp:ShowTeam    = func:Team
  Else !func:Team <> ''
      tmp:ShowTeam    = 'ALL'
  End !func:Team <> ''
  
  If func:Engineer <> ''
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code   = func:Engineer
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Found
          tmp:ShowEngineer    = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  
  
  Else !func:Engineer <> ''
      tmp:ShowEngineer    = 'ALL'
  End !func:Engineer <> ''
  
  If func:AccountNumber <> ''
      tmp:ShowAccountNumber   = func:AccountNumber
  Else !func:AccountNumber <> ''
      tmp:ShowAccountNumber   = 'ALL'
  End !func:AccountNumber <> ''
  
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Spares Received Report'
  END
  report{Prop:Preview} = PrintPreviewImage













StockNotification PROCEDURE(func:JobNumber,func:PartNumber,func:Description,func:Location,func:Quantity)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
save_sto_ali_id      USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:JobNumber        LONG
tmp:Manufacturer     STRING(30)
tmp:ModelNumber      STRING(30)
tmp:PartNumber       STRING(30)
tmp:Description      STRING(30)
tmp:Quantity         LONG
tmp:TotalInStock     LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(LOCATION)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,781,7521,1656),USE(?unnamed)
                         STRING('Job Number:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s8),AT(5885,156),USE(tmp:JobNumber),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Manufacturer:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,365),USE(tmp:Manufacturer),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5885,573),USE(tmp:ModelNumber),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5885,781),USE(tmp:PartNumber),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5885,990),USE(tmp:Description),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Part Number:'),AT(5000,781),USE(?string27:4),TRN,FONT(,8,,)
                         STRING('Description:'),AT(5000,990),USE(?string27:5),TRN,FONT(,8,,)
                         STRING('Model Number:'),AT(5000,573),USE(?string27:3),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(5000,1198),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,1198),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,1198),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s30),AT(4010,0),USE(sto_ali:Second_Location),TRN,FONT(,8,,)
                           STRING(@N8),AT(6042,0),USE(sto_ali:Quantity_Stock),TRN,RIGHT,FONT(,8,,)
                           STRING(@s30),AT(156,0),USE(sto_ali:Location),TRN,FONT(,8,,)
                           STRING(@s30),AT(2083,0),USE(sto_ali:Shelf_Location),TRN,FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,333),USE(?unnamed:2)
                           LINE,AT(219,52,7083,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total In Stock:'),AT(5000,104),USE(?String32),TRN,FONT(,8,,)
                           STRING(@s8),AT(6010,104),USE(tmp:TotalInStock),TRN,RIGHT,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:user_name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('STOCK NOTIFICATION REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:address_line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:address_line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:address_line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:FaxNumber),TRN,FONT(,9,,)
                         STRING('Site Location'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Shelf Location'),AT(2083,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Second Location'),AT(4010,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Quantity'),AT(6094,2083),USE(?string25),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('StockNotification')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 76
      ! Save Window Name
   AddToLog('Report','Open','StockNotification')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:LOCATION.Open
  Relate:STOCK_ALIAS.Open
  Access:JOBS_ALIAS.UseFile
  
  
  RecordsToProcess = RECORDS(LOCATION)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(LOCATION,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(loc:Location_Key)
      Process:View{Prop:Filter} = ''
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        If loc:Location <> func:Location
            Save_sto_ali_ID = Access:STOCK_ALIAS.SaveFile()
            Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Part_Description_Key)
            sto_ali:Location    = loc:Location
            sto_ali:Part_Number = func:PartNumber
            sto_ali:Description = func:Description
            Set(sto_ali:Location_Part_Description_Key,sto_ali:Location_Part_Description_Key)
            Loop
                If Access:STOCK_ALIAS.NEXT()
                   Break
                End !If
                If sto_ali:Location    <> loc:Location      |
                Or sto_ali:Part_Number <> func:PartNumber      |
                Or sto_ali:Description <> func:Description      |
                    Then Break.  ! End If
                If sto_ali:Quantity_Stock <> 0
                    tmp:TotalInStock += sto_ali:Quantity_Stock
                    Print(rpt:Detail)
                End !If sto:Quantity_Stock <> 0
            End !Loop
            Access:STOCK_ALIAS.RestoreFile(Save_sto_ali_ID)
        End !loc:Location = func:Location
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(LOCATION,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          DO AsciiOutputRoutine
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        DO AsciiOutputRoutine
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','StockNotification')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:JOBS_ALIAS.Close
    Relate:STOCK_ALIAS.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN

AsciiOutputRoutine    ROUTINE
IF RECORDS(PrintPreviewQueue)
  Wmf2AsciiName = 'c:\REPORT.TXT'
  IF FileDialog(CPCS:AsciiOutTitle,Wmf2AsciiName,CPCS:AsciiOutMasks,1)
    Wmf2Ascii(PrintPreviewQueue, Wmf2AsciiName, AsciiLineOption)
  END
END

GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'LOCATION')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  tmp:JobNumber       = func:JobNumber
  Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
  job_ali:Ref_Number  = func:JobNumber
  If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
      !Found
  
  Else! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
  
  tmp:Manufacturer    = job_ali:Manufacturer
  tmp:ModelNumber     = job_ali:Model_Number
  tmp:PartNumber      = func:PartNumber
  tmp:Description     = func:Description
  tmp:Quantity        = func:Quantity
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Stock Notification Report'
  END
  report{Prop:Preview} = PrintPreviewImage













NewFeaturesReport PROCEDURE(func:StartDate,func:EndDate)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:StartDate        DATE
tmp:EndDate          DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(NEWFEAT)
                       PROJECT(fea:Text)
                       PROJECT(fea:date)
                       PROJECT(fea:description)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT,AT(10,38,188,238),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),MM
                       HEADER,AT(10,13,188,24),USE(?unnamed:2)
                         STRING('ServiceBase Cellular - New Features'),AT(58,2),USE(?String6),TRN,FONT(,12,,FONT:bold)
                         STRING('Date'),AT(10,15),USE(?String4),TRN,FONT('Arial',8,,)
                         STRING('Enhancement/Fix'),AT(29,15),USE(?String5),TRN,FONT('Arial',8,,)
                         LINE,AT(7,21,175,0),USE(?Line1),COLOR(COLOR:Black)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,21),USE(?DetailBand)
                           STRING(@d6),AT(8,0),USE(fea:date),RIGHT,FONT('Arial',8,,)
                           STRING(@s20),AT(29,0),USE(fea:description),FONT('Arial',8,,)
                           TEXT,AT(67,0,117,15),USE(fea:Text),FONT('Arial',8,,),RESIZE
                           LINE,AT(9,17,171,0),USE(?Line2),COLOR(COLOR:Black)
                         END
                       END
                       FOOTER,AT(10,276,188,6),USE(?unnamed)
                         STRING('Page:'),AT(165,0),USE(?PagePrompt)
                         STRING(@N4),AT(177,0),PAGENO,USE(?ReportPageNo),LEFT
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('NewFeaturesReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','NewFeaturesReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  BIND('tmp:StartDate',tmp:StartDate)
  BIND('tmp:EndDate',tmp:EndDate)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  ! Before Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
  ! After Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  FileOpensReached = True
  FilesOpened = True
  Relate:NEWFEAT.Open
  
  
  RecordsToProcess = RECORDS(NEWFEAT)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(NEWFEAT,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(fea:date_key)
      Process:View{Prop:Filter} = |
      'fea:date >= tmp:StartDate AND fea:date <<= tmp:EndDate'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(RPT:DETAIL)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(NEWFEAT,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','NewFeaturesReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:NEWFEAT.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'NEWFEAT')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='NewFeaturesReport'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END







NewFeaturesReportCriteria PROCEDURE                   !Generated from procedure template - Window

tmp:StartDate        DATE
tmp:EndDate          DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Printing New Features'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Printing New Features'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('New Features Created Date Range'),AT(248,192),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Start Date'),AT(248,204),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6),AT(324,204,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           PROMPT('End Date'),AT(248,216),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6),AT(324,216,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                         END
                       END
                       BUTTON,AT(296,258),USE(?Button1),TRN,FLAT,ICON('printp.jpg')
                       BUTTON,AT(364,258),USE(?Close),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020167'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('NewFeaturesReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  tmp:StartDate   = Deformat('1/1/1990',@d6)
  tmp:EndDate     = Today()
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','NewFeaturesReportCriteria')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','NewFeaturesReportCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020167'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020167'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020167'&'0')
      ***
    OF ?Button1
      ThisWindow.Update
      NewFeaturesReport(tmp:StartDate,tmp:EndDate)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






RapidStatusAllocationsReport PROCEDURE(func:Engineer,func:Copies,func:Type)
RejectRecord         LONG,AUTO
save_jac_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:job_queue        QUEUE,PRE(jobque)
JobNumber            LONG
                     END
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:telephonenumber  STRING(20)
tmp:faxnumber        STRING(20)
tmp:count            LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Accessories      STRING(255)
tmp:Engineer         STRING(60)
tmp:BranchIdentification STRING(2)
Address:User_Name    STRING(30)
address:Address_Line1 STRING(30)
address:Address_Line2 STRING(30)
address:Address_Line3 STRING(30)
address:Post_Code    STRING(20)
address:Telephone_Number STRING(20)
address:Fax_No       STRING(20)
address:Email        STRING(50)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:ESN)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,885,7521,1344),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(5885,990),USE(tmp:Engineer),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Engineer:'),AT(5000,990),USE(?EngineerText),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,781),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,781),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,781),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,781,375,208),USE(?CPCSPgOfPgStr),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,219),USE(?detailband)
                           STRING(@s8),AT(208,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s16),AT(833,0),USE(job:ESN),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s20),AT(1927,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           TEXT,AT(3281,0,4063,156),USE(tmp:Accessories),FONT(,8,,),RESIZE
                         END
                         FOOTER,AT(0,0,,417),USE(?unnamed:2)
                           LINE,AT(302,52,6927,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Jobs:'),AT(313,104),USE(?String32),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(1667,104),USE(tmp:count),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING('Processed By:'),AT(2448,104),USE(?String36),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(3333,260,1615,0),USE(?Line2),COLOR(COLOR:Black)
                           LINE,AT(5729,260,1615,0),USE(?Line2:3),COLOR(COLOR:Black)
                           STRING('Received By:'),AT(5000,104),USE(?String36:2),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(Address:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('ALLOCATIONS REPORT'),AT(3542,0,3906,260),USE(?TitleText),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(address:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(address:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(address:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s20),AT(156,729,1156,156),USE(address:Post_Code),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1042),USE(address:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1198),USE(address:Fax_No),TRN,FONT(,9,,)
                         STRING(@s50),AT(573,1354,3844,198),USE(address:Email),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Job No'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(833,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Accessories'),AT(3281,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Model Number'),AT(1927,2083),USE(?string45:2),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('RapidStatusAllocationsReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 76
      ! Save Window Name
   AddToLog('Report','Open','RapidStatusAllocationsReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'AllocatRep'
  If PrintOption(PreviewReq,glo:ExportReport,'Allocations Report') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Access:USERS.UseFile
  Access:JOBACC.UseFile
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess    = Records(glo:Queue) * 2
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Free(tmp:job_queue)
        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            jobque:jobnumber   = glo:pointer
            Add(tmp:job_queue)
            RecordsProcessed += 1
            if ~glo:webjob then Do DisplayProgress.
        End!Loop x# = 1 To Records(glo:Queue)
        
        Sort(tmp:job_queue,jobque:jobnumber)
        Loop x# = 1 To Records(tmp:job_queue)
            Get(tmp:job_queue,x#)
            RecordsProcessed += 1
            if ~glo:webjob then Do DisplayProgress.
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number  = jobque:JobNumber
            If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                tmp:Accessories = ''
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:DamagedKey)
                jac:Ref_Number = job:Ref_Number
                jac:Damaged    = 0
                Set(jac:DamagedKey,jac:DamagedKey)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
        
                    If jac:Damaged = 1
                        jac:Accessory = 'DAMAGED ' & Clip(jac:Accessory)
                    End !If jac:Damaged = 1
        
                    If tmp:Accessories = ''
                        tmp:Accessories = jac:Accessory
                    Else !If tmp:Accessories = ''
                        tmp:Accessories = Clip(tmp:Accessories) & ', ' & jac:Accessory
                    End !If tmp:Accessories = ''
                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)
        
                tmp:count += 1
                Print(rpt:detail)
            End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
        End!Loop x# = 1 To Records(glo:Queue)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','RapidStatusAllocationsReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBACC.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  Access:USERS.ClearKey(use:Password_Key)
  use:Password = glo:Password
  If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Found
      tmp:printedby = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Error
  End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
  
  Access:USERS.Clearkey(use:User_Code_Key)
  use:User_Code   = func:Engineer
  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Found
      tmp:Engineer    = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  
   !HERE!
   printer{propprint:Copies} = func:Copies
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  SetTarget(Report)
  
  If func:Type = 'HO'
      ?TitleText{prop:Text} = 'HAND-OVER CONFIRMATION REPORT'
      ?EngineerText{prop:Text} = 'User:'
  End !func:Type = 'HO'
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  !--- Webify SETUP ClarioNET:UseReportPreview(0) tmp:BranchIdentification ---------------
  ! 26 Aug 2002 John
  ! 02 sep 2002 modified by J to refer to variables acutally used ;-)
  if glo:WebJob then
      access:tradeacc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = ClarioNET:Global.Param2
      IF Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
          tmp:BranchIdentification = tra:BranchIdentification
      ELSE
          tmp:BranchIdentification = ''
      END !IF
  
  
      !*************************set the display address to the head account if booked on as a web job***********************
      Address:User_Name=        tra:Company_Name
      address:Address_Line1=    tra:Address_Line1
      address:Address_Line2=    tra:Address_Line2
      address:Address_Line3=    tra:Address_Line3
      address:Post_Code=        tra:Postcode
      address:Telephone_Number= tra:Telephone_Number
      address:Fax_No=           tra:Fax_Number
      address:Email=            tra:EmailAddress
  
  
  
  
      !*********************************************************************************************************************
  Else
      Address:User_Name=def:User_Name
      address:Address_Line1=def:Address_Line1
      address:Address_Line2=def:Address_Line2
      address:Address_Line3=def:Address_Line3
      address:Post_Code=def:Postcode
      address:Telephone_Number=def:telephone_Number
      address:Fax_No=def:Fax_Number
      address:Email=def:EmailAddress
  END
  !---------------------------------------------------------------------------------------
  
  
  SetTarget()
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='RapidStatusAllocationsReport'
  END
  report{Prop:Preview} = PrintPreviewImage













StockRequisition PROCEDURE
Default_Invoice_Company_Name_Temp STRING(30)
tmp:RecordsCount     LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
first_page_temp      BYTE(1)
pos                  STRING(255)
Part_Queue           QUEUE,PRE()
Order_Number_Temp    REAL
Part_Number_Temp     STRING(30)
Description_Temp     STRING(30)
Quantity_Temp        LONG
Purchase_cost_temp   REAL
Sale_Cost_temp       REAL
                     END
Order_Temp           REAL
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Total_Quantity_Temp  LONG
total_cost_temp      REAL
total_cost_total_temp REAL
Total_Lines_Temp     REAL
user_name_temp       STRING(22)
no_temp              STRING('NO')
tmp:RequisitionNumber LONG
tmp:false            BYTE(0)
!-----------------------------------------------------------------------------
Process:View         VIEW(REQUISIT)
                       PROJECT(req:RecordNumber)
                       PROJECT(req:TheDate)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Parts Order'),AT(396,4604,7521,4125),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,4167),USE(?unnamed)
                         STRING(@D6b),AT(5844,760),USE(ReportRunDate),TRN,LEFT,FONT(,8,,)
                         STRING('Date Printed:'),AT(5083,760),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@T3),AT(6521,760),USE(ReportRunTime),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(156,323),USE(def:OrderAddressLine1,,?def:OrderAddressLine1:2),TRN
                         STRING(@s30),AT(156,104),USE(def:OrderCompanyName,,?def:OrderCompanyName:2),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,635),USE(def:OrderAddressLine3,,?def:OrderAddressLine3:2),TRN
                         STRING(@s30),AT(156,802),USE(def:OrderPostcode,,?def:OrderPostcode:2),TRN
                         STRING('Tel: '),AT(156,958),USE(?String15),TRN
                         STRING('Fax:'),AT(156,1094),USE(?String16),TRN
                         STRING(@s30),AT(573,1094),USE(def:OrderFaxNumber,,?def:OrderFaxNumber:2),TRN
                         STRING('Email:'),AT(156,1250,521,156),USE(?String16:2),TRN
                         STRING(@s255),AT(573,1250),USE(def:OrderEmailAddress),TRN
                         STRING(@s30),AT(156,1667),USE(sup:Company_Name),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1667),USE(def:OrderCompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1823),USE(Address_Line1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1823),USE(def:OrderAddressLine1),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1979),USE(Address_Line2_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1979),USE(def:OrderAddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2135),USE(Address_Line3_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2135),USE(def:OrderAddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2292),USE(Address_Line4_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2292),USE(def:OrderPostcode),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,2448),USE(?String26),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2448),USE(sup:Telephone_Number),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(4083,2448),USE(?String26:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4396,2448,990,188),USE(def:OrderTelephoneNumber),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(156,2604),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2604),USE(sup:Fax_Number),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(4083,2604),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4396,2604,990,188),USE(def:OrderFaxNumber),TRN,FONT(,8,,)
                         STRING(@s15),AT(156,3385),USE(sup:Account_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(4531,3385,1406,156),USE(tmp:PrintedBy),TRN,FONT(,8,,)
                         STRING(@s30),AT(5990,3385,1563,208),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(3177,3385),USE(req:TheDate),TRN,FONT(,8,,)
                         STRING(@s8),AT(1667,3385),USE(req:RecordNumber),TRN,FONT(,8,,)
                         STRING('STOCK REQUISITION'),AT(4948,104,2521,323),USE(?String19),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(573,958),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:3),TRN
                         STRING(@s30),AT(156,479),USE(def:OrderAddressLine2,,?def:OrderAddressLine2:2),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@p<<<<<<<#p),AT(52,0),USE(GLO:Q_Quantity),TRN,RIGHT,FONT(,8,,)
                           STRING(@s30),AT(844,0),USE(GLO:Q_Part_Number),TRN,FONT(,8,,)
                           STRING(@s30),AT(3083,0),USE(GLO:Q_Description),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(5094,0),USE(GLO:Q_Purchase_Cost),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6479,0),USE(total_cost_temp),TRN,RIGHT,FONT(,8,,)
                         END
detail1                  DETAIL,PAGEAFTER(-1),AT(,,,42),USE(?detail1),ABSOLUTE
                         END
Totals                   DETAIL,AT(396,9396),USE(?Totals),ABSOLUTE
                           STRING('Total Items: '),AT(396,83),USE(?String47),TRN,FONT(,10,,FONT:bold)
                           STRING('Total Lines: '),AT(396,323),USE(?String40),TRN,FONT(,,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,323),USE(Total_Lines_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING(@n14.2),AT(6156,156),USE(total_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,83),USE(Total_Quantity_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING('Total Order Value:'),AT(4844,156),USE(?String41),TRN,FONT(,,,FONT:bold)
                         END
Continue                 DETAIL,AT(396,9396),USE(?Continue),ABSOLUTE
                           STRING('Number Of Items On Page : 20'),AT(313,104),USE(?String60),TRN,FONT(,,,FONT:bold)
                           STRING('Continued Over -'),AT(5990,104),USE(?String60:2),TRN,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1125)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,10802)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('Requisition Number'),AT(1594,3177),USE(?String32),TRN,FONT(,8,,FONT:bold)
                         STRING('Requisition Date'),AT(3073,3177),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact'),AT(4531,3177),USE(?String45),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Number'),AT(5990,3177),USE(?String58),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(125,3177),USE(?String30),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1458),USE(?String42),TRN,FONT(,9,,FONT:bold)
                         STRING('Quantity'),AT(156,3823),USE(?String31),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3083,3823),USE(?String33),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(844,3823),USE(?String34),TRN,FONT(,8,,FONT:bold)
                         STRING('Item Cost'),AT(5313,3823),USE(?String35),TRN,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(6750,3823),USE(?String36),TRN,FONT(,8,,FONT:bold)
                         STRING('SUPPLIER ADDRESS'),AT(104,1458),USE(?String25),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('StockRequisition')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','StockRequisition')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'StockReq'
  If PrintOption(PreviewReq,glo:ExportReport,'Stock Requisitions') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('tmp:false',tmp:false)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:REQUISIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:ORDPARTS.Open
  Relate:STANTEXT.Open
  Access:SUPPLIER.UseFile
  Access:USERS.UseFile
  Access:STOCK.UseFile
  
  
  RecordsToProcess = RECORDS(REQUISIT)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(REQUISIT,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(req:OrderedNumberKey)
      Process:View{Prop:Filter} = |
      'req:Ordered = tmp:false'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        access:supplier.clearkey(sup:company_name_key)
        sup:company_name = req:supplier
        if access:supplier.fetch(sup:company_name_key) = Level:Benign
            address_line1_temp  = sup:address_line1
            address_line2_temp  = sup:address_line2
            If sup:address_line3 = ''
                address_line3_temp = sup:postcode
                address_line4_temp = ''
            Else
                address_line3_temp  = sup:address_line3
                address_line4_temp  = sup:postcode
            End
        end!if access:supplier.fetch(sup:company_name_key) = Level:Benign
        
        multi_print# = 1
        If glo:select1 <> ''
            If ord:order_number <> glo:select1
                multi_print# = 0
            End
        End!If glo:select1 <> ''
        
        If multi_print# = 1
        
            Sort(glo:Q_PartsOrder,glo:q_supplier,glo:q_order_number,glo:q_part_number)
        
            count# = 0
            Loop x# = 1 To Records(glo:Q_PartsOrder)
                Get(glo:Q_PartsOrder,x#)
                If glo:q_order_Number <> req:RecordNumber Then Cycle.
        
                ! #12127 If a free text Supplier. Don't show costs. (Bryan: 12/07/2011)
                SETTARGET(REPORT)
                ?glo:Q_Purchase_Cost{prop:Hide} = 0
                ?total_cost_temp{prop:Hide} = 0
        
                Access:STOCK.Clearkey(sto:Location_Key)
                sto:Location = MainStoreLocation()
                sto:Part_Number = glo:q_Part_Number
                IF (Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign)
                    IF (sto:ExchangeUnit = 'YES')
                        IF (sup:UseFreeStock)
                            ?glo:Q_Purchase_Cost{prop:Hide} = 1
                            ?total_cost_temp{prop:Hide} = 1
                        END! IF (sup:UseFreeStock)
                    END
                END ! IF (Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign)
                SETTARGET()
        
                total_cost_temp = glo:q_purchase_cost * glo:q_quantity
                total_cost_total_temp += total_cost_temp
                total_quantity_temp += glo:q_quantity
                total_lines_temp += 1
                If order_temp <> req:RecordNumber And first_page_temp <> 1
                    Print(rpt:detail1)
                End
                tmp:RecordsCount += 1
                count# += 1
                If count# > 20
                    Print(rpt:continue)
                    Print(rpt:detail1)
                    count# = 1
                End!If count# > 25
                Print(rpt:detail)
                order_temp = req:RecordNumber
                first_page_temp = 0
                print# = 1
            End!Loop x# = 1 To Records(glo:Q_PartsOrder)
        
            ! Show totals - TrkBs: 6442 (DBH: 03-10-2005)
            If print# = 1
                Print(rpt:totals)
                total_cost_total_temp = 0
                total_quantity_temp = 0
                ! Reset Total Lines - TrkBs: 6443 (DBH: 03-10-2005)
        
                total_lines_temp = 0
        !        pos = Position(ord:supplier_printed_key)
        !        ord:printed = 'YES'
        !        access:orders.update()
        !        Reset(ord:supplier_printed_key,pos)
            End!If print# = 1
        
        End!If print# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(REQUISIT,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','StockRequisition')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:ORDPARTS.Close
    Relate:REQUISIT.Close
    Relate:STANTEXT.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'REQUISIT')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !Alternative Invoice Address
  Set(Defaults)
  access:Defaults.next()
  If def:use_invoice_address = 'YES' And def:use_for_order = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='StockRequisition'
  END
  Report{Prop:Preview} = PrintPreviewImage













Goods_Received_Note PROCEDURE
Default_Invoice_Company_Name_Temp STRING(30)
tmp:RecordsCount     LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
first_page_temp      BYTE(1)
pos                  STRING(255)
Part_Queue           QUEUE,PRE()
Order_Number_Temp    REAL
Part_Number_Temp     STRING(30)
Description_Temp     STRING(30)
Quantity_Temp        LONG
Purchase_cost_temp   REAL
Sale_Cost_temp       REAL
                     END
Order_Temp           REAL
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Total_Quantity_Temp  LONG
total_cost_temp      REAL
total_cost_total_temp REAL
Total_Lines_Temp     REAL
user_name_temp       STRING(22)
no_temp              STRING('NO')
tmp:Order_No_Filter  REAL
tmp:Date_Received_Filter DATE
Parts_Q              QUEUE,PRE(pq)
PartNo               STRING(30)
Description          STRING(30)
QtyOrdered           LONG
QtyReceived          LONG
Location             STRING(30)
Cost                 REAL
                     END
parts_record_number_Temp REAL
warparts_record_number_temp REAL
retstock_record_number_temp REAL
tmp:ExchangeRate     STRING(30)
tmp:TotalValueForeign STRING(30)
tmp:OrderNumber      STRING(13)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:CurrencyLineCost REAL
tmp:ReceivedDailyRate REAL
tmp:ReceivedCurrency STRING(30)
tmp:ReceivedDivideMultiply STRING(30)
!-----------------------------------------------------------------------------
Process:View         VIEW(ORDERS)
                       PROJECT(ord:Date)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Goods Received Note'),AT(396,4604,7521,4125),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,,CHARSET:ANSI),THOUS
                       HEADER,AT(396,479,7521,4167),USE(?unnamed)
                         STRING(@D6b),AT(5844,760),USE(ReportRunDate),TRN,LEFT,FONT(,8,,)
                         STRING('Date Printed:'),AT(5083,760),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@T3),AT(6521,760),USE(ReportRunTime),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(156,323),USE(def:OrderAddressLine1,,?def:OrderAddressLine1:2),TRN
                         STRING(@s20),AT(5844,542),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:regular,CHARSET:ANSI)
                         STRING('Note No'),AT(5083,542),USE(?String66),TRN,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s30),AT(156,104),USE(def:OrderCompanyName,,?def:OrderCompanyName:2),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,635),USE(def:OrderAddressLine3,,?def:OrderAddressLine3:2),TRN
                         STRING(@s30),AT(156,802),USE(def:OrderPostcode,,?def:OrderPostcode:2),TRN
                         STRING('Tel: '),AT(156,958),USE(?String15),TRN
                         STRING('Fax:'),AT(156,1094),USE(?String16),TRN
                         STRING(@s30),AT(573,1094),USE(def:OrderFaxNumber,,?def:OrderFaxNumber:2),TRN
                         STRING('Email:'),AT(156,1250,521,156),USE(?String16:2),TRN
                         STRING(@s255),AT(573,1250),USE(def:OrderEmailAddress),TRN
                         STRING(@s30),AT(156,1667),USE(sup:Company_Name),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1667),USE(def:OrderCompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1823),USE(Address_Line1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1823),USE(def:OrderAddressLine1),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1979),USE(Address_Line2_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1979),USE(def:OrderAddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2135),USE(Address_Line3_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2135),USE(def:OrderAddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2292),USE(Address_Line4_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2292),USE(def:OrderPostcode),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,2448),USE(?String26),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2448),USE(sup:Telephone_Number),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(4083,2448),USE(?String26:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4396,2448,990,188),USE(def:OrderTelephoneNumber),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(156,2604),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2604),USE(sup:Fax_Number),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(4083,2604),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4396,2604,990,188),USE(def:OrderFaxNumber),TRN,FONT(,8,,)
                         STRING(@s15),AT(156,3385),USE(sup:Account_Number),TRN,FONT(,8,,)
                         STRING(@s13),AT(1615,3385),USE(tmp:OrderNumber),TRN,FONT(,8,,)
                         STRING(@d6b),AT(3083,3385),USE(ord:Date),TRN,FONT(,8,,)
                         STRING(@s20),AT(4531,3385,1406,156),USE(tmp:PrintedBy),TRN,FONT(,8,,)
                         STRING(@s30),AT(5990,3385,1563,208),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:2),TRN,FONT(,8,,)
                         STRING('GOODS RECEIVED NOTE'),AT(4917,83,2521,323),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(573,958),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:3),TRN
                         STRING(@s30),AT(156,479),USE(def:OrderAddressLine2,,?def:OrderAddressLine2:2),TRN
                       END
DETAIL                 DETAIL,AT(,,,198),USE(?DetailBand),FONT('Arial',7,,)
                         STRING(@s8),AT(125,0),USE(pq:QtyOrdered),TRN,RIGHT,FONT(,7,,)
                         STRING(@s30),AT(1406,0),USE(pq:PartNo),TRN,FONT(,7,,)
                         STRING(@s30),AT(3073,0),USE(pq:Description),TRN,FONT(,7,,)
                         STRING(@s8),AT(729,0),USE(pq:QtyReceived),TRN,RIGHT,FONT(,7,,)
                         STRING(@s30),AT(4688,0,1406,156),USE(pq:Location),TRN,LEFT,FONT(,7,,)
                         STRING(@n14.2),AT(6635,0),USE(total_cost_temp),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                         STRING(@n14.2),AT(5896,0),USE(tmp:CurrencyLineCost),TRN,RIGHT,FONT(,7,,)
                       END
detail1                DETAIL,PAGEAFTER(-1),AT(,,,42),USE(?detail1),ABSOLUTE
                       END
Totals                 DETAIL,AT(396,9125,,906),USE(?Totals),ABSOLUTE
                         STRING('Total Items: '),AT(396,156),USE(?String47),TRN,FONT(,10,,FONT:bold)
                         STRING('Total Lines: '),AT(396,396),USE(?String40),TRN,FONT(,,,FONT:bold)
                         STRING(@p<<<<<<<#p),AT(1323,396),USE(Total_Lines_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                         STRING('Total GRN Value (CUR):'),AT(4844,521),USE(?Text:TotalGRNValue:Currency),TRN,FONT(,,,FONT:bold)
                         STRING(@n14.2),AT(6271,521),USE(tmp:TotalValueForeign),TRN,RIGHT,FONT(,10,,FONT:bold)
                         STRING(@n14.2),AT(6271,313),USE(total_cost_total_temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                         STRING(@p<<<<<<<#p),AT(1323,156),USE(Total_Quantity_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                         STRING('Exchange Rate (CUR):'),AT(4844,104),USE(?Text:ExchangeRate),TRN,FONT(,10,,FONT:bold)
                         STRING(@n12.4),AT(6417,104),USE(tmp:ExchangeRate),TRN,RIGHT,FONT(,10,,FONT:bold)
                         STRING('Total GRN Value (R):'),AT(4844,313),USE(?strTotalGRNValue),TRN,FONT(,,,FONT:bold)
                       END
Continue               DETAIL,AT(396,9396),USE(?Continue),ABSOLUTE
                         STRING('Number of Items On Page : 20'),AT(313,104),USE(?String60),TRN,FONT(,,,FONT:bold)
                         STRING('Continued Over -'),AT(5990,104),USE(?String60:2),TRN,FONT(,,,FONT:bold)
                       END
                       FOOTER,AT(396,10156,7521,1125)
                         TEXT,AT(104,521,7344,521),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,10802),USE(?unnamed:2)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('Order Number'),AT(1594,3177),USE(?String32),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Date'),AT(3073,3177),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact'),AT(4531,3177),USE(?String45),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Number'),AT(5990,3177),USE(?String58),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(125,3177),USE(?String30),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1458),USE(?String42),TRN,FONT(,9,,FONT:bold)
                         STRING('Qty Ordered'),AT(156,3854),USE(?String31),TRN,FONT(,7,,FONT:bold)
                         STRING('Description'),AT(3073,3854),USE(?String33),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(1406,3854),USE(?String34),TRN,FONT(,7,,FONT:bold)
                         STRING('Qty Rcvd'),AT(833,3854),USE(?String35),TRN,FONT(,7,,FONT:bold)
                         STRING('Shelf Location'),AT(4688,3854),USE(?String36),TRN,FONT(,7,,FONT:bold)
                         STRING('GRN Value (CUR)'),AT(5781,3854),USE(?Text:CurrencyGRNValue),TRN,FONT(,7,,FONT:bold)
                         STRING('GRN Value (R)'),AT(6667,3854),USE(?Text:GRNValue),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Received By'),AT(104,9844),USE(?String64),TRN,FONT('Arial',9,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(938,9990,2656,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('SUPPLIER ADDRESS'),AT(104,1458),USE(?String25),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Goods_Received_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Goods_Received_Note')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'GoodsRec'
  If PrintOption(PreviewReq,glo:ExportReport,'Goods Received Note') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('tmp:Order_No_Filter',tmp:Order_No_Filter)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:ORDERS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:GRNOTES.Open
  Relate:GRNOTESAlias.Open
  Relate:RETSTOCK.Open
  Relate:STANTEXT.Open
  Access:SUPPLIER.UseFile
  Access:USERS.UseFile
  Access:ORDPARTS.UseFile
  Access:STOCK.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  ! Before Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  if GLO:Select1 <> ''
      Access:GRNOTES.ClearKey(grn:Goods_Received_Number_Key)
      grn:Goods_Received_Number = GLO:Select1
      if not Access:GRNOTES.Fetch(grn:Goods_Received_Number_Key)
          tmp:Order_No_Filter = grn:Order_Number
          tmp:Date_Received_Filter = grn:Goods_Received_Date
      end
  else
      tmp:Order_No_Filter = GLO:Select2
      tmp:Date_Received_Filter = GLO:Select3
      !get first ordernumber for this day
      access:grnotes.clearkey(grn:Order_Number_Key)
      grn:Order_Number = glo:select2
      set(grn:Order_Number_Key,grn:Order_Number_Key)
      if access:grnotes.next() = level:benign then
          glo:select1 = grn:order_number
      END !gets first GR number for this date
  end
  ! After Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  
  
  RecordsToProcess = RECORDS(ORDERS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(ORDERS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(ord:Order_Number_Key)
      Process:View{Prop:Filter} = |
      'ord:Order_Number = tmp:Order_No_Filter'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        SetTarget(Report)
        If grn:CurrencyCode <> ''
            ?Text:CurrencyGRNValue{prop:Text} = 'GRN Value (' & Clip(grn:CurrencyCode) & ')'
        Else ! If grn:CurrencyCode <> ''
            ?Text:CurrencyGRNValue{prop:Hide} = True
            ?tmp:CurrencyLineCost{prop:Hide} = True
        End ! If grn:CurrencyCode <> ''
        SetTarget()
        
        ! Work out what the suffix should be from the Order Number - TrkBs: 5110 (DBH: 17-08-2005)
        GRNSuffix# = 0
        Access:GRNOTESAlias.ClearKey(grnali:Order_Number_Key)
        grnali:Order_Number        = ord:Order_Number
        Set(grnali:Order_Number_Key,grnali:Order_Number_Key)
        Loop
        
            If Access:GRNOTESAlias.NEXT()
               Break
            End !If
        
            If grnali:Order_Number        <> ord:Order_Number      |
                Then Break.  ! End If
        
            !Does this GRN Have Parts Attached?
            PartsAttached# = False
            Access:ORDPARTS.ClearKey(orp:Part_Number_Key)
            orp:Order_Number = grnali:Order_Number
            Set(orp:Part_Number_Key,orp:Part_Number_Key)
            Loop
                If Access:ORDPARTS.NEXT()
                   Break
                End !If
                If orp:Order_Number <> grnali:Order_Number      |
                    Then Break.  ! End If
                IF (grn:Uncaptured = 1)
                    IF (orp:UncapturedGRNNumber = grnali:Goods_Received_Number)
                        PartsAttached# = 1
                        BREAK
                    END
                ELSE
                    If orp:GRN_Number = grnali:Goods_Received_Number
                        PartsAttached# = True
                        Break
                    End ! If orp:GRN_Number = grnali:Goods_Received_Number
                END
            End !Loop
        
            If PartsAttached# = False
                Cycle
            End ! If PartsAttached# = False
        
            GRNSuffix# += 1
        
            If grnali:Goods_Received_Number = grn:Goods_Received_Number
                tmp:OrderNumber = Format(ord:Order_Number,@n~SS~010) & '/' & Format(GRNSuffix#,@n02)
                Break
            End ! If grnali:Goods_Received_Number = grn:Goods_Received_Number
        
        End !Loop
        !End - Work out what the suffix should be from the Order Number - TrkBs: 5110 (DBH: 17-08-2005)
        
        access:supplier.clearkey(sup:company_name_key)
        sup:company_name = ord:supplier
        if access:supplier.fetch(sup:company_name_key) = Level:Benign
            address_line1_temp = sup:address_line1
            address_line2_temp = sup:address_line2
            If sup:address_line3 = ''
                address_line3_temp = sup:postcode
                address_line4_temp = ''
            Else
                address_line3_temp = sup:address_line3
                address_line4_temp = sup:postcode
            End ! If
        end! if access:supplier.fetch(sup:company_name_key) = Level:Benign
        
        Access:OrdParts.ClearKey(ORP:Order_Number_key)
        ORP:Order_Number =  ORD:Order_Number
        set(ORP:Order_Number_Key, ORP:Order_Number_Key)
        loop until Access:OrdParts.Next()
            if ORP:Order_Number <> ORD:Order_Number then break.
        
            IF (grn:Uncaptured = 1) ! #12342 Show "Received Items Only" (DBH: 17/02/2012)
                IF (orp:Date_Received <> tmp:Date_Received_Filter)
                    CYCLE
                END
                IF (orp:UncapturedGRNNumber <> grn:Goods_Received_Number)
                    CYCLE
                END
            ELSE
        
                if orp:date_received = '' then cycle.
                if orp:Date_Received <> tmp:Date_Received_Filter then cycle.
                ! Changing (DBH 11/01/2006) #6979 - There should always be a GRN Number in the file
                ! if orp:GRN_Number <> 0
                !         if orp:GRN_Number <> grn:Goods_Received_Number then cycle.
                !     end ! if
                ! to (DBH 11/01/2006) #6979
                If orp:GRN_Number <> grn:Goods_Received_Number
                    Cycle
                End ! If orp:GRN_Number <> grn:Goods_Received_Number
                ! End (DBH 11/01/2006) #6979
            END
            ! Inserting (DBH 11/25/2005) #6789 - Save the currency details recorded when the parts where received. They should be the same for all the parts on the grn
            ! Inserting (DBH 31/01/2006) #7106 - Only check for currency rate if it was set of the point of order
            If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> '0'
                ! End (DBH 31/01/2006) #7106
                tmp:ReceivedDailyRate      = orp:ReceivedDailyRate
                tmp:ReceivedCurrency       = orp:ReceivedCurrency
                tmp:ReceivedDivideMultiply = orp:ReceivedDivideMultiply
            Else ! If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> 0
                tmp:ReceivedDailyRate      = ''
                tmp:ReceivedCurrency       = ''
                tmp:ReceivedDivideMultiply = ''
            End ! If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> 0
            ! End (DBH 11/25/2005) #6789
        
            Parts_Q.pq:PartNo = orp:Part_Number
            Parts_Q.pq:Cost   = orp:Purchase_Cost
            get(Parts_Q, Parts_Q.pq:PartNo, Parts_Q.pq:Cost)
            if error()  ! Insert queue entry
                Parts_Q.pq:PartNo      = orp:Part_Number
                Parts_Q.pq:Description = orp:Description
                Parts_Q.pq:QtyOrdered  = orp:Quantity
                Parts_Q.pq:Cost        = orp:Purchase_Cost
        
        !        Case orp:part_type
        !            Of 'JOB'
        !                Access:PARTS.Clearkey(par:RecordNumberKey)
        !                par:Record_Number   = Parts_Record_Number_Temp
        !                If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
        !                  Parts_Q.pq:Cost = par:Purchase_Cost
        !                End
        !            of 'WAR'
        !                Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
        !                wpr:Record_Number   = Warparts_Record_Number_Temp
        !                If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey)  = Level:Benign
        !                    Parts_Q.pq:Cost = wpr:Purchase_Cost
        !                End
        !        End
        
                if orp:Date_Received <> ''
        !            if orp:Number_Received <> 0
                    Parts_Q.pq:QtyReceived = orp:Number_Received
        !            else
        !                Parts_Q.pq:QtyReceived = orp:Quantity
        !            end
                else
                    Parts_Q.pq:QtyReceived = 0
                end ! if
                Parts_Q.pq:Location = ''
        
                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:ref_number = orp:Part_Ref_Number
                if not Access:STOCK.Fetch(sto:Ref_Number_Key)
                    Parts_Q.pq:Location = sto:Shelf_Location
                end ! if
        
                add(Parts_Q, Parts_Q.pq:PartNo, Parts_Q.pq:Cost)
            else
                Parts_Q.pq:QtyOrdered += orp:Quantity
                if orp:Date_Received <> ''
        !            if orp:Number_Received <> 0
                    Parts_Q.pq:QtyReceived += orp:Number_Received
        !            else
        !                Parts_Q.pq:QtyReceived += orp:Quantity
        !            end
                end ! if
                put(Parts_Q, Parts_Q.pq:PartNo, Parts_Q.pq:Cost)
            end ! if
        
        end ! loop
        
        
        ! Show the foreign currency, if it was applied at order creation - TrkBs: 5110 (DBH: 24-05-2005)
        SetTarget(Report)
        
        If tmp:ReceivedCurrency <> ''
            tmp:ExchangeRate             = tmp:ReceivedDailyRate
            ?Text:ExchangeRate{PROP:Text} = 'Exchange Rate (' & Clip(tmp:ReceivedCurrency) & '):'
            ?Text:TotalGRNValue:Currency{prop:Text} = 'Total GRN Value (' & Clip(tmp:ReceivedCurrency) & '):'
            ?tmp:ExchangeRate{PROP:Hide}      = False
            ?tmp:TotalValueForeign{prop:Hide} = False
            ?Text:ExchangeRate{PROP:Hide}     = False
            ?Text:TotalGRNValue:Currency{prop:Hide} = False
        Else ! ord:OrderedCurrency <> ''
            ?tmp:ExchangeRate{PROP:Hide}      = True
            ?tmp:TotalValueForeign{prop:Hide} = True
            ?Text:ExchangeRate{PROP:Hide}     = True
            ?Text:TotalGRNValue:Currency{prop:Hide} = True
        End ! ord:OrderedCurrency <> ''
        
        IF (grn:Uncaptured = 1)
            ! #12342 Hide costs of "uncaptured" GRN (DBH: 17/02/2012)
            ?tmp:CurrencyLineCost{prop:Hide} = 1
            ?Total_Cost_Temp{prop:Hide} = 1
            ?Text:ExchangeRate{prop:Hide} = 1
            ?tmp:ExchangeRate{prop:Hide} = 1
            ?strTotalGRNValue{prop:Hide} = 1
            ?Total_Cost_Total_Temp{prop:Hide} = 1
            ?Text:TotalGRNValue:Currency{prop:Hide} = 1
            ?tmp:TotalValueForeign{prop:Hide} = 1
            ?Text:CurrencyGRNValue{prop:Hide} = 1
            ?Text:GRNValue{prop:Hide} = 1
        END ! IF (grn:UncapturedGRNNumber > 0)
        
        SetTarget()
        ! End   - Show the foreign currency, if it was applied at order creation - TrkBs: 5110 (DBH: 24-05-2005)
        
        sort(Parts_Q, Parts_Q.pq:PartNo)
        
        loop a# = 1 to records(Parts_Q)
            get(Parts_Q, a#)
        
        ! Changing (DBH 11/25/2005) #6789 - Use the rate at point of receipt, not order
        ! A bit of a fudge because the correct daily rate is now not being recorded in the GRN file (DBH: 25-11-2005)
        !     If grn:CurrencyCode
        !         Case grn:DivideMultiply
        !         Of '*'
        !             tmp:CurrencyLineCost = (Parts_Q.pq:Cost / grn:DailyRate) * Parts_Q.pq:QtyReceived
        !         Of '/'
        !             tmp:CurrencyLineCost = (Parts_Q.pq:Cost / grn:DailyRate) / Parts_Q.pq:QtyReceived
        !         End ! Case grn:DivideMultiply
        !         tmp:TotalValueForeign += tmp:CurrencyLineCost
        !     End ! If grn:CurrencyCode
        ! to (DBH 11/25/2005) #6789
            If tmp:ReceivedCurrency <> ''
                Case tmp:ReceivedDivideMultiply
                Of '*'
                    tmp:CurrencyLineCost = (Parts_Q.pq:Cost / tmp:ReceivedDailyRate) * Parts_Q.pq:QtyReceived
                Of '/'
                    tmp:CurrencyLineCost = (Parts_Q.pq:Cost / tmp:ReceivedDailyRate) / Parts_Q.pq:QtyReceived
                End ! Case tmp:ReceivedDivideMultiply
                tmp:TotalValueForeign += tmp:CurrencyLineCost
            End ! If tmp:ReceivedCurrency <> ''
            ! End (DBH 11/25/2005) #6789
        
            If order_temp <> ord:order_number And first_page_temp <> 1
                Print(rpt:detail1)
            End ! If
        
            tmp:RecordsCount += 1
            count#           += 1
            If count# > 20
                Print(rpt:continue)
                Print(rpt:detail1)
                count# = 1
            End! If count# > 25
        
            total_cost_temp = Parts_Q.pq:Cost * Parts_Q.pq:QtyReceived
        
            total_cost_total_temp += total_cost_temp
            total_quantity_temp   += Parts_Q.pq:QtyReceived
            total_lines_temp      += 1
        
            Print(rpt:detail)
            order_temp      = ord:order_number
            first_page_temp = 0
            print#          = 1
        end ! loop
        
        
        
        If print# = 1
            Print(rpt:totals)
        End ! If print# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(ORDERS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','Goods_Received_Note')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:GRNOTES.Close
    Relate:GRNOTESAlias.Close
    Relate:ORDERS.Close
    Relate:RETSTOCK.Close
    Relate:STANTEXT.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'ORDERS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !Alternative Invoice Address
  Set(Defaults)
  access:Defaults.next()
  If def:use_invoice_address = 'YES' And def:use_for_order = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'PARTS ORDER'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Goods_Received_Note'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


! Before Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()
Get_Record_Numbers      Routine
    data
save_par_id ushort,auto
save_wpr_id ushort,auto
save_res_id ushort,auto
    code
    setcursor(cursor:wait)
    save_par_id = access:parts.savefile()
    access:parts.clearkey(par:order_number_key)
    par:ref_number   = orp:job_number
    par:order_number = orp:order_number
    set(par:order_number_key,par:order_number_key)
    loop
        if access:parts.next()
           break
        end !if
        if par:ref_number   <> orp:job_number      |
        or par:order_number <> orp:order_number      |
            then break.  ! end if

        If par:part_number = orp:Part_Number
            parts_record_number_temp = par:record_number
            Break
        End!If par:date_received = '' And par:part_number = sav:PartNumber
    end !loop
    access:parts.restorefile(save_par_id)

    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:order_number_key)
    wpr:ref_number   = orp:job_number
    wpr:order_number = orp:order_number
    set(wpr:order_number_key,wpr:order_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number   <> orp:job_number      |
        or wpr:order_number <> orp:order_number      |
            then break.  ! end if

        If wpr:part_number = orp:Part_Number
            warparts_record_number_temp = wpr:record_number
            Break
        End!If wpr:date_received = '' And wpr:part_number = sav:PartNumber
    end !loop
    access:warparts.restorefile(save_wpr_id)

    save_res_id = access:retstock.savefile()
    access:retstock.clearkey(res:order_number_key)
    res:ref_number  = orp:job_number
    res:order_number    = orp:order_number
    Set(res:order_number_key,res:order_number_key)
    Loop
        if access:retstock.next()
            break
        End
        if res:ref_number   <> orp:job_number   |
        or res:order_number <> orp:order_number |
            then break.

        If res:part_number = orp:Part_Number
            retstock_record_number_temp = res:record_number
            Break
        End!If wpr:date_received = '' And wpr:part_number = sav:PartNumber
    End!Loop
    access:retstock.restorefile(save_res_id)

    setcursor()
! After Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()











Goods_Received_Note_Retail PROCEDURE
Default_Invoice_Company_Name_Temp STRING(30)
tmp:RecordsCount     LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
first_page_temp      BYTE(1)
pos                  STRING(255)
Part_Queue           QUEUE,PRE()
Order_Number_Temp    REAL
Part_Number_Temp     STRING(30)
Description_Temp     STRING(30)
Quantity_Temp        LONG
Purchase_cost_temp   REAL
Sale_Cost_temp       REAL
                     END
Order_Temp           REAL
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Total_Quantity_Temp  LONG
total_cost_temp      REAL
total_cost_total_temp REAL
Total_Lines_Temp     REAL
user_name_temp       STRING(22)
no_temp              STRING('NO')
tmp:Order_No_Filter  REAL
tmp:Date_Received_Filter DATE
Parts_Q              QUEUE,PRE(pq)
PartNo               STRING(30)
Description          STRING(30)
QtyOrdered           REAL
QtyReceived          LONG
Location             STRING(30)
Cost                 REAL
OrderNumber          LONG
LineCost             REAL
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(RETSALES)
                       PROJECT(ret:Date_Despatched)
                       PROJECT(ret:Ref_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Goods Received Note'),AT(396,4604,7521,4125),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,4167),USE(?unnamed)
                         STRING(@D6b),AT(5844,760),USE(ReportRunDate),TRN,LEFT,FONT(,8,,)
                         STRING('Date Printed:'),AT(5083,760),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@T3),AT(6521,760),USE(ReportRunTime),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(156,323),USE(tra:Address_Line1,,?tra:Address_Line1:2),TRN
                         STRING(@s30),AT(156,104),USE(tra:Company_Name,,?tra:Company_Name:2),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,635),USE(tra:Address_Line3,,?tra:Address_Line3:2),TRN
                         STRING('REPRINT'),AT(3385,677),USE(?Reprint),TRN,HIDE,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(156,802),USE(tra:Postcode,,?tra:Postcode:2),TRN
                         STRING('Tel: '),AT(156,958),USE(?String15),TRN
                         STRING('Fax:'),AT(156,1094),USE(?String16),TRN
                         STRING(@s30),AT(573,1094),USE(tra:Fax_Number,,?tra:Fax_Number:2),TRN
                         STRING('Email:'),AT(156,1250,521,156),USE(?String16:2),TRN
                         STRING(@s255),AT(573,1250),USE(tra:EmailAddress),TRN
                         STRING(@s30),AT(156,1667),USE(def:OrderCompanyName,,?def:OrderCompanyName:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1667),USE(tra:Company_Name),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1823),USE(def:OrderAddressLine1,,?def:OrderAddressLine1:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1823),USE(tra:Address_Line1),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1979),USE(def:OrderAddressLine2,,?def:OrderAddressLine2:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1979),USE(tra:Address_Line2),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2135),USE(def:OrderAddressLine3,,?def:OrderAddressLine3:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2135),USE(tra:Address_Line3),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2292),USE(def:OrderPostcode,,?def:OrderPostcode:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4083,2292),USE(tra:Postcode),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,2448),USE(?String26),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2448),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:3),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(4083,2448),USE(?String26:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4396,2448,990,188),USE(tra:Telephone_Number),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(156,2604),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s30),AT(521,2604,990,188),USE(def:OrderFaxNumber,,?def:OrderFaxNumber:2),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(4083,2604),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4396,2604,990,188),USE(tra:Fax_Number),TRN,FONT(,8,,)
                         STRING(@s15),AT(156,3385),USE(tra:Account_Number),TRN,FONT(,8,,)
                         STRING(@s8),AT(1615,3385),USE(ret:Ref_Number),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(3083,3385),USE(ret:Date_Despatched),TRN,FONT(,8,,)
                         STRING(@s20),AT(5844,938,1406,156),USE(tmp:PrintedBy),TRN,FONT(,8,,)
                         STRING(@s30),AT(5990,3385,1563,208),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:2),TRN,FONT(,8,,)
                         STRING('RRC GOODS RECEIVED NOTE'),AT(4396,104,2917,313),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(573,958),USE(tra:Telephone_Number,,?tra:Telephone_Number:2),TRN
                         STRING('Printed by'),AT(5083,938),USE(?String67),TRN,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s30),AT(156,479),USE(tra:Address_Line2,,?tra:Address_Line2:2),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@s8),AT(125,0),USE(pq:QtyOrdered),TRN,RIGHT,FONT(,7,,)
                           STRING(@s30),AT(1833,0),USE(pq:PartNo),TRN,FONT(,7,,)
                           STRING(@s30),AT(3333,0),USE(pq:Description),TRN,FONT(,7,,)
                           STRING(@s8),AT(833,0),USE(pq:QtyReceived),TRN,RIGHT,FONT(,7,,)
                           STRING(@s30),AT(5052,0),USE(pq:Location),TRN,LEFT,FONT(,7,,)
                           STRING(@n14.2),AT(6719,0),USE(total_cost_temp),TRN,FONT(,7,,,CHARSET:ANSI)
                         END
detail1                  DETAIL,PAGEAFTER(-1),AT(,,,42),USE(?detail1),ABSOLUTE
                         END
Totals                   DETAIL,AT(396,9396),USE(?Totals),ABSOLUTE
                           STRING('Total Items: '),AT(396,83),USE(?String47),TRN,FONT(,10,,FONT:bold)
                           STRING('Total Lines: '),AT(396,323),USE(?String40),TRN,FONT(,,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,323),USE(Total_Lines_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING(@n14.2),AT(6156,156),USE(total_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,83),USE(Total_Quantity_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING('Total GRN Value:'),AT(4844,156),USE(?String41),TRN,FONT(,,,FONT:bold)
                         END
Continue                 DETAIL,AT(396,9396),USE(?Continue),ABSOLUTE
                           STRING('Number of Items On Page : 20'),AT(313,104),USE(?String60),TRN,FONT(,,,FONT:bold)
                           STRING('Continued Over -'),AT(5990,104),USE(?String60:2),TRN,FONT(,,,FONT:bold)
                         END
detailExchange           DETAIL,AT(,,,188),USE(?detailExchange)
                           STRING(@n_8),AT(156,0),USE(pq:OrderNumber),TRN,RIGHT(1),FONT(,7,,,CHARSET:ANSI)
                           STRING(@n_8),AT(833,0),USE(pq:QtyReceived,,?pq:QtyReceived:2),TRN,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s60),AT(1354,0,2917,156),USE(pq:PartNo,,?pq:PartNo:2),TRN,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s30),AT(4427,0,1406,156),USE(pq:Description,,?pq:Description:2),TRN,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(5990,0),USE(pq:Cost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(6823,0),USE(pq:LineCost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1125)
                         TEXT,AT(104,521,7344,521),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,10802),USE(?unnamed:2)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('Sale Number'),AT(1604,3177),USE(?String32),TRN,FONT(,8,,FONT:bold)
                         STRING('Sales Date'),AT(3083,3177),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact'),AT(4531,3177),USE(?String45),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Number'),AT(5990,3177),USE(?String58),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(125,3177),USE(?String30),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1458),USE(?String42),TRN,FONT(,9,,FONT:bold)
                         GROUP,AT(104,3698,7344,500),USE(?groupTitle1)
                           STRING('Qty Ordered'),AT(156,3854),USE(?strQtyOrdered),TRN,FONT(,8,,FONT:bold)
                           STRING('Qty Received'),AT(990,3854),USE(?strQtyReceived),TRN,FONT(,8,,FONT:bold)
                           STRING('Part Number'),AT(1875,3854),USE(?String34),TRN,FONT(,8,,FONT:bold)
                           STRING('Description'),AT(3333,3854),USE(?String33),TRN,FONT(,8,,FONT:bold)
                           STRING('Shelf Location'),AT(5052,3854),USE(?String36),TRN,FONT(,8,,FONT:bold)
                           STRING('GRN Value'),AT(6719,3854),USE(?String66),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                         GROUP,AT(52,3646,7344,500),USE(?groupTitle2),TRN,HIDE
                           STRING('Line Cost'),AT(6792,3854),USE(?String69:7),TRN,FONT(,8,,FONT:bold)
                           STRING('Order No'),AT(156,3854),USE(?String69),TRN,FONT(,8,,FONT:bold)
                           STRING('Quantity'),AT(823,3854),USE(?String69:2),TRN,FONT(,8,,FONT:bold)
                           STRING('Part Number / Model Number'),AT(1354,3854),USE(?String69:3),TRN,FONT(,8,,FONT:bold)
                           STRING('Description'),AT(4427,3854),USE(?String69:4),TRN,FONT(,8,,FONT:bold)
                           STRING('Item Cost'),AT(6135,3854),USE(?String69:6),TRN,FONT(,8,,FONT:bold)
                         END
                         STRING('Received By'),AT(104,9844),USE(?String64),TRN,FONT('Arial',9,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(938,9990,2656,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('SUPPLIER ADDRESS'),AT(104,1458),USE(?String25),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Goods_Received_Note_Retail')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Goods_Received_Note_Retail')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'GoodsRecRet'
  If PrintOption(PreviewReq,glo:ExportReport,'Goods Received Note Retail') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('tmp:Order_No_Filter',tmp:Order_No_Filter)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:RETSALES.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:STANTEXT.Open
  Relate:STOCK.Open
  Access:SUPPLIER.UseFile
  Access:USERS.UseFile
  Access:RETSTOCK.UseFile
  Access:TRADEACC.UseFile
  Access:LOAN.UseFile
  ! Before Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  tmp:Order_No_Filter = GLO:Select2
  tmp:Date_Received_Filter = GLO:Select3
  
  !Fetch this traders address
  If glo:WebJob
      Access:tradeacc.clearkey(tra:Account_Number_Key)
      tra:Account_Number = ClarioNET:Global.Param2
      access:tradeacc.fetch(tra:account_Number_key)
  Else !glo:WebJOb
      Access:tradeacc.clearkey(tra:Account_Number_Key)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
      access:tradeacc.fetch(tra:account_Number_key)
  
  End !glo:WebJOb
  
  !fetch the record
  Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
  ret:Invoice_Number = tmp:Order_No_Filter
  Access:RETSALES.TryFetch(ret:Invoice_Number_Key)
  
  
  Access:USERS.Clearkey(use:Password_Key)
  use:Password = glo:Password
  Access:USERS.TryFetch(use:Password_Key)
  tmp:PrintedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE - RETAIL'
  Access:STANTEXT.Tryfetch(stt:Description_Key)
  ! After Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  
  
  RecordsToProcess = RECORDS(RETSALES)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(RETSALES,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(ret:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'ret:Invoice_Number = tmp:Order_No_Filter'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
            SETTARGET(REPORT)
            ?groupTitle1{prop:hide} = 1
            ?groupTitle2{prop:hide} = 0
            SETTARGET()
        END
        
        Access:RETSTOCK.ClearKey(res:Part_Number_Key)
        res:Ref_Number  = ret:Ref_Number
        Set(res:Part_Number_Key,res:Part_Number_Key)
        Loop
            If Access:RETSTOCK.NEXT()
               Break
            End !If
            If res:Ref_Number  <> ret:Ref_Number      |
                Then Break.  ! End If
            If ~res:Received
                Cycle
            End !If ~res:Received
            If res:GRNNumber <> glo:Select1
                Cycle
            End !If res:GRNNumber <> glo:Select1
        
            IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
                Parts_Q.PartNo = res:Part_Number
        
                Access:STOCK.Clearkey(sto:Location_Key)
                sto:Location     = MainStoreLocation()
                sto:Part_Number  = res:Part_Number
                IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
                    
                END ! IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
        
                IF (ret:ExchangeOrder = 1)
                    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                    xch:Ref_Number = res:ExchangeRefNumber
                    IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                        Parts_Q.Description = xch:ESN
                    ELSE ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                        Parts_Q.Description = ''
                    END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                    Parts_Q.PartNo  = CLIP(Parts_Q.PartNo) & ' ' & CLIP(sto:ExchangeModelNumber)
                END ! IF (ret:ExchangeOrder = 1)
                IF (ret:LoanOrder = 1)
                    Access:LOAN.Clearkey(loa:Ref_Number_Key)
                    loa:Ref_Number = res:LoanRefNumber
                    IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                        Parts_Q.Description = loa:ESN
                    ELSE ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                        Parts_Q.Description = ''
                    END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                    Parts_Q.PartNo  = CLIP(Parts_Q.PartNo) & ' ' & CLIP(sto:LoanModelNumber)
                END ! IF (ret:ExchangeOrder = 1)
        
                Parts_Q.QtyReceived = res:Quantity
                Parts_Q.Cost        = res:Item_Cost
                Parts_Q.LineCost    = res:Item_Cost
                Parts_Q.OrderNumber = res:Purchase_Order_Number
        
                ADD(Parts_Q)
            ElSE ! IF (ret:ExchangeOrder = 1)
            
        
                Parts_Q.pq:PartNo = res:Part_Number
                Parts_Q.pq:Cost = res:Purchase_Cost
                get(Parts_Q, Parts_Q.pq:PartNo, Parts_Q:pq:Cost)
                if error()  ! Insert queue entry
                    Parts_Q.pq:PartNo = res:Part_Number
                    Parts_Q.pq:Description = res:Description
                    Parts_Q.pq:QtyOrdered = res:Quantity   !=====================================
                    Parts_Q.pq:Cost = res:Item_Cost        ! Was purchase_cost    ref G132 / l277
                    if res:Despatched = 'YES'              !=====================================
            !            if res:QuantityReceived <> 0
                            Parts_Q.pq:QtyReceived = res:QuantityReceived
            !            else
            !                Parts_Q.pq:QtyReceived = res:Quantity
            !            end
                    else
                        Parts_Q.pq:QtyReceived = 0
                    end
                    Parts_Q.pq:Location = ''
        
                    !Find the shelf locatin of the RRC's part
                    Access:USERS.Clearkey(use:Password_Key)
                    use:Password    = glo:Password
                    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        !Found
                        Access:STOCK.Clearkey(sto:Location_Key)
                        sto:Location    = use:Location
                        sto:Part_Number = res:Part_Number
                        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            !Found
                            Parts_Q.pq:Location = sto:Shelf_Location
                        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        !Error
                    End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        
        
                    add(Parts_Q, Parts_Q.pq:PartNo, Parts_Q.pq:Cost)
                else
                    Parts_Q.pq:QtyOrdered += res:Quantity
                    if res:Despatched = 'YES'
            !            if res:QuantityReceived <> 0
                            Parts_Q.pq:QtyReceived += res:QuantityReceived
            !            else
            !                Parts_Q.pq:QtyReceived += res:Quantity
            !            end
                    end
                    put(Parts_Q, Parts_Q.pq:PartNo, Parts_Q.pq:Cost)
                end
            END ! IF (ret:ExchangeOrder = 1)
        end
        
        sort(Parts_Q, Parts_Q.pq:PartNo)
        
        loop a# = 1 to records(Parts_Q)
            get(Parts_Q,a#)
        
            If order_temp <> res:Ref_Number And first_page_temp <> 1
                Print(rpt:detail1)
            End
        
            tmp:RecordsCount += 1
            count# += 1
            If count# > 20
                Print(rpt:continue)
                Print(rpt:detail1)
                count# = 1
            End!If count# > 25
        
            total_cost_temp = Parts_Q.pq:Cost * Parts_Q.pq:QtyReceived
            total_cost_total_temp += total_cost_temp
            total_quantity_temp   += Parts_Q.pq:QtyReceived
            total_lines_temp      += 1
        
            IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
                Print(rpt:detailExchange)
            ELSE
                Print(rpt:detail)
            END
            order_temp = res:Ref_Number
            first_page_temp = 0
            print# = 1
        end
        
        If print# = 1
            Print(rpt:totals)
        End !If print# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RETSALES,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','Goods_Received_Note_Retail')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:RETSALES.Close
    Relate:STANTEXT.Close
    Relate:STOCK.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'RETSALES')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !Alternative Invoice Address
  Set(Defaults)
  access:Defaults.next()
  
  !removed ref G132/L277
  
  !If def:use_invoice_address = 'YES' And def:use_for_order = 'YES'
  !    Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
  !    Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
  !    Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
  !    Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
  !    Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
  !    Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
  !    Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
  !    Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  !Else!If def:use_invoice_address = 'YES'
  !    Default_Invoice_Company_Name_Temp       = DEF:User_Name
  !    Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
  !    Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
  !    Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
  !    Default_Invoice_Postcode_Temp           = DEF:Postcode
  !    Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
  !    Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
  !    Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  !End!If def:use_invoice_address = 'YES'
  
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'PARTS ORDER'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  If glo:Select4 = 'REPRINT'
      SetTarget(Report)
      ?Reprint{prop:Hide} = 0
      SetTarget()
  End !glo:Select4 = 'REPRINT'
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Goods_Received_Note_Retail'
  END
  Report{Prop:Preview} = PrintPreviewImage







