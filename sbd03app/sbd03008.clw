

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03008.INC'),ONCE        !Local module procedure declarations
                     END


TLCExport PROCEDURE (func:StartDate,func:EndDate,func:AccountNumber) !Generated from procedure template - Process

Progress:Thermometer BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
SheetDesc            CSTRING(41)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:Accessories      STRING(255)
save_jac_id          USHORT,AUTO
tmp:ExchangeIMEI     STRING(30)
tmp:vps              STRING(20)
tmp:ParcelOut        STRING(30)
tmp:JobType          STRING(30)
Process:View         VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       JOIN(jbn:RefNumberKey,job:Ref_Number)
                       END
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,148,60),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(4,4,140,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(4,30,140,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(44,40,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
Update                 PROCEDURE(),DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
LastPctValue        LONG(0)                           !---ClarioNET 20
ClarioNET:PW:UserString   STRING(40)
ClarioNET:PW:PctText      STRING(40)
ClarioNET:PW WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
               PROGRESS,USE(Progress:Thermometer,,?ClarioNET:Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
               STRING(''),AT(0,3,141,10),USE(ClarioNET:PW:UserString),CENTER
               STRING(''),AT(0,30,141,10),USE(ClarioNET:PW:PctText),CENTER
             END
?F1SS                EQUATE(1000)
!# SW2 Extension            STRING(3)
f1Action             BYTE
f1FileName           CSTRING(256)
RowNumber            USHORT
StartRow             LONG
StartCol             LONG
Template             CSTRING(129)
f1Disabled           BYTE(0)
ssFieldQ              QUEUE(tqField).
CQ                    QUEUE(tqField).
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!---------------------
ssInit         ROUTINE
!---------------------
  DO ssBuildQueue                                     ! Initialize f1 field queue
  IF NOT UsBrowse(SsFieldQ,'TLCExport',,,,CQ,SheetDesc,StartRow,StartCol,Template)
    ThisWindow.Kill
    RETURN
  END

!---------------------
ssBuildQueue   ROUTINE
!---------------------
    ssFieldQ.Name = 'job:Account_Number'
    ssFieldQ.Desc = 'CERT NO'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Order_Number'
    ssFieldQ.Desc = 'VPS'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Incoming_Consignment_Number'
    ssFieldQ.Desc = 'PARCEL IN'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:ParcelOut'
    ssFieldQ.Desc = 'PARCEL OUT'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'chr(39) & job:ESN'
    ssFieldQ.Desc = 'ORG IMEI'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'chr(39) & tmp:ExchangeIMEI'
    ssFieldQ.Desc = 'EXC IMEI'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:JobType'
    ssFieldQ.Desc = 'Job Type'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'jbn:Fault_Description'
    ssFieldQ.Desc = 'Fault Desc'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Accessories'
    ssFieldQ.Desc = 'Accessories'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Format(job:date_booked,@d17)'
    ssFieldQ.Desc = 'Date Booked'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Format(job:DOP,@d17)'
    ssFieldQ.Desc = 'DOP'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Format(job:Date_Despatched,@d17)'
    ssFieldQ.Desc = 'Desp Date'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
  SORT(ssFieldQ,+ssFieldQ.Desc)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  LastPctValue = 0                                    !---ClarioNET 37
  ClarioNET:PW:PctText = ?Progress:PctText{Prop:Text}
  ClarioNET:PW:UserString{Prop:Text} = ?Progress:UserString{Prop:Text}
  ClarioNET:OpenPushWindow(ClarioNET:PW)
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('TLCExport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  tmp:StartDate   = func:StartDAte
  tmp:EndDate     = func:EndDate
    BIND('tmp:Accessories',tmp:Accessories)
    BIND('tmp:ExchangeIMEI',tmp:ExchangeIMEI)
    BIND('tmp:JobType',tmp:JobType)
    BIND('tmp:ParcelOut',tmp:ParcelOut)
    BIND('tmp:vps',tmp:vps)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  DO ssInit
  Relate:EXCHANGE.Open
  Relate:JOBACC.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','TLCExport')
  Bryan.CompFieldColour()
  IF ThisWindow.Response <> RequestCancelled
    UsInitDoc(CQ,?F1ss,StartRow,StartCol,Template)
    ?F1SS{'Sheets("Sheet1").Name'} = SheetDesc
    IF Template = ''
      RowNumber = StartRow + 1
    ELSE
      RowNumber = StartRow
    END
  END
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:date_booked)
  ThisProcess.AddSortOrder(job:Date_Booked_Key)
  ThisProcess.AddRange(job:date_booked,tmp:StartDate,tmp:EndDate)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='TLC Export'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(JOBS,'QUICKSCAN=on')
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','TLCExport')
  Bryan.CompFieldColour()


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:JOBACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','TLCExport')
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF ProgressWindow{Prop:AcceptAll} THEN RETURN.
  jbn:RefNumber = job:Ref_Number                      ! Assign linking field value
  Access:JOBNOTES.Fetch(jbn:RefNumberKey)
  PARENT.Reset(Force)


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Progress:Cancel
                                                      ! SW2 This section changed
      IF Message('Would you like to view the partially created spreadsheet?','Process Cancelled',ICON:Question,Button:Yes+Button:No,Button:No)|
      = Button:Yes
        UsDeInit(f1FileName, ?F1SS, 1)
      END
      ReturnValue = Level:Fatal
      RETURN ReturnValue
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ThisWindow.Response = RequestCompleted
    f1FileName = 'TLC Export'
    f1Action = 2
  
    IF UsGetFileName( f1FileName, 1, f1Action )
      UsDeInit( f1FileName, ?F1SS, f1Action )
    END
    ?F1SS{'quit()'}
  ELSE
    IF RowNumber THEN ?F1SS{'quit()'}.
  END
  ReturnValue = PARENT.TakeCloseEvent()
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:ClosePushWindow(ClarioNET:PW)         !---ClarioNET 88
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.Update PROCEDURE

  CODE
  PARENT.Update
  jbn:RefNumber = job:Ref_Number                      ! Assign linking field value
  Access:JOBNOTES.Fetch(jbn:RefNumberKey)


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF LastPctValue <> Progress:Thermometer             !---ClarioNET 99
    IF INLIST(Progress:Thermometer,'5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95')
      LastPctValue = Progress:Thermometer
      ClarioNET:UpdatePushWindow(ClarioNET:PW)
    END
  END
  ReturnValue = PARENT.TakeRecord()
  ! Before Embed Point: %BeforeFillRow) DESC(SpreadWizard: Before Filling Export Row) ARG()
  Access:SUBTRACC.ClearKey(sub:Main_Account_Key)
  sub:Main_Account_Number = func:AccountNumber
  sub:Account_Number      = job:Account_Number
  If Access:SUBTRACC.TryFetch(sub:Main_Account_Key)
      Return Level:User
  End!If job:Account_Number <> func:AccountNumber
  
  tmp:ParcelOut   = job:Consignment_Number
  If job:Exchange_Unit_Number <> ''
      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
      xch:Ref_Number    = job:Exchange_Unit_Number
      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        !Found
          tmp:ExchangeIMEI  = xch:ESN
          tmp:ParcelOut     = job:Exchange_Consignment_Number
      Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
          tmp:ExchangeIMEI  = ''
      End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
  
  Else!If job:Exchange_Unit_Number <> ''
      tmp:ExchangeIMEI = ''
  End!If job:Exchange_Unit_Number <> ''
  tmp:Accessories = ''
  Save_jac_ID = Access:JOBACC.SaveFile()
  Access:JOBACC.ClearKey(jac:Ref_Number_Key)
  jac:Ref_Number = job:Ref_Number
  Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
  Loop
      If Access:JOBACC.NEXT()
         Break
      End !If
      If jac:Ref_Number <> job:Ref_Number      |
          Then Break.  ! End If
      If tmp:Accessories = ''
          tmp:Accessories = jac:Accessory
      Else!If tmp:Accessories = ''
          tmp:Accessories = Clip(tmp:Accessories) & ', ' & jac:Accessory
      End!If tmp:Accessories = ''
  End !Loop
  Access:JOBACC.RestoreFile(Save_jac_ID)
  
  If job:Warranty_Job = 'YES'
      tmp:JobType = job:Warranty_Charge_Type
  Else!If job:Warranty_Job = 'YES'
      tmp:JobType = ''
  End!If job:Warranty_Job = 'YES'
  
  ! After Embed Point: %BeforeFillRow) DESC(SpreadWizard: Before Filling Export Row) ARG()
  UsFillRow(CQ,?F1SS,RowNumber,StartCol)
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
StatusExport PROCEDURE                                !Generated from procedure template - Process

Progress:Thermometer BYTE
save_aus_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
SheetDesc            CSTRING(41)
tmp:CustomerName     STRING(60)
tmp:StatusUser       STRING(60)
tmp:StatusDate       DATE
tmp:OldStatus        STRING(30)
Process:View         VIEW(JOBS)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,148,60),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(4,4,140,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(4,30,140,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(44,40,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
LastPctValue        LONG(0)                           !---ClarioNET 20
ClarioNET:PW:UserString   STRING(40)
ClarioNET:PW:PctText      STRING(40)
ClarioNET:PW WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
               PROGRESS,USE(Progress:Thermometer,,?ClarioNET:Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
               STRING(''),AT(0,3,141,10),USE(ClarioNET:PW:UserString),CENTER
               STRING(''),AT(0,30,141,10),USE(ClarioNET:PW:PctText),CENTER
             END
?F1SS                EQUATE(1000)
!# SW2 Extension            STRING(3)
f1Action             BYTE
f1FileName           CSTRING(256)
RowNumber            USHORT
StartRow             LONG
StartCol             LONG
Template             CSTRING(129)
f1Disabled           BYTE(0)
ssFieldQ              QUEUE(tqField).
CQ                    QUEUE(tqField).
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!---------------------
ssInit         ROUTINE
!---------------------
  DO ssBuildQueue                                     ! Initialize f1 field queue
  IF NOT UsBrowse(SsFieldQ,'StatusExport',,,,CQ,SheetDesc,StartRow,StartCol,Template)
    ThisWindow.Kill
    RETURN
  END

!---------------------
ssBuildQueue   ROUTINE
!---------------------
    ssFieldQ.Name = 'Format(job:Date_Booked,@d17)'
    ssFieldQ.Desc = 'Date Booked'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Ref_Number'
    ssFieldQ.Desc = 'Job Number'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Account_Number'
    ssFieldQ.Desc = 'Account Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Model_Number'
    ssFieldQ.Desc = 'Model Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'chr(39) & job:esn'
    ssFieldQ.Desc = 'I.M.E.I. Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:CustomerName'
    ssFieldQ.Desc = 'Customer Details'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Current_Status'
    ssFieldQ.Desc = 'Status'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:OldStatus'
    ssFieldQ.Desc = 'Previous Status'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Format(aus:StatusDate,@d17)'
    ssFieldQ.Desc = 'Status Changed Date'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:StatusUser'
    ssFieldQ.Desc = 'Status Changed By'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Location'
    ssFieldQ.Desc = 'Location'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Unit_Type'
    ssFieldQ.Desc = 'Unit Type'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Manufacturer'
    ssFieldQ.Desc = 'Manufacturer'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
  SORT(ssFieldQ,+ssFieldQ.Desc)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  LastPctValue = 0                                    !---ClarioNET 37
  ClarioNET:PW:PctText = ?Progress:PctText{Prop:Text}
  ClarioNET:PW:UserString{Prop:Text} = ?Progress:UserString{Prop:Text}
  ClarioNET:OpenPushWindow(ClarioNET:PW)
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StatusExport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
    BIND('tmp:CustomerName',tmp:CustomerName)
    BIND('tmp:OldStatus',tmp:OldStatus)
    BIND('tmp:StatusDate',tmp:StatusDate)
    BIND('tmp:StatusUser',tmp:StatusUser)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  DO ssInit
  Relate:AUDSTATS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','StatusExport')
  Bryan.CompFieldColour()
  IF ThisWindow.Response <> RequestCancelled
    UsInitDoc(CQ,?F1ss,StartRow,StartCol,Template)
    ?F1SS{'Sheets("Sheet1").Name'} = SheetDesc
    IF Template = ''
      RowNumber = StartRow + 1
    ELSE
      RowNumber = StartRow
    END
  END
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:date_booked)
  ThisProcess.AddSortOrder(job:Date_Booked_Key)
  ThisProcess.AddRange(job:date_booked,GLO:Select20,GLO:Select21)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Status Report'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(JOBS,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','StatusExport')
  Bryan.CompFieldColour()


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDSTATS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','StatusExport')
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Progress:Cancel
                                                      ! SW2 This section changed
      IF Message('Would you like to view the partially created spreadsheet?','Process Cancelled',ICON:Question,Button:Yes+Button:No,Button:No)|
      = Button:Yes
        UsDeInit(f1FileName, ?F1SS, 1)
      END
      ReturnValue = Level:Fatal
      RETURN ReturnValue
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ThisWindow.Response = RequestCompleted
    f1FileName = ''
    UsDeInit(f1FileName, ?F1SS, 1)
  
  ELSE
    IF RowNumber THEN ?F1SS{'quit()'}.
  END
  ReturnValue = PARENT.TakeCloseEvent()
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:ClosePushWindow(ClarioNET:PW)         !---ClarioNET 88
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  IF LastPctValue <> Progress:Thermometer             !---ClarioNET 99
    IF INLIST(Progress:Thermometer,'5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95')
      LastPctValue = Progress:Thermometer
      ClarioNET:UpdatePushWindow(ClarioNET:PW)
    END
  END
      print# = 1
      tmp:CustomerName=''
      tmp:StatusUser=''
      tmp:StatusDate=''
      tmp:OldStatus=''
  
      Case glo:select22
          Of 'WAR'
              If job:chargeable_job = 'YES' Or job:warranty_job <> 'YES'
                  print# = 0
              End!If job:chargeable_job = 'YES'
          Of 'CHA'
              IF job:warranty_job = 'YES' Or job:chargeable_job <> 'YES'
                  print# = 0
              End!IF job:warranty_job = 'YES'
          Of 'SPL'
              If job:warranty_job <> 'YES' Or job:chargeable_job <> 'YES'
                  print# = 0
              End!If job:warranty_job <> 'YES' Or job:chargeable_job <> 'YES'
          Of 'WAS'
              If job:warranty_job <> 'YES'
                  print# = 0
              End!If job:warranty_job <> 'YES'
          Of 'CHS'
              If job:chargeable_job <> 'YES'
                  print# = 0
              End!If job:chargeable_job <> 'YES'
      End!Case glo:select22
  
      If glo:select1 <> ''
          If job:account_number <> glo:select1
              print# = 0
          End
      End
      If glo:select30 <> ''
          access:subtracc.clearkey(sub:account_number_key)
          sub:account_number = job:account_number
          if access:subtracc.fetch(sub:account_number_key) = level:benign
              If sub:main_account_number <> glo:select30
                  print# = 0
              End!If sub:main_account_number <> glo:select30
          Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
              print# = 0
          end!if access:subtracc.fetch(sub:account_number_key) = level:benign
      End!If glo:select30 <> ''
  
      If glo:select2 <> ''
          If job:current_status <> glo:select2
              print# = 0
          End
      End
  
      If glo:select3 <> ''
          If JOB:Turnaround_Time <> glo:select3
              Print# = 0
          End
      End
  
      If glo:select4 <> ''
          If job:workshop <> glo:select4
              print# = 0
          End
      End
  
      If glo:select5 <> ''
          If job:location <> glo:select5
              print# = 0
          End
      End
  
      If glo:select6 <> ''
          If job:engineer <> glo:select6
              print# = 0
          End
      End
  
      IF glo:select7 <> ''
          If job:model_number <> glo:select7
              print# = 0
          End
      End
  
      If glo:select8 <> ''
          If job:manufacturer <> glo:select8
              print# = 0
          End
      End
  
      If glo:select9 <> ''
          If job:unit_type <> glo:select9
              print# = 0
          End
      End
  
      If glo:select10 <> ''
          If job:transit_type <> glo:select10
              print# = 0
          End
      End
  
      If glo:select11 <> ''
          If job:chargeable_job = 'YES'
              If job:charge_type <> glo:select11
                  print# = 0
              End
          Else
              if glo:select22 = 'CHA'
                  print# = 0
              End!if glo:select22 = 'CHA'
          End!If job:chargeable_job = 'YES'
      End
  
      If glo:select12 <> ''
          If job:warranty_job = 'YES'
              If job:warranty_charge_type <> glo:select12
                  print# = 0
              Else
                  If glo:select22 = 'WAR'
                      print# = 0
                  End!If glo:select22 = 'WAR'
              End
          End!If job:warranty_job = 'YES'
      End
  
      If glo:select15 <> ''
          If job:chargeable_job = 'YES'
              If job:repair_type <> glo:select15
                  print# = 0
              End
          Else!If job:chargeable_job = 'YES'
              If glo:select22 = 'CHA'
                  print# = 0
              End!If job:select22 = 'CHA'
          End!If job:chargeable_job = 'YES'
      End
  
      If glo:select16 <> ''
          If job:warranty_job = 'YES'
              If job:repair_type_warranty <> glo:select16
                  print# = 0
              End
          Else!If job:warranty_job = 'YES'
              If glo:select22 = 'WAR'
                  print# = 0
              End!If glo:select22 = 'WAR'
          End!If job:warranty_job = 'YES'
      End
  
      Case glo:select17
          Of 'INV'
              Case glo:select22
                  Of 'WAR'
                      If job:invoice_number_warranty = ''
                          print# = 0
                      End!If job:invoice_number_warranty = ''
                  Of 'CHA'
                      If job:invoice_number = ''
                          print# = 0
                      End
                  Else
                      If (job:chargeable_job = 'YES' and job:invoice_number = '') Or |
                          (job:warranty_job = 'YES' and job:invoice_number_warranty = '')
                          print# = 0
                      End!If job:invoice_number = '' Of job:invoice_Number_warranty = ''
              End!Case glo:select22
          Of 'NOT'
              Case glo:select22
                  Of 'WAR'
                      If job:invoice_number_warranty <> ''
                          print# = 0
                      End!If job:invoice_number_warranty = ''
                  Of 'CHA'
                      If job:invoice_number <> ''
                          print# = 0
                      End
                  Else
                      If (job:chargeable_job = 'YES' and job:invoice_number <> '') Or |
                          (job:warranty_job = 'YES' and job:invoice_number_warranty <> '')
                          print# = 0
                      End!If job:invoice_number = '' Of job:invoice_Number_warranty = ''
              End!Case glo:select22
      End
  
      Case glo:select19
          Of 'DES'
              If job:consignment_number = ''
                  Print# = 0
              End!If job:consignment_number = ''
          Of 'NOT'
              If job:consignment_number <> ''
                  Print# = 0
              End!If job:consignment_number <> ''
      End!Case glo:select19
  
      Case glo:select18
          Of 'COM'
              If job:date_completed = ''
                  print# = 0
              End
          Of 'NOT'
              If job:date_completed <> ''
                  print# = 0
              End
      End
      If glo:select23 <> ''
          If job:exchange_status <> glo:select23
              print# = 0
          End!If job:exchange_status <> glo:select23
      End!If glo:select23
  
      If glo:select24 <> ''
          If job:loan_status <> glo:select24
              print# = 0
          End!If job:loan_status <> glo:select24
      End!If glo:select24 <> ''
  
      If print# = 1
  !        count_temp += 1
  !        tmp:RecordsCount += 1
          If job:surname  = ''
              access:subtracc.clearkey(sub:account_number_key)
              sub:account_number = job:account_number
              if access:subtracc.fetch(sub:account_number_key) = level:benign
                  access:tradeacc.clearkey(tra:account_number_key)
                  tra:account_number = sub:main_account_number
                  if access:tradeacc.fetch(tra:account_number_key) = level:benign
                      if tra:use_sub_accounts = 'YES'
                          tmp:CustomerName    = sub:company_name
                      else!if tra:use_sub_accounts = 'YES'
                          tmp:CustomerName    = tra:company_name
                      end!if tra:use_sub_accounts = 'YES'
                  end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
              end!if access:subtracc.fetch(sub:account_number_key) = level:benign
          Else!If job:surname  = ''
              tmp:CustomerName    = job:surname
          End!If job:surname  = ''
  
          Access:AUDSTATS.ClearKey(aus:NewStatusKey)
          aus:RefNumber = job:Ref_Number
          aus:Type      = 'JOB'
          aus:NewStatus = job:Current_Status
          Set(aus:NewStatusKey,aus:NewStatusKey)
          Loop
              If Access:AUDSTATS.PREVIOUS()
                 Break
              End !If
              If aus:RefNumber <> job:Ref_Number      |
              Or aus:Type      <> 'JOB'      |
              Or aus:NewStatus <> job:Current_Status      |
                  Then Break.  ! End If
              tmp:OldStatus   = aus:OldStatus
              tmp:StatusDate  = aus:DateChanged
  
              Access:USERS.Clearkey(use:User_Code_Key)
              use:User_Code   = aus:UserCode
              If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                  !Found
                  tmp:StatusUser  = Clip(use:Forename) & ' ' & Clip(use:Surname)
              Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  
              Break
  
          End !Loop
  
      Else
          Return Level:User
      End
  ReturnValue = PARENT.TakeRecord()
  UsFillRow(CQ,?F1SS,RowNumber,StartCol)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






Stock_Audit_Report PROCEDURE(STRING Audit_No)
! Before Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
qDefault    QUEUE(),PRE(qDefault),TYPE
RecordNumber    LONG()
StoAuditRecordNumber LONG()
StockRecordNumber LONG()
TotalAudited        LONG()
TotalLinesInAudit   LONG()
StockQty            LONG()
LinesNotAudited LONG()
PercentageAudited REAL()
            END ! QUEUE

locTotalAudited LONG()
locTotalLinesInAudit LONG()

i LONG()

qShortages      QUEUE(qDefault),PRE(qShortages)
                END ! QUEUE
qExcesses      QUEUE(qDefault),PRE(qExcesses)
                END ! QUEUE   
! After Embed Point: %DeclarationSection) DESC(Declaration Section) ARG()
RejectRecord         LONG,AUTO
Address:User_Name    STRING(30)
address:Address_Line1 STRING(30)
address:Address_Line2 STRING(30)
address:Address_Line3 STRING(30)
address:Post_Code    STRING(20)
Address:Telephone_Number STRING(20)
Total_Lines_in_Audit REAL
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Local_Stock_Type     STRING(15)
Stock_Type_Filter    STRING(1)
Stock_Type_Filter_Display STRING(20)
Grand_Total_Qty      LONG
Grand_Available_Qty  LONG
Stock_Category_filter STRING(30)
Summary_Filter       STRING(3)
Total_No_Of_Lines    LONG
Suppress_Zero        STRING(1)
BarCode_String       CSTRING(16)
Barcode_IMEI         CSTRING(21)
code_temp            BYTE
Option_temp          BYTE
bar_code_temp_String CSTRING(21)
bar_code_string      CSTRING(21)
Serial_No_Rep        STRING(20)
Audit_Qty            LONG
Line_Cost            REAL
Total_Line           REAL
Total_Audited        LONG
Percentage_Audited   REAL
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
Lines_Not_audited    REAL
Orig_Qty             REAL
ExportFilename       STRING(255),STATIC
WinPrint             CLASS(CWin32file)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(STOCK)
                       PROJECT(sto:Description)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Purchase_Cost)
                       PROJECT(sto:Shelf_Location)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

Report               REPORT,AT(396,2802,7521,7990),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?StringCompleted:2)
                         STRING('STOCK AUDIT REPORT'),AT(4948,573),USE(?String3),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,646),USE(address:Address_Line3),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Location :'),AT(4948,1115),USE(?String43),TRN,FONT('Arial',8,,)
                         STRING(@s30),AT(5448,1115),USE(stoa:Site_Location),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(52,833),USE(address:Post_Code),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(4948,1302),USE(?String16),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@D6),AT(6302,1302),USE(ReportRunDate),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('This Audit is TEMP COMPLETED only'),AT(115,1417),USE(?StringCompleted),TRN,HIDE,FONT(,12,,FONT:bold,CHARSET:ANSI)
                         STRING(@s20),AT(52,1010),USE(Address:Telephone_Number),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4948,1458),USE(?String16:2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@N3),AT(6250,1458),PAGENO,USE(?PageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6552,1458),USE(?String16:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6719,1458,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('String 21'),AT(3656,0,3438,260),USE(?String21),TRN,RIGHT(10),FONT('Arial',14,,FONT:bold)
                         STRING(@s30),AT(52,271),USE(address:Address_Line1),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,458),USE(address:Address_Line2),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,0),USE(Address:User_Name),FONT('Arial',14,,FONT:bold,CHARSET:ANSI)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s25),AT(104,0),USE(sto:Part_Number),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s25),AT(1688,0),USE(sto:Description),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n8),AT(5448,0),USE(Audit_Qty),TRN,RIGHT(1),FONT('Arial',8,,)
                           STRING(@n10.2),AT(5969,10),USE(sto:Purchase_Cost),TRN,RIGHT,FONT('Arial',8,,)
                           STRING(@n10.2),AT(6583,10),USE(Line_Cost),TRN,RIGHT,FONT('Arial',8,,)
                           STRING(@s30),AT(3292,0,1406,208),USE(sto:Shelf_Location),FONT('Arial',8,,)
                           STRING(@n8),AT(4875,0),USE(Orig_Qty),RIGHT(1),FONT('Arial',8,,)
                         END
                       END
detail1                DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?unnamed:6)
                       END
detail4                DETAIL,AT(,,,969),USE(?unnamed:9)
                         LINE,AT(115,42,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Totals'),AT(4844,104),USE(?String28),TRN,FONT('Arial',8,,FONT:bold)
                         LINE,AT(5417,73,1833,1),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@n10.2),AT(6615,104),USE(Total_Line),TRN,RIGHT,FONT('Arial',8,,FONT:bold)
                         STRING('Stock Lines'),AT(94,260),USE(?String32),TRN,FONT('Arial',8,,FONT:bold)
                         STRING(@n-10),AT(1625,260),USE(Total_Lines_in_Audit),TRN,RIGHT(1),FONT('Arial',8,,)
                         STRING(@n-10),AT(1625,427),USE(Total_Audited),RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Lines Not Audited'),AT(104,594),USE(?String34),TRN,FONT('Arial',8,,FONT:bold)
                         STRING(@n-10),AT(1625,594),USE(Lines_Not_audited),TRN,RIGHT(1),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Lines Audited'),AT(104,427),USE(?String26),TRN,FONT('Arial',8,,FONT:bold)
                         STRING(@n-10.2),AT(1625,760),USE(Percentage_Audited),RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('%'),AT(2240,760),USE(?String30),TRN,FONT('Arial',8,,)
                         STRING('Percentage Audited'),AT(104,760),USE(?String27),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('Total Number Of Lines:'),AT(104,94),USE(?String29),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-10),AT(1625,94),USE(Total_No_Of_Lines),TRN,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11),AT(5313,104),USE(Grand_Available_Qty),TRN,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
detail2                DETAIL,AT(,,,260),USE(?unnamed:7)
                         STRING('NO ITEMS TO REPORT'),AT(115,42,7094,208),USE(?String25),TRN,CENTER,FONT(,12,,FONT:bold)
                       END
                       FOOTER,AT(396,10833,7521,177),USE(?unnamed:3)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('Stock Code'),AT(135,1979),USE(?String5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Description'),AT(1719,1979),USE(?String6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Shelf Location'),AT(3323,1979),USE(?String31),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('Orig. Qty'),AT(4938,1979),USE(?String44),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('Qty'),AT(5813,1979),USE(?String8),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Unit Cost'),AT(6167,1979),USE(?String9),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6771,1979),USE(?String9:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Stock_Audit_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','Stock_Audit_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  ! Before Embed Point: %AfterPreviewReq) DESC(After Setting Preview Request (PreviewReq)) ARG()
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 1 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'AuditStock'
  If PrintOption(PreviewReq,glo:ExportReport,'Stock Audit') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  !message('Have preview required as '&clip(PreviewReq))
  ! After Embed Point: %AfterPreviewReq) DESC(After Setting Preview Request (PreviewReq)) ARG()
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:STOCK.Open
  Relate:DEFAULTS.Open
  Relate:GENSHORT.Open
  Access:STOAUDIT.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:STMASAUD.UseFile
  ! Before Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  ! Build Qs
  
      prog.ProgressSetup(500)
      FREE(qShortages)
      FREE(qExcesses)
  
      Access:STOAUDIT.ClearKey(stoa:Report_Key)
      stoa:Audit_Ref_No = Audit_No
      SET(stoa:Report_Key,stoa:Report_Key)
      LOOP UNTIL Access:STOAUDIT.Next() <> Level:Benign
  
          IF (stoa:Audit_Ref_No <> Audit_No)
              BREAK
          END ! IF
  
          IF (prog.InsideLoop())
              BREAK
          END ! IF
  
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = stoa:Stock_Ref_No
          IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
              CYCLE
          END ! !IF
          
          IF (stoa:Confirmed = 'Y')
  
              Total_Audited += 1
              Total_Lines_in_Audit += 1
  
          ELSE
  
              Total_Lines_in_Audit += 1
  
          END
          
          Access:GENSHORT.ClearKey(gens:Lock_Down_Key)
          gens:Audit_No = Audit_No
          gens:Stock_Ref_No = sto:Ref_Number
          SET(gens:Lock_Down_Key,gens:Lock_Down_Key)
          LOOP UNTIL Access:GENSHORT.Next() <> Level:Benign
  
              IF (gens:Audit_No <> Audit_No OR |
                  gens:Stock_Ref_No <> sto:Ref_Number)
                  BREAK
              END ! IF
  
              IF (gens:STock_Qty <= 0)
                  qShortages.RecordNumber = gens:Autonumber_Field
                  qShortages.StoAuditRecordNumber = stoa:Internal_AutoNumber
                  qShortages.StockRecordNumber = sto:Ref_Number
                  qShortages.LinesNotAudited = Total_Lines_in_Audit - Total_Audited
                  qShortages.PercentageAudited = (locTotalAudited / locTotalLinesInAudit) * 100
                  qShortages.StockQty = gens:Stock_Qty
                  ADD(qShortages)
              END ! IF
              
              IF (gens:Stock_Qty >= 0)
                  qExcesses.RecordNumber = gens:Autonumber_Field
                  qExcesses.StoAuditRecordNumber = stoa:Internal_AutoNumber
                  qExcesses.StockRecordNumber = sto:Ref_Number
                  ! qExcesses.LinesNotAudited = locTotalLinesInAudit - locTotalAudited
                  ! qExcesses.PercentageAudited = (locTotalAudited / locTotalLinesInAudit) * 100
                  qExcesses.StockQty = gens:Stock_Qty
                  ADD(qExcesses)
              END ! IF
              
          END ! LOOP
          
      END ! LOOP
  
      prog.ProgressFinish()
  ! After Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  
  
  RecordsToProcess = BYTES(STOCK)
  RecordsPerCycle = 10
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(STOCK,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      !Set Records For Progress Window
      RecordsToProcess = RECORDS(Stock)
      
      
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !!Okay, we need to go through , H, G, and X & Y
        !    SETTARGET(Report)
        !    ?String21{PROP:Text} = 'SHORTAGES'
        !    ?String29{PROP:Text} = 'Total Shortage Lines'
        !
        !    Access:STMASAUD.clearkey(stom:AutoIncrement_Key)
        !    stom:Audit_No = Audit_no
        !    if access:STMASAUD.fetch(stom:AutoIncrement_Key)
        !    !error
        !    END
        !
        !    if stom:Complete = 'N' then
        !        unhide(?StringCompleted)
        !    END
        !
        !    SETTARGET()
        !
        !    if glo:ExportToCSV = 'Y' then
        !    !top of page lines
        !        WinPrint.write('"STOCK AUDIT REPORT","'&clip(stom:branch)&'"<13,10>"'&|
        !            Format(today(),@d7)&'"<13,10>"SHORTAGE"<13,10>')
        !
        !        if Stom:Complete = 'N' then
        !            WinPrint.write('"This Audit is not fully completed"<13,10,13,10>')
        !        END
        !
        !        Winprint.write('"Stock Code","Description","Shelf Location","Orig Qty","Qty","Unit Cost","Line Cost"<13,10>')
        !    END
        !
        !!Loop not here any more
        !    Access:StoAudit.ClearKey(stoa:Report_Key)
        !    STOA:Audit_Ref_No = Audit_No
        !    SET(stoa:Report_Key,stoa:Report_Key)
        !    LOOP
        !
        !        IF Access:StoAudit.Next()
        !            BREAK
        !        END
        !
        !        IF STOA:Audit_Ref_No <> Audit_No
        !            BREAK
        !        END
        !
        !  !Got a stock code, let's see if it is +/-!
        !  !Update Progress Display
        !        SETTARGET(ProgressWindow)
        !        DO DisplayProgress
        !
        !  !Counter here to see how many items were actually audited!
        !  !hang 5!
        !        Access:Stock.ClearKey(sto:Ref_Number_Key)
        !        sto:Ref_Number = STOA:Stock_Ref_No
        !        IF Access:Stock.Fetch(sto:Ref_Number_Key)
        !    !Error!
        !            CYCLE
        !        END
        !        IF stoa:Confirmed = 'Y'
        !            Total_Audited +=1
        !            Total_Lines_in_Audit += 1
        !        ELSE
        !            Total_Lines_in_Audit += 1
        !        END
        !
        !  !OR if it's just general!
        !        Access:GenShort.ClearKey(GENS:Lock_Down_Key)
        !        GENS:Audit_No = Audit_No
        !        GENS:Stock_Ref_No = sto:Ref_Number
        !        SET(GENS:Lock_Down_Key,GENS:Lock_Down_Key)
        !        LOOP
        !            IF Access:GenShort.Next()
        !                BREAK
        !            END
        !            IF GENS:Audit_No <> Audit_No
        !                BREAK
        !            END
        !            IF GENS:Stock_Ref_No <> sto:Ref_Number
        !                BREAK
        !            END
        !            IF GENS:Stock_Qty > 0
        !                CYCLE
        !            END
        !    !got one?!
        !            Serial_No_Rep = 'N/A'
        !            Grand_Available_Qty+=-1*(GENS:Stock_Qty)
        !            Audit_Qty = -1*(GENS:Stock_Qty)
        !            Orig_Qty = stoa:Original_Level
        !            Line_Cost = sto:Purchase_Cost * Audit_Qty
        !            Total_No_Of_Lines += 1
        !            Total_Line += Line_Cost
        !            Lines_Not_audited = Total_Lines_in_Audit - Total_Audited
        !            Percentage_Audited = (Total_Audited/Total_Lines_in_Audit)*100
        !            if glo:ExportToCSV = 'Y' then
        !                Winprint.write('"'&   clip(sto:Part_Number)              &'","'&|
        !                    clip(sto:Description)              &'","'&|
        !                    clip(sto:Shelf_Location)           &'","'&|
        !                    format(Orig_Qty,@n9)               &'","'&|
        !                    format(Audit_Qty,@n9)              &'","'&|
        !                    format(sto:Purchase_Cost,@n-10.2)  &'","'&|
        !                    format(Line_Cost,@n-10.2)          &'"<13,10>')
        !            ELSE
        !                PRINT(RPT:Detail)
        !            END
        !        END
        !    END
        !
        !    Lines_Not_audited = Total_Lines_in_Audit - Total_Audited
        !    Percentage_Audited = (Total_Audited/Total_Lines_in_Audit)*100
        !    IF Total_No_Of_Lines = 0
        !        if glo:ExportToCSV = 'Y' then
        !            WinPrint.write('<13,10,13,10>,,"NO ITEMS TO REPORT"<13,10,13,10>')
        !        ELSE
        !        !message('Printing detail 2')
        !            PRINT(RPT:Detail2)
        !        END
        !    ELSE
        !        if glo:ExportToCSV = 'Y' then
        !            WinPrint.write('<13,10>"Total Shortage Lines","'   &format(Total_No_Of_Lines,@n9)&'",,"Totals","'&format(Grand_Available_Qty,@n9)&'",,"'&format(Total_Line,@n9.2)&'"<13,10>'&|
        !                '"Stock Lines","'            &format(Total_Lines_in_Audit,@n9)  &'"<13,10>'&|
        !                '"Lines Audited","'          &format(Total_Audited,@n9)         &'"<13,10>'&|
        !                '"Lines Not Audited","'      &format(Lines_Not_audited,@n9)     &'"<13,10>'&|
        !                '"Percentage Audited","'     &format(Percentage_Audited,@n9.2)  &'%"<13,10>')
        !
        !        ELSE
        !            PRINT(RPT:Detail4)
        !        END
        !    END
        !
        !    if glo:ExportToCSV = 'Y' then
        !        Winprint.write('<13,10>')
        !    ELSe
        !        PRINT(RPT:Detail1)
        !    END
        !    Grand_Available_Qty = 0
        !    Total_No_Of_Lines = 0
        !    Total_Line = 0
        !
        !!=============================================================================
        !!========================SECOND PART - DO THE EXCESSES========================
        !!=============================================================================
        !
        !    SETTARGET(Report)
        !    ?String21{PROP:Text} = 'EXCESSES'
        !    ?String29{PROP:Text} = 'Total Excesses Lines'
        !    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        !    tra:Account_Number  = Clarionet:Global.Param2
        !    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !    !Found
        !        address:User_Name        = tra:company_name
        !        address:Address_Line1    = tra:address_line1
        !        address:Address_Line2    = tra:address_line2
        !        address:Address_Line3    = tra:address_line3
        !        address:Post_code        = tra:postcode
        !        address:Telephone_Number = tra:telephone_number
        !
        !    Else ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !    !Error
        !    End !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !
        !    SETTARGET()
        !
        !
        !    if glo:ExportToCSV = 'Y' then
        !    !top of page lines
        !        WinPrint.write('"EXCESSES"<13,10,13,10>')
        !
        !    END
        !
        !
        !!Loop not here any more
        !    Access:StoAudit.ClearKey(stoa:Report_Key)
        !    STOA:Audit_Ref_No = Audit_No
        !    SET(stoa:Report_Key,stoa:Report_Key)
        !    LOOP
        !        IF Access:StoAudit.Next()
        !            BREAK
        !        END
        !        IF STOA:Audit_Ref_No <> Audit_No
        !            BREAK
        !        END
        !  !Got a stock code, let's see if it is +/-!
        !  !Update Progress Display
        !        SETTARGET(ProgressWindow)
        !        DO DisplayProgress
        !
        !  !Counter here to see how many items were actually audited!
        !  !hang 5!
        !        Access:Stock.ClearKey(sto:Ref_Number_Key)
        !        sto:Ref_Number = STOA:Stock_Ref_No
        !        IF Access:Stock.Fetch(sto:Ref_Number_Key)
        !    !Error!
        !            CYCLE
        !        END
        !        IF stoa:Confirmed = 'Y'
        !    !Total_Audited +=1
        !    !Total_Lines_in_Audit += 1
        !    !MESSAGE('A'&Total_Lines_in_Audit)
        !        ELSE
        !    !Total_Lines_in_Audit += 1
        !    !MESSAGE('B'&Total_Lines_in_Audit)
        !        END
        !  !OR if it's just general!
        !        Access:GenShort.ClearKey(GENS:Lock_Down_Key)
        !        GENS:Audit_No = Audit_No
        !        GENS:Stock_Ref_No = sto:Ref_Number
        !        SET(GENS:Lock_Down_Key,GENS:Lock_Down_Key)
        !        LOOP
        !            IF Access:GenShort.Next()
        !                BREAK
        !            END
        !            IF GENS:Audit_No <> Audit_No
        !                BREAK
        !            END
        !            IF GENS:Stock_Ref_No <> sto:Ref_Number
        !                BREAK
        !            END
        !            IF GENS:Stock_Qty < 0
        !                CYCLE
        !            END
        !    !got one?!
        !            Serial_No_Rep = 'N/A'
        !            Grand_Available_Qty+=(GENS:Stock_Qty)
        !            Audit_Qty = (GENS:Stock_Qty)
        !            Orig_Qty = stoa:Original_Level
        !            Line_Cost = sto:Purchase_Cost * Audit_Qty
        !            Total_Line += Line_Cost
        !            Total_No_Of_Lines += 1
        !            Lines_Not_audited = Total_Lines_in_Audit - Total_Audited
        !            Percentage_Audited = (Total_Audited/Total_Lines_in_Audit)*100
        !            if glo:ExportToCSV = 'Y' then
        !                Winprint.write('"'&   clip(sto:Part_Number)       &'","'&|
        !                    clip(sto:Description)              &'","'&|
        !                    clip(sto:Shelf_Location)           &'","'&|
        !                    format(Orig_Qty,@n9)               &'","'&|
        !                    format(Audit_Qty,@n9)              &'","'&|
        !                    format(sto:Purchase_Cost,@n-10.2)  &'","'&|
        !                    format(Line_Cost,@n-10.2)          &'"<13,10>')
        !            ELSE
        !                PRINT(RPT:Detail)
        !            END
        !        END
        !    END
        !
        !    IF Total_No_Of_Lines = 0
        !        if glo:ExportToCSV = 'Y' then
        !            WinPrint.write('<13,10,13,10>,,"NO ITEMS TO REPORT"<13,10,13,10>')
        !        ELSE
        !        !message('Printing detail 2')
        !            PRINT(RPT:Detail2)
        !        END
        !    ELSE
        !        if glo:ExportToCSV = 'Y' then
        !            WinPrint.write('<13,10>"Total Excess Lines","'     &format(Total_No_Of_Lines,@n9)&'",,"Totals","'&format(Grand_Available_Qty,@n9)&'",,"'&format(Total_Line,@n9.2)&'"<13,10>'&|
        !                '"Stock Lines","'            &format(Total_Lines_in_Audit,@n9)  &'"<13,10>'&|
        !                '"Lines Audited","'          &format(Total_Audited,@n9)         &'"<13,10>'&|
        !                '"Lines Not Audited","'      &format(Lines_Not_audited,@n9)     &'"<13,10>'&|
        !                '"Percentage Audited","'     &format(Percentage_Audited,@n9.2)  &'%"<13,10>')
        !
        !        ELSe
        !            PRINT(RPT:Detail4)
        !        END
        !    END
        !
        !    if glo:ExportToCSV = 'Y' then
        !        WinPrint.close()
        !        Miss# = missive('CSV Export Complete : |'&clip(ExportFilename),'ServiceBase 3g','Mexclam.jpg','OK')
        !        if glo:WebJob then
        !            SendFileToClient(ExportFilename)
        !            Remove(ExportFilename)
        !        END
        !    END
        !
        !    BREAK
        ! #978 Refractor code to try and speed up for WM (DBH: 19/08/2013)
            RecordsToProcess = RECORDS(qShortages) + RECORDS(qExcesses)
            
            SETTARGET(Report)
            ?String21{PROP:Text} = 'SHORTAGES'
            ?String29{PROP:Text} = 'Total Shortage Lines'
        
            Access:STMASAUD.clearkey(stom:AutoIncrement_Key)
            stom:Audit_No = Audit_no
            if access:STMASAUD.fetch(stom:AutoIncrement_Key)
            !error
            END
        
            if stom:Complete = 'N' then
                unhide(?StringCompleted)
            END
        
            SETTARGET()
        
            if glo:ExportToCSV = 'Y' then
            !top of page lines
                WinPrint.write('"STOCK AUDIT REPORT","'&clip(stom:branch)&'"<13,10>"'&|
                    Format(today(),@d7)&'"<13,10>"SHORTAGE"<13,10>')
        
                if Stom:Complete = 'N' then
                    WinPrint.write('"This Audit is not fully completed"<13,10,13,10>')
                END
        
                Winprint.write('"Stock Code","Description","Shelf Location","Orig Qty","Qty","Unit Cost","Line Cost"<13,10>')
            END
            
            LOOP i = 1 TO RECORDS(qShortages)   
                GET(qShortages,i)
                
                SETTARGET(ProgressWindow)
                DO DisplayProgress
                
                Access:STOAUDIT.ClearKey(stoa:Internal_AutoNumber_Key)
                stoa:Internal_AutoNumber = qShortages.StoAuditRecordNumber
                IF (Access:STOAUDIT.TryFetch(stoa:Internal_AutoNumber_Key))
                END ! IF
                
                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = qShortages.StockRecordNumber
                IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
                END ! IF
                
        !        Total_Audited += qShortages.TotalAudited
        !        Total_Lines_in_Audit += qShortages.TotalLinesInAudit
                
                Serial_No_Rep = 'N/A'
                Grand_Available_Qty += -1 * qShortages.StockQty
                Audit_Qty = -1 * qShortages.StockQty
                Orig_Qty = stoa:Original_Level
                Line_Cost = sto:Purchase_Cost * Audit_Qty
                Total_No_Of_Lines += 1
                Total_Line += Line_Cost
                Lines_Not_Audited = qShortages.TotalLinesInAudit - qShortages.TotalAudited
                Percentage_Audited = (qShortages.TotalAudited / qShortages.TotalLinesInAudit) * 100
                
                IF (glo:ExportToCSV = 'Y')
                    Winprint.write('"'&   clip(sto:Part_Number)              &'","'&|
                        clip(sto:Description)              &'","'&|
                        clip(sto:Shelf_Location)           &'","'&|
                        format(Orig_Qty,@n9)               &'","'&|
                        format(Audit_Qty,@n9)              &'","'&|
                        format(sto:Purchase_Cost,@n-10.2)  &'","'&|
                        format(Line_Cost,@n-10.2)          &'"<13,10>')        
                ELSE !
                    PRINT(rpt:Detail)
                END ! 
                
            END ! LOOP
            
            Lines_Not_audited = Total_Lines_in_Audit - Total_Audited
            Percentage_Audited = (Total_Audited/Total_Lines_in_Audit)*100
            IF Total_No_Of_Lines = 0
                if glo:ExportToCSV = 'Y' then
                    WinPrint.write('<13,10,13,10>,,"NO ITEMS TO REPORT"<13,10,13,10>')
                ELSE
                !message('Printing detail 2')
                    PRINT(RPT:Detail2)
                END
            ELSE
                if glo:ExportToCSV = 'Y' then
                    WinPrint.write('<13,10>"Total Shortage Lines","'   &format(Total_No_Of_Lines,@n9)&'",,"Totals","'&format(Grand_Available_Qty,@n9)&'",,"'&format(Total_Line,@n9.2)&'"<13,10>'&|
                        '"Stock Lines","'            &format(Total_Lines_in_Audit,@n9)  &'"<13,10>'&|
                        '"Lines Audited","'          &format(Total_Audited,@n9)         &'"<13,10>'&|
                        '"Lines Not Audited","'      &format(Lines_Not_audited,@n9)     &'"<13,10>'&|
                        '"Percentage Audited","'     &format(Percentage_Audited,@n9.2)  &'%"<13,10>')
        
                ELSE
                    PRINT(RPT:Detail4)
                END
            END
        
            if glo:ExportToCSV = 'Y' then
                Winprint.write('<13,10>')
            ELSe
                PRINT(RPT:Detail1)
            END
            Grand_Available_Qty = 0
            Total_No_Of_Lines = 0
            Total_Line = 0
            
        !=============================================================================
        !========================SECOND PART - DO THE EXCESSES========================
        !=============================================================================
        
            SETTARGET(Report)
            ?String21{PROP:Text} = 'EXCESSES'
            ?String29{PROP:Text} = 'Total Excesses Lines'
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = Clarionet:Global.Param2
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
                address:User_Name        = tra:company_name
                address:Address_Line1    = tra:address_line1
                address:Address_Line2    = tra:address_line2
                address:Address_Line3    = tra:address_line3
                address:Post_code        = tra:postcode
                address:Telephone_Number = tra:telephone_number
        
            Else ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            End !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
            SETTARGET()
        
        
            if glo:ExportToCSV = 'Y' then
            !top of page lines
                WinPrint.write('"EXCESSES"<13,10,13,10>')
        
            END    
            
            LOOP i = 1 TO RECORDS(qExcesses)   
                GET(qExcesses,i)
                
                SETTARGET(ProgressWindow)
                DO DisplayProgress
                
                Access:STOAUDIT.ClearKey(stoa:Internal_AutoNumber_Key)
                stoa:Internal_AutoNumber = qExcesses.StoAuditRecordNumber
                IF (Access:STOAUDIT.TryFetch(stoa:Internal_AutoNumber_Key))
                END ! IF
                
                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = qExcesses.StockRecordNumber
                IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
                END ! IF
                
                ! Total_Audited += qExcesses.TotalAudited
                ! Total_Lines_in_Audit += qExcesses.TotalLinesInAudit
                
                Serial_No_Rep = 'N/A'
                Grand_Available_Qty += qExcesses.StockQty
                Audit_Qty = qExcesses.StockQty
                Orig_Qty = stoa:Original_Level
                Line_Cost = sto:Purchase_Cost * Audit_Qty
                Total_No_Of_Lines += 1
                Total_Line += Line_Cost
                Lines_Not_Audited = Total_Lines_in_Audit - Total_Audited
                Percentage_Audited = (Total_Audited/Total_Lines_in_Audit) * 100
                
                IF (glo:ExportToCSV = 'Y')
                    Winprint.write('"'&   clip(sto:Part_Number)              &'","'&|
                        clip(sto:Description)              &'","'&|
                        clip(sto:Shelf_Location)           &'","'&|
                        format(Orig_Qty,@n9)               &'","'&|
                        format(Audit_Qty,@n9)              &'","'&|
                        format(sto:Purchase_Cost,@n-10.2)  &'","'&|
                        format(Line_Cost,@n-10.2)          &'"<13,10>')        
                ELSE !
                    PRINT(rpt:Detail)
                END ! 
                
            END ! LOOP    
            
            IF Total_No_Of_Lines = 0
                if glo:ExportToCSV = 'Y' then
                    WinPrint.write('<13,10,13,10>,,"NO ITEMS TO REPORT"<13,10,13,10>')
                ELSE
                !message('Printing detail 2')
                    PRINT(RPT:Detail2)
                END
            ELSE
                if glo:ExportToCSV = 'Y' then
                    WinPrint.write('<13,10>"Total Excess Lines","'     &format(Total_No_Of_Lines,@n9)&'",,"Totals","'&format(Grand_Available_Qty,@n9)&'",,"'&format(Total_Line,@n9.2)&'"<13,10>'&|
                        '"Stock Lines","'            &format(Total_Lines_in_Audit,@n9)  &'"<13,10>'&|
                        '"Lines Audited","'          &format(Total_Audited,@n9)         &'"<13,10>'&|
                        '"Lines Not Audited","'      &format(Lines_Not_audited,@n9)     &'"<13,10>'&|
                        '"Percentage Audited","'     &format(Percentage_Audited,@n9.2)  &'%"<13,10>')
        
                ELSe
                    PRINT(RPT:Detail4)
                END
            END
        
            if glo:ExportToCSV = 'Y' then
                WinPrint.close()
                Miss# = missive('CSV Export Complete : |'&clip(ExportFilename),'ServiceBase 3g','Mexclam.jpg','OK')
                if glo:WebJob then
                    SendFileToClient(ExportFilename)
                    Remove(ExportFilename)
                END
            END
        
            BREAK
            
            
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(STOCK,'QUICKSCAN=off').
  ! Before Embed Point: %EndOfReportGeneration) DESC(End of Report Generation) ARG()
  if glo:ExportToCSV = 'Y' then CPCSDummyDetail = 1.
  !CPCSDummyDetail is used when the "[] Generate a Page even if NO Detail is printed" is ticked
  !That is needed for the previews to work properly, but this turns it off at the last second
  ! After Embed Point: %EndOfReportGeneration) DESC(End of Report Generation) ARG()
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        OMIT('**End Omit Nothing Message**')
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
        OMIT('**End Omit Nothing Message**')
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        OMIT('**End Omit Nothing Message**')
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
        OMIT('**End Omit Nothing Message**')
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','Stock_Audit_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:GENSHORT.Close
    Relate:STMASAUD.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = Clarionet:Global.Param2
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      address:User_Name        = tra:company_name
      address:Address_Line1    = tra:address_line1
      address:Address_Line2    = tra:address_line2
      address:Address_Line3    = tra:address_line3
      address:Post_code         = tra:postcode
      address:Telephone_Number = tra:telephone_number
  
  Else ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !After Opening The Report
  Total_No_Of_Lines = 0
  if glo:ExportToCSV = 'Y' then
      !message('This will export to CSV')
      ExportFilename = SetReportsFolder('ServiceBase Export','Stock Audit Report',glo:WebJob) & 'Stock_Audit_'&format(Today(),@d12)&'_'&format(Clock(),@t5)&'.csv'
      !message('Export file name '&clip(ExportFilename))
      WinPrint.init(ExportFilename,append_mode)
  END
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Stock Audit'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: UnivAbcReport
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        ?Prog:CancelButton{prop:Hide} = 1
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 20
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0





NetworkReportCriteria PROCEDURE                       !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Colour           STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
window               WINDOW('Colour Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Colour Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Colour'),AT(225,171),USE(?tmp:Colour:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(301,171,124,10),USE(tmp:Colour),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Colour'),TIP('Colour'),UPR
                           BUTTON,AT(429,168),USE(?LookupColour),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Booked From'),AT(225,194),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6),AT(301,194,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           BUTTON,AT(368,190),USE(?LookupStartDate),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Booked To'),AT(225,216,68,12),USE(?tmp:EndDate:Prompt),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6),AT(301,216,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),REQ,UPR
                           BUTTON,AT(368,211),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:Colour                Like(tmp:Colour)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020166'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('NetworkReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:COLOUR.Open
  SELF.FilesOpened = True
  !Prime Dates
  tmp:StartDate   = Deformat('1/1/1990',@d6)
  tmp:EndDate     = Today()
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Change Colour Name?
  If GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?WindowTitle{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & ' Report Criteria'
      ?tmp:Colour:prompt{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
  End !GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ! Save Window Name
   AddToLog('Window','Open','NetworkReportCriteria')
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Colour{Prop:Tip} AND ~?LookupColour{Prop:Tip}
     ?LookupColour{Prop:Tip} = 'Select ' & ?tmp:Colour{Prop:Tip}
  END
  IF ?tmp:Colour{Prop:Msg} AND ~?LookupColour{Prop:Msg}
     ?LookupColour{Prop:Msg} = 'Select ' & ?tmp:Colour{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COLOUR.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','NetworkReportCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickDefaultColour
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020166'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020166'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020166'&'0')
      ***
    OF ?tmp:Colour
      IF tmp:Colour OR ?tmp:Colour{Prop:Req}
        col:Colour = tmp:Colour
        !Save Lookup Field Incase Of error
        look:tmp:Colour        = tmp:Colour
        IF Access:COLOUR.TryFetch(col:Colour_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Colour = col:Colour
          ELSE
            !Restore Lookup On Error
            tmp:Colour = look:tmp:Colour
            SELECT(?tmp:Colour)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupColour
      ThisWindow.Update
      col:Colour = tmp:Colour
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Colour = col:Colour
          Select(?+1)
      ELSE
          Select(?tmp:Colour)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Colour)
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      NetworkReport(tmp:Colour,tmp:StartDate,tmp:EndDate)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






NetworkReport PROCEDURE(func:Colour,func:StartDate,func:EndDate)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:Count            LONG
tmp:ManModel         STRING(60)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Current_Status)
                       PROJECT(job:ESN)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:date_booked)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(406,844,7521,1438),USE(?unnamed)
                         STRING('Colour:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(func:Colour),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@d6),AT(5885,365),USE(tmp:StartDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@d6),AT(5885,573),USE(tmp:EndDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Booked To:'),AT(5000,573),USE(?string22:3),TRN,FONT(,8,,)
                         STRING('Booked From:'),AT(5000,365),USE(?string22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,781),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,781),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,990),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,990),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,990),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,1198),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,1198),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,1198),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,1198,375,208),USE(?CPCSPgOfPgStr),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@d6b),AT(156,0),USE(job:date_booked),FONT(,7,,,CHARSET:ANSI)
                           STRING(@s25),AT(677,0),USE(sub:Company_Name),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s12),AT(2292,0),USE(job:Order_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s8),AT(2969,0),USE(job:Ref_Number),TRN,RIGHT(1),FONT(,7,,,CHARSET:ANSI)
                           STRING(@s18),AT(3490,0),USE(job:ESN),TRN,LEFT(1),FONT(,7,,,CHARSET:ANSI)
                           STRING(@s25),AT(4531,0,1354,156),USE(tmp:ManModel),TRN,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s30),AT(5938,0,1510,156),USE(job:Current_Status),TRN,FONT(,7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(313,52,6771,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Jobs:'),AT(156,156),USE(?String41),TRN
                           STRING(@s8),AT(1615,156),USE(tmp:Count),TRN,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7500,11250),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('COLOUR REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:FaxNumber),TRN,FONT(,9,,)
                         STRING('Booked'),AT(156,2083),USE(?string44),TRN,FONT(,7,,FONT:bold)
                         STRING('Account Name'),AT(677,2083),USE(?string45),TRN,FONT(,7,,FONT:bold)
                         STRING('Order No'),AT(2292,2083),USE(?string24),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Job Status'),AT(5938,2083),USE(?string25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Manufacturer / Model No'),AT(4531,2083),USE(?string25:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('I.M.E.I. No'),AT(3490,2083),USE(?string24:3),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Job No'),AT(3083,2083),USE(?string24:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('NetworkReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','NetworkReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('tmp:StartDate',tmp:StartDate)
  BIND('tmp:EndDate',tmp:EndDate)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  ! Before Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  tmp:StartDate = func:StartDate
  tmp:EndDate = func:EndDate
  ! After Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job:Date_Booked_Key)
      Process:View{Prop:Filter} = |
      'job:date_booked >= tmp:StartDate AND job:date_booked <<= tmp:EndDate'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        !Get the Sub Account to fill in the Name on the report
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = job:Account_Number
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
        
        Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        
        tmp:ManModel    = Clip(job:Manufacturer) & ' / ' & Clip(job:Model_Number)
        
        
        Print# = 1
        If func:Colour <> ''
            If job:Colour <> func:Colour
                Print# = 0
            End !If job:Colour <> func:Network
        End !func:Network <> ''
        
        If Print# = 1
            tmp:Count += 1
            Print(Rpt:Detail)
        End !Print# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','NetworkReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBS.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  If GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      SetTarget(Report)
      ?String20{prop:Text} = Clip(Upper(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))) & ' REPORT'
      ?String22{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & ':'
      SetTarget()
  End !GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='NetworkReport'
  END
  report{Prop:Preview} = PrintPreviewImage







BrowseStatusCriteria PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(STATREP)
                       PROJECT(star:Description)
                       PROJECT(star:Location)
                       PROJECT(star:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
star:Description       LIKE(star:Description)         !List box control field - type derived from field
star:Location          LIKE(star:Location)            !List box control field - type derived from field
star:RecordNumber      LIKE(star:RecordNumber)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Status Report Types'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('240L(2)|M~Description~@s60@0L(2)|M'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Status Report Type File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6E7EFH),SPREAD
                         TAB('By Description'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(264,98,124,10),USE(star:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Description'),TIP('Description'),UPR
                           BUTTON,AT(448,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           STRING(@s40),AT(436,180,76,12),USE(GLO:Select50),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(448,220),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(448,247),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020153'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseStatusCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:STATREP.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STATREP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !sort out the users location - use as limit on browse and insert
    if glo:webjob then
        GLO:Select50 = clip(ClarioNET:Global.Param2)
    Else
        GLO:Select50 = ''  !getini('BOOKING','HeadAccount',,clip(path())&'\SB2KDEF.INI')
    End !if glo webjob
      ! Save Window Name
   AddToLog('Window','Open','BrowseStatusCriteria')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,star:LocationDescriptionKey)
  BRW1.AddRange(star:Location,GLO:Select50)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?star:Description,star:Description,1,BRW1)
  BRW1.AddField(star:Description,BRW1.Q.star:Description)
  BRW1.AddField(star:Location,BRW1.Q.star:Location)
  BRW1.AddField(star:RecordNumber,BRW1.Q.star:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STATREP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseStatusCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateStatusCriteria
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020153'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020153'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020153'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?star:Description
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateStatusCriteria PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::star:Record LIKE(star:RECORD),STATIC
QuickWindow          WINDOW('Update Status Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Status Report Type Description'),AT(248,196),USE(?star:Description:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s60),AT(248,212,184,10),USE(star:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Description'),TIP('Description'),REQ,UPR
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Status Report Type'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(304,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Insert A Status Report Criteria'
  OF ChangeRecord
    ActionMessage = 'Changing A Status Report Criteria'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020154'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateStatusCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel5
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(star:Record,History::star:Record)
  SELF.AddHistoryField(?star:Description,2)
  SELF.AddUpdateFile(Access:STATREP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STATREP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STATREP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !set the default location
  star:Location = glo:select50
      ! Save Window Name
   AddToLog('Window','Open','UpdateStatusCriteria')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STATREP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateStatusCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020154'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020154'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020154'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

InsertCriteriaDescription PROCEDURE                   !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Description      STRING(60)
window               WINDOW('Insert Status Report Criteria Name'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Status Report Name'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Description'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(252,206,176,10),USE(tmp:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Description'),TIP('Description'),REQ,UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Description)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020164'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InsertCriteriaDescription')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:STATCRIT.Open
  Access:STATREP.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','InsertCriteriaDescription')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STATCRIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','InsertCriteriaDescription')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020164'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020164'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020164'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      Error# = 0
      If tmp:Description = ''
          Select(?tmp:Description)
          Error# = 1
      End !tmp:Description = ''
      
      If Error# = 0
          Access:STATREP.ClearKey(star:DescriptionKey)
          star:Description = tmp:Description
          If Access:STATREP.TryFetch(star:DescriptionKey) = Level:Benign
              !Found
              Case Missive('The selected Description has already been saved. Do you wish to overwrite it?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      relate:STATREP.delete(0)
                  Of 1 ! No Button
                      tmp:Description = ''
                      Select(?tmp:Description)
                      Error# = 1
              End ! Case Missive
          Else!If Access:STATREP.TryFetch(star:DescriptionKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:STATREP.TryFetch(star:DescriptionKey) = Level:Benign
      End !Error# = 0
      
      If Error# = 0
          Post(Event:CloseWindow)
      End !Error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:Description = ''
      Post(event:Closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
StatusReportValidation PROCEDURE  (func:DateRangeType,func:StartDate,func:EndDate,func:JobType,func:InvoiceType,func:DespatchedType,func:CompletedType,func:StatusType,func:JobBatchNumber,func:EDIBatchNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
        Case func:DateRangeType
            Of 0
                If ~INRANGE(job:Date_Booked,func:StartDate,func:EndDate)
                    Return Level:Fatal
                End !If ~INRANGE(job:Date_Booked,func:Start_Date,func:End_Date)
            Of 1
                If ~INRANGE(job:Date_Completed,func:StartDate,func:EndDate)
                    Return Level:Fatal
                End !If ~INRANGE(job:Date_Booked,func:Start_Date,func:End_Date)
        End !Case func:DateRangeType

        !Well - If webjob, no need for any of the rest!
        IF Glo:WebJob
          Access:WebJob.ClearKey(wob:RefNumberKey)
          wob:RefNumber = job:ref_number
          IF Access:WebJob.Fetch(wob:RefNumberKey)
            !Error!
            Return Level:Fatal
          ELSE
            If wob:HeadAccountNumber <> ClarioNET:Global.Param2
              Return Level:Fatal
            END
          END

            !Check Generic Accounts
            If Records(glo:Queue)
                Clear(glo:Queue)
                Sort(glo:Queue,glo:Pointer)
                glo:Pointer = job:Account_Number
                Get(glo:Queue,glo:Pointer)
                If Error()
                    Return Level:Fatal
                End !If Error()

            End !If Records(glo:Queue)

        END

        !Check Sub Account
        If Records(glo:queue2)
            Clear(glo:Queue2)
            Sort(glo:Queue2,glo:Pointer2)
            glo:Pointer2    = job:Account_Number
            Get(glo:Queue2,glo:Pointer2)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:queue2)


        IF ~glo:Webjob
            Case func:JobType
                Of 1 !Warranty Only
                    If job:chargeable_job = 'YES' Or job:warranty_job <> 'YES'
                        Return Level:Fatal
                    End!If job:chargeable_job = 'YES'
                Of 4 !Chargeable Only
                    IF job:warranty_job = 'YES' Or job:chargeable_job <> 'YES'
                        Return Level:Fatal
                    End!IF job:warranty_job = 'YES'
                Of 3 !Split Only
                    If job:warranty_job <> 'YES' Or job:chargeable_job <> 'YES'
                        Return Level:Fatal
                    End!If job:warranty_job <> 'YES' Or job:chargeable_job <> 'YES'
                Of 2 !Warranty Split
                    If job:warranty_job <> 'YES'
                        Return Level:Fatal
                    End!If job:warranty_job <> 'YES'
                Of 5 !Chargeable Split
                    If job:chargeable_job <> 'YES'
                        Return Level:Fatal
                    End!If job:chargeable_job <> 'YES'
            End!Case glo:select22

            If func:JobBatchNumber <> 0
                If func:JobBatchNumber <> job:Batch_Number
                    Return Level:Fatal
                End !If func:JobBatchNumber <> job:Batch_Number
            End !If func:JobBatchNumber <> 0

            If func:EDIBatchNumber <> 0
                If func:EDIBatchNumber <> job:EDI_Batch_Number
                    Return Level:Fatal
                End !If func:EDIBatchNumber <> job:EDI_Batch_Number
            End !If func:EDIBatchNumber <> 0

            Case func:InvoiceType
                Of 1 !Invoiced Only
                    Case func:JobType
                        Of 1 Orof 2 !Warranty Only, or Warranty (Split)
                            If job:invoice_number_warranty = ''
                                Return Level:Fatal
                            End!If job:invoice_number_warranty = ''
                        Of 4 Orof 5 !Chargeable Only, or Chargeable (Split)
                            If job:invoice_number = ''
                                Return Level:Fatal
                            End
                        Else
                            If (job:chargeable_job = 'YES' and job:invoice_number = '') Or |
                                (job:warranty_job = 'YES' and job:invoice_number_warranty = '')
                                Return Level:Fatal
                            End!If job:invoice_number = '' Of job:invoice_Number_warranty = ''
                    End!Case glo:select22
                Of 2 !UnInvoiced Only
                    Case func:JobType
                        Of 1 Orof 2 !Warranty Only, or Warranty (Split)
                            If job:invoice_number_warranty <> ''
                                Return Level:Fatal
                            End!If job:invoice_number_warranty = ''
                        Of 4 Orof 5 !Chargealble Only, or Chargeable (Split)
                            If job:invoice_number <> ''
                                Return Level:Fatal
                            End
                        Else
                            If (job:chargeable_job = 'YES' and job:invoice_number <> '') Or |
                                (job:warranty_job = 'YES' and job:invoice_number_warranty <> '')
                                Return Level:Fatal
                            End!If job:invoice_number = '' Of job:invoice_Number_warranty = ''
                    End!Case glo:select22
            End

            Case func:DespatchedType
                Of 1 !Despatched Only
                    If job:consignment_number = ''
                        Return Level:Fatal
                    End!If job:consignment_number = ''
                Of 2 !Non-Despatched Only
                    If job:consignment_number <> ''
                        Return Level:Fatal
                    End!If job:consignment_number <> ''
            End!Case glo:select19

            Case func:CompletedType
                Of 1 !Completed Only
                    If job:date_completed = ''
                        Return Level:Fatal
                    End
                Of 2 !Incomplete Only
                    If job:date_completed <> ''
                        Return Level:Fatal
                    End
            End


            !Check Head Account
            If Records(glo:Queue)
              Access:WebJob.ClearKey(wob:RefNumberKey)
              wob:RefNumber = job:ref_number
              IF Access:WebJob.Fetch(wob:RefNumberKey)
                !Error!
                Return Level:Fatal
              ELSE
                Clear(glo:Queue)
                Sort(glo:Queue,glo:Pointer)
                glo:Pointer = wob:HeadAccountNumber
                Get(glo:Queue,glo:Pointer)
                If Error()
                    Return Level:Fatal
                End !If Error()
              END


!                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
!                sub:Account_Number   = job:Account_Number
!                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!                    !Found
!                    Clear(glo:Queue)
!                    Sort(glo:queue,glo:Pointer)
!                    glo:Pointer = sub:Main_Account_Number
!                    Get(glo:queue,glo:Pointer)
!                    If Error()
!                        Return Level:Fatal
!                    End !If Error()
!                Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            End !If Records(glo:Queue)


            If job:Chargeable_Job = 'YES'
                !Check Chargeable Charge Type
                If Records(glo:Queue3)
                    Clear(glo:Queue3)
                    Sort(glo:Queue3,glo:Pointer3)
                    glo:Pointer3    = job:Charge_Type
                    Get(glo:Queue3,glo:Pointer3)
                    If Error()
                        Return Level:Fatal
                    End !If Error()
                End !If Records(glo:Queue3)

                !Check Chargeable Repair Type
                If Records(glo:queue5)
                    Clear(glo:Queue5)
                    Sort(glo:Queue5,glo:Pointer5)
                    glo:Pointer5    = job:Repair_Type
                    Get(glo:Queue5,glo:Pointer5)
                    If Error()
                        Return Level:Fatal
                    End !If Error()
                End !If Records(glo:queue5)
            End !If job:Chargeable_Job = 'YES'

            If job:Warranty_Job = 'YES'
                !Check Warranty Charge Type
                If Records(glo:Queue4)
                    Clear(glo:Queue4)
                    Sort(glo:Queue4,glo:Pointer4)
                    glo:Pointer4 = job:Warranty_Charge_Type
                    Get(glo:Queue4,glo:Pointer4)
                    If Error()
                        Return Level:Fatal
                    End !If Error()
                End !If Records(glo:Queue4)

                !Check Warranty Repair Type
                If Records(glo:Queue6)
                    Clear(glo:Queue6)
                    Sort(glo:Queue6,glo:Pointer6)
                    glo:Pointer6    = job:Repair_Type_Warranty
                    Get(glo:Queue6,glo:Pointer6)
                    If Error()
                        Return Level:Fatal
                    End !If Error()
                End !If Records(glo:Queue6)
            End !If job:Warranty_Job = 'YES'
        End !If ~Glo:Webjob

        !Check Status
        If Records(glo:queue7)
            Clear(glo:Queue7)
            Sort(glo:Queue7,glo:Pointer7)

            Case func:StatusType
                Of 0 !Job Status
                    glo:Pointer7    = job:Current_Status
                Of 1 !Exchange Status
                    glo:Pointer7    = job:Exchange_Status
                Of 2 !Loan Status
                    glo:Pointer7    = job:Loan_Status
            End !Case func:StatusType

            Get(glo:Queue7,glo:Pointer7)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:queue7)

        !Check Locations
        If Records(glo:queue8)
            Clear(glo:Queue8)
            Sort(glo:Queue8,glo:Pointer8)
            glo:Pointer8    = job:Location
            Get(glo:Queue8,glo:Pointer8)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:queue8)

        IF ~glo:WebJob

            !Check Engineers

            If Records(glo:Queue9)
                Clear(glo:Queue9)
                Sort(glo:Queue9,glo:Pointer9)
                glo:Pointer9    = job:engineer
                Get(glo:Queue9,glo:Pointer9)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If Records(glo:Queue9)

            !Check Manufacturer
            If Records(glo:Queue10)
                Clear(glo:Queue10)
                Sort(glo:Queue10,glo:Pointer10)
                glo:Pointer10   = job:Manufacturer
                Get(glo:Queue10,glo:Pointer10)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If Records(glo:Queue10)


            If Records(glo:queue11)
                Clear(glo:Queue11)
                Sort(glo:Queue11,glo:Pointer11)
                glo:Pointer11   = job:Model_Number
                Get(glo:Queue11,glo:Pointer11)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If Records(glo:queue11)

            !Check Unit Types
            If Records(glo:queue12)
                Clear(glo:Queue12)
                Sort(glo:Queue12,glo:Pointer12)
                glo:Pointer12   = job:Unit_Type
                Get(glo:Queue12,glo:Pointer12)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If Records(glo:queue12)

            !Check Transit Type
            If Records(glo:queue13)
                Clear(glo:Queue13)
                Sort(glo:Queue13,glo:Pointer13)
                glo:Pointer13   = job:Transit_Type
                Get(glo:Queue13,glo:Pointer13)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If Records(glo:queue13)

            !Check Turnaround Time
            If Records(glo:queue14)
                Clear(glo:Queue14)
                Sort(glo:Queue14,glo:Pointer14)
                glo:Pointer14   = job:Turnaround_Time
                Get(glo:Queue14,glo:Pointer14)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If Records(glo:queue14)
       End !IF ~Glo:Webjob
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()






StatusReport PROCEDURE(func:DateRangeType,func:StartDate,func:EndDate,func:JobType,func:InvoiceType,func:DespatchedType,func:CompletedType,func:StatusType,func:ReportOrder,func:StatusReportType,func:JobBatchNumber,func:EDIBatchNumber,func:CustomerStatus)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Local                CLASS
GetDays              Procedure(Byte,Byte,Date),Long
                     END
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
save_aus_id          USHORT,AUTO
tmp:DefaultFax       STRING(20)
page_one_temp        LONG
count_temp           LONG
save_job_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
value_temp           REAL
invoice_job_type_temp STRING(30)
completed_job_type_temp STRING(30)
Date_range_Type_Temp STRING(30)
tmp:PrintedBy        STRING(60)
job_type_temp        STRING(60)
tmp:DespatchType     STRING(60)
tmp:custname         STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:OldStatus        STRING(30)
tmp:StatusDate       DATE
tmp:StatusUser       STRING(3)
tmp:status           STRING(30)
tmp:StatusType       STRING(1)
tmp:DaysInStatus     LONG
tmp:Loaned           STRING(3)
tmp:DateRange        STRING(60)
tmp:OSDays           LONG
tmp:Job_Number       STRING(20)
tmp:JobType          STRING(1)
tmp:StatusNo         BYTE
DefaultAddress       GROUP,PRE(address)
Name                 STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
Telephone            STRING(30)
Fax                  STRING(30)
EmailAddress         STRING(255)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:Engineer)
                       PROJECT(job:Location)
                       PROJECT(job:Model_Number)
                       PROJECT(job:date_booked)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

report               REPORT('Status Report'),AT(458,1938,10938,5719),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,),LANDSCAPE,THOUS
                       HEADER,AT(427,323,9740,1375),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(address:Name),TRN,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(104,260,3531,198),USE(address:AddressLine1),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(7448,573),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(8438,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,365,3531,198),USE(address:AddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,469,3531,198),USE(address:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,573),USE(address:Postcode),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(7448,729,625,208),USE(?String67),TRN,FONT(,8,,)
                         STRING('Date Range:'),AT(7448,417),USE(?RunPrompt:2),TRN,FONT(,8,,)
                         STRING(@s60),AT(8438,417),USE(tmp:DateRange),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(8438,729),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Tel: '),AT(104,729),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,729),USE(address:Telephone),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(104,833),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,833),USE(address:Fax),TRN,FONT(,8,,)
                         STRING(@s255),AT(573,938,3531,198),USE(address:EmailAddress),TRN,FONT(,8,,)
                         STRING('Email:'),AT(104,938),USE(?String16:2),TRN,FONT(,9,,)
                         STRING('Page:'),AT(7448,885),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@s4),AT(8438,885),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(8802,885),USE(?String72),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(9063,885,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                       END
break1                 BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,146),USE(?DetailBand)
                           STRING(@s3),AT(10302,0),USE(job:Engineer,,?job:Engineer:2),TRN,FONT(,7,,)
                           STRING(@s1),AT(9688,0),USE(tmp:JobType),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s4),AT(7917,0),USE(tmp:OSDays),TRN,FONT(,7,,)
                           STRING(@s3),AT(9938,0),USE(tmp:Loaned),TRN,FONT(,7,,)
                           STRING(@s25),AT(8281,0),USE(job:Location,,?job:Location:2),TRN,FONT(,7,,)
                           STRING(@d6b),AT(63,0),USE(job:date_booked),TRN,FONT(,7,,)
                           STRING(@s20),AT(563,0),USE(tmp:Job_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s12),AT(1625,0),USE(job:Account_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s15),AT(3438,0,990,156),USE(job:Model_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s20),AT(2375,0,1042,156),USE(tmp:custname),TRN,LEFT,FONT(,7,,)
                           STRING(@s16),AT(4479,0),USE(job:ESN),TRN,LEFT,FONT(,7,,)
                           STRING(@s30),AT(5625,0),USE(tmp:status),TRN,FONT(,7,,)
                           STRING(@s4),AT(7396,0),USE(tmp:DaysInStatus),TRN,FONT(,7,,)
                           STRING(@s1),AT(5458,0),USE(tmp:StatusType),TRN,FONT(,7,,)
                         END
detail1                  DETAIL,AT(,,,146),USE(?unnamed:4)
                           STRING(@d6b),AT(63,0),USE(job:date_booked,,?job:date_booked:2),TRN,FONT(,7,,)
                           STRING(@s20),AT(563,0),USE(tmp:Job_Number,,?tmp:Job_Number:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s15),AT(1625,0,729,156),USE(job:Model_Number,,?job:Model_Number:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s30),AT(9167,0),USE(tmp:status,,?tmp:Status:2),TRN,FONT(,7,,)
                           STRING(@s15),AT(3438,0),USE(job:Account_Number,,?job:Account_Number:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s20),AT(4375,0),USE(job:ESN,,?job:ESN:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s3),AT(8854,0),USE(tmp:StatusUser),TRN,FONT(,7,,)
                           STRING(@d6),AT(8229,0),USE(tmp:StatusDate,,?tmp:StatusDate:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s30),AT(6510,0),USE(tmp:OldStatus),TRN,FONT(,7,,)
                           STRING(@s3),AT(6042,0),USE(job:Engineer),TRN,FONT(,7,,)
                           STRING(@s20),AT(2375,10),USE(job:Location),TRN,FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:3)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,156),USE(?String68),TRN,FONT(,10,,FONT:bold)
                           STRING(@s9),AT(1198,156),USE(count_temp),TRN,FONT(,10,,FONT:bold)
                         END
                       END
                       FORM,AT(406,313,10969,7448),USE(?unnamed)
                         IMAGE('Rlistlan.gif'),AT(0,0,10938,7448),USE(?Image1)
                         STRING('STATUS REPORT'),AT(4625,52),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         GROUP,AT(52,1198,10729,365),USE(?MainTitle),BOXED
                           STRING('Booked'),AT(125,1354),USE(?String23),TRN,FONT(,7,,FONT:bold)
                           STRING('Job No'),AT(615,1354),USE(?String20),TRN,FONT(,7,,FONT:bold)
                           STRING('Account No'),AT(1458,1354),USE(?String21),TRN,FONT(,7,,FONT:bold)
                           STRING('Model No'),AT(3438,1354),USE(?String25),TRN,FONT(,7,,FONT:bold)
                           STRING('Cust Details'),AT(2292,1354),USE(?String27),TRN,FONT(,7,,FONT:bold)
                           STRING('I.M.E.I. No'),AT(4531,1354),USE(?String29),TRN,FONT(,7,,FONT:bold)
                           STRING('Days'),AT(7396,1250),USE(?CurrentStatus:3),TRN,FONT(,7,,FONT:bold)
                           STRING('Total O/S'),AT(7813,1250),USE(?CurrentStatus:8),TRN,FONT(,7,,FONT:bold)
                           STRING('In Status'),AT(7292,1354),USE(?CurrentStatus:7),TRN,FONT(,7,,FONT:bold)
                           STRING('Job Location'),AT(8333,1354),USE(?CurrentStatus:4),TRN,FONT(,7,,FONT:bold)
                           STRING('Loan'),AT(9990,1354),USE(?CurrentStatus:5),TRN,FONT(,7,,FONT:bold)
                           STRING('Eng'),AT(10354,1354),USE(?CurrentStatus:6),TRN,FONT(,7,,FONT:bold)
                           STRING('Type'),AT(9698,1354),USE(?CurrentStatus:10),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                           STRING('Days'),AT(7865,1354),USE(?CurrentStatus:9),TRN,FONT(,7,,FONT:bold)
                           STRING('Status'),AT(5677,1344),USE(?CurrentStatus:2),TRN,FONT(,7,,FONT:bold)
                         END
                         GROUP,AT(63,1198,10729,365),USE(?StatusTitle),BOXED
                           STRING('Booked'),AT(135,1354),USE(?String23:2),TRN,FONT(,7,,FONT:bold)
                           STRING('Job No'),AT(625,1354),USE(?String20:2),TRN,FONT(,7,,FONT:bold)
                           STRING('Model No'),AT(1677,1344),USE(?String25:2),TRN,FONT(,7,,FONT:bold)
                           STRING('Location'),AT(2427,1344),USE(?Location),TRN,FONT(,7,,FONT:bold)
                           STRING('Previous Status'),AT(6573,1354),USE(?String31:4),TRN,FONT(,7,,FONT:bold)
                           STRING('Changed'),AT(8292,1354),USE(?String31:3),TRN,FONT(,7,,FONT:bold)
                           STRING('Current Status'),AT(9229,1354),USE(?CurrentStatus),TRN,FONT(,7,,FONT:bold)
                           STRING('Account No.'),AT(3490,1344),USE(?Location:3),TRN,FONT(,7,,FONT:bold)
                           STRING('I.M.E.I. No.'),AT(4438,1354),USE(?Location:2),TRN,FONT(,7,,FONT:bold)
                           STRING('User'),AT(8917,1354),USE(?String31:6),TRN,FONT(,7,,FONT:bold)
                           STRING('Eng'),AT(6104,1354),USE(?String31:2),TRN,FONT(,7,,FONT:bold)
                         END
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
SpinCnt1                BYTE
SpinCnt2                BYTE
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('StatusReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','StatusReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  Access:AUDSTATS.UseFile
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Spinner:Ctl)
  HIDE(?Progress:PctText)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = Records(Jobs)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !(func:DateRangeType,func:StartDate,func:EndDate,func:JobType,func:InvoiceType,func:DespatchedType,func:CompletedType,func:StatusType,func:ReportOrder,func:StatusReportType,func:JobBatchNumber,func:EDIBatchNumber,func:CustomerStatus)
        !=====================================================================================================================               =======================
        
            save_job_id = access:jobs.savefile()
            Case func:ReportOrder
                Of 'JOB NUMBER'
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    set(job:ref_number_key,0)
                Of 'I.M.E.I. NUMBER'
                    Access:JOBS.Clearkey(job:ESN_Key)
                    set(job:esn_key,0)
                Of 'ACCOUNT NUMBER'
                    Access:JOBS.Clearkey(job:AccountNumberKey)
                    set(job:accountnumberkey,0)
                Of 'MODEL NUMBER'
                    Access:JOBS.Clearkey(job:Model_Number_Key)
                    set(job:model_number_key,0)
                Of 'STATUS'
                    Case func:StatusType
                        Of 0 !Job Status
                            Access:JOBS.Clearkey(job:By_Status)
                            set(job:by_status,0)
                        Of 1 !Exchange Status
                            Access:JOBS.Clearkey(job:ExcStatusKey)
                            Set(job:ExcStatusKey)
                        Of 2 !Loan Status
                            Access:JOBS.Clearkey(job:LoanStatusKey)
                            Set(job:LoanStatusKey)
                    End !Case func:StatusType
                Of 'MSN'
                    Access:JOBS.Clearkey(job:MSN_Key)
                    set(job:msn_key,0)
                Of 'SURNAME'
                    Access:JOBS.Clearkey(job:Surname_Key)
                    set(job:surname_key,0)
                Of 'ENGINEER'
                    Access:JOBS.Clearkey(job:Engineer_Key)
                    set(job:engineer_key,0)
                Of 'MOBILE NUMBER'
                    Access:JOBS.Clearkey(job:MobileNumberKey)
                    set(job:mobilenumberkey,0)
                Of 'DATE BOOKED'
                    Access:JOBS.Clearkey(job:Date_Booked_Key)
                    Case func:DateRangeType
                        Of 0 !Booking
                            job:date_booked = func:StartDate
                            set(job:date_booked_key,job:date_booked_key)
                        Of 1 !Completed
                            Set(job:Date_Booked_Key,0)
                    End !tmp:DateRangeType
                Of 'DATE COMPLETED'
                    Access:JOBS.ClearKey(job:DateCompletedKey)
                    Case func:DateRangeType
                        Of 0 !Booking
                            Set(job:DateCompletedKey,0)
                        Of 1 !Completed
                            job:date_completed = func:StartDate
                            set(job:datecompletedkey,job:datecompletedkey)
                    End !Case tmp:DateRangeType
            End !Case tmp:ReportOrder
        
            !main body starts here
            prog.ProgressSetup(RecordsToProcess)
        
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
        
                Yield()
                !RecordsProcessed += 1
                !Do DisplayProgress
                IF (prog.InsideLoop())
                    BREAK
                END ! IF
        
        
                Case func:ReportOrder
                    Of 'DATE BOOKED'
                        If func:DateRangeType = 0
                            If job:Date_Booked > func:EndDate
                                Break
                            End !If job:Date_Booked > tmp:EndDate
                        End !If tmp:DateRangeType = 0
                    Of 'DATE COMPLETED'
                        If func:DateRangeType = 1
                            If job:Date_Completed > func:EndDate
                                Break
                            End !If job:Date_Completed > tmp:EndDate
                        End !If tmp:DateRangeType = 1
                End !Case tmp:ReportOrder
        
                If StatusReportValidation(func:DateRangeType,func:StartDate,func:EndDate,func:JobType,func:InvoiceType,func:DespatchedType,func:CompletedType,func:StatusType,func:JobBatchNumber,func:EDIBatchNumber)
                    Cycle
                End !If StatusReportValidation(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType)
        
                count_temp += 1
                tmp:RecordsCount += 1
                If job:surname  = ''
                    access:subtracc.clearkey(sub:account_number_key)
                    sub:account_number = job:account_number
                    if access:subtracc.fetch(sub:account_number_key) = level:benign
                        access:tradeacc.clearkey(tra:account_number_key)
                        tra:account_number = sub:main_account_number
                        if access:tradeacc.fetch(tra:account_number_key) = level:benign
                            if tra:use_sub_accounts = 'YES'
                                tmp:custname    = sub:company_name
                            else!if tra:use_sub_accounts = 'YES'
                                tmp:custname    = tra:company_name
                            end!if tra:use_sub_accounts = 'YES'
                        end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                    end!if access:subtracc.fetch(sub:account_number_key) = level:benign
                Else!If job:surname  = ''
                    tmp:custname    = job:surname
                End!If job:surname = ''
        
                sat# = 0
                sun# = 0
        
                tmp:Job_Number = job:Ref_Number
        
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = wob:HeadAccountNumber
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Found
                        tmp:Job_Number = job:Ref_Number & '-' & tra:BranchIdentification & wob:jobnumber
                        If tra:IncludeSaturday = 'YES'
                            sat# = 1
                        End !If tra:IncludeSaturday = 'YES'
                        If tra:IncludeSunday = 'YES'
                            sun# = 1
                        End !If tra:IncludeSunday = 'YES'
                    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Error
                    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
                tmp:OSDays          = Local.GetDays(Sat#,Sun#,job:Date_Booked)
                !message('Job No: '&clip(job:Ref_Number)&' func:StatusType='&clip(func:StatusType)&' from Date booked ='&format(job:Date_Booked,@d06)&' get days ='&clip(tmp:OSDays))
        
                tmp:OldStatus   = ''
                tmp:StatusDate  = ''
                tmp:StatusUser = ''
                tmp:Status  = ''
                tmp:DaysInStatus = 0
                
                !copy status type into a local variable so I can mess about with it
                tmp:StatusNo = func:StatusType !this copes with changing from job to exchange status if "Customer Status" requested
        
                Save_aus_ID = Access:AUDSTATS.SaveFile()
                Access:AUDSTATS.ClearKey(aus:DateChangedKey)
                aus:RefNumber   = job:Ref_Number
                If func:CustomerStatus And func:StatusReportType = 0
                    !message('In first option: Cust status and report type =0')
                    If job:Exchange_Unit_Number <> ''
                        tmp:StatusNo = 1
                    End !If job:Exchange_Unit_Number <> ''
               END  !If func:CustomerStatus And func:StatusReportType = 0
        
                Case tmp:StatusNo
                    Of 0
                        aus:Type = 'JOB'
                        tmp:Status  = job:Current_Status
                        tmp:StatusType  = 'J'
                        !TB6634 finally being implemented in printed report
                        !TB13210 - uses WEBJOB field
                        if wob:Current_Status_Date = 0 then
                            tmp:DaysInStatus    = 0
                        else
                            tmp:DaysInStatus    = Local.GetDays(Sat#,Sun#,wob:Current_Status_Date)
                                  !compare with   Local.GetDays(Sat#,Sun#,job:Date_Booked)
                            !message('Job No: '&clip(job:Ref_Number)&' from Current Status ='&format(wob:Current_Status_Date,@d06)&' get days ='&clip(tmp:DaysInStatus))
                        END
        
                    Of 1
                        aus:Type = 'EXC'
                        tmp:Status  = job:Exchange_Status
                        tmp:StatusType  = 'E'
                        !was tmp:DaysInStatus    = Local.GetDays(Sat#,Sun#,aus:DateChanged)
                        !TB13210 - uses WEBJOB field
                        !TB6634 finally being implemented in printed report
                        if wob:Exchange_Status_Date = 0 then
                            tmp:DaysInStatus    = 0
                        else
                            tmp:DaysInStatus    = Local.GetDays(Sat#,Sun#,wob:Exchange_Status_Date)
                            !message('Job No: '&clip(job:Ref_Number)&' from Exchange Status ='&format(wob:Exchange_Status_Date,@d06)&' get days ='&clip(tmp:OSDays))
                        end
        
                    Of 2
                        aus:Type = 'LOA'
                        tmp:Status      = job:Loan_Status
                        tmp:StatusType  = 'L'
                        !TB13210 was tmp:DaysInStatus    = Local.GetDays(Sat#,Sun#,aus:DateChanged)
                        !TB13210 - uses WEBJOB field
                        if wob:Loan_Status_Date = 0 then
                            tmp:DaysInStatus    = 0
                        else
                            tmp:DaysInStatus    = Local.GetDays(Sat#,Sun#,wob:Loan_Status_Date)
                        end
                End !Case func:StatusType
        
                
        
                aus:DateChanged = Today()
                Set(aus:DateChangedKey,aus:DateChangedKey)
                Loop
                    If Access:AUDSTATS.PREVIOUS()
                       !message('Breaking on PREVIOUS()')
                       Break
                    End !If
                    If aus:RefNumber   <> job:Ref_Number then
                        !message('BREAK on not job:refnumber')
                        break
                    end
                    if aus:DateChanged > Today() then
                        !message('break after today' )
                        Break
                    End !If
        
                    Case tmp:StatusNo       !was func:StatusType
                        Of 0
                            If aus:Type <> 'JOB'
                                !MESSAGE('BREAKING on not JOB')
                                Break
                            End !If aus:Type <> 'JOB'
                            !message('Found as JOB')
                            If aus:newstatus = job:Current_Status
                                tmp:OldStatus   = aus:OldStatus
                                tmp:StatusDate  = aus:DateChanged
                                tmp:StatusUser = aus:UserCode
        
                                Date"   = aus:DateChanged
                                Break
                            End!If aud:newstatus = job:Current_Status
        
                        Of 1
                            If aus:Type <> 'EXC'
                                !message('BREAKING on not exchange')
                                Break
                            End !If aus:Type <> 'EXC'
                            If aus:newstatus = job:Exchange_Status
                                tmp:OldStatus   = aus:OldStatus
                                tmp:StatusDate  = aus:DateChanged
                                tmp:StatusUser = aus:UserCode
                                
                                Break
                            End!If aud:newstatus = job:Current_Status
        
                        Of 2
                            If aus:Type <> 'LOA'
                                !message('BREAKING on not Loan')
                                Break
                            End !If aus:Type <> 'LOA'
                            If aus:newstatus = job:Loan_Status
                                tmp:OldStatus   = aus:OldStatus
                                tmp:StatusDate  = aus:DateChanged
                                tmp:StatusUser = aus:UserCode
                                Break
                            End!If aud:newstatus = job:Current_Status
        
                    End !Case func:StatusType
                End !Loop
                Access:AUDSTATS.RestoreFile(Save_aus_ID)
        
                If job:Loan_Unit_Number <> 0
                    tmp:Loaned = 'YES'
                Else !If job:Loan_Unit_Number <> 0
                    tmp:Loaned = 'NO'
                End !If job:Loan_Unit_Number <> 0
        
                if job:Chargeable_Job = 'YES' and job:Warranty_Job = 'YES'
                    tmp:JobType = 'S'
                else
                    if job:Chargeable_Job = 'YES'
                        tmp:JobType = 'C'
                    end
                    if job:Warranty_Job = 'YES'
                        tmp:JobType = 'W'
                    end
                end
        
                Case func:StatusReportType
                    Of 0
                        Print(rpt:detail)
                    Of 2
                        Print(rpt:detail1)
                End!Case func:Status
        
        
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
            prog.ProgressFinish()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','StatusReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDSTATS.Close
    Relate:DEFAULTS.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  SpinCnt1 += 1
  IF SpinCnt1 = 1
    SpinCnt1 = 0
    SpinCnt2 += 1
    IF SpinCnt2 > 4; SpinCnt2 = 1.
    EXECUTE SpinCnt2
      ?Spinner:Ctl{PROP:Text}='/'
      ?Spinner:Ctl{PROP:Text}='---'
      ?Spinner:Ctl{PROP:Text}='\'
      ?Spinner:Ctl{PROP:Text}='|'
    END
    DisplayProgress = True
  END
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
      !*********CHANGE LICENSE ADDRESS*********
      set(defaults)
      access:defaults.next()
      if glo:WebJob then
  
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = Clarionet:Global.Param2
          IF access:tradeacc.fetch(tra:account_number_key)
            !Error!
          END
          address:Name            = tra:Company_Name
          address:AddressLine1    = tra:Address_Line1
          address:AddressLine2    = tra:Address_Line2
          address:AddressLine3    = tra:Address_Line3
          address:Postcode        = tra:Postcode
          address:Telephone       = tra:Telephone_Number
          address:Fax             = tra:Fax_Number
          address:EmailAddress    = tra:EmailAddress
      Else
          address:Name            = def:User_Name
          address:AddressLine1    = def:Address_Line1
          address:AddressLine2    = def:Address_Line2
          address:AddressLine3    = def:Address_Line3
          address:Postcode        = def:Postcode
          address:Telephone       = def:Telephone_Number
          address:Fax             = def:Fax_Number
          address:EmailAddress    = def:EmailAddress
      end
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  Settarget(Report)
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      ?job:Location{prop:Hide} = 1
      ?location{prop:Hide} = 1
!      ?LocationTitle{prop:Hide} = 1
!      ?glo:Select5{prop:Hide} = 1
  End !def:HideLocation
  !I'm keeping the boxes visible in design mode, because the report formatter is crap
  ?MainTitle{prop:Boxed} = 0
  ?StatusTitle{prop:Boxed} = 0
  Case func:StatusReportType
      Of 0
          Hide(?StatusTitle)
          Unhide(?MainTitle)
      Of 2
          Unhide(?StatusTitle)
          Hide(?MainTitle)

  End!Case func:Status
  Case func:StatusType
      Of 1 !Job
          ?CurrentStatus{prop:Text} = 'Job Status'
          ?CurrentStatus:2{prop:Text} = 'Job Status'
      Of 2 !Exchange
          ?CurrentStatus{prop:Text} = 'Exchange Status'
          ?CurrentStatus:2{prop:Text} = 'Exchange Status'
      Of 3 !Loan
          ?CurrentStatus{prop:Text} = 'Loan Status'
          ?CurrentStatus:2{prop:Text} = 'Loan Status'
  End !func:StatusType
  Settarget()

  page_one_temp = 1

  count_temp = 0


    tmp:DateRange   = Format(func:StartDate,@d6) & ' to ' & Format(func:EndDate,@d6)
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Status Report'
  END
  report{Prop:Preview} = PrintPreviewImage


! Before Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()
Local.GetDays       Procedure(Byte sent:Sat,Byte sent:Sun,Date sent:Start)

loc:Count       Long
loc:Days        Long

    Code


    loc:Days = 0
    loc:Count = 0
    Loop

        loc:Count += 1

        If (sent:Start + loc:Count) %7 = 0 And sent:Sun = 0
            Cycle
        End !If (aus:DateChanged + Count#) %7 = 0 And ~sun#

        If (sent:Start + loc:Count) %7 = 6 And sent:Sat = 0
            Cycle
        End !If (aus:DateChanged + Count#) %7 = 0 And ~sun#

        loc:Days += 1

        If sent:Start + loc:Count >= Today()
            Break
        End !If aus:DateChanged + Count# = Today()

    End !Loop

    !TB13210 - Days counted
    !this method of counting may seem slow, but at most this is going to be counting a few days
    !any other method - like calculating the number of weeks and adding bits for weekends is going to be slower

    Return loc:Days 


! After Embed Point: %ProcRoutines) DESC(Procedure Routines) ARG()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: UnivAbcReport
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        ?Prog:CancelButton{prop:Hide} = 1
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 20
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0





