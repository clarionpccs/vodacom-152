

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBD03007.INC'),ONCE        !Local module procedure declarations
                     END


Part_Usage_Export PROCEDURE                           !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::4:TAGFLAG          BYTE(0)
DASBRW::4:TAGMOUSE         BYTE(0)
DASBRW::4:TAGDISPSTATUS    BYTE(0)
DASBRW::4:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGFLAG         BYTE(0)
DASBRW::12:TAGMOUSE        BYTE(0)
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:tag              STRING(1)
tmp:costtotal        REAL
tmp:saletotal        REAL
savepath             STRING(255)
save_ret_id          USHORT,AUTO
save_res_id          USHORT,AUTO
tmp:SiteLocation     STRING(30)
tmp:AnyLocation      BYTE(1)
tmp:tag2             STRING(1)
tmp:startdate        DATE
tmp:enddate          DATE
tmp:retail           BYTE(0)
tmp:service          BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:SiteLocation
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW3::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(STOCK)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Description)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:tag2               LIKE(tmp:tag2)                 !List box control field - type derived from local data
tmp:tag2_Icon          LONG                           !Entry's icon ID
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Primary key field - type derived from field
sto:Location           LIKE(sto:Location)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB8::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
window               WINDOW('Parts Usage Export Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Tab 1'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('This export is used to display the parts used for the selected trade accounts wi' &|
   'thin a selected date range.'),AT(174,124,332,16),USE(?Prompt1),CENTER,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Trade Account'),AT(191,145),USE(?Prompt3),FONT(,,080FFFFH,FONT:bold+FONT:underline,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(191,156,229,170),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@65L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(345,233,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(321,262,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(427,180),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(427,209),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(427,240),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 2'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select A Site Location And Part Number'),AT(267,124),USE(?Prompt4),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Site Location'),AT(188,137),USE(?SiteLocation),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(264,137,124,10),USE(tmp:SiteLocation),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M*@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           LIST,AT(188,152,228,154),USE(?List:2),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@99L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(299,220,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(307,257,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(426,172),USE(?DASTAG:2),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(426,201),USE(?DASTAGAll:2),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(426,232),USE(?DASUNTAGALL:2),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           PROMPT('Select ''Any Location'' if you want to include Parts Numbers from ANY location and' &|
   ' not just the selected location.'),AT(188,310,216,16),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Any Location'),AT(408,314),USE(tmp:AnyLocation),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                         END
                         TAB('Tab 3'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Date Range'),AT(252,132),USE(?Prompt7),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Export Type'),AT(252,212),USE(?Prompt7:2),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Select which parts you wish to appear on the export. Retail/Service or both.'),AT(252,225,200,20),USE(?Prompt11),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Retail Parts'),AT(324,257),USE(tmp:retail),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Service Parts'),AT(324,276),USE(tmp:service),DISABLE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           PROMPT('Start Date'),AT(252,152),USE(?tmp:startdate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(324,152,64,10),USE(tmp:startdate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(392,148),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(392,168),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(252,172),USE(?tmp:enddate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(324,172,64,10),USE(tmp:enddate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Parts Usage Export Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(168,332),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(232,332),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(384,332),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                       BUTTON,AT(452,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

Wizard7         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::4:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW3.UpdateBuffer
   GLO:Queue.Pointer = sub:Account_Number
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = sub:Account_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(GLO:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = sub:Account_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::4:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::4:QUEUE = GLO:Queue
    ADD(DASBRW::4:QUEUE)
  END
  FREE(GLO:Queue)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::4:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::4:QUEUE,DASBRW::4:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = sub:Account_Number
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASSHOWTAG Routine
   CASE DASBRW::4:TAGDISPSTATUS
   OF 0
      DASBRW::4:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::4:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::4:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW9.UpdateBuffer
   GLO:Queue2.Pointer2 = sto:Part_Number
   GET(GLO:Queue2,GLO:Queue2.Pointer2)
  IF ERRORCODE()
     GLO:Queue2.Pointer2 = sto:Part_Number
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    tmp:tag2 = '*'
  ELSE
    DELETE(GLO:Queue2)
    tmp:tag2 = ''
  END
    Queue:Browse:1.tmp:tag2 = tmp:tag2
  IF (tmp:tag2 = '*')
    Queue:Browse:1.tmp:tag2_Icon = 2
  ELSE
    Queue:Browse:1.tmp:tag2_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(GLO:Queue2)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue2.Pointer2 = sto:Part_Number
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue2)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue2)
    GET(GLO:Queue2,QR#)
    DASBRW::12:QUEUE = GLO:Queue2
    ADD(DASBRW::12:QUEUE)
  END
  FREE(GLO:Queue2)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer2 = sto:Part_Number
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer2)
    IF ERRORCODE()
       GLO:Queue2.Pointer2 = sto:Part_Number
       ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020171'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Part_Usage_Export')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Relate:RETSALES.Open
  Access:RETSTOCK.UseFile
  SELF.FilesOpened = True
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  BRW9.Init(?List:2,Queue:Browse:1.ViewPosition,BRW9::View:Browse,Queue:Browse:1,Relate:STOCK,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:startdate = Deformat('1/1/1990',@d6)
  tmp:enddate     = Today()
      ! Save Window Name
   AddToLog('Window','Open','Part_Usage_Export')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard7.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:startdate{Prop:Alrt,255} = MouseLeft2
  ?tmp:enddate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW3.Q &= Queue:Browse
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,sub:Account_Number_Key)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,sub:Account_Number,1,BRW3)
  BIND('tmp:tag',tmp:tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(tmp:tag,BRW3.Q.tmp:tag)
  BRW3.AddField(sub:Account_Number,BRW3.Q.sub:Account_Number)
  BRW3.AddField(sub:Company_Name,BRW3.Q.sub:Company_Name)
  BRW3.AddField(sub:RecordNumber,BRW3.Q.sub:RecordNumber)
  BRW9.Q &= Queue:Browse:1
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,sto:Location_Key)
  BRW9.AddRange(sto:Location,tmp:SiteLocation)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,sto:Part_Number,1,BRW9)
  BIND('tmp:tag2',tmp:tag2)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:tag2,BRW9.Q.tmp:tag2)
  BRW9.AddField(sto:Part_Number,BRW9.Q.sto:Part_Number)
  BRW9.AddField(sto:Description,BRW9.Q.sto:Description)
  BRW9.AddField(sto:Ref_Number,BRW9.Q.sto:Ref_Number)
  BRW9.AddField(sto:Location,BRW9.Q.sto:Location)
  FDCB8.Init(tmp:SiteLocation,?tmp:SiteLocation,Queue:FileDropCombo.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo
  FDCB8.AddSortOrder(loc:Location_Key)
  FDCB8.AddField(loc:Location,FDCB8.Q.loc:Location)
  FDCB8.AddField(loc:RecordNumber,FDCB8.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
    Relate:RETSALES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Part_Usage_Export')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard7.Validate()
        DISABLE(Wizard7.NextControl())
     ELSE
        ENABLE(Wizard7.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:SiteLocation
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, Accepted)
      BRW9.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, Accepted)
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:startdate = TINCALENDARStyle1(tmp:startdate)
          Display(?tmp:startdate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:enddate = TINCALENDARStyle1(tmp:enddate)
          Display(?tmp:enddate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020171'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020171'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020171'&'0')
      ***
    OF ?VSBackButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
      do_next# = 1
      Case Choice(?Sheet1)
          Of 1
              If ~records(glo:queue)
                  Case Missive('You must select at least ONE Trade Account.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  do_next# = 0
              End!If ~records(glo:queue)
          Of 2
              If ~records(glo:queue2)
                  Case Missive('You must select at least ONE Part Number.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  do_next# = 0
              End!If ~records(glo:queue2)
      End!Case Choice(?Sheet1)
      
      If do_next# = 1
      
      
         Wizard7.TakeAccepted()
      End!If do_next# = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      If tmp:retail = 0 And tmp:service = 0
          Case Missive('You must select "Retail Parts" and/or "Service Parts".','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If tmp:retail = 0 And tmp:service = 0
      
          savepath = path()
          set(defaults)
          access:defaults.next()
          If def:exportpath <> ''
              glo:file_name = Clip(def:exportpath) & '\SPAUSE.CSV'
          Else!If def:exportpath <> ''
              glo:file_name = 'C:\SPAUSE.CSV'
          End!If def:exportpath <> ''
      
          if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                      file:keepdir + file:noerror + file:longname)
              !failed
              setpath(savepath)
          else!if not filedialog
              !found
              setpath(savepath)
              Remove(glo:file_name)
              access:expgen.open()
              access:expgen.usefile()
      
              Clear(gen:record)
              gen:line1   = 'Parts Usage Export'
              access:expgen.insert()
      
              Clear(gen:record)
              gen:line1   = 'Part Number,Description,Cost,Sale Cost,Quantity,Total Cost,Account Number,Date,Type'
              access:expgen.insert()
              count# = 0
              tmp:costtotal = 0
              tmp:saletotal = 0
              setcursor(cursor:wait)
              save_ret_id = access:retsales.savefile()
              access:retsales.clearkey(ret:date_booked_key)
              ret:date_booked = tmp:startdate
              set(ret:date_booked_key,ret:date_booked_key)
              loop
                  if access:retsales.next()
                     break
                  end !if
                  if ret:date_booked > tmp:endDate      |
                      then break.  ! end if
                  Sort(glo:queue,glo:pointer)
                  glo:pointer = ret:account_number
                  Get(glo:queue,glo:pointer)
                  If Error()
                      Cycle
                  End!If Error()
      
                  save_res_id = access:retstock.savefile()
                  access:retstock.clearkey(res:part_number_key)
                  res:ref_number  = ret:ref_number
                  set(res:part_number_key,res:part_number_key)
                  loop
                      if access:retstock.next()
                         break
                      end !if
                      if res:ref_number  <> ret:ref_number      |
                          then break.  ! end if
                      If res:Despatched <> 'YES'
                          Cycle
                      End !If res:Despatched <> 'YES'
      
                      Sort(glo:queue2,glo:pointer2)
                      glo:pointer2    = res:part_number
                      Get(glo:queue2,glo:pointer2)
                      If Error()
                          Cycle
                      End!If Error()
      
                      If tmp:anylocation <> 1
                          If res:part_ref_number <> ''
                              access:stock.clearkey(sto:ref_number_key)
                              sto:ref_number   = res:part_ref_number
                              If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                  If sto:location <> tmp:sitelocation
                                      Cycle
                                  End!If sto:location <> tmp:sitelocation
                              End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                          End!If tmp:anylocation <> 1
                      End!If tmp:anylocation <> 1
      
                      Clear(gen:record)
                      gen:line1   = Clip(res:part_number)
                      gen:line1   = CLip(gen:line1) & ',' & CLip(res:description)
                      gen:line1   = Clip(gen:line1) & ',' & Clip(Round(res:purchase_cost,.01))
                      gen:line1   = Clip(gen:line1) & ',' & Clip(Round(res:item_cost,.01))
                      gen:line1   = Clip(gen:line1) & ',' & CLip(res:quantity)
                      gen:line1   = CLip(gen:line1) & ',' & Clip(Round(Round(res:item_cost,.01) * Round(res:quantity,.01),.01))
                      gen:line1   = Clip(gen:line1) & ',' & CLip(ret:account_number)
                      gen:line1   = Clip(gen:line1) & ',' & Clip(Format(ret:date_booked,@d6))
                      gen:line1   = Clip(gen:line1) & ',' & 'Retail'
                      access:expgen.insert()
                      count# += 1
                      tmp:costtotal += Round(Round(res:item_cost,.01) * Round(res:quantity,.01),.01)
                  end !loop
                  access:retstock.restorefile(save_res_id)
      
              end !loop
              access:retsales.restorefile(save_ret_id)
              setcursor()
              If count# <> 0
                  Clear(gen:record)
                  gen:line1   = 'Totals:, ' & Clip(count#) & ',,,,' & Clip(tmp:costtotal)
                  access:expgen.insert()
              End!If count# <> 0
      
              access:expgen.close()
              Case Missive('Export Completed.','ServiceBase 3g',|
                             'midea.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          end!if not filedialog
      End!If tmp:retail = 0 And tmp:service = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tmp:SiteLocation
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tmp:startdate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:enddate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::4:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::12:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      DO DASBRW::12:DASUNTAGALL
      DO DASBRW::4:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW3.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = sub:Account_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = sub:Account_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::4:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = sto:Part_Number
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:tag2 = ''
    ELSE
      tmp:tag2 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag2 = '*')
    SELF.Q.tmp:tag2_Icon = 2
  ELSE
    SELF.Q.tmp:tag2_Icon = 1
  END


BRW9.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = sto:Part_Number
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue


FDCB8.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

Wizard7.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW3.Q &= NULL) ! Has Browse Object been initialized?
       BRW3.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW9.Q &= NULL) ! Has Browse Object been initialized?
       BRW9.ResetQueue(Reset:Queue)
    END

Wizard7.TakeBackEmbed PROCEDURE
   CODE

Wizard7.TakeNextEmbed PROCEDURE
   CODE

Wizard7.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False






RepairPerformanceReport PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:defaulttelephone STRING(20)
tmp:defaultfax       STRING(20)
save_job_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:telephonenumber  STRING(20)
tmp:faxnumber        STRING(20)
tmp:daterange        STRING(30)
tmp:total1           LONG
tmp:total2           LONG
tmp:detail           STRING(25),DIM(4)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:total3           LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Range:'),AT(5000,156),USE(?string27:4),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(tmp:daterange),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
FrontPageTop             DETAIL,AT(,,,2198),USE(?detail1)
                           STRING('REPORT CRITERIA'),AT(3229,104),USE(?String60),TRN,FONT(,9,,FONT:bold)
                           STRING('GENERAL EXCLUSIONS'),AT(365,417),USE(?String60:2),TRN,FONT(,9,,FONT:bold)
                           STRING('REPAIR TYPES EXCLUDED'),AT(365,1875),USE(?String60:3),TRN,FONT(,9,,FONT:bold)
                           STRING('Third Party Repairs:'),AT(469,729),USE(?String62),TRN
                           STRING(@s3),AT(2083,729),USE(GLO:Select20),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s3),AT(2083,938),USE(GLO:Select21),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s3),AT(2083,1146),USE(GLO:Select22),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s3),AT(2083,1354),USE(GLO:Select23),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s3),AT(2083,1563),USE(GLO:Select24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING('Awaiting Parts:'),AT(469,938),USE(?String62:2),TRN
                           STRING('Exchanged:'),AT(469,1146),USE(?String62:3),TRN
                           STRING('Loaned:'),AT(469,1354),USE(?String62:4),TRN
                           STRING('Estimate:'),AT(469,1563),USE(?String62:5),TRN
                         END
detail1                  DETAIL,USE(?unnamed:5)
                           STRING(@s25),AT(469,0),USE(tmp:detail[1]),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(2135,0),USE(tmp:detail[2]),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(3854,0),USE(tmp:detail[3]),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(5573,0),USE(tmp:detail[4]),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         END
TradeTitle               DETAIL,AT(,,,292),USE(?TradeTitle)
                           STRING('TRADE ACCOUNTS INCLUDED'),AT(365,52),USE(?String60:4),TRN,FONT(,9,,FONT:bold)
                         END
ManufactTitle            DETAIL,USE(?ManufactTitle)
                           STRING('MANUFACTURERS INCLUDED'),AT(365,52),USE(?String60:5),TRN,FONT(,9,,FONT:bold)
                         END
TeamsTitle               DETAIL,USE(?TeamsTitle)
                           STRING('TEAMS INCLUDED'),AT(365,52),USE(?String60:6),TRN,FONT(,9,,FONT:bold)
                         END
EngineerTitle            DETAIL,USE(?EngineerTitle)
                           STRING('ENGINEERS INCLUDED'),AT(365,104),USE(?String60:7),TRN,FONT(,9,,FONT:bold)
                         END
FrontPageEnd             DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?FrontPageEnd)
                         END
detail                   DETAIL,AT(,,,7990)
                           STRING('PERIOD'),AT(1823,313),USE(?String35),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING('COMPLETED'),AT(4167,313),USE(?String35:2),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING('COMPLETED'),AT(3344,2958),USE(?String35:4),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING('INCOMPLETE'),AT(4688,2969),USE(?String35:6),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING('SUMMARY ANALYSIS'),AT(1823,5833),USE(?String35:5),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING('EXCLUSIONS'),AT(1823,2969),USE(?String35:3),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING(@s30),AT(4167,573,646,208),USE(GLO:Select1)
                           STRING('Day One:'),AT(1823,833),USE(?String37:2),TRN
                           STRING('Same Day:'),AT(1823,573),USE(?String37),TRN
                           STRING(@s40),AT(4167,833,646,208),USE(GLO:Select2)
                           STRING('Day Two:'),AT(1823,1094),USE(?String37:3),TRN
                           STRING(@s40),AT(4167,1094,646,208),USE(GLO:Select3)
                           STRING('Day Three:'),AT(1823,1354),USE(?String37:4),TRN
                           STRING(@s40),AT(4167,1354,646,208),USE(GLO:Select4)
                           STRING('Day Four:'),AT(1823,1615),USE(?String37:5),TRN
                           STRING('Over 5 Days:'),AT(1823,1875),USE(?String37:6),TRN
                           STRING(@s40),AT(4167,1615,646,208),USE(GLO:Select5)
                           STRING(@s40),AT(4167,1875,646,208),USE(GLO:Select6)
                           LINE,AT(4167,2083,1042,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING(@s8),AT(4167,2135),USE(tmp:total1),LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(4167,6615),USE(tmp:total1,,?tmp:total1:2),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(3333,4740,2135,0),USE(?Line1:2),COLOR(COLOR:Black)
                           STRING(@s8),AT(3333,4844),USE(tmp:total2),LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(4688,4844),USE(tmp:total3),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(4167,6875),USE(tmp:total2,,?tmp:total2:2),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(4167,7135),USE(GLO:Select17),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(5052,6146),USE(GLO:Select16),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(4167,7500),USE(GLO:Select16,,?glo:select16:2),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING('Sent To 3rd Party:'),AT(1823,3229),USE(?String37:7),TRN
                           STRING(@s40),AT(3333,3229,646,208),USE(GLO:Select8)
                           STRING('Awaiting Parts:'),AT(1823,3490),USE(?String37:8),TRN
                           STRING(@s40),AT(3344,3490,646,208),USE(GLO:Select9)
                           STRING(@s40),AT(4688,3750,646,208),USE(GLO:Select27),TRN
                           STRING('Exchanged:'),AT(1823,3750),USE(?String37:9),TRN
                           STRING(@s40),AT(3333,3750,646,208),USE(GLO:Select10)
                           STRING(@s40),AT(4688,4010,646,208),USE(GLO:Select28),TRN
                           STRING(@s40),AT(4688,3229,646,208),USE(GLO:Select25),TRN
                           STRING(@s40),AT(4688,3490,646,208),USE(GLO:Select26),TRN
                           STRING('Loan:'),AT(1823,4010),USE(?String37:10),TRN
                           STRING(@s40),AT(3333,4010,646,208),USE(GLO:Select11)
                           STRING(@s40),AT(4688,4271,646,208),USE(GLO:Select29),TRN
                           STRING('B.E.R.:'),AT(1823,4271),USE(?String37:11),TRN
                           STRING(@s40),AT(3344,4271,646,208),USE(GLO:Select12)
                           STRING(@s40),AT(4688,4531,646,208),USE(GLO:Select30),TRN
                           STRING('Estimated:'),AT(1823,4531),USE(?String37:12),TRN
                           STRING(@s40),AT(3333,4531,646,208),USE(GLO:Select13)
                           STRING('Total Jobs Booked:'),AT(1823,6146),USE(?String37:13),TRN
                           STRING('Completed:'),AT(1823,6615),USE(?String37:14),TRN
                           STRING('Excluded:'),AT(1823,6875),USE(?String37:15),TRN
                           STRING('Incomplete:'),AT(1823,7135),USE(?String37:16),TRN
                           STRING('Total:'),AT(1823,7552),USE(?String37:17),TRN
                           STRING(@s8),AT(5000,7500),USE(GLO:Select16,,?glo:select16:3),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(4167,7396,1563,0),USE(?Line1:3),COLOR(COLOR:Black)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('REPAIR PERFORMANCE REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s15),AT(521,1042),USE(def:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s15),AT(521,1198),USE(def:Fax_Number),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('RepairPerformanceReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','RepairPerformanceReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Print(rpt:FrontPageTop)
        a# = 0
        Clear(tmp:detail)
        Loop x# = 1 To records(glo:queue)
            Get(glo:queue,x#)
            a# += 1
            If a# > 4
                Print(rpt:detail1)
                Clear(tmp:detail)
                a# = 1
            End!If a# > 4
            tmp:detail[a#]  = glo:pointer
        End!Loop x# = 1 To records(glo:queue)
        If Records(glo:queue)
            Print(rpt:detail1)
        End!If a# < 4
        
        Print(rpt:tradetitle)
        a# = 0
        Clear(tmp:detail)
        Loop x# = 1 To records(glo:queue2)
            Get(glo:queue2,x#)
            a# += 1
            If a# > 4
                Print(rpt:detail1)
                Clear(tmp:detail)
                a# = 1
            End!If a# > 4
            tmp:detail[a#]  = glo:pointer2
        End!Loop x# = 1 To records(glo:queue)
        If Records(glo:queue2)
            Print(rpt:detail1)
        End!If a# < 4
        
        Print(rpt:manufacttitle)
        a# = 0
        Clear(tmp:detail)
        Loop x# = 1 To records(glo:queue3)
            Get(glo:queue3,x#)
            a# += 1
            If a# > 4
                Print(rpt:detail1)
                Clear(tmp:detail)
                a# = 1
            End!If a# > 4
            tmp:detail[a#]  = glo:pointer3
        End!Loop x# = 1 To records(glo:queue)
        If Records(glo:queue3)
            Print(rpt:detail1)
        End!If a# < 4
        
        Print(rpt:teamstitle)
        a# = 0
        Clear(tmp:detail)
        Loop x# = 1 To records(glo:queue4)
            Get(glo:queue4,x#)
            a# += 1
            If a# > 4
                Print(rpt:detail1)
                Clear(tmp:detail)
                a# = 1
            End!If a# > 4
            tmp:detail[a#]  = glo:pointer4
        End!Loop x# = 1 To records(glo:queue)
        If Records(glo:queue4)
            Print(rpt:detail1)
        End!If a# < 4
        
        Print(rpt:engineertitle)
        a# = 0
        Clear(tmp:detail)
        Loop x# = 1 To records(glo:queue5)
            Get(glo:queue5,x#)
            a# += 1
            If a# > 4
                Print(rpt:detail1)
                Clear(tmp:detail)
                a# = 1
            End!If a# > 4
            access:users.clearkey(use:user_code_key)
            use:user_code = glo:pointer5
            if access:users.tryfetch(use:user_code_key) = Level:Benign
                tmp:detail[a#]  = Clip(glo:pointer5) & ' - ' & Clip(use:surname) & ', ' & Clip(use:forename)
            End!if access:users.tryfetch(use:user_code_key) = Level:Benign
        End!Loop x# = 1 To records(glo:queue)
        If Records(glo:queue5)
            Print(rpt:detail1)
        End!If a# < 4
        
        
        Print(rpt:FrontPageEnd)
        Print(rpt:detail)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','RepairPerformanceReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBS.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  tmp:daterange   = Format(glo:select14,@d6) & ' - ' & Format(glo:select15,@d6)
  tmp:total1  = glo:select1 + glo:select2 + glo:select3 + glo:select4 + glo:select5 + glo:select6
  tmp:total2  = glo:select8 + glo:select9 + glo:select10 + glo:select11 + glo:select12 + glo:select13
  tmp:total3  = glo:select25 + glo:Select26 + glo:select27 + glo:select28 + glo:select29 + glo:select30
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='RepairPerformanceReport'
  END
  report{Prop:Preview} = PrintPreviewImage







CompletedJobStatisticsExportCriteria PROCEDURE        !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
FilesOpened          BYTE
save_tra_id          USHORT,AUTO
save_sub_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select4
tratmp:Account_Number  LIKE(tratmp:Account_Number)    !List box control field - type derived from field
tratmp:Company_Name    LIKE(tratmp:Company_Name)      !List box control field - type derived from field
tratmp:Type            LIKE(tratmp:Type)              !List box control field - type derived from field
tratmp:RefNumber       LIKE(tratmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(TRADETMP)
                       PROJECT(tratmp:Account_Number)
                       PROJECT(tratmp:Company_Name)
                       PROJECT(tratmp:Type)
                       PROJECT(tratmp:RefNumber)
                     END
window               WINDOW('Completed Jobs Statistics Export'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Completed Jobs Statistics Export'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Date Range'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Start Date'),AT(240,162),USE(?glo:select1:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(316,162,64,10),USE(GLO:Select1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(AltD),CAP
                           BUTTON,AT(384,158),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(240,184),USE(?glo:select2:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(316,184,64,10),USE(GLO:Select2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(AltD),UPR
                           BUTTON,AT(384,180),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                           CHECK('Select Trade Account'),AT(316,206),USE(GLO:Select3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           PROMPT('Trade Account'),AT(240,222),USE(?Prompt3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s40),AT(316,222,124,10),USE(GLO:Select4),IMM,DISABLE,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('66L(2)|M@s15@129L(2)|M@s30@12L(2)|M@s3@'),DROP(10,200),FROM(Queue:FileDropCombo)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020158'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CompletedJobStatisticsExportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SUBTRACC.Open
  Relate:TRADETMP.Open
  SELF.FilesOpened = True
    recordspercycle     = 25
    recordsprocessed    = 0
    percentprogress     = 0
    setcursor(cursor:wait)
    open(progresswindow)
    progress:thermometer    = 0
    ?progress:pcttext{prop:text} = '0% Completed'
  
    recordstoprocess    = Records(tradeacc)
  
  
    save_tra_id = access:tradeacc.savefile()
    set(tra:account_number_key)
    loop
        if access:tradeacc.next()
           break
        end !if
        Do GetNextRecord2
        If tra:invoice_sub_accounts <> 'YES'
            get(tradetmp,0)
            if access:tradetmp.primerecord() = Level:Benign
                tratmp:account_number = tra:account_number
                tratmp:company_name   = tra:company_name
                tratmp:type           = 'MAI'
                if access:tradetmp.insert()
                   access:tradetmp.cancelautoinc()
                end
            end!if access:tradetmp.primerecord() = Level:Benign
        Else!If tra:invoice_sub_accounts = 'YES'
            save_sub_id = access:subtracc.savefile()
            access:subtracc.clearkey(sub:main_account_key)
            sub:main_account_number = tra:account_number
            set(sub:main_account_key,sub:main_account_key)
            loop
                if access:subtracc.next()
                   break
                end !if
                if sub:main_account_number <> tra:account_number      |
                    then break.  ! end if
                get(tradetmp,0)
                if access:tradetmp.primerecord() = Level:Benign
                    tratmp:account_number = sub:account_number
                    tratmp:company_name   = sub:company_name
                    tratmp:type           = 'SUB'
                    if access:tradetmp.insert()
                       access:tradetmp.cancelautoinc()
                    end
                end!if access:tradetmp.primerecord() = Level:Benign
            end !loop
            access:subtracc.restorefile(save_sub_id)
        End!If tra:invoice_sub_accounts = 'YES'
    end !loop
    access:tradeacc.restorefile(save_tra_id)
  
    setcursor()
    close(progresswindow)
  
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  glo:select1 = Deformat('1/1/1990',@d6b)
  glo:select2 = Today()
  glo:select3 = 0
  glo:select4 = ''
  Display()
      ! Save Window Name
   AddToLog('Window','Open','CompletedJobStatisticsExportCriteria')
  Bryan.CompFieldColour()
  ?GLO:Select1{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select2{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?GLO:Select3{Prop:Checked} = True
    ENABLE(?GLO:Select4)
  END
  IF ?GLO:Select3{Prop:Checked} = False
    DISABLE(?GLO:Select4)
  END
  FDCB6.Init(GLO:Select4,?GLO:Select4,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:TRADETMP,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(tratmp:AccountNoKey)
  FDCB6.AddField(tratmp:Account_Number,FDCB6.Q.tratmp:Account_Number)
  FDCB6.AddField(tratmp:Company_Name,FDCB6.Q.tratmp:Company_Name)
  FDCB6.AddField(tratmp:Type,FDCB6.Q.tratmp:Type)
  FDCB6.AddField(tratmp:RefNumber,FDCB6.Q.tratmp:RefNumber)
  FDCB6.AddUpdateField(tratmp:Account_Number,GLO:Select4)
  FDCB6.AddUpdateField(tratmp:Type,GLO:Select5)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SUBTRACC.Close
    Relate:TRADETMP.Close
  Remove(glo:tradetmp)
  If Error()
      access:tradetmp.close()
      Remove(glo:tradetmp)
  End!If Error()
  END
  ! Save Window Name
   AddToLog('Window','Close','CompletedJobStatisticsExportCriteria')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020158'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020158'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020158'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?GLO:Select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?GLO:Select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?GLO:Select3
      IF ?GLO:Select3{Prop:Checked} = True
        ENABLE(?GLO:Select4)
      END
      IF ?GLO:Select3{Prop:Checked} = False
        DISABLE(?GLO:Select4)
      END
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      Completed_Job_Stats_Export
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      glo:select4 = ''
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?GLO:Select1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?GLO:Select1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, AlertKey)
      glo:select1 = Deformat('1/1/1990',@d6b)
      Display(?glo:select1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, AlertKey)
    END
  OF ?GLO:Select2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select2, AlertKey)
      glo:select2 = Today()
      Display(?glo:select2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select2, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
SelectMainSubAccount PROCEDURE                        !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
save_sub_id          USHORT,AUTO
save_tra_id          USHORT,AUTO
LocalRequest         LONG
FilesOpened          BYTE
main_account_temp    STRING(15)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(TRADETMP)
                       PROJECT(tratmp:Account_Number)
                       PROJECT(tratmp:Company_Name)
                       PROJECT(tratmp:Type)
                       PROJECT(tratmp:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tratmp:Account_Number  LIKE(tratmp:Account_Number)    !List box control field - type derived from field
tratmp:Company_Name    LIKE(tratmp:Company_Name)      !List box control field - type derived from field
tratmp:Type            LIKE(tratmp:Type)              !List box control field - type derived from field
tratmp:RefNumber       LIKE(tratmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Trade Account File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(168,110,276,214),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(InsertKey),FORMAT('66L(2)|M~Account Number~@s15@128L(2)|M~Company Name~@s30@12L(2)|M~Type~@s3@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,164),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Account Number'),USE(?Tab:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(168,98,64,10),USE(tratmp:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Company Name'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,124,10),USE(tratmp:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Trade Account File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020155'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectMainSubAccount')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:TRADEACC.Open
  Relate:TRADETMP.Open
  Relate:USELEVEL.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TRADETMP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','SelectMainSubAccount')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,tratmp:CompanyNameKey)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?tratmp:Company_Name,tratmp:Company_Name,1,BRW1)
  BRW1.AddSortOrder(,tratmp:AccountNoKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?tratmp:Account_Number,tratmp:Account_Number,1,BRW1)
  BRW1.AddField(tratmp:Account_Number,BRW1.Q.tratmp:Account_Number)
  BRW1.AddField(tratmp:Company_Name,BRW1.Q.tratmp:Company_Name)
  BRW1.AddField(tratmp:Type,BRW1.Q.tratmp:Type)
  BRW1.AddField(tratmp:RefNumber,BRW1.Q.tratmp:RefNumber)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:4{PROP:TEXT} = 'By Account Number'
    ?Tab3{PROP:TEXT} = 'By Company Name'
    ?Browse:1{PROP:FORMAT} ='66L(2)|M~Account Number~@s15@#1#128L(2)|M~Company Name~@s30@#2#12L(2)|M~Type~@s3@#3#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRADEACC.Close
    Relate:TRADETMP.Close
    Relate:USELEVEL.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','SelectMainSubAccount')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020155'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020155'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020155'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='66L(2)|M~Account Number~@s15@#1#128L(2)|M~Company Name~@s30@#2#12L(2)|M~Type~@s3@#3#'
          ?Tab:4{PROP:TEXT} = 'By Account Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='128L(2)|M~Company Name~@s30@#2#66L(2)|M~Account Number~@s15@#1#12L(2)|M~Type~@s3@#3#'
          ?Tab3{PROP:TEXT} = 'By Company Name'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?tratmp:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Account_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Account_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?tratmp:Company_Name
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Company_Name, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Company_Name, Selected)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

PriceListExport PROCEDURE                             !Generated from procedure template - Process

Progress:Thermometer BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
SheetDesc            CSTRING(41)
Process:View         VIEW(STOCK)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,144,60),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(16,16,112,12),RANGE(0,100)
                       STRING(''),AT(0,4,144,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,144,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(44,42,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                  !Progress Manager
LastPctValue        LONG(0)                           !---ClarioNET 20
ClarioNET:PW:UserString   STRING(40)
ClarioNET:PW:PctText      STRING(40)
ClarioNET:PW WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
               PROGRESS,USE(Progress:Thermometer,,?ClarioNET:Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
               STRING(''),AT(0,3,141,10),USE(ClarioNET:PW:UserString),CENTER
               STRING(''),AT(0,30,141,10),USE(ClarioNET:PW:PctText),CENTER
             END
?F1SS                EQUATE(1000)
!# SW2 Extension            STRING(3)
f1Action             BYTE
f1FileName           CSTRING(256)
RowNumber            USHORT
StartRow             LONG
StartCol             LONG
Template             CSTRING(129)
f1Disabled           BYTE(0)
ssFieldQ              QUEUE(tqField).
CQ                    QUEUE(tqField).
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!---------------------
ssInit         ROUTINE
!---------------------
  DO ssBuildQueue                                     ! Initialize f1 field queue
  IF NOT UsBrowse(SsFieldQ,'PriceListExport',,,,CQ,SheetDesc,StartRow,StartCol,Template)
    ThisWindow.Kill
    RETURN
  END

!---------------------
ssBuildQueue   ROUTINE
!---------------------
  ssFieldQ.Desc = 'STOCK' & ':' & 'Accessory Cost'
  ssFieldQ.Name = 'sto:AccessoryCost'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Quantity Requested'
  ssFieldQ.Name = 'sto:QuantityRequested'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Suspend Part'
  ssFieldQ.Name = 'sto:Suspend'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'E1'
  ssFieldQ.Name = 'sto:E1'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'E2'
  ssFieldQ.Name = 'sto:E2'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'E3'
  ssFieldQ.Name = 'sto:E3'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Return Faulty Spare'
  ssFieldQ.Name = 'sto:ReturnFaultySpare'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Chargeable Part Only'
  ssFieldQ.Name = 'sto:ChargeablePartOnly'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Attach By Solder'
  ssFieldQ.Name = 'sto:AttachBySolder'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Allow Duplicate Part On Jobs'
  ssFieldQ.Name = 'sto:AllowDuplicate'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Percentage Mark Up'
  ssFieldQ.Name = 'sto:RetailMarkup'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Entry Real'
  ssFieldQ.Name = 'sto:VirtPurchaseCost'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Entry Real'
  ssFieldQ.Name = 'sto:VirtTradeCost'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Entry Real'
  ssFieldQ.Name = 'sto:VirtRetailCost'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Percent Mark Up'
  ssFieldQ.Name = 'sto:VirtTradeMarkup'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Percentage Mark Up'
  ssFieldQ.Name = 'sto:VirtRetailMarkup'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Average Purchase Cost'
  ssFieldQ.Name = 'sto:AveragePurchaseCost'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Percentage Mark Up'
  ssFieldQ.Name = 'sto:PurchaseMarkUp'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Repair Index'
  ssFieldQ.Name = 'sto:RepairLevel'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Skill Level'
  ssFieldQ.Name = 'sto:SkillLevel'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Date Booked'
  ssFieldQ.Name = 'FORMAT(sto:DateBooked,@d6)'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Accessory Warranty Period'
  ssFieldQ.Name = 'sto:AccWarrantyPeriod'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Exclude From Level 1 && 2 Repairs'
  ssFieldQ.Name = 'sto:ExcludeLevel12Repair'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Exchange Order Cap'
  ssFieldQ.Name = 'sto:ExchangeOrderCap'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Exchange Model Number'
  ssFieldQ.Name = 'sto:ExchangeModelNumber'
  ssFieldQ.IsString = TRUE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Loan Unit'
  ssFieldQ.Name = 'sto:LoanUnit'
  ssFieldQ.IsString = FALSE
  ADD(ssFieldQ)
  ssFieldQ.Desc = 'STOCK' & ':' & 'Loan Model Number'
  ssFieldQ.Name = 'sto:LoanModelNumber'
  ssFieldQ.IsString = TRUE
  ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Sundry_Item'
    ssFieldQ.Desc = 'Sundry Item'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Part_Number'
    ssFieldQ.Desc = 'Part Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Description'
    ssFieldQ.Desc = 'Description'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Supplier'
    ssFieldQ.Desc = 'Supplier'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Purchase_Cost'
    ssFieldQ.Desc = 'Trade Cost'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Sale_Cost'
    ssFieldQ.Desc = 'Sale Cost'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Retail_Cost'
    ssFieldQ.Desc = 'Retail Cost'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Location'
    ssFieldQ.Desc = 'Stock Location'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Shelf_Location'
    ssFieldQ.Desc = 'Shelf Location'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Second_Location'
    ssFieldQ.Desc = 'Second Location'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Quantity_Stock'
    ssFieldQ.Desc = 'Quantity In Stock'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Manufacturer'
    ssFieldQ.Desc = 'Manufacturer'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Minimum_Level'
    ssFieldQ.Desc = 'Minimum Level'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Reorder_Level'
    ssFieldQ.Desc = 'Reorder Level'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sto:Accessory'
    ssFieldQ.Desc = 'Accessory'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
  SORT(ssFieldQ,+ssFieldQ.Desc)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  LastPctValue = 0                                    !---ClarioNET 37
  ClarioNET:PW:PctText = ?Progress:PctText{Prop:Text}
  ClarioNET:PW:UserString{Prop:Text} = ?Progress:UserString{Prop:Text}
  ClarioNET:OpenPushWindow(ClarioNET:PW)
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('PriceListExport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  DO ssInit
  Relate:STOCK.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','PriceListExport')
  Bryan.CompFieldColour()
  IF ThisWindow.Response <> RequestCancelled
    UsInitDoc(CQ,?F1ss,StartRow,StartCol,Template)
    ?F1SS{'Sheets("Sheet1").Name'} = SheetDesc
    IF Template = ''
      RowNumber = StartRow + 1
    ELSE
      RowNumber = StartRow
    END
  END
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisProcess.Init(Process:View, Relate:STOCK, ?Progress:PctText, Progress:Thermometer, ProgressMgr, sto:Location)
  ThisProcess.CaseSensitiveValue = FALSE
  ThisProcess.AddSortOrder(sto:Location_Key)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(STOCK,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','PriceListExport')
  Bryan.CompFieldColour()


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','PriceListExport')
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Progress:Cancel
                                                      ! SW2 This section changed
      IF Message('Would you like to view the partially created spreadsheet?','Process Cancelled',ICON:Question,Button:Yes+Button:No,Button:No)|
      = Button:Yes
        UsDeInit(f1FileName, ?F1SS, 1)
      END
      ReturnValue = Level:Fatal
      RETURN ReturnValue
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ThisWindow.Response = RequestCompleted
    f1FileName = ''
    UsDeInit(f1FileName, ?F1SS, 1)
  
  ELSE
    IF RowNumber THEN ?F1SS{'quit()'}.
  END
  ReturnValue = PARENT.TakeCloseEvent()
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:ClosePushWindow(ClarioNET:PW)         !---ClarioNET 88
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF LastPctValue <> Progress:Thermometer             !---ClarioNET 99
    IF INLIST(Progress:Thermometer,'5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95')
      LastPctValue = Progress:Thermometer
      ClarioNET:UpdatePushWindow(ClarioNET:PW)
    END
  END
  ReturnValue = PARENT.TakeRecord()
  UsFillRow(CQ,?F1SS,RowNumber,StartCol)
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
RepairExchangeReportCriteria PROCEDURE                !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGFLAG         BYTE(0)
DASBRW::12:TAGMOUSE        BYTE(0)
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:tag              STRING(1)
tmp:Manufacturer     STRING(30)
tmp:tag2             STRING(1)
tmp:StartDate        DATE
tmp:EndDate          DATE
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:tag2               LIKE(tmp:tag2)                 !List box control field - type derived from local data
tmp:tag2_Icon          LONG                           !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB10::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
window               WINDOW('Repair/Exchange Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),BELOW,COLOR(0D6E7EFH),SPREAD
                         TAB('Tab 1'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select the Trade Account(s) you wish to include'),AT(256,112),USE(?SelectTradeAccount),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(168,126,272,190),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@60L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(294,202,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(298,222,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(448,156),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(448,190),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(448,222),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 2'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select the Model Numbers you wish to include'),AT(256,112),USE(?Prompt3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Manufacturer'),AT(168,126),USE(?Manufacturer),FONT(,7,,FONT:bold),COLOR(09A6A7CH)
                           COMBO(@s30),AT(168,134,124,10),USE(tmp:Manufacturer),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           LIST,AT(168,146,272,170),USE(?List:2),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Model Number~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(318,204,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(322,228,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(448,170),USE(?DASTAG:2),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(448,202),USE(?DASTAGAll:2),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(448,234),USE(?DASUNTAGALL:2),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 3'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select the (Booking) Date Range for the report.'),AT(251,137),USE(?Prompt5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Start Date'),AT(265,184),USE(?tmp:StartDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(322,184,64,10),USE(tmp:StartDate),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(389,180),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(265,210),USE(?tmp:EndDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(322,210,64,10),USE(tmp:EndDate),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(389,206),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Repair / Exchange Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(168,332),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(236,332),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

Wizard7         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = sub:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::5:QUEUE = glo:Queue
    ADD(DASBRW::5:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sub:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW9.UpdateBuffer
   glo:Queue2.Pointer2 = mod:Model_Number
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = mod:Model_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:tag2 = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:tag2 = ''
  END
    Queue:Browse:1.tmp:tag2 = tmp:tag2
  IF (tmp:tag2 = '*')
    Queue:Browse:1.tmp:tag2_Icon = 2
  ELSE
    Queue:Browse:1.tmp:tag2_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = mod:Model_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::12:QUEUE = glo:Queue2
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue2)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer2 = mod:Model_Number
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = mod:Model_Number
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020172'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('RepairExchangeReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SelectTradeAccount
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  tmp:StartDate   = Deformat('1/1/1990',@d6)
  tmp:EndDate     = Today()
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  BRW9.Init(?List:2,Queue:Browse:1.ViewPosition,BRW9::View:Browse,Queue:Browse:1,Relate:MODELNUM,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','RepairExchangeReportCriteria')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard7.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,sub:Account_Number_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,sub:Account_Number,1,BRW4)
  BIND('tmp:tag',tmp:tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:tag,BRW4.Q.tmp:tag)
  BRW4.AddField(sub:Account_Number,BRW4.Q.sub:Account_Number)
  BRW4.AddField(sub:Company_Name,BRW4.Q.sub:Company_Name)
  BRW4.AddField(sub:RecordNumber,BRW4.Q.sub:RecordNumber)
  BRW9.Q &= Queue:Browse:1
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,mod:Manufacturer_Key)
  BRW9.AddRange(mod:Manufacturer,tmp:Manufacturer)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,mod:Model_Number,1,BRW9)
  BIND('tmp:tag2',tmp:tag2)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:tag2,BRW9.Q.tmp:tag2)
  BRW9.AddField(mod:Model_Number,BRW9.Q.mod:Model_Number)
  BRW9.AddField(mod:Manufacturer,BRW9.Q.mod:Manufacturer)
  FDCB10.Init(tmp:Manufacturer,?tmp:Manufacturer,Queue:FileDropCombo.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo
  FDCB10.AddSortOrder(man:Manufacturer_Key)
  FDCB10.AddField(man:Manufacturer,FDCB10.Q.man:Manufacturer)
  FDCB10.AddField(man:RecordNumber,FDCB10.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:MANUFACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','RepairExchangeReportCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard7.Validate()
        DISABLE(Wizard7.NextControl())
     ELSE
        ENABLE(Wizard7.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      BRW9.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020172'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020172'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020172'&'0')
      ***
    OF ?VSBackButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      RepairExchangeReport(tmp:StartDate,tmp:EndDate)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::12:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      DO DASBRW::12:DASUNTAGALL
      DO DASBRW::5:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = mod:Model_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:tag2 = ''
    ELSE
      tmp:tag2 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag2 = '*')
    SELF.Q.tmp:tag2_Icon = 2
  ELSE
    SELF.Q.tmp:tag2_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = mod:Model_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue

Wizard7.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW4.Q &= NULL) ! Has Browse Object been initialized?
       BRW4.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW9.Q &= NULL) ! Has Browse Object been initialized?
       BRW9.ResetQueue(Reset:Queue)
    END

Wizard7.TakeBackEmbed PROCEDURE
   CODE

Wizard7.TakeNextEmbed PROCEDURE
   CODE

Wizard7.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False






RepairExchangeReport PROCEDURE(func:StartDate,func:EndDate)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
tmp:ModelCountQueue  QUEUE,PRE()
tmp:ModelNumber      STRING(30)
tmp:Repaired         LONG
tmp:Exchanged        LONG
                     END
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:DateRange        STRING(25)
tmp:AccountNumber    STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
save_job_id          USHORT,AUTO
tmp:RepairedTotal    LONG
tmp:ExchangedTotal   LONG
tmp:AccountName      STRING(50)
tmp:GrandRepairedTotal LONG
tmp:GrandExchangedTotal LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Date Range:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s25),AT(5885,156),USE(tmp:DateRange),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,781),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,781),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,781),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,781,375,208),USE(?CPCSPgOfPgStr),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s8),AT(5104,0),USE(tmp:Repaired),TRN,RIGHT
                           STRING(@s8),AT(6354,0),USE(tmp:Exchanged),TRN,RIGHT
                           STRING(@s30),AT(2448,0),USE(mod:Manufacturer),TRN,LEFT
                           STRING(@s30),AT(208,0),USE(tmp:ModelNumber),TRN,LEFT
                         END
TradeTitle               DETAIL,USE(?TradeTitle)
                           STRING('Trade Account: '),AT(208,0),USE(?String29),TRN
                           STRING(@s15),AT(1146,0),USE(sub:Account_Number),TRN,FONT(,,,FONT:bold)
                           LINE,AT(208,208,2031,0),USE(?Line1),COLOR(COLOR:Black)
                         END
TradeTotal               DETAIL,AT(,,,542),USE(?TradeTotal)
                           LINE,AT(5052,52,2031,0),USE(?Line1:2),COLOR(COLOR:Black)
                           STRING(@s8),AT(5104,104),USE(tmp:RepairedTotal),TRN,RIGHT
                           STRING(@s8),AT(6354,104),USE(tmp:ExchangedTotal),TRN,RIGHT
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           STRING(@s8),AT(5104,104),USE(tmp:GrandRepairedTotal),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(6354,104),USE(tmp:GrandExchangedTotal),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING('Total:'),AT(4271,104),USE(?String36),TRN,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('REPAIR/EXCHANGE REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1042),USE(tmp:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1198),USE(tmp:FaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Model Number'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Qty Repaired'),AT(5104,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Qty Exchanged'),AT(6250,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Manufacturer'),AT(2448,2083),USE(?string44:2),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('RepairExchangeReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','RepairExchangeReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:TRANTYPE.Open
  Access:SUBTRACC.UseFile
  Access:MODELNUM.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = Records(glo:Queue)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        tmp:GrandExchangedTotal = 0
        tmp:GrandRepairedTotal  = 0
        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            RecordsProcessed += 1
            Do DisplayProgress
        
            Clear(tmp:ModelCountQueue)
            Free(tmp:ModelCountQUeue)
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:Date_Booked_Key)
            job:Date_Booked = tmp:StartDate
            Set(job:Date_Booked_Key,job:Date_Booked_Key)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Date_Booked > tmp:EndDate      |
                    Then Break.  ! End If
                If job:Account_Number <> glo:Pointer
                    Cycle
                End!If job:Account_Number <> glo:Pointer
        
                !Does this Job's Model Number exists in the second Queue
                glo:Pointer2    = job:Model_Number
                Get(glo:Queue2,glo:Pointer2)
                If ~Error()
                    !If It does, check whether it exists in the temporary table
                    !If it does, add one. If not add a record for that Model Number
                    tmp:ModelNumber = job:Model_Number
                    Get(tmp:ModelCountQueue,tmp:ModelNumber)
                    If Error()
                        tmp:ModelNumber = job:Model_Number
                        If ToBeExchanged()
                            tmp:Exchanged   = 1
                            tmp:Repaired    = 0
                        Else!If ToBeExchanged()
                            tmp:Exchanged   = 0
                            tmp:Repaired    = 1
                        End!If ToBeExchanged()
                        Add(tmp:ModelCountQueue)
                    Else!If Error()
                        If ToBeExchanged()
                            tmp:Exchanged   += 1
                        Else!If ToBeExchanged()
                            tmp:Repaired    += 1
                        End!If ToBeExchanged()
                        Put(tmp:ModelCountQueue)
                    End!If Error()
                End!If ~Error()
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
            If Records(tmp:ModelCountQueue)
                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = glo:Pointer
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found
                    tmp:AccountName = Clip(sub:Account_Number) & ' - ' & sub:Company_Name
                Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        
                Print(rpt:TradeTitle)
                tmp:AccountNumber   = glo:Pointer
        
                tmp:ExchangedTotal  = 0
                tmp:RepairedTotal   = 0
                Loop y# = 1 To Records(tmp:ModelCountQueue)
                    Get(tmp:ModelCountQueue,y#)
                    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                    mod:Model_Number    = tmp:ModelNumber
                    If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                        !Found
        
                    Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        
                    Print(rpt:Detail)
                    tmp:ExchangedTotal += tmp:Exchanged
                    tmp:RepairedTotal  += tmp:Repaired
                End!Loop x# = 1 To Records(tmp:ModelCountQueue)
                tmp:GrandExchangedTotal  += tmp:ExchangedTotal
                tmp:GrandRepairedTotal  += tmp:RepairedTotal
                Print(rpt:TradeTotal)
            End!If Records(tmp:ModelCountQueue)
        End!Loop x# = 1 To Records(glo:Queue)
        
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'c:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','RepairExchangeReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:TRANTYPE.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
  tmp:DateRange = Format(tmp:StartDate,@d6) & ' - ' & Format(tmp:EndDate,@d6)
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='RepairExchangeReport'
  END
  report{Prop:Preview} = PrintPreviewImage













Multiple_Job_Booking_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:Ref_number       LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:job_queue        QUEUE,PRE(jobque)
JobNumber            LONG
                     END
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:telephonenumber  STRING(20)
tmp:faxnumber        STRING(20)
tmp:count            LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:Location)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Warranty_Charge_Type)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,885,7521,1000),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,781),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,781),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,781),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,781,375,208),USE(?CPCSPgOfPgStr),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,177),USE(?detailband)
                           STRING(@s8),AT(208,0),USE(tmp:Ref_number),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s20),AT(833,0),USE(job:Location),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(2188,0),USE(job:Charge_Type),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(3802,0),USE(job:Warranty_Charge_Type),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(5417,0),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(302,52,6927,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Jobs:'),AT(313,104),USE(?String32),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(1667,104),USE(tmp:count),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('MULTIPLE JOB BOOKING REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Job No'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Charge Type'),AT(2188,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Warranty Charge Type'),AT(3802,2083),USE(?string45:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(5417,2083),USE(?string45:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Location'),AT(833,2083),USE(?Location),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Multiple_Job_Booking_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 76
      ! Save Window Name
   AddToLog('Report','Open','Multiple_Job_Booking_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess    = Records(glo:Queue) * 2
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Free(tmp:job_queue)
        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            jobque:jobnumber   = glo:pointer
            Add(tmp:job_queue)
            RecordsProcessed += 1
            Do DisplayProgress
        End!Loop x# = 1 To Records(glo:Queue)
        
        Sort(tmp:job_queue,jobque:jobnumber)
        Loop x# = 1 To Records(tmp:job_queue)
            Get(tmp:job_queue,x#)
            RecordsProcessed += 1
            Do DisplayProgress
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number  = jobque:JobNumber
            If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
        
            tmp:Ref_number = job:Ref_number
            if glo:WebJob then
                !Change the tmp ref number if you
                !can Look up from the wob file
                access:Webjob.clearkey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_number
                if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                    tmp:Ref_number = wob:JobNumber
                END
            END
        
                tmp:count += 1
                Print(rpt:detail)
            End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
        End!Loop x# = 1 To Records(glo:Queue)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Multiple_Job_Booking_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      ?job:Location{prop:Hide} = 1
      ?Location{Prop:Hide} = 1
  End !def:HideLocation
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Multiple_Job_Booking_Report'
  END
  report{Prop:Preview} = PrintPreviewImage







CustomerEnquiryReportCriteria PROCEDURE               !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Tag              STRING(1)
tmp:DespatchDays     LONG
tmp:StartDate        DATE
tmp:EndDate          DATE
BRW4::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Customer Enquiry Report Critiera'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Customer Enquiry Report Criteria'),USE(?Tab1)
                           PROMPT('Select Trade Account'),AT(168,86),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Include job that have been despatched in the following number of days'),AT(364,95,148,16),USE(?Prompt1:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Include jobs that have NOT been despatch during the following period:'),AT(364,158,148,16),USE(?Prompt1:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Start Date'),AT(364,182),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(416,182,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(484,178),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(364,202),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(416,202,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(484,198),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Days'),AT(364,119),USE(?tmp:DespatchDays:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(416,119,64,10),USE(tmp:DespatchDays),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Days'),TIP('Days'),UPR,STEP(1)
                           LIST,AT(168,98,192,196),USE(?List),IMM,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@60L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(264,158,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(244,180,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(168,298),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(296,298),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           BUTTON,AT(232,298),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Customer Enquiry Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = sub:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::5:QUEUE = glo:Queue
    ADD(DASBRW::5:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sub:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020159'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CustomerEnquiryReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:SUBTRACC.Open
  SELF.FilesOpened = True
  tmp:StartDate = Deformat('1/1/1990',@d6)
  tmp:EndDate = Today()
  tmp:DespatchDays = 3
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','CustomerEnquiryReportCriteria')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,sub:Account_Number_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,sub:Account_Number,1,BRW4)
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:Tag,BRW4.Q.tmp:Tag)
  BRW4.AddField(sub:Account_Number,BRW4.Q.sub:Account_Number)
  BRW4.AddField(sub:Company_Name,BRW4.Q.sub:Company_Name)
  BRW4.AddField(sub:RecordNumber,BRW4.Q.sub:RecordNumber)
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:SUBTRACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','CustomerEnquiryReportCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020159'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020159'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020159'&'0')
      ***
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      CustomerEnquiryReport(tmp:StartDate,tmp:EndDate,tmp:DespatchDays)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::5:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue







CustomerEnquiryReport PROCEDURE(func:StartDate,func:EndDate,func:DespatchDays)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:CustName         STRING(30)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:JobType          STRING(1)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:IMEI             STRING(16)
tmp:Count            LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Current_Status)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:date_booked)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT('Status Report'),AT(396,2771,7521,8156),PRE(RPT),FONT('Arial',10,,),THOUS
                       HEADER,AT(396,479,7521,1667),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(def:User_Name),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(94,313,3531,198),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING('Date Printed:'),AT(4948,573),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(5938,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(94,469,3531,198),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,625,3531,198),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(94,781),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Printed By:'),AT(4948,938,625,208),USE(?String67),TRN,FONT(,8,,)
                         STRING(@s60),AT(5938,938),USE(tmp:printedby),TRN,FONT(,8,,FONT:bold)
                         STRING('Tel: '),AT(94,990),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s20),AT(563,990),USE(tmp:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(94,1146),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(563,1146),USE(tmp:FaxNumber),TRN,FONT(,9,,)
                         STRING('Email:'),AT(104,1302),USE(?String16:2),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1302,3531,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Page:'),AT(4948,1302),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@s4),AT(5938,1302),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6302,1302),USE(?String72),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6563,1302,375,208),USE(?CPCSPgOfPgStr),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
break1                 BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,156),USE(?DetailBand)
                           STRING(@d6b),AT(156,0),USE(job:date_booked),TRN,FONT(,7,,)
                           STRING(@s9),AT(781,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,7,,)
                           STRING(@s1),AT(1510,0),USE(tmp:JobType),TRN,LEFT,FONT(,7,,)
                           STRING(@s20),AT(1771,0,833,156),USE(tmp:CustName),TRN,LEFT,FONT(,7,,)
                           STRING(@s16),AT(2760,0),USE(tmp:IMEI),TRN,LEFT,FONT(,7,,)
                           STRING(@s28),AT(3750,0),USE(job:Current_Status),TRN,FONT(,7,,)
                           STRING(@s15),AT(5313,0),USE(job:Unit_Type),TRN,FONT(,7,,)
                           STRING(@s30),AT(6198,0),USE(job:Order_Number),TRN,FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:3)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,156),USE(?String68),TRN,FONT(,10,,FONT:bold)
                           STRING(@s9),AT(1198,156),USE(tmp:Count),TRN,FONT(,10,,FONT:bold)
                         END
                       END
                       FORM,AT(396,479,7521,11198)
                         IMAGE,AT(0,0,7521,11198),USE(?Image1)
                         STRING('CUSTOMER ENQUIRY REPORT'),AT(4427,52),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         STRING('Job No'),AT(906,2083),USE(?String20),TRN,FONT(,8,,FONT:bold)
                         STRING('Type'),AT(1406,2083),USE(?String21),TRN,FONT(,8,,FONT:bold)
                         STRING('Cust Name'),AT(1771,2083),USE(?String27),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(5313,2083),USE(?String27:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(6198,2083),USE(?String27:3),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. No'),AT(2760,2083),USE(?String29),TRN,FONT(,8,,FONT:bold)
                         STRING('Status'),AT(3750,2083),USE(?String31),TRN,FONT(,8,,FONT:bold)
                         STRING('Booked'),AT(240,2083),USE(?String23),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('CustomerEnquiryReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','CustomerEnquiryReport')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('tmp:StartDate',tmp:StartDate)
  BIND('tmp:EndDate',tmp:EndDate)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  ! Before Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
  ! After Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job:Date_Booked_Key)
      Process:View{Prop:Filter} = |
      'job:date_booked >= tmp:StartDate AND job:date_booked <<= tmp:EndDate'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        error# = 0
        print# = 1
        glo:Pointer = job:Account_Number
        Get(glo:Queue,glo:Pointer)
        If Error()
            error# = 1
        End!If Error()
        
        If error# = 0
            If job:surname  = ''
                access:subtracc.clearkey(sub:account_number_key)
                sub:account_number = job:account_number
                if access:subtracc.fetch(sub:account_number_key) = level:benign
                    access:tradeacc.clearkey(tra:account_number_key)
                    tra:account_number = sub:main_account_number
                    if access:tradeacc.fetch(tra:account_number_key) = level:benign
                        if tra:use_sub_accounts = 'YES'
                            tmp:custname    = sub:company_name
                        else!if tra:use_sub_accounts = 'YES'
                            tmp:custname    = tra:company_name
                        end!if tra:use_sub_accounts = 'YES'
                    end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                end!if access:subtracc.fetch(sub:account_number_key) = level:benign
            Else!If job:surname  = ''
                tmp:custname    = job:surname
            End!If job:surname  = ''
        
            tmp:IMEI    = job:ESN
            tmp:JobType = 'J'
            If job:Exchange_Unit_Number <> ''
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = job:Exchange_Unit_Number
                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Found
                    tmp:IMEI    = xch:ESN
                    tmp:JobType = 'E'
                Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            End!If job:Exchange_Unit_Number <> ''
        
            If job:Date_Despatched <> ''
                If job:Date_Despatched < Today() - func:DespatchDays
                    print# = 0
                End!If job:Date_Despatched < Today() - func:DespatchDays
            End!If job:Date_Despatched <> ''
        
            If print# = 1
                tmp:Count += 1
                Print(rpt:Detail)
        
            End!If print# = 1
        End!If error# = 0
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','CustomerEnquiryReport')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='CustomerEnquiryReport'
  END
  report{Prop:Preview} = PrintPreviewImage







