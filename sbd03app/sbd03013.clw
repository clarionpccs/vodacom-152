

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBD03013.INC'),ONCE        !Local module procedure declarations
                     END


AvailableExchangeUnitsReport PROCEDURE                !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tagLocation          STRING(1)
locTrue              BYTE(1)
MaxTabs              BYTE
TabNumber            BYTE
tagStockType         STRING(1)
locYES               STRING('YES')
locReportType        BYTE
locOneLocation       STRING(30)
qSummary             QUEUE,PRE()
sumLocation          STRING(30)
sumManufacturer      STRING(30)
sumModelNumber       STRING(30)
sumQuantity          LONG
                     END
BRW8::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                       PROJECT(loc:Active)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tagLocation            LIKE(tagLocation)              !List box control field - type derived from local data
tagLocation_Icon       LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
loc:Active             LIKE(loc:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                       PROJECT(stp:Use_Exchange)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tagStockType           LIKE(tagStockType)             !List box control field - type derived from local data
tagStockType_Icon      LONG                           !Entry's icon ID
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
stp:Use_Exchange       LIKE(stp:Use_Exchange)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(850,5,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       BUTTON,AT(648,4),USE(?ButtonHelp:2),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(164,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Available Exchange Report'),AT(168,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),BELOW,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Select Location'),AT(248,86),USE(?Prompt3),TRN,FONT(,,,FONT:bold)
                           LIST,AT(248,98,188,174),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(72,170,50,13),USE(?DASREVTAG),HIDE
                           BUTTON,AT(244,274),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(307,274),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(372,274),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           OPTION('Report Type'),AT(257,302,167,25),USE(locReportType),BOXED,TRN,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                             RADIO('Detailed'),AT(271,313),USE(?locReportType:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                             RADIO('Summary'),AT(352,313,47,10),USE(?locReportType:Radio1:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                           END
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Select Stock Type'),AT(248,88),USE(?Prompt4),TRN,FONT(,,,FONT:bold)
                           LIST,AT(248,100,188,192),USE(?List:2),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Stock Type~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(532,190,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(524,222,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(244,296),USE(?DASTAG:2),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(307,296),USE(?DASTAGAll:2),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(372,296),USE(?DASUNTAGALL:2),TRN,FLAT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON('sho&W tags'),AT(56,202,70,13),USE(?DASSHOWTAG),HIDE
                       PANEL,AT(164,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(168,332),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(232,332),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(380,332),USE(?Export),TRN,FLAT,ICON('exportp.jpg')
                     END

Wizard11         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
EX                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW12                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
LoadSBOnlineCriteria    PROCEDURE(),LONG
    END! MAP
RepQ    RepQClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = loc:Location
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
    tagLocation = '*'
  ELSE
    DELETE(glo:Queue)
    tagLocation = ''
  END
    Queue:Browse.tagLocation = tagLocation
  IF (tagLocation = '*')
    Queue:Browse.tagLocation_Icon = 2
  ELSE
    Queue:Browse.tagLocation_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = loc:Location
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = loc:Location
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW12.UpdateBuffer
   glo:Queue2.Pointer2 = stp:Stock_Type
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = stp:Stock_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tagStockType = '*'
  ELSE
    DELETE(glo:Queue2)
    tagStockType = ''
  END
    Queue:Browse:1.tagStockType = tagStockType
  IF (tagStockType = '*')
    Queue:Browse:1.tagStockType_Icon = 2
  ELSE
    Queue:Browse:1.tagStockType_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW12.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = stp:Stock_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW12.Reset
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::13:QUEUE = glo:Queue2
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:Queue2)
  BRW12.Reset
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer2 = stp:Stock_Type
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = stp:Stock_Type
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW12.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
AddRecord       ROUTINE
DATA
locDateReceived DATE()
CODE

    ! Location
    SBR.AddToExport(xch:Location,1)
    ! Stock Type
    SBR.AddToExport(xch:Stock_Type)
    ! Manufacturer
    SBR.AddToExport(xch:Manufacturer)
    ! Model Number
    SBR.AddToExport(xch:Model_Number)
    ! Description
    SBR.AddToExport('''' & xch:ESN)
    ! Date Received
    ! Days Available
    
    Access:EXCHHIST.Clearkey(exh:Ref_Number_Key)
    exh:Ref_Number = xch:Ref_Number
    exh:Date = Today()
    SET(exh:Ref_Number_Key,exh:Ref_Number_Key)
    LOOP UNTIL Access:EXCHHIST.Next()
        IF (exh:Ref_Number <> xch:Ref_Number)
            BREAK
        END
        IF (exh:Status = 'UNIT RECEIVED')
            locDateReceived = exh:Date
            BREAK
        END
    END
    IF (locDateReceived = 0)
        SBR.AddToExport('')
        SBR.AddToExport('',,1)
    ELSE
        SBR.AddToExport(Format(locDateReceived,@d06))
        SBR.AddToExport(Today() - locDateReceived,,1)
    END



CreateTitle     ROUTINE

    SBR.AddToExport('Location',1)

    IF (Records(glo:Queue) = 1)
        GET(glo:Queue,1)
        SBR.AddToExport(glo:Pointer,,1)
    ELSE
        SBR.AddToExport('Multiple',,1)
    END

    SBR.AddToExport('Stock Type',1)
    IF (Records(glo:Queue2) = 1)
        GET(glo:Queue2,1)
        SBR.AddToExport(glo:Pointer2,,1)
    ELSE
        SBR.AddToExport('Multiple',,1)
    END

    SBR.AddToExport('Date Created',1)
    SBR.AddToExport(Format(TODAY(),@d06),,1)

    SBR.AddToExport('',1,1)

CreateTitleExcel        ROUTINE
    EX.WriteToCell('Location','A2')

    IF (Records(glo:Queue) = 1)
        GET(glo:Queue,1)
        EX.WriteToCell(glo:Pointer,'B2')
    ELSE
        EX.WriteToCell('Multiple','B2')
    END

    EX.WriteToCell('Stock Type','A3')
    IF (Records(glo:Queue2) = 1)
        GET(glo:Queue2,1)
        EX.WriteToCell(glo:Pointer2,'B3')
    ELSE
        EX.WriteToCell('Multiple','B3')
    END

    EX.WriteToCell('Date Created','A4')
    EX.WriteToCell(Format(TODAY(),@d06),'B4')

    EX.WriteToCell('Report Type','A6')
    EX.WriteToCell('Summary','B6')

    EX.WriteToCell('Location','A8')
    EX.WriteToCell('Manufacturer','B8')
    EX.WriteToCell('Model Number','C8')
    EX.WriteToCell('Quantity','D8')

CreateDetailTitle       ROUTINE
    SBR.AddToExport('Location',1)
    SBR.AddToExport('Stock Type')
    SBR.AddToExport('Manufacturer')
    SBR.AddToExport('Model Number')
    SBR.AddToExport('Description')
    SBR.AddToExport('Date Received')
    SBR.AddToExport('Days Available',,1)


CreateSummaryTitle      ROUTINE
    SBR.AddToExport('Location',1)
    SBR.AddToExport('Manufacturer')
    SBR.AddToExport('Model Number')
    SBR.AddToExport('Quantity',,1)
Export      ROUTINE
DATA
kReportFolder       CSTRING(255)
kReportName         STRING(50)
kFileName           STRING(255)
kTempFolder         CSTRING(255)
kTotalRecords       LONG()
kLastDataColumn     STRING(1)
kPasteDataCell      STRING(30)
locClipBoard        ANY
CODE

    kReportName = 'Available Exchange Units Report'

    kTempFolder = GetTempFolder()

    IF (kTempFolder = '')
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('An error occurred creating the report.'&|
            '|'&|
            '|Please ty again.','ServiceBase',|
                       'mstop.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        EXIT
    END

    IF (glo:WebJob = 0)
        kFileName = clip(kReportName) & |
        ' ' & FORMAT(today(),@d12) & '.xls'
    ELSE ! IF (glo:WebJob = 1)
        IF (locReportType = 1)
            kFileName = clip(kReportName) & ' (Summary) ' & FORMAT(today(),@d12) & '.csv'
        ELSE
            kFileName = clip(kReportName) & ' (Detailed) ' & FORMAT(today(),@d12) & '.csv'
        END
    END ! IF (glo:WebJob = 1)

    SBR:ExportFilePath = CLIP(kTempFolder) & 'AEREP_' & CLOCK() & RANDOM(1,1000) & '.CSV'

    SBR.Init(RepQ.SBOReport)

    SBR.OpenProgressWindow(100)

    SBR.UpdateProgressText('')

    SBR.UpdateProgressText('Report Started: ' & FORMAT(SBR:StartDate,@d6) & |
        ' ' & FORMAT(SBR:StartTime,@t1))
    SBR.UpdateProgressText('')

    SBR.AddToExport(kReportName,1,1)

    Do CreateTitle

    IF (locReportType = 0)
        ! Showing detail titles
        SBR.AddToExport('Report Type',1)
        SBR.AddToExport('Detailed',,1)
        SBR.AddToExport(,1,1)

        Do CreateDetailTitle
    END

    LOOP l# = 1 TO RECORDS(glo:Queue)
        GET(glo:Queue,l#)

        SBR.UpdateProgressText('')

        IF (SBR.UpdateProgressWindow())
            BREAK
        END

        RepQ.TotalRecords = RECORDS(glo:Queue2)
        

        LOOP s# = 1 TO RECORDS(glo:Queue2)
            GET(glo:Queue2,s#)

            IF (SBR.UpdateProgressWindow())
                BREAK
            END
            SBR.UpdateProgressText('Location: ' & CLIP(glo:Pointer) & ', Stock Type: ' & CLIP(glo:Pointer2))

            IF (RepQ.SBOReport)
                IF (RepQ.UpdateProgress())
                    SBR:CancelPressed = 2
                    BREAK
                END ! IF
            END ! IF

            count# = 0
            Access:EXCHANGE.CLearkey(xch:LocStockAvailModelKey)
            xch:Location    = glo:Pointer
            xch:Stock_Type  = glo:Pointer2
            xch:Available   = 'AVL'
            SET(xch:LocStockAvailModelKey,xch:LocStockAvailModelKey)
            LOOP UNTIL Access:EXCHANGE.Next()
                IF (xch:Location <> glo:Pointer OR |
                    xch:Stock_Type <> glo:Pointer2 OR |
                    xch:Available <> 'AVL')
                    BREAK
                END

                IF (SBR.UpdateProgressWindow())
                    BREAK
                END

                IF (locReportType <> 1)
                    ! If Detail
                    Do AddRecord
                END
                count# += 1
                kTotalRecords += 1

                ! Add To Summary
                qSummary.sumLocation = xch:Location
                qSummary.sumManufacturer = xch:Manufacturer
                qSummary.sumModelNumber = xch:Model_Number
                GET(qSummary,qSummary.sumLocation,qSummary.sumManufacturer,qSummary.sumModelNumber)
                IF (ERROR())
                    qSummary.sumQuantity = 1
                    ADD(qSummary)
                ELSE
                    qSummary.sumQuantity += 1
                    PUT(qSummary)
                END

            END
            SBR.UpdateProgressText('Records Found: ' & count#,1)
            IF (SBR:CancelPressed > 0)
                BREAK
            END
        END ! LOOP s# = 1 TO RECORDS(glo:Queue2)
        IF (SBR:CancelPressed > 0)
            BREAK
        END
    END ! LOOP l# = 1 TO RECORDS(glo:Queue)

    IF (locReportType = 1)
        ! RRC Should be the only one who does summary only
        SBR.AddToExport('Report Type',1)
        SBR.AddToExport('Summary',,1)
        SBR.AddToExport()

        Do CreateSummaryTitle

        LOOP ll# = 1 TO RECORDS(qSummary)
            GET(qSummary,ll#)
            SBR.AddToExport(qSummary.sumLocation,1)
            SBR.AddToExport(qSummary.sumManufacturer)
            SBR.AddToExport(qSummary.sumModelNumber)
            SBR.AddToExport(qSummary.sumQuantity,,1)
        END
        SBR.AddToExport('Totals:',1)
        SBR.AddToExport(RECORDS(qSummary))
        SBR.AddToExport()
        SBR.AddToExport(kTotalRecords,,1)

    ELSE
        SBR.AddToExport('Total Records',1)
        SBR.AddToExport(kTotalRecords,,1)

    END

    SBR.Kill()

    IF (SBR:CancelPressed = 2)
        SBR.UpdateProgressText('===============')
        SBR.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))
       SBR.EndProgress()
        REMOVE(SBR:ExportFile)
    ELSE
        IF (glo:WebJob = 1)

            IF (RepQ.SBOReport = TRUE)
                ! Lose the date on the filename, as I'll add it in later
                kFileName = CLIP(kReportName) & ' ' & CHOOSE(locReportType = 1,'(Summary)','(Detailed)') & '.csv'

                IF (RepQ.FinishReport(kFileName,SBR:ExportFilePath))
                END ! IF

                SBR.EndProgress()
                POST(Event:CLoseWindow)
                EXIT
            ELSE
                REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
                RENAME(SBR:ExportFilePath,CLIP(kTempFolder) & CLIP(kFileName))
                SendFileToClient(CLIP(kTempFolder) & CLIP(kFileName))
                REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
                SBR.EndProgress()

                Beep(Beep:SystemAsterisk)  ;  Yield()
                Case Missive('Export File Created.'&|
                    '|'&|
                    '|' & Clip(kFileName) & '','ServiceBase',|
                               'midea.jpg','/&OK') 
                Of 1 ! &OK Button
                End!Case Message
            END ! IF
            


        ELSE
            SBR.UpdateProgressText()
            SBR.UpdateProgressText('Formatting Document')
            SBR.UpdateProgressText()
            SBR.UpdateProgressWindow()
            ! Build Excel Document
            If EX.Init(0,0) = 0     
                Case Missive('An error has occurred finding your Excel document.'&|
                  '<13,10>'&|
                  '<13,10>Please quit and try again.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Exit
            End !If EX.Init(0,0,1) = 0

            SETCLIPBOARD(SBR.LoadTempData(EX,SBR:ExportFilePath,'G' & (kTotalRecords+15)))

            SBR.UpdateProgressWindow()

            EX.NewWorkBook()
            EX.RenameWorkSheet('Detailed')
            EX.SelectCells('A1')
            EX.Paste()
            SBR.UpdateProgressWindow()
            SBR.ClearClipboard()

            SBR.FinishFormat(EX,'G',kTotalRecords + 10,'A9')

            SBR.UpdateProgressWindow()
            EX.InsertWorksheet()
            EX.RenameWorkSheet('Summary')
            EX.WriteToCell(kReportName,'A1')
            Do CreateTitleExcel

            EX.SelectCells('A9')

            row# = 9
            LOOP ll# = 1 TO RECORDS(qSummary)
                GET(qSummary,ll#)
                SBR.UpdateProgressWindow()
                EX.WriteToCell(qSummary.sumLocation,'A' & row#)
                EX.WriteToCell(qSummary.sumManufacturer,'B' & row#)
                EX.WriteToCell(qSummary.sumModelNumber,'C' & row#)
                EX.WriteToCell(qSummary.sumQuantity,'D' & row#)
                row# += 1
            END
            EX.WriteToCell('Totals:','A' & row#)
            EX.WriteToCell(RECORDS(qSummary),'B' & row#)
            EX.WriteToCell(kTotalRecords,'D' & row#)

            SBR.FinishFormat(EX,'D',row#,'A9')

            REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
            EX.SaveAs(CLIP(kTempFolder) & CLIP(kFileName))
            EX.CloseWorkBook(2)
            EX.Kill()
            SBR.UpdateProgressWindow()

            SBR.UpdateProgressText('===============')
            SBR.UpdateProgressText('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

            kReportFolder = SetReportsFolder('ServiceBase Export',CLIP(kReportName),glo:WebJob)
            IF (SBR.CopyFinalFile(CLIP(kTempFolder) & Clip(kFileName),CLIP(kReportFolder) & CLIP(kFilename)))
                ! Couldn't save final file
                SBR.EndProgress()
            ELSE
                SBR.EndProgress(kReportFolder)
            END
        END
    END
    REMOVE(SBR:ExportFilePath)

    Post(EVENT:CloseWindow)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020790'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AvailableExchangeUnitsReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:LOCATION.Open
  Relate:REPORTQ.Open
  Access:USERS.UseFile
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:LOCATION,SELF)
  BRW12.Init(?List:2,Queue:Browse:1.ViewPosition,BRW12::View:Browse,Queue:Browse:1,Relate:STOCKTYP,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','AvailableExchangeUnitsReport')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard11.Init(?Sheet1, |                          ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Export, |                       ! OK button
                     ?Cancel, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF (RepQ.Init(COMMAND()) = Level:Benign)
      0{prop:Hide} = 1
  END ! IF
  BRW8.Q &= Queue:Browse
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,loc:ActiveLocationKey)
  BRW8.AddRange(loc:Active,locTrue)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,loc:Location,1,BRW8)
  BIND('tagLocation',tagLocation)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tagLocation,BRW8.Q.tagLocation)
  BRW8.AddField(loc:Location,BRW8.Q.loc:Location)
  BRW8.AddField(loc:RecordNumber,BRW8.Q.loc:RecordNumber)
  BRW8.AddField(loc:Active,BRW8.Q.loc:Active)
  BRW12.Q &= Queue:Browse:1
  BRW12.RetainRow = 0
  BRW12.AddSortOrder(,stp:Use_Exchange_Key)
  BRW12.AddRange(stp:Use_Exchange,locYES)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,stp:Stock_Type,1,BRW12)
  BIND('tagStockType',tagStockType)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW12.AddField(tagStockType,BRW12.Q.tagStockType)
  BRW12.AddField(stp:Stock_Type,BRW12.Q.stp:Stock_Type)
  BRW12.AddField(stp:Use_Exchange,BRW12.Q.stp:Use_Exchange)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  BRW12.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW12.AskProcedure = 0
      CLEAR(BRW12.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
  IF (glo:WebJob = 1)
  
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = glo:Password
      IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          glo:Pointer = use:Location
          ADD(glo:Queue)
      END ! IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
      locOneLocation = use:Location
      ?locReportType{prop:Hide} = false
      ?List{prop:Disable} = true
  
  END
  
  
  IF (RepQ.SBOReport = TRUE)
      IF (LoadSBOnlineCriteria())
          RETURN RequestCancelled
      END ! IF
  END ! IF
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:EXCHANGE.Close
    Relate:LOCATION.Close
    Relate:REPORTQ.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','AvailableExchangeUnitsReport')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard11.Validate()
        DISABLE(Wizard11.NextControl())
     ELSE
        ENABLE(Wizard11.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020790'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020790'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020790'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    OF ?VSBackButton
      ThisWindow.Update
         Wizard11.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard11.TakeAccepted()
    OF ?Export
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
      If (RECORDS(glo:Queue) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any locations.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      IF (RECORDS(glo:Queue2) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any stock types.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      Do Export
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    EX.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LoadSBOnlineCriteria    PROCEDURE()!,LONG
RetValue LONG(Level:Benign)
i LONG
qStockTypes QUEUE(),PRE(qStockTypes)
StockType STRING(30)
            END ! QUEUE
locAllStockTypes CSTRING(10)
locReportingType CSTRING(10)
xmlFileName STRING(255)
qXML    QUEUE(),PRE(qXml)
ElementName STRING(30)
    END! QUEUE
    CODE
        LOOP 1 TIMES
            glo:WebJob = 1
            
            IF (XML:LoadFromFile(RepQ.FullCriteriaFilename))
                
                RetValue = Level:Fatal
            END ! IF
            XML:GotoTop

            IF (~XML:FindNextNode('ReportType'))
                IF (XML:ReadCurrentRecord(qXML,locReportingType))
                END ! IF
                locReportType = locReportingType
            END ! IF
            XML:GotoTop
            IF (~XML:FindNextNode('AllStockTypes'))
                IF (XML:ReadCurrentRecord(qXML,locAllStockTypes))
                END ! IF
                IF (locAllStockTypes = 1)

                    DO DASBRW::13:DASTAGALL
                ELSE ! IF
                    XML:GotoTop
                    IF (~XML:FindNextNode('StockTypes'))
                        recs# = XML:LoadQueue(qStockTypes,1,1)
                        
                        IF (RECORDS(qStockTypes) > 0)
                            FREE(glo:Queue2); CLEAR(glo:Queue2)
                        END ! IF
                        
                        LOOP i = 1 TO RECORDS(qStockTypes)
                            GET(qStockTypes,i)
                            glo:Pointer2 = qStockTypes.StockType
                            ADD(glo:Queue2)
                        END ! IF
                    END ! IF
                END ! IF
            END ! IF
        XML:Free()

        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code = RepQ.Usercode
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            glo:Pointer = use:Location
            locOneLocation = use:Location
            ADD(glo:Queue)
        END ! IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
        
        RepQ.UpdateStatus('Running')

        DO Export

    END ! BREAK LOOP

    IF (RetValue = Level:Fatal)
        RepQ.Failed()
    END ! IF
    RETURN RetValue
!---------------------------------------------------------------------------------
EX.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
EX.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
EX.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tagLocation = ''
    ELSE
      tagLocation = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagLocation = '*')
    SELF.Q.tagLocation_Icon = 2
  ELSE
    SELF.Q.tagLocation_Icon = 1
  END


BRW8.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  IF (locOneLocation <> '')
      IF (loc:Location <> locOneLocation)
          RETURN Record:Filtered
      END
  END
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW12.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = stp:Stock_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tagStockType = ''
    ELSE
      tagStockType = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagStockType = '*')
    SELF.Q.tagStockType_Icon = 2
  ELSE
    SELF.Q.tagStockType_Icon = 1
  END


BRW12.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW12::RecordStatus=ReturnValue
  IF BRW12::RecordStatus NOT=Record:OK THEN RETURN BRW12::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = stp:Stock_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW12::RecordStatus
  RETURN ReturnValue

Wizard11.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW8.Q &= NULL) ! Has Browse Object been initialized?
       BRW8.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW12.Q &= NULL) ! Has Browse Object been initialized?
       BRW12.ResetQueue(Reset:Queue)
    END

Wizard11.TakeBackEmbed PROCEDURE
   CODE

Wizard11.TakeNextEmbed PROCEDURE
   CODE

Wizard11.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
ExchangeStockValuationReport PROCEDURE                !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tagLocation          STRING(1)
locTrue              BYTE(1)
MaxTabs              BYTE
TabNumber            BYTE
tagStockType         STRING(1)
locYES               STRING('YES')
locOneLocation       STRING(30)
locTotalValue        REAL
locTotalTotal        REAL
qSummary             QUEUE,PRE()
sumManufacturer      STRING(30)
sumModelNumber       STRING(30)
sumForP              STRING(1)
sumQuantity          LONG
                     END
BRW8::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                       PROJECT(loc:Active)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tagLocation            LIKE(tagLocation)              !List box control field - type derived from local data
tagLocation_Icon       LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
loc:Active             LIKE(loc:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                       PROJECT(stp:Use_Exchange)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tagStockType           LIKE(tagStockType)             !List box control field - type derived from local data
tagStockType_Icon      LONG                           !Entry's icon ID
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
stp:Use_Exchange       LIKE(stp:Use_Exchange)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(850,5,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Exchange Stock Valuation Report'),AT(168,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(666,71),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),BELOW,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Select Location'),AT(248,86),USE(?Prompt3),TRN,FONT(,,,FONT:bold)
                           LIST,AT(248,98,188,198),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(180,182,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(188,206,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(244,298),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,298),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(372,298),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Select Stock Type'),AT(248,88),USE(?Prompt4),TRN,FONT(,,,FONT:bold)
                           LIST,AT(248,100,188,196),USE(?List:2),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Stock Type~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(302,177,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(302,197,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(244,298),USE(?DASTAG:2),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,298),USE(?DASTAGAll:2),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(372,298),USE(?DASUNTAGALL:2),TRN,FLAT,ICON('untagalp.jpg')
                         END
                       END
                       PANEL,AT(164,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(168,332),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(232,332),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(380,332),USE(?Export),TRN,FLAT,ICON('exportp.jpg')
                     END

Wizard11         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW12                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
LoadSBOnlineCriteria    PROCEDURE(),LONG
    END! MAP
RepQ    RepQClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = loc:Location
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
    tagLocation = '*'
  ELSE
    DELETE(glo:Queue)
    tagLocation = ''
  END
    Queue:Browse.tagLocation = tagLocation
  IF (tagLocation = '*')
    Queue:Browse.tagLocation_Icon = 2
  ELSE
    Queue:Browse.tagLocation_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = loc:Location
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = loc:Location
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW12.UpdateBuffer
   glo:Queue2.Pointer2 = stp:Stock_Type
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = stp:Stock_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tagStockType = '*'
  ELSE
    DELETE(glo:Queue2)
    tagStockType = ''
  END
    Queue:Browse:1.tagStockType = tagStockType
  IF (tagStockType = '*')
    Queue:Browse:1.tagStockType_Icon = 2
  ELSE
    Queue:Browse:1.tagStockType_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW12.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = stp:Stock_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW12.Reset
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::13:QUEUE = glo:Queue2
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:Queue2)
  BRW12.Reset
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer2 = stp:Stock_Type
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = stp:Stock_Type
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW12.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
AddRecord       ROUTINE
    ! Location
    SBR.AddToExport(glo:Pointer,1)
    ! Stock Type
    SBR.AddToExport(glo:Pointer2)


    Access:STOCK.Clearkey(sto:ExchangeModelKey)
    sto:Location = MainStoreLocation()
    sto:Manufacturer = qSummary.sumManufacturer
    sto:ExchangeModelNumber = qSummary.sumModelNumber
    stockError# = Access:STOCK.TryFetch(sto:ExchangeModelKey)

    ! Part Number
    IF (stockError# = 0)
        SBR.AddToExport(sto:Part_Number)
    ELSE
        SBR.AddToExport()
    END
    ! Manufacturer
    SBR.AddToExport(qSummary.sumManufacturer)
    ! Model Number
    SBR.AddToExport(qSummary.sumModelNumber)
    ! Quantity
    SBR.AddToExport(qSummary.sumQuantity)

    ! Value
    ! Total
    Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
    mod:Manufacturer = qSummary.sumManufacturer
    mod:Model_Number = qSummary.sumModelNumber
    IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)
        SBR.AddToExport(Format(mod:ExchReplaceValue,@n_14.2))
        SBR.AddToExport(Format(mod:ExchReplaceValue * qSummary.sumQuantity,@n_14.2))
        locTotalValue += mod:ExchReplaceValue
        locTotalTotal += mod:ExchReplaceValue * qSummary.sumQuantity
    ELSE ! IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)
        SBR.AddToExport()
        SBR.AddToExport()
    END ! IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)

    ! Purchased FOC
    IF (glo:WebJob = 1)
        SBR.AddToExport(,,1)
    ELSE
        IF (glo:Pointer = MainStoreLocation())
            IF (qSummary.sumForP = '')
                SBR.AddToExport('F',,1)
            ELSE
                SBR.AddToExport(qSummary.sumForP,,1)
            END
        ELSE
            SBR.AddToExport(,,1)
        END
    END


CreateTitle     ROUTINE
    SBR.AddToExport('Location',1)

    IF (Records(glo:Queue) = 1)
        GET(glo:Queue,1)
        SBR.AddToExport(glo:Pointer,,1)
    ELSE
        SBR.AddToExport('Multiple',,1)
    END

    SBR.AddToExport('Stock Type',1)
    IF (Records(glo:Queue2) = 1)
        GET(glo:Queue2,1)
        SBR.AddToExport(glo:Pointer2,,1)
    ELSE
        SBR.AddToExport('Multiple',,1)
    END

    SBR.AddToExport('Date Created',1)
    SBR.AddToExport(Format(TODAY(),@d06),,1)

    SBR.AddToExport('',1,1)

CreateTitleExcel        ROUTINE
    E1.WriteToCell('Location','A2')

    IF (Records(glo:Queue) = 1)
        GET(glo:Queue,1)
        E1.WriteToCell(glo:Pointer,'B2')
    ELSE
        E1.WriteToCell('Multiple','B2')
    END

    E1.WriteToCell('Stock Type','A3')
    IF (Records(glo:Queue2) = 1)
        GET(glo:Queue2,1)
        E1.WriteToCell(glo:Pointer2,'B3')
    ELSE
        E1.WriteToCell('Multiple','B3')
    END

    E1.WriteToCell('Date Created','A4')
    E1.WriteToCell(Format(TODAY(),@d06),'B4')

    E1.WriteToCell('Report Type','A6')
    E1.WriteToCell('Summary','B6')

    E1.WriteToCell('Location','A8')
    E1.WriteToCell('Manufacturer','B8')
    E1.WriteToCell('Model Number','C8')
    E1.WriteToCell('Quantity','D8')

CreateDetailTitle       ROUTINE
    SBR.AddToExport('Location',1)
    SBR.AddToExport('Stock Type')
    SBR.AddToExport('Part Number')
    SBR.AddToExport('Manufacturer')
    SBR.AddToExport('Model Number')
    SBR.AddToExport('Quantity')
    SBR.AddToExport('Value')
    IF (glo:WebJob)
        SBR.AddToExport('Total',,1)
    ELSE
        SBR.AddToExport('Total')
        SBR.AddToExport('Purchase / FOC',,1)
    END


Export      ROUTINE
DATA
kReportFolder       CSTRING(255)
kReportName         STRING(50)
kFileName           STRING(255)
kTempFolder         CSTRING(255)
kTotalRecords       LONG()
kTotalQuantity      LONG()

CODE
    kReportName = 'Exchange Stock Valuation Report'

    kTempFolder = GetTempFolder()

    IF (kTempFolder = '')
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('An error occurred creating the report.'&|
            '|'&|
            '|Please ty again.','ServiceBase',|
                       'mstop.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        EXIT
    END

    IF (glo:WebJob = 0)
        kFileName = clip(kReportName) & |
        ' ' & FORMAT(today(),@d12) & '.xls'
    ELSE ! IF (glo:WebJob = 1)
        kFileName = clip(kReportName) & ' ' & FORMAT(today(),@d12) & '.csv'
    END ! IF (glo:WebJob = 1)

    SBR:ExportFilePath = CLIP(kTempFolder) & 'ASEREP_' & CLOCK() & RANDOM(1,1000) & '.CSV'

    SBR.Init(RepQ.SBOReport)

    SBR.OpenProgressWindow(100)

    SBR.UpdateProgressText('')

    SBR.UpdateProgressText('Report Started: ' & FORMAT(SBR:StartDate,@d6) & |
        ' ' & FORMAT(SBR:StartTime,@t1))
    SBR.UpdateProgressText('')

    SBR.AddToExport(kReportName,1,1)

    Do CreateTitle

    Do CreateDetailTitle

    LOOP l# = 1 TO RECORDS(glo:Queue)
        GET(glo:Queue,l#)

        SBR.UpdateProgressText('',1)

        IF (SBR.UpdateProgressWindow())
            BREAK
        END

        RepQ.TotalRecords = RECORDS(glo:Queue2)

        LOOP s# = 1 TO RECORDS(glo:Queue2)
            GET(glo:Queue2,s#)

            IF (SBR.UpdateProgressWindow())
                BREAK
            END
            SBR.UpdateProgressText('Location: ' & CLIP(glo:Pointer) & ', Stock Type: ' & CLIP(glo:Pointer2))

            IF (RepQ.SBOReport)
                IF (RepQ.UpdateProgress())
                    SBR:CancelPressed = 2
                    BREAK
                END ! IF
            END ! IF

            count# = 0
            FREE(qSummary)
            Access:EXCHANGE.CLearkey(xch:LocStockAvailModelKey)
            xch:Location    = glo:Pointer
            xch:Stock_Type  = glo:Pointer2
            xch:Available   = 'AVL'
            SET(xch:LocStockAvailModelKey,xch:LocStockAvailModelKey)
            LOOP UNTIL Access:EXCHANGE.Next()
                IF (xch:Location <> glo:Pointer OR |
                    xch:Stock_Type <> glo:Pointer2 OR |
                    xch:Available <> 'AVL')
                    BREAK
                END

                IF (SBR.UpdateProgressWindow())
                    BREAK
                END
                qSummary.sumManufacturer = xch:Manufacturer
                qSummary.sumModelNumber = xch:Model_Number
                qSummary.sumForP = xch:FreeStockPurchased
                GET(qSummary,qSummary.sumManufacturer,qSummary.sumModelNumber,qSummary.sumForP)
                IF (NOT ERROR())
                    qSummary.sumQuantity += 1
                    PUT(qSummary)
                ELSE
                    qSummary.sumQuantity = 1
                    ADD(qSummary)
                END
                


            END

            LOOP q# = 1 TO RECORDS(qSummary)
                GET(qSummary,q#)

                IF (SBR.UpdateProgressWindow())
                    BREAK
                END

                Do AddRecord
                count# += 1
                kTotalRecords += 1
                kTotalQuantity += qSummary.sumQuantity

            END

            SBR.UpdateProgressText('Records Found: ' & RECORDS(qSummary),1)
            IF (SBR:CancelPressed > 0)
                BREAK
            END
        END ! LOOP s# = 1 TO RECORDS(glo:Queue2)
        IF (SBR:CancelPressed > 0)
            BREAK
        END
    END ! LOOP l# = 1 TO RECORDS(glo:Queue)

    SBR.AddToExport('Totals',1)
    SBR.AddToExport(kTotalRecords)
    SBR.AddToExport()
    SBR.AddToExport()
    SBR.AddToExport()
    SBR.AddToExport(kTotalQuantity)
    SBR.AddToExport(Format(locTotalValue,@n_14.2))
    SBR.AddToExport(Format(locTotalTotal,@n_14.2),,1)

    SBR.Kill()

    IF (SBR:CancelPressed = 2)
        SBR.UpdateProgressText('===============')
        SBR.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))
        SBR.EndProgress()
        REMOVE(SBR:ExportFile)
    ELSE
        IF (glo:WebJob = 1)
            IF (RepQ.SBOReport = TRUE)
                SBR.EndProgress()
                kFilename = CLIP(kReportName) & '.csv'
                IF (RepQ.FInishReport(kFilename,SBR:ExportFilePath))
                END ! IF
                REMOVE(SBR:ExportFilePath)
                Post(Event:CloseWindow)
                EXIT

            ELSE ! IF
                REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
                RENAME(SBR:ExportFilePath,CLIP(kTempFolder) & CLIP(kFileName))
                SendFileToClient(CLIP(kTempFolder) & CLIP(kFileName))
                REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
                SBR.EndProgress()

                Beep(Beep:SystemAsterisk)  ;  Yield()
                Case Missive('Export File Created.'&|
                    '|'&|
                    '|' & Clip(kFileName) & '','ServiceBase',|
                               'midea.jpg','/&OK') 
                Of 1 ! &OK Button
                End!Case Message
            END ! IF
        ELSE
            SBR.UpdateProgressText()
            SBR.UpdateProgressText('Formatting Document')
            SBR.UpdateProgressText()
            ! Build Excel Document
            If E1.Init(0,0) = 0
                Case Missive('An error has occurred finding your Excel document.'&|
                  '<13,10>'&|
                  '<13,10>Please quit and try again.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Exit
            End !If E1.Init(0,0,1) = 0

            SETCLIPBOARD(SBR.LoadTempData(E1,SBR:ExportFilePath,'I' & (kTotalRecords+15)))

            E1.NewWorkBook()
            E1.RenameWorkSheet('Detailed')
            E1.SelectCells('A1')
            E1.Paste()
            SBR.ClearClipboard()

            SBR.FinishFormat(E1,'I',kTotalRecords + 10,'A7')

            REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
            E1.SaveAs(CLIP(kTempFolder) & CLIP(kFileName))
            E1.CloseWorkBook(3)
            E1.Kill()

            SBR.UpdateProgressText('===============')
            SBR.UpdateProgressText('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

            kReportFolder = SetReportsFolder('ServiceBase Export',CLIP(kReportName),glo:WebJob)

            IF (SBR.CopyFinalFile(CLIP(kTempFolder) & Clip(kFileName),CLIP(kReportFolder) & CLIP(kFilename)))
                ! Couldn't save final file
                SBR.EndProgress()
            ELSE
                SBR.EndProgress(kReportFolder)
            END
        END
    END
    REMOVE(SBR:ExportFilePath)

    Post(EVENT:CloseWindow)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ExchangeStockValuationReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:LOCATION.Open
  Access:USERS.UseFile
  Access:EXCHHIST.UseFile
  Access:STOCK.UseFile
  Access:MODELNUM.UseFile
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:LOCATION,SELF)
  BRW12.Init(?List:2,Queue:Browse:1.ViewPosition,BRW12::View:Browse,Queue:Browse:1,Relate:STOCKTYP,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (RepQ.Init(COMMAND()) = Level:Benign)
      0{prop:Hide} = 1
  END!  IF
      ! Save Window Name
   AddToLog('Window','Open','ExchangeStockValuationReport')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard11.Init(?Sheet1, |                          ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Export, |                       ! OK button
                     ?Cancel, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     0, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,loc:ActiveLocationKey)
  BRW8.AddRange(loc:Active,locTrue)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,loc:Location,1,BRW8)
  BIND('tagLocation',tagLocation)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tagLocation,BRW8.Q.tagLocation)
  BRW8.AddField(loc:Location,BRW8.Q.loc:Location)
  BRW8.AddField(loc:RecordNumber,BRW8.Q.loc:RecordNumber)
  BRW8.AddField(loc:Active,BRW8.Q.loc:Active)
  BRW12.Q &= Queue:Browse:1
  BRW12.RetainRow = 0
  BRW12.AddSortOrder(,stp:Use_Exchange_Key)
  BRW12.AddRange(stp:Use_Exchange,locYES)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,stp:Stock_Type,1,BRW12)
  BIND('tagStockType',tagStockType)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW12.AddField(tagStockType,BRW12.Q.tagStockType)
  BRW12.AddField(stp:Stock_Type,BRW12.Q.stp:Stock_Type)
  BRW12.AddField(stp:Use_Exchange,BRW12.Q.stp:Use_Exchange)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  BRW12.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW12.AskProcedure = 0
      CLEAR(BRW12.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
  IF (glo:WebJob = 1)
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = glo:Password
      IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          glo:Pointer = use:Location
          ADD(glo:Queue)
      END ! IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
      locOneLocation = use:Location
      ?List{prop:Disable} = 1
      ?VSBackButton{prop:Hide} = 1
      ?VSNextButton{prop:Hide} = 1
      SELECT(?Sheet1,2)
  END
  
  IF (RepQ.SBOReport = TRUE)
      IF (LoadSBOnlineCriteria())
          RETURN RequestCancelled
      END ! IF
  END ! IF
  
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:EXCHANGE.Close
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ExchangeStockValuationReport')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard11.Validate()
        DISABLE(Wizard11.NextControl())
     ELSE
        ENABLE(Wizard11.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?VSBackButton
      ThisWindow.Update
         Wizard11.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard11.TakeAccepted()
    OF ?Export
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
      If (RECORDS(glo:Queue) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any locations.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      IF (RECORDS(glo:Queue2) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any stock types.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      Do Export
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LoadSBOnlineCriteria    PROCEDURE()!,LONG
RetValue LONG(Level:Benign)
i LONG
qStockTypes QUEUE(),PRE(qStockTypes)
StockType STRING(30)
            END ! QUEUE
qXML    QUEUE(),PRE(qXml)
AllStockTypes CSTRING(30)
    END! QUEUE
    
    CODE
        LOOP 1 TIMES
            glo:WebJob = 1
            
            IF (XML:LoadFromFile(RepQ.FullCriteriaFilename))
                RetValue = Level:Fatal
                BREAK
            END ! IF
            XML:GotoTop

            IF (~XML:FindNextNode('Defaults'))
                recs# = XML:LoadQueue(qXML,1,1)
                IF (RECORDS(qXML) = 0)
                    RetValue = Level:Fatal
                    BREAK
                END ! IF
                GET(qXML, 1)
                IF (qXML.AllStockTypes = 1)
                    DO DASBRW::13:DASTAGALL
                ELSE
                    XML:GotoTop
                    IF (~XML:FindNextNode('StockTypes'))
                        recs# = XML:LoadQueue(qStockTypes,1,1)
                        IF (RECORDS(qStockTypes) = 0)
                            BREAK
                        END ! IF
                        LOOP i = 1 TO RECORDS(qStockTypes)
                            GET(qStockTypes, i)
                            glo:Pointer2 = qStockTypes.StockType
                            ADD(glo:Queue2)
                        END ! LOOP
                    END ! IF
                END ! IF
                XML:Free()
            END ! IF

            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = RepQ.UserCode
            Access:USERS.TryFetch(use:User_Code_Key)
            glo:Pointer = use:Location
            ADD(glo:Queue)
            locOneLocation = glo:Pointer

            RepQ.UpdateStatus('Running')

            DO Export
        END ! BREAK LOOP

        IF (RetValue = Level:Fatal)
            RepQ.Failed()
        END ! IF
        RETURN RetValue
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tagLocation = ''
    ELSE
      tagLocation = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagLocation = '*')
    SELF.Q.tagLocation_Icon = 2
  ELSE
    SELF.Q.tagLocation_Icon = 1
  END


BRW8.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  IF (locOneLocation <> '')
      IF (loc:Location <> locOneLocation)
          RETURN Record:Filtered
      END
  END
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW12.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = stp:Stock_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tagStockType = ''
    ELSE
      tagStockType = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagStockType = '*')
    SELF.Q.tagStockType_Icon = 2
  ELSE
    SELF.Q.tagStockType_Icon = 1
  END


BRW12.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW12::RecordStatus=ReturnValue
  IF BRW12::RecordStatus NOT=Record:OK THEN RETURN BRW12::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = stp:Stock_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW12::RecordStatus
  RETURN ReturnValue

Wizard11.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW8.Q &= NULL) ! Has Browse Object been initialized?
       BRW8.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW12.Q &= NULL) ! Has Browse Object been initialized?
       BRW12.ResetQueue(Reset:Queue)
    END

Wizard11.TakeBackEmbed PROCEDURE
   CODE

Wizard11.TakeNextEmbed PROCEDURE
   CODE

Wizard11.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
ExchangeLoanDeviceSoldReport PROCEDURE (BYTE fLoan)   !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tagLocation          STRING(1)
locTrue              BYTE(1)
locYES               STRING('YES')
locReportType        BYTE
locOneLocation       STRING(30)
locStartDate         DATE
locEndDate           DATE
locRecordCount       LONG
locTotalRecords      LONG
locTotalPrice        REAL
qSummary             QUEUE,PRE()
sumInvoiceNumber     LONG
sumLocation          STRING(30)
sumManufacturer      STRING(30)
sumModelNumber       STRING(30)
sumQuantity          LONG
sumDefaultPrice      REAL
sumPrice             REAL
                     END
BRW8::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                       PROJECT(loc:Active)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tagLocation            LIKE(tagLocation)              !List box control field - type derived from local data
tagLocation_Icon       LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
loc:Active             LIKE(loc:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(850,5,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Exchange Device Sold Report'),AT(168,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(666,71),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),BELOW,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           BUTTON,AT(392,84),USE(?lookupStartDate),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Start Date'),AT(256,88),USE(?locStartDate:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d17),AT(324,88,64,10),USE(locStartDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(392,104),USE(?lookupEndDate),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(256,108),USE(?locEndDate:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d17),AT(324,110,64,10),USE(locEndDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Select Location'),AT(184,126),USE(?Prompt3),TRN,FONT(,,,FONT:bold)
                           LIST,AT(248,126,188,174),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(180,182,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(188,206,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(440,136),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(440,166),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(440,196),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           OPTION('Report Type'),AT(257,302,167,25),USE(locReportType),BOXED,TRN,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                             RADIO('Detailed'),AT(271,313),USE(?locReportType:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                             RADIO('Summary'),AT(352,313,47,10),USE(?locReportType:Radio1:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                           END
                         END
                       END
                       PANEL,AT(164,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?Export),TRN,FLAT,ICON('exportp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
EE MyExportClass

    MAP
LoadSBOnlineCriteria    PROCEDURE(),LONG
    END ! MAP
RepQ RepQClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = loc:Location
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
    tagLocation = '*'
  ELSE
    DELETE(glo:Queue)
    tagLocation = ''
  END
    Queue:Browse.tagLocation = tagLocation
  IF (tagLocation = '*')
    Queue:Browse.tagLocation_Icon = 2
  ELSE
    Queue:Browse.tagLocation_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = loc:Location
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = loc:Location
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
AddRecord       ROUTINE
DATA
locOrderDate        DATE()
locOrderNumber      LONG()
locInvoiceNumber    LONG()
locPrice            REAL
CODE

    IF (fLoan)
        Access:LOANHIST.Clearkey(loh:Ref_Number_Key)
        loh:Ref_Number = loa:Ref_Number
        loh:Date        = locEndDate            !tb13319 - J - 04/07/14 was TODAY()
        SET(loh:Ref_Number_Key,loh:Ref_Number_Key)
        LOOP UNTIL Access:LOANHIST.Next()
            IF (loh:Ref_Number <> loa:Ref_Number)
                BREAK
            END

            IF (loh:Status = 'IN TRANSIT' AND locOrderNumber = 0)
                notesLength# = len(clip(loh:notes))  !TB13319 - J 04/07/14 - needed to check limits
                if NotesLength# > 65 then           !TB13319 - J 04/17014 - should be 70 if it contains all the details expected

                    locOrderDate = loh:Date
                    invStart# = INSTRING('RETAIL INVOICE NUMBER',loh:Notes,1,1) + 23
                    priceStart# = INSTRING('PRICE',loh:Notes,1,1)

                    !TB13319 - J 04/07/14 - replace sub with string slicing to speed things up
    !                locOrderNumber = CLIP(SUB(loh:Notes,15,invStart# - 2))
    !                locInvoiceNumber = SUB(loh:Notes,invStart#,priceStart# - invStart# - 2)
    !                locPrice = DEFORMAT(SUB(loh:Notes,priceStart# + 8,30))

                    locOrderNumber = loh:Notes[ 15 : invStart# - 2 ]
                    locInvoiceNumber = loh:Notes[ invStart# : priceStart# - invStart# - 2 ]
                    locPrice = DEFORMAT(loh:Notes[ priceStart# + 8 : NotesLength# ])
                !TB13319 - J 04/07/14 - end replace subs
                END !if noteslength# > 65

                if locOrderNumber > 0 then break.       !TB13319 - J 04/07/14 - this kept looping until out of loanhist
            END
        END

        IF (locOrderDate < locStartDate)                !TB13319 second check not needed, this is where we start -  OR locOrderDate > locEndDate)
            EXIT
        END

!J - 03/07/14 - What is this doing in the middle of a report - setting all orders to received?
!but ret:Ref_number is not defined - restock is not even opened
!        Access:RETSTOCK.Clearkey(res:LoanRefNumberKey)
!        res:Ref_Number = ret:Ref_Number
!        res:ExchangeRefNumber = loa:Ref_Number
!        IF (Access:RETSTOCK.Tryfetch(res:LoanRefNumberKey) = Level:Benign)
!            res:Received = 1
!            res:DateReceived = TODAY()
!            IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
!
!            END !IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
!        END ! IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)


        IF (locReportType <> 1)
            ! Detail Selected

            ! Invoice Number
            EE.AddField(locInvoiceNumber,1)
            ! Original Order Number
            EE.AddField(locOrderNumber)
            ! Date Franchise Placed The Order
            Access:LOANORNO.Clearkey(lno:LocationRecordKey)
            lno:Location = glo:Pointer
            lno:RecordNumber = locOrderNumber
            IF (Access:LOANORNO.TryFetch(lno:LocationRecordKey) = Level:Benign)
                EE.AddField(Format(lno:DateCreated,@d06))
            ELSE ! IF (Access:EXCHORNO.TryFetch(eno:LocationRecordKey) = Level:Benign)
                EE.AddField()
            END ! IF (Access:EXCHORNO.TryFetch(eno:LocationRecordKey) = Level:Benign)
            
            ! Location
            EE.AddField(glo:Pointer)
            ! Description
            EE.AddField('''' & loa:ESN)
            ! Manufacturer
            EE.AddField(loa:Manufacturer)
            ! Model Number
            EE.AddField(loa:Model_Number)
            ! Unit Price
            EE.AddField(Format(locPrice,@n_14.2),,1)
        END! IF (locReportType = 1)

        locRecordCount += 1
        locTotalRecords += 1
        ! Add To Summary

        Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
        mod:Manufacturer = loa:Manufacturer
        mod:Model_Number = loa:Model_Number
        IF (Access:MODELNUM.Tryfetch(mod:Manufacturer_Key) = Level:Benign)
        END

        qSummary.sumInvoiceNumber = locInvoiceNumber
        qSummary.sumLocation = glo:Pointer
        qSummary.sumManufacturer = loa:Manufacturer
        qSummary.sumModelNumber = loa:Model_Number
        GET(qSummary,qSummary.sumInvoiceNumber,qSummary.sumLocation,qSummary.sumManufacturer,qSummary.sumModelNumber)
        IF (ERROR())
            qSummary.sumQuantity = 1
            qSummary.sumPrice = locPrice
            qSummary.sumDefaultPrice = mod:LoanReplacementValue  ! #12533 Use Loan Selling Value (DBH: 05/04/2012)
            ADD(qSummary)
        ELSE
            qSummary.sumQuantity += 1
            qSummary.sumPrice += locPrice
            PUT(qSummary)
        END

        locTotalPrice += locPrice

    ELSE ! IF (fLoan)

        Access:EXCHHIST.Clearkey(exh:Ref_Number_Key)
        exh:Ref_Number = xch:Ref_Number
        exh:Date        = locEndDate            !tb13319 - J - 04/07/14 was TODAY()
        SET(exh:Ref_Number_Key,exh:Ref_Number_Key)
        LOOP UNTIL Access:EXCHHIST.Next()
            IF (exh:Ref_Number <> xch:Ref_Number)
                BREAK
            END
            IF (exh:Status = 'IN TRANSIT' AND locOrderNumber = 0)
                locOrderDate = exh:Date

                notesLength# = len(clip(exh:notes))  !TB13319 - J 04/07/14 - needed to check limits
                if NotesLength# > 65 then           !TB13319 - J 04/17014 - should be 70 if it contains all the details expected
                    ! #12223 Add invoice details to "In Transit" entry. (Bryan: 29/07/2011)
                    invStart# = INSTRING('RETAIL INVOICE NUMBER',exh:Notes,1,1) + 23
                    priceStart# = INSTRING('PRICE',exh:Notes,1,1)
!TB13319 - replacing sub with string slicing to speed things up
!                    locOrderNumber = CLIP(SUB(exh:Notes,15,invStart# - 2))
!                    locInvoiceNumber = SUB(exh:Notes,invStart#,priceStart# - invStart# - 2)
!                    locPrice = DEFORMAT(SUB(exh:Notes,priceStart# + 8,30))
                    locOrderNumber = exh:Notes[ 15 : invStart# - 2]
                    locInvoiceNumber = exh:Notes[invStart# : priceStart# - invStart# - 2]
                    locPrice = DEFORMAT(exh:Notes[priceStart# + 8 : NotesLength# ])
                END     !if NotesLength# > 65

                if locOrderNumber > 0 then break.       !TB13319 - J 04/07/14 - this kept looping until out of exchhist

            END
        END

        IF locOrderDate < locStartDate
            EXIT
        END

!TB13319 - J 04/07/14 - this is doing nothing except take up time
!        Access:RETSTOCK.Clearkey(res:ExchangeRefNumberKey)
!        res:Ref_Number = ret:Ref_Number
!        res:ExchangeRefNumber = xch:Ref_Number
!        IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
!            res:Received = 1
!            res:DateReceived = TODAY()
!            IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
!
!            END !IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
!        END ! IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)


        IF (locReportType <> 1)
            ! Detail Selected

            ! Invoice Number
            EE.AddField(locInvoiceNumber,1)
            ! Original Order Number
            EE.AddField(locOrderNumber)
            ! Date Franchise Placed The Order
            Access:EXCHORNO.Clearkey(eno:LocationRecordKey)
            eno:Location = glo:Pointer
            eno:RecordNumber = locOrderNumber
            IF (Access:EXCHORNO.TryFetch(eno:LocationRecordKey) = Level:Benign)
                EE.AddField(Format(eno:DateCreated,@d06))
            ELSE ! IF (Access:EXCHORNO.TryFetch(eno:LocationRecordKey) = Level:Benign)
                EE.AddField()
            END ! IF (Access:EXCHORNO.TryFetch(eno:LocationRecordKey) = Level:Benign)
            
            ! Location
            EE.AddField(glo:Pointer)
            ! Description
            EE.AddField('''' & xch:ESN)
            ! Manufacturer
            EE.AddField(xch:Manufacturer)
            ! Model Number
            EE.AddField(xch:Model_Number)
            ! Unit Price
            EE.AddField(Format(locPrice,@n_14.2),,1)
        END! IF (locReportType = 1)

        locRecordCount += 1
        locTotalRecords += 1
        ! Add To Summary

        Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
        mod:Manufacturer = xch:Manufacturer
        mod:Model_Number = xch:Model_Number
        IF (Access:MODELNUM.Tryfetch(mod:Manufacturer_Key) = Level:Benign)
        END

        qSummary.sumInvoiceNumber = locInvoiceNumber
        qSummary.sumLocation = glo:Pointer
        qSummary.sumManufacturer = xch:Manufacturer
        qSummary.sumModelNumber = xch:Model_Number
        GET(qSummary,qSummary.sumInvoiceNumber,qSummary.sumLocation,qSummary.sumManufacturer,qSummary.sumModelNumber)
        IF (ERROR())
            qSummary.sumQuantity = 1
            qSummary.sumPrice = locPrice
            qSummary.sumDefaultPrice = mod:ExchReplaceValue
            ADD(qSummary)
        ELSE
            qSummary.sumQuantity += 1
            qSummary.sumPrice += locPrice
            PUT(qSummary)
        END

        locTotalPrice += locPrice

    END ! IF (fLoan)

CreateTitle     ROUTINE
    EE.AddField('Start Date',1)
    EE.AddField(Format(locStartDate,@d06),,1)

    EE.AddField('End Date',1)
    EE.AddField(Format(locEndDate,@d06),,1)

    EE.AddField('Location',1)

    IF (Records(glo:Queue) = 1)
        GET(glo:Queue,1)
        EE.AddField(glo:Pointer,,1)
    ELSE
        EE.AddField('Multiple',,1)
    END

    EE.AddField('Date Created',1)
    EE.AddField(Format(TODAY(),@d06),,1)

    EE.AddField('',1,1)

CreateTitleExcel        ROUTINE
    E1.WriteToCell('Start Date','A2')
    E1.WriteToCell(Format(locStartDate,@d06),'B2')
    E1.WriteToCell('End Date','A3')
    E1.WriteToCell(Format(locEndDate,@d06),'B3')

    E1.WriteToCell('Location','A4')

    IF (Records(glo:Queue) = 1)
        GET(glo:Queue,1)
        E1.WriteToCell(glo:Pointer,'B4')
    ELSE
        E1.WriteToCell('Multiple','B4')
    END

    E1.WriteToCell('Date Created','A5')
    E1.WriteToCell(Format(TODAY(),@d06),'B5')

    E1.WriteToCell('Report Type','A7')
    E1.WriteToCell('Summary','B7')

    E1.WriteToCell('Invoice Number','A9')
    E1.WriteToCell('Location','B9')
    E1.WriteToCell('Manufacturer','C9')
    E1.WriteToCell('Model Number','D9')
    E1.WriteToCell('Quantity','E9')
    E1.WriteToCell('Unit Price','F9')
    E1.WriteToCell('Total Price','G9')

CreateDetailTitle       ROUTINE
    EE.AddField('Invoice Number',1)
    EE.AddField('Original Order Number')
    EE.AddField('Date')
    EE.AddField('Location')
    EE.AddField('Description')
    EE.AddField('Manufacturer')
    EE.AddField('Model Number')
    EE.AddField('Unit Price',,1)



CreateSummaryTitle      ROUTINE
    EE.AddField('Invoice Number',1)
    EE.AddField('Location')
    EE.AddField('Manufacturer')
    EE.AddField('Model Number')
    EE.AddField('Quantity')
    EE.AddField('Unit Price')
    EE.AddField('Total Price',,1)
Export      ROUTINE
DATA
kReportFolder       CSTRING(255)
kReportName         STRING(50)
kFileName           STRING(255)
kTempFolder         CSTRING(255)
kTempFilename       CSTRING(255)
kLastDataColumn     STRING(1)
kPasteDataCell      STRING(30)
locClipBoard        ANY
CODE
    ! Set Report Title
    IF (fLoan)
        kReportName = 'Loan Device Sold Report'
    ELSE
        kReportName = 'Exchange Device Sold Report'
    END

    ! Get Temp Folder For CSV Export
    kTempFolder = GetTempFolder()

    IF (kTempFolder = '')
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('An error occurred initialising the report.'&|
            '|'&|
            '|Please ty again.','ServiceBase',|
                       'mstop.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        EXIT
    END

    ! Set Final Filename
    IF (glo:WebJob = 0)
        kFileName = clip(kReportName) & |
        ' ' & FORMAT(today(),@d12) & '.xls'
    ELSE ! IF (glo:WebJob = 1)
        IF (locReportType = 1)
            kFileName = clip(kReportName) & ' (Summary) ' & FORMAT(today(),@d12) & '.csv'
        ELSE
            kFileName = clip(kReportName) & ' (Detailed) ' & FORMAT(today(),@d12) & '.csv'
        END
    END ! IF (glo:WebJob = 1)

    kTempFilename = CLIP(kTempFolder) & 'EDSREP_' & CLOCK() & RANDOM(1,1000) & '.CSV'
    !message(kTempFilename)

    IF (EE.OpenDataFile(kTempFilename,,1))
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('An error occurred creating the report.','ServiceBase',|
                       'mstop.jpg','/&OK')
        Of 1 ! &OK Button
        End!Case Message
        RETURN
    END

    ! Start Progress Window
    EE.OpenProgressWindow(,,RepQ.SBOReport)

    EE.UpdateProgressText('')

    EE.UpdateProgressText('Report Started: ' & FORMAT(EE.StartDate,@d6) & |
        ' ' & FORMAT(EE.StartTime,@t1))
    EE.UpdateProgressText('')

    ! Add Report Title
    EE.AddField(kReportName,1,1)

    ! Add Header
    Do CreateTitle

    IF (locReportType = 0)
        ! If a detailed report, show the detail title
        EE.AddField('Report Type',1)
        EE.AddField('Detailed',,1)
        EE.AddField(,1,1)

        Do CreateDetailTitle
    END

    ! Build Temp File
    LOOP l# = 1 TO RECORDS(glo:Queue)
        GET(glo:Queue,l#)

        EE.UpdateProgressText('',1)

        IF (EE.UpdateProgressWindow())
            BREAK
        END

        EE.UpdateProgressText('Location: ' & CLIP(glo:Pointer))


        locRecordCount = 0

        IF (fLoan)
            RepQ.TotalRecords = RECORDS(LOAN)

            Access:LOAN.Clearkey(loa:LocIMEIKey)
            loa:Location = glo:Pointer
            SET(loa:LocIMEIKey,loa:LocIMEIKey)
            LOOP UNTIL Access:LOAN.NEXT()
                IF (loa:Location <> glo:Pointer)
                    BREAK
                END
                IF (EE.UpdateProgressWindow())
                    BREAK
                END

                Do AddRecord

                IF (RepQ.SBOReport)
                    IF (RepQ.UpdateProgress())
                        SBR:CancelPressed = 2
                        BREAK
                    END ! IF
                END ! IF

            END
        ELSE ! IF (fLoan)
            RepQ.TotalRecords = RECORDS(EXCHANGE)
            Access:EXCHANGE.CLearkey(xch:LocIMEIKey)
            xch:Location    = glo:Pointer
            SET(xch:LocIMEIKey,xch:LocIMEIKey)
            LOOP UNTIL Access:EXCHANGE.Next()
                IF (xch:Location <> glo:Pointer)
                    BREAK
                END

                IF (EE.UpdateProgressWindow())
                    BREAK
                END

                Do AddRecord

                IF (RepQ.SBOReport)
                    IF (RepQ.UpdateProgress())
                        SBR:CancelPressed = 2
                        BREAK
                    END ! IF
                END ! IF

            END
        END ! IF (fLoan)
        EE.UpdateProgressText('Records Found: ' & locRecordCount,1)
        IF (EE.CancelPressed > 0)
            BREAK
        END

    END ! LOOP l# = 1 TO RECORDS(glo:Queue)

    IF (locReportType = 1)
        ! RRC Should be the only one who does summary only
        EE.AddField('Report Type',1)
        EE.AddField('Summary',,1)
        EE.AddField()

        Do CreateSummaryTitle

        LOOP ll# = 1 TO RECORDS(qSummary)
            GET(qSummary,ll#)
            EE.AddField(qSummary.sumInvoiceNumber,1)
            EE.AddField(qSummary.sumLocation)
            EE.AddField(qSummary.sumManufacturer)
            EE.AddField(qSummary.sumModelNumber)
            EE.AddField(qSummary.sumQuantity)
            EE.AddField(Format(qSummary.sumDefaultPrice,@n_14.2))
            EE.AddField(Format(qSummary.sumPrice,@n_14.2),,1)
        END
        EE.AddField('Totals:',1)
        EE.AddField(RECORDS(qSummary))
        EE.AddField()
        EE.AddField()
        EE.AddField(locTotalRecords)
        EE.AddField()
        EE.AddField(locTotalPrice,,1)

    ELSE
        EE.AddField('Total Records',1)
        EE.AddField(locTotalRecords,,1)

    END

    EE.CloseDataFile()

    IF (SBR:CancelPressed = 2)
        EE.UpdateProgressText('===============')
        EE.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))
        EE.FinishProgress()
        REMOVE(kTempFilename)
    ELSE
        IF (glo:WebJob = 1)
            EE.FinishProgress()

            IF (RepQ.SBOReport = TRUE)

                kFileName = CLIP(kReportName) & ' ' & CHOOSE(locReportType = 1,'(Summary)','(Detailed)') & '.csv'

                IF (RepQ.FinishReport(kFilename,kTempFilename))
                END ! IF

                Post(Event:CloseWindow)
                RETURN
            ELSE ! IF
                REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
                RENAME(kTempFilename,CLIP(kTempFolder) & CLIP(kFileName))
                SendFileToClient(CLIP(kTempFolder) & CLIP(kFileName))
                REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
                

                Beep(Beep:SystemAsterisk)  ;  Yield()
                Case Missive('Export File Created.'&|
                    '|'&|
                    '|' & Clip(kFileName) & '','ServiceBase',|
                               'midea.jpg','/&OK') 
                Of 1 ! &OK Button
                End!Case Message
            END ! IF


        ELSE

            EE.UpdateProgressText()
            EE.UpdateProgressText('Formatting Document')
            EE.UpdateProgressText()

            ! Build Excel Document
            If E1.Init(0,0) = 0
                Case Missive('An error has occurred finding your Excel document.'&|
                  '<13,10>'&|
                  '<13,10>Please quit and try again.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Exit
            End !If E1.Init(0,0,1) = 0

            SETCLIPBOARD(EE.CopyDataFileToClipboard(E1,'H' & (locTotalRecords+15),kTempFilename))

            E1.NewWorkBook()
            E1.RenameWorkSheet('Detailed')
            E1.SelectCells('A1')
            E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'H10','H' & locTotalRecords + 10)
            E1.Paste()
            EE.ClearClipboard()

            EE.FinishFormat(E1,'H',locTotalRecords + 10,'A10')
            
            EE.UpdateProgressText('Formatting Worksheets')

            E1.InsertWorksheet()
            E1.RenameWorkSheet('Summary')
            !TB13319 - J - 21/07/14 -does not seem to be moving to the new worksheet so
            E1.SelectWorksheet('Summary')
            !end TB13319
            E1.WriteToCell(kReportName,'A1')
            Do CreateTitleExcel

            E1.SelectCells('A10')

            row# = 10
            LOOP ll# = 1 TO RECORDS(qSummary)
                GET(qSummary,ll#)
                E1.WriteToCell(qSummary.sumInvoiceNumber,'A' & row#)
                E1.WriteToCell(qSummary.sumLocation,'B' & row#)
                E1.WriteToCell(qSummary.sumManufacturer,'C' & row#)
                E1.WriteToCell(qSummary.sumModelNumber,'D' & row#)
                E1.WriteToCell(qSummary.sumQuantity,'E' & row#)
                E1.WriteToCell(Format(qSummary.sumDefaultPrice,@n_14.2),'F' & row#)
                E1.WriteToCell(Format(qSummary.sumPrice,@n_14.2),'G' & row#)
                E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'F' & row#,'G' & row#)
                row# += 1
            END
            E1.WriteToCell('Totals:','A' & row#)
            E1.WriteToCell(RECORDS(qSummary),'B' & row#)
            E1.WriteToCell(locTotalRecords,'E' & row#)
            E1.WriteToCell(locTotalPrice,'G' & row#)
            E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'F' & row#,'G' & row#)

            EE.FinishFormat(E1,'G',row#,'A9')

            REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
            E1.SaveAs(CLIP(kTempFolder) & CLIP(kFileName))
            E1.CloseWorkBook(3)
            E1.Kill()

            EE.UpdateProgressText('===============')
            EE.UpdateProgressText('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

            kReportFolder = SetReportsFolder('ServiceBase Export',CLIP(kReportName),glo:WebJob)
            IF (EE.CopyFinalFile(CLIP(kTempFolder) & Clip(kFileName),CLIP(kReportFolder) & CLIP(kFilename)))
                ! Couldn't save final file
                EE.FinishProgress()
            ELSE
                EE.FinishProgress(kReportFolder)
            END
        END
    END
    REMOVE(kTempFilename)

    Post(EVENT:CloseWindow)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ExchangeLoanDeviceSoldReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:EXCHORNO.Open
  Relate:LOANORNO.Open
  Relate:LOCATION.Open
  Relate:RETSTOCK.Open
  Access:USERS.UseFile
  Access:EXCHHIST.UseFile
  Access:MODELNUM.UseFile
  Access:LOAN.UseFile
  Access:LOANHIST.UseFile
  Access:LOAORDR.UseFile
  SELF.FilesOpened = True
  locStartDate = TODAY()
  locEndDate = TODAY()
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:LOCATION,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (RepQ.Init(COMMAND()) = Level:Benign)
      0{prop:Hide} = 1
  END ! IF
      ! Save Window Name
   AddToLog('Window','Open','ExchangeLoanDeviceSoldReport')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,loc:ActiveLocationKey)
  BRW8.AddRange(loc:Active,locTrue)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,loc:Location,1,BRW8)
  BIND('tagLocation',tagLocation)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tagLocation,BRW8.Q.tagLocation)
  BRW8.AddField(loc:Location,BRW8.Q.loc:Location)
  BRW8.AddField(loc:RecordNumber,BRW8.Q.loc:RecordNumber)
  BRW8.AddField(loc:Active,BRW8.Q.loc:Active)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  IF (glo:WebJob = 1)
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = glo:Password
      IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          glo:Pointer = use:Location
          ADD(glo:Queue)
      END ! IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
      locOneLocation = use:Location
      ?locReportType{prop:Hide} = 0
      ?List{prop:Disable} = 1
      ?lookupStartDate{prop:Hide} = 1
      ?lookupEndDate{prop:Hide} = 1
  END
  
  IF (fLoan)
      ! #12341 Combine exchange/loan report in one (DBH: 30/01/2012)
      ?WindowTitle{prop:Text} = 'Loan Device Sold Report'
  END
  
  IF (RepQ.SBOReport = TRUE)
      IF (LoadSBOnlineCriteria())
          RETURN RequestCancelled
      END ! IF
  END ! IF
  
  
  
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:EXCHANGE.Close
    Relate:EXCHORNO.Close
    Relate:LOANORNO.Close
    Relate:LOCATION.Close
    Relate:RETSTOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ExchangeLoanDeviceSoldReport')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?lookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          locStartDate = TINCALENDARStyle1(locStartDate)
          Display(?locStartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?lookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          locEndDate = TINCALENDARStyle1(locEndDate)
          Display(?locEndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Export
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
      If (RECORDS(glo:Queue) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any locations.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      Do Export
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LoadSBOnlineCriteria    PROCEDURE()!,LONG
RetValue    LONG(Level:Benign)
ReportType  CSTRING(30)
ReportKind  CSTRING(30)
qXML    QUEUE(),PRE(qXml)
ReportKind CSTRING(30)
StartDate CSTRING(30)
EndDate CSTRING(30)
    END! QUEUE
    CODE
        LOOP 1 TIMES
            glo:WebJob = 1

            IF (XML:LoadFromFile(RepQ.FullCriteriaFilename))
                RetValue = Level:Fatal
                BREAK
            END ! IF

            XML:GotoTop

            IF (~XML:FindNextNode('Criteria'))
                recs# = XML:LoadQueue(qXML,1,1)
                !XML:DebugMyQueue(qXML)
                IF (RECORDS(qXML) = 0)
                    
                    RetValue = Level:Fatal
                    BREAK
                END ! IF

                GET(qXML,1)
                CASE CLIP(qXML.ReportKind)
                OF 'D'
                    locReportType = 0
                OF 'S'
                    locReportType = 1
                END ! CASE

                locStartDate = qXML.StartDate
                locEndDate = qXML.EndDate
            END ! IF
            XML:Free()

            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = RepQ.Usercode
            Access:USERS.TryFetch(use:User_Code_Key)
            glo:Pointer = use:Location
            ADD(glo:Queue)
            locOneLocation = glo:Pointer

            RepQ.UpdateStatus('Running')

            DO Export
        END ! LOOP

        IF (RetValue = Level:Fatal)
            RepQ.Failed()
        END ! IF

        RETURN RetValue
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tagLocation = ''
    ELSE
      tagLocation = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagLocation = '*')
    SELF.Q.tagLocation_Icon = 2
  ELSE
    SELF.Q.tagLocation_Icon = 1
  END


BRW8.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  IF (locOneLocation <> '')
      IF (loc:Location <> locOneLocation)
          RETURN Record:Filtered
      END
  END
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  RETURN ReturnValue

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
ExchangePhoneReport PROCEDURE                         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::16:TAGFLAG         BYTE(0)
DASBRW::16:TAGMOUSE        BYTE(0)
DASBRW::16:TAGDISPSTATUS   BYTE(0)
DASBRW::16:QUEUE          QUEUE
Pointer3                      LIKE(glo:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tagLocation          STRING(1)
locTrue              BYTE(1)
MaxTabs              BYTE
TabNumber            BYTE
tagStockType         STRING(1)
locYES               STRING('YES')
locOneLocation       STRING(30)
locTotalValue        REAL
locTotalTotal        REAL
qSummary             QUEUE,PRE()
sumManufacturer      STRING(30)
sumModelNumber       STRING(30)
sumForP              STRING(1)
sumQuantity          LONG
                     END
tagStatus            STRING(1)
locStartDate         DATE
locEndDate           DATE
BRW8::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                       PROJECT(loc:Active)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tagLocation            LIKE(tagLocation)              !List box control field - type derived from local data
tagLocation_Icon       LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
loc:Active             LIKE(loc:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                       PROJECT(stp:Use_Exchange)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tagStockType           LIKE(tagStockType)             !List box control field - type derived from local data
tagStockType_Icon      LONG                           !Entry's icon ID
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
stp:Use_Exchange       LIKE(stp:Use_Exchange)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW15::View:Browse   VIEW(MODELPART)
                       PROJECT(model:Manufacturer)
                       PROJECT(model:ModelNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tagStatus              LIKE(tagStatus)                !List box control field - type derived from local data
tagStatus_Icon         LONG                           !Entry's icon ID
model:Manufacturer     LIKE(model:Manufacturer)       !List box control field - type derived from field
model:ModelNumber      LIKE(model:ModelNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(850,5,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Exchange Phone Report'),AT(168,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(666,71),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),BELOW,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Select Location'),AT(248,86),USE(?Prompt3),TRN,FONT(,,,FONT:bold)
                           LIST,AT(248,98,188,198),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(180,182,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(188,206,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(244,298),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,298),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(372,298),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Select Stock Type'),AT(248,88),USE(?Prompt4),TRN,FONT(,,,FONT:bold)
                           LIST,AT(248,100,188,196),USE(?List:2),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Stock Type~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(302,177,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(302,197,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(244,298),USE(?DASTAG:2),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,298),USE(?DASTAGAll:2),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(372,298,68,26),USE(?DASUNTAGALL:2),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 3'),USE(?Tab3)
                           PROMPT('Select Exchange Status'),AT(248,88),USE(?Prompt5),TRN,FONT(,,,FONT:bold)
                           LIST,AT(248,100,188,196),USE(?List:3),IMM,VSCROLL,FONT(,,0101010H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)~Exchange Status~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Rev tags'),AT(210,169,50,13),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(210,189,70,13),USE(?DASSHOWTAG:3),HIDE
                           BUTTON,AT(244,300),USE(?DASTAG:3),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,300),USE(?DASTAGAll:3),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(372,300),USE(?DASUNTAGALL:3),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 4'),USE(?Tab4)
                           PROMPT('Status Start Date'),AT(252,170),USE(?locStartDate:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d17),AT(328,170,64,10),USE(locStartDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(396,166),USE(?lookupStartdate),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Status End Date'),AT(252,202),USE(?locEndDate:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d17),AT(328,202,64,10),USE(locEndDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(396,198),USE(?lookupEndDate),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       PANEL,AT(164,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(168,332),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(232,332),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(380,332),USE(?Export),TRN,FLAT,ICON('exportp.jpg')
                     END

Wizard11         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW12                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
BRW15                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW15::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
    MAP
ConvertExchangeStatus   PROCEDURE(STRING pInString),STRING
LoadSBOnlineCriteria    PROCEDURE(),LONG
    END! MAP
RepQ    RepQClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = loc:Location
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
    tagLocation = '*'
  ELSE
    DELETE(glo:Queue)
    tagLocation = ''
  END
    Queue:Browse.tagLocation = tagLocation
  IF (tagLocation = '*')
    Queue:Browse.tagLocation_Icon = 2
  ELSE
    Queue:Browse.tagLocation_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = loc:Location
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = loc:Location
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW12.UpdateBuffer
   glo:Queue2.Pointer2 = stp:Stock_Type
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = stp:Stock_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tagStockType = '*'
  ELSE
    DELETE(glo:Queue2)
    tagStockType = ''
  END
    Queue:Browse:1.tagStockType = tagStockType
  IF (tagStockType = '*')
    Queue:Browse:1.tagStockType_Icon = 2
  ELSE
    Queue:Browse:1.tagStockType_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW12.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = stp:Stock_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW12.Reset
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::13:QUEUE = glo:Queue2
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:Queue2)
  BRW12.Reset
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer2 = stp:Stock_Type
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = stp:Stock_Type
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW12.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::16:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW15.UpdateBuffer
   glo:Queue3.Pointer3 = model:Manufacturer
   GET(glo:Queue3,glo:Queue3.Pointer3)
  IF ERRORCODE()
     glo:Queue3.Pointer3 = model:Manufacturer
     ADD(glo:Queue3,glo:Queue3.Pointer3)
    tagStatus = '*'
  ELSE
    DELETE(glo:Queue3)
    tagStatus = ''
  END
    Queue:Browse:2.tagStatus = tagStatus
  IF (tagStatus = '*')
    Queue:Browse:2.tagStatus_Icon = 2
  ELSE
    Queue:Browse:2.tagStatus_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::16:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW15.Reset
  FREE(glo:Queue3)
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue3.Pointer3 = model:Manufacturer
     ADD(glo:Queue3,glo:Queue3.Pointer3)
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::16:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue3)
  BRW15.Reset
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::16:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::16:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue3)
    GET(glo:Queue3,QR#)
    DASBRW::16:QUEUE = glo:Queue3
    ADD(DASBRW::16:QUEUE)
  END
  FREE(glo:Queue3)
  BRW15.Reset
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::16:QUEUE.Pointer3 = model:Manufacturer
     GET(DASBRW::16:QUEUE,DASBRW::16:QUEUE.Pointer3)
    IF ERRORCODE()
       glo:Queue3.Pointer3 = model:Manufacturer
       ADD(glo:Queue3,glo:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::16:DASSHOWTAG Routine
   CASE DASBRW::16:TAGDISPSTATUS
   OF 0
      DASBRW::16:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::16:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::16:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW15.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
AddRecord       ROUTINE
    ! Location
    SBR.AddToExport(glo:Pointer,1)
    ! IMEI Number
    SBR.AddToExport('''' & xch:ESN)
    ! MSN
    IF (xch:MSN <> '' AND xch:MSN <> 'N/A')
        SBR.AddToExport('''' & xch:MSN)
    ELSE
        SBR.AddToExport()
    END
    ! Manufacturer
    SBR.AddToExport(xch:Manufacturer)
    ! Model Number
    SBR.AddToExport(xch:Model_Number)
    ! Stock Type
    SBR.AddToExport(glo:Pointer2)
    ! Unit Number
    SBR.AddToExport(xch:Ref_Number)
    ! Unit Status
    SBR.AddToExport(GetExchangeStatus(xch:Available,xch:Job_Number))
    ! Status Date
    SBR.AddToExport(Format(xch:StatusChangeDate,@d06))
    ! Reason Not Available
    IF (xch:Available = 'NOA')
        Found# = 0
        Access:EXCHHIST.Clearkey(exh:Ref_Number_Key)
        exh:Ref_Number = xch:Ref_Number
        exh:Date = TODAY()
        SET(exh:Ref_Number_Key,exh:Ref_Number_Key)
        LOOP UNTIL Access:EXCHHIST.Next()
            IF (exh:Ref_Number <> xch:Ref_Number OR |
                exh:Date > TODAY())
                BREAK
            END
            Found# = 1
            SBR.AddToExport(exh:Status)
            BREAK
        END
        IF (Found# = 0)
            SBR.AddToExport()
        END
    ELSE ! IF (xch:Available = 'NOA')
        SBR.AddToExport()
    END ! IF (xch:Available = 'NOA')
    ! Job Number
    ! #12127 Double check unit is attached to job. (Bryan: 13/07/2011)
    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number = xch:Job_Number
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        IF (job:Exchange_Unit_Number = xch:Ref_Number)
            SBR.AddToExport(xch:Job_Number,,1)
        ELSE
            SBR.AddToExport(,,1)
        END
    ELSE
        SBR.AddToExport(,,1)
    END
CreateTitle     ROUTINE
    SBR.AddToExport('Location',1)

    IF (Records(glo:Queue) = 1)
        GET(glo:Queue,1)
        SBR.AddToExport(glo:Pointer,,1)
    ELSE
        SBR.AddToExport('Multiple',,1)
    END

    SBR.AddToExport('Stock Type',1)
    IF (Records(glo:Queue2) = 1)
        GET(glo:Queue2,1)
        SBR.AddToExport(glo:Pointer2,,1)
    ELSE
        SBR.AddToExport('Multiple',,1)
    END

    SBR.AddToExport('Status',1)
    IF (Records(glo:Queue3) = 1)
        GET(glo:Queue3,1)
        SBR.AddToExport(glo:Pointer3,,1)
    ELSE
        SBR.AddToExport('Multiple',,1)
    END

    SBR.AddToExport('Start Date',1)
    SBR.AddToExport(Format(locStartDate,@d06),,1)
    SBR.AddToExport('End Date',1)
    SBR.AddToExport(Format(locEndDate,@d06),,1)

    SBR.AddToExport('Date Created',1)
    SBR.AddToExport(Format(TODAY(),@d06),,1)

    SBR.AddToExport('',1,1)

CreateTitleExcel        ROUTINE
    E1.WriteToCell('Location','A2')

    IF (Records(glo:Queue) = 1)
        GET(glo:Queue,1)
        E1.WriteToCell(glo:Pointer,'B2')
    ELSE
        E1.WriteToCell('Multiple','B2')
    END

    E1.WriteToCell('Stock Type','A3')
    IF (Records(glo:Queue2) = 1)
        GET(glo:Queue2,1)
        E1.WriteToCell(glo:Pointer2,'B3')
    ELSE
        E1.WriteToCell('Multiple','B3')
    END

    E1.WriteToCell('Date Created','A4')
    E1.WriteToCell(Format(TODAY(),@d06),'B4')

    E1.WriteToCell('Report Type','A6')
    E1.WriteToCell('Summary','B6')

    E1.WriteToCell('Location','A8')
    E1.WriteToCell('Manufacturer','B8')
    E1.WriteToCell('Model Number','C8')
    E1.WriteToCell('Quantity','D8')

CreateDetailTitle       ROUTINE
    SBR.AddToExport('Location',1)
    SBR.AddToExport('I.M.E.I. Number')
    SBR.AddToExport('M.S.N.')
    SBR.AddToExport('Manufacturer')
    SBR.AddToExport('Model Number')
    SBR.AddToExport('Stock Type')
    SBR.AddToExport('Unit Number')
    SBR.AddToExport('Unit Status')
    SBR.AddToExport('Status Date')
    SBR.AddToExport('Reason Not Available')
    SBR.AddToExport('Job Number',,1)


Export      ROUTINE
DATA
kReportFolder       CSTRING(255)
kReportName         STRING(50)
kFileName           STRING(255)
kTempFolder         CSTRING(255)
kTotalRecords       LONG()
kTotalQuantity      LONG()

CODE
    kReportName = 'Exchange Phone Report'

    kTempFolder = GetTempFolder()

    IF (kTempFolder = '')
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('An error occurred creating the report.'&|
            '|'&|
            '|Please ty again.','ServiceBase',|
                       'mstop.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        EXIT
    END

    IF (glo:WebJob = 0)
        kFileName = clip(kReportName) & |
        ' ' & FORMAT(today(),@d12) & '.xls'
    ELSE ! IF (glo:WebJob = 1)
        kFileName = clip(kReportName) & ' ' & FORMAT(today(),@d12) & '.csv'
    END ! IF (glo:WebJob = 1)

    SBR:ExportFilePath = CLIP(kTempFolder) & 'EPR_' & CLOCK() & RANDOM(1,1000) & '.CSV'

    SBR.Init(RepQ.SBOReport)

    SBR.OpenProgressWindow(100)

    SBR.UpdateProgressText('')

    SBR.UpdateProgressText('Report Started: ' & FORMAT(SBR:StartDate,@d6) & |
        ' ' & FORMAT(SBR:StartTime,@t1))
    SBR.UpdateProgressText('')

    SBR.AddToExport(kReportName,1,1)

    Do CreateTitle

    Do CreateDetailTitle

    LOOP l# = 1 TO RECORDS(glo:Queue)
        GET(glo:Queue,l#)

        SBR.UpdateProgressText('',1)
        SBR.UpdateProgressText('Location: ' & CLIP(glo:Pointer))

        IF (SBR.UpdateProgressWindow())
            BREAK
        END

        RepQ.TotalRecords = RECORDS(EXCHANGE)

        count# = 0
        Access:EXCHANGE.Clearkey(xch:LocStatusChangeDateKey)
        xch:Location = glo:Pointer
        xch:StatusChangeDate = locStartDate
        SET(xch:LocStatusChangeDateKey,xch:LocStatusChangeDateKey)
        LOOP Until Access:EXCHANGE.Next()
            IF (xch:Location <> glo:Pointer OR |
                xch:StatusChangeDate > locEndDate)
                BREAK
            END

            IF (RepQ.SBOReport)
                IF (RepQ.UpdateProgress())
                    SBR:CancelPressed = 2
                END ! IF
            END ! IF

            IF (SBR.UpdateProgressWindow())
                BREAK
            END

            glo:Pointer2 = xch:Stock_Type
            GET(glo:Queue2,glo:Pointer2)
            IF (ERROR())
                CYCLE
            END

            Access:MODELPART.Clearkey(model:ModelNumberKey)
            model:ModelNumber = xch:Available
            IF (Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign)
                ! Look up the available flag in the modelpart file, see if it's been tagged
                glo:Pointer3 = model:Manufacturer
                GET(glo:Queue3,glo:Pointer3)
                IF (ERROR())
                    CYCLE
                END
            ELSE
                CYCLE
            END ! IF (Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign)

            Do AddRecord
            count# += 1
            kTotalRecords += 1

            IF (SBR:CancelPressed > 0)
                BREAK
            END
        END ! LOOP s# = 1 TO RECORDS(glo:Queue2)
        SBR.UpdateProgressText('Records Found: ' & count#,1)

        IF (SBR:CancelPressed > 0)
            BREAK
        END
    END ! LOOP l# = 1 TO RECORDS(glo:Queue)

    SBR.AddToExport('Totals',1)
    SBR.AddToExport(kTotalRecords,,1)
    SBR.Kill()

    IF (SBR:CancelPressed = 2)
        SBR.UpdateProgressText('===============')
        SBR.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))
        SBR.EndProgress()
        REMOVE(SBR:ExportFile)
    ELSE
        IF (glo:WebJob = 1)
            IF (RepQ.SBOReport = TRUE)
                SBR.EndProgress()
                kFileName = CLIP(kReportName) & '.csv'

                IF (RepQ.FinishReport(kFilename,SBR:ExportFilePath))
                END ! IF
                REMOVE(SBR:ExportFilePath)
                POST(Event:CLoseWindow)
                EXIT
            ELSE ! IF


                REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
                RENAME(SBR:ExportFilePath,CLIP(kTempFolder) & CLIP(kFileName))
                SendFileToClient(CLIP(kTempFolder) & CLIP(kFileName))
                REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
                SBR.EndProgress()

                Beep(Beep:SystemAsterisk)  ;  Yield()
                Case Missive('Export File Created.'&|
                    '|'&|
                    '|' & Clip(kFileName) & '','ServiceBase',|
                               'midea.jpg','/&OK') 
                Of 1 ! &OK Button
                End!Case Message
            END ! IF
        ELSE
            SBR.UpdateProgressText()
            SBR.UpdateProgressText('Formatting Document')
            SBR.UpdateProgressText()
            ! Build Excel Document
            If E1.Init(0,0) = 0
                Case Missive('An error has occurred finding your Excel document.'&|
                  '<13,10>'&|
                  '<13,10>Please quit and try again.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Exit
            End !If E1.Init(0,0,1) = 0

            SETCLIPBOARD(SBR.LoadTempData(E1,SBR:ExportFilePath,'K' & (kTotalRecords+15)))

            E1.NewWorkBook()
            E1.RenameWorkSheet('Detailed')
            E1.SelectCells('A1')
            E1.Paste()
            SBR.ClearClipboard()

            SBR.FinishFormat(E1,'K',kTotalRecords + 10,'A10')

            REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
            E1.SaveAs(CLIP(kTempFolder) & CLIP(kFileName))
            E1.CloseWorkBook(2)
            E1.Kill()

            SBR.UpdateProgressText('===============')
            SBR.UpdateProgressText('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

            kReportFolder = SetReportsFolder('ServiceBase Export',CLIP(kReportName),glo:WebJob)

            IF (SBR.CopyFinalFile(CLIP(kTempFolder) & Clip(kFileName),CLIP(kReportFolder) & CLIP(kFilename)))
                ! Couldn't save final file
                SBR.EndProgress()
            ELSE
                SBR.EndProgress(kReportFolder)
            END
        END
    END
    REMOVE(SBR:ExportFilePath)

    Post(EVENT:CloseWindow)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ExchangePhoneReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  ! Set temp file
  glo:FileName = GetTempFolder() & 'EPR' & CLOCK() & RANDOM(1,1000) & '.TMP'
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Relate:MODELPART.Open
  Access:USERS.UseFile
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
      ! Fill Status Label
      LOOP ll# = 1 TO 18
          Access:MODELPART.PrimeRecord()
          CASE ll#
          OF 1
              model:ModelNumber = 'AVL'
          OF 2
              model:ModelNumber = 'EXC'
          OF 3
              model:ModelNumber = 'INC'
          OF 4
              model:ModelNumber = 'FAU'
          OF 5
              model:ModelNumber = 'NOA'
          OF 6
              model:ModelNumber = 'REP'
          OF 7
              model:ModelNumber = 'SUS'
          OF 8
              model:ModelNumber = 'DES'
          OF 9
              model:ModelNumber = 'QA1'
          OF 10
              model:ModelNumber = 'QA2'
          OF 11
              model:ModelNumber = 'QAF'
          OF 12
              model:ModelNumber = 'RTS'
          OF 13
              model:ModelNumber = 'INR' 
          OF 14
              model:ModelNumber = 'ESC'
          OF 15
              model:ModelNumber = 'IT4'
          OF 16
              model:ModelNumber = 'ITM'
          OF 17
              model:ModelNumber = 'ITR'
          OF 18
              model:ModelNumber = 'ITP'
          END ! CASE
          model:Manufacturer = ConvertExchangeStatus(model:ModelNumber)
          Access:MODELPART.TryInsert()
      END ! LOOP
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:LOCATION,SELF)
  BRW12.Init(?List:2,Queue:Browse:1.ViewPosition,BRW12::View:Browse,Queue:Browse:1,Relate:STOCKTYP,SELF)
  BRW15.Init(?List:3,Queue:Browse:2.ViewPosition,BRW15::View:Browse,Queue:Browse:2,Relate:MODELPART,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (RepQ.Init(COMMAND()) = Level:Benign)
     0{prop:Hide} = 1
  END!  IF
      ! Save Window Name
   AddToLog('Window','Open','ExchangePhoneReport')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard11.Init(?Sheet1, |                          ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Export, |                       ! OK button
                     ?Cancel, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?locStartDate{Prop:Alrt,255} = MouseLeft2
  ?locEndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,loc:ActiveLocationKey)
  BRW8.AddRange(loc:Active,locTrue)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,loc:Location,1,BRW8)
  BIND('tagLocation',tagLocation)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tagLocation,BRW8.Q.tagLocation)
  BRW8.AddField(loc:Location,BRW8.Q.loc:Location)
  BRW8.AddField(loc:RecordNumber,BRW8.Q.loc:RecordNumber)
  BRW8.AddField(loc:Active,BRW8.Q.loc:Active)
  BRW12.Q &= Queue:Browse:1
  BRW12.RetainRow = 0
  BRW12.AddSortOrder(,stp:Use_Exchange_Key)
  BRW12.AddRange(stp:Use_Exchange,locYES)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,stp:Stock_Type,1,BRW12)
  BIND('tagStockType',tagStockType)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW12.AddField(tagStockType,BRW12.Q.tagStockType)
  BRW12.AddField(stp:Stock_Type,BRW12.Q.stp:Stock_Type)
  BRW12.AddField(stp:Use_Exchange,BRW12.Q.stp:Use_Exchange)
  BRW15.Q &= Queue:Browse:2
  BRW15.RetainRow = 0
  BRW15.AddSortOrder(,model:ModelNumberKey)
  BRW15.AddLocator(BRW15::Sort0:Locator)
  BRW15::Sort0:Locator.Init(,model:ModelNumber,1,BRW15)
  BIND('tagStatus',tagStatus)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW15.AddField(tagStatus,BRW15.Q.tagStatus)
  BRW15.AddField(model:Manufacturer,BRW15.Q.model:Manufacturer)
  BRW15.AddField(model:ModelNumber,BRW15.Q.model:ModelNumber)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  BRW12.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW12.AskProcedure = 0
      CLEAR(BRW12.AskProcedure, 1)
    END
  END
  BRW15.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW15.AskProcedure = 0
      CLEAR(BRW15.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:3{Prop:Alrt,239} = SpaceKey
  IF (glo:WebJob = 1)
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = glo:Password
      IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          glo:Pointer = use:Location
          ADD(glo:Queue)
      END ! IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
      locOneLocation = use:Location
      ?List{prop:Disable} = 1
      ?lookupStartDate{prop:Hide} = 1
      ?lookupEnddate{prop:Hide} = 1
      ?Tab1{prop:Hide} = 1
      POST(EVENT:Accepted,?VSNextButton)
  
  END
  
  locStartDate = TODAY()
  locEndDate = TODAY()
  
  IF (RepQ.SBOReport = TRUE)
      IF (LoadSBOnlineCriteria())
          RETURN RequestCancelled
      END ! IF
  END ! IF
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:MODELPART.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ExchangePhoneReport')
  REMOVE(glo:FileName)
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard11.Validate()
        DISABLE(Wizard11.NextControl())
     ELSE
        ENABLE(Wizard11.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?lookupStartdate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          locStartDate = TINCALENDARStyle1(locStartDate)
          Display(?locStartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?lookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          locEndDate = TINCALENDARStyle1(locEndDate)
          Display(?locEndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?VSBackButton
      ThisWindow.Update
         Wizard11.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard11.TakeAccepted()
    OF ?Export
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
      If (RECORDS(glo:Queue) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any locations.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      IF (RECORDS(glo:Queue2) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any stock types.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      IF (RECORDS(glo:Queue3) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any statuses.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      Do Export
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:3
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?locStartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?lookupStartdate)
      CYCLE
    END
  OF ?locEndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?lookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::16:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:3)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
ConvertExchangeStatus   PROCEDURE(STRING pInString)!,STRING
RetValue STRING(60)
    CODE
        CASE pInString
        OF 'AVL'
            RetValue = 'AVAILABLE'
        OF 'EXC'
            RetValue = 'EXCHANGED'
        OF 'INC'
            RetValue = 'INCOMING TRANSIT'
        OF 'FAU'
            RetValue = 'FAULTY'
        OF 'NOA'
            RetValue = 'NOT AVAILABLE'
        OF 'REP'
            RetValue = 'IN REPAIR'
        OF 'SUS'
            RetValue = 'SUSPENDED'
        OF 'DES'
            RetValue = 'DESPATCHED'
        OF 'QA1'
            RetValue = 'ELECTRONIC QA REQUIRED'
        OF 'QA2'
            RetValue = 'MANUAL QA REQUIRED'
        OF 'QAF'
            RetValue = 'QA FAILED'
        OF 'RTS'
            RetValue = 'RETURN TO STOCK'
        OF 'INR'
            RetValue = 'EXCHANGE REPAIR'
        OF 'ESC'
            RetValue = 'EXCHANGE SCRAP CONFIRMED'
        OF 'IT4'
            RetValue = 'IN TRANSIT TO 48HR STORES'
        OF 'ITM'
            RetValue = 'IN TRANSIT FROM MAIN STORE'
        OF 'ITR'
            RetValue = 'IN TRANSIT TO SCRAPPING'
        OF 'ITP'
            RetValue = 'PHANTOM IN TRANSIT'
        END ! CASE

        RETURN CLIP(RetValue)
        
LoadSBOnlineCriteria        PROCEDURE()!,LONG
RetValue                        LONG(Level:Benign)
i                               LONG
qStockTypes                     QUEUE(),PRE(qStockTypes)
StockType                           STRING(30)
                                END ! QUEUE
xmlFileName                     STRING(255)
qXML                            QUEUE(),PRE(qXml)
AllStockTypes                       CSTRING(30)
AllStatuses                         CSTRING(30)
StartDate                           CSTRING(10)
EndDate                             CSTRING(10)
                                END! QUEUE
qStatus                         QUEUE(),PRE(qStatus)
Status                              CSTRING(30)
                                END! QUEUE
    
    CODE
        LOOP 1 TIMES
            glo:WebJob = 1
            
            IF (XML:LoadFromFile(RepQ.FullCriteriaFilename))
                RetValue = Level:Fatal
                BREAK
            END ! IF
            XML:GotoTop

            IF (~XML:FindNextNode('Defaults'))
                recs# = XML:LoadQueue(qXML,1,1)
                IF (RECORDS(qXML) = 0)
                    RetValue = Level:Fatal
                    BREAK
                END ! IF
                GET(qXML, 1)
                !XML:DebugMyQueue(qXML,'qXML')
                locStartDate = qXML.StartDate
                locEndDate = qXML.EndDate

                IF (qXML.AllStockTypes = 1)
                    DO DASBRW::13:DASTAGALL
                ELSE
                    XML:GotoTop
                    IF (~XML:FindNextNode('StockTypes'))
                        recs# = XML:LoadQueue(qStockTypes,1,1)
                        !XML:DebugMyQueue(qStockTypes,'qStockTypes')
                        IF (RECORDS(qStockTypes) = 0)
                            !RetValue = Level:Fatal
                            BREAK
                        END ! IF

                        LOOP i = 1 TO RECORDS(qStockTypes)
                            GET(qStockTypes, i)
                            glo:Pointer2 = qStockTypes.StockType
                            ADD(glo:Queue2)
                        END ! LOOP
              
                    END ! IF
                END ! IF

                !STOP(qXML.AllStatuses)
                IF (qXML.AllStatuses = 1)
                    DO DASBRW::16:DASTAGALL
                ELSE 
                    XML:GotoTop
                    IF (~XML:FindNextNode('Statuses'))
                        recs# = XML:LoadQueue(qStatus,1,1)
                        !XML:DebugMyQueue(qStatus,'qStatus')
                        IF (RECORDS(qStatus) = 0)
                            !RetValue = Level:Fatal
                            BREAK
                        END ! IF

                        LOOP i = 1 TO RECORDS(qStatus)
                            GET(qStatus, i)
                            glo:Pointer3 = ConvertExchangeStatus(qStatus.Status)
                            ADD(glo:Queue3)
                        END ! LOOP
                    END ! IF               
                END ! IF

                XML:Free()
            END ! IF

            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = RepQ.UserCode
            Access:USERS.TryFetch(use:User_Code_Key)
            glo:Pointer = use:Location
            ADD(glo:Queue)
            locOneLocation = glo:Pointer

            RepQ.UpdateStatus('Running')

            DO Export

        END ! BREAK LOOP

        IF (RetValue = Level:Fatal)
            RepQ.Failed()
        END !I F

        RETURN RetValue
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tagLocation = ''
    ELSE
      tagLocation = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagLocation = '*')
    SELF.Q.tagLocation_Icon = 2
  ELSE
    SELF.Q.tagLocation_Icon = 1
  END


BRW8.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  IF (locOneLocation <> '')
      IF (loc:Location <> locOneLocation)
          RETURN Record:Filtered
      END
  END
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW12.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = stp:Stock_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tagStockType = ''
    ELSE
      tagStockType = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagStockType = '*')
    SELF.Q.tagStockType_Icon = 2
  ELSE
    SELF.Q.tagStockType_Icon = 1
  END


BRW12.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW12::RecordStatus=ReturnValue
  IF BRW12::RecordStatus NOT=Record:OK THEN RETURN BRW12::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = stp:Stock_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW12::RecordStatus
  RETURN ReturnValue


BRW15.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = model:Manufacturer
     GET(glo:Queue3,glo:Queue3.Pointer3)
    IF ERRORCODE()
      tagStatus = ''
    ELSE
      tagStatus = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagStatus = '*')
    SELF.Q.tagStatus_Icon = 2
  ELSE
    SELF.Q.tagStatus_Icon = 1
  END


BRW15.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW15.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW15.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW15::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW15::RecordStatus=ReturnValue
  IF BRW15::RecordStatus NOT=Record:OK THEN RETURN BRW15::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = model:Manufacturer
     GET(glo:Queue3,glo:Queue3.Pointer3)
    EXECUTE DASBRW::16:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW15::RecordStatus
  RETURN ReturnValue

Wizard11.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW8.Q &= NULL) ! Has Browse Object been initialized?
       BRW8.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW12.Q &= NULL) ! Has Browse Object been initialized?
       BRW12.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW15.Q &= NULL) ! Has Browse Object been initialized?
       BRW15.ResetQueue(Reset:Queue)
    END

Wizard11.TakeBackEmbed PROCEDURE
   CODE

Wizard11.TakeNextEmbed PROCEDURE
   CODE

Wizard11.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()






CreditNoteRequest PROCEDURE(LONG fCreditNoteNumber)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
locCreditNoteNumber  LONG
AddressGroup         GROUP,PRE(address)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
                     END
locLineCost          REAL
locWhoPrinted        STRING(60)
locTotalLines        LONG
locTotalLineCount    REAL
locItemCost          REAL
!-----------------------------------------------------------------------------
Process:View         VIEW(RTNORDER)
                       PROJECT(rtn:CreditNoteRequestNumber)
                       PROJECT(rtn:Description)
                       PROJECT(rtn:InvoiceNumber)
                       PROJECT(rtn:OrderNumber)
                       PROJECT(rtn:PartNumber)
                       PROJECT(rtn:QuantityReturned)
                       PROJECT(rtn:ReturnType)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2729,7521,6302),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,979,7521,1385),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,583),USE(?string27),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(5000,792),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,792),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,792),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(5885,583),USE(locWhoPrinted),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5885,375),USE(tra:StoresAccount),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@n_8),AT(5885,156),USE(rtn:CreditNoteRequestNumber),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Credit Note No:'),AT(5000,156),USE(?string27:5),TRN,FONT(,8,,)
                         STRING('Account No:'),AT(5000,365),USE(?string27:4),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(5000,1000),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,1000),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,1000),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,1000,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,177),USE(?detailband)
                           STRING(@n_8),AT(104,0),USE(rtn:QuantityReturned),TRN,RIGHT(1),FONT(,7,,)
                           STRING(@s20),AT(677,0),USE(rtn:PartNumber),TRN,LEFT(1),FONT(,7,,)
                           STRING(@s25),AT(1771,0,1615,156),USE(rtn:Description),TRN,LEFT(1),FONT(,7,,)
                           STRING(@n_8),AT(3281,0),USE(rtn:OrderNumber),TRN,RIGHT(1),FONT(,7,,)
                           STRING(@n_8),AT(3854,0),USE(rtn:InvoiceNumber),TRN,RIGHT(1),FONT(,7,,)
                           STRING(@s25),AT(4531,0),USE(rtn:ReturnType),TRN,LEFT(1),FONT(,7,,)
                           STRING(@n14.2),AT(5833,0),USE(locItemCost),TRN,RIGHT(1),FONT(,7,,)
                           STRING(@n14.2),AT(6563,0),SUM(locTotalLineCount),USE(locLineCost),TRN,RIGHT(1),FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,333),USE(?unnamed:2)
                           LINE,AT(156,52,7188,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total No Of Lines:'),AT(156,104),USE(?String40),TRN,FONT(,8,,FONT:bold)
                           STRING(@n_8),AT(1250,104),CNT,RESET(endofreportbreak),USE(locTotalLines),TRN,FONT(,8,,)
                           STRING('Total:'),AT(5938,104),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                           STRING(@n14.2),AT(6438,104),USE(locTotalLineCount),TRN,RIGHT,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(375,9073,7521,2292),USE(?unnamed:4)
                         TEXT,AT(260,52,7031,781),USE(stt:Text),TRN,FONT(,8,,)
                         BOX,AT(156,885,7240,156),USE(?Box1),COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         STRING('AUTHORISATION'),AT(3333,885),USE(?String46),TRN,FONT(,8,,FONT:bold)
                         STRING('Franchisee/Delegate'),AT(833,1042),USE(?String46:2),TRN,FONT(,8,,FONT:bold)
                         STRING('National Stores Representative'),AT(2917,1042),USE(?String46:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Logistics Manager'),AT(5781,1042),USE(?String46:4),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(156,1042,7240,208),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(156,1250,7240,260),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Name:'),AT(208,1250),USE(?String46:5),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(156,1510,7240,469),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(5156,885,0,1302),USE(?Line2:2),COLOR(COLOR:Black)
                         STRING('Name:'),AT(5208,1250),USE(?String46:7),TRN,FONT(,8,,FONT:bold)
                         STRING('Signature:'),AT(208,1771),USE(?String46:8),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(156,1979,7240,208),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Signature:'),AT(2552,1771),USE(?String46:9),TRN,FONT(,8,,FONT:bold)
                         STRING('Signature:'),AT(5208,1771),USE(?String46:10),TRN,FONT(,8,,FONT:bold)
                         STRING('Date:'),AT(208,1979),USE(?String46:11),TRN,FONT(,8,,FONT:bold)
                         STRING('Date:'),AT(2552,1979),USE(?String46:12),TRN,FONT(,8,,FONT:bold)
                         STRING('Date:'),AT(5208,1979),USE(?String46:13),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(2500,885,0,1302),USE(?Line2),COLOR(COLOR:Black)
                         STRING('Name:'),AT(2552,1250),USE(?String46:6),TRN,FONT(,8,,FONT:bold)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,4167,260),USE(address:CompanyName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('CREDIT NOTE REQUEST'),AT(4583,0,2760,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(address:AddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(address:AddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(address:AddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1042),USE(address:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354),USE(address:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Qty'),AT(375,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part No'),AT(677,2083),USE(?strPartNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(1771,2083),USE(?strDescription),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Sale No'),AT(3333,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Inv No'),AT(4010,2083),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Return Type'),AT(4531,2083),USE(?strReturnType),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost'),AT(6031,2083),USE(?string25:4),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6771,2083),USE(?string25:5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('CreditNoteRequest')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','CreditNoteRequest')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'CredNotReq'
  If PrintOption(PreviewReq,glo:ExportReport,'Credit Note Request') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('locCreditNoteNumber',locCreditNoteNumber)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:RTNORDER.Open
  Relate:STANTEXT.Open
  Relate:TRADEACC.Open
  Access:USERS.UseFile
  ! Before Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  locCreditNoteNumber = fCreditNoteNumber
  
  ! After Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  
  
  RecordsToProcess = RECORDS(RTNORDER)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(RTNORDER,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(rtn:CreditNoteNumberKey)
      Process:View{Prop:Filter} = |
      'rtn:CreditNoteRequestNumber = locCreditNoteNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        IF (rtn:ExchangeOrder = 1 OR rtn:ExchangeOrder = 2)
            locItemCost = rtn:ExchangePrice
            SETTARGET(REPORT)
            ?strPartNumber{prop:Text} = 'Model Number'
            ?strDescription{prop:Text} = 'IMEI Number'
            IF (rtn:ExchangeOrder = 2)
                ?strReturnType{prop:Hide} = 1  ! #12341 No Return Type for Loan returns (DBH: 27/01/2012)
            END
            SETTARGET()
        ELSE
            locItemCost = rtn:PurchaseCost
        END
        
        locLineCost = rtn:QuantityReturned * locItemCost
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(rpt:detail)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RTNORDER,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','CreditNoteRequest')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:RTNORDER.Close
    Relate:STANTEXT.Close
    Relate:TRADEACC.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'RTNORDER')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  Access:USERS.Clearkey(use:Password_Key)
  use:Password = glo:Password
  IF (Access:USERS.TryFetch(use:Password_Key))
  END
  
  Access:TRADEACC.Clearkey(tra:SiteLocationKey)
  tra:SiteLocation  = use:Location
  If Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign
      !Found
      address:CompanyName     = tra:Company_Name
      address:AddressLine1    = tra:Address_Line1
      address:AddressLine2    = tra:Address_Line2
      address:AddressLine3    = tra:Address_Line3
      address:Postcode        = tra:Postcode
      address:TelephoneNumber = tra:Telephone_Number
      address:FaxNumber       = tra:Fax_Number
      address:EmailAddress    = tra:EmailAddress
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  locWhoPrinted = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'CREDIT NOTE REQUEST'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Credit Note Request'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













ReturnsHandover PROCEDURE(LONG fCreditNoteNumber,qDefRec qRec)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
locCreditNoteNumber  LONG
AddressGroup         GROUP,PRE(address)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
                     END
locLineCost          REAL
locWhoPrinted        STRING(60)
locTotalLines        LONG
locTotalLineCount    REAL
locItemCost          REAL
locType              STRING(8)
locProcessedBy       STRING(60)
locReceivedBy        STRING(60)
!-----------------------------------------------------------------------------
Process:View         VIEW(RTNORDER)
                       PROJECT(rtn:Description)
                       PROJECT(rtn:PartNumber)
                       PROJECT(rtn:QuantityReturned)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2729,7521,7448),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,979,7521,1385),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(5885,365),USE(locWhoPrinted),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Department:'),AT(5000,156),USE(?string27:4),TRN,FONT(,8,,)
                         STRING('Returns Awaiting Processing'),AT(5885,156),USE(?string27:5),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,781),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,781),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,781),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,781,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,177),USE(?detailband)
                           STRING(@n_8),AT(104,0),USE(rtn:QuantityReturned),TRN,RIGHT(1),FONT(,8,,)
                           STRING(@s30),AT(2344,0),USE(rtn:PartNumber),TRN,LEFT(1),FONT(,8,,)
                           STRING(@s30),AT(4531,0,2135,156),USE(rtn:Description),TRN,LEFT(1),FONT(,8,,)
                           STRING(@s8),AT(1198,0),USE(locType),TRN,LEFT(1),FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,333),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(323,10260,7521,1052),USE(?unnamed:4)
                         LINE,AT(156,52,7188,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Processed By:'),AT(1823,156),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s60),AT(2656,156,2031,208),USE(locReceivedBy),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Received By:'),AT(4740,156),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s60),AT(5469,156,1927,208),USE(locProcessedBy),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Signature:'),AT(1823,573),USE(?String32:3),TRN,FONT(,8,,)
                         STRING('Signature:'),AT(4740,625),USE(?String32:4),TRN,FONT(,8,,)
                         LINE,AT(2604,833,4740,0),USE(?Line2),COLOR(COLOR:Black)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,4167,260),USE(address:CompanyName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('RETURNS HANDOVER'),AT(4583,0,2760,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(address:AddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(address:AddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(address:AddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1042),USE(address:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354),USE(address:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Qty'),AT(375,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Spare / Exchange'),AT(1042,2083),USE(?strPartNumber:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Part No'),AT(2344,2083),USE(?strPartNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(4531,2063),USE(?strDescription),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('ReturnsHandover')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
      ! Save Window Name
   AddToLog('Report','Open','ReturnsHandover')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'RtnHandOver'
  If PrintOption(PreviewReq,glo:ExportReport,'Returns Handover') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('locCreditNoteNumber',locCreditNoteNumber)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:RTNORDER.Open
  Relate:TRADEACC.Open
  Access:USERS.UseFile
  ! Before Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  locCreditNoteNumber = fCreditNoteNumber
  
  ! After Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  
  
  RecordsToProcess = RECORDS(RTNORDER)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(RTNORDER,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(rtn:CreditNoteNumberKey)
      Process:View{Prop:Filter} = |
      'rtn:CreditNoteRequestNumber = locCreditNoteNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      ! Before Embed Point: %AfterInitialGet) DESC(After first record retrieval) ARG()
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code = rtn:WhoReceived
      IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
          locReceivedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
      END
      
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code = rtn:WhoProcessed
      IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
          locProcessedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
      END
      ! After Embed Point: %AfterInitialGet) DESC(After first record retrieval) ARG()
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        IF (rtn:ExchangeOrder)
            SETTARGET(REPORT)
            ?strPartNumber{prop:Text} = 'Model Number'
            ?strDescription{prop:Text} = 'IMEI Number'
            SETTARGET()
            IF (rtn:ExchangeOrder = 1)
                locType = 'Exchange'
            ELSE
                locType = 'Loan'
            END
        ELSE
            locType = 'Spare'
        END
        
        ! Only Print Records Passed In The Queue
        qRec.RecordNumber = rtn:RecordNumber
        GET(qRec,qRec.RecordNumber)
        IF ERROR()
            CYCLE
        END
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(rpt:detail)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RTNORDER,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
      ! Save Window Name
   AddToLog('Report','Close','ReturnsHandover')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:RTNORDER.Close
    Relate:TRADEACC.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'RTNORDER')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  Access:USERS.Clearkey(use:Password_Key)
  use:Password = glo:Password
  IF (Access:USERS.TryFetch(use:Password_Key))
  END
  
  Access:TRADEACC.Clearkey(tra:SiteLocationKey)
  tra:SiteLocation  = use:Location
  If Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign
      !Found
      address:CompanyName     = tra:Company_Name
      address:AddressLine1    = tra:Address_Line1
      address:AddressLine2    = tra:Address_Line2
      address:AddressLine3    = tra:Address_Line3
      address:Postcode        = tra:Postcode
      address:TelephoneNumber = tra:Telephone_Number
      address:FaxNumber       = tra:Fax_Number
      address:EmailAddress    = tra:EmailAddress
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  locWhoPrinted = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Returns Handover'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













GoodsReceivedNoteReturnsProcess PROCEDURE(LONG fGRNNumber)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:RecordsCount     LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
first_page_temp      BYTE(1)
pos                  STRING(255)
Part_Queue           QUEUE,PRE()
Order_Number_Temp    REAL
Part_Number_Temp     STRING(30)
Description_Temp     STRING(30)
Quantity_Temp        LONG
Purchase_cost_temp   REAL
Sale_Cost_temp       REAL
                     END
Order_Temp           REAL
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Total_Quantity_Temp  LONG
total_cost_temp      REAL
total_cost_total_temp REAL
Total_Lines_Temp     REAL
user_name_temp       STRING(22)
no_temp              STRING('NO')
tmp:Order_No_Filter  REAL
tmp:Date_Received_Filter DATE
locShelfLocation     STRING(20)
locGRNNumber         LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(RTNAWAIT)
                       PROJECT(rta:CNRRecordNumber)
                       PROJECT(rta:DateCreated)
                       PROJECT(rta:Description)
                       PROJECT(rta:GRNNumber)
                       PROJECT(rta:PartNumber)
                       PROJECT(rta:Quantity)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Goods Received Note'),AT(396,4604,7521,4125),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,4167),USE(?unnamed)
                         STRING(@D6b),AT(5833,781),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(5104,781),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@T3),AT(6458,781),USE(ReportRunTime),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,323),USE(def:OrderAddressLine1),TRN
                         STRING(@s30),AT(156,104),USE(def:OrderCompanyName),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,635),USE(def:OrderAddressLine3),TRN
                         STRING(@s30),AT(156,802),USE(def:OrderPostcode),TRN
                         STRING('Tel: '),AT(156,958),USE(?String15),TRN
                         STRING('Fax:'),AT(156,1094),USE(?String16),TRN
                         STRING(@s30),AT(573,1094),USE(def:OrderFaxNumber),TRN
                         STRING('Note No:'),AT(5104,573),USE(?RunPrompt:2),TRN,FONT(,8,,)
                         STRING('Email:'),AT(156,1250,521,156),USE(?String16:2),TRN
                         STRING(@s255),AT(573,1250),USE(def:OrderEmailAddress),TRN
                         STRING(@n-_10),AT(5833,573,1406,156),USE(rta:GRNNumber),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,1667),USE(tra:Company_Name),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1667),USE(def:OrderCompanyName,,?def:OrderCompanyName:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1823),USE(tra:Address_Line1),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1823),USE(def:OrderAddressLine1,,?def:OrderAddressLine1:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1979),USE(tra:Address_Line2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1979),USE(def:OrderAddressLine2,,?def:OrderAddressLine2:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2135),USE(tra:Address_Line3),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2135),USE(def:OrderAddressLine3,,?def:OrderAddressLine3:3),TRN,FONT(,8,,)
                         STRING(@s15),AT(156,2292),USE(tra:Postcode),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2292),USE(def:OrderPostcode,,?def:OrderPostcode:3),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,2448),USE(?String26),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2448),USE(tra:Telephone_Number),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(4083,2448),USE(?String26:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4396,2448,990,188),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:2),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(156,2604),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2604,990,188),USE(tra:Fax_Number),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(4083,2604),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4396,2604,990,188),USE(def:OrderFaxNumber,,?def:OrderFaxNumber:3),TRN,FONT(,8,,)
                         STRING(@s15),AT(156,3385),USE(tra:Account_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(5833,990,1406,156),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING(@d17),AT(3083,3385),USE(rta:DateCreated),TRN,FONT(,8,,)
                         STRING(@n-14),AT(1875,3385),USE(rta:CNRRecordNumber),TRN,FONT(,8,,)
                         STRING('GOODS RETURNED NOTE'),AT(3906,104,3542,313),USE(?strTitle),TRN,RIGHT,FONT(,13,,FONT:bold)
                         STRING(@s30),AT(573,958),USE(def:OrderTelephoneNumber),TRN
                         STRING('Printed by'),AT(5104,990),USE(?String67),TRN,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s30),AT(156,479),USE(def:OrderAddressLine2),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@n_8),AT(125,0),USE(rta:Quantity),TRN,RIGHT,FONT(,7,,)
                           STRING(@s30),AT(1833,0),USE(rta:PartNumber),TRN,FONT(,7,,)
                           STRING(@s30),AT(3333,0),USE(rta:Description),TRN,FONT(,7,,)
                           STRING(@n_8),AT(833,0),USE(rta:Quantity,,?rta:Quantity:2),TRN,RIGHT,FONT(,7,,)
                           STRING(@s20),AT(5052,0),USE(locShelfLocation),TRN,LEFT,FONT(,7,,)
                           STRING(@n14.2),AT(6719,0),USE(total_cost_temp),TRN,FONT(,7,,,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(396,9396),USE(?Totals),ABSOLUTE
                           STRING('Total Items: '),AT(396,83),USE(?String47),TRN,FONT(,10,,FONT:bold)
                           STRING('Total Lines: '),AT(396,323),USE(?String40),TRN,FONT(,,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,323),USE(Total_Lines_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING(@n14.2),AT(6156,156),USE(total_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,83),USE(Total_Quantity_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING('Total GRN Value:'),AT(4844,156),USE(?String41),TRN,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1125),USE(?unnamed:3)
                         TEXT,AT(104,521,7344,521),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,10802),USE(?unnamed:2)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('Credit Note Request No'),AT(1604,3177),USE(?strCreditNoteRequestNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Date'),AT(3083,3177),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(125,3177),USE(?String30),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1458),USE(?String42),TRN,FONT(,9,,FONT:bold)
                         GROUP,AT(104,3698,7344,500),USE(?groupTitle1)
                           STRING('Qty Ordered'),AT(156,3854),USE(?strQtyOrdered),TRN,FONT(,8,,FONT:bold)
                           STRING('Qty Received'),AT(990,3854),USE(?strQtyReceived),TRN,FONT(,8,,FONT:bold)
                           STRING('Part Number'),AT(1875,3854),USE(?strPartNumber),TRN,FONT(,8,,FONT:bold)
                           STRING('Description'),AT(3333,3854),USE(?strDescription),TRN,FONT(,8,,FONT:bold)
                           STRING('Shelf Location'),AT(5052,3854),USE(?String36),TRN,FONT(,8,,FONT:bold)
                           STRING('GRN Value'),AT(6719,3854),USE(?String66),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                         STRING('Received By'),AT(104,9844),USE(?String64),TRN,FONT('Arial',9,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(938,9990,2656,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('FRANCHISE ADDRESS'),AT(104,1458),USE(?String25),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('GoodsReceivedNoteReturnsProcess')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','GoodsReceivedNoteReturnsProcess')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  if 0 = 1 then 
    glo:ExportToCSV = '?'
  ELSE
    glo:ExportToCSV = ''
  END
  glo:ReportName = 'GoodsRetNote'
  If PrintOption(PreviewReq,glo:ExportReport,'Goods Returns Note') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('locGRNNumber',locGRNNumber)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  ! Before Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  locGRNNumber = fGRNNumber
  ! After Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  FileOpensReached = True
  FilesOpened = True
  Relate:RTNAWAIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:LOAN.Open
  Relate:RTNORDER.Open
  Relate:STANTEXT.Open
  Relate:TRADEACC.Open
  Access:USERS.UseFile
  Access:GRNOTESP.UseFile
  ! Before Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  Access:USERS.Clearkey(use:Password_Key)
  use:Password = glo:Password
  Access:USERS.TryFetch(use:Password_Key)
  tmp:PrintedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  ! After Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  
  
  RecordsToProcess = RECORDS(RTNAWAIT)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(RTNAWAIT,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(rta:GRNNumberKey)
      Process:View{Prop:Filter} = |
      'rta:GRNNumber = locGRNNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      ! Before Embed Point: %AfterInitialGet) DESC(After first record retrieval) ARG()
      ! Lookup the Franchise details from the location of the item
      Access:TRADEACC.Clearkey(tra:SiteLocationKey)
      tra:SiteLocation = rta:Location
      IF (Access:TRADEACC.TryFetch(tra:SiteLocationKey))
      END
      ! After Embed Point: %AfterInitialGet) DESC(After first record retrieval) ARG()
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        Access:RTNORDER.Clearkey(rtn:RecordNumberKey)
        rtn:RecordNumber = rta:RTNRecordNumber
        IF (Access:RTNORDER.TryFetch(rtn:RecordNumberKey) = Level:Benign)
        END !IF (Access:RTNORDER.TryFetch(rtn:RecordNumberKey) = Level:Benig)
        
        IF (rta:ExchangeOrder = 1)
            SETTARGET(REPORT)
            ?strPartNumber{prop:Text} = 'Model Number'
            ?strDescription{prop:Text} = 'I.M.E.I. Number'
            SETTARGET()
        
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number = rta:RefNumber
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
            END
            locShelfLocation = xch:Shelf_Location
            total_cost_temp = rtn:ExchangePrice
            total_cost_total_temp += total_cost_temp
        ELSIF (rta:ExchangeOrder = 2)
            SETTARGET(REPORT)
            ?strPartNumber{prop:Text} = 'Model Number'
            ?strDescription{prop:Text} = 'I.M.E.I. Number'
            SETTARGET()
        
            Access:LOAN.Clearkey(loa:Ref_Number_Key)
            loa:Ref_Number = rta:RefNumber
            IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
            END
            locShelfLocation = loa:Shelf_Location
            total_cost_temp = rtn:ExchangePrice
            total_cost_total_temp += total_cost_temp
        ELSE
            ! Lookup the shelf location from the new part, i.e. Main Store or other (if rejected)
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number = rta:NewRefNumber
            IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
            END
            locShelfLocation = sto:Shelf_Location
            total_cost_temp = rta:Quantity * rtn:PurchaseCost
            total_cost_total_temp += total_cost_temp
        
        END
        
        IF (rta:AcceptReject = 'REJECT')
            SETTARGET(REPORT)
            ?strTitle{prop:Text} = 'GOODS RETURNED REJECTION NOTE'
            ?strCreditNoteRequestNumber{prop:Hide} = 1
            ?rta:CNRRecordNumber{prop:Hide} = 1
            SETTARGET()
        END
        
        
        
        total_quantity_temp += rta:Quantity
        Total_Lines_Temp += 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(RPT:DETAIL)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RTNAWAIT,'QUICKSCAN=off').
  ! Before Embed Point: %EndOfReportGeneration) DESC(End of Report Generation) ARG()
  Print(rpt:Totals)
  ! After Embed Point: %EndOfReportGeneration) DESC(End of Report Generation) ARG()
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(Report)
      ! Save Window Name
   AddToLog('Report','Close','GoodsReceivedNoteReturnsProcess')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:GRNOTESP.Close
    Relate:LOAN.Close
    Relate:RTNORDER.Close
    Relate:STANTEXT.Close
    Relate:TRADEACC.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'RTNAWAIT')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 2
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  Set(Defaults)
  access:Defaults.next()
  
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'PARTS ORDER'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='GoodsReceivedNoteReturnsProcess'
  END
  Report{Prop:Preview} = PrintPreviewImage







ExchangeLoanUnitsReturnedToMainStore PROCEDURE (BYTE fLoan) !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tagLocation          STRING(1)
locTrue              BYTE(1)
locYES               STRING('YES')
locReportType        BYTE
locOneLocation       STRING(30)
locStartDate         DATE
locEndDate           DATE
locRecordCount       LONG
locTotalRecords      LONG
locReportName        STRING(100)
locTempFolder        CSTRING(255)
locExportFile0       STRING(255)
locExportFile1       STRING(255)
BRW8::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                       PROJECT(loc:Active)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tagLocation            LIKE(tagLocation)              !List box control field - type derived from local data
tagLocation_Icon       LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
loc:Active             LIKE(loc:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(850,5,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Exchange Units Returned To Main Store'),AT(168,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(666,71),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),BELOW,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           BUTTON,AT(392,84),USE(?lookupStartDate),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Start Date'),AT(256,88),USE(?locStartDate:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d17),AT(324,88,64,10),USE(locStartDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(392,104),USE(?lookupEndDate),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(256,108),USE(?locEndDate:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d17),AT(324,110,64,10),USE(locEndDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Select Location'),AT(184,126),USE(?Prompt3),TRN,FONT(,,,FONT:bold)
                           LIST,AT(248,126,188,174),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(180,182,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(188,206,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(440,136),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(440,166),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(440,196),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           OPTION('Report Type'),AT(257,302,167,25),USE(locReportType),BOXED,TRN,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                             RADIO('In Transit'),AT(271,313),USE(?locReportType:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                             RADIO('Received'),AT(352,313,47,10),USE(?locReportType:Radio1:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                           END
                         END
                       END
                       PANEL,AT(164,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?Export),TRN,FLAT,ICON('exportp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
CreateExportFile        PROCEDURE(BYTE fReportType)
LoadSBOnlineCriteria    PROCEDURE(),LONG
    END
RepQ    RepQClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = loc:Location
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
    tagLocation = '*'
  ELSE
    DELETE(glo:Queue)
    tagLocation = ''
  END
    Queue:Browse.tagLocation = tagLocation
  IF (tagLocation = '*')
    Queue:Browse.tagLocation_Icon = 2
  ELSE
    Queue:Browse.tagLocation_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = loc:Location
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = loc:Location
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
AddRecord       ROUTINE
DATA
locReceivedDate     DATE()
locWhoReceived      STRING(3)
locPRice            REAL()
CODE
    IF (fLoan)
        !Model Number
        SBR.AddToExport(loa:Model_Number,1)
        !Description
        SBR.AddToExport('''' & loa:ESN)
        !Location
        SBR.AddToExport(glo:Pointer)
        !Credit Note Request Number
        SBR.AddToExport(rtn:CreditNoteRequestNumber)
        !Invoice Number
        SBR.AddToExport(rtn:InvoiceNumber)
        !Original Order Number
        SBR.AddToExport(rtn:OrderNumber)
        ! Received Date
        ! Find original exchange history item based on the invoice number
        Access:LOANHIST.Clearkey(loh:Ref_Number_Key)
        loh:Ref_Number = loa:Ref_Number
        loh:Date = TODAY()
        SET(loh:Ref_Number_Key,loh:Ref_Number_Key)
        LOOP UNTIL Access:LOANHIST.Next()
            IF (loh:Ref_Number <> loa:ref_Number)
                BREAK
            END
            IF (loh:Date > TODAY())
                BREAK
            END
            IF (loh:Status = 'UNIT RECEIVED')
                IF (INSTRING('RETAIL INVOICE NUMBER: ' & CLIP(rtn:InvoiceNumber),UPPER(loh:Notes),1,1))
                    locReceivedDate = loh:Date
                    locWhoReceived = loh:User
                    BREAK
                END
            END
        END

        SBR.AddToExport(Format(locReceivedDate,@d06b))
        ! Received By
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code = locWhoReceived
        IF (Access:USERS.Tryfetch(use:User_Code_Key))
            SBR.AddToExport('')
        ELSE
            SBR.AddToExport(CLip(use:Forename) & ' ' & Clip(use:Surname))
        END
        !Manufacturer
        SBR.AddToExport(loa:Manufacturer)
        !Model
        SBR.AddToExport(loa:Model_Number)
        !Price
        Access:LOANHIST.Clearkey(loh:Ref_Number_Key)
        loh:Ref_Number = loa:Ref_Number
        loh:Date        = TODAY()
        SET(loh:Ref_Number_Key,loh:Ref_Number_Key)
        LOOP UNTIL Access:LOANHIST.Next()
            IF (loh:Ref_Number <> loa:Ref_Number)
                BREAK
            END
            IF (loh:Status = 'IN TRANSIT')
                invStart# = INSTRING('RETAIL INVOICE NUMBER',loh:Notes,1,1) + 23
                priceStart# = INSTRING('PRICE',loh:Notes,1,1)
                locPrice = DEFORMAT(SUB(loh:Notes,priceStart# + 8,30))
            END
        END
        SBR.AddToExport(Format(locPrice,@n14.2),,1)

        locRecordCount += 1

        ! Work out the maximum number of records on each sheet
        ! Use this when cutting from the temp file
        If (locTotalRecords < locRecordCount)
            locTotalRecords = locRecordCount
        END

    ELSE ! IF (fLoan)
        ! Part Number
        Access:STOCK.Clearkey(sto:ExchangeModelKey)
        sto:Location = MainStoreLocation()
        sto:Manufacturer = xch:Manufacturer
        sto:ExchangeModelNumber = xch:Model_Number
        stockError# = Access:STOCK.TryFetch(sto:ExchangeModelKey)

        ! Part Number
        IF (stockError# = 0)
            SBR.AddToExport(sto:Part_Number,1)
        ELSE
            SBR.AddToExport(,1)
        END
        ! Description
        SBR.AddToExport('''' & xch:ESN)
        ! Location
        SBR.AddToExport(glo:Pointer)
        ! Credit Note Request No
        SBR.AddToExport(rtn:CreditNoteRequestNumber)
        ! Invoice Number
        SBR.AddToExport(rtn:InvoiceNumber)
        ! Order Number
        SBR.AddToExport(rtn:OrderNumber)
        ! Received Date
        ! Find original exchange history item based on the invoice number
        Access:EXCHHIST.Clearkey(exh:Ref_Number_Key)
        exh:Ref_Number = xch:Ref_Number
        exh:Date = TODAY()
        SET(exh:Ref_Number_Key,exh:Ref_Number_Key)
        LOOP UNTIL Access:EXCHHIST.Next()
            IF (exh:Ref_Number <> xch:ref_Number)
                BREAK
            END
            IF (exh:Date > TODAY())
                BREAK
            END
            IF (exh:Status = 'UNIT RECEIVED')
                IF (INSTRING('RETAIL INVOICE NUMBER: ' & CLIP(rtn:InvoiceNumber),UPPER(exh:Notes),1,1))
                    locReceivedDate = exh:Date
                    locWhoReceived = exh:User
                    BREAK
                END
            END
        END

        SBR.AddToExport(Format(locReceivedDate,@d06b))
        ! Received By
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code = locWhoReceived
        IF (Access:USERS.Tryfetch(use:User_Code_Key))
            SBR.AddToExport('')
        ELSE
            SBR.AddToExport(CLip(use:Forename) & ' ' & Clip(use:Surname))
        END
        ! Manufacturer
        SBR.AddToExport(xch:Manufacturer)
        IF (locReportType = 0)
            ! In Transit
            ! Model Number
            SBR.AddToExport(xch:Model_Number,,1)
        ELSE
            !Model Number
            SBR.AddToExport(xch:Model_Number)
            ! Accept/Reject
            Access:RTNAWAIT.Clearkey(rta:RTNORDERKey)
            rta:RTNRecordNumber = rtn:RecordNumber
            IF (Access:RTNAWAIT.TryFetch(rta:RTNORDERKey))
                SBR.AddToExport(,,1)
            ELSE
                SBR.AddToExport(rta:AcceptReject,,1)
            END
        END
        locRecordCount += 1

        ! Work out the maximum number of records on each sheet
        ! Use this when cutting from the temp file
        If (locTotalRecords < locRecordCount)
            locTotalRecords = locRecordCount
        END
    END !IF (FLoan)
CreateTitle     ROUTINE


CreateTitleExcel        ROUTINE
    E1.WriteToCell('Start Date','A2')
    E1.WriteToCell(Format(locStartDate,@d06),'B2')
    E1.WriteToCell('End Date','A3')
    E1.WriteToCell(Format(locEndDate,@d06),'B3')

    E1.WriteToCell('Location','A4')

    IF (Records(glo:Queue) = 1)
        GET(glo:Queue,1)
        E1.WriteToCell(glo:Pointer,'B4')
    ELSE
        E1.WriteToCell('Multiple','B4')
    END

    E1.WriteToCell('Date Created','A5')
    E1.WriteToCell(Format(TODAY(),@d06),'B5')

    E1.WriteToCell('Report Type','A7')
    IF (locReportType = 0)
        E1.WriteToCell('In Transit','B7')
    ELSE
        E1.WriteTocell('Received','B7')
    END

    IF (fLoan)
        E1.WriteToCell('Model Number','A9')
    ELSE
        E1.WriteToCell('Part Number','A9')
    END
    E1.WriteToCell('Description','B9')
    E1.WriteToCell('Location','C9')
    E1.WriteToCell('Credit Note Request Number','D9')
    E1.WriteToCell('Invoice Number','E9')
    E1.WriteToCell('Original Order Number','F9')
    E1.WriteToCell('Received Date','G9')
    E1.WriteToCell('Received By','H9')
    E1.WriteToCell('Manufacturer','I9')
    E1.WriteToCell('Model','J9')
    IF (fLoan)
        E1.WriteToCell('Price','K9')
    ELSE
        IF (locReportType = 1)
            E1.WriteToCell('Accept/Reject','K9')
        END
    END

CreateDetailTitle       ROUTINE
    IF (fLoan)
        SBR.AddToExport('Model Number',1)
    ELSE
        SBR.AddToExport('Part Number',1)
    END
    SBR.AddToExport('Description')
    SBR.AddToExport('Location')
    SBR.AddToExport('Credit Note Request No')
    SBR.AddToExport('Invoice Number')
    SBR.AddToExport('Original Order No')
    SBR.AddToExport('Received Date')
    SBR.AddToExport('Received By')
    SBR.AddToExport('Manufacturer')
    IF (fLoan)
        SBR.AddToExport('Model')
        SBR.AddToExport('Price',,1)
    ELSE
    
        IF (locReportType = 0)
            ! In Transit
            SBR.AddToExport('Model Number',,1)
        ELSE
            ! Received
            SBR.AddToExport('Model Number')
            SBR.AddToExport('Accepted / Rejected',,1)
        END
    END


CreateSummaryTitle      ROUTINE
    SBR.AddToExport('Invoice Number',1)
    SBR.AddToExport('Location')
    SBR.AddToExport('Manufacturer')
    SBR.AddToExport('Model Number')
    SBR.AddToExport('Quantity')
    SBR.AddToExport('Unit Price')
    SBR.AddToExport('Total Price',,1)
Export      ROUTINE
DATA
kReportFolder       CSTRING(255)
kFileName           STRING(255)
kLastDataColumn     STRING(1)
kPasteDataCell      STRING(30)
locClipBoard        ANY

kExportFile1        STRING(255)
kExportFile2        STRING(255)
CODE
    ! Set Report Title
    IF (fLoan)
        locReportName = 'Loan Units Returned To Main Store'
    ELSE
        locReportName = 'Exchange Units Returned To Main Store'
    END

    ! Get Temp Folder For CSV Export
    locTempFolder = GetTempFolder()

    IF (locTempFolder = '')
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('An error occurred creating the report.'&|
            '|'&|
            '|Please ty again.','ServiceBase',|
                       'mstop.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        EXIT
    END

    ! Set Final Filename
    IF (glo:WebJob = 0)
        kFileName = clip(locReportName) & |
        ' ' & FORMAT(today(),@d12) & '.xls'
    ELSE ! IF (glo:WebJob = 1)
        IF (fLoan)
            IF (locReportType = 1)
                kFileName = 'LoanReturnsReceived(' & FORMAT(today(),@d12) & ').csv'
            ELSE
                kFileName = 'LoanReturnsInTransit(' & FORMAT(today(),@d12) & ').csv'
            END
        ELSE
            IF (locReportType = 1)
                kFileName = 'ExchReturnsReceived(' & FORMAT(today(),@d12) & ').csv'
            ELSE
                kFileName = 'ExchReturnsInTransit(' & FORMAT(today(),@d12) & ').csv'
            END
        END
    END ! IF (glo:WebJob = 1)

    SBR.OpenProgressWindow(100)

    SBR.UpdateProgressText('')

    SBR.UpdateProgressText('Report Started: ' & FORMAT(SBR:StartDate,@d6) & |
        ' ' & FORMAT(SBR:StartTime,@t1))
    SBR.UpdateProgressText('')

    ! Add Report Title
    

    IF (glo:WebJob = 0)
        ! If Main Client, build report for received and in transit
        SBR.UpdateProgressText('In Transit....')
        SBR.UpdateProgressText('')
        CreateExportFile(0)
        SBR.UpdateProgressText('Received.....')
        SBR.UpdateProgressText('')
        CreateExportFile(1)
    ELSE
        ! If RRC build report based on selection
        CreateExportFile(locReportType)
    END


    IF (SBR:CancelPressed = 2)
        SBR.UpdateProgressText('===============')
        SBR.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
            ' ' & FORMAT(clock(),@t1))
        SBR.EndProgress()
        REMOVE(SBR:ExportFile)
    ELSE
        IF (glo:WebJob = 1)
            IF (RepQ.SBOReport = TRUE)
                SBR.EndProgress()
                kFileName = CHOOSE(fLoan = 1,'Loan','Exchange') & ' Units Returned To Main Store (' & |
                    CHOOSE(locReportType = 1,'Received','In Transit') & |
                    ').csv'

                IF (RepQ.FinishReport(kFilename,SBR:ExportFilePath))
                END ! IF
                REMOVE(SBR:ExportFilePath)
                POST(Event:CLoseWindow)
                EXIT
            ELSE ! IF
            
                REMOVE(CLIP(locTempFolder) & CLIP(kFileName))
                RENAME(SBR:ExportFilePath,CLIP(locTempFolder) & CLIP(kFileName))
                SendFileToClient(CLIP(locTempFolder) & CLIP(kFileName))
                REMOVE(CLIP(locTempFolder) & CLIP(kFileName))
                SBR.EndProgress()

                Beep(Beep:SystemAsterisk)  ;  Yield()
                Case Missive('Export File Created.'&|
                    '|'&|
                    '|' & Clip(kFileName) & '','ServiceBase',|
                               'midea.jpg','/&OK') 
                Of 1 ! &OK Button
                End!Case Message
            END ! IF
        ELSE
            SBR.UpdateProgressText()
            SBR.UpdateProgressText('Formatting Document')
            SBR.UpdateProgressText()
            ! Build Excel Document
            If E1.Init(0,0) = 0
                Case Missive('An error has occurred finding your Excel document.'&|
                  '<13,10>'&|
                  '<13,10>Please quit and try again.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Exit
            End !If E1.Init(0,0,1) = 0

            SETCLIPBOARD(SBR.LoadTempData(E1,locExportFile0,'K' & (locTotalRecords+15)))

            E1.NewWorkBook()
            ! Create In Transit Worksheet
            E1.RenameWorkSheet('In Transit')
            E1.SelectCells('A1')
            
            !E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'K10','K' & locTotalRecords + 10)
            E1.Paste()
            SBR.ClearClipboard()

            !Do CreateTitleExcel

            SBR.FinishFormat(E1,'K',locTotalRecords + 10,'A10')

            ! Save doc, so we can copy the other data and paste it in
            REMOVE(CLIP(locTempFolder) & CLIP(kFileName))
            E1.SaveAs(CLIP(locTempFolder) & CLIP(kFileName))
            E1.CloseWorkBook(3)

            SETCLIPBOARD(SBR.LoadTempData(E1,locExportFile1,'K' & (locTotalRecords + 15)))

            E1.OpenWorkBook(CLIP(locTempFolder) & CLIP(kFileName))

            ! Create Received Worksheer
            E1.InsertWorkSheet()
            E1.RenameWorkSheet('Received')
            E1.SelectCells('A1')
            
            E1.Paste()
            SBR.ClearClipboard()

            SBR.FinishFormat(E1,'K',locTotalRecords + 10,'A10')
            

            REMOVE(CLIP(locTempFolder) & CLIP(kFileName))
            E1.Save()
            E1.CloseWorkBook(3)
            E1.Kill()

            SBR.UpdateProgressText('===============')
            SBR.UpdateProgressText('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

            kReportFolder = SetReportsFolder('ServiceBase Export',CLIP(locReportName),glo:WebJob)
            IF (SBR.CopyFinalFile(CLIP(locTempFolder) & Clip(kFileName),CLIP(kReportFolder) & CLIP(kFilename)))
                ! Couldn't save final file
                SBR.EndProgress()
            ELSE
                SBR.EndProgress(kReportFolder)
            END
        END
    END
    REMOVE(SBR:ExportFilePath)

    Post(EVENT:CloseWindow)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020766'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ExchangeLoanUnitsReturnedToMainStore')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:EXCHORNO.Open
  Relate:LOCATION.Open
  Relate:RTNAWAIT.Open
  Relate:RTNORDER.Open
  Access:USERS.UseFile
  Access:STOCK.UseFile
  Access:EXCHHIST.UseFile
  Access:LOANHIST.UseFile
  Access:LOAN.UseFile
  SELF.FilesOpened = True
  locStartDate = TODAY()
  locEndDate = TODAY()
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:LOCATION,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (RepQ.Init(COMMAND()) = Level:Benign)
     0{prop:Hide} = 1
  END!  IF
      ! Save Window Name
   AddToLog('Window','Open','ExchangeLoanUnitsReturnedToMainStore')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,loc:ActiveLocationKey)
  BRW8.AddRange(loc:Active,locTrue)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,loc:Location,1,BRW8)
  BIND('tagLocation',tagLocation)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tagLocation,BRW8.Q.tagLocation)
  BRW8.AddField(loc:Location,BRW8.Q.loc:Location)
  BRW8.AddField(loc:RecordNumber,BRW8.Q.loc:RecordNumber)
  BRW8.AddField(loc:Active,BRW8.Q.loc:Active)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  IF (glo:WebJob = 1)
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = glo:Password
      IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          glo:Pointer = use:Location
          ADD(glo:Queue)
      END ! IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
      locOneLocation = use:Location
      ?locReportType{prop:Hide} = 0
      ?List{prop:Disable} = 1
      ?lookupStartDate{prop:Hide} = 1
      ?lookupEndDate{prop:Hide} = 1
  END
  
  IF (fLoan)
      ! #12341 Combine exchange/loan report in one (DBH: 31/01/2012)
      ?WindowTitle{prop:Text} = 'Loan Units Returned To Main Store'
  END
  
  
  IF (RepQ.SBOReport = TRUE)
      IF (LoadSBOnlineCriteria())
          RETURN RequestCancelled
      END ! IF
  END ! IF
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:EXCHANGE.Close
    Relate:EXCHORNO.Close
    Relate:LOCATION.Close
    Relate:RTNAWAIT.Close
    Relate:RTNORDER.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ExchangeLoanUnitsReturnedToMainStore')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020766'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020766'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020766'&'0')
      ***
    OF ?lookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          locStartDate = TINCALENDARStyle1(locStartDate)
          Display(?locStartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?lookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          locEndDate = TINCALENDARStyle1(locEndDate)
          Display(?locEndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Export
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
      If (RECORDS(glo:Queue) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any locations.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      Do Export
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
CreateExportFile        PROCEDURE(BYTE fReportType)
    CODE
    SBR:ExportFilePath = CLIP(locTempFolder) & fReportType & 'EURTMSREP_' & CLOCK() & RANDOM(1,1000) & '.CSV'

    ! Save the export file name to create document later
    IF (fReportType = 0)
        locExportFile0 = SBR:ExportFilePath
    ELSE
        locExportFile1 = SBR:ExportFilePath
    END

    ! Start Progress Window
    SBR.Init(RepQ.SBOReport)


    SBR.AddToExport(locReportName,1,1)


    ! Add Header
    SBR.AddToExport('Start Date',1)
    SBR.AddToExport(Format(locStartDate,@d06),,1)

    SBR.AddToExport('End Date',1)
    SBR.AddToExport(Format(locEndDate,@d06),,1)

    SBR.AddToExport('Location',1)

    IF (Records(glo:Queue) = 1)
        GET(glo:Queue,1)
        SBR.AddToExport(glo:Pointer,,1)
    ELSE
        SBR.AddToExport('Multiple',,1)
    END

    SBR.AddToExport('Date Created',1)
    SBR.AddToExport(Format(TODAY(),@d06),,1)

    SBR.AddToExport('',1,1)


    IF (fReportType = 1)
        SBR.AddToExport('Report Type',1)
        SBR.AddToExport('Received',,1)
        SBR.AddToExport('',1,1)

    ELSE
        SBR.AddToExport('Report Type',1)
        SBR.AddToExport('In Transit',,1)
        SBR.AddToExport('',1,1)
    END


    Do CreateDetailTitle

    locTotalRecords = 0

    ! Build Temp File
    LOOP l# = 1 TO RECORDS(glo:Queue)
        GET(glo:Queue,l#)

        SBR.UpdateProgressText('',1)

        IF (SBR.UpdateProgressWindow())
            BREAK
        END

        SBR.UpdateProgressText('Location: ' & CLIP(glo:Pointer))


        locRecordCount = 0

        RepQ.TotalRecords = RECORDS(RTNORDER)

        Access:RTNORDER.Clearkey(rtn:DateOrderedReceivedKey)
        rtn:Location = glo:Pointer
        rtn:Received = fReportType
        rtn:DateOrdered = locStartDate
        SET(rtn:DateOrderedReceivedKey,rtn:DateOrderedReceivedKey)
        LOOP UNTIL Access:RTNORDER.Next()
            IF (rtn:Location <> glo:Pointer OR |
                rtn:Received <> fReportType OR |
                rtn:DateOrdered > locEndDate)
                BREAK
            END
            IF (SBR.UpdateProgressWindow())
                BREAK
            END

            IF (RepQ.SBOReport)
                IF (RepQ.UpdateProgress())
                    SBR:CancelPressed = 2
                END !IF
            END !I F

            IF (fLoan)
                IF (rtn:ExchangeOrder <> 2)
                    CYCLE
                END

                Access:LOAN.Clearkey(loa:Ref_Number_Key)
                loa:Ref_Number = rtn:RefNumber
                IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
                    CYCLE
                END

            ELSE
                IF (rtn:ExchangeOrder <> 1)
                    CYCLE
                END

                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number = rtn:RefNumber
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
                    CYCLE
                END

            END

            Do AddRecord

        END
        SBR.UpdateProgressText('Records Found: ' & locRecordCount,1)
        IF (SBR:CancelPressed > 0)
            BREAK
        END

    END ! LOOP l# = 1 TO RECORDS(glo:Queue)


    SBR.Kill()
LoadSBOnlineCriteria        PROCEDURE()!,LONG
RetValue                        LONG(Level:Benign)
i                               LONG
qXML                            QUEUE(),PRE(qXml)
ReportType CSTRING(10)
StartDate                           CSTRING(10)
EndDate                             CSTRING(10)
                                END! QUEUE
    
    CODE
        LOOP 1 TIMES
            glo:WebJob = 1
            
            IF (XML:LoadFromFile(RepQ.FullCriteriaFilename))
                RetValue = Level:Fatal
                BREAK
            END ! IF
            XML:GotoTop

            IF (~XML:FindNextNode('Defaults'))
                recs# = XML:LoadQueue(qXML,1,1)
                IF (RECORDS(qXML) = 0)
                    RetValue = Level:Fatal
                    BREAK
                END ! IF
                GET(qXML, 1)
                !XML:DebugMyQueue(qXML,'qXML')
                locStartDate = qXML.StartDate
                locEndDate = qXML.EndDate
                locReportType = qXML.ReportType

                XML:Free()
            END ! IF

            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = RepQ.UserCode
            Access:USERS.TryFetch(use:User_Code_Key)
            glo:Pointer = use:Location
            ADD(glo:Queue)
            locOneLocation = glo:Pointer

            RepQ.UpdateStatus('Running')

            DO Export

        END ! BREAK LOOP

        IF (RetValue = Level:Fatal)
            RepQ.Failed()
        END !I F

        RETURN RetValue
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tagLocation = ''
    ELSE
      tagLocation = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagLocation = '*')
    SELF.Q.tagLocation_Icon = 2
  ELSE
    SELF.Q.tagLocation_Icon = 1
  END


BRW8.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  IF (locOneLocation <> '')
      IF (loc:Location <> locOneLocation)
          RETURN Record:Filtered
      END
  END
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  RETURN ReturnValue

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LoanAvailableQuantityCriteria PROCEDURE               !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::11:TAGFLAG         BYTE(0)
DASBRW::11:TAGDISPSTATUS   BYTE(0)
DASBRW::11:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::14:TAGFLAG         BYTE(0)
DASBRW::14:TAGDISPSTATUS   BYTE(0)
DASBRW::14:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tagLocation          STRING(1)
locTrue              BYTE(1)
tagManufacturer      STRING(1)
locStockType         STRING(30)
qResults             QUEUE,PRE(qr)
Manufacturer         STRING(30)
ModelNumber          STRING(30)
StockType            STRING(30)
Location             STRING(30)
Qty                  LONG
                     END
BRW10::View:Browse   VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                       PROJECT(loc:Active)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tagLocation            LIKE(tagLocation)              !List box control field - type derived from local data
tagLocation_Icon       LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
loc:Active             LIKE(loc:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW13::View:Browse   VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tagManufacturer        LIKE(tagManufacturer)          !List box control field - type derived from local data
tagManufacturer_Icon   LONG                           !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(650,5,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,70,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Available Loan Quantity'),AT(168,72),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(464,72),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,84,352,248),USE(?Sheet1),BELOW,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Tag Location(s)'),AT(272,98),USE(?Prompt3),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           LIST,AT(268,124,144,148),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(330,199,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(330,215,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(240,290),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(307,289),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(376,290),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Tag Manufacturer(s)'),AT(256,92),USE(?Prompt3:2),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           LIST,AT(256,106,144,148),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(326,180,1,1),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(318,199,1,1),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(404,168),USE(?DASTAG:2),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(404,198),USE(?DASTAGAll:2),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(404,228),USE(?DASUNTAGALL:2),TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON,AT(416,272),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Stock Type'),AT(236,276),USE(?locStockType:Prompt),FONT(,,COLOR:White,FONT:bold)
                           ENTRY(@s30),AT(288,276,124,10),USE(locStockType),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(380,334),USE(?OK),TRN,FLAT,ICON('crerepp.jpg')
                         END
                         TAB('Tab 3'),USE(?Tab3),HIDE
                           PROMPT('Results'),AT(168,90),USE(?Prompt6),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           LIST,AT(168,108,344,188),USE(?List3),HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('80L(2)|M~Manufacturer~@s30@80L(2)|M~Model Number~@s30@80L(2)|M~Stock Type~@s30@6' &|
   '0L(2)|M~Location~@s30@56R(2)|M~Qty~@n_14@'),FROM(qResults)
                           BUTTON,AT(444,300),USE(?btnExport),TRN,FLAT,ICON('exportp.jpg')
                           BUTTON,AT(168,334),USE(?btnBack),TRN,FLAT,ICON('backp.jpg')
                         END
                       END
                       PANEL,AT(164,334,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,334),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(168,334),USE(?VSBackButton),TRN,FLAT,ICON('backp.jpg')
                       BUTTON,AT(236,334),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                     END

myProg        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
myProg:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(myProg.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(myProg.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(myProg.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?myProg:CancelButton)
     END

omit('***',ClarionetUsed=0)

myProg:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(myProg.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(myProg.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(myProg.CNPercentText),CENTER
     END
***

Wizard8         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW10                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
BRW13                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW13::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
locSavePath         CSTRING(255)
locExportFile       CSTRING(255),STATIC
ExportFile          File,DRIVER('BASIC'),PRE(expfil),Name(locExportFile),CREATE,BINDABLE,THREAD
RECORD                  RECORD
Location                    STRING(30)
StockType                   STRING(30)
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
Quantity                    STRING(30)
                        END
                    END
!Save Entry Fields Incase Of Lookup
look:locStockType                Like(locStockType)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::11:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW10.UpdateBuffer
   glo:Queue.Pointer = loc:Location
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
    tagLocation = '*'
  ELSE
    DELETE(glo:Queue)
    tagLocation = ''
  END
    Queue:Browse.tagLocation = tagLocation
  IF (tagLocation = '*')
    Queue:Browse.tagLocation_Icon = 2
  ELSE
    Queue:Browse.tagLocation_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW10.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW10.Reset
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::11:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::11:QUEUE = glo:Queue
    ADD(DASBRW::11:QUEUE)
  END
  FREE(glo:Queue)
  BRW10.Reset
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::11:QUEUE.Pointer = loc:Location
     GET(DASBRW::11:QUEUE,DASBRW::11:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = loc:Location
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASSHOWTAG Routine
   CASE DASBRW::11:TAGDISPSTATUS
   OF 0
      DASBRW::11:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::11:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::11:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW10.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::14:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW13.UpdateBuffer
   glo:Queue2.Pointer2 = man:Manufacturer
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = man:Manufacturer
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tagManufacturer = '*'
  ELSE
    DELETE(glo:Queue2)
    tagManufacturer = ''
  END
    Queue:Browse:1.tagManufacturer = tagManufacturer
  IF (tagManufacturer = '*')
    Queue:Browse:1.tagManufacturer_Icon = 2
  ELSE
    Queue:Browse:1.tagManufacturer_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::14:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW13.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW13::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = man:Manufacturer
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW13.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::14:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW13.Reset
  SETCURSOR
  BRW13.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::14:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::14:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::14:QUEUE = glo:Queue2
    ADD(DASBRW::14:QUEUE)
  END
  FREE(glo:Queue2)
  BRW13.Reset
  LOOP
    NEXT(BRW13::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::14:QUEUE.Pointer2 = man:Manufacturer
     GET(DASBRW::14:QUEUE,DASBRW::14:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = man:Manufacturer
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW13.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::14:DASSHOWTAG Routine
   CASE DASBRW::14:TAGDISPSTATUS
   OF 0
      DASBRW::14:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::14:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::14:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW13.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Export              ROUTINE
    DATA
locTempFolder   STRING(255)
locFileName     STRING(255)
    CODE
        locFileName = 'Available Loan Quantity ' & FORMAT(TODAY(),@d12) & '.csv'
        
        IF (glo:WebJob = 0)
            locSavePath = Path()
            locExportFile = locFileName
            if (fileDialog('Choose File',locExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName + file:Save))
                !Found
                setPath(locSavePath)

            else ! if (fileDialog)
                !Error
                setPath(locSavePath)
                EXIT
            end ! if (fileDialog)
        ELSE
            locTempFolder = GetTempFolder()
            
            IF (locTempFolder = '')
                Beep(Beep:SystemHand)  ;  Yield()
                Case Missive('An error occurred creating the report.'&|
                    '|'&|
                    '|Please ty again.','ServiceBase',|
                    'mstop.jpg','/&OK') 
                Of 1 ! &OK Button
                End!Case Message
                EXIT
            END

            locExportFile = CLIP(locTempFolder) & CLIP(locFileName)
        END
        
        myProg.Init(RECORDS(qResults))
        line1# = 1
        REMOVE(locExportFile)
        CREATE(ExportFile)
        OPEN(ExportFile)
        LOOP i# = 1 TO RECORDS(qResults)
            GET(qResults,i#)
            
            IF (myProg.Update())
                BREAK
            END
            
            IF (line1# = 1)
                ! Print Header
                CLEAR(exp:Record)
                expfil:Location = 'Loan Report'
                ADD(ExportFile)
                CLEAR(exp:Record)
                expfil:Location = 'Date'
                expfil:StockType = FORMAT(TODAY(),@d06b)
                ADD(ExportFile)
                CLEAR(exp:Record)
                expfil:Location = 'Location'
                expfil:StockType = 'Stock Type'
                expfil:Manufacturer = 'Manufacturer'
                expfil:ModelNumber = 'Model Number'
                expfil:Quantity = 'Quantity'
                ADD(ExportFile)
                line1# = 0
            END
            CLEAR(exp:Record)
            expfil:Manufacturer = qResults.Manufacturer
            expfil:ModelNumber = qResults.ModelNumber
            expfil:StockType = qResults.StockType
            expfil:Location = qResults.Location
            expfil:Quantity = qResults.Qty
            ADD(ExportFile)
        END
        
        CLOSE(ExportFile)
        
        myProg.Kill()
        
        IF (glo:WebJob = 1)
            SendFileToClient(locExportFile)
            REMOVE(locExportFile)
        END
        Beep(Beep:SystemAsterisk)  ;  Yield()
        Case Missive('Export File Created.'&|
            '|'&|
            '|' & Clip(locFilename) & '','ServiceBase',|
            'midea.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message        
RunReport       ROUTINE
    FREE(qResults)
    myProg.init(RECORDS(LOAN))

    IF (glo:WebJob)
        ! Use location based key for Webmaster
        GET(glo:Queue,1)
        Access:LOAN.Clearkey(loa:LocStockAvailRefKey)
        loa:Location = glo:Pointer
        loa:Available = 'AVL'
        loa:Stock_Type = locStockType
        SET(loa:LocStockAvailRefKey,loa:LocStockAvailRefKey)
    ELSE
        Access:LOAN.Clearkey(loa:ESN_Available_Key)
        loa:Available = 'AVL'
        loa:Stock_Type = locStockTYpe
        SET(loa:ESN_Available_Key,loa:ESN_Available_Key)
    END
    LOOP UNTIL Access:LOAN.Next()
        IF (glo:WebJob)
            IF (loa:Location <> glo:Pointer)
                BREAK
            END
        END

        IF (loa:Available <> 'AVL')
            BREAK
        END
        IF (loa:Stock_Type <> locStockType)
            BREAK
        END

        IF (myProg.Update())
            BREAK
        END

        ! Criteria
        ! Is it one of the tagged locations?
        IF (glo:WebJob = 0)
            glo:Pointer = loa:Location
            Get(glo:Queue,glo:Pointer)
            IF (ERROR())
                CYCLE
            END
        END

        ! Is it one of the tagged Manufacturers?
        glo:Pointer2 = loa:Manufacturer
        GET(glo:Queue2,glo:Pointer2)
        IF (ERROR())
            CYCLE
        END

        qResults.Manufacturer = loa:Manufacturer
        qResults.ModelNumber = loa:Model_Number
        qResults.StockType = loa:Stock_Type
        qResults.Location = loa:Location
        GET(qResults,qResults.Manufacturer,qResults.ModelNumber,qResults.StockType,qResults.Location)
        IF (ERROR())
            qResults.Qty = 1
            ADD(qResults,qResults.Manufacturer,qResults.ModelNumber,qResults.StockType,qResults.Location)
        ELSE
            qResults.Qty += 1
            PUT(qResults)
        END
    END

    myProg.Kill()

    DO ShowResults



ShowResults     ROUTINE
    ?vsBackButton{prop:Hide} = 1
    ?vsNextButton{prop:Hide} = 1
    ?Tab1{prop:Hide} = 1
    ?Tab2{prop:Hide} = 1
    ?Tab3{prop:Hide} = 0
    Select(?Tab3)
    IF (RECORDS(qResults))
        ?btnExport{prop:Hide} = 0
    ELSE
        ?btnExport{prop:Hide} = 1
    END
HideResults     ROUTINE
    IF (glo:WebJob <> 1)
        ?Tab1{prop:Hide} = 0
        ?vsBackButton{prop:Hide} = 0
        ?vsNextButton{prop:Hide} = 0
    END
    ?Tab2{prop:Hide} = 0
    ?Tab3{prop:Hide} = 1
    Select(?Tab2)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020768'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('LoanAvailableQuantityCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOAN.Open
  Relate:LOCATION.Open
  Access:STOCKTYP.UseFile
  SELF.FilesOpened = True
  BRW10.Init(?List,Queue:Browse.ViewPosition,BRW10::View:Browse,Queue:Browse,Relate:LOCATION,SELF)
  BRW13.Init(?List:2,Queue:Browse:1.ViewPosition,BRW13::View:Browse,Queue:Browse:1,Relate:MANUFACT,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','LoanAvailableQuantityCriteria')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?List3{prop:vcr} = False
    Wizard8.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?OK, |                           ! OK button
                     ?Cancel, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     0, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW10.Q &= Queue:Browse
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,loc:ActiveLocationKey)
  BRW10.AddRange(loc:Active,locTrue)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,loc:Location,1,BRW10)
  BIND('tagLocation',tagLocation)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW10.AddField(tagLocation,BRW10.Q.tagLocation)
  BRW10.AddField(loc:Location,BRW10.Q.loc:Location)
  BRW10.AddField(loc:RecordNumber,BRW10.Q.loc:RecordNumber)
  BRW10.AddField(loc:Active,BRW10.Q.loc:Active)
  BRW13.Q &= Queue:Browse:1
  BRW13.RetainRow = 0
  BRW13.AddSortOrder(,man:Manufacturer_Key)
  BRW13.AddLocator(BRW13::Sort0:Locator)
  BRW13::Sort0:Locator.Init(,man:Manufacturer,1,BRW13)
  BIND('tagManufacturer',tagManufacturer)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW13.AddField(tagManufacturer,BRW13.Q.tagManufacturer)
  BRW13.AddField(man:Manufacturer,BRW13.Q.man:Manufacturer)
  BRW13.AddField(man:RecordNumber,BRW13.Q.man:RecordNumber)
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  BRW13.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW13.AskProcedure = 0
      CLEAR(BRW13.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
    IF (glo:WebJob = 1)
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = glo:Password
      IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          glo:Pointer = use:Location
          ADD(glo:Queue)
      END ! IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
      ?Tab1{prop:Hide} = 1
      ?VSNextButton{prop:Hide} = 1
      ?VSBackButton{prop:Hide} = 1
      POST(Event:Accepted,?VSNextButton)
  
  END
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOAN.Close
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','LoanAvailableQuantityCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickLoanStockType
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard8.Validate()
        DISABLE(Wizard8.NextControl())
     ELSE
        ENABLE(Wizard8.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020768'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020768'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020768'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?CallLookup
      ThisWindow.Update
      stp:Stock_Type = locStockType
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        locStockType = stp:Stock_Type
      END
      ThisWindow.Reset(1)
    OF ?locStockType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locStockType, Accepted)
      ?locStockType{prop:Touched} = 1
      IF locStockType OR ?locStockType{Prop:Req}
        stp:Stock_Type = locStockType
        stp:Use_Loan = 'YES'
        !Save Lookup Field Incase Of error
        look:locStockType        = locStockType
        IF Access:STOCKTYP.TryFetch(stp:Use_Loan_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            locStockType = stp:Stock_Type
          ELSE
            CLEAR(stp:Use_Loan)
            !Restore Lookup On Error
            locStockType = look:locStockType
            SELECT(?locStockType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locStockType, Accepted)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      IF (Records(glo:Queue) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any Locations.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      IF (Records(glo:Queue2) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not tagged any Manufacturers.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      IF (locStockType = '')
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have not selected a Stock Type.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      DO RunReport
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?btnExport
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnExport, Accepted)
      Do Export
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnExport, Accepted)
    OF ?btnBack
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnBack, Accepted)
      Do HideResults
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnBack, Accepted)
    OF ?VSBackButton
      ThisWindow.Update
         Wizard8.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard8.TakeAccepted()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW10.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tagLocation = ''
    ELSE
      tagLocation = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagLocation = '*')
    SELF.Q.tagLocation_Icon = 2
  ELSE
    SELF.Q.tagLocation_Icon = 1
  END


BRW10.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW10::RecordStatus=ReturnValue
  IF BRW10::RecordStatus NOT=Record:OK THEN RETURN BRW10::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::11:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW10::RecordStatus
  RETURN ReturnValue


BRW13.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = man:Manufacturer
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tagManufacturer = ''
    ELSE
      tagManufacturer = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagManufacturer = '*')
    SELF.Q.tagManufacturer_Icon = 2
  ELSE
    SELF.Q.tagManufacturer_Icon = 1
  END


BRW13.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW13.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW13.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW13::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW13::RecordStatus=ReturnValue
  IF BRW13::RecordStatus NOT=Record:OK THEN RETURN BRW13::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = man:Manufacturer
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::14:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW13::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW13::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW13::RecordStatus
  RETURN ReturnValue

Wizard8.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW10.Q &= NULL) ! Has Browse Object been initialized?
       BRW10.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW13.Q &= NULL) ! Has Browse Object been initialized?
       BRW13.ResetQueue(Reset:Queue)
    END

Wizard8.TakeBackEmbed PROCEDURE
   CODE

Wizard8.TakeNextEmbed PROCEDURE
   CODE

Wizard8.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
myProg.Init                 PROCEDURE(LONG func:Records)
    CODE
        myProg.ProgressSetup(func:Records)
myProg.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        myProg.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(myProg:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        myProg.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.ResetProgress      Procedure(Long func:Records)
CODE

    myProg.recordsToProcess = func:Records
    myProg.recordsprocessed = 0
    myProg.percentProgress = 0
    myProg.progressThermometer = 0
    myProg.CNprogressThermometer = 0
    myProg.skipRecords = 0
    myProg.userText = ''
    myProg.CNuserText = ''
    myProg.percentText = '0% Completed'
    myProg.CNpercentText = myProg.percentText


myProg.Update      Procedure(<String func:String>)
    CODE
        RETURN (myProg.InsideLoop(func:String))
myProg.InsideLoop     Procedure(<String func:String>)
CODE

    myProg.SkipRecords += 1
    If myProg.SkipRecords < 20
        myProg.RecordsProcessed += 1
        Return 0
    Else
        myProg.SkipRecords = 0
    End
    if (func:String <> '')
        myProg.UserText = Clip(func:String)
        myProg.CNUserText = myProg.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        myProg.NextRecord()
        ClarioNet:UpdatePushWindow(myProg:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        myProg.NextRecord()
        if (myProg.CancelLoop())
            return 1
        end ! if (myProg.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

myProg.ProgressText        Procedure(String    func:String)
CODE

    myProg.UserText = Clip(func:String)
    myProg.CNUserText = myProg.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.Kill     Procedure()
    CODE
        myProg.ProgressFinish()
myProg.ProgressFinish     Procedure()
CODE

    myProg.ProgressThermometer = 100
    myProg.CNProgressThermometer = 100
    myProg.PercentText = '100% Completed'
    myProg.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(myProg:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.NextRecord      Procedure()
CODE
    Yield()
    myProg.RecordsProcessed += 1
    !If myProg.percentprogress < 100
        myProg.percentprogress = (myProg.recordsprocessed / myProg.recordstoprocess)*100
        If myProg.percentprogress > 100 or myProg.percentProgress < 0
            myProg.percentprogress = 0
        End
        If myProg.percentprogress <> myProg.ProgressThermometer then
            myProg.ProgressThermometer = myProg.percentprogress
            myProg.PercentText = format(myProg:percentprogress,@n3) & '% Completed'
        End
    !End
    myProg.CNPercentText = myProg.PercentText
    myProg.CNProgressThermometer = myProg.ProgressThermometer
    Display()

myProg.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?myProg:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
StockReturnForCreditReport PROCEDURE                  !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGFLAG         BYTE(0)
DASBRW::12:TAGMOUSE        BYTE(0)
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::15:TAGFLAG         BYTE(0)
DASBRW::15:TAGMOUSE        BYTE(0)
DASBRW::15:TAGDISPSTATUS   BYTE(0)
DASBRW::15:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
locStartDate         DATE
locEndDate           DATE
tagTrade             STRING(1)
tagReturnType        STRING(1)
locTRUE              BYTE(1)
BRW11::View:Browse   VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tagTrade               LIKE(tagTrade)                 !List box control field - type derived from local data
tagTrade_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(RETTYPES)
                       PROJECT(rtt:Description)
                       PROJECT(rtt:RecordNumber)
                       PROJECT(rtt:Active)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tagReturnType          LIKE(tagReturnType)            !List box control field - type derived from local data
tagReturnType_Icon     LONG                           !Entry's icon ID
rtt:Description        LIKE(rtt:Description)          !List box control field - type derived from field
rtt:RecordNumber       LIKE(rtt:RecordNumber)         !Primary key field - type derived from field
rtt:Active             LIKE(rtt:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(647,6,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,70,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Stock Returned For Credit By Franchise/ARC'),AT(167,72),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(463,72),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,82,352,250),USE(?Sheet1),BELOW,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Start Date'),AT(256,94),USE(?locStartDate:Prompt),FONT(,,COLOR:White,FONT:bold)
                           ENTRY(@d17),AT(328,94,64,10),USE(locStartDate),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(396,90),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(256,116),USE(?locEndDate:Prompt),FONT(,,COLOR:White,FONT:bold)
                           ENTRY(@d17),AT(328,116,64,10),USE(locEndDate),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Select Franchise'),AT(172,136),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold)
                           LIST,AT(172,148,264,164),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)J@s1@60L(2)|M~Account Number~@s15@120L(2)~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(444,224),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON('&Rev tags'),AT(255,202,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(259,220,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(444,256),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(444,286),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON,AT(396,112),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Select Return Type(s) (leave blank for Loan Units)'),AT(236,102),USE(?Prompt5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           LIST,AT(236,126,172,190),USE(?List:2),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Description~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(315,211,1,1),USE(?DASREVTAG:2),HIDE
                           BUTTON,AT(420,234),USE(?DASTAG:2),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON('sho&W tags'),AT(307,231,1,1),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(420,264),USE(?DASTAGAll:2),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(420,292),USE(?DASUNTAGALL:2),TRN,FLAT,ICON('untagalp.jpg')
                         END
                       END
                       PANEL,AT(164,334,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(444,334),USE(?Close),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(168,334),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(232,334),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(376,334),USE(?OK),TRN,FLAT,ICON('exportp.jpg')
                     END

Wizard13         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW11                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                 !Default Locator
BRW14                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
EE MyExportClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW11.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tagTrade = '*'
  ELSE
    DELETE(glo:Queue)
    tagTrade = ''
  END
    Queue:Browse.tagTrade = tagTrade
  IF (tagTrade = '*')
    Queue:Browse.tagTrade_Icon = 2
  ELSE
    Queue:Browse.tagTrade_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW11.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW11.Reset
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::12:QUEUE = glo:Queue
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue)
  BRW11.Reset
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW11.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::15:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW14.UpdateBuffer
   glo:Queue2.Pointer2 = rtt:Description
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = rtt:Description
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tagReturnType = '*'
  ELSE
    DELETE(glo:Queue2)
    tagReturnType = ''
  END
    Queue:Browse:1.tagReturnType = tagReturnType
  IF (tagReturnType = '*')
    Queue:Browse:1.tagReturnType_Icon = 2
  ELSE
    Queue:Browse:1.tagReturnType_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::15:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW14.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = rtt:Description
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::15:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW14.Reset
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::15:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::15:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::15:QUEUE = glo:Queue2
    ADD(DASBRW::15:QUEUE)
  END
  FREE(glo:Queue2)
  BRW14.Reset
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::15:QUEUE.Pointer2 = rtt:Description
     GET(DASBRW::15:QUEUE,DASBRW::15:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = rtt:Description
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::15:DASSHOWTAG Routine
   CASE DASBRW::15:TAGDISPSTATUS
   OF 0
      DASBRW::15:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::15:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::15:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW14.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Export              ROUTINE
    DATA
kReportName     STRING(50)
kTempFolder     CSTRING(255)
kReportFolder   CSTRING(255)
kFilename       STRING(255)
kTotalRecords   LONG()
kTempFilename   CSTRING(255)
i       LONG()
    CODE
        
        kReportName = 'Stock Returned For Credit'
        kTempFolder = GetTempFolder()
        
        IF (kTempFolder = '')
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error occurred creating the report.'&|
                '|'&|
                '|Please ty again.','ServiceBase',|
                'mstop.jpg','/&OK') 
            Of 1 ! &OK Button
            End!Case Message
            EXIT            
        END
        
        kFilename = CLIP(kReportName) & ' ' & FORMAT(TODAY(),@d12) & '.xls'
        
        kTempFilename = CLIP(kTempFolder) & 'SRFC_' & CLOCK() & RANDOM(1,1000) & '.CSV'
        
        IF (EE.OpenDataFile(kTempFilename,,1))
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error occurred creating the report.','ServiceBase',|
                           'mstop.jpg','/&OK') 
            Of 1 ! &OK Button
            End!Case Message
            RETURN
        END

        
        EE.OpenProgressWindow()
        
        EE.UpdateProgressText()
        
        EE.UpdateProgressText('Report Started: ' & FORMAT(EE.StartDate,@d06) & |
            ' ' & FORMAT(EE.StartTime,@t01))
        
        EE.UpdateProgressText()
        
        ! Set Title
        EE.AddField(kReportName,1,1)
        EE.AddField('Location',1)
        IF (RECORDS(glo:Queue) = 1)
            GET(glo:Queue,1)
            EE.AddField(glo:Pointer,,1)
        ELSE
            EE.AddField('Multiple',,1)
        END
        EE.AddField('Start Date',1)
        EE.AddField(Format(locStartDate,@d06),,1)
        EE.AddField('End Date',1)
        EE.AddField(Format(locEndDate,@d06),,1)

        EE.AddField('Date Created',1)
        EE.AddField(Format(TODAY(),@d06),,1)

        EE.AddField('',1,1)
        
        EE.AddField('Date Received',1)
        EE.AddField('Original Invoice Number')
        EE.AddField('Original Order Number')
        EE.AddField('Franchise Name')
        EE.AddField('Franchise Account Number')
        EE.AddField('Unit Type Return')
        EE.AddField('Return Type')
        EE.AddField('Return Status')
        EE.AddField('Waybill Number')
        EE.AddField('Model Number')
        EE.AddField('Quantity Returned')
        EE.AddField('Credit Note Request Number')
        EE.AddField('Part Number / Model Number')
        EE.AddField('Part Description / IMEI Number')
        EE.AddField('Item Cost')
        EE.AddField('Line Cost')
        EE.AddField('Average Cost',,1)
        
        ! -------------------
        
        LOOP i = 1 TO RECORDS(glo:Queue)
            GET(glo:Queue,i)
            
            EE.UpdateProgressText('',1)
            EE.UpdateProgressText('Location: ' & CLIP(glo:Pointer))
            
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number = glo:Pointer
            IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key))
                CYCLE
            END
            
            
            countRecords# = 0
            
            Access:RTNORDER.Clearkey(rtn:DateOrderedReceivedKey)
            rtn:Location = tra:SiteLocation
            rtn:DateOrdered = locStartDate
            rtn:Received = 1
            SET(rtn:DateOrderedReceivedKey,rtn:DateOrderedReceivedKey)
            LOOP UNTIL Access:RTNORDER.NEXT()
                IF (rtn:Location <> tra:SiteLocation)
                    BREAK
                END
                IF (rtn:Received <> 1)
                    BREAK
                END
                IF (rtn:DateOrdered > locEndDate)
                    BREAK
                END
                
                Access:RTNAWAIT.Clearkey(rta:RTNORDERKey)
                rta:RTNRecordNumber = rtn:RecordNumber
                IF (Access:RTNAWAIT.Tryfetch(rta:RTNORDERKey))
                    CYCLE
                END
                
                IF (RECORDS(glo:Queue2))
                    glo:Pointer2 = rtn:ReturnType
                    GET(glo:Queue2,glo:Pointer2)
                    IF (ERROR())
                        CYCLE
                    END
                END
                
                ! Date Received
                EE.AddField(FORMAT(rta:DateProcessed,@d06b),1)
                ! Original Invoice Number
                EE.AddField(rtn:InvoiceNumber)
                ! Original Order Number
                EE.AddField(rtn:OrderNumber)
                ! Franchise Name
                ! Franchise Account Number
                EE.AddField(tra:Company_Name)
                EE.AddField(tra:Account_Number)
                ! Unit Type Return
                ! Return Type
                CASE rtn:ExchangeOrder
                OF 0
                    EE.AddField('Spare')
                    EE.AddField(rtn:ReturnType)
                OF 1
                    EE.AddField('Exchange')
                    EE.AddField(rtn:ReturnType)
                OF 2
                    EE.AddField('Loan')
                    EE.AddField('FAULTY')
                END
                ! Return Status
                IF (rta:Processed = 0)
                    EE.AddField('PENDING')
                ELSE
                    EE.AddField(CLIP(rta:AcceptReject) & 'ED')
                END
                ! Waybill Number
                EE.AddField(rtn:WaybillNumber)
                ! Model Number
                ! Quantity Returned
                IF (rtn:ExchangeOrder = 0)
                    EE.AddField('')
                    EE.AddField(rta:Quantity)
                ELSE
                    EE.AddField(rtn:PartNumber)
                    EE.AddField('1')
                END
                ! Credit Note Request Number
                EE.AddField(rtn:CreditNoteRequestNumber)
                ! Part Number
                EE.AddField(rtn:PartNumber)
                ! Description
                EE.AddField('''' & rtn:Description)
                ! Item Cost
                ! Line Cost
                IF (rtn:ExchangeOrder = 0)
                    EE.AddField(FORMAT(rtn:SaleCost,@n_14.2))
                    EE.AddField(FORMAT(rtn:SaleCost * rta:Quantity,@n_14.2))
                ELSE
                    EE.AddField(FORMAT(rtn:ExchangePrice,@n_14.2))
                    EE.AddField(FORMAT(rtn:ExchangePrice,@n_14.2))
                END
                ! Average Cost
                IF (rtn:ExchangeOrder = 0)
                    EE.AddField(FORMAT(rtn:PurchaseCost,@n_14.2),,1)
                ELSE 
                    EE.AddField(FORMAT(rtn:ExchangePrice,@n_14.2),,1)
                END
                
                kTotalRecords += 1    
                countRecords# += 1
                IF (SBR:CancelPressed > 0)
                    BREAK
                END
                
            END ! LOOP
            EE.UpdateProgressText('Records Found: ' & count#,1)
            
            IF (EE.UpdateProgressWindow())
                BREAK
            END
            
        END ! LOOP i = 1 TO RECORDS(glo:Queue)
        
        EE.CloseDataFile()

        IF (EE.CancelPressed = 2)
            EE.UpdateProgressText('===============')
            EE.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
                ' ' & FORMAT(clock(),@t1))
            EE.FinishProgress()
            REMOVE(kTempFilename)
        ELSE
            EE.UpdateProgressText()
            EE.UpdateProgressText('Formatting Document')
            EE.UpdateProgressText()
            
            ! Build Excel Document
            If E1.Init(0,0) = 0
                Case Missive('An error has occurred finding your Excel document.'&|
                    '<13,10>'&|
                    '<13,10>Please quit and try again.','ServiceBase 3g',|
                    'mstop.jpg','/OK')
                Of 1 ! OK Button
                End ! Case Missive
                Exit
            End !If E1.Init(0,0,1) = 0

            SETCLIPBOARD(EE.CopyDataFileToClipboard(E1,'Q' & (kTotalRecords + 15),kTempFilename))
            
            E1.NewWorkBook()
            E1.RenameWorkSheet('Detailed')
            E1.SelectCells('A1')
            E1.Paste()
            EE.ClearClipboard()

            EE.FinishFormat(E1,'Q',kTotalRecords + 10,'A8')

            REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
            E1.SaveAs(CLIP(kTempFolder) & CLIP(kFileName))
            E1.CloseWorkBook(2)
            E1.Kill()

            EE.UpdateProgressText('===============')
            EE.UpdateProgressText('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

            kReportFolder = SetReportsFolder('ServiceBase Export',CLIP(kReportName),glo:WebJob)

            IF (EE.CopyFinalFile(CLIP(kTempFolder) & Clip(kFileName),CLIP(kReportFolder) & CLIP(kFilename)))
                ! Couldn't save final file
                EE.FinishProgress()
            ELSE
                EE.FinishProgress(kReportFolder)
            END
        END
        REMOVE(kTempFilename)

        POST(EVENT:CloseWindow)
      
        
        
        
        

        
        

!Export              ROUTINE
!    DATA
!kReportName     STRING(50)
!kTempFolder     CSTRING(255)
!kReportFolder   CSTRING(255)
!kFilename       STRING(255)
!kTotalRecords   LONG()
!i       LONG()
!    CODE
!        kReportName = 'Stock Returned For Credit'
!        kTempFolder = GetTempFolder()
!        
!        IF (kTempFolder = '')
!            Beep(Beep:SystemHand)  ;  Yield()
!            Case Missive('An error occurred creating the report.'&|
!                '|'&|
!                '|Please ty again.','ServiceBase',|
!                'mstop.jpg','/&OK') 
!            Of 1 ! &OK Button
!            End!Case Message
!            EXIT            
!        END
!        
!        kFilename = CLIP(kReportName) & ' ' & FORMAT(TODAY(),@d12) & '.xls'
!        
!        SBR:ExportFilePath = CLIP(kTempFolder) & 'SRFC_' & CLOCK() & RANDOM(1,1000) & '.CSV'
!        
!        SBR.Init()
!        
!        SBR.OpenProgressWindow(100)
!        
!        SBR.UpdateProgressText('')
!        
!        SBR.UpdateProgressText('Report Started: ' & FORMAT(SBR:StartDate,@d6) & |
!            ' ' & FORMAT(SBR:StartTime,@t1))
!        SBR.UpdateProgressText('')
!        
!        ! Set Title
!
!        SBR.AddToExport(kReportName,1,1)
!        SBR.AddToExport('Location',1)
!        IF (RECORDS(glo:Queue) = 1)
!            GET(glo:Queue,1)
!            SBR.AddToExport(glo:Pointer,,1)
!        ELSE
!            SBR.AddToExport('Multiple',,1)
!        END
!        SBR.AddToExport('Start Date',1)
!        SBR.AddToExport(Format(locStartDate,@d06),,1)
!        SBR.AddToExport('End Date',1)
!        SBR.AddToExport(Format(locEndDate,@d06),,1)
!
!        SBR.AddToExport('Date Created',1)
!        SBR.AddToExport(Format(TODAY(),@d06),,1)
!
!        SBR.AddToExport('',1,1)
!        
!        SBR.AddToExport('Date Received',1)
!        SBR.AddToExport('Original Invoice Number')
!        SBR.AddToExport('Original Order Number')
!        SBR.AddToExport('Franchise Name')
!        SBR.AddToExport('Franchise Account Number')
!        SBR.AddToExport('Unit Type Return')
!        SBR.AddToExport('Return Type')
!        SBR.AddToExport('Return Status')
!        SBR.AddToExport('Waybill Number')
!        SBR.AddToExport('Model Number')
!        SBR.AddToExport('Quantity Returned')
!        SBR.AddToExport('Credit Note Request Number')
!        SBR.AddToExport('Part Number / Model Number')
!        SBR.AddToExport('Part Description / IMEI Number')
!        SBR.AddToExport('Item Cost')
!        SBR.AddToExport('Line Cost')
!        SBR.AddToExport('Average Cost',,1)
!        
!        ! -------------------
!        
!        LOOP i = 1 TO RECORDS(glo:Queue)
!            GET(glo:Queue,i)
!            
!            SBR.UpdateProgressText('',1)
!            SBR.UpdateProgressText('Location: ' & CLIP(glo:Pointer))
!            
!            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!            tra:Account_Number = glo:Pointer
!            IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key))
!                CYCLE
!            END
!            
!            
!            countRecords# = 0
!            
!            Access:RTNORDER.Clearkey(rtn:DateOrderedReceivedKey)
!            rtn:Location = tra:SiteLocation
!            rtn:DateOrdered = locStartDate
!            rtn:Received = 1
!            SET(rtn:DateOrderedReceivedKey,rtn:DateOrderedReceivedKey)
!            LOOP UNTIL Access:RTNORDER.NEXT()
!                IF (rtn:Location <> tra:SiteLocation)
!                    BREAK
!                END
!                IF (rtn:Received <> 1)
!                    BREAK
!                END
!                IF (rtn:DateOrdered > locEndDate)
!                    BREAK
!                END
!                
!                Access:RTNAWAIT.Clearkey(rta:RTNORDERKey)
!                rta:RTNRecordNumber = rtn:RecordNumber
!                IF (Access:RTNAWAIT.Tryfetch(rta:RTNORDERKey))
!                    CYCLE
!                END
!                
!                IF (RECORDS(glo:Queue2))
!                    glo:Pointer2 = rtn:ReturnType
!                    GET(glo:Queue2,glo:Pointer2)
!                    IF (ERROR())
!                        CYCLE
!                    END
!                END
!                
!                ! Date Received
!                SBR.AddToExport(FORMAT(rta:DateProcessed,@d06b),1)
!                ! Original Invoice Number
!                SBR.AddToExport(rtn:InvoiceNumber)
!                ! Original Order Number
!                SBR.AddToExport(rtn:OrderNumber)
!                ! Franchise Name
!                ! Franchise Account Number
!                SBR.AddToExport(tra:Company_Name)
!                SBR.AddToExport(tra:Account_Number)
!                ! Unit Type Return
!                ! Return Type
!                CASE rtn:ExchangeOrder
!                OF 0
!                    SBR.AddToExport('Spare')
!                    SBR.AddToExport(rtn:ReturnType)
!                OF 1
!                    SBR.AddToExport('Exchange')
!                    SBR.AddToExport(rtn:ReturnType)
!                OF 2
!                    SBR.AddToExport('Loan')
!                    SBR.AddToExport('FAULTY')
!                END
!                ! Return Status
!                IF (rta:Processed = 0)
!                    SBR.AddToExport('PENDING')
!                ELSE
!                    SBR.AddToExport(CLIP(rta:AcceptReject) & 'ED')
!                END
!                ! Waybill Number
!                SBR.AddToExport(rtn:WaybillNumber)
!                ! Model Number
!                ! Quantity Returned
!                IF (rtn:ExchangeOrder = 0)
!                    SBR.AddToExport('')
!                    SBR.AddToExport(rta:Quantity)
!                ELSE
!                    SBR.AddToExport(rtn:PartNumber)
!                    SBR.AddToExport('1')
!                END
!                ! Credit Note Request Number
!                SBR.AddToExport(rtn:CreditNoteRequestNumber)
!                ! Part Number
!                SBR.AddToExport(rtn:PartNumber)
!                ! Description
!                SBR.AddToExport('''' & rtn:Description)
!                ! Item Cost
!                ! Line Cost
!                IF (rtn:ExchangeOrder = 0)
!                    SBR.AddToExport(FORMAT(rtn:SaleCost,@n_14.2))
!                    SBR.AddToExport(FORMAT(rtn:SaleCost * rta:Quantity,@n_14.2))
!                ELSE
!                    SBR.AddToExport(FORMAT(rtn:ExchangePrice,@n_14.2))
!                    SBR.AddToExport(FORMAT(rtn:ExchangePrice,@n_14.2))
!                END
!                ! Average Cost
!                IF (rtn:ExchangeOrder = 0)
!                    SBR.AddToExport(FORMAT(rtn:PurchaseCost,@n_14.2),,1)
!                ELSE 
!                    SBR.AddToExport(FORMAT(rtn:ExchangePrice,@n_14.2),,1)
!                END
!                
!                kTotalRecords += 1    
!                countRecords# += 1
!                IF (SBR:CancelPressed > 0)
!                    BREAK
!                END
!                
!            END ! LOOP
!            SBR.UpdateProgressText('Records Found: ' & count#,1)
!
!            IF (SBR:CancelPressed > 0)
!                BREAK
!            END
!            
!        END ! LOOP i = 1 TO RECORDS(glo:Queue)
!        
!        SBR.Kill()
!        
!        IF (SBR:CancelPressed = 2)
!            SBR.UpdateProgressText('===============')
!            SBR.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
!                ' ' & FORMAT(clock(),@t1))
!            SBR.EndProgress()
!            REMOVE(SBR:ExportFile)
!        ELSE
!            SBR.UpdateProgressText()
!            SBR.UpdateProgressText('Formatting Document')
!            SBR.UpdateProgressText()
!            ! Build Excel Document
!            If E1.Init(0,0) = 0
!                Case Missive('An error has occurred finding your Excel document.'&|
!                    '<13,10>'&|
!                    '<13,10>Please quit and try again.','ServiceBase 3g',|
!                    'mstop.jpg','/OK')
!                Of 1 ! OK Button
!                End ! Case Missive
!                Exit
!            End !If E1.Init(0,0,1) = 0
!
!            SETCLIPBOARD(SBR.LoadTempData(E1,SBR:ExportFilePath,'Q' & (kTotalRecords+15)))
!
!            E1.NewWorkBook()
!            E1.RenameWorkSheet('Detailed')
!            E1.SelectCells('A1')
!            E1.Paste()
!            SBR.ClearClipboard()
!
!            SBR.FinishFormat(E1,'Q',kTotalRecords + 10,'A8')
!
!            REMOVE(CLIP(kTempFolder) & CLIP(kFileName))
!            E1.SaveAs(CLIP(kTempFolder) & CLIP(kFileName))
!            E1.CloseWorkBook(2)
!            E1.Kill()
!
!            SBR.UpdateProgressText('===============')
!            SBR.UpdateProgressText('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))
!
!            kReportFolder = SetReportsFolder('ServiceBase Export',CLIP(kReportName),glo:WebJob)
!
!            IF (SBR.CopyFinalFile(CLIP(kTempFolder) & Clip(kFileName),CLIP(kReportFolder) & CLIP(kFilename)))
!                ! Couldn't save final file
!                SBR.EndProgress()
!            ELSE
!                SBR.EndProgress(kReportFolder)
!            END
!        END
!        REMOVE(SBR:ExportFilePath)
!
!        Post(EVENT:CloseWindow)
!      
!        
!        
!        
!        
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020771'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('StockReturnForCreditReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RETTYPES.Open
  Relate:RTNAWAIT.Open
  Relate:RTNORDER.Open
  Relate:TRADEACC.Open
  SELF.FilesOpened = True
  locStartDate = TODAY()
  locEndDate = TODAY()
  BRW11.Init(?List,Queue:Browse.ViewPosition,BRW11::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  BRW14.Init(?List:2,Queue:Browse:1.ViewPosition,BRW14::View:Browse,Queue:Browse:1,Relate:RETTYPES,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','StockReturnForCreditReport')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard13.Init(?Sheet1, |                          ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?OK, |                           ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     0, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?locStartDate{Prop:Alrt,255} = MouseLeft2
  ?locEndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW11.Q &= Queue:Browse
  BRW11.RetainRow = 0
  BRW11.AddSortOrder(,tra:Account_Number_Key)
  BRW11.AddLocator(BRW11::Sort0:Locator)
  BRW11::Sort0:Locator.Init(,tra:Account_Number,1,BRW11)
  BRW11.SetFilter('(UPPER(tra:SiteLocation) <<> '''')')
  BIND('tagTrade',tagTrade)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW11.AddField(tagTrade,BRW11.Q.tagTrade)
  BRW11.AddField(tra:Account_Number,BRW11.Q.tra:Account_Number)
  BRW11.AddField(tra:Company_Name,BRW11.Q.tra:Company_Name)
  BRW11.AddField(tra:RecordNumber,BRW11.Q.tra:RecordNumber)
  BRW14.Q &= Queue:Browse:1
  BRW14.RetainRow = 0
  BRW14.AddSortOrder(,rtt:ActiveDescriptionKey)
  BRW14.AddRange(rtt:Active,locTRUE)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,rtt:Description,1,BRW14)
  BIND('tagReturnType',tagReturnType)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW14.AddField(tagReturnType,BRW14.Q.tagReturnType)
  BRW14.AddField(rtt:Description,BRW14.Q.rtt:Description)
  BRW14.AddField(rtt:RecordNumber,BRW14.Q.rtt:RecordNumber)
  BRW14.AddField(rtt:Active,BRW14.Q.rtt:Active)
  BRW11.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW11.AskProcedure = 0
      CLEAR(BRW11.AskProcedure, 1)
    END
  END
  BRW14.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW14.AskProcedure = 0
      CLEAR(BRW14.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:RETTYPES.Close
    Relate:RTNAWAIT.Close
    Relate:RTNORDER.Close
    Relate:TRADEACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','StockReturnForCreditReport')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard13.Validate()
        DISABLE(Wizard13.NextControl())
     ELSE
        ENABLE(Wizard13.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020771'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020771'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020771'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          locStartDate = TINCALENDARStyle1(locStartDate)
          Display(?locStartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          locEndDate = TINCALENDARStyle1(locEndDate)
          Display(?locEndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?VSBackButton
      ThisWindow.Update
         Wizard13.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard13.TakeAccepted()
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      Do Export
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?locStartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?locEndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::12:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::15:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW11.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tagTrade = ''
    ELSE
      tagTrade = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagTrade = '*')
    SELF.Q.tagTrade_Icon = 2
  ELSE
    SELF.Q.tagTrade_Icon = 1
  END


BRW11.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW11.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW11.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW11::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW11::RecordStatus=ReturnValue
  IF BRW11::RecordStatus NOT=Record:OK THEN RETURN BRW11::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW11::RecordStatus
  RETURN ReturnValue


BRW14.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = rtt:Description
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tagReturnType = ''
    ELSE
      tagReturnType = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tagReturnType = '*')
    SELF.Q.tagReturnType_Icon = 2
  ELSE
    SELF.Q.tagReturnType_Icon = 1
  END


BRW14.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW14::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW14::RecordStatus=ReturnValue
  IF BRW14::RecordStatus NOT=Record:OK THEN RETURN BRW14::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = rtt:Description
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::15:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW14::RecordStatus
  RETURN ReturnValue

Wizard13.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW11.Q &= NULL) ! Has Browse Object been initialized?
       BRW11.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW14.Q &= NULL) ! Has Browse Object been initialized?
       BRW14.ResetQueue(Reset:Queue)
    END

Wizard13.TakeBackEmbed PROCEDURE
   CODE

Wizard13.TakeNextEmbed PROCEDURE
   CODE

Wizard13.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
