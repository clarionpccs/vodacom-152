

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03001.INC'),ONCE        !Local module procedure declarations
                     END








Courier_Collection_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
jobs_queue           QUEUE,PRE(jobque)
ref_number           REAL
surname              STRING(30)
type                 STRING(3)
                     END
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(30)
job_number_temp      STRING(9)
total_lines_temp     REAL
consignment_number_temp STRING(30)
address_line_temp    STRING(90)
customer_name_temp   STRING(40)
company_name_temp    STRING(30)
telephone_Number_temp STRING(15)
TextBoxLineNdx       SHORT
TextBoxHeight        LONG
TextBoxLines         LONG
TextBoxStartLine     LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Company_Name_Delivery)
                       PROJECT(job:Postcode_Delivery)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Telephone_Delivery)
                       JOIN(jbn:RefNumberKey,job:Ref_Number)
                         PROJECT(jbn:Delivery_Text)
                       END
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Courier Collection Report'),AT(438,2094,10917,4427),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),LANDSCAPE,THOUS
                       HEADER,AT(438,323,10917,1719),USE(?unnamed)
                         STRING(@s30),AT(156,104,3281,260),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s60),AT(156,365,3281,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s60),AT(156,521,3281,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s60),AT(156,677,3281,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,833,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Date To:'),AT(7396,990),USE(?String22:2),TRN
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(625,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN
                         STRING(@s20),AT(625,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('Date From:'),AT(7396,781),USE(?String22),TRN
                         STRING(@d6b),AT(8646,781),USE(GLO:Select2),TRN,RIGHT,FONT(,,,FONT:bold)
                         STRING(@d6b),AT(8646,990),USE(GLO:Select3),TRN,RIGHT,FONT(,,,FONT:bold)
                         STRING(@n-7),AT(8646,1198),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,,,FONT:bold)
                         STRING(@s255),AT(625,1354,3281,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN
                         STRING('Page:'),AT(7396,1198),USE(?PagePrompt),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:2)
DETAIL                   DETAIL,AT(,,,354),USE(?DetailBand)
                           STRING(@s9),AT(104,0),USE(job:Ref_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s50),AT(4271,0,3177,156),USE(address_line_temp),TRN,FONT(,8,,)
                           STRING(@s10),AT(7448,0),USE(job:Postcode_Delivery),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(8125,0),USE(job:Telephone_Delivery),TRN,LEFT,FONT(,8,,)
                           TEXT,AT(9115,0,1615,313),USE(jbn:Delivery_Text),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(729,0,1875,208),USE(customer_name_temp),TRN,FONT(,8,,)
                           STRING(@s25),AT(2656,0),USE(job:Company_Name_Delivery),TRN,FONT(,8,,)
                         END
                         FOOTER,AT(438,7000,,656),USE(?unnamed:3),ABSOLUTE
                           STRING('Total Lines: '),AT(7396,208),USE(?String36),TRN
                           STRING(@s9),AT(8646,208),USE(total_lines_temp),TRN,LEFT,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(438,6938,10917,708),USE(?unnamed:5)
                         TEXT,AT(104,52,7083,573),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(438,417,10917,7417),USE(?unnamed:4)
                         IMAGE,AT(0,0,10938,7448),USE(?Image1)
                         STRING('COURIER COLLECTION REPORT'),AT(7396,104),USE(?String35),TRN,FONT(,14,,FONT:bold)
                         STRING('Job No'),AT(104,1406),USE(?String17),TRN,FONT(,8,,FONT:bold)
                         STRING('Name'),AT(729,1406),USE(?String18),TRN,FONT(,8,,FONT:bold)
                         STRING('Company Name'),AT(2656,1406),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Address'),AT(4271,1406),USE(?String119),TRN,FONT(,8,,FONT:bold)
                         STRING('Postcode'),AT(7448,1406),USE(?String20),TRN,FONT(,8,,FONT:bold)
                         STRING('Delivery Text'),AT(9115,1406),USE(?String21),TRN,FONT(,8,,FONT:bold)
                         STRING('Telephone No'),AT(8125,1406),USE(?String43),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Courier_Collection_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Courier_Collection_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:STANTEXT.Open
  Relate:TRANTYPE.Open
  Access:JOBNOTES.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job:Date_Booked_Key)
      Process:View{Prop:Filter} = |
      'job:date_booked >= GLO:Select1 AND job:date_booked <<= GLO:Select2'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        print# = 0
        If job:workshop <> 'YES'
            access:trantype.clearkey(trt:transit_type_key)
            trt:transit_type = job:transit_type
            if access:trantype.fetch(trt:transit_type_key) = Level:Benign
                If trt:courier_collection_report = 'YES'
                    print# = 1
                End!If trt:courier_collection_report = 'YES'
            end!if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        End!If job:workshop <> 'YES'
        If print# = 1
            if job:title = '' and job:initial = ''
                customer_name_temp = clip(job:surname)
            elsif job:title = '' and job:initial <> ''
                customer_name_temp = clip(job:surname) & ', ' & clip(job:initial)
            elsif job:title <> '' and job:initial = ''
                customer_name_temp = Clip(job:surname) & ', ' & clip(job:title)
            elsif job:title <> '' and job:initial <> ''
                customer_name_temp = Clip(job:surname) & ', ' & clip(job:title) & ' ' & clip(job:initial)
            else
                customer_name_temp = ''
            end
        
            If job:address_line3_delivery = ''
                address_line_temp = Clip(job:address_line1_delivery) & ', ' & Clip(job:address_line2_delivery)
            Else
                address_line_temp = Clip(job:address_line1_delivery) & ', ' & CLip(job:address_line2_delivery) & ', ' & Clip(job:address_line3_delivery)
            End
            total_lines_temp += 1
            tmp:RecordsCount += 1
            Print(rpt:detail)
        End!If print# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Courier_Collection_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:STANTEXT.Close
    Relate:TRANTYPE.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','COURIER COLLECTION REPORT')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'COURIER COLLECTION REPORT'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvlan.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'COURIER COLLECTION REPORT'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'COURIER COLLECTION REPORT'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Courier_Collection_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage







Special_Delivery_Criteria PROCEDURE (f_type)          !Generated from procedure template - Window

FilesOpened          BYTE
tmp:printer          STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:summary          BYTE(0)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
Window               WINDOW('Special Delivery Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Special Delivery Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Report Criteria'),USE(?Tab1)
                           PROMPT('Courier'),AT(240,158),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(316,158,124,10),USE(GLO:Select1),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Despatched From'),AT(240,180),USE(?glo:select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(316,180,64,10),USE(GLO:Select2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(384,175),USE(?PopCalendar),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('To'),AT(240,202,17,11),USE(?glo:select3:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(316,202,64,10),USE(GLO:Select3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(384,198),USE(?PopCalendar:2),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           CHECK('Group Consignment Numbers'),AT(315,223),USE(tmp:summary),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Summarize Consignment Numbers'),TIP('Summarize Consignment Numbers'),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020176'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Special_Delivery_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:COURIER.Open
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Special_Delivery_Criteria')
  Bryan.CompFieldColour()
  ?glo:select2{Prop:Alrt,255} = MouseLeft2
  ?glo:select3{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB1.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(cou:Courier_Key)
  FDCB1.AddField(cou:Courier,FDCB1.Q.cou:Courier)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Special_Delivery_Criteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020176'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020176'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020176'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?glo:select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select3 = TINCALENDARStyle1(GLO:Select3)
          Display(?glo:select3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If glo:select1 = ''
          Select(?glo:select1)
          error# = 1
      End
      If error# = 0 And glo:select2 = ''
          Select(?glo:select2)
          error# = 1
      End
      If error# = 0 And glo:select3 = ''
          Select(?glo:select3)
          error# = 1
      End
      If error# = 0
          Case f_type
              Of 'SPECIAL DELIVERY'
                  Special_Delivery_Report(tmp:Summary)
              Of 'COURIER COLLECTION'
                  Courier_Collection_Report
          End!Case f_type
      
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select3
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      glo:select1 = ''
      glo:select2 = Today()
      glo:select3 = Today()
      Case f_type
          Of 'SPECIAL DELIVERY'
              ?WindowTitle{prop:text} = 'Special Delivery Report Criteria'
              ?tmp:Summary{prop:Hide} = 0
          Of 'COURIER COLLECTION'
              ?WindowTitle{prop:text} = 'Courier Collection Report Criteria'
              ?tmp:Summary{prop:Hide} = 1
      End!Case f_type
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






Exchange_Exceptions_Report PROCEDURE(f_type)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
title_temp           STRING(26)
update_status_temp   STRING(3)
invoice_name_temp    STRING(40)
pos                  STRING(255)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
count_temp           REAL
customer_temp        STRING(30)
days_overdue_temp    REAL
date_range_temp      STRING(30)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:ESN)
                       PROJECT(job:Exchange_Consignment_Number)
                       PROJECT(job:Exchange_Despatched)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Telephone_Collection)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Exchange Exceptions Report'),AT(365,2760,7521,7615),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,1677),USE(?unnamed)
                         STRING(@s30),AT(156,83,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,365,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,521,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,677,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,833,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Courier:'),AT(4948,521),USE(?String35),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,1094,260,198),USE(?Telephone),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1094),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Printed By:'),AT(4948,1146),USE(?String42),TRN,FONT(,8,,)
                         STRING(@s60),AT(5833,1146),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5833,833),USE(date_range_temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(4948,833),USE(?String36),TRN,FONT(,8,,)
                         STRING('Fax: '),AT(156,1250),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1250),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1406,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1406),USE(?String19:2),TRN,FONT(,9,,)
                         STRING(@s3),AT(5833,1458),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5833,521),USE(invoice_name_temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed:'),AT(4948,1302),USE(?String31),TRN,FONT(,8,,)
                         STRING(@D6),AT(5833,1302),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@T3),AT(6510,1302),USE(ReportRunTime),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Account No.:'),AT(4948,677),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,677,1000,240),USE(cou:Account_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Page Number'),AT(4948,1458,917,198),USE(?String34),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@s15),AT(729,0),USE(job:Exchange_Consignment_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@p<<<<<<<<#p),AT(0,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,8,,)
                           STRING(@s20),AT(4063,0),USE(customer_temp),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(5417,0),USE(job:Telephone_Collection),TRN,LEFT,FONT(,8,,)
                           STRING(@n6b),AT(7031,0),USE(days_overdue_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s20),AT(1719,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s16),AT(3021,0),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                           STRING(@d6b),AT(6385,0),USE(job:Exchange_Despatched),TRN,RIGHT,FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,406),USE(?unnamed:4)
                           LINE,AT(365,83,6802,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(5760,156),USE(?String30),TRN,FONT(,10,,FONT:bold)
                           STRING(@n8),AT(6750,156),USE(count_temp),TRN,LEFT,FONT(,10,,FONT:bold)
                         END
                       END
                       FOOTER,AT(365,10333,7521,1156),USE(?unnamed:2),FONT('Arial',,,)
                         LINE,AT(365,52,6802,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(156,104,7198,958),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11198),USE(?Image1)
                         STRING(@s26),AT(4375,104),USE(title_temp),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING('Job No'),AT(156,2083),USE(?String29),TRN,FONT(,8,,FONT:bold)
                         STRING('Consign. No'),AT(698,2083),USE(?String33),TRN,FONT(,8,,FONT:bold)
                         STRING('Model No'),AT(1688,2083),USE(?String39),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(2990,2083),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('Customer'),AT(4031,2083),USE(?String37),TRN,FONT(,8,,FONT:bold)
                         STRING('Tel. No.'),AT(5385,2083),USE(?String38),TRN,FONT(,8,,FONT:bold)
                         STRING('Desp.'),AT(6354,2083),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Days Over'),AT(6823,2083),USE(?String40),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Exchange_Exceptions_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Exchange_Exceptions_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:STAHEAD.Open
  Relate:STANTEXT.Open
  Access:COURIER.UseFile
  Access:AUDIT.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job:Ref_Number_Key)
      Process:View{Prop:Filter} = ''
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        print# = 0
        If job:exchange_courier = glo:select1
            If job:workshop <> 'YES' And job:exchange_despatched <> ''
                Weekend_Routine(JOB:Exchange_Despatched,2,end_date")
                If Today() > end_date"
                    print# = 1
                End!If Today() < end_date"
        
                If print# = 1
                    Days_Between_Routine(end_date",Today(),days")
                    days_overdue_temp = days"
                    count_temp += 1
                    If job:surname = ''
                        customer_temp = Clip(job:company_Name)
                    Else!If job:surname = ''
                        if job:title = '' and job:initial = ''
                            customer_temp = clip(job:surname)
                        elsif job:title = '' and job:initial <> ''
                            customer_temp = clip(job:surname) & ', ' & clip(job:initial)
                        elsif job:title <> '' and job:initial = ''
                            customer_temp = clip(job:surname) & ', ' & clip(job:title)
                        elsif job:title <> '' and job:initial <> ''
                            customer_temp = clip(job:surname) & ', ' & clip(job:title) & ' ' & clip(job:initial)
                        else
                            customer_temp = ''
                        end
                    End!If job:surname = ''
                    tmp:RecordsCount += 1
                    Print(rpt:detail)
        
                    If update_status_temp = 'YES'
                        GetStatus(950,1,'JOB') !exchange claim made
        
                        access:jobs.update()
                    End!If update_status_temp = 'YES'
        
                    CASE f_Type
                        OF 'E'
                            IF (AddToAudit(job:ref_number,'JOB','REPORT GENERATION','THIS JOB WAS INCLUDED IN THE EXCHANGE EXCEPTIONS REPORT'))
                            END ! IF
                        OF 'C'
                            IF (AddToAudit(job:ref_number,'JOB','REPORT GENERATION','THIS JOB WAS INCLUDED IN THE EXCHANGE CLAIMS REPORT'))
                            END ! IF
                    END ! CASE
        
                End!If print# = 1
            End!If job:workshop <> 'YES' And job:exchange_despatch <> ''
        End!If job:exchange_courier = glo:select1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Exchange_Exceptions_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:STAHEAD.Close
    Relate:STANTEXT.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  If f_type = 'C'
      Case Missive('Do you wish to update the Status of all the jobs in this report to:'&|
        '<13,10>'&|
        '<13,10>950 EXCHANGE CLAIM MADE?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              update_status_temp = 'YES'
          Of 1 ! No Button
              update_status_temp = 'NO'
      End ! Case Missive
  End!If f
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  access:courier.clearkey(cou:courier_key)
  cou:courier = glo:select1
  if access:courier.fetch(cou:courier_key) = Level:Benign
      invoice_name_temp   = cou:courier
      address_line1_temp  = cou:address_line1
      address_line2_temp  = cou:address_line2
      If sup:address_line3 = ''
          address_line3_temp = cou:postcode
          address_line4_temp = ''
      Else
          address_line3_temp  = cou:address_line3
          address_line4_temp  = cou:postcode
      End
  end
  count_temp = 0

  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = Clip(use:forename) & ' ' & Clip(use:surname)

  date_range_temp = Format(glo:select2,@d6) & ' - ' & Format(glo:select3,@d6)
  Case f_type
      Of 'E'
          title_temp = 'EXCHANGE EXCEPTIONS REPORT'
          access:stantext.clearkey(stt:description_key)
          stt:description = 'EXCHANGE EXCEPTIONS REPORT'
          access:stantext.fetch(stt:description_key)
      Of 'C'
          title_temp = 'EXCHANGE CLAIMS REPORT'
          access:stantext.clearkey(stt:description_key)
          stt:description = 'EXCHANGE CLAIMS REPORT'
          access:stantext.fetch(stt:description_key)
  End!Case
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Exchange_Exceptions_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage







Exchange_Exceptions_Criteria PROCEDURE (f_type)       !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
QuickWindow          WINDOW('Window'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Exchange Exceptions Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Exchange Exceptions Criteria'),USE(?Tab1)
                           PROMPT('Courier'),AT(236,172),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(316,172,124,10),USE(GLO:Select1),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Start Date'),AT(236,192),USE(?glo:select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6b),AT(316,192,64,10),USE(GLO:Select2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ
                           BUTTON,AT(384,190),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(236,214),USE(?glo:select3:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6b),AT(316,214,64,10),USE(GLO:Select3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(384,210),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020161'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Exchange_Exceptions_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:COURIER.Open
  SELF.FilesOpened = True
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Exchange_Exceptions_Criteria')
  Bryan.CompFieldColour()
  ?glo:select2{Prop:Alrt,255} = MouseLeft2
  ?glo:select3{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB1.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(cou:Courier_Key)
  FDCB1.AddField(cou:Courier,FDCB1.Q.cou:Courier)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Exchange_Exceptions_Criteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020161'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020161'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020161'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?glo:select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select3 = TINCALENDARStyle1(GLO:Select3)
          Display(?glo:select3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      Exchange_Exceptions_Report(f_type)
      ThisWindow.Reset
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      Clear(glo:G_Select1)
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select3
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Clear(glo:G_Select1)
      glo:select2 = Deformat('1/1/1990',@d6)
      glo:select3 = Today()
      Display()
      
      Case f_type
          Of 'E'
              ?WindowTitle{prop:text} = 'Exchange Exceptions Report Criteria'
          Of 'C'
              ?WindowTitle{prop:text} = 'Exchange Claims Report Criteria'
      End!Case f_type
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Completed_Job_Stats_Export PROCEDURE                  ! Declare Procedure
savepath             STRING(255)
tmp:LabourValue      REAL,DIM(50)
tmp:PartsValue       REAL,DIM(50)
tmp:WarLabourValue   REAL,DIM(50)
tmp:WarPartsValue    REAL,DIM(50)
tmp:Booked           LONG,DIM(50)
tmp:LineLabour       REAL
tmp:LineParts        REAL
tmp:LineWarLabour    REAL
tmp:LineWarParts     REAL
tmp:LineCost         REAL
tmp:LineWarCost      REAL
tmp:TotalLabour      REAL
tmp:TotalParts       REAL
tmp:TotalChar        REAL
tmp:TotalWarLabour   REAL
tmp:TotalWarParts    REAL
tmp:TotalWar         REAL
save_job_id          USHORT,AUTO
save_mod_id          USHORT,AUTO
tmp:ChargeType       STRING(60),DIM(50)
tmp:JobTotal         LONG,DIM(50)
save_cha_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
!Shell Execute Variables
AssocFile    CString(255)
Operation    CString(255)
Param        CString(255)
Dir          CString(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:CHARTYPE.Open
   Relate:JOBS.Open
   Relate:MANUFACT.Open
   Relate:MODELNUM.Open
   Relate:DEFAULTS.Open
   Relate:SUBTRACC.Open
   Relate:TRADEACC.Open
    Set(defaults)
    access:Defaults.next()
    savepath = path()
    If def:exportpath <> ''
        glo:file_name = Clip(def:exportpath) & '\JOBSTATS.CSV'
    Else!If def:exportpath <> ''
        glo:file_name = 'C:\JOBSTATS.CSV'
    End!If def:exportpath <> ''
        if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                file:keepdir + file:noerror + file:longname)
        !failed
        setpath(savepath)
    else!if not filedialog
        !found
        setpath(savepath)
        Remove(glo:file_name)
        access:expgendm.open()
        access:expgendm.usefile()
        Display()
        recordspercycle     = 25
        recordsprocessed    = 0
        percentprogress     = 0
        setcursor(cursor:wait)
        open(progresswindow)
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'

        recordstoprocess    = Records(Modelnum)

        tmp:TotalLabour     = 0
        tmp:TotalParts      = 0
        tmp:TotalChar       = 0
        tmp:TotalWarLabour  = 0
        tmp:TotalWarParts   = 0
        tmp:TotalWar        = 0

        Clear(exp:record)
        exp:line1[1] = 'Completed Jobs Statistics Export'
        access:expgendm.insert()
        exp:line1[1] = 'Jobs Completed Between: '
        exp:line1[2] = Format(glo:select1,@d6)
        exp:line1[3] = Format(glo:select2,@d6)
        access:expgendm.insert()

        Clear(exp:record)
        access:expgendm.insert()

        If glo:select3 = 1
            Clear(exp:record)
            exp:line1[1] = 'Account Number:'
            exp:line1[2] = glo:select4
            access:expgendm.insert()
        End!If glo:select3 = 1

        Clear(exp:record)
        exp:line1[1] = 'Make/Model'
        x# = 1
        save_cha_id = access:chartype.savefile()
        access:chartype.clearkey(cha:warranty_key)
        set(cha:warranty_key)
        loop
            if access:chartype.next()
               break
            end !if
            x# += 1
            exp:line1[x#] = Clip(cha:charge_type)
            tmp:ChargeType[x#] = exp:line1[x#]
        end !loop
        access:chartype.restorefile(save_cha_id)
        exp:line1[x#+1]     = 'Total'
        exp:line1[x#+2]     = 'Labour (Chargeable)'
        exp:line1[x#+3]     = 'Parts (Chargeable)'
        exp:line1[x#+4]     = 'Total (Chargeable Ex V.A.T.)'
        exp:line1[x#+5]     = 'Labour (Warranty)'
        exp:line1[x#+6]     = 'Parts (Warranty)'
        exp:line1[x#+7]     = 'Total (Warranty Ex V.A.T.)'
        all_jobs# = 0

        access:expgendm.insert()

!loop through models

        Clear(exp:record)
        save_mod_id = access:modelnum.savefile()
        access:modelnum.clearkey(mod:manufacturer_key)
        set(mod:manufacturer_key)
        loop
            if access:modelnum.next()
               break
            end !if
            Do GetNextRecord2
            exp:line1[1] = Clip(mod:manufacturer) & ' - ' & CLip(mod:model_number)
            line_count# = 0
            tmp:LineLabour      = 0
            tmp:LineParts       = 0
            tmp:LineWarLabour   = 0
            tmp:LineWarParts    = 0
            tmp:LineCost        = 0
            tmp:LineWarCost     = 0

!loop through the saved charge types
            Loop y# = 2 to x#
                count_jobs# = 0

!loop through jobs in completed date order and count
                save_job_id = access:jobs.savefile()
                access:jobs.clearkey(job:modelcompkey)
                job:model_number   = mod:model_number
                job:date_completed = glo:select1
                set(job:modelcompkey,job:modelcompkey)
                loop
                    if access:jobs.next()
                       break
                    end !if
                    if job:model_number   <> mod:model_number      |
                    or job:date_completed > glo:select2      |
                        then break.  ! end if

                    If glo:select3 = 1
                        Case glo:select5
                            Of 'MAI'
                                !The selected account was a main account
                                access:subtracc.clearkey(sub:account_number_key)
                                sub:account_number  = job:account_number
                                If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                                    !Found
                                    !Get the Sub Account and see if the Sub's Main matched the criteria
                                    If sub:main_account_number <> glo:select4
                                        Cycle
                                    End!If sub:main_account_number <> glo:select4
                                Else! If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                                    !Error
                                End! If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign

                            Of 'SUB'
                                If job:account_number <> glo:select4
                                    Cycle
                                End!If job:account_number <> glo:select4
                        End!Case glo:select5
                    End!If glo:select3 = 1
                    If job:chargeable_job = 'YES' and job:warranty_job = 'YES'
                        If job:charge_type <> tmp:ChargeType[y#] And |
                            job:warranty_charge_type <> tmp:ChargeType[y#]
                            Cycle
                        End!job:warranty_charge_type <> tmp:ChargeType[y#]
                    Else!If job:chargeable_job = 'YES' and job:warranty_job = 'YES'
                        If job:chargeable_job = 'YES'
                            If job:charge_type <> tmp:ChargeType[y#]
                                Cycle
                            End!If job:charge_type <> tmp:ChargeType[y#]
                        End!If job:chargeable_job = 'YES'
                        If job:warranty_job = 'YES'
                            If job:warranty_charge_type <> tmp:ChargeType[y#]
                                Cycle
                            End!If job:warranty_charge_type <> tmp:ChargeType[y#]
                        End!If job:warranty_job = 'YES'

                    End!If job:chargeable_job = 'YES' and job:warranty_job = 'YES'

                    count_jobs# += 1
                    tmp:LineLabour      += Round(job:labour_cost,.01)
                    tmp:LineParts       += Round(job:parts_cost,.01)
                    tmp:LineWarLabour   += Round(job:labour_cost_warranty,.01)
                    tmp:LineWarParts    += Round(job:parts_cost_warranty,.01)
                    tmp:LineCost        += Round(job:sub_total,.01)
                    tmp:LineWarCost     += Round(job:sub_total_Warranty,.01)
                    If job:date_booked => glo:select1 And job:date_booked <= glo:select2
                        tmp:booked[y#] += 1
                    End!If job:date_booked => glo:select1 And job:date_booked <= glo:select2
                    tmp:LabourValue[y#]     += Round(job:labour_cost,.01)
                    tmp:PartsValue[y#]      += Round(job:parts_cost,.01)
                    tmp:WarLabourValue[y#]  += Round(job:labour_cost_warranty,.01)
                    tmp:WarPartsValue[y#]   += Round(job:parts_cost_warranty,.01)

                end !loop
                access:jobs.restorefile(save_job_id)
                exp:line1[y#]   = count_jobs#
                tmp:JobTotal[y#]    += count_jobs#
                line_count# += count_jobs#
            End!Loop y# = 2 to x#
            exp:line1[x#+1]     = line_count#
            exp:line1[x#+2]     = tmp:LineLabour
            exp:line1[x#+3]     = tmp:LineParts
            exp:line1[x#+4]     = tmp:LineCost
            exp:line1[x#+5]     = tmp:LineWarLabour
            exp:line1[x#+6]     = tmp:LineWarParts
            exp:line1[x#+7]     = tmp:LineWarCost

            access:expgendm.insert()

            tmp:TotalLabour     += tmp:linelabour
            tmp:TotalParts      += tmp:lineparts
            tmp:TotalChar       += tmp:linecost
            tmp:TotalWarLabour  += tmp:linewarlabour
            tmp:TotalWarParts   += tmp:linewarparts
            tmp:TotalWar        += tmp:linewarcost
            all_jobs#           += line_count#

        end !loop
        access:modelnum.restorefile(save_mod_id)

        Clear(exp:record)
        exp:line1[1] = 'Totals'
        Loop y# = 2 to x#
            exp:line1[y#]   = tmp:JobTotal[y#]
        End!Loop y# = 2 to x#
        exp:line1[x#+1]     = all_jobs#
        exp:line1[x#+2]     = tmp:TotalLabour
        exp:line1[x#+3]     = tmp:TotalParts
        exp:line1[x#+4]     = tmp:TotalChar
        exp:line1[x#+5]     = tmp:TotalWarLabour
        exp:line1[x#+6]     = tmp:TotalWarParts
        exp:line1[x#+7]     = tmp:TotalWar

        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Labour Value (Chargeable)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:LabourValue[y#]
        End!Loop y# = 2 to x#

        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Parts Value (Chargeable)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:PartsValue[y#]
        End!Loop y# = 2 to x#
        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Total Value (Chargeable Ex V.A.T.)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:PartsValue[y#] + tmp:LabourValue[y#]
        End!Loop y# = 2 to x#
        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Labour Value (Warranty)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:WarLabourValue[y#]
        End!Loop y# = 2 to x#

        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Parts Value (Warranty)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:WarPartsValue[y#]
        End!Loop y# = 2 to x#
        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Total Value (Chargeable Ex V.A.T.)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:WarPartsValue[y#] + tmp:WarLabourValue[y#]
        End!Loop y# = 2 to x#
        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Total Booked'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:Booked[y#]
        End!Loop y# = 2 to x#
        access:expgendm.insert()


        setcursor()
        close(progresswindow)
        access:expgendm.close()

        Case Missive('Export Completed.'&|
          '<13,10>'&|
          '<13,10>Do you wish to view the export file now?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
                AssocFile = Clip(glo:file_name)
                Param = ''
                Dir = ''
                Operation = 'Open'
                ShellExecute(GetDesktopWindow(), Operation, AssocFile, Param, Dir, 5)
            Of 1 ! No Button
        End ! Case Missive
    end!if not filedialog

   Relate:CHARTYPE.Close
   Relate:JOBS.Close
   Relate:MANUFACT.Close
   Relate:MODELNUM.Close
   Relate:DEFAULTS.Close
   Relate:SUBTRACC.Close
   Relate:TRADEACC.Close
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Date_Range PROCEDURE (f_title)                        !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Date Range'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Date Range'),USE(?Tab1)
                           PROMPT('Start Date'),AT(248,196),USE(?glo:select1:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(324,196,64,10),USE(GLO:Select1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(AltD),CAP
                           BUTTON,AT(392,192),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(248,212),USE(?glo:select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(324,212,64,10),USE(GLO:Select2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(AltD),UPR
                           BUTTON,AT(392,210),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(304,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020160'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Date_Range')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?WindowTitle{prop:text} = Clip(f_title)
  If f_title =  'Completed Jobs Statistics Export'
      ?WindowTitle{prop:text} = 'Completed Date Range'
  End!If f_title =  'Completed Jobs Statistics Export'
  case f_title
      Of 'Courier Collection Report'
          glo:select1 = Today()
      Else
          glo:select1 = Deformat('1/1/1990',@d6b)
  End!case f_title
  glo:select2 = Today()
  Display()
      ! Save Window Name
   AddToLog('Window','Open','Date_Range')
  Bryan.CompFieldColour()
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
  ?glo:select2{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','Date_Range')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020160'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020160'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020160'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?glo:select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      Case f_title
      
          Of 'Courier Collection Report'
              Courier_Collection_Report
          Of 'QA Failure Report'
              QA_Failure_Report
          Of 'Completed Jobs Statistics Export'
              Completed_Job_Stats_Export
          Of 'Sagem Daily Export'
              SagemDailyExport
      End!
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = ''
      glo:select2 = ''
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?GLO:Select1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?GLO:Select1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, AlertKey)
      glo:select1 = Deformat('1/1/1990',@d6b)
      Display(?glo:select1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, AlertKey)
    END
  OF ?GLO:Select2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select2, AlertKey)
      glo:select2 = Today()
      Display(?glo:select2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select2, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






QA_Failure_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_aud_id          USHORT,AUTO
Reason_Queue_Temp    QUEUE,PRE(REAQUE)
Reason               STRING(60)
Number               LONG
                     END
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
total_temp           LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(QAREASON)
                       PROJECT(qar:Reason)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Status Summary Report'),AT(396,2760,7521,6958),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Date Range: '),AT(5000,156),USE(?String22),TRN,FONT(,8,,)
                         STRING(@s3),AT(5781,781),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(5781,156),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(6615,156),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?String24),TRN,FONT(,8,,)
                         STRING(@s60),AT(5781,365),USE(tmp:PrintedBy),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(5781,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(' to '),AT(6406,156),USE(?String23),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,781),USE(?String59),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,229),USE(?DetailBand)
                           STRING(@s60),AT(2115,0),USE(qar:Reason),TRN,LEFT
                           STRING(@s8),AT(4667,0),USE(count#),TRN,RIGHT
                         END
                         FOOTER,AT(0,0),USE(?unnamed:2)
                           LINE,AT(2031,52,3438,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of QA Failures'),AT(2115,125),USE(?String21),TRN
                           STRING(@n-14),AT(4260,125),USE(total_temp),TRN,RIGHT,FONT(,,,FONT:bold)
                         END
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('QA FAILURE REPORT'),AT(4271,0,3177,260),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,885),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,885),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1042),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('Failure Reason'),AT(2135,2083),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('No Of Occurances'),AT(4792,2083),USE(?String45),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('QA_Failure_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','QA_Failure_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:QAREASON.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  
  
  RecordsToProcess = RECORDS(QAREASON)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(QAREASON,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(qar:ReasonKey)
      Process:View{Prop:Filter} = ''
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        count# = 0
        save_aud_id = access:audit.savefile()
        access:audit.clearkey(aud:actiononlykey)
        aud:action = 'QA REJECTION MANUAL: ' & Clip(qar:reason)
        aud:date   = glo:select1
        set(aud:actiononlykey,aud:actiononlykey)
        loop
            if access:audit.next()
               break
            end !if
            if aud:action <> 'QA REJECTION MANUAL: ' & Clip(qar:reason)      |
            or aud:date   > glo:select2      |
                then break.  ! end if
            count# += 1
        end !loop
        access:audit.restorefile(save_aud_id)
        If count# <> 0
            Print(rpt:detail)
            total_temp += count#
        End!If count# <> 0
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(QAREASON,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','QA_Failure_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:QAREASON.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'QAREASON')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='QA_Failure_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage







SagemDailyExport     PROCEDURE                        ! Declare Procedure
savepath             STRING(255)
local:FileName       STRING(255),STATIC
save_job_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_mod_id          USHORT,AUTO
count                LONG
tmp:warrdate         DATE
save_aud_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:count            LONG
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,imm,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
       button('Cancel'),at(54,44,56,16),use(?cancel),left,icon('cancel.gif')
     end

Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(local:FileName),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(1030)
          . . .

!out_file    DOS,PRE(ouf),NAME(filename)
Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
EDI_No         String(15)
Order_No       STRING(28)
Customer       STRING(28)
Warranty       STRING(5)
IMEI_In        STRING(18)
Sag_Ref_In     STRING(12)
Date_Code      STRING(10)
IMEI_Out       STRING(18)
Sag_Ref_Out    STRING(12)
Battery        STRING(12)
Charger        STRING(12)
Antenna        STRING(6)
DOP            STRING(11)
Booked         STRING(11)
Est_Out        STRING(11)
Est_In         STRING(11)
Despatched     STRING(11)
Cust_Code      STRING(8)
Fault_Code     STRING(8)
Soft_In        STRING(14)
Soft_Out       STRING(14)
Repair_Level   STRING(8)
Part1          STRING(12)
Part2          STRING(12)
Part3          STRING(12)
Part4          STRING(12)
Part5          STRING(12)
Part6          STRING(12)
Part7          STRING(12)
Part8          STRING(12)
Part9          STRING(12)
Refurb1        STRING(12)
Refurb2        STRING(12)
Refurb3        STRING(12)
Refurb4        STRING(12)
Refurb5        STRING(12)
Refurb6        STRING(12)
Refurb7        STRING(12)
RFS            STRING(12)
ModelNumber    String(30)
Re_Rep         String(30)
            .

Out_Header GROUP,OVER(Ouf:Out_Group),PRE(L2)
field1         STRING(11)
field2         STRING(33)
field3         STRING(11)
filer          STRING(255)
filer1         STRING(23)
            .
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:DEFAULTS.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS.Open
   Relate:MANUFACT.Open
   Relate:MODELNUM.Open
    savepath = path()
    set(defaults)
    access:defaults.next()
    If def:exportpath <> ''
        local:FileName = Clip(def:exportpath) & '\SAGEMEXP.RPT'
    Else!If def:exportpath <> ''
        local:FileName = 'C:\SAGEMEXP.RPT'
    End!If def:exportpath <> ''

    if not filedialog ('Choose File',local:FileName,'RPT Files|*.RPT|All Files|*.*', |
                file:keepdir + file:noerror + file:longname)
        !failed
        setpath(savepath)
    else!if not filedialog
        !found
        setpath(savepath)

        Remove(local:FileName)

        Open(out_file)
        If Error()
            Create(out_file)
            Open(out_file)
            empty(out_file)
        Else
            Empty(out_file)
        End!If Error()

        access:manufact.clearkey(man:manufacturer_key)
        man:manufacturer = 'SAGEM'
        if access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
            tmp:warrdate    = Today() - man:warranty_period
        End!if access:manufact.tryfetch(man:manufacturerkey) = Level:Benign

        Do Export2

        recordspercycle     = 25
        recordsprocessed    = 0
        percentprogress     = 0
        open(progresswindow)
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'

        recordstoprocess    = Records(MODELNUM)

    !---before routine

        !All incomplete jobs, expect Exchanges
        !Loop through all the sagem models, and then all the
        !incomplete jobs for that model. That should be quicker
        !than just going through the incomplete jobs.

        tmp:Count = 0
        Save_mod_ID = Access:MODELNUM.SaveFile()
        Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
        mod:Manufacturer = 'SAGEM'
        Set(mod:Manufacturer_Key,mod:Manufacturer_Key)
        Loop
            If Access:MODELNUM.NEXT()
               Break
            End !If
            If mod:Manufacturer <> 'SAGEM'      |
                Then Break.  ! End If
            0{prop:Text} = ?progress:pcttext{prop:text}
            ?progress:userstring{prop:Text} = 'Found: ' & tmp:Count
            do getnextrecord2
            do cancelcheck
            if tmp:cancel = 1
                break
            end!if tmp:cancel = 1

            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:ModelCompKey)
            job:Model_Number   = mod:Model_Number
            job:Date_Completed = 0
            Set(job:ModelCompKey,job:ModelCompKey)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Model_Number   <> mod:Model_Number      |
                Or job:Date_Completed <> 0      |
                    Then Break.  ! End If

                If job:Exchange_Unit_Number <> ''
                    Cycle
                End !If job:Exchange_Unit_Number <> ''
                Do Export
                If job:date_booked => tmp:warrdate
                    L1:Warranty     = '"UW";'
                Else!If job:datebooked => tmp:warrdate
                    L1:Warranty     = '"OW";'
                End!If job:datebooked => tmp:warrdate
                tmp:Count += 1
                ADD(out_file)

            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)

        End !Loop
        Access:MODELNUM.RestoreFile(Save_mod_ID)

        recordsprocessed    = 0
        percentprogress     = 0
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'

        recordstoprocess    = Records(JOBS)

        !Changed from date completed key, to date booked key
        !at Intec's request.
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:Date_Booked_Key)
        job:Date_Booked = glo:select1
        set(job:Date_Booked_Key,job:Date_Booked_Key)
        loop
            if access:jobs.next()
               break
            end !if
    !---insert routine
            0{prop:Text} = ?progress:pcttext{prop:text}
            ?progress:userstring{prop:Text} = 'Found: ' & tmp:Count
            do getnextrecord2
            do cancelcheck
            if tmp:cancel = 1
                break
            end!if tmp:cancel = 1

            if job:Date_Booked > glo:select2      |
                then break.  ! end if
            If job:manufacturer <> 'SAGEM'
                Cycle
            End!If job:manufacturer <> 'SAGEM'
            Do Export
            If job:date_booked => tmp:warrdate
                L1:Warranty     = '"UW";'
            Else!If job:datebooked => tmp:warrdate
                L1:Warranty     = '"OW";'
            End!If job:datebooked => tmp:warrdate
            tmp:Count += 1
            ADD(out_file)

        end !loop
        access:jobs.restorefile(save_job_id)

    !---after routine
        do endprintrun
        close(progresswindow)
        Close(Out_File)

        If tmp:Count = 0
            Case Missive('There are no records that match the selected criteria.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        Else !If tmp:Count = 0
            Case Missive('Export File Created:'&|
              '<13,10>'&|
              '<13,10>' & Clip(local:FileName) & '.','ServiceBase 3g',|
                           'midea.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !If tmp:Count = 0

    End!if not filedialog
   Relate:DEFAULTS.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS.Close
   Relate:MANUFACT.Close
   Relate:MODELNUM.Close
Export         Routine
    Clear(ouf:record)
    L1:Edi_No       = '"'&Format(Upper(man:EDI_Account_Number),@s12)&'";'
    L1:Order_No     = '"'&UPPER(Strippoint(Format(job:Ref_Number,@s25)))&'";'
    L1:Customer     = '"'&UPPER(Format(job:surname,@s25))&'";'
    L1:Warranty     = '"UW";'
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number
                IMEIError# = 0
            End !If jot:RefNumber <> job:Ref_Number
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        L1:IMEI_In      = '"'&UPPER(Format(job:esn,@s15))&'";'
        L1:Sag_Ref_In   = '"'&UPPER(Format(job:msn,@s9))&'";'
    Else !IMEIError# = 1
        L1:IMEI_In      = '"'&UPPER(Format(jot:OriginalIMEI,@s15))&'";'
        L1:Sag_Ref_In   = '"'&UPPER(Format(jot:Originalmsn,@s9))&'";'
    End !IMEIError# = 1
    L1:imei_Out     = '"'&UPPER(Format(job:esn,@s15))&'";'
    L1:Sag_Ref_Out  = '"'&UPPER(Format(job:msn,@s9))&'";'

    L1:Date_Code    = '"'&UPPER(Format(job:Fault_Code8,@s7))&'";'
    access:exchange.clearkey(xch:Ref_Number_Key)
    xch:Ref_Number = job:Exchange_Unit_Number
    if access:exchange.tryfetch(xch:Ref_Number_Key) = Level:Benign
        L1:imei_Out     = '"'&UPPER(Format(xch:esn,@s15))&'";'
        L1:Sag_Ref_Out  = '"'&UPPER(Format(xch:msn,@s9))&'";'
    End!if access:exchange.tryfetch(xch:RefNumberKey)
    L1:Battery      = '"'&UPPER(Format(job:Fault_Code3,@s9))&'";'
    L1:Charger      = '"'&UPPER(Format(job:Fault_Code4,@s9))&'";'
    L1:Antenna      = '"'&UPPER(Format(job:Fault_Code5,@s3))&'";'
    If l1:dop <> ''
        L1:DOP          = '"'&UPPER(Format(FORMAT(DAY(job:dop),@n02)&FORMAT(MONTH(job:dop),@n02)&YEAR(job:dop),@s8))&'";'
    Else!If l1:dop <> ''
        l1:dop          = '"        ";'
    End!If l1:dop <> ''
    L1:Booked       = '"'&UPPER(Format(FORMAT(DAY(job:Date_Booked),@n02)&FORMAT(MONTH(job:Date_Booked),@n02)&YEAR(job:Date_Booked),@s8))&'";'
    !loop through contacts to get dates for the estimate!
    found_out# = 0
    found_in# = 0
    save_aud_id = access:audit.savefile()
    access:audit.clearkey(aud:Ref_Number_Key)
    aud:Ref_Number = job:Ref_Number
    set(aud:Ref_Number_Key,aud:Ref_Number_Key)
    loop
        if access:audit.next()
           break
        end !if
        if aud:Ref_Number <> job:Ref_Number      |
            then break.  ! end if
        If Instring(aud:action,1,1) = '520 ESTIMATE SENT'
            L1:Est_Out      = '"'&UPPER(Format(FORMAT(DAY(aud:date),@n02)&FORMAT(MONTH(aud:date),@n02)&YEAR(aud:date),@s8))&'";'
            found_out# = 1
        End!If Instring(aud:action,1,1) = '520 ESTIMATE SENT'
        If instring(aud:action,1,1) = '535 ESTIMATE ACCEPTED'
            L1:Est_In       = '"'&UPPER(Format(FORMAT(DAY(aud:date),@n02)&FORMAT(MONTH(aud:date),@n02)&YEAR(aud:date),@s8))&'";'
            found_in# = 1
        End!If instring(aud:action,1,1) = '535 ESTIMATE ACCEPTED'
    end !loop
    access:audit.restorefile(save_aud_id)
    If found_out# = 0
        l1:est_out  = '"        ";'
    End!If found_out# = 0
    If found_out# = 0
        l1:est_in   = '"        ";'
    End!If found_out# = 0

    !To show Exchange Despatched Date, then Completed date, or blank
    If job:Exchange_Despatched <> ''
        L1:Despatched   = '"'&UPPER(Format(FORMAT(DAY(job:Exchange_Despatched),@n02)&FORMAT(MONTH(job:Exchange_Despatched),@n02)&YEAR(job:Exchange_Despatched),@s8))&'";'
    Else !If job:Exchange_Despatched <> ''
        If job:Date_Completed <> ''
            L1:Despatched   = '"'&UPPER(Format(FORMAT(DAY(job:Date_Completed),@n02)&FORMAT(MONTH(job:Date_Completed),@n02)&YEAR(job:Date_Completed),@s8))&'";'
        Else !If job:Date_Completed <> ''
            L1:Despatched   = '"        ";'
        End !If job:Date_Completed <> ''
    End !If job:Exchange_Despatched <> ''

    L1:Cust_Code    = '"'&UPPER(Format(job:Fault_Code6,@s5))&'";'
    L1:Fault_Code   = '"'&UPPER(Format(job:Fault_Code7,@s5))&'";'
    L1:Soft_In      = '"'&(Format(job:Fault_Code1,@s11))&'";'
    L1:Soft_Out     = '"'&(Format(job:Fault_Code2,@s11))&'";'
    access:reptydef.clearkey(rtd:warranty_key)
    rtd:warranty    = 'YES'
    rtd:repair_type = job:Repair_Type_Warranty
    if access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign
        L1:Repair_Level = '"' & Upper(Format(rtd:WarrantyCode,@s5))&'";'
    Else!if access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign
        L1:Repair_Level = '"'&UPPER(Format(job:Repair_Type_Warranty,@s5))&'";'
    End!if access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign

    xp#=0

    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:Ref_Number  <> job:Ref_Number      |
            then break.  ! end if
        If man:includeadjustment <> 'YES' and wpr:Part_Number = 'ADJUSTMENT'
            Cycle
        End!If man:includeadjustment <> 'YES' and wpr:PartNumber = 'ADJUSTMENT'

        xp#+=1
        CASE xp#
          OF 1
            If wpr:Part_Number = 'ADJUSTMENT'
                L1:part1 = '"'&Format('',@s9)&'";'
            Else!If wpr:PartNumber = 'ADJUSTMENT'
                L1:part1 = '"'&Format(wpr:Part_Number,@s9)&'";'
            End!If wpr:PartNumber = 'ADJUSTMENT'
          OF 2
            If wpr:Part_Number = 'ADJUSTMENT'
                L1:part2 = '"'&Format('',@s9)&'";'
            Else!If wpr:PartNumber = 'ADJUSTMENT'
                L1:part2 = '"'&Format(wpr:Part_Number,@s9)&'";'
            End!If wpr:PartNumber = 'ADJUSTMENT'
          OF 3
            If wpr:Part_Number = 'ADJUSTMENT'
                L1:part3 = '"'&Format('',@s9)&'";'
            Else!If wpr:PartNumber = 'ADJUSTMENT'
                L1:part3 = '"'&Format(wpr:Part_Number,@s9)&'";'
            End!If wpr:PartNumber = 'ADJUSTMENT'
          OF 4
            If wpr:Part_Number = 'ADJUSTMENT'
                L1:part4 = '"'&Format('',@s9)&'";'
            Else!If wpr:PartNumber = 'ADJUSTMENT'
                L1:part4 = '"'&Format(wpr:Part_Number,@s9)&'";'
            End!If wpr:PartNumber = 'ADJUSTMENT'
          OF 5
            If wpr:Part_Number = 'ADJUSTMENT'
                L1:part5 = '"'&Format('',@s9)&'";'
            Else!If wpr:PartNumber = 'ADJUSTMENT'
                L1:part5 = '"'&Format(wpr:Part_Number,@s9)&'";'
            End!If wpr:PartNumber = 'ADJUSTMENT'
          OF 6
            If wpr:Part_Number = 'ADJUSTMENT'
                L1:part6 = '"'&Format('',@s9)&'";'
            Else!If wpr:PartNumber = 'ADJUSTMENT'
                L1:part6 = '"'&Format(wpr:Part_Number,@s9)&'";'
            End!If wpr:art_number = 'ADJUSTMENT'
          OF 7
            If wpr:Part_Number = 'ADJUSTMENT'
                L1:part7 = '"'&Format('',@s9)&'";'
            Else!If wpr:PartNumber = 'ADJUSTMENT'
                L1:part7 = '"'&Format(wpr:Part_Number,@s9)&'";'
            End!If wpr:PartNumber = 'ADJUSTMENT'
          OF 8
            If wpr:Part_Number = 'ADJUSTMENT'
                L1:part8 = '"'&Format('',@s9)&'";'
            Else!If wpr:PartNumber = 'ADJUSTMENT'
                L1:part8 = '"'&Format(wpr:Part_Number,@s9)&'";'
            End!If wpr:PartNumber = 'ADJUSTMENT'
          OF 9
            If wpr:Part_Number = 'ADJUSTMENT'
                L1:part9 = '"'&Format('',@s9)&'";'
            Else!If wpr:PartNumber = 'ADJUSTMENT'
                L1:part9 = '"'&Format(wpr:Part_Number,@s9)&'";'
            End!If wpr:PartNumber = 'ADJUSTMENT'

        END

    end !loop
    access:warparts.restorefile(save_wpr_id)

    IF l1:part1 = ''
      L1:part1 = '"'&Format('',@s9)&'";'
    END
    IF l1:part2 = ''
      L1:part2 = '"'&Format('',@s9)&'";'
    END
    IF l1:part3 = ''
      L1:part3 = '"'&Format('',@s9)&'";'
    END
    IF l1:part4 = ''
      L1:part4 = '"'&Format('',@s9)&'";'
    END
    IF l1:part5 = ''
      L1:part5 = '"'&Format('',@s9)&'";'
    END
    IF l1:part6 = ''
      L1:part6 = '"'&Format('',@s9)&'";'
    END
    IF l1:part7 = ''
      L1:part7 = '"'&Format('',@s9)&'";'
    END
    IF l1:part8 = ''
      L1:part8 = '"'&Format('',@s9)&'";'
    END
    IF l1:part9 = ''
      L1:part9 = '"'&Format('',@s9)&'";'
    END
    L1:refurb1 = '"'&Format('',@s9)&'";'
    L1:refurb2 = '"'&Format('',@s9)&'";'
    L1:refurb3 = '"'&Format('',@s9)&'";'
    L1:refurb4 = '"'&Format('',@s9)&'";'
    L1:refurb5 = '"'&Format('',@s9)&'";'
    L1:refurb6 = '"'&Format('',@s9)&'";'
    L1:refurb7 = '"'&Format('',@s9)&'";'
    L1:RFS     = '"'&UPPER(Format(FORMAT(DAY(job:date_completed),@n02)&FORMAT(MONTH(job:Date_Completed),@n02)&YEAR(job:Date_Completed),@s9))&'";'
    L1:ModelNumber   = '"'&Format(job:model_number,@s27)&'";'
    L1:Re_Rep  = '"'&UPPER(Format(job:fault_code9,@s27))&'"'
    count += 1


Export2     Routine
    CLEAR(ouf:RECORD)

    Set(defaults)
    access:defaults.next()

    L2:Field1 = '"'&Format('UK',@s8)&'";'
    L2:Field2 = '"'&Format(def:User_Name,@s30)&'";'
    L2:Field3 = '"'&Format(man:EDI_Account_Number,@s8)&'"'
    ADD(out_file)
    L2:Field1 = '"'&Format(FORMAT(DAY(TODAY()),@n02)&FORMAT(MONTH(TODAY()),@n02)&YEAR(TODAY()),@s8)&'";'
    L2:Field2 = '"'&Format(FORMAT(DAY(glo:select1),@n02)&FORMAT(MONTH(glo:select1),@n02)&YEAR(glo:select1),@s30)&'";'
    L2:Field3 = '"'&Format(FORMAT(DAY(glo:select2),@n02)&FORMAT(MONTH(glo:select2),@n02)&YEAR(glo:select2),@s8)&'"'
    ADD(out_file)
    If job:edi <> ''
        L2:Field1 = '"'&Format(Strippoint(Upper(Clip(job:edi))),@s8)&'";'
    Else!If f_batch_number = 0
        L2:Field1 = '""'
    End!If f_batch_number = 0
    L2:Field2 = '""'
    L2:Field3 = ''
    ADD(out_file)
    CLEAR(ouf:RECORD)
    LOOP h# = 1 to 7
      L2:Field1 = ''
      ADD(out_file)
    END
    L1:Edi_NO       = '"'&Format('A0',@s12)&'";'
    L1:Order_No     = '"'&format('A1',@s25)&'";'
    L1:Customer     = '"'&format('A2',@s25)&'";'
    L1:Warranty     = '"'&Format('A3',@s2)&'";'
    L1:IMEI_In      = '"'&Format('A4',@s15)&'";'
    L1:Sag_Ref_In   = '"'&Format('A5',@s9)&'";'
    L1:Date_Code    = '"'&Format('A6',@s7)&'";'
    L1:IMEI_Out     = '"'&Format('A7',@s15)&'";'
    L1:Sag_Ref_Out  = '"'&Format('A8',@s9)&'";'
    L1:Battery      = '"'&Format('A9',@s9)&'";'
    L1:Charger      = '"'&Format('A10',@s9)&'";'
    L1:Antenna      = '"'&Format('A11',@s3)&'";'
    L1:DOP          = '"'&Format('A12',@s8)&'";'
    L1:Booked       = '"'&Format('A13',@s8)&'";'
    L1:Est_Out      = '"'&FOrmat('A14',@s8)&'";'
    L1:Est_In       = '"'&Format('A15',@s8)&'";'
    L1:Despatched   = '"'&Format('A16',@s8)&'";'
    L1:Cust_Code    = '"'&Format('A17',@s5)&'";'
    L1:Fault_Code   = '"'&Format('A18',@s5)&'";'
    L1:Soft_In      = '"'&Format('A19',@s11)&'";'
    L1:Soft_Out     = '"'&Format('A20',@s11)&'";'
    L1:Repair_Level = '"'&Format('A21',@s5)&'";'
    L1:part1        = '"'&Format('A22',@s9)&'";'
    L1:part2        = '"'&Format('A23',@s9)&'";'
    L1:part3        = '"'&Format('A24',@s9)&'";'
    L1:part4        = '"'&Format('A25',@s9)&'";'
    L1:part5        = '"'&Format('A26',@s9)&'";'
    L1:part6        = '"'&Format('A27',@s9)&'";'
    L1:part7        = '"'&Format('A28',@s9)&'";'
    L1:part8        = '"'&Format('A29',@s9)&'";'
    L1:part9        = '"'&Format('A30',@s9)&'";'
    L1:Refurb1      = '"'&Format('A31',@s9)&'";'
    L1:Refurb2      = '"'&Format('A32',@s9)&'";'
    L1:Refurb3      = '"'&Format('A33',@s9)&'";'
    L1:Refurb4      = '"'&Format('A34',@s9)&'";'
    L1:Refurb5      = '"'&Format('A35',@s9)&'";'
    L1:Refurb6      = '"'&Format('A36',@s9)&'";'
    L1:Refurb7      = '"'&Format('A37',@s9)&'";'
    L1:RFS          = '"'&Format('A38',@s9)&'";'
    L1:ModelNumber  = '"'&Format('A39',@s27)&'";'
    L1:Re_Rep       = '"'&Format('A40',@s27)&'"'
    ADD(out_file)


getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
        display()
      end
    end

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        case event()
            of event:timer
                break
            of event:closewindow
                cancel# = 1
                break
            of event:accepted
                if field() = ?cancel
                    cancel# = 1
                    break
                end!if field() = ?button1
        end!case event()
    end!accept
    if cancel# = 1
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
                tmp:cancel = 1
            Of 1 ! No Button
        End ! Case Missive
    end!if cancel# = 1

endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()






Status_Summary_Report PROCEDURE(func:DateRangeType,func:StartDate,func:EndDate,func:JobType,func:InvoiceType,func:DespatchedType,func:CompletedType,func:StatusType,func:ReportOrder,func:StatusReportType,func:JobBatchNumber,func:EDIBatchNumber,func:CustomerStatus)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:status           STRING(30)
tmp:number           LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_job_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
status_queue_temp    QUEUE,PRE(STAQUE)
Status               STRING(30)
Number_Temp          LONG
RRCNumber            LONG
                     END
tmp:ExchangeStatusQueue QUEUE,PRE(excque)
Status               STRING(30)
StatusNumber         LONG
RRCNumber            LONG
                     END
tmp:LoanStatusQueue  QUEUE,PRE(loaque)
Status               STRING(30)
StatusNumber         LONG
RRCNumber            LONG
                     END
job_count_temp       REAL
total_jobs_temp      REAL
tmp:PrintedBy        STRING(60)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:NumberRRC        LONG
tmp:RRCTotal         LONG
AddressGroup         GROUP,PRE(address)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Status Summary Report'),AT(396,2760,7521,8531),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Date Range: '),AT(5000,156),USE(?String22),TRN,FONT(,8,,)
                         STRING(@s3),AT(5781,781),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6),AT(5781,156),USE(tmp:StartDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6),AT(6615,156),USE(tmp:EndDate),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?String24),TRN,FONT(,8,,)
                         STRING(@s60),AT(5781,365),USE(tmp:PrintedBy),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(5781,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(' to '),AT(6406,156),USE(?String23),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,781),USE(?String59),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,229),USE(?DetailBand)
                           STRING(@s8b),AT(5208,0),USE(tmp:NumberRRC),TRN,RIGHT
                           STRING(@s30),AT(2115,0),USE(tmp:status),TRN,LEFT
                           STRING(@s8),AT(4479,0),USE(tmp:number),TRN,RIGHT
                         END
totals                   DETAIL,USE(?totals)
                           LINE,AT(2031,52,3438,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Jobs:'),AT(2115,135),USE(?String21),TRN
                           STRING(@s8b),AT(5208,104),USE(tmp:RRCTotal),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING(@s9),AT(4427,104),USE(total_jobs_temp),TRN,RIGHT,FONT(,,,FONT:bold)
                         END
job_status_title         DETAIL,AT(,,,438),USE(?job_status_title)
                           STRING('Job Status'),AT(3417,0),USE(?String21:2),TRN,FONT(,,,FONT:bold)
                           STRING('ARC'),AT(4844,208),USE(?String34),TRN,FONT(,,,FONT:bold)
                           STRING('RRC'),AT(5573,208),USE(?String34:2),TRN,FONT(,,,FONT:bold)
                         END
exchange_status_title    DETAIL,PAGEBEFORE(-1),AT(,,,490),USE(?exchange_status_title)
                           STRING('Exchange Status'),AT(3208,0),USE(?String21:3),TRN,FONT(,,,FONT:bold)
                         END
loan_status_title        DETAIL,PAGEBEFORE(-1),AT(,,,469),USE(?Loan_status_title)
                           STRING('Loan Status'),AT(3375,0),USE(?String21:4),TRN,FONT(,,,FONT:bold)
                         END
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(address:CompanyName),LEFT,FONT(,16,,FONT:bold)
                         STRING('SUMMARY STATUS REPORT'),AT(4271,0,3177,260),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(address:AddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(address:AddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(address:AddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,885),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,885),USE(address:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1042),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,1042),USE(address:FaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1198,3844,198),USE(address:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1198),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('Status Type'),AT(2135,2083),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('No Of Entries'),AT(4792,2083),USE(?String45),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Status_Summary_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Status_Summary_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:WEBJOB.Open
  Access:JOBSE.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = Records(Jobs)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Case func:ReportOrder
            Of 'JOB NUMBER'
                Access:JOBS.Clearkey(job:Ref_Number_Key)
                set(job:ref_number_key,0)
            Of 'I.M.E.I. NUMBER'
                Access:JOBS.Clearkey(job:ESN_Key)
                set(job:esn_key,0)
            Of 'ACCOUNT NUMBER'
                Access:JOBS.Clearkey(job:AccountNumberKey)
                set(job:accountnumberkey,0)
            Of 'MODEL NUMBER'
                Access:JOBS.Clearkey(job:Model_Number_Key)
                set(job:model_number_key,0)
            Of 'STATUS'
                Case func:StatusType
                    Of 0 !Job Status
                        Access:JOBS.Clearkey(job:By_Status)
                        set(job:by_status,0)
                    Of 1 !Exchange Status
                        Access:JOBS.Clearkey(job:ExcStatusKey)
                        Set(job:ExcStatusKey)
                    Of 2 !Loan Status
                        Access:JOBS.Clearkey(job:LoanStatusKey)
                        Set(job:LoanStatusKey)
                End !Case tmp:StatusType
            Of 'MSN'
                Access:JOBS.Clearkey(job:MSN_Key)
                set(job:msn_key,0)
            Of 'SURNAME'
                Access:JOBS.Clearkey(job:Surname_Key)
                set(job:surname_key,0)
            Of 'ENGINEER'
                Access:JOBS.Clearkey(job:Engineer_Key)
                set(job:engineer_key,0)
            Of 'MOBILE NUMBER'
                Access:JOBS.Clearkey(job:MobileNumberKey)
                set(job:mobilenumberkey,0)
            Of 'DATE BOOKED'
                Access:JOBS.Clearkey(job:Date_Booked_Key)
                Case func:DateRangeType
                    Of 0 !Booking
                        job:date_booked = func:StartDate
                        set(job:date_booked_key,job:date_booked_key)
                    Of 1 !Completed
                        Set(job:Date_Booked_Key,0)
                End !tmp:DateRangeType
            Of 'DATE COMPLETED'
                Access:JOBS.ClearKey(job:DateCompletedKey)
                Case func:DateRangeType
                        Of 0 !Booking
                            Set(job:DateCompletedKey,0)
                        Of 1 !Completed
                            job:date_completed = func:StartDate
                            set(job:datecompletedkey,job:datecompletedkey)
                    End !Case tmp:DateRangeType
            End !Case tmp:ReportOrder
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
        
                RecordsProcessed += 1
                Do DisplayProgress
        
                Case func:ReportOrder
                    Of 'DATE BOOKED'
                        If func:DateRangeType = 0
                            If job:Date_Booked > func:EndDate
                                Break
                            End !If job:Date_Booked > tmp:EndDate
                        End !If tmp:DateRangeType = 0
                    Of 'DATE COMPLETED'
                        If func:DateRangeType = 1
                            If job:Date_Completed > func:EndDate
                                Break
                            End !If job:Date_Completed > tmp:EndDate
                        End !If tmp:DateRangeType = 1
                End !Case tmp:ReportOrder
        
                If StatusReportValidation(func:DateRangeType,func:StartDate,func:EndDate,func:JobType,func:InvoiceType,func:DespatchedType,func:CompletedType,func:StatusType,func:JobBatchNumber,func:EDIBatchNumber)
                    Cycle
                End !If StatusReportValidation(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType)
        
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Found
        
                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
        
                Sort(status_queue_temp,STAQUE:Status)
                staque:status = job:current_status
                Get(status_queue_temp,staque:status)
                If Error()
                    staque:status = job:current_status
                    If ~jobe:HubRepair
                        staque:RRCNumber = 1
                        staque:Number_Temp = 0
                    Else !If jobe:HubRepair
                        staque:number_temp = 1
                        staque:RRCNumber = 0
                    End !If jobe:HubRepair
                    Add(status_queue_temp)
                Else!If Error()
                    If ~jobe:HubRepair
                        staque:RRCNumber += 1
                    Else !If jobe:HubRepair
                        staque:number_temp += 1
                    End !If jobe:HubRepair
                    Put(status_queue_temp)
                End!If Error()
        
                Sort(tmp:ExchangeStatusQueue,excque:status)
                excque:status   = job:exchange_status
                Get(tmp:exchangeStatusQueue,excque:status)
                if Error()
                    excque:status   = job:exchange_status
                    excque:StatusNumber = 1
                    Add(tmp:ExchangeStatusQueue)
                Else!if Error()
                    excque:StatusNumber += 1
                    Put(tmp:ExchangeStatusQueue)
                End!if Error()
        
                Sort(tmp:LoanStatusQueue,loaque:status)
                loaque:status   = job:loan_status
                Get(tmp:LoanStatusQueue,loaque:status)
                if Error()
                    loaque:status   = job:Loan_status
                    loaque:StatusNumber = 1
                    Add(tmp:LoanStatusQueue)
                Else!if Error()
                    loaque:StatusNumber += 1
                    Put(tmp:LoanStatusQueue)
                End!if Error()
        
        
        end !loop
        access:jobs.restorefile(save_job_id)
        
        
        RecordsToProcess = Records(status_queue_temp) + Records(tmp:ExchangeStatusQueue) + Records(tmp:LoanStatusQueue)
        
        If Records(status_queue_temp)
            Print(rpt:job_status_title)
            total_jobs_temp = 0
            Loop x# = 1 To Records(status_queue_temp)
                Get(status_queue_temp,x#)
        
                RecordsProcessed += 1
                Do DisplayProgress
        
                total_jobs_temp     += staque:number_temp
                tmp:RRCTotal        += staque:RRCNumber
                tmp:RecordsCount    += 1
                tmp:status          = staque:status
                tmp:number          = staque:number_temp
                tmp:NumberRRC       = staque:RRCNumber
                Print(rpt:detail)
            End!Loop x# = 1 To Records(status_queue_temp)
            Print(rpt:totals)
        End!If Records(status_queue_temp)
        
        SetTarget(Report)
        ?tmp:RRCTotal{prop:Hide} = 1
        ?tmp:NumberRRC{prop:hide} = 1
        SetTarget()
        
        If Records(tmp:ExchangeStatusQueue)
            Print(rpt:exchange_status_title)
            total_jobs_temp = 0
            Loop x# = 1 To Records(tmp:ExchangeStatusQueue)
                RecordsProcessed += 1
                Do DisplayProgress
                Get(tmp:ExchangeStatusQueue,x#)
                total_jobs_temp += excque:StatusNUmber
                tmp:RecordsCount += 1
                tmp:status  = excque:status
                tmp:number  = excque:statusNumber
                Print(rpt:detail)
            End!Loop x# = 1 To Records(tmp:ExchangeStatusQueueu)
            Print(rpt:totals)
        End!If Records(tmp:ExchangeStatusQueue)
        
        If Records(tmp:LoanStatusQueue)
            Print(rpt:Loan_status_title)
            total_jobs_temp = 0
            Loop x# = 1 To Records(tmp:LoanStatusQueue)
                RecordsProcessed += 1
                Do DisplayProgress
                Get(tmp:LoanStatusQueue,x#)
                total_jobs_temp += loaque:StatusNUmber
                tmp:RecordsCount += 1
                tmp:status  = loaque:status
                tmp:number  = loaque:statusnumber
                Print(rpt:detail)
            End!Loop x# = 1 To Records(tmp:ExchangeStatusQueueu)
            Print(rpt:totals)
        End!If Records(tmp:ExchangeStatusQueue)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Status_Summary_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:WEBJOB.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  tmp:StartDate = func:StartDate
  tmp:EndDate = func:EndDate
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  If glo:WebJob
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = clarionet:global.param2
      IF access:tradeacc.fetch(tra:account_number_key)
        !Error!
      END
      address:CompanyName     = tra:Company_Name
      address:AddressLine1    = tra:Address_Line1
      address:AddressLine2    = tra:Address_Line2
      address:AddressLine3    = tra:Address_Line3
      address:Postcode        = tra:Postcode
      address:TelephoneNumber = tra:Telephone_Number
      address:FaxNumber       = tra:Fax_Number
      address:EmailAddress    = tra:EmailAddress
  Else !glo:WebJob
      address:CompanyName     = def:User_Name
      address:AddressLine1    = def:Address_Line1
      address:AddressLine2    = def:Address_Line2
      address:AddressLine3    = def:Address_Line3
      address:Postcode        = def:Postcode
      address:TelephoneNumber = def:Telephone_Number
      address:FaxNumber       = def:Fax_Number
      address:EmailAddress    = def:EmailAddress
  
  End !glo:WebJob
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Status_Summary_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage







Open_Files PROCEDURE                                  !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Open_Files')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXPAUDIT.Open
  Relate:EXPGENDM.Open
  Relate:EXPPARTS.Open
  Relate:EXPWARPARTS.Open
  Relate:LABLGTMP.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Open_Files')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXPAUDIT.Close
    Relate:EXPGENDM.Close
    Relate:EXPPARTS.Close
    Relate:EXPWARPARTS.Close
    Relate:LABLGTMP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Open_Files')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
