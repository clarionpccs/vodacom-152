

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBD03025.INC'),ONCE        !Local module procedure declarations
                     END


AuthorityNumberExportCriteria PROCEDURE               !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
save_job_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:FileName         STRING(255),STATIC
tmp:VAT              REAL
tmp:Total            REAL
tmp:Paid             REAL
window               WINDOW('Authority Number Export Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Authority Number Export Criteria'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Start Date'),AT(252,196),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(324,196,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           PROMPT('End Date'),AT(252,214),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(324,214,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                         END
                       END
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

!static webjob window
webjobwindow WINDOW,AT(,,165,42),FONT('Arial',8,,),COLOR(COLOR:White),CENTER,IMM,GRAY,DOUBLE,AUTO
       STRING('Running Report'),AT(12,8,144,16),USE(?Str1),TRN,CENTER,FONT(,18,COLOR:Red,FONT:bold)
       STRING('Please Wait'),AT(56,28),USE(?String2),TRN,FONT('Arial',10,COLOR:Navy,FONT:regular+FONT:italic,CHARSET:ANSI)
     END
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020189'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AuthorityNumberExportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:INVOICE.Open
  Relate:JOBPAYMT.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:StartDate = Deformat('1/1/2000',@d6)
  tmp:EndDate = Today()
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
    Relate:JOBPAYMT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020189'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020189'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020189'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      ! Inserting (DBH 14/02/2008) # 9752 - Check for valid dates
      If IsDateInValid(tmp:StartDate)
          Beep(Beep:SystemHand);  Yield()
         Case Missive('Invalid Start Date.','ServiceBase 3g',|
                        'mstop.jpg','/OK')
             Of 1 ! OK Button
         End ! Case Missive
         tmp:StartDate = ''
         Select(?tmp:StartDate)
         Cycle
      End ! If IsDateInValid(tmp:StartDate)
      If IsDateInValid(tmp:EndDate)
          Beep(Beep:SystemHand);  Yield()
         Case Missive('Invalid End Date.','ServiceBase 3g',|
                        'mstop.jpg','/OK')
             Of 1 ! OK Button
         End ! Case Missive
         tmp:EndDate = ''
         Select(?tmp:EndDate)
         Cycle
      End ! If IsDateInValid(tmp:StartDate)
      
      Beep(Beep:SystemQuestion);  Yield()
      Case Missive('Confirm Date Range:'&|
          '|    From:   ' & Format(tmp:StartDate,@d18) & |
          '|    To:        ' & Format(tmp:EndDate,@d18) & |
          '||Run Report?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
          Of 1 ! No Button
              Display()
              Cycle
      End ! Case Missive
      ! End (DBH 14/02/2008) #9752
      
      
      glo:File_Name = 'Insurance Claim Export ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(Today()),@n04) & Format(Clock(),@t2) & '.csv'
      Remove(glo:File_Name)
      
      Access:EXPGEN.Open()
      Access:EXPGEN.UseFile()
      
      Clear(gen:Record)
      gen:Line1   = 'AUTHORITY NUMBER REPORT    (Version: ' & Clip(ProgramVersionNumber()) & ')'
      Access:EXPGEN.Insert()
      
      Clear(gen:Record)
      gen:Line1 = 'Job Number,Charge Type,Authorisation Number,Trade Account Number,Trade Account Name,' &|
                  'Customer,Date Booked,Date Completed,Manufacturer,Model Number,Repair Type,' &|
                  'Parts Selling,Labour,V.A.T.,Total,Excess Paid,Outstanding Amount'
      Access:EXPGEN.Insert()
      
      Prog.ProgressSetup(Records(JOBS))
      
      Save_job_ID = Access:JOBS.SaveFile()
      Access:JOBS.ClearKey(job:DateCompletedKey)
      job:Date_Completed = tmp:StartDate
      Set(job:DateCompletedKey,job:DateCompletedKey)
      Loop
          If Access:JOBS.NEXT()
             Break
          End !If
          If job:Date_Completed > tmp:EndDate       |
              Then Break.  ! End If
          If Prog.InsideLoop()
              Break
          End !If Prog.InsideLoop()
      
          If job:Chargeable_Job <> 'YES'
              Cycle
          End !If job:Chargeable_Job <> 'YES'
      
          If job:Invoice_Number = 0
              Cycle
          End !If job:Invoice_Number = 0
      
          !only show jobs for your account
          Access:WEBJOB.Clearkey(wob:RefNumberKey)
          wob:RefNumber   = job:Ref_Number
          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
              !Found
              If wob:HeadAccountNumber <> Clarionet:Global.Param2
                  Cycle
              End !If wob:HeadAccountNumber <> Clarionet:Global.Param2
          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
              !Error
          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber    = job:Ref_Number
          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found
      
          Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
          End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
          Clear(gen:Record)
          gen:Line1   = job:Ref_Number
          gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Charge_Type)
          gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Authority_Number)
          gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Account_Number)
      
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number  = job:Account_Number
          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Found
              gen:Line1   = Clip(gen:Line1) & ',' & StripComma(sub:Company_Name)
          Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
          gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Company_Name)
          gen:Line1   = Clip(gen:Line1) & ',' & Format(job:Date_Booked,@d06b)
          gen:Line1   = Clip(gen:Line1) & ',' & Format(job:Date_Completed,@d06b)
          gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Manufacturer)
          gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Model_Number)
          gen:Line1   = Clip(gen:Line1) & ',' & StripComma(job:Repair_Type)
          gen:Line1   = Clip(gen:Line1) & ',' & Format(jobe:InvRRCCPartsCost,@n_14.2)
          gen:Line1   = Clip(gen:Line1) & ',' & Format(jobe:InvRRCCLabourCost,@n_14.2)
      
          Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
          inv:Invoice_Number  = job:Invoice_Number
          If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
              !Found
              If ~inv:ExportedRRCOracle
                  Cycle
              End !If ~inv:ExportedRRCOracle
      
          Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
              !Error
          End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
      
          tmp:VAT = (jobe:InvRRCCPartsCost * inv:RRCVatRateLabour/100 + |
                                                      jobe:InvRRCCLabourCost * inv:RRCVatRateParts/100 + |
                                                      job:Invoice_Courier_Cost * inv:RRCVatRateLabour/100)
          gen:Line1   = Clip(gen:Line1) & ',' & Format(tmp:VAT,@n_14.2)
      
          tmp:Total = tmp:VAT + jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + |
                                                      job:Invoice_Courier_Cost
          gen:Line1   = Clip(gen:Line1) & ',' & Format(tmp:Total,@n_14.2)
      
          tmp:Paid = 0
          Save_jpt_ID = Access:JOBPAYMT.SaveFile()
          Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
          jpt:Ref_Number = job:Ref_Number
          Set(jpt:All_Date_Key,jpt:All_Date_Key)
          Loop
              If Access:JOBPAYMT.NEXT()
                 Break
              End !If
              If jpt:Ref_Number <> job:Ref_Number      |
                  Then Break.  ! End If
              tmp:Paid += jpt:Amount
          End !Loop
          Access:JOBPAYMT.RestoreFile(Save_jpt_ID)
      
          gen:Line1   = Clip(gen:Line1) & ',' & Format(tmp:Paid,@n_14.2)
      
          !Is there an outstanding payment?
          If tmp:Paid < tmp:Total
              gen:Line1   = Clip(gen:Line1) & ',' & Format(tmp:Total - tmp:Paid,@n_14.2)
          Else !If tmp:Paid < tmp:Total
              gen:Line1   = Clip(gen:Line1) & ','
          End !If tmp:Paid < tmp:Total
      
          Access:EXPGEN.Insert()
      End !Loop
      Access:JOBS.RestoreFile(Save_job_ID)
      
      Access:EXPGEN.Close()
      
      Prog.ProgressFinish()
      
      SendFileToClient(glo:File_Name)
      
      Case Missive('Export Completed.','ServiceBase 3g',|
                     'midea.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Remove(glo:File_Name)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 500
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
RFExchangeReportCriteria PROCEDURE                    !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::10:TAGFLAG         BYTE(0)
DASBRW::10:TAGMOUSE        BYTE(0)
DASBRW::10:TAGDISPSTATUS   BYTE(0)
DASBRW::10:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGFLAG         BYTE(0)
DASBRW::12:TAGMOUSE        BYTE(0)
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer3                      LIKE(glo:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_sub_id          USHORT,AUTO
tmp:AccountTag       STRING(1)
tmp:PaymentTag       STRING(1)
tmp:ChargeTypeTag    STRING(1)
tmp:ChargeType       STRING(30)
tmp:WarrantyChargeType STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:includeARC       BYTE
tmp:AllAccounts      BYTE(1)
tmp:MainAccount      STRING(30)
tmp:True             BYTE(1)
tmp:AllChargeTypes   BYTE(1)
tmp:ReportOrder      BYTE(0)
tmp:No               STRING('NO {28}')
tmp:Yes              STRING('YES')
tmp:ManufacturerTag  STRING(1)
tmp:AllManufacturer  BYTE(1)
tmp:Paper            BYTE(1)
tmp:arc              BYTE
BRW5::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                       PROJECT(sub:Generic_Account)
                       PROJECT(sub:Main_Account_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:AccountTag         LIKE(tmp:AccountTag)           !List box control field - type derived from local data
tmp:AccountTag_Icon    LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
sub:Generic_Account    LIKE(sub:Generic_Account)      !Browse key field - type derived from field
sub:Main_Account_Number LIKE(sub:Main_Account_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:ChargeTypeTag      LIKE(tmp:ChargeTypeTag)        !List box control field - type derived from local data
tmp:ChargeTypeTag_Icon LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:ManufacturerTag    LIKE(tmp:ManufacturerTag)      !List box control field - type derived from local data
tmp:ManufacturerTag_Icon LONG                         !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Warranty Parts Sent To ARC Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Tab1'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select the Trade Accounts you wish to include'),AT(219,124),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All Accounts'),AT(400,124),USE(tmp:AllAccounts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Accounts'),TIP('All Accounts'),VALUE('1','0')
                           SHEET,AT(168,138,344,188),USE(?Sheet2),SPREAD
                             TAB('By Account Number'),USE(?Tab4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                             TAB('Generic Accounts'),USE(?Tab5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                           END
                           LIST,AT(172,154,236,168),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@67L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(249,164,5,5),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(249,185,5,5),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(420,188),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(420,224),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(420,260),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 6'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Charge Types To Include'),AT(233,126),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All Charge Types'),AT(369,126),USE(tmp:AllChargeTypes),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Charge Types'),TIP('All Charge Types'),VALUE('1','0')
                           LIST,AT(266,138,150,134),USE(?List:2),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(289,178,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(293,210,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(236,276),USE(?DASTAG:2),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,276),USE(?DASTAGAll:2),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,276),USE(?DASUNTAGALL:2),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 7'),USE(?Tab7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Manufacturer To Include'),AT(233,124),USE(?Prompt7),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('All Manufacturers'),AT(365,124),USE(tmp:AllManufacturer),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Manufacturers'),TIP('All Manufacturers'),VALUE('1','0')
                           LIST,AT(252,138,180,136),USE(?List:3),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Rev tags'),AT(299,210,50,13),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(307,226,70,13),USE(?DASSHOWTAG:3),HIDE
                           BUTTON,AT(236,276),USE(?DASTAG:3),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,276),USE(?DASTAGAll:3),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,276),USE(?DASUNTAGALL:3),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 3'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Action Date Range'),AT(253,124),USE(?Prompt4),FONT(,,080FFFFH,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Start Date'),AT(252,142),USE(?tmp:StartDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(340,142,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(408,138),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(252,166),USE(?tmp:EndDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(340,166,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(408,162),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           CHECK(' Include ARC Repaired Jobs'),AT(252,186),USE(tmp:includeARC),DISABLE,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           OPTION('Report Order'),AT(248,204,244,28),USE(tmp:ReportOrder),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('By Manufacturer'),AT(408,217),USE(?tmp:ReportOrder:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('By Job Number'),AT(256,217),USE(?tmp:ReportOrder:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('By Action Date'),AT(332,217),USE(?tmp:ReportOrder:Radio2:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           OPTION('Report Type'),AT(248,240,244,28),USE(tmp:Paper),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Paper'),AT(256,250),USE(?tmp:Paper:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Electronic'),AT(332,250),USE(?tmp:Paper:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                           END
                           PROMPT('Path to Save File'),AT(249,278),USE(?SavePathPrompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(249,288,223,11),USE(glo:CSV_SavePath),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           BUTTON,AT(476,284),USE(?LookupFile),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(168,332),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(232,332),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Warranty Parts Sent To ARC Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(384,332),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

! Clarionet Moving Bar
CN:recordstoprocess     long,auto
CN:recordsprocessed     long,auto
CN:recordspercycle      long,auto
CN:recordsthiscycle     long,auto
CN:percentprogress      byte
CN:recordstatus         byte,auto

CN:progress:thermometer byte
CN:progresswindow WINDOW('Please Wait...'),AT(,,164,22),FONT('Arial',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(CN:progress:thermometer),AT(4,4,156,12),RANGE(0,100)
     END
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

Wizard7         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet2)  = 2
BRW9                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW11                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                 !Default Locator
FileLookup16         SelectFileClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue.Pointer = sub:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:AccountTag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:AccountTag = ''
  END
    Queue:Browse.tmp:AccountTag = tmp:AccountTag
  IF (tmp:Accounttag = '*')
    Queue:Browse.tmp:AccountTag_Icon = 2
  ELSE
    Queue:Browse.tmp:AccountTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::6:QUEUE = glo:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sub:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::10:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW9.UpdateBuffer
   glo:Queue2.Pointer2 = cha:Charge_Type
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = cha:Charge_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:ChargeTypeTag = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:ChargeTypeTag = ''
  END
    Queue:Browse:1.tmp:ChargeTypeTag = tmp:ChargeTypeTag
  IF (tmp:ChargeTypeTag = '*')
    Queue:Browse:1.tmp:ChargeTypeTag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:ChargeTypeTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = cha:Charge_Type
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::10:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::10:QUEUE = glo:Queue2
    ADD(DASBRW::10:QUEUE)
  END
  FREE(glo:Queue2)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::10:QUEUE.Pointer2 = cha:Charge_Type
     GET(DASBRW::10:QUEUE,DASBRW::10:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = cha:Charge_Type
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASSHOWTAG Routine
   CASE DASBRW::10:TAGDISPSTATUS
   OF 0
      DASBRW::10:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::10:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::10:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW11.UpdateBuffer
   glo:Queue3.Pointer3 = man:Manufacturer
   GET(glo:Queue3,glo:Queue3.Pointer3)
  IF ERRORCODE()
     glo:Queue3.Pointer3 = man:Manufacturer
     ADD(glo:Queue3,glo:Queue3.Pointer3)
    tmp:ManufacturerTag = '*'
  ELSE
    DELETE(glo:Queue3)
    tmp:ManufacturerTag = ''
  END
    Queue:Browse:2.tmp:ManufacturerTag = tmp:ManufacturerTag
  IF (tmp:Manufacturertag = '*')
    Queue:Browse:2.tmp:ManufacturerTag_Icon = 2
  ELSE
    Queue:Browse:2.tmp:ManufacturerTag_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW11.Reset
  FREE(glo:Queue3)
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue3.Pointer3 = man:Manufacturer
     ADD(glo:Queue3,glo:Queue3.Pointer3)
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue3)
  BRW11.Reset
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue3)
    GET(glo:Queue3,QR#)
    DASBRW::12:QUEUE = glo:Queue3
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue3)
  BRW11.Reset
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer3 = man:Manufacturer
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer3)
    IF ERRORCODE()
       glo:Queue3.Pointer3 = man:Manufacturer
       ADD(glo:Queue3,glo:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW11.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
CN:getnextrecord      routine
    CN:recordsprocessed += 1
    CN:recordsthiscycle += 1
    if CN:percentprogress < 100
      CN:percentprogress = (CN:recordsprocessed / CN:recordstoprocess)*100
      if CN:percentprogress > 100
        CN:percentprogress = 100
      end
      if CN:percentprogress <> CN:progress:thermometer then
        CN:progress:thermometer = CN:percentprogress
      end
    end

CN:endprintrun         routine
    CN:progress:thermometer = 100
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020193'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('RFExchangeReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  tmp:StartDate = Today()
  tmp:EndDate   = Today()
  
  If glo:WebJob
      tmp:MainAccount = Clarionet:Global.Param2
  Else !glo:WebJob
      tmp:MainAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  End !glo:WebJob
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  BRW9.Init(?List:2,Queue:Browse:1.ViewPosition,BRW9::View:Browse,Queue:Browse:1,Relate:CHARTYPE,SELF)
  BRW11.Init(?List:3,Queue:Browse:2.ViewPosition,BRW11::View:Browse,Queue:Browse:2,Relate:MANUFACT,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If glo:WebJob
      ?LookupStartDate{prop:Hide} = 1
      ?LookupEndDate{prop:Hide} = 1
  ELSE
      HIDE(?Tab1)
      tmp:arc = TRUE
  End !glo:WebJob
  set(defaults)
  access:defaults.next()
  Glo:CSV_SavePath = def:ExportPath
  if Glo:CSV_SavePath[len(clip(Glo:CSV_SavePath))] <> '\' then
      Glo:CSV_SavePath = clip(Glo:CSV_SavePath)&'\'
  END
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard7.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Cancel, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     0, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,sub:GenericAccountKey)
  BRW5.AddRange(sub:Generic_Account,tmp:True)
  BRW5.AddLocator(BRW5::Sort1:Locator)
  BRW5::Sort1:Locator.Init(,sub:Account_Number,1,BRW5)
  BRW5.AddSortOrder(,sub:Main_Account_Key)
  BRW5.AddRange(sub:Main_Account_Number,tmp:MainAccount)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,sub:Account_Number,1,BRW5)
  BIND('tmp:AccountTag',tmp:AccountTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(tmp:AccountTag,BRW5.Q.tmp:AccountTag)
  BRW5.AddField(sub:Account_Number,BRW5.Q.sub:Account_Number)
  BRW5.AddField(sub:Company_Name,BRW5.Q.sub:Company_Name)
  BRW5.AddField(sub:RecordNumber,BRW5.Q.sub:RecordNumber)
  BRW5.AddField(sub:Generic_Account,BRW5.Q.sub:Generic_Account)
  BRW5.AddField(sub:Main_Account_Number,BRW5.Q.sub:Main_Account_Number)
  BRW9.Q &= Queue:Browse:1
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,cha:Charge_Type_Key)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,cha:Charge_Type,1,BRW9)
  BIND('tmp:ChargeTypeTag',tmp:ChargeTypeTag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:ChargeTypeTag,BRW9.Q.tmp:ChargeTypeTag)
  BRW9.AddField(cha:Charge_Type,BRW9.Q.cha:Charge_Type)
  BRW11.Q &= Queue:Browse:2
  BRW11.RetainRow = 0
  BRW11.AddSortOrder(,man:Manufacturer_Key)
  BRW11.AddLocator(BRW11::Sort0:Locator)
  BRW11::Sort0:Locator.Init(,man:Manufacturer,1,BRW11)
  BIND('tmp:ManufacturerTag',tmp:ManufacturerTag)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW11.AddField(tmp:ManufacturerTag,BRW11.Q.tmp:ManufacturerTag)
  BRW11.AddField(man:Manufacturer,BRW11.Q.man:Manufacturer)
  BRW11.AddField(man:RecordNumber,BRW11.Q.man:RecordNumber)
  IF ?tmp:AllAccounts{Prop:Checked} = True
    DISABLE(?List)
  END
  IF ?tmp:AllAccounts{Prop:Checked} = False
    ENABLE(?List)
  END
  IF ?tmp:AllChargeTypes{Prop:Checked} = True
    DISABLE(?List:2)
  END
  IF ?tmp:AllChargeTypes{Prop:Checked} = False
    ENABLE(?List:2)
  END
  IF ?tmp:AllManufacturer{Prop:Checked} = True
    DISABLE(?List:3)
  END
  IF ?tmp:AllManufacturer{Prop:Checked} = False
    ENABLE(?List:3)
  END
  FileLookup16.Init
  FileLookup16.Flags=BOR(FileLookup16.Flags,FILE:LongName)
  FileLookup16.Flags=BOR(FileLookup16.Flags,FILE:Directory)
  FileLookup16.SetMask('All Files','*.*')
  FileLookup16.DefaultDirectory='Glo:CSV_SavePath'
  BRW5.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  BRW11.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW11.AskProcedure = 0
      CLEAR(BRW11.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard7.Validate()
        DISABLE(Wizard7.NextControl())
     ELSE
        ENABLE(Wizard7.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:Paper
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Paper, Accepted)
      if tmp:paper then
          hide(?SavePathPrompt)
          hide(Glo:Csv_savePath)
          hide(?LookupFile)
      ELSE
          if glo:webjob then
              !ignore - not applicable to thin client
          ELSE
              unhide(?SavePathPrompt)
              unhide(?Glo:CSV_SavePath)
              unhide(?lookupfile)
          END !if glo:webjob
      END !If tmp:paper
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Paper, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:AllAccounts
      IF ?tmp:AllAccounts{Prop:Checked} = True
        DISABLE(?List)
      END
      IF ?tmp:AllAccounts{Prop:Checked} = False
        ENABLE(?List)
      END
      ThisWindow.Reset
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllChargeTypes
      IF ?tmp:AllChargeTypes{Prop:Checked} = True
        DISABLE(?List:2)
      END
      IF ?tmp:AllChargeTypes{Prop:Checked} = False
        ENABLE(?List:2)
      END
      ThisWindow.Reset
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllManufacturer
      IF ?tmp:AllManufacturer{Prop:Checked} = True
        DISABLE(?List:3)
      END
      IF ?tmp:AllManufacturer{Prop:Checked} = False
        ENABLE(?List:3)
      END
      ThisWindow.Reset
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupFile
      ThisWindow.Update
      glo:CSV_SavePath = FileLookup16.Ask(1)
      DISPLAY
    OF ?VSBackButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020193'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020193'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020193'&'0')
      ***
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      ! Inserting (DBH 14/02/2008) # 9752 - Check for valid dates
      If IsDateInValid(tmp:StartDate)
          Beep(Beep:SystemHand);  Yield()
         Case Missive('Invalid Start Date.','ServiceBase 3g',|
                        'mstop.jpg','/OK')
             Of 1 ! OK Button
         End ! Case Missive
         tmp:StartDate = ''
         Select(?tmp:StartDate)
         Cycle
      End ! If IsDateInValid(tmp:StartDate)
      If IsDateInValid(tmp:EndDate)
          Beep(Beep:SystemHand);  Yield()
         Case Missive('Invalid End Date.','ServiceBase 3g',|
                        'mstop.jpg','/OK')
             Of 1 ! OK Button
         End ! Case Missive
         tmp:EndDate = ''
         Select(?tmp:EndDate)
         Cycle
      End ! If IsDateInValid(tmp:StartDate)
      
      Beep(Beep:SystemQuestion);  Yield()
      Case Missive('Confirm Date Range:'&|
          '|    From:   ' & Format(tmp:StartDate,@d18) & |
          '|    To:        ' & Format(tmp:EndDate,@d18) & |
          '||Run Report?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
          Of 1 ! No Button
              Display()
              Cycle
      End ! Case Missive
      ! End (DBH 14/02/2008) #9752
      RFExchangeFeeReport(tmp:StartDate,tmp:EndDate,tmp:AllAccounts,tmp:AllChargeTypes,tmp:AllManufacturer,tmp:ReportOrder,tmp:includearc,tmp:paper,tmp:arc)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If KeyCode() = Mouseleft2
          Post(EVENT:Accepted,?DasTag)
      End ! If KeyCode() = Mouseleft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::10:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::12:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:3)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2)  = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:AccountTag = ''
    ELSE
      tmp:AccountTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Accounttag = '*')
    SELF.Q.tmp:AccountTag_Icon = 2
  ELSE
    SELF.Q.tmp:AccountTag_Icon = 1
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = cha:Charge_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:ChargeTypeTag = ''
    ELSE
      tmp:ChargeTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:ChargeTypeTag = '*')
    SELF.Q.tmp:ChargeTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:ChargeTypeTag_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = cha:Charge_Type
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::10:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue


BRW11.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = man:Manufacturer
     GET(glo:Queue3,glo:Queue3.Pointer3)
    IF ERRORCODE()
      tmp:ManufacturerTag = ''
    ELSE
      tmp:ManufacturerTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Manufacturertag = '*')
    SELF.Q.tmp:ManufacturerTag_Icon = 2
  ELSE
    SELF.Q.tmp:ManufacturerTag_Icon = 1
  END


BRW11.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW11.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW11::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW11::RecordStatus=ReturnValue
  IF BRW11::RecordStatus NOT=Record:OK THEN RETURN BRW11::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = man:Manufacturer
     GET(glo:Queue3,glo:Queue3.Pointer3)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW11::RecordStatus
  RETURN ReturnValue

Wizard7.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW5.Q &= NULL) ! Has Browse Object been initialized?
       BRW5.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW9.Q &= NULL) ! Has Browse Object been initialized?
       BRW9.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW11.Q &= NULL) ! Has Browse Object been initialized?
       BRW11.ResetQueue(Reset:Queue)
    END

Wizard7.TakeBackEmbed PROCEDURE
   CODE

Wizard7.TakeNextEmbed PROCEDURE
   CODE

Wizard7.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0






RFExchangeFeeReport PROCEDURE(func:StartDate,func:EndDate,func:AllAccounts,func:AllChargeTypes,func:AllManufacturers,func:ReportOrder,func:includearc,func:paper,func:arc)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
save_job_id          USHORT,AUTO
tmp:main_site        STRING(30)
save_jpt_id          USHORT,AUTO
save_inv_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
AddressGroup         GROUP,PRE(address)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:CChargeType      STRING(30)
tmp:WChargeType      STRING(30)
tmp:All              BYTE(0)
ReportGroup          GROUP,PRE(report)
Exchange_IMEI        STRING(30)
Part_No              STRING(30)
Description          STRING(30)
In_Warranty_Price    REAL
Quantity             REAL
VAT                  REAL
In_Warranty_Line_Totoal REAL
JobNumber            STRING(16)
ActionDate           DATE
CompanyName          STRING(30)
Manufacturer         STRING(30)
ModelNumber          STRING(30)
ChargeType           STRING(30)
Repair               STRING(3)
Exchanged            STRING(3)
Type                 STRING(20)
HandlingFee          REAL
Parts                REAL
PartsSelling         REAL
Labour               REAL
Total                REAL
                     END
TotalGroup           GROUP,PRE(total)
Lines                LONG
HandlingFee          REAL
Labour               REAL
Parts                REAL
PartsSelling         REAL
VAT                  REAL
Total                REAL
                     END
NumberQueue          QUEUE,PRE(number)
Manufacturer         STRING(30)
ModelNumber          STRING(30)
JobNumber            LONG
ActionDate           DATE
RepairType           STRING(1)
                     END
tmp:DateRange        STRING(60)
tmp:ReportOrder      BYTE(0)
tmp:AllChargeTypes   BYTE(0)
tmp:AllManufacturers BYTE(0)
tmp:AllAccounts      BYTE(0)
tmp:ExchangeDate     DATE
ShowEmpty            BYTE
tempFilePath         CSTRING(255)
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

report               REPORT('Handling/Exchange Fee Report'),AT(458,2010,10938,5688),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,),LANDSCAPE,THOUS
                       HEADER,AT(427,323,9740,1375),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(address:CompanyName),TRN,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(104,260,3531,198),USE(address:AddressLine1),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(7448,729),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(8438,729),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,365,3531,198),USE(address:AddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,469,3531,198),USE(address:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,573),USE(address:Postcode),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(7448,573,625,208),USE(?String67),TRN,FONT(,8,,)
                         STRING(@s60),AT(8438,573),USE(tmp:printedby),TRN,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(8438,417),USE(tmp:DateRange),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(7448,417,625,208),USE(?String67:2),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(104,729),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,729),USE(address:TelephoneNumber),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(104,833),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s30),AT(573,833),USE(address:FaxNumber),TRN,FONT(,8,,)
                         STRING(@s255),AT(573,938,3531,198),USE(address:EmailAddress),TRN,FONT(,8,,)
                         STRING('Email:'),AT(104,938),USE(?String16:2),TRN,FONT(,9,,)
                         STRING('Page:'),AT(7448,885),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@s4),AT(8438,885),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(8802,885),USE(?String72),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(9010,885,313,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
break1                 BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(10,10,10875,167),USE(?DetailBand)
                           STRING(@n14.2),AT(7646,0,1146,156),USE(report:VAT),TRN,RIGHT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n8),AT(6615,0,1146,156),USE(report:Quantity,,?report:Quantity:2),TRN,RIGHT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n14.2),AT(6250,0,,156),USE(report:In_Warranty_Price),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s15),AT(146,0),USE(report:JobNumber),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(2781,0),USE(report:Part_No),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(4500,0),USE(report:Description),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n10.2),AT(9708,0),USE(report:In_Warranty_Line_Totoal),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(990,0),USE(report:Exchange_IMEI),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:3)
                           LINE,AT(198,52,10063,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,156),USE(?String68),TRN,FONT(,7,,FONT:bold)
                           STRING(@s8),AT(885,156),USE(total:Lines),FONT(,7,,FONT:bold)
                           STRING(@n12.2),AT(9583,156),USE(total:HandlingFee),TRN,HIDE,RIGHT,FONT(,7,,FONT:bold)
                         END
                       END
NewPage                DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?unnamed:4)
                       END
TitlePage              DETAIL,AT(,,,219),USE(?unnamed:6),ABSOLUTE
                       END
                       FORM,AT(406,313,10969,7448),USE(?unnamed)
                         IMAGE('Rlistlan.gif'),AT(52,0,10938,7448),USE(?Image1)
                         STRING('WARRANTY PARTS SENT TO ARC REPORT'),AT(3823,52),USE(?ReportTitle),TRN,FONT(,12,,FONT:bold)
                         STRING('Job No'),AT(208,1406),USE(?InvoiceNumberText),TRN,FONT(,7,,)
                         STRING('Part No.'),AT(2844,1406),USE(?String26:4),TRN,FONT(,7,,FONT:bold)
                         STRING('Exchange IMEI'),AT(1052,1406),USE(?InvoiceDateText),TRN,FONT(,7,,)
                         STRING('Quantity'),AT(7427,1406),USE(?String26:12),TRN,FONT(,7,,FONT:bold)
                         STRING('Unit In Warranty Price'),AT(5969,1406),USE(?String26:5),TRN,FONT(,7,,FONT:bold)
                         STRING('Description'),AT(4563,1406,,156),USE(?String26:11),TRN,FONT(,7,,FONT:bold)
                         STRING('Total VAT'),AT(8427,1406),USE(?String26:13),TRN,FONT(,7,,FONT:bold)
                         STRING('Total In Warranty Price'),AT(9208,1406),USE(?String57),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('RFExchangeFeeReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULTS.Open
  Relate:DISCOUNT.Open
  Relate:EXCHANGE.Open
  Relate:JOBPAYMT.Open
  Relate:PAYTYPES.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBS.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBSE.UseFile
  Access:SUBTRACC.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:AUDIT.UseFile
  Access:TRACHAR.UseFile
  Access:CHARTYPE.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:STOCK.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      RecordsToProcess = Records(JOBS)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Fees from Jobs
        
        !added 3/12/02 - L386 - allow export to CSV file
        if ~func:paper then
            !this is an export to csv file - dont show error when paper report is empty
            ShowEmpty=false
        
            !prepare file for export
            If GetTempPathA(255,TempFilePath)
                If Sub(TempFilePath,-1,1) = '\'
                    glo:File_Name = Clip(TempFilePath) & 'Warranty Parts Sent To ARC Report ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
                Else !If Sub(TempFilePath,-1,1) = '\'
                    glo:File_Name = Clip(TempFilePath) & '\Warranty Parts Sent To ARC Report ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
                End !If Sub(TempFilePath,-1,1) = '\'
        
                Remove(glo:File_Name)
                Access:EXPGEN.Open()
                Access:EXPGEN.UseFile()
        
                !write the title
                Clear(gen:record)
                Gen:line1   = 'WARRANTY PARTS SENT TO ARC PORT    (Version: ' & Clip(ProgramVersionNumber()) & ')'
                access:expgen.insert()
        
                Clear(gen:record)
                Gen:line1   = 'Date Range:,' & tmp:DateRange
                Access:expgen.insert()
                Clear(gen:record)
                Gen:line1   = 'Printed By,' & tmp:printedby
                Access:expgen.insert()
                Clear(gen:record)
                Gen:line1   = 'Date Printed,'&format(Today(),@d6)
                Access:expgen.insert()
        
                Clear(gen:record)
                gen:line1 = 'Job No,Branch ID,Franchise Job No,Exchange IMEI,Exchange Model No,Part No,Description,Qty,Unit In Warranty Price,Total VAT,Total In Warranty Price'
                Access:EXPGEN.Insert()
        
            ELSE
                if glo:webjob then
                    Case Missive('Unable to source a temporary directory on the web server. Please report this error to the IT Department.'&|
                      '<13,10>'&|
                      '<13,10>A paper report will now be prepared.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                ELSE
                    Case Missive('Unable to souce a temporary directory on your computer. Please setup a temporary directory for Windows to use.'&|
                      '<13,10>'&|
                      '<13,10>A paper report will now be prepared.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                END
                !Can't get temp file - print a paper file
                showEmpty = true
                func:paper = 1
            END
        ELSE
            ShowEmpty=true
        END !if not paper report
        !end of added 3/12/02 - L386 - allow export to CSV file - more at bottom of proc
        
        
        Clear(TotalGroup)
        
        Prog.ProgressSetup(Records(JOBS))
        
        Save_job_ID = Access:JOBS.SaveFile()
        
        ! This report is SLOW, we cannot use a key
        set(job:Ref_Number_Key)
        Loop
            If Access:JOBS.NEXT()
               Break
            End !If
            If Prog.InsideLoop()
                Break
            End !If Prog.InsideLoop()
        
            clear(NumberQueue)
        
            If ~tmp:AllAccounts
                Sort(glo:Queue,glo:Pointer)
                glo:Pointer = job:Account_Number
                Get(glo:Queue,glo:Pointer)
                If Error()
                    Cycle
                End !If Error()
            End !If ~tmp:AllAccounts
        
            If ~tmp:AllChargeTypes
                Sort(glo:Queue2,glo:Pointer2)
                glo:Pointer2    = job:Warranty_Charge_Type
                Get(glo:Queue2,glo:Pointer2)
                If Error()
                    Cycle
                End !If Error()
            End !If ~tmp:AllChargeTypes
        
            If ~tmp:AllManufacturers
                Sort(glo:Queue3,glo:Pointer3)
                glo:Pointer3    = job:Manufacturer
                Get(glo:Queue3,glo:Pointer3)
                If Error()
                    Cycle
                End !If Error()
            End !If ~tmp:AllManufacturers
        
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found
                If glo:WebJob
                    If wob:HeadAccountNumber <> Clarionet:Global.Param2
                        Cycle
                    End !If wob:HeadAccountNumber <> Clarionet:Global.Param2
        
                Else !If glo:WebJob
                    If wob:HeadAccountNumber = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                        Cycle
                    End !If wob:HeadAccountNumber <> GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                End !If glo:WebJob
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
                cycle
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
            number:Manufacturer = job:Manufacturer
            number:ModelNumber  = job:Model_Number
            number:JobNumber    = job:Ref_Number
        
            tmp:ExchangeDate = ''
        
            if job:Exchange_Unit_Number <> 0
                Access:Audit.CLEARKEY(aud:TypeRefKey)
                aud:Ref_Number = job:Ref_Number
                aud:Type = 'EXC'
                SET(aud:TypeRefKey,aud:TypeRefKey)
                IF Access:Audit.NEXT() THEN
                    tmp:ExchangeDate = ''
                ELSE
                    IF aud:Ref_Number = job:Ref_Number AND aud:Type = 'EXC' THEN
                        tmp:ExchangeDate = aud:Date
                    ELSE
                        tmp:ExchangeDate = aud:Date
                    END !IF
                END !IF
            end
        
            if (tmp:ExchangeDate <> '' and jobe:ExchangedATRRC = true) AND job:Warranty_Job = 'YES'
                number:RepairType   = 'E' ! Exchange
                number:ActionDate   = tmp:ExchangeDate
            end
        
        !    if jobe:OBFvalidateDate <> ''
        !        number:RepairType   = 'O' ! OBF
        !        number:ActionDate   = jobe:OBFvalidateDate
        !    end
        
            if number:RepairType = '' then cycle.
        
            if number:ActionDate < tmp:StartDate then cycle.
            if number:ActionDate > tmp:EndDate then cycle.
        
            Add(NumberQueue)
        End !Loop
        Access:JOBS.RestoreFile(Save_job_ID)
        
        Prog.ProgressFinish()
        
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
             tmp:main_Site = tra:SiteLocation
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
        
        RecordsToProcess = Records(NumberQueue)
        RecordsProcessed = 0
        
        Case tmp:ReportOrder
            Of 0
                Sort(NumberQueue,number:JobNumber)
            Of 1
                Sort(NumberQueue,number:ActionDate)
            Of 2
                Sort(NumberQueue,number:Manufacturer,number:ModelNumber,number:JobNumber)
        End !tmp:ReportType
        
        Prog.ProgressSetup(Records(NumberQueue))
        
        Loop x# = 1 To Records(NumberQueue)
            Get(NumberQueue,x#)
            If Prog.InsideLoop()
                Break
            End !If Prog.InsideLoop()
        
            Clear(ReportGroup)
        
            report:ActionDate = number:ActionDate
        
            case number:RepairType
                of 'R'
                    report:Type = 'REPAIR'
                of 'E'
                    report:Type = 'EXCH'
                of 'O'
                    report:Type = 'OBF'
            end
        
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = number:JobNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
        
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        
        
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
        
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found
        
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number  = job:Account_Number
            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Found
                report:CompanyName = sub:Company_Name
            Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        
        
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = wob:HeadAccountNumber
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                 report:JobNumber     = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
        
            report:Manufacturer = job:Manufacturer
            report:ModelNumber  = job:Model_Number
        
            Access:Warparts.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number = job:ref_number
            SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
            LOOP
              IF Access:WarParts.Next()
                BREAK
              END
              IF wpr:Ref_Number <> job:ref_number
                BREAK
              END
              IF wpr:Part_number = 'EXCH'
                CYCLE
              END
              Access:Stock.ClearKey(sto:Ref_Number_Key)
              sto:Ref_Number = wpr:Part_Ref_Number
              IF Access:Stock.Fetch(sto:Ref_Number_Key)
                !Error!
                CYCLE
              ELSE
                IF sto:Location = tmp:main_site
                  CYCLE
                END
              END
              !Get Exchange!
              Access:Exchange.ClearKey(xch:Ref_Number_Key)
              xch:Ref_Number = job:Exchange_Unit_Number
              IF Access:Exchange.Fetch(xch:Ref_Number_Key)
                !Error
                report:Exchange_IMEI = 'UNKNOWN'
              ELSE
                report:Exchange_IMEI = xch:ESN
              END
              report:Part_No = wpr:Part_Number
              report:Description = wpr:Description
              report:In_Warranty_Price = wpr:RRCPurchaseCost
              report:Quantity = wpr:Quantity
              report:VAT = wpr:Quantity * (wpr:RRCPurchaseCost * (VatRate(job:Account_Number,'P') /100))
              report:In_Warranty_Line_Totoal = (wpr:RRCPurchaseCost * wpr:Quantity) + report:VAT
              total:lines+=1
              if func:paper
                  Print(rpt:Detail)
              ELSE
                  Clear(gen:record)
                  gen:line1 = clip(job:ref_number)&','&clip(tra:BranchIdentification)&','&clip(wob:JobNumber)&','&clip(format(report:Exchange_IMEI))&','&clip(xch:Model_Number)&','&clip(report:Part_No)&','&|
                              clip(report:Description)&','&clip(FORMAT(report:Quantity,@n_10.2))&','&clip(FORMAT(report:In_Warranty_Price,@n_10.2))&','&|
                              clip(FORMAT(report:VAT,@N_10.2))&','&clip(FORMAT(report:In_Warranty_Line_Totoal,@n_10.2))
                  Access:EXPGEN.Insert()
              END
            END
        
        End !x# = 1 To Records(NumberQueue)
        Prog.ProgressFinish()
        
        !added 3/12/02 - L386 - allow export to CSV file
        if func:paper
            !all done and finished
        ELSE
            !Print the final lines
            Clear(gen:Record)
            gen:Line1   = 'TOTALS'
            Access:EXPGEN.Insert()
        
            Clear(gen:Record)
            Gen:line1   =   'Parts Counted,'&clip(Total:Lines)&',,,,,,,,,'
            Access:EXPGEN.Insert()
        
            Access:EXPGEN.close()
        
            if Total:lines = 0 then
                Case Missive('There are no records that match the selected criteria.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            ELSE
                if glo:webjob then
                    !send the file to the client
                    SendFileToClient(glo:File_Name)
                ELSe
                    !copy it to where it is expected
                    copy(glo:file_name,Glo:CSV_SavePath)
                END
        
                Case Missive('"Warranty Parts Sent To ARC Report" Created.','ServiceBase 3g',|
                               'midea.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            END !if total:lines = 0
        
            !remove the file
            Remove(glo:File_name)
        END!if tmp:paper
        
        !end of - added 3/12/02 - L386 - allow export to CSV file
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF ShowEmpty = TRUE
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'c:\REPORT.TXT'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
        IF ShowEmpty = TRUE
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF ShowEmpty = TRUE
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
        IF ShowEmpty = TRUE
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DISCOUNT.Close
    Relate:EXCHANGE.Close
    Relate:JOBPAYMT.Close
    Relate:PAYTYPES.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Before Embed Point: %EndOfProcedure) DESC(End of Procedure) ARG()
  If func:Paper
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  End ! If func:Paper
  ! After Embed Point: %EndOfProcedure) DESC(End of Procedure) ARG()
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
  tmp:AllAccounts = func:AllAccounts
  tmp:AllChargeTypes  = func:AllChargeTypes
  tmp:AllManufacturers    = func:AllManufacturers
  tmp:ReportOrder = func:ReportOrder
  
  tmp:DateRange   = Format(tmp:StartDate,@d6) & ' to ' & Format(tmp:EndDate,@d6)
  
  If ~glo:WebJob
      address:CompanyName     = def:User_Name
      address:AddressLine1    = def:Address_Line1
      address:AddressLine2    = def:Address_Line2
      address:AddressLine3    = def:Address_Line3
      address:Postcode        = def:Postcode
      address:TelephoneNumber = def:Telephone_Number
      address:FaxNumber       = def:Fax_Number
      address:EmailAddress    = def:EmailAddress
  Else !glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          address:CompanyName     = tra:Company_Name
          address:AddressLine1    = tra:Address_Line1
          address:AddressLine2    = tra:Address_Line2
          address:AddressLine3    = tra:Address_Line3
          address:Postcode        = tra:Postcode
          address:TelephoneNumber = tra:Telephone_Number
          address:FaxNumber       = tra:Fax_Number
          address:EmailAddress    = tra:EmailAddress
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  End !glo:WebJob
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='RFExchangeFeeReport'
  END
  report{Prop:Preview} = PrintPreviewImage


Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: UnivAbcReport
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 500
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0











FaultyStockReturnNote PROCEDURE(func:RefNumber,func:Quantity)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:Quantity         LONG
tmp:PartNumber       STRING(30)
tmp:Description      STRING(30)
tmp:ItemCost         REAL
tmp:LineCost         REAL
tmp:RunningTotal     LONG
save_shi_id          USHORT,AUTO
tmp:TotalLineCost    REAL
tmp:Total            LONG
AddressGroup         GROUP,PRE(address)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(STOHIST)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,781),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,781),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,781),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6302,781,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s30),AT(781,0),USE(tmp:PartNumber),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(2760,0),USE(tmp:Description),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@n14.2),AT(4844,0),USE(tmp:ItemCost),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(5990,0),USE(tmp:LineCost),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s8),AT(156,0),USE(tmp:Quantity),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(167,52,7188,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(156,104),USE(?String28),TRN,FONT(,8,,FONT:bold)
                           STRING('Total: '),AT(4896,104),USE(?String31),TRN,FONT(,8,,FONT:bold)
                           STRING(@n14.2),AT(5990,104),USE(tmp:TotalLineCost),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s8),AT(1510,104),USE(tmp:Total),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(address:CompanyName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('FAULTY STOCK RETURN NOTE'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(address:AddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(address:AddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(address:AddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1042),USE(address:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354),USE(address:EmailAddress),TRN,FONT(,9,,)
                         STRING('Qty'),AT(417,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(781,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(2760,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost'),AT(5188,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6333,2083),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('FaultyStockReturnNote')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  !
  ! EMBED ID:           %ProcedureSetup
  ! EMBED Description:  Procedure Setup
  ! EMBED Parameters:   
  ! CW Version:         5507
  !
  ! #AT(%ProcedureSetup,) <- Place quotes around non-numeric parameters
  !
  ! After Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:STOHIST.Open
  Relate:STOCK_ALIAS.Open
  
  
  RecordsToProcess = BYTES(STOHIST)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(STOHIST,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
            tmp:RunningTotal = func:Quantity
            Access:STOCK_ALIAS.Clearkey(sto_ali:Ref_Number_Key)
            sto_ali:Ref_Number  = func:RefNumber
            If Access:STOCK_ALIAS.Tryfetch(sto_ali:Ref_Number_Key) = Level:Benign
                !Found
                tmp:PartNumber  = sto_ali:Part_Number
                tmp:Description = sto_ali:Description
            Else ! If Access:STOCK_ALIAS.Tryfetch(sto_ali:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:STOCK_ALIAS.Tryfetch(sto_ali:Ref_Number_Key) = Level:Benign
        
            Save_shi_ID = Access:STOHIST.SaveFile()
            Access:STOHIST.ClearKey(shi:Transaction_Type_Key)
            shi:Ref_Number       = func:RefNumber
            shi:Transaction_Type = 'ADD'
            shi:Date             = Today()
            Set(shi:Transaction_Type_Key,shi:Transaction_Type_Key)
            Loop
                If Access:STOHIST.Previous()
                   Break
                End !If
                If shi:Ref_Number       <> func:RefNumber      |
                Or shi:Transaction_Type <> 'ADD'      |
                    Then Break.  ! End If
        
                If shi:Notes = 'STOCK ADDED FROM ORDER' Or shi:Notes = 'INITIAL STOCK QUANTITY'
                    If shi:Quantity <= tmp:RunningTotal
                        tmp:Quantity    = shi:Quantity
                        tmp:ItemCost    = shi:Purchase_Cost
                        tmp:LineCost    = shi:Purchase_Cost * shi:Quantity
                        tmp:RunningTotal -= shi:Quantity
                    Else
                        tmp:Quantity    = tmp:RunningTotal
                        tmp:ItemCost    = shi:Purchase_Cost
                        tmp:LineCost    = shi:Purchase_Cost * tmp:RunningTotal
                        tmp:RunningTotal = 0
                    End !If shi:Quantity > tmp:RunningTotal
        
                    tmp:Total += 1
                    tmp:TotalLineCost += tmp:LineCost
        
                    Print(rpt:Detail)
        
                    !In case total falls below zero - 4003 (DBH: 25-03-2004)
                    If tmp:RunningTotal <= 0
                        Break
                    End !If tmp:RunningTotal = func:Quantity
        
                End !If shi:Notes = 'STOCK ADDED FROM ORDER'
            End !Loop
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(STOHIST,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:STOCK_ALIAS.Close
    Relate:STOHIST.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !*********CHANGE LICENSE ADDRESS*********
  set(defaults)
  access:defaults.next()
  
      if glo:WebJob then
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = Clarionet:Global.Param2
          IF access:tradeacc.fetch(tra:account_number_key)
            !Error!
          END
          address:CompanyName            = tra:Company_Name
          address:AddressLine1    = tra:Address_Line1
          address:AddressLine2    = tra:Address_Line2
          address:AddressLine3    = tra:Address_Line3
          address:Postcode        = tra:Postcode
          address:TelephoneNumber       = tra:Telephone_Number
          address:FaxNumber             = tra:Fax_Number
          address:EmailAddress    = tra:EmailAddress
      Else
          address:CompanyName            = def:User_Name
          address:AddressLine1    = def:Address_Line1
          address:AddressLine2    = def:Address_Line2
          address:AddressLine3    = def:Address_Line3
          address:Postcode        = def:Postcode
          address:TelephoneNumber       = def:Telephone_Number
          address:FaxNumber             = def:Fax_Number
          address:EmailAddress    = def:EmailAddress
      end
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Faulty Stock Return Note'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END







LoanUnitReportCriteria PROCEDURE                      !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
save_aud_id          USHORT,AUTO
save_loa_id          USHORT,AUTO
tmp:LoanStatus       STRING(30)
tmp:ReportType       BYTE(0)
tmp:ExportPath       STRING(255)
tmp:Available        STRING(30)
tmp:StockType        STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
FDB10::View:FileDrop VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?tmp:StockType
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Loan Unit Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Loan Unit Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Loan Unit Report Criteria'),USE(?Tab1)
                           LIST,AT(301,144,124,10),USE(tmp:LoanStatus),VSCROLL,LEFT(2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),DROP(10),FROM('AVAILABLE|DESPATCHED')
                           OPTION('Report Type'),AT(301,162,124,28),USE(tmp:ReportType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Paper Report'),AT(309,174),USE(?tmp:ReportType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Export File'),AT(373,174),USE(?tmp:ReportType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           PROMPT('Export Path'),AT(225,200),USE(?tmp:ExportPath:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(301,200,124,10),USE(tmp:ExportPath),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           LIST,AT(301,221,124,10),USE(tmp:StockType),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDrop)
                           STRING('Date From:'),AT(225,242),USE(?String3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d17),AT(301,242,60,11),USE(tmp:StartDate),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(368,238),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@d17),AT(301,264,60,10),USE(tmp:EndDate),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(368,259),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                           STRING('Date To:'),AT(225,264),USE(?String4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           STRING('Stock Type'),AT(225,221),USE(?String2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(429,196),USE(?LookupExportPath),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Loan Status'),AT(225,144),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

! Clarionet Moving Bar
CN:recordstoprocess     long,auto
CN:recordsprocessed     long,auto
CN:recordspercycle      long,auto
CN:recordsthiscycle     long,auto
CN:percentprogress      byte
CN:recordstatus         byte,auto

CN:progress:thermometer byte
CN:progresswindow WINDOW('Please Wait...'),AT(,,164,22),FONT('Arial',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(CN:progress:thermometer),AT(4,4,156,12),RANGE(0,100)
     END


TempFilePath         CSTRING(255)
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup6          SelectFileClass
FDB10                CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Export      Routine
    If glo:WebJob
        If GetTempPathA(255,TempFilePath)
            If Sub(TempFilePath,-1,1) = '\'
                glo:File_Name = Clip(TempFilePath) & 'Loan Phone Report ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
            Else !If Sub(TempFilePath,-1,1) = '\'
                glo:File_Name = Clip(TempFilePath) & '\Loan Phone Report ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
            End !If Sub(TempFilePath,-1,1) = '\'
        End
    Else !If glo:WebJob
        If Sub(tmp:ExportPath,-1,1) = '\'
            glo:File_Name = Clip(tmp:ExportPath) & 'Loan Phone Report ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
        Else !If Sub(TempFilePath,-1,1) = '\'
            glo:File_Name = Clip(tmp:ExportPath) & '\Loan Phone Report ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
        End !If Sub(TempFilePath,-1,1) = '\'

    End !If glo:WebJob

    Remove(glo:File_Name)
    Access:EXPGEN.Open()
    Access:EXPGEN.UseFile()

    Clear(gen:Record)
    gen:Line1 = 'LOAN UNIT REPORT  (Version: ' & Clip(ProgramVersionNumber()) & ')'
    Access:EXPGEN.Insert()

    Clear(gen:Record)
    gen:Line1 = 'I.M.E.I. Number,' &|
                'M.S.N.,' &|
                'Manufacturer,' &|
                'Model Number,' &|
                'Stock Type,' &|
                'Unit Number,' &|
                'Unit Status,' &|
                'Status Date,' &|
                'Reason Not Available,' & |
                'Job Number,' &|
                'Date Job Booked,' &|
                'Date Loan Issued,' &|
                'Contact Name,' &|
                'Contact Number,' &|
                'Current Job Status,' &|
                'Current Exchange Status,' &|
                'Current Loan Status,' &|
                'Delivery Address Line 1,' &|
                'Delivery Address Line 2,' &|
                'Delivery Address Line 3,' &|
                'Delivery Address Line 4'
    Access:EXPGEN.Insert()

    If glo:WebJob
        CN:recordspercycle         = 25
        CN:recordsprocessed        = 0
        CN:percentprogress         = 0
        CN:progress:thermometer    = 0
        ClarioNet:OpenPushWindow(CN:ProgressWindow)

        CN:recordstoprocess    = Records(LOAN)

    Else !If glo:WebJob
        Prog.ProgressSetup(Records(LOAN))
    End !If glo:WebJob

    Save_loa_ID = Access:LOAN.SaveFile()
    Access:LOAN.ClearKey(loa:LocIMEIKey)
    loa:Location = use:Location
    Set(loa:LocIMEIKey,loa:LocIMEIKey)
    Loop
        If Access:LOAN.NEXT()
           Break
        End !If
        If loa:Location <> use:Location      |
            Then Break.  ! End If


        If tmp:LoanStatus <> ''
            If loa:Available <> tmp:Available
                Cycle
            End !If loa:Available <> tmp:Available

        End !If tmp:Status <> ''



        !Paul 02/06/2009 Log No 10684
        If clip(tmp:StockType) <> '' then
            If clip(loa:Stock_Type) <> clip(tmp:StockType) then
                Cycle
            End !If clip(loa:Stock_Type) <> clip(tmp:StockType) then
        End !clip(tmp:StockType) <> '' then

        If tmp:StartDate > 0 then
            If loa:StatusChangeDate < tmp:StartDate then
                !Date is before start date so ignore
                Cycle
            End !If loa:StatusChangeDate < tmp:StartDate then
        End !If tmp:StartDate > 0 then

        If tmp:EndDate > 0 then
            If loa:StatusChangeDate > tmp:EndDate then
                !Date is after to date so ignore
                Cycle
            End !If loa:StatusChangeDate > tmp:EndDate then
        End !If tmp:EndDate > 0 then



        If glo:WebJob
            Do CN:GetNextRecord
            Clarionet:UpdatePushWindow(CN:ProgressWindow)

        Else !If glo:WebJob
            If Prog.InsideLoop()
                Break
            End !If Prog.InsideLoop()

        End !If glo:WebJob



        Clear(gen:Record)
        gen:Line1   = Clip(loa:ESN)
        gen:Line1   = Clip(gen:Line1) & ',' & Clip(loa:MSN)
        gen:Line1   = Clip(gen:Line1) & ',' & Clip(loa:Manufacturer)
        gen:Line1   = Clip(gen:Line1) & ',' & Clip(loa:Model_Number)
        gen:Line1   = Clip(gen:Line1) & ',' & Clip(loa:Stock_Type)
        gen:Line1   = Clip(gen:Line1) & ',' & Clip(loa:Ref_Number)
        gen:Line1   = Clip(gen:Line1) & ',' & Clip(tmp:LoanStatus)
        gen:Line1   = Clip(gen:Line1) & ',' & Clip(Format(loa:StatusChangeDate,@d06b))
        ! Inserting (DBH 15/06/2007) # 4123 - Show the loan unavailable reason
        Found# = 0
        If loa:Available = 'NOA'
            Access:LOANHIST.Clearkey(loh:Ref_Number_Key)
            loh:Ref_Number = loa:Ref_Number
            Set(loh:Ref_Number_Key,loh:Ref_Number_Key)
            Loop ! Begin Loop
                If Access:LOANHIST.Next()
                    Break
                End ! If Access:LOANHIST.Next()
                If loh:Ref_Number <> loa:Ref_Number
                    Break
                End ! If loh:Ref_Number <> loa:Ref_Number
                If Instring('UNIT AVAILABLE',loh:Status,1,1)
                    Cycle
                End ! If Instring('UNIT AVAILABLE',loh:Status,1,1)
                If Instring('UNIT DESPATCHED',loh:Status,1,1)
                    Cycle
                End ! If Instring('UNIT DESPATCHED',loh:Status,1,1)
                If Instring('UNIT RETURNED',loh:Status,1,1)
                    Cycle
                End ! If Instring('UNIT RETURNED',log:Status,1,1)
                If Instring('UNIT LOANED',loh:Status,1,1)
                    Cycle
                End ! If Instring('UNIT LOANED',loh:Status,1,1)
                If Instring('UNIT MOVED FROM',loh:Status,1,1)
                    Cycle
                End ! If Instring('UNIT MOVED FROM',loh:Status,1,1)
                If Instring('UNIT RE-STOCKED',loh:Status,1,1)
                    Cycle
                End ! If Instring('UNIT RE-STOCKED',loh:Status,1,1)
                If Instring('INITIAL ENTRY',loh:Status,1,1)
                    Cycle
                End ! If Instring('INITIAL ENTRY',loh:Status,1,1)
                If Instring('REPLACE LOAN',loh:Status,1,1)
                    Cycle
                End ! If Instring('REPLACE LOAN',loh:Status,1,1)
                If Instring('AWAITING QA',loh:Status,1,1)
                    Cycle
                End ! If Instring('AWAITING QA',loh:Status,1,1)
                If Instring('RAPID QA UPDATE',loh:Status,1,1)
                    Cycle
                End ! If Instring('RAPID QA UPDATE',loh:Status,1,1)
                If Instring('QA PASS ON JOB',loh:Status,1,1)
                    Cycle
                End ! If Instring('QA PASS ON JOB',loh:Status,1,1)
                If Instring('AUDIT SHORTAGE',loh:Status,1,1)
                    Cycle
                End ! If Instring('AUDIT SHORTAGE',loh:Status,1,1)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(loh:Status)
                Found# = 1
                Break
            End ! Loop
        End ! If tmp:LoanStatus = 'NOT AVAILABLE'
        If Found# = 0
            gen:Line1 = Clip(gen:Line1) & ','
        End ! If Found# = 0
        ! End (DBH 15/06/2007) #4123
        If loa:Job_Number <> 0
            gen:Line1   = Clip(gen:line1) & ',' & Clip(loa:Job_Number)
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = loa:Job_Number
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(Format(job:Date_Booked,@d06b))
                !Find date attached
                Date$ = 0
                Save_aud_ID = Access:AUDIT.SaveFile()
                Access:AUDIT.ClearKey(aud:Action_Key)
                aud:Ref_Number = job:Ref_Number
                aud:Action     = 'LOAN UNIT ATTACHED TO JOB'
                Set(aud:Action_Key,aud:Action_Key)
                Loop
                    If Access:AUDIT.NEXT()
                       Break
                    End !If
                    If aud:Ref_Number <> job:Ref_Number      |
                    Or aud:Action     <> 'LOAN UNIT ATTACHED TO JOB'      |
                        Then Break.  ! End If
                    Date$   = aud:Date
                    Break
                End !Loop
                Access:AUDIT.RestoreFile(Save_aud_ID)

                If Date$ <> 0
                    gen:Line1   = Clip(gen:Line1) & ',' & Clip(Format(Date$,@d06b))
                Else !If Date$ <> 0
                    gen:Line1   = Clip(gen:Line1) & ',' & Clip(Format(job:Date_Booked,@d06b))
                End !If Date$ <> 0

                gen:Line1   = Clip(gen:Line1) & ',' & Clip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(job:Telephone_Delivery)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(job:Current_Status)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(job:Exchange_Status)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(job:Loan_Status)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(job:Address_Line1_Delivery)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(job:Address_Line2_Delivery)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(job:Address_Line3_Delivery)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(job:Postcode_Delivery)
                Access:EXPGEN.Insert()

            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        Else !If loa:Job_Number <> 0
            gen:Line1   = Clip(gen:Line1) & ',,,,,,,,,,,'
            Access:EXPGEN.Insert()
        End !If loa:Job_Number <> 0
    End !Loop
    Access:LOAN.RestoreFile(Save_loa_ID)
    Access:EXPGEN.Close()

    If glo:WebJob
        Do CN:EndPrintRun
        ClarioNet:UpdatePushWindow(CN:ProgressWindow)
        ClarioNet:ClosePushWindow(CN:ProgressWindow)

        SendFileToClient(glo:File_Name)
    Else !If glo:WebJob
        Prog.ProgressFinish()
    End !If glo:WebJob

    Case Missive('Export Completed.','ServiceBase 3g',|
                   'midea.jpg','/OK')
        Of 1 ! OK Button
    End ! Case Missive
CN:getnextrecord      routine
    CN:recordsprocessed += 1
    CN:recordsthiscycle += 1
    if CN:percentprogress < 100
      CN:percentprogress = (CN:recordsprocessed / CN:recordstoprocess)*100
      if CN:percentprogress > 100
        CN:percentprogress = 100
      end
      if CN:percentprogress <> CN:progress:thermometer then
        CN:progress:thermometer = CN:percentprogress
      end
    end

CN:endprintrun         routine
    CN:progress:thermometer = 100
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020192'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('LoanUnitReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:LOAN.Open
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:LOANHIST.UseFile
  SELF.FilesOpened = True
  tmp:ExportPath = Upper(Path())
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?tmp:LoanStatus{prop:from} = '|AVAILABLE|DESPATCHED|ELECTRONIC QA REQUIRED|FAULTY|IN REPAIR|INCOMING TRANSIT|LOAN LOST|LOANED|MANUAL QA RQUIRED|NOT AVAILABLE|QA FAILED'
  
  !Paul 03/06/2009 Log No 10684
  If glo:WebJob = 1 Then
      !hide both of the calendar buttons
      ?PopCalendar{prop:Hide} = True
      ?PopCalendar:2{prop:Hide} = True
      !autofill with todays date
      tmp:StartDate   = today()
      tmp:EndDate     = today()
  End !If glo:WebJob = 1 Then
  
  ?tmp:LoanStatus{prop:vcr} = TRUE
  ?tmp:StockType{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FileLookup6.Init
  FileLookup6.Flags=BOR(FileLookup6.Flags,FILE:LongName)
  FileLookup6.Flags=BOR(FileLookup6.Flags,FILE:Directory)
  FileLookup6.SetMask('All Files','*.*')
  FDB10.Init(?tmp:StockType,Queue:FileDrop.ViewPosition,FDB10::View:FileDrop,Queue:FileDrop,Relate:STOCKTYP,ThisWindow)
  FDB10.Q &= Queue:FileDrop
  FDB10.AddSortOrder(stp:Stock_Type_Key)
  FDB10.AddField(stp:Stock_Type,FDB10.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDB10.WindowComponent)
  FDB10.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:LOAN.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020192'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020192'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020192'&'0')
      ***
    OF ?tmp:ReportType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ReportType, Accepted)
      Case tmp:ReportType
          Of 0
              ?tmp:ExportPath{prop:Hide} = 1
              ?tmp:ExportPath:Prompt{prop:Hide} = 1
              ?LookupExportPath{prop:Hide} = 1
          Of 1
              If ~glo:WebJob
                  ?tmp:ExportPath{prop:Hide} = 0
                  ?tmp:ExportPath:Prompt{prop:Hide} = 0
                  ?LookupExportPath{prop:Hide} = 0
              End !If ~glo:WebJob
      End !tmp:ReportType
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ReportType, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1()
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1()
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupExportPath
      ThisWindow.Update
      tmp:ExportPath = Upper(FileLookup6.Ask(1)  )
      DISPLAY
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
          Case tmp:LoanStatus
              Of 'AVAILABLE'
                  tmp:Available = 'AVL'
              Of 'LOANED'
                  tmp:Available = 'LOA'
              Of 'LOAN LOST'
                  tmp:Available = 'LOS'
              Of 'INCOMING TRANSIT'
                  tmp:Available = 'INC'
              Of 'FAULTY'
                  tmp:Available = 'FAU'
              Of 'NOT AVAILABLE'
                  tmp:Available = 'NOA'
              Of 'IN REPAIR'
                  tmp:Available = 'INR'
              Of 'SUSPENDED'
                  tmp:Available = 'SUS'
              Of 'DESPATCHED'
                  tmp:Available = 'DES'
              Of 'ELECTRONIC QA REQUIRED'
                  tmp:Available = 'QA1'
              Of 'MANUAL QA REQUIRED'
                  tmp:Available = 'QA2'
              Of 'QA FAILED'
                  tmp:Available = 'QAF'
              Of 'RETURN TO STOCK'
                  tmp:Available = 'RTS'
              Of ''
                  tmp:Available = ''
          End !Case tmp:LoanStatus
      
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
      
          Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
          End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
          Case tmp:ReportType
              Of 0
      
                  LoanPhoneReport(tmp:Available,use:Location,tmp:StockType,tmp:StartDate,tmp:EndDate)
              Of 1
                  Do Export
          End !tmp:Type
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?tmp:LoanStatus{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:LoanStatus{prop:Use} = DBHControl{prop:Use}
        IF ?tmp:StockType{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:StockType{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0






LoanPhoneReport PROCEDURE(func:Available,func:Location,func:StockType,func:Fromdate,func:todate)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:Status           STRING(60)
tmp:Contact          STRING(60)
tmp:Available        STRING(30)
tmp:Total            LONG
tmp:Location         STRING(30)
tmp:StockType        STRING(30)
tmp:FromDate         DATE
tmp:ToDate           DATE
AddressGroup         GROUP,PRE(address)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
Postcode             STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(LOAN)
                       PROJECT(loa:ESN)
                       PROJECT(loa:Model_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,781),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,781),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,781),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,781,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s16),AT(104,0),USE(loa:ESN),TRN,FONT(,8,,)
                           STRING(@s20),AT(1198,0),USE(loa:Model_Number),TRN,FONT(,8,,)
                           STRING(@s60),AT(2500,0,2240,208),USE(tmp:Status),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s60),AT(4844,0,2396,208),USE(tmp:Contact),TRN,FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(167,52,7188,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(156,104),USE(?String28),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(1510,104),CNT,USE(tmp:Total),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(address:CompanyName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('LOAN PHONE REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(address:AddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(address:AddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(address:AddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1042),USE(address:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354),USE(address:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('I.M.E.I. Number'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Model No'),AT(1198,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Status'),AT(2500,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Contact'),AT(4844,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('LoanPhoneReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  tmp:Location    = func:Location
  tmp:Available   = func:Available
  tmp:StockType   = func:StockType
  tmp:FromDate    = func:Fromdate
  tmp:ToDate      = func:todate
  
  ! After Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('tmp:Available',tmp:Available)
  BIND('tmp:Location',tmp:Location)
  BIND('tmp:Location',tmp:Location)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:LOAN.Open
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(LOAN)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(LOAN,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(loa:LocIMEIKey)
      Process:View{Prop:Filter} = |
      'UPPER(loa:Location) = UPPER(tmp:Location)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        CASE (loa:Available)
        OF 'AVL'
          tmp:Status = 'AVAILABLE'
        OF 'LOA'
          tmp:Status = 'LOANED - JOB NO: ' & loa:Job_Number
        OF 'LOS'
          tmp:Status = 'LOAN LOST'
        OF 'INC'
          tmp:Status = 'INCOMING TRANSIT - JOB NO: ' & loa:Job_Number
        OF 'FAU'
          tmp:Status = 'FAULTY'
        Of 'NOA'
          tmp:Status = 'NOT AVAILABLE'
        OF 'REP'
          tmp:Status = 'IN REPAIR - JOB NO: ' & loa:Job_Number
        OF 'SUS'
          tmp:Status = 'SUSPENDED'
        OF 'DES'
          tmp:Status = 'DESPATCHED - JOB NO: ' & loa:Job_Number
        OF 'QA1'
          tmp:Status = 'ELECTRONIC QA REQUIRED - JOB NO: ' & loa:Job_Number
        OF 'QA2'
          tmp:Status = 'MANUAL QA REQUIRED - JOB NO: ' & loa:Job_Number
        OF 'QAF'
          tmp:Status = 'QA FAILED'
        OF 'RTS'
          tmp:Status = 'RETURN TO STOCK'
        ELSE
          tmp:Status = 'IN REPAIR - JOB NO: ' & loa:Job_Number
        END
        
        
        If loa:Job_Number <> 0
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = loa:Job_Number
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
                tmp:Contact = Clip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname) & ' ' & Clip(job:Telephone_Delivery)
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        
        Else !loa:Job_Number <> 0
            tmp:Contact = ''
        End !loa:Job_Number <> 0
        
        Print# = 1
        
        If tmp:Available <> ''
            If loa:Available <> tmp:Available
                Print# = 0
            End !If loa:Available <> tmp:Available
        End !tmp:Available = ''
        
        !Paul 03/06/2009 Log No 10684
        If clip(tmp:StockType) <> '' then
            If Clip(loa:Stock_Type) <> clip(tmp:StockType) then
                Print# = 0
            End !If clip(tmp:StockType) <> '' then
        End !If clip(tmp:StockType) <> '' then
        
        If tmp:FromDate > 0 then
            If loa:StatusChangeDate < tmp:FromDate then
                Print# = 0
            End !If loa:StatusChangeDate < tmp:FromDate then
        End !If tmp:FromDate > 0 then
        
        If tmp:ToDate > 0 then
            If loa:StatusChangeDate > tmp:ToDate then
                Print# = 0
            End !If loa:StatusChangeDate > tmp:ToDate then
        End !If tmp:ToDate > 0 then
        
        
        
        If Print#
            Print(rpt:Detail)
        End !Print#
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(LOAN,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:LOAN.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'LOAN')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  If ~glo:WebJob
      address:CompanyName     = def:User_Name
      address:AddressLine1    = def:Address_Line1
      address:AddressLine2    = def:Address_Line2
      address:AddressLine3    = def:Address_Line3
      address:Postcode        = def:Postcode
      address:TelephoneNumber = def:Telephone_Number
      address:FaxNumber       = def:Fax_Number
      address:EmailAddress    = def:EmailAddress
  Else !glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          address:CompanyName     = tra:Company_Name
          address:AddressLine1    = tra:Address_Line1
          address:AddressLine2    = tra:Address_Line2
          address:AddressLine3    = tra:Address_Line3
          address:Postcode        = tra:Postcode
          address:TelephoneNumber = tra:Telephone_Number
          address:FaxNumber       = tra:Fax_Number
          address:EmailAddress    = tra:EmailAddress
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  End !glo:WebJob
  
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Loan Phone Report'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END













OBFValidationReport PROCEDURE(func:RefNumber)
RejectRecord         LONG,AUTO
tmp:RefNumber        LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_aud_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
code_temp            BYTE
option_temp          BYTE
bar_code_string_temp CSTRING(21)
bar_code_temp        CSTRING(21)
qa_reason_temp       STRING(255)
engineer_temp        STRING(60)
tmp:POP              STRING(3)
tmp:OriginalPackaging STRING(3)
tmp:OriginalBattery  STRING(3)
tmp:OriginalCharger  STRING(3)
tmp:OriginalAntenna  STRING(3)
tmp:OriginalManuals  STRING(3)
tmp:PhysicalDamage   STRING(3)
tmp:FailureReason    STRING(255)
tmp:Replacement      STRING(3)
tmp:Failed           BYTE(0)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:DOP)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Ref_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(406,1042,7521,9781),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,9115),USE(?tmp:OriginalAntenna:Prompt:4)
                           STRING('Job Number:'),AT(1667,208),USE(?String2),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(4063,208),USE(job_ali:Ref_Number,,?JOB_ALI:Ref_Number:2),TRN,LEFT,FONT(,20,,FONT:bold)
                           STRING('Handset I.M.E.I.:'),AT(1302,781),USE(?String6),TRN,FONT(,18,,FONT:bold)
                           STRING('Box I.M.E.I.:'),AT(1302,1115),USE(?String6:2),TRN,FONT(,18,,FONT:bold)
                           STRING(@s20),AT(4063,1115),USE(jobe:BoxESN),TRN,LEFT,FONT(,18,,,CHARSET:ANSI)
                           STRING('Network:'),AT(1302,1448),USE(?String6:3),TRN,FONT(,18,,FONT:bold)
                           STRING(@s30),AT(4063,1448),USE(jobe:Network),TRN,LEFT,FONT(,18,,,CHARSET:ANSI)
                           STRING('Purchase Date:'),AT(1302,1781),USE(?String6:4),TRN,FONT(,18,,FONT:bold)
                           STRING(@d6),AT(4063,1781),USE(job_ali:DOP),TRN,LEFT,FONT(,18,,,CHARSET:ANSI)
                           STRING('Return Date:'),AT(1302,2115),USE(?String6:5),TRN,FONT(,18,,FONT:bold)
                           STRING(@s16),AT(4063,781),USE(job_ali:ESN),TRN,LEFT,FONT(,18,,,CHARSET:ANSI)
                           STRING('Talk Time:'),AT(1302,2448),USE(?String6:6),TRN,FONT(,18,,FONT:bold)
                           STRING(@d6),AT(4063,2115),USE(jobe:ReturnDate),TRN,LEFT,FONT(,18,,,CHARSET:ANSI)
                           STRING('Original Dealer:'),AT(1302,2781),USE(?String6:7),TRN,FONT(,18,,FONT:bold)
                           STRING(@s3),AT(4063,2448),USE(jobe:TalkTime),TRN,LEFT,FONT(,18,,,CHARSET:ANSI)
                           STRING('Mins'),AT(4531,2448),USE(?String6:15),TRN,FONT(,18,,)
                           STRING(@s254),AT(4063,2781),USE(jobe:OriginalDealer),LEFT,FONT(,18,,,CHARSET:ANSI)
                           STRING('Branch Of Return:'),AT(1302,3115),USE(?String6:8),TRN,FONT(,18,,FONT:bold)
                           STRING(@s30),AT(4063,3115),USE(jobe:BranchOfReturn),TRN,LEFT,FONT(,18,,,CHARSET:ANSI)
                           STRING(@s30),AT(4063,3448),USE(jof:StoreReferenceNumber),TRN,LEFT,FONT(,18,,,CHARSET:ANSI)
                           STRING('Store Ref No:'),AT(1302,3448),USE(?String6:11),TRN,FONT(,18,,FONT:bold)
                           STRING('Proof Of Purchase:'),AT(1302,3781),USE(?String6:9),TRN,FONT(,18,,FONT:bold)
                           STRING('Original Packaging:'),AT(1302,4115),USE(?String6:10),TRN,FONT(,18,,FONT:bold)
                           STRING(@s3),AT(4063,3781),USE(tmp:POP),LEFT,FONT(,18,,,CHARSET:ANSI)
                           STRING(@s3),AT(4063,4115),USE(tmp:OriginalPackaging),TRN,LEFT,FONT(,18,,,CHARSET:ANSI)
                           STRING(@s3),AT(4063,5115),USE(tmp:PhysicalDamage),FONT(,18,,)
                           STRING(@s3),AT(4063,5448),USE(tmp:Replacement),TRN,FONT(,18,,)
                           STRING(@s30),AT(4063,5781),USE(jof:LAccountNumber),TRN,FONT(,18,,)
                           STRING(@s30),AT(4063,6115),USE(jof:ReplacementIMEI),TRN,FONT(,18,,)
                           STRING('Replacement IMEI No:'),AT(1302,6115),USE(?String6:17),TRN,FONT(,18,,FONT:bold)
                           STRING('L/Account Number:'),AT(1302,5781),USE(?String6:16),TRN,FONT(,18,,FONT:bold)
                           STRING('Replacement:'),AT(1302,5448),USE(?String6:13),TRN,FONT(,18,,FONT:bold)
                           STRING('Failure Reason:'),AT(1302,6510),USE(?FailedReasonText),TRN,HIDE,FONT(,18,,FONT:bold)
                           TEXT,AT(4063,6510,3125,1146),USE(tmp:FailureReason),TRN,FONT(,18,,),RESIZE
                           STRING('Physical Damage:'),AT(1302,5115),USE(?String6:14),TRN,FONT(,18,,FONT:bold)
                           STRING(@s3),AT(4063,4781),USE(tmp:OriginalManuals),TRN,FONT(,18,,)
                           STRING(@s3),AT(4063,4448),USE(tmp:OriginalBattery),TRN,FONT(,18,,)
                           STRING('Original Accessories:'),AT(1302,4448),USE(?Third_Party_Agent),TRN,FONT(,18,,FONT:bold)
                           STRING('Original Manuals:'),AT(1302,4781),USE(?String6:12),TRN,FONT(,18,,FONT:bold)
                         END
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('OBF Validation Report'),AT(1208,52,5104,417),USE(?string20),TRN,CENTER,FONT(,24,,FONT:bold)
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('OBFValidationReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  ! Before Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  tmp:RefNumber   = func:RefNumber
  ! After Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  !Export: Call Customer Preview Options
  glo:ExportReport = 0
  PreviewReq = False
  glo:ReportName = 'OBFVal'
  If PrintOption(PreviewReq,glo:ExportReport,'OBF Validation Report') = False
      Do ProcedureReturn
  End ! If PrintOption(PreviewReq,glo:ExportReport) = False
  If ClarioNETServer:Active()
      ClarioNET:UseReportPreview(PreviewReq)
  End ! If ClarioNETServer:Active()
  BIND('tmp:RefNumber',tmp:RefNumber)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFPRINT.Open
  Access:AUDIT.UseFile
  Access:JOBSE.UseFile
  Access:JOBSOBF.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = tmp:RefNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job_ali:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found
            Access:JOBSOBF.ClearKey(jof:RefNumberKey)
            jof:RefNumber = job_ali:Ref_Number
            If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
                !Error
            End ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
        
            tmp:FailureReason = ''
            If tmp:Failed
                SetTarget(Report)
                ?FailedReasonText{prop:Hide} = 0
                SetTarget()
        
                Found# = 0
                Save_aud_ID = Access:AUDIT.SaveFile()
                Access:AUDIT.ClearKey(aud:Action_Key)
                aud:Ref_Number = job_ali:Ref_Number
                aud:Action     = 'O.B.F. VALIDATION FAILED'
                Set(aud:Action_Key,aud:Action_Key)
                Loop
                    If Access:AUDIT.NEXT()
                       Break
                    End !If
                    If aud:Ref_Number <> job_ali:Ref_Number      |
                    Or aud:Action     <> 'O.B.F. VALIDATION FAILED'      |
                        Then Break.  ! End If
                    tmp:FailureReason = Clip(aud:Notes)
                    Found# = 1
                    Break
                End !Loop
        
                If Found# = 0
                    Save_aud_ID = Access:AUDIT.SaveFile()
                    Access:AUDIT.ClearKey(aud:Action_Key)
                    aud:Ref_Number = job_ali:Ref_Number
                    aud:Action     = 'OBF REJECTED'
                    Set(aud:Action_Key,aud:Action_Key)
                    Loop
                        If Access:AUDIT.NEXT()
                           Break
                        End !If
                        If aud:Ref_Number <> job_ali:Ref_Number      |
                        Or aud:Action     <> 'OBF REJECTED'      |
                            Then Break.  ! End If
                        tmp:FailureReason = Clip(Sub(aud:Notes,9,255))
                        Found# = 1
                        Break
                    End !Loop
                End ! If FOund# = 0
                Access:AUDIT.RestoreFile(Save_aud_ID)
        
            End !If ~jobe:OBFValidated
            If jobe:ValidPOP = 'YES'
                tmp:POP = 'YES'
            Else !If jobe:ValidPOP
                tmp:POP = 'NO'
            End !If jobe:ValidPOP
        
            If jobe:OriginalPackaging
                tmp:OriginalPackaging = 'YES'
            Else !If jobe:OriginalPackaging
                tmp:OriginalPackaging = 'NO'
            End !If jobe:OriginalPackaging
        
            If jobe:OriginalBattery
                tmp:OriginalBattery = 'YES'
            Else !If jobe:OriginalBattery
                tmp:OriginalBattery = 'NO'
            End !If jobe:OriginalBattery
        
            If jobe:OriginalCharger
                tmp:OriginalCharger = 'YES'
            Else !If jobe:OriginalCharger
                tmp:OriginalCharger = 'NO'
            End !If jobe:OriginalCharger
        
            If jobe:OriginalAntenna
                tmp:OriginalAntenna = 'YES'
            Else !If jobe:OriginalAntenna
                tmp:OriginalAntenna = 'NO'
            End !If jobe:OriginalAntenna
        
            If jobe:OriginalManuals
                tmp:OriginalManuals = 'YES'
            Else !If jobe:OriginalManuals
                tmp:OriginalManuals = 'NO'
            End !If jobe:OriginalManuals
        
            If jobe:PhysicalDamage
                tmp:PhysicalDamage = 'YES'
            Else !If jobe:PhysicalDamage
                tmp:PhysicalDamage = 'NO'
            End !If jobe:PhysicalDamage
        
            If jof:Replacement
                tmp:Replacement = 'YES'
            Else ! If jof:Replacement
                tmp:Replacement = 'NO'
            End ! If jof:Replacement
        
        ! Deleting (DBH 14/03/2007) # 8718 - Not needed
        !    Access:ACCESSOR.ClearKey(acr:Accesory_Key)
        !    acr:Model_Number = job_ali:Model_Number
        !    acr:Accessory    = 'ANTENNA'
        !    If Access:ACCESSOR.TryFetch(acr:Accesory_Key) = Level:Benign
        !        !Found
        !
        !    Else!If Access:ACCESSOR.TryFetch(acr:Accesory_Key) = Level:Benign
        !        !Error
        !        !Assert(0,'<13,10>Fetch Error<13,10>')
        !        SetTarget(Report)
        !        ?OriginalAntenna{prop:Hide} = 1
        !        ?tmp:OriginalAntenna{prop:Hide} = 1
        !        SetTarget(Report)
        !    End!If Access:ACCESSOR.TryFetch(acr:Accesory_Key) = Level:Benign
        ! End (DBH 14/03/2007) #8718
        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(rpt:detail)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  !Export: Finish Off
  If glo:ExportReport
      Report{prop:TempNameFunc} = 0
  End ! If glo:ExportReport
  CLOSE(report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFPRINT.Close
  END
  !Export: Copy The Temp WMF
  If glo:ExportReport And ClarioNETServer:Active()
      Loop files# = 1 To Records(FileListQueue)
          Get(FileListQueue,files#)
          Loop char# = Len(flq:FileName) To 1 By -1
              If Sub(flq:FileName,char#,1) = '\'
                  Break
              End ! If Sub(flq:FileName,char#,1) = '\'
          End ! Loop char# = Len(flq:FileName) To 1 By -1
          Copy(flq:FileName,Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8))
          flq:FileName = Sub(flq:FileName,1,char#) & Clip(glo:ReportName) & ' (' & Format(Today(), @d12) & ' ' & Format(Clock(), @t2) & ') Page ' & Sub(flq:FileName,Len(Clip(flq:FileName))- 7,8)
          Put(FileListQueue)
      End ! Loop files# = 1 To Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active()
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  !Export: Send Files To Client
  If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
      Return" = ClarioNET:SendFilesToClient(1,0)
      !Delete files from temp folder
      Loop files# = 1 to Records(FileListQueue)
          Get(FileListQueue,files#)
          Remove(flq:FileName)
      End ! Loop files# = 1 to Records(FileListQueue)
  End ! If glo:ExportReport And ClarioNETServer:Active() And Records(FileListQueue)
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  tmp:Failed = 0
  Found# = 0
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job_ali:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
      If ~jobe:OBFValidated
          Found# = 0
  
          Save_aud_ID = Access:AUDIT.SaveFile()
          Access:AUDIT.ClearKey(aud:Action_Key)
          aud:Ref_Number = job_ali:Ref_Number
          aud:Action     = 'O.B.F. VALIDATION FAILED'
          Set(aud:Action_Key,aud:Action_Key)
          Loop
              If Access:AUDIT.NEXT()
                 Break
              End !If
              If aud:Ref_Number <> job_ali:Ref_Number      |
              Or aud:Action     <> 'O.B.F. VALIDATION FAILED'      |
                  Then Break.  ! End If
              Found# = 1
              Break
          End !Loop
  
          If Found# = 0
              Access:AUDIT.ClearKey(aud:Action_Key)
              aud:Ref_Number = job_ali:Ref_Number
              aud:Action     = 'OBF REJECTED'
              Set(aud:Action_Key,aud:Action_Key)
              Loop
                  If Access:AUDIT.NEXT()
                     Break
                  End !If
                  If aud:Ref_Number <> job_ali:Ref_Number      |
                  Or aud:Action     <> 'OBF REJECTED'      |
                      Then Break.  ! End If
                  Found# = 1
                  Break
              End !Loop
          End ! If Found# = 0
          Access:AUDIT.RestoreFile(Save_aud_ID)
          If Found# = 1
              tmp:Failed = 1
          End ! If Found# = 1
      Else
          Found# = 1
      End !If ~jobe:OBFValidated
  End
  If Found# = 0
      Case Missive('The selected job is not an O.B.F. job.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Do ProcedureReturn
  End !Found# = 0
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  !Export: Set Report Name
  If glo:ExportReport = 1
      PageNumber += 1
      Report{prop:TempNameFunc} = Address(PageNames)
  End ! If glo:ExportReport = 1
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='OBFValidationReport'
  END
  report{Prop:Preview} = PrintPreviewImage







CreditReportCriteria PROCEDURE                        !Generated from procedure template - Window

tmp:ExportFile       STRING(255),STATIC
tmp:StartDate        DATE
tmp:EndDate          DATE
save_jobsinv_id      USHORT,AUTO
TotalsGroup          GROUP,PRE(total)
HandingFee           REAL
ARCCharge            REAL
RRCLostLoan          REAL
RRCPartsCost         REAL
RRCPartsSelling      REAL
RRCLabour            REAL
ARCMarkup            REAL
RRCVAT               REAL
RRCTotal             REAL
Paid                 REAL
Outstanding          REAL
CNAmount             REAL
NewRRCTotal          REAL
Refund               REAL
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(364,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(296,258),USE(?Button3),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,162,192,94),USE(?Panel5),SCROLL,FILL(09A6A7CH)
                       PROMPT('Start Date'),AT(248,194),USE(?tmp:StartDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(320,194,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Start Date'),TIP('Start Date'),UPR
                       BUTTON,AT(388,190),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('End Date'),AT(248,214),USE(?tmp:EndDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(320,214,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('End Date'),TIP('End Date'),UPR
                       BUTTON,AT(388,210),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Credit Report Criteria'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('wizcncl.ico'),HIDE
     END
ExportFIle    File,Driver('ASCII'),Pre(expfil),Name(tmp:ExportFile),Create,Bindable,Thread
Record              Record
Line                String(1000)
                    End
                End
tmp:TempFilePath    CString(255)
tmp:DesktopFolder   CString(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    If glo:WebJob
        Clarionet:OpenPushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Open(Prog:ProgressWindow)
        ?Prog:Cancel{prop:Hide} = 0
    End ! If glo:WebJob

    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    If glo:WebJob
        Clarionet:UpdatePushWindow(Prog:ProgressWindow)
    End ! If glo:WebJob
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    If glo:WebJob
        Clarionet:ClosePushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Close(Prog:ProgressWindow)
    End ! If glo:WebJob

    Display()
Export          Routine
Data
local:NewFileName       CString(255)
Code
    tmp:ExportFile = 'CREDREP' & Today() & Clock() & '.TMP'
    If GetTempPathA(255,tmp:TempFilePath)
        If Sub(tmp:TempFilePath,-1,1) = '\'
            tmp:ExportFile = Clip(tmp:TempFilePath) & Clip(tmp:ExportFile)
        Else !If Sub(TempFilePath,-1,1) = '\'
            tmp:ExportFile = Clip(tmp:TempFilePath) & '\' & Clip(tmp:ExportFile)
        End !If Sub(tmp:TempFilePath,-1,1) = '\'
    End

    SHGetSpecialFolderPath( GetDesktopWindow(), tmp:DesktopFolder, 16, FALSE )
    tmp:DesktopFolder = tmp:DesktopFolder & '\ServiceBase Export'
    If ~Exists(tmp:DesktopFolder)
        If MkDir(tmp:DesktopFolder)
        End ! If ~MkDir(tmp:Desktop)
    End ! If ~Exists(tmp:Desktop)
    tmp:DesktopFolder = tmp:DesktopFolder & '\Credit Report'
    If ~Exists(tmp:DesktopFolder)
        If MkDir(tmp:DesktopFolder)
        End ! If ~MkDir(tmp:Desktop)
    End ! If ~Exists(tmp:Desktop)

    Remove(tmp:ExportFile)
    Create(ExportFile)
    Open(ExportFile)

    CountRecords# = 0

    If glo:WebJob
        Access:JOBSINV.ClearKey(jov:BookingDateTypeKey)
        jov:BookingAccount = Clarionet:Global.Param2
        jov:Type = 'C'
        jov:DateCreated = tmp:StartDate
        Set(jov:BookingDateTypeKey,jov:BookingDateTypeKey)
    Else ! If glo:WebJob
        Access:JOBSINV.ClearKey(jov:TypeDateKey)
        jov:Type = 'C'
        jov:DateCreated = tmp:StartDate
        Set(jov:TypeDateKey,jov:TypeDateKey)
    End ! If glo:WebJob
    Loop
        If Access:JOBSINV.Next()
            Break
        End ! If Access:JOBSINV.Next()
        If jov:Type <> 'C'
            Break
        End ! If job:Type <> 'C'
        If jov:DateCreated > tmp:EndDate
            Break
        End ! If jov:DateCreated > tmp:EndDate
        If glo:WebJob
            If jov:BookingAccount <> Clarionet:GLobal.Param2
                Break
            End ! If jov:BookingAccount <> Clarionet:GLobal.Param2
        End ! If glo:WebJob
        CountRecords# += 1
        If CountRecords# > 1000
            CountRecords# = Records(JOBSINV)
            Break
        End ! If CountRecords# > 1000
    End ! Loop

    expfil:Line = 'Job No,Branch ID,Franchise Job No,Original Invoice No,Original Invoice Date,' & |
                    'CN Number,CN Date,Passed By,New Inv No,New Inv Date,Head Account No,Head Account Name,' & |
                    'Sub Account No,Sub Account Name,Chargeable Charge Type,Completed Date,' & |
                    'Chargeable Repair Type,Repair,Handling/Exchange Fee,ARC Charge,RRC Lost Loan,' & |
                    'RRC Parts Cost,RRC Parts Sell,RRC Labour,ARC M/Up,RRC Vat,RRC Total,Paid,Outstanding,' & |
                    'CN Amount,New RRC Total,Refund'
    Add(ExportFile)

    Clear(TotalsGroup)

    Do Prog:ProgressSetup
    Prog:TotalRecords = CountRecords#
    Prog:ShowPercentage = 1 !Show Percentage Figure

    If glo:WebJob
        Access:JOBSINV.ClearKey(jov:BookingDateTypeKey)
        jov:BookingAccount = Clarionet:Global.Param2
        jov:Type = 'C'
        jov:DateCreated = tmp:StartDate
        Set(jov:BookingDateTypeKey,jov:BookingDateTypeKey)
    Else ! If glo:WebJob
        Access:JOBSINV.ClearKey(jov:TypeDateKey)
        jov:Type = 'C'
        jov:DateCreated = tmp:StartDate
        Set(jov:TypeDateKey,jov:TypeDateKey)
    End ! If glo:WebJob

    Accept
        Case Event()
        Of Event:Timer
            Loop 25 Times
                !Inside Loop
                If Access:JOBSINV.Next()
                    Prog:Exit = 1
                    Break
                End ! If Access:JOBSINV.Next()
                If jov:Type <> 'C'
                    Prog:Exit = 1
                    Break
                End ! If jov:Type <> 'C'
                If jov:DateCreated > tmp:EndDate
                    Prog:Exit = 1
                    Break
                End ! If jov:DateCreated > tmp:EndDate
                If glo:WebJob
                    If jov:BookingAccount <> Clarionet:Global.Param2
                        Prog:Exit = 1
                        Break
                    End ! If jov:BookingAccount <> Clarionet:Global.Param2
                End ! If glo:WebJob

                Do Exporting

                Do Prog:UpdateScreen

                Prog:RecordCount += 1
                ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords
            End ! Loop 25 Times
        Of Event:CloseWindow
            Prog:Exit = 1
            Prog:Cancelled = 1
            Break
        Of Event:Accepted
            If Field() = ?Prog:Cancel
                Beep(Beep:SystemQuestion)  ;  Yield()
                Case Message('Are you sure you want to cancel?','Cancel Pressed',icon:Question,'&Yes|&No',2,2)
                Of 1 ! Yes
                    Prog:Exit = 1
                    Prog:Cancelled = 1
                    Break
                Of 2 ! No
                End ! Case Message
            End! If FIeld()
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    expfil:Line  = 'Totals,,,,,,,,,,,,,,,,,,' & |
                    Format(total:HandingFee,@n~R~-_14.2) & ',' &|
                    Format(total:ARCCharge,@n~R~-_14.2) & ',' &|
                    Format(total:RRCLostLoan,@n~R~-_14.2) & ',' &|
                    Format(total:RRCPartsCost,@n~R~-_14.2) & ',' &|
                    Format(total:RRCPartsSelling,@n~R~-_14.2) & ',' &|
                    Format(total:RRCLabour,@n~R~-_14.2) & ',' &|
                    Format(total:ARCMarkup,@n~R~-_14.2) & ',' &|
                    Format(total:RRCVat,@n~R~-_14.2) & ',' &|
                    Format(total:RRCTotal,@n~R~-_14.2) & ',' &|
                    Format(total:Paid,@n~R~-_14.2) & ',' &|
                    Format(total:Outstanding,@n~R~-_14.2) & ',' &|
                    Format(total:CNAmount,@n~R~-_14.2) & ',' &|
                    Format(total:NewRRCTotal,@n~R~-_14.2) & ',' &|
                    Format(total:Refund,@n~R~-_14.2)
    Add(ExportFile)
    expfil:Line = 'Jobs Counted,' & Prog:RecordCount
    Add(ExportFile)

    Close(ExportFile)

    If glo:WebJob
        local:NewFileName = Clip(tmp:TempFilePath) & '\Credit Note Report ' & Format(Today(),@D012) & '.csv'
        Remove(local:newFileName)
        Rename(tmp:ExportFile,local:NewFileName)
        SendFileToClient(local:NewFileName)
        Remove(tmp:ExportFile)
        Case Missive('Export file created.','ServiceBase 3g',|
                       'midea.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    Else ! If glo:WebJob
        local:NewFileName = tmp:DesktopFolder & '\Credit Note Report ' & Format(Today(),@d012) & '.csv'
        Remove(local:NewFileName)
        Rename(tmp:ExportFile,local:NewFileName)

        Case Missive('Report Created:'&|
          '|' & local:NewFileName,'ServiceBase 3g',|
                       'midea.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    End ! If glo:WebJob

    Post(Event:CloseWindow)
Exporting       Routine
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = jov:RefNumber
    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        !Found
    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        !Error
    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign


    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

    Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
    inv:Invoice_Number = job:Invoice_Number
    If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        !Found
    Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        !Error
    End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign

    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job:Account_Number
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign


    Clear(expfil:Record)
    !Job No
    expfil:Line = '"' & job:Ref_Number
    !Branch ID
    expfil:Line = Clip(expfil:Line) & '","' & Clip(tra:BranchIdentification)
    !Franchise Job No
    expfil:Line = Clip(expfil:Line) & '","' & Clip(wob:JobNumber)
    !Original Invoice No
    expfil:Line= Clip(expfil:Line) & '","' & Clip(job:Invoice_Number)
    !Original Invoice Date
    expfil:Line = Clip(expfil:Line) & '","' & Format(inv:RRCInvoiceDate,@d06b)
    !CN Number
    expfil:Line = Clip(expfil:Line) & '","' & Clip('CN' & Clip(jov:InvoiceNumber) & jov:Suffix)
    !CN Date
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:DateCreated,@d06b)
    !Passed By
    expfil:Line = Clip(expfil:Line) & '","' & Clip(jov:UserCode)
    !New Inv No
    expfil:Line = Clip(expfil:Line) & '","' & Clip(jov:NewInvoiceNumber)
    !New Inv Date
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:DateCreated,@d06b)
    !Head Account No
    expfil:Line = Clip(expfil:Line) & '","' & Clip(wob:HeadAccountNumber)
    !Head Account Name
    expfil:Line = Clip(expfil:Line) & '","' & Clip(tra:Company_Name)
    !Sub Account No
    expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Account_Number)
    !Sub Account Name
    expfil:Line = Clip(expfil:Line) & '","' & Clip(sub:Company_Name)
    !Chargeable Charge Type
    expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Charge_Type)
    !Completed Date
    expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(job:Date_Completed,@d06b))
    !Chargeable Repair Type
    expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Repair_Type)
    !Repair
    If SentToHub(job:Ref_Number)
        expfil:Line = Clip(expfil:Line) & '","' & Clip('ARC')
    Else ! If SentToHub(job:Ref_Number
        expfil:Line = Clip(expfil:Line) & '","' & Clip('RRC')
    End ! If SentToHub(job:Ref_Number
    !Handling/Exchange Fee
    If jov:HandlingFee > 0
        expfil:Line = Clip(expfil:Line) & '","' & Format(jov:HandlingFee,@n~R~_14.2)
        total:HandingFee += jov:HandlingFee
    Else ! If jov:HandlingFree > 0
        expfil:Line = Clip(expfil:Line) & '","' & Format(jov:ExchangeRate,@n~R~_14.2)
        total:HandingFee += jov:ExchangeRate
    End ! If jov:HandlingFree > 0
    !ARC Charge
    If SentToHub(job:Ref_Number)
        expfil:Line = Clip(expfil:Line) & '","' & Format(jov:ARCCharge,@n~R~_14.2)
        total:ARCCharge += jov:ARCCharge
    Else ! If SentToHub(job:Ref_Number)
        expfil:Line = Clip(expfil:Line) & '","'
    End ! If SentToHub(job:Ref_Number)
    !RRC Lost Loan
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:RRCLostLoanCost,@n~R~_14.2)
    total:RRCLostLoan += jov:RRCLostLoanCost
    !RRC Parts Cost
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:RRCPartsCost,@n~R~_14.2)
    total:RRCPartsCost += jov:RRCPartsCost
    !RRC Parts Sell
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:RRCPartsSelling,@n~R~_14.2)
    total:RRCPartsSelling += jov:RRCPartsSelling
    !RRC Labour
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:RRCLabour,@n~R~_14.2)
    total:RRCLabour += jov:RRCLabour
    !ARC M/Up
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:ARCMarkup,@n~R~_14.2)
    total:ARCMarkup += jov:ARCMarkup
    !RRC VAT
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:RRCVAT,@n~R~_14.2)
    total:RRCVAT += jov:RRCVAT
    !RRC Total
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:RRCLabour + jov:RRCPartsSelling + jov:RRCLostLoanCost + jov:RRCVAT,@n~R~_14.2)
    total:RRCTotal += (jov:RRCLabour + jov:RRCPartsSelling + jov:RRCLostLoanCost + jov:RRCVAT)
    !Paid
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:Paid,@n~R~-_14.2)
    total:Paid += jov:Paid
    !Outstanding
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:Outstanding,@n~R~-_14.2)
    total:Outstanding += jov:Outstanding
    !CN Amount
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:CreditAmount,@n~R~-_14.2)
    total:CNAmount += jov:CreditAmount
    !New RRC Total
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:NewTotalCost,@n~R~-_14.2)
    total:NewRRCTotal += jov:NewTOtalCost
    !Refund
    expfil:Line = Clip(expfil:Line) & '","' & Format(jov:Refund,@n~R~-_14.2)
    total:Refund += jov:Refund
    expfil:Line = Clip(expfil:Line) & '"'
    Add(ExportFile)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020686'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CreditReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:INVOICE.Open
  Relate:WEBJOB.Open
  Access:JOBSINV.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:LOCATLOG.UseFile
  SELF.FilesOpened = True
  tmp:StartDate = Today()
  tmp:EndDate = Today()
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If glo:WebJob
      ?PopCalendar{prop:Hide} = 1
      ?PopCalendar:2{prop:Hide} = 1
  End ! If glo:WebJob
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      ! Inserting (DBH 14/02/2008) # 9752 - Check for valid dates
      If IsDateInValid(tmp:StartDate)
          Beep(Beep:SystemHand);  Yield()
         Case Missive('Invalid Start Date.','ServiceBase 3g',|
                        'mstop.jpg','/OK')
             Of 1 ! OK Button
         End ! Case Missive
         tmp:StartDate = ''
         Select(?tmp:StartDate)
         Cycle
      End ! If IsDateInValid(tmp:StartDate)
      If IsDateInValid(tmp:EndDate)
          Beep(Beep:SystemHand);  Yield()
         Case Missive('Invalid End Date.','ServiceBase 3g',|
                        'mstop.jpg','/OK')
             Of 1 ! OK Button
         End ! Case Missive
         tmp:EndDate = ''
         Select(?tmp:EndDate)
         Cycle
      End ! If IsDateInValid(tmp:StartDate)
      
      Beep(Beep:SystemQuestion);  Yield()
      Case Missive('Confirm Date Range:'&|
          '|    From:   ' & Format(tmp:StartDate,@d18) & |
          '|    To:        ' & Format(tmp:EndDate,@d18) & |
          '||Run Report?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
          Of 1 ! No Button
              Display()
              Cycle
      End ! Case Missive
      ! End (DBH 14/02/2008) #9752
      Do Export
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020686'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020686'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020686'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






VCPLoanAuditReport PROCEDURE(f:StockType,f:AuditNumber)
RejectRecord         LONG,AUTO
tmp:StockType        STRING(30)
tmp:AuditNumber      LONG
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
!-----------------------------------------------------------------------------
Process:View         VIEW(AUDVCPLN)
                       PROJECT(avcp:Audit_Number)
                       PROJECT(avcp:Audit_Option)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Report               REPORT('Loan Audit Report'),AT(25,143,481,508),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                       HEADER,AT(25,31,481,113),USE(?unnamed)
                         STRING('AUDIT REPORT'),AT(396,4),USE(?String8),TRN,FONT(,11,,FONT:bold)
                         STRING(@s30),AT(8,17),USE(sub:Address_Line1)
                         STRING(@s30),AT(8,26),USE(sub:Address_Line2)
                         STRING('Audit No:'),AT(360,30),USE(?String26),TRN,FONT(,,,FONT:bold)
                         STRING(@s8),AT(412,30),USE(tmp:AuditNumber),TRN
                         STRING(@s30),AT(8,34),USE(sub:Address_Line3)
                         STRING(@s15),AT(8,43),USE(sub:Postcode),TRN,LEFT
                         STRING('Date:'),AT(360,47),USE(?String26:2),TRN,FONT(,,,FONT:bold)
                         STRING(@d6),AT(412,47),USE(ReportRunDate),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(8,56),USE(?String14),TRN
                         STRING(@s15),AT(36,56),USE(sub:Telephone_Number),TRN,LEFT
                         STRING('Email:'),AT(8,73),USE(?String18),TRN
                         STRING(@s255),AT(36,73),USE(sub:EmailAddress),LEFT
                         STRING('IMEI Number'),AT(8,95),USE(?String20),TRN,FONT(,,,FONT:bold)
                         STRING('MSN'),AT(92,95),USE(?String21),TRN,FONT(,,,FONT:bold)
                         STRING('Make'),AT(172,95),USE(?String22),TRN,FONT(,,,FONT:bold)
                         STRING('Model'),AT(276,95),USE(?String22:2),TRN,FONT(,,,FONT:bold)
                         STRING('Status'),AT(380,95),USE(?String22:3),TRN,FONT(,,,FONT:bold)
                         LINE,AT(8,108,464,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Fax:'),AT(8,65),USE(?String15),TRN
                         STRING(@s15),AT(36,65),USE(sub:Fax_Number),TRN,LEFT
                         STRING(@t1),AT(412,56),USE(ReportRunTime),TRN
                         STRING(@s30),AT(8,4),USE(sub:Company_Name),TRN,LEFT,FONT(,12,,FONT:bold)
                       END
DETAIL                 DETAIL,AT(,,,13),USE(?DetailBand)
                         STRING(@s18),AT(8,0,80,9),USE(loa:ESN)
                         STRING(@s18),AT(92,0),USE(loa:MSN)
                         STRING(@s30),AT(172,0,96,9),USE(loa:Manufacturer)
                         STRING(@s30),AT(276,0,88,9),USE(loa:Model_Number)
                         STRING(@s15),AT(380,0),USE(avcp:Audit_Option),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                       END
                       FOOTER,AT(25,652,481,14)
                         STRING('Page:'),AT(4,4),USE(?PagePrompt)
                         STRING(@N4),AT(24,4),PAGENO,USE(?ReportPageNo),LEFT
                         LINE,AT(8,0,464,0),USE(?Line2),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('VCPLoanAuditReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('tmp:AuditNumber',tmp:AuditNumber)
  BIND('tmp:StockType',tmp:StockType)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  ! Before Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  tmp:AuditNumber = f:AuditNumber
  tmp:StockType = f:StockType
  
  ! After Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  FileOpensReached = True
  FilesOpened = True
  Relate:AUDVCPLN.Open
  Relate:LOAN.Open
  Relate:SUBTRACC.Open
  ! Before Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  Access:SUBTRACC.Clearkey(sub:Company_Name_Key)
  sub:Company_Name = tmp:StockType
  If Access:SUBTRACC.TryFetch(sub:Company_Name_Key) = Level:Benign
  
  End ! If Access:SUBTRACC.TryFetch(sub:Company_Name_Key) = Level:Benign
  ! After Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  
  
  RecordsToProcess = RECORDS(AUDVCPLN)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(AUDVCPLN,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 75
      SET(avcp:StockType_Audit_Key)
      Process:View{Prop:Filter} = |
      'UPPER(avcp:Stock_Type) = UPPER(tmp:StockType) AND (avcp:Audit_Number =' & |
      ' tmp:AuditNumber)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number = avcp:Ref_Number
        If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
        Else ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
            loa:ESN = 'Unknown Loan Unit'
            loa:MSN = 'Loan Ref No: ' & avcp:Ref_Number
            loa:Manufacturer = ''
            loa:Model_Number = ''
        
        End ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(RPT:DETAIL)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(AUDVCPLN,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDVCPLN.Close
    Relate:LOAN.Close
    Relate:SUBTRACC.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'AUDVCPLN')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='VCPLoanAuditReport'
  END
  Report{Prop:Preview} = PrintPreviewImage













QAHandover PROCEDURE
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:UserName         STRING(60)
MultiColQueueNdx     SHORT
MultiColQueueRowNbr  SHORT
MultiColQueueColNbr  SHORT
MultiColQueueCurrRows SHORT
tmp:CurrentType      STRING(30)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:ESN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
report               REPORT,AT(406,1344,7521,7583),PAPER(PAPER:A4),PRE(rpt),FONT('Tahoma',8,,FONT:regular),THOUS
                       HEADER,AT(406,1000,7521,323),USE(?unnamed:2)
                         STRING('Job Handover'),AT(104,52),USE(?String1),TRN,FONT(,10,,)
                         STRING(@d06),AT(6510,52),USE(ReportRunDate),TRN,FONT(,10,,)
                       END
Title                  DETAIL,PAGEBEFORE(-1),AT(,,,1479),USE(?Title)
                         STRING('Report Title'),AT(2146,0,3229,156),USE(?Text:ReportTitle),TRN,CENTER,FONT(,10,,FONT:bold+FONT:underline)
                         STRING('From: ARC'),AT(260,313),USE(?String4),TRN,FONT(,10,,)
                         STRING('Person:'),AT(260,625),USE(?String5),TRN,FONT(,10,,)
                         STRING(@s60),AT(833,625),USE(tmp:UserName),TRN,FONT(,10,,FONT:bold)
                         STRING('Job Number'),AT(156,1250),USE(?String7),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Manufacturer'),AT(938,1250),USE(?String8),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Model Number'),AT(2292,1250),USE(?String9),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('I.M.E.I. Number'),AT(3646,1250),USE(?String10),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Status'),AT(4948,1250),USE(?String11),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Order Number'),AT(5833,1250),USE(?String12),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(271,1406,6979,0),USE(?Line1),COLOR(COLOR:Black)
                       END
detail                 DETAIL,AT(,,,281),USE(?detailband)
                         STRING(@s8),AT(208,52),USE(job:Ref_Number),TRN
                         STRING(@s20),AT(885,52),USE(job:Manufacturer),TRN
                         STRING(@s20),AT(2292,52),USE(job:Model_Number),TRN
                         STRING(@s18),AT(3646,52),USE(job:ESN),TRN
                         STRING('Unavailable'),AT(4948,52),USE(?String17),TRN
                         STRING(@s30),AT(5833,52),USE(job:Order_Number),TRN,HIDE
                       END
                       FOOTER,AT(406,9000,7521,1479),USE(?unnamed)
                         LINE,AT(260,52,6979,0),USE(?Line5),COLOR(COLOR:Black)
                         STRING('Received By:'),AT(260,156),USE(?String18),TRN,FONT(,10,,)
                         STRING('Date Received:'),AT(4531,156),USE(?String18:2),TRN,FONT(,10,,)
                         STRING('Signature'),AT(260,885),USE(?String18:3),TRN,FONT(,10,,)
                         LINE,AT(260,1354,2708,0),USE(?Line2:3),COLOR(COLOR:Black)
                         LINE,AT(260,625,2708,0),USE(?Line2),COLOR(COLOR:Black)
                         LINE,AT(4531,625,2708,0),USE(?Line2:2),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('QAHandover')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Access:USERS.UseFile
  
  
  RecordsToProcess = BYTES(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        ! Resuing the Invoice Queue to save dictionary changes (DBH: 20/03/2009) #10473
        Sort(glo:Q_Invoice,glo:Account_Number,glo:Invoice_Number)
        
        tmp:CurrentType = ''
        loop x# = 1 to Records(glo:Q_Invoice)
            get(glo:Q_Invoice,x#)
            if (tmp:CurrentType <> glo:Account_Number)
                SetTarget(Report)
                case glo:Account_Number
                of '48H'
                    ?Text:ReportTitle{prop:Text} = '48HOUR EXCHANGE'
                of 'SCR'
                    ?Text:ReportTitle{prop:Text} = 'SCRAPPING'
                of 'RTM'
                    ?Text:ReportTitle{prop:Text} = 'RTM'
                of 'REF'
                    ?Text:ReportTitle{prop:Text} = 'REFURB'
                of 'PHA'
                    ?Text:ReportTitle{prop:Text} = 'PHANTOM'
                end ! case glo:Account_Number
                SetTarget()
                Print(rpt:Title)
                tmp:CurrentType = glo:Account_Number
            end !if (tmp:CurrentType = '')
        
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number    = glo:Invoice_Number
            if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                ! Found
                SetTarget(Report)
                if (glo:Account_Number = 'PHA')
                    ?job:Order_Number{prop:Hide} = 0
                else ! if (glo:Account_Number = 'PHA')
                    ?job:Order_Number{prop:Hide} = 1
                end !if (glo:Account_Number = 'PHA')
                SetTarget()
                Print(rpt:Detail)
            else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        end ! loop x# = 1 to Records(glo:Q_Invoice)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:JOBS.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
      ! Found
      tmp:Username = clip(use:Forename) & ' ' & clip(use:Surname)
  else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
      ! Error
  end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Handover'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END







