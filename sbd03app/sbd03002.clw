

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03002.INC'),ONCE        !Local module procedure declarations
                     END








Stock_Value_Summary_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_sto_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
stock_queue          QUEUE,PRE(stoque)
site_location        STRING(30)
manufacturer         STRING(30)
total_stock          REAL
purchase_value       REAL
AveragePurchaseCost  REAL
sale_value           REAL
                     END
site_location_temp   STRING(30)
count_temp           STRING(9)
average_price_temp   REAL
count_average_temp   REAL
job_count_temp       REAL
total_jobs_temp      REAL
Manufacturer_Temp    STRING(30)
total_stock_temp     STRING(12)
sale_value_temp      REAL
purchase_value_temp  REAL
purchase_value_total_temp REAL
sale_value_total_temp REAL
total_stock_total_temp STRING(12)
location_temp        STRING(30)
tmp:TotalAveragePurchaseCost REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(STOCK)
                     END
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 27
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

Report               REPORT('Stock Value Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,781,7521,1438),USE(?unnamed)
                         STRING('Inc. Accessories:'),AT(4948,156),USE(?String22),TRN,FONT(,8,,)
                         STRING(@s40),AT(5885,156),USE(GLO:Select3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(5885,365),USE(GLO:Select4),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Suppress Zeros:'),AT(4948,365),USE(?String22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(4948,573),USE(?String27),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(4948,990),USE(?String59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,990),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6146,990),USE(?String34),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6302,990,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(5885,573),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(4948,781),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5885,781),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,229),USE(?DetailBand)
                           STRING(@s30),AT(156,0),USE(site_location_temp),TRN,FONT(,8,,)
                           STRING(@s30),AT(2083,0),USE(Manufacturer_Temp),TRN,FONT(,8,,)
                           STRING(@s8),AT(4115,0),USE(stoque:total_stock),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(4740,0),USE(stoque:purchase_value),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6510,0),USE(stoque:sale_value),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(5625,0),USE(stoque:AveragePurchaseCost),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(,,,344),USE(?totals)
                           LINE,AT(208,52,7083,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(208,104),USE(?String26),TRN,FONT(,8,,FONT:bold)
                           STRING(@s9),AT(4052,104),USE(total_stock_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n-14.2),AT(4760,104),USE(purchase_value_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n-14.2),AT(6531,104),USE(sale_value_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n14.2),AT(5625,104),USE(tmp:TotalAveragePurchaseCost),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@s9),AT(2083,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('STOCK VALUE REPORT'),AT(4271,0,3177,260),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('Manufacturer'),AT(2083,2083),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('Average Purchase'),AT(5781,2031),USE(?String45:4),TRN,FONT(,7,,FONT:bold)
                         STRING('Site Location'),AT(156,2083),USE(?String44:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Stock Items'),AT(4104,2125),USE(?String45),TRN,FONT(,7,,FONT:bold)
                         STRING('In Warranty'),AT(5000,2031),USE(?String45:3),TRN,FONT(,7,,FONT:bold)
                         STRING('Value'),AT(5156,2135),USE(?String24),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Cost'),AT(6094,2135),USE(?String24:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Out Warranty'),AT(6719,2031),USE(?String45:5),TRN,FONT(,7,,FONT:bold)
                         STRING('Value'),AT(6906,2135),USE(?String25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Total'),AT(4250,2021),USE(?String45:2),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Stock_Value_Summary_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
      ! Save Window Name
   AddToLog('Report','Open','Stock_Value_Summary_Report')
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:STOCK.Open
  Relate:DEFAULTS.Open
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(STOCK)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
   OMIT('(0)',ClarioNETUsed)                                                      !--- ClarioNET 75B
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(STOCK,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ClarioNET:StartReport                                                      !--- ClarioNET 75A
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        RecordsToProcess    = Records(glo:Queue)
        
        setcursor(cursor:wait)
        save_sto_id = access:stock.savefile()
        access:stock.clearkey(sto:location_part_description_key)
        set(sto:location_part_description_key,sto:location_part_description_key)
        loop
            if access:stock.next()
               break
            end !if
            Yield()
            print# = 1
            Sort(glo:Queue,glo:pointer)
            glo:pointer = sto:location
            Get(glo:Queue,glo:pointer)
            If error()
                print# = 0
            End!If ~error()
            If glo:select3 <> 'YES'
                If sto:accessory = 'YES'
                    print# = 0
                End!If sto:accessory = 'YES'
            End!If glo:select2 <> 'YES'
        
            If glo:select4 = 'YES' and sto:quantity_stock = 0
                print# = 0
            End!If glo:select4 = 'YES' and sto:quantity_stock = 0
        
            If print# = 1
                RecordsProcessed += 1
                Do DisplayProgress
                Sort(stock_queue,stoque:site_location,stoque:manufacturer)
                stoque:site_location    = sto:location
                stoque:manufacturer     = sto:manufacturer
                Get(stock_queue,stoque:site_location,stoque:manufacturer)
                If Error()
                    stoque:site_location    = sto:location
                    stoque:manufacturer     = sto:manufacturer
                    stoque:total_stock      = sto:quantity_stock
                    stoque:purchase_value   = Round(sto:purchase_cost * sto:quantity_stock,.01)
                    !Add Average Purchase Cost - TrkBs: 3277 (DBH: 16-02-2005)
                    stoque:AveragePurchaseCost = Round(sto:AveragePurchaseCOst * sto:Quantity_Stock,.01)
                    stoque:sale_value       = Round(sto:sale_cost * sto:quantity_stock,.01)
                    Add(stock_queue)
                Else!If Error()
                    stoque:total_stock      += sto:quantity_stock
                    stoque:purchase_value   += Round(sto:purchase_cost * sto:quantity_stock,.01)
                    stoque:AveragePurchaseCost += Round(sto:AveragePurchaseCOst * sto:Quantity_Stock,.01)
                    stoque:sale_value       += Round(sto:sale_cost * sto:quantity_stock,.01)
                    Put(stock_queue)
                End!If Error()
            End!If print# = 1
        
        end !loop
        access:stock.restorefile(save_sto_id)
        setcursor()
        
        Prog.ProgressSetup(Records(Stock_Queue))
        
        
        If Records(stock_queue)
            count_temp = 0
        
            Sort(stock_queue,stoque:site_location,stoque:manufacturer)
            Loop x# = 1 To Records(stock_queue)
                If Prog.InsideLoop()
        
                End ! IF Prog.InsideLoop()
                Get(stock_queue,x#)
                If count_temp = 0
                    location" = stoque:site_location
                    site_location_temp = stoque:site_location
                Else!If count_temp = 0
                    If location" <> stoque:site_location
                        site_location_temp = stoque:site_location
                        location" = stoque:site_location
                    Else!If location" <> stoque:site_locatoin
                        site_location_temp = ''
                    End!If location" <> stoque:site_locatoin
                End!If count_temp = 0
        !        site_location_temp = stoque:site_location
                manufacturer_temp   = stoque:manufacturer
                count_temp += 1
                tmp:RecordsCount += 1
                Print(rpt:detail)
                total_stock_total_temp      += stoque:total_stock
                purchase_value_total_temp   += stoque:purchase_value
                sale_value_total_temp       += stoque:sale_value
                tmp:TotalAveragePurchaseCost += stoque:AveragePurchaseCost
            End!Loop x# = 1 To Records(stock_queue)
            Print(rpt:totals)
        End!If Records(stock_queue)
        
        Prog.ProgressFinish()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(STOCK,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ClarioNETServer:Active()                   !---ClarioNET 51
          ClarioNET:PutIni('ClarioNET','CPCSDesiredInitZoom'   ,100,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSIniFileToUse'      ,CLIP(INIFileToUse), '.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewOptions'    ,PreviewOptions,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSWmf2AsciiName'     ,Wmf2AsciiName,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSAsciiLineOption'   ,AsciiLineOption,'.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpFile'   ,'','.\CPCSCNET.INI')
          ClarioNET:PutIni('ClarioNET','CPCSPreviewHelpContext','','.\CPCSCNET.INI')
          BackupGlobalResponse# = GlobalResponse
          LocalResponse = RequestCancelled
          GlobalResponse = RequestCancelled
          LOOP TempNameFunc_Flag = 1 TO RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, TempNameFunc_Flag)
            ClarioNET:AddMetafileName(PrintPreviewQueue)
          END
          ClarioNET:SetReportProperties(Report)
          TempNameFunc_Flag = 0
        ELSE
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        END                                           !---ClarioNET 53
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 59
    GlobalResponse = BackupGlobalResponse#
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
      ! Save Window Name
   AddToLog('Report','Close','Stock_Value_Summary_Report')
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:STOCK.Close
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 64
    ClarioNET:EndReport                               !---ClarioNET 66
  END                                                 !---ClarioNET 67
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF ClarioNETServer:Active()                         !---ClarioNET 85
    IF TempNameFunc_Flag = 0
      TempNameFunc_Flag = 1
      Report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
    END
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Stock_Value_Summary_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage


Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: UnivAbcReport
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0





Stock_Value_Criteria PROCEDURE                        !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::3:TAGDISPSTATUS    BYTE(0)
DASBRW::3:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
FilesOpened          BYTE
save_loc_id          USHORT,AUTO
tag_temp             STRING(20)
tmp:ExportPath       STRING(255)
tmp:ReportOutput     BYTE(0)
tmp:ExportFileName   STRING(255),STATIC
stock_queue          QUEUE,PRE(stoque)
site_location        STRING(30)
manufacturer         STRING(30)
total_stock          REAL
purchase_value       REAL
AveragePurchaseCost  REAL
sale_value           REAL
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW2::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Stock Value Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('Select Site Location(s)'),AT(168,86),USE(?Prompt1),FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel6),FILL(09A6A7CH)
                       LIST,AT(256,84,168,194),USE(?List),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),VCR,FORMAT('11L(2)I@s1@120L(2)|M*~Location~@s30@'),FROM(Queue:Browse)
                       BUTTON,AT(432,168),USE(?DASTAG),TRN,FLAT,LEFT,MSG('Tag'),KEY(AltT),ICON('tagitemp.jpg')
                       BUTTON('sho&W tags'),AT(432,110,70,13),USE(?DASSHOWTAG),HIDE
                       BUTTON('&Rev tags'),AT(444,136,50,13),USE(?DASREVTAG),HIDE
                       BUTTON,AT(432,200),USE(?DASTAGAll),TRN,FLAT,LEFT,MSG('Tag All'),KEY(AltA),ICON('tagallp.jpg')
                       BUTTON,AT(432,232),USE(?DASUNTAGALL),TRN,FLAT,LEFT,MSG('Untag All'),KEY(AltU),ICON('untagalp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Stock Value Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                       CHECK('Include Accessories In Report'),AT(168,294),USE(GLO:Select3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                       OPTION('Report Output'),AT(300,284,208,42),USE(tmp:ReportOutput),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Report Output'),TIP('Report Output')
                         RADIO('Paper Report'),AT(308,294),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                         RADIO('Export'),AT(380,294),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                       END
                       PROMPT('Export Path'),AT(308,308),USE(?tmp:ExportPath:Prompt),HIDE,FONT(,8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s255),AT(360,308,112,10),USE(tmp:ExportPath),HIDE,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Export Path'),TIP('Export Path'),UPR
                       BUTTON,AT(476,304),USE(?LookupExportPath),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                       CHECK('Suppress Zeros'),AT(168,312),USE(GLO:Select4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
FileLookup10         SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ExportFile    File,Driver('ASCII'),Pre(expfil),Name(tmp:ExportFileName),Create,Bindable,Thread
Record                  Record
Line                    String(1000)
                        End
                    End

TempFilePath         CSTRING(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::3:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW2.UpdateBuffer
   GLO:Queue.Pointer = loc:Location
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = loc:Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW2.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = loc:Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW2.Reset
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::3:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::3:QUEUE = GLO:Queue
    ADD(DASBRW::3:QUEUE)
  END
  FREE(GLO:Queue)
  BRW2.Reset
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::3:QUEUE.Pointer = loc:Location
     GET(DASBRW::3:QUEUE,DASBRW::3:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = loc:Location
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASSHOWTAG Routine
   CASE DASBRW::3:TAGDISPSTATUS
   OF 0
      DASBRW::3:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::3:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::3:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW2.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
SummaryExport       Routine
Data
local:SiteLocation      String(30)
local:TotalLines        Long
local:TotalTotalStockItems  Long
local:TotalPurchaseValue    Real
local:TotalAveragePurchaseCost  Real
local:TotalSaleValue        Real
local:FileName          String(255)
local:LocationCount     Long()
Code
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            tmp:ExportFileName = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            tmp:ExportFileName = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
        tmp:ExportFileName = Clip(tmp:ExportFileName) & 'STOCKVAL' & Today() & Clock() & '.TMP'

        Remove(tmp:ExportFileName)

        Create(ExportFile)
        Open(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"STOCK VALUE REPORT (SUMMARY)"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Include Accessories","' & Clip(glo:Select3) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Suppress Zeros","' & Clip(glo:Select4) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Printed By","' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"Date Printed","' & Format(Today(),@d6) & '"'
        Add(ExportFile)

        Clear(expfil:Record)
        expfil:Line = '"' & Clip('Site Location')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Manufacturer')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Total Stock Items')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('In Warranty Value')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Average Purchase Cost')
        expfil:Line = Clip(expfil:Line) & '","' & Clip('Out Warranty Value') & '"'
        Add(ExportFile)

        local:LocationCount = 0
        Save_loc_ID = Access:LOCATION.SaveFile()
        Access:LOCATION.ClearKey(loc:Location_Key)
        Set(loc:Location_Key,loc:Location_Key)
        Loop
            If Access:LOCATION.NEXT()
               Break
            End !If
            local:LocationCount += 1
        End !Loop
        Access:LOCATION.RestoreFile(Save_loc_ID)

        Prog.ProgressSetup(Records(STOCK) / (Records(glo:Queue)/local:LocationCount))

        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            Access:STOCK.ClearKey(sto:Location_Key)
            sto:Location    = glo:Pointer
            Set(sto:Location_Key,sto:Location_Key)
            Loop
                If Access:STOCK.NEXT()
                   Break
                End !If
                If sto:Location    <> glo:Pointer      |
                    Then Break.  ! End If
                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop()
                !Include accessories? - 3277  (DBH: 17-06-2004)
                If glo:Select3 <> 'YES'
                    If sto:Accessory = 'YES'
                        Cycle
                    End !If sto:Accessory = 'YES'
                End !If glo:Select3 <> 'YES'

                !Include zero value parts? - 3277  (DBH: 17-06-2004)
                If glo:Select4 = 'YES' and sto:Quantity_Stock = 0
                    Cycle
                End !If glo:Select4 = 'YES' and sto:Quantity_Stock = 0

                Sort(stock_queue,stoque:site_location,stoque:manufacturer)
                stoque:site_location    = sto:location
                stoque:manufacturer     = sto:manufacturer
                Get(stock_queue,stoque:site_location,stoque:manufacturer)
                If Error()
                    stoque:site_location    = sto:location
                    stoque:manufacturer     = sto:manufacturer
                    stoque:total_stock      = sto:quantity_stock
                    stoque:purchase_value   = Round(sto:purchase_cost * sto:quantity_stock,.01)
                    stoque:AveragePurchaseCost = Round(sto:AveragePurchaseCost * sto:Quantity_Stock,.01)
                    stoque:sale_value       = Round(sto:sale_cost * sto:quantity_stock,.01)
                    Add(stock_queue)
                Else!If Error()
                    stoque:total_stock      += sto:quantity_stock
                    stoque:purchase_value   += Round(sto:purchase_cost * sto:quantity_stock,.01)
                    stoque:AveragePurchaseCost += Round(sto:AveragePurchaseCost * sto:Quantity_Stock,.01)
                    stoque:sale_value       += Round(sto:sale_cost * sto:quantity_stock,.01)
                    Put(stock_queue)
                End!If Error()
            End !Loop

        End ! Loop x# = 1 To Records(glo:Queue)


        Prog.ProgressFinish()
        Prog.ProgressSetup(Records(Stock_Queue))
        If Records(stock_queue)
            count# = 0

            Sort(stock_queue,stoque:site_location,stoque:manufacturer)
            Loop x# = 1 To Records(stock_queue)
                Get(stock_queue,x#)

                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop()

                If count# = 0
                    location" = stoque:site_location
                    local:SiteLocation = stoque:site_location
                Else!If count_temp = 0
                    If location" <> stoque:site_location
                        local:SiteLocation = stoque:site_location
                        location" = stoque:site_location
                    Else!If location" <> stoque:site_locatoin
                        local:SiteLocation = ''
                    End!If location" <> stoque:site_locatoin
                End!If count_temp = 0

                count# += 1

                Clear(expfil:Record)
                expfil:Line = '"' & Clip(local:SiteLocation)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(stoque:Manufacturer)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(stoque:Total_Stock)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(stoque:Purchase_Value,@n_14.2))
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(stoque:AveragePurchaseCost,@n_14.2))
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(stoque:Sale_Value,@n_14.2)) & '"'
                Add(ExportFile)

                local:TotalLines += 1
                local:TotalTotalStockItems  += stoque:Total_Stock
                local:TotalPurchaseValue    += stoque:Purchase_Value
                local:TotalAveragePurchaseCost += stoque:AveragePurchaseCost
                local:TotalSaleValue        += stoque:Sale_Value
            End!Loop x# = 1 To Records(stock_queue)

            Clear(expfil:Record)
            expfil:Line = '"' & Clip('Total Number Of Lines')
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:TotalLines)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:TotalTotalStockItems)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(local:TotalPurchaseValue,@n_14.2))
            expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(local:TotalAveragePurchaseCost,@n_14.2))
            expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(local:TotalSaleValue,@n_14.2)) & '"'
            Add(ExportFile)
        End!If Records(stock_queue)

        Prog.ProgressFinish()
        Close(ExportFile)

        local:FileName = 'STOCK VALUE REPORT (SUMMARY) ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'

        !Make sure the is only one trailing backslash - TrkBs: 3277 (DBH: 21-02-2005)
        If Sub(tmp:ExportPath,-1,1) = '\'
        Else !If Sub(TempFilePath,-1,1) = '\'
            tmp:ExportPath = Clip(tmp:ExportPath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'

        If glo:WebJob
            Remove(Clip(TempFilePath) & CLip(local:FileName))
            Rename(ExportFile,Clip(TempFilePath) & Clip(local:FileName))
            SendFileToClient(Clip(TempFilePath) & Clip(local:FileName))
        Else !If glo:WebJob
            Remove(Clip(tmp:ExportPath) & Clip(local:FileName))
            Copy(ExportFile,Clip(tmp:ExportPath) & Clip(local:FileName))
        End !If glo:WebJob
        !Remove the temp file - 3277  (DBH: 18-06-2004)
        Remove(Clip(TempFilePath) & Clip(local:FileName))

        Case Missive('Export Completed.','ServiceBase 3g',|
                       'midea.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    End


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020183'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Stock_Value_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:LOCATION,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  glo:select3 = 'NO'
  glo:select4 = 'NO'
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  tmp:ExportPath = def:ExportPath
      ! Save Window Name
   AddToLog('Window','Open','Stock_Value_Criteria')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW2.Q &= Queue:Browse
  BRW2.RetainRow = 0
  BRW2.AddSortOrder(,loc:Location_Key)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,loc:Location,1,BRW2)
  BIND('tag_temp',tag_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW2.AddField(tag_temp,BRW2.Q.tag_temp)
  BRW2.AddField(loc:Location,BRW2.Q.loc:Location)
  BRW2.AddField(loc:RecordNumber,BRW2.Q.loc:RecordNumber)
  FileLookup10.Init
  FileLookup10.Flags=BOR(FileLookup10.Flags,FILE:LongName)
  FileLookup10.Flags=BOR(FileLookup10.Flags,FILE:Directory)
  FileLookup10.SetMask('All Files','*.*')
  BRW2.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW2.AskProcedure = 0
      CLEAR(BRW2.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  glo:select3 = ''
  glo:select4 = ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Stock_Value_Criteria')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020183'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020183'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020183'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If ~Records(glo:Queue)
          Case Missive('You must select at least ONE Site Location.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If ~Records(glo:Queue)
          !Add option to export - TrkBs: 3277 (DBH: 16-02-2005)
          If tmp:ReportOutput = 1
              Do SummaryExport
          Else ! If tmp:ReportOutput = 1
              Stock_Value_Summary_Report
          End ! If tmp:ReportOutput = 1
          !End   - Add option to export - TrkBs: 3277 (DBH: 16-02-2005)
      
      End!If ~Records(glo:Queue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?tmp:ReportOutput
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ReportOutput, Accepted)
      If tmp:ReportOutput = 0
          ?tmp:ExportPath{prop:Hide} = True
          ?tmp:ExportPath:Prompt{prop:Hide} = True
          ?LookupExportPath{prop:Hide} = True
      Else ! tmp:ReportOutput = 0
          ?tmp:ExportPath{prop:Hide} = False
          ?tmp:ExportPath:Prompt{prop:Hide} = False
          ?LookupExportPath{prop:Hide} = False
      End ! tmp:ReportOutput = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ReportOutput, Accepted)
    OF ?LookupExportPath
      ThisWindow.Update
      tmp:ExportPath = Upper(FileLookup10.Ask(1)  )
      DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If KeyCode() = MouseLeft2
          Post(EVENT:Accepted,?DasTag)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::3:DASuntagall
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW2.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = loc:Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END
  IF (loc:Main_Store = 'YES')
    SELF.Q.loc:Location_NormalFG = 255
    SELF.Q.loc:Location_NormalBG = 16777215
    SELF.Q.loc:Location_SelectedFG = 16777215
    SELF.Q.loc:Location_SelectedBG = 255
  ELSIF (loc:VirtualSite = 1)
    SELF.Q.loc:Location_NormalFG = 32768
    SELF.Q.loc:Location_NormalBG = 16777215
    SELF.Q.loc:Location_SelectedFG = 16777215
    SELF.Q.loc:Location_SelectedBG = 32768
  ELSIF (loc:Active = 0)
    SELF.Q.loc:Location_NormalFG = 8421504
    SELF.Q.loc:Location_NormalBG = 16777215
    SELF.Q.loc:Location_SelectedFG = 16777215
    SELF.Q.loc:Location_SelectedBG = 8421504
  ELSE
    SELF.Q.loc:Location_NormalFG = -1
    SELF.Q.loc:Location_NormalBG = -1
    SELF.Q.loc:Location_SelectedFG = -1
    SELF.Q.loc:Location_SelectedBG = -1
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW2.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW2::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW2::RecordStatus=ReturnValue
  IF BRW2::RecordStatus NOT=Record:OK THEN RETURN BRW2::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = loc:Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::3:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW2::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
Stock_Usage_Report PROCEDURE                          !Generated from procedure template - Report

Progress:Thermometer BYTE
save_stm_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:RecordsCount     REAL
tmp:printer          STRING(255)
tmp:PrintedBy        STRING(60)
endofreport          BYTE,AUTO
count_temp           REAL
FilesOpened          BYTE
used_temp            REAL
purchase_cost_total_temp REAL
line_total_temp      REAL
line_total_total_temp REAL
used_total_temp      REAL
on_order_total_temp  REAL
in_stock_total_temp  REAL
location_temp        STRING(30)
supplier_temp        STRING(30)
shelf_location_temp  STRING(30)
purchase_cost_temp   REAL
tmp:FirstModel       STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(LOCATION)
                       PROJECT(loc:Location)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(43,42,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

report               REPORT('Stock Usage Report'),AT(396,2771,7521,8583),PRE(RPT),FONT('Arial',10,,),THOUS
                       HEADER,AT(396,479,7521,1750),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(def:User_Name),TRN,FONT(,14,,FONT:bold)
                         STRING('Site Location:'),AT(4896,469),USE(?String34),TRN,FONT(,8,,)
                         STRING(@s30),AT(5938,469),USE(location_temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(94,313,3531,198),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING('Supplier:'),AT(4896,625),USE(?String35),TRN,FONT(,8,,)
                         STRING(@s30),AT(5938,625),USE(supplier_temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(4896,1354),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@s30),AT(94,469,3531,198),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,625,3531,198),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING('Suppress Zeros:'),AT(4896,781),USE(?String36),TRN,FONT(,8,,)
                         STRING(@s30),AT(5938,781),USE(GLO:Select5),TRN,FONT(,8,,FONT:bold)
                         STRING('XX/XX/XXXX'),AT(5938,1354),USE(?ReportDateStamp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(94,781),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Date Range:'),AT(4896,938),USE(?String37),TRN,FONT(,8,,)
                         STRING(@d6),AT(5938,938),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6),AT(6667,938),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(4896,1198,625,208),USE(?String67),TRN,FONT(,8,,)
                         STRING(@s60),AT(5938,1198),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Tel: '),AT(104,1094),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s15),AT(573,1094),USE(def:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(104,1250),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s15),AT(573,1250),USE(def:Fax_Number),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1406,3531,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(104,1406),USE(?String16:2),TRN,FONT(,9,,)
                         STRING('Page:'),AT(4896,1510),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@s3),AT(5938,1510),USE(ReportPageNumber),TRN,FONT(,8,,FONT:bold)
                       END
break1                 BREAK(EndOfReport),USE(?unnamed:4)
DETAIL                   DETAIL,AT(,,,156),USE(?DetailBand)
                           STRING(@s20),AT(2719,0),USE(sto:Part_Number),TRN,FONT(,7,,)
                           STRING(@s25),AT(1313,0),USE(sto:Description),TRN,FONT(,7,,)
                           STRING(@s22),AT(104,0),USE(shelf_location_temp),TRN,FONT(,7,,)
                           STRING(@N8),AT(5000,0),USE(sto:Quantity_Stock),TRN,RIGHT,FONT(,7,,)
                           STRING(@n7),AT(5469,0),USE(sto:Quantity_On_Order),TRN,RIGHT,FONT(,7,,)
                           STRING(@n-8),AT(5833,0,469,208),USE(used_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(6354,0),USE(sto:Purchase_Cost),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(6875,0),USE(line_total_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s30),AT(3802,0,1198,156),USE(tmp:FirstModel),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:3)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(104,104),USE(?String68),TRN,FONT(,8,,FONT:bold)
                           STRING(@s9),AT(1094,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                           STRING(@n9),AT(4969,104),USE(in_stock_total_temp),TRN,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                           STRING(@n7),AT(5469,104),USE(on_order_total_temp),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n-8),AT(5885,104),USE(used_total_temp),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n10.2),AT(6354,104),USE(purchase_cost_total_temp),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n11.2),AT(6823,104),USE(line_total_total_temp),TRN,RIGHT,FONT(,7,,FONT:bold)
                         END
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:5)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11198),USE(?Image1)
                         STRING('STOCK USAGE REPORT'),AT(5104,52),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         STRING('Description'),AT(1313,2083),USE(?String20),TRN,FONT(,7,,FONT:bold)
                         STRING('Location'),AT(104,2083),USE(?String21),TRN,FONT(,7,,FONT:bold)
                         STRING('In Stock'),AT(5052,2083),USE(?String25),TRN,FONT(,7,,FONT:bold)
                         STRING('On Order'),AT(5417,2083),USE(?String27),TRN,FONT(,7,,FONT:bold)
                         STRING('Used'),AT(6063,2083),USE(?String29),TRN,FONT(,7,,FONT:bold)
                         STRING('Purch Cost'),AT(6344,2083),USE(?String31),TRN,FONT(,7,,FONT:bold)
                         STRING('Total Cost'),AT(6917,2083),USE(?String47),TRN,FONT(,7,,FONT:bold)
                         STRING('First Model No'),AT(3802,2083),USE(?String21:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(2719,2083),USE(?String23),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                  !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
save_sto_id   ushort,auto
save_shi_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(AskPreview, ())
  if tmp:RecordsCount = 0
      Case Missive('There are no records that match the selected criteria.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      tmp:RecordsCount = 1
  end!if tmp:RecordsCount = 0
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(report)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(AskPreview, ())


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Stock_Usage_Report')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  Access:STOMODEL.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:LOCATION, ?Progress:PctText, Progress:Thermometer, ProgressMgr, loc:Location)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(loc:Location_Key)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:LOCATION.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Report','Close','Stock_Usage_Report')
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  tmp:printer = printer{propprint:device}
  choose_printer(new_printer",preview",copies")
  printer{propprint:device} = new_printer"
  printer{propprint:copies} = copies"
  if preview" <> 'YES'
      self.skippreview = true
  end!if preview" <> 'YES'
  ReturnValue = PARENT.OpenReport()
  set(defaults)
  access:Defaults.next()
  If glo:select3 = ''
      location_temp = 'ALL'
  Else
      location_Temp = glo:select3
  End
  If glo:select4 = ''
      supplier_temp = 'ALL'
  Else
      supplier_Temp = glo:select4
  End
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = Clip(use:Forename) & ' '  & Clip(use:surname)
  IF ~ReturnValue
    report$?ReportPageNumber{PROP:PageNo}=True
  END
  IF ~ReturnValue
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@d6)
  END
      ! Save Window Name
   AddToLog('Report','Open','Stock_Usage_Report')
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeNoRecords, ())
  return
  PARENT.TakeNoRecords
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeNoRecords, ())


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  !tmp:MainStore   = MainStoreLocation()
  
  print# = 1
  If glo:select3 <> ''
      If glo:select3 <> loc:location
          print# = 0
      End!If glo:select3 = loc:location
  End!If glo:select3 <> ''
  If print# = 1
      IF GLO:Select6 = 'P' !Added by Neil, allows to sort by location or part number!
        save_sto_id = access:stock.savefile()
        access:stock.clearkey(sto:location_key)
        sto:location    = loc:location
        set(sto:location_key,sto:location_key)
      ELSE
        save_sto_id = access:stock.savefile()
        access:stock.clearkey(sto:Shelf_location_key)
        sto:location    = loc:location
        set(sto:Shelf_location_key,sto:Shelf_location_key)
      END
      loop
          if access:stock.next()
             break
          end !if
          if sto:location    <> loc:location      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          If glo:select4 <> ''
              If sto:supplier <> glo:select4
                  Cycle
              End!If sto:supplier <> glo:select4
          End!If glo:select4 <> ''
  
  !Get First Model
  
          tmp:FirstModel = ''
          save_stm_id = access:stomodel.savefile()
          access:stomodel.clearkey(stm:model_number_key)
          stm:ref_number   = sto:ref_number
          stm:manufacturer = sto:manufacturer
          set(stm:model_number_key,stm:model_number_key)
          loop
              if access:stomodel.next()
                 break
              end !if
              if stm:ref_number   <> sto:ref_number      |
              or stm:manufacturer <> sto:manufacturer      |
                  then break.  ! end if
              tmp:FirstModel = stm:model_number
              Break
          end !loop
          access:stomodel.restorefile(save_stm_id)
  
          used_temp = 0
          line_total_temp = 0
          save_shi_id = access:stohist.savefile()
          access:stohist.clearkey(shi:ref_number_key)
          shi:ref_number = sto:ref_number
          shi:date       = glo:select1
          set(shi:ref_number_key,shi:ref_number_key)
          loop
              if access:stohist.next()
                 break
              end !if
              if shi:ref_number <> sto:ref_number      |
              or shi:date       > glo:select2      |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              If shi:job_number = ''
                  Cycle
              End
  
              If shi:transaction_type = 'DEC'
                  used_temp += shi:quantity
              End!If shi:transaction_type = 'DEC'
              If shi:transaction_type = 'REC'
                  used_temp -= shi:quantity
              End!If shi:transaction_type = 'REC'
          end !loop
          access:stohist.restorefile(save_shi_id)
          If glo:select5 = 'YES'
              If used_temp = 0
                  Cycle
              End!If used_temp = 0
          End!If glo:select5 = 'YES'
          line_total_temp += used_temp * sto:purchase_cost
          on_order_total_temp += sto:quantity_on_order
          in_stock_total_temp += sto:quantity_stock
          line_total_total_temp += line_Total_temp
          used_total_temp += used_temp
          purchase_cost_total_temp += sto:purchase_cost
          count_temp += 1
          shelf_location_temp = Clip(sto:shelf_location) & ' / ' & Clip(sto:second_location)
          tmp:RecordsCount += 1
          Print(rpt:detail)
      end !loop
      access:stock.restorefile(save_sto_id)
  
  End!If print# = 1
  ReturnValue = PARENT.TakeRecord()
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Stock_Usage_Criteria PROCEDURE                        !Generated from procedure template - Window

FilesOpened          BYTE
site_location_temp   STRING('ALL')
supplier_temp        STRING('ALL')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select3
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select4
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB3::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
window               WINDOW('Stock Usage Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       OPTION('Supplier'),AT(222,184,236,32),USE(supplier_temp),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('All'),AT(226,196),USE(?Option2:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                         RADIO('Individual'),AT(258,196),USE(?Option2:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('IND')
                       END
                       COMBO(@s30),AT(324,194,124,10),USE(GLO:Select4),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                       CHECK('Suppress Zeros'),AT(324,218),USE(GLO:Select5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                       STRING('Sort Order'),AT(260,236),USE(?String1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       OPTION('Option 3'),AT(328,234,136,12),USE(GLO:Select6)
                         RADIO('Part Number'),AT(324,234),USE(?Option3:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('P')
                         RADIO('Shelf Location'),AT(393,234),USE(?Option3:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('S')
                       END
                       CHECK('Transfers From Main Store Only'),AT(324,252),USE(GLO:Select7),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                       PROMPT('Start Date'),AT(260,270),USE(?glo:select1:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6b),AT(324,270,64,10),USE(GLO:Select1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       BUTTON,AT(392,266),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('End Date'),AT(260,290),USE(?glo:select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6b),AT(324,290,64,10),USE(GLO:Select2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       BUTTON,AT(392,286),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       OPTION('Site Location'),AT(222,144,236,32),USE(site_location_temp),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('All'),AT(226,156),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                         RADIO('Individual'),AT(258,156),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('IND')
                       END
                       COMBO(@s30),AT(324,156,124,10),USE(GLO:Select3),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M*@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Usage Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
show_hide        Routine
    Case site_location_temp
        Of 'ALL'
            Disable(?glo:select3)
        Of 'IND'
            Enable(?glo:select3)
    End!Case glo:select3

    Case supplier_temp
        Of 'ALL'
            Disable(?glo:select4)
        Of 'IND'
            Enable(?glo:select4)
    End!Case supplier_temp
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020182'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Stock_Usage_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Option2:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Do show_hide
  glo:select1 = Deformat('1/1/1990',@d6)
  glo:select2 = Today()
  glo:select3 = ''
  glo:select4 = ''
  glo:select5 = 'NO'
  glo:select6 = 'P'
  glo:Select7 = 0
      ! Save Window Name
   AddToLog('Window','Open','Stock_Usage_Criteria')
  Bryan.CompFieldColour()
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
  ?glo:select2{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB2.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(loc:Location_Key)
  FDCB2.AddField(loc:Location,FDCB2.Q.loc:Location)
  FDCB2.AddField(loc:RecordNumber,FDCB2.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB3.Init(GLO:Select4,?GLO:Select4,Queue:FileDropCombo:1.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo:1,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo:1
  FDCB3.AddSortOrder(sup:Company_Name_Key)
  FDCB3.AddField(sup:Company_Name,FDCB3.Q.sup:Company_Name)
  FDCB3.AddField(sup:RecordNumber,FDCB3.Q.sup:RecordNumber)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  Clear(glo:G_Select1)
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Stock_Usage_Criteria')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?supplier_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?supplier_temp, Accepted)
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?supplier_temp, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?glo:select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020182'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020182'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020182'&'0')
      ***
    OF ?site_location_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?site_location_temp, Accepted)
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?site_location_temp, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If glo:select1 = ''
          error# = 1
          Select(?glo:select1)
      End!If glo:select1 = ''
      If glo:select2 = ''
          error# = 1
          Select(?glo:select2)
      End
      If site_location_temp = 'ALL'
          glo:select3 = ''
      End!If site_location_temp = 'ALL'
      If supplier_temp = 'ALL'
          glo:select4 = ''
      End!If site_location_temp = 'ALL'
      If error# = 0
          stock_usage_report
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      Clear(glo:G_Select1)
      Post(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?GLO:Select1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

FDCB2.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

Parts_Not_Received_Criteria PROCEDURE                 !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::4:TAGFLAG          BYTE(0)
DASBRW::4:TAGMOUSE         BYTE(0)
DASBRW::4:TAGDISPSTATUS    BYTE(0)
DASBRW::4:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
FilesOpened          BYTE
tag_temp             STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Parts Not Received Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Parts Not Received Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(212,102,172,188),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@80L(2)|M~Supplier~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('By Company Name'),USE(?Tab:2)
                           PROMPT('Tag the Suppliers to include in the Parts Not Received Report'),AT(225,84),USE(?Prompt1),FONT(,8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON('&Rev tags'),AT(244,191,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(204,215,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(408,126),USE(?DASTAG),TRN,FLAT,LEFT,KEY(AltT),ICON('tagitemp.jpg')
                           BUTTON,AT(408,158),USE(?DASTAGAll),TRN,FLAT,LEFT,KEY(AltA),ICON('tagallp.jpg')
                           BUTTON,AT(408,190),USE(?DASUNTAGALL),TRN,FLAT,LEFT,KEY(AltU),ICON('untagalp.jpg')
                           OPTION('Report Type'),AT(212,296,256,28),USE(GLO:Select1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All Parts Not Received'),AT(235,308),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                             RADIO('Overdue Parts Not Received'),AT(340,308),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('OVR')
                           END
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::4:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:Queue.Pointer = sup:Company_Name
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = sup:Company_Name
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = sup:Company_Name
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::4:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::4:QUEUE = GLO:Queue
    ADD(DASBRW::4:QUEUE)
  END
  FREE(GLO:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::4:QUEUE.Pointer = sup:Company_Name
     GET(DASBRW::4:QUEUE,DASBRW::4:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = sup:Company_Name
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASSHOWTAG Routine
   CASE DASBRW::4:TAGDISPSTATUS
   OF 0
      DASBRW::4:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::4:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::4:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020170'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Parts_Not_Received_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:SUPPLIER.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:SUPPLIER,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  glo:select1 = 'ALL'
      ! Save Window Name
   AddToLog('Window','Open','Parts_Not_Received_Criteria')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,sup:Company_Name_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,sup:Company_Name,1,BRW1)
  BIND('tag_temp',tag_temp)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tag_temp,BRW1.Q.tag_temp)
  BRW1.AddField(sup:Company_Name,BRW1.Q.sup:Company_Name)
  BRW1.AddField(sup:RecordNumber,BRW1.Q.sup:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:SUPPLIER.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Parts_Not_Received_Criteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020170'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020170'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020170'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      If glo:Queue
          Parts_Not_Received_Report
      Else!If glo:Queue
          Case Missive('You must select at least ONE Supplier.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      End!If glo:Queue
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::4:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::4:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = sup:Company_Name
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = sup:Company_Name
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::4:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Days_Between_Routine PROCEDURE  (f_start_date,f_end_date,f_days) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(defaults)
    access:defaults.next()
    f_days = 0
    Loop
        f_start_date += 1
        If def:include_saturday <> 'YES'
            If (f_start_date) % 7 = 6
                Cycle
            End!If (f_start_date + count#) % 7 = 6
        End!If def:include_saturday <> 'YES'
        If def:include_sunday <> 'YES'
            If (f_start_date) % 7 = 0
                Cycle
            End!If (f_start_date + count#) % 7 = 6
        End!If def:include_saturday <> 'YES'
        f_days += 1
        If f_start_date > = f_end_date
            Break
        End!If f_start_date > = f_end_date
    End!Loop
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Choose_Printer PROCEDURE (f_printer,f_preview,f_copies) !Generated from procedure template - Window

FilesOpened          BYTE
defaultprinter       CSTRING(256)
tmp:printer          CSTRING(256)
printer_name_temp    CSTRING(256)
preview_temp         STRING('YES')
copies_temp          LONG(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Choose Destination Printer'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Choose Destination Printer'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('Printer Name'),AT(248,218),USE(?printer_name_temp:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s255),AT(300,218,112,10),USE(printer_name_temp),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                       BUTTON,AT(412,214),USE(?Choose_Printer),TRN,FLAT,LEFT,TIP('Change Selected Printer'),ICON('lookupp.jpg')
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Choose Destination Printer'),USE(?Tab1)
                           PROMPT('# Copies'),AT(248,206),USE(?copies_temp:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           SPIN(@n8),AT(300,206,64,10),USE(copies_temp),LEFT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       CHECK('Show Print Preview'),AT(300,242),USE(preview_temp),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                       BUTTON,AT(368,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!EnumPtrcl        EnumPrtClType

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020157'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Choose_Printer')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  printer_name_temp   = printer{propprint:device}
      ! Save Window Name
   AddToLog('Window','Open','Choose_Printer')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  !EnumPtrCl.init()
  !EnumPtrCl.GetDefaultPrinter(DefaultPrinter)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','Choose_Printer')
  !EnumPtrCl.kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020157'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020157'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020157'&'0')
      ***
    OF ?Choose_Printer
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Choose_Printer, Accepted)
!      If EnumPtrCl.SetDefaultPrinter(1)  then
!        Message('error setting default printer')
!      end
!      EnumPtrCl.GetDefaultPrinter(printer_name_temp)
!      !printer_name_temp = Upper(printer_name_temp)
!      display(?printer_name_temp)
!      If EnumPtrCl.SetDefaultPrinter(0,defaultprinter)
!          Stop(error())
!      End!If EnumPtrCl.SetDefaultPrinter(tmp:printer)

    If PrinterDialog('Select printer')
       printer_Name_temp = PRINTER{PROPPRINT:Device}
       Display(?Printer_Name_Temp)
    End

      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Choose_Printer, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      f_printer   = printer_name_temp
      f_preview   = preview_temp
      f_copies    = copies_temp
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Exchange_Units_In_Channel PROCEDURE                   !Generated from procedure template - Report

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
Progress:Thermometer BYTE
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:RecordsCount     REAL
Exchange_Queue       QUEUE,PRE(excque)
model_number         STRING(30)
request_received     REAL
same_day             REAL
other_day            REAL
received_in_workshop REAL
                     END
In_Repair_Queue      QUEUE,PRE(repque)
model_number         STRING(30)
in_stock             REAL
in_workshop          REAL
awaiting_parts       REAL
RTM                  REAL
                     END
first_page_temp      BYTE(1)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
endofreport          BYTE,AUTO
tmp:printer          STRING(255)
FilesOpened          BYTE
tmp:PrintedBy        STRING(60)
account_number_temp  STRING(60)
model_number_temp    STRING(30)
requests_received_temp STRING(6)
same_day_temp        STRING(6)
other_day_temp       STRING(6)
Failed_Exchange_Temp STRING(6)
Despatched_Temp      STRING(6)
In_Workshop_Temp     STRING(6)
Awaiting_Arrival_Temp STRING(6)
In_Stock_Temp        STRING(6)
Units_In_Workshop_Temp STRING(6)
Received_In_Workshop_Temp STRING(6)
Awaiting_Parts_Temp  STRING(6)
RTM_Temp             STRING(6)
In_Repair_Total      STRING(6)
In_Channel_Temp      STRING(6)
Removed_From_Stock_Temp STRING(6)
In_Scheme_Temp       STRING(6)
Manufacturer_Temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS)
                       PROJECT(job:date_booked)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

! moving bar window
rejectrecord2         long
recordstoprocess2     long,auto
recordsprocessed2     long,auto
recordspercycle2      long,auto
recordsthiscycle2     long,auto
percentprogress2      byte
recordstatus2         byte,auto

progress:thermometer2 byte
progresswindow2 WINDOW('Printing...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(Progress:Thermometer2),AT(15,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,141,10),USE(?progress:userstring2),CENTER
       STRING(''),AT(0,30,141,10),USE(?progress:pcttext2),CENTER
     END
report               REPORT('Exchange Units In Channel Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,750,7521,1771),USE(?unnamed)
                         STRING('Trade Account:'),AT(4948,208),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s60),AT(5729,208),USE(account_number_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(4948,469),USE(?String23),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5729,469),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(6406,469),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(4948,729),USE(?String26),TRN,FONT(,8,,)
                         STRING(@s60),AT(5729,729),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(4948,990),USE(?String27),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(4948,1250),USE(?string59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5729,1250),USE(ReportPageNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('XX/XX/XXXX'),AT(5729,990),USE(?ReportDateStamp),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,7208),USE(?detailband)
                           STRING('MODEL NUMBER:'),AT(677,0),USE(?model_string),TRN,FONT(,9,,FONT:bold)
                           STRING(@s30),AT(1875,0),USE(model_number_temp),TRN,FONT(,9,,FONT:bold)
                           STRING('EXCHANGE TRANSACTION'),AT(677,365),USE(?String28),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,677),USE(requests_received_temp),TRN,RIGHT
                           STRING('IN TRANSIT'),AT(698,2135),USE(?String28:2),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,2396),USE(Despatched_Temp),TRN,RIGHT
                           STRING('IN REPAIR'),AT(698,3750),USE(?String28:3),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,3958),USE(In_Stock_Temp),TRN,RIGHT
                           STRING('GENERAL DETAILS'),AT(677,5729),USE(?String28:4),TRN,FONT(,9,,FONT:bold)
                           STRING('Exchange Units In Channel:'),AT(1875,5990),USE(?String41),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,5990),USE(In_Channel_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           LINE,AT(5938,6198,500,0),USE(?Line1:5),COLOR(COLOR:Black)
                           STRING('Exchange Units In Stock:'),AT(1875,3958),USE(?String36),TRN
                           STRING('Exchange Units In Workshop:'),AT(1875,4271),USE(?String37),TRN
                           STRING(@s6),AT(5906,4271),USE(In_Workshop_Temp),TRN,RIGHT
                           STRING('Exchange Units Awaiting Parts:'),AT(1875,4583),USE(?String38),TRN
                           STRING(@s6),AT(5906,4583),USE(Awaiting_Parts_Temp),TRN,RIGHT
                           STRING('Exchange Units R.T.M.:'),AT(1875,4896),USE(?String39),TRN
                           STRING(@s6),AT(5906,4896),USE(RTM_Temp),TRN,RIGHT
                           STRING('Total:'),AT(1875,5208),USE(?String45),TRN,FONT(,10,,FONT:bold)
                           STRING(@s6),AT(5906,5208),USE(In_Repair_Total),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING('Exchange Units Despatched:'),AT(1875,2396),USE(?String33),TRN
                           STRING('Exchange Units Received Into Workshop:'),AT(1875,2708),USE(?String34),TRN
                           STRING(@s6),AT(5906,2708),USE(Received_In_Workshop_Temp),TRN,RIGHT
                           STRING('Exchange Units Awaiting Arrival:'),AT(1875,3021),USE(?String35),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,3021),USE(Awaiting_Arrival_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING('Exchange Requests Received:'),AT(1875,677),USE(?String29),TRN
                           STRING('Exchange Units Despatched (Same Day):'),AT(1875,990),USE(?String30),TRN
                           STRING(@s6),AT(5906,990),USE(same_day_temp),TRN,RIGHT
                           STRING('Exchange Units Despatched (Not Same Day):'),AT(1875,1354),USE(?String31),TRN
                           STRING(@s6),AT(5906,1354),USE(other_day_temp),TRN,RIGHT
                           LINE,AT(5938,1667,500,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Failed Exchange Transactions:'),AT(1875,1719),USE(?String32),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,1719),USE(Failed_Exchange_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           LINE,AT(5938,2969,500,0),USE(?Line1:2),COLOR(COLOR:Black)
                           LINE,AT(5938,5156,500,0),USE(?Line1:3),COLOR(COLOR:Black)
                           LINE,AT(5938,5938,500,0),USE(?Line1:4),COLOR(COLOR:Black)
                         END
new_page                 DETAIL,PAGEBEFORE(-1),AT(,,,52),USE(?new_page)
                         END
first_page_title         DETAIL,AT(,,,656),USE(?first_page_title)
                           STRING('SELECTED MODEL NUMBERS'),AT(677,104),USE(?String28:5),TRN,FONT(,9,,FONT:bold)
                         END
first_page_detail        DETAIL,AT(,,,167),USE(?first_page_detail)
                           STRING(@s20),AT(1875,0),USE(GLO:Pointer),TRN,FONT(,8,,)
                           STRING(@s30),AT(3385,0),USE(Manufacturer_Temp),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('EXCHANGE UNITS IN CHANNEL'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,885),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s15),AT(521,885),USE(def:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1042),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s15),AT(521,1042),USE(def:Fax_Number),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1198,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1198),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Description'),AT(698,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Units In Channel'),AT(5906,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
getnextrecord2      routine
  recordsprocessed2 += 1
  recordsthiscycle2 += 1
  if percentprogress2 < 100
    percentprogress2 = (recordsprocessed2 / recordstoprocess2)*100
    if percentprogress2 > 100
      percentprogress2 = 100
    end
    if percentprogress2 <> progress:thermometer2 then
      progress:thermometer2 = percentprogress2
      ?progress:pcttext2{prop:text} = format(percentprogress2,@n3) & '% Completed'
      display()
    end
  end
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(AskPreview, ())
  recordspercycle2     = 25
  recordsprocessed2    = 0
  percentprogress2     = 0
  setcursor(cursor:wait)
  open(progresswindow2)
  Display()
  progress:thermometer2    = 0
  ?progress:pcttext2{prop:text} = '0% Completed'
  
  recordstoprocess2    = Records(glo:Queue) + Records(exchange)
  
  setcursor(cursor:wait)
  
  in_stock_temp = 0
  in_workshop_temp = 0
  Awaiting_Parts_Temp = 0
  RTM_Temp = 0
  setcursor(cursor:wait)
  
  save_xch_id = access:exchange.savefile()
  set(xch:ref_number_key)
  loop
      if access:exchange.next()
         break
      end !if
      Do GetNextRecord2
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      Sort(glo:Queue,glo:pointer)
      glo:pointer = xch:model_number
      Get(glo:Queue,glo:pointer)
      If error()
          Cycle
      End
      If glo:select4 = 'YES'                                                               !If One Model Per Page. Build up the
          Clear(in_repair_queue)
          Sort(in_repair_queue,repque:model_number)                                           !In Repair Queue.
          repque:model_number = xch:model_number
          Get(in_repair_queue,repque:model_number)
          If Error()                                                                          !Check the model number of the exchange
              repque:model_number = xch:model_number                                          !unit. Add if not found, put if found.
              If xch:available = 'AVL'
                  repque:in_stock = 1
              Else
                  repque:in_stock = 0
              End
              Add(in_repair_queue)
          Else
              If xch:available = 'AVL'
                  repque:in_stock += 1
                  Put(in_repair_queue)
              End
          End!If Error()
      Else!If glo:select4 = 'YES'
          If xch:available = 'AVL'
              in_stock_temp += 1
          End!If xch:job_number <> 0
      End!If glo:select4 = 'YES'
  
  
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = xch:job_number
      if access:jobs.fetch(job:ref_number_key) = Level:Benign
          If job:date_completed = ''
  !Model Dependent
              workshop# = 0
              If job:workshop = 'YES'
                  workshop# = 1
              End!If job:workshop = 'YES'
              setcursor(cursor:wait)
              rtm# = 0
              If job:third_party_site <> '' And job:workshop <> 'YES'
                  rtm# = 1
              Else!If job:third_party_site <> '' And job:workshop <> 'YES'
                  found# = 0
                  save_wpr_id = access:warparts.savefile()
                  access:warparts.clearkey(wpr:part_number_key)
                  wpr:ref_number  = job:ref_number
                  set(wpr:part_number_key,wpr:part_number_key)
                  loop
                      if access:warparts.next()
                         break
                      end !if
                      if wpr:ref_number  <> job:ref_number      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      If wpr:date_ordered <> '' and wpr:date_received = ''
                          found# = 1
                      Else!If wpr:date_ordered <> and wpr:date_received = 'YES'
                          If wpr:pending_ref_number <> '' and wpr:date_received = ''
                              found# = 1
                          End!If wpr:pending_ref_number <> ''
                      End!If wpr:date_ordered <> and wpr:date_received = 'YES'
                  end !loop
                  access:warparts.restorefile(save_wpr_id)
                  setcursor()
  
                  If found# = 0
                      setcursor(cursor:wait)
                      save_par_id = access:parts.savefile()
                      access:parts.clearkey(par:part_number_key)
                      par:ref_number  = job:ref_number
                      set(par:part_number_key,par:part_number_key)
                      loop
                          if access:parts.next()
                             break
                          end !if
                          if par:ref_number  <> job:ref_number      |
                              then break.  ! end if
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          If par:date_ordered <> '' and par:date_received = ''
                              found# = 1
                          Else!If wpr:date_ordered <> and wpr:date_received = 'YES'
                              If par:pending_ref_number <> '' and par:date_received = ''
                                  found# = 1
                              End!If wpr:pending_ref_number <> ''
                          End!If wpr:date_ordered <> and wpr:date_received = 'YES'
                      end !loop
                      access:parts.restorefile(save_par_id)
                      setcursor()
                  End!If found# = 0
  
              End!If job:third_party_site <> '' And job:workshop <> 'YES'
  
              If glo:select4 = 'YES'
                  Clear(in_repair_queue)
                  Sort(in_repair_queue,repque:model_number)
                  repque:model_number = xch:model_number
                  Get(in_repair_queue,repque:model_number)
                  If workshop# = 1
                      repque:in_workshop += 1
                  End!If job:workshop = 'YES'
                  If rtm# = 1
                      repque:rtm += 1
                  End!If rtm# = 1
                  If found# = 1
                      repque:awaiting_parts += 1
                  End!If found# = 1
                  Put(in_repair_queue)
              Else!If glo:select4 = 'YES'
                  If workshop# = 1
                      in_workshop_temp += 1
                  End!If job:workshop = 'YES'
                  If rtm# = 1
                      rtm_temp += 1
                  End!If rtm# = 1
                  If found# = 1
                      awaiting_parts_temp += 1
                  End!If found# = 1
  
              End!If glo:select4 = 'YES'
  
          End!If job:date_completed = ''
      end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
  
  end !loop
  access:exchange.restorefile(save_xch_id)
  setcursor()
  
  !If One Model Per Page
  If glo:select4 = 'YES'
  
      first_page# = 1
      Sort(glo:Queue,glo:pointer)                                                       !Loop through the global list of models
      Loop x# = 1 to Records(glo:Queue)                                                    !so that even if there aren't any records
          Get(glo:Queue,x#)                                                                !in the exchange queue, a page will still
          in_stock_temp = 0
          in_workshop_temp = 0
          Awaiting_Parts_Temp = 0
          RTM_Temp = 0
          requests_received_temp  = 0
          same_day_temp           = 0
          other_day_temp          = 0
          received_in_workshop_temp = 0
          Clear(exchange_queue)
          Sort(exchange_queue,excque:model_number)                                            !be printed for the in stock bit.
          excque:model_number = glo:pointer
          Get(exchange_queue,excque:model_number)
          Do getnextrecord2
          model_number_temp       = excque:model_number
          requests_received_temp  = excque:request_received
          same_day_temp           = excque:same_day
          other_day_temp          = excque:other_day
          received_in_workshop_temp = excque:received_in_workshop
          Failed_Exchange_Temp = requests_received_temp - same_day_temp - other_day_temp
          Despatched_Temp = same_day_temp + other_day_temp
          Awaiting_Arrival_Temp = Despatched_Temp - Received_In_Workshop_Temp
          Clear(in_repair_queue)
          Sort(in_repair_queue,repque:model_number)
          Loop y# = 1 To Records(in_repair_queue)
              Get(in_repair_queue,y#)
              If repque:model_number <> excque:model_number
                  Cycle
              End
              in_stock_temp   += repque:in_stock
              in_workshop_temp    += repque:in_workshop
              awaiting_parts_temp += repque:awaiting_parts
              rtm_temp    += repque:RTM
          End!Loop x# = 1 To Records(in_repair_queue)
          In_Repair_Total = In_Stock_Temp + In_Workshop_Temp + Awaiting_Parts_Temp + RTM_Temp
          In_Channel_Temp = awaiting_arrival_temp + in_repair_total
          tmp:RecordsCount += 1
          Print(rpt:Detail)
          If x# <> Records(glo:Queue)
              Print(rpt:new_page)
          End!If first_page# = 1
      End!Loop x# = 1 To Records(exchange_queue)
  
  Else!If glo:select4 = 'YES'
      Failed_Exchange_Temp = requests_received_temp - same_day_temp - other_day_temp
      Despatched_Temp = same_day_temp + other_day_temp
      Awaiting_Arrival_Temp = Despatched_Temp - Received_In_Workshop_Temp
      In_Repair_Total = In_Stock_Temp + In_Workshop_Temp + Awaiting_Parts_Temp + RTM_Temp
      In_Channel_Temp = awaiting_arrival_temp + in_repair_total
      tmp:RecordsCount += 1
      Print(rpt:Detail)
  End!If glo:select4 = 'YES'
  
  setcursor()
  close(progresswindow2)
  
  if tmp:RecordsCount = 0
      Case Missive('There are no records that match the selected criteria.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      tmp:RecordsCount = 1
  end!if tmp:RecordsCount = 0
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(report)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(AskPreview, ())


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Exchange_Units_In_Channel')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Relate:TRANTYPE.Open
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:date_booked)
  ThisReport.AddSortOrder(job:Date_Booked_Key)
  ThisReport.AddRange(job:date_booked,GLO:Select1,GLO:Select2)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:TRANTYPE.Close
  END
  ! Save Window Name
   AddToLog('Report','Close','Exchange_Units_In_Channel')
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !Choose Printer
  tmp:printer = printer{propprint:device}
  Choose_Printer(new_printer",preview",copies")
  printer{propprint:device} = new_printer"
  printer{propprint:copies} = copies"
  if preview" <> 'YES'
      self.skippreview = True
  End!if preview" <> 'YES'
  ReturnValue = PARENT.OpenReport()
  !printed by
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF ~ReturnValue
    report$?ReportPageNumber{PROP:PageNo}=True
  END
  IF ~ReturnValue
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@d6b)
  END
      ! Save Window Name
   AddToLog('Report','Open','Exchange_Units_In_Channel')
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeNoRecords, ())
  return
  PARENT.TakeNoRecords
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeNoRecords, ())


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  error# = 0
  If glo:select3 <> ''
      If job:account_number <> glo:select3
          error# = 1
      End!If job:account_number <> glo:select3
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = glo:select3
      if access:subtracc.fetch(sub:account_number_key) = Level:Benign
          account_number_temp = Clip(sub:account_number) & '-' & Clip(sub:company_name)
      end
  Else!If glo:select <> ''
      account_number_temp = 'ALL'
  End!If glo:select <> ''
  
  If error# = 0
  !IF glo:select4 = YES, i.e. one model per page. Don't print the first (summary) page.
  !Otherwise, show the model on top of each page
      If glo:select4 <> 'YES'
          Settarget(report)
          Hide(?model_string)
          model_number_temp = ''
          Settarget()
          If first_page_temp = 1
              first_page_temp = 0
              Print(rpt:first_page_title)
              Sort(glo:Queue,glo:pointer)
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  access:modelnum.clearkey(mod:model_number_key)
                  mod:model_number = glo:pointer
                  if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                      manufacturer_temp = mod:manufacturer
                  end!if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                  Print(rpt:first_page_detail)
              End!Loop x# = 1 To Records(glo:Queue)
              Print(rpt:new_page)
          End!If first_page_temp = 1
      End!If glo:select4 = 'YES'
  
  !Looping through jobs in date order.
  
  !First If glo:select4 = 'YES', means one page per model.
  
  !Check the queue of models and see if the job model is in it
      Sort(glo:Queue,glo:pointer)
      glo:pointer  = job:model_number
      Get(glo:Queue,glo:pointer)
      If ~Error()
          request# = 0
          access:trantype.clearkey(trt:transit_type_key)                                      !Check the transit type to see if the
          trt:transit_type = job:transit_type                                                 !default status is '108 Exchange Unit
          if access:trantype.fetch(trt:transit_type_key) = Level:benign                       !Required'.
              If sub(trt:initial_status,1,3) = '108'
                  request# = 1                                                                !Request Received
              End!If sub(trt:initial_status,1,3) = '108'
          end!if access:trantype.fetch(trt:transit_type_key) = Level:benign
          If glo:select4 = 'YES'
              Clear(exchange_queue)                                                       !to the job. Hopefully this should tie
              Sort(exchange_queue,excque:model_number)                                    !up with the requests, but there may be
              excque:model_number = job:model_number                                      !a mismatch due to transit types being
              Get(exchange_queue,excque:model_number)                                     !changed on the job.
              If Error()                                                                  !Model Number doesn't exist
                  excque:model_number = job:model_number
                  If request# = 1
                      excque:request_received = 1
                  Else!If request# = 1
                      excque:request_received = 0
                  End!If request# = 1
                  If job:exchange_unit_number <> ''
                      If job:exchange_despatched <> ''
                          If job:exchange_despatched = job:date_booked
                              excque:same_day = 1
                              excque:other_day = 0
                          Else!If job:exchange_despatched = job:date_booked
                              excque:other_day = 1
                              excque:same_day = 0
                          End!If job:exchange_despatched = job:date_booked
                      End!If job:exchange_despatched <> ''
  
                      If job:workshop = 'YES'
                          excque:received_in_workshop = 1
                      Else!If job:workshop = 'YES'
                          excque:received_in_workshop = 0
                      End!If job:workshop = 'YES'
                  End!If job:exchange_unit_number <> ''
                  Add(Exchange_Queue)
  
              Else!If Error()                                                             !Model number does exist
                  If request# = 1
                      excque:request_received += 1
                  End
                  If job:exchange_unit_number <> ''
                      If job:exchange_despatched <> ''
                          If job:exchange_despatched = job:date_booked
                              excque:same_day += 1
                          Else!If job:exchange_despatched = job:date_booked
                              excque:other_day += 1
                          End!If job:exchange_despatched = job:date_booked
                      End!If job:exchange_despatched <> ''
  
                      If job:workshop = 'YES'
                          excque:received_in_workshop += 1
                      End!If job:workshop = 'YES'
                  End!If job:exchange_unit_number <> ''
                  Put(Exchange_Queue)
              End!If Error()
          Else!If glo:select4 = 'YES'
              If request# = 1
                  requests_received_temp += 1
              End!If request# = 1
              If job:exchange_unit_number <> ''
                  If job:exchange_despatched <> ''
                      If job:exchange_despatched = job:date_booked
                          same_day_temp += 1
                      Else!If job:exchange_despatched = job:date_booked
                          other_day_temp += 1
                      End!If job:exchange_despatched = job:date_booked
                  End!If job:exchange_despatched <> ''
                  If job:workshop = 'YES'
                      Received_In_Workshop_Temp += 1
                  End!If job:workshop = 'YES'
              End!If job:exchange_unit_number <> ''
          End!If glo:select4 = 'YES'
      End!If ~Error()
  End!If error# = 0
  ReturnValue = PARENT.TakeRecord()
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Stock_Forecast_Criteria PROCEDURE                     !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select3
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB5::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
window               WINDOW('Stock Forecast Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Site Location'),AT(248,196),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s30),AT(308,196,124,10),USE(GLO:Select1),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       PROMPT('Manufacturer'),AT(248,210),USE(?Prompt1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s40),AT(308,210,124,10),USE(GLO:Select3),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                       CHECK('Suppress Zeros'),AT(308,222),USE(GLO:Select2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Forecast Report Criteria'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(304,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020181'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Stock_Forecast_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  SELF.FilesOpened = True
  glo:select1 = ''
  glo:select2 = 'NO'
  glo:select3 = ''
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Stock_Forecast_Criteria')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB2.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(loc:Location_Key)
  FDCB2.AddField(loc:Location,FDCB2.Q.loc:Location)
  FDCB2.AddField(loc:RecordNumber,FDCB2.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB5.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo:1.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:1
  FDCB5.AddSortOrder(man:Manufacturer_Key)
  FDCB5.AddField(man:Manufacturer,FDCB5.Q.man:Manufacturer)
  FDCB5.AddField(man:RecordNumber,FDCB5.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Stock_Forecast_Criteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020181'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020181'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020181'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If glo:select1 = ''
          Select(?glo:select1)
      Else!If glo:select1 = ''
          Stock_Forecast_Report
      End!If glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

FDCB2.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

Exchange_Unit_Audit_Criteria PROCEDURE                !Generated from procedure template - Window

FilesOpened          BYTE
All_temp             STRING('NO {1}')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
window               WINDOW('Exchange Unit Audit Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Exchange Unit Audit Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Exchange Unit Audit Report Criteria'),USE(?Tab1)
                           COMBO(@s30),AT(304,151,124,10),USE(GLO:Select1),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           CHECK('Available'),AT(316,175),USE(GLO:Select2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Incoming Transit'),AT(316,186),USE(GLO:Select3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('In Repair'),AT(316,199),USE(GLO:Select4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Exchanged'),AT(316,210),USE(GLO:Select5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Despatched'),AT(316,223),USE(GLO:Select6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Suspended'),AT(316,234),USE(GLO:Select7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('All'),AT(316,247),USE(All_temp),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           GROUP('Status'),AT(304,162,124,100),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           PROMPT('Stock Type'),AT(228,151),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020162'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Exchange_Unit_Audit_Criteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:STOCKTYP.Open
  SELF.FilesOpened = True
  glo:select1 = ''
  glo:select2 = 'NO'
  glo:select3 = 'NO'
  glo:select4 = 'NO'
  glo:select5 = 'NO'
  glo:select6 = 'NO'
  glo:select7 = 'NO'
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Exchange_Unit_Audit_Criteria')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?All_temp{Prop:Checked} = True
    GLO:Select7 = 'YES'
    GLO:Select6 = 'YES'
    GLO:Select5 = 'YES'
    GLO:Select4 = 'YES'
    GLO:Select3 = 'YES'
    GLO:Select2 = 'YES'
  END
  IF ?All_temp{Prop:Checked} = False
    GLO:Select7 = 'NO'
    GLO:Select6 = 'NO'
    GLO:Select5 = 'NO'
    GLO:Select4 = 'NO'
    GLO:Select3 = 'NO'
    GLO:Select2 = 'NO'
  END
  FDCB2.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(stp:Use_Exchange_Key)
  FDCB2.AddField(stp:Stock_Type,FDCB2.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  Clear(glo:G_Select1)
  Clear(glo:G_Select1)
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCKTYP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Exchange_Unit_Audit_Criteria')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020162'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020162'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020162'&'0')
      ***
    OF ?All_temp
      IF ?All_temp{Prop:Checked} = True
        GLO:Select7 = 'YES'
        GLO:Select6 = 'YES'
        GLO:Select5 = 'YES'
        GLO:Select4 = 'YES'
        GLO:Select3 = 'YES'
        GLO:Select2 = 'YES'
      END
      IF ?All_temp{Prop:Checked} = False
        GLO:Select7 = 'NO'
        GLO:Select6 = 'NO'
        GLO:Select5 = 'NO'
        GLO:Select4 = 'NO'
        GLO:Select3 = 'NO'
        GLO:Select2 = 'NO'
      END
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If glo:select1 = ''
          Select(?glo:select1)
      Else!If glo:select1 = ''
          exchange_unit_audit_report
      End!If glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
