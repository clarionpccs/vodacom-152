

   MEMBER('cellular.clw')                             ! This is a MEMBER module


!------------ File: WAYBCONF, version 1 ----------------------
!------------ modified 11.03.2015 at 09:18:41 -----------------
MOD:stFileName:WAYBCONFVer1  STRING(260)
WAYBCONFVer1           FILE,DRIVER('Btrieve'),OEM,PRE(WAYBCONFVer1),NAME(MOD:stFileName:WAYBCONFVer1)
KeyRecordNo              KEY(WAYBCONFVer1:RecordNo),NOCASE,PRIMARY
KeyWaybillNo             KEY(WAYBCONFVer1:WaybillNo),DUP,NOCASE
KeyGenerateDateTime      KEY(WAYBCONFVer1:GenerateDate,WAYBCONFVer1:GenerateTime),DUP,NOCASE
KeyConfirmDateTime       KEY(WAYBCONFVer1:ConfirmationSent,WAYBCONFVer1:ConfirmDate,WAYBCONFVer1:ConfirmTime),DUP,NOCASE
KeyDeliveredDateTime     KEY(WAYBCONFVer1:Delivered,WAYBCONFVer1:DeliverDate,WAYBCONFVer1:DeliverTime),DUP,NOCASE
KeyReceivedDateTime      KEY(WAYBCONFVer1:Received,WAYBCONFVer1:ReceiveDate,WAYBCONFVer1:ReceiveTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
WaybillNo                   LONG
GenerateDate                DATE
GenerateTime                TIME
ConfirmationSent            BYTE
ConfirmDate                 DATE
ConfirmTime                 TIME
ConfirmResentQty            LONG
Delivered                   BYTE
DeliverDate                 DATE
DeliverTime                 TIME
Received                    BYTE
ReceiveDate                 DATE
ReceiveTime                 TIME
                         END
                       END
!------------ End File: WAYBCONF, version 1 ------------------

!------------ File: SOAPERRS, version 1 ----------------------
!------------ modified 20.10.2014 at 14:59:47 -----------------
MOD:stFileName:SOAPERRSVer1  STRING(260)
SOAPERRSVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SOAPERRSVer1),PRE(SOAPERRSVer1)
KeyRecordNumber          KEY(SOAPERRSVer1:RecordNumber),NOCASE,PRIMARY
KeyErrorCode             KEY(SOAPERRSVer1:ErrorCode),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ErrorCode                   STRING(20)
SoapFault                   STRING(50)
ErrorType                   STRING(20)
Description                 STRING(100)
Action                      STRING(15)
                         END
                       END
!------------ End File: SOAPERRS, version 1 ------------------

!------------ File: JOBSE3, version 1 ----------------------
!------------ modified 20.10.2014 at 14:59:47 -----------------
MOD:stFileName:JOBSE3Ver1  STRING(260)
JOBSE3Ver1             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSE3Ver1),PRE(JOBSE3Ver1)
KeyRecordNumber          KEY(JOBSE3Ver1:RecordNumber),NOCASE,PRIMARY
KeyRefNumber             KEY(JOBSE3Ver1:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
LoyaltyStatus               STRING(250)
                         END
                       END
!------------ End File: JOBSE3, version 1 ------------------

!------------ File: JOBSE3, version 2 ----------------------
!------------ modified 09.12.2014 at 14:23:05 -----------------
MOD:stFileName:JOBSE3Ver2  STRING(260)
JOBSE3Ver2             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSE3Ver2),PRE(JOBSE3Ver2),CREATE
KeyRecordNumber          KEY(JOBSE3Ver2:RecordNumber),NOCASE,PRIMARY
KeyRefNumber             KEY(JOBSE3Ver2:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
LoyaltyStatus               STRING(250)
Exchange_IntLocation        STRING(30)
                         END
                       END
!------------ End File: JOBSE3, version 2 ------------------

!------------ File: STOAUUSE, version 1 ----------------------
!------------ modified 03.10.2012 at 15:54:20 -----------------
MOD:stFileName:STOAUUSEVer1  STRING(260)
STOAUUSEVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STOAUUSEVer1),PRE(STOAUUSEVer1)
KeyRecordNo              KEY(STOAUUSEVer1:Record_No),NOCASE,PRIMARY
KeyAuditTypeNo           KEY(STOAUUSEVer1:AuditType,STOAUUSEVer1:Audit_No),DUP,NOCASE
KeyAuditTypeNoUser       KEY(STOAUUSEVer1:AuditType,STOAUUSEVer1:Audit_No,STOAUUSEVer1:User_code),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
AuditType                   STRING(1)
Audit_No                    LONG
User_code                   STRING(3)
Current                     STRING(1)
                         END
                       END
!------------ End File: STOAUUSE, version 1 ------------------

!------------ File: PREJOB, version 1 ----------------------
!------------ modified 07.08.2012 at 16:06:24 -----------------
MOD:stFileName:PREJOBVer1  STRING(260)
PREJOBVer1             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PREJOBVer1),PRE(PREJOBVer1)
KeyRefNumber             KEY(PREJOBVer1:RefNumber),NOCASE,PRIMARY
KeyAPIRecordNo           KEY(PREJOBVer1:APIRecordNumber),DUP,NOCASE
KeyJobRef_Number         KEY(PREJOBVer1:JobRef_number),DUP,NOCASE
KeyESN                   KEY(PREJOBVer1:ESN),DUP,NOCASE
KeyDateTime              KEY(PREJOBVer1:Date_booked,PREJOBVer1:Time_Booked),DUP,NOCASE
KeyESNDateTime           KEY(PREJOBVer1:ESN,PREJOBVer1:Date_booked,PREJOBVer1:Time_Booked),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
APIRecordNumber             LONG
JobRef_number               LONG
VodacomPortalRef            STRING(30)
Title                       STRING(5)
Initial                     STRING(1)
Surname                     STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Suburb                      STRING(30)
Region                      STRING(30)
Telephone_number            STRING(15)
Mobile_number               STRING(15)
ID_number                   STRING(30)
ESN                         STRING(20)
Manufacturer                STRING(30)
Model_number                STRING(30)
DOP                         DATE
FaultText                   STRING(200)
RepairOutletRef             LONG
Date_booked                 DATE
Time_Booked                 TIME
                         END
                       END
!------------ End File: PREJOB, version 1 ------------------

!------------ File: PREJOB, version 2 ----------------------
!------------ modified 09.08.2012 at 11:50:09 -----------------
MOD:stFileName:PREJOBVer2  STRING(260)
PREJOBVer2             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PREJOBVer2),PRE(PREJOBVer2),CREATE
KeyRefNumber             KEY(PREJOBVer2:RefNumber),NOCASE,PRIMARY
KeyAPIRecordNo           KEY(PREJOBVer2:APIRecordNumber),DUP,NOCASE
KeyJobRef_Number         KEY(PREJOBVer2:JobRef_number),DUP,NOCASE
KeyESN                   KEY(PREJOBVer2:ESN),DUP,NOCASE
KeyDateTime              KEY(PREJOBVer2:Date_booked,PREJOBVer2:Time_Booked),DUP,NOCASE
KeyESNDateTime           KEY(PREJOBVer2:ESN,PREJOBVer2:Date_booked,PREJOBVer2:Time_Booked),DUP,NOCASE
KeyPortalRef             KEY(PREJOBVer2:VodacomPortalRef),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
APIRecordNumber             LONG
JobRef_number               LONG
VodacomPortalRef            STRING(30)
Title                       STRING(5)
Initial                     STRING(1)
Surname                     STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Suburb                      STRING(30)
Region                      STRING(30)
Telephone_number            STRING(15)
Mobile_number               STRING(15)
ID_number                   STRING(30)
ESN                         STRING(20)
Manufacturer                STRING(30)
Model_number                STRING(30)
DOP                         DATE
FaultText                   STRING(200)
RepairOutletRef             LONG
Date_booked                 DATE
Time_Booked                 TIME
                         END
                       END
!------------ End File: PREJOB, version 2 ------------------

!------------ File: TRADEAC2, version 1 ----------------------
!------------ modified 21.05.2012 at 14:53:40 -----------------
MOD:stFileName:TRADEAC2Ver1  STRING(260)
TRADEAC2Ver1           FILE,DRIVER('Btrieve'),OEM,PRE(TRADEAC2Ver1),NAME(MOD:stFileName:TRADEAC2Ver1)
KeyRecordNo              KEY(TRADEAC2Ver1:RecordNo),NOCASE,PRIMARY
KeyAccountNumber         KEY(TRADEAC2Ver1:Account_Number),NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Account_Number              STRING(15)
coSMSname                   STRING(40)
SMS_Email2                  STRING(100)
SMS_Email3                  STRING(100)
SMS_ReportEmail             STRING(255)
UseSparesRequest            STRING(3)
                         END
                       END
!------------ End File: TRADEAC2, version 1 ------------------

!------------ File: TRADEAC2, version 2 ----------------------
!------------ modified 09.08.2012 at 11:18:48 -----------------
MOD:stFileName:TRADEAC2Ver2  STRING(260)
TRADEAC2Ver2           FILE,DRIVER('Btrieve'),OEM,PRE(TRADEAC2Ver2),NAME(MOD:stFileName:TRADEAC2Ver2),CREATE
KeyRecordNo              KEY(TRADEAC2Ver2:RecordNo),NOCASE,PRIMARY
KeyAccountNumber         KEY(TRADEAC2Ver2:Account_Number),NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Account_Number              STRING(15)
coSMSname                   STRING(40)
SMS_Email2                  STRING(100)
SMS_Email3                  STRING(100)
SMS_ReportEmail             STRING(255)
UseSparesRequest            STRING(3)
Pre_Book_Region             STRING(30)
                         END
                       END
!------------ End File: TRADEAC2, version 2 ------------------

!------------ File: TRADEAC2, version 3 ----------------------
!------------ modified 05.09.2012 at 15:52:41 -----------------
MOD:stFileName:TRADEAC2Ver3  STRING(260)
TRADEAC2Ver3           FILE,DRIVER('Btrieve'),OEM,PRE(TRADEAC2Ver3),NAME(MOD:stFileName:TRADEAC2Ver3),CREATE
KeyRecordNo              KEY(TRADEAC2Ver3:RecordNo),NOCASE,PRIMARY
KeyAccountNumber         KEY(TRADEAC2Ver3:Account_Number),NOCASE
KeyPreBookRegion         KEY(TRADEAC2Ver3:Pre_Book_Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Account_Number              STRING(15)
coSMSname                   STRING(40)
SMS_Email2                  STRING(100)
SMS_Email3                  STRING(100)
SMS_ReportEmail             STRING(255)
UseSparesRequest            STRING(3)
Pre_Book_Region             STRING(30)
                         END
                       END
!------------ End File: TRADEAC2, version 3 ------------------

!------------ File: SMSRECVD, version 1 ----------------------
!------------ modified 05.03.2012 at 11:10:51 -----------------
MOD:stFileName:SMSRECVDVer1  STRING(260)
SMSRECVDVer1        FILE,DRIVER('Btrieve'),OEM,PRE(SMSRECVDVer1),NAME(MOD:stFileName:SMSRECVDVer1)
KeyRecordNo              KEY(SMSRECVDVer1:RecordNo),NOCASE,PRIMARY
KeyJob_Ref_Number        KEY(SMSRECVDVer1:Job_Ref_Number),DUP,NOCASE
KeyJob_Date_Time_Dec     KEY(SMSRECVDVer1:Job_Ref_Number,-SMSRECVDVer1:DateReceived,-SMSRECVDVer1:TimeReceived),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Job_Ref_Number              LONG
MSISDN                      STRING(15)
DateReceived                DATE
TimeReceived                TIME
TextReceived                STRING(200)
                         END
                       END
!------------ End File: SMSRECVD, version 1 ------------------

!------------ File: SMSRECVD, version 2 ----------------------
!------------ modified 05.03.2012 at 12:07:02 -----------------
MOD:stFileName:SMSRECVDVer2  STRING(260)
SMSRECVDVer2        FILE,DRIVER('Btrieve'),OEM,PRE(SMSRECVDVer2),NAME(MOD:stFileName:SMSRECVDVer2),CREATE
KeyRecordNo              KEY(SMSRECVDVer2:RecordNo),NOCASE,PRIMARY
KeyJob_Ref_Number        KEY(SMSRECVDVer2:Job_Ref_Number),DUP,NOCASE
KeyJob_Date_Time_Dec     KEY(SMSRECVDVer2:Job_Ref_Number,-SMSRECVDVer2:DateReceived,-SMSRECVDVer2:TimeReceived),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Job_Ref_Number              LONG
MSISDN                      STRING(15)
DateReceived                DATE
TimeReceived                TIME
TextReceived                STRING(200)
SMSType                     STRING(1)
                         END
                       END
!------------ End File: SMSRECVD, version 2 ------------------

!------------ File: SMSRECVD, version 3 ----------------------
!------------ modified 14.03.2012 at 12:05:16 -----------------
MOD:stFileName:SMSRECVDVer3  STRING(260)
SMSRECVDVer3        FILE,DRIVER('Btrieve'),OEM,PRE(SMSRECVDVer3),NAME(MOD:stFileName:SMSRECVDVer3),CREATE
KeyRecordNo              KEY(SMSRECVDVer3:RecordNo),NOCASE,PRIMARY
KeyJob_Ref_Number        KEY(SMSRECVDVer3:Job_Ref_Number),DUP,NOCASE
KeyJob_Date_Time_Dec     KEY(SMSRECVDVer3:Job_Ref_Number,-SMSRECVDVer3:DateReceived,-SMSRECVDVer3:TimeReceived),DUP,NOCASE
KeyAccount_Date_Time     KEY(SMSRECVDVer3:AccountNumber,SMSRECVDVer3:DateReceived,SMSRECVDVer3:TextReceived),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Job_Ref_Number              LONG
AccountNumber               STRING(15)
MSISDN                      STRING(15)
DateReceived                DATE
TimeReceived                TIME
TextReceived                STRING(200)
SMSType                     STRING(1)
                         END
                       END
!------------ End File: SMSRECVD, version 3 ------------------

!------------ File: SMSRECVD, version 4 ----------------------
!------------ modified 15.03.2012 at 14:22:45 -----------------
MOD:stFileName:SMSRECVDVer4  STRING(260)
SMSRECVDVer4           FILE,DRIVER('Btrieve'),OEM,PRE(SMSRECVDVer4),NAME(MOD:stFileName:SMSRECVDVer4),CREATE
KeyRecordNo              KEY(SMSRECVDVer4:RecordNo),NOCASE,PRIMARY
KeyJob_Ref_Number        KEY(SMSRECVDVer4:Job_Ref_Number),DUP,NOCASE
KeyJob_Date_Time_Dec     KEY(SMSRECVDVer4:Job_Ref_Number,-SMSRECVDVer4:DateReceived,-SMSRECVDVer4:TimeReceived),DUP,NOCASE
KeyAccount_Date_Time     KEY(SMSRECVDVer4:AccountNumber,SMSRECVDVer4:DateReceived,SMSRECVDVer4:TextReceived),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Job_Ref_Number              LONG
AccountNumber               STRING(15)
MSISDN                      STRING(15)
DateReceived                DATE
TimeReceived                TIME
TextReceived                STRING(200)
SMSType                     STRING(1)
                         END
                       END
!------------ End File: SMSRECVD, version 4 ------------------

!------------ File: GRNOTES, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:GRNOTESVer1  STRING(260)
GRNOTESVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:GRNOTESVer1),PRE(GRNOTESVer1)
Goods_Received_Number_Key KEY(GRNOTESVer1:Goods_Received_Number),NOCASE,PRIMARY
Order_Number_Key         KEY(GRNOTESVer1:Order_Number,GRNOTESVer1:Goods_Received_Date),DUP,NOCASE
Date_Recd_Key            KEY(GRNOTESVer1:Goods_Received_Date),DUP,NOCASE
NotPrintedGRNKey         KEY(GRNOTESVer1:BatchRunNotPrinted,GRNOTESVer1:Goods_Received_Number),DUP,NOCASE
NotPrintedOrderKey       KEY(GRNOTESVer1:BatchRunNotPrinted,GRNOTESVer1:Order_Number,GRNOTESVer1:Goods_Received_Number),DUP,NOCASE
Record                   RECORD,PRE()
Goods_Received_Number       LONG
Order_Number                LONG
Goods_Received_Date         DATE
CurrencyCode                STRING(30)
DailyRate                   REAL
DivideMultiply              STRING(1)
BatchRunNotPrinted          BYTE
                         END
                       END
!------------ End File: GRNOTES, version 1 ------------------

!------------ File: GRNOTES, version 2 ----------------------
!------------ modified 17.02.2012 at 14:37:53 -----------------
MOD:stFileName:GRNOTESVer2  STRING(260)
GRNOTESVer2            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:GRNOTESVer2),PRE(GRNOTESVer2),CREATE
Goods_Received_Number_Key KEY(GRNOTESVer2:Goods_Received_Number),NOCASE,PRIMARY
Order_Number_Key         KEY(GRNOTESVer2:Order_Number,GRNOTESVer2:Goods_Received_Date),DUP,NOCASE
Date_Recd_Key            KEY(GRNOTESVer2:Goods_Received_Date),DUP,NOCASE
NotPrintedGRNKey         KEY(GRNOTESVer2:BatchRunNotPrinted,GRNOTESVer2:Goods_Received_Number),DUP,NOCASE
NotPrintedOrderKey       KEY(GRNOTESVer2:BatchRunNotPrinted,GRNOTESVer2:Order_Number,GRNOTESVer2:Goods_Received_Number),DUP,NOCASE
Record                   RECORD,PRE()
Goods_Received_Number       LONG
Order_Number                LONG
Goods_Received_Date         DATE
CurrencyCode                STRING(30)
DailyRate                   REAL
DivideMultiply              STRING(1)
BatchRunNotPrinted          BYTE
Uncaptured                  BYTE
                         END
                       END
!------------ End File: GRNOTES, version 2 ------------------

!------------ File: EXCHAUI, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:EXCHAUIVer1  STRING(260)
EXCHAUIVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:EXCHAUIVer1),PRE(EXCHAUIVer1)
Internal_No_Key          KEY(EXCHAUIVer1:Internal_No),NOCASE,PRIMARY
Audit_Number_Key         KEY(EXCHAUIVer1:Audit_Number),DUP,NOCASE
Ref_Number_Key           KEY(EXCHAUIVer1:Ref_Number),DUP,NOCASE
Exch_Browse_Key          KEY(EXCHAUIVer1:Audit_Number,EXCHAUIVer1:Exists),DUP,NOCASE
AuditIMEIKey             KEY(EXCHAUIVer1:Audit_Number,EXCHAUIVer1:New_IMEI),DUP,NOCASE
Locate_IMEI_Key          KEY(EXCHAUIVer1:Audit_Number,EXCHAUIVer1:Site_Location,EXCHAUIVer1:IMEI_Number),DUP,NOCASE,OPT
Main_Browse_Key          KEY(EXCHAUIVer1:Audit_Number,EXCHAUIVer1:Confirmed,EXCHAUIVer1:Site_Location,EXCHAUIVer1:Stock_Type,EXCHAUIVer1:Shelf_Location,EXCHAUIVer1:Internal_No),DUP,NOCASE
Record                   RECORD,PRE()
Internal_No                 LONG
Site_Location               STRING(30)
Shelf_Location              STRING(30)
Location                    STRING(30)
Audit_Number                LONG
Ref_Number                  LONG
Exists                      BYTE
New_IMEI                    BYTE
IMEI_Number                 STRING(20)
Confirmed                   BYTE
Stock_Type                  STRING(30)
                         END
                       END
!------------ End File: EXCHAUI, version 1 ------------------

!------------ File: RTNAWAIT, version 1 ----------------------
!------------ modified 19.09.2011 at 12:00:23 -----------------
MOD:stFileName:RTNAWAITVer1  STRING(260)
RTNAWAITVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNAWAITVer1),PRE(RTNAWAITVer1)
RecordNumberKey          KEY(RTNAWAITVer1:RecordNumber),NOCASE,PRIMARY
ArcProPartKey            KEY(RTNAWAITVer1:Archive,RTNAWAITVer1:Processed,RTNAWAITVer1:PartNumber),DUP,NOCASE
ArcProDescKey            KEY(RTNAWAITVer1:Archive,RTNAWAITVer1:Processed,RTNAWAITVer1:Description),DUP,NOCASE
ArcProExcPartKey         KEY(RTNAWAITVer1:Archive,RTNAWAITVer1:Processed,RTNAWAITVer1:ExchangeUnit,RTNAWAITVer1:PartNumber),DUP,NOCASE
ArcProExcDescKey         KEY(RTNAWAITVer1:Archive,RTNAWAITVer1:Processed,RTNAWAITVer1:ExchangeUnit,RTNAWAITVer1:Description),DUP,NOCASE
RTNORDERKey              KEY(RTNAWAITVer1:RTNRecordNumber),DUP,NOCASE
CREDNOTRKey              KEY(RTNAWAITVer1:CNRRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archive                     BYTE
DateCreated                 DATE
TimeCreated                 TIME
RTNRecordNumber             LONG
CNRRecordNumber             LONG
RefNumber                   LONG
ExchangeUnit                BYTE
PartNumber                  STRING(30)
Description                 STRING(30)
Processed                   BYTE
DateProcessed               DATE
TimeProcessed               TIME
UsercodeProcessed           STRING(3)
AcceptReject                STRING(6)
                         END
                       END
!------------ End File: RTNAWAIT, version 1 ------------------

!------------ File: RTNAWAIT, version 2 ----------------------
!------------ modified 19.09.2011 at 12:13:02 -----------------
MOD:stFileName:RTNAWAITVer2  STRING(260)
RTNAWAITVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNAWAITVer2),PRE(RTNAWAITVer2),CREATE
RecordNumberKey          KEY(RTNAWAITVer2:RecordNumber),NOCASE,PRIMARY
ArcProPartKey            KEY(RTNAWAITVer2:Archive,RTNAWAITVer2:Processed,RTNAWAITVer2:PartNumber),DUP,NOCASE
ArcProDescKey            KEY(RTNAWAITVer2:Archive,RTNAWAITVer2:Processed,RTNAWAITVer2:Description),DUP,NOCASE
ArcProExcPartKey         KEY(RTNAWAITVer2:Archive,RTNAWAITVer2:Processed,RTNAWAITVer2:ExchangeUnit,RTNAWAITVer2:PartNumber),DUP,NOCASE
ArcProExcDescKey         KEY(RTNAWAITVer2:Archive,RTNAWAITVer2:Processed,RTNAWAITVer2:ExchangeUnit,RTNAWAITVer2:Description),DUP,NOCASE
RTNORDERKey              KEY(RTNAWAITVer2:RTNRecordNumber),DUP,NOCASE
CREDNOTRKey              KEY(RTNAWAITVer2:CNRRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archive                     BYTE
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
RTNRecordNumber             LONG
CNRRecordNumber             LONG
RefNumber                   LONG
ExchangeUnit                BYTE
PartNumber                  STRING(30)
Description                 STRING(30)
Processed                   BYTE
DateProcessed               DATE
TimeProcessed               TIME
UsercodeProcessed           STRING(3)
AcceptReject                STRING(6)
                         END
                       END
!------------ End File: RTNAWAIT, version 2 ------------------

!------------ File: RTNAWAIT, version 3 ----------------------
!------------ modified 22.09.2011 at 14:48:41 -----------------
MOD:stFileName:RTNAWAITVer3  STRING(260)
RTNAWAITVer3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNAWAITVer3),PRE(RTNAWAITVer3),CREATE
RecordNumberKey          KEY(RTNAWAITVer3:RecordNumber),NOCASE,PRIMARY
ArcProPartKey            KEY(RTNAWAITVer3:Archive,RTNAWAITVer3:Processed,RTNAWAITVer3:PartNumber),DUP,NOCASE
ArcProDescKey            KEY(RTNAWAITVer3:Archive,RTNAWAITVer3:Processed,RTNAWAITVer3:Description),DUP,NOCASE
ArcProExcPartKey         KEY(RTNAWAITVer3:Archive,RTNAWAITVer3:Processed,RTNAWAITVer3:ExchangeOrder,RTNAWAITVer3:PartNumber),DUP,NOCASE
ArcProExcDescKey         KEY(RTNAWAITVer3:Archive,RTNAWAITVer3:Processed,RTNAWAITVer3:ExchangeOrder,RTNAWAITVer3:Description),DUP,NOCASE
RTNORDERKey              KEY(RTNAWAITVer3:RTNRecordNumber),DUP,NOCASE
CREDNOTRKey              KEY(RTNAWAITVer3:CNRRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archive                     BYTE
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
RTNRecordNumber             LONG
CNRRecordNumber             LONG
RefNumber                   LONG
ExchangeOrder               BYTE
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
Processed                   BYTE
DateProcessed               DATE
TimeProcessed               TIME
UsercodeProcessed           STRING(3)
AcceptReject                STRING(6)
                         END
                       END
!------------ End File: RTNAWAIT, version 3 ------------------

!------------ File: RTNAWAIT, version 4 ----------------------
!------------ modified 23.09.2011 at 10:55:29 -----------------
MOD:stFileName:RTNAWAITVer4  STRING(260)
RTNAWAITVer4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNAWAITVer4),PRE(RTNAWAITVer4),CREATE
RecordNumberKey          KEY(RTNAWAITVer4:RecordNumber),NOCASE,PRIMARY
ArcProPartKey            KEY(RTNAWAITVer4:Archive,RTNAWAITVer4:Processed,RTNAWAITVer4:PartNumber),DUP,NOCASE
ArcProDescKey            KEY(RTNAWAITVer4:Archive,RTNAWAITVer4:Processed,RTNAWAITVer4:Description),DUP,NOCASE
ArcProExcPartKey         KEY(RTNAWAITVer4:Archive,RTNAWAITVer4:Processed,RTNAWAITVer4:ExchangeOrder,RTNAWAITVer4:PartNumber),DUP,NOCASE
ArcProExcDescKey         KEY(RTNAWAITVer4:Archive,RTNAWAITVer4:Processed,RTNAWAITVer4:ExchangeOrder,RTNAWAITVer4:Description),DUP,NOCASE
RTNORDERKey              KEY(RTNAWAITVer4:RTNRecordNumber),DUP,NOCASE
CREDNOTRKey              KEY(RTNAWAITVer4:CNRRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archive                     BYTE
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
RTNRecordNumber             LONG
CNRRecordNumber             LONG
RefNumber                   LONG
ExchangeOrder               BYTE
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
Processed                   BYTE
DateProcessed               DATE
TimeProcessed               TIME
UsercodeProcessed           STRING(3)
AcceptReject                STRING(6)
NewRefNumber                LONG
                         END
                       END
!------------ End File: RTNAWAIT, version 4 ------------------

!------------ File: RTNAWAIT, version 5 ----------------------
!------------ modified 23.09.2011 at 11:13:03 -----------------
MOD:stFileName:RTNAWAITVer5  STRING(260)
RTNAWAITVer5           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNAWAITVer5),PRE(RTNAWAITVer5),CREATE
RecordNumberKey          KEY(RTNAWAITVer5:RecordNumber),NOCASE,PRIMARY
ArcProPartKey            KEY(RTNAWAITVer5:Archive,RTNAWAITVer5:Processed,RTNAWAITVer5:PartNumber),DUP,NOCASE
ArcProDescKey            KEY(RTNAWAITVer5:Archive,RTNAWAITVer5:Processed,RTNAWAITVer5:Description),DUP,NOCASE
ArcProExcPartKey         KEY(RTNAWAITVer5:Archive,RTNAWAITVer5:Processed,RTNAWAITVer5:ExchangeOrder,RTNAWAITVer5:PartNumber),DUP,NOCASE
ArcProExcDescKey         KEY(RTNAWAITVer5:Archive,RTNAWAITVer5:Processed,RTNAWAITVer5:ExchangeOrder,RTNAWAITVer5:Description),DUP,NOCASE
ArcProCNRKey             KEY(RTNAWAITVer5:Archive,RTNAWAITVer5:Processed,RTNAWAITVer5:CNRRecordNumber),DUP,NOCASE
ArcProExcCNRKey          KEY(RTNAWAITVer5:Archive,RTNAWAITVer5:Processed,RTNAWAITVer5:ExchangeOrder,RTNAWAITVer5:CNRRecordNumber),DUP,NOCASE
RTNORDERKey              KEY(RTNAWAITVer5:RTNRecordNumber),DUP,NOCASE
CREDNOTRKey              KEY(RTNAWAITVer5:CNRRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archive                     BYTE
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
RTNRecordNumber             LONG
CNRRecordNumber             LONG
RefNumber                   LONG
ExchangeOrder               BYTE
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
Processed                   BYTE
DateProcessed               DATE
TimeProcessed               TIME
UsercodeProcessed           STRING(3)
AcceptReject                STRING(6)
NewRefNumber                LONG
                         END
                       END
!------------ End File: RTNAWAIT, version 5 ------------------

!------------ File: RTNAWAIT, version 6 ----------------------
!------------ modified 23.09.2011 at 11:35:31 -----------------
MOD:stFileName:RTNAWAITVer6  STRING(260)
RTNAWAITVer6           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNAWAITVer6),PRE(RTNAWAITVer6),CREATE
RecordNumberKey          KEY(RTNAWAITVer6:RecordNumber),NOCASE,PRIMARY
ArcProPartKey            KEY(RTNAWAITVer6:Archive,RTNAWAITVer6:Processed,RTNAWAITVer6:PartNumber),DUP,NOCASE
ArcProDescKey            KEY(RTNAWAITVer6:Archive,RTNAWAITVer6:Processed,RTNAWAITVer6:Description),DUP,NOCASE
ArcProExcPartKey         KEY(RTNAWAITVer6:Archive,RTNAWAITVer6:Processed,RTNAWAITVer6:ExchangeOrder,RTNAWAITVer6:PartNumber),DUP,NOCASE
ArcProExcDescKey         KEY(RTNAWAITVer6:Archive,RTNAWAITVer6:Processed,RTNAWAITVer6:ExchangeOrder,RTNAWAITVer6:Description),DUP,NOCASE
ArcProCNRKey             KEY(RTNAWAITVer6:Archive,RTNAWAITVer6:Processed,RTNAWAITVer6:CNRRecordNumber),DUP,NOCASE
ArcProExcCNRKey          KEY(RTNAWAITVer6:Archive,RTNAWAITVer6:Processed,RTNAWAITVer6:ExchangeOrder,RTNAWAITVer6:CNRRecordNumber),DUP,NOCASE
RTNORDERKey              KEY(RTNAWAITVer6:RTNRecordNumber),DUP,NOCASE
CREDNOTRKey              KEY(RTNAWAITVer6:CNRRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archive                     BYTE
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
Location                    STRING(30)
RTNRecordNumber             LONG
CNRRecordNumber             LONG
RefNumber                   LONG
ExchangeOrder               BYTE
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
Processed                   BYTE
DateProcessed               DATE
TimeProcessed               TIME
UsercodeProcessed           STRING(3)
AcceptReject                STRING(6)
NewRefNumber                LONG
                         END
                       END
!------------ End File: RTNAWAIT, version 6 ------------------

!------------ File: STMASAUD, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:STMASAUDVer1  STRING(260)
STMASAUDVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STMASAUDVer1),PRE(STMASAUDVer1)
AutoIncrement_Key        KEY(-STMASAUDVer1:Audit_No),NOCASE,PRIMARY
Compeleted_Key           KEY(STMASAUDVer1:Complete),DUP,NOCASE
Sent_Key                 KEY(STMASAUDVer1:Send),DUP,NOCASE
Record                   RECORD,PRE()
Audit_No                    LONG
branch                      STRING(30)
branch_id                   LONG
Audit_Date                  LONG
Audit_Time                  LONG
End_Date                    LONG
End_Time                    LONG
Audit_User                  STRING(3)
Complete                    STRING(1)
Send                        STRING(1)
                         END
                       END
!------------ End File: STMASAUD, version 1 ------------------

!------------ File: ORDITEMS, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:ORDITEMSVer1  STRING(260)
ORDITEMSVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:ORDITEMSVer1),PRE(ORDITEMSVer1)
Keyordhno                KEY(ORDITEMSVer1:ordhno),DUP,NOCASE
recordnumberkey          KEY(ORDITEMSVer1:recordnumber),NOCASE,PRIMARY
OrdHNoPartKey            KEY(ORDITEMSVer1:ordhno,ORDITEMSVer1:partno),DUP,NOCASE
PartNoKey                KEY(ORDITEMSVer1:partno),DUP,NOCASE
Record                   RECORD,PRE()
recordnumber                LONG
ordhno                      LONG
manufact                    STRING(40)
partno                      STRING(20)
partdiscription             STRING(60)
qty                         LONG
itemcost                    REAL
totalcost                   REAL
                         END
                       END
!------------ End File: ORDITEMS, version 1 ------------------

!------------ File: LOCATLOG, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:LOCATLOGVer1  STRING(260)
LOCATLOGVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:LOCATLOGVer1),PRE(LOCATLOGVer1)
RecordNumberKey          KEY(LOCATLOGVer1:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(LOCATLOGVer1:RefNumber,LOCATLOGVer1:TheDate),DUP,NOCASE
NewLocationKey           KEY(LOCATLOGVer1:RefNumber,LOCATLOGVer1:NewLocation),DUP,NOCASE
DateNewLocationKey       KEY(LOCATLOGVer1:NewLocation,LOCATLOGVer1:TheDate,LOCATLOGVer1:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TheDate                     DATE
TheTime                     TIME
UserCode                    STRING(30)
PreviousLocation            STRING(30)
NewLocation                 STRING(30)
                         END
                       END
!------------ End File: LOCATLOG, version 1 ------------------

!------------ File: DEFAULT2, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:DEFAULT2Ver1  STRING(260)
DEFAULT2Ver1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFAULT2Ver1),PRE(DEFAULT2Ver1)
RecordNumberKey          KEY(DEFAULT2Ver1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
UserSkillLevel              BYTE
Inv2CompanyName             STRING(30)
Inv2AddressLine1            STRING(30)
Inv2AddressLine2            STRING(30)
Inv2AddressLine3            STRING(30)
Inv2Postcode                STRING(15)
Inv2TelephoneNumber         STRING(15)
Inv2FaxNumber               STRING(15)
Inv2EmailAddress            STRING(255)
Inv2VATNumber               STRING(30)
CurrencySymbol              STRING(3)
UseRequisitionNumber        BYTE
PickEngStockOnly            BYTE
AllocateEngPassword         BYTE
PrintRetainedAccLabel       BYTE
OverdueBackOrderText        STRING(255)
EmailUserName               STRING(255)
EmailPassword               STRING(60)
                         END
                       END
!------------ End File: DEFAULT2, version 1 ------------------

!------------ File: DEFAULT2, version 2 ----------------------
!------------ modified 13.04.2011 at 10:24:32 -----------------
MOD:stFileName:DEFAULT2Ver2  STRING(260)
DEFAULT2Ver2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFAULT2Ver2),PRE(DEFAULT2Ver2),CREATE
RecordNumberKey          KEY(DEFAULT2Ver2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
UserSkillLevel              BYTE
Inv2CompanyName             STRING(30)
Inv2AddressLine1            STRING(30)
Inv2AddressLine2            STRING(30)
Inv2AddressLine3            STRING(30)
Inv2Postcode                STRING(15)
Inv2TelephoneNumber         STRING(15)
Inv2FaxNumber               STRING(15)
Inv2EmailAddress            STRING(255)
Inv2VATNumber               STRING(30)
CurrencySymbol              STRING(3)
UseRequisitionNumber        BYTE
PickEngStockOnly            BYTE
AllocateEngPassword         BYTE
PrintRetainedAccLabel       BYTE
OverdueBackOrderText        STRING(255)
EmailUserName               STRING(255)
EmailPassword               STRING(60)
GlobalPrintText             STRING(255)
                         END
                       END
!------------ End File: DEFAULT2, version 2 ------------------

!------------ File: DEFAULT2, version 3 ----------------------
!------------ modified 17.04.2012 at 16:19:04 -----------------
MOD:stFileName:DEFAULT2Ver3  STRING(260)
DEFAULT2Ver3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFAULT2Ver3),PRE(DEFAULT2Ver3),CREATE
RecordNumberKey          KEY(DEFAULT2Ver3:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
UserSkillLevel              BYTE
Inv2CompanyName             STRING(30)
Inv2AddressLine1            STRING(30)
Inv2AddressLine2            STRING(30)
Inv2AddressLine3            STRING(30)
Inv2Postcode                STRING(15)
Inv2TelephoneNumber         STRING(15)
Inv2FaxNumber               STRING(15)
Inv2EmailAddress            STRING(255)
Inv2VATNumber               STRING(30)
CurrencySymbol              STRING(3)
UseRequisitionNumber        BYTE
PickEngStockOnly            BYTE
AllocateEngPassword         BYTE
PrintRetainedAccLabel       BYTE
OverdueBackOrderText        STRING(255)
EmailUserName               STRING(255)
EmailPassword               STRING(60)
GlobalPrintText             STRING(255)
DefaultFromEmail            STRING(100)
                         END
                       END
!------------ End File: DEFAULT2, version 3 ------------------

!------------ File: DEFAULT2, version 4 ----------------------
!------------ modified 02.07.2012 at 09:42:29 -----------------
MOD:stFileName:DEFAULT2Ver4  STRING(260)
DEFAULT2Ver4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFAULT2Ver4),PRE(DEFAULT2Ver4),CREATE
RecordNumberKey          KEY(DEFAULT2Ver4:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
UserSkillLevel              BYTE
Inv2CompanyName             STRING(30)
Inv2AddressLine1            STRING(30)
Inv2AddressLine2            STRING(30)
Inv2AddressLine3            STRING(30)
Inv2Postcode                STRING(15)
Inv2TelephoneNumber         STRING(15)
Inv2FaxNumber               STRING(15)
Inv2EmailAddress            STRING(255)
Inv2VATNumber               STRING(30)
CurrencySymbol              STRING(3)
UseRequisitionNumber        BYTE
PickEngStockOnly            BYTE
AllocateEngPassword         BYTE
PrintRetainedAccLabel       BYTE
OverdueBackOrderText        STRING(255)
EmailUserName               STRING(255)
EmailPassword               STRING(60)
GlobalPrintText             STRING(255)
DefaultFromEmail            STRING(100)
PLE                         DATE
                         END
                       END
!------------ End File: DEFAULT2, version 4 ------------------

!------------ File: RETSALES, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:RETSALESVer1  STRING(260)
RETSALESVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RETSALESVer1),PRE(RETSALESVer1)
Ref_Number_Key           KEY(RETSALESVer1:Ref_Number),NOCASE,PRIMARY
Invoice_Number_Key       KEY(RETSALESVer1:Invoice_Number),DUP,NOCASE
Despatched_Purchase_Key  KEY(RETSALESVer1:Despatched,RETSALESVer1:Purchase_Order_Number),DUP,NOCASE
Despatched_Ref_Key       KEY(RETSALESVer1:Despatched,RETSALESVer1:Ref_Number),DUP,NOCASE
Despatched_Account_Key   KEY(RETSALESVer1:Despatched,RETSALESVer1:Account_Number,RETSALESVer1:Purchase_Order_Number),DUP,NOCASE
Account_Number_Key       KEY(RETSALESVer1:Account_Number,RETSALESVer1:Ref_Number),DUP,NOCASE
Purchase_Number_Key      KEY(RETSALESVer1:Purchase_Order_Number),DUP,NOCASE
Despatch_Number_Key      KEY(RETSALESVer1:Despatch_Number),DUP,NOCASE
Date_Booked_Key          KEY(RETSALESVer1:date_booked),DUP,NOCASE
AccountInvoiceKey        KEY(RETSALESVer1:Account_Number,RETSALESVer1:Invoice_Number),DUP,NOCASE
DespatchedPurchDateKey   KEY(RETSALESVer1:Despatched,RETSALESVer1:Purchase_Order_Number,RETSALESVer1:date_booked),DUP,NOCASE
DespatchNoRefNoKey       KEY(RETSALESVer1:Despatch_Number,RETSALESVer1:Ref_Number),DUP,NOCASE
AccountPurchaseNumberKey KEY(RETSALESVer1:Account_Number,RETSALESVer1:Purchase_Order_Number),DUP,NOCASE
WaybillNumberKey         KEY(RETSALESVer1:WaybillNumber,RETSALESVer1:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Account_Number              STRING(15)
Contact_Name                STRING(30)
Purchase_Order_Number       STRING(30)
Delivery_Collection         STRING(3)
Payment_Method              STRING(3)
Courier_Cost                REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier                     STRING(30)
Consignment_Number          STRING(30)
Date_Despatched             DATE
Despatched                  STRING(3)
Despatch_Number             LONG
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Courier_Cost        REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
WebOrderNumber              LONG
WebDateCreated              DATE
WebTimeCreated              STRING(20)
WebCreatedUser              STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Building_Name               STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Postcode_Delivery           STRING(10)
Company_Name_Delivery       STRING(30)
Building_Name_Delivery      STRING(30)
Address_Line1_Delivery      STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Fax_Number_Delivery         STRING(15)
Delivery_Text               STRING(255)
Invoice_Text                STRING(255)
WaybillNumber               LONG
DatePickingNotePrinted      DATE
TimePickingNotePrinted      TIME
                         END
                       END
!------------ End File: RETSALES, version 1 ------------------

!------------ File: RETSALES, version 2 ----------------------
!------------ modified 04.07.2011 at 11:32:46 -----------------
MOD:stFileName:RETSALESVer2  STRING(260)
RETSALESVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RETSALESVer2),PRE(RETSALESVer2),CREATE
Ref_Number_Key           KEY(RETSALESVer2:Ref_Number),NOCASE,PRIMARY
Invoice_Number_Key       KEY(RETSALESVer2:Invoice_Number),DUP,NOCASE
Despatched_Purchase_Key  KEY(RETSALESVer2:Despatched,RETSALESVer2:Purchase_Order_Number),DUP,NOCASE
Despatched_Ref_Key       KEY(RETSALESVer2:Despatched,RETSALESVer2:Ref_Number),DUP,NOCASE
Despatched_Account_Key   KEY(RETSALESVer2:Despatched,RETSALESVer2:Account_Number,RETSALESVer2:Purchase_Order_Number),DUP,NOCASE
Account_Number_Key       KEY(RETSALESVer2:Account_Number,RETSALESVer2:Ref_Number),DUP,NOCASE
Purchase_Number_Key      KEY(RETSALESVer2:Purchase_Order_Number),DUP,NOCASE
Despatch_Number_Key      KEY(RETSALESVer2:Despatch_Number),DUP,NOCASE
Date_Booked_Key          KEY(RETSALESVer2:date_booked),DUP,NOCASE
AccountInvoiceKey        KEY(RETSALESVer2:Account_Number,RETSALESVer2:Invoice_Number),DUP,NOCASE
DespatchedPurchDateKey   KEY(RETSALESVer2:Despatched,RETSALESVer2:Purchase_Order_Number,RETSALESVer2:date_booked),DUP,NOCASE
DespatchNoRefNoKey       KEY(RETSALESVer2:Despatch_Number,RETSALESVer2:Ref_Number),DUP,NOCASE
AccountPurchaseNumberKey KEY(RETSALESVer2:Account_Number,RETSALESVer2:Purchase_Order_Number),DUP,NOCASE
WaybillNumberKey         KEY(RETSALESVer2:WaybillNumber,RETSALESVer2:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Account_Number              STRING(15)
Contact_Name                STRING(30)
Purchase_Order_Number       STRING(30)
Delivery_Collection         STRING(3)
Payment_Method              STRING(3)
Courier_Cost                REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier                     STRING(30)
Consignment_Number          STRING(30)
Date_Despatched             DATE
Despatched                  STRING(3)
Despatch_Number             LONG
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Courier_Cost        REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
WebOrderNumber              LONG
WebDateCreated              DATE
WebTimeCreated              STRING(20)
WebCreatedUser              STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Building_Name               STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Postcode_Delivery           STRING(10)
Company_Name_Delivery       STRING(30)
Building_Name_Delivery      STRING(30)
Address_Line1_Delivery      STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Fax_Number_Delivery         STRING(15)
Delivery_Text               STRING(255)
Invoice_Text                STRING(255)
WaybillNumber               LONG
DatePickingNotePrinted      DATE
TimePickingNotePrinted      TIME
ExchangeOrder               BYTE
                         END
                       END
!------------ End File: RETSALES, version 2 ------------------

!------------ File: LOAORDR, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:LOAORDRVer1  STRING(260)
LOAORDRVer1            FILE,DRIVER('Btrieve'),OEM,PRE(LOAORDRVer1),NAME(MOD:stFileName:LOAORDRVer1)
Ref_Number_Key           KEY(LOAORDRVer1:Ref_Number),NOCASE,PRIMARY
By_Location_Key          KEY(LOAORDRVer1:Location,LOAORDRVer1:Manufacturer,LOAORDRVer1:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Location                    STRING(30)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Qty_Required                LONG
Qty_Received                LONG
Notes                       STRING(255)
DateCreated                 DATE
DateOrdered                 DATE
                         END
                       END
!------------ End File: LOAORDR, version 1 ------------------

!------------ File: WIPAMF, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:WIPAMFVer1  STRING(260)
WIPAMFVer1             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:WIPAMFVer1),PRE(WIPAMFVer1)
Audit_Number_Key         KEY(WIPAMFVer1:Audit_Number),NOCASE,PRIMARY
Secondary_Key            KEY(WIPAMFVer1:Complete_Flag,WIPAMFVer1:Audit_Number),DUP,NOCASE
Record                   RECORD,PRE()
Audit_Number                LONG
Date                        DATE
Time                        LONG
User                        STRING(3)
Status                      STRING(30)
Site_location               STRING(30)
Complete_Flag               BYTE
                         END
                       END
!------------ End File: WIPAMF, version 1 ------------------

!------------ File: TEAMS, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:TEAMSVer1  STRING(260)
TEAMSVer1              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:TEAMSVer1),PRE(TEAMSVer1)
Record_Number_Key        KEY(TEAMSVer1:Record_Number),NOCASE,PRIMARY
Team_Key                 KEY(TEAMSVer1:Team),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Team                        STRING(30)
                         END
                       END
!------------ End File: TEAMS, version 1 ------------------

!------------ File: TEAMS, version 2 ----------------------
!------------ modified 11.07.2012 at 12:49:32 -----------------
MOD:stFileName:TEAMSVer2  STRING(260)
TEAMSVer2              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:TEAMSVer2),PRE(TEAMSVer2),CREATE
Record_Number_Key        KEY(TEAMSVer2:Record_Number),NOCASE,PRIMARY
Team_Key                 KEY(TEAMSVer2:Team),NOCASE
KeyTraceAccount_number   KEY(TEAMSVer2:TradeAccount_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Team                        STRING(30)
TradeAccount_Number         STRING(15)
                         END
                       END
!------------ End File: TEAMS, version 2 ------------------

!------------ File: WIPAUI, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:WIPAUIVer1  STRING(260)
WIPAUIVer1             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:WIPAUIVer1),PRE(WIPAUIVer1)
Internal_No_Key          KEY(WIPAUIVer1:Internal_No),NOCASE,PRIMARY
Audit_Number_Key         KEY(WIPAUIVer1:Audit_Number),DUP,NOCASE
Ref_Number_Key           KEY(WIPAUIVer1:Ref_Number),DUP,NOCASE
Exch_Browse_Key          KEY(WIPAUIVer1:Audit_Number),DUP,NOCASE
AuditIMEIKey             KEY(WIPAUIVer1:Audit_Number,WIPAUIVer1:New_In_Status),DUP,NOCASE
Locate_IMEI_Key          KEY(WIPAUIVer1:Audit_Number,WIPAUIVer1:Site_Location,WIPAUIVer1:Ref_Number),DUP,NOCASE
Main_Browse_Key          KEY(WIPAUIVer1:Audit_Number,WIPAUIVer1:Confirmed,WIPAUIVer1:Site_Location,WIPAUIVer1:Status,WIPAUIVer1:Ref_Number),DUP,NOCASE
AuditRefNumberKey        KEY(WIPAUIVer1:Audit_Number,WIPAUIVer1:Ref_Number),DUP,NOCASE
AuditStatusRefNoKey      KEY(WIPAUIVer1:Audit_Number,WIPAUIVer1:Status,WIPAUIVer1:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Internal_No                 LONG
Site_Location               STRING(30)
Status                      STRING(30)
Audit_Number                LONG
Ref_Number                  LONG
New_In_Status               BYTE
Confirmed                   BYTE
                         END
                       END
!------------ End File: WIPAUI, version 1 ------------------

!------------ File: EXCHORDR, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:18 -----------------
MOD:stFileName:EXCHORDRVer1  STRING(260)
EXCHORDRVer1           FILE,DRIVER('Btrieve'),OEM,PRE(EXCHORDRVer1),NAME(MOD:stFileName:EXCHORDRVer1)
Ref_Number_Key           KEY(EXCHORDRVer1:Ref_Number),NOCASE,PRIMARY
By_Location_Key          KEY(EXCHORDRVer1:Location,EXCHORDRVer1:Manufacturer,EXCHORDRVer1:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Location                    STRING(30)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Qty_Required                LONG
Qty_Received                LONG
Notes                       STRING(255)
DateCreated                 DATE
DateOrdered                 DATE
                         END
                       END
!------------ End File: EXCHORDR, version 1 ------------------

!------------ File: EXCHORDR, version 2 ----------------------
!------------ modified 01.07.2011 at 19:38:08 -----------------
MOD:stFileName:EXCHORDRVer2  STRING(260)
EXCHORDRVer2           FILE,DRIVER('Btrieve'),OEM,PRE(EXCHORDRVer2),NAME(MOD:stFileName:EXCHORDRVer2),CREATE
Ref_Number_Key           KEY(EXCHORDRVer2:Ref_Number),NOCASE,PRIMARY
By_Location_Key          KEY(EXCHORDRVer2:Location,EXCHORDRVer2:Manufacturer,EXCHORDRVer2:Model_Number),DUP,NOCASE
OrderNumberKey           KEY(EXCHORDRVer2:Location,EXCHORDRVer2:OrderNumber),DUP,NOCASE
LocationReceivedKey      KEY(EXCHORDRVer2:Received,EXCHORDRVer2:Location,EXCHORDRVer2:Manufacturer,EXCHORDRVer2:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Location                    STRING(30)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Qty_Required                LONG
Qty_Received                LONG
Notes                       STRING(255)
DateCreated                 DATE
DateOrdered                 DATE
DateReceived                DATE
OrderNumber                 LONG
Received                    BYTE
                         END
                       END
!------------ End File: EXCHORDR, version 2 ------------------

!------------ File: EXCHORDR, version 3 ----------------------
!------------ modified 01.07.2011 at 20:51:02 -----------------
MOD:stFileName:EXCHORDRVer3  STRING(260)
EXCHORDRVer3           FILE,DRIVER('Btrieve'),OEM,PRE(EXCHORDRVer3),NAME(MOD:stFileName:EXCHORDRVer3),CREATE
Ref_Number_Key           KEY(EXCHORDRVer3:Ref_Number),NOCASE,PRIMARY
By_Location_Key          KEY(EXCHORDRVer3:Location,EXCHORDRVer3:Manufacturer,EXCHORDRVer3:Model_Number),DUP,NOCASE
OrderNumberKey           KEY(EXCHORDRVer3:Location,EXCHORDRVer3:OrderNumber),DUP,NOCASE
LocationReceivedKey      KEY(EXCHORDRVer3:Received,EXCHORDRVer3:Location,EXCHORDRVer3:Manufacturer,EXCHORDRVer3:Model_Number),DUP,NOCASE
OrderNumberOnlyKey       KEY(EXCHORDRVer3:OrderNumber),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Location                    STRING(30)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Qty_Required                LONG
Qty_Received                LONG
Notes                       STRING(255)
DateCreated                 DATE
DateOrdered                 DATE
DateReceived                DATE
OrderNumber                 LONG
Received                    BYTE
                         END
                       END
!------------ End File: EXCHORDR, version 3 ------------------

!------------ File: EXCHORDR, version 4 ----------------------
!------------ modified 04.07.2011 at 11:32:46 -----------------
MOD:stFileName:EXCHORDRVer4  STRING(260)
EXCHORDRVer4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:EXCHORDRVer4),PRE(EXCHORDRVer4),CREATE
Ref_Number_Key           KEY(EXCHORDRVer4:Ref_Number),NOCASE,PRIMARY
By_Location_Key          KEY(EXCHORDRVer4:Location,EXCHORDRVer4:Manufacturer,EXCHORDRVer4:Model_Number),DUP,NOCASE
OrderNumberKey           KEY(EXCHORDRVer4:Location,EXCHORDRVer4:OrderNumber),DUP,NOCASE
LocationReceivedKey      KEY(EXCHORDRVer4:Received,EXCHORDRVer4:Location,EXCHORDRVer4:Manufacturer,EXCHORDRVer4:Model_Number),DUP,NOCASE
OrderNumberOnlyKey       KEY(EXCHORDRVer4:OrderNumber),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Location                    STRING(30)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Qty_Required                LONG
Qty_Received                LONG
Notes                       STRING(255)
DateCreated                 DATE
DateOrdered                 DATE
DateReceived                DATE
OrderNumber                 LONG
Received                    BYTE
                         END
                       END
!------------ End File: EXCHORDR, version 4 ------------------

!------------ File: EXCHORDR, version 5 ----------------------
!------------ modified 06.07.2011 at 16:35:04 -----------------
MOD:stFileName:EXCHORDRVer5  STRING(260)
EXCHORDRVer5           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:EXCHORDRVer5),PRE(EXCHORDRVer5),CREATE
Ref_Number_Key           KEY(EXCHORDRVer5:Ref_Number),NOCASE,PRIMARY
By_Location_Key          KEY(EXCHORDRVer5:Location,EXCHORDRVer5:Manufacturer,EXCHORDRVer5:Model_Number),DUP,NOCASE
OrderNumberKey           KEY(EXCHORDRVer5:Location,EXCHORDRVer5:OrderNumber),DUP,NOCASE
LocationReceivedKey      KEY(EXCHORDRVer5:Received,EXCHORDRVer5:Location,EXCHORDRVer5:Manufacturer,EXCHORDRVer5:Model_Number),DUP,NOCASE
OrderNumberOnlyKey       KEY(EXCHORDRVer5:OrderNumber),DUP,NOCASE
LocationDateReceivedKey  KEY(EXCHORDRVer5:Location,EXCHORDRVer5:DateReceived),DUP,NOCASE
ExchangeRefNumberKey     KEY(EXCHORDRVer5:ExchangeRefNumber),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Location                    STRING(30)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Qty_Required                LONG
Qty_Received                LONG
Notes                       STRING(255)
DateCreated                 DATE
DateOrdered                 DATE
DateReceived                DATE
OrderNumber                 LONG
Received                    BYTE
ExchangeRefNumber           LONG
                         END
                       END
!------------ End File: EXCHORDR, version 5 ------------------

!------------ File: CONTHIST, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:CONTHISTVer1  STRING(260)
CONTHISTVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:CONTHISTVer1),PRE(CONTHISTVer1)
Ref_Number_Key           KEY(CONTHISTVer1:Ref_Number,-CONTHISTVer1:Date,-CONTHISTVer1:Time,CONTHISTVer1:Action),DUP,NOCASE
Action_Key               KEY(CONTHISTVer1:Ref_Number,CONTHISTVer1:Action,-CONTHISTVer1:Date),DUP,NOCASE
User_Key                 KEY(CONTHISTVer1:Ref_Number,CONTHISTVer1:User,-CONTHISTVer1:Date),DUP,NOCASE
Record_Number_Key        KEY(CONTHISTVer1:Record_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Notes                       STRING(2000)
SystemHistory               BYTE
                         END
                       END
!------------ End File: CONTHIST, version 1 ------------------

!------------ File: STANTEXT, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:STANTEXTVer1  STRING(260)
STANTEXTVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STANTEXTVer1),PRE(STANTEXTVer1)
Description_Key          KEY(STANTEXTVer1:Description),NOCASE,PRIMARY
Record                   RECORD,PRE()
Description                 STRING(30)
TelephoneNumber             STRING(30)
FaxNumber                   STRING(30)
DummyField                  STRING(1)
Text                        STRING(500)
                         END
                       END
!------------ End File: STANTEXT, version 1 ------------------

!------------ File: JOBSWARR, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:JOBSWARRVer1  STRING(260)
JOBSWARRVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSWARRVer1),PRE(JOBSWARRVer1)
RecordNumberKey          KEY(JOBSWARRVer1:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSWARRVer1:RefNumber),DUP,NOCASE
StatusManFirstKey        KEY(JOBSWARRVer1:Status,JOBSWARRVer1:Manufacturer,JOBSWARRVer1:FirstSecondYear,JOBSWARRVer1:RefNumber),DUP,NOCASE
StatusManKey             KEY(JOBSWARRVer1:Status,JOBSWARRVer1:Manufacturer,JOBSWARRVer1:RefNumber),DUP,NOCASE
ClaimStatusManKey        KEY(JOBSWARRVer1:Status,JOBSWARRVer1:Manufacturer,JOBSWARRVer1:ClaimSubmitted,JOBSWARRVer1:RefNumber),DUP,NOCASE
ClaimStatusManFirstKey   KEY(JOBSWARRVer1:Status,JOBSWARRVer1:Manufacturer,JOBSWARRVer1:FirstSecondYear,JOBSWARRVer1:ClaimSubmitted,JOBSWARRVer1:RefNumber),DUP,NOCASE
RRCStatusKey             KEY(JOBSWARRVer1:BranchID,JOBSWARRVer1:RepairedAt,JOBSWARRVer1:RRCStatus,JOBSWARRVer1:RefNumber),DUP,NOCASE
RRCStatusManKey          KEY(JOBSWARRVer1:BranchID,JOBSWARRVer1:RepairedAt,JOBSWARRVer1:RRCStatus,JOBSWARRVer1:Manufacturer,JOBSWARRVer1:RefNumber),DUP,NOCASE
RRCReconciledManKey      KEY(JOBSWARRVer1:BranchID,JOBSWARRVer1:RepairedAt,JOBSWARRVer1:RRCStatus,JOBSWARRVer1:Manufacturer,JOBSWARRVer1:RRCDateReconciled,JOBSWARRVer1:RefNumber),DUP,NOCASE
RRCReconciledKey         KEY(JOBSWARRVer1:BranchID,JOBSWARRVer1:RepairedAt,JOBSWARRVer1:RRCStatus,JOBSWARRVer1:RRCDateReconciled,JOBSWARRVer1:RefNumber),DUP,NOCASE
RepairedRRCStatusKey     KEY(JOBSWARRVer1:RepairedAt,JOBSWARRVer1:RRCStatus,JOBSWARRVer1:RefNumber),DUP,NOCASE
RepairedRRCStatusManKey  KEY(JOBSWARRVer1:RepairedAt,JOBSWARRVer1:RRCStatus,JOBSWARRVer1:Manufacturer,JOBSWARRVer1:RefNumber),DUP,NOCASE
RepairedRRCReconciledKey KEY(JOBSWARRVer1:RepairedAt,JOBSWARRVer1:RRCStatus,JOBSWARRVer1:DateReconciled,JOBSWARRVer1:RefNumber),DUP,NOCASE
RepairedRRCReconManKey   KEY(JOBSWARRVer1:RepairedAt,JOBSWARRVer1:RRCStatus,JOBSWARRVer1:Manufacturer,JOBSWARRVer1:RRCDateReconciled,JOBSWARRVer1:RefNumber),DUP,NOCASE
SubmittedRepairedBranchKey KEY(JOBSWARRVer1:BranchID,JOBSWARRVer1:RepairedAt,JOBSWARRVer1:ClaimSubmitted,JOBSWARRVer1:RefNumber),DUP,NOCASE
SubmittedBranchKey       KEY(JOBSWARRVer1:BranchID,JOBSWARRVer1:ClaimSubmitted,JOBSWARRVer1:RefNumber),DUP,NOCASE
ClaimSubmittedKey        KEY(JOBSWARRVer1:ClaimSubmitted,JOBSWARRVer1:RefNumber),DUP,NOCASE
RepairedRRCAccManKey     KEY(JOBSWARRVer1:BranchID,JOBSWARRVer1:RepairedAt,JOBSWARRVer1:RRCStatus,JOBSWARRVer1:Manufacturer,JOBSWARRVer1:DateAccepted,JOBSWARRVer1:RefNumber),DUP,NOCASE
AcceptedBranchKey        KEY(JOBSWARRVer1:BranchID,JOBSWARRVer1:DateAccepted,JOBSWARRVer1:RefNumber),DUP,NOCASE
DateAcceptedKey          KEY(JOBSWARRVer1:DateAccepted,JOBSWARRVer1:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
BranchID                    STRING(2)
RepairedAt                  STRING(3)
Manufacturer                STRING(30)
FirstSecondYear             BYTE
Status                      STRING(3)
Submitted                   LONG
FromApproved                BYTE
ClaimSubmitted              DATE
DateAccepted                DATE
DateReconciled              DATE
RRCDateReconciled           DATE
RRCStatus                   STRING(3)
                         END
                       END
!------------ End File: JOBSWARR, version 1 ------------------

!------------ File: JOBSWARR, version 2 ----------------------
!------------ modified 21.07.2010 at 11:49:46 -----------------
MOD:stFileName:JOBSWARRVer2  STRING(260)
JOBSWARRVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSWARRVer2),PRE(JOBSWARRVer2),CREATE
RecordNumberKey          KEY(JOBSWARRVer2:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSWARRVer2:RefNumber),DUP,NOCASE
StatusManFirstKey        KEY(JOBSWARRVer2:Status,JOBSWARRVer2:Manufacturer,JOBSWARRVer2:FirstSecondYear,JOBSWARRVer2:RefNumber),DUP,NOCASE
StatusManKey             KEY(JOBSWARRVer2:Status,JOBSWARRVer2:Manufacturer,JOBSWARRVer2:RefNumber),DUP,NOCASE
ClaimStatusManKey        KEY(JOBSWARRVer2:Status,JOBSWARRVer2:Manufacturer,JOBSWARRVer2:ClaimSubmitted,JOBSWARRVer2:RefNumber),DUP,NOCASE
ClaimStatusManFirstKey   KEY(JOBSWARRVer2:Status,JOBSWARRVer2:Manufacturer,JOBSWARRVer2:FirstSecondYear,JOBSWARRVer2:ClaimSubmitted,JOBSWARRVer2:RefNumber),DUP,NOCASE
RRCStatusKey             KEY(JOBSWARRVer2:BranchID,JOBSWARRVer2:RepairedAt,JOBSWARRVer2:RRCStatus,JOBSWARRVer2:RefNumber),DUP,NOCASE
RRCStatusManKey          KEY(JOBSWARRVer2:BranchID,JOBSWARRVer2:RepairedAt,JOBSWARRVer2:RRCStatus,JOBSWARRVer2:Manufacturer,JOBSWARRVer2:RefNumber),DUP,NOCASE
RRCReconciledManKey      KEY(JOBSWARRVer2:BranchID,JOBSWARRVer2:RepairedAt,JOBSWARRVer2:RRCStatus,JOBSWARRVer2:Manufacturer,JOBSWARRVer2:RRCDateReconciled,JOBSWARRVer2:RefNumber),DUP,NOCASE
RRCReconciledKey         KEY(JOBSWARRVer2:BranchID,JOBSWARRVer2:RepairedAt,JOBSWARRVer2:RRCStatus,JOBSWARRVer2:RRCDateReconciled,JOBSWARRVer2:RefNumber),DUP,NOCASE
RepairedRRCStatusKey     KEY(JOBSWARRVer2:RepairedAt,JOBSWARRVer2:RRCStatus,JOBSWARRVer2:RefNumber),DUP,NOCASE
RepairedRRCStatusManKey  KEY(JOBSWARRVer2:RepairedAt,JOBSWARRVer2:RRCStatus,JOBSWARRVer2:Manufacturer,JOBSWARRVer2:RefNumber),DUP,NOCASE
RepairedRRCReconciledKey KEY(JOBSWARRVer2:RepairedAt,JOBSWARRVer2:RRCStatus,JOBSWARRVer2:DateReconciled,JOBSWARRVer2:RefNumber),DUP,NOCASE
RepairedRRCReconManKey   KEY(JOBSWARRVer2:RepairedAt,JOBSWARRVer2:RRCStatus,JOBSWARRVer2:Manufacturer,JOBSWARRVer2:RRCDateReconciled,JOBSWARRVer2:RefNumber),DUP,NOCASE
SubmittedRepairedBranchKey KEY(JOBSWARRVer2:BranchID,JOBSWARRVer2:RepairedAt,JOBSWARRVer2:ClaimSubmitted,JOBSWARRVer2:RefNumber),DUP,NOCASE
SubmittedBranchKey       KEY(JOBSWARRVer2:BranchID,JOBSWARRVer2:ClaimSubmitted,JOBSWARRVer2:RefNumber),DUP,NOCASE
ClaimSubmittedKey        KEY(JOBSWARRVer2:ClaimSubmitted,JOBSWARRVer2:RefNumber),DUP,NOCASE
RepairedRRCAccManKey     KEY(JOBSWARRVer2:BranchID,JOBSWARRVer2:RepairedAt,JOBSWARRVer2:RRCStatus,JOBSWARRVer2:Manufacturer,JOBSWARRVer2:DateAccepted,JOBSWARRVer2:RefNumber),DUP,NOCASE
AcceptedBranchKey        KEY(JOBSWARRVer2:BranchID,JOBSWARRVer2:DateAccepted,JOBSWARRVer2:RefNumber),DUP,NOCASE
DateAcceptedKey          KEY(JOBSWARRVer2:DateAccepted,JOBSWARRVer2:RefNumber),DUP,NOCASE
RejectedBranchKey        KEY(JOBSWARRVer2:BranchID,JOBSWARRVer2:DateRejected,JOBSWARRVer2:RefNumber),DUP,NOCASE
RejectedKey              KEY(JOBSWARRVer2:DateRejected,JOBSWARRVer2:RefNumber),DUP,NOCASE
FinalRejectionBranchKey  KEY(JOBSWARRVer2:BranchID,JOBSWARRVer2:DateFinalRejection,JOBSWARRVer2:RefNumber),DUP,NOCASE
FinalRejectionKey        KEY(JOBSWARRVer2:DateFinalRejection,JOBSWARRVer2:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
BranchID                    STRING(2)
RepairedAt                  STRING(3)
Manufacturer                STRING(30)
FirstSecondYear             BYTE
Status                      STRING(3)
Submitted                   LONG
FromApproved                BYTE
ClaimSubmitted              DATE
DateAccepted                DATE
DateReconciled              DATE
RRCDateReconciled           DATE
RRCStatus                   STRING(3)
DateRejected                DATE
DateFinalRejection          DATE
                         END
                       END
!------------ End File: JOBSWARR, version 2 ------------------

!------------ File: CURRENCY, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:CURRENCYVer1  STRING(260)
CURRENCYVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:CURRENCYVer1),PRE(CURRENCYVer1)
RecordNumberKey          KEY(CURRENCYVer1:RecordNumber),NOCASE,PRIMARY
CurrencyCodeKey          KEY(CURRENCYVer1:CurrencyCode),NOCASE
LastUpdateDateKey        KEY(CURRENCYVer1:LastUpdateDate,CURRENCYVer1:CurrencyCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
CurrencyCode                STRING(30)
Description                 STRING(60)
DailyRate                   REAL
DivideMultiply              STRING(1)
LastUpdateDate              DATE
                         END
                       END
!------------ End File: CURRENCY, version 1 ------------------

!------------ File: CURRENCY, version 2 ----------------------
!------------ modified 06.08.2010 at 09:47:17 -----------------
MOD:stFileName:CURRENCYVer2  STRING(260)
CURRENCYVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:CURRENCYVer2),PRE(CURRENCYVer2),CREATE
RecordNumberKey          KEY(CURRENCYVer2:RecordNumber),NOCASE,PRIMARY
CurrencyCodeKey          KEY(CURRENCYVer2:CurrencyCode),NOCASE
LastUpdateDateKey        KEY(CURRENCYVer2:LastUpdateDate,CURRENCYVer2:CurrencyCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
CurrencyCode                STRING(30)
Description                 STRING(60)
DailyRate                   REAL
DivideMultiply              STRING(1)
LastUpdateDate              DATE
CorrelationCode             STRING(30)
                         END
                       END
!------------ End File: CURRENCY, version 2 ------------------

!------------ File: JOBSE2, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:JOBSE2Ver1  STRING(260)
JOBSE2Ver1             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSE2Ver1),PRE(JOBSE2Ver1)
RecordNumberKey          KEY(JOBSE2Ver1:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSE2Ver1:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IDNumber                    STRING(13)
InPendingDate               DATE
Contract                    BYTE
Prepaid                     BYTE
WarrantyRefNo               STRING(30)
SIDBookingName              STRING(60)
XAntenna                    BYTE
XLens                       BYTE
XFCover                     BYTE
XBCover                     BYTE
XKeypad                     BYTE
XBattery                    BYTE
XCharger                    BYTE
XLCD                        BYTE
XSimReader                  BYTE
XSystemConnector            BYTE
XNone                       BYTE
XNotes                      STRING(255)
ExchangeTerms               BYTE
WaybillNoFromPUP            LONG
WaybillNoToPUP              LONG
SMSNotification             BYTE
EmailNotification           BYTE
SMSAlertNumber              STRING(30)
EmailAlertAddress           STRING(255)
DateReceivedAtPUP           DATE
TimeReceivedAtPUP           TIME
DateDespatchFromPUP         DATE
TimeDespatchFromPUP         TIME
LoanIDNumber                STRING(13)
CourierWaybillNumber        STRING(30)
HubCustomer                 STRING(30)
HubCollection               STRING(30)
HubDelivery                 STRING(30)
WLabourPaid                 REAL
WPartsPaid                  REAL
WOtherCosts                 REAL
WSubTotal                   REAL
WVAT                        REAL
WTotal                      REAL
WarrantyReason              STRING(255)
WInvoiceRefNo               STRING(255)
SMSDate                     DATE
                         END
                       END
!------------ End File: JOBSE2, version 1 ------------------

!------------ File: JOBSE2, version 2 ----------------------
!------------ modified 04.10.2010 at 11:07:06 -----------------
MOD:stFileName:JOBSE2Ver2  STRING(260)
JOBSE2Ver2             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSE2Ver2),PRE(JOBSE2Ver2),CREATE
RecordNumberKey          KEY(JOBSE2Ver2:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSE2Ver2:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IDNumber                    STRING(13)
InPendingDate               DATE
Contract                    BYTE
Prepaid                     BYTE
WarrantyRefNo               STRING(30)
SIDBookingName              STRING(60)
XAntenna                    BYTE
XLens                       BYTE
XFCover                     BYTE
XBCover                     BYTE
XKeypad                     BYTE
XBattery                    BYTE
XCharger                    BYTE
XLCD                        BYTE
XSimReader                  BYTE
XSystemConnector            BYTE
XNone                       BYTE
XNotes                      STRING(255)
ExchangeTerms               BYTE
WaybillNoFromPUP            LONG
WaybillNoToPUP              LONG
SMSNotification             BYTE
EmailNotification           BYTE
SMSAlertNumber              STRING(30)
EmailAlertAddress           STRING(255)
DateReceivedAtPUP           DATE
TimeReceivedAtPUP           TIME
DateDespatchFromPUP         DATE
TimeDespatchFromPUP         TIME
LoanIDNumber                STRING(13)
CourierWaybillNumber        STRING(30)
HubCustomer                 STRING(30)
HubCollection               STRING(30)
HubDelivery                 STRING(30)
WLabourPaid                 REAL
WPartsPaid                  REAL
WOtherCosts                 REAL
WSubTotal                   REAL
WVAT                        REAL
WTotal                      REAL
WarrantyReason              STRING(255)
WInvoiceRefNo               STRING(255)
SMSDate                     DATE
JobDiscountAmnt             REAL
InvDiscountAmnt             REAL
                         END
                       END
!------------ End File: JOBSE2, version 2 ------------------

!------------ File: JOBSE2, version 3 ----------------------
!------------ modified 01.02.2011 at 11:16:08 -----------------
MOD:stFileName:JOBSE2Ver3  STRING(260)
JOBSE2Ver3             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSE2Ver3),PRE(JOBSE2Ver3),CREATE
RecordNumberKey          KEY(JOBSE2Ver3:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSE2Ver3:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IDNumber                    STRING(13)
InPendingDate               DATE
Contract                    BYTE
Prepaid                     BYTE
WarrantyRefNo               STRING(30)
SIDBookingName              STRING(60)
XAntenna                    BYTE
XLens                       BYTE
XFCover                     BYTE
XBCover                     BYTE
XKeypad                     BYTE
XBattery                    BYTE
XCharger                    BYTE
XLCD                        BYTE
XSimReader                  BYTE
XSystemConnector            BYTE
XNone                       BYTE
XNotes                      STRING(255)
ExchangeTerms               BYTE
WaybillNoFromPUP            LONG
WaybillNoToPUP              LONG
SMSNotification             BYTE
EmailNotification           BYTE
SMSAlertNumber              STRING(30)
EmailAlertAddress           STRING(255)
DateReceivedAtPUP           DATE
TimeReceivedAtPUP           TIME
DateDespatchFromPUP         DATE
TimeDespatchFromPUP         TIME
LoanIDNumber                STRING(13)
CourierWaybillNumber        STRING(30)
HubCustomer                 STRING(30)
HubCollection               STRING(30)
HubDelivery                 STRING(30)
WLabourPaid                 REAL
WPartsPaid                  REAL
WOtherCosts                 REAL
WSubTotal                   REAL
WVAT                        REAL
WTotal                      REAL
WarrantyReason              STRING(255)
WInvoiceRefNo               STRING(255)
SMSDate                     DATE
JobDiscountAmnt             REAL
InvDiscountAmnt             REAL
POPConfirmed                BYTE
                         END
                       END
!------------ End File: JOBSE2, version 3 ------------------

!------------ File: ORDPEND, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:ORDPENDVer1  STRING(260)
ORDPENDVer1            FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:ORDPENDVer1),PRE(ORDPENDVer1)
Ref_Number_Key           KEY(ORDPENDVer1:Ref_Number),NOCASE,PRIMARY
Supplier_Key             KEY(ORDPENDVer1:Supplier,ORDPENDVer1:Part_Number),DUP,NOCASE
DescriptionKey           KEY(ORDPENDVer1:Supplier,ORDPENDVer1:Description),DUP,NOCASE
Supplier_Name_Key        KEY(ORDPENDVer1:Supplier),DUP,NOCASE
Part_Ref_Number_Key      KEY(ORDPENDVer1:Part_Ref_Number),DUP,NOCASE
Awaiting_Supplier_Key    KEY(ORDPENDVer1:Awaiting_Stock,ORDPENDVer1:Supplier,ORDPENDVer1:Part_Number),DUP,NOCASE
PartRecordNumberKey      KEY(ORDPENDVer1:PartRecordNumber),DUP,NOCASE
ReqPartNumber            KEY(ORDPENDVer1:StockReqNumber,ORDPENDVer1:Part_Number),DUP,NOCASE
ReqDescriptionKey        KEY(ORDPENDVer1:StockReqNumber,ORDPENDVer1:Description),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Part_Ref_Number             LONG
Job_Number                  LONG
Part_Type                   STRING(3)
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    LONG
Account_Number              STRING(15)
Awaiting_Stock              STRING(3)
Reason                      STRING(255)
PartRecordNumber            LONG
StockReqNumber              LONG
                         END
                       END
!------------ End File: ORDPEND, version 1 ------------------

!------------ File: REPTYDEF, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:REPTYDEFVer1  STRING(260)
REPTYDEFVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:REPTYDEFVer1),PRE(REPTYDEFVer1)
RecordNumberKey          KEY(REPTYDEFVer1:RecordNumber),NOCASE,PRIMARY
ManRepairTypeKey         KEY(REPTYDEFVer1:Manufacturer,REPTYDEFVer1:Repair_Type),NOCASE
Repair_Type_Key          KEY(REPTYDEFVer1:Repair_Type),DUP,NOCASE
Chargeable_Key           KEY(REPTYDEFVer1:Chargeable,REPTYDEFVer1:Repair_Type),DUP,NOCASE
Warranty_Key             KEY(REPTYDEFVer1:Warranty,REPTYDEFVer1:Repair_Type),DUP,NOCASE
ChaManRepairTypeKey      KEY(REPTYDEFVer1:Manufacturer,REPTYDEFVer1:Chargeable,REPTYDEFVer1:Repair_Type),DUP,NOCASE
WarManRepairTypeKey      KEY(REPTYDEFVer1:Manufacturer,REPTYDEFVer1:Warranty,REPTYDEFVer1:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Repair_Type                 STRING(30)
Chargeable                  STRING(3)
Warranty                    STRING(3)
WarrantyCode                STRING(5)
CompFaultCoding             BYTE
ExcludeFromEDI              BYTE
ExcludeFromInvoicing        BYTE
BER                         BYTE
ExcludeFromBouncer          BYTE
PromptForExchange           BYTE
JobWeighting                LONG
SkillLevel                  LONG
Manufacturer                STRING(30)
RepairLevel                 LONG
ForceAdjustment             BYTE
ScrapExchange               BYTE
ExcludeHandlingFee          BYTE
NotAvailable                BYTE
                         END
                       END
!------------ End File: REPTYDEF, version 1 ------------------

!------------ File: REPTYDEF, version 2 ----------------------
!------------ modified 01.11.2010 at 10:17:55 -----------------
MOD:stFileName:REPTYDEFVer2  STRING(260)
REPTYDEFVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:REPTYDEFVer2),PRE(REPTYDEFVer2),CREATE
RecordNumberKey          KEY(REPTYDEFVer2:RecordNumber),NOCASE,PRIMARY
ManRepairTypeKey         KEY(REPTYDEFVer2:Manufacturer,REPTYDEFVer2:Repair_Type),NOCASE
Repair_Type_Key          KEY(REPTYDEFVer2:Repair_Type),DUP,NOCASE
Chargeable_Key           KEY(REPTYDEFVer2:Chargeable,REPTYDEFVer2:Repair_Type),DUP,NOCASE
Warranty_Key             KEY(REPTYDEFVer2:Warranty,REPTYDEFVer2:Repair_Type),DUP,NOCASE
ChaManRepairTypeKey      KEY(REPTYDEFVer2:Manufacturer,REPTYDEFVer2:Chargeable,REPTYDEFVer2:Repair_Type),DUP,NOCASE
WarManRepairTypeKey      KEY(REPTYDEFVer2:Manufacturer,REPTYDEFVer2:Warranty,REPTYDEFVer2:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Repair_Type                 STRING(30)
Chargeable                  STRING(3)
Warranty                    STRING(3)
WarrantyCode                STRING(5)
CompFaultCoding             BYTE
ExcludeFromEDI              BYTE
ExcludeFromInvoicing        BYTE
BER                         BYTE
ExcludeFromBouncer          BYTE
PromptForExchange           BYTE
JobWeighting                LONG
SkillLevel                  LONG
Manufacturer                STRING(30)
RepairLevel                 LONG
ForceAdjustment             BYTE
ScrapExchange               BYTE
ExcludeHandlingFee          BYTE
NotAvailable                BYTE
NoParts                     STRING(1)
                         END
                       END
!------------ End File: REPTYDEF, version 2 ------------------

!------------ File: MANFAULT, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:MANFAULTVer1  STRING(260)
MANFAULTVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANFAULTVer1),PRE(MANFAULTVer1)
Field_Number_Key         KEY(MANFAULTVer1:Manufacturer,MANFAULTVer1:Field_Number),NOCASE,PRIMARY
MainFaultKey             KEY(MANFAULTVer1:Manufacturer,MANFAULTVer1:MainFault),DUP,NOCASE
InFaultKey               KEY(MANFAULTVer1:Manufacturer,MANFAULTVer1:InFault),DUP,NOCASE
ScreenOrderKey           KEY(MANFAULTVer1:Manufacturer,MANFAULTVer1:ScreenOrder),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
MainFault                   BYTE
PromptForExchange           BYTE
InFault                     BYTE
CompulsoryIfExchange        BYTE
NotAvailable                BYTE
CharCompulsory              BYTE
CharCompulsoryBooking       BYTE
ScreenOrder                 LONG
RestrictAvailability        BYTE
RestrictServiceCentre       BYTE
GenericFault                BYTE
NotCompulsoryThirdParty     BYTE
HideThirdParty              BYTE
HideRelatedCodeIfBlank      BYTE
BlankRelatedCode            LONG
FillFromDOP                 BYTE
CompulsoryForRepairType     BYTE
CompulsoryRepairType        STRING(30)
                         END
                       END
!------------ End File: MANFAULT, version 1 ------------------

!------------ File: LOCVALUE, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:LOCVALUEVer1  STRING(260)
LOCVALUEVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:LOCVALUEVer1),PRE(LOCVALUEVer1)
RecordNumberKey          KEY(LOCVALUEVer1:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(LOCVALUEVer1:Location,LOCVALUEVer1:TheDate),DUP,NOCASE
DateOnly                 KEY(LOCVALUEVer1:TheDate),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
TheDate                     DATE
PurchaseTotal               REAL
SaleCostTotal               REAL
RetailCostTotal             REAL
QuantityTotal               LONG
                         END
                       END
!------------ End File: LOCVALUE, version 1 ------------------

!------------ File: COURIER, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:COURIERVer1  STRING(260)
COURIERVer1            FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:COURIERVer1),PRE(COURIERVer1)
Courier_Key              KEY(COURIERVer1:Courier),NOCASE,PRIMARY
Courier_Type_Key         KEY(COURIERVer1:Courier_Type,COURIERVer1:Courier),DUP,NOCASE
Record                   RECORD,PRE()
Courier                     STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name                STRING(60)
Export_Path                 STRING(255)
Include_Estimate            STRING(3)
Include_Chargeable          STRING(3)
Include_Warranty            STRING(3)
Service                     STRING(15)
Import_Path                 STRING(255)
Courier_Type                STRING(30)
Last_Despatch_Time          TIME
ICOLSuffix                  STRING(3)
ContractNumber              STRING(20)
ANCPath                     STRING(255)
ANCCount                    LONG
DespatchClose               STRING(3)
LabGOptions                 STRING(60)
CustomerCollection          BYTE
NewStatus                   STRING(30)
NoOfDespatchNotes           LONG
AutoConsignmentNo           BYTE
LastConsignmentNo           LONG
PrintLabel                  BYTE
PrintWaybill                BYTE
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             BYTE
IncludeSunday               BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
EmailAddress                STRING(255)
FromEmailAddress            STRING(255)
                         END
                       END
!------------ End File: COURIER, version 1 ------------------

!------------ File: TRDPARTY, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:TRDPARTYVer1  STRING(260)
TRDPARTYVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRDPARTYVer1),PRE(TRDPARTYVer1)
Company_Name_Key         KEY(TRDPARTYVer1:Company_Name),NOCASE,PRIMARY
Account_Number_Key       KEY(TRDPARTYVer1:Account_Number),NOCASE
Special_Instructions_Key KEY(TRDPARTYVer1:Special_Instructions),DUP,NOCASE
Courier_Only_Key         KEY(TRDPARTYVer1:Courier),DUP,NOCASE
DeactivateCompanyKey     KEY(TRDPARTYVer1:Deactivate,TRDPARTYVer1:Company_Name),DUP,NOCASE
ASCIDKey                 KEY(TRDPARTYVer1:ASCID),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Account_Number              STRING(30)
Contact_Name                STRING(60)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Turnaround_Time             REAL
Courier_Direct              STRING(3)
Courier                     STRING(30)
Print_Order                 STRING(15)
Special_Instructions        STRING(30)
Batch_Number                LONG
BatchLimit                  LONG
VATRate                     REAL
Markup                      REAL
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
Deactivate                  BYTE
LHubAccount                 BYTE
ASCID                       STRING(30)
                         END
                       END
!------------ End File: TRDPARTY, version 1 ------------------

!------------ File: TRDPARTY, version 2 ----------------------
!------------ modified 02.02.2012 at 16:52:24 -----------------
MOD:stFileName:TRDPARTYVer2  STRING(260)
TRDPARTYVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRDPARTYVer2),PRE(TRDPARTYVer2),CREATE
Company_Name_Key         KEY(TRDPARTYVer2:Company_Name),NOCASE,PRIMARY
Account_Number_Key       KEY(TRDPARTYVer2:Account_Number),NOCASE
Special_Instructions_Key KEY(TRDPARTYVer2:Special_Instructions),DUP,NOCASE
Courier_Only_Key         KEY(TRDPARTYVer2:Courier),DUP,NOCASE
DeactivateCompanyKey     KEY(TRDPARTYVer2:Deactivate,TRDPARTYVer2:Company_Name),DUP,NOCASE
ASCIDKey                 KEY(TRDPARTYVer2:ASCID),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Account_Number              STRING(30)
Contact_Name                STRING(60)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Turnaround_Time             REAL
Courier_Direct              STRING(3)
Courier                     STRING(30)
Print_Order                 STRING(15)
Special_Instructions        STRING(30)
Batch_Number                LONG
BatchLimit                  LONG
VATRate                     REAL
Markup                      REAL
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
Deactivate                  BYTE
LHubAccount                 BYTE
ASCID                       STRING(30)
PreCompletionClaiming       BYTE
                         END
                       END
!------------ End File: TRDPARTY, version 2 ------------------

!------------ File: TRDPARTY, version 3 ----------------------
!------------ modified 15.01.2013 at 11:42:25 -----------------
MOD:stFileName:TRDPARTYVer3  STRING(260)
TRDPARTYVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRDPARTYVer3),PRE(TRDPARTYVer3),CREATE
Company_Name_Key         KEY(TRDPARTYVer3:Company_Name),NOCASE,PRIMARY
Account_Number_Key       KEY(TRDPARTYVer3:Account_Number),NOCASE
Special_Instructions_Key KEY(TRDPARTYVer3:Special_Instructions),DUP,NOCASE
Courier_Only_Key         KEY(TRDPARTYVer3:Courier),DUP,NOCASE
DeactivateCompanyKey     KEY(TRDPARTYVer3:Deactivate,TRDPARTYVer3:Company_Name),DUP,NOCASE
ASCIDKey                 KEY(TRDPARTYVer3:ASCID),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Account_Number              STRING(30)
Contact_Name                STRING(60)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Turnaround_Time             REAL
Courier_Direct              STRING(3)
Courier                     STRING(30)
Print_Order                 STRING(15)
Special_Instructions        STRING(30)
Batch_Number                LONG
BatchLimit                  LONG
VATRate                     REAL
Markup                      REAL
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
Deactivate                  BYTE
LHubAccount                 BYTE
ASCID                       STRING(30)
PreCompletionClaiming       BYTE
EVO_AccNumber               STRING(20)
EVO_VendorNumber            STRING(20)
                         END
                       END
!------------ End File: TRDPARTY, version 3 ------------------

!------------ File: TRDPARTY, version 4 ----------------------
!------------ modified 30.01.2013 at 15:51:55 -----------------
MOD:stFileName:TRDPARTYVer4  STRING(260)
TRDPARTYVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRDPARTYVer4),PRE(TRDPARTYVer4),CREATE
Company_Name_Key         KEY(TRDPARTYVer4:Company_Name),NOCASE,PRIMARY
Account_Number_Key       KEY(TRDPARTYVer4:Account_Number),NOCASE
Special_Instructions_Key KEY(TRDPARTYVer4:Special_Instructions),DUP,NOCASE
Courier_Only_Key         KEY(TRDPARTYVer4:Courier),DUP,NOCASE
DeactivateCompanyKey     KEY(TRDPARTYVer4:Deactivate,TRDPARTYVer4:Company_Name),DUP,NOCASE
ASCIDKey                 KEY(TRDPARTYVer4:ASCID),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Account_Number              STRING(30)
Contact_Name                STRING(60)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Turnaround_Time             REAL
Courier_Direct              STRING(3)
Courier                     STRING(30)
Print_Order                 STRING(15)
Special_Instructions        STRING(30)
Batch_Number                LONG
BatchLimit                  LONG
VATRate                     REAL
Markup                      REAL
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
Deactivate                  BYTE
LHubAccount                 BYTE
ASCID                       STRING(30)
PreCompletionClaiming       BYTE
EVO_AccNumber               STRING(20)
EVO_VendorNumber            STRING(20)
EVO_Profit_Centre           STRING(30)
                         END
                       END
!------------ End File: TRDPARTY, version 4 ------------------

!------------ File: TRANTYPE, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:TRANTYPEVer1  STRING(260)
TRANTYPEVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRANTYPEVer1),PRE(TRANTYPEVer1)
Transit_Type_Key         KEY(TRANTYPEVer1:Transit_Type),NOCASE,PRIMARY
RRCKey                   KEY(TRANTYPEVer1:RRC,TRANTYPEVer1:Transit_Type),DUP,NOCASE
ARCKey                   KEY(TRANTYPEVer1:ARC,TRANTYPEVer1:Transit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Transit_Type                STRING(30)
Courier_Collection_Report   STRING(3)
Collection_Address          STRING(3)
Delivery_Address            STRING(3)
Loan_Unit                   STRING(3)
Exchange_Unit               STRING(3)
Force_DOP                   STRING(3)
Force_Location              STRING(3)
Skip_Workshop               STRING(3)
HideLocation                BYTE
InternalLocation            STRING(30)
Workshop_Label              STRING(3)
Job_Card                    STRING(3)
JobReceipt                  STRING(3)
Initial_Status              STRING(30)
Initial_Priority            STRING(30)
ExchangeStatus              STRING(30)
LoanStatus                  STRING(30)
Location                    STRING(3)
ANCCollNote                 STRING(3)
InWorkshop                  STRING(3)
HubRepair                   BYTE
ARC                         BYTE
RRC                         BYTE
OBF                         BYTE
                         END
                       END
!------------ End File: TRANTYPE, version 1 ------------------

!------------ File: TRANTYPE, version 2 ----------------------
!------------ modified 05.05.2011 at 12:32:47 -----------------
MOD:stFileName:TRANTYPEVer2  STRING(260)
TRANTYPEVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRANTYPEVer2),PRE(TRANTYPEVer2),CREATE
Transit_Type_Key         KEY(TRANTYPEVer2:Transit_Type),NOCASE,PRIMARY
RRCKey                   KEY(TRANTYPEVer2:RRC,TRANTYPEVer2:Transit_Type),DUP,NOCASE
ARCKey                   KEY(TRANTYPEVer2:ARC,TRANTYPEVer2:Transit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Transit_Type                STRING(30)
Courier_Collection_Report   STRING(3)
Collection_Address          STRING(3)
Delivery_Address            STRING(3)
Loan_Unit                   STRING(3)
Exchange_Unit               STRING(3)
Force_DOP                   STRING(3)
Force_Location              STRING(3)
Skip_Workshop               STRING(3)
HideLocation                BYTE
InternalLocation            STRING(30)
Workshop_Label              STRING(3)
Job_Card                    STRING(3)
JobReceipt                  STRING(3)
Initial_Status              STRING(30)
Initial_Priority            STRING(30)
ExchangeStatus              STRING(30)
LoanStatus                  STRING(30)
Location                    STRING(3)
ANCCollNote                 STRING(3)
InWorkshop                  STRING(3)
HubRepair                   BYTE
ARC                         BYTE
RRC                         BYTE
OBF                         BYTE
WaybillComBook              BYTE
WaybillComComp              BYTE
                         END
                       END
!------------ End File: TRANTYPE, version 2 ------------------

!------------ File: ESNMODEL, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:ESNMODELVer1  STRING(260)
ESNMODELVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:ESNMODELVer1),PRE(ESNMODELVer1)
Record_Number_Key        KEY(ESNMODELVer1:Record_Number),NOCASE,PRIMARY
ESN_Key                  KEY(ESNMODELVer1:ESN,ESNMODELVer1:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(ESNMODELVer1:ESN),DUP,NOCASE
Model_Number_Key         KEY(ESNMODELVer1:Model_Number,ESNMODELVer1:ESN),DUP,NOCASE
Manufacturer_Key         KEY(ESNMODELVer1:Manufacturer,ESNMODELVer1:ESN),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
ESN                         STRING(8)
Model_Number                STRING(30)
Manufacturer                STRING(30)
Include48Hour               BYTE
                         END
                       END
!------------ End File: ESNMODEL, version 1 ------------------

!------------ File: STOCK, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:STOCKVer1  STRING(260)
STOCKVer1              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STOCKVer1),PRE(STOCKVer1)
Ref_Number_Key           KEY(STOCKVer1:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(STOCKVer1:Sundry_Item),DUP,NOCASE
Description_Key          KEY(STOCKVer1:Location,STOCKVer1:Description),DUP,NOCASE
Description_Accessory_Key KEY(STOCKVer1:Location,STOCKVer1:Accessory,STOCKVer1:Description),DUP,NOCASE
Shelf_Location_Key       KEY(STOCKVer1:Location,STOCKVer1:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(STOCKVer1:Location,STOCKVer1:Accessory,STOCKVer1:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(STOCKVer1:Location,STOCKVer1:Accessory,STOCKVer1:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(STOCKVer1:Manufacturer,STOCKVer1:Part_Number),DUP,NOCASE
Supplier_Key             KEY(STOCKVer1:Location,STOCKVer1:Supplier),DUP,NOCASE
Location_Key             KEY(STOCKVer1:Location,STOCKVer1:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(STOCKVer1:Location,STOCKVer1:Accessory,STOCKVer1:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(STOCKVer1:Location,STOCKVer1:Ref_Number,STOCKVer1:Part_Number,STOCKVer1:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(STOCKVer1:Location,STOCKVer1:Manufacturer,STOCKVer1:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(STOCKVer1:Location,STOCKVer1:Accessory,STOCKVer1:Manufacturer,STOCKVer1:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(STOCKVer1:Location,STOCKVer1:Part_Number,STOCKVer1:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(STOCKVer1:Ref_Number,STOCKVer1:Part_Number,STOCKVer1:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(STOCKVer1:Minimum_Stock,STOCKVer1:Location,STOCKVer1:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(STOCKVer1:Minimum_Stock,STOCKVer1:Location,STOCKVer1:Description),DUP,NOCASE
SecondLocKey             KEY(STOCKVer1:Location,STOCKVer1:Shelf_Location,STOCKVer1:Second_Location,STOCKVer1:Part_Number),DUP,NOCASE
RequestedKey             KEY(STOCKVer1:QuantityRequested,STOCKVer1:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(STOCKVer1:ExchangeUnit,STOCKVer1:Location,STOCKVer1:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(STOCKVer1:ExchangeUnit,STOCKVer1:Location,STOCKVer1:Description),DUP,NOCASE
DateBookedKey            KEY(STOCKVer1:DateBooked),DUP,NOCASE
Supplier_Only_Key        KEY(STOCKVer1:Supplier),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
ReturnFaultySpare           BYTE
ChargeablePartOnly          BYTE
AttachBySolder              BYTE
AllowDuplicate              BYTE
RetailMarkup                REAL
VirtPurchaseCost            REAL
VirtTradeCost               REAL
VirtRetailCost              REAL
VirtTradeMarkup             REAL
VirtRetailMarkup            REAL
AveragePurchaseCost         REAL
PurchaseMarkUp              REAL
RepairLevel                 LONG
SkillLevel                  LONG
DateBooked                  DATE
AccWarrantyPeriod           LONG
ExcludeLevel12Repair        BYTE
                         END
                       END
!------------ End File: STOCK, version 1 ------------------

!------------ File: STOCK, version 2 ----------------------
!------------ modified 14.07.2010 at 10:10:00 -----------------
MOD:stFileName:STOCKVer2  STRING(260)
STOCKVer2              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STOCKVer2),PRE(STOCKVer2),CREATE
Ref_Number_Key           KEY(STOCKVer2:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(STOCKVer2:Sundry_Item),DUP,NOCASE
Description_Key          KEY(STOCKVer2:Location,STOCKVer2:Description),DUP,NOCASE
Description_Accessory_Key KEY(STOCKVer2:Location,STOCKVer2:Suspend,STOCKVer2:Accessory,STOCKVer2:Description),DUP,NOCASE
Shelf_Location_Key       KEY(STOCKVer2:Location,STOCKVer2:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(STOCKVer2:Location,STOCKVer2:Suspend,STOCKVer2:Accessory,STOCKVer2:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(STOCKVer2:Location,STOCKVer2:Suspend,STOCKVer2:Accessory,STOCKVer2:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(STOCKVer2:Manufacturer,STOCKVer2:Part_Number),DUP,NOCASE
Supplier_Key             KEY(STOCKVer2:Location,STOCKVer2:Suspend,STOCKVer2:Supplier),DUP,NOCASE
Location_Key             KEY(STOCKVer2:Location,STOCKVer2:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(STOCKVer2:Location,STOCKVer2:Suspend,STOCKVer2:Accessory,STOCKVer2:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(STOCKVer2:Location,STOCKVer2:Ref_Number,STOCKVer2:Part_Number,STOCKVer2:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(STOCKVer2:Location,STOCKVer2:Manufacturer,STOCKVer2:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(STOCKVer2:Location,STOCKVer2:Suspend,STOCKVer2:Accessory,STOCKVer2:Manufacturer,STOCKVer2:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(STOCKVer2:Location,STOCKVer2:Part_Number,STOCKVer2:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(STOCKVer2:Ref_Number,STOCKVer2:Part_Number,STOCKVer2:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(STOCKVer2:Minimum_Stock,STOCKVer2:Location,STOCKVer2:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(STOCKVer2:Minimum_Stock,STOCKVer2:Location,STOCKVer2:Description),DUP,NOCASE
SecondLocKey             KEY(STOCKVer2:Location,STOCKVer2:Shelf_Location,STOCKVer2:Second_Location,STOCKVer2:Part_Number),DUP,NOCASE
RequestedKey             KEY(STOCKVer2:QuantityRequested,STOCKVer2:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(STOCKVer2:ExchangeUnit,STOCKVer2:Location,STOCKVer2:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(STOCKVer2:ExchangeUnit,STOCKVer2:Location,STOCKVer2:Description),DUP,NOCASE
DateBookedKey            KEY(STOCKVer2:DateBooked),DUP,NOCASE
Supplier_Only_Key        KEY(STOCKVer2:Supplier),DUP,NOCASE
LocPartSuspendKey        KEY(STOCKVer2:Location,STOCKVer2:Suspend,STOCKVer2:Part_Number),DUP,NOCASE
LocDescSuspendKey        KEY(STOCKVer2:Location,STOCKVer2:Suspend,STOCKVer2:Description),DUP,NOCASE
LocShelfSuspendKey       KEY(STOCKVer2:Location,STOCKVer2:Suspend,STOCKVer2:Shelf_Location),DUP,NOCASE
LocManSuspendKey         KEY(STOCKVer2:Location,STOCKVer2:Suspend,STOCKVer2:Manufacturer,STOCKVer2:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
ReturnFaultySpare           BYTE
ChargeablePartOnly          BYTE
AttachBySolder              BYTE
AllowDuplicate              BYTE
RetailMarkup                REAL
VirtPurchaseCost            REAL
VirtTradeCost               REAL
VirtRetailCost              REAL
VirtTradeMarkup             REAL
VirtRetailMarkup            REAL
AveragePurchaseCost         REAL
PurchaseMarkUp              REAL
RepairLevel                 LONG
SkillLevel                  LONG
DateBooked                  DATE
AccWarrantyPeriod           LONG
ExcludeLevel12Repair        BYTE
                         END
                       END
!------------ End File: STOCK, version 2 ------------------

!------------ File: STOCK, version 3 ----------------------
!------------ modified 28.06.2011 at 15:59:14 -----------------
MOD:stFileName:STOCKVer3  STRING(260)
STOCKVer3              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STOCKVer3),PRE(STOCKVer3),CREATE
Ref_Number_Key           KEY(STOCKVer3:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(STOCKVer3:Sundry_Item),DUP,NOCASE
Description_Key          KEY(STOCKVer3:Location,STOCKVer3:Description),DUP,NOCASE
Description_Accessory_Key KEY(STOCKVer3:Location,STOCKVer3:Suspend,STOCKVer3:Accessory,STOCKVer3:Description),DUP,NOCASE
Shelf_Location_Key       KEY(STOCKVer3:Location,STOCKVer3:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(STOCKVer3:Location,STOCKVer3:Suspend,STOCKVer3:Accessory,STOCKVer3:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(STOCKVer3:Location,STOCKVer3:Suspend,STOCKVer3:Accessory,STOCKVer3:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(STOCKVer3:Manufacturer,STOCKVer3:Part_Number),DUP,NOCASE
Supplier_Key             KEY(STOCKVer3:Location,STOCKVer3:Suspend,STOCKVer3:Supplier),DUP,NOCASE
Location_Key             KEY(STOCKVer3:Location,STOCKVer3:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(STOCKVer3:Location,STOCKVer3:Suspend,STOCKVer3:Accessory,STOCKVer3:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(STOCKVer3:Location,STOCKVer3:Ref_Number,STOCKVer3:Part_Number,STOCKVer3:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(STOCKVer3:Location,STOCKVer3:Manufacturer,STOCKVer3:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(STOCKVer3:Location,STOCKVer3:Suspend,STOCKVer3:Accessory,STOCKVer3:Manufacturer,STOCKVer3:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(STOCKVer3:Location,STOCKVer3:Part_Number,STOCKVer3:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(STOCKVer3:Ref_Number,STOCKVer3:Part_Number,STOCKVer3:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(STOCKVer3:Minimum_Stock,STOCKVer3:Location,STOCKVer3:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(STOCKVer3:Minimum_Stock,STOCKVer3:Location,STOCKVer3:Description),DUP,NOCASE
SecondLocKey             KEY(STOCKVer3:Location,STOCKVer3:Shelf_Location,STOCKVer3:Second_Location,STOCKVer3:Part_Number),DUP,NOCASE
RequestedKey             KEY(STOCKVer3:QuantityRequested,STOCKVer3:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(STOCKVer3:ExchangeUnit,STOCKVer3:Location,STOCKVer3:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(STOCKVer3:ExchangeUnit,STOCKVer3:Location,STOCKVer3:Description),DUP,NOCASE
DateBookedKey            KEY(STOCKVer3:DateBooked),DUP,NOCASE
Supplier_Only_Key        KEY(STOCKVer3:Supplier),DUP,NOCASE
LocPartSuspendKey        KEY(STOCKVer3:Location,STOCKVer3:Suspend,STOCKVer3:Part_Number),DUP,NOCASE
LocDescSuspendKey        KEY(STOCKVer3:Location,STOCKVer3:Suspend,STOCKVer3:Description),DUP,NOCASE
LocShelfSuspendKey       KEY(STOCKVer3:Location,STOCKVer3:Suspend,STOCKVer3:Shelf_Location),DUP,NOCASE
LocManSuspendKey         KEY(STOCKVer3:Location,STOCKVer3:Suspend,STOCKVer3:Manufacturer,STOCKVer3:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
ReturnFaultySpare           BYTE
ChargeablePartOnly          BYTE
AttachBySolder              BYTE
AllowDuplicate              BYTE
RetailMarkup                REAL
VirtPurchaseCost            REAL
VirtTradeCost               REAL
VirtRetailCost              REAL
VirtTradeMarkup             REAL
VirtRetailMarkup            REAL
AveragePurchaseCost         REAL
PurchaseMarkUp              REAL
RepairLevel                 LONG
SkillLevel                  LONG
DateBooked                  DATE
AccWarrantyPeriod           LONG
ExcludeLevel12Repair        BYTE
ExchangeOrderCap            LONG
                         END
                       END
!------------ End File: STOCK, version 3 ------------------

!------------ File: STOCK, version 4 ----------------------
!------------ modified 01.07.2011 at 13:09:13 -----------------
MOD:stFileName:STOCKVer4  STRING(260)
STOCKVer4              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STOCKVer4),PRE(STOCKVer4),CREATE
Ref_Number_Key           KEY(STOCKVer4:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(STOCKVer4:Sundry_Item),DUP,NOCASE
Description_Key          KEY(STOCKVer4:Location,STOCKVer4:Description),DUP,NOCASE
Description_Accessory_Key KEY(STOCKVer4:Location,STOCKVer4:Suspend,STOCKVer4:Accessory,STOCKVer4:Description),DUP,NOCASE
Shelf_Location_Key       KEY(STOCKVer4:Location,STOCKVer4:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(STOCKVer4:Location,STOCKVer4:Suspend,STOCKVer4:Accessory,STOCKVer4:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(STOCKVer4:Location,STOCKVer4:Suspend,STOCKVer4:Accessory,STOCKVer4:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(STOCKVer4:Manufacturer,STOCKVer4:Part_Number),DUP,NOCASE
Supplier_Key             KEY(STOCKVer4:Location,STOCKVer4:Suspend,STOCKVer4:Supplier),DUP,NOCASE
Location_Key             KEY(STOCKVer4:Location,STOCKVer4:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(STOCKVer4:Location,STOCKVer4:Suspend,STOCKVer4:Accessory,STOCKVer4:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(STOCKVer4:Location,STOCKVer4:Ref_Number,STOCKVer4:Part_Number,STOCKVer4:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(STOCKVer4:Location,STOCKVer4:Manufacturer,STOCKVer4:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(STOCKVer4:Location,STOCKVer4:Suspend,STOCKVer4:Accessory,STOCKVer4:Manufacturer,STOCKVer4:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(STOCKVer4:Location,STOCKVer4:Part_Number,STOCKVer4:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(STOCKVer4:Ref_Number,STOCKVer4:Part_Number,STOCKVer4:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(STOCKVer4:Minimum_Stock,STOCKVer4:Location,STOCKVer4:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(STOCKVer4:Minimum_Stock,STOCKVer4:Location,STOCKVer4:Description),DUP,NOCASE
SecondLocKey             KEY(STOCKVer4:Location,STOCKVer4:Shelf_Location,STOCKVer4:Second_Location,STOCKVer4:Part_Number),DUP,NOCASE
RequestedKey             KEY(STOCKVer4:QuantityRequested,STOCKVer4:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(STOCKVer4:ExchangeUnit,STOCKVer4:Location,STOCKVer4:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(STOCKVer4:ExchangeUnit,STOCKVer4:Location,STOCKVer4:Description),DUP,NOCASE
DateBookedKey            KEY(STOCKVer4:DateBooked),DUP,NOCASE
Supplier_Only_Key        KEY(STOCKVer4:Supplier),DUP,NOCASE
LocPartSuspendKey        KEY(STOCKVer4:Location,STOCKVer4:Suspend,STOCKVer4:Part_Number),DUP,NOCASE
LocDescSuspendKey        KEY(STOCKVer4:Location,STOCKVer4:Suspend,STOCKVer4:Description),DUP,NOCASE
LocShelfSuspendKey       KEY(STOCKVer4:Location,STOCKVer4:Suspend,STOCKVer4:Shelf_Location),DUP,NOCASE
LocManSuspendKey         KEY(STOCKVer4:Location,STOCKVer4:Suspend,STOCKVer4:Manufacturer,STOCKVer4:Part_Number),DUP,NOCASE
ExchangeModelKey         KEY(STOCKVer4:Location,STOCKVer4:ExchangeManufacturer,STOCKVer4:ExchangeModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
ReturnFaultySpare           BYTE
ChargeablePartOnly          BYTE
AttachBySolder              BYTE
AllowDuplicate              BYTE
RetailMarkup                REAL
VirtPurchaseCost            REAL
VirtTradeCost               REAL
VirtRetailCost              REAL
VirtTradeMarkup             REAL
VirtRetailMarkup            REAL
AveragePurchaseCost         REAL
PurchaseMarkUp              REAL
RepairLevel                 LONG
SkillLevel                  LONG
DateBooked                  DATE
AccWarrantyPeriod           LONG
ExcludeLevel12Repair        BYTE
ExchangeOrderCap            LONG
ExchangeManufacturer        STRING(30)
ExchangeModelNumber         STRING(30)
                         END
                       END
!------------ End File: STOCK, version 4 ------------------

!------------ File: STOCK, version 5 ----------------------
!------------ modified 01.07.2011 at 13:39:06 -----------------
MOD:stFileName:STOCKVer5  STRING(260)
STOCKVer5              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STOCKVer5),PRE(STOCKVer5),CREATE
Ref_Number_Key           KEY(STOCKVer5:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(STOCKVer5:Sundry_Item),DUP,NOCASE
Description_Key          KEY(STOCKVer5:Location,STOCKVer5:Description),DUP,NOCASE
Description_Accessory_Key KEY(STOCKVer5:Location,STOCKVer5:Suspend,STOCKVer5:Accessory,STOCKVer5:Description),DUP,NOCASE
Shelf_Location_Key       KEY(STOCKVer5:Location,STOCKVer5:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(STOCKVer5:Location,STOCKVer5:Suspend,STOCKVer5:Accessory,STOCKVer5:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(STOCKVer5:Location,STOCKVer5:Suspend,STOCKVer5:Accessory,STOCKVer5:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(STOCKVer5:Manufacturer,STOCKVer5:Part_Number),DUP,NOCASE
Supplier_Key             KEY(STOCKVer5:Location,STOCKVer5:Suspend,STOCKVer5:Supplier),DUP,NOCASE
Location_Key             KEY(STOCKVer5:Location,STOCKVer5:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(STOCKVer5:Location,STOCKVer5:Suspend,STOCKVer5:Accessory,STOCKVer5:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(STOCKVer5:Location,STOCKVer5:Ref_Number,STOCKVer5:Part_Number,STOCKVer5:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(STOCKVer5:Location,STOCKVer5:Manufacturer,STOCKVer5:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(STOCKVer5:Location,STOCKVer5:Suspend,STOCKVer5:Accessory,STOCKVer5:Manufacturer,STOCKVer5:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(STOCKVer5:Location,STOCKVer5:Part_Number,STOCKVer5:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(STOCKVer5:Ref_Number,STOCKVer5:Part_Number,STOCKVer5:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(STOCKVer5:Minimum_Stock,STOCKVer5:Location,STOCKVer5:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(STOCKVer5:Minimum_Stock,STOCKVer5:Location,STOCKVer5:Description),DUP,NOCASE
SecondLocKey             KEY(STOCKVer5:Location,STOCKVer5:Shelf_Location,STOCKVer5:Second_Location,STOCKVer5:Part_Number),DUP,NOCASE
RequestedKey             KEY(STOCKVer5:QuantityRequested,STOCKVer5:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(STOCKVer5:ExchangeUnit,STOCKVer5:Location,STOCKVer5:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(STOCKVer5:ExchangeUnit,STOCKVer5:Location,STOCKVer5:Description),DUP,NOCASE
DateBookedKey            KEY(STOCKVer5:DateBooked),DUP,NOCASE
Supplier_Only_Key        KEY(STOCKVer5:Supplier),DUP,NOCASE
LocPartSuspendKey        KEY(STOCKVer5:Location,STOCKVer5:Suspend,STOCKVer5:Part_Number),DUP,NOCASE
LocDescSuspendKey        KEY(STOCKVer5:Location,STOCKVer5:Suspend,STOCKVer5:Description),DUP,NOCASE
LocShelfSuspendKey       KEY(STOCKVer5:Location,STOCKVer5:Suspend,STOCKVer5:Shelf_Location),DUP,NOCASE
LocManSuspendKey         KEY(STOCKVer5:Location,STOCKVer5:Suspend,STOCKVer5:Manufacturer,STOCKVer5:Part_Number),DUP,NOCASE
ExchangeModelKey         KEY(STOCKVer5:Location,STOCKVer5:Manufacturer,STOCKVer5:ExchangeManufacturer),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
ReturnFaultySpare           BYTE
ChargeablePartOnly          BYTE
AttachBySolder              BYTE
AllowDuplicate              BYTE
RetailMarkup                REAL
VirtPurchaseCost            REAL
VirtTradeCost               REAL
VirtRetailCost              REAL
VirtTradeMarkup             REAL
VirtRetailMarkup            REAL
AveragePurchaseCost         REAL
PurchaseMarkUp              REAL
RepairLevel                 LONG
SkillLevel                  LONG
DateBooked                  DATE
AccWarrantyPeriod           LONG
ExcludeLevel12Repair        BYTE
ExchangeOrderCap            LONG
ExchangeManufacturer        STRING(30)
                         END
                       END
!------------ End File: STOCK, version 5 ------------------

!------------ File: STOCK, version 6 ----------------------
!------------ modified 18.01.2012 at 16:20:35 -----------------
MOD:stFileName:STOCKVer6  STRING(260)
STOCKVer6              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STOCKVer6),PRE(STOCKVer6),CREATE
Ref_Number_Key           KEY(STOCKVer6:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(STOCKVer6:Sundry_Item),DUP,NOCASE
Description_Key          KEY(STOCKVer6:Location,STOCKVer6:Description),DUP,NOCASE
Description_Accessory_Key KEY(STOCKVer6:Location,STOCKVer6:Suspend,STOCKVer6:Accessory,STOCKVer6:Description),DUP,NOCASE
Shelf_Location_Key       KEY(STOCKVer6:Location,STOCKVer6:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(STOCKVer6:Location,STOCKVer6:Suspend,STOCKVer6:Accessory,STOCKVer6:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(STOCKVer6:Location,STOCKVer6:Suspend,STOCKVer6:Accessory,STOCKVer6:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(STOCKVer6:Manufacturer,STOCKVer6:Part_Number),DUP,NOCASE
Supplier_Key             KEY(STOCKVer6:Location,STOCKVer6:Suspend,STOCKVer6:Supplier),DUP,NOCASE
Location_Key             KEY(STOCKVer6:Location,STOCKVer6:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(STOCKVer6:Location,STOCKVer6:Suspend,STOCKVer6:Accessory,STOCKVer6:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(STOCKVer6:Location,STOCKVer6:Ref_Number,STOCKVer6:Part_Number,STOCKVer6:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(STOCKVer6:Location,STOCKVer6:Manufacturer,STOCKVer6:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(STOCKVer6:Location,STOCKVer6:Suspend,STOCKVer6:Accessory,STOCKVer6:Manufacturer,STOCKVer6:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(STOCKVer6:Location,STOCKVer6:Part_Number,STOCKVer6:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(STOCKVer6:Ref_Number,STOCKVer6:Part_Number,STOCKVer6:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(STOCKVer6:Minimum_Stock,STOCKVer6:Location,STOCKVer6:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(STOCKVer6:Minimum_Stock,STOCKVer6:Location,STOCKVer6:Description),DUP,NOCASE
SecondLocKey             KEY(STOCKVer6:Location,STOCKVer6:Shelf_Location,STOCKVer6:Second_Location,STOCKVer6:Part_Number),DUP,NOCASE
RequestedKey             KEY(STOCKVer6:QuantityRequested,STOCKVer6:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(STOCKVer6:ExchangeUnit,STOCKVer6:Location,STOCKVer6:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(STOCKVer6:ExchangeUnit,STOCKVer6:Location,STOCKVer6:Description),DUP,NOCASE
DateBookedKey            KEY(STOCKVer6:DateBooked),DUP,NOCASE
Supplier_Only_Key        KEY(STOCKVer6:Supplier),DUP,NOCASE
LocPartSuspendKey        KEY(STOCKVer6:Location,STOCKVer6:Suspend,STOCKVer6:Part_Number),DUP,NOCASE
LocDescSuspendKey        KEY(STOCKVer6:Location,STOCKVer6:Suspend,STOCKVer6:Description),DUP,NOCASE
LocShelfSuspendKey       KEY(STOCKVer6:Location,STOCKVer6:Suspend,STOCKVer6:Shelf_Location),DUP,NOCASE
LocManSuspendKey         KEY(STOCKVer6:Location,STOCKVer6:Suspend,STOCKVer6:Manufacturer,STOCKVer6:Part_Number),DUP,NOCASE
ExchangeModelKey         KEY(STOCKVer6:Location,STOCKVer6:Manufacturer,STOCKVer6:ExchangeModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
ReturnFaultySpare           BYTE
ChargeablePartOnly          BYTE
AttachBySolder              BYTE
AllowDuplicate              BYTE
RetailMarkup                REAL
VirtPurchaseCost            REAL
VirtTradeCost               REAL
VirtRetailCost              REAL
VirtTradeMarkup             REAL
VirtRetailMarkup            REAL
AveragePurchaseCost         REAL
PurchaseMarkUp              REAL
RepairLevel                 LONG
SkillLevel                  LONG
DateBooked                  DATE
AccWarrantyPeriod           LONG
ExcludeLevel12Repair        BYTE
ExchangeOrderCap            LONG
ExchangeModelNumber         STRING(30)
LoanUnit                    BYTE
LoanModelNumber             STRING(30)
                         END
                       END
!------------ End File: STOCK, version 6 ------------------

!------------ File: LOAN, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:LOANVer1  STRING(260)
LOANVer1               FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:LOANVer1),PRE(LOANVer1)
Ref_Number_Key           KEY(LOANVer1:Ref_Number),NOCASE,PRIMARY
ESN_Only_Key             KEY(LOANVer1:ESN),DUP,NOCASE
MSN_Only_Key             KEY(LOANVer1:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(LOANVer1:Stock_Type,LOANVer1:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(LOANVer1:Stock_Type,LOANVer1:Model_Number,LOANVer1:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(LOANVer1:Stock_Type,LOANVer1:ESN),DUP,NOCASE
MSN_Key                  KEY(LOANVer1:Stock_Type,LOANVer1:MSN),DUP,NOCASE
ESN_Available_Key        KEY(LOANVer1:Available,LOANVer1:Stock_Type,LOANVer1:ESN),DUP,NOCASE
MSN_Available_Key        KEY(LOANVer1:Available,LOANVer1:Stock_Type,LOANVer1:MSN),DUP,NOCASE
Ref_Available_Key        KEY(LOANVer1:Available,LOANVer1:Stock_Type,LOANVer1:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(LOANVer1:Available,LOANVer1:Stock_Type,LOANVer1:Model_Number,LOANVer1:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(LOANVer1:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(LOANVer1:Model_Number,LOANVer1:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(LOANVer1:Available,LOANVer1:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(LOANVer1:Available,LOANVer1:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(LOANVer1:Available,LOANVer1:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(LOANVer1:Available,LOANVer1:Model_Number,LOANVer1:Ref_Number),DUP,NOCASE
DateBookedKey            KEY(LOANVer1:Date_Booked),DUP,NOCASE
LocStockAvailIMEIKey     KEY(LOANVer1:Location,LOANVer1:Stock_Type,LOANVer1:Available,LOANVer1:ESN),DUP,NOCASE
LocStockIMEIKey          KEY(LOANVer1:Location,LOANVer1:Stock_Type,LOANVer1:ESN),DUP,NOCASE
LocIMEIKey               KEY(LOANVer1:Location,LOANVer1:ESN),DUP,NOCASE
LocStockAvailRefKey      KEY(LOANVer1:Location,LOANVer1:Stock_Type,LOANVer1:Available,LOANVer1:Ref_Number),DUP,NOCASE
LocStockRefKey           KEY(LOANVer1:Location,LOANVer1:Stock_Type,LOANVer1:Ref_Number),DUP,NOCASE
LocRefKey                KEY(LOANVer1:Location,LOANVer1:Ref_Number),DUP,NOCASE
LocStockAvailModelKey    KEY(LOANVer1:Location,LOANVer1:Stock_Type,LOANVer1:Available,LOANVer1:Model_Number,LOANVer1:Ref_Number),DUP,NOCASE
LocStockModelKey         KEY(LOANVer1:Location,LOANVer1:Stock_Type,LOANVer1:Model_Number,LOANVer1:Ref_Number),DUP,NOCASE
LocModelKey              KEY(LOANVer1:Location,LOANVer1:Model_Number,LOANVer1:Ref_Number),DUP,NOCASE
LocStockAvailMSNKey      KEY(LOANVer1:Location,LOANVer1:Stock_Type,LOANVer1:Available,LOANVer1:MSN),DUP,NOCASE
LocStockMSNKey           KEY(LOANVer1:Location,LOANVer1:Stock_Type,LOANVer1:MSN),DUP,NOCASE
LocMSNKey                KEY(LOANVer1:Location,LOANVer1:MSN),DUP,NOCASE
AvailLocIMEI             KEY(LOANVer1:Available,LOANVer1:Location,LOANVer1:ESN),DUP,NOCASE
AvailLocMSN              KEY(LOANVer1:Available,LOANVer1:Location,LOANVer1:MSN),DUP,NOCASE
AvailLocRef              KEY(LOANVer1:Available,LOANVer1:Location,LOANVer1:Ref_Number),DUP,NOCASE
AvailLocModel            KEY(LOANVer1:Available,LOANVer1:Location,LOANVer1:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
DummyField                  STRING(1)
StatusChangeDate            DATE
                         END
                       END
!------------ End File: LOAN, version 1 ------------------

!------------ File: SMSMAIL, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:SMSMAILVer1  STRING(260)
SMSMAILVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSMAILVer1),PRE(SMSMAILVer1)
RecordNumberKey          KEY(SMSMAILVer1:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(SMSMAILVer1:SendToSMS,SMSMAILVer1:SMSSent),DUP,NOCASE
EmailSentKey             KEY(SMSMAILVer1:SendToEmail,SMSMAILVer1:EmailSent),DUP,NOCASE
RefSMSSentKey            KEY(SMSMAILVer1:RefNumber,SMSMAILVer1:MSISDN,SMSMAILVer1:MSG,SMSMAILVer1:SendToSMS,SMSMAILVer1:SMSSent),DUP,NOCASE
RefEmailSentKey          KEY(SMSMAILVer1:RefNumber,SMSMAILVer1:MSISDN,SMSMAILVer1:MSG,SMSMAILVer1:SendToEmail,SMSMAILVer1:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(SMSMAILVer1:SMSSent,SMSMAILVer1:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(SMSMAILVer1:EmailSent,SMSMAILVer1:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(SMSMAILVer1:RefNumber,SMSMAILVer1:DateInserted,SMSMAILVer1:TimeInserted),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(160)
EmailAddress                STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
                         END
                       END
!------------ End File: SMSMAIL, version 1 ------------------

!------------ File: SMSMAIL, version 2 ----------------------
!------------ modified 28.06.2010 at 11:27:28 -----------------
MOD:stFileName:SMSMAILVer2  STRING(260)
SMSMAILVer2            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSMAILVer2),PRE(SMSMAILVer2),CREATE
RecordNumberKey          KEY(SMSMAILVer2:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(SMSMAILVer2:SendToSMS,SMSMAILVer2:SMSSent),DUP,NOCASE
EmailSentKey             KEY(SMSMAILVer2:SendToEmail,SMSMAILVer2:EmailSent),DUP,NOCASE
RefSMSSentKey            KEY(SMSMAILVer2:RefNumber,SMSMAILVer2:MSISDN,SMSMAILVer2:MSG,SMSMAILVer2:SendToSMS,SMSMAILVer2:SMSSent),DUP,NOCASE
RefEmailSentKey          KEY(SMSMAILVer2:RefNumber,SMSMAILVer2:MSISDN,SMSMAILVer2:MSG,SMSMAILVer2:SendToEmail,SMSMAILVer2:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(SMSMAILVer2:SMSSent,SMSMAILVer2:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(SMSMAILVer2:EmailSent,SMSMAILVer2:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(SMSMAILVer2:RefNumber,SMSMAILVer2:DateInserted,SMSMAILVer2:TimeInserted),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
                         END
                       END
!------------ End File: SMSMAIL, version 2 ------------------

!------------ File: SMSMAIL, version 3 ----------------------
!------------ modified 28.06.2010 at 11:38:23 -----------------
MOD:stFileName:SMSMAILVer3  STRING(260)
SMSMAILVer3            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSMAILVer3),PRE(SMSMAILVer3),CREATE
RecordNumberKey          KEY(SMSMAILVer3:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(SMSMAILVer3:SendToSMS,SMSMAILVer3:SMSSent),DUP,NOCASE
EmailSentKey             KEY(SMSMAILVer3:SendToEmail,SMSMAILVer3:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(SMSMAILVer3:SMSSent,SMSMAILVer3:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(SMSMAILVer3:EmailSent,SMSMAILVer3:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(SMSMAILVer3:RefNumber,SMSMAILVer3:DateInserted,SMSMAILVer3:TimeInserted),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
                         END
                       END
!------------ End File: SMSMAIL, version 3 ------------------

!------------ File: SMSMAIL, version 4 ----------------------
!------------ modified 28.06.2010 at 12:01:48 -----------------
MOD:stFileName:SMSMAILVer4  STRING(260)
SMSMAILVer4            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSMAILVer4),PRE(SMSMAILVer4),CREATE
RecordNumberKey          KEY(SMSMAILVer4:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(SMSMAILVer4:SendToSMS,SMSMAILVer4:SMSSent),DUP,NOCASE
EmailSentKey             KEY(SMSMAILVer4:SendToEmail,SMSMAILVer4:EmailSent),DUP,NOCASE
RefSMSSentKey            KEY(SMSMAILVer4:RefNumber,SMSMAILVer4:MSISDN,SMSMAILVer4:MSG,SMSMAILVer4:SendToSMS,SMSMAILVer4:SMSSent),DUP,NOCASE
RefEmailSentKey          KEY(SMSMAILVer4:RefNumber,SMSMAILVer4:MSISDN,SMSMAILVer4:MSG,SMSMAILVer4:SendToEmail,SMSMAILVer4:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(SMSMAILVer4:SMSSent,SMSMAILVer4:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(SMSMAILVer4:EmailSent,SMSMAILVer4:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(SMSMAILVer4:RefNumber,SMSMAILVer4:DateInserted,SMSMAILVer4:TimeInserted),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
                         END
                       END
!------------ End File: SMSMAIL, version 4 ------------------

!------------ File: SMSMAIL, version 5 ----------------------
!------------ modified 28.06.2010 at 12:07:42 -----------------
MOD:stFileName:SMSMAILVer5  STRING(260)
SMSMAILVer5            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSMAILVer5),PRE(SMSMAILVer5),CREATE
RecordNumberKey          KEY(SMSMAILVer5:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(SMSMAILVer5:SendToSMS,SMSMAILVer5:SMSSent),DUP,NOCASE
EmailSentKey             KEY(SMSMAILVer5:SendToEmail,SMSMAILVer5:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(SMSMAILVer5:SMSSent,SMSMAILVer5:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(SMSMAILVer5:EmailSent,SMSMAILVer5:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(SMSMAILVer5:RefNumber,SMSMAILVer5:DateInserted,SMSMAILVer5:TimeInserted),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
                         END
                       END
!------------ End File: SMSMAIL, version 5 ------------------

!------------ File: SMSMAIL, version 6 ----------------------
!------------ modified 20.02.2012 at 15:28:42 -----------------
MOD:stFileName:SMSMAILVer6  STRING(260)
SMSMAILVer6            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSMAILVer6),PRE(SMSMAILVer6),CREATE
RecordNumberKey          KEY(SMSMAILVer6:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(SMSMAILVer6:SendToSMS,SMSMAILVer6:SMSSent),DUP,NOCASE
EmailSentKey             KEY(SMSMAILVer6:SendToEmail,SMSMAILVer6:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(SMSMAILVer6:SMSSent,SMSMAILVer6:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(SMSMAILVer6:EmailSent,SMSMAILVer6:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(SMSMAILVer6:RefNumber,SMSMAILVer6:DateInserted,SMSMAILVer6:TimeInserted),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
SMSType                     STRING(1)
                         END
                       END
!------------ End File: SMSMAIL, version 6 ------------------

!------------ File: SMSMAIL, version 7 ----------------------
!------------ modified 05.03.2012 at 11:11:07 -----------------
MOD:stFileName:SMSMAILVer7  STRING(260)
SMSMAILVer7            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSMAILVer7),PRE(SMSMAILVer7),CREATE
RecordNumberKey          KEY(SMSMAILVer7:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(SMSMAILVer7:SendToSMS,SMSMAILVer7:SMSSent),DUP,NOCASE
EmailSentKey             KEY(SMSMAILVer7:SendToEmail,SMSMAILVer7:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(SMSMAILVer7:SMSSent,SMSMAILVer7:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(SMSMAILVer7:EmailSent,SMSMAILVer7:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(SMSMAILVer7:RefNumber,SMSMAILVer7:DateInserted,SMSMAILVer7:TimeInserted),DUP,NOCASE
KeyMSISDN_DateDec        KEY(SMSMAILVer7:MSISDN,-SMSMAILVer7:DateSMSSent),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
SMSType                     STRING(1)
                         END
                       END
!------------ End File: SMSMAIL, version 7 ------------------

!------------ File: SMSMAIL, version 8 ----------------------
!------------ modified 05.03.2012 at 12:07:19 -----------------
MOD:stFileName:SMSMAILVer8  STRING(260)
SMSMAILVer8            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSMAILVer8),PRE(SMSMAILVer8),CREATE
RecordNumberKey          KEY(SMSMAILVer8:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(SMSMAILVer8:SendToSMS,SMSMAILVer8:SMSSent),DUP,NOCASE
EmailSentKey             KEY(SMSMAILVer8:SendToEmail,SMSMAILVer8:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(SMSMAILVer8:SMSSent,SMSMAILVer8:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(SMSMAILVer8:EmailSent,SMSMAILVer8:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(SMSMAILVer8:RefNumber,SMSMAILVer8:DateInserted,SMSMAILVer8:TimeInserted),DUP,NOCASE
KeyMSISDN_DateDec        KEY(SMSMAILVer8:MSISDN,-SMSMAILVer8:DateSMSSent),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
EmailAddress2               STRING(255)
EmailAddress3               STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
SMSType                     STRING(1)
                         END
                       END
!------------ End File: SMSMAIL, version 8 ------------------

!------------ File: SMSMAIL, version 9 ----------------------
!------------ modified 08.03.2012 at 14:48:22 -----------------
MOD:stFileName:SMSMAILVer9  STRING(260)
SMSMAILVer9            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSMAILVer9),PRE(SMSMAILVer9),CREATE
RecordNumberKey          KEY(SMSMAILVer9:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(SMSMAILVer9:SendToSMS,SMSMAILVer9:SMSSent),DUP,NOCASE
EmailSentKey             KEY(SMSMAILVer9:SendToEmail,SMSMAILVer9:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(SMSMAILVer9:SMSSent,SMSMAILVer9:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(SMSMAILVer9:EmailSent,SMSMAILVer9:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(SMSMAILVer9:RefNumber,SMSMAILVer9:DateInserted,SMSMAILVer9:TimeInserted),DUP,NOCASE
KeyMSISDN_DateDec        KEY(SMSMAILVer9:MSISDN,-SMSMAILVer9:DateSMSSent),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
EmailAddress2               STRING(255)
EmailAddress3               STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
SMSType                     STRING(1)
EmailSubject                STRING(255)
                         END
                       END
!------------ End File: SMSMAIL, version 9 ------------------

!------------ File: SMSMAIL, version 10 ----------------------
!------------ modified 27.03.2012 at 14:23:20 -----------------
MOD:stFileName:SMSMAILVer10  STRING(260)
SMSMAILVer10           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSMAILVer10),PRE(SMSMAILVer10),CREATE
RecordNumberKey          KEY(SMSMAILVer10:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(SMSMAILVer10:SendToSMS,SMSMAILVer10:SMSSent),DUP,NOCASE
EmailSentKey             KEY(SMSMAILVer10:SendToEmail,SMSMAILVer10:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(SMSMAILVer10:SMSSent,SMSMAILVer10:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(SMSMAILVer10:EmailSent,SMSMAILVer10:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(SMSMAILVer10:RefNumber,SMSMAILVer10:DateInserted,SMSMAILVer10:TimeInserted),DUP,NOCASE
KeyMSISDN_DateDec        KEY(SMSMAILVer10:MSISDN,-SMSMAILVer10:DateSMSSent),DUP,NOCASE
KeyRefDateTimeDec        KEY(SMSMAILVer10:RefNumber,-SMSMAILVer10:DateInserted,-SMSMAILVer10:TimeInserted),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
EmailAddress2               STRING(255)
EmailAddress3               STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
SMSType                     STRING(1)
EmailSubject                STRING(255)
                         END
                       END
!------------ End File: SMSMAIL, version 10 ------------------

!------------ File: LOANHIST, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:LOANHISTVer1  STRING(260)
LOANHISTVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:LOANHISTVer1),PRE(LOANHISTVer1)
Record_Number_Key        KEY(LOANHISTVer1:record_number),NOCASE,PRIMARY
Ref_Number_Key           KEY(LOANHISTVer1:Ref_Number,-LOANHISTVer1:Date,-LOANHISTVer1:Time),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                       END
!------------ End File: LOANHIST, version 1 ------------------

!------------ File: EXCHHIST, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:EXCHHISTVer1  STRING(260)
EXCHHISTVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:EXCHHISTVer1),PRE(EXCHHISTVer1)
Record_Number_Key        KEY(EXCHHISTVer1:record_number),NOCASE,PRIMARY
Ref_Number_Key           KEY(EXCHHISTVer1:Ref_Number,-EXCHHISTVer1:Date,-EXCHHISTVer1:Time),DUP,NOCASE
DateStatusKey            KEY(EXCHHISTVer1:Date,EXCHHISTVer1:Status,EXCHHISTVer1:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                       END
!------------ End File: EXCHHIST, version 1 ------------------

!------------ File: EXCHHIST, version 2 ----------------------
!------------ modified 29.06.2011 at 09:57:03 -----------------
MOD:stFileName:EXCHHISTVer2  STRING(260)
EXCHHISTVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:EXCHHISTVer2),PRE(EXCHHISTVer2),CREATE
Record_Number_Key        KEY(EXCHHISTVer2:record_number),NOCASE,PRIMARY
Ref_Number_Key           KEY(EXCHHISTVer2:Ref_Number,-EXCHHISTVer2:Date,-EXCHHISTVer2:Time),DUP,NOCASE
DateStatusKey            KEY(EXCHHISTVer2:Date,EXCHHISTVer2:Status,EXCHHISTVer2:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
record_number               LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(255)
                         END
                       END
!------------ End File: EXCHHIST, version 2 ------------------

!------------ File: EXCHHIST, version 3 ----------------------
!------------ modified 29.06.2011 at 20:25:30 -----------------
MOD:stFileName:EXCHHISTVer3  STRING(260)
EXCHHISTVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:EXCHHISTVer3),PRE(EXCHHISTVer3),CREATE
Record_Number_Key        KEY(EXCHHISTVer3:record_number),NOCASE,PRIMARY
Ref_Number_Key           KEY(EXCHHISTVer3:Ref_Number,-EXCHHISTVer3:Date,-EXCHHISTVer3:Time),DUP,NOCASE
DateStatusKey            KEY(EXCHHISTVer3:Date,EXCHHISTVer3:Status,EXCHHISTVer3:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
record_number               LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                       END
!------------ End File: EXCHHIST, version 3 ------------------

!------------ File: EXCHHIST, version 4 ----------------------
!------------ modified 29.06.2011 at 20:34:51 -----------------
MOD:stFileName:EXCHHISTVer4  STRING(260)
EXCHHISTVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:EXCHHISTVer4),PRE(EXCHHISTVer4),CREATE
Record_Number_Key        KEY(EXCHHISTVer4:record_number),NOCASE,PRIMARY
Ref_Number_Key           KEY(EXCHHISTVer4:Ref_Number,-EXCHHISTVer4:Date,-EXCHHISTVer4:Time),DUP,NOCASE
DateStatusKey            KEY(EXCHHISTVer4:Date,EXCHHISTVer4:Status,EXCHHISTVer4:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
record_number               LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(255)
                         END
                       END
!------------ End File: EXCHHIST, version 4 ------------------

!------------ File: AUDIT, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:AUDITVer1  STRING(260)
AUDITVer1              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:AUDITVer1),PRE(AUDITVer1)
Ref_Number_Key           KEY(AUDITVer1:Ref_Number,-AUDITVer1:Date,-AUDITVer1:Time,AUDITVer1:Action),DUP,NOCASE
Action_Key               KEY(AUDITVer1:Ref_Number,AUDITVer1:Action,-AUDITVer1:Date),DUP,NOCASE
User_Key                 KEY(AUDITVer1:Ref_Number,AUDITVer1:User,-AUDITVer1:Date),DUP,NOCASE
Record_Number_Key        KEY(AUDITVer1:record_number),NOCASE,PRIMARY
ActionOnlyKey            KEY(AUDITVer1:Action,AUDITVer1:Date),DUP,NOCASE
TypeRefKey               KEY(AUDITVer1:Ref_Number,AUDITVer1:Type,-AUDITVer1:Date,-AUDITVer1:Time,AUDITVer1:Action),DUP,NOCASE
TypeActionKey            KEY(AUDITVer1:Ref_Number,AUDITVer1:Type,AUDITVer1:Action,-AUDITVer1:Date),DUP,NOCASE
TypeUserKey              KEY(AUDITVer1:Ref_Number,AUDITVer1:Type,AUDITVer1:User,-AUDITVer1:Date),DUP,NOCASE
DateActionJobKey         KEY(AUDITVer1:Date,AUDITVer1:Action,AUDITVer1:Ref_Number),DUP,NOCASE
DateJobKey               KEY(AUDITVer1:Date,AUDITVer1:Ref_Number),DUP,NOCASE
DateTypeJobKey           KEY(AUDITVer1:Date,AUDITVer1:Type,AUDITVer1:Ref_Number),DUP,NOCASE
DateTypeActionKey        KEY(AUDITVer1:Date,AUDITVer1:Type,AUDITVer1:Action,AUDITVer1:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
DummyField                  STRING(2)
Notes                       STRING(255)
Type                        STRING(3)
                         END
                       END
!------------ End File: AUDIT, version 1 ------------------

!------------ File: USERS, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:USERSVer1  STRING(260)
USERSVer1              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:USERSVer1),PRE(USERSVer1)
User_Code_Key            KEY(USERSVer1:User_Code),NOCASE,PRIMARY
User_Type_Active_Key     KEY(USERSVer1:User_Type,USERSVer1:Active,USERSVer1:Surname),DUP,NOCASE
Surname_Active_Key       KEY(USERSVer1:Active,USERSVer1:Surname),DUP,NOCASE
User_Code_Active_Key     KEY(USERSVer1:Active,USERSVer1:User_Code),DUP,NOCASE
User_Type_Key            KEY(USERSVer1:User_Type,USERSVer1:Surname),DUP,NOCASE
surname_key              KEY(USERSVer1:Surname),DUP,NOCASE
password_key             KEY(USERSVer1:Password),NOCASE
Logged_In_Key            KEY(USERSVer1:Logged_In,USERSVer1:Surname),DUP,NOCASE
Team_Surname             KEY(USERSVer1:Team,USERSVer1:Surname),DUP,NOCASE
Active_Team_Surname      KEY(USERSVer1:Team,USERSVer1:Active,USERSVer1:Surname),DUP,NOCASE
LocationSurnameKey       KEY(USERSVer1:Location,USERSVer1:Surname),DUP,NOCASE
LocationForenameKey      KEY(USERSVer1:Location,USERSVer1:Forename),DUP,NOCASE
LocActiveSurnameKey      KEY(USERSVer1:Active,USERSVer1:Location,USERSVer1:Surname),DUP,NOCASE
LocActiveForenameKey     KEY(USERSVer1:Active,USERSVer1:Location,USERSVer1:Forename),DUP,NOCASE
TeamStatusKey            KEY(USERSVer1:IncludeInEngStatus,USERSVer1:Team,USERSVer1:Surname),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Forename                    STRING(30)
Surname                     STRING(30)
Password                    STRING(20)
User_Type                   STRING(15)
Job_Assignment              STRING(15)
Supervisor                  STRING(3)
User_Level                  STRING(30)
Logged_In                   STRING(1)
Logged_in_date              DATE
Logged_In_Time              TIME
Restrict_Logins             STRING(3)
Concurrent_Logins           REAL
Active                      STRING(3)
Team                        STRING(30)
Location                    STRING(30)
Repair_Target               REAL
SkillLevel                  LONG
StockFromLocationOnly       BYTE
EmailAddress                STRING(255)
RenewPassword               LONG
PasswordLastChanged         DATE
RestrictParts               BYTE
RestrictChargeable          BYTE
RestrictWarranty            BYTE
IncludeInEngStatus          BYTE
                         END
                       END
!------------ End File: USERS, version 1 ------------------

!------------ File: USERS, version 2 ----------------------
!------------ modified 11.05.2012 at 08:35:24 -----------------
MOD:stFileName:USERSVer2  STRING(260)
USERSVer2              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:USERSVer2),PRE(USERSVer2),CREATE
User_Code_Key            KEY(USERSVer2:User_Code),NOCASE,PRIMARY
User_Type_Active_Key     KEY(USERSVer2:User_Type,USERSVer2:Active,USERSVer2:Surname),DUP,NOCASE
Surname_Active_Key       KEY(USERSVer2:Active,USERSVer2:Surname),DUP,NOCASE
User_Code_Active_Key     KEY(USERSVer2:Active,USERSVer2:User_Code),DUP,NOCASE
User_Type_Key            KEY(USERSVer2:User_Type,USERSVer2:Surname),DUP,NOCASE
surname_key              KEY(USERSVer2:Surname),DUP,NOCASE
password_key             KEY(USERSVer2:Password),NOCASE
Logged_In_Key            KEY(USERSVer2:Logged_In,USERSVer2:Surname),DUP,NOCASE
Team_Surname             KEY(USERSVer2:Team,USERSVer2:Surname),DUP,NOCASE
Active_Team_Surname      KEY(USERSVer2:Team,USERSVer2:Active,USERSVer2:Surname),DUP,NOCASE
LocationSurnameKey       KEY(USERSVer2:Location,USERSVer2:Surname),DUP,NOCASE
LocationForenameKey      KEY(USERSVer2:Location,USERSVer2:Forename),DUP,NOCASE
LocActiveSurnameKey      KEY(USERSVer2:Active,USERSVer2:Location,USERSVer2:Surname),DUP,NOCASE
LocActiveForenameKey     KEY(USERSVer2:Active,USERSVer2:Location,USERSVer2:Forename),DUP,NOCASE
TeamStatusKey            KEY(USERSVer2:IncludeInEngStatus,USERSVer2:Team,USERSVer2:Surname),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Forename                    STRING(30)
Surname                     STRING(30)
Password                    STRING(20)
User_Type                   STRING(15)
Job_Assignment              STRING(15)
Supervisor                  STRING(3)
User_Level                  STRING(30)
Logged_In                   STRING(1)
Logged_in_date              DATE
Logged_In_Time              TIME
Restrict_Logins             STRING(3)
Concurrent_Logins           REAL
Active                      STRING(3)
Team                        STRING(30)
Location                    STRING(30)
Repair_Target               REAL
SkillLevel                  LONG
StockFromLocationOnly       BYTE
EmailAddress                STRING(255)
RenewPassword               LONG
PasswordLastChanged         DATE
RestrictParts               BYTE
RestrictChargeable          BYTE
RestrictWarranty            BYTE
IncludeInEngStatus          BYTE
MobileNumber                STRING(20)
                         END
                       END
!------------ End File: USERS, version 2 ------------------

!------------ File: USERS, version 3 ----------------------
!------------ modified 16.12.2014 at 13:36:57 -----------------
MOD:stFileName:USERSVer3  STRING(260)
USERSVer3              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:USERSVer3),PRE(USERSVer3),CREATE
User_Code_Key            KEY(USERSVer3:User_Code),NOCASE,PRIMARY
User_Type_Active_Key     KEY(USERSVer3:User_Type,USERSVer3:Active,USERSVer3:Surname),DUP,NOCASE
Surname_Active_Key       KEY(USERSVer3:Active,USERSVer3:Surname),DUP,NOCASE
User_Code_Active_Key     KEY(USERSVer3:Active,USERSVer3:User_Code),DUP,NOCASE
User_Type_Key            KEY(USERSVer3:User_Type,USERSVer3:Surname),DUP,NOCASE
surname_key              KEY(USERSVer3:Surname),DUP,NOCASE
password_key             KEY(USERSVer3:Password),NOCASE
Logged_In_Key            KEY(USERSVer3:Logged_In,USERSVer3:Surname),DUP,NOCASE
Team_Surname             KEY(USERSVer3:Team,USERSVer3:Surname),DUP,NOCASE
Active_Team_Surname      KEY(USERSVer3:Team,USERSVer3:Active,USERSVer3:Surname),DUP,NOCASE
LocationSurnameKey       KEY(USERSVer3:Location,USERSVer3:Surname),DUP,NOCASE
LocationForenameKey      KEY(USERSVer3:Location,USERSVer3:Forename),DUP,NOCASE
LocActiveSurnameKey      KEY(USERSVer3:Active,USERSVer3:Location,USERSVer3:Surname),DUP,NOCASE
LocActiveForenameKey     KEY(USERSVer3:Active,USERSVer3:Location,USERSVer3:Forename),DUP,NOCASE
TeamStatusKey            KEY(USERSVer3:IncludeInEngStatus,USERSVer3:Team,USERSVer3:Surname),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Forename                    STRING(30)
Surname                     STRING(30)
Password                    STRING(20)
User_Type                   STRING(15)
Job_Assignment              STRING(15)
Supervisor                  STRING(3)
User_Level                  STRING(30)
Logged_In                   STRING(1)
Logged_in_date              DATE
Logged_In_Time              TIME
Restrict_Logins             STRING(3)
Concurrent_Logins           REAL
Active                      STRING(3)
Team                        STRING(30)
Location                    STRING(30)
Repair_Target               REAL
SkillLevel                  LONG
StockFromLocationOnly       BYTE
EmailAddress                STRING(255)
RenewPassword               LONG
PasswordLastChanged         DATE
RestrictParts               BYTE
RestrictChargeable          BYTE
RestrictWarranty            BYTE
IncludeInEngStatus          BYTE
MobileNumber                STRING(20)
RRC_MII_Dealer_ID           STRING(30)
                         END
                       END
!------------ End File: USERS, version 3 ------------------

!------------ File: LOCSHELF, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:19 -----------------
MOD:stFileName:LOCSHELFVer1  STRING(260)
LOCSHELFVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:LOCSHELFVer1),PRE(LOCSHELFVer1)
Shelf_Location_Key       KEY(LOCSHELFVer1:Site_Location,LOCSHELFVer1:Shelf_Location),NOCASE,PRIMARY
Record                   RECORD,PRE()
Site_Location               STRING(30)
Shelf_Location              STRING(30)
                         END
                       END
!------------ End File: LOCSHELF, version 1 ------------------

!------------ File: RTNORDER, version 1 ----------------------
!------------ modified 06.09.2011 at 10:33:01 -----------------
MOD:stFileName:RTNORDERVer1  STRING(260)
RTNORDERVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer1),PRE(RTNORDERVer1)
RecordNumberKey          KEY(RTNORDERVer1:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer1:Location,RTNORDERVer1:ExchangeOrder,RTNORDERVer1:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer1:Location,RTNORDERVer1:ExchangeOrder,RTNORDERVer1:Status,RTNORDERVer1:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer1:OrderNumber,RTNORDERVer1:InvoiceNumber,RTNORDERVer1:ExchangeOrder,RTNORDERVer1:Status,RTNORDERVer1:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer1:OrderNumber,RTNORDERVer1:InvoiceNumber,RTNORDERVer1:ExchangeOrder,RTNORDERVer1:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
STOCKRefNumber              LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
                         END
                       END
!------------ End File: RTNORDER, version 1 ------------------

!------------ File: RTNORDER, version 2 ----------------------
!------------ modified 06.09.2011 at 12:38:27 -----------------
MOD:stFileName:RTNORDERVer2  STRING(260)
RTNORDERVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer2),PRE(RTNORDERVer2),CREATE
RecordNumberKey          KEY(RTNORDERVer2:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer2:Location,RTNORDERVer2:ExchangeOrder,RTNORDERVer2:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer2:Location,RTNORDERVer2:ExchangeOrder,RTNORDERVer2:Status,RTNORDERVer2:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer2:OrderNumber,RTNORDERVer2:InvoiceNumber,RTNORDERVer2:ExchangeOrder,RTNORDERVer2:Status,RTNORDERVer2:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer2:OrderNumber,RTNORDERVer2:InvoiceNumber,RTNORDERVer2:ExchangeOrder,RTNORDERVer2:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer2:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer2:WaybillNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
CreditNoteRequestNumber     LONG
CreditNoteUser              STRING(3)
CreditNoteDate              DATE
CreditNoteTime              TIME
WaybillNumber               LONG
                         END
                       END
!------------ End File: RTNORDER, version 2 ------------------

!------------ File: RTNORDER, version 3 ----------------------
!------------ modified 06.09.2011 at 13:53:46 -----------------
MOD:stFileName:RTNORDERVer3  STRING(260)
RTNORDERVer3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer3),PRE(RTNORDERVer3),CREATE
RecordNumberKey          KEY(RTNORDERVer3:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer3:Location,RTNORDERVer3:ExchangeOrder,RTNORDERVer3:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer3:Location,RTNORDERVer3:ExchangeOrder,RTNORDERVer3:Status,RTNORDERVer3:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer3:OrderNumber,RTNORDERVer3:InvoiceNumber,RTNORDERVer3:ExchangeOrder,RTNORDERVer3:Status,RTNORDERVer3:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer3:OrderNumber,RTNORDERVer3:InvoiceNumber,RTNORDERVer3:ExchangeOrder,RTNORDERVer3:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer3:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer3:WaybillNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
                         END
                       END
!------------ End File: RTNORDER, version 3 ------------------

!------------ File: RTNORDER, version 4 ----------------------
!------------ modified 06.09.2011 at 14:07:31 -----------------
MOD:stFileName:RTNORDERVer4  STRING(260)
RTNORDERVer4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer4),PRE(RTNORDERVer4),CREATE
RecordNumberKey          KEY(RTNORDERVer4:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer4:Location,RTNORDERVer4:ExchangeOrder,RTNORDERVer4:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer4:Location,RTNORDERVer4:ExchangeOrder,RTNORDERVer4:Status,RTNORDERVer4:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer4:OrderNumber,RTNORDERVer4:InvoiceNumber,RTNORDERVer4:ExchangeOrder,RTNORDERVer4:Status,RTNORDERVer4:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer4:OrderNumber,RTNORDERVer4:InvoiceNumber,RTNORDERVer4:ExchangeOrder,RTNORDERVer4:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer4:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer4:WaybillNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
ReturnType                  STRING(30)
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
                         END
                       END
!------------ End File: RTNORDER, version 4 ------------------

!------------ File: RTNORDER, version 5 ----------------------
!------------ modified 06.09.2011 at 14:22:47 -----------------
MOD:stFileName:RTNORDERVer5  STRING(260)
RTNORDERVer5           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer5),PRE(RTNORDERVer5),CREATE
RecordNumberKey          KEY(RTNORDERVer5:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer5:Location,RTNORDERVer5:ExchangeOrder,RTNORDERVer5:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer5:Location,RTNORDERVer5:ExchangeOrder,RTNORDERVer5:Status,RTNORDERVer5:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer5:OrderNumber,RTNORDERVer5:InvoiceNumber,RTNORDERVer5:ExchangeOrder,RTNORDERVer5:Status,RTNORDERVer5:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer5:OrderNumber,RTNORDERVer5:InvoiceNumber,RTNORDERVer5:ExchangeOrder,RTNORDERVer5:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer5:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer5:WaybillNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
ReturnType                  STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
ExchangePrice               REAL
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
                         END
                       END
!------------ End File: RTNORDER, version 5 ------------------

!------------ File: RTNORDER, version 6 ----------------------
!------------ modified 07.09.2011 at 11:42:34 -----------------
MOD:stFileName:RTNORDERVer6  STRING(260)
RTNORDERVer6           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer6),PRE(RTNORDERVer6),CREATE
RecordNumberKey          KEY(RTNORDERVer6:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer6:Location,RTNORDERVer6:ExchangeOrder,RTNORDERVer6:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer6:Location,RTNORDERVer6:ExchangeOrder,RTNORDERVer6:Status,RTNORDERVer6:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer6:OrderNumber,RTNORDERVer6:InvoiceNumber,RTNORDERVer6:ExchangeOrder,RTNORDERVer6:Status,RTNORDERVer6:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer6:OrderNumber,RTNORDERVer6:InvoiceNumber,RTNORDERVer6:ExchangeOrder,RTNORDERVer6:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer6:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer6:WaybillNumber),DUP,NOCASE
RefNumberKey             KEY(RTNORDERVer6:ExchangeOrder,RTNORDERVer6:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
ReturnType                  STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
ExchangePrice               REAL
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
                         END
                       END
!------------ End File: RTNORDER, version 6 ------------------

!------------ File: RTNORDER, version 7 ----------------------
!------------ modified 07.09.2011 at 13:40:47 -----------------
MOD:stFileName:RTNORDERVer7  STRING(260)
RTNORDERVer7           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer7),PRE(RTNORDERVer7),CREATE
RecordNumberKey          KEY(RTNORDERVer7:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer7:Location,RTNORDERVer7:ExchangeOrder,RTNORDERVer7:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer7:Location,RTNORDERVer7:ExchangeOrder,RTNORDERVer7:Status,RTNORDERVer7:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer7:OrderNumber,RTNORDERVer7:ExchangeOrder,RTNORDERVer7:Status,RTNORDERVer7:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer7:OrderNumber,RTNORDERVer7:ExchangeOrder,RTNORDERVer7:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer7:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer7:WaybillNumber),DUP,NOCASE
RefNumberKey             KEY(RTNORDERVer7:ExchangeOrder,RTNORDERVer7:RefNumber),DUP,NOCASE
OrderStatusReturnRefNoKey KEY(RTNORDERVer7:OrderNumber,RTNORDERVer7:ExchangeOrder,RTNORDERVer7:ReturnType,RTNORDERVer7:Status,RTNORDERVer7:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
ReturnType                  STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
ExchangePrice               REAL
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
                         END
                       END
!------------ End File: RTNORDER, version 7 ------------------

!------------ File: RTNORDER, version 8 ----------------------
!------------ modified 07.09.2011 at 14:22:22 -----------------
MOD:stFileName:RTNORDERVer8  STRING(260)
RTNORDERVer8           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer8),PRE(RTNORDERVer8),CREATE
RecordNumberKey          KEY(RTNORDERVer8:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer8:Location,RTNORDERVer8:ExchangeOrder,RTNORDERVer8:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer8:Location,RTNORDERVer8:ExchangeOrder,RTNORDERVer8:Status,RTNORDERVer8:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer8:OrderNumber,RTNORDERVer8:ExchangeOrder,RTNORDERVer8:Status,RTNORDERVer8:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer8:OrderNumber,RTNORDERVer8:ExchangeOrder,RTNORDERVer8:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer8:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer8:WaybillNumber),DUP,NOCASE
RefNumberKey             KEY(RTNORDERVer8:ExchangeOrder,RTNORDERVer8:RefNumber),DUP,NOCASE
OrderStatusReturnRefNoKey KEY(RTNORDERVer8:OrderNumber,RTNORDERVer8:ExchangeOrder,RTNORDERVer8:ReturnType,RTNORDERVer8:Status,RTNORDERVer8:RefNumber),DUP,NOCASE
LocArcOrdWaybillKey      KEY(RTNORDERVer8:Archived,RTNORDERVer8:Ordered,RTNORDERVer8:Location,RTNORDERVer8:WaybillNumber),DUP,NOCASE
ArcOrdWaybillKey         KEY(RTNORDERVer8:Archived,RTNORDERVer8:Ordered,RTNORDERVer8:WaybillNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archived                    BYTE
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
ReturnType                  STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
ExchangePrice               REAL
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
Ordered                     BYTE
DateOrdered                 DATE
TimeOrdered                 TIME
WhoOrdered                  STRING(3)
Received                    BYTE
DateReceived                DATE
TimeReceived                TIME
WhoReceived                 STRING(3)
                         END
                       END
!------------ End File: RTNORDER, version 8 ------------------

!------------ File: RTNORDER, version 9 ----------------------
!------------ modified 15.09.2011 at 15:52:14 -----------------
MOD:stFileName:RTNORDERVer9  STRING(260)
RTNORDERVer9           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer9),PRE(RTNORDERVer9),CREATE
RecordNumberKey          KEY(RTNORDERVer9:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer9:Location,RTNORDERVer9:ExchangeOrder,RTNORDERVer9:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer9:Location,RTNORDERVer9:ExchangeOrder,RTNORDERVer9:Status,RTNORDERVer9:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer9:OrderNumber,RTNORDERVer9:ExchangeOrder,RTNORDERVer9:Status,RTNORDERVer9:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer9:OrderNumber,RTNORDERVer9:ExchangeOrder,RTNORDERVer9:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer9:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer9:WaybillNumber),DUP,NOCASE
RefNumberKey             KEY(RTNORDERVer9:ExchangeOrder,RTNORDERVer9:RefNumber),DUP,NOCASE
OrderStatusReturnRefNoKey KEY(RTNORDERVer9:OrderNumber,RTNORDERVer9:ExchangeOrder,RTNORDERVer9:ReturnType,RTNORDERVer9:Status,RTNORDERVer9:RefNumber),DUP,NOCASE
LocArcOrdWaybillKey      KEY(RTNORDERVer9:Archived,RTNORDERVer9:Ordered,RTNORDERVer9:Location,RTNORDERVer9:WaybillNumber),DUP,NOCASE
ArcOrdWaybillKey         KEY(RTNORDERVer9:Archived,RTNORDERVer9:Ordered,RTNORDERVer9:WaybillNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archived                    BYTE
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
ReturnType                  STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
ExchangePrice               REAL
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
Ordered                     BYTE
DateOrdered                 DATE
TimeOrdered                 TIME
WhoOrdered                  STRING(3)
Received                    BYTE
DateReceived                DATE
TimeReceived                TIME
WhoReceived                 STRING(3)
WhoProcessed                STRING(3)
                         END
                       END
!------------ End File: RTNORDER, version 9 ------------------

!------------ File: RTNORDER, version 10 ----------------------
!------------ modified 26.09.2011 at 15:59:52 -----------------
MOD:stFileName:RTNORDERVer10  STRING(260)
RTNORDERVer10          FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer10),PRE(RTNORDERVer10),CREATE
RecordNumberKey          KEY(RTNORDERVer10:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer10:Location,RTNORDERVer10:ExchangeOrder,RTNORDERVer10:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer10:Location,RTNORDERVer10:ExchangeOrder,RTNORDERVer10:Status,RTNORDERVer10:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer10:OrderNumber,RTNORDERVer10:ExchangeOrder,RTNORDERVer10:Status,RTNORDERVer10:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer10:OrderNumber,RTNORDERVer10:ExchangeOrder,RTNORDERVer10:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer10:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer10:WaybillNumber),DUP,NOCASE
RefNumberKey             KEY(RTNORDERVer10:ExchangeOrder,RTNORDERVer10:RefNumber),DUP,NOCASE
OrderStatusReturnRefNoKey KEY(RTNORDERVer10:OrderNumber,RTNORDERVer10:ExchangeOrder,RTNORDERVer10:ReturnType,RTNORDERVer10:Status,RTNORDERVer10:RefNumber),DUP,NOCASE
LocArcOrdWaybillKey      KEY(RTNORDERVer10:Archived,RTNORDERVer10:Ordered,RTNORDERVer10:Location,RTNORDERVer10:WaybillNumber),DUP,NOCASE
ArcOrdWaybillKey         KEY(RTNORDERVer10:Archived,RTNORDERVer10:Ordered,RTNORDERVer10:WaybillNumber),DUP,NOCASE
ArcLocDateKey            KEY(RTNORDERVer10:Archived,RTNORDERVer10:Location,RTNORDERVer10:DateCreated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archived                    BYTE
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
ReturnType                  STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
ExchangePrice               REAL
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
Ordered                     BYTE
DateOrdered                 DATE
TimeOrdered                 TIME
WhoOrdered                  STRING(3)
Received                    BYTE
DateReceived                DATE
TimeReceived                TIME
WhoReceived                 STRING(3)
WhoProcessed                STRING(3)
                         END
                       END
!------------ End File: RTNORDER, version 10 ------------------

!------------ File: RTNORDER, version 11 ----------------------
!------------ modified 26.09.2011 at 16:10:17 -----------------
MOD:stFileName:RTNORDERVer11  STRING(260)
RTNORDERVer11          FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer11),PRE(RTNORDERVer11),CREATE
RecordNumberKey          KEY(RTNORDERVer11:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer11:Location,RTNORDERVer11:ExchangeOrder,RTNORDERVer11:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer11:Location,RTNORDERVer11:ExchangeOrder,RTNORDERVer11:Status,RTNORDERVer11:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer11:OrderNumber,RTNORDERVer11:ExchangeOrder,RTNORDERVer11:Status,RTNORDERVer11:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer11:OrderNumber,RTNORDERVer11:ExchangeOrder,RTNORDERVer11:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer11:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer11:WaybillNumber),DUP,NOCASE
RefNumberKey             KEY(RTNORDERVer11:ExchangeOrder,RTNORDERVer11:RefNumber),DUP,NOCASE
OrderStatusReturnRefNoKey KEY(RTNORDERVer11:OrderNumber,RTNORDERVer11:ExchangeOrder,RTNORDERVer11:ReturnType,RTNORDERVer11:Status,RTNORDERVer11:RefNumber),DUP,NOCASE
LocArcOrdWaybillKey      KEY(RTNORDERVer11:Archived,RTNORDERVer11:Ordered,RTNORDERVer11:Location,RTNORDERVer11:WaybillNumber),DUP,NOCASE
ArcOrdWaybillKey         KEY(RTNORDERVer11:Archived,RTNORDERVer11:Ordered,RTNORDERVer11:WaybillNumber),DUP,NOCASE
ArcLocDateKey            KEY(RTNORDERVer11:Archived,RTNORDERVer11:Location,RTNORDERVer11:DateCreated),DUP,NOCASE
ArcDateKey               KEY(RTNORDERVer11:Archived,RTNORDERVer11:DateCreated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archived                    BYTE
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
ReturnType                  STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
ExchangePrice               REAL
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
Ordered                     BYTE
DateOrdered                 DATE
TimeOrdered                 TIME
WhoOrdered                  STRING(3)
Received                    BYTE
DateReceived                DATE
TimeReceived                TIME
WhoReceived                 STRING(3)
WhoProcessed                STRING(3)
                         END
                       END
!------------ End File: RTNORDER, version 11 ------------------

!------------ File: RTNORDER, version 12 ----------------------
!------------ modified 04.10.2011 at 15:01:17 -----------------
MOD:stFileName:RTNORDERVer12  STRING(260)
RTNORDERVer12          FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RTNORDERVer12),PRE(RTNORDERVer12),CREATE
RecordNumberKey          KEY(RTNORDERVer12:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer12:Location,RTNORDERVer12:ExchangeOrder,RTNORDERVer12:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer12:Location,RTNORDERVer12:ExchangeOrder,RTNORDERVer12:Status,RTNORDERVer12:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer12:OrderNumber,RTNORDERVer12:ExchangeOrder,RTNORDERVer12:Status,RTNORDERVer12:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer12:OrderNumber,RTNORDERVer12:ExchangeOrder,RTNORDERVer12:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer12:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer12:WaybillNumber),DUP,NOCASE
RefNumberKey             KEY(RTNORDERVer12:ExchangeOrder,RTNORDERVer12:RefNumber),DUP,NOCASE
OrderStatusReturnRefNoKey KEY(RTNORDERVer12:OrderNumber,RTNORDERVer12:ExchangeOrder,RTNORDERVer12:ReturnType,RTNORDERVer12:Status,RTNORDERVer12:RefNumber),DUP,NOCASE
LocArcOrdWaybillKey      KEY(RTNORDERVer12:Archived,RTNORDERVer12:Ordered,RTNORDERVer12:Location,RTNORDERVer12:WaybillNumber),DUP,NOCASE
ArcOrdWaybillKey         KEY(RTNORDERVer12:Archived,RTNORDERVer12:Ordered,RTNORDERVer12:WaybillNumber),DUP,NOCASE
ArcLocDateKey            KEY(RTNORDERVer12:Archived,RTNORDERVer12:Location,RTNORDERVer12:DateCreated),DUP,NOCASE
ArcDateKey               KEY(RTNORDERVer12:Archived,RTNORDERVer12:DateCreated),DUP,NOCASE
DateOrderedKey           KEY(RTNORDERVer12:Location,RTNORDERVer12:DateOrdered,RTNORDERVer12:TimeOrdered),DUP,NOCASE
DateOrderedReceivedKey   KEY(RTNORDERVer12:Location,RTNORDERVer12:Received,RTNORDERVer12:DateOrdered,RTNORDERVer12:TimeOrdered),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archived                    BYTE
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
ReturnType                  STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
ExchangePrice               REAL
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
Ordered                     BYTE
DateOrdered                 DATE
TimeOrdered                 TIME
WhoOrdered                  STRING(3)
Received                    BYTE
DateReceived                DATE
TimeReceived                TIME
WhoReceived                 STRING(3)
WhoProcessed                STRING(3)
                         END
                       END
!------------ End File: RTNORDER, version 12 ------------------

!------------ File: C3DMONIT, version 1 ----------------------
!------------ modified 20.10.2014 at 14:59:52 -----------------
MOD:stFileName:C3DMONITVer1  STRING(260)
C3DMONITVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:C3DMONITVer1),PRE(C3DMONITVer1)
KeyRecordNumber          KEY(C3DMONITVer1:RecordNumber),NOCASE,PRIMARY
KeyRefNumber             KEY(C3DMONITVer1:RefNumber),DUP,NOCASE
KeyDateErrorCode         KEY(C3DMONITVer1:CallDate,C3DMONITVer1:ErrorCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(20)
CallDate                    DATE
CallTime                    TIME
ErrorCode                   STRING(20)
                         END
                       END
!------------ End File: C3DMONIT, version 1 ------------------

!------------ File: SMSText, version 1 ----------------------
!------------ modified 15.02.2012 at 14:57:11 -----------------
MOD:stFileName:SMSTextVer1  STRING(260)
SMSTextVer1            FILE,DRIVER('Btrieve'),OEM,PRE(SMSTextVer1),NAME(MOD:stFileName:SMSTextVer1)
Key_Record_No            KEY(SMSTextVer1:Record_No),NOCASE,PRIMARY
Key_StatusType_Location_Trigger KEY(SMSTextVer1:Status_Type,SMSTextVer1:Location,SMSTextVer1:Trigger_Status),DUP,NOCASE,OPT
Record                   RECORD,PRE()
Record_No                   LONG
Description                 STRING(50)
Location                    STRING(1)
Status_Type                 STRING(1)
Trigger_Status              STRING(30)
Auto_SMS                    STRING(3)
CSI                         STRING(3)
Duplicate_Estimate          STRING(3)
SMSText                     STRING(200)
Resend_Estimate_Days        LONG
Final_Estimate_Text         STRING(200)
                         END
                       END
!------------ End File: SMSText, version 1 ------------------

!------------ File: SMSText, version 2 ----------------------
!------------ modified 20.02.2012 at 11:57:46 -----------------
MOD:stFileName:SMSTextVer2  STRING(260)
SMSTextVer2            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSTextVer2),PRE(SMSTextVer2),CREATE
Key_Record_No            KEY(SMSTextVer2:Record_No),NOCASE,PRIMARY
Key_StatusType_Location_Trigger KEY(SMSTextVer2:Status_Type,SMSTextVer2:Location,SMSTextVer2:Trigger_Status),DUP,NOCASE,OPT
Key_Description          KEY(SMSTextVer2:Description),DUP,NOCASE
Key_StatusType_Description KEY(SMSTextVer2:Status_Type,SMSTextVer2:Description),DUP,NOCASE
Key_TriggerStatus        KEY(SMSTextVer2:Trigger_Status),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
Description                 STRING(50)
Location                    STRING(1)
Status_Type                 STRING(1)
Trigger_Status              STRING(30)
Auto_SMS                    STRING(3)
CSI                         STRING(3)
Duplicate_Estimate          STRING(3)
SMSText                     STRING(200)
Resend_Estimate_Days        LONG
Final_Estimate_Text         STRING(200)
                         END
                       END
!------------ End File: SMSText, version 2 ------------------

!------------ File: SMSText, version 3 ----------------------
!------------ modified 24.02.2012 at 09:00:58 -----------------
MOD:stFileName:SMSTextVer3  STRING(260)
SMSTextVer3            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSTextVer3),PRE(SMSTextVer3),CREATE
Key_Record_No            KEY(SMSTextVer3:Record_No),NOCASE,PRIMARY
Key_StatusType_Location_Trigger KEY(SMSTextVer3:Status_Type,SMSTextVer3:Location,SMSTextVer3:Trigger_Status),DUP,NOCASE,OPT
Key_Description          KEY(SMSTextVer3:Description),DUP,NOCASE
Key_StatusType_Description KEY(SMSTextVer3:Status_Type,SMSTextVer3:Description),DUP,NOCASE
Key_TriggerStatus        KEY(SMSTextVer3:Trigger_Status),DUP,NOCASE
Key_Location_CSI         KEY(SMSTextVer3:Location,SMSTextVer3:CSI),DUP,NOCASE
Key_Location_Duplicate   KEY(SMSTextVer3:Location,SMSTextVer3:Duplicate_Estimate),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
Description                 STRING(50)
Location                    STRING(1)
Status_Type                 STRING(1)
Trigger_Status              STRING(30)
Auto_SMS                    STRING(3)
CSI                         STRING(3)
Duplicate_Estimate          STRING(3)
SMSText                     STRING(500)
Resend_Estimate_Days        LONG
Final_Estimate_Text         STRING(500)
                         END
                       END
!------------ End File: SMSText, version 3 ------------------

!------------ File: SMSText, version 4 ----------------------
!------------ modified 08.03.2012 at 14:48:29 -----------------
MOD:stFileName:SMSTextVer4  STRING(260)
SMSTextVer4            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSTextVer4),PRE(SMSTextVer4),CREATE
Key_Record_No            KEY(SMSTextVer4:Record_No),NOCASE,PRIMARY
Key_StatusType_Location_Trigger KEY(SMSTextVer4:Status_Type,SMSTextVer4:Location,SMSTextVer4:Trigger_Status),DUP,NOCASE,OPT
Key_Description          KEY(SMSTextVer4:Description),DUP,NOCASE
Key_StatusType_Description KEY(SMSTextVer4:Status_Type,SMSTextVer4:Description),DUP,NOCASE
Key_TriggerStatus        KEY(SMSTextVer4:Trigger_Status),DUP,NOCASE
Key_Location_CSI         KEY(SMSTextVer4:Location,SMSTextVer4:CSI),DUP,NOCASE
Key_Location_Duplicate   KEY(SMSTextVer4:Location,SMSTextVer4:Duplicate_Estimate),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
Description                 STRING(50)
Location                    STRING(1)
Status_Type                 STRING(1)
Trigger_Status              STRING(30)
Auto_SMS                    STRING(3)
CSI                         STRING(3)
Duplicate_Estimate          STRING(3)
BER                         STRING(3)
SMSText                     STRING(500)
Resend_Estimate_Days        LONG
Final_Estimate_Text         STRING(500)
                         END
                       END
!------------ End File: SMSText, version 4 ------------------

!------------ File: SMSText, version 5 ----------------------
!------------ modified 09.03.2012 at 08:49:29 -----------------
MOD:stFileName:SMSTextVer5  STRING(260)
SMSTextVer5            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSTextVer5),PRE(SMSTextVer5),CREATE
Key_Record_No            KEY(SMSTextVer5:Record_No),NOCASE,PRIMARY
Key_StatusType_Location_Trigger KEY(SMSTextVer5:Status_Type,SMSTextVer5:Location,SMSTextVer5:Trigger_Status),DUP,NOCASE,OPT
Key_Description          KEY(SMSTextVer5:Description),DUP,NOCASE
Key_StatusType_Description KEY(SMSTextVer5:Status_Type,SMSTextVer5:Description),DUP,NOCASE
Key_TriggerStatus        KEY(SMSTextVer5:Trigger_Status),DUP,NOCASE
Key_Location_CSI         KEY(SMSTextVer5:Location,SMSTextVer5:CSI),DUP,NOCASE
Key_Location_Duplicate   KEY(SMSTextVer5:Location,SMSTextVer5:Duplicate_Estimate),DUP,NOCASE
Key_Location_BER         KEY(SMSTextVer5:Location,SMSTextVer5:BER),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
Description                 STRING(50)
Location                    STRING(1)
Status_Type                 STRING(1)
Trigger_Status              STRING(30)
Auto_SMS                    STRING(3)
CSI                         STRING(3)
Duplicate_Estimate          STRING(3)
BER                         STRING(3)
SMSText                     STRING(500)
Resend_Estimate_Days        LONG
Final_Estimate_Text         STRING(500)
                         END
                       END
!------------ End File: SMSText, version 5 ------------------

!------------ File: SMSText, version 6 ----------------------
!------------ modified 13.03.2012 at 14:36:43 -----------------
MOD:stFileName:SMSTextVer6  STRING(260)
SMSTextVer6            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSTextVer6),PRE(SMSTextVer6),CREATE
Key_Record_No            KEY(SMSTextVer6:Record_No),NOCASE,PRIMARY
Key_StatusType_Location_Trigger KEY(SMSTextVer6:Status_Type,SMSTextVer6:Location,SMSTextVer6:Trigger_Status),DUP,NOCASE,OPT
Key_Description          KEY(SMSTextVer6:Description),DUP,NOCASE
Key_StatusType_Description KEY(SMSTextVer6:Status_Type,SMSTextVer6:Description),DUP,NOCASE
Key_TriggerStatus        KEY(SMSTextVer6:Trigger_Status),DUP,NOCASE
Key_Location_CSI         KEY(SMSTextVer6:Location,SMSTextVer6:CSI),DUP,NOCASE
Key_Location_Duplicate   KEY(SMSTextVer6:Location,SMSTextVer6:Duplicate_Estimate),DUP,NOCASE
Key_Location_BER         KEY(SMSTextVer6:Location,SMSTextVer6:BER),DUP,NOCASE
Key_Location_Liquid      KEY(SMSTextVer6:Location,SMSTextVer6:LiquidDamage),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
Description                 STRING(50)
Location                    STRING(1)
Status_Type                 STRING(1)
Trigger_Status              STRING(30)
Auto_SMS                    STRING(3)
CSI                         STRING(3)
Duplicate_Estimate          STRING(3)
BER                         STRING(3)
LiquidDamage                STRING(3)
SMSText                     STRING(500)
Resend_Estimate_Days        LONG
Final_Estimate_Text         STRING(500)
                         END
                       END
!------------ End File: SMSText, version 6 ------------------

!------------ File: SMSText, version 7 ----------------------
!------------ modified 24.10.2012 at 14:24:46 -----------------
MOD:stFileName:SMSTextVer7  STRING(260)
SMSTextVer7            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMSTextVer7),PRE(SMSTextVer7),CREATE
Key_Record_No            KEY(SMSTextVer7:Record_No),NOCASE,PRIMARY
Key_StatusType_Location_Trigger KEY(SMSTextVer7:Status_Type,SMSTextVer7:Location,SMSTextVer7:Trigger_Status),DUP,NOCASE
Key_Description          KEY(SMSTextVer7:Description),DUP,NOCASE
Key_StatusType_Description KEY(SMSTextVer7:Status_Type,SMSTextVer7:Description),DUP,NOCASE
Key_TriggerStatus        KEY(SMSTextVer7:Trigger_Status),DUP,NOCASE
Key_Location_CSI         KEY(SMSTextVer7:Location,SMSTextVer7:CSI),DUP,NOCASE
Key_Location_Duplicate   KEY(SMSTextVer7:Location,SMSTextVer7:Duplicate_Estimate),DUP,NOCASE
Key_Location_BER         KEY(SMSTextVer7:Location,SMSTextVer7:BER),DUP,NOCASE
Key_Location_Liquid      KEY(SMSTextVer7:Location,SMSTextVer7:LiquidDamage),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
Description                 STRING(50)
Location                    STRING(1)
Status_Type                 STRING(1)
Trigger_Status              STRING(30)
Auto_SMS                    STRING(3)
CSI                         STRING(3)
Duplicate_Estimate          STRING(3)
BER                         STRING(3)
LiquidDamage                STRING(3)
SMSText                     STRING(500)
Resend_Estimate_Days        LONG
Final_Estimate_Text         STRING(500)
                         END
                       END
!------------ End File: SMSText, version 7 ------------------

!------------ File: RETSTOCK, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:RETSTOCKVer1  STRING(260)
RETSTOCKVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RETSTOCKVer1),PRE(RETSTOCKVer1)
Record_Number_Key        KEY(RETSTOCKVer1:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Part_Number),DUP,NOCASE
Despatched_Key           KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Despatched,RETSTOCKVer1:Despatch_Note_Number,RETSTOCKVer1:Part_Number),DUP,NOCASE
Despatched_Only_Key      KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Despatched),DUP,NOCASE
Pending_Ref_Number_Key   KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Pending_Ref_Number),DUP,NOCASE
Order_Part_Key           KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Order_Part_Number),DUP,NOCASE
Order_Number_Key         KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Order_Number),DUP,NOCASE
DespatchedKey            KEY(RETSTOCKVer1:Despatched),DUP,NOCASE
DespatchedPartKey        KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Despatched,RETSTOCKVer1:Part_Number),DUP,NOCASE
Amended_Receipt_Key      KEY(RETSTOCKVer1:Amend_Site_Loc,RETSTOCKVer1:Amended,RETSTOCKVer1:Ref_Number),DUP,NOCASE
Delimit_Stock_Key        KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Part_Ref_Number),DUP,NOCASE
PartRefNumberKey         KEY(RETSTOCKVer1:Part_Ref_Number),DUP,NOCASE
PartNumberKey            KEY(RETSTOCKVer1:Part_Number),DUP,NOCASE
DespatchPartKey          KEY(RETSTOCKVer1:Despatched,RETSTOCKVer1:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Item_Cost                   REAL
Quantity                    REAL
Despatched                  STRING(20)
Order_Number                REAL
Date_Ordered                DATE
Date_Received               DATE
Despatch_Note_Number        REAL
Despatch_Date               DATE
Part_Ref_Number             REAL
Pending_Ref_Number          REAL
Order_Part_Number           REAL
Purchase_Order_Number       STRING(30)
Previous_Sale_Number        LONG
InvoicePart                 LONG
Credit                      STRING(3)
QuantityReceived            LONG
Received                    BYTE
Amended                     BYTE
Amend_Reason                CSTRING(255)
Amend_Site_Loc              STRING(30)
GRNNumber                   REAL
DateReceived                DATE
ScannedQty                  LONG
                         END
                       END
!------------ End File: RETSTOCK, version 1 ------------------

!------------ File: RETSTOCK, version 2 ----------------------
!------------ modified 04.07.2011 at 11:32:50 -----------------
MOD:stFileName:RETSTOCKVer2  STRING(260)
RETSTOCKVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RETSTOCKVer2),PRE(RETSTOCKVer2),CREATE
Record_Number_Key        KEY(RETSTOCKVer2:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Part_Number),DUP,NOCASE
Despatched_Key           KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Despatched,RETSTOCKVer2:Despatch_Note_Number,RETSTOCKVer2:Part_Number),DUP,NOCASE
Despatched_Only_Key      KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Despatched),DUP,NOCASE
Pending_Ref_Number_Key   KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Pending_Ref_Number),DUP,NOCASE
Order_Part_Key           KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Order_Part_Number),DUP,NOCASE
Order_Number_Key         KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Order_Number),DUP,NOCASE
DespatchedKey            KEY(RETSTOCKVer2:Despatched),DUP,NOCASE
DespatchedPartKey        KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Despatched,RETSTOCKVer2:Part_Number),DUP,NOCASE
Amended_Receipt_Key      KEY(RETSTOCKVer2:Amend_Site_Loc,RETSTOCKVer2:Amended,RETSTOCKVer2:Ref_Number),DUP,NOCASE
Delimit_Stock_Key        KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Part_Ref_Number),DUP,NOCASE
PartRefNumberKey         KEY(RETSTOCKVer2:Part_Ref_Number),DUP,NOCASE
PartNumberKey            KEY(RETSTOCKVer2:Part_Number),DUP,NOCASE
DespatchPartKey          KEY(RETSTOCKVer2:Despatched,RETSTOCKVer2:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Item_Cost                   REAL
Quantity                    REAL
Despatched                  STRING(20)
Order_Number                REAL
Date_Ordered                DATE
Date_Received               DATE
Despatch_Note_Number        REAL
Despatch_Date               DATE
Part_Ref_Number             REAL
Pending_Ref_Number          REAL
Order_Part_Number           REAL
Purchase_Order_Number       STRING(30)
Previous_Sale_Number        LONG
InvoicePart                 LONG
Credit                      STRING(3)
QuantityReceived            LONG
Received                    BYTE
Amended                     BYTE
Amend_Reason                CSTRING(255)
Amend_Site_Loc              STRING(30)
GRNNumber                   REAL
DateReceived                DATE
ScannedQty                  LONG
ExchangeIMEINumber          STRING(30)
                         END
                       END
!------------ End File: RETSTOCK, version 2 ------------------

!------------ File: RETSTOCK, version 3 ----------------------
!------------ modified 05.07.2011 at 11:34:38 -----------------
MOD:stFileName:RETSTOCKVer3  STRING(260)
RETSTOCKVer3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RETSTOCKVer3),PRE(RETSTOCKVer3),CREATE
Record_Number_Key        KEY(RETSTOCKVer3:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(RETSTOCKVer3:Ref_Number,RETSTOCKVer3:Part_Number),DUP,NOCASE
Despatched_Key           KEY(RETSTOCKVer3:Ref_Number,RETSTOCKVer3:Despatched,RETSTOCKVer3:Despatch_Note_Number,RETSTOCKVer3:Part_Number),DUP,NOCASE
Despatched_Only_Key      KEY(RETSTOCKVer3:Ref_Number,RETSTOCKVer3:Despatched),DUP,NOCASE
Pending_Ref_Number_Key   KEY(RETSTOCKVer3:Ref_Number,RETSTOCKVer3:Pending_Ref_Number),DUP,NOCASE
Order_Part_Key           KEY(RETSTOCKVer3:Ref_Number,RETSTOCKVer3:Order_Part_Number),DUP,NOCASE
Order_Number_Key         KEY(RETSTOCKVer3:Ref_Number,RETSTOCKVer3:Order_Number),DUP,NOCASE
DespatchedKey            KEY(RETSTOCKVer3:Despatched),DUP,NOCASE
DespatchedPartKey        KEY(RETSTOCKVer3:Ref_Number,RETSTOCKVer3:Despatched,RETSTOCKVer3:Part_Number),DUP,NOCASE
Amended_Receipt_Key      KEY(RETSTOCKVer3:Amend_Site_Loc,RETSTOCKVer3:Amended,RETSTOCKVer3:Ref_Number),DUP,NOCASE
Delimit_Stock_Key        KEY(RETSTOCKVer3:Ref_Number,RETSTOCKVer3:Part_Ref_Number),DUP,NOCASE
PartRefNumberKey         KEY(RETSTOCKVer3:Part_Ref_Number),DUP,NOCASE
PartNumberKey            KEY(RETSTOCKVer3:Part_Number),DUP,NOCASE
DespatchPartKey          KEY(RETSTOCKVer3:Despatched,RETSTOCKVer3:Part_Number),DUP,NOCASE
IMEINumberKey            KEY(RETSTOCKVer3:Ref_Number,RETSTOCKVer3:ExchangeIMEINumber),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Item_Cost                   REAL
Quantity                    REAL
Despatched                  STRING(20)
Order_Number                REAL
Date_Ordered                DATE
Date_Received               DATE
Despatch_Note_Number        REAL
Despatch_Date               DATE
Part_Ref_Number             REAL
Pending_Ref_Number          REAL
Order_Part_Number           REAL
Purchase_Order_Number       STRING(30)
Previous_Sale_Number        LONG
InvoicePart                 LONG
Credit                      STRING(3)
QuantityReceived            LONG
Received                    BYTE
Amended                     BYTE
Amend_Reason                CSTRING(255)
Amend_Site_Loc              STRING(30)
GRNNumber                   REAL
DateReceived                DATE
ScannedQty                  LONG
ExchangeIMEINumber          STRING(30)
                         END
                       END
!------------ End File: RETSTOCK, version 3 ------------------

!------------ File: RETSTOCK, version 4 ----------------------
!------------ modified 05.07.2011 at 12:11:57 -----------------
MOD:stFileName:RETSTOCKVer4  STRING(260)
RETSTOCKVer4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RETSTOCKVer4),PRE(RETSTOCKVer4),CREATE
Record_Number_Key        KEY(RETSTOCKVer4:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(RETSTOCKVer4:Ref_Number,RETSTOCKVer4:Part_Number),DUP,NOCASE
Despatched_Key           KEY(RETSTOCKVer4:Ref_Number,RETSTOCKVer4:Despatched,RETSTOCKVer4:Despatch_Note_Number,RETSTOCKVer4:Part_Number),DUP,NOCASE
Despatched_Only_Key      KEY(RETSTOCKVer4:Ref_Number,RETSTOCKVer4:Despatched),DUP,NOCASE
Pending_Ref_Number_Key   KEY(RETSTOCKVer4:Ref_Number,RETSTOCKVer4:Pending_Ref_Number),DUP,NOCASE
Order_Part_Key           KEY(RETSTOCKVer4:Ref_Number,RETSTOCKVer4:Order_Part_Number),DUP,NOCASE
Order_Number_Key         KEY(RETSTOCKVer4:Ref_Number,RETSTOCKVer4:Order_Number),DUP,NOCASE
DespatchedKey            KEY(RETSTOCKVer4:Despatched),DUP,NOCASE
DespatchedPartKey        KEY(RETSTOCKVer4:Ref_Number,RETSTOCKVer4:Despatched,RETSTOCKVer4:Part_Number),DUP,NOCASE
Amended_Receipt_Key      KEY(RETSTOCKVer4:Amend_Site_Loc,RETSTOCKVer4:Amended,RETSTOCKVer4:Ref_Number),DUP,NOCASE
Delimit_Stock_Key        KEY(RETSTOCKVer4:Ref_Number,RETSTOCKVer4:Part_Ref_Number),DUP,NOCASE
PartRefNumberKey         KEY(RETSTOCKVer4:Part_Ref_Number),DUP,NOCASE
PartNumberKey            KEY(RETSTOCKVer4:Part_Number),DUP,NOCASE
DespatchPartKey          KEY(RETSTOCKVer4:Despatched,RETSTOCKVer4:Part_Number),DUP,NOCASE
ExchangeRefNumberKey     KEY(RETSTOCKVer4:Ref_Number,RETSTOCKVer4:ExchangeRefNumber),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Item_Cost                   REAL
Quantity                    REAL
Despatched                  STRING(20)
Order_Number                REAL
Date_Ordered                DATE
Date_Received               DATE
Despatch_Note_Number        REAL
Despatch_Date               DATE
Part_Ref_Number             REAL
Pending_Ref_Number          REAL
Order_Part_Number           REAL
Purchase_Order_Number       STRING(30)
Previous_Sale_Number        LONG
InvoicePart                 LONG
Credit                      STRING(3)
QuantityReceived            LONG
Received                    BYTE
Amended                     BYTE
Amend_Reason                CSTRING(255)
Amend_Site_Loc              STRING(30)
GRNNumber                   REAL
DateReceived                DATE
ScannedQty                  LONG
ExchangeRefNumber           LONG
                         END
                       END
!------------ End File: RETSTOCK, version 4 ------------------

!------------ File: STOCKALX, version 1 ----------------------
!------------ modified 14.05.2012 at 09:17:10 -----------------
MOD:stFileName:STOCKALXVer1  STRING(260)
STOCKALXVer1           FILE,DRIVER('Btrieve'),OEM,PRE(STOCKALXVer1),NAME(MOD:stFileName:STOCKALXVer1)
KeyRecordNo              KEY(STOCKALXVer1:RecordNo),NOCASE,PRIMARY
KeySTLRecordNumber       KEY(STOCKALXVer1:StlRecordNumber),DUP,NOCASE
KeyRequestDateTime       KEY(STOCKALXVer1:RequestDate,STOCKALXVer1:RequestTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
StlRecordNumber             LONG
RequestDate                 DATE
RequestTime                 TIME
AllocateDate                DATE
AllocateTime                STRING(20)
AllocateUser                STRING(3)
                         END
                       END
!------------ End File: STOCKALX, version 1 ------------------

!------------ File: STOCKALX, version 2 ----------------------
!------------ modified 15.05.2012 at 10:18:53 -----------------
MOD:stFileName:STOCKALXVer2  STRING(260)
STOCKALXVer2           FILE,DRIVER('Btrieve'),OEM,PRE(STOCKALXVer2),NAME(MOD:stFileName:STOCKALXVer2),CREATE
KeyRecordNo              KEY(STOCKALXVer2:RecordNo),NOCASE,PRIMARY
KeySTLRecordNumber       KEY(STOCKALXVer2:StlRecordNumber),DUP,NOCASE
KeyRequestDateTime       KEY(STOCKALXVer2:RequestDate,STOCKALXVer2:RequestTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
StlRecordNumber             LONG
JobNo                       LONG
PartNo                      STRING(30)
Description                 STRING(30)
RequestDate                 DATE
RequestTime                 TIME
AllocateDate                DATE
AllocateTime                STRING(20)
AllocateUser                STRING(3)
                         END
                       END
!------------ End File: STOCKALX, version 2 ------------------

!------------ File: ORDPARTS, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:ORDPARTSVer1  STRING(260)
ORDPARTSVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:ORDPARTSVer1),PRE(ORDPARTSVer1)
Order_Number_Key         KEY(ORDPARTSVer1:Order_Number,ORDPARTSVer1:Part_Ref_Number),DUP,NOCASE
record_number_key        KEY(ORDPARTSVer1:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(ORDPARTSVer1:Part_Ref_Number,ORDPARTSVer1:Part_Number,ORDPARTSVer1:Description),DUP,NOCASE
Part_Number_Key          KEY(ORDPARTSVer1:Order_Number,ORDPARTSVer1:Part_Number,ORDPARTSVer1:Description),DUP,NOCASE
Description_Key          KEY(ORDPARTSVer1:Order_Number,ORDPARTSVer1:Description,ORDPARTSVer1:Part_Number),DUP,NOCASE
Received_Part_Number_Key KEY(ORDPARTSVer1:All_Received,ORDPARTSVer1:Part_Number),DUP,NOCASE
Account_Number_Key       KEY(ORDPARTSVer1:Account_Number,ORDPARTSVer1:Part_Type,ORDPARTSVer1:All_Received,ORDPARTSVer1:Part_Number),DUP,NOCASE
Allocated_Key            KEY(ORDPARTSVer1:Part_Type,ORDPARTSVer1:All_Received,ORDPARTSVer1:Allocated_To_Sale,ORDPARTSVer1:Part_Number),DUP,NOCASE
Allocated_Account_Key    KEY(ORDPARTSVer1:Part_Type,ORDPARTSVer1:All_Received,ORDPARTSVer1:Allocated_To_Sale,ORDPARTSVer1:Account_Number,ORDPARTSVer1:Part_Number),DUP,NOCASE
Order_Type_Received      KEY(ORDPARTSVer1:Order_Number,ORDPARTSVer1:Part_Type,ORDPARTSVer1:All_Received,ORDPARTSVer1:Part_Number,ORDPARTSVer1:Description),DUP,NOCASE
PartRecordNumberKey      KEY(ORDPARTSVer1:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Record_Number               LONG
Part_Ref_Number             LONG
Quantity                    LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Job_Number                  LONG
Part_Type                   STRING(3)
Number_Received             LONG
Date_Received               DATE
All_Received                STRING(3)
Allocated_To_Sale           STRING(3)
Account_Number              STRING(15)
DespatchNoteNumber          STRING(30)
Reason                      STRING(255)
PartRecordNumber            LONG
GRN_Number                  LONG
OrderedCurrency             STRING(30)
OrderedDailyRate            REAL
OrderedDivideMultiply       STRING(1)
ReceivedCurrency            STRING(30)
ReceivedDailyRate           REAL
ReceivedDivideMultiply      STRING(1)
                         END
                       END
!------------ End File: ORDPARTS, version 1 ------------------

!------------ File: ORDPARTS, version 2 ----------------------
!------------ modified 01.07.2011 at 11:44:14 -----------------
MOD:stFileName:ORDPARTSVer2  STRING(260)
ORDPARTSVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:ORDPARTSVer2),PRE(ORDPARTSVer2),CREATE
Order_Number_Key         KEY(ORDPARTSVer2:Order_Number,ORDPARTSVer2:Part_Ref_Number),DUP,NOCASE
record_number_key        KEY(ORDPARTSVer2:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(ORDPARTSVer2:Part_Ref_Number,ORDPARTSVer2:Part_Number,ORDPARTSVer2:Description),DUP,NOCASE
Part_Number_Key          KEY(ORDPARTSVer2:Order_Number,ORDPARTSVer2:Part_Number,ORDPARTSVer2:Description),DUP,NOCASE
Description_Key          KEY(ORDPARTSVer2:Order_Number,ORDPARTSVer2:Description,ORDPARTSVer2:Part_Number),DUP,NOCASE
Received_Part_Number_Key KEY(ORDPARTSVer2:All_Received,ORDPARTSVer2:Part_Number),DUP,NOCASE
Account_Number_Key       KEY(ORDPARTSVer2:Account_Number,ORDPARTSVer2:Part_Type,ORDPARTSVer2:All_Received,ORDPARTSVer2:Part_Number),DUP,NOCASE
Allocated_Key            KEY(ORDPARTSVer2:Part_Type,ORDPARTSVer2:All_Received,ORDPARTSVer2:Allocated_To_Sale,ORDPARTSVer2:Part_Number),DUP,NOCASE
Allocated_Account_Key    KEY(ORDPARTSVer2:Part_Type,ORDPARTSVer2:All_Received,ORDPARTSVer2:Allocated_To_Sale,ORDPARTSVer2:Account_Number,ORDPARTSVer2:Part_Number),DUP,NOCASE
Order_Type_Received      KEY(ORDPARTSVer2:Order_Number,ORDPARTSVer2:Part_Type,ORDPARTSVer2:All_Received,ORDPARTSVer2:Part_Number,ORDPARTSVer2:Description),DUP,NOCASE
PartRecordNumberKey      KEY(ORDPARTSVer2:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Record_Number               LONG
Part_Ref_Number             LONG
Quantity                    LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Job_Number                  LONG
Part_Type                   STRING(3)
Number_Received             LONG
Date_Received               DATE
All_Received                STRING(3)
Allocated_To_Sale           STRING(3)
Account_Number              STRING(15)
DespatchNoteNumber          STRING(30)
Reason                      STRING(255)
PartRecordNumber            LONG
GRN_Number                  LONG
OrderedCurrency             STRING(30)
OrderedDailyRate            REAL
OrderedDivideMultiply       STRING(1)
ReceivedCurrency            STRING(30)
ReceivedDailyRate           REAL
ReceivedDivideMultiply      STRING(1)
FreeExchangeStock           BYTE
                         END
                       END
!------------ End File: ORDPARTS, version 2 ------------------

!------------ File: ORDPARTS, version 3 ----------------------
!------------ modified 23.08.2011 at 14:13:41 -----------------
MOD:stFileName:ORDPARTSVer3  STRING(260)
ORDPARTSVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:ORDPARTSVer3),PRE(ORDPARTSVer3),CREATE
Order_Number_Key         KEY(ORDPARTSVer3:Order_Number,ORDPARTSVer3:Part_Ref_Number),DUP,NOCASE
record_number_key        KEY(ORDPARTSVer3:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(ORDPARTSVer3:Part_Ref_Number,ORDPARTSVer3:Part_Number,ORDPARTSVer3:Description),DUP,NOCASE
Part_Number_Key          KEY(ORDPARTSVer3:Order_Number,ORDPARTSVer3:Part_Number,ORDPARTSVer3:Description),DUP,NOCASE
Description_Key          KEY(ORDPARTSVer3:Order_Number,ORDPARTSVer3:Description,ORDPARTSVer3:Part_Number),DUP,NOCASE
Received_Part_Number_Key KEY(ORDPARTSVer3:All_Received,ORDPARTSVer3:Part_Number),DUP,NOCASE
Account_Number_Key       KEY(ORDPARTSVer3:Account_Number,ORDPARTSVer3:Part_Type,ORDPARTSVer3:All_Received,ORDPARTSVer3:Part_Number),DUP,NOCASE
Allocated_Key            KEY(ORDPARTSVer3:Part_Type,ORDPARTSVer3:All_Received,ORDPARTSVer3:Allocated_To_Sale,ORDPARTSVer3:Part_Number),DUP,NOCASE
Allocated_Account_Key    KEY(ORDPARTSVer3:Part_Type,ORDPARTSVer3:All_Received,ORDPARTSVer3:Allocated_To_Sale,ORDPARTSVer3:Account_Number,ORDPARTSVer3:Part_Number),DUP,NOCASE
Order_Type_Received      KEY(ORDPARTSVer3:Order_Number,ORDPARTSVer3:Part_Type,ORDPARTSVer3:All_Received,ORDPARTSVer3:Part_Number,ORDPARTSVer3:Description),DUP,NOCASE
PartRecordNumberKey      KEY(ORDPARTSVer3:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Record_Number               LONG
Part_Ref_Number             LONG
Quantity                    LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Job_Number                  LONG
Part_Type                   STRING(3)
Number_Received             LONG
Date_Received               DATE
All_Received                STRING(3)
Allocated_To_Sale           STRING(3)
Account_Number              STRING(15)
DespatchNoteNumber          STRING(30)
Reason                      STRING(255)
PartRecordNumber            LONG
GRN_Number                  LONG
OrderedCurrency             STRING(30)
OrderedDailyRate            REAL
OrderedDivideMultiply       STRING(1)
ReceivedCurrency            STRING(30)
ReceivedDailyRate           REAL
ReceivedDivideMultiply      STRING(1)
FreeExchangeStock           BYTE
TimeReceived                TIME
DatePriceCaptured           DATE
TimePriceCaptured           TIME
                         END
                       END
!------------ End File: ORDPARTS, version 3 ------------------

!------------ File: LOCATION, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:LOCATIONVer1  STRING(260)
LOCATIONVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:LOCATIONVer1),PRE(LOCATIONVer1)
RecordNumberKey          KEY(LOCATIONVer1:RecordNumber),NOCASE,PRIMARY
Location_Key             KEY(LOCATIONVer1:Location),NOCASE
Main_Store_Key           KEY(LOCATIONVer1:Main_Store,LOCATIONVer1:Location),DUP,NOCASE
ActiveLocationKey        KEY(LOCATIONVer1:Active,LOCATIONVer1:Location),DUP,NOCASE
ActiveMainStoreKey       KEY(LOCATIONVer1:Active,LOCATIONVer1:Main_Store,LOCATIONVer1:Location),DUP,NOCASE
VirtualLocationKey       KEY(LOCATIONVer1:VirtualSite,LOCATIONVer1:Location),DUP,NOCASE
VirtualMainStoreKey      KEY(LOCATIONVer1:VirtualSite,LOCATIONVer1:Main_Store,LOCATIONVer1:Location),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
Main_Store                  STRING(3)
Active                      BYTE
VirtualSite                 BYTE
Level1                      BYTE
Level2                      BYTE
Level3                      BYTE
InWarrantyMarkUp            REAL
OutWarrantyMarkUp           REAL
UseRapidStock               BYTE
                         END
                       END
!------------ End File: LOCATION, version 1 ------------------

!------------ File: STOHISTE, version 1 ----------------------
!------------ modified 11.02.2011 at 12:12:56 -----------------
MOD:stFileName:STOHISTEVer1  STRING(260)
STOHISTEVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:STOHISTEVer1),PRE(STOHISTEVer1)
RecordNumberKey          KEY(STOHISTEVer1:RecordNumber),NOCASE,PRIMARY
SHIRecordNumberKey       KEY(STOHISTEVer1:SHIRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SHIRecordNumber             LONG
PreviousAveragePurchaseCost REAL
PurchaseCost                REAL
SaleCost                    REAL
RetailCost                  REAL
                         END
                       END
!------------ End File: STOHISTE, version 1 ------------------

!------------ File: STOHISTE, version 2 ----------------------
!------------ modified 09.05.2012 at 09:36:46 -----------------
MOD:stFileName:STOHISTEVer2  STRING(260)
STOHISTEVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:STOHISTEVer2),PRE(STOHISTEVer2),CREATE
RecordNumberKey          KEY(STOHISTEVer2:RecordNumber),NOCASE,PRIMARY
SHIRecordNumberKey       KEY(STOHISTEVer2:SHIRecordNumber),DUP,NOCASE
KeyArcStatus             KEY(STOHISTEVer2:ARC_Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SHIRecordNumber             LONG
PreviousAveragePurchaseCost REAL
PurchaseCost                REAL
SaleCost                    REAL
RetailCost                  REAL
HistDate                    DATE
ARC_Status                  STRING(1)
                         END
                       END
!------------ End File: STOHISTE, version 2 ------------------

!------------ File: CREDNOTR, version 1 ----------------------
!------------ modified 06.09.2011 at 13:53:47 -----------------
MOD:stFileName:CREDNOTRVer1  STRING(260)
CREDNOTRVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:CREDNOTRVer1),PRE(CREDNOTRVer1)
RecordNumberKey          KEY(CREDNOTRVer1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
Usercode                    STRING(3)
                         END
                       END
!------------ End File: CREDNOTR, version 1 ------------------

!------------ File: CREDNOTR, version 2 ----------------------
!------------ modified 14.09.2011 at 16:08:16 -----------------
MOD:stFileName:CREDNOTRVer2  STRING(260)
CREDNOTRVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:CREDNOTRVer2),PRE(CREDNOTRVer2),CREATE
RecordNumberKey          KEY(CREDNOTRVer2:RecordNumber),NOCASE,PRIMARY
ReceivedKey              KEY(CREDNOTRVer2:Received,CREDNOTRVer2:RecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
Usercode                    STRING(3)
Received                    BYTE
                         END
                       END
!------------ End File: CREDNOTR, version 2 ------------------

!------------ File: EXCHANGE, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:EXCHANGEVer1  STRING(260)
EXCHANGEVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:EXCHANGEVer1),PRE(EXCHANGEVer1)
Ref_Number_Key           KEY(EXCHANGEVer1:Ref_Number),NOCASE,PRIMARY
AvailLocIMEI             KEY(EXCHANGEVer1:Available,EXCHANGEVer1:Location,EXCHANGEVer1:ESN),DUP,NOCASE
AvailLocMSN              KEY(EXCHANGEVer1:Available,EXCHANGEVer1:Location,EXCHANGEVer1:MSN),DUP,NOCASE
AvailLocRef              KEY(EXCHANGEVer1:Available,EXCHANGEVer1:Location,EXCHANGEVer1:Ref_Number),DUP,NOCASE
AvailLocModel            KEY(EXCHANGEVer1:Available,EXCHANGEVer1:Location,EXCHANGEVer1:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(EXCHANGEVer1:ESN),DUP,NOCASE
MSN_Only_Key             KEY(EXCHANGEVer1:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Model_Number,EXCHANGEVer1:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(EXCHANGEVer1:Stock_Type,EXCHANGEVer1:ESN),DUP,NOCASE
MSN_Key                  KEY(EXCHANGEVer1:Stock_Type,EXCHANGEVer1:MSN),DUP,NOCASE
ESN_Available_Key        KEY(EXCHANGEVer1:Available,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:ESN),DUP,NOCASE
MSN_Available_Key        KEY(EXCHANGEVer1:Available,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:MSN),DUP,NOCASE
Ref_Available_Key        KEY(EXCHANGEVer1:Available,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(EXCHANGEVer1:Available,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Model_Number,EXCHANGEVer1:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(EXCHANGEVer1:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(EXCHANGEVer1:Model_Number,EXCHANGEVer1:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(EXCHANGEVer1:Available,EXCHANGEVer1:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(EXCHANGEVer1:Available,EXCHANGEVer1:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(EXCHANGEVer1:Available,EXCHANGEVer1:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(EXCHANGEVer1:Available,EXCHANGEVer1:Model_Number,EXCHANGEVer1:Ref_Number),DUP,NOCASE
StockBookedKey           KEY(EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Model_Number,EXCHANGEVer1:Date_Booked,EXCHANGEVer1:Ref_Number),DUP,NOCASE
AvailBookedKey           KEY(EXCHANGEVer1:Available,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Model_Number,EXCHANGEVer1:Date_Booked,EXCHANGEVer1:Ref_Number),DUP,NOCASE
DateBookedKey            KEY(EXCHANGEVer1:Date_Booked),DUP,NOCASE
LocStockAvailIMEIKey     KEY(EXCHANGEVer1:Location,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Available,EXCHANGEVer1:ESN),DUP,NOCASE
LocStockIMEIKey          KEY(EXCHANGEVer1:Location,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:ESN),DUP,NOCASE
LocIMEIKey               KEY(EXCHANGEVer1:Location,EXCHANGEVer1:ESN),DUP,NOCASE
LocStockAvailRefKey      KEY(EXCHANGEVer1:Location,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Available,EXCHANGEVer1:Ref_Number),DUP,NOCASE
LocStockRefKey           KEY(EXCHANGEVer1:Location,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Ref_Number),DUP,NOCASE
LocRefKey                KEY(EXCHANGEVer1:Location,EXCHANGEVer1:Ref_Number),DUP,NOCASE
LocStockAvailModelKey    KEY(EXCHANGEVer1:Location,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Available,EXCHANGEVer1:Model_Number,EXCHANGEVer1:Ref_Number),DUP,NOCASE
LocStockModelKey         KEY(EXCHANGEVer1:Location,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Model_Number,EXCHANGEVer1:Ref_Number),DUP,NOCASE
LocModelKey              KEY(EXCHANGEVer1:Location,EXCHANGEVer1:Model_Number,EXCHANGEVer1:Ref_Number),DUP,NOCASE
LocStockAvailMSNKey      KEY(EXCHANGEVer1:Location,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:Available,EXCHANGEVer1:MSN),DUP,NOCASE
LocStockMSNKey           KEY(EXCHANGEVer1:Location,EXCHANGEVer1:Stock_Type,EXCHANGEVer1:MSN),DUP,NOCASE
LocMSNKey                KEY(EXCHANGEVer1:Location,EXCHANGEVer1:MSN),DUP,NOCASE
InTransitLocationKey     KEY(EXCHANGEVer1:InTransit,EXCHANGEVer1:Location,EXCHANGEVer1:ESN),DUP,NOCASE
InTransitKey             KEY(EXCHANGEVer1:InTransit,EXCHANGEVer1:ESN),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
Audit_Number                REAL
InTransit                   BYTE
                         END
                       END
!------------ End File: EXCHANGE, version 1 ------------------

!------------ File: EXCHANGE, version 2 ----------------------
!------------ modified 01.07.2011 at 11:44:14 -----------------
MOD:stFileName:EXCHANGEVer2  STRING(260)
EXCHANGEVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:EXCHANGEVer2),PRE(EXCHANGEVer2),CREATE
Ref_Number_Key           KEY(EXCHANGEVer2:Ref_Number),NOCASE,PRIMARY
AvailLocIMEI             KEY(EXCHANGEVer2:Available,EXCHANGEVer2:Location,EXCHANGEVer2:ESN),DUP,NOCASE
AvailLocMSN              KEY(EXCHANGEVer2:Available,EXCHANGEVer2:Location,EXCHANGEVer2:MSN),DUP,NOCASE
AvailLocRef              KEY(EXCHANGEVer2:Available,EXCHANGEVer2:Location,EXCHANGEVer2:Ref_Number),DUP,NOCASE
AvailLocModel            KEY(EXCHANGEVer2:Available,EXCHANGEVer2:Location,EXCHANGEVer2:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(EXCHANGEVer2:ESN),DUP,NOCASE
MSN_Only_Key             KEY(EXCHANGEVer2:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Model_Number,EXCHANGEVer2:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(EXCHANGEVer2:Stock_Type,EXCHANGEVer2:ESN),DUP,NOCASE
MSN_Key                  KEY(EXCHANGEVer2:Stock_Type,EXCHANGEVer2:MSN),DUP,NOCASE
ESN_Available_Key        KEY(EXCHANGEVer2:Available,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:ESN),DUP,NOCASE
MSN_Available_Key        KEY(EXCHANGEVer2:Available,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:MSN),DUP,NOCASE
Ref_Available_Key        KEY(EXCHANGEVer2:Available,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(EXCHANGEVer2:Available,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Model_Number,EXCHANGEVer2:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(EXCHANGEVer2:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(EXCHANGEVer2:Model_Number,EXCHANGEVer2:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(EXCHANGEVer2:Available,EXCHANGEVer2:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(EXCHANGEVer2:Available,EXCHANGEVer2:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(EXCHANGEVer2:Available,EXCHANGEVer2:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(EXCHANGEVer2:Available,EXCHANGEVer2:Model_Number,EXCHANGEVer2:Ref_Number),DUP,NOCASE
StockBookedKey           KEY(EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Model_Number,EXCHANGEVer2:Date_Booked,EXCHANGEVer2:Ref_Number),DUP,NOCASE
AvailBookedKey           KEY(EXCHANGEVer2:Available,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Model_Number,EXCHANGEVer2:Date_Booked,EXCHANGEVer2:Ref_Number),DUP,NOCASE
DateBookedKey            KEY(EXCHANGEVer2:Date_Booked),DUP,NOCASE
LocStockAvailIMEIKey     KEY(EXCHANGEVer2:Location,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Available,EXCHANGEVer2:ESN),DUP,NOCASE
LocStockIMEIKey          KEY(EXCHANGEVer2:Location,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:ESN),DUP,NOCASE
LocIMEIKey               KEY(EXCHANGEVer2:Location,EXCHANGEVer2:ESN),DUP,NOCASE
LocStockAvailRefKey      KEY(EXCHANGEVer2:Location,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Available,EXCHANGEVer2:Ref_Number),DUP,NOCASE
LocStockRefKey           KEY(EXCHANGEVer2:Location,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Ref_Number),DUP,NOCASE
LocRefKey                KEY(EXCHANGEVer2:Location,EXCHANGEVer2:Ref_Number),DUP,NOCASE
LocStockAvailModelKey    KEY(EXCHANGEVer2:Location,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Available,EXCHANGEVer2:Model_Number,EXCHANGEVer2:Ref_Number),DUP,NOCASE
LocStockModelKey         KEY(EXCHANGEVer2:Location,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Model_Number,EXCHANGEVer2:Ref_Number),DUP,NOCASE
LocModelKey              KEY(EXCHANGEVer2:Location,EXCHANGEVer2:Model_Number,EXCHANGEVer2:Ref_Number),DUP,NOCASE
LocStockAvailMSNKey      KEY(EXCHANGEVer2:Location,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:Available,EXCHANGEVer2:MSN),DUP,NOCASE
LocStockMSNKey           KEY(EXCHANGEVer2:Location,EXCHANGEVer2:Stock_Type,EXCHANGEVer2:MSN),DUP,NOCASE
LocMSNKey                KEY(EXCHANGEVer2:Location,EXCHANGEVer2:MSN),DUP,NOCASE
InTransitLocationKey     KEY(EXCHANGEVer2:InTransit,EXCHANGEVer2:Location,EXCHANGEVer2:ESN),DUP,NOCASE
InTransitKey             KEY(EXCHANGEVer2:InTransit,EXCHANGEVer2:ESN),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
Audit_Number                REAL
InTransit                   BYTE
FreeStockPurchased          STRING(1)
                         END
                       END
!------------ End File: EXCHANGE, version 2 ------------------

!------------ File: SUPPLIER, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:SUPPLIERVer1  STRING(260)
SUPPLIERVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUPPLIERVer1),PRE(SUPPLIERVer1)
RecordNumberKey          KEY(SUPPLIERVer1:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(SUPPLIERVer1:Account_Number),DUP,NOCASE
Company_Name_Key         KEY(SUPPLIERVer1:Company_Name),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Company_Name                STRING(30)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(60)
Account_Number              STRING(15)
Order_Period                REAL
Normal_Supply_Period        REAL
Minimum_Order_Value         REAL
HistoryUsage                LONG
Factor                      REAL
Notes                       STRING(255)
UseForeignCurrency          BYTE
CurrencyCode                STRING(30)
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
                         END
                       END
!------------ End File: SUPPLIER, version 1 ------------------

!------------ File: SUPPLIER, version 2 ----------------------
!------------ modified 28.06.2011 at 14:01:25 -----------------
MOD:stFileName:SUPPLIERVer2  STRING(260)
SUPPLIERVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUPPLIERVer2),PRE(SUPPLIERVer2),CREATE
RecordNumberKey          KEY(SUPPLIERVer2:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(SUPPLIERVer2:Account_Number),DUP,NOCASE
Company_Name_Key         KEY(SUPPLIERVer2:Company_Name),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Company_Name                STRING(30)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(60)
Account_Number              STRING(15)
Order_Period                REAL
Normal_Supply_Period        REAL
Minimum_Order_Value         REAL
HistoryUsage                LONG
Factor                      REAL
Notes                       STRING(255)
UseForeignCurrency          BYTE
CurrencyCode                STRING(30)
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
UseFreeStock                BYTE
                         END
                       END
!------------ End File: SUPPLIER, version 2 ------------------

!------------ File: SUPPLIER, version 3 ----------------------
!------------ modified 07.11.2012 at 10:15:35 -----------------
MOD:stFileName:SUPPLIERVer3  STRING(260)
SUPPLIERVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUPPLIERVer3),PRE(SUPPLIERVer3),CREATE
RecordNumberKey          KEY(SUPPLIERVer3:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(SUPPLIERVer3:Account_Number),DUP,NOCASE
Company_Name_Key         KEY(SUPPLIERVer3:Company_Name),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Company_Name                STRING(30)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(60)
Account_Number              STRING(15)
Order_Period                REAL
Normal_Supply_Period        REAL
Minimum_Order_Value         REAL
HistoryUsage                LONG
Factor                      REAL
Notes                       STRING(255)
UseForeignCurrency          BYTE
CurrencyCode                STRING(30)
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
UseFreeStock                BYTE
EVO_GL_Acc_No               STRING(10)
EVO_Vendor_Number           STRING(10)
EVO_TaxExempt               BYTE
                         END
                       END
!------------ End File: SUPPLIER, version 3 ------------------

!------------ File: SUPPLIER, version 4 ----------------------
!------------ modified 30.01.2013 at 15:51:58 -----------------
MOD:stFileName:SUPPLIERVer4  STRING(260)
SUPPLIERVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUPPLIERVer4),PRE(SUPPLIERVer4),CREATE
RecordNumberKey          KEY(SUPPLIERVer4:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(SUPPLIERVer4:Account_Number),DUP,NOCASE
Company_Name_Key         KEY(SUPPLIERVer4:Company_Name),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Company_Name                STRING(30)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(60)
Account_Number              STRING(15)
Order_Period                REAL
Normal_Supply_Period        REAL
Minimum_Order_Value         REAL
HistoryUsage                LONG
Factor                      REAL
Notes                       STRING(255)
UseForeignCurrency          BYTE
CurrencyCode                STRING(30)
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
UseFreeStock                BYTE
EVO_GL_Acc_No               STRING(10)
EVO_Vendor_Number           STRING(10)
EVO_TaxExempt               BYTE
EVO_Profit_Centre           STRING(30)
                         END
                       END
!------------ End File: SUPPLIER, version 4 ------------------

!------------ File: SUBURB, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:SUBURBVer1  STRING(260)
SUBURBVer1             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SUBURBVer1),PRE(SUBURBVer1)
RecordNumberKey          KEY(SUBURBVer1:RecordNumber),NOCASE,PRIMARY
SuburbKey                KEY(SUBURBVer1:Suburb),DUP,NOCASE
PostcodeKey              KEY(SUBURBVer1:Postcode),DUP,NOCASE
HubKey                   KEY(SUBURBVer1:Hub),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Suburb                      STRING(30)
Postcode                    STRING(30)
Hub                         STRING(30)
                         END
                       END
!------------ End File: SUBURB, version 1 ------------------

!------------ File: SUPVALA, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:SUPVALAVer1  STRING(260)
SUPVALAVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SUPVALAVer1),PRE(SUPVALAVer1)
RecordNumberKey          KEY(SUPVALAVer1:RecordNumber),NOCASE,PRIMARY
RunDateKey               KEY(SUPVALAVer1:Supplier,SUPVALAVer1:RunDate),DUP,NOCASE
DateOnly                 KEY(SUPVALAVer1:RunDate),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Supplier                    STRING(30)
RunDate                     DATE
OldBackOrder                DATE
OldOutOrder                 DATE
                         END
                       END
!------------ End File: SUPVALA, version 1 ------------------

!------------ File: SUPVALB, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:SUPVALBVer1  STRING(260)
SUPVALBVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SUPVALBVer1),PRE(SUPVALBVer1)
RecordNumberKey          KEY(SUPVALBVer1:RecordNumber),NOCASE,PRIMARY
RunDateKey               KEY(SUPVALBVer1:Supplier,SUPVALBVer1:RunDate),DUP,NOCASE
LocationKey              KEY(SUPVALBVer1:Supplier,SUPVALBVer1:RunDate,SUPVALBVer1:Location),DUP,NOCASE
DateOnly                 KEY(SUPVALBVer1:RunDate),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Supplier                    STRING(30)
RunDate                     DATE
Location                    STRING(30)
BackOrderValue              REAL
                         END
                       END
!------------ End File: SUPVALB, version 1 ------------------

!------------ File: MODELNUM, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:MODELNUMVer1  STRING(260)
MODELNUMVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer1),PRE(MODELNUMVer1)
Model_Number_Key         KEY(MODELNUMVer1:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer1:Manufacturer,MODELNUMVer1:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer1:Manufacturer,MODELNUMVer1:Unit_Type),DUP,NOCASE
SalesModelKey            KEY(MODELNUMVer1:Manufacturer,MODELNUMVer1:SalesModel),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
ReplacementValue            REAL
WarrantyRepairLimit         REAL
LoanReplacementValue        REAL
ExcludedRRCRepair           BYTE
AllowIMEICharacters         BYTE
UseReplenishmentProcess     BYTE
SalesModel                  STRING(30)
ExcludeAutomaticRebookingProcess BYTE
                         END
                       END
!------------ End File: MODELNUM, version 1 ------------------

!------------ File: MODELNUM, version 2 ----------------------
!------------ modified 28.06.2010 at 11:19:04 -----------------
MOD:stFileName:MODELNUMVer2  STRING(260)
MODELNUMVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer2),PRE(MODELNUMVer2),CREATE
Model_Number_Key         KEY(MODELNUMVer2:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer2:Manufacturer,MODELNUMVer2:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer2:Manufacturer,MODELNUMVer2:Unit_Type),DUP,NOCASE
SalesModelKey            KEY(MODELNUMVer2:Manufacturer,MODELNUMVer2:SalesModel),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
ReplacementValue            REAL
WarrantyRepairLimit         REAL
LoanReplacementValue        REAL
ExcludedRRCRepair           BYTE
AllowIMEICharacters         BYTE
UseReplenishmentProcess     BYTE
SalesModel                  STRING(30)
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
                         END
                       END
!------------ End File: MODELNUM, version 2 ------------------

!------------ File: MODELNUM, version 3 ----------------------
!------------ modified 28.06.2010 at 12:01:52 -----------------
MOD:stFileName:MODELNUMVer3  STRING(260)
MODELNUMVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer3),PRE(MODELNUMVer3),CREATE
Model_Number_Key         KEY(MODELNUMVer3:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer3:Manufacturer,MODELNUMVer3:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer3:Manufacturer,MODELNUMVer3:Unit_Type),DUP,NOCASE
SalesModelKey            KEY(MODELNUMVer3:Manufacturer,MODELNUMVer3:SalesModel),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
ReplacementValue            REAL
WarrantyRepairLimit         REAL
LoanReplacementValue        REAL
ExcludedRRCRepair           BYTE
AllowIMEICharacters         BYTE
UseReplenishmentProcess     BYTE
SalesModel                  STRING(30)
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
Active                      STRING(1)
                         END
                       END
!------------ End File: MODELNUM, version 3 ------------------

!------------ File: MODELNUM, version 4 ----------------------
!------------ modified 28.06.2011 at 14:32:55 -----------------
MOD:stFileName:MODELNUMVer4  STRING(260)
MODELNUMVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer4),PRE(MODELNUMVer4),CREATE
Model_Number_Key         KEY(MODELNUMVer4:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer4:Manufacturer,MODELNUMVer4:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer4:Manufacturer,MODELNUMVer4:Unit_Type),DUP,NOCASE
SalesModelKey            KEY(MODELNUMVer4:Manufacturer,MODELNUMVer4:SalesModel),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
ReplacementValue            REAL
WarrantyRepairLimit         REAL
LoanReplacementValue        REAL
ExcludedRRCRepair           BYTE
AllowIMEICharacters         BYTE
UseReplenishmentProcess     BYTE
SalesModel                  STRING(30)
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
Active                      STRING(1)
ExchReplaceValue            REAL
                         END
                       END
!------------ End File: MODELNUM, version 4 ------------------

!------------ File: MODELNUM, version 5 ----------------------
!------------ modified 18.01.2012 at 16:20:40 -----------------
MOD:stFileName:MODELNUMVer5  STRING(260)
MODELNUMVer5           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer5),PRE(MODELNUMVer5),CREATE
Model_Number_Key         KEY(MODELNUMVer5:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer5:Manufacturer,MODELNUMVer5:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer5:Manufacturer,MODELNUMVer5:Unit_Type),DUP,NOCASE
SalesModelKey            KEY(MODELNUMVer5:Manufacturer,MODELNUMVer5:SalesModel),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
ReplacementValue            REAL
WarrantyRepairLimit         REAL
LoanReplacementValue        REAL
ExcludedRRCRepair           BYTE
AllowIMEICharacters         BYTE
UseReplenishmentProcess     BYTE
SalesModel                  STRING(30)
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
Active                      STRING(1)
ExchReplaceValue            REAL
LoanSellingPrice            REAL
RRCOrderCap                 LONG
                         END
                       END
!------------ End File: MODELNUM, version 5 ------------------

!------------ File: TRDBATCH, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:TRDBATCHVer1  STRING(260)
TRDBATCHVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:TRDBATCHVer1),PRE(TRDBATCHVer1)
RecordNumberKey          KEY(TRDBATCHVer1:RecordNumber),NOCASE,PRIMARY
Batch_Number_Key         KEY(TRDBATCHVer1:Company_Name,TRDBATCHVer1:Batch_Number),DUP,NOCASE
Batch_Number_Status_Key  KEY(TRDBATCHVer1:Status,TRDBATCHVer1:Batch_Number),DUP,NOCASE
BatchOnlyKey             KEY(TRDBATCHVer1:Batch_Number),DUP,NOCASE
Batch_Company_Status_Key KEY(TRDBATCHVer1:Status,TRDBATCHVer1:Company_Name,TRDBATCHVer1:Batch_Number),DUP,NOCASE
ESN_Only_Key             KEY(TRDBATCHVer1:ESN),DUP,NOCASE
ESN_Status_Key           KEY(TRDBATCHVer1:Status,TRDBATCHVer1:ESN),DUP,NOCASE
Company_Batch_ESN_Key    KEY(TRDBATCHVer1:Company_Name,TRDBATCHVer1:Batch_Number,TRDBATCHVer1:ESN),DUP,NOCASE
AuthorisationKey         KEY(TRDBATCHVer1:AuthorisationNo),DUP,NOCASE
StatusAuthKey            KEY(TRDBATCHVer1:Status,TRDBATCHVer1:AuthorisationNo),DUP,NOCASE
CompanyDateKey           KEY(TRDBATCHVer1:Company_Name,TRDBATCHVer1:Status,TRDBATCHVer1:DateReturn),DUP,NOCASE
JobStatusKey             KEY(TRDBATCHVer1:Status,TRDBATCHVer1:Ref_Number),DUP,NOCASE
JobNumberKey             KEY(TRDBATCHVer1:Ref_Number),DUP,NOCASE
CompanyDespatchedKey     KEY(TRDBATCHVer1:Company_Name,TRDBATCHVer1:DateDespatched),DUP,NOCASE
ReturnDateKey            KEY(TRDBATCHVer1:DateReturn),DUP,NOCASE
ReturnCompanyKey         KEY(TRDBATCHVer1:DateReturn,TRDBATCHVer1:Company_Name),DUP,NOCASE
NotPrintedManJobKey      KEY(TRDBATCHVer1:BatchRunNotPrinted,TRDBATCHVer1:Status,TRDBATCHVer1:Company_Name,TRDBATCHVer1:Ref_Number),DUP,NOCASE
PurchaseOrderKey         KEY(TRDBATCHVer1:PurchaseOrderNumber,TRDBATCHVer1:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Batch_Number                LONG
Company_Name                STRING(30)
Ref_Number                  LONG
ESN                         STRING(20)
Status                      STRING(3)
Date                        DATE
Time                        TIME
AuthorisationNo             STRING(30)
Exchanged                   STRING(3)
DateReturn                  DATE
TimeReturn                  TIME
TurnTime                    LONG
MSN                         STRING(30)
DateDespatched              DATE
TimeDespatched              TIME
ReturnUser                  STRING(3)
ReturnWaybillNo             STRING(30)
ReturnRepairType            STRING(30)
ReturnRejectedAmount        REAL
ReturnRejectedReason        STRING(255)
ThirdPartyInvoiceNo         STRING(30)
ThirdPartyInvoiceDate       DATE
ThirdPartyInvoiceCharge     REAL
ThirdPartyVAT               REAL
ThirdPartyMarkup            REAL
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
BatchRunNotPrinted          BYTE
PurchaseOrderNumber         LONG
NewThirdPartySite           STRING(30)
NewThirdPartySiteID         STRING(30)
                         END
                       END
!------------ End File: TRDBATCH, version 1 ------------------

!------------ File: TRADEACC, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:TRADEACCVer1  STRING(260)
TRADEACCVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer1),PRE(TRADEACCVer1)
RecordNumberKey          KEY(TRADEACCVer1:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer1:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer1:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(TRADEACCVer1:ReplicateAccount,TRADEACCVer1:Account_Number),DUP,NOCASE
StoresAccountKey         KEY(TRADEACCVer1:StoresAccount),DUP,NOCASE
SiteLocationKey          KEY(TRADEACCVer1:SiteLocation),DUP,NOCASE
RegionKey                KEY(TRADEACCVer1:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
CCommissionInHouse          LONG
WCommissionInHouse          LONG
CCommissionOutSource        LONG
WCommissionOutSource        LONG
ReplicateAccount            STRING(30)
RemoteRepairCentre          BYTE
SiteLocation                STRING(30)
RRCFactor                   LONG
ARCFactor                   LONG
TransitType                 STRING(30)
ForceMobileNumber           BYTE
BranchIdentification        STRING(2)
AllocateQAEng               BYTE
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
UseTimingsFrom              STRING(30)
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             STRING(3)
IncludeSunday               STRING(3)
StoresAccount               STRING(30)
RepairEngineerQA            BYTE
SecondYearAccount           BYTE
Activate48Hour              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
Line500AccountNumber        STRING(30)
CompanyOwned                BYTE
OverrideMobileDefault       BYTE
Region                      STRING(30)
AllowCreditNotes            BYTE
DoMSISDNCheck               BYTE
Hub                         STRING(30)
UseSBOnline                 BYTE
IgnoreReplenishmentProcess  BYTE
VCPWaybillPrefix            STRING(30)
RRCWaybillPrefix            STRING(30)
OBFCompanyName              STRING(30)
OBFAddress1                 STRING(30)
OBFAddress2                 STRING(30)
OBFSuburb                   STRING(30)
OBFContactName              STRING(30)
OBFContactNumber            STRING(15)
OBFEmailAddress             STRING(255)
                         END
                       END
!------------ End File: TRADEACC, version 1 ------------------

!------------ File: TRADEACC, version 2 ----------------------
!------------ modified 09.09.2010 at 12:07:17 -----------------
MOD:stFileName:TRADEACCVer2  STRING(260)
TRADEACCVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer2),PRE(TRADEACCVer2),CREATE
RecordNumberKey          KEY(TRADEACCVer2:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer2:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer2:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(TRADEACCVer2:ReplicateAccount,TRADEACCVer2:Account_Number),DUP,NOCASE
StoresAccountKey         KEY(TRADEACCVer2:StoresAccount),DUP,NOCASE
SiteLocationKey          KEY(TRADEACCVer2:SiteLocation),DUP,NOCASE
RegionKey                KEY(TRADEACCVer2:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
CCommissionInHouse          LONG
WCommissionInHouse          LONG
CCommissionOutSource        LONG
WCommissionOutSource        LONG
ReplicateAccount            STRING(30)
RemoteRepairCentre          BYTE
SiteLocation                STRING(30)
RRCFactor                   LONG
ARCFactor                   LONG
TransitType                 STRING(30)
ForceMobileNumber           BYTE
BranchIdentification        STRING(2)
AllocateQAEng               BYTE
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
UseTimingsFrom              STRING(30)
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             STRING(3)
IncludeSunday               STRING(3)
StoresAccount               STRING(30)
RepairEngineerQA            BYTE
SecondYearAccount           BYTE
Activate48Hour              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
Line500AccountNumber        STRING(30)
CompanyOwned                BYTE
OverrideMobileDefault       BYTE
Region                      STRING(30)
AllowCreditNotes            BYTE
DoMSISDNCheck               BYTE
Hub                         STRING(30)
UseSBOnline                 BYTE
IgnoreReplenishmentProcess  BYTE
VCPWaybillPrefix            STRING(30)
RRCWaybillPrefix            STRING(30)
OBFCompanyName              STRING(30)
OBFAddress1                 STRING(30)
OBFAddress2                 STRING(30)
OBFSuburb                   STRING(30)
OBFContactName              STRING(30)
OBFContactNumber            STRING(15)
OBFEmailAddress             STRING(255)
SBOnlineJobProgress         BYTE
                         END
                       END
!------------ End File: TRADEACC, version 2 ------------------

!------------ File: TRADEACC, version 3 ----------------------
!------------ modified 13.04.2011 at 10:25:19 -----------------
MOD:stFileName:TRADEACCVer3  STRING(260)
TRADEACCVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer3),PRE(TRADEACCVer3),CREATE
RecordNumberKey          KEY(TRADEACCVer3:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer3:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer3:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(TRADEACCVer3:ReplicateAccount,TRADEACCVer3:Account_Number),DUP,NOCASE
StoresAccountKey         KEY(TRADEACCVer3:StoresAccount),DUP,NOCASE
SiteLocationKey          KEY(TRADEACCVer3:SiteLocation),DUP,NOCASE
RegionKey                KEY(TRADEACCVer3:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
CCommissionInHouse          LONG
WCommissionInHouse          LONG
CCommissionOutSource        LONG
WCommissionOutSource        LONG
ReplicateAccount            STRING(30)
RemoteRepairCentre          BYTE
SiteLocation                STRING(30)
RRCFactor                   LONG
ARCFactor                   LONG
TransitType                 STRING(30)
ForceMobileNumber           BYTE
BranchIdentification        STRING(2)
AllocateQAEng               BYTE
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
UseTimingsFrom              STRING(30)
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             STRING(3)
IncludeSunday               STRING(3)
StoresAccount               STRING(30)
RepairEngineerQA            BYTE
SecondYearAccount           BYTE
Activate48Hour              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
Line500AccountNumber        STRING(30)
CompanyOwned                BYTE
OverrideMobileDefault       BYTE
Region                      STRING(30)
AllowCreditNotes            BYTE
DoMSISDNCheck               BYTE
Hub                         STRING(30)
UseSBOnline                 BYTE
IgnoreReplenishmentProcess  BYTE
VCPWaybillPrefix            STRING(30)
RRCWaybillPrefix            STRING(30)
OBFCompanyName              STRING(30)
OBFAddress1                 STRING(30)
OBFAddress2                 STRING(30)
OBFSuburb                   STRING(30)
OBFContactName              STRING(30)
OBFContactNumber            STRING(15)
OBFEmailAddress             STRING(255)
SBOnlineJobProgress         BYTE
coTradingName               STRING(40)
coTradingName2              STRING(40)
coLocation                  STRING(40)
coRegistrationNo            STRING(30)
coVATNumber                 STRING(30)
coAddressLine1              STRING(40)
coAddressLine2              STRING(40)
coAddressLine3              STRING(40)
coAddressLine4              STRING(40)
coTelephoneNumber           STRING(30)
coFaxNumber                 STRING(30)
coEmailAddress              STRING(255)
                         END
                       END
!------------ End File: TRADEACC, version 3 ------------------

!------------ File: TRADEACC, version 4 ----------------------
!------------ modified 31.08.2011 at 15:36:35 -----------------
MOD:stFileName:TRADEACCVer4  STRING(260)
TRADEACCVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer4),PRE(TRADEACCVer4),CREATE
RecordNumberKey          KEY(TRADEACCVer4:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer4:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer4:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(TRADEACCVer4:ReplicateAccount,TRADEACCVer4:Account_Number),DUP,NOCASE
StoresAccountKey         KEY(TRADEACCVer4:StoresAccount),DUP,NOCASE
SiteLocationKey          KEY(TRADEACCVer4:SiteLocation),DUP,NOCASE
RegionKey                KEY(TRADEACCVer4:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
CCommissionInHouse          LONG
WCommissionInHouse          LONG
CCommissionOutSource        LONG
WCommissionOutSource        LONG
ReplicateAccount            STRING(30)
RemoteRepairCentre          BYTE
SiteLocation                STRING(30)
RRCFactor                   LONG
ARCFactor                   LONG
TransitType                 STRING(30)
ForceMobileNumber           BYTE
BranchIdentification        STRING(2)
AllocateQAEng               BYTE
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
UseTimingsFrom              STRING(30)
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             STRING(3)
IncludeSunday               STRING(3)
StoresAccount               STRING(30)
RepairEngineerQA            BYTE
SecondYearAccount           BYTE
Activate48Hour              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
Line500AccountNumber        STRING(30)
CompanyOwned                BYTE
OverrideMobileDefault       BYTE
Region                      STRING(30)
AllowCreditNotes            BYTE
DoMSISDNCheck               BYTE
Hub                         STRING(30)
UseSBOnline                 BYTE
IgnoreReplenishmentProcess  BYTE
VCPWaybillPrefix            STRING(30)
RRCWaybillPrefix            STRING(30)
OBFCompanyName              STRING(30)
OBFAddress1                 STRING(30)
OBFAddress2                 STRING(30)
OBFSuburb                   STRING(30)
OBFContactName              STRING(30)
OBFContactNumber            STRING(15)
OBFEmailAddress             STRING(255)
SBOnlineJobProgress         BYTE
coTradingName               STRING(40)
coTradingName2              STRING(40)
coLocation                  STRING(40)
coRegistrationNo            STRING(30)
coVATNumber                 STRING(30)
coAddressLine1              STRING(40)
coAddressLine2              STRING(40)
coAddressLine3              STRING(40)
coAddressLine4              STRING(40)
coTelephoneNumber           STRING(30)
coFaxNumber                 STRING(30)
coEmailAddress              STRING(255)
RtnExchangeAccount          STRING(30)
                         END
                       END
!------------ End File: TRADEACC, version 4 ------------------

!------------ File: TRADEACC, version 5 ----------------------
!------------ modified 20.02.2012 at 15:16:19 -----------------
MOD:stFileName:TRADEACCVer5  STRING(260)
TRADEACCVer5           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer5),PRE(TRADEACCVer5),CREATE
RecordNumberKey          KEY(TRADEACCVer5:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer5:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer5:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(TRADEACCVer5:ReplicateAccount,TRADEACCVer5:Account_Number),DUP,NOCASE
StoresAccountKey         KEY(TRADEACCVer5:StoresAccount),DUP,NOCASE
SiteLocationKey          KEY(TRADEACCVer5:SiteLocation),DUP,NOCASE
RegionKey                KEY(TRADEACCVer5:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
CCommissionInHouse          LONG
WCommissionInHouse          LONG
CCommissionOutSource        LONG
WCommissionOutSource        LONG
ReplicateAccount            STRING(30)
RemoteRepairCentre          BYTE
SiteLocation                STRING(30)
RRCFactor                   LONG
ARCFactor                   LONG
TransitType                 STRING(30)
ForceMobileNumber           BYTE
BranchIdentification        STRING(2)
AllocateQAEng               BYTE
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
UseTimingsFrom              STRING(30)
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             STRING(3)
IncludeSunday               STRING(3)
StoresAccount               STRING(30)
RepairEngineerQA            BYTE
SecondYearAccount           BYTE
Activate48Hour              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
Line500AccountNumber        STRING(30)
CompanyOwned                BYTE
OverrideMobileDefault       BYTE
Region                      STRING(30)
AllowCreditNotes            BYTE
DoMSISDNCheck               BYTE
Hub                         STRING(30)
UseSBOnline                 BYTE
IgnoreReplenishmentProcess  BYTE
VCPWaybillPrefix            STRING(30)
RRCWaybillPrefix            STRING(30)
OBFCompanyName              STRING(30)
OBFAddress1                 STRING(30)
OBFAddress2                 STRING(30)
OBFSuburb                   STRING(30)
OBFContactName              STRING(30)
OBFContactNumber            STRING(15)
OBFEmailAddress             STRING(255)
SBOnlineJobProgress         BYTE
coTradingName               STRING(40)
coTradingName2              STRING(40)
coLocation                  STRING(40)
coRegistrationNo            STRING(30)
coVATNumber                 STRING(30)
coAddressLine1              STRING(40)
coAddressLine2              STRING(40)
coAddressLine3              STRING(40)
coAddressLine4              STRING(40)
coTelephoneNumber           STRING(30)
coFaxNumber                 STRING(30)
coEmailAddress              STRING(255)
RtnExchangeAccount          STRING(30)
coSMSname                   STRING(40)
                         END
                       END
!------------ End File: TRADEACC, version 5 ------------------

!------------ File: TRADEACC, version 6 ----------------------
!------------ modified 05.03.2012 at 12:55:52 -----------------
MOD:stFileName:TRADEACCVer6  STRING(260)
TRADEACCVer6           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer6),PRE(TRADEACCVer6),CREATE
RecordNumberKey          KEY(TRADEACCVer6:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer6:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer6:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(TRADEACCVer6:ReplicateAccount,TRADEACCVer6:Account_Number),DUP,NOCASE
StoresAccountKey         KEY(TRADEACCVer6:StoresAccount),DUP,NOCASE
SiteLocationKey          KEY(TRADEACCVer6:SiteLocation),DUP,NOCASE
RegionKey                KEY(TRADEACCVer6:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
CCommissionInHouse          LONG
WCommissionInHouse          LONG
CCommissionOutSource        LONG
WCommissionOutSource        LONG
ReplicateAccount            STRING(30)
RemoteRepairCentre          BYTE
SiteLocation                STRING(30)
RRCFactor                   LONG
ARCFactor                   LONG
TransitType                 STRING(30)
ForceMobileNumber           BYTE
BranchIdentification        STRING(2)
AllocateQAEng               BYTE
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
UseTimingsFrom              STRING(30)
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             STRING(3)
IncludeSunday               STRING(3)
StoresAccount               STRING(30)
RepairEngineerQA            BYTE
SecondYearAccount           BYTE
Activate48Hour              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
Line500AccountNumber        STRING(30)
CompanyOwned                BYTE
OverrideMobileDefault       BYTE
Region                      STRING(30)
AllowCreditNotes            BYTE
DoMSISDNCheck               BYTE
Hub                         STRING(30)
UseSBOnline                 BYTE
IgnoreReplenishmentProcess  BYTE
VCPWaybillPrefix            STRING(30)
RRCWaybillPrefix            STRING(30)
OBFCompanyName              STRING(30)
OBFAddress1                 STRING(30)
OBFAddress2                 STRING(30)
OBFSuburb                   STRING(30)
OBFContactName              STRING(30)
OBFContactNumber            STRING(15)
OBFEmailAddress             STRING(255)
SBOnlineJobProgress         BYTE
coTradingName               STRING(40)
coTradingName2              STRING(40)
coLocation                  STRING(40)
coRegistrationNo            STRING(30)
coVATNumber                 STRING(30)
coAddressLine1              STRING(40)
coAddressLine2              STRING(40)
coAddressLine3              STRING(40)
coAddressLine4              STRING(40)
coTelephoneNumber           STRING(30)
coFaxNumber                 STRING(30)
coEmailAddress              STRING(255)
RtnExchangeAccount          STRING(30)
coSMSname                   STRING(40)
SMS_Email2                  STRING(100)
SMS_Email3                  STRING(100)
                         END
                       END
!------------ End File: TRADEACC, version 6 ------------------

!------------ File: TRADEACC, version 7 ----------------------
!------------ modified 08.03.2012 at 14:48:46 -----------------
MOD:stFileName:TRADEACCVer7  STRING(260)
TRADEACCVer7           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer7),PRE(TRADEACCVer7),CREATE
RecordNumberKey          KEY(TRADEACCVer7:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer7:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer7:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(TRADEACCVer7:ReplicateAccount,TRADEACCVer7:Account_Number),DUP,NOCASE
StoresAccountKey         KEY(TRADEACCVer7:StoresAccount),DUP,NOCASE
SiteLocationKey          KEY(TRADEACCVer7:SiteLocation),DUP,NOCASE
RegionKey                KEY(TRADEACCVer7:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
CCommissionInHouse          LONG
WCommissionInHouse          LONG
CCommissionOutSource        LONG
WCommissionOutSource        LONG
ReplicateAccount            STRING(30)
RemoteRepairCentre          BYTE
SiteLocation                STRING(30)
RRCFactor                   LONG
ARCFactor                   LONG
TransitType                 STRING(30)
ForceMobileNumber           BYTE
BranchIdentification        STRING(2)
AllocateQAEng               BYTE
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
UseTimingsFrom              STRING(30)
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             STRING(3)
IncludeSunday               STRING(3)
StoresAccount               STRING(30)
RepairEngineerQA            BYTE
SecondYearAccount           BYTE
Activate48Hour              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
Line500AccountNumber        STRING(30)
CompanyOwned                BYTE
OverrideMobileDefault       BYTE
Region                      STRING(30)
AllowCreditNotes            BYTE
DoMSISDNCheck               BYTE
Hub                         STRING(30)
UseSBOnline                 BYTE
IgnoreReplenishmentProcess  BYTE
VCPWaybillPrefix            STRING(30)
RRCWaybillPrefix            STRING(30)
OBFCompanyName              STRING(30)
OBFAddress1                 STRING(30)
OBFAddress2                 STRING(30)
OBFSuburb                   STRING(30)
OBFContactName              STRING(30)
OBFContactNumber            STRING(15)
OBFEmailAddress             STRING(255)
SBOnlineJobProgress         BYTE
coTradingName               STRING(40)
coTradingName2              STRING(40)
coLocation                  STRING(40)
coRegistrationNo            STRING(30)
coVATNumber                 STRING(30)
coAddressLine1              STRING(40)
coAddressLine2              STRING(40)
coAddressLine3              STRING(40)
coAddressLine4              STRING(40)
coTelephoneNumber           STRING(30)
coFaxNumber                 STRING(30)
coEmailAddress              STRING(255)
RtnExchangeAccount          STRING(30)
coSMSname                   STRING(40)
SMS_Email2                  STRING(100)
SMS_Email3                  STRING(100)
SMS_ReportEmail             STRING(255)
                         END
                       END
!------------ End File: TRADEACC, version 7 ------------------

!------------ File: TRADEACC, version 8 ----------------------
!------------ modified 21.03.2012 at 16:40:51 -----------------
MOD:stFileName:TRADEACCVer8  STRING(260)
TRADEACCVer8           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer8),PRE(TRADEACCVer8),CREATE
RecordNumberKey          KEY(TRADEACCVer8:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer8:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer8:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(TRADEACCVer8:ReplicateAccount,TRADEACCVer8:Account_Number),DUP,NOCASE
StoresAccountKey         KEY(TRADEACCVer8:StoresAccount),DUP,NOCASE
SiteLocationKey          KEY(TRADEACCVer8:SiteLocation),DUP,NOCASE
RegionKey                KEY(TRADEACCVer8:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
CCommissionInHouse          LONG
WCommissionInHouse          LONG
CCommissionOutSource        LONG
WCommissionOutSource        LONG
ReplicateAccount            STRING(30)
RemoteRepairCentre          BYTE
SiteLocation                STRING(30)
RRCFactor                   LONG
ARCFactor                   LONG
TransitType                 STRING(30)
ForceMobileNumber           BYTE
BranchIdentification        STRING(2)
AllocateQAEng               BYTE
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
UseTimingsFrom              STRING(30)
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             STRING(3)
IncludeSunday               STRING(3)
StoresAccount               STRING(30)
RepairEngineerQA            BYTE
SecondYearAccount           BYTE
Activate48Hour              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
Line500AccountNumber        STRING(30)
CompanyOwned                BYTE
OverrideMobileDefault       BYTE
Region                      STRING(30)
AllowCreditNotes            BYTE
DoMSISDNCheck               BYTE
Hub                         STRING(30)
UseSBOnline                 BYTE
IgnoreReplenishmentProcess  BYTE
VCPWaybillPrefix            STRING(30)
RRCWaybillPrefix            STRING(30)
OBFCompanyName              STRING(30)
OBFAddress1                 STRING(30)
OBFAddress2                 STRING(30)
OBFSuburb                   STRING(30)
OBFContactName              STRING(30)
OBFContactNumber            STRING(15)
OBFEmailAddress             STRING(255)
SBOnlineJobProgress         BYTE
coTradingName               STRING(40)
coTradingName2              STRING(40)
coLocation                  STRING(40)
coRegistrationNo            STRING(30)
coVATNumber                 STRING(30)
coAddressLine1              STRING(40)
coAddressLine2              STRING(40)
coAddressLine3              STRING(40)
coAddressLine4              STRING(40)
coTelephoneNumber           STRING(30)
coFaxNumber                 STRING(30)
coEmailAddress              STRING(255)
RtnExchangeAccount          STRING(30)
coSMSname                   STRING(40)
SMS_Email2                  STRING(100)
SMS_Email3                  STRING(100)
SMS_ReportEmail             STRING(255)
UseSparesRequest            STRING(3)
                         END
                       END
!------------ End File: TRADEACC, version 8 ------------------

!------------ File: REQUISIT, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:REQUISITVer1  STRING(260)
REQUISITVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:REQUISITVer1),PRE(REQUISITVer1)
RecordNumberKey          KEY(REQUISITVer1:RecordNumber),NOCASE,PRIMARY
SupplierKey              KEY(REQUISITVer1:Supplier),DUP,NOCASE
OrderedNumberKey         KEY(REQUISITVer1:Ordered,REQUISITVer1:RecordNumber),DUP,NOCASE
SupplierNumberKey        KEY(REQUISITVer1:Ordered,REQUISITVer1:Supplier),DUP,NOCASE
DateKey                  KEY(REQUISITVer1:TheDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
TheDate                     DATE
TheTime                     TIME
Supplier                    STRING(30)
Ordered                     BYTE
                         END
                       END
!------------ End File: REQUISIT, version 1 ------------------

!------------ File: SUBTRACC, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:SUBTRACCVer1  STRING(260)
SUBTRACCVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUBTRACCVer1),PRE(SUBTRACCVer1)
RecordNumberKey          KEY(SUBTRACCVer1:RecordNumber),NOCASE,PRIMARY
Main_Account_Key         KEY(SUBTRACCVer1:Main_Account_Number,SUBTRACCVer1:Account_Number),NOCASE
Main_Name_Key            KEY(SUBTRACCVer1:Main_Account_Number,SUBTRACCVer1:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(SUBTRACCVer1:Account_Number),NOCASE
Branch_Key               KEY(SUBTRACCVer1:Branch),DUP,NOCASE
Main_Branch_Key          KEY(SUBTRACCVer1:Main_Account_Number,SUBTRACCVer1:Branch),DUP,NOCASE
Company_Name_Key         KEY(SUBTRACCVer1:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(SUBTRACCVer1:ReplicateAccount,SUBTRACCVer1:Account_Number),DUP,NOCASE
GenericAccountKey        KEY(SUBTRACCVer1:Generic_Account,SUBTRACCVer1:Account_Number),DUP,NOCASE
GenericCompanyKey        KEY(SUBTRACCVer1:Generic_Account,SUBTRACCVer1:Company_Name),DUP,NOCASE
GenericBranchKey         KEY(SUBTRACCVer1:Generic_Account,SUBTRACCVer1:Branch),DUP,NOCASE
RegionKey                KEY(SUBTRACCVer1:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
UseAlternativeAdd           BYTE
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
ReplicateAccount            STRING(30)
FactorAccount               BYTE
ForceEstimate               BYTE
EstimateIfOver              REAL
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
Generic_Account             BYTE
StoresAccountNumber         STRING(30)
Force_Customer_Name         BYTE
FinanceContactName          STRING(30)
FinanceTelephoneNo          STRING(30)
FinanceEmailAddress         STRING(255)
FinanceAddress1             STRING(30)
FinanceAddress2             STRING(30)
FinanceAddress3             STRING(30)
FinancePostcode             STRING(30)
AccountLimit                REAL
ExcludeBouncer              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
ExcludeFromTATReport        BYTE
OverrideHeadVATNo           BYTE
Line500AccountNumber        STRING(30)
OverrideHeadMobile          BYTE
OverrideMobileDefault       BYTE
SIDJobBooking               BYTE
SIDJobEnquiry               BYTE
Region                      STRING(30)
AllowVCPLoanUnits           BYTE
Hub                         STRING(30)
RefurbishmentAccount        BYTE
VCPWaybillPrefix            STRING(30)
PrintOOWVCPFee              BYTE
OOWVCPFeeLabel              STRING(30)
OOWVCPFeeAmount             REAL
PrintWarrantyVCPFee         BYTE
WarrantyVCPFeeAmount        REAL
                         END
                       END
!------------ End File: SUBTRACC, version 1 ------------------

!------------ File: SUBTRACC, version 2 ----------------------
!------------ modified 29.11.2010 at 10:18:56 -----------------
MOD:stFileName:SUBTRACCVer2  STRING(260)
SUBTRACCVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUBTRACCVer2),PRE(SUBTRACCVer2),CREATE
RecordNumberKey          KEY(SUBTRACCVer2:RecordNumber),NOCASE,PRIMARY
Main_Account_Key         KEY(SUBTRACCVer2:Main_Account_Number,SUBTRACCVer2:Account_Number),NOCASE
Main_Name_Key            KEY(SUBTRACCVer2:Main_Account_Number,SUBTRACCVer2:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(SUBTRACCVer2:Account_Number),NOCASE
Branch_Key               KEY(SUBTRACCVer2:Branch),DUP,NOCASE
Main_Branch_Key          KEY(SUBTRACCVer2:Main_Account_Number,SUBTRACCVer2:Branch),DUP,NOCASE
Company_Name_Key         KEY(SUBTRACCVer2:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(SUBTRACCVer2:ReplicateAccount,SUBTRACCVer2:Account_Number),DUP,NOCASE
GenericAccountKey        KEY(SUBTRACCVer2:Generic_Account,SUBTRACCVer2:Account_Number),DUP,NOCASE
GenericCompanyKey        KEY(SUBTRACCVer2:Generic_Account,SUBTRACCVer2:Company_Name),DUP,NOCASE
GenericBranchKey         KEY(SUBTRACCVer2:Generic_Account,SUBTRACCVer2:Branch),DUP,NOCASE
RegionKey                KEY(SUBTRACCVer2:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
UseAlternativeAdd           BYTE
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
ReplicateAccount            STRING(30)
FactorAccount               BYTE
ForceEstimate               BYTE
EstimateIfOver              REAL
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
Generic_Account             BYTE
StoresAccountNumber         STRING(30)
Force_Customer_Name         BYTE
FinanceContactName          STRING(30)
FinanceTelephoneNo          STRING(30)
FinanceEmailAddress         STRING(255)
FinanceAddress1             STRING(30)
FinanceAddress2             STRING(30)
FinanceAddress3             STRING(30)
FinancePostcode             STRING(30)
AccountLimit                REAL
ExcludeBouncer              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
ExcludeFromTATReport        BYTE
OverrideHeadVATNo           BYTE
Line500AccountNumber        STRING(30)
OverrideHeadMobile          BYTE
OverrideMobileDefault       BYTE
SIDJobBooking               BYTE
SIDJobEnquiry               BYTE
Region                      STRING(30)
AllowVCPLoanUnits           BYTE
Hub                         STRING(30)
RefurbishmentAccount        BYTE
VCPWaybillPrefix            STRING(30)
PrintOOWVCPFee              BYTE
OOWVCPFeeLabel              STRING(30)
OOWVCPFeeAmount             REAL
PrintWarrantyVCPFee         BYTE
WarrantyVCPFeeAmount        REAL
DealerID                    STRING(30)
                         END
                       END
!------------ End File: SUBTRACC, version 2 ------------------

!------------ File: WAYCNR, version 1 ----------------------
!------------ modified 07.09.2011 at 11:02:16 -----------------
MOD:stFileName:WAYCNRVer1  STRING(260)
WAYCNRVer1             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:WAYCNRVer1),PRE(WAYCNRVer1)
RecordNumberKey          KEY(WAYCNRVer1:RecordNumber),NOCASE,PRIMARY
EnteredKey               KEY(WAYCNRVer1:WAYBILLSRecordnumber,WAYCNRVer1:RecordNumber),DUP,NOCASE
PartNumberKey            KEY(WAYCNRVer1:WAYBILLSRecordnumber,WAYCNRVer1:ExchangeOrder,WAYCNRVer1:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
WAYBILLSRecordnumber        LONG
PartNumber                  STRING(30)
Description                 STRING(30)
ExchangeOrder               BYTE
Quantity                    LONG
                         END
                       END
!------------ End File: WAYCNR, version 1 ------------------

!------------ File: WAYCNR, version 2 ----------------------
!------------ modified 08.09.2011 at 12:28:42 -----------------
MOD:stFileName:WAYCNRVer2  STRING(260)
WAYCNRVer2             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:WAYCNRVer2),PRE(WAYCNRVer2),CREATE
RecordNumberKey          KEY(WAYCNRVer2:RecordNumber),NOCASE,PRIMARY
EnteredKey               KEY(WAYCNRVer2:WAYBILLSRecordnumber,WAYCNRVer2:RecordNumber),DUP,NOCASE
PartNumberKey            KEY(WAYCNRVer2:WAYBILLSRecordnumber,WAYCNRVer2:ExchangeOrder,WAYCNRVer2:PartNumber),DUP,NOCASE
ProcessedKey             KEY(WAYCNRVer2:WAYBILLSRecordnumber,WAYCNRVer2:Processed,WAYCNRVer2:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
WAYBILLSRecordnumber        LONG
PartNumber                  STRING(30)
Description                 STRING(30)
ExchangeOrder               BYTE
Quantity                    LONG
QuantityReceived            LONG
Processed                   BYTE
                         END
                       END
!------------ End File: WAYCNR, version 2 ------------------

!------------ File: MANUFACT, version 1 ----------------------
!------------ modified 28.06.2010 at 10:56:20 -----------------
MOD:stFileName:MANUFACTVer1  STRING(260)
MANUFACTVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer1),PRE(MANUFACTVer1)
RecordNumberKey          KEY(MANUFACTVer1:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer1:Manufacturer),NOCASE
EDIFileTypeKey           KEY(MANUFACTVer1:EDIFileType,MANUFACTVer1:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
                         END
                       END
!------------ End File: MANUFACT, version 1 ------------------

!------------ File: MANUFACT, version 2 ----------------------
!------------ modified 28.06.2010 at 11:19:06 -----------------
MOD:stFileName:MANUFACTVer2  STRING(260)
MANUFACTVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer2),PRE(MANUFACTVer2),CREATE
RecordNumberKey          KEY(MANUFACTVer2:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer2:Manufacturer),NOCASE
EDIFileTypeKey           KEY(MANUFACTVer2:EDIFileType,MANUFACTVer2:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
                         END
                       END
!------------ End File: MANUFACT, version 2 ------------------

!------------ File: MANUFACT, version 3 ----------------------
!------------ modified 14.10.2010 at 09:59:25 -----------------
MOD:stFileName:MANUFACTVer3  STRING(260)
MANUFACTVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer3),PRE(MANUFACTVer3),CREATE
RecordNumberKey          KEY(MANUFACTVer3:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer3:Manufacturer),NOCASE
EDIFileTypeKey           KEY(MANUFACTVer3:EDIFileType,MANUFACTVer3:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
EDItransportFee             REAL
                         END
                       END
!------------ End File: MANUFACT, version 3 ------------------

!------------ File: MANUFACT, version 4 ----------------------
!------------ modified 03.02.2011 at 14:23:35 -----------------
MOD:stFileName:MANUFACTVer4  STRING(260)
MANUFACTVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer4),PRE(MANUFACTVer4),CREATE
RecordNumberKey          KEY(MANUFACTVer4:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer4:Manufacturer),NOCASE
EDIFileTypeKey           KEY(MANUFACTVer4:EDIFileType,MANUFACTVer4:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
EDItransportFee             REAL
SagemVersionNumber          STRING(30)
                         END
                       END
!------------ End File: MANUFACT, version 4 ------------------

!------------ File: MANUFACT, version 5 ----------------------
!------------ modified 24.05.2011 at 12:11:22 -----------------
MOD:stFileName:MANUFACTVer5  STRING(260)
MANUFACTVer5           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer5),PRE(MANUFACTVer5),CREATE
RecordNumberKey          KEY(MANUFACTVer5:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer5:Manufacturer),NOCASE
EDIFileTypeKey           KEY(MANUFACTVer5:EDIFileType,MANUFACTVer5:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
EDItransportFee             REAL
SagemVersionNumber          STRING(30)
UseBouncerRules             BYTE
BouncerPeriod               LONG
BouncerType                 BYTE
BouncerInFault              BYTE
BouncerOutFault             BYTE
BouncerSpares               BYTE
BouncerPeriodIMEI           LONG
DoNotBounceWarranty         BYTE
                         END
                       END
!------------ End File: MANUFACT, version 5 ------------------

!------------ File: MANUFACT, version 6 ----------------------
!------------ modified 29.06.2011 at 20:14:48 -----------------
MOD:stFileName:MANUFACTVer6  STRING(260)
MANUFACTVer6           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer6),PRE(MANUFACTVer6),CREATE
RecordNumberKey          KEY(MANUFACTVer6:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer6:Manufacturer),NOCASE
EDIFileTypeKey           KEY(MANUFACTVer6:EDIFileType,MANUFACTVer6:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
EDItransportFee             REAL
SagemVersionNumber          STRING(30)
UseBouncerRules             BYTE
BouncerPeriod               LONG
BouncerType                 BYTE
BouncerInFault              BYTE
BouncerOutFault             BYTE
BouncerSpares               BYTE
BouncerPeriodIMEI           LONG
DoNotBounceWarranty         BYTE
BounceRulesType             BYTE
                         END
                       END
!------------ End File: MANUFACT, version 6 ------------------

!------------ File: MANUFACT, version 7 ----------------------
!------------ modified 29.06.2011 at 20:25:40 -----------------
MOD:stFileName:MANUFACTVer7  STRING(260)
MANUFACTVer7           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer7),PRE(MANUFACTVer7),CREATE
RecordNumberKey          KEY(MANUFACTVer7:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer7:Manufacturer),NOCASE
EDIFileTypeKey           KEY(MANUFACTVer7:EDIFileType,MANUFACTVer7:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
EDItransportFee             REAL
SagemVersionNumber          STRING(30)
UseBouncerRules             BYTE
BouncerPeriod               LONG
BouncerType                 BYTE
BouncerInFault              BYTE
BouncerOutFault             BYTE
BouncerSpares               BYTE
BouncerPeriodIMEI           LONG
DoNotBounceWarranty         BYTE
                         END
                       END
!------------ End File: MANUFACT, version 7 ------------------

!------------ File: MANUFACT, version 8 ----------------------
!------------ modified 29.06.2011 at 20:35:01 -----------------
MOD:stFileName:MANUFACTVer8  STRING(260)
MANUFACTVer8           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer8),PRE(MANUFACTVer8),CREATE
RecordNumberKey          KEY(MANUFACTVer8:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer8:Manufacturer),NOCASE
EDIFileTypeKey           KEY(MANUFACTVer8:EDIFileType,MANUFACTVer8:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
EDItransportFee             REAL
SagemVersionNumber          STRING(30)
UseBouncerRules             BYTE
BouncerPeriod               LONG
BouncerType                 BYTE
BouncerInFault              BYTE
BouncerOutFault             BYTE
BouncerSpares               BYTE
BouncerPeriodIMEI           LONG
DoNotBounceWarranty         BYTE
BounceRulesType             BYTE
                         END
                       END
!------------ End File: MANUFACT, version 8 ------------------

!------------ File: MANUFACT, version 9 ----------------------
!------------ modified 02.02.2012 at 16:52:32 -----------------
MOD:stFileName:MANUFACTVer9  STRING(260)
MANUFACTVer9           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer9),PRE(MANUFACTVer9),CREATE
RecordNumberKey          KEY(MANUFACTVer9:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer9:Manufacturer),NOCASE
EDIFileTypeKey           KEY(MANUFACTVer9:EDIFileType,MANUFACTVer9:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
EDItransportFee             REAL
SagemVersionNumber          STRING(30)
UseBouncerRules             BYTE
BouncerPeriod               LONG
BouncerType                 BYTE
BouncerInFault              BYTE
BouncerOutFault             BYTE
BouncerSpares               BYTE
BouncerPeriodIMEI           LONG
DoNotBounceWarranty         BYTE
BounceRulesType             BYTE
ThirdPartyHandlingFee       REAL
                         END
                       END
!------------ End File: MANUFACT, version 9 ------------------

!------------ File: MANUFACT, version 10 ----------------------
!------------ modified 30.03.2012 at 09:30:24 -----------------
MOD:stFileName:MANUFACTVer10  STRING(260)
MANUFACTVer10          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer10),PRE(MANUFACTVer10),CREATE
RecordNumberKey          KEY(MANUFACTVer10:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer10:Manufacturer),NOCASE
EDIFileTypeKey           KEY(MANUFACTVer10:EDIFileType,MANUFACTVer10:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
EDItransportFee             REAL
SagemVersionNumber          STRING(30)
UseBouncerRules             BYTE
BouncerPeriod               LONG
BouncerType                 BYTE
BouncerInFault              BYTE
BouncerOutFault             BYTE
BouncerSpares               BYTE
BouncerPeriodIMEI           LONG
DoNotBounceWarranty         BYTE
BounceRulesType             BYTE
ThirdPartyHandlingFee       REAL
ProductCodeCompulsory       STRING(3)
                         END
                       END
!------------ End File: MANUFACT, version 10 ------------------

!------------ Declaration Current Files Structure --------------------

!------------ modified 17.03.2015 at 11:25:26 -----------------
MOD:stFileName:WAYBCONFVer2  STRING(260)
WAYBCONFVer2           FILE,DRIVER('Btrieve'),OEM,PRE(WAYBCONFVer2),NAME('WAYBCONF'),CREATE
KeyRecordNo              KEY(WAYBCONFVer2:RecordNo),NOCASE,PRIMARY
KeyWaybillNo             KEY(WAYBCONFVer2:WaybillNo),DUP,NOCASE
KeyAccountGenerateDateTime KEY(WAYBCONFVer2:AccountNumber,WAYBCONFVer2:GenerateDate,WAYBCONFVer2:GenerateTime),DUP,NOCASE
KeyAccountConfirmDateTime KEY(WAYBCONFVer2:AccountNumber,WAYBCONFVer2:ConfirmationSent,WAYBCONFVer2:ConfirmDate,WAYBCONFVer2:ConfirmTime),DUP,NOCASE
KeyAccountDeliveredDateTime KEY(WAYBCONFVer2:AccountNumber,WAYBCONFVer2:Delivered,WAYBCONFVer2:DeliverDate,WAYBCONFVer2:DeliverTime),DUP,NOCASE
KeyAccountReceivedDateTime KEY(WAYBCONFVer2:AccountNumber,WAYBCONFVer2:Received,WAYBCONFVer2:ReceiveDate,WAYBCONFVer2:ReceiveTime),DUP,NOCASE
KeyConfirmOnly           KEY(WAYBCONFVer2:ConfirmationSent),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
AccountNumber               STRING(30)
WaybillNo                   LONG
GenerateDate                DATE
GenerateTime                TIME
ConfirmationSent            BYTE
ConfirmationReceived        BYTE
ConfirmDate                 DATE
ConfirmTime                 TIME
ConfirmResentQty            LONG
Delivered                   BYTE
DeliverDate                 DATE
DeliverTime                 TIME
Received                    BYTE
ReceiveDate                 DATE
ReceiveTime                 TIME
                         END
                       END

!------------ modified 20.10.2014 at 15:59:09 -----------------
MOD:stFileName:SOAPERRSVer2  STRING(260)
SOAPERRSVer2           FILE,DRIVER('Btrieve'),OEM,NAME('SOAPERRS.DAT'),PRE(SOAPERRSVer2),CREATE
KeyRecordNumber          KEY(SOAPERRSVer2:RecordNumber),NOCASE,PRIMARY
KeyErrorCode             KEY(SOAPERRSVer2:ErrorCode),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ErrorCode                   STRING(20)
SoapFault                   STRING(50)
ErrorType                   STRING(20)
Description                 STRING(100)
Action                      STRING(15)
Subject                     STRING(100)
Body                        STRING(100)
                         END
                       END

!------------ modified 05.01.2015 at 15:58:15 -----------------
MOD:stFileName:JOBSE3Ver3  STRING(260)
JOBSE3Ver3             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE3.DAT'),PRE(JOBSE3Ver3),CREATE
KeyRecordNumber          KEY(JOBSE3Ver3:RecordNumber),NOCASE,PRIMARY
KeyRefNumber             KEY(JOBSE3Ver3:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
LoyaltyStatus               STRING(250)
Exchange_IntLocation        STRING(30)
SpecificNeeds               BYTE
SN_Software_Installed       BYTE
FaultCode21                 STRING(30)
FaultCode22                 STRING(30)
FaultCode23                 STRING(30)
FaultCode24                 STRING(30)
FaultCode25                 STRING(30)
FaultCode26                 STRING(30)
FaultCode27                 STRING(30)
FaultCode28                 STRING(30)
FaultCode29                 STRING(30)
FaultCode30                 STRING(30)
                         END
                       END

!------------ modified 12.11.2012 at 11:41:02 -----------------
MOD:stFileName:STOAUUSEVer2  STRING(260)
STOAUUSEVer2           FILE,DRIVER('Btrieve'),NAME('STOAUUSE.DAT'),PRE(STOAUUSEVer2),CREATE
KeyRecordNo              KEY(STOAUUSEVer2:Record_No),NOCASE,PRIMARY
KeyAuditTypeNo           KEY(STOAUUSEVer2:AuditType,STOAUUSEVer2:Audit_No),DUP,NOCASE
KeyAuditTypeNoUser       KEY(STOAUUSEVer2:AuditType,STOAUUSEVer2:Audit_No,STOAUUSEVer2:User_code),DUP,NOCASE
KeyTypeNoPart            KEY(STOAUUSEVer2:AuditType,STOAUUSEVer2:Audit_No,STOAUUSEVer2:StockPartNumber),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
AuditType                   STRING(1)
Audit_No                    LONG
User_code                   STRING(3)
Current                     STRING(1)
StockPartNumber             STRING(30)
                         END
                       END

!------------ modified 06.09.2012 at 15:52:39 -----------------
MOD:stFileName:PREJOBVer3  STRING(260)
PREJOBVer3             FILE,DRIVER('Btrieve'),OEM,NAME('PREJOB.DAT'),PRE(PREJOBVer3),CREATE
KeyRefNumber             KEY(PREJOBVer3:RefNumber),NOCASE,PRIMARY
KeyAPIRecordNo           KEY(PREJOBVer3:APIRecordNumber),DUP,NOCASE
KeyJobRef_Number         KEY(PREJOBVer3:JobRef_number),DUP,NOCASE
KeyESN                   KEY(PREJOBVer3:ESN),DUP,NOCASE
KeyDateTime              KEY(PREJOBVer3:Date_booked,PREJOBVer3:Time_Booked),DUP,NOCASE
KeyESNDateTime           KEY(PREJOBVer3:ESN,PREJOBVer3:Date_booked,PREJOBVer3:Time_Booked),DUP,NOCASE
KeyPortalRef             KEY(PREJOBVer3:VodacomPortalRef),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
APIRecordNumber             LONG
JobRef_number               LONG
VodacomPortalRef            STRING(30)
Title                       STRING(5)
Initial                     STRING(1)
Surname                     STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Suburb                      STRING(30)
Region                      STRING(30)
Telephone_number            STRING(15)
Mobile_number               STRING(15)
ID_number                   STRING(30)
ESN                         STRING(20)
Manufacturer                STRING(30)
Model_number                STRING(30)
DOP                         DATE
FaultText                   STRING(200)
RepairOutletRef             LONG
Date_booked                 DATE
Time_Booked                 TIME
ChargeType                  STRING(30)
                         END
                       END

!------------ modified 17.09.2013 at 12:27:11 -----------------
MOD:stFileName:TRADEAC2Ver4  STRING(260)
TRADEAC2Ver4           FILE,DRIVER('Btrieve'),OEM,PRE(TRADEAC2Ver4),NAME('TRADEAC2'),CREATE
KeyRecordNo              KEY(TRADEAC2Ver4:RecordNo),NOCASE,PRIMARY
KeyAccountNumber         KEY(TRADEAC2Ver4:Account_Number),NOCASE
KeyPreBookRegion         KEY(TRADEAC2Ver4:Pre_Book_Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Account_Number              STRING(15)
coSMSname                   STRING(40)
SMS_Email2                  STRING(100)
SMS_Email3                  STRING(100)
SMS_ReportEmail             STRING(255)
UseSparesRequest            STRING(3)
Pre_Book_Region             STRING(30)
SBOnlineDespatch            BYTE
SBOnlineStock               BYTE
SBOnlineQAEtc               BYTE
SBOnlineWarrClaims          BYTE
SBOnlineWaybills            BYTE
SBOnlineTradeAccs           BYTE
SBOnlineEngineer            BYTE
SBOnlineAudits              BYTE
SBOnlineUsers               BYTE
SBOnlineLoansExch           BYTE
SBOnlineBouncers            BYTE
SBOnlineReports             BYTE
SBOnlineSpare1              BYTE
SBOnlineSpare2              BYTE
SBOnlineSpare3              BYTE
                         END
                       END

!------------ modified 28.03.2012 at 10:48:27 -----------------
MOD:stFileName:SMSRECVDVer5  STRING(260)
SMSRECVDVer5           FILE,DRIVER('Btrieve'),OEM,NAME('SMSRECVD.DAT'),PRE(SMSRECVDVer5),CREATE
KeyRecordNo              KEY(SMSRECVDVer5:RecordNo),NOCASE,PRIMARY
KeyJob_Ref_Number        KEY(SMSRECVDVer5:Job_Ref_Number),DUP,NOCASE
KeyJob_Date_Time_Dec     KEY(SMSRECVDVer5:Job_Ref_Number,-SMSRECVDVer5:DateReceived,-SMSRECVDVer5:TimeReceived),DUP,NOCASE
KeyAccount_Date_Time     KEY(SMSRECVDVer5:AccountNumber,SMSRECVDVer5:DateReceived,SMSRECVDVer5:TextReceived),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Job_Ref_Number              LONG
AccountNumber               STRING(15)
MSISDN                      STRING(15)
DateReceived                DATE
TimeReceived                TIME
TextReceived                STRING(200)
SMSType                     STRING(1)
                         END
                       END

!------------ modified 30.10.2012 at 09:36:51 -----------------
MOD:stFileName:GRNOTESVer3  STRING(260)
GRNOTESVer3            FILE,DRIVER('Btrieve'),OEM,NAME('GRNOTES.DAT'),PRE(GRNOTESVer3),CREATE
Goods_Received_Number_Key KEY(GRNOTESVer3:Goods_Received_Number),NOCASE,PRIMARY
Order_Number_Key         KEY(GRNOTESVer3:Order_Number,GRNOTESVer3:Goods_Received_Date),DUP,NOCASE
Date_Recd_Key            KEY(GRNOTESVer3:Goods_Received_Date),DUP,NOCASE
NotPrintedGRNKey         KEY(GRNOTESVer3:BatchRunNotPrinted,GRNOTESVer3:Goods_Received_Number),DUP,NOCASE
NotPrintedOrderKey       KEY(GRNOTESVer3:BatchRunNotPrinted,GRNOTESVer3:Order_Number,GRNOTESVer3:Goods_Received_Number),DUP,NOCASE
KeyEVO_Status            KEY(GRNOTESVer3:EVO_Status),DUP,NOCASE
Record                   RECORD,PRE()
Goods_Received_Number       LONG
Order_Number                LONG
Goods_Received_Date         DATE
CurrencyCode                STRING(30)
DailyRate                   REAL
DivideMultiply              STRING(1)
BatchRunNotPrinted          BYTE
Uncaptured                  BYTE
EVO_Status                  STRING(1)
                         END
                       END

!------------ modified 24.10.2012 at 14:24:41 -----------------
MOD:stFileName:EXCHAUIVer2  STRING(260)
EXCHAUIVer2            FILE,DRIVER('Btrieve'),OEM,NAME('EXCHAUI.DAT'),PRE(EXCHAUIVer2),CREATE
Internal_No_Key          KEY(EXCHAUIVer2:Internal_No),NOCASE,PRIMARY
Audit_Number_Key         KEY(EXCHAUIVer2:Audit_Number),DUP,NOCASE
Ref_Number_Key           KEY(EXCHAUIVer2:Ref_Number),DUP,NOCASE
Exch_Browse_Key          KEY(EXCHAUIVer2:Audit_Number,EXCHAUIVer2:Exists),DUP,NOCASE
AuditIMEIKey             KEY(EXCHAUIVer2:Audit_Number,EXCHAUIVer2:New_IMEI),DUP,NOCASE
Locate_IMEI_Key          KEY(EXCHAUIVer2:Audit_Number,EXCHAUIVer2:Site_Location,EXCHAUIVer2:IMEI_Number),DUP,NOCASE
Main_Browse_Key          KEY(EXCHAUIVer2:Audit_Number,EXCHAUIVer2:Confirmed,EXCHAUIVer2:Site_Location,EXCHAUIVer2:Stock_Type,EXCHAUIVer2:Shelf_Location,EXCHAUIVer2:Internal_No),DUP,NOCASE
Record                   RECORD,PRE()
Internal_No                 LONG
Site_Location               STRING(30)
Shelf_Location              STRING(30)
Location                    STRING(30)
Audit_Number                LONG
Ref_Number                  LONG
Exists                      BYTE
New_IMEI                    BYTE
IMEI_Number                 STRING(20)
Confirmed                   BYTE
Stock_Type                  STRING(30)
                         END
                       END

!------------ modified 23.09.2011 at 12:26:37 -----------------
MOD:stFileName:RTNAWAITVer7  STRING(260)
RTNAWAITVer7           FILE,DRIVER('Btrieve'),OEM,NAME('RTNAWAIT.DAT'),PRE(RTNAWAITVer7),CREATE
RecordNumberKey          KEY(RTNAWAITVer7:RecordNumber),NOCASE,PRIMARY
ArcProPartKey            KEY(RTNAWAITVer7:Archive,RTNAWAITVer7:Processed,RTNAWAITVer7:PartNumber),DUP,NOCASE
ArcProDescKey            KEY(RTNAWAITVer7:Archive,RTNAWAITVer7:Processed,RTNAWAITVer7:Description),DUP,NOCASE
ArcProExcPartKey         KEY(RTNAWAITVer7:Archive,RTNAWAITVer7:Processed,RTNAWAITVer7:ExchangeOrder,RTNAWAITVer7:PartNumber),DUP,NOCASE
ArcProExcDescKey         KEY(RTNAWAITVer7:Archive,RTNAWAITVer7:Processed,RTNAWAITVer7:ExchangeOrder,RTNAWAITVer7:Description),DUP,NOCASE
ArcProCNRKey             KEY(RTNAWAITVer7:Archive,RTNAWAITVer7:Processed,RTNAWAITVer7:CNRRecordNumber),DUP,NOCASE
ArcProExcCNRKey          KEY(RTNAWAITVer7:Archive,RTNAWAITVer7:Processed,RTNAWAITVer7:ExchangeOrder,RTNAWAITVer7:CNRRecordNumber),DUP,NOCASE
RTNORDERKey              KEY(RTNAWAITVer7:RTNRecordNumber),DUP,NOCASE
CREDNOTRKey              KEY(RTNAWAITVer7:CNRRecordNumber),DUP,NOCASE
GRNNumberKey             KEY(RTNAWAITVer7:GRNNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archive                     BYTE
DateCreated                 DATE
TimeCreated                 TIME
UserCode                    STRING(3)
Location                    STRING(30)
RTNRecordNumber             LONG
CNRRecordNumber             LONG
RefNumber                   LONG
ExchangeOrder               BYTE
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
Processed                   BYTE
DateProcessed               DATE
TimeProcessed               TIME
UsercodeProcessed           STRING(3)
AcceptReject                STRING(6)
NewRefNumber                LONG
GRNNumber                   LONG
                         END
                       END

!------------ modified 25.09.2012 at 12:39:49 -----------------
MOD:stFileName:STMASAUDVer2  STRING(260)
STMASAUDVer2           FILE,DRIVER('Btrieve'),NAME('STMASAUD.DAT'),PRE(STMASAUDVer2),CREATE
AutoIncrement_Key        KEY(-STMASAUDVer2:Audit_No),NOCASE,PRIMARY
Compeleted_Key           KEY(STMASAUDVer2:Complete),DUP,NOCASE
Sent_Key                 KEY(STMASAUDVer2:Send),DUP,NOCASE
Record                   RECORD,PRE()
Audit_No                    LONG
branch                      STRING(30)
branch_id                   LONG
Audit_Date                  LONG
Audit_Time                  LONG
End_Date                    LONG
End_Time                    LONG
Audit_User                  STRING(3)
Complete                    STRING(1)
Send                        STRING(1)
CompleteType                STRING(1)
                         END
                       END

!------------ modified 13.02.2014 at 15:36:56 -----------------
MOD:stFileName:ORDITEMSVer2  STRING(260)
ORDITEMSVer2           FILE,DRIVER('Btrieve'),NAME('ORDITEMS.DAT'),PRE(ORDITEMSVer2),CREATE
Keyordhno                KEY(ORDITEMSVer2:ordhno),DUP,NOCASE
recordnumberkey          KEY(ORDITEMSVer2:recordnumber),NOCASE,PRIMARY
OrdHNoPartKey            KEY(ORDITEMSVer2:ordhno,ORDITEMSVer2:partno),DUP,NOCASE
PartNoKey                KEY(ORDITEMSVer2:partno),DUP,NOCASE
Record                   RECORD,PRE()
recordnumber                LONG
ordhno                      LONG
manufact                    STRING(40)
partno                      STRING(20)
partdiscription             STRING(60)
qty                         LONG
itemcost                    REAL
totalcost                   REAL
OrderDate                   DATE
OrderTime                   TIME
                         END
                       END

!------------ modified 09.12.2014 at 14:23:06 -----------------
MOD:stFileName:LOCATLOGVer2  STRING(260)
LOCATLOGVer2           FILE,DRIVER('Btrieve'),OEM,NAME('LOCATLOG.DAT'),PRE(LOCATLOGVer2),CREATE
RecordNumberKey          KEY(LOCATLOGVer2:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(LOCATLOGVer2:RefNumber,LOCATLOGVer2:TheDate),DUP,NOCASE
NewLocationKey           KEY(LOCATLOGVer2:RefNumber,LOCATLOGVer2:NewLocation),DUP,NOCASE
DateNewLocationKey       KEY(LOCATLOGVer2:NewLocation,LOCATLOGVer2:TheDate,LOCATLOGVer2:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TheDate                     DATE
TheTime                     TIME
UserCode                    STRING(30)
PreviousLocation            STRING(30)
NewLocation                 STRING(30)
Exchange                    STRING(1)
                         END
                       END

!------------ modified 14.02.2014 at 16:25:48 -----------------
MOD:stFileName:DEFAULT2Ver5  STRING(260)
DEFAULT2Ver5           FILE,DRIVER('Btrieve'),OEM,NAME('DEFAULT2.DAT'),PRE(DEFAULT2Ver5),CREATE
RecordNumberKey          KEY(DEFAULT2Ver5:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
UserSkillLevel              BYTE
Inv2CompanyName             STRING(30)
Inv2AddressLine1            STRING(30)
Inv2AddressLine2            STRING(30)
Inv2AddressLine3            STRING(30)
Inv2Postcode                STRING(15)
Inv2TelephoneNumber         STRING(15)
Inv2FaxNumber               STRING(15)
Inv2EmailAddress            STRING(255)
Inv2VATNumber               STRING(30)
CurrencySymbol              STRING(3)
UseRequisitionNumber        BYTE
PickEngStockOnly            BYTE
AllocateEngPassword         BYTE
PrintRetainedAccLabel       BYTE
OverdueBackOrderText        STRING(255)
EmailUserName               STRING(255)
EmailPassword               STRING(60)
GlobalPrintText             STRING(255)
DefaultFromEmail            STRING(100)
PLE                         DATE
J_Collection_Rate           LONG
                         END
                       END

!------------ modified 18.01.2012 at 16:20:32 -----------------
MOD:stFileName:RETSALESVer3  STRING(260)
RETSALESVer3           FILE,DRIVER('Btrieve'),OEM,NAME('RETSALES.DAT'),PRE(RETSALESVer3),CREATE
Ref_Number_Key           KEY(RETSALESVer3:Ref_Number),NOCASE,PRIMARY
Invoice_Number_Key       KEY(RETSALESVer3:Invoice_Number),DUP,NOCASE
Despatched_Purchase_Key  KEY(RETSALESVer3:Despatched,RETSALESVer3:Purchase_Order_Number),DUP,NOCASE
Despatched_Ref_Key       KEY(RETSALESVer3:Despatched,RETSALESVer3:Ref_Number),DUP,NOCASE
Despatched_Account_Key   KEY(RETSALESVer3:Despatched,RETSALESVer3:Account_Number,RETSALESVer3:Purchase_Order_Number),DUP,NOCASE
Account_Number_Key       KEY(RETSALESVer3:Account_Number,RETSALESVer3:Ref_Number),DUP,NOCASE
Purchase_Number_Key      KEY(RETSALESVer3:Purchase_Order_Number),DUP,NOCASE
Despatch_Number_Key      KEY(RETSALESVer3:Despatch_Number),DUP,NOCASE
Date_Booked_Key          KEY(RETSALESVer3:date_booked),DUP,NOCASE
AccountInvoiceKey        KEY(RETSALESVer3:Account_Number,RETSALESVer3:Invoice_Number),DUP,NOCASE
DespatchedPurchDateKey   KEY(RETSALESVer3:Despatched,RETSALESVer3:Purchase_Order_Number,RETSALESVer3:date_booked),DUP,NOCASE
DespatchNoRefNoKey       KEY(RETSALESVer3:Despatch_Number,RETSALESVer3:Ref_Number),DUP,NOCASE
AccountPurchaseNumberKey KEY(RETSALESVer3:Account_Number,RETSALESVer3:Purchase_Order_Number),DUP,NOCASE
WaybillNumberKey         KEY(RETSALESVer3:WaybillNumber,RETSALESVer3:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Account_Number              STRING(15)
Contact_Name                STRING(30)
Purchase_Order_Number       STRING(30)
Delivery_Collection         STRING(3)
Payment_Method              STRING(3)
Courier_Cost                REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier                     STRING(30)
Consignment_Number          STRING(30)
Date_Despatched             DATE
Despatched                  STRING(3)
Despatch_Number             LONG
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Courier_Cost        REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
WebOrderNumber              LONG
WebDateCreated              DATE
WebTimeCreated              STRING(20)
WebCreatedUser              STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Building_Name               STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Postcode_Delivery           STRING(10)
Company_Name_Delivery       STRING(30)
Building_Name_Delivery      STRING(30)
Address_Line1_Delivery      STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Fax_Number_Delivery         STRING(15)
Delivery_Text               STRING(255)
Invoice_Text                STRING(255)
WaybillNumber               LONG
DatePickingNotePrinted      DATE
TimePickingNotePrinted      TIME
ExchangeOrder               BYTE
LoanOrder                   BYTE
                         END
                       END

!------------ modified 18.01.2012 at 16:20:33 -----------------
MOD:stFileName:LOAORDRVer2  STRING(260)
LOAORDRVer2            FILE,DRIVER('Btrieve'),OEM,PRE(LOAORDRVer2),NAME('LOAORDR'),CREATE
Ref_Number_Key           KEY(LOAORDRVer2:Ref_Number),NOCASE,PRIMARY
By_Location_Key          KEY(LOAORDRVer2:Location,LOAORDRVer2:Manufacturer,LOAORDRVer2:Model_Number),DUP,NOCASE
OrderNumberKey           KEY(LOAORDRVer2:Location,LOAORDRVer2:OrderNumber),DUP,NOCASE
LocationReceivedKey      KEY(LOAORDRVer2:Received,LOAORDRVer2:Location,LOAORDRVer2:Manufacturer,LOAORDRVer2:Model_Number),DUP,NOCASE
OrderNumberOnlyKey       KEY(LOAORDRVer2:OrderNumber),DUP,NOCASE
LocationDateReceivedKey  KEY(LOAORDRVer2:Location,LOAORDRVer2:DateReceived),DUP,NOCASE
LoanRefNumberKey         KEY(LOAORDRVer2:LoanRefNumber),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Location                    STRING(30)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Qty_Required                LONG
Qty_Received                LONG
Notes                       STRING(255)
DateCreated                 DATE
DateOrdered                 DATE
DateReceived                DATE
OrderNumber                 LONG
Received                    BYTE
LoanRefNumber               LONG
                         END
                       END

!------------ modified 27.04.2012 at 15:14:22 -----------------
MOD:stFileName:WIPAMFVer2  STRING(260)
WIPAMFVer2             FILE,DRIVER('Btrieve'),OEM,NAME('WIPAMF.DAT'),PRE(WIPAMFVer2),CREATE
Audit_Number_Key         KEY(WIPAMFVer2:Audit_Number),NOCASE,PRIMARY
Secondary_Key            KEY(WIPAMFVer2:Complete_Flag,WIPAMFVer2:Audit_Number),DUP,NOCASE
Record                   RECORD,PRE()
Audit_Number                LONG
Date                        DATE
Time                        LONG
User                        STRING(3)
Status                      STRING(30)
Site_location               STRING(30)
Complete_Flag               BYTE
Ignore_IMEI                 BYTE
Ignore_Job_Number           BYTE
Date_Completed              DATE
Time_Completed              TIME
                         END
                       END

!------------ modified 12.07.2012 at 09:59:18 -----------------
MOD:stFileName:TEAMSVer3  STRING(260)
TEAMSVer3              FILE,DRIVER('Btrieve'),OEM,NAME('TEAMS.DAT'),PRE(TEAMSVer3),CREATE
Record_Number_Key        KEY(TEAMSVer3:Record_Number),NOCASE,PRIMARY
Team_Key                 KEY(TEAMSVer3:Team),NOCASE
KeyTraceAccount_number   KEY(TEAMSVer3:TradeAccount_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Team                        STRING(30)
TradeAccount_Number         STRING(15)
Associated                  STRING(1)
                         END
                       END

!------------ modified 27.04.2012 at 15:14:22 -----------------
MOD:stFileName:WIPAUIVer2  STRING(260)
WIPAUIVer2             FILE,DRIVER('Btrieve'),OEM,NAME('WIPAUI.DAT'),PRE(WIPAUIVer2),CREATE
Internal_No_Key          KEY(WIPAUIVer2:Internal_No),NOCASE,PRIMARY
Audit_Number_Key         KEY(WIPAUIVer2:Audit_Number),DUP,NOCASE
Ref_Number_Key           KEY(WIPAUIVer2:Ref_Number),DUP,NOCASE
Exch_Browse_Key          KEY(WIPAUIVer2:Audit_Number),DUP,NOCASE
AuditIMEIKey             KEY(WIPAUIVer2:Audit_Number,WIPAUIVer2:New_In_Status),DUP,NOCASE
Locate_IMEI_Key          KEY(WIPAUIVer2:Audit_Number,WIPAUIVer2:Site_Location,WIPAUIVer2:Ref_Number),DUP,NOCASE
Main_Browse_Key          KEY(WIPAUIVer2:Audit_Number,WIPAUIVer2:Confirmed,WIPAUIVer2:Site_Location,WIPAUIVer2:Status,WIPAUIVer2:Ref_Number),DUP,NOCASE
AuditRefNumberKey        KEY(WIPAUIVer2:Audit_Number,WIPAUIVer2:Ref_Number),DUP,NOCASE
AuditStatusRefNoKey      KEY(WIPAUIVer2:Audit_Number,WIPAUIVer2:Status,WIPAUIVer2:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Internal_No                 LONG
Site_Location               STRING(30)
Status                      STRING(30)
Audit_Number                LONG
Ref_Number                  LONG
New_In_Status               BYTE
Confirmed                   BYTE
IsExchange                  BYTE
                         END
                       END

!------------ modified 14.02.2014 at 10:40:38 -----------------
MOD:stFileName:EXCHORDRVer6  STRING(260)
EXCHORDRVer6           FILE,DRIVER('Btrieve'),OEM,NAME('EXCHORDR.DAT'),PRE(EXCHORDRVer6),CREATE
Ref_Number_Key           KEY(EXCHORDRVer6:Ref_Number),NOCASE,PRIMARY
By_Location_Key          KEY(EXCHORDRVer6:Location,EXCHORDRVer6:Manufacturer,EXCHORDRVer6:Model_Number),DUP,NOCASE
OrderNumberKey           KEY(EXCHORDRVer6:Location,EXCHORDRVer6:OrderNumber),DUP,NOCASE
LocationReceivedKey      KEY(EXCHORDRVer6:Received,EXCHORDRVer6:Location,EXCHORDRVer6:Manufacturer,EXCHORDRVer6:Model_Number),DUP,NOCASE
OrderNumberOnlyKey       KEY(EXCHORDRVer6:OrderNumber),DUP,NOCASE
LocationDateReceivedKey  KEY(EXCHORDRVer6:Location,EXCHORDRVer6:DateReceived),DUP,NOCASE
ExchangeRefNumberKey     KEY(EXCHORDRVer6:ExchangeRefNumber),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Location                    STRING(30)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Qty_Required                LONG
Qty_Received                LONG
Notes                       STRING(255)
DateCreated                 DATE
DateOrdered                 DATE
DateReceived                DATE
OrderNumber                 LONG
Received                    BYTE
ExchangeRefNumber           LONG
TimeCreated                 TIME
                         END
                       END

!------------ modified 16.08.2012 at 12:55:07 -----------------
MOD:stFileName:CONTHISTVer2  STRING(260)
CONTHISTVer2           FILE,DRIVER('Btrieve'),NAME('CONTHIST.DAT'),PRE(CONTHISTVer2),CREATE
Ref_Number_Key           KEY(CONTHISTVer2:Ref_Number,-CONTHISTVer2:Date,-CONTHISTVer2:Time,CONTHISTVer2:Action),DUP,NOCASE
Action_Key               KEY(CONTHISTVer2:Ref_Number,CONTHISTVer2:Action,-CONTHISTVer2:Date),DUP,NOCASE
User_Key                 KEY(CONTHISTVer2:Ref_Number,CONTHISTVer2:User,-CONTHISTVer2:Date),DUP,NOCASE
Record_Number_Key        KEY(CONTHISTVer2:Record_Number),NOCASE,PRIMARY
KeyRefSticky             KEY(CONTHISTVer2:Ref_Number,CONTHISTVer2:SN_StickyNote),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Notes                       STRING(2000)
SystemHistory               BYTE
SN_StickyNote               STRING(1)
SN_Completed                STRING(1)
SN_EngAlloc                 STRING(1)
SN_EngUpdate                STRING(1)
SN_CustService              STRING(1)
SN_WaybillConf              STRING(1)
SN_Despatch                 STRING(1)
                         END
                       END

!------------ modified 17.05.2011 at 14:29:47 -----------------
MOD:stFileName:STANTEXTVer2  STRING(260)
STANTEXTVer2           FILE,DRIVER('Btrieve'),NAME('STANTEXT.DAT'),PRE(STANTEXTVer2),CREATE
Description_Key          KEY(STANTEXTVer2:Description),NOCASE,PRIMARY
Record                   RECORD,PRE()
Description                 STRING(30)
TelephoneNumber             STRING(30)
FaxNumber                   STRING(30)
Text                        STRING(1000)
                         END
                       END

!------------ modified 04.09.2012 at 16:01:40 -----------------
MOD:stFileName:JOBSWARRVer3  STRING(260)
JOBSWARRVer3           FILE,DRIVER('Btrieve'),OEM,NAME('JOBSWARR.DAT'),PRE(JOBSWARRVer3),CREATE
RecordNumberKey          KEY(JOBSWARRVer3:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSWARRVer3:RefNumber),DUP,NOCASE
StatusManFirstKey        KEY(JOBSWARRVer3:Status,JOBSWARRVer3:Manufacturer,JOBSWARRVer3:FirstSecondYear,JOBSWARRVer3:RefNumber),DUP,NOCASE
StatusManKey             KEY(JOBSWARRVer3:Status,JOBSWARRVer3:Manufacturer,JOBSWARRVer3:RefNumber),DUP,NOCASE
ClaimStatusManKey        KEY(JOBSWARRVer3:Status,JOBSWARRVer3:Manufacturer,JOBSWARRVer3:ClaimSubmitted,JOBSWARRVer3:RefNumber),DUP,NOCASE
ClaimStatusManFirstKey   KEY(JOBSWARRVer3:Status,JOBSWARRVer3:Manufacturer,JOBSWARRVer3:FirstSecondYear,JOBSWARRVer3:ClaimSubmitted,JOBSWARRVer3:RefNumber),DUP,NOCASE
RRCStatusKey             KEY(JOBSWARRVer3:BranchID,JOBSWARRVer3:RepairedAt,JOBSWARRVer3:RRCStatus,JOBSWARRVer3:RefNumber),DUP,NOCASE
RRCStatusManKey          KEY(JOBSWARRVer3:BranchID,JOBSWARRVer3:RepairedAt,JOBSWARRVer3:RRCStatus,JOBSWARRVer3:Manufacturer,JOBSWARRVer3:RefNumber),DUP,NOCASE
RRCReconciledManKey      KEY(JOBSWARRVer3:BranchID,JOBSWARRVer3:RepairedAt,JOBSWARRVer3:RRCStatus,JOBSWARRVer3:Manufacturer,JOBSWARRVer3:RRCDateReconciled,JOBSWARRVer3:RefNumber),DUP,NOCASE
RRCReconciledKey         KEY(JOBSWARRVer3:BranchID,JOBSWARRVer3:RepairedAt,JOBSWARRVer3:RRCStatus,JOBSWARRVer3:RRCDateReconciled,JOBSWARRVer3:RefNumber),DUP,NOCASE
RepairedRRCStatusKey     KEY(JOBSWARRVer3:RepairedAt,JOBSWARRVer3:RRCStatus,JOBSWARRVer3:RefNumber),DUP,NOCASE
RepairedRRCStatusManKey  KEY(JOBSWARRVer3:RepairedAt,JOBSWARRVer3:RRCStatus,JOBSWARRVer3:Manufacturer,JOBSWARRVer3:RefNumber),DUP,NOCASE
RepairedRRCReconciledKey KEY(JOBSWARRVer3:RepairedAt,JOBSWARRVer3:RRCStatus,JOBSWARRVer3:DateReconciled,JOBSWARRVer3:RefNumber),DUP,NOCASE
RepairedRRCReconManKey   KEY(JOBSWARRVer3:RepairedAt,JOBSWARRVer3:RRCStatus,JOBSWARRVer3:Manufacturer,JOBSWARRVer3:RRCDateReconciled,JOBSWARRVer3:RefNumber),DUP,NOCASE
SubmittedRepairedBranchKey KEY(JOBSWARRVer3:BranchID,JOBSWARRVer3:RepairedAt,JOBSWARRVer3:ClaimSubmitted,JOBSWARRVer3:RefNumber),DUP,NOCASE
SubmittedBranchKey       KEY(JOBSWARRVer3:BranchID,JOBSWARRVer3:ClaimSubmitted,JOBSWARRVer3:RefNumber),DUP,NOCASE
ClaimSubmittedKey        KEY(JOBSWARRVer3:ClaimSubmitted,JOBSWARRVer3:RefNumber),DUP,NOCASE
RepairedRRCAccManKey     KEY(JOBSWARRVer3:BranchID,JOBSWARRVer3:RepairedAt,JOBSWARRVer3:RRCStatus,JOBSWARRVer3:Manufacturer,JOBSWARRVer3:DateAccepted,JOBSWARRVer3:RefNumber),DUP,NOCASE
AcceptedBranchKey        KEY(JOBSWARRVer3:BranchID,JOBSWARRVer3:DateAccepted,JOBSWARRVer3:RefNumber),DUP,NOCASE
DateAcceptedKey          KEY(JOBSWARRVer3:DateAccepted,JOBSWARRVer3:RefNumber),DUP,NOCASE
RejectedBranchKey        KEY(JOBSWARRVer3:BranchID,JOBSWARRVer3:DateRejected,JOBSWARRVer3:RefNumber),DUP,NOCASE
RejectedKey              KEY(JOBSWARRVer3:DateRejected,JOBSWARRVer3:RefNumber),DUP,NOCASE
FinalRejectionBranchKey  KEY(JOBSWARRVer3:BranchID,JOBSWARRVer3:DateFinalRejection,JOBSWARRVer3:RefNumber),DUP,NOCASE
FinalRejectionKey        KEY(JOBSWARRVer3:DateFinalRejection,JOBSWARRVer3:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
BranchID                    STRING(2)
RepairedAt                  STRING(3)
Manufacturer                STRING(30)
FirstSecondYear             BYTE
Status                      STRING(3)
Submitted                   LONG
FromApproved                BYTE
ClaimSubmitted              DATE
DateAccepted                DATE
DateReconciled              DATE
RRCDateReconciled           DATE
RRCStatus                   STRING(3)
DateRejected                DATE
DateFinalRejection          DATE
Orig_Sub_Date               DATE
                         END
                       END

!------------ modified 06.08.2010 at 10:06:12 -----------------
MOD:stFileName:CURRENCYVer3  STRING(260)
CURRENCYVer3           FILE,DRIVER('Btrieve'),OEM,NAME('CURRENCY.DAT'),PRE(CURRENCYVer3),CREATE
RecordNumberKey          KEY(CURRENCYVer3:RecordNumber),NOCASE,PRIMARY
CurrencyCodeKey          KEY(CURRENCYVer3:CurrencyCode),NOCASE
LastUpdateDateKey        KEY(CURRENCYVer3:LastUpdateDate,CURRENCYVer3:CurrencyCode),DUP,NOCASE
CorrelationCodeKey       KEY(CURRENCYVer3:CorrelationCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
CurrencyCode                STRING(30)
Description                 STRING(60)
DailyRate                   REAL
DivideMultiply              STRING(1)
LastUpdateDate              DATE
CorrelationCode             STRING(30)
                         END
                       END

!------------ modified 16.02.2012 at 12:20:39 -----------------
MOD:stFileName:JOBSE2Ver4  STRING(260)
JOBSE2Ver4             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE2.DAT'),PRE(JOBSE2Ver4),CREATE
RecordNumberKey          KEY(JOBSE2Ver4:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSE2Ver4:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IDNumber                    STRING(13)
InPendingDate               DATE
Contract                    BYTE
Prepaid                     BYTE
WarrantyRefNo               STRING(30)
SIDBookingName              STRING(60)
XAntenna                    BYTE
XLens                       BYTE
XFCover                     BYTE
XBCover                     BYTE
XKeypad                     BYTE
XBattery                    BYTE
XCharger                    BYTE
XLCD                        BYTE
XSimReader                  BYTE
XSystemConnector            BYTE
XNone                       BYTE
XNotes                      STRING(255)
ExchangeTerms               BYTE
WaybillNoFromPUP            LONG
WaybillNoToPUP              LONG
SMSNotification             BYTE
EmailNotification           BYTE
SMSAlertNumber              STRING(30)
EmailAlertAddress           STRING(255)
DateReceivedAtPUP           DATE
TimeReceivedAtPUP           TIME
DateDespatchFromPUP         DATE
TimeDespatchFromPUP         TIME
LoanIDNumber                STRING(13)
CourierWaybillNumber        STRING(30)
HubCustomer                 STRING(30)
HubCollection               STRING(30)
HubDelivery                 STRING(30)
WLabourPaid                 REAL
WPartsPaid                  REAL
WOtherCosts                 REAL
WSubTotal                   REAL
WVAT                        REAL
WTotal                      REAL
WarrantyReason              STRING(255)
WInvoiceRefNo               STRING(255)
SMSDate                     DATE
JobDiscountAmnt             REAL
InvDiscountAmnt             REAL
POPConfirmed                BYTE
ThirdPartyHandlingFee       REAL
InvThirdPartyHandlingFee    REAL
                         END
                       END

!------------ modified 12.02.2013 at 12:43:25 -----------------
MOD:stFileName:ORDPENDVer2  STRING(260)
ORDPENDVer2            FILE,DRIVER('Btrieve'),NAME('ORDPEND.DAT'),PRE(ORDPENDVer2),CREATE
Ref_Number_Key           KEY(ORDPENDVer2:Ref_Number),NOCASE,PRIMARY
Supplier_Key             KEY(ORDPENDVer2:Supplier,ORDPENDVer2:Part_Number),DUP,NOCASE
DescriptionKey           KEY(ORDPENDVer2:Supplier,ORDPENDVer2:Description),DUP,NOCASE
Supplier_Name_Key        KEY(ORDPENDVer2:Supplier),DUP,NOCASE
Part_Ref_Number_Key      KEY(ORDPENDVer2:Part_Ref_Number),DUP,NOCASE
Awaiting_Supplier_Key    KEY(ORDPENDVer2:Awaiting_Stock,ORDPENDVer2:Supplier,ORDPENDVer2:Part_Number),DUP,NOCASE
PartRecordNumberKey      KEY(ORDPENDVer2:PartRecordNumber),DUP,NOCASE
ReqPartNumber            KEY(ORDPENDVer2:StockReqNumber,ORDPENDVer2:Part_Number),DUP,NOCASE
ReqDescriptionKey        KEY(ORDPENDVer2:StockReqNumber,ORDPENDVer2:Description),DUP,NOCASE
KeyPrevStoReqNo          KEY(ORDPENDVer2:PrevStoReqNo),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Part_Ref_Number             LONG
Job_Number                  LONG
Part_Type                   STRING(3)
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    LONG
Account_Number              STRING(15)
Awaiting_Stock              STRING(3)
Reason                      STRING(255)
PartRecordNumber            LONG
StockReqNumber              LONG
PrevStoReqNo                LONG
                         END
                       END

!------------ modified 05.04.2012 at 08:34:09 -----------------
MOD:stFileName:REPTYDEFVer3  STRING(260)
REPTYDEFVer3           FILE,DRIVER('Btrieve'),NAME('REPTYDEF.DAT'),PRE(REPTYDEFVer3),CREATE
RecordNumberKey          KEY(REPTYDEFVer3:RecordNumber),NOCASE,PRIMARY
ManRepairTypeKey         KEY(REPTYDEFVer3:Manufacturer,REPTYDEFVer3:Repair_Type),NOCASE
Repair_Type_Key          KEY(REPTYDEFVer3:Repair_Type),DUP,NOCASE
Chargeable_Key           KEY(REPTYDEFVer3:Chargeable,REPTYDEFVer3:Repair_Type),DUP,NOCASE
Warranty_Key             KEY(REPTYDEFVer3:Warranty,REPTYDEFVer3:Repair_Type),DUP,NOCASE
ChaManRepairTypeKey      KEY(REPTYDEFVer3:Manufacturer,REPTYDEFVer3:Chargeable,REPTYDEFVer3:Repair_Type),DUP,NOCASE
WarManRepairTypeKey      KEY(REPTYDEFVer3:Manufacturer,REPTYDEFVer3:Warranty,REPTYDEFVer3:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Repair_Type                 STRING(30)
Chargeable                  STRING(3)
Warranty                    STRING(3)
WarrantyCode                STRING(5)
CompFaultCoding             BYTE
ExcludeFromEDI              BYTE
ExcludeFromInvoicing        BYTE
BER                         BYTE
ExcludeFromBouncer          BYTE
PromptForExchange           BYTE
JobWeighting                LONG
SkillLevel                  LONG
Manufacturer                STRING(30)
RepairLevel                 LONG
ForceAdjustment             BYTE
ScrapExchange               BYTE
ExcludeHandlingFee          BYTE
NotAvailable                BYTE
NoParts                     STRING(1)
SMSSendType                 STRING(1)
                         END
                       END

!------------ modified 11.12.2014 at 15:04:16 -----------------
MOD:stFileName:MANFAULTVer2  STRING(260)
MANFAULTVer2           FILE,DRIVER('Btrieve'),NAME('MANFAULT.DAT'),PRE(MANFAULTVer2),CREATE
Field_Number_Key         KEY(MANFAULTVer2:Manufacturer,MANFAULTVer2:Field_Number),NOCASE,PRIMARY
MainFaultKey             KEY(MANFAULTVer2:Manufacturer,MANFAULTVer2:MainFault),DUP,NOCASE
InFaultKey               KEY(MANFAULTVer2:Manufacturer,MANFAULTVer2:InFault),DUP,NOCASE
ScreenOrderKey           KEY(MANFAULTVer2:Manufacturer,MANFAULTVer2:ScreenOrder),DUP,NOCASE
BouncerFaultKey          KEY(MANFAULTVer2:Manufacturer,MANFAULTVer2:BouncerFault),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
MainFault                   BYTE
PromptForExchange           BYTE
InFault                     BYTE
CompulsoryIfExchange        BYTE
NotAvailable                BYTE
CharCompulsory              BYTE
CharCompulsoryBooking       BYTE
ScreenOrder                 LONG
RestrictAvailability        BYTE
RestrictServiceCentre       BYTE
GenericFault                BYTE
NotCompulsoryThirdParty     BYTE
HideThirdParty              BYTE
HideRelatedCodeIfBlank      BYTE
BlankRelatedCode            LONG
FillFromDOP                 BYTE
CompulsoryForRepairType     BYTE
CompulsoryRepairType        STRING(30)
BouncerFault                BYTE
                         END
                       END

!------------ modified 24.10.2012 at 14:24:43 -----------------
MOD:stFileName:LOCVALUEVer2  STRING(260)
LOCVALUEVer2           FILE,DRIVER('Btrieve'),OEM,NAME('LOCVALUE.DAT'),PRE(LOCVALUEVer2),CREATE
RecordNumberKey          KEY(LOCVALUEVer2:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(LOCVALUEVer2:Location,LOCVALUEVer2:TheDate),DUP,NOCASE
DateOnly                 KEY(LOCVALUEVer2:TheDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
TheDate                     DATE
PurchaseTotal               REAL
SaleCostTotal               REAL
RetailCostTotal             REAL
QuantityTotal               LONG
                         END
                       END

!------------ modified 17.04.2015 at 09:15:03 -----------------
MOD:stFileName:COURIERVer2  STRING(260)
COURIERVer2            FILE,DRIVER('Btrieve'),NAME('COURIER.DAT'),PRE(COURIERVer2),CREATE
Courier_Key              KEY(COURIERVer2:Courier),NOCASE,PRIMARY
Courier_Type_Key         KEY(COURIERVer2:Courier_Type,COURIERVer2:Courier),DUP,NOCASE
Record                   RECORD,PRE()
Courier                     STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name                STRING(60)
Export_Path                 STRING(255)
Include_Estimate            STRING(3)
Include_Chargeable          STRING(3)
Include_Warranty            STRING(3)
Service                     STRING(15)
Import_Path                 STRING(255)
Courier_Type                STRING(30)
Last_Despatch_Time          TIME
ICOLSuffix                  STRING(3)
ContractNumber              STRING(20)
ANCPath                     STRING(255)
ANCCount                    LONG
DespatchClose               STRING(3)
LabGOptions                 STRING(60)
CustomerCollection          BYTE
NewStatus                   STRING(30)
NoOfDespatchNotes           LONG
AutoConsignmentNo           BYTE
LastConsignmentNo           LONG
PrintLabel                  BYTE
PrintWaybill                BYTE
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             BYTE
IncludeSunday               BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
EmailAddress                STRING(255)
FromEmailAddress            STRING(255)
InterfaceUse                BYTE
InterfaceName               STRING(50)
InterfacePassword           STRING(50)
InterfaceURL                STRING(200)
                         END
                       END

!------------ modified 13.02.2014 at 09:37:02 -----------------
MOD:stFileName:TRDPARTYVer5  STRING(260)
TRDPARTYVer5           FILE,DRIVER('Btrieve'),NAME('TRDPARTY.DAT'),PRE(TRDPARTYVer5),CREATE
Company_Name_Key         KEY(TRDPARTYVer5:Company_Name),NOCASE,PRIMARY
Account_Number_Key       KEY(TRDPARTYVer5:Account_Number),NOCASE
Special_Instructions_Key KEY(TRDPARTYVer5:Special_Instructions),DUP,NOCASE
Courier_Only_Key         KEY(TRDPARTYVer5:Courier),DUP,NOCASE
DeactivateCompanyKey     KEY(TRDPARTYVer5:Deactivate,TRDPARTYVer5:Company_Name),DUP,NOCASE
ASCIDKey                 KEY(TRDPARTYVer5:ASCID),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Account_Number              STRING(30)
Contact_Name                STRING(60)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Turnaround_Time             REAL
Courier_Direct              STRING(3)
Courier                     STRING(30)
Print_Order                 STRING(15)
Special_Instructions        STRING(30)
Batch_Number                LONG
BatchLimit                  LONG
VATRate                     REAL
Markup                      REAL
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
Deactivate                  BYTE
LHubAccount                 BYTE
ASCID                       STRING(30)
PreCompletionClaiming       BYTE
EVO_AccNumber               STRING(20)
EVO_VendorNumber            STRING(20)
EVO_Profit_Centre           STRING(30)
EVO_Excluded                BYTE
                         END
                       END

!------------ modified 24.12.2014 at 11:13:33 -----------------
MOD:stFileName:TRANTYPEVer3  STRING(260)
TRANTYPEVer3           FILE,DRIVER('Btrieve'),NAME('TRANTYPE.DAT'),PRE(TRANTYPEVer3),CREATE
Transit_Type_Key         KEY(TRANTYPEVer3:Transit_Type),NOCASE,PRIMARY
RRCKey                   KEY(TRANTYPEVer3:RRC,TRANTYPEVer3:Transit_Type),DUP,NOCASE
ARCKey                   KEY(TRANTYPEVer3:ARC,TRANTYPEVer3:Transit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Transit_Type                STRING(30)
Courier_Collection_Report   STRING(3)
Collection_Address          STRING(3)
Delivery_Address            STRING(3)
Loan_Unit                   STRING(3)
Exchange_Unit               STRING(3)
Force_DOP                   STRING(3)
Force_Location              STRING(3)
Skip_Workshop               STRING(3)
HideLocation                BYTE
InternalLocation            STRING(30)
Workshop_Label              STRING(3)
Job_Card                    STRING(3)
JobReceipt                  STRING(3)
Initial_Status              STRING(30)
Initial_Priority            STRING(30)
ExchangeStatus              STRING(30)
LoanStatus                  STRING(30)
Location                    STRING(3)
ANCCollNote                 STRING(3)
InWorkshop                  STRING(3)
HubRepair                   BYTE
ARC                         BYTE
RRC                         BYTE
OBF                         BYTE
WaybillComBook              BYTE
WaybillComComp              BYTE
SeamsTransactionCode        STRING(20)
                         END
                       END

!------------ modified 28.06.2010 at 12:01:47 -----------------
MOD:stFileName:ESNMODELVer2  STRING(260)
ESNMODELVer2           FILE,DRIVER('Btrieve'),NAME('ESNMODEL.DAT'),PRE(ESNMODELVer2),CREATE
Record_Number_Key        KEY(ESNMODELVer2:Record_Number),NOCASE,PRIMARY
ESN_Key                  KEY(ESNMODELVer2:ESN,ESNMODELVer2:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(ESNMODELVer2:ESN),DUP,NOCASE
Model_Number_Key         KEY(ESNMODELVer2:Model_Number,ESNMODELVer2:ESN),DUP,NOCASE
Manufacturer_Key         KEY(ESNMODELVer2:Manufacturer,ESNMODELVer2:ESN),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
ESN                         STRING(8)
Model_Number                STRING(30)
Manufacturer                STRING(30)
Include48Hour               BYTE
Active                      STRING(1)
                         END
                       END

!------------ modified 19.01.2012 at 10:00:38 -----------------
MOD:stFileName:STOCKVer7  STRING(260)
STOCKVer7              FILE,DRIVER('Btrieve'),NAME('STOCK.DAT'),PRE(STOCKVer7),CREATE
Ref_Number_Key           KEY(STOCKVer7:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(STOCKVer7:Sundry_Item),DUP,NOCASE
Description_Key          KEY(STOCKVer7:Location,STOCKVer7:Description),DUP,NOCASE
Description_Accessory_Key KEY(STOCKVer7:Location,STOCKVer7:Suspend,STOCKVer7:Accessory,STOCKVer7:Description),DUP,NOCASE
Shelf_Location_Key       KEY(STOCKVer7:Location,STOCKVer7:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(STOCKVer7:Location,STOCKVer7:Suspend,STOCKVer7:Accessory,STOCKVer7:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(STOCKVer7:Location,STOCKVer7:Suspend,STOCKVer7:Accessory,STOCKVer7:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(STOCKVer7:Manufacturer,STOCKVer7:Part_Number),DUP,NOCASE
Supplier_Key             KEY(STOCKVer7:Location,STOCKVer7:Suspend,STOCKVer7:Supplier),DUP,NOCASE
Location_Key             KEY(STOCKVer7:Location,STOCKVer7:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(STOCKVer7:Location,STOCKVer7:Suspend,STOCKVer7:Accessory,STOCKVer7:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(STOCKVer7:Location,STOCKVer7:Ref_Number,STOCKVer7:Part_Number,STOCKVer7:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(STOCKVer7:Location,STOCKVer7:Manufacturer,STOCKVer7:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(STOCKVer7:Location,STOCKVer7:Suspend,STOCKVer7:Accessory,STOCKVer7:Manufacturer,STOCKVer7:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(STOCKVer7:Location,STOCKVer7:Part_Number,STOCKVer7:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(STOCKVer7:Ref_Number,STOCKVer7:Part_Number,STOCKVer7:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(STOCKVer7:Minimum_Stock,STOCKVer7:Location,STOCKVer7:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(STOCKVer7:Minimum_Stock,STOCKVer7:Location,STOCKVer7:Description),DUP,NOCASE
SecondLocKey             KEY(STOCKVer7:Location,STOCKVer7:Shelf_Location,STOCKVer7:Second_Location,STOCKVer7:Part_Number),DUP,NOCASE
RequestedKey             KEY(STOCKVer7:QuantityRequested,STOCKVer7:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(STOCKVer7:ExchangeUnit,STOCKVer7:Location,STOCKVer7:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(STOCKVer7:ExchangeUnit,STOCKVer7:Location,STOCKVer7:Description),DUP,NOCASE
DateBookedKey            KEY(STOCKVer7:DateBooked),DUP,NOCASE
Supplier_Only_Key        KEY(STOCKVer7:Supplier),DUP,NOCASE
LocPartSuspendKey        KEY(STOCKVer7:Location,STOCKVer7:Suspend,STOCKVer7:Part_Number),DUP,NOCASE
LocDescSuspendKey        KEY(STOCKVer7:Location,STOCKVer7:Suspend,STOCKVer7:Description),DUP,NOCASE
LocShelfSuspendKey       KEY(STOCKVer7:Location,STOCKVer7:Suspend,STOCKVer7:Shelf_Location),DUP,NOCASE
LocManSuspendKey         KEY(STOCKVer7:Location,STOCKVer7:Suspend,STOCKVer7:Manufacturer,STOCKVer7:Part_Number),DUP,NOCASE
ExchangeModelKey         KEY(STOCKVer7:Location,STOCKVer7:Manufacturer,STOCKVer7:ExchangeModelNumber),DUP,NOCASE
LoanModelKey             KEY(STOCKVer7:Location,STOCKVer7:Manufacturer,STOCKVer7:LoanModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
ReturnFaultySpare           BYTE
ChargeablePartOnly          BYTE
AttachBySolder              BYTE
AllowDuplicate              BYTE
RetailMarkup                REAL
VirtPurchaseCost            REAL
VirtTradeCost               REAL
VirtRetailCost              REAL
VirtTradeMarkup             REAL
VirtRetailMarkup            REAL
AveragePurchaseCost         REAL
PurchaseMarkUp              REAL
RepairLevel                 LONG
SkillLevel                  LONG
DateBooked                  DATE
AccWarrantyPeriod           LONG
ExcludeLevel12Repair        BYTE
ExchangeOrderCap            LONG
ExchangeModelNumber         STRING(30)
LoanUnit                    BYTE
LoanModelNumber             STRING(30)
                         END
                       END

!------------ modified 18.01.2012 at 16:20:35 -----------------
MOD:stFileName:LOANVer2  STRING(260)
LOANVer2               FILE,DRIVER('Btrieve'),NAME('LOAN.DAT'),PRE(LOANVer2),CREATE
Ref_Number_Key           KEY(LOANVer2:Ref_Number),NOCASE,PRIMARY
ESN_Only_Key             KEY(LOANVer2:ESN),DUP,NOCASE
MSN_Only_Key             KEY(LOANVer2:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(LOANVer2:Stock_Type,LOANVer2:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(LOANVer2:Stock_Type,LOANVer2:Model_Number,LOANVer2:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(LOANVer2:Stock_Type,LOANVer2:ESN),DUP,NOCASE
MSN_Key                  KEY(LOANVer2:Stock_Type,LOANVer2:MSN),DUP,NOCASE
ESN_Available_Key        KEY(LOANVer2:Available,LOANVer2:Stock_Type,LOANVer2:ESN),DUP,NOCASE
MSN_Available_Key        KEY(LOANVer2:Available,LOANVer2:Stock_Type,LOANVer2:MSN),DUP,NOCASE
Ref_Available_Key        KEY(LOANVer2:Available,LOANVer2:Stock_Type,LOANVer2:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(LOANVer2:Available,LOANVer2:Stock_Type,LOANVer2:Model_Number,LOANVer2:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(LOANVer2:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(LOANVer2:Model_Number,LOANVer2:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(LOANVer2:Available,LOANVer2:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(LOANVer2:Available,LOANVer2:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(LOANVer2:Available,LOANVer2:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(LOANVer2:Available,LOANVer2:Model_Number,LOANVer2:Ref_Number),DUP,NOCASE
DateBookedKey            KEY(LOANVer2:Date_Booked),DUP,NOCASE
LocStockAvailIMEIKey     KEY(LOANVer2:Location,LOANVer2:Stock_Type,LOANVer2:Available,LOANVer2:ESN),DUP,NOCASE
LocStockIMEIKey          KEY(LOANVer2:Location,LOANVer2:Stock_Type,LOANVer2:ESN),DUP,NOCASE
LocIMEIKey               KEY(LOANVer2:Location,LOANVer2:ESN),DUP,NOCASE
LocStockAvailRefKey      KEY(LOANVer2:Location,LOANVer2:Stock_Type,LOANVer2:Available,LOANVer2:Ref_Number),DUP,NOCASE
LocStockRefKey           KEY(LOANVer2:Location,LOANVer2:Stock_Type,LOANVer2:Ref_Number),DUP,NOCASE
LocRefKey                KEY(LOANVer2:Location,LOANVer2:Ref_Number),DUP,NOCASE
LocStockAvailModelKey    KEY(LOANVer2:Location,LOANVer2:Stock_Type,LOANVer2:Available,LOANVer2:Model_Number,LOANVer2:Ref_Number),DUP,NOCASE
LocStockModelKey         KEY(LOANVer2:Location,LOANVer2:Stock_Type,LOANVer2:Model_Number,LOANVer2:Ref_Number),DUP,NOCASE
LocModelKey              KEY(LOANVer2:Location,LOANVer2:Model_Number,LOANVer2:Ref_Number),DUP,NOCASE
LocStockAvailMSNKey      KEY(LOANVer2:Location,LOANVer2:Stock_Type,LOANVer2:Available,LOANVer2:MSN),DUP,NOCASE
LocStockMSNKey           KEY(LOANVer2:Location,LOANVer2:Stock_Type,LOANVer2:MSN),DUP,NOCASE
LocMSNKey                KEY(LOANVer2:Location,LOANVer2:MSN),DUP,NOCASE
AvailLocIMEI             KEY(LOANVer2:Available,LOANVer2:Location,LOANVer2:ESN),DUP,NOCASE
AvailLocMSN              KEY(LOANVer2:Available,LOANVer2:Location,LOANVer2:MSN),DUP,NOCASE
AvailLocRef              KEY(LOANVer2:Available,LOANVer2:Location,LOANVer2:Ref_Number),DUP,NOCASE
AvailLocModel            KEY(LOANVer2:Available,LOANVer2:Location,LOANVer2:Model_Number),DUP,NOCASE
InTransitLocationKey     KEY(LOANVer2:InTransit,LOANVer2:Location,LOANVer2:ESN),DUP,NOCASE
InTransitKey             KEY(LOANVer2:InTransit,LOANVer2:ESN),DUP,NOCASE
StatusChangeDateKey      KEY(LOANVer2:StatusChangeDate),DUP,NOCASE
LocStatusChangeDateKey   KEY(LOANVer2:Location,LOANVer2:StatusChangeDate),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
DummyField                  STRING(1)
StatusChangeDate            DATE
InTransit                   BYTE
                         END
                       END

!------------ modified 22.10.2012 at 16:11:36 -----------------
MOD:stFileName:SMSMAILVer11  STRING(260)
SMSMAILVer11           FILE,DRIVER('Btrieve'),OEM,NAME('SMSMAIL.DAT'),PRE(SMSMAILVer11),CREATE
RecordNumberKey          KEY(SMSMAILVer11:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(SMSMAILVer11:SendToSMS,SMSMAILVer11:SMSSent),DUP,NOCASE
EmailSentKey             KEY(SMSMAILVer11:SendToEmail,SMSMAILVer11:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(SMSMAILVer11:SMSSent,SMSMAILVer11:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(SMSMAILVer11:EmailSent,SMSMAILVer11:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(SMSMAILVer11:RefNumber,SMSMAILVer11:DateInserted,SMSMAILVer11:TimeInserted),DUP,NOCASE
KeyMSISDN_DateDec        KEY(SMSMAILVer11:MSISDN,-SMSMAILVer11:DateSMSSent),DUP,NOCASE
KeyRefDateTimeDec        KEY(SMSMAILVer11:RefNumber,-SMSMAILVer11:DateInserted,-SMSMAILVer11:TimeInserted),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
SMSType                     STRING(1)
EmailSubject                STRING(255)
EmailAddress2               STRING(255)
EmailAddress3               STRING(255)
                         END
                       END

!------------ modified 18.01.2012 at 16:20:36 -----------------
MOD:stFileName:LOANHISTVer2  STRING(260)
LOANHISTVer2           FILE,DRIVER('Btrieve'),NAME('LOANHIST.DAT'),PRE(LOANHISTVer2),CREATE
Record_Number_Key        KEY(LOANHISTVer2:record_number),NOCASE,PRIMARY
Ref_Number_Key           KEY(LOANHISTVer2:Ref_Number,-LOANHISTVer2:Date,-LOANHISTVer2:Time),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
Notes                       STRING(255)
                         END
                       END

!------------ modified 29.06.2011 at 20:48:20 -----------------
MOD:stFileName:EXCHHISTVer5  STRING(260)
EXCHHISTVer5           FILE,DRIVER('Btrieve'),NAME('EXCHHIST.DAT'),PRE(EXCHHISTVer5),CREATE
Record_Number_Key        KEY(EXCHHISTVer5:record_number),NOCASE,PRIMARY
Ref_Number_Key           KEY(EXCHHISTVer5:Ref_Number,-EXCHHISTVer5:Date,-EXCHHISTVer5:Time),DUP,NOCASE
DateStatusKey            KEY(EXCHHISTVer5:Date,EXCHHISTVer5:Status,EXCHHISTVer5:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
record_number               LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
Notes                       STRING(255)
                         END
                       END

!------------ modified 09.08.2013 at 16:01:02 -----------------
MOD:stFileName:AUDITVer2  STRING(260)
AUDITVer2              FILE,DRIVER('Btrieve'),NAME('AUDIT.DAT'),PRE(AUDITVer2),CREATE
Ref_Number_Key           KEY(AUDITVer2:Ref_Number,-AUDITVer2:Date,-AUDITVer2:Time,AUDITVer2:Action),DUP,NOCASE
Action_Key               KEY(AUDITVer2:Ref_Number,AUDITVer2:Action,-AUDITVer2:Date),DUP,NOCASE
User_Key                 KEY(AUDITVer2:Ref_Number,AUDITVer2:User,-AUDITVer2:Date),DUP,NOCASE
Record_Number_Key        KEY(AUDITVer2:record_number),NOCASE,PRIMARY
ActionOnlyKey            KEY(AUDITVer2:Action,AUDITVer2:Date),DUP,NOCASE
TypeRefKey               KEY(AUDITVer2:Ref_Number,AUDITVer2:Type,-AUDITVer2:Date,-AUDITVer2:Time,AUDITVer2:Action),DUP,NOCASE
TypeActionKey            KEY(AUDITVer2:Ref_Number,AUDITVer2:Type,AUDITVer2:Action,-AUDITVer2:Date),DUP,NOCASE
TypeUserKey              KEY(AUDITVer2:Ref_Number,AUDITVer2:Type,AUDITVer2:User,-AUDITVer2:Date),DUP,NOCASE
DateActionJobKey         KEY(AUDITVer2:Date,AUDITVer2:Action,AUDITVer2:Ref_Number),DUP,NOCASE
DateJobKey               KEY(AUDITVer2:Date,AUDITVer2:Ref_Number),DUP,NOCASE
DateTypeJobKey           KEY(AUDITVer2:Date,AUDITVer2:Type,AUDITVer2:Ref_Number),DUP,NOCASE
DateTypeActionKey        KEY(AUDITVer2:Date,AUDITVer2:Type,AUDITVer2:Action,AUDITVer2:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Type                        STRING(3)
                         END
                       END

!------------ modified 04.02.2015 at 14:50:16 -----------------
MOD:stFileName:USERSVer4  STRING(260)
USERSVer4              FILE,DRIVER('Btrieve'),NAME('USERS.DAT'),PRE(USERSVer4),CREATE
User_Code_Key            KEY(USERSVer4:User_Code),NOCASE,PRIMARY
User_Type_Active_Key     KEY(USERSVer4:User_Type,USERSVer4:Active,USERSVer4:Surname),DUP,NOCASE
Surname_Active_Key       KEY(USERSVer4:Active,USERSVer4:Surname),DUP,NOCASE
User_Code_Active_Key     KEY(USERSVer4:Active,USERSVer4:User_Code),DUP,NOCASE
User_Type_Key            KEY(USERSVer4:User_Type,USERSVer4:Surname),DUP,NOCASE
surname_key              KEY(USERSVer4:Surname),DUP,NOCASE
password_key             KEY(USERSVer4:Password),NOCASE
Logged_In_Key            KEY(USERSVer4:Logged_In,USERSVer4:Surname),DUP,NOCASE
Team_Surname             KEY(USERSVer4:Team,USERSVer4:Surname),DUP,NOCASE
Active_Team_Surname      KEY(USERSVer4:Team,USERSVer4:Active,USERSVer4:Surname),DUP,NOCASE
LocationSurnameKey       KEY(USERSVer4:Location,USERSVer4:Surname),DUP,NOCASE
LocationForenameKey      KEY(USERSVer4:Location,USERSVer4:Forename),DUP,NOCASE
LocActiveSurnameKey      KEY(USERSVer4:Active,USERSVer4:Location,USERSVer4:Surname),DUP,NOCASE
LocActiveForenameKey     KEY(USERSVer4:Active,USERSVer4:Location,USERSVer4:Forename),DUP,NOCASE
TeamStatusKey            KEY(USERSVer4:IncludeInEngStatus,USERSVer4:Team,USERSVer4:Surname),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Forename                    STRING(30)
Surname                     STRING(30)
Password                    STRING(20)
User_Type                   STRING(15)
Job_Assignment              STRING(15)
Supervisor                  STRING(3)
User_Level                  STRING(30)
Logged_In                   STRING(1)
Logged_in_date              DATE
Logged_In_Time              TIME
Restrict_Logins             STRING(3)
Concurrent_Logins           REAL
Active                      STRING(3)
Team                        STRING(30)
Location                    STRING(30)
Repair_Target               REAL
SkillLevel                  LONG
StockFromLocationOnly       BYTE
EmailAddress                STRING(255)
RenewPassword               LONG
PasswordLastChanged         DATE
RestrictParts               BYTE
RestrictChargeable          BYTE
RestrictWarranty            BYTE
IncludeInEngStatus          BYTE
MobileNumber                STRING(20)
RRC_MII_Dealer_ID           STRING(30)
AppleAccredited             BYTE
AppleID                     STRING(20)
                         END
                       END

!------------ modified 26.11.2014 at 16:33:25 -----------------
MOD:stFileName:LOCSHELFVer2  STRING(260)
LOCSHELFVer2           FILE,DRIVER('Btrieve'),NAME('LOCSHELF.DAT'),PRE(LOCSHELFVer2),CREATE
RecordNumberKey          KEY(LOCSHELFVer2:RecordNumber),NOCASE,PRIMARY
Shelf_Location_Key       KEY(LOCSHELFVer2:Site_Location,LOCSHELFVer2:Shelf_Location),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Site_Location               STRING(30)
Shelf_Location              STRING(30)
                         END
                       END

!------------ modified 30.01.2012 at 15:58:34 -----------------
MOD:stFileName:RTNORDERVer13  STRING(260)
RTNORDERVer13          FILE,DRIVER('Btrieve'),OEM,NAME('RTNORDER.DAT'),PRE(RTNORDERVer13),CREATE
RecordNumberKey          KEY(RTNORDERVer13:RecordNumber),NOCASE,PRIMARY
LocationPartNumberKey    KEY(RTNORDERVer13:Location,RTNORDERVer13:ExchangeOrder,RTNORDERVer13:PartNumber),DUP,NOCASE
LocationStatusPartKey    KEY(RTNORDERVer13:Location,RTNORDERVer13:ExchangeOrder,RTNORDERVer13:Status,RTNORDERVer13:PartNumber),DUP,NOCASE
OrderStatusPartNumberKey KEY(RTNORDERVer13:OrderNumber,RTNORDERVer13:ExchangeOrder,RTNORDERVer13:Status,RTNORDERVer13:PartNumber),DUP,NOCASE
OrderPartNumberKey       KEY(RTNORDERVer13:OrderNumber,RTNORDERVer13:ExchangeOrder,RTNORDERVer13:PartNumber),DUP,NOCASE
CreditNoteNumberKey      KEY(RTNORDERVer13:CreditNoteRequestNumber),DUP,NOCASE
WaybillNumberKey         KEY(RTNORDERVer13:WaybillNumber),DUP,NOCASE
RefNumberKey             KEY(RTNORDERVer13:ExchangeOrder,RTNORDERVer13:RefNumber),DUP,NOCASE
OrderStatusReturnRefNoKey KEY(RTNORDERVer13:OrderNumber,RTNORDERVer13:ExchangeOrder,RTNORDERVer13:ReturnType,RTNORDERVer13:Status,RTNORDERVer13:RefNumber),DUP,NOCASE
LocArcOrdWaybillKey      KEY(RTNORDERVer13:Archived,RTNORDERVer13:Ordered,RTNORDERVer13:Location,RTNORDERVer13:WaybillNumber),DUP,NOCASE
ArcOrdWaybillKey         KEY(RTNORDERVer13:Archived,RTNORDERVer13:Ordered,RTNORDERVer13:WaybillNumber),DUP,NOCASE
ArcLocDateKey            KEY(RTNORDERVer13:Archived,RTNORDERVer13:Location,RTNORDERVer13:DateCreated),DUP,NOCASE
ArcDateKey               KEY(RTNORDERVer13:Archived,RTNORDERVer13:DateCreated),DUP,NOCASE
DateOrderedKey           KEY(RTNORDERVer13:Location,RTNORDERVer13:DateOrdered,RTNORDERVer13:TimeOrdered),DUP,NOCASE
DateOrderedReceivedKey   KEY(RTNORDERVer13:Location,RTNORDERVer13:Received,RTNORDERVer13:DateOrdered,RTNORDERVer13:TimeOrdered),DUP,NOCASE
ArcOrdExcWaybillKey      KEY(RTNORDERVer13:Archived,RTNORDERVer13:Ordered,RTNORDERVer13:ExchangeOrder,RTNORDERVer13:WaybillNumber),DUP,NOCASE
LocArcOrdExcWaybillKey   KEY(RTNORDERVer13:Archived,RTNORDERVer13:Ordered,RTNORDERVer13:Location,RTNORDERVer13:ExchangeOrder,RTNORDERVer13:WaybillNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Archived                    BYTE
DateCreated                 DATE
TimeCreated                 TIME
Location                    STRING(30)
UserCode                    STRING(3)
RefNumber                   LONG
OrderNumber                 LONG
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
QuantityReturned            LONG
ExchangeOrder               BYTE
Status                      STRING(3)
Notes                       STRING(255)
ReturnType                  STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
ExchangePrice               REAL
CreditNoteRequestNumber     LONG
WaybillNumber               LONG
Ordered                     BYTE
DateOrdered                 DATE
TimeOrdered                 TIME
WhoOrdered                  STRING(3)
Received                    BYTE
DateReceived                DATE
TimeReceived                TIME
WhoReceived                 STRING(3)
WhoProcessed                STRING(3)
                         END
                       END

!------------ modified 28.10.2014 at 10:07:58 -----------------
MOD:stFileName:C3DMONITVer2  STRING(260)
C3DMONITVer2           FILE,DRIVER('Btrieve'),OEM,NAME('C3DMONIT.DAT'),PRE(C3DMONITVer2),CREATE
KeyRecordNumber          KEY(C3DMONITVer2:RecordNumber),NOCASE,PRIMARY
KeyRefNumber             KEY(C3DMONITVer2:RefNumber),DUP,NOCASE
KeyDateTime              KEY(C3DMONITVer2:CallDate,C3DMONITVer2:CallTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(20)
CallDate                    DATE
CallTime                    TIME
ErrorCode                   STRING(20)
                         END
                       END

!------------ modified 19.12.2014 at 10:01:25 -----------------
MOD:stFileName:SMSTextVer8  STRING(260)
SMSTextVer8            FILE,DRIVER('Btrieve'),OEM,NAME('SMSText.Dat'),PRE(SMSTextVer8),CREATE
Key_Record_No            KEY(SMSTextVer8:Record_No),NOCASE,PRIMARY
Key_StatusType_Location_Trigger KEY(SMSTextVer8:Status_Type,SMSTextVer8:Location,SMSTextVer8:Trigger_Status),DUP,NOCASE
Key_Description          KEY(SMSTextVer8:Description),DUP,NOCASE
Key_StatusType_Description KEY(SMSTextVer8:Status_Type,SMSTextVer8:Description),DUP,NOCASE
Key_TriggerStatus        KEY(SMSTextVer8:Trigger_Status),DUP,NOCASE
Key_Location_CSI         KEY(SMSTextVer8:Location,SMSTextVer8:CSI),DUP,NOCASE
Key_Location_Duplicate   KEY(SMSTextVer8:Location,SMSTextVer8:Duplicate_Estimate),DUP,NOCASE
Key_Location_BER         KEY(SMSTextVer8:Location,SMSTextVer8:BER),DUP,NOCASE
Key_Location_Liquid      KEY(SMSTextVer8:Location,SMSTextVer8:LiquidDamage),DUP,NOCASE
Record                   RECORD,PRE()
Record_No                   LONG
Description                 STRING(50)
Location                    STRING(1)
Status_Type                 STRING(1)
Trigger_Status              STRING(30)
Auto_SMS                    STRING(3)
CSI                         STRING(3)
Duplicate_Estimate          STRING(3)
BER                         STRING(3)
LiquidDamage                STRING(3)
SMSText                     STRING(500)
Resend_Estimate_Days        LONG
Final_Estimate_Text         STRING(500)
SEAMS_Notification          STRING(1)
                         END
                       END

!------------ modified 18.01.2012 at 16:20:37 -----------------
MOD:stFileName:RETSTOCKVer5  STRING(260)
RETSTOCKVer5           FILE,DRIVER('Btrieve'),OEM,NAME('RETSTOCK.DAT'),PRE(RETSTOCKVer5),CREATE
Record_Number_Key        KEY(RETSTOCKVer5:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(RETSTOCKVer5:Ref_Number,RETSTOCKVer5:Part_Number),DUP,NOCASE
Despatched_Key           KEY(RETSTOCKVer5:Ref_Number,RETSTOCKVer5:Despatched,RETSTOCKVer5:Despatch_Note_Number,RETSTOCKVer5:Part_Number),DUP,NOCASE
Despatched_Only_Key      KEY(RETSTOCKVer5:Ref_Number,RETSTOCKVer5:Despatched),DUP,NOCASE
Pending_Ref_Number_Key   KEY(RETSTOCKVer5:Ref_Number,RETSTOCKVer5:Pending_Ref_Number),DUP,NOCASE
Order_Part_Key           KEY(RETSTOCKVer5:Ref_Number,RETSTOCKVer5:Order_Part_Number),DUP,NOCASE
Order_Number_Key         KEY(RETSTOCKVer5:Ref_Number,RETSTOCKVer5:Order_Number),DUP,NOCASE
DespatchedKey            KEY(RETSTOCKVer5:Despatched),DUP,NOCASE
DespatchedPartKey        KEY(RETSTOCKVer5:Ref_Number,RETSTOCKVer5:Despatched,RETSTOCKVer5:Part_Number),DUP,NOCASE
Amended_Receipt_Key      KEY(RETSTOCKVer5:Amend_Site_Loc,RETSTOCKVer5:Amended,RETSTOCKVer5:Ref_Number),DUP,NOCASE
Delimit_Stock_Key        KEY(RETSTOCKVer5:Ref_Number,RETSTOCKVer5:Part_Ref_Number),DUP,NOCASE
PartRefNumberKey         KEY(RETSTOCKVer5:Part_Ref_Number),DUP,NOCASE
PartNumberKey            KEY(RETSTOCKVer5:Part_Number),DUP,NOCASE
DespatchPartKey          KEY(RETSTOCKVer5:Despatched,RETSTOCKVer5:Part_Number),DUP,NOCASE
ExchangeRefNumberKey     KEY(RETSTOCKVer5:Ref_Number,RETSTOCKVer5:ExchangeRefNumber),DUP,NOCASE
LoanRefNumberKey         KEY(RETSTOCKVer5:Ref_Number,RETSTOCKVer5:LoanRefNumber),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Item_Cost                   REAL
Quantity                    REAL
Despatched                  STRING(20)
Order_Number                REAL
Date_Ordered                DATE
Date_Received               DATE
Despatch_Note_Number        REAL
Despatch_Date               DATE
Part_Ref_Number             REAL
Pending_Ref_Number          REAL
Order_Part_Number           REAL
Purchase_Order_Number       STRING(30)
Previous_Sale_Number        LONG
InvoicePart                 LONG
Credit                      STRING(3)
QuantityReceived            LONG
Received                    BYTE
Amended                     BYTE
Amend_Reason                CSTRING(255)
Amend_Site_Loc              STRING(30)
GRNNumber                   REAL
DateReceived                DATE
ScannedQty                  LONG
ExchangeRefNumber           LONG
LoanRefNumber               LONG
                         END
                       END

!------------ modified 15.05.2012 at 16:42:10 -----------------
MOD:stFileName:STOCKALXVer3  STRING(260)
STOCKALXVer3           FILE,DRIVER('Btrieve'),OEM,PRE(STOCKALXVer3),NAME('STOCKALX'),CREATE
KeyRecordNo              KEY(STOCKALXVer3:RecordNo),NOCASE,PRIMARY
KeySTLRecordNumber       KEY(STOCKALXVer3:StlRecordNumber),DUP,NOCASE
KeyRequestDateTime       KEY(STOCKALXVer3:RequestDate,STOCKALXVer3:RequestTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
StlRecordNumber             LONG
JobNo                       LONG
PartNo                      STRING(30)
Description                 STRING(30)
RequestDate                 DATE
RequestTime                 TIME
AllocateDate                DATE
AllocateTime                TIME
AllocateUser                STRING(3)
                         END
                       END

!------------ modified 17.02.2012 at 14:37:58 -----------------
MOD:stFileName:ORDPARTSVer4  STRING(260)
ORDPARTSVer4           FILE,DRIVER('Btrieve'),NAME('ORDPARTS.DAT'),PRE(ORDPARTSVer4),CREATE
Order_Number_Key         KEY(ORDPARTSVer4:Order_Number,ORDPARTSVer4:Part_Ref_Number),DUP,NOCASE
record_number_key        KEY(ORDPARTSVer4:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(ORDPARTSVer4:Part_Ref_Number,ORDPARTSVer4:Part_Number,ORDPARTSVer4:Description),DUP,NOCASE
Part_Number_Key          KEY(ORDPARTSVer4:Order_Number,ORDPARTSVer4:Part_Number,ORDPARTSVer4:Description),DUP,NOCASE
Description_Key          KEY(ORDPARTSVer4:Order_Number,ORDPARTSVer4:Description,ORDPARTSVer4:Part_Number),DUP,NOCASE
Received_Part_Number_Key KEY(ORDPARTSVer4:All_Received,ORDPARTSVer4:Part_Number),DUP,NOCASE
Account_Number_Key       KEY(ORDPARTSVer4:Account_Number,ORDPARTSVer4:Part_Type,ORDPARTSVer4:All_Received,ORDPARTSVer4:Part_Number),DUP,NOCASE
Allocated_Key            KEY(ORDPARTSVer4:Part_Type,ORDPARTSVer4:All_Received,ORDPARTSVer4:Allocated_To_Sale,ORDPARTSVer4:Part_Number),DUP,NOCASE
Allocated_Account_Key    KEY(ORDPARTSVer4:Part_Type,ORDPARTSVer4:All_Received,ORDPARTSVer4:Allocated_To_Sale,ORDPARTSVer4:Account_Number,ORDPARTSVer4:Part_Number),DUP,NOCASE
Order_Type_Received      KEY(ORDPARTSVer4:Order_Number,ORDPARTSVer4:Part_Type,ORDPARTSVer4:All_Received,ORDPARTSVer4:Part_Number,ORDPARTSVer4:Description),DUP,NOCASE
PartRecordNumberKey      KEY(ORDPARTSVer4:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Record_Number               LONG
Part_Ref_Number             LONG
Quantity                    LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Job_Number                  LONG
Part_Type                   STRING(3)
Number_Received             LONG
Date_Received               DATE
All_Received                STRING(3)
Allocated_To_Sale           STRING(3)
Account_Number              STRING(15)
DespatchNoteNumber          STRING(30)
Reason                      STRING(255)
PartRecordNumber            LONG
GRN_Number                  LONG
OrderedCurrency             STRING(30)
OrderedDailyRate            REAL
OrderedDivideMultiply       STRING(1)
ReceivedCurrency            STRING(30)
ReceivedDailyRate           REAL
ReceivedDivideMultiply      STRING(1)
FreeExchangeStock           BYTE
TimeReceived                TIME
DatePriceCaptured           DATE
TimePriceCaptured           TIME
UncapturedGRNNumber         LONG
                         END
                       END

!------------ modified 31.08.2011 at 15:36:32 -----------------
MOD:stFileName:LOCATIONVer2  STRING(260)
LOCATIONVer2           FILE,DRIVER('Btrieve'),NAME('LOCATION.DAT'),PRE(LOCATIONVer2),CREATE
RecordNumberKey          KEY(LOCATIONVer2:RecordNumber),NOCASE,PRIMARY
Location_Key             KEY(LOCATIONVer2:Location),NOCASE
Main_Store_Key           KEY(LOCATIONVer2:Main_Store,LOCATIONVer2:Location),DUP,NOCASE
ActiveLocationKey        KEY(LOCATIONVer2:Active,LOCATIONVer2:Location),DUP,NOCASE
ActiveMainStoreKey       KEY(LOCATIONVer2:Active,LOCATIONVer2:Main_Store,LOCATIONVer2:Location),DUP,NOCASE
VirtualLocationKey       KEY(LOCATIONVer2:VirtualSite,LOCATIONVer2:Location),DUP,NOCASE
VirtualMainStoreKey      KEY(LOCATIONVer2:VirtualSite,LOCATIONVer2:Main_Store,LOCATIONVer2:Location),DUP,NOCASE
FaultyLocationKey        KEY(LOCATIONVer2:FaultyPartsLocation),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
Main_Store                  STRING(3)
Active                      BYTE
VirtualSite                 BYTE
Level1                      BYTE
Level2                      BYTE
Level3                      BYTE
InWarrantyMarkUp            REAL
OutWarrantyMarkUp           REAL
UseRapidStock               BYTE
FaultyPartsLocation         BYTE
                         END
                       END

!------------ modified 10.05.2012 at 11:05:06 -----------------
MOD:stFileName:STOHISTEVer3  STRING(260)
STOHISTEVer3           FILE,DRIVER('Btrieve'),OEM,NAME('STOHISTE.DAT'),PRE(STOHISTEVer3),CREATE
RecordNumberKey          KEY(STOHISTEVer3:RecordNumber),NOCASE,PRIMARY
SHIRecordNumberKey       KEY(STOHISTEVer3:SHIRecordNumber),DUP,NOCASE
KeyArcStatus             KEY(STOHISTEVer3:ARC_Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SHIRecordNumber             LONG
PreviousAveragePurchaseCost REAL
PurchaseCost                REAL
SaleCost                    REAL
RetailCost                  REAL
HistTime                    TIME
ARC_Status                  STRING(1)
                         END
                       END

!------------ modified 19.09.2011 at 14:36:15 -----------------
MOD:stFileName:CREDNOTRVer3  STRING(260)
CREDNOTRVer3           FILE,DRIVER('Btrieve'),OEM,NAME('CREDNOTR.DAT'),PRE(CREDNOTRVer3),CREATE
RecordNumberKey          KEY(CREDNOTRVer3:RecordNumber),NOCASE,PRIMARY
ReceivedKey              KEY(CREDNOTRVer3:Received,CREDNOTRVer3:RecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
Usercode                    STRING(3)
Received                    BYTE
DateReceived                DATE
TimeReceived                TIME
ReceivedUsercode            STRING(3)
                         END
                       END

!------------ modified 13.07.2011 at 12:44:44 -----------------
MOD:stFileName:EXCHANGEVer3  STRING(260)
EXCHANGEVer3           FILE,DRIVER('Btrieve'),NAME('EXCHANGE.DAT'),PRE(EXCHANGEVer3),CREATE
Ref_Number_Key           KEY(EXCHANGEVer3:Ref_Number),NOCASE,PRIMARY
AvailLocIMEI             KEY(EXCHANGEVer3:Available,EXCHANGEVer3:Location,EXCHANGEVer3:ESN),DUP,NOCASE
AvailLocMSN              KEY(EXCHANGEVer3:Available,EXCHANGEVer3:Location,EXCHANGEVer3:MSN),DUP,NOCASE
AvailLocRef              KEY(EXCHANGEVer3:Available,EXCHANGEVer3:Location,EXCHANGEVer3:Ref_Number),DUP,NOCASE
AvailLocModel            KEY(EXCHANGEVer3:Available,EXCHANGEVer3:Location,EXCHANGEVer3:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(EXCHANGEVer3:ESN),DUP,NOCASE
MSN_Only_Key             KEY(EXCHANGEVer3:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Model_Number,EXCHANGEVer3:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(EXCHANGEVer3:Stock_Type,EXCHANGEVer3:ESN),DUP,NOCASE
MSN_Key                  KEY(EXCHANGEVer3:Stock_Type,EXCHANGEVer3:MSN),DUP,NOCASE
ESN_Available_Key        KEY(EXCHANGEVer3:Available,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:ESN),DUP,NOCASE
MSN_Available_Key        KEY(EXCHANGEVer3:Available,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:MSN),DUP,NOCASE
Ref_Available_Key        KEY(EXCHANGEVer3:Available,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(EXCHANGEVer3:Available,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Model_Number,EXCHANGEVer3:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(EXCHANGEVer3:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(EXCHANGEVer3:Model_Number,EXCHANGEVer3:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(EXCHANGEVer3:Available,EXCHANGEVer3:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(EXCHANGEVer3:Available,EXCHANGEVer3:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(EXCHANGEVer3:Available,EXCHANGEVer3:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(EXCHANGEVer3:Available,EXCHANGEVer3:Model_Number,EXCHANGEVer3:Ref_Number),DUP,NOCASE
StockBookedKey           KEY(EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Model_Number,EXCHANGEVer3:Date_Booked,EXCHANGEVer3:Ref_Number),DUP,NOCASE
AvailBookedKey           KEY(EXCHANGEVer3:Available,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Model_Number,EXCHANGEVer3:Date_Booked,EXCHANGEVer3:Ref_Number),DUP,NOCASE
DateBookedKey            KEY(EXCHANGEVer3:Date_Booked),DUP,NOCASE
LocStockAvailIMEIKey     KEY(EXCHANGEVer3:Location,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Available,EXCHANGEVer3:ESN),DUP,NOCASE
LocStockIMEIKey          KEY(EXCHANGEVer3:Location,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:ESN),DUP,NOCASE
LocIMEIKey               KEY(EXCHANGEVer3:Location,EXCHANGEVer3:ESN),DUP,NOCASE
LocStockAvailRefKey      KEY(EXCHANGEVer3:Location,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Available,EXCHANGEVer3:Ref_Number),DUP,NOCASE
LocStockRefKey           KEY(EXCHANGEVer3:Location,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Ref_Number),DUP,NOCASE
LocRefKey                KEY(EXCHANGEVer3:Location,EXCHANGEVer3:Ref_Number),DUP,NOCASE
LocStockAvailModelKey    KEY(EXCHANGEVer3:Location,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Available,EXCHANGEVer3:Model_Number,EXCHANGEVer3:Ref_Number),DUP,NOCASE
LocStockModelKey         KEY(EXCHANGEVer3:Location,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Model_Number,EXCHANGEVer3:Ref_Number),DUP,NOCASE
LocModelKey              KEY(EXCHANGEVer3:Location,EXCHANGEVer3:Model_Number,EXCHANGEVer3:Ref_Number),DUP,NOCASE
LocStockAvailMSNKey      KEY(EXCHANGEVer3:Location,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:Available,EXCHANGEVer3:MSN),DUP,NOCASE
LocStockMSNKey           KEY(EXCHANGEVer3:Location,EXCHANGEVer3:Stock_Type,EXCHANGEVer3:MSN),DUP,NOCASE
LocMSNKey                KEY(EXCHANGEVer3:Location,EXCHANGEVer3:MSN),DUP,NOCASE
InTransitLocationKey     KEY(EXCHANGEVer3:InTransit,EXCHANGEVer3:Location,EXCHANGEVer3:ESN),DUP,NOCASE
InTransitKey             KEY(EXCHANGEVer3:InTransit,EXCHANGEVer3:ESN),DUP,NOCASE
StatusChangeDateKey      KEY(EXCHANGEVer3:StatusChangeDate),DUP,NOCASE
LocStatusChangeDatekey   KEY(EXCHANGEVer3:Location,EXCHANGEVer3:StatusChangeDate),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
Audit_Number                REAL
InTransit                   BYTE
FreeStockPurchased          STRING(1)
StatusChangeDate            DATE
                         END
                       END

!------------ modified 13.02.2014 at 09:37:05 -----------------
MOD:stFileName:SUPPLIERVer5  STRING(260)
SUPPLIERVer5           FILE,DRIVER('Btrieve'),NAME('SUPPLIER.DAT'),PRE(SUPPLIERVer5),CREATE
RecordNumberKey          KEY(SUPPLIERVer5:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(SUPPLIERVer5:Account_Number),DUP,NOCASE
Company_Name_Key         KEY(SUPPLIERVer5:Company_Name),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Company_Name                STRING(30)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(60)
Account_Number              STRING(15)
Order_Period                REAL
Normal_Supply_Period        REAL
Minimum_Order_Value         REAL
HistoryUsage                LONG
Factor                      REAL
Notes                       STRING(255)
UseForeignCurrency          BYTE
CurrencyCode                STRING(30)
MOPSupplierNumber           STRING(30)
MOPItemID                   STRING(30)
UseFreeStock                BYTE
EVO_GL_Acc_No               STRING(10)
EVO_Vendor_Number           STRING(10)
EVO_TaxExempt               BYTE
EVO_Profit_Centre           STRING(30)
EVO_Excluded                BYTE
                         END
                       END

!------------ modified 07.08.2012 at 16:06:30 -----------------
MOD:stFileName:SUBURBVer2  STRING(260)
SUBURBVer2             FILE,DRIVER('Btrieve'),OEM,NAME('SUBURB.DAT'),PRE(SUBURBVer2),CREATE
RecordNumberKey          KEY(SUBURBVer2:RecordNumber),NOCASE,PRIMARY
SuburbKey                KEY(SUBURBVer2:Suburb),DUP,NOCASE
PostcodeKey              KEY(SUBURBVer2:Postcode),DUP,NOCASE
HubKey                   KEY(SUBURBVer2:Hub),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Suburb                      STRING(30)
Postcode                    STRING(30)
Hub                         STRING(30)
Region                      STRING(30)
                         END
                       END

!------------ modified 24.10.2012 at 14:24:46 -----------------
MOD:stFileName:SUPVALAVer2  STRING(260)
SUPVALAVer2            FILE,DRIVER('Btrieve'),OEM,NAME('SUPVALA.DAT'),PRE(SUPVALAVer2),CREATE
RecordNumberKey          KEY(SUPVALAVer2:RecordNumber),NOCASE,PRIMARY
RunDateKey               KEY(SUPVALAVer2:Supplier,SUPVALAVer2:RunDate),DUP,NOCASE
DateOnly                 KEY(SUPVALAVer2:RunDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Supplier                    STRING(30)
RunDate                     DATE
OldBackOrder                DATE
OldOutOrder                 DATE
                         END
                       END

!------------ modified 24.10.2012 at 14:24:47 -----------------
MOD:stFileName:SUPVALBVer2  STRING(260)
SUPVALBVer2            FILE,DRIVER('Btrieve'),OEM,NAME('SUPVALB.DAT'),PRE(SUPVALBVer2),CREATE
RecordNumberKey          KEY(SUPVALBVer2:RecordNumber),NOCASE,PRIMARY
RunDateKey               KEY(SUPVALBVer2:Supplier,SUPVALBVer2:RunDate),DUP,NOCASE
LocationKey              KEY(SUPVALBVer2:Supplier,SUPVALBVer2:RunDate,SUPVALBVer2:Location),DUP,NOCASE
DateOnly                 KEY(SUPVALBVer2:RunDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Supplier                    STRING(30)
RunDate                     DATE
Location                    STRING(30)
BackOrderValue              REAL
                         END
                       END

!------------ modified 19.01.2012 at 10:51:03 -----------------
MOD:stFileName:MODELNUMVer6  STRING(260)
MODELNUMVer6           FILE,DRIVER('Btrieve'),NAME('MODELNUM.DAT'),PRE(MODELNUMVer6),CREATE
Model_Number_Key         KEY(MODELNUMVer6:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer6:Manufacturer,MODELNUMVer6:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer6:Manufacturer,MODELNUMVer6:Unit_Type),DUP,NOCASE
SalesModelKey            KEY(MODELNUMVer6:Manufacturer,MODELNUMVer6:SalesModel),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
ReplacementValue            REAL
WarrantyRepairLimit         REAL
LoanReplacementValue        REAL
ExcludedRRCRepair           BYTE
AllowIMEICharacters         BYTE
UseReplenishmentProcess     BYTE
SalesModel                  STRING(30)
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
Active                      STRING(1)
ExchReplaceValue            REAL
RRCOrderCap                 LONG
                         END
                       END

!------------ modified 15.01.2013 at 10:26:18 -----------------
MOD:stFileName:TRDBATCHVer2  STRING(260)
TRDBATCHVer2           FILE,DRIVER('Btrieve'),OEM,NAME('TRDBATCH.DAT'),PRE(TRDBATCHVer2),CREATE
RecordNumberKey          KEY(TRDBATCHVer2:RecordNumber),NOCASE,PRIMARY
Batch_Number_Key         KEY(TRDBATCHVer2:Company_Name,TRDBATCHVer2:Batch_Number),DUP,NOCASE
Batch_Number_Status_Key  KEY(TRDBATCHVer2:Status,TRDBATCHVer2:Batch_Number),DUP,NOCASE
BatchOnlyKey             KEY(TRDBATCHVer2:Batch_Number),DUP,NOCASE
Batch_Company_Status_Key KEY(TRDBATCHVer2:Status,TRDBATCHVer2:Company_Name,TRDBATCHVer2:Batch_Number),DUP,NOCASE
ESN_Only_Key             KEY(TRDBATCHVer2:ESN),DUP,NOCASE
ESN_Status_Key           KEY(TRDBATCHVer2:Status,TRDBATCHVer2:ESN),DUP,NOCASE
Company_Batch_ESN_Key    KEY(TRDBATCHVer2:Company_Name,TRDBATCHVer2:Batch_Number,TRDBATCHVer2:ESN),DUP,NOCASE
AuthorisationKey         KEY(TRDBATCHVer2:AuthorisationNo),DUP,NOCASE
StatusAuthKey            KEY(TRDBATCHVer2:Status,TRDBATCHVer2:AuthorisationNo),DUP,NOCASE
CompanyDateKey           KEY(TRDBATCHVer2:Company_Name,TRDBATCHVer2:Status,TRDBATCHVer2:DateReturn),DUP,NOCASE
JobStatusKey             KEY(TRDBATCHVer2:Status,TRDBATCHVer2:Ref_Number),DUP,NOCASE
JobNumberKey             KEY(TRDBATCHVer2:Ref_Number),DUP,NOCASE
CompanyDespatchedKey     KEY(TRDBATCHVer2:Company_Name,TRDBATCHVer2:DateDespatched),DUP,NOCASE
ReturnDateKey            KEY(TRDBATCHVer2:DateReturn),DUP,NOCASE
ReturnCompanyKey         KEY(TRDBATCHVer2:DateReturn,TRDBATCHVer2:Company_Name),DUP,NOCASE
NotPrintedManJobKey      KEY(TRDBATCHVer2:BatchRunNotPrinted,TRDBATCHVer2:Status,TRDBATCHVer2:Company_Name,TRDBATCHVer2:Ref_Number),DUP,NOCASE
PurchaseOrderKey         KEY(TRDBATCHVer2:PurchaseOrderNumber,TRDBATCHVer2:Ref_Number),DUP,NOCASE
KeyEVO_Status            KEY(TRDBATCHVer2:EVO_Status),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Batch_Number                LONG
Company_Name                STRING(30)
Ref_Number                  LONG
ESN                         STRING(20)
Status                      STRING(3)
Date                        DATE
Time                        TIME
AuthorisationNo             STRING(30)
Exchanged                   STRING(3)
DateReturn                  DATE
TimeReturn                  TIME
TurnTime                    LONG
MSN                         STRING(30)
DateDespatched              DATE
TimeDespatched              TIME
ReturnUser                  STRING(3)
ReturnWaybillNo             STRING(30)
ReturnRepairType            STRING(30)
ReturnRejectedAmount        REAL
ReturnRejectedReason        STRING(255)
ThirdPartyInvoiceNo         STRING(30)
ThirdPartyInvoiceDate       DATE
ThirdPartyInvoiceCharge     REAL
ThirdPartyVAT               REAL
ThirdPartyMarkup            REAL
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
BatchRunNotPrinted          BYTE
PurchaseOrderNumber         LONG
NewThirdPartySite           STRING(30)
NewThirdPartySiteID         STRING(30)
EVO_Status                  STRING(1)
                         END
                       END

!------------ modified 21.05.2012 at 14:54:23 -----------------
MOD:stFileName:TRADEACCVer9  STRING(260)
TRADEACCVer9           FILE,DRIVER('Btrieve'),NAME('TRADEACC.DAT'),PRE(TRADEACCVer9),CREATE
RecordNumberKey          KEY(TRADEACCVer9:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer9:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer9:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(TRADEACCVer9:ReplicateAccount,TRADEACCVer9:Account_Number),DUP,NOCASE
StoresAccountKey         KEY(TRADEACCVer9:StoresAccount),DUP,NOCASE
SiteLocationKey          KEY(TRADEACCVer9:SiteLocation),DUP,NOCASE
RegionKey                KEY(TRADEACCVer9:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
CCommissionInHouse          LONG
WCommissionInHouse          LONG
CCommissionOutSource        LONG
WCommissionOutSource        LONG
ReplicateAccount            STRING(30)
RemoteRepairCentre          BYTE
SiteLocation                STRING(30)
RRCFactor                   LONG
ARCFactor                   LONG
TransitType                 STRING(30)
ForceMobileNumber           BYTE
BranchIdentification        STRING(2)
AllocateQAEng               BYTE
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
UseTimingsFrom              STRING(30)
StartWorkHours              TIME
EndWorkHours                TIME
IncludeSaturday             STRING(3)
IncludeSunday               STRING(3)
StoresAccount               STRING(30)
RepairEngineerQA            BYTE
SecondYearAccount           BYTE
Activate48Hour              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
Line500AccountNumber        STRING(30)
CompanyOwned                BYTE
OverrideMobileDefault       BYTE
Region                      STRING(30)
AllowCreditNotes            BYTE
DoMSISDNCheck               BYTE
Hub                         STRING(30)
UseSBOnline                 BYTE
IgnoreReplenishmentProcess  BYTE
VCPWaybillPrefix            STRING(30)
RRCWaybillPrefix            STRING(30)
OBFCompanyName              STRING(30)
OBFAddress1                 STRING(30)
OBFAddress2                 STRING(30)
OBFSuburb                   STRING(30)
OBFContactName              STRING(30)
OBFContactNumber            STRING(15)
OBFEmailAddress             STRING(255)
SBOnlineJobProgress         BYTE
coTradingName               STRING(40)
coTradingName2              STRING(40)
coLocation                  STRING(40)
coRegistrationNo            STRING(30)
coVATNumber                 STRING(30)
coAddressLine1              STRING(40)
coAddressLine2              STRING(40)
coAddressLine3              STRING(40)
coAddressLine4              STRING(40)
coTelephoneNumber           STRING(30)
coFaxNumber                 STRING(30)
coEmailAddress              STRING(255)
RtnExchangeAccount          STRING(30)
                         END
                       END

!------------ modified 11.02.2013 at 10:05:17 -----------------
MOD:stFileName:REQUISITVer2  STRING(260)
REQUISITVer2           FILE,DRIVER('Btrieve'),OEM,NAME('REQUISIT.DAT'),PRE(REQUISITVer2),CREATE
RecordNumberKey          KEY(REQUISITVer2:RecordNumber),NOCASE,PRIMARY
SupplierKey              KEY(REQUISITVer2:Supplier),DUP,NOCASE
OrderedNumberKey         KEY(REQUISITVer2:Ordered,REQUISITVer2:RecordNumber),DUP,NOCASE
SupplierNumberKey        KEY(REQUISITVer2:Ordered,REQUISITVer2:Supplier),DUP,NOCASE
DateKey                  KEY(REQUISITVer2:TheDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
TheDate                     DATE
TheTime                     TIME
Supplier                    STRING(30)
Ordered                     BYTE
Approved                    STRING(1)
                         END
                       END

!------------ modified 28.11.2014 at 10:47:50 -----------------
MOD:stFileName:SUBTRACCVer3  STRING(260)
SUBTRACCVer3           FILE,DRIVER('Btrieve'),NAME('SUBTRACC.DAT'),PRE(SUBTRACCVer3),CREATE
RecordNumberKey          KEY(SUBTRACCVer3:RecordNumber),NOCASE,PRIMARY
Main_Account_Key         KEY(SUBTRACCVer3:Main_Account_Number,SUBTRACCVer3:Account_Number),NOCASE
Main_Name_Key            KEY(SUBTRACCVer3:Main_Account_Number,SUBTRACCVer3:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(SUBTRACCVer3:Account_Number),NOCASE
Branch_Key               KEY(SUBTRACCVer3:Branch),DUP,NOCASE
Main_Branch_Key          KEY(SUBTRACCVer3:Main_Account_Number,SUBTRACCVer3:Branch),DUP,NOCASE
Company_Name_Key         KEY(SUBTRACCVer3:Company_Name),DUP,NOCASE
ReplicateFromKey         KEY(SUBTRACCVer3:ReplicateAccount,SUBTRACCVer3:Account_Number),DUP,NOCASE
GenericAccountKey        KEY(SUBTRACCVer3:Generic_Account,SUBTRACCVer3:Account_Number),DUP,NOCASE
GenericCompanyKey        KEY(SUBTRACCVer3:Generic_Account,SUBTRACCVer3:Company_Name),DUP,NOCASE
GenericBranchKey         KEY(SUBTRACCVer3:Generic_Account,SUBTRACCVer3:Branch),DUP,NOCASE
RegionKey                KEY(SUBTRACCVer3:Region),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Price_First_Copy_Only       BYTE
Num_Despatch_Note_Copies    LONG
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
UseAlternativeAdd           BYTE
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
ForceEndUserName            BYTE
ChangeInvAddress            BYTE
ChangeCollAddress           BYTE
ChangeDelAddress            BYTE
AllowMaximumDiscount        BYTE
MaximumDiscount             REAL
SetInvoicedJobStatus        BYTE
SetDespatchJobStatus        BYTE
InvoicedJobStatus           STRING(30)
DespatchedJobStatus         STRING(30)
ReplicateAccount            STRING(30)
FactorAccount               BYTE
ForceEstimate               BYTE
EstimateIfOver              REAL
InvoiceAtCompletion         BYTE
InvoiceTypeComplete         BYTE
Generic_Account             BYTE
StoresAccountNumber         STRING(30)
Force_Customer_Name         BYTE
FinanceContactName          STRING(30)
FinanceTelephoneNo          STRING(30)
FinanceEmailAddress         STRING(255)
FinanceAddress1             STRING(30)
FinanceAddress2             STRING(30)
FinanceAddress3             STRING(30)
FinancePostcode             STRING(30)
AccountLimit                REAL
ExcludeBouncer              BYTE
SatStartWorkHours           TIME
SatEndWorkHours             TIME
SunStartWorkHours           TIME
SunEndWorkHours             TIME
ExcludeFromTATReport        BYTE
OverrideHeadVATNo           BYTE
Line500AccountNumber        STRING(30)
OverrideHeadMobile          BYTE
OverrideMobileDefault       BYTE
SIDJobBooking               BYTE
SIDJobEnquiry               BYTE
Region                      STRING(30)
AllowVCPLoanUnits           BYTE
Hub                         STRING(30)
RefurbishmentAccount        BYTE
VCPWaybillPrefix            STRING(30)
PrintOOWVCPFee              BYTE
OOWVCPFeeLabel              STRING(30)
OOWVCPFeeAmount             REAL
PrintWarrantyVCPFee         BYTE
WarrantyVCPFeeAmount        REAL
DealerID                    STRING(30)
AllowChangeSiebelInfo       BYTE
                         END
                       END

!------------ modified 14.09.2011 at 11:00:40 -----------------
MOD:stFileName:WAYCNRVer3  STRING(260)
WAYCNRVer3             FILE,DRIVER('Btrieve'),OEM,NAME('WAYCNR.DAT'),PRE(WAYCNRVer3),CREATE
RecordNumberKey          KEY(WAYCNRVer3:RecordNumber),NOCASE,PRIMARY
EnteredKey               KEY(WAYCNRVer3:WAYBILLSRecordnumber,WAYCNRVer3:RecordNumber),DUP,NOCASE
PartNumberKey            KEY(WAYCNRVer3:WAYBILLSRecordnumber,WAYCNRVer3:ExchangeOrder,WAYCNRVer3:PartNumber),DUP,NOCASE
ProcessedKey             KEY(WAYCNRVer3:WAYBILLSRecordnumber,WAYCNRVer3:Processed,WAYCNRVer3:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
WAYBILLSRecordnumber        LONG
PartNumber                  STRING(30)
Description                 STRING(30)
ExchangeOrder               BYTE
Quantity                    LONG
QuantityReceived            LONG
Processed                   BYTE
RefNumber                   LONG
                         END
                       END

!------------ modified 05.02.2014 at 13:37:37 -----------------
MOD:stFileName:MANUFACTVer11  STRING(260)
MANUFACTVer11          FILE,DRIVER('Btrieve'),NAME('MANUFACT.DAT'),PRE(MANUFACTVer11),CREATE
RecordNumberKey          KEY(MANUFACTVer11:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer11:Manufacturer),NOCASE
EDIFileTypeKey           KEY(MANUFACTVer11:EDIFileType,MANUFACTVer11:Manufacturer),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(30)
Postcode                    STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(30)
Fax_Number                  STRING(30)
EmailAddress                STRING(30)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                LONG
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
ApplyMSNFormat              BYTE
MSNFormat                   STRING(30)
ValidateDateCode            BYTE
ForceStatus                 BYTE
StatusRequired              STRING(30)
POPPeriod                   LONG
ClaimPeriod                 LONG
QAParts                     BYTE
QANetwork                   BYTE
POPRequired                 BYTE
UseInvTextForFaults         BYTE
ForceAccessoryCode          BYTE
UseInternetValidation       BYTE
AutoRepairType              BYTE
BillingConfirmation         BYTE
CreateEDIReport             BYTE
EDIFileType                 STRING(30)
CreateEDIFile               BYTE
ExchangeFee                 REAL
QALoan                      BYTE
ForceCharFaultCodes         BYTE
IncludeCharJobs             BYTE
SecondYrExchangeFee         REAL
SecondYrTradeAccount        STRING(30)
VATNumber                   STRING(30)
UseProdCodesForEXC          BYTE
UseFaultCodesForOBF         BYTE
KeyRepairRequired           BYTE
UseReplenishmentProcess     BYTE
UseResubmissionLimit        BYTE
ResubmissionLimit           LONG
AutoTechnicalReports        BYTE
AlertEmailAddress           STRING(255)
LimitResubmissions          BYTE
TimesToResubmit             LONG
CopyDOPFromBouncer          BYTE
ExcludeAutomaticRebookingProcess BYTE
OneYearWarrOnly             STRING(1)
EDItransportFee             REAL
SagemVersionNumber          STRING(30)
UseBouncerRules             BYTE
BouncerPeriod               LONG
BouncerType                 BYTE
BouncerInFault              BYTE
BouncerOutFault             BYTE
BouncerSpares               BYTE
BouncerPeriodIMEI           LONG
DoNotBounceWarranty         BYTE
BounceRulesType             BYTE
ThirdPartyHandlingFee       REAL
ProductCodeCompulsory       STRING(3)
Inactive                    BYTE
                         END
                       END

!------------ End of Declaration Current Files Structure -------------

DCDummySQLTableOwner   STRING(260)
MOD:stFileName STRING(260)

FileQueue QUEUE,PRE()
FileLabel   &FILE
FileName    &STRING
          END

TempFileQueue QUEUE,PRE()
TempFileLabel   &FILE
TempFileName    &STRING
              END

ListExt       QUEUE,PRE()
Extention       CSTRING(260)
              END


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELLU001.INC'),ONCE        !Local module procedure declarations
ConvertWAYBCONF        PROCEDURE(BYTE),BYTE
ConvertSOAPERRS        PROCEDURE(BYTE),BYTE
ConvertJOBSE3          PROCEDURE(BYTE),BYTE
ConvertSTOAUUSE        PROCEDURE(BYTE),BYTE
ConvertPREJOB          PROCEDURE(BYTE),BYTE
ConvertTRADEAC2        PROCEDURE(BYTE),BYTE
ConvertSMSRECVD        PROCEDURE(BYTE),BYTE
ConvertGRNOTES         PROCEDURE(BYTE),BYTE
ConvertEXCHAUI         PROCEDURE(BYTE),BYTE
ConvertRTNAWAIT        PROCEDURE(BYTE),BYTE
ConvertSTMASAUD        PROCEDURE(BYTE),BYTE
ConvertORDITEMS        PROCEDURE(BYTE),BYTE
ConvertLOCATLOG        PROCEDURE(BYTE),BYTE
ConvertDEFAULT2        PROCEDURE(BYTE),BYTE
ConvertRETSALES        PROCEDURE(BYTE),BYTE
ConvertLOAORDR         PROCEDURE(BYTE),BYTE
ConvertWIPAMF          PROCEDURE(BYTE),BYTE
ConvertTEAMS           PROCEDURE(BYTE),BYTE
ConvertWIPAUI          PROCEDURE(BYTE),BYTE
ConvertEXCHORDR        PROCEDURE(BYTE),BYTE
ConvertCONTHIST        PROCEDURE(BYTE),BYTE
ConvertSTANTEXT        PROCEDURE(BYTE),BYTE
ConvertJOBSWARR        PROCEDURE(BYTE),BYTE
ConvertCURRENCY        PROCEDURE(BYTE),BYTE
ConvertJOBSE2          PROCEDURE(BYTE),BYTE
ConvertORDPEND         PROCEDURE(BYTE),BYTE
ConvertREPTYDEF        PROCEDURE(BYTE),BYTE
ConvertMANFAULT        PROCEDURE(BYTE),BYTE
ConvertLOCVALUE        PROCEDURE(BYTE),BYTE
ConvertCOURIER         PROCEDURE(BYTE),BYTE
ConvertTRDPARTY        PROCEDURE(BYTE),BYTE
ConvertTRANTYPE        PROCEDURE(BYTE),BYTE
ConvertESNMODEL        PROCEDURE(BYTE),BYTE
ConvertSTOCK           PROCEDURE(BYTE),BYTE
ConvertLOAN            PROCEDURE(BYTE),BYTE
ConvertSMSMAIL         PROCEDURE(BYTE),BYTE
ConvertLOANHIST        PROCEDURE(BYTE),BYTE
ConvertEXCHHIST        PROCEDURE(BYTE),BYTE
ConvertAUDIT           PROCEDURE(BYTE),BYTE
ConvertUSERS           PROCEDURE(BYTE),BYTE
ConvertLOCSHELF        PROCEDURE(BYTE),BYTE
ConvertRTNORDER        PROCEDURE(BYTE),BYTE
ConvertC3DMONIT        PROCEDURE(BYTE),BYTE
ConvertSMSText         PROCEDURE(BYTE),BYTE
ConvertRETSTOCK        PROCEDURE(BYTE),BYTE
ConvertSTOCKALX        PROCEDURE(BYTE),BYTE
ConvertORDPARTS        PROCEDURE(BYTE),BYTE
ConvertLOCATION        PROCEDURE(BYTE),BYTE
ConvertSTOHISTE        PROCEDURE(BYTE),BYTE
ConvertCREDNOTR        PROCEDURE(BYTE),BYTE
ConvertEXCHANGE        PROCEDURE(BYTE),BYTE
ConvertSUPPLIER        PROCEDURE(BYTE),BYTE
ConvertSUBURB          PROCEDURE(BYTE),BYTE
ConvertSUPVALA         PROCEDURE(BYTE),BYTE
ConvertSUPVALB         PROCEDURE(BYTE),BYTE
ConvertMODELNUM        PROCEDURE(BYTE),BYTE
ConvertTRDBATCH        PROCEDURE(BYTE),BYTE
ConvertTRADEACC        PROCEDURE(BYTE),BYTE
ConvertREQUISIT        PROCEDURE(BYTE),BYTE
ConvertSUBTRACC        PROCEDURE(BYTE),BYTE
ConvertWAYCNR          PROCEDURE(BYTE),BYTE
ConvertMANUFACT        PROCEDURE(BYTE),BYTE
ProcessFileQueue       PROCEDURE(),BYTE
ErrorBox               PROCEDURE(STRING)
FileErrorBox           PROCEDURE()
CreateTempName         PROCEDURE(*STRING,STRING,FILE)
RemoveFiles            PROCEDURE(FILE,STRING)
GetFileNameWithoutOwner PROCEDURE(STRING),STRING
RenameFile             PROCEDURE(FILE,STRING)
PrepareExtentionQueue  PROCEDURE(FILE)
PrepareMultiTableFileName PROCEDURE(FILE,STRING,*STRING),BYTE
PrepareDestFile        PROCEDURE(FILE,STRING,FILE,STRING,STRING),BYTE
RenameDestFile         PROCEDURE(FILE,STRING,STRING),BYTE
                       MODULE('ClarionRTL')
DC:FnMerge               PROCEDURE(*CSTRING,*CSTRING,*CSTRING,*CSTRING,*CSTRING),RAW,NAME('_fnmerge')
DC:FnSplit               PROCEDURE(*CSTRING,*CSTRING,*CSTRING,*CSTRING,*CSTRING),SHORT,RAW,NAME('_fnsplit')
DC:SetError              PROCEDURE(LONG),NAME('Cla$seterror')
DC:SetErrorFile          PROCEDURE(FILE),NAME('Cla$SetErrorFile')
                       END
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

window               WINDOW('Update'),AT(,,188,76),FONT('Tahoma',8,,),COLOR(091BB9DH),CENTER,GRAY,DOUBLE
                       PROMPT('Update Complete.'),AT(45,10),USE(?Prompt1),TRN,FONT(,12,COLOR:Yellow,FONT:bold)
                       PROMPT('Please now install the latest patch.'),AT(28,34),USE(?Prompt2),FONT(,10,,)
                       BUTTON('Close'),AT(66,54,56,16),USE(?Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  PUTINI('STARTING','Run',True,)
  GLODC:ConversionResult = DC:FilesConverter()
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  !TB13393 - J - 05/02/15 - add default ASV code to all trade account
  Add_ASV_Code
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


DC:FilesConverter PROCEDURE(<STRING PAR:FileLabel>,<STRING PAR:FileName>)
  CODE
  IF ~OMITTED(1) AND ~OMITTED(2) AND PAR:FileLabel AND PAR:FileName
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('WAYBCONF')
      WAYBCONFVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertWAYBCONF(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SOAPERRS')
      SOAPERRSVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSOAPERRS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('JOBSE3')
      JOBSE3Ver3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertJOBSE3(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('STOAUUSE')
      STOAUUSEVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSTOAUUSE(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('PREJOB')
      PREJOBVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertPREJOB(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('TRADEAC2')
      TRADEAC2Ver4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertTRADEAC2(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SMSRECVD')
      SMSRECVDVer5{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSMSRECVD(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('GRNOTES')
      GRNOTESVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertGRNOTES(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('EXCHAUI')
      EXCHAUIVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertEXCHAUI(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('RTNAWAIT')
      RTNAWAITVer7{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertRTNAWAIT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('STMASAUD')
      STMASAUDVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSTMASAUD(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('ORDITEMS')
      ORDITEMSVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertORDITEMS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOCATLOG')
      LOCATLOGVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOCATLOG(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFAULT2')
      DEFAULT2Ver5{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFAULT2(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('RETSALES')
      RETSALESVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertRETSALES(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOAORDR')
      LOAORDRVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOAORDR(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('WIPAMF')
      WIPAMFVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertWIPAMF(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('TEAMS')
      TEAMSVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertTEAMS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('WIPAUI')
      WIPAUIVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertWIPAUI(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('EXCHORDR')
      EXCHORDRVer6{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertEXCHORDR(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('CONTHIST')
      CONTHISTVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertCONTHIST(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('STANTEXT')
      STANTEXTVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSTANTEXT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('JOBSWARR')
      JOBSWARRVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertJOBSWARR(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('CURRENCY')
      CURRENCYVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertCURRENCY(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('JOBSE2')
      JOBSE2Ver4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertJOBSE2(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('ORDPEND')
      ORDPENDVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertORDPEND(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('REPTYDEF')
      REPTYDEFVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertREPTYDEF(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MANFAULT')
      MANFAULTVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMANFAULT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOCVALUE')
      LOCVALUEVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOCVALUE(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('COURIER')
      COURIERVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertCOURIER(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('TRDPARTY')
      TRDPARTYVer5{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertTRDPARTY(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('TRANTYPE')
      TRANTYPEVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertTRANTYPE(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('ESNMODEL')
      ESNMODELVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertESNMODEL(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('STOCK')
      STOCKVer7{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSTOCK(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOAN')
      LOANVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOAN(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SMSMAIL')
      SMSMAILVer11{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSMSMAIL(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOANHIST')
      LOANHISTVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOANHIST(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('EXCHHIST')
      EXCHHISTVer5{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertEXCHHIST(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('AUDIT')
      AUDITVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertAUDIT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('USERS')
      USERSVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertUSERS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOCSHELF')
      LOCSHELFVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOCSHELF(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('RTNORDER')
      RTNORDERVer13{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertRTNORDER(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('C3DMONIT')
      C3DMONITVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertC3DMONIT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SMSText')
      SMSTextVer8{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSMSText(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('RETSTOCK')
      RETSTOCKVer5{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertRETSTOCK(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('STOCKALX')
      STOCKALXVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSTOCKALX(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('ORDPARTS')
      ORDPARTSVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertORDPARTS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOCATION')
      LOCATIONVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOCATION(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('STOHISTE')
      STOHISTEVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSTOHISTE(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('CREDNOTR')
      CREDNOTRVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertCREDNOTR(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('EXCHANGE')
      EXCHANGEVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertEXCHANGE(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SUPPLIER')
      SUPPLIERVer5{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSUPPLIER(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SUBURB')
      SUBURBVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSUBURB(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SUPVALA')
      SUPVALAVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSUPVALA(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SUPVALB')
      SUPVALBVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSUPVALB(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MODELNUM')
      MODELNUMVer6{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMODELNUM(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('TRDBATCH')
      TRDBATCHVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertTRDBATCH(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('TRADEACC')
      TRADEACCVer9{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertTRADEACC(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('REQUISIT')
      REQUISITVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertREQUISIT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SUBTRACC')
      SUBTRACCVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSUBTRACC(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('WAYCNR')
      WAYCNRVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertWAYCNR(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MANUFACT')
      MANUFACTVer11{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMANUFACT(0) THEN RETURN(1).
    END
  ELSE
    IF ConvertWAYBCONF(1) THEN RETURN(1).
    IF ConvertSOAPERRS(1) THEN RETURN(1).
    IF ConvertJOBSE3(1) THEN RETURN(1).
    IF ConvertSTOAUUSE(1) THEN RETURN(1).
    IF ConvertPREJOB(1) THEN RETURN(1).
    IF ConvertTRADEAC2(1) THEN RETURN(1).
    IF ConvertSMSRECVD(1) THEN RETURN(1).
    IF ConvertGRNOTES(1) THEN RETURN(1).
    IF ConvertEXCHAUI(1) THEN RETURN(1).
    IF ConvertRTNAWAIT(1) THEN RETURN(1).
    IF ConvertSTMASAUD(1) THEN RETURN(1).
    IF ConvertORDITEMS(1) THEN RETURN(1).
    IF ConvertLOCATLOG(1) THEN RETURN(1).
    IF ConvertDEFAULT2(1) THEN RETURN(1).
    IF ConvertRETSALES(1) THEN RETURN(1).
    IF ConvertLOAORDR(1) THEN RETURN(1).
    IF ConvertWIPAMF(1) THEN RETURN(1).
    IF ConvertTEAMS(1) THEN RETURN(1).
    IF ConvertWIPAUI(1) THEN RETURN(1).
    IF ConvertEXCHORDR(1) THEN RETURN(1).
    IF ConvertCONTHIST(1) THEN RETURN(1).
    IF ConvertSTANTEXT(1) THEN RETURN(1).
    IF ConvertJOBSWARR(1) THEN RETURN(1).
    IF ConvertCURRENCY(1) THEN RETURN(1).
    IF ConvertJOBSE2(1) THEN RETURN(1).
    IF ConvertORDPEND(1) THEN RETURN(1).
    IF ConvertREPTYDEF(1) THEN RETURN(1).
    IF ConvertMANFAULT(1) THEN RETURN(1).
    IF ConvertLOCVALUE(1) THEN RETURN(1).
    IF ConvertCOURIER(1) THEN RETURN(1).
    IF ConvertTRDPARTY(1) THEN RETURN(1).
    IF ConvertTRANTYPE(1) THEN RETURN(1).
    IF ConvertESNMODEL(1) THEN RETURN(1).
    IF ConvertSTOCK(1) THEN RETURN(1).
    IF ConvertLOAN(1) THEN RETURN(1).
    IF ConvertSMSMAIL(1) THEN RETURN(1).
    IF ConvertLOANHIST(1) THEN RETURN(1).
    IF ConvertEXCHHIST(1) THEN RETURN(1).
    IF ConvertAUDIT(1) THEN RETURN(1).
    IF ConvertUSERS(1) THEN RETURN(1).
    IF ConvertLOCSHELF(1) THEN RETURN(1).
    IF ConvertRTNORDER(1) THEN RETURN(1).
    IF ConvertC3DMONIT(1) THEN RETURN(1).
    IF ConvertSMSText(1) THEN RETURN(1).
    IF ConvertRETSTOCK(1) THEN RETURN(1).
    IF ConvertSTOCKALX(1) THEN RETURN(1).
    IF ConvertORDPARTS(1) THEN RETURN(1).
    IF ConvertLOCATION(1) THEN RETURN(1).
    IF ConvertSTOHISTE(1) THEN RETURN(1).
    IF ConvertCREDNOTR(1) THEN RETURN(1).
    IF ConvertEXCHANGE(1) THEN RETURN(1).
    IF ConvertSUPPLIER(1) THEN RETURN(1).
    IF ConvertSUBURB(1) THEN RETURN(1).
    IF ConvertSUPVALA(1) THEN RETURN(1).
    IF ConvertSUPVALB(1) THEN RETURN(1).
    IF ConvertMODELNUM(1) THEN RETURN(1).
    IF ConvertTRDBATCH(1) THEN RETURN(1).
    IF ConvertTRADEACC(1) THEN RETURN(1).
    IF ConvertREQUISIT(1) THEN RETURN(1).
    IF ConvertSUBTRACC(1) THEN RETURN(1).
    IF ConvertWAYCNR(1) THEN RETURN(1).
    IF ConvertMANUFACT(1) THEN RETURN(1).
  END
  RETURN(0)
!---------------------------------------------------------------------

ConvertWAYBCONF PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = WAYBCONFVer2{PROP:Name}
  MOD:stFileName:WAYBCONFVer1 = 'WAYBCONF'
  FileQueue.FileLabel &= WAYBCONFVer1
  FileQueue.FileName &= MOD:stFileName:WAYBCONFVer1
  ADD(FileQueue)
  MOD:stFileName:WAYBCONFVer2 = 'WAYBCONF'
  FileQueue.FileLabel &= WAYBCONFVer2
  FileQueue.FileName &= MOD:stFileName:WAYBCONFVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'WAYBCONF'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSOAPERRS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SOAPERRSVer2{PROP:Name}
  MOD:stFileName:SOAPERRSVer1 = 'SOAPERRS.DAT'
  FileQueue.FileLabel &= SOAPERRSVer1
  FileQueue.FileName &= MOD:stFileName:SOAPERRSVer1
  ADD(FileQueue)
  MOD:stFileName:SOAPERRSVer2 = 'SOAPERRS.DAT'
  FileQueue.FileLabel &= SOAPERRSVer2
  FileQueue.FileName &= MOD:stFileName:SOAPERRSVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SOAPERRS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertJOBSE3 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = JOBSE3Ver3{PROP:Name}
  MOD:stFileName:JOBSE3Ver1 = 'JOBSE3.DAT'
  FileQueue.FileLabel &= JOBSE3Ver1
  FileQueue.FileName &= MOD:stFileName:JOBSE3Ver1
  ADD(FileQueue)
  MOD:stFileName:JOBSE3Ver2 = 'JOBSE3.DAT'
  FileQueue.FileLabel &= JOBSE3Ver2
  FileQueue.FileName &= MOD:stFileName:JOBSE3Ver2
  ADD(FileQueue)
  MOD:stFileName:JOBSE3Ver3 = 'JOBSE3.DAT'
  FileQueue.FileLabel &= JOBSE3Ver3
  FileQueue.FileName &= MOD:stFileName:JOBSE3Ver3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'JOBSE3'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSTOAUUSE PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = STOAUUSEVer2{PROP:Name}
  MOD:stFileName:STOAUUSEVer1 = 'STOAUUSE.DAT'
  FileQueue.FileLabel &= STOAUUSEVer1
  FileQueue.FileName &= MOD:stFileName:STOAUUSEVer1
  ADD(FileQueue)
  MOD:stFileName:STOAUUSEVer2 = 'STOAUUSE.DAT'
  FileQueue.FileLabel &= STOAUUSEVer2
  FileQueue.FileName &= MOD:stFileName:STOAUUSEVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'STOAUUSE'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertPREJOB PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = PREJOBVer3{PROP:Name}
  MOD:stFileName:PREJOBVer1 = 'PREJOB.DAT'
  FileQueue.FileLabel &= PREJOBVer1
  FileQueue.FileName &= MOD:stFileName:PREJOBVer1
  ADD(FileQueue)
  MOD:stFileName:PREJOBVer2 = 'PREJOB.DAT'
  FileQueue.FileLabel &= PREJOBVer2
  FileQueue.FileName &= MOD:stFileName:PREJOBVer2
  ADD(FileQueue)
  MOD:stFileName:PREJOBVer3 = 'PREJOB.DAT'
  FileQueue.FileLabel &= PREJOBVer3
  FileQueue.FileName &= MOD:stFileName:PREJOBVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'PREJOB'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertTRADEAC2 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = TRADEAC2Ver4{PROP:Name}
  MOD:stFileName:TRADEAC2Ver1 = 'TRADEAC2'
  FileQueue.FileLabel &= TRADEAC2Ver1
  FileQueue.FileName &= MOD:stFileName:TRADEAC2Ver1
  ADD(FileQueue)
  MOD:stFileName:TRADEAC2Ver2 = 'TRADEAC2'
  FileQueue.FileLabel &= TRADEAC2Ver2
  FileQueue.FileName &= MOD:stFileName:TRADEAC2Ver2
  ADD(FileQueue)
  MOD:stFileName:TRADEAC2Ver3 = 'TRADEAC2'
  FileQueue.FileLabel &= TRADEAC2Ver3
  FileQueue.FileName &= MOD:stFileName:TRADEAC2Ver3
  ADD(FileQueue)
  MOD:stFileName:TRADEAC2Ver4 = 'TRADEAC2'
  FileQueue.FileLabel &= TRADEAC2Ver4
  FileQueue.FileName &= MOD:stFileName:TRADEAC2Ver4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'TRADEAC2'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSMSRECVD PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SMSRECVDVer5{PROP:Name}
  MOD:stFileName:SMSRECVDVer1 = 'SMSRECEI'
  FileQueue.FileLabel &= SMSRECVDVer1
  FileQueue.FileName &= MOD:stFileName:SMSRECVDVer1
  ADD(FileQueue)
  MOD:stFileName:SMSRECVDVer2 = 'SMSRECEI'
  FileQueue.FileLabel &= SMSRECVDVer2
  FileQueue.FileName &= MOD:stFileName:SMSRECVDVer2
  ADD(FileQueue)
  MOD:stFileName:SMSRECVDVer3 = 'SMSRECEI'
  FileQueue.FileLabel &= SMSRECVDVer3
  FileQueue.FileName &= MOD:stFileName:SMSRECVDVer3
  ADD(FileQueue)
  MOD:stFileName:SMSRECVDVer4 = 'SMSRECVD'
  FileQueue.FileLabel &= SMSRECVDVer4
  FileQueue.FileName &= MOD:stFileName:SMSRECVDVer4
  ADD(FileQueue)
  MOD:stFileName:SMSRECVDVer5 = 'SMSRECVD.DAT'
  FileQueue.FileLabel &= SMSRECVDVer5
  FileQueue.FileName &= MOD:stFileName:SMSRECVDVer5
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SMSRECVD'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertGRNOTES PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = GRNOTESVer3{PROP:Name}
  MOD:stFileName:GRNOTESVer1 = 'GRNOTES.DAT'
  FileQueue.FileLabel &= GRNOTESVer1
  FileQueue.FileName &= MOD:stFileName:GRNOTESVer1
  ADD(FileQueue)
  MOD:stFileName:GRNOTESVer2 = 'GRNOTES.DAT'
  FileQueue.FileLabel &= GRNOTESVer2
  FileQueue.FileName &= MOD:stFileName:GRNOTESVer2
  ADD(FileQueue)
  MOD:stFileName:GRNOTESVer3 = 'GRNOTES.DAT'
  FileQueue.FileLabel &= GRNOTESVer3
  FileQueue.FileName &= MOD:stFileName:GRNOTESVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'GRNOTES'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertEXCHAUI PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = EXCHAUIVer2{PROP:Name}
  MOD:stFileName:EXCHAUIVer1 = 'EXCHAUI.DAT'
  FileQueue.FileLabel &= EXCHAUIVer1
  FileQueue.FileName &= MOD:stFileName:EXCHAUIVer1
  ADD(FileQueue)
  MOD:stFileName:EXCHAUIVer2 = 'EXCHAUI.DAT'
  FileQueue.FileLabel &= EXCHAUIVer2
  FileQueue.FileName &= MOD:stFileName:EXCHAUIVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'EXCHAUI'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertRTNAWAIT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = RTNAWAITVer7{PROP:Name}
  MOD:stFileName:RTNAWAITVer1 = 'RTNAWAIT.DAT'
  FileQueue.FileLabel &= RTNAWAITVer1
  FileQueue.FileName &= MOD:stFileName:RTNAWAITVer1
  ADD(FileQueue)
  MOD:stFileName:RTNAWAITVer2 = 'RTNAWAIT.DAT'
  FileQueue.FileLabel &= RTNAWAITVer2
  FileQueue.FileName &= MOD:stFileName:RTNAWAITVer2
  ADD(FileQueue)
  MOD:stFileName:RTNAWAITVer3 = 'RTNAWAIT.DAT'
  FileQueue.FileLabel &= RTNAWAITVer3
  FileQueue.FileName &= MOD:stFileName:RTNAWAITVer3
  ADD(FileQueue)
  MOD:stFileName:RTNAWAITVer4 = 'RTNAWAIT.DAT'
  FileQueue.FileLabel &= RTNAWAITVer4
  FileQueue.FileName &= MOD:stFileName:RTNAWAITVer4
  ADD(FileQueue)
  MOD:stFileName:RTNAWAITVer5 = 'RTNAWAIT.DAT'
  FileQueue.FileLabel &= RTNAWAITVer5
  FileQueue.FileName &= MOD:stFileName:RTNAWAITVer5
  ADD(FileQueue)
  MOD:stFileName:RTNAWAITVer6 = 'RTNAWAIT.DAT'
  FileQueue.FileLabel &= RTNAWAITVer6
  FileQueue.FileName &= MOD:stFileName:RTNAWAITVer6
  ADD(FileQueue)
  MOD:stFileName:RTNAWAITVer7 = 'RTNAWAIT.DAT'
  FileQueue.FileLabel &= RTNAWAITVer7
  FileQueue.FileName &= MOD:stFileName:RTNAWAITVer7
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'RTNAWAIT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSTMASAUD PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = STMASAUDVer2{PROP:Name}
  MOD:stFileName:STMASAUDVer1 = 'STMASAUD.DAT'
  FileQueue.FileLabel &= STMASAUDVer1
  FileQueue.FileName &= MOD:stFileName:STMASAUDVer1
  ADD(FileQueue)
  MOD:stFileName:STMASAUDVer2 = 'STMASAUD.DAT'
  FileQueue.FileLabel &= STMASAUDVer2
  FileQueue.FileName &= MOD:stFileName:STMASAUDVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'STMASAUD'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertORDITEMS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = ORDITEMSVer2{PROP:Name}
  MOD:stFileName:ORDITEMSVer1 = 'ORDITEMS.DAT'
  FileQueue.FileLabel &= ORDITEMSVer1
  FileQueue.FileName &= MOD:stFileName:ORDITEMSVer1
  ADD(FileQueue)
  MOD:stFileName:ORDITEMSVer2 = 'ORDITEMS.DAT'
  FileQueue.FileLabel &= ORDITEMSVer2
  FileQueue.FileName &= MOD:stFileName:ORDITEMSVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'ORDITEMS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOCATLOG PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOCATLOGVer2{PROP:Name}
  MOD:stFileName:LOCATLOGVer1 = 'LOCATLOG.DAT'
  FileQueue.FileLabel &= LOCATLOGVer1
  FileQueue.FileName &= MOD:stFileName:LOCATLOGVer1
  ADD(FileQueue)
  MOD:stFileName:LOCATLOGVer2 = 'LOCATLOG.DAT'
  FileQueue.FileLabel &= LOCATLOGVer2
  FileQueue.FileName &= MOD:stFileName:LOCATLOGVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOCATLOG'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFAULT2 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFAULT2Ver5{PROP:Name}
  MOD:stFileName:DEFAULT2Ver1 = 'DEFAULT2.DAT'
  FileQueue.FileLabel &= DEFAULT2Ver1
  FileQueue.FileName &= MOD:stFileName:DEFAULT2Ver1
  ADD(FileQueue)
  MOD:stFileName:DEFAULT2Ver2 = 'DEFAULT2.DAT'
  FileQueue.FileLabel &= DEFAULT2Ver2
  FileQueue.FileName &= MOD:stFileName:DEFAULT2Ver2
  ADD(FileQueue)
  MOD:stFileName:DEFAULT2Ver3 = 'DEFAULT2.DAT'
  FileQueue.FileLabel &= DEFAULT2Ver3
  FileQueue.FileName &= MOD:stFileName:DEFAULT2Ver3
  ADD(FileQueue)
  MOD:stFileName:DEFAULT2Ver4 = 'DEFAULT2.DAT'
  FileQueue.FileLabel &= DEFAULT2Ver4
  FileQueue.FileName &= MOD:stFileName:DEFAULT2Ver4
  ADD(FileQueue)
  MOD:stFileName:DEFAULT2Ver5 = 'DEFAULT2.DAT'
  FileQueue.FileLabel &= DEFAULT2Ver5
  FileQueue.FileName &= MOD:stFileName:DEFAULT2Ver5
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFAULT2'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertRETSALES PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = RETSALESVer3{PROP:Name}
  MOD:stFileName:RETSALESVer1 = 'RETSALES.DAT'
  FileQueue.FileLabel &= RETSALESVer1
  FileQueue.FileName &= MOD:stFileName:RETSALESVer1
  ADD(FileQueue)
  MOD:stFileName:RETSALESVer2 = 'RETSALES.DAT'
  FileQueue.FileLabel &= RETSALESVer2
  FileQueue.FileName &= MOD:stFileName:RETSALESVer2
  ADD(FileQueue)
  MOD:stFileName:RETSALESVer3 = 'RETSALES.DAT'
  FileQueue.FileLabel &= RETSALESVer3
  FileQueue.FileName &= MOD:stFileName:RETSALESVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'RETSALES'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOAORDR PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOAORDRVer2{PROP:Name}
  MOD:stFileName:LOAORDRVer1 = 'LOAORDR'
  FileQueue.FileLabel &= LOAORDRVer1
  FileQueue.FileName &= MOD:stFileName:LOAORDRVer1
  ADD(FileQueue)
  MOD:stFileName:LOAORDRVer2 = 'LOAORDR'
  FileQueue.FileLabel &= LOAORDRVer2
  FileQueue.FileName &= MOD:stFileName:LOAORDRVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOAORDR'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertWIPAMF PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = WIPAMFVer2{PROP:Name}
  MOD:stFileName:WIPAMFVer1 = 'WIPAMF.DAT'
  FileQueue.FileLabel &= WIPAMFVer1
  FileQueue.FileName &= MOD:stFileName:WIPAMFVer1
  ADD(FileQueue)
  MOD:stFileName:WIPAMFVer2 = 'WIPAMF.DAT'
  FileQueue.FileLabel &= WIPAMFVer2
  FileQueue.FileName &= MOD:stFileName:WIPAMFVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'WIPAMF'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertTEAMS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = TEAMSVer3{PROP:Name}
  MOD:stFileName:TEAMSVer1 = 'TEAMS.DAT'
  FileQueue.FileLabel &= TEAMSVer1
  FileQueue.FileName &= MOD:stFileName:TEAMSVer1
  ADD(FileQueue)
  MOD:stFileName:TEAMSVer2 = 'TEAMS.DAT'
  FileQueue.FileLabel &= TEAMSVer2
  FileQueue.FileName &= MOD:stFileName:TEAMSVer2
  ADD(FileQueue)
  MOD:stFileName:TEAMSVer3 = 'TEAMS.DAT'
  FileQueue.FileLabel &= TEAMSVer3
  FileQueue.FileName &= MOD:stFileName:TEAMSVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'TEAMS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertWIPAUI PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = WIPAUIVer2{PROP:Name}
  MOD:stFileName:WIPAUIVer1 = 'WIPAUI.DAT'
  FileQueue.FileLabel &= WIPAUIVer1
  FileQueue.FileName &= MOD:stFileName:WIPAUIVer1
  ADD(FileQueue)
  MOD:stFileName:WIPAUIVer2 = 'WIPAUI.DAT'
  FileQueue.FileLabel &= WIPAUIVer2
  FileQueue.FileName &= MOD:stFileName:WIPAUIVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'WIPAUI'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertEXCHORDR PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = EXCHORDRVer6{PROP:Name}
  MOD:stFileName:EXCHORDRVer1 = 'EXCHORDR'
  FileQueue.FileLabel &= EXCHORDRVer1
  FileQueue.FileName &= MOD:stFileName:EXCHORDRVer1
  ADD(FileQueue)
  MOD:stFileName:EXCHORDRVer2 = 'EXCHORDR'
  FileQueue.FileLabel &= EXCHORDRVer2
  FileQueue.FileName &= MOD:stFileName:EXCHORDRVer2
  ADD(FileQueue)
  MOD:stFileName:EXCHORDRVer3 = 'EXCHORDR'
  FileQueue.FileLabel &= EXCHORDRVer3
  FileQueue.FileName &= MOD:stFileName:EXCHORDRVer3
  ADD(FileQueue)
  MOD:stFileName:EXCHORDRVer4 = 'EXCHORDR.DAT'
  FileQueue.FileLabel &= EXCHORDRVer4
  FileQueue.FileName &= MOD:stFileName:EXCHORDRVer4
  ADD(FileQueue)
  MOD:stFileName:EXCHORDRVer5 = 'EXCHORDR.DAT'
  FileQueue.FileLabel &= EXCHORDRVer5
  FileQueue.FileName &= MOD:stFileName:EXCHORDRVer5
  ADD(FileQueue)
  MOD:stFileName:EXCHORDRVer6 = 'EXCHORDR.DAT'
  FileQueue.FileLabel &= EXCHORDRVer6
  FileQueue.FileName &= MOD:stFileName:EXCHORDRVer6
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'EXCHORDR'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertCONTHIST PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = CONTHISTVer2{PROP:Name}
  MOD:stFileName:CONTHISTVer1 = 'CONTHIST.DAT'
  FileQueue.FileLabel &= CONTHISTVer1
  FileQueue.FileName &= MOD:stFileName:CONTHISTVer1
  ADD(FileQueue)
  MOD:stFileName:CONTHISTVer2 = 'CONTHIST.DAT'
  FileQueue.FileLabel &= CONTHISTVer2
  FileQueue.FileName &= MOD:stFileName:CONTHISTVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'CONTHIST'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSTANTEXT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = STANTEXTVer2{PROP:Name}
  MOD:stFileName:STANTEXTVer1 = 'STANTEXT.DAT'
  FileQueue.FileLabel &= STANTEXTVer1
  FileQueue.FileName &= MOD:stFileName:STANTEXTVer1
  ADD(FileQueue)
  MOD:stFileName:STANTEXTVer2 = 'STANTEXT.DAT'
  FileQueue.FileLabel &= STANTEXTVer2
  FileQueue.FileName &= MOD:stFileName:STANTEXTVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'STANTEXT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertJOBSWARR PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = JOBSWARRVer3{PROP:Name}
  MOD:stFileName:JOBSWARRVer1 = 'JOBSWARR.DAT'
  FileQueue.FileLabel &= JOBSWARRVer1
  FileQueue.FileName &= MOD:stFileName:JOBSWARRVer1
  ADD(FileQueue)
  MOD:stFileName:JOBSWARRVer2 = 'JOBSWARR.DAT'
  FileQueue.FileLabel &= JOBSWARRVer2
  FileQueue.FileName &= MOD:stFileName:JOBSWARRVer2
  ADD(FileQueue)
  MOD:stFileName:JOBSWARRVer3 = 'JOBSWARR.DAT'
  FileQueue.FileLabel &= JOBSWARRVer3
  FileQueue.FileName &= MOD:stFileName:JOBSWARRVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'JOBSWARR'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertCURRENCY PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = CURRENCYVer3{PROP:Name}
  MOD:stFileName:CURRENCYVer1 = 'CURRENCY.DAT'
  FileQueue.FileLabel &= CURRENCYVer1
  FileQueue.FileName &= MOD:stFileName:CURRENCYVer1
  ADD(FileQueue)
  MOD:stFileName:CURRENCYVer2 = 'CURRENCY.DAT'
  FileQueue.FileLabel &= CURRENCYVer2
  FileQueue.FileName &= MOD:stFileName:CURRENCYVer2
  ADD(FileQueue)
  MOD:stFileName:CURRENCYVer3 = 'CURRENCY.DAT'
  FileQueue.FileLabel &= CURRENCYVer3
  FileQueue.FileName &= MOD:stFileName:CURRENCYVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'CURRENCY'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertJOBSE2 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = JOBSE2Ver4{PROP:Name}
  MOD:stFileName:JOBSE2Ver1 = 'JOBSE2.DAT'
  FileQueue.FileLabel &= JOBSE2Ver1
  FileQueue.FileName &= MOD:stFileName:JOBSE2Ver1
  ADD(FileQueue)
  MOD:stFileName:JOBSE2Ver2 = 'JOBSE2.DAT'
  FileQueue.FileLabel &= JOBSE2Ver2
  FileQueue.FileName &= MOD:stFileName:JOBSE2Ver2
  ADD(FileQueue)
  MOD:stFileName:JOBSE2Ver3 = 'JOBSE2.DAT'
  FileQueue.FileLabel &= JOBSE2Ver3
  FileQueue.FileName &= MOD:stFileName:JOBSE2Ver3
  ADD(FileQueue)
  MOD:stFileName:JOBSE2Ver4 = 'JOBSE2.DAT'
  FileQueue.FileLabel &= JOBSE2Ver4
  FileQueue.FileName &= MOD:stFileName:JOBSE2Ver4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'JOBSE2'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertORDPEND PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = ORDPENDVer2{PROP:Name}
  MOD:stFileName:ORDPENDVer1 = 'ORDPEND.DAT'
  FileQueue.FileLabel &= ORDPENDVer1
  FileQueue.FileName &= MOD:stFileName:ORDPENDVer1
  ADD(FileQueue)
  MOD:stFileName:ORDPENDVer2 = 'ORDPEND.DAT'
  FileQueue.FileLabel &= ORDPENDVer2
  FileQueue.FileName &= MOD:stFileName:ORDPENDVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'ORDPEND'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertREPTYDEF PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = REPTYDEFVer3{PROP:Name}
  MOD:stFileName:REPTYDEFVer1 = 'REPTYDEF.DAT'
  FileQueue.FileLabel &= REPTYDEFVer1
  FileQueue.FileName &= MOD:stFileName:REPTYDEFVer1
  ADD(FileQueue)
  MOD:stFileName:REPTYDEFVer2 = 'REPTYDEF.DAT'
  FileQueue.FileLabel &= REPTYDEFVer2
  FileQueue.FileName &= MOD:stFileName:REPTYDEFVer2
  ADD(FileQueue)
  MOD:stFileName:REPTYDEFVer3 = 'REPTYDEF.DAT'
  FileQueue.FileLabel &= REPTYDEFVer3
  FileQueue.FileName &= MOD:stFileName:REPTYDEFVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'REPTYDEF'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMANFAULT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MANFAULTVer2{PROP:Name}
  MOD:stFileName:MANFAULTVer1 = 'MANFAULT.DAT'
  FileQueue.FileLabel &= MANFAULTVer1
  FileQueue.FileName &= MOD:stFileName:MANFAULTVer1
  ADD(FileQueue)
  MOD:stFileName:MANFAULTVer2 = 'MANFAULT.DAT'
  FileQueue.FileLabel &= MANFAULTVer2
  FileQueue.FileName &= MOD:stFileName:MANFAULTVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MANFAULT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOCVALUE PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOCVALUEVer2{PROP:Name}
  MOD:stFileName:LOCVALUEVer1 = 'LOCVALUE.DAT'
  FileQueue.FileLabel &= LOCVALUEVer1
  FileQueue.FileName &= MOD:stFileName:LOCVALUEVer1
  ADD(FileQueue)
  MOD:stFileName:LOCVALUEVer2 = 'LOCVALUE.DAT'
  FileQueue.FileLabel &= LOCVALUEVer2
  FileQueue.FileName &= MOD:stFileName:LOCVALUEVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOCVALUE'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertCOURIER PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = COURIERVer2{PROP:Name}
  MOD:stFileName:COURIERVer1 = 'COURIER.DAT'
  FileQueue.FileLabel &= COURIERVer1
  FileQueue.FileName &= MOD:stFileName:COURIERVer1
  ADD(FileQueue)
  MOD:stFileName:COURIERVer2 = 'COURIER.DAT'
  FileQueue.FileLabel &= COURIERVer2
  FileQueue.FileName &= MOD:stFileName:COURIERVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'COURIER'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertTRDPARTY PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = TRDPARTYVer5{PROP:Name}
  MOD:stFileName:TRDPARTYVer1 = 'TRDPARTY.DAT'
  FileQueue.FileLabel &= TRDPARTYVer1
  FileQueue.FileName &= MOD:stFileName:TRDPARTYVer1
  ADD(FileQueue)
  MOD:stFileName:TRDPARTYVer2 = 'TRDPARTY.DAT'
  FileQueue.FileLabel &= TRDPARTYVer2
  FileQueue.FileName &= MOD:stFileName:TRDPARTYVer2
  ADD(FileQueue)
  MOD:stFileName:TRDPARTYVer3 = 'TRDPARTY.DAT'
  FileQueue.FileLabel &= TRDPARTYVer3
  FileQueue.FileName &= MOD:stFileName:TRDPARTYVer3
  ADD(FileQueue)
  MOD:stFileName:TRDPARTYVer4 = 'TRDPARTY.DAT'
  FileQueue.FileLabel &= TRDPARTYVer4
  FileQueue.FileName &= MOD:stFileName:TRDPARTYVer4
  ADD(FileQueue)
  MOD:stFileName:TRDPARTYVer5 = 'TRDPARTY.DAT'
  FileQueue.FileLabel &= TRDPARTYVer5
  FileQueue.FileName &= MOD:stFileName:TRDPARTYVer5
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'TRDPARTY'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertTRANTYPE PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = TRANTYPEVer3{PROP:Name}
  MOD:stFileName:TRANTYPEVer1 = 'TRANTYPE.DAT'
  FileQueue.FileLabel &= TRANTYPEVer1
  FileQueue.FileName &= MOD:stFileName:TRANTYPEVer1
  ADD(FileQueue)
  MOD:stFileName:TRANTYPEVer2 = 'TRANTYPE.DAT'
  FileQueue.FileLabel &= TRANTYPEVer2
  FileQueue.FileName &= MOD:stFileName:TRANTYPEVer2
  ADD(FileQueue)
  MOD:stFileName:TRANTYPEVer3 = 'TRANTYPE.DAT'
  FileQueue.FileLabel &= TRANTYPEVer3
  FileQueue.FileName &= MOD:stFileName:TRANTYPEVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'TRANTYPE'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertESNMODEL PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = ESNMODELVer2{PROP:Name}
  MOD:stFileName:ESNMODELVer1 = 'ESNMODEL.DAT'
  FileQueue.FileLabel &= ESNMODELVer1
  FileQueue.FileName &= MOD:stFileName:ESNMODELVer1
  ADD(FileQueue)
  MOD:stFileName:ESNMODELVer2 = 'ESNMODEL.DAT'
  FileQueue.FileLabel &= ESNMODELVer2
  FileQueue.FileName &= MOD:stFileName:ESNMODELVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'ESNMODEL'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSTOCK PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = STOCKVer7{PROP:Name}
  MOD:stFileName:STOCKVer1 = 'STOCK.DAT'
  FileQueue.FileLabel &= STOCKVer1
  FileQueue.FileName &= MOD:stFileName:STOCKVer1
  ADD(FileQueue)
  MOD:stFileName:STOCKVer2 = 'STOCK.DAT'
  FileQueue.FileLabel &= STOCKVer2
  FileQueue.FileName &= MOD:stFileName:STOCKVer2
  ADD(FileQueue)
  MOD:stFileName:STOCKVer3 = 'STOCK.DAT'
  FileQueue.FileLabel &= STOCKVer3
  FileQueue.FileName &= MOD:stFileName:STOCKVer3
  ADD(FileQueue)
  MOD:stFileName:STOCKVer4 = 'STOCK.DAT'
  FileQueue.FileLabel &= STOCKVer4
  FileQueue.FileName &= MOD:stFileName:STOCKVer4
  ADD(FileQueue)
  MOD:stFileName:STOCKVer5 = 'STOCK.DAT'
  FileQueue.FileLabel &= STOCKVer5
  FileQueue.FileName &= MOD:stFileName:STOCKVer5
  ADD(FileQueue)
  MOD:stFileName:STOCKVer6 = 'STOCK.DAT'
  FileQueue.FileLabel &= STOCKVer6
  FileQueue.FileName &= MOD:stFileName:STOCKVer6
  ADD(FileQueue)
  MOD:stFileName:STOCKVer7 = 'STOCK.DAT'
  FileQueue.FileLabel &= STOCKVer7
  FileQueue.FileName &= MOD:stFileName:STOCKVer7
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'STOCK'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOAN PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOANVer2{PROP:Name}
  MOD:stFileName:LOANVer1 = 'LOAN.DAT'
  FileQueue.FileLabel &= LOANVer1
  FileQueue.FileName &= MOD:stFileName:LOANVer1
  ADD(FileQueue)
  MOD:stFileName:LOANVer2 = 'LOAN.DAT'
  FileQueue.FileLabel &= LOANVer2
  FileQueue.FileName &= MOD:stFileName:LOANVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOAN'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSMSMAIL PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SMSMAILVer11{PROP:Name}
  MOD:stFileName:SMSMAILVer1 = 'SMSMAIL.DAT'
  FileQueue.FileLabel &= SMSMAILVer1
  FileQueue.FileName &= MOD:stFileName:SMSMAILVer1
  ADD(FileQueue)
  MOD:stFileName:SMSMAILVer2 = 'SMSMAIL.DAT'
  FileQueue.FileLabel &= SMSMAILVer2
  FileQueue.FileName &= MOD:stFileName:SMSMAILVer2
  ADD(FileQueue)
  MOD:stFileName:SMSMAILVer3 = 'SMSMAIL.DAT'
  FileQueue.FileLabel &= SMSMAILVer3
  FileQueue.FileName &= MOD:stFileName:SMSMAILVer3
  ADD(FileQueue)
  MOD:stFileName:SMSMAILVer4 = 'SMSMAIL.DAT'
  FileQueue.FileLabel &= SMSMAILVer4
  FileQueue.FileName &= MOD:stFileName:SMSMAILVer4
  ADD(FileQueue)
  MOD:stFileName:SMSMAILVer5 = 'SMSMAIL.DAT'
  FileQueue.FileLabel &= SMSMAILVer5
  FileQueue.FileName &= MOD:stFileName:SMSMAILVer5
  ADD(FileQueue)
  MOD:stFileName:SMSMAILVer6 = 'SMSMAIL.DAT'
  FileQueue.FileLabel &= SMSMAILVer6
  FileQueue.FileName &= MOD:stFileName:SMSMAILVer6
  ADD(FileQueue)
  MOD:stFileName:SMSMAILVer7 = 'SMSMAIL.DAT'
  FileQueue.FileLabel &= SMSMAILVer7
  FileQueue.FileName &= MOD:stFileName:SMSMAILVer7
  ADD(FileQueue)
  MOD:stFileName:SMSMAILVer8 = 'SMSMAIL.DAT'
  FileQueue.FileLabel &= SMSMAILVer8
  FileQueue.FileName &= MOD:stFileName:SMSMAILVer8
  ADD(FileQueue)
  MOD:stFileName:SMSMAILVer9 = 'SMSMAIL.DAT'
  FileQueue.FileLabel &= SMSMAILVer9
  FileQueue.FileName &= MOD:stFileName:SMSMAILVer9
  ADD(FileQueue)
  MOD:stFileName:SMSMAILVer10 = 'SMSMAIL.DAT'
  FileQueue.FileLabel &= SMSMAILVer10
  FileQueue.FileName &= MOD:stFileName:SMSMAILVer10
  ADD(FileQueue)
  MOD:stFileName:SMSMAILVer11 = 'SMSMAIL.DAT'
  FileQueue.FileLabel &= SMSMAILVer11
  FileQueue.FileName &= MOD:stFileName:SMSMAILVer11
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SMSMAIL'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOANHIST PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOANHISTVer2{PROP:Name}
  MOD:stFileName:LOANHISTVer1 = 'LOANHIST.DAT'
  FileQueue.FileLabel &= LOANHISTVer1
  FileQueue.FileName &= MOD:stFileName:LOANHISTVer1
  ADD(FileQueue)
  MOD:stFileName:LOANHISTVer2 = 'LOANHIST.DAT'
  FileQueue.FileLabel &= LOANHISTVer2
  FileQueue.FileName &= MOD:stFileName:LOANHISTVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOANHIST'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertEXCHHIST PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = EXCHHISTVer5{PROP:Name}
  MOD:stFileName:EXCHHISTVer1 = 'EXCHHIST.DAT'
  FileQueue.FileLabel &= EXCHHISTVer1
  FileQueue.FileName &= MOD:stFileName:EXCHHISTVer1
  ADD(FileQueue)
  MOD:stFileName:EXCHHISTVer2 = 'EXCHHIST.DAT'
  FileQueue.FileLabel &= EXCHHISTVer2
  FileQueue.FileName &= MOD:stFileName:EXCHHISTVer2
  ADD(FileQueue)
  MOD:stFileName:EXCHHISTVer3 = 'EXCHHIST.DAT'
  FileQueue.FileLabel &= EXCHHISTVer3
  FileQueue.FileName &= MOD:stFileName:EXCHHISTVer3
  ADD(FileQueue)
  MOD:stFileName:EXCHHISTVer4 = 'EXCHHIST.DAT'
  FileQueue.FileLabel &= EXCHHISTVer4
  FileQueue.FileName &= MOD:stFileName:EXCHHISTVer4
  ADD(FileQueue)
  MOD:stFileName:EXCHHISTVer5 = 'EXCHHIST.DAT'
  FileQueue.FileLabel &= EXCHHISTVer5
  FileQueue.FileName &= MOD:stFileName:EXCHHISTVer5
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'EXCHHIST'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertAUDIT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = AUDITVer2{PROP:Name}
  MOD:stFileName:AUDITVer1 = 'AUDIT.DAT'
  FileQueue.FileLabel &= AUDITVer1
  FileQueue.FileName &= MOD:stFileName:AUDITVer1
  ADD(FileQueue)
  MOD:stFileName:AUDITVer2 = 'AUDIT.DAT'
  FileQueue.FileLabel &= AUDITVer2
  FileQueue.FileName &= MOD:stFileName:AUDITVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'AUDIT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertUSERS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = USERSVer4{PROP:Name}
  MOD:stFileName:USERSVer1 = 'USERS.DAT'
  FileQueue.FileLabel &= USERSVer1
  FileQueue.FileName &= MOD:stFileName:USERSVer1
  ADD(FileQueue)
  MOD:stFileName:USERSVer2 = 'USERS.DAT'
  FileQueue.FileLabel &= USERSVer2
  FileQueue.FileName &= MOD:stFileName:USERSVer2
  ADD(FileQueue)
  MOD:stFileName:USERSVer3 = 'USERS.DAT'
  FileQueue.FileLabel &= USERSVer3
  FileQueue.FileName &= MOD:stFileName:USERSVer3
  ADD(FileQueue)
  MOD:stFileName:USERSVer4 = 'USERS.DAT'
  FileQueue.FileLabel &= USERSVer4
  FileQueue.FileName &= MOD:stFileName:USERSVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'USERS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOCSHELF PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOCSHELFVer2{PROP:Name}
  MOD:stFileName:LOCSHELFVer1 = 'LOCSHELF.DAT'
  FileQueue.FileLabel &= LOCSHELFVer1
  FileQueue.FileName &= MOD:stFileName:LOCSHELFVer1
  ADD(FileQueue)
  MOD:stFileName:LOCSHELFVer2 = 'LOCSHELF.DAT'
  FileQueue.FileLabel &= LOCSHELFVer2
  FileQueue.FileName &= MOD:stFileName:LOCSHELFVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOCSHELF'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertRTNORDER PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = RTNORDERVer13{PROP:Name}
  MOD:stFileName:RTNORDERVer1 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer1
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer1
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer2 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer2
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer2
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer3 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer3
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer3
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer4 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer4
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer4
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer5 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer5
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer5
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer6 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer6
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer6
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer7 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer7
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer7
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer8 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer8
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer8
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer9 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer9
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer9
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer10 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer10
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer10
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer11 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer11
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer11
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer12 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer12
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer12
  ADD(FileQueue)
  MOD:stFileName:RTNORDERVer13 = 'RTNORDER.DAT'
  FileQueue.FileLabel &= RTNORDERVer13
  FileQueue.FileName &= MOD:stFileName:RTNORDERVer13
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'RTNORDER'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertC3DMONIT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = C3DMONITVer2{PROP:Name}
  MOD:stFileName:C3DMONITVer1 = 'C3DMONIT.DAT'
  FileQueue.FileLabel &= C3DMONITVer1
  FileQueue.FileName &= MOD:stFileName:C3DMONITVer1
  ADD(FileQueue)
  MOD:stFileName:C3DMONITVer2 = 'C3DMONIT.DAT'
  FileQueue.FileLabel &= C3DMONITVer2
  FileQueue.FileName &= MOD:stFileName:C3DMONITVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'C3DMONIT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSMSText PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SMSTextVer8{PROP:Name}
  MOD:stFileName:SMSTextVer1 = 'SMSText'
  FileQueue.FileLabel &= SMSTextVer1
  FileQueue.FileName &= MOD:stFileName:SMSTextVer1
  ADD(FileQueue)
  MOD:stFileName:SMSTextVer2 = 'SMSText.Dat'
  FileQueue.FileLabel &= SMSTextVer2
  FileQueue.FileName &= MOD:stFileName:SMSTextVer2
  ADD(FileQueue)
  MOD:stFileName:SMSTextVer3 = 'SMSText.Dat'
  FileQueue.FileLabel &= SMSTextVer3
  FileQueue.FileName &= MOD:stFileName:SMSTextVer3
  ADD(FileQueue)
  MOD:stFileName:SMSTextVer4 = 'SMSText.Dat'
  FileQueue.FileLabel &= SMSTextVer4
  FileQueue.FileName &= MOD:stFileName:SMSTextVer4
  ADD(FileQueue)
  MOD:stFileName:SMSTextVer5 = 'SMSText.Dat'
  FileQueue.FileLabel &= SMSTextVer5
  FileQueue.FileName &= MOD:stFileName:SMSTextVer5
  ADD(FileQueue)
  MOD:stFileName:SMSTextVer6 = 'SMSText.Dat'
  FileQueue.FileLabel &= SMSTextVer6
  FileQueue.FileName &= MOD:stFileName:SMSTextVer6
  ADD(FileQueue)
  MOD:stFileName:SMSTextVer7 = 'SMSText.Dat'
  FileQueue.FileLabel &= SMSTextVer7
  FileQueue.FileName &= MOD:stFileName:SMSTextVer7
  ADD(FileQueue)
  MOD:stFileName:SMSTextVer8 = 'SMSText.Dat'
  FileQueue.FileLabel &= SMSTextVer8
  FileQueue.FileName &= MOD:stFileName:SMSTextVer8
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SMSText'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertRETSTOCK PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = RETSTOCKVer5{PROP:Name}
  MOD:stFileName:RETSTOCKVer1 = 'RETSTOCK.DAT'
  FileQueue.FileLabel &= RETSTOCKVer1
  FileQueue.FileName &= MOD:stFileName:RETSTOCKVer1
  ADD(FileQueue)
  MOD:stFileName:RETSTOCKVer2 = 'RETSTOCK.DAT'
  FileQueue.FileLabel &= RETSTOCKVer2
  FileQueue.FileName &= MOD:stFileName:RETSTOCKVer2
  ADD(FileQueue)
  MOD:stFileName:RETSTOCKVer3 = 'RETSTOCK.DAT'
  FileQueue.FileLabel &= RETSTOCKVer3
  FileQueue.FileName &= MOD:stFileName:RETSTOCKVer3
  ADD(FileQueue)
  MOD:stFileName:RETSTOCKVer4 = 'RETSTOCK.DAT'
  FileQueue.FileLabel &= RETSTOCKVer4
  FileQueue.FileName &= MOD:stFileName:RETSTOCKVer4
  ADD(FileQueue)
  MOD:stFileName:RETSTOCKVer5 = 'RETSTOCK.DAT'
  FileQueue.FileLabel &= RETSTOCKVer5
  FileQueue.FileName &= MOD:stFileName:RETSTOCKVer5
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'RETSTOCK'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSTOCKALX PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = STOCKALXVer3{PROP:Name}
  MOD:stFileName:STOCKALXVer1 = 'STOCKALX'
  FileQueue.FileLabel &= STOCKALXVer1
  FileQueue.FileName &= MOD:stFileName:STOCKALXVer1
  ADD(FileQueue)
  MOD:stFileName:STOCKALXVer2 = 'STOCKALX'
  FileQueue.FileLabel &= STOCKALXVer2
  FileQueue.FileName &= MOD:stFileName:STOCKALXVer2
  ADD(FileQueue)
  MOD:stFileName:STOCKALXVer3 = 'STOCKALX'
  FileQueue.FileLabel &= STOCKALXVer3
  FileQueue.FileName &= MOD:stFileName:STOCKALXVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'STOCKALX'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertORDPARTS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = ORDPARTSVer4{PROP:Name}
  MOD:stFileName:ORDPARTSVer1 = 'ORDPARTS.DAT'
  FileQueue.FileLabel &= ORDPARTSVer1
  FileQueue.FileName &= MOD:stFileName:ORDPARTSVer1
  ADD(FileQueue)
  MOD:stFileName:ORDPARTSVer2 = 'ORDPARTS.DAT'
  FileQueue.FileLabel &= ORDPARTSVer2
  FileQueue.FileName &= MOD:stFileName:ORDPARTSVer2
  ADD(FileQueue)
  MOD:stFileName:ORDPARTSVer3 = 'ORDPARTS.DAT'
  FileQueue.FileLabel &= ORDPARTSVer3
  FileQueue.FileName &= MOD:stFileName:ORDPARTSVer3
  ADD(FileQueue)
  MOD:stFileName:ORDPARTSVer4 = 'ORDPARTS.DAT'
  FileQueue.FileLabel &= ORDPARTSVer4
  FileQueue.FileName &= MOD:stFileName:ORDPARTSVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'ORDPARTS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOCATION PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOCATIONVer2{PROP:Name}
  MOD:stFileName:LOCATIONVer1 = 'LOCATION.DAT'
  FileQueue.FileLabel &= LOCATIONVer1
  FileQueue.FileName &= MOD:stFileName:LOCATIONVer1
  ADD(FileQueue)
  MOD:stFileName:LOCATIONVer2 = 'LOCATION.DAT'
  FileQueue.FileLabel &= LOCATIONVer2
  FileQueue.FileName &= MOD:stFileName:LOCATIONVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOCATION'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSTOHISTE PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = STOHISTEVer3{PROP:Name}
  MOD:stFileName:STOHISTEVer1 = 'STOHISTE.DAT'
  FileQueue.FileLabel &= STOHISTEVer1
  FileQueue.FileName &= MOD:stFileName:STOHISTEVer1
  ADD(FileQueue)
  MOD:stFileName:STOHISTEVer2 = 'STOHISTE.DAT'
  FileQueue.FileLabel &= STOHISTEVer2
  FileQueue.FileName &= MOD:stFileName:STOHISTEVer2
  ADD(FileQueue)
  MOD:stFileName:STOHISTEVer3 = 'STOHISTE.DAT'
  FileQueue.FileLabel &= STOHISTEVer3
  FileQueue.FileName &= MOD:stFileName:STOHISTEVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'STOHISTE'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertCREDNOTR PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = CREDNOTRVer3{PROP:Name}
  MOD:stFileName:CREDNOTRVer1 = 'CREDNOTR.DAT'
  FileQueue.FileLabel &= CREDNOTRVer1
  FileQueue.FileName &= MOD:stFileName:CREDNOTRVer1
  ADD(FileQueue)
  MOD:stFileName:CREDNOTRVer2 = 'CREDNOTR.DAT'
  FileQueue.FileLabel &= CREDNOTRVer2
  FileQueue.FileName &= MOD:stFileName:CREDNOTRVer2
  ADD(FileQueue)
  MOD:stFileName:CREDNOTRVer3 = 'CREDNOTR.DAT'
  FileQueue.FileLabel &= CREDNOTRVer3
  FileQueue.FileName &= MOD:stFileName:CREDNOTRVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'CREDNOTR'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertEXCHANGE PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = EXCHANGEVer3{PROP:Name}
  MOD:stFileName:EXCHANGEVer1 = 'EXCHANGE.DAT'
  FileQueue.FileLabel &= EXCHANGEVer1
  FileQueue.FileName &= MOD:stFileName:EXCHANGEVer1
  ADD(FileQueue)
  MOD:stFileName:EXCHANGEVer2 = 'EXCHANGE.DAT'
  FileQueue.FileLabel &= EXCHANGEVer2
  FileQueue.FileName &= MOD:stFileName:EXCHANGEVer2
  ADD(FileQueue)
  MOD:stFileName:EXCHANGEVer3 = 'EXCHANGE.DAT'
  FileQueue.FileLabel &= EXCHANGEVer3
  FileQueue.FileName &= MOD:stFileName:EXCHANGEVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'EXCHANGE'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSUPPLIER PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SUPPLIERVer5{PROP:Name}
  MOD:stFileName:SUPPLIERVer1 = 'SUPPLIER.DAT'
  FileQueue.FileLabel &= SUPPLIERVer1
  FileQueue.FileName &= MOD:stFileName:SUPPLIERVer1
  ADD(FileQueue)
  MOD:stFileName:SUPPLIERVer2 = 'SUPPLIER.DAT'
  FileQueue.FileLabel &= SUPPLIERVer2
  FileQueue.FileName &= MOD:stFileName:SUPPLIERVer2
  ADD(FileQueue)
  MOD:stFileName:SUPPLIERVer3 = 'SUPPLIER.DAT'
  FileQueue.FileLabel &= SUPPLIERVer3
  FileQueue.FileName &= MOD:stFileName:SUPPLIERVer3
  ADD(FileQueue)
  MOD:stFileName:SUPPLIERVer4 = 'SUPPLIER.DAT'
  FileQueue.FileLabel &= SUPPLIERVer4
  FileQueue.FileName &= MOD:stFileName:SUPPLIERVer4
  ADD(FileQueue)
  MOD:stFileName:SUPPLIERVer5 = 'SUPPLIER.DAT'
  FileQueue.FileLabel &= SUPPLIERVer5
  FileQueue.FileName &= MOD:stFileName:SUPPLIERVer5
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SUPPLIER'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSUBURB PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SUBURBVer2{PROP:Name}
  MOD:stFileName:SUBURBVer1 = 'SUBURB.DAT'
  FileQueue.FileLabel &= SUBURBVer1
  FileQueue.FileName &= MOD:stFileName:SUBURBVer1
  ADD(FileQueue)
  MOD:stFileName:SUBURBVer2 = 'SUBURB.DAT'
  FileQueue.FileLabel &= SUBURBVer2
  FileQueue.FileName &= MOD:stFileName:SUBURBVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SUBURB'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSUPVALA PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SUPVALAVer2{PROP:Name}
  MOD:stFileName:SUPVALAVer1 = 'SUPVALA.DAT'
  FileQueue.FileLabel &= SUPVALAVer1
  FileQueue.FileName &= MOD:stFileName:SUPVALAVer1
  ADD(FileQueue)
  MOD:stFileName:SUPVALAVer2 = 'SUPVALA.DAT'
  FileQueue.FileLabel &= SUPVALAVer2
  FileQueue.FileName &= MOD:stFileName:SUPVALAVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SUPVALA'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSUPVALB PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SUPVALBVer2{PROP:Name}
  MOD:stFileName:SUPVALBVer1 = 'SUPVALB.DAT'
  FileQueue.FileLabel &= SUPVALBVer1
  FileQueue.FileName &= MOD:stFileName:SUPVALBVer1
  ADD(FileQueue)
  MOD:stFileName:SUPVALBVer2 = 'SUPVALB.DAT'
  FileQueue.FileLabel &= SUPVALBVer2
  FileQueue.FileName &= MOD:stFileName:SUPVALBVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SUPVALB'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMODELNUM PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MODELNUMVer6{PROP:Name}
  MOD:stFileName:MODELNUMVer1 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer1
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer1
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer2 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer2
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer2
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer3 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer3
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer3
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer4 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer4
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer4
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer5 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer5
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer5
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer6 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer6
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer6
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MODELNUM'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertTRDBATCH PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = TRDBATCHVer2{PROP:Name}
  MOD:stFileName:TRDBATCHVer1 = 'TRDBATCH.DAT'
  FileQueue.FileLabel &= TRDBATCHVer1
  FileQueue.FileName &= MOD:stFileName:TRDBATCHVer1
  ADD(FileQueue)
  MOD:stFileName:TRDBATCHVer2 = 'TRDBATCH.DAT'
  FileQueue.FileLabel &= TRDBATCHVer2
  FileQueue.FileName &= MOD:stFileName:TRDBATCHVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'TRDBATCH'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertTRADEACC PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = TRADEACCVer9{PROP:Name}
  MOD:stFileName:TRADEACCVer1 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer1
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer1
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer2 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer2
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer2
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer3 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer3
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer3
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer4 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer4
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer4
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer5 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer5
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer5
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer6 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer6
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer6
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer7 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer7
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer7
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer8 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer8
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer8
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer9 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer9
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer9
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'TRADEACC'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertREQUISIT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = REQUISITVer2{PROP:Name}
  MOD:stFileName:REQUISITVer1 = 'REQUISIT.DAT'
  FileQueue.FileLabel &= REQUISITVer1
  FileQueue.FileName &= MOD:stFileName:REQUISITVer1
  ADD(FileQueue)
  MOD:stFileName:REQUISITVer2 = 'REQUISIT.DAT'
  FileQueue.FileLabel &= REQUISITVer2
  FileQueue.FileName &= MOD:stFileName:REQUISITVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'REQUISIT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSUBTRACC PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SUBTRACCVer3{PROP:Name}
  MOD:stFileName:SUBTRACCVer1 = 'SUBTRACC.DAT'
  FileQueue.FileLabel &= SUBTRACCVer1
  FileQueue.FileName &= MOD:stFileName:SUBTRACCVer1
  ADD(FileQueue)
  MOD:stFileName:SUBTRACCVer2 = 'SUBTRACC.DAT'
  FileQueue.FileLabel &= SUBTRACCVer2
  FileQueue.FileName &= MOD:stFileName:SUBTRACCVer2
  ADD(FileQueue)
  MOD:stFileName:SUBTRACCVer3 = 'SUBTRACC.DAT'
  FileQueue.FileLabel &= SUBTRACCVer3
  FileQueue.FileName &= MOD:stFileName:SUBTRACCVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SUBTRACC'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertWAYCNR PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = WAYCNRVer3{PROP:Name}
  MOD:stFileName:WAYCNRVer1 = 'WAYCNR.DAT'
  FileQueue.FileLabel &= WAYCNRVer1
  FileQueue.FileName &= MOD:stFileName:WAYCNRVer1
  ADD(FileQueue)
  MOD:stFileName:WAYCNRVer2 = 'WAYCNR.DAT'
  FileQueue.FileLabel &= WAYCNRVer2
  FileQueue.FileName &= MOD:stFileName:WAYCNRVer2
  ADD(FileQueue)
  MOD:stFileName:WAYCNRVer3 = 'WAYCNR.DAT'
  FileQueue.FileLabel &= WAYCNRVer3
  FileQueue.FileName &= MOD:stFileName:WAYCNRVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'WAYCNR'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMANUFACT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MANUFACTVer11{PROP:Name}
  MOD:stFileName:MANUFACTVer1 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer1
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer1
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer2 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer2
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer2
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer3 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer3
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer3
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer4 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer4
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer4
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer5 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer5
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer5
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer6 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer6
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer6
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer7 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer7
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer7
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer8 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer8
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer8
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer9 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer9
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer9
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer10 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer10
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer10
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer11 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer11
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer11
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MANUFACT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ProcessFileQueue PROCEDURE

LOC:lLoopIndex        LONG,AUTO
LOC:lTempIndex        LONG,AUTO
LOC:stStatusFullBuild STRING(3),AUTO
LOC:byReturnCode      BYTE(1)
LOC:byErrorStatus     BYTE(0)

  CODE
  FREE(TempFileQueue)
  LOOP LOC:lLoopIndex = RECORDS(FileQueue) TO 1 BY -1
    GET(FileQueue,LOC:lLoopIndex)
    OPEN(FileQueue.FileLabel,42h)
    IF ERRORCODE()
      IF ERRORCODE() = BadKeyErr
        IF UPPER(FileQueue.FileLabel{PROP:Driver}) <> 'CLARION' AND UPPER(FileQueue.FileLabel{PROP:Driver}) <> 'BTRIEVE'
          CLOSE(FileQueue.FileLabel)
          OPEN(FileQueue.FileLabel,12h)
          IF UPPER(FileQueue.FileLabel{PROP:Driver}) = 'TOPSPEED'
            LOC:stStatusFullBuild = SEND(FileQueue.FileLabel,'FULLBUILD')
            Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=On')
          END
          BUILD(FileQueue.FileLabel)
          IF UPPER(FileQueue.FileLabel{PROP:Driver}) = 'TOPSPEED'
            IF CLIP(UPPER(LOC:stStatusFullBuild)) = 'ON'
              Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=On')
            ELSE
              Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=Off')
            END
          END
        ELSE
          LOC:byErrorStatus = 1
          IF LOC:lLoopIndex <> 1
            CYCLE
          END
        END
      END
    ELSE
      IF LOC:lLoopIndex = RECORDS(FileQueue)
        CLOSE(FileQueue.FileLabel)
        LOC:byReturnCode = 0
        DO ProcedureReturn
      END
    END
    CLOSE(FileQueue.FileLabel)
    OPEN(FileQueue.FileLabel,12h)
    IF ERRORCODE()
      CASE ERRORCODE()
      OF NoAccessErr
        IF MESSAGE('Converting can not be finished because data file|"' & CLIP(ERRORFILE()) & '"|is locked by other station.|Ask users to quit all programs, which uses file|"' & CLIP(ERRORFILE()) & '"|and try again.','Conversion Error',ICON:Question,BUTTON:RETRY+BUTTON:ABORT,BUTTON:RETRY) = BUTTON:RETRY
          LOC:lLoopIndex += 1
          CYCLE
        ELSE
          DO ProcedureReturn
        END
      OF InvalidFileErr
      OROF NoPathErr
      OROF NoFileErr
      OROF BadKeyErr
        CASE ERRORCODE()
        OF NoPathErr
        OROF NoFileErr      ; IF LOC:byErrorStatus = 0 THEN LOC:byErrorStatus = 2.
        OF InvalidFileErr   ; IF LOC:byErrorStatus = 2 THEN LOC:byErrorStatus = 0.
        END
        IF LOC:lLoopIndex = 1
          CASE LOC:byErrorStatus
          OF 1 ; ErrorBox('Keys must be rebuild|File: ' & ERRORFILE())
          OF 2 ; LOC:byReturnCode = 0
          ELSE ; ErrorBox('No match file structure|File: ' & ERRORFILE())
          END  
          DO ProcedureReturn
        ELSE
        END
      ELSE
        IF LOC:lLoopIndex = 1 THEN FileErrorBox(); DO ProcedureReturn.
      END
    ELSE
      CLOSE(FileQueue.FileLabel)
      LOOP LOC:lTempIndex = 1 TO RECORDS(TempFileQueue)
        GET(TempFileQueue,LOC:lTempIndex)
        CreateTempName(TempFileQueue.TempFileName,'m' & LOC:lTempIndex,TempFileQueue.TempFileLabel)
        CASE UPPER(TempFileQueue.TempFileLabel{PROP:Driver})
        ELSE
          RemoveFiles(TempFileQueue.TempFileLabel,TempFileQueue.TempFileName)
        END
        CREATE(TempFileQueue.TempFileLabel)
        IF ERRORCODE() THEN FileErrorBox(); DO ProcedureReturn.
        OPEN(TempFileQueue.TempFileLabel,12h)
        IF ERRORCODE() THEN FileErrorBox(); DO ProcedureReturn.
      END
      LOC:byReturnCode = Converting(LOC:lLoopIndex)
      LOOP LOC:lTempIndex = 1 TO RECORDS(TempFileQueue)
        GET(TempFileQueue,LOC:lTempIndex)
        CLOSE(TempFileQueue.TempFileLabel)
        CASE UPPER(TempFileQueue.TempFileLabel{PROP:Driver})
        ELSE
          RemoveFiles(TempFileQueue.TempFileLabel,TempFileQueue.TempFileName)
        END
      END
      DO ProcedureReturn
    END
  END
  LOC:byReturnCode = 0
  DO ProcedureReturn
!----------------------------------------------------------

AddQueueTempFile ROUTINE
  CLEAR(TempFileQueue)
  TempFileQueue.TempFileLabel &= FileQueue.FileLabel
  TempFileQueue.TempFileName  &= FileQueue.FileName
  ADD(TempFileQueue)
  EXIT
!---------------------------------------------------------------------

ProcedureReturn ROUTINE
  FREE(FileQueue)
  FREE(TempFileQueue)
  RETURN(LOC:byReturnCode)
!---------------------------------------------------------------------

ErrorBox PROCEDURE(STRING ErrorMessage)
  CODE
  MESSAGE('Conversion impossible!|' & CLIP(ErrorMessage),'Conversion Error',ICON:Exclamation,BUTTON:ABORT)
  RETURN
!---------------------------------------------------------------------

FileErrorBox PROCEDURE
  CODE
  ErrorBox('Error: ' & CLIP(CHOOSE(ERRORCODE() = 90,FILEERROR(),ERROR())) & '|Error File: ' & CHOOSE(LEN(ERRORFILE()) > 0,CLIP(ERRORFILE()),CLIP(MOD:stFileName)))
  RETURN
!---------------------------------------------------------------------

CreateTempName PROCEDURE(*STRING PAR:ProcessFileName,STRING PAR:Postfix,FILE PAR:File)

LOC:cstPath     CSTRING(260),AUTO
LOC:cstDrive    CSTRING(260),AUTO
LOC:cstDir      CSTRING(260),AUTO
LOC:cstName     CSTRING(260),AUTO
LOC:cstExt      CSTRING(260),AUTO
LOC:cstCopyName CSTRING(260),AUTO
LOC:cstCopyExt  CSTRING(260),AUTO

  CODE
  CASE UPPER(PAR:File{PROP:Driver})
  OF 'MSSQL'
  OROF 'SQLANYWHERE'
    LOC:cstPath = CLIP(PAR:ProcessFileName) & '_' & CLIP(PAR:Postfix)
  ELSE
    LOC:cstPath = CLIP(PAR:ProcessFileName)
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF LOC:cstName[1] = '!'
      LOC:cstCopyName = LOC:cstName
      LOC:cstCopyExt  = LOC:cstExt
      LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
      Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
      LOC:cstDir = LOC:cstDir & LOC:cstName & '$' & LOC:cstCopyName[2 : LEN(LOC:cstCopyName)] & '$' & CLIP(PAR:Postfix) & LOC:cstExt & '\'
      DC:FnMerge(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstCopyName,LOC:cstCopyExt)
    ELSE
      LOC:cstName = CLIP(LOC:cstName & '$' & PAR:Postfix)
      DC:FnMerge(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    END
  END
  PAR:ProcessFileName = LOC:cstPath
  RETURN
!---------------------------------------------------------------------

RemoveFiles PROCEDURE(FILE PAR:ProcessFileLabel,STRING PAR:ProcessFileName)

LOC:cstPath    CSTRING(260),AUTO
LOC:cstDrive   CSTRING(260),AUTO
LOC:cstDir     CSTRING(260),AUTO
LOC:cstName    CSTRING(260),AUTO
LOC:cstExt     CSTRING(260),AUTO
LOC:lLoopIndex LONG,AUTO
LOC:lLoopExt   LONG,AUTO
ListDir        QUEUE(FILE:Queue),PRE(LD)
               END

  CODE
  PrepareExtentionQueue(PAR:ProcessFileLabel)
  LOC:cstPath = CLIP(PAR:ProcessFileName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOOP LOC:lLoopExt = 1 TO RECORDS(ListExt)
    GET(ListExt,LOC:lLoopExt)
    FREE(ListDir)
    DIRECTORY(ListDir,LOC:cstDrive & LOC:cstDir & LOC:cstName & ListExt.Extention,ff_:ARCHIVE)
    LOOP LOC:lLoopIndex = RECORDS(ListDir) TO 1 BY -1
      GET(ListDir,LOC:lLoopIndex)
      IF CLIP(LD:ShortName) = '..' OR CLIP(LD:ShortName) = '.' THEN CYCLE.
      REMOVE(LOC:cstDrive & LOC:cstDir & LD:Name)
    END
  END
  FREE(ListExt)
  FREE(ListDir)
  RETURN
!---------------------------------------------------------------------

GetFileNameWithoutOwner PROCEDURE(STRING PAR:FileName)

LOC:lPosition LONG,AUTO

  CODE
  IF PAR:FileName
    LOC:lPosition = INSTRING('.',PAR:FileName,1,1)
    IF LOC:lPosition
      RETURN(PAR:FileName[LOC:lPosition + 1 : LEN(CLIP(PAR:FileName))])
    ELSE
      RETURN(PAR:FileName)
    END
  ELSE
    RETURN('')
  END
!---------------------------------------------------------------------

RenameFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:TargetName)

LOC:lLoopIndex       LONG,AUTO
LOC:cstPath          CSTRING(260),AUTO
LOC:cstDrive         CSTRING(260),AUTO
LOC:cstDir           CSTRING(260),AUTO
LOC:cstName          CSTRING(260),AUTO
LOC:cstExt           CSTRING(260),AUTO
ListDir              QUEUE(FILE:Queue),PRE(LD)
                     END
LOC:cstTempName      CSTRING(260),AUTO
LOC:stTempSourceName STRING(260),AUTO
LOC:stTempTargetName STRING(260),AUTO
LOC:lErrorCode       LONG,AUTO
LOC:lLoopExt         LONG,AUTO
loc:extcount         LONG,AUTO
loc:Result           CSTRING(34),AUTO

  CODE
  LOC:cstPath = CLIP(PAR:SourceFile{PROP:Name})
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOC:cstTempName = LOC:cstDrive & LOC:cstDir & LOC:cstName
  LOC:stTempSourceName = LOC:cstDrive & LOC:cstDir
  LOC:cstPath = CLIP(PAR:TargetName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    RemoveFiles(PAR:SourceFile,LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1])
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOC:stTempTargetName = LOC:cstDrive & LOC:cstDir & LOC:cstName
  PrepareExtentionQueue(PAR:SourceFile)
  LOOP LOC:lLoopExt = 1 TO RECORDS(ListExt)
    GET(ListExt,LOC:lLoopExt)
    FREE(ListDir)
    DIRECTORY(ListDir,LOC:cstTempName & ListExt.Extention,ff_:ARCHIVE)
    LOOP LOC:lLoopIndex = RECORDS(ListDir) TO 1 BY -1
      GET(ListDir,LOC:lLoopIndex)
      IF CLIP(LD:ShortName) = '..' OR CLIP(LD:ShortName) = '.' THEN CYCLE.
      LOC:cstPath = CLIP(LOC:stTempSourceName) & CLIP(LD:Name)
      Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
      RENAME(CLIP(LOC:stTempSourceName) & LD:Name,CLIP(LOC:stTempTargetName) & LOC:cstExt)
      !Look for and rename extensions
      Loop loc:extcount = 1 to 255
          LtoA(loc:extcount,loc:Result,16)
          If Len(loc:Result) = 1
              loc:Result = '0' & loc:Result
          End ! If Len(loc:Result) = 1
          loc:Result = UPPER(loc:Result)

          If Exists(CLIP(LOC:stTempSourceName) & Clip(loc:cstName) & '.^' & loc:Result)
              Rename(CLIP(LOC:stTempSourceName) & Clip(loc:cstName) & '.^' & loc:Result,Clip(loc:stTempTargetName) & '.^' & loc:Result)
          Else
              Break
          End ! If Exists(CLip(loc:stTempSourceName) & '.^' & loc:Result)
      End ! Loop loc:extcount = 1 to 255

      IF ERRORCODE() 
        LOC:lErrorCode = ERRORCODE()
        FREE(ListExt)
        FREE(ListDir)
        DC:SetError(LOC:lErrorCode)
        RETURN
      END
    END
  END
  FREE(ListExt)
  FREE(ListDir)
  RETURN
!---------------------------------------------------------------------

PrepareExtentionQueue PROCEDURE(FILE PAR:ProcessFileLabel)

LOC:lLoopIndex LONG,AUTO
LOC:cstPath    CSTRING(260),AUTO
LOC:cstDrive   CSTRING(260),AUTO
LOC:cstDir     CSTRING(260),AUTO
LOC:cstName    CSTRING(260),AUTO
LOC:cstExt     CSTRING(260),AUTO

  CODE
  LOC:cstPath = CLIP(PAR:ProcessFileLabel{PROP:Name})
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  FREE(ListExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF ~LOC:cstExt
      ListExt.Extention = '.tps'
    ELSE
      ListExt.Extention = LOC:cstExt
    END
    ADD(ListExt)
  ELSE
    IF ~LOC:cstExt
      CASE UPPER(SUB(PAR:ProcessFileLabel{PROP:Driver},1,3))
      OF   'CLA'
      OROF   'BTR'
        ListExt.Extention = '.dat'
      OF   'TOP'
        ListExt.Extention = '.tps'
      OF   'DBA'
      OROF 'CLI'
      OROF 'FOX'
        ListExt.Extention = '.dbf'
      END
    ELSE
      ListExt.Extention = LOC:cstExt
    END
    ADD(ListExt)
    CASE UPPER(SUB(PAR:ProcessFileLabel{PROP:Driver},1,3))
    OF 'CLA'
      ListExt.Extention = '.mem'
      ADD(ListExt)
      LOOP LOC:lLoopIndex = 1 TO 99
        ListExt.Extention = '.k' & FORMAT(LOC:lLoopIndex,@N02)
        ADD(ListExt)
        ListExt.Extention = '.i' & FORMAT(LOC:lLoopIndex,@N02)
        ADD(ListExt)
      END
    OF 'BTR'
      ListExt.Extention = '.dbt'
      ADD(ListExt)
      ListExt.Extention = '.ndx'
      ADD(ListExt)
    OF 'DBA'
      ListExt.Extention = '.ndx'
      ADD(ListExt)
      ListExt.Extention = '.mdx'
      ADD(ListExt)
      ListExt.Extention = '.dbt'
      ADD(ListExt)
    OF 'CLI'
      ListExt.Extention = '.ntx'
      ADD(ListExt)
      ListExt.Extention = '.cdx'
      ADD(ListExt)
      ListExt.Extention = '.dbt'
      ADD(ListExt)
    OF 'FOX'
      ListExt.Extention = '.inx'
      ADD(ListExt)
      ListExt.Extention = '.cdx'
      ADD(ListExt)
      ListExt.Extention = '.fbt'
      ADD(ListExt)
    END
  END
  RETURN
!---------------------------------------------------------------------

PrepareMultiTableFileName PROCEDURE(FILE PAR:File,STRING PAR:SourceName,*STRING PAR:TargetName)

LOC:cstPath      CSTRING(260),AUTO
LOC:cstDrive     CSTRING(260),AUTO
LOC:cstDir       CSTRING(260),AUTO
LOC:cstName      CSTRING(260),AUTO
LOC:cstExt       CSTRING(260),AUTO
LOC:byMultyTable BYTE(False)

  CODE
  CLEAR(PAR:TargetName)
  LOC:cstPath = CLIP(PAR:SourceName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF UPPER(PAR:File{PROP:Driver}) = 'TOPSPEED' AND LOC:cstName[1] = '!'
    PAR:TargetName = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF ~LOC:cstExt
      PAR:TargetName = CLIP(PAR:TargetName) & '.tps'
    END
    LOC:byMultyTable = True
  END
  RETURN(LOC:byMultyTable)
!---------------------------------------------------------------------

PrepareDestFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:SourceName,FILE PAR:TargetFile,STRING PAR:TargetName,STRING PAR:OriginalTargetName)

LOC:stTempSourceFile STRING(260),AUTO
LOC:stTempTargetFile STRING(260),AUTO
LOC:byMultyTable     BYTE(False)

  CODE
  IF PrepareMultiTableFileName(PAR:SourceFile,PAR:SourceName,LOC:stTempSourceFile)
    IF PrepareMultiTableFileName(PAR:TargetFile,PAR:TargetName,LOC:stTempTargetFile)
      SETCURSOR(CURSOR:Wait)
      COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
      SETCURSOR
      IF ERRORCODE()
        DC:SetErrorFile(PAR:SourceFile)
        RETURN(1)
      END
      PAR:SourceFile{PROP:Name} = PAR:TargetName
      REMOVE(PAR:SourceFile)
      IF ERRORCODE()
        DC:SetErrorFile(PAR:SourceFile)
        RETURN(1)
      END
      PAR:SourceFile{PROP:Name} = PAR:SourceName
      LOC:byMultyTable = True
    END
  ELSE
    IF PrepareMultiTableFileName(PAR:TargetFile,PAR:TargetName,LOC:stTempTargetFile)
      IF PrepareMultiTableFileName(PAR:TargetFile,PAR:OriginalTargetName,LOC:stTempSourceFile)
        IF EXISTS(LOC:stTempSourceFile)
          SETCURSOR(CURSOR:Wait)
          COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
          SETCURSOR
          IF ERRORCODE()
            DC:SetErrorFile(PAR:SourceFile)
            RETURN(1)
          END
        END
        LOC:byMultyTable = True
      END
    END
  END
  IF LOC:byMultyTable = False
    RemoveFiles(PAR:TargetFile,PAR:TargetName)
  END
  RETURN(0)
!---------------------------------------------------------------------

RenameDestFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:TargetName,STRING PAR:OriginalTargetName)

LOC:stTempSourceFile STRING(260),AUTO
LOC:stTempTargetFile STRING(260),AUTO
LOC:byMultyTable     BYTE(False)
ROU:lErrorCode       LONG,AUTO

  CODE
  IF ~PrepareMultiTableFileName(PAR:SourceFile,PAR:OriginalTargetName,LOC:stTempSourceFile)
    IF PrepareMultiTableFileName(PAR:SourceFile,PAR:TargetName,LOC:stTempTargetFile)
      IF PrepareMultiTableFileName(PAR:SourceFile,PAR:SourceFile{PROP:Name},LOC:stTempSourceFile)
        SETCURSOR(CURSOR:Wait)
        COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
        SETCURSOR
        IF ERRORCODE()
          DC:SetErrorFile(PAR:SourceFile)
          RETURN(1)
        END
        REMOVE(PAR:SourceFile)
        IF ERRORCODE()
          ROU:lErrorCode = ERRORCODE()
          RemoveFiles(PAR:SourceFile,LOC:stTempTargetFile)
          DC:SetError(ROU:lErrorCode)
          DC:SetErrorFile(PAR:SourceFile)
          RETURN(1)
        END
        LOC:byMultyTable = True
      END
    END
  END
  IF LOC:byMultyTable = False
    RenameFile(PAR:SourceFile,PAR:TargetName)
    IF ERRORCODE()
      RETURN(1)
    END
  END
  RETURN(0)
!---------------------------------------------------------------------

Converting FUNCTION(LONG Version)

LocalRequest         LONG
OriginalRequest      LONG
LocalResponse        LONG
FilesOpened          LONG
WindowOpened         LONG
WindowInitialized    LONG
ForceRefresh         LONG
ReturnCode           BYTE
LOCSHELF_autonumber  LONG
LOC:byTPSDriver         BYTE,AUTO
LOC:byDestSQLDriver     BYTE,AUTO
LOC:byDestMSSQLDriver   BYTE,AUTO
LOC:byDestASADriver     BYTE,AUTO
LOC:bySourceSQLDriver   BYTE,AUTO
LOC:bySourceMSSQLDriver BYTE,AUTO
LOC:bySourceASADriver   BYTE,AUTO
LOC:byTransactionActive BYTE,AUTO
LOC:rfSourceFileLabel   &FILE
LOC:stSourceFileName    STRING(260),AUTO
LOC:rfDestFileLabel     &FILE
LOC:stDestFileName      STRING(260),AUTO
LOC:stTargetFileName    STRING(260),AUTO
RecordProcessed      LONG
RecordTotal          LONG
InfoString           STRING(50)
DummyFileName1       STRING(260)
DummyFileName2       STRING(260)
SaveRecordProcessed  LONG
SavePercent          LONG
SaveFillBar          LONG

SPBBorderControl     SIGNED
SPBFillControl       SIGNED
SPBPercentControl    SIGNED

window               WINDOW('File Conversion'),AT(,,355,52),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       PANEL,AT(4,4,348,44),USE(?Panel1),BEVEL(-1)
                       PROGRESS,USE(RecordProcessed),AT(12,12,336,12),RANGE(0,100)
                       STRING(@s50),AT(12,32,336,10),USE(InfoString),CENTER,FONT(,,COLOR:Navy,,CHARSET:ANSI)
                     END
  CODE
  PUSHBIND
  SETCURSOR
  ReturnCode = 1
  LocalRequest = GlobalRequest
  OriginalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  ForceRefresh = False
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  IF KEYCODE() = MouseRight
    SETKEYCODE(0)
  END
  DO PrepareProcedure
  ACCEPT
    CASE EVENT()
    OF EVENT:OpenWindow
      IF NOT WindowInitialized
        DO InitializeWindow
        WindowInitialized = True
      END
      SELECT(?Panel1)
    OF EVENT:GainFocus
      ForceRefresh = True
      IF NOT WindowInitialized
        DO InitializeWindow
        WindowInitialized = True
      ELSE
        DO RefreshWindow
      END
    OF Event:Rejected
      BEEP
      DISPLAY(?)
      SELECT(?)
    END
  END
  DO ProcedureReturn
!---------------------------------------------------------------------------
PrepareProcedure ROUTINE
  OMIT('DataConverter: End Comment open procedure files')
  CheckOpen(JOBS,1)
  BIND(job:RECORD)
  FilesOpened = True
  ! DataConverter: End Comment open procedure files
  OPEN(window)
  WindowOpened=True
  window{PROP:Text} = CLIP(window{PROP:Text}) & ' - ' & MOD:stFileName
  DO ConversionProcess
  DO ProcedureReturn
!---------------------------------------------------------------------------
ProcedureReturn ROUTINE
!|
!| This routine provides a common procedure exit point for all template
!| generated procedures.
!|
!| First, all of the files opened by this procedure are closed.
!|
!| Next, if it was opened by this procedure, the window is closed.
!|
!| Next, GlobalResponse is assigned a value to signal the calling procedure
!| what happened in this procedure.
!|
!| Next, we replace the BINDings that were in place when the procedure initialized
!| (and saved with PUSHBIND) using POPBIND.
!|
!| Finally, we return to the calling procedure, passing ReturnCode back.
!|
  IF FilesOpened
    OMIT('DataConverter: End Comment close procedure files')
    ! DataConverter: End Comment close procedure files
  END
  IF WindowOpened
    CLOSE(window)
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  POPBIND
  RETURN(ReturnCode)
!---------------------------------------------------------------------------
InitializeWindow ROUTINE
!|
!| This routine is used to prepare any control templates for use. It should be called once
!| per procedure.
!|
  DO RefreshWindow
!---------------------------------------------------------------------------
RefreshWindow ROUTINE
!|
!| This routine is used to keep all displays and control templates current.
!|
  IF window{Prop:AcceptAll} THEN EXIT.
  DISPLAY()
  ForceRefresh = False
!---------------------------------------------------------------------------
SyncWindow ROUTINE
!|
!| This routine is used to insure that any records pointed to in control
!| templates are fetched before any procedures are called via buttons or menu
!| options.
!|
!---------------------------------------------------------------------------

ConversionProcess ROUTINE
  DATA
ROU:fCurrentFile      &FILE
ROU:lConvertedRecNum  LONG,AUTO
ROU:lRecordsThisCycle LONG,AUTO
ROU:lDim1             LONG,AUTO
ROU:lDim2             LONG,AUTO
ROU:lDim3             LONG,AUTO
ROU:lDim4             LONG,AUTO
ROU:byError           BYTE,AUTO
  CODE
  GET(FileQueue,Version)
  LOC:rfSourceFileLabel &= FileQueue.FileLabel
  LOC:stSourceFileName   = FileQueue.FileName
  GET(FileQueue,RECORDS(FileQueue))
  LOC:rfDestFileLabel &= FileQueue.FileLabel
  LOC:stDestFileName   = FileQueue.FileName
  LOC:stTargetFileName = LOC:stDestFileName
  LOC:byTransactionActive = False
  LOC:byTPSDriver         = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'TOPSPEED',True,False)
  LOC:bySourceMSSQLDriver = CHOOSE(UPPER(LOC:rfSourceFileLabel{PROP:Driver}) = 'MSSQL',True,False)
  LOC:bySourceASADriver   = CHOOSE(UPPER(LOC:rfSourceFileLabel{PROP:Driver}) = 'SQLANYWHERE',True,False)
  LOC:bySourceSQLDriver   = LOC:bySourceMSSQLDriver + LOC:bySourceASADriver
  LOC:byDestMSSQLDriver   = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'MSSQL',True,False)
  LOC:byDestASADriver     = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'SQLANYWHERE',True,False)
  LOC:byDestSQLDriver     = LOC:byDestMSSQLDriver + LOC:byDestASADriver
  CreateTempName(LOC:stDestFileName,'tmp',LOC:rfDestFileLabel)
  IF LOC:byDestSQLDriver
    DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
  ELSE
    IF PrepareMultiTableFileName(LOC:rfDestFileLabel,LOC:stDestFileName,DummyFileName1) AND |
       (PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:stSourceFileName,DummyFileName1) OR |
        (PrepareMultiTableFileName(LOC:rfDestFileLabel,LOC:stTargetFileName,DummyFileName2) AND EXISTS(DummyFileName2)) |
       )
      InfoString = 'File copying...'
      DISPLAY
    END
    IF PrepareDestFile(LOC:rfSourceFileLabel,LOC:stSourceFileName,LOC:rfDestFileLabel,LOC:stDestFileName,LOC:stTargetFileName) THEN DO PreReturn; EXIT.
  END
  OPEN(LOC:rfSourceFileLabel,12h)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
  CREATE(LOC:rfDestFileLabel)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
  OPEN(LOC:rfDestFileLabel,12h)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  IF LOC:byTPSDriver 
    ROU:lConvertedRecNum = 0
  ELSE
    IF ~LOC:byDestSQLDriver THEN STREAM(LOC:rfDestFileLabel).
  END
  ?RecordProcessed{PROP:Use} = RecordProcessed
  ?InfoString{PROP:Use} = InfoString
  DO InitalizeSolidProgressBar
  ?RecordProcessed{PROP:RangeHigh} = RECORDS(LOC:rfSourceFileLabel)
  RecordProcessed = 0
  RecordTotal     = RECORDS(LOC:rfSourceFileLabel)
  SaveRecordProcessed = 0
  SavePercent         = 0
  SaveFillBar         = 0
  SETCURSOR(CURSOR:Wait)
  DISPLAY
  SET(LOC:rfSourceFileLabel)
  IF LOC:byTPSDriver
    LOGOUT(1,LOC:rfDestFileLabel)
    IF ERRORCODE() THEN DO PreReturn; EXIT.
    LOC:byTransactionActive = True
  END
  ROU:byError = 0
  window{PROP:Timer} = 1
  ACCEPT
    CASE EVENT()
    OF EVENT:GainFocus
      DISPLAY
    OF EVENT:CloseWindow
      CYCLE
    OF EVENT:Completed
      BREAK
    OF EVENT:Timer
      ROU:lRecordsThisCycle = 0
      LOOP WHILE ROU:lRecordsThisCycle < 25
        NEXT(LOC:rfSourceFileLabel)
        IF ERRORCODE()
          POST(EVENT:Completed)
          BREAK
        END
        ROU:fCurrentFile &= LOC:rfSourceFileLabel
        IF ROU:fCurrentFile &= WAYBCONFVer1
          ! Field "AccountNumber" added
          ! Field "ConfirmationReceived" added
          ! Key/Index "KeyAccountGenerateDateTime" changed (KeyGenerateDateTime KEY(WAC:GenerateDate,WAC:GenerateTime),DUP,NOCASE -> KeyAccountGenerateDateTime KEY(WAC:AccountNumber,WAC:GenerateDate,WAC:GenerateTime),DUP,NOCASE)
          ! Key/Index "KeyAccountConfirmDateTime" changed (KeyConfirmDateTime KEY(WAC:ConfirmationSent,WAC:ConfirmDate,WAC:ConfirmTime),DUP,NOCASE -> KeyAccountConfirmDateTime KEY(WAC:AccountNumber,WAC:ConfirmationSent,WAC:ConfirmDate,WAC:ConfirmTime),DUP,NOCASE)
          ! Key/Index "KeyAccountDeliveredDateTime" changed (KeyDeliveredDateTime KEY(WAC:Delivered,WAC:DeliverDate,WAC:DeliverTime),DUP,NOCASE -> KeyAccountDeliveredDateTime KEY(WAC:AccountNumber,WAC:Delivered,WAC:DeliverDate,WAC:DeliverTime),DUP,NOCASE)
          ! Key/Index "KeyAccountReceivedDateTime" changed (KeyReceivedDateTime KEY(WAC:Received,WAC:ReceiveDate,WAC:ReceiveTime),DUP,NOCASE -> KeyAccountReceivedDateTime KEY(WAC:AccountNumber,WAC:Received,WAC:ReceiveDate,WAC:ReceiveTime),DUP,NOCASE)
          ! Key/Index "KeyConfirmOnly" added
          CLEAR(WAYBCONFVer2:RECORD)
          WAYBCONFVer2:RECORD :=: WAYBCONFVer1:RECORD
        END
        IF ROU:fCurrentFile &= SOAPERRSVer1
          ! Field "Subject" added
          ! Field "Body" added
          CLEAR(SOAPERRSVer2:RECORD)
          SOAPERRSVer2:RECORD :=: SOAPERRSVer1:RECORD
        END
        IF ROU:fCurrentFile &= JOBSE3Ver1
          ! Field "Exchange_IntLocation" added
          CLEAR(JOBSE3Ver2:RECORD)
          JOBSE3Ver2:RECORD :=: JOBSE3Ver1:RECORD
          ROU:fCurrentFile &= JOBSE3Ver2
        END
        IF ROU:fCurrentFile &= JOBSE3Ver2
          ! Field "SpecificNeeds" added
          ! Field "SN_Software_Installed" added
          ! Field "FaultCode21" added
          ! Field "FaultCode22" added
          ! Field "FaultCode23" added
          ! Field "FaultCode24" added
          ! Field "FaultCode25" added
          ! Field "FaultCode26" added
          ! Field "FaultCode27" added
          ! Field "FaultCode28" added
          ! Field "FaultCode29" added
          ! Field "FaultCode30" added
          CLEAR(JOBSE3Ver3:RECORD)
          JOBSE3Ver3:RECORD :=: JOBSE3Ver2:RECORD
        END
        IF ROU:fCurrentFile &= STOAUUSEVer1
          ! Field "StockPartNumber" added
          ! Key/Index "KeyTypeNoPart" added
          CLEAR(STOAUUSEVer2:RECORD)
          STOAUUSEVer2:RECORD :=: STOAUUSEVer1:RECORD
        END
        IF ROU:fCurrentFile &= PREJOBVer1
          ! Key/Index "KeyPortalRef" added
          CLEAR(PREJOBVer2:RECORD)
          PREJOBVer2:RECORD :=: PREJOBVer1:RECORD
          ROU:fCurrentFile &= PREJOBVer2
        END
        IF ROU:fCurrentFile &= PREJOBVer2
          ! Field "ChargeType" added
          CLEAR(PREJOBVer3:RECORD)
          PREJOBVer3:RECORD :=: PREJOBVer2:RECORD
        END
        IF ROU:fCurrentFile &= TRADEAC2Ver1
          ! Field "Pre_Book_Region" added
          CLEAR(TRADEAC2Ver2:RECORD)
          TRADEAC2Ver2:RECORD :=: TRADEAC2Ver1:RECORD
          ROU:fCurrentFile &= TRADEAC2Ver2
        END
        IF ROU:fCurrentFile &= TRADEAC2Ver2
          ! Key/Index "KeyPreBookRegion" added
          CLEAR(TRADEAC2Ver3:RECORD)
          TRADEAC2Ver3:RECORD :=: TRADEAC2Ver2:RECORD
          ROU:fCurrentFile &= TRADEAC2Ver3
        END
        IF ROU:fCurrentFile &= TRADEAC2Ver3
          ! Field "SBOnlineDespatch" added
          ! Field "SBOnlineStock" added
          ! Field "SBOnlineQAEtc" added
          ! Field "SBOnlineWarrClaims" added
          ! Field "SBOnlineWaybills" added
          ! Field "SBOnlineTradeAccs" added
          ! Field "SBOnlineEngineer" added
          ! Field "SBOnlineAudits" added
          ! Field "SBOnlineUsers" added
          ! Field "SBOnlineLoansExch" added
          ! Field "SBOnlineBouncers" added
          ! Field "SBOnlineReports" added
          ! Field "SBOnlineSpare1" added
          ! Field "SBOnlineSpare2" added
          ! Field "SBOnlineSpare3" added
          CLEAR(TRADEAC2Ver4:RECORD)
          TRADEAC2Ver4:RECORD :=: TRADEAC2Ver3:RECORD
        END
        IF ROU:fCurrentFile &= SMSRECVDVer1
          ! Field "SMSType" added
          CLEAR(SMSRECVDVer2:RECORD)
          SMSRECVDVer2:RECORD :=: SMSRECVDVer1:RECORD
          ROU:fCurrentFile &= SMSRECVDVer2
        END
        IF ROU:fCurrentFile &= SMSRECVDVer2
          ! Field "AccountNumber" added
          ! Key/Index "KeyAccount_Date_Time" added
          CLEAR(SMSRECVDVer3:RECORD)
          SMSRECVDVer3:RECORD :=: SMSRECVDVer2:RECORD
          ROU:fCurrentFile &= SMSRECVDVer3
        END
        IF ROU:fCurrentFile &= SMSRECVDVer3
          ! File Label changed "SMSRECEI" to "SMSRECVD"
          CLEAR(SMSRECVDVer4:RECORD)
          SMSRECVDVer4:RECORD :=: SMSRECVDVer3:RECORD
          ROU:fCurrentFile &= SMSRECVDVer4
        END
        IF ROU:fCurrentFile &= SMSRECVDVer4
          ! Attribute "NAME" changed (<Empty> -> 'SMSRECVD.DAT')
          CLEAR(SMSRECVDVer5:RECORD)
          SMSRECVDVer5:RECORD :=: SMSRECVDVer4:RECORD
        END
        IF ROU:fCurrentFile &= GRNOTESVer1
          ! Field "Uncaptured" added
          CLEAR(GRNOTESVer2:RECORD)
          GRNOTESVer2:RECORD :=: GRNOTESVer1:RECORD
          ROU:fCurrentFile &= GRNOTESVer2
        END
        IF ROU:fCurrentFile &= GRNOTESVer2
          ! Field "EVO_Status" added
          ! Key/Index "KeyEVO_Status" added
          CLEAR(GRNOTESVer3:RECORD)
          GRNOTESVer3:RECORD :=: GRNOTESVer2:RECORD
        END
        IF ROU:fCurrentFile &= EXCHAUIVer1
          ! Key/Index "Locate_IMEI_Key" changed (Locate_IMEI_Key KEY(eau:Audit_Number,eau:Site_Location,eau:IMEI_Number),DUP,NOCASE,OPT -> Locate_IMEI_Key KEY(eau:Audit_Number,eau:Site_Location,eau:IMEI_Number),DUP,NOCASE)
          CLEAR(EXCHAUIVer2:RECORD)
          EXCHAUIVer2:RECORD :=: EXCHAUIVer1:RECORD
        END
        IF ROU:fCurrentFile &= RTNAWAITVer1
          ! Field "UserCode" added
          CLEAR(RTNAWAITVer2:RECORD)
          RTNAWAITVer2:RECORD :=: RTNAWAITVer1:RECORD
          ROU:fCurrentFile &= RTNAWAITVer2
        END
        IF ROU:fCurrentFile &= RTNAWAITVer2
          ! Field "Quantity" added
          ! Field "ExchangeUnit" changed label to "ExchangeOrder"
          ! Key/Index "ArcProExcPartKey" changed (ArcProExcPartKey KEY(rta:Archive,rta:Processed,rta:ExchangeUnit,rta:PartNumber),DUP,NOCASE -> ArcProExcPartKey KEY(rta:Archive,rta:Processed,rta:ExchangeOrder,rta:PartNumber),DUP,NOCASE)
          ! Key/Index "ArcProExcDescKey" changed (ArcProExcDescKey KEY(rta:Archive,rta:Processed,rta:ExchangeUnit,rta:Description),DUP,NOCASE -> ArcProExcDescKey KEY(rta:Archive,rta:Processed,rta:ExchangeOrder,rta:Description),DUP,NOCASE)
          CLEAR(RTNAWAITVer3:RECORD)
          RTNAWAITVer3:RECORD :=: RTNAWAITVer2:RECORD
          RTNAWAITVer3:ExchangeOrder = RTNAWAITVer2:ExchangeUnit
          ROU:fCurrentFile &= RTNAWAITVer3
        END
        IF ROU:fCurrentFile &= RTNAWAITVer3
          ! Field "NewRefNumber" added
          CLEAR(RTNAWAITVer4:RECORD)
          RTNAWAITVer4:RECORD :=: RTNAWAITVer3:RECORD
          ROU:fCurrentFile &= RTNAWAITVer4
        END
        IF ROU:fCurrentFile &= RTNAWAITVer4
          ! Key/Index "ArcProCNRKey" added
          CLEAR(RTNAWAITVer5:RECORD)
          RTNAWAITVer5:RECORD :=: RTNAWAITVer4:RECORD
          ROU:fCurrentFile &= RTNAWAITVer5
        END
        IF ROU:fCurrentFile &= RTNAWAITVer5
          ! Field "Location" added
          CLEAR(RTNAWAITVer6:RECORD)
          RTNAWAITVer6:RECORD :=: RTNAWAITVer5:RECORD
          ROU:fCurrentFile &= RTNAWAITVer6
        END
        IF ROU:fCurrentFile &= RTNAWAITVer6
          ! Field "GRNNumber" added
          ! Key/Index "GRNNumberKey" added
          CLEAR(RTNAWAITVer7:RECORD)
          RTNAWAITVer7:RECORD :=: RTNAWAITVer6:RECORD
        END
        IF ROU:fCurrentFile &= STMASAUDVer1
          ! Field "CompleteType" added
          CLEAR(STMASAUDVer2:RECORD)
          STMASAUDVer2:RECORD :=: STMASAUDVer1:RECORD
        END
        IF ROU:fCurrentFile &= ORDITEMSVer1
          ! Field "OrderDate" added
          ! Field "OrderTime" added
          CLEAR(ORDITEMSVer2:RECORD)
          ORDITEMSVer2:RECORD :=: ORDITEMSVer1:RECORD
        END
        IF ROU:fCurrentFile &= LOCATLOGVer1
          ! Field "Exchange" added
          CLEAR(LOCATLOGVer2:RECORD)
          LOCATLOGVer2:RECORD :=: LOCATLOGVer1:RECORD
        END
        IF ROU:fCurrentFile &= DEFAULT2Ver1
          ! Field "GlobalPrintText" added
          CLEAR(DEFAULT2Ver2:RECORD)
          DEFAULT2Ver2:RECORD :=: DEFAULT2Ver1:RECORD
          ROU:fCurrentFile &= DEFAULT2Ver2
        END
        IF ROU:fCurrentFile &= DEFAULT2Ver2
          ! Field "DefaultFromEmail" added
          CLEAR(DEFAULT2Ver3:RECORD)
          DEFAULT2Ver3:RECORD :=: DEFAULT2Ver2:RECORD
          ROU:fCurrentFile &= DEFAULT2Ver3
        END
        IF ROU:fCurrentFile &= DEFAULT2Ver3
          ! Field "PLE" added
          CLEAR(DEFAULT2Ver4:RECORD)
          DEFAULT2Ver4:RECORD :=: DEFAULT2Ver3:RECORD
          ROU:fCurrentFile &= DEFAULT2Ver4
        END
        IF ROU:fCurrentFile &= DEFAULT2Ver4
          ! Field "J_Collection_Rate" added
          CLEAR(DEFAULT2Ver5:RECORD)
          DEFAULT2Ver5:RECORD :=: DEFAULT2Ver4:RECORD
        END
        IF ROU:fCurrentFile &= RETSALESVer1
          ! Field "ExchangeOrder" added
          CLEAR(RETSALESVer2:RECORD)
          RETSALESVer2:RECORD :=: RETSALESVer1:RECORD
          ROU:fCurrentFile &= RETSALESVer2
        END
        IF ROU:fCurrentFile &= RETSALESVer2
          ! Field "LoanOrder" added
          CLEAR(RETSALESVer3:RECORD)
          RETSALESVer3:RECORD :=: RETSALESVer2:RECORD
        END
        IF ROU:fCurrentFile &= LOAORDRVer1
          ! Field "DateReceived" added
          ! Field "OrderNumber" added
          ! Field "Received" added
          ! Field "LoanRefNumber" added
          ! Key/Index "OrderNumberKey" added
          CLEAR(LOAORDRVer2:RECORD)
          LOAORDRVer2:RECORD :=: LOAORDRVer1:RECORD
        END
        IF ROU:fCurrentFile &= WIPAMFVer1
          ! Field "Ignore_IMEI" added
          ! Field "Ignore_Job_Number" added
          ! Field "Date_Completed" added
          ! Field "Time_Completed" added
          CLEAR(WIPAMFVer2:RECORD)
          WIPAMFVer2:RECORD :=: WIPAMFVer1:RECORD
        END
        IF ROU:fCurrentFile &= TEAMSVer1
          ! Field "TradeAccount_Number" added
          ! Key/Index "KeyTraceAccount_number" added
          CLEAR(TEAMSVer2:RECORD)
          TEAMSVer2:RECORD :=: TEAMSVer1:RECORD
          ROU:fCurrentFile &= TEAMSVer2
        END
        IF ROU:fCurrentFile &= TEAMSVer2
          ! Field "Associated" added
          CLEAR(TEAMSVer3:RECORD)
          TEAMSVer3:RECORD :=: TEAMSVer2:RECORD
        END
        IF ROU:fCurrentFile &= WIPAUIVer1
          ! Field "IsExchange" added
          CLEAR(WIPAUIVer2:RECORD)
          WIPAUIVer2:RECORD :=: WIPAUIVer1:RECORD
        END
        IF ROU:fCurrentFile &= EXCHORDRVer1
          ! Field "DateReceived" added
          ! Field "OrderNumber" added
          ! Field "Received" added
          ! Key/Index "OrderNumberKey" added
          CLEAR(EXCHORDRVer2:RECORD)
          EXCHORDRVer2:RECORD :=: EXCHORDRVer1:RECORD
          ROU:fCurrentFile &= EXCHORDRVer2
        END
        IF ROU:fCurrentFile &= EXCHORDRVer2
          ! Key/Index "OrderNumberOnlyKey" added
          CLEAR(EXCHORDRVer3:RECORD)
          EXCHORDRVer3:RECORD :=: EXCHORDRVer2:RECORD
          ROU:fCurrentFile &= EXCHORDRVer3
        END
        IF ROU:fCurrentFile &= EXCHORDRVer3
          ! Attribute "NAME" changed (<Empty> -> 'EXCHORDR.DAT')
          CLEAR(EXCHORDRVer4:RECORD)
          EXCHORDRVer4:RECORD :=: EXCHORDRVer3:RECORD
          ROU:fCurrentFile &= EXCHORDRVer4
        END
        IF ROU:fCurrentFile &= EXCHORDRVer4
          ! Field "ExchangeRefNumber" added
          ! Key/Index "LocationDateReceivedKey" added
          CLEAR(EXCHORDRVer5:RECORD)
          EXCHORDRVer5:RECORD :=: EXCHORDRVer4:RECORD
          ROU:fCurrentFile &= EXCHORDRVer5
        END
        IF ROU:fCurrentFile &= EXCHORDRVer5
          ! Field "TimeCreated" added
          CLEAR(EXCHORDRVer6:RECORD)
          EXCHORDRVer6:RECORD :=: EXCHORDRVer5:RECORD
        END
        IF ROU:fCurrentFile &= CONTHISTVer1
          ! Field "SN_StickyNote" added
          ! Field "SN_Completed" added
          ! Field "SN_EngAlloc" added
          ! Field "SN_EngUpdate" added
          ! Field "SN_CustService" added
          ! Field "SN_WaybillConf" added
          ! Field "SN_Despatch" added
          ! Key/Index "KeyRefSticky" added
          CLEAR(CONTHISTVer2:RECORD)
          CONTHISTVer2:RECORD :=: CONTHISTVer1:RECORD
        END
        IF ROU:fCurrentFile &= STANTEXTVer1
          ! Field "DummyField" removed
          ! Field "Text" changed (Text STRING(500) -> Text STRING(1000))
          CLEAR(STANTEXTVer2:RECORD)
          STANTEXTVer2:RECORD :=: STANTEXTVer1:RECORD
        END
        IF ROU:fCurrentFile &= JOBSWARRVer1
          ! Field "DateRejected" added
          ! Field "DateFinalRejection" added
          ! Key/Index "RejectedBranchKey" added
          CLEAR(JOBSWARRVer2:RECORD)
          JOBSWARRVer2:RECORD :=: JOBSWARRVer1:RECORD
          ROU:fCurrentFile &= JOBSWARRVer2
        END
        IF ROU:fCurrentFile &= JOBSWARRVer2
          ! Field "Orig_Sub_Date" added
          CLEAR(JOBSWARRVer3:RECORD)
          JOBSWARRVer3:RECORD :=: JOBSWARRVer2:RECORD
        END
        IF ROU:fCurrentFile &= CURRENCYVer1
          ! Field "CorrelationCode" added
          CLEAR(CURRENCYVer2:RECORD)
          CURRENCYVer2:RECORD :=: CURRENCYVer1:RECORD
          ROU:fCurrentFile &= CURRENCYVer2
        END
        IF ROU:fCurrentFile &= CURRENCYVer2
          ! Key/Index "CorrelationCodeKey" added
          CLEAR(CURRENCYVer3:RECORD)
          CURRENCYVer3:RECORD :=: CURRENCYVer2:RECORD
        END
        IF ROU:fCurrentFile &= JOBSE2Ver1
          ! Field "JobDiscountAmnt" added
          ! Field "InvDiscountAmnt" added
          CLEAR(JOBSE2Ver2:RECORD)
          JOBSE2Ver2:RECORD :=: JOBSE2Ver1:RECORD
          ROU:fCurrentFile &= JOBSE2Ver2
        END
        IF ROU:fCurrentFile &= JOBSE2Ver2
          ! Field "POPConfirmed" added
          CLEAR(JOBSE2Ver3:RECORD)
          JOBSE2Ver3:RECORD :=: JOBSE2Ver2:RECORD
          ROU:fCurrentFile &= JOBSE2Ver3
        END
        IF ROU:fCurrentFile &= JOBSE2Ver3
          ! Field "ThirdPartyHandlingFee" added
          ! Field "InvThirdPartyHandlingFee" added
          CLEAR(JOBSE2Ver4:RECORD)
          JOBSE2Ver4:RECORD :=: JOBSE2Ver3:RECORD
        END
        IF ROU:fCurrentFile &= ORDPENDVer1
          ! Field "PrevStoReqNo" added
          ! Key/Index "KeyPrevStoReqNo" added
          CLEAR(ORDPENDVer2:RECORD)
          ORDPENDVer2:RECORD :=: ORDPENDVer1:RECORD
        END
        IF ROU:fCurrentFile &= REPTYDEFVer1
          ! Field "NoParts" added
          CLEAR(REPTYDEFVer2:RECORD)
          REPTYDEFVer2:RECORD :=: REPTYDEFVer1:RECORD
          ROU:fCurrentFile &= REPTYDEFVer2
        END
        IF ROU:fCurrentFile &= REPTYDEFVer2
          ! Field "SMSSendType" added
          CLEAR(REPTYDEFVer3:RECORD)
          REPTYDEFVer3:RECORD :=: REPTYDEFVer2:RECORD
        END
        IF ROU:fCurrentFile &= MANFAULTVer1
          ! Field "BouncerFault" added
          ! Key/Index "BouncerFaultKey" added
          CLEAR(MANFAULTVer2:RECORD)
          MANFAULTVer2:RECORD :=: MANFAULTVer1:RECORD
        END
        IF ROU:fCurrentFile &= LOCVALUEVer1
          ! Key/Index "DateOnly" changed (DateOnly KEY(lov:TheDate),DUP,NOCASE,OPT -> DateOnly KEY(lov:TheDate),DUP,NOCASE)
          CLEAR(LOCVALUEVer2:RECORD)
          LOCVALUEVer2:RECORD :=: LOCVALUEVer1:RECORD
        END
        IF ROU:fCurrentFile &= COURIERVer1
          ! Field "InterfaceUse" added
          ! Field "InterfaceName" added
          ! Field "InterfacePassword" added
          ! Field "InterfaceURL" added
          CLEAR(COURIERVer2:RECORD)
          COURIERVer2:RECORD :=: COURIERVer1:RECORD
        END
        IF ROU:fCurrentFile &= TRDPARTYVer1
          ! Field "PreCompletionClaiming" added
          CLEAR(TRDPARTYVer2:RECORD)
          TRDPARTYVer2:RECORD :=: TRDPARTYVer1:RECORD
          ROU:fCurrentFile &= TRDPARTYVer2
        END
        IF ROU:fCurrentFile &= TRDPARTYVer2
          ! Field "EVO_AccNumber" added
          ! Field "EVO_VendorNumber" added
          CLEAR(TRDPARTYVer3:RECORD)
          TRDPARTYVer3:RECORD :=: TRDPARTYVer2:RECORD
          ROU:fCurrentFile &= TRDPARTYVer3
        END
        IF ROU:fCurrentFile &= TRDPARTYVer3
          ! Field "EVO_Profit_Centre" added
          CLEAR(TRDPARTYVer4:RECORD)
          TRDPARTYVer4:RECORD :=: TRDPARTYVer3:RECORD
          ROU:fCurrentFile &= TRDPARTYVer4
        END
        IF ROU:fCurrentFile &= TRDPARTYVer4
          ! Field "EVO_Excluded" added
          CLEAR(TRDPARTYVer5:RECORD)
          TRDPARTYVer5:RECORD :=: TRDPARTYVer4:RECORD
        END
        IF ROU:fCurrentFile &= TRANTYPEVer1
          ! Field "WaybillComBook" added
          ! Field "WaybillComComp" added
          CLEAR(TRANTYPEVer2:RECORD)
          TRANTYPEVer2:RECORD :=: TRANTYPEVer1:RECORD
          ROU:fCurrentFile &= TRANTYPEVer2
        END
        IF ROU:fCurrentFile &= TRANTYPEVer2
          ! Field "SeamsTransactionCode" added
          CLEAR(TRANTYPEVer3:RECORD)
          TRANTYPEVer3:RECORD :=: TRANTYPEVer2:RECORD
        END
        IF ROU:fCurrentFile &= ESNMODELVer1
          ! Field "Active" added
          CLEAR(ESNMODELVer2:RECORD)
          ESNMODELVer2:RECORD :=: ESNMODELVer1:RECORD
        END
        IF ROU:fCurrentFile &= STOCKVer1
          ! Key/Index "Description_Accessory_Key" changed (Description_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Description),DUP,NOCASE -> Description_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Description),DUP,NOCASE)
          ! Key/Index "Shelf_Location_Accessory_Key" changed (Shelf_Location_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Shelf_Location),DUP,NOCASE -> Shelf_Location_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Shelf_Location),DUP,NOCASE)
          ! Key/Index "Supplier_Accessory_Key" changed (Supplier_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Shelf_Location),DUP,NOCASE -> Supplier_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Shelf_Location),DUP,NOCASE)
          ! Key/Index "Supplier_Key" changed (Supplier_Key KEY(sto:Location,sto:Supplier),DUP,NOCASE -> Supplier_Key KEY(sto:Location,sto:Suspend,sto:Supplier),DUP,NOCASE)
          ! Key/Index "Part_Number_Accessory_Key" changed (Part_Number_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Part_Number),DUP,NOCASE -> Part_Number_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Part_Number),DUP,NOCASE)
          ! Key/Index "Manufacturer_Accessory_Key" changed (Manufacturer_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Manufacturer,sto:Part_Number),DUP,NOCASE -> Manufacturer_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Manufacturer,sto:Part_Number),DUP,NOCASE)
          ! Key/Index "LocPartSuspendKey" added
          CLEAR(STOCKVer2:RECORD)
          STOCKVer2:RECORD :=: STOCKVer1:RECORD
          ROU:fCurrentFile &= STOCKVer2
        END
        IF ROU:fCurrentFile &= STOCKVer2
          ! Field "ExchangeOrderCap" added
          CLEAR(STOCKVer3:RECORD)
          STOCKVer3:RECORD :=: STOCKVer2:RECORD
          ROU:fCurrentFile &= STOCKVer3
        END
        IF ROU:fCurrentFile &= STOCKVer3
          ! Field "ExchangeManufacturer" added
          ! Field "ExchangeModelNumber" added
          ! Key/Index "ExchangeModelKey" added
          CLEAR(STOCKVer4:RECORD)
          STOCKVer4:RECORD :=: STOCKVer3:RECORD
          ROU:fCurrentFile &= STOCKVer4
        END
        IF ROU:fCurrentFile &= STOCKVer4
          ! Field "ExchangeModelNumber" removed
          ! Key/Index "ExchangeModelKey" changed (ExchangeModelKey KEY(sto:Location,sto:ExchangeManufacturer,sto:ExchangeModelNumber),DUP,NOCASE -> ExchangeModelKey KEY(sto:Location,sto:Manufacturer,sto:ExchangeManufacturer),DUP,NOCASE)
          CLEAR(STOCKVer5:RECORD)
          STOCKVer5:RECORD :=: STOCKVer4:RECORD
          ROU:fCurrentFile &= STOCKVer5
        END
        IF ROU:fCurrentFile &= STOCKVer5
          ! Field "LoanUnit" added
          ! Field "LoanModelNumber" added
          ! Field "ExchangeManufacturer" changed label to "ExchangeModelNumber"
          ! Key/Index "ExchangeModelKey" changed (ExchangeModelKey KEY(sto:Location,sto:Manufacturer,sto:ExchangeManufacturer),DUP,NOCASE -> ExchangeModelKey KEY(sto:Location,sto:Manufacturer,sto:ExchangeModelNumber),DUP,NOCASE)
          CLEAR(STOCKVer6:RECORD)
          STOCKVer6:RECORD :=: STOCKVer5:RECORD
          STOCKVer6:ExchangeModelNumber = STOCKVer5:ExchangeManufacturer
          ROU:fCurrentFile &= STOCKVer6
        END
        IF ROU:fCurrentFile &= STOCKVer6
          ! Key/Index "LoanModelKey" added
          CLEAR(STOCKVer7:RECORD)
          STOCKVer7:RECORD :=: STOCKVer6:RECORD
        END
        IF ROU:fCurrentFile &= LOANVer1
          ! Field "InTransit" added
          ! Key/Index "InTransitLocationKey" added
          CLEAR(LOANVer2:RECORD)
          LOANVer2:RECORD :=: LOANVer1:RECORD
        END
        IF ROU:fCurrentFile &= SMSMAILVer1
          ! Field "MSG" changed (MSG STRING(160) -> MSG STRING(255))
          CLEAR(SMSMAILVer2:RECORD)
          SMSMAILVer2:RECORD :=: SMSMAILVer1:RECORD
          ROU:fCurrentFile &= SMSMAILVer2
        END
        IF ROU:fCurrentFile &= SMSMAILVer2
          ! Key/Index "RefSMSSentKey" removed
          ! Key/Index "RefEmailSentKey" removed
          ! Key/Index "SMSSBUpdateKey" shifted up on 2 position(s)
          ! Key/Index "EmailSBUpdateKey" shifted up on 2 position(s)
          ! Key/Index "DateTimeInsertedKey" shifted up on 2 position(s)
          CLEAR(SMSMAILVer3:RECORD)
          SMSMAILVer3:RECORD :=: SMSMAILVer2:RECORD
          ROU:fCurrentFile &= SMSMAILVer3
        END
        IF ROU:fCurrentFile &= SMSMAILVer3
          ! Key/Index "RefSMSSentKey" added
          CLEAR(SMSMAILVer4:RECORD)
          SMSMAILVer4:RECORD :=: SMSMAILVer3:RECORD
          ROU:fCurrentFile &= SMSMAILVer4
        END
        IF ROU:fCurrentFile &= SMSMAILVer4
          ! Key/Index "RefSMSSentKey" removed
          ! Key/Index "RefEmailSentKey" removed
          ! Key/Index "SMSSBUpdateKey" shifted up on 2 position(s)
          ! Key/Index "EmailSBUpdateKey" shifted up on 2 position(s)
          ! Key/Index "DateTimeInsertedKey" shifted up on 2 position(s)
          CLEAR(SMSMAILVer5:RECORD)
          SMSMAILVer5:RECORD :=: SMSMAILVer4:RECORD
          ROU:fCurrentFile &= SMSMAILVer5
        END
        IF ROU:fCurrentFile &= SMSMAILVer5
          ! Field "SMSType" added
          CLEAR(SMSMAILVer6:RECORD)
          SMSMAILVer6:RECORD :=: SMSMAILVer5:RECORD
          ROU:fCurrentFile &= SMSMAILVer6
        END
        IF ROU:fCurrentFile &= SMSMAILVer6
          ! Key/Index "KeyMSISDN_DateDec" added
          CLEAR(SMSMAILVer7:RECORD)
          SMSMAILVer7:RECORD :=: SMSMAILVer6:RECORD
          ROU:fCurrentFile &= SMSMAILVer7
        END
        IF ROU:fCurrentFile &= SMSMAILVer7
          ! Field "EmailAddress2" added
          ! Field "EmailAddress3" added
          CLEAR(SMSMAILVer8:RECORD)
          SMSMAILVer8:RECORD :=: SMSMAILVer7:RECORD
          ROU:fCurrentFile &= SMSMAILVer8
        END
        IF ROU:fCurrentFile &= SMSMAILVer8
          ! Field "EmailSubject" added
          CLEAR(SMSMAILVer9:RECORD)
          SMSMAILVer9:RECORD :=: SMSMAILVer8:RECORD
          ROU:fCurrentFile &= SMSMAILVer9
        END
        IF ROU:fCurrentFile &= SMSMAILVer9
          ! Key/Index "KeyRefDateTimeDec" added
          CLEAR(SMSMAILVer10:RECORD)
          SMSMAILVer10:RECORD :=: SMSMAILVer9:RECORD
          ROU:fCurrentFile &= SMSMAILVer10
        END
        IF ROU:fCurrentFile &= SMSMAILVer10
          ! Field "SendToSMS" shifted up on 2 position(s)
          ! Field "SendToEmail" shifted up on 2 position(s)
          ! Field "SMSSent" shifted up on 2 position(s)
          ! Field "DateInserted" shifted up on 2 position(s)
          ! Field "TimeInserted" shifted up on 2 position(s)
          ! Field "EmailSent" shifted up on 2 position(s)
          ! Field "DateSMSSent" shifted up on 2 position(s)
          ! Field "TimeSMSSent" shifted up on 2 position(s)
          ! Field "DateEmailSent" shifted up on 2 position(s)
          ! Field "TimeEmailSent" shifted up on 2 position(s)
          ! Field "PathToEstimate" shifted up on 2 position(s)
          ! Field "SBUpdated" shifted up on 2 position(s)
          ! Field "SMSType" shifted up on 2 position(s)
          ! Field "EmailSubject" shifted up on 2 position(s)
          ! Field "EmailAddress2" shifted down on 14 position(s)
          ! Field "EmailAddress3" shifted down on 14 position(s)
          CLEAR(SMSMAILVer11:RECORD)
          SMSMAILVer11:RECORD :=: SMSMAILVer10:RECORD
        END
        IF ROU:fCurrentFile &= LOANHISTVer1
          ! Field "Notes" added
          CLEAR(LOANHISTVer2:RECORD)
          LOANHISTVer2:RECORD :=: LOANHISTVer1:RECORD
        END
        IF ROU:fCurrentFile &= EXCHHISTVer1
          ! Field "Ref_Number" changed (Ref_Number REAL -> Ref_Number LONG)
          ! Field "record_number" changed (record_number REAL -> record_number LONG)
          ! Field "Status" changed (Status STRING(60) -> Status STRING(255))
          CLEAR(EXCHHISTVer2:RECORD)
          EXCHHISTVer2:RECORD :=: EXCHHISTVer1:RECORD
          ROU:fCurrentFile &= EXCHHISTVer2
        END
        IF ROU:fCurrentFile &= EXCHHISTVer2
          ! Field "Status" changed (Status STRING(255) -> Status STRING(60))
          CLEAR(EXCHHISTVer3:RECORD)
          EXCHHISTVer3:RECORD :=: EXCHHISTVer2:RECORD
          ROU:fCurrentFile &= EXCHHISTVer3
        END
        IF ROU:fCurrentFile &= EXCHHISTVer3
          ! Field "Status" changed (Status STRING(60) -> Status STRING(255))
          CLEAR(EXCHHISTVer4:RECORD)
          EXCHHISTVer4:RECORD :=: EXCHHISTVer3:RECORD
          ROU:fCurrentFile &= EXCHHISTVer4
        END
        IF ROU:fCurrentFile &= EXCHHISTVer4
          ! Field "Status" changed (Status STRING(255) -> Status STRING(60))
          ! Field "Notes" added
          CLEAR(EXCHHISTVer5:RECORD)
          EXCHHISTVer5:RECORD :=: EXCHHISTVer4:RECORD
        END
        IF ROU:fCurrentFile &= AUDITVer1
          ! Field "DummyField" removed
          ! Field "Notes" removed
          CLEAR(AUDITVer2:RECORD)
          AUDITVer2:RECORD :=: AUDITVer1:RECORD
        END
        IF ROU:fCurrentFile &= USERSVer1
          ! Field "MobileNumber" added
          CLEAR(USERSVer2:RECORD)
          USERSVer2:RECORD :=: USERSVer1:RECORD
          ROU:fCurrentFile &= USERSVer2
        END
        IF ROU:fCurrentFile &= USERSVer2
          ! Field "RRC_MII_Dealer_ID" added
          CLEAR(USERSVer3:RECORD)
          USERSVer3:RECORD :=: USERSVer2:RECORD
          ROU:fCurrentFile &= USERSVer3
        END
        IF ROU:fCurrentFile &= USERSVer3
          ! Field "AppleAccredited" added
          ! Field "AppleID" added
          CLEAR(USERSVer4:RECORD)
          USERSVer4:RECORD :=: USERSVer3:RECORD
        END
        IF ROU:fCurrentFile &= LOCSHELFVer1
          ! Field "RecordNumber" added
          ! Key/Index "RecordNumberKey" added
          CLEAR(LOCSHELFVer2:RECORD)
          LOCSHELFVer2:RECORD :=: LOCSHELFVer1:RECORD
          LOCSHELF_autonumber += 1
          LOCSHELFVer2:RecordNumber = LOCSHELF_autonumber
        END
        IF ROU:fCurrentFile &= RTNORDERVer1
          ! Field "CreditNoteRequestNumber" added
          ! Field "CreditNoteUser" added
          ! Field "CreditNoteDate" added
          ! Field "CreditNoteTime" added
          ! Field "WaybillNumber" added
          ! Field "STOCKRefNumber" changed label to "RefNumber"
          ! Key/Index "CreditNoteNumberKey" added
          CLEAR(RTNORDERVer2:RECORD)
          RTNORDERVer2:RECORD :=: RTNORDERVer1:RECORD
          RTNORDERVer2:RefNumber = RTNORDERVer1:STOCKRefNumber
          ROU:fCurrentFile &= RTNORDERVer2
        END
        IF ROU:fCurrentFile &= RTNORDERVer2
          ! Field "CreditNoteUser" removed
          ! Field "CreditNoteDate" removed
          ! Field "CreditNoteTime" removed
          CLEAR(RTNORDERVer3:RECORD)
          RTNORDERVer3:RECORD :=: RTNORDERVer2:RECORD
          ROU:fCurrentFile &= RTNORDERVer3
        END
        IF ROU:fCurrentFile &= RTNORDERVer3
          ! Field "ReturnType" added
          CLEAR(RTNORDERVer4:RECORD)
          RTNORDERVer4:RECORD :=: RTNORDERVer3:RECORD
          ROU:fCurrentFile &= RTNORDERVer4
        END
        IF ROU:fCurrentFile &= RTNORDERVer4
          ! Field "PurchaseCost" added
          ! Field "SaleCost" added
          ! Field "ExchangePrice" added
          CLEAR(RTNORDERVer5:RECORD)
          RTNORDERVer5:RECORD :=: RTNORDERVer4:RECORD
          ROU:fCurrentFile &= RTNORDERVer5
        END
        IF ROU:fCurrentFile &= RTNORDERVer5
          ! Key/Index "RefNumberKey" added
          CLEAR(RTNORDERVer6:RECORD)
          RTNORDERVer6:RECORD :=: RTNORDERVer5:RECORD
          ROU:fCurrentFile &= RTNORDERVer6
        END
        IF ROU:fCurrentFile &= RTNORDERVer6
          ! Key/Index "OrderStatusPartNumberKey" changed (OrderStatusPartNumberKey KEY(rtn:OrderNumber,rtn:InvoiceNumber,rtn:ExchangeOrder,rtn:Status,rtn:PartNumber),DUP,NOCASE -> OrderStatusPartNumberKey KEY(rtn:OrderNumber,rtn:ExchangeOrder,rtn:Status,rtn:PartNumber),DUP,NOCASE)
          ! Key/Index "OrderPartNumberKey" changed (OrderPartNumberKey KEY(rtn:OrderNumber,rtn:InvoiceNumber,rtn:ExchangeOrder,rtn:PartNumber),DUP,NOCASE -> OrderPartNumberKey KEY(rtn:OrderNumber,rtn:ExchangeOrder,rtn:PartNumber),DUP,NOCASE)
          ! Key/Index "OrderStatusReturnRefNoKey" added
          CLEAR(RTNORDERVer7:RECORD)
          RTNORDERVer7:RECORD :=: RTNORDERVer6:RECORD
          ROU:fCurrentFile &= RTNORDERVer7
        END
        IF ROU:fCurrentFile &= RTNORDERVer7
          ! Field "Archived" added
          ! Field "Ordered" added
          ! Field "DateOrdered" added
          ! Field "TimeOrdered" added
          ! Field "WhoOrdered" added
          ! Field "Received" added
          ! Field "DateReceived" added
          ! Field "TimeReceived" added
          ! Field "WhoReceived" added
          ! Key/Index "LocArcOrdWaybillKey" added
          CLEAR(RTNORDERVer8:RECORD)
          RTNORDERVer8:RECORD :=: RTNORDERVer7:RECORD
          ROU:fCurrentFile &= RTNORDERVer8
        END
        IF ROU:fCurrentFile &= RTNORDERVer8
          ! Field "WhoProcessed" added
          CLEAR(RTNORDERVer9:RECORD)
          RTNORDERVer9:RECORD :=: RTNORDERVer8:RECORD
          ROU:fCurrentFile &= RTNORDERVer9
        END
        IF ROU:fCurrentFile &= RTNORDERVer9
          ! Key/Index "ArcLocDateKey" added
          CLEAR(RTNORDERVer10:RECORD)
          RTNORDERVer10:RECORD :=: RTNORDERVer9:RECORD
          ROU:fCurrentFile &= RTNORDERVer10
        END
        IF ROU:fCurrentFile &= RTNORDERVer10
          ! Key/Index "ArcDateKey" added
          CLEAR(RTNORDERVer11:RECORD)
          RTNORDERVer11:RECORD :=: RTNORDERVer10:RECORD
          ROU:fCurrentFile &= RTNORDERVer11
        END
        IF ROU:fCurrentFile &= RTNORDERVer11
          ! Key/Index "DateOrderedKey" added
          CLEAR(RTNORDERVer12:RECORD)
          RTNORDERVer12:RECORD :=: RTNORDERVer11:RECORD
          ROU:fCurrentFile &= RTNORDERVer12
        END
        IF ROU:fCurrentFile &= RTNORDERVer12
          ! Key/Index "ArcOrdExcWaybillKey" added
          CLEAR(RTNORDERVer13:RECORD)
          RTNORDERVer13:RECORD :=: RTNORDERVer12:RECORD
        END
        IF ROU:fCurrentFile &= C3DMONITVer1
          ! Key/Index "KeyDateTime" changed (KeyDateErrorCode KEY(C3D:CallDate,C3D:ErrorCode),DUP,NOCASE -> KeyDateTime KEY(C3D:CallDate,C3D:CallTime),DUP,NOCASE)
          CLEAR(C3DMONITVer2:RECORD)
          C3DMONITVer2:RECORD :=: C3DMONITVer1:RECORD
        END
        IF ROU:fCurrentFile &= SMSTextVer1
          ! Attribute "NAME" changed (<Empty> -> 'SMSText.Dat')
          ! Key/Index "Key_Description" added
          CLEAR(SMSTextVer2:RECORD)
          SMSTextVer2:RECORD :=: SMSTextVer1:RECORD
          ROU:fCurrentFile &= SMSTextVer2
        END
        IF ROU:fCurrentFile &= SMSTextVer2
          ! Field "SMSText" changed (SMSText STRING(200) -> SMSText STRING(500))
          ! Field "Final_Estimate_Text" changed (Final_Estimate_Text STRING(200) -> Final_Estimate_Text STRING(500))
          ! Key/Index "Key_Location_CSI" added
          CLEAR(SMSTextVer3:RECORD)
          SMSTextVer3:RECORD :=: SMSTextVer2:RECORD
          ROU:fCurrentFile &= SMSTextVer3
        END
        IF ROU:fCurrentFile &= SMSTextVer3
          ! Field "BER" added
          CLEAR(SMSTextVer4:RECORD)
          SMSTextVer4:RECORD :=: SMSTextVer3:RECORD
          ROU:fCurrentFile &= SMSTextVer4
        END
        IF ROU:fCurrentFile &= SMSTextVer4
          ! Key/Index "Key_Location_BER" added
          CLEAR(SMSTextVer5:RECORD)
          SMSTextVer5:RECORD :=: SMSTextVer4:RECORD
          ROU:fCurrentFile &= SMSTextVer5
        END
        IF ROU:fCurrentFile &= SMSTextVer5
          ! Field "LiquidDamage" added
          ! Key/Index "Key_Location_Liquid" added
          CLEAR(SMSTextVer6:RECORD)
          SMSTextVer6:RECORD :=: SMSTextVer5:RECORD
          ROU:fCurrentFile &= SMSTextVer6
        END
        IF ROU:fCurrentFile &= SMSTextVer6
          ! Key/Index "Key_StatusType_Location_Trigger" changed (Key_StatusType_Location_Trigger KEY(SMT:Status_Type,SMT:Location,SMT:Trigger_Status),DUP,NOCASE,OPT -> Key_StatusType_Location_Trigger KEY(SMT:Status_Type,SMT:Location,SMT:Trigger_Status),DUP,NOCASE)
          CLEAR(SMSTextVer7:RECORD)
          SMSTextVer7:RECORD :=: SMSTextVer6:RECORD
          ROU:fCurrentFile &= SMSTextVer7
        END
        IF ROU:fCurrentFile &= SMSTextVer7
          ! Field "SEAMS_Notification" added
          CLEAR(SMSTextVer8:RECORD)
          SMSTextVer8:RECORD :=: SMSTextVer7:RECORD
        END
        IF ROU:fCurrentFile &= RETSTOCKVer1
          ! Field "ExchangeIMEINumber" added
          CLEAR(RETSTOCKVer2:RECORD)
          RETSTOCKVer2:RECORD :=: RETSTOCKVer1:RECORD
          ROU:fCurrentFile &= RETSTOCKVer2
        END
        IF ROU:fCurrentFile &= RETSTOCKVer2
          ! Key/Index "IMEINumberKey" added
          CLEAR(RETSTOCKVer3:RECORD)
          RETSTOCKVer3:RECORD :=: RETSTOCKVer2:RECORD
          ROU:fCurrentFile &= RETSTOCKVer3
        END
        IF ROU:fCurrentFile &= RETSTOCKVer3
          ! Field "ExchangeRefNumber" changed (ExchangeIMEINumber STRING(30) -> ExchangeRefNumber LONG)
          ! Field "ExchangeIMEINumber" changed label to "ExchangeRefNumber"
          ! Key/Index "ExchangeRefNumberKey" changed (IMEINumberKey KEY(res:Ref_Number,res:ExchangeIMEINumber),DUP,NOCASE -> ExchangeRefNumberKey KEY(res:Ref_Number,res:ExchangeRefNumber),DUP,NOCASE)
          CLEAR(RETSTOCKVer4:RECORD)
          RETSTOCKVer4:RECORD :=: RETSTOCKVer3:RECORD
          RETSTOCKVer4:ExchangeRefNumber = RETSTOCKVer3:ExchangeIMEINumber
          ROU:fCurrentFile &= RETSTOCKVer4
        END
        IF ROU:fCurrentFile &= RETSTOCKVer4
          ! Field "LoanRefNumber" added
          ! Key/Index "LoanRefNumberKey" added
          CLEAR(RETSTOCKVer5:RECORD)
          RETSTOCKVer5:RECORD :=: RETSTOCKVer4:RECORD
        END
        IF ROU:fCurrentFile &= STOCKALXVer1
          ! Field "JobNo" added
          ! Field "PartNo" added
          ! Field "Description" added
          ! Key/Index "KeyRecordNo" changed (KeyRecordNo KEY(STO1:RecordNo),NOCASE,PRIMARY -> KeyRecordNo KEY(STLX:RecordNo),NOCASE,PRIMARY)
          ! Key/Index "KeySTLRecordNumber" changed (KeySTLRecordNumber KEY(STO1:StlRecordNumber),DUP,NOCASE -> KeySTLRecordNumber KEY(STLX:StlRecordNumber),DUP,NOCASE)
          ! Key/Index "KeyRequestDateTime" changed (KeyRequestDateTime KEY(STO1:RequestDate,STO1:RequestTime),DUP,NOCASE -> KeyRequestDateTime KEY(STLX:RequestDate,STLX:RequestTime),DUP,NOCASE)
          CLEAR(STOCKALXVer2:RECORD)
          STOCKALXVer2:RECORD :=: STOCKALXVer1:RECORD
          ROU:fCurrentFile &= STOCKALXVer2
        END
        IF ROU:fCurrentFile &= STOCKALXVer2
          ! Field "AllocateTime" changed (AllocateTime STRING(20) -> AllocateTime TIME)
          CLEAR(STOCKALXVer3:RECORD)
          STOCKALXVer3:RECORD :=: STOCKALXVer2:RECORD
        END
        IF ROU:fCurrentFile &= ORDPARTSVer1
          ! Field "FreeExchangeStock" added
          CLEAR(ORDPARTSVer2:RECORD)
          ORDPARTSVer2:RECORD :=: ORDPARTSVer1:RECORD
          ROU:fCurrentFile &= ORDPARTSVer2
        END
        IF ROU:fCurrentFile &= ORDPARTSVer2
          ! Field "TimeReceived" added
          ! Field "DatePriceCaptured" added
          ! Field "TimePriceCaptured" added
          CLEAR(ORDPARTSVer3:RECORD)
          ORDPARTSVer3:RECORD :=: ORDPARTSVer2:RECORD
          ROU:fCurrentFile &= ORDPARTSVer3
        END
        IF ROU:fCurrentFile &= ORDPARTSVer3
          ! Field "UncapturedGRNNumber" added
          CLEAR(ORDPARTSVer4:RECORD)
          ORDPARTSVer4:RECORD :=: ORDPARTSVer3:RECORD
        END
        IF ROU:fCurrentFile &= LOCATIONVer1
          ! Field "FaultyPartsLocation" added
          ! Key/Index "FaultyLocationKey" added
          CLEAR(LOCATIONVer2:RECORD)
          LOCATIONVer2:RECORD :=: LOCATIONVer1:RECORD
        END
        IF ROU:fCurrentFile &= STOHISTEVer1
          ! Field "HistDate" added
          ! Field "ARC_Status" added
          ! Key/Index "KeyArcStatus" added
          CLEAR(STOHISTEVer2:RECORD)
          STOHISTEVer2:RECORD :=: STOHISTEVer1:RECORD
          ROU:fCurrentFile &= STOHISTEVer2
        END
        IF ROU:fCurrentFile &= STOHISTEVer2
          ! Field "HistTime" changed (HistDate DATE -> HistTime TIME)
          ! Field "HistDate" changed label to "HistTime"
          CLEAR(STOHISTEVer3:RECORD)
          STOHISTEVer3:RECORD :=: STOHISTEVer2:RECORD
          STOHISTEVer3:HistTime = STOHISTEVer2:HistDate
        END
        IF ROU:fCurrentFile &= CREDNOTRVer1
          ! Field "Received" added
          ! Key/Index "ReceivedKey" added
          CLEAR(CREDNOTRVer2:RECORD)
          CREDNOTRVer2:RECORD :=: CREDNOTRVer1:RECORD
          ROU:fCurrentFile &= CREDNOTRVer2
        END
        IF ROU:fCurrentFile &= CREDNOTRVer2
          ! Field "DateReceived" added
          ! Field "TimeReceived" added
          ! Field "ReceivedUsercode" added
          CLEAR(CREDNOTRVer3:RECORD)
          CREDNOTRVer3:RECORD :=: CREDNOTRVer2:RECORD
        END
        IF ROU:fCurrentFile &= EXCHANGEVer1
          ! Field "FreeStockPurchased" added
          CLEAR(EXCHANGEVer2:RECORD)
          EXCHANGEVer2:RECORD :=: EXCHANGEVer1:RECORD
          ROU:fCurrentFile &= EXCHANGEVer2
        END
        IF ROU:fCurrentFile &= EXCHANGEVer2
          ! Field "StatusChangeDate" added
          ! Key/Index "StatusChangeDateKey" added
          CLEAR(EXCHANGEVer3:RECORD)
          EXCHANGEVer3:RECORD :=: EXCHANGEVer2:RECORD
        END
        IF ROU:fCurrentFile &= SUPPLIERVer1
          ! Field "UseFreeStock" added
          CLEAR(SUPPLIERVer2:RECORD)
          SUPPLIERVer2:RECORD :=: SUPPLIERVer1:RECORD
          ROU:fCurrentFile &= SUPPLIERVer2
        END
        IF ROU:fCurrentFile &= SUPPLIERVer2
          ! Field "EVO_GL_Acc_No" added
          ! Field "EVO_Vendor_Number" added
          ! Field "EVO_TaxExempt" added
          CLEAR(SUPPLIERVer3:RECORD)
          SUPPLIERVer3:RECORD :=: SUPPLIERVer2:RECORD
          ROU:fCurrentFile &= SUPPLIERVer3
        END
        IF ROU:fCurrentFile &= SUPPLIERVer3
          ! Field "EVO_Profit_Centre" added
          CLEAR(SUPPLIERVer4:RECORD)
          SUPPLIERVer4:RECORD :=: SUPPLIERVer3:RECORD
          ROU:fCurrentFile &= SUPPLIERVer4
        END
        IF ROU:fCurrentFile &= SUPPLIERVer4
          ! Field "EVO_Excluded" added
          CLEAR(SUPPLIERVer5:RECORD)
          SUPPLIERVer5:RECORD :=: SUPPLIERVer4:RECORD
        END
        IF ROU:fCurrentFile &= SUBURBVer1
          ! Field "Region" added
          CLEAR(SUBURBVer2:RECORD)
          SUBURBVer2:RECORD :=: SUBURBVer1:RECORD
        END
        IF ROU:fCurrentFile &= SUPVALAVer1
          ! Key/Index "DateOnly" changed (DateOnly KEY(suva:RunDate),DUP,NOCASE,OPT -> DateOnly KEY(suva:RunDate),DUP,NOCASE)
          CLEAR(SUPVALAVer2:RECORD)
          SUPVALAVer2:RECORD :=: SUPVALAVer1:RECORD
        END
        IF ROU:fCurrentFile &= SUPVALBVer1
          ! Key/Index "DateOnly" changed (DateOnly KEY(suvb:RunDate),DUP,NOCASE,OPT -> DateOnly KEY(suvb:RunDate),DUP,NOCASE)
          CLEAR(SUPVALBVer2:RECORD)
          SUPVALBVer2:RECORD :=: SUPVALBVer1:RECORD
        END
        IF ROU:fCurrentFile &= MODELNUMVer1
          ! Field "OneYearWarrOnly" added
          CLEAR(MODELNUMVer2:RECORD)
          MODELNUMVer2:RECORD :=: MODELNUMVer1:RECORD
          ROU:fCurrentFile &= MODELNUMVer2
        END
        IF ROU:fCurrentFile &= MODELNUMVer2
          ! Field "Active" added
          CLEAR(MODELNUMVer3:RECORD)
          MODELNUMVer3:RECORD :=: MODELNUMVer2:RECORD
          ROU:fCurrentFile &= MODELNUMVer3
        END
        IF ROU:fCurrentFile &= MODELNUMVer3
          ! Field "ExchReplaceValue" added
          CLEAR(MODELNUMVer4:RECORD)
          MODELNUMVer4:RECORD :=: MODELNUMVer3:RECORD
          ROU:fCurrentFile &= MODELNUMVer4
        END
        IF ROU:fCurrentFile &= MODELNUMVer4
          ! Field "LoanSellingPrice" added
          ! Field "RRCOrderCap" added
          CLEAR(MODELNUMVer5:RECORD)
          MODELNUMVer5:RECORD :=: MODELNUMVer4:RECORD
          ROU:fCurrentFile &= MODELNUMVer5
        END
        IF ROU:fCurrentFile &= MODELNUMVer5
          ! Field "LoanSellingPrice" removed
          CLEAR(MODELNUMVer6:RECORD)
          MODELNUMVer6:RECORD :=: MODELNUMVer5:RECORD
        END
        IF ROU:fCurrentFile &= TRDBATCHVer1
          ! Field "EVO_Status" added
          ! Key/Index "KeyEVO_Status" added
          CLEAR(TRDBATCHVer2:RECORD)
          TRDBATCHVer2:RECORD :=: TRDBATCHVer1:RECORD
        END
        IF ROU:fCurrentFile &= TRADEACCVer1
          ! Field "SBOnlineJobProgress" added
          CLEAR(TRADEACCVer2:RECORD)
          TRADEACCVer2:RECORD :=: TRADEACCVer1:RECORD
          ROU:fCurrentFile &= TRADEACCVer2
        END
        IF ROU:fCurrentFile &= TRADEACCVer2
          ! Field "coTradingName" added
          ! Field "coTradingName2" added
          ! Field "coLocation" added
          ! Field "coRegistrationNo" added
          ! Field "coVATNumber" added
          ! Field "coAddressLine1" added
          ! Field "coAddressLine2" added
          ! Field "coAddressLine3" added
          ! Field "coAddressLine4" added
          ! Field "coTelephoneNumber" added
          ! Field "coFaxNumber" added
          ! Field "coEmailAddress" added
          CLEAR(TRADEACCVer3:RECORD)
          TRADEACCVer3:RECORD :=: TRADEACCVer2:RECORD
          ROU:fCurrentFile &= TRADEACCVer3
        END
        IF ROU:fCurrentFile &= TRADEACCVer3
          ! Field "RtnExchangeAccount" added
          CLEAR(TRADEACCVer4:RECORD)
          TRADEACCVer4:RECORD :=: TRADEACCVer3:RECORD
          ROU:fCurrentFile &= TRADEACCVer4
        END
        IF ROU:fCurrentFile &= TRADEACCVer4
          ! Field "coSMSname" added
          CLEAR(TRADEACCVer5:RECORD)
          TRADEACCVer5:RECORD :=: TRADEACCVer4:RECORD
          ROU:fCurrentFile &= TRADEACCVer5
        END
        IF ROU:fCurrentFile &= TRADEACCVer5
          ! Field "SMS_Email2" added
          ! Field "SMS_Email3" added
          CLEAR(TRADEACCVer6:RECORD)
          TRADEACCVer6:RECORD :=: TRADEACCVer5:RECORD
          ROU:fCurrentFile &= TRADEACCVer6
        END
        IF ROU:fCurrentFile &= TRADEACCVer6
          ! Field "SMS_ReportEmail" added
          CLEAR(TRADEACCVer7:RECORD)
          TRADEACCVer7:RECORD :=: TRADEACCVer6:RECORD
          ROU:fCurrentFile &= TRADEACCVer7
        END
        IF ROU:fCurrentFile &= TRADEACCVer7
          ! Field "UseSparesRequest" added
          CLEAR(TRADEACCVer8:RECORD)
          TRADEACCVer8:RECORD :=: TRADEACCVer7:RECORD
          ROU:fCurrentFile &= TRADEACCVer8
        END
        IF ROU:fCurrentFile &= TRADEACCVer8
          ! Field "coSMSname" removed
          ! Field "SMS_Email2" removed
          ! Field "SMS_Email3" removed
          ! Field "SMS_ReportEmail" removed
          ! Field "UseSparesRequest" removed
          CLEAR(TRADEACCVer9:RECORD)
          TRADEACCVer9:RECORD :=: TRADEACCVer8:RECORD
        END
        IF ROU:fCurrentFile &= REQUISITVer1
          ! Field "Approved" added
          CLEAR(REQUISITVer2:RECORD)
          REQUISITVer2:RECORD :=: REQUISITVer1:RECORD
        END
        IF ROU:fCurrentFile &= SUBTRACCVer1
          ! Field "DealerID" added
          CLEAR(SUBTRACCVer2:RECORD)
          SUBTRACCVer2:RECORD :=: SUBTRACCVer1:RECORD
          ROU:fCurrentFile &= SUBTRACCVer2
        END
        IF ROU:fCurrentFile &= SUBTRACCVer2
          ! Field "AllowChangeSiebelInfo" added
          CLEAR(SUBTRACCVer3:RECORD)
          SUBTRACCVer3:RECORD :=: SUBTRACCVer2:RECORD
        END
        IF ROU:fCurrentFile &= WAYCNRVer1
          ! Field "QuantityReceived" added
          ! Field "Processed" added
          ! Key/Index "ProcessedKey" added
          CLEAR(WAYCNRVer2:RECORD)
          WAYCNRVer2:RECORD :=: WAYCNRVer1:RECORD
          ROU:fCurrentFile &= WAYCNRVer2
        END
        IF ROU:fCurrentFile &= WAYCNRVer2
          ! Field "RefNumber" added
          CLEAR(WAYCNRVer3:RECORD)
          WAYCNRVer3:RECORD :=: WAYCNRVer2:RECORD
        END
        IF ROU:fCurrentFile &= MANUFACTVer1
          ! Field "OneYearWarrOnly" added
          CLEAR(MANUFACTVer2:RECORD)
          MANUFACTVer2:RECORD :=: MANUFACTVer1:RECORD
          ROU:fCurrentFile &= MANUFACTVer2
        END
        IF ROU:fCurrentFile &= MANUFACTVer2
          ! Field "EDItransportFee" added
          CLEAR(MANUFACTVer3:RECORD)
          MANUFACTVer3:RECORD :=: MANUFACTVer2:RECORD
          ROU:fCurrentFile &= MANUFACTVer3
        END
        IF ROU:fCurrentFile &= MANUFACTVer3
          ! Field "SagemVersionNumber" added
          CLEAR(MANUFACTVer4:RECORD)
          MANUFACTVer4:RECORD :=: MANUFACTVer3:RECORD
          ROU:fCurrentFile &= MANUFACTVer4
        END
        IF ROU:fCurrentFile &= MANUFACTVer4
          ! Field "UseBouncerRules" added
          ! Field "BouncerPeriod" added
          ! Field "BouncerType" added
          ! Field "BouncerInFault" added
          ! Field "BouncerOutFault" added
          ! Field "BouncerSpares" added
          ! Field "BouncerPeriodIMEI" added
          ! Field "DoNotBounceWarranty" added
          CLEAR(MANUFACTVer5:RECORD)
          MANUFACTVer5:RECORD :=: MANUFACTVer4:RECORD
          ROU:fCurrentFile &= MANUFACTVer5
        END
        IF ROU:fCurrentFile &= MANUFACTVer5
          ! Field "BounceRulesType" added
          CLEAR(MANUFACTVer6:RECORD)
          MANUFACTVer6:RECORD :=: MANUFACTVer5:RECORD
          ROU:fCurrentFile &= MANUFACTVer6
        END
        IF ROU:fCurrentFile &= MANUFACTVer6
          ! Field "BounceRulesType" removed
          CLEAR(MANUFACTVer7:RECORD)
          MANUFACTVer7:RECORD :=: MANUFACTVer6:RECORD
          ROU:fCurrentFile &= MANUFACTVer7
        END
        IF ROU:fCurrentFile &= MANUFACTVer7
          ! Field "BounceRulesType" added
          CLEAR(MANUFACTVer8:RECORD)
          MANUFACTVer8:RECORD :=: MANUFACTVer7:RECORD
          ROU:fCurrentFile &= MANUFACTVer8
        END
        IF ROU:fCurrentFile &= MANUFACTVer8
          ! Field "ThirdPartyHandlingFee" added
          CLEAR(MANUFACTVer9:RECORD)
          MANUFACTVer9:RECORD :=: MANUFACTVer8:RECORD
          ROU:fCurrentFile &= MANUFACTVer9
        END
        IF ROU:fCurrentFile &= MANUFACTVer9
          ! Field "ProductCodeCompulsory" added
          CLEAR(MANUFACTVer10:RECORD)
          MANUFACTVer10:RECORD :=: MANUFACTVer9:RECORD
          ROU:fCurrentFile &= MANUFACTVer10
        END
        IF ROU:fCurrentFile &= MANUFACTVer10
          ! Field "Inactive" added
          CLEAR(MANUFACTVer11:RECORD)
          MANUFACTVer11:RECORD :=: MANUFACTVer10:RECORD
        END
        IF ROU:fCurrentFile &= WAYBCONFVer1
          IF ~WAYBCONFVer2:RecordNo
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SOAPERRSVer1
          IF ~SOAPERRSVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~SOAPERRSVer2:ErrorCode
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= JOBSE3Ver2
          IF ~JOBSE3Ver3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= STOAUUSEVer1
          IF ~STOAUUSEVer2:Record_No
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= PREJOBVer2
          IF ~PREJOBVer3:RefNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= TRADEAC2Ver3
          IF ~TRADEAC2Ver4:RecordNo
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~TRADEAC2Ver4:Account_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SMSRECVDVer4
          IF ~SMSRECVDVer5:RecordNo
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= GRNOTESVer2
          IF ~GRNOTESVer3:Goods_Received_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= EXCHAUIVer1
          IF ~EXCHAUIVer2:Internal_No
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= RTNAWAITVer6
          IF ~RTNAWAITVer7:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= STMASAUDVer1
          IF ~STMASAUDVer2:Audit_No
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= ORDITEMSVer1
          IF ~ORDITEMSVer2:recordnumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOCATLOGVer1
          IF ~LOCATLOGVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFAULT2Ver4
          IF ~DEFAULT2Ver5:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= RETSALESVer2
          IF ~RETSALESVer3:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOAORDRVer1
          IF ~LOAORDRVer2:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= WIPAMFVer1
          IF ~WIPAMFVer2:Audit_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= TEAMSVer2
          IF ~TEAMSVer3:Record_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~TEAMSVer3:Team
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= WIPAUIVer1
          IF ~WIPAUIVer2:Internal_No
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= EXCHORDRVer5
          IF ~EXCHORDRVer6:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= CONTHISTVer1
          IF ~CONTHISTVer2:Record_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= STANTEXTVer1
          IF ~STANTEXTVer2:Description
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= JOBSWARRVer2
          IF ~JOBSWARRVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= CURRENCYVer2
          IF ~CURRENCYVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~CURRENCYVer3:CurrencyCode
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= JOBSE2Ver3
          IF ~JOBSE2Ver4:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= ORDPENDVer1
          IF ~ORDPENDVer2:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= REPTYDEFVer2
          IF ~REPTYDEFVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~REPTYDEFVer3:Manufacturer AND ~REPTYDEFVer3:Repair_Type
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MANFAULTVer1
          IF ~MANFAULTVer2:Manufacturer AND ~MANFAULTVer2:Field_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOCVALUEVer1
          IF ~LOCVALUEVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= COURIERVer1
          IF ~COURIERVer2:Courier
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= TRDPARTYVer4
          IF ~TRDPARTYVer5:Company_Name
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~TRDPARTYVer5:Account_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= TRANTYPEVer2
          IF ~TRANTYPEVer3:Transit_Type
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= ESNMODELVer1
          IF ~ESNMODELVer2:Record_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= STOCKVer6
          IF ~STOCKVer7:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOANVer1
          IF ~LOANVer2:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SMSMAILVer10
          IF ~SMSMAILVer11:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOANHISTVer1
          IF ~LOANHISTVer2:record_number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= EXCHHISTVer4
          IF ~EXCHHISTVer5:record_number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= AUDITVer1
          IF ~AUDITVer2:record_number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= USERSVer3
          IF ~USERSVer4:User_Code
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~USERSVer4:Password
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOCSHELFVer1
          IF ~LOCSHELFVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~LOCSHELFVer2:Site_Location AND ~LOCSHELFVer2:Shelf_Location
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= RTNORDERVer12
          IF ~RTNORDERVer13:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= C3DMONITVer1
          IF ~C3DMONITVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SMSTextVer7
          IF ~SMSTextVer8:Record_No
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= RETSTOCKVer4
          IF ~RETSTOCKVer5:Record_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= STOCKALXVer2
          IF ~STOCKALXVer3:RecordNo
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= ORDPARTSVer3
          IF ~ORDPARTSVer4:Record_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOCATIONVer1
          IF ~LOCATIONVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~LOCATIONVer2:Location
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= STOHISTEVer2
          IF ~STOHISTEVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= CREDNOTRVer2
          IF ~CREDNOTRVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= EXCHANGEVer2
          IF ~EXCHANGEVer3:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SUPPLIERVer4
          IF ~SUPPLIERVer5:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~SUPPLIERVer5:Company_Name
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SUBURBVer1
          IF ~SUBURBVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SUPVALAVer1
          IF ~SUPVALAVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SUPVALBVer1
          IF ~SUPVALBVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MODELNUMVer5
          IF ~MODELNUMVer6:Model_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= TRDBATCHVer1
          IF ~TRDBATCHVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= TRADEACCVer8
          IF ~TRADEACCVer9:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~TRADEACCVer9:Account_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= REQUISITVer1
          IF ~REQUISITVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SUBTRACCVer2
          IF ~SUBTRACCVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~SUBTRACCVer3:Main_Account_Number AND ~SUBTRACCVer3:Account_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~SUBTRACCVer3:Account_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= WAYCNRVer2
          IF ~WAYCNRVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MANUFACTVer10
          IF ~MANUFACTVer11:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~MANUFACTVer11:Manufacturer
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF LOC:byTPSDriver
          ADD(LOC:rfDestFileLabel)
        ELSE
          APPEND(LOC:rfDestFileLabel)
        END
        IF ERRORCODE()
          DO PreReturn
          ROU:byError = 1
          POST(EVENT:Completed)
          BREAK
        END
        IF LOC:byTPSDriver
          ROU:lConvertedRecNum += 1
          IF ROU:lConvertedRecNum % 1000 = 0
            COMMIT
            IF ERRORCODE()
              DO PreReturn
              ROU:byError = 1
              POST(EVENT:Completed)
              BREAK
            END
            LOC:byTransactionActive = False
            LOGOUT(1,LOC:rfDestFileLabel)
            IF ERRORCODE()
              DO PreReturn
              ROU:byError = 1
              POST(EVENT:Completed)
              BREAK
            END
            LOC:byTransactionActive = True
          END
        END
        RecordProcessed += 1
        DO DisplaySolidProgressBar
        InfoString = 'Records processed ' & RecordProcessed & ' from ' & RecordTotal
        DISPLAY
        ROU:lRecordsThisCycle += 1
      END
    END
  END
  IF ROU:byError = 0
    IF LOC:byTPSDriver
      COMMIT
      IF ERRORCODE() THEN DO PreReturn; EXIT.
      LOC:byTransactionActive = False
    ELSE
      InfoString = 'Now rebuild keys...'
      DISPLAY
      IF ~LOC:byDestSQLDriver THEN FLUSH(LOC:rfDestFileLabel).
      BUILD(LOC:rfDestFileLabel)
    END

    ReturnCode = 0
    LocalResponse = RequestCompleted
    DO PreReturn
  END
  EXIT
!----------------------------------------------------------

PreReturn ROUTINE
  DATA
ROU:lErrorCode        LONG,AUTO
ROU:stTempFile        STRING(260),AUTO
  CODE
  SETCURSOR
  IF ReturnCode
    IF LOC:byTPSDriver AND LOC:byTransactionActive
      ROU:lErrorCode = ERRORCODE()
      ROLLBACK
      DC:SetError(ROU:lErrorCode)
      DC:SetErrorFile(LOC:rfSourceFileLabel)
    END
    IF ~LOC:byDestSQLDriver AND ~LOC:byTPSDriver THEN FLUSH(LOC:rfDestFileLabel).
    FileErrorBox()
  END
  CLOSE(LOC:rfSourceFileLabel)
  CLOSE(LOC:rfDestFileLabel)
  IF ReturnCode
    DO RemoveTempFile
  ELSE
    IF LOC:bySourceSQLDriver
      ROU:stTempFile = LOC:stSourceFileName
      CreateTempName(ROU:stTempFile,FORMAT(Version,@N03),LOC:rfSourceFileLabel)
      DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    ELSE
      ROU:stTempFile = LOC:stSourceFileName
      CreateTempName(ROU:stTempFile,FORMAT(Version,@N03),LOC:rfSourceFileLabel)
      RemoveFiles(LOC:rfSourceFileLabel,ROU:stTempFile)
      IF ~PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:stTargetFileName,DummyFileName1)
        IF PrepareMultiTableFileName(LOC:rfSourceFileLabel,ROU:stTempFile,DummyFileName1)
          IF PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:rfSourceFileLabel{PROP:Name},DummyFileName1)
            InfoString = 'File copying...'
            DISPLAY
          END
        END
      END
      IF RenameDestFile(LOC:rfSourceFileLabel,ROU:stTempFile,LOC:stTargetFileName)
        ROU:lErrorCode = ERRORCODE()
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    END
    IF LOC:byDestSQLDriver
      LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
      DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        IF LOC:bySourceSQLDriver
          DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
        ELSE
          LOC:rfSourceFileLabel{PROP:Name} = ROU:stTempFile
          RenameFile(LOC:rfSourceFileLabel,LOC:stSourceFileName)
        END
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    ELSE
      LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
      RemoveFiles(LOC:rfDestFileLabel,LOC:stTargetFileName)
      RenameFile(LOC:rfDestFileLabel,LOC:stTargetFileName)
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        IF LOC:bySourceSQLDriver
          DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
        ELSE
          LOC:rfSourceFileLabel{PROP:Name} = ROU:stTempFile
          RenameFile(LOC:rfSourceFileLabel,LOC:stSourceFileName)
        END
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    END
    IF LOC:byDestSQLDriver
      DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
    ELSE
      RemoveFiles(LOC:rfDestFileLabel,LOC:stDestFileName)
    END
  END
  EXIT
!----------------------------------------------------------

RemoveTempFile ROUTINE
  IF LOC:byDestSQLDriver
    DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
  ELSE
    LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
    REMOVE(LOC:rfDestFileLabel)
  END
  EXIT
!----------------------------------------------------------

InitalizeSolidProgressBar ROUTINE
  DATA
ROU:lPercentControlWidth   LONG
ROU:lPercentControlHeight  LONG
ROU:lProgressControlHeight LONG
ROU:byOldPropPixels        BYTE
  CODE
  ROU:byOldPropPixels = window{PROP:Pixels}
  window{PROP:Buffer} = True
  window{PROP:Pixels} = True
  ?RecordProcessed{PROP:Hide} = True

  SPBBorderControl = CREATE(0,CREATE:Panel,?RecordProcessed{PROP:Parent})
  SPBBorderControl{PROP:Xpos}   = ?RecordProcessed{PROP:Xpos}
  SPBBorderControl{PROP:Ypos}   = ?RecordProcessed{PROP:Ypos}
  SPBBorderControl{PROP:Width}  = ?RecordProcessed{PROP:Width}
  SPBBorderControl{PROP:Height} = ?RecordProcessed{PROP:Height}
  IF ?RecordProcessed{PROP:Background} <> 0FFFFFFFFH
    SPBBorderControl{PROP:Fill} = ?RecordProcessed{PROP:Background}
  END
  SPBBorderControl{PROP:BevelOuter} = -1
  SPBBorderControl{PROP:Hide} = False

  SPBFillControl = CREATE(0,CREATE:Box,?RecordProcessed{PROP:Parent})
  SPBFillControl{PROP:Xpos}   = ?RecordProcessed{PROP:Xpos} + 1
  SPBFillControl{PROP:Ypos}   = ?RecordProcessed{PROP:Ypos} + 1
  SPBFillControl{PROP:Width}  = 0
  SPBFillControl{PROP:Height} = ?RecordProcessed{PROP:Height} - 1
  IF ?RecordProcessed{PROP:SelectedFillColor} <> 0FFFFFFFFH
    SPBFillControl{PROP:Fill} = ?RecordProcessed{PROP:SelectedFillColor}
  ELSE
    SPBFillControl{PROP:Fill} = 08000000DH
  END
  SPBFillControl{PROP:Hide} = False

  SPBPercentControl = CREATE(0,CREATE:String,?RecordProcessed{PROP:Parent})
  SPBPercentControl{PROP:FontStyle} = FONT:bold
  SPBPercentControl{PROP:FontSize}  = ?RecordProcessed{PROP:Height}
  LOOP
    ROU:lPercentControlHeight  = SPBPercentControl{PROP:Height}
    ROU:lProgressControlHeight = ?RecordProcessed{PROP:Height}
    IF ROU:lPercentControlHeight <= ROU:lProgressControlHeight OR SPBPercentControl{PROP:FontSize} = 1
      BREAK
    END
    IF SPBPercentControl{PROP:FontSize} < 8
      SPBPercentControl{PROP:FontName} = 'Small Fonts'
    END
    SPBPercentControl{PROP:FontSize} = SPBPercentControl{PROP:FontSize} - 1
  END

  SPBPercentControl{PROP:Text}      = '100%'
  ROU:lPercentControlWidth          = SPBPercentControl{PROP:Width}
  SPBPercentControl{PROP:Xpos}      = ?RecordProcessed{PROP:Xpos} + (?RecordProcessed{PROP:Width} - ROU:lPercentControlWidth) / 2
  SPBPercentControl{PROP:Width}     = ROU:lPercentControlWidth
  SPBPercentControl{PROP:Ypos}      = ?RecordProcessed{PROP:Ypos} + (?RecordProcessed{PROP:Height} - SPBPercentControl{PROP:Height}) / 2 + 1
  IF ?RecordProcessed{PROP:SelectedColor} <> 0FFFFFFFFH
    SPBPercentControl{PROP:FontColor} = ?RecordProcessed{PROP:SelectedColor}
  ELSE
    SPBPercentControl{PROP:FontColor} = 0FFFFFFH
  END
  SPBPercentControl{PROP:Trn}       = True
  SPBPercentControl{PROP:Center}    = True
  SPBPercentControl{PROP:Text}      = '0%'
  SPBPercentControl{PROP:Hide}      = False
  window{PROP:Pixels} = ROU:byOldPropPixels
  EXIT
!---------------------------------------------------------------------

DisplaySolidProgressBar ROUTINE
  DATA
ROU:lRangeLow  LONG,AUTO
ROU:lRangeHigh LONG,AUTO
ROU:lPercent   LONG,AUTO
ROU:lFillBar   LONG,AUTO
  CODE
  IF SaveRecordProcessed <> RecordProcessed
    ROU:lRangeLow  = ?RecordProcessed{PROP:RangeLow}
    ROU:lRangeHigh = ?RecordProcessed{PROP:RangeHigh}
    IF RecordProcessed <= ROU:lRangeHigh
      ROU:lFillBar = (RecordProcessed / (ROU:lRangeHigh - ROU:lRangeLow)) * (?RecordProcessed{PROP:Width} - 1)
      IF ROU:lFillBar <> SaveFillBar
        SPBFillControl{PROP:Width} = ROU:lFillBar
        DISPLAY(SPBFillControl)
        SaveFillBar = ROU:lFillBar
      END
      ROU:lPercent = (RecordProcessed / (ROU:lRangeHigh - ROU:lRangeLow)) * 100
      IF ROU:lPercent <> SavePercent
        SPBPercentControl{PROP:Text} = FORMAT(ROU:lPercent,@N3) & '%'
        DISPLAY(SPBPercentControl)
        SavePercent = ROU:lPercent
      END
      SaveRecordProcessed = RecordProcessed
    ELSE
      RecordProcessed = ROU:lRangeHigh
    END
  END
  EXIT
!---------------------------------------------------------------------
Add_ASV_Code         PROCEDURE                        ! Declare Procedure
  CODE
   Relate:TRADEACC.Open
   Relate:ASVACC.Open
!TB13393 - Apple specific stuff - J - 05/02/15
!All trade accounts need to default to ASV code for APPLE of '0000796914'

    Access:TradeAcc.clearkey(tra:RecordNumberKey)
    Set(tra:RecordNumberKey,tra:RecordNumberKey)
    Loop
        if access:TradeAcc.next() then break.

        Access:ASVACC.clearkey(ASV:TradeACCManufKey)
        ASV:TradeAccNo   = tra:Account_Number
        ASV:Manufacturer = 'APPLE'
        if access:ASVACC.fetch(ASV:TradeACCManufKey)
            !not found - add it
            Access:ASVACC.primerecord()
            ASV:TradeAccNo   = tra:Account_Number
            ASV:Manufacturer = 'APPLE'
            ASV:ASVCode      = '0000796914'
            Access:ASVACC.update()
        END !if record not already found

    END !loop through tradeacc
   Relate:TRADEACC.Close
   Relate:ASVACC.Close
