

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01006.INC'),ONCE        !Local module procedure declarations
                     END


tmp:One   BYTE(1)
Report_Type PROCEDURE (f_ref_number)                  !Generated from procedure template - Window

report_type_temp     STRING(3)
save_retstock_id     USHORT,AUTO
invoice_number_temp  LONG
save_res_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Select Report Type'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Select Report Type'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Select Report Type'),USE(?Tab1)
                           OPTION,AT(248,192,184,32),USE(report_type_temp),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Picking Note'),AT(252,198),USE(?report_type_temp:Radio1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Despatch Note'),AT(312,198),USE(?report_type_temp:Radio2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('Invoice'),AT(384,198),USE(?report_type_temp:Radio3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                             RADIO('Waybill'),AT(252,212),USE(?report_type_temp:Radio4),VALUE('4')
                           END
                         END
                       END
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(300,258),USE(?Print),TRN,FLAT,LEFT,ICON('printp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
windowsdir      CString(260)
systemdir       String(260)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Create_Invoice      Routine
    SystemDir   = GetSystemDirectory(WindowsDir,Size(WindowsDir))
    Set(defaults)
    access:defaults.next()
    invoice_number_temp = 0
    Case Missive('Are you sure you want to reprint the Retail Invoice?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button
            error# = 0
            access:retsales.clearkey(ret:ref_number_key)
            ret:ref_number = f_ref_number
            if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
                If ret:invoice_number <> '' and error# = 0
                    Retail_Single_Invoice(1,ret:Invoice_Number)   !1 implies reprint 0 a first time print
                Else!If ret:invoice_number <> ''
                    Case Missive('The selected sale has not been invoiced.'&|
                      '|To create a new invoice, edit this sale and click "Print Invoice".','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
!                    error# = 0
!                    If error# = 0
!                        fetch_error# = 0
!                        despatch# = 0
!                        UseEuro# = 0
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = ret:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                            Case Missive('Error! Cannot find Sub Account.','ServiceBase 3g',|
!                                           'mstop.jpg','/OK')
!                                Of 1 ! OK Button
!                            End ! Case Missive
!                        Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                                Case Missive('Error! Cannot find Trade Account.','ServiceBase 3g',|
!                                               'mstop.jpg','/OK')
!                                    Of 1 ! OK Button
!                                End ! Case Missive
!                                fetch_error# = 1
!                            Else!if access:tradeacc.fetch(tra:account_number_key)
!                                If tra:use_sub_accounts = 'YES'
!                                    If sub:despatch_invoiced_jobs = 'YES'
!                                        If sub:despatch_paid_jobs = 'YES'
!    !                                        If ret:paid = 'YES'
!    !                                            despatch# = 1
!    !                                        Else
!    !                                            despatch# = 2
!    !                                        End
!                                        Else
!                                            despatch# = 1
!                                        End!If sub:despatch_paid_jobs = 'YES' And ret:paid = 'YES'
!
!                                    End!If sub:despatch_invoiced_jobs = 'YES'
!                                End!If tra:use_sub_accounts = 'YES'
!                                if tra:invoice_sub_accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
!                                    If sub:EuroApplies
!                                        UseEuro# = 1
!                                    End !If sub:EuroApplies
!                                    vat_number" = sub:vat_number
!                                    access:vatcode.clearkey(vat:vat_code_key)
!                                    vat:vat_code = sub:retail_vat_code
!                                    if access:vatcode.fetch(vat:vat_code_key)
!                                        Case Missive('Error! A V.A.T. Rate has not been setup for this Trade Account.','ServiceBase 3g',|
!                                                       'mstop.jpg','/OK')
!                                            Of 1 ! OK Button
!                                        End ! Case Missive
!                                        fetch_error# = 1
!                                    Else!if access:vatcode.fetch(vat:vat_code_key)
!                                       retail_rate$ = vat:vat_rate
!                                    end!if access:vatcode.fetch(vat:vat_code_key)
!                                else!if tra:use_sub_accounts = 'YES'
!                                    If tra:EuroApplies
!                                        UseEuro# = 1
!                                    End !If tra:EuroApplies
!                                    If tra:despatch_invoiced_jobs = 'YES'
!                                        If tra:despatch_paid_jobs = 'YES'
!    !                                        If ret:paid = 'YES'
!    !                                            despatch# = 1
!    !                                        Else
!    !                                            despatch# = 2
!    !                                        End!If ret:paid = 'YES'
!                                        Else
!                                            despatch# = 1
!                                        End!If tra:despatch_paid_jobs = 'YES' and ret:paid = 'YES'
!                                    End!If tra:despatch_invoiced_jobs = 'YES'
!                                    vat_number" = tra:vat_number
!                                    access:vatcode.clearkey(vat:vat_code_key)
!                                    vat:vat_code = tra:retail_vat_code
!                                    if access:vatcode.fetch(vat:vat_code_key)
!                                        fetch_error# = 1
!                                        Case Missive('Error! A V.A.T. Rate has not been setup for this Trade Account.','ServiceBase 3g',|
!                                                       'mstop.jpg','/OK')
!                                            Of 1 ! OK Button
!                                        End ! Case Missive
!                                    Else!if access:vatcode.fetch(vat:vat_code_key)
!                                       retail_rate$ = vat:vat_rate
!                                    end
!                                end!if tra:use_sub_accounts = 'YES'
!                            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
!                        end!if access:subtracc.fetch(sub:account_number_key) = level:benign
!                        If fetch_error# = 0
!        !SAGE BIT
!                            If def:use_sage = 'YES'
!    !                            file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
!    !                            Remove(paramss)
!    !                            access:paramss.open()
!    !                            access:paramss.usefile()
!    !         !LABOUR
!    !                            get(paramss,0)
!    !                            if access:paramss.primerecord() = Level:Benign
!    !                                prm:account_ref       = Stripcomma(ret:Account_number)
!    !                                prm:name              = Stripcomma(ret:company_name)
!    !                                prm:address_1         = Stripcomma(ret:address_line1)
!    !                                prm:address_2         = Stripcomma(ret:address_line2)
!    !                                prm:address_3         = Stripcomma(ret:address_line3)
!    !                                prm:address_4         = ''
!    !                                prm:address_5         = Stripcomma(ret:postcode)
!    !                                prm:del_address_1     = Stripcomma(ret:address_line1_delivery)
!    !                                prm:del_address_2     = Stripcomma(ret:address_line2_delivery)
!    !                                prm:del_address_3     = Stripcomma(ret:address_line3_delivery)
!    !                                prm:del_address_4     = ''
!    !                                prm:del_address_5     = Stripcomma(ret:postcode_delivery)
!    !                                prm:cust_tel_number   = Stripcomma(ret:telephone_number)
!    !                                prm:contact_name      = Stripcomma(ret:contact_name)
!    !                                prm:notes_1           = ''
!    !                                prm:notes_2           = Stripcomma(Stripreturn(ret:invoice_text))
!    !                                prm:notes_3           = ''
!    !                                prm:taken_by          = Stripcomma(ret:who_booked)
!    !                                prm:order_number      = Stripcomma(ret:purchase_order_number)
!    !                                prm:cust_order_number = 'J' & Stripcomma(ret:ref_number)
!    !                                prm:payment_ref       = ''
!    !                                prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
!    !                                prm:global_details    = ''
!    !                                prm:items_net         = Stripcomma(ret:labour_cost + ret:parts_cost + ret:courier_cost)
!    !                                prm:items_tax         = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01) + |
!    !                                                            Round(ret:parts_cost * (parts_rate$/100),.01) + |
!    !                                                            Round(ret:courier_cost * (labour_rate$/100),.01))
!    !                                prm:stock_code        = Stripcomma(DEF:Labour_Stock_Code)
!    !                                prm:description       = Stripcomma(DEF:Labour_Description)
!    !                                prm:nominal_code      = Stripcomma(DEF:Labour_Code)
!    !                                prm:qty_order         = '1'
!    !                                prm:unit_price        = '0'
!    !                                prm:net_amount        = Stripcomma(ret:labour_cost)
!    !                                prm:tax_amount        = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01))
!    !                                prm:comment_1         = Stripcomma(ret:charge_type)
!    !                                prm:comment_2         = Stripcomma(ret:warranty_charge_type)
!    !                                prm:unit_of_sale      = '0'
!    !                                prm:full_net_amount   = '0'
!    !                                prm:invoice_date      = '*'
!    !                                prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
!    !                                prm:user_name         = Clip(DEF:User_Name_Sage)
!    !                                prm:password          = Clip(DEF:Password_Sage)
!    !                                prm:set_invoice_number= '*'
!    !                                prm:invoice_no        = '*'
!    !                                access:paramss.insert()
!    !                            end!if access:paramss.primerecord() = Level:Benign
!    !        !PARTS
!    !                            get(paramss,0)
!    !                            if access:paramss.primerecord() = Level:Benign
!    !                                prm:account_ref       = Stripcomma(ret:Account_number)
!    !                                prm:name              = Stripcomma(ret:company_name)
!    !                                prm:address_1         = Stripcomma(ret:address_line1)
!    !                                prm:address_2         = Stripcomma(ret:address_line2)
!    !                                prm:address_3         = Stripcomma(ret:address_line3)
!    !                                prm:address_4         = ''
!    !                                prm:address_5         = Stripcomma(ret:postcode)
!    !                                prm:del_address_1     = Stripcomma(ret:address_line1_delivery)
!    !                                prm:del_address_2     = Stripcomma(ret:address_line2_delivery)
!    !                                prm:del_address_3     = Stripcomma(ret:address_line3_delivery)
!    !                                prm:del_address_4     = ''
!    !                                prm:del_address_5     = Stripcomma(ret:postcode_delivery)
!    !                                prm:cust_tel_number   = Stripcomma(ret:telephone_number)
!    !                                If ret:surname <> ''
!    !                                    if ret:title = '' and ret:initial = ''
!    !                                        prm:contact_name = Stripcomma(clip(ret:surname))
!    !                                    elsif ret:title = '' and ret:initial <> ''
!    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:initial))
!    !                                    elsif ret:title <> '' and ret:initial = ''
!    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title))
!    !                                    elsif ret:title <> '' and ret:initial <> ''
!    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title) & ' ' & clip(ret:initial))
!    !                                    else
!    !                                        prm:contact_name = ''
!    !                                    end
!    !                                else
!    !                                    prm:contact_name = ''
!    !                                end
!    !                                prm:notes_1           = Stripcomma(Stripreturn(ret:fault_description))
!    !                                prm:notes_2           = Stripcomma(Stripreturn(ret:invoice_text))
!    !                                prm:notes_3           = ''
!    !                                prm:taken_by          = Stripcomma(ret:who_booked)
!    !                                prm:order_number      = Stripcomma(ret:order_number)
!    !                                prm:cust_order_number = 'J' & Stripcomma(ret:ref_number)
!    !                                prm:payment_ref       = ''
!    !                                prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
!    !                                prm:global_details    = ''
!    !                                prm:items_net         = Stripcomma(ret:labour_cost + ret:parts_cost + ret:courier_cost)
!    !                                prm:items_tax         = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01) + |
!    !                                                            Round(ret:parts_cost * (parts_rate$/100),.01) + |
!    !                                                            Round(ret:courier_cost * (labour_rate$/100),.01))
!    !                                prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
!    !                                prm:description       = Stripcomma(DEF:Parts_Description)
!    !                                prm:nominal_code      = Stripcomma(DEF:Parts_Code)
!    !                                prm:qty_order         = '1'
!    !                                prm:unit_price        = '0'
!    !                                prm:net_amount        = Stripcomma(ret:parts_cost)
!    !                                prm:tax_amount        = Stripcomma(Round(ret:parts_cost * (parts_rate$/100),.01))
!    !                                prm:comment_1         = Stripcomma(ret:charge_type)
!    !                                prm:comment_2         = Stripcomma(ret:warranty_charge_type)
!    !                                prm:unit_of_sale      = '0'
!    !                                prm:full_net_amount   = '0'
!    !                                prm:invoice_date      = '*'
!    !                                prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
!    !                                prm:user_name         = Clip(DEF:User_Name_Sage)
!    !                                prm:password          = Clip(DEF:Password_Sage)
!    !                                prm:set_invoice_number= '*'
!    !                                prm:invoice_no        = '*'
!    !                                access:paramss.insert()
!    !                            end!if access:paramss.primerecord() = Level:Benign
!    !        !COURIER
!    !                            get(paramss,0)
!    !                            if access:paramss.primerecord() = Level:Benign
!    !                                prm:account_ref       = Stripcomma(ret:Account_number)
!    !                                prm:name              = Stripcomma(ret:company_name)
!    !                                prm:address_1         = Stripcomma(ret:address_line1)
!    !                                prm:address_2         = Stripcomma(ret:address_line2)
!    !                                prm:address_3         = Stripcomma(ret:address_line3)
!    !                                prm:address_4         = ''
!    !                                prm:address_5         = Stripcomma(ret:postcode)
!    !                                prm:del_address_1     = Stripcomma(ret:address_line1_delivery)
!    !                                prm:del_address_2     = Stripcomma(ret:address_line2_delivery)
!    !                                prm:del_address_3     = Stripcomma(ret:address_line3_delivery)
!    !                                prm:del_address_4     = ''
!    !                                prm:del_address_5     = Stripcomma(ret:postcode_delivery)
!    !                                prm:cust_tel_number   = Stripcomma(ret:telephone_number)
!    !                                If ret:surname <> ''
!    !                                    if ret:title = '' and ret:initial = ''
!    !                                        prm:contact_name = Stripcomma(clip(ret:surname))
!    !                                    elsif ret:title = '' and ret:initial <> ''
!    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:initial))
!    !                                    elsif ret:title <> '' and ret:initial = ''
!    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title))
!    !                                    elsif ret:title <> '' and ret:initial <> ''
!    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title) & ' ' & clip(ret:initial))
!    !                                    else
!    !                                        prm:contact_name = ''
!    !                                    end
!    !                                else
!    !                                    prm:contact_name = ''
!    !                                end
!    !                                prm:notes_1           = Stripcomma(Stripreturn(ret:fault_description))
!    !                                prm:notes_2           = Stripcomma(Stripreturn(ret:invoice_text))
!    !                                prm:notes_3           = ''
!    !                                prm:taken_by          = Stripcomma(ret:who_booked)
!    !                                prm:order_number      = Stripcomma(ret:order_number)
!    !                                prm:cust_order_number = 'J' & Stripcomma(ret:ref_number)
!    !                                prm:payment_ref       = ''
!    !                                prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
!    !                                prm:global_details    = ''
!    !                                prm:items_net         = Stripcomma(ret:labour_cost + ret:parts_cost + ret:courier_cost)
!    !                                prm:items_tax         = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01) + |
!    !                                                            Round(ret:parts_cost * (parts_rate$/100),.01) + |
!    !                                                            Round(ret:courier_cost * (labour_rate$/100),.01))
!    !                                prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
!    !                                prm:description       = Stripcomma(DEF:Courier_Description)
!    !                                prm:nominal_code      = Stripcomma(DEF:Courier_Code)
!    !                                prm:qty_order         = '1'
!    !                                prm:unit_price        = '0'
!    !                                prm:net_amount        = Stripcomma(ret:courier_cost)
!    !                                prm:tax_amount        = Stripcomma(Round(ret:courier_cost * (labour_rate$/100),.01))
!    !                                prm:comment_1         = Stripcomma(ret:charge_type)
!    !                                prm:comment_2         = Stripcomma(ret:warranty_charge_type)
!    !                                prm:unit_of_sale      = '0'
!    !                                prm:full_net_amount   = '0'
!    !                                prm:invoice_date      = '*'
!    !                                prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
!    !                                prm:user_name         = Clip(DEF:User_Name_Sage)
!    !                                prm:password          = Clip(DEF:Password_Sage)
!    !                                prm:set_invoice_number= '*'
!    !                                prm:invoice_no        = '*'
!    !                                access:paramss.insert()
!    !                            end!if access:paramss.primerecord() = Level:Benign
!    !                            access:paramss.close()
!    !                            Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
!    !                            sage_error# = 0
!    !                            access:paramss.open()
!    !                            access:paramss.usefile()
!    !                            Set(paramss,0)
!    !                            If access:paramss.next()
!    !                                sage_error# = 1
!    !                            Else!If access:paramss.next()
!    !                                If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
!    !                                    sage_error# = 1
!    !                                Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
!    !                                    invoice_number_temp = prm:invoice_no
!    !                                    If invoice_number_temp = 0
!    !                                        sage_error# = 1
!    !                                    End!If invoice_number_temp = 0
!    !                                End!If prm:invoice_no = '-1'
!    !                            End!If access:paramss.next()
!    !                            access:paramss.close()
!                            End!If def:use_sage = 'YES'
!                            If sage_error# = 0
!                                invoice_error# = 1
!                                get(invoice,0)
!                                if access:invoice.primerecord() = level:benign
!    !                                If def:use_sage = 'YES'
!    !                                    inv:invoice_number = invoice_number_temp
!    !                                End!If def:use_sage = 'YES'
!                                    inv:invoice_type       = 'RET'
!                                    inv:job_number         = ret:ref_number
!                                    inv:date_created       = Today()
!                                    inv:account_number     = ret:account_number
!                                    inv:total              = ret:sub_total
!                                    inv:vat_rate_labour    = ''
!                                    inv:vat_rate_parts     = ''
!                                    inv:vat_rate_retail    = retail_rate$
!                                    inv:vat_number         = def:vat_number
!                                    INV:Courier_Paid       = ret:courier_cost
!                                    inv:parts_paid         = ret:parts_cost
!                                    inv:labour_paid        = ''
!                                    inv:invoice_vat_number = vat_number"
!                                    If UseEuro#
!                                        inv:UseAlternativeAddress   = 1
!                                        inv:EuroExhangeRate = def:EuroRate
!                                    Else
!                                        inv:UseAlternativeAddress   = 0
!                                        inv:EuroExhangeRate = def:EuroRate
!                                    End !If UseEuro#
!                                    If access:invoice.insert()
!                                        invoice_error# = 1
!                                        access:invoice.cancelautoinc()
!                                    End!If access:invoice.insert()
!                                    invoice_error# = 0
!                                    If access:invoice.insert()
!                                        invoice_error# = 1
!                                        access:invoice.cancelautoinc()
!                                    End!If access:invoice.insert()
!                                end!if access:invoice.primerecord() = level:benign
!                                If Invoice_error# = 0
!    !                                Case despatch#
!    !                                    Of 1
!    !                                        If ret:despatched = 'YES'
!    !                                            Status_Routine(710,ret:current_status,a",a",a")
!    !                                        Else!If ret:despatched = 'YES'
!    !                                            Status_Routine(810,ret:current_status,a",a",a")
!    !                                            ret:despatched = 'REA'
!    !                                            ret:despatch_type = 'JOB'
!    !                                            ret:current_courier = ret:courier
!    !                                        End!If ret:despatched <> 'YES'
!    !                                    Of 2
!    !                                        Status_Routine(803,ret:current_status,a",a",a")
!    !                                    Else
!    !                                        Status_Routine(710,ret:current_status,a",a",a")
!    !
!    !                                End!Case despatch#
!
!                                    ret:invoice_number        = inv:invoice_number
!                                    ret:invoice_date          = Today()
!                                    ret:invoice_courier_cost  = ret:courier_cost
!                                    ret:invoice_parts_cost    = ret:parts_cost
!                                    ret:invoice_sub_total     = ret:sub_total
!                                    access:retsales.update()
!                                    glo:select1 = inv:invoice_number
!                                    Retail_Single_Invoice(0)   !0 implies first time invoice
!                                    glo:select1 = ''
!                                End!If Invoice_error# = 0
!                            End!If def:use_sage = 'YES' and sage_error# = 1
!                        End!If fetch_error# = 0
!                    End!If error# = 0
                End!If ret:invoice_number <> ''
            end!if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign

        Of 1 ! No Button
    End ! Case Missive
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020350'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Report_Type')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RETDESNO.Open
  Relate:VATCODE.Open
  Relate:WAYBILLJ.Open
  Access:RETSTOCK.UseFile
  Access:RETSALES.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:WAYBILLS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Report_Type')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RETDESNO.Close
    Relate:VATCODE.Close
    Relate:WAYBILLJ.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Report_Type')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020350'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020350'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020350'&'0')
      ***
    OF ?Print
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
      Case report_type_temp
          Of 1 !Picking Note
              Set(DEFAULTS)
              Access:DEFAULTS.Next()
              continue# = 0
              setcursor(cursor:wait)
              save_res_id = access:retstock.savefile()
              access:retstock.clearkey(res:part_number_key)
              res:ref_number  = f_ref_number
              set(res:part_number_key,res:part_number_key)
              loop
                  if access:retstock.next()
                     break
                  end !if
                  if res:ref_number  <> f_ref_number      |
                      then break.  ! end if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  If res:despatched <> 'YES'
                      continue# = 1
                      Break
                  End!If res:despatched <> 'YES'
              end !loop
              access:retstock.restorefile(save_res_id)
              setcursor()

              If continue# = 1
                  glo:select1  = f_ref_number
                  Retail_Picking_Note('PIK')
                  glo:select1  = ''
              Else!If continue# = 1
                  Case Missive('All the items have been despatched.'&|
                    '<13,10>'&|
                    '<13,10>Do you wish to reprint the original Picking Note?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          glo:select1  = f_ref_number
                          Retail_Picking_Note('ALL')
                          glo:select1  = ''
                      Of 1 ! No Button
                  End ! Case Missive
              End!If continue# = 1
          Of 2 !Despatch Note
              access:retsales.clearkey(ret:ref_number_key)
              ret:ref_number = f_ref_number
              if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
                  If ret:despatch_number <> ''
                      
                      Retail_Despatch_Note(ret:Despatch_Number)
                      
                  Else!If ret:despatch_number <> ''
                      get(retdesno,0)
                      if access:retdesno.primerecord() = Level:Benign
                          RDN:Consignment_Number  = ''
                          RDN:Courier             = ret:courier
                          rdn:sale_number         = ret:ref_number
                          access:retdesno.insert()

                          save_res_id = access:retstock.savefile()
                          access:retstock.clearkey(res:part_number_key)
                          res:ref_number  = ret:ref_number
                          set(res:part_number_key,res:part_number_key)
                          loop
                              if access:retstock.next()
                                 break
                              end !if
                              if res:ref_number  <> ret:ref_number      |
                                  then break.  ! end if
                              If res:pending_ref_number <> ''
                                  res:despatched  = 'PEN'
                              Else!If res:pending_ref_number <> ''
                                  res:despatched = 'YES'
                              End!If res:pending_ref_number <> ''

                              access:retstock.update
                          end !loop
                          access:retstock.restorefile(save_res_id)

                          ret:despatched = 'PRO'
                          ret:despatch_number = rdn:despatch_number
                          access:retsales.update()

                          
                          Retail_Despatch_Note(rdn:Despatch_Number)
                          
                          thiswindow.reset(1)
                      End!if access:retdesno.primerecord() = Level:Benign
                  End!If ret:despatch_number <> ''

              End!if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign


          Of 3 !Invoice
              Do Create_Invoice
          Of 4 !Waybill
              ! Inserting (DBH 23/08/2006) # 7964 - Print a retail waybill
! Deleting (DBH 20/10/2006) # 838 - just show despatched items
!              Found# = False
!              Save_RETSTOCK_ID = Access:RETSTOCK.SaveFile()
!              Access:RETSTOCK.Clearkey(res:Part_Number_Key)
!              res:Ref_Number = f_Ref_Number
!              Set(res:Part_Number_Key,res:Part_Number_Key)
!              Loop ! Begin Loop
!                  If Access:RETSTOCK.Next()
!                      Break
!                  End ! If Access:RETSTOCK.Next()
!                  If res:Ref_Number <> f_Ref_Number
!                      Break
!                  End ! If res:Ref_Number <> f_Ref_Number
!                  If res:Despatched <> 'YES'
!                      Found# = True
!                      Break
!                  End ! If res:Despatched <> 'YES'
!              End ! Loop
!              Access:RETSTOCK.RestoreFile(Save_RETSTOCK_ID)
!
!              If Found# = True
!                  Type" = 'PIK'
!              Else ! If Found# = True
!                  Type" = 'ALL'
!              End ! If Found# = True
! End (DBH 20/10/2006) #838
              Access:RETSALES.ClearKey(ret:Ref_Number_Key)
              ret:ref_Number = f_Ref_Number
              If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                  !Found
                  If ret:WaybillNumber > 0
                      glo:Select1 = ret:WayBillNumber
                      WaybillRetail
                      glo:Select1 = ''
                  Else ! If ret:WaybillNumber > 0
                      ret:WaybillNumber = NextWayBillNumber()
                      If ret:WayBillNumber > 0
                          Access:RETSALES.Update()
                          Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
                          way:WaybillNumber = ret:WayBillNumber
                          If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
                              !Found
                              way:AccountNumber = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                              way:WaybillID = 200
                              way:WayBillType = 10
                              way:FromAccount = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                              way:ToAccount = ret:Account_Number
                              If Access:WAYBILLS.Update() = Level:Benign
                                  If Access:WAYBILLJ.PrimeRecord() = Level:Benign
                                      waj:WayBillNumber = way:WayBillNumber
                                      waj:JobNumber = ret:Ref_Number
                                      waj:OrderNumber = ret:Purchase_Order_Number
                                      waj:JobType = 'RET'
                                      If Access:WAYBILLJ.TryInsert() = Level:Benign
                                          !Insert
                                          glo:Select1 = way:WayBillNumber
                                          WaybillRetail
                                          glo:Select1 = ''
                                      Else ! If Access:WAYBILLJ.TryInsert() = Level:Benign
                                          Access:WAYBILLJ.CancelAutoInc()
                                      End ! If Access:WAYBILLJ.TryInsert() = Level:Benign
                                  End ! If Access.WAYBILLJ.PrimeRecord() = Level:Benign
                              End ! If Access:WAYBILLS.Update() = Level:Benign

                          Else ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
                              !Error
                          End ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
                      End ! If tmp:NewWayBillNumber > 0

                  End ! If ret:WaybillNumber > 0
              Else ! If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                  !Error
              End ! If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
              ! End (DBH 23/08/2006) #7964
      End!Case report_type_temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Browse_All_Retail_Sales PROCEDURE                     !Generated from procedure template - Window

CurrentTab           STRING(80)
save_retstock_id     USHORT,AUTO
save_res_id          USHORT,AUTO
save_ret_ali_id      USHORT,AUTO
Account_Number       STRING(15)
RET_temp             STRING('RET')
no_value_temp        REAL
tag_temp             STRING(1)
stop_on_errors_temp  BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:AccountNoFilter  STRING(15)
BRW1::View:Browse    VIEW(RETSALES)
                       PROJECT(ret:Ref_Number)
                       PROJECT(ret:Purchase_Order_Number)
                       PROJECT(ret:Account_Number)
                       PROJECT(ret:date_booked)
                       PROJECT(ret:Invoice_Date)
                       PROJECT(ret:who_booked)
                       PROJECT(ret:Courier)
                       PROJECT(ret:Invoice_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ret:Ref_Number         LIKE(ret:Ref_Number)           !List box control field - type derived from field
ret:Purchase_Order_Number LIKE(ret:Purchase_Order_Number) !List box control field - type derived from field
ret:Account_Number     LIKE(ret:Account_Number)       !List box control field - type derived from field
ret:date_booked        LIKE(ret:date_booked)          !List box control field - type derived from field
ret:Invoice_Date       LIKE(ret:Invoice_Date)         !List box control field - type derived from field
ret:who_booked         LIKE(ret:who_booked)           !List box control field - type derived from field
ret:Courier            LIKE(ret:Courier)              !List box control field - type derived from field
ret:Invoice_Number     LIKE(ret:Invoice_Number)       !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse All Retail Sales'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Retail Sale File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(68,84,472,274),USE(?Browse:1),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('39R(2)|M~Sales No~L@s8@92L(2)|M~Purchase Order Number~@s30@63L(2)|M~Account Numb' &|
   'er~@s15@43R(2)|M~Date Raised~@d6b@41R(2)|M~Inv Date~@d6b@22L(2)|M~User~@s3@80L(2' &|
   ')|M~Courier~@s30@38L(2)|M~Inv No~@s8@'),FROM(Queue:Browse:1)
                       BUTTON('&Insert'),AT(368,224,42,12),USE(?Insert),HIDE
                       BUTTON,AT(548,302),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                       BUTTON,AT(548,330),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       BUTTON,AT(548,132),USE(?Print),TRN,FLAT,LEFT,ICON('prnroutp.jpg')
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Sale Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(68,70,64,10),USE(ret:Ref_Number),RIGHT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('Purchase Order No'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(68,70,124,10),USE(ret:Purchase_Order_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Invoice Number'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(68,70,64,10),USE(ret:Invoice_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Account Number'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(68,72,64,10),USE(ret:Ref_Number,,?RET:Ref_Number:2),RIGHT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s15),AT(136,72,124,10),USE(Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(264,68),USE(?LookupSubAccount),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 and glo:WebJob = 0
BRW1::Sort5:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 and glo:WebJob = 1
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 and glo:WebJob = 0
BRW1::Sort7:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 and glo:WebJob = 1
BRW1::Sort4:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 and glo:WebJob = 0
BRW1::Sort6:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 and glo:WebJob = 1
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
BRW1::Sort1:StepClass StepLongClass                   !Conditional Step Manager - Choice(?CurrentTab) = 1 and glo:WebJob = 0
BRW1::Sort2:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 and glo:WebJob = 0
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
windowsdir      CString(260)
systemdir       String(260)
!Save Entry Fields Incase Of Lookup
look:Account_Number                Like(Account_Number)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020330'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_All_Retail_Sales')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RETSALES.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:USELEVEL.Open
  Relate:VATCODE.Open
  Access:RETSTOCK.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  ! Hide fields/tab if web job
  
  if glo:webJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          If tra:StoresAccount <> ''
              tmp:AccountNoFilter   = tra:StoresAccount
          Else !If tmp:StoresAccount <> ''
              tmp:AccountNoFilter   = Clarionet:Global.Param2
          End !If tmp:StoresAccount <> ''
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  end
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RETSALES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If glo:WebJob
      ?Tab2{prop:Hide} = 1
      ?Print{prop:Hide} = 1
      ?Delete{prop:Hide} = 1
  End !If glo:WebJob
  ! Save Window Name
   AddToLog('Window','Open','Browse_All_Retail_Sales')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?Account_Number{Prop:Tip} AND ~?LookupSubAccount{Prop:Tip}
     ?LookupSubAccount{Prop:Tip} = 'Select ' & ?Account_Number{Prop:Tip}
  END
  IF ?Account_Number{Prop:Msg} AND ~?LookupSubAccount{Prop:Msg}
     ?LookupSubAccount{Prop:Msg} = 'Select ' & ?Account_Number{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,ret:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?ret:Ref_Number,ret:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,ret:Account_Number_Key)
  BRW1.AddRange(ret:Account_Number,tmp:AccountNoFilter)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?ret:Ref_Number,ret:Ref_Number,1,BRW1)
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,ret:Purchase_Number_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?ret:Purchase_Order_Number,ret:Purchase_Order_Number,1,BRW1)
  BRW1.AddSortOrder(,ret:AccountPurchaseNumberKey)
  BRW1.AddRange(ret:Account_Number,tmp:AccountNoFilter)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?ret:Purchase_Order_Number,ret:Purchase_Order_Number,1,BRW1)
  BRW1.AddSortOrder(,ret:Invoice_Number_Key)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?ret:Invoice_Number,ret:Invoice_Number,1,BRW1)
  BRW1.AddSortOrder(,ret:AccountInvoiceKey)
  BRW1.AddRange(ret:Account_Number,tmp:AccountNoFilter)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?ret:Invoice_Number,ret:Invoice_Number,1,BRW1)
  BRW1.AddSortOrder(,ret:Account_Number_Key)
  BRW1.AddRange(ret:Account_Number,Account_Number)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?RET:Ref_Number:2,ret:Ref_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ret:Account_Number_Key)
  BRW1.AddRange(ret:Account_Number,tmp:AccountNoFilter)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?ret:Ref_Number,ret:Ref_Number,1,BRW1)
  BIND('tmp:AccountNoFilter',tmp:AccountNoFilter)
  BRW1.AddField(ret:Ref_Number,BRW1.Q.ret:Ref_Number)
  BRW1.AddField(ret:Purchase_Order_Number,BRW1.Q.ret:Purchase_Order_Number)
  BRW1.AddField(ret:Account_Number,BRW1.Q.ret:Account_Number)
  BRW1.AddField(ret:date_booked,BRW1.Q.ret:date_booked)
  BRW1.AddField(ret:Invoice_Date,BRW1.Q.ret:Invoice_Date)
  BRW1.AddField(ret:who_booked,BRW1.Q.ret:who_booked)
  BRW1.AddField(ret:Courier,BRW1.Q.ret:Courier)
  BRW1.AddField(ret:Invoice_Number,BRW1.Q.ret:Invoice_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 2
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Sale Number'
    ?Tab3{PROP:TEXT} = 'Purchase Order No'
    ?Tab4{PROP:TEXT} = 'By Invoice Number'
    ?Tab2{PROP:TEXT} = 'By Account Number'
    ?Browse:1{PROP:FORMAT} ='39R(2)|M~Sales No~L@s8@#1#92L(2)|M~Purchase Order Number~@s30@#2#63L(2)|M~Account Number~@s15@#3#43R(2)|M~Date Raised~@d6b@#4#41R(2)|M~Inv Date~@d6b@#5#22L(2)|M~User~@s3@#6#80L(2)|M~Courier~@s30@#7#38L(2)|M~Inv No~@s8@#8#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RETSALES.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:USELEVEL.Close
    Relate:VATCODE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_All_Retail_Sales')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of deleterecord
          check_access('RETAIL SALES - DELETE',x")
          if x" = false
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
      of insertrecord
          do_update# = false
          Case Missive('You cannot insert from here.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
  end !case request
  
  if do_update# = true
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Sub_Accounts
      Process_Retail_Sale
    END
    ReturnValue = GlobalResponse
  END
      ! Inserting (DBH 25/08/2006) # 7964 - Print waybill?
      If glo:Select20 = 'PRINT WAYBILL'
          If glo:Select21 > 0
              glo:Select1 = glo:Select21
! Deleting (DBH 20/10/2006) # 8381 - Only print despatched items
!              Found# = False
!              Save_RETSTOCK_ID = Access:RETSTOCK.SaveFile()
!              Access:RETSTOCK.Clearkey(res:Part_Number_Key)
!              res:Ref_Number = glo:Select22
!              Set(res:Part_Number_Key,res:Part_Number_Key)
!              Loop ! Begin Loop
!                  If Access:RETSTOCK.Next()
!                      Break
!                  End ! If Access:RETSTOCK.Next()
!                  If res:Ref_Number <> glo:Select22
!                      Break
!                  End ! If res:Ref_Number <> f_Ref_Number
!                  If res:Despatched <> 'YES'
!                      Found# = True
!                      Break
!                  End ! If res:Despatched <> 'YES'
!              End ! Loop
!              Access:RETSTOCK.RestoreFile(Save_RETSTOCK_ID)
!
!              If Found# = True
!                  WaybillRetail('PIK')
!              Else ! If Found# = True
! End (DBH 20/10/2006) #8381
                  WaybillRetail
!              End ! If Found# = True
          End ! If glo:Select21 > 0
          glo:Select1 = ''
          glo:Select20 = ''
          glo:Select21 = ''
          glo:Select22 = ''
      End ! If glo:Select20 = 'PRINT WAYBILL'
      ! End (DBH 25/08/2006) #7964
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020330'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020330'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020330'&'0')
      ***
    OF ?Print
      ThisWindow.Update
      Report_Type(ret:ref_number)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
    OF ?Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number, Accepted)
      BRW1.ResetSort(1)
      IF Account_Number OR ?Account_Number{Prop:Req}
        sub:Account_Number = Account_Number
        !Save Lookup Field Incase Of error
        look:Account_Number        = Account_Number
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Account_Number = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            Account_Number = look:Account_Number
            SELECT(?Account_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number, Accepted)
    OF ?LookupSubAccount
      ThisWindow.Update
      sub:Account_Number = Account_Number
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          Account_Number = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?Account_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Account_Number)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='39R(2)|M~Sales No~L@s8@#1#92L(2)|M~Purchase Order Number~@s30@#2#63L(2)|M~Account Number~@s15@#3#43R(2)|M~Date Raised~@d6b@#4#41R(2)|M~Inv Date~@d6b@#5#22L(2)|M~User~@s3@#6#80L(2)|M~Courier~@s30@#7#38L(2)|M~Inv No~@s8@#8#'
          ?Tab:2{PROP:TEXT} = 'By Sale Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='92L(2)|M~Purchase Order Number~@s30@#2#39R(2)|M~Sales No~L@s8@#1#63L(2)|M~Account Number~@s15@#3#43R(2)|M~Date Raised~@d6b@#4#41R(2)|M~Inv Date~@d6b@#5#22L(2)|M~User~@s3@#6#80L(2)|M~Courier~@s30@#7#38L(2)|M~Inv No~@s8@#8#'
          ?Tab3{PROP:TEXT} = 'Purchase Order No'
        OF 3
          ?Browse:1{PROP:FORMAT} ='38L(2)|M~Inv No~@s8@#8#39R(2)|M~Sales No~L@s8@#1#92L(2)|M~Purchase Order Number~@s30@#2#63L(2)|M~Account Number~@s15@#3#43R(2)|M~Date Raised~@d6b@#4#41R(2)|M~Inv Date~@d6b@#5#22L(2)|M~User~@s3@#6#80L(2)|M~Courier~@s30@#7#'
          ?Tab4{PROP:TEXT} = 'By Invoice Number'
        OF 4
          ?Browse:1{PROP:FORMAT} ='39R(2)|M~Sales No~L@s8@#1#92L(2)|M~Purchase Order Number~@s30@#2#63L(2)|M~Account Number~@s15@#3#43R(2)|M~Date Raised~@d6b@#4#41R(2)|M~Inv Date~@d6b@#5#22L(2)|M~User~@s3@#6#80L(2)|M~Courier~@s30@#7#38L(2)|M~Inv No~@s8@#8#'
          ?Tab2{PROP:TEXT} = 'By Account Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 and glo:WebJob = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 and glo:WebJob = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 2 and glo:WebJob = 0
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 and glo:WebJob = 1
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 3 and glo:WebJob = 0
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 3 and glo:WebJob = 1
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 4
    RETURN SELF.SetSort(7,Force)
  ELSE
    RETURN SELF.SetSort(8,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Update_Retail_Payment PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?rtp:Payment_Type
pay:Payment_Type       LIKE(pay:Payment_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(PAYTYPES)
                       PROJECT(pay:Payment_Type)
                     END
History::rtp:Record  LIKE(rtp:RECORD),STATIC
QuickWindow          WINDOW('Update the JOBPAYMT File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Retail Payment'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Payment Type'),AT(238,170),USE(?jpt:payment_type:prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(322,170,124,10),USE(rtp:Payment_Type),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Date'),AT(238,150),USE(?JPT:Date:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6b),AT(322,150,64,10),USE(rtp:Date),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(390,146),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Payment Received'),AT(238,248),USE(?JPT:Amount:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           GROUP,AT(234,186,208,47),USE(?Credit_Card_Group)
                             PROMPT('Credit Card Number'),AT(238,189),USE(?JPT:Credit_Card_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s20),AT(322,189,124,10),USE(rtp:Credit_Card_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             PROMPT('Expiry Date'),AT(238,208),USE(?JPT:Expiry_Date:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@p##/##p),AT(322,208,64,10),USE(rtp:Expiry_Date),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             PROMPT('Issue Number'),AT(238,229),USE(?JPT:Issue_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s5),AT(322,229,64,10),USE(rtp:Issue_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           END
                           ENTRY(@n-14.2),AT(322,248,64,10),USE(rtp:Amount),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Credit_Card_Bit     Routine
    access:paytypes.clearkey(pay:payment_type_key)
    pay:payment_type = rtp:payment_type
    if access:paytypes.fetch(pay:payment_type_key) = Level:Benign
        If pay:credit_card = 'YES'
            Enable(?credit_card_group)
            ?rtp:credit_card_number{prop:req} = 1
            ?rtp:expiry_date{prop:req} = 1
        Else
            Disable(?credit_card_group)
            ?rtp:credit_card_number{prop:req} = 0
            ?rtp:expiry_date{prop:req} = 0
        End
    end
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Payment'
  OF ChangeRecord
    ActionMessage = 'Changing A Payment'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020337'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Retail_Payment')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(rtp:Record,History::rtp:Record)
  SELF.AddHistoryField(?rtp:Payment_Type,4)
  SELF.AddHistoryField(?rtp:Date,3)
  SELF.AddHistoryField(?rtp:Credit_Card_Number,5)
  SELF.AddHistoryField(?rtp:Expiry_Date,6)
  SELF.AddHistoryField(?rtp:Issue_Number,7)
  SELF.AddHistoryField(?rtp:Amount,8)
  SELF.AddUpdateFile(Access:RETPAY)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:PAYTYPES.Open
  Relate:RETPAY.Open
  Access:RETSALES.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETPAY
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If thiswindow.request = Insertrecord
      access:users.clearkey(use:password_key)
      use:password =glo:password
      if access:users.fetch(use:password_key) = Level:Benign
         rtp:user_code = use:user_code
      end
      rtp:amount  = glo:select1
  End!If thiswindow.request = Insertrecord
  ! Save Window Name
   AddToLog('Window','Open','Update_Retail_Payment')
  Bryan.CompFieldColour()
  ?RTP:Date{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB7.Init(rtp:Payment_Type,?rtp:Payment_Type,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:PAYTYPES,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(pay:Payment_Type_Key)
  FDCB7.AddField(pay:Payment_Type,FDCB7.Q.pay:Payment_Type)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PAYTYPES.Close
    Relate:RETPAY.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Retail_Payment')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020337'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020337'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020337'&'0')
      ***
    OF ?rtp:Payment_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rtp:Payment_Type, Accepted)
      Do credit_card_bit
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rtp:Payment_Type, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          rtp:Date = TINCALENDARStyle1()
          Display(?RTP:Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?rtp:Date
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do credit_card_bit
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      FDCB7.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Retail_Payments PROCEDURE (f_ref_number,f_account_number,f_courier_cost,func:Type) !Generated from procedure template - Window

CurrentTab           STRING(80)
vat_rate_temp        REAL
ref_number_temp      REAL
items_total_temp     REAL
sub_total_temp       REAL
vat_temp             REAL
total_temp           REAL
amount_paid_temp     REAL
balance_due_temp     REAL
line_cost_temp       REAL
tmp:Type             STRING(3)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(RETPAY)
                       PROJECT(rtp:Date)
                       PROJECT(rtp:Payment_Type)
                       PROJECT(rtp:Amount)
                       PROJECT(rtp:Record_Number)
                       PROJECT(rtp:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
rtp:Date               LIKE(rtp:Date)                 !List box control field - type derived from field
rtp:Payment_Type       LIKE(rtp:Payment_Type)         !List box control field - type derived from field
rtp:Amount             LIKE(rtp:Amount)               !List box control field - type derived from field
rtp:Record_Number      LIKE(rtp:Record_Number)        !Primary key field - type derived from field
rtp:Ref_Number         LIKE(rtp:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(RETSTOCK)
                       PROJECT(res:Part_Number)
                       PROJECT(res:Description)
                       PROJECT(res:Quantity)
                       PROJECT(res:Item_Cost)
                       PROJECT(res:Record_Number)
                       PROJECT(res:Ref_Number)
                       PROJECT(res:Despatched)
                       PROJECT(res:Despatch_Note_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
res:Part_Number        LIKE(res:Part_Number)          !List box control field - type derived from field
res:Description        LIKE(res:Description)          !List box control field - type derived from field
res:Quantity           LIKE(res:Quantity)             !List box control field - type derived from field
res:Item_Cost          LIKE(res:Item_Cost)            !List box control field - type derived from field
line_cost_temp         LIKE(line_cost_temp)           !List box control field - type derived from local data
res:Record_Number      LIKE(res:Record_Number)        !Primary key field - type derived from field
res:Ref_Number         LIKE(res:Ref_Number)           !Browse key field - type derived from field
res:Despatched         LIKE(res:Despatched)           !Browse key field - type derived from field
res:Despatch_Note_Number LIKE(res:Despatch_Note_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK11::res:Despatched      LIKE(res:Despatched)
HK11::res:Ref_Number      LIKE(res:Ref_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Retail Sales Payment Details'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Retail Sale Payment Details'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,160),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Items Requested'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(148,74,384,134),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@39R(2)|M~Quantity~@n8@46R(' &|
   '2)|M~Item Cost~@n14.2@56R(2)|M~Line Cost~@n14.2@'),FROM(Queue:Browse)
                         END
                       END
                       LIST,AT(104,234,216,124),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('49R(2)|M~Date~L(0)@d6b@80L(2)|M~Payment Type~@s30@68R(12)|M~Payment Received~R(0' &|
   ')@n-14.2@'),FROM(Queue:Browse:1)
                       BUTTON,AT(324,258),USE(?Insert:2),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(324,288),USE(?Change:2),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(324,318),USE(?Delete:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(64,218,552,144),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Payments Received'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Courier Cost'),AT(408,238),USE(?Prompt1),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n14.2),AT(500,238),USE(f_courier_cost),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@n14.2),AT(500,254),USE(items_total_temp),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           PROMPT('Items Total'),AT(408,254),USE(?Prompt1:2),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Sub Total (Excl V.A.T.)'),AT(408,270),USE(?Prompt1:3),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n14.2),AT(500,270),USE(sub_total_temp),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           LINE,AT(408,282,154,0),USE(?Line1),COLOR(COLOR:White)
                           LINE,AT(408,298,154,0),USE(?Line1:2),COLOR(COLOR:White)
                           LINE,AT(408,322,154,0),USE(?Line1:3),COLOR(COLOR:White)
                           STRING(@n14.2),AT(500,286),USE(vat_temp),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@n14.2),AT(500,306),USE(total_temp),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@n14.2),AT(500,326),USE(amount_paid_temp),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           PROMPT('V.A.T. '),AT(408,286),USE(?Prompt1:4),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Total (Inc. V.A.T.)'),AT(408,306),USE(?Prompt1:5),TRN,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Amount Paid'),AT(408,326),USE(?Prompt1:6),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14.2),AT(503,342),USE(balance_due_temp),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           PROMPT('Balance Due'),AT(408,342),USE(?Prompt1:7),TRN,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
BRW1::Sort1:StepClass StepRealClass                   !Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetFromView          PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020331'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Retail_Payments')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RETPAY.Open
  Relate:SUBTRACC.Open
  Relate:VATCODE.Open
  Access:RETSALES.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  ref_number_temp = f_ref_number
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = f_account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          if tra:invoice_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = sub:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          else!if tra:use_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = tra:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  tmp:Type     = func:Type
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RETPAY,SELF)
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:RETSTOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Retail_Payments')
  ?List{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,rtp:Date_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,rtp:Ref_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,rtp:Date_Key)
  BRW1.AddRange(rtp:Ref_Number,ref_number_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,rtp:Date,1,BRW1)
  BRW1.AddField(rtp:Date,BRW1.Q.rtp:Date)
  BRW1.AddField(rtp:Payment_Type,BRW1.Q.rtp:Payment_Type)
  BRW1.AddField(rtp:Amount,BRW1.Q.rtp:Amount)
  BRW1.AddField(rtp:Record_Number,BRW1.Q.rtp:Record_Number)
  BRW1.AddField(rtp:Ref_Number,BRW1.Q.rtp:Ref_Number)
  BRW6.Q &= Queue:Browse
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,res:Despatched_Key)
  BRW6.AddRange(res:Despatched)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,res:Despatch_Note_Number,1,BRW6)
  BIND('line_cost_temp',line_cost_temp)
  BRW6.AddField(res:Part_Number,BRW6.Q.res:Part_Number)
  BRW6.AddField(res:Description,BRW6.Q.res:Description)
  BRW6.AddField(res:Quantity,BRW6.Q.res:Quantity)
  BRW6.AddField(res:Item_Cost,BRW6.Q.res:Item_Cost)
  BRW6.AddField(line_cost_temp,BRW6.Q.line_cost_temp)
  BRW6.AddField(res:Record_Number,BRW6.Q.res:Record_Number)
  BRW6.AddField(res:Ref_Number,BRW6.Q.res:Ref_Number)
  BRW6.AddField(res:Despatched,BRW6.Q.res:Despatched)
  BRW6.AddField(res:Despatch_Note_Number,BRW6.Q.res:Despatch_Note_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETPAY.Close
    Relate:SUBTRACC.Close
    Relate:VATCODE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Retail_Payments')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  If balance_due_temp > 0
      glo:select1    = balance_due_temp
  Else!If balance_due_temp > 0
      glo:select1  = ''
  End!If balance_due_temp > 0
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Retail_Payment
    ReturnValue = GlobalResponse
  END
  glo:select1 = ''
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020331'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020331'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020331'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.ResetFromView PROCEDURE

amount_paid_temp:Sum REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:RETPAY.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    amount_paid_temp:Sum += rtp:Amount
  END
  amount_paid_temp = amount_paid_temp:Sum
  PARENT.ResetFromView
  Relate:RETPAY.SetQuickScan(0)
  SETCURSOR()


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = ref_number_temp
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = tmp:Type
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW6.ResetFromView PROCEDURE

items_total_temp:Sum REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:RETSTOCK.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    items_total_temp:Sum += line_cost_temp
  END
  items_total_temp = items_total_temp:Sum
  PARENT.ResetFromView
  Relate:RETSTOCK.SetQuickScan(0)
  SETCURSOR()


BRW6.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, SetQueueRecord, ())
  line_cost_temp = Round(Round(res:item_cost,.01) * res:quantity,.01)
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, SetQueueRecord, ())


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Do_Note_Delete PROCEDURE                              !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Do_Note_Delete')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXPCITY.Open
  Relate:EXPGEN.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Do_Note_Delete')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXPCITY.Close
    Relate:EXPGEN.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Do_Note_Delete')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateRETSALES PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::ret:Record  LIKE(ret:RECORD),STATIC
QuickWindow          WINDOW('Update the RETSALES File'),AT(,,358,182),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateRETSALES'),SYSTEM,GRAY,NOFRAME
                       SHEET,AT(4,4,350,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Sales Number'),AT(8,20),USE(?RET:Ref_Number:Prompt),TRN
                           ENTRY(@s8),AT(100,20,40,10),USE(ret:Ref_Number),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('who booked'),AT(8,34),USE(?RET:who_booked:Prompt)
                           ENTRY(@s3),AT(100,34,40,10),USE(ret:who_booked),COLOR(COLOR:Yellow),UPR
                           PROMPT('date booked:'),AT(8,48),USE(?RET:date_booked:Prompt)
                           ENTRY(@d6b),AT(100,48,104,10),USE(ret:date_booked),COLOR(COLOR:Yellow)
                           PROMPT('time booked:'),AT(8,62),USE(?RET:time_booked:Prompt)
                           ENTRY(@t1),AT(100,62,104,10),USE(ret:time_booked)
                           PROMPT('Account Number'),AT(8,76),USE(?RET:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(100,76,64,10),USE(ret:Account_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Yellow,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Contact Name'),AT(8,90),USE(?RET:Contact_Name:Prompt),TRN
                           ENTRY(@s30),AT(100,90,124,10),USE(ret:Contact_Name),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Purchase Order Number'),AT(8,104),USE(?RET:Purchase_Order_Number:Prompt),TRN
                           ENTRY(@s30),AT(100,104,124,10),USE(ret:Purchase_Order_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Delivery Or Collection'),AT(8,118),USE(?RET:Delivery_Collection:Prompt)
                           ENTRY(@s3),AT(100,118,40,10),USE(ret:Delivery_Collection)
                           PROMPT('Payment Method'),AT(8,132),USE(?RET:Payment_Method:Prompt)
                           ENTRY(@s3),AT(100,132,40,10),USE(ret:Payment_Method)
                           PROMPT('Courier Cost'),AT(8,146),USE(?RET:Courier_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(100,146,60,10),USE(ret:Courier_Cost),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('General (cont.)'),USE(?Tab:2)
                           PROMPT('Courier Cost'),AT(8,20),USE(?RET:Parts_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(100,20,60,10),USE(ret:Parts_Cost),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Courier Cost'),AT(8,34),USE(?RET:Sub_Total:Prompt),TRN
                           ENTRY(@n14.2),AT(100,34,60,10),USE(ret:Sub_Total),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Courier'),AT(8,48),USE(?RET:Courier:Prompt),TRN
                           ENTRY(@s30),AT(100,48,124,10),USE(ret:Courier),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Consignment Number'),AT(8,62),USE(?RET:Consignment_Number:Prompt),TRN
                           ENTRY(@s30),AT(100,62,124,10),USE(ret:Consignment_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Date Despatched'),AT(8,76),USE(?RET:Date_Despatched:Prompt),TRN
                           ENTRY(@d6b),AT(100,76,104,10),USE(ret:Date_Despatched),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Despatched'),AT(8,90),USE(?RET:Despatched:Prompt)
                           ENTRY(@s3),AT(100,90,40,10),USE(ret:Despatched),UPR
                           PROMPT('Despatch_Number'),AT(8,104),USE(?RET:Despatch_Number:Prompt)
                           ENTRY(@s8),AT(100,104,40,10),USE(ret:Despatch_Number),RIGHT(1),UPR
                           PROMPT('Invoice Number'),AT(8,118),USE(?RET:Invoice_Number:Prompt),TRN
                           ENTRY(@s8),AT(100,118,40,10),USE(ret:Invoice_Number),RIGHT(1),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Invoice Date'),AT(8,132),USE(?RET:Invoice_Date:Prompt),TRN
                           ENTRY(@d6b),AT(100,132,104,10),USE(ret:Invoice_Date),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Courier Cost'),AT(8,146),USE(?RET:Invoice_Courier_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(100,146,60,10),USE(ret:Invoice_Courier_Cost),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('General (cont. 2)'),USE(?Tab:3)
                           PROMPT('Parts Cost'),AT(8,20),USE(?RET:Invoice_Parts_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(100,20,60,10),USE(ret:Invoice_Parts_Cost),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Sub Total'),AT(8,34),USE(?RET:Invoice_Sub_Total:Prompt),TRN
                           ENTRY(@n14.2),AT(100,34,60,10),USE(ret:Invoice_Sub_Total),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Postcode'),AT(8,48),USE(?RET:Postcode:Prompt),TRN
                           ENTRY(@s10),AT(100,48,44,10),USE(ret:Postcode),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Company Name'),AT(8,62),USE(?RET:Company_Name:Prompt),TRN
                           ENTRY(@s30),AT(100,62,124,10),USE(ret:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Building Name/No'),AT(8,76),USE(?RET:Building_Name:Prompt),TRN
                           ENTRY(@s30),AT(100,76,124,10),USE(ret:Building_Name),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Address'),AT(8,90),USE(?RET:Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(100,90,124,10),USE(ret:Address_Line1),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('DELETE THIS'),AT(8,104),USE(?RET:Address_Line2:Prompt)
                           ENTRY(@s30),AT(100,104,124,10),USE(ret:Address_Line2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('DELETE THIS'),AT(8,118),USE(?RET:Address_Line3:Prompt)
                           ENTRY(@s30),AT(100,118,124,10),USE(ret:Address_Line3),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(8,132),USE(?RET:Telephone_Number:Prompt),TRN
                           ENTRY(@s15),AT(100,132,64,10),USE(ret:Telephone_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Fax Number'),AT(8,146),USE(?RET:Fax_Number:Prompt),TRN
                           ENTRY(@s15),AT(100,146,64,10),USE(ret:Fax_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('General (cont. 3)'),USE(?Tab:4)
                           PROMPT('Postcode'),AT(8,20),USE(?RET:Postcode_Delivery:Prompt),TRN,LEFT
                           ENTRY(@s10),AT(100,20,44,10),USE(ret:Postcode_Delivery),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Company Name'),AT(8,34),USE(?RET:Company_Name_Delivery:Prompt),TRN
                           ENTRY(@s30),AT(100,34,124,10),USE(ret:Company_Name_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Building Name/No'),AT(8,48),USE(?RET:Building_Name_Delivery:Prompt),TRN
                           ENTRY(@s30),AT(100,48,124,10),USE(ret:Building_Name_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Address'),AT(8,62),USE(?RET:Address_Line1_Delivery:Prompt)
                           ENTRY(@s30),AT(100,62,124,10),USE(ret:Address_Line1_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('DELETE THIS'),AT(8,76),USE(?RET:Address_Line2_Delivery:Prompt)
                           ENTRY(@s30),AT(100,76,124,10),USE(ret:Address_Line2_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('DELETE THIS'),AT(8,90),USE(?RET:Address_Line3_Delivery:Prompt)
                           ENTRY(@s30),AT(100,90,124,10),USE(ret:Address_Line3_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(8,104),USE(?RET:Telephone_Delivery:Prompt),TRN
                           ENTRY(@s15),AT(100,104,64,10),USE(ret:Telephone_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Fax Number'),AT(8,118),USE(?RET:Fax_Number_Delivery:Prompt),TRN
                           ENTRY(@s15),AT(100,118,64,10),USE(ret:Fax_Number_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Delivery Text'),AT(8,132),USE(?RET:Delivery_Text:Prompt)
                           ENTRY(@s255),AT(100,132,250,10),USE(ret:Delivery_Text),UPR
                           PROMPT('Invoice Text:'),AT(8,146),USE(?RET:Invoice_Text:Prompt)
                           ENTRY(@s255),AT(100,146,250,10),USE(ret:Invoice_Text)
                         END
                       END
                       BUTTON('OK'),AT(260,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(309,164,45,14),USE(?Cancel)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateRETSALES')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RET:Ref_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ret:Record,History::ret:Record)
  SELF.AddHistoryField(?ret:Ref_Number,1)
  SELF.AddHistoryField(?ret:who_booked,2)
  SELF.AddHistoryField(?ret:date_booked,3)
  SELF.AddHistoryField(?ret:time_booked,4)
  SELF.AddHistoryField(?ret:Account_Number,5)
  SELF.AddHistoryField(?ret:Contact_Name,6)
  SELF.AddHistoryField(?ret:Purchase_Order_Number,7)
  SELF.AddHistoryField(?ret:Delivery_Collection,8)
  SELF.AddHistoryField(?ret:Payment_Method,9)
  SELF.AddHistoryField(?ret:Courier_Cost,10)
  SELF.AddHistoryField(?ret:Parts_Cost,11)
  SELF.AddHistoryField(?ret:Sub_Total,12)
  SELF.AddHistoryField(?ret:Courier,13)
  SELF.AddHistoryField(?ret:Consignment_Number,14)
  SELF.AddHistoryField(?ret:Date_Despatched,15)
  SELF.AddHistoryField(?ret:Despatched,16)
  SELF.AddHistoryField(?ret:Despatch_Number,17)
  SELF.AddHistoryField(?ret:Invoice_Number,18)
  SELF.AddHistoryField(?ret:Invoice_Date,19)
  SELF.AddHistoryField(?ret:Invoice_Courier_Cost,20)
  SELF.AddHistoryField(?ret:Invoice_Parts_Cost,21)
  SELF.AddHistoryField(?ret:Invoice_Sub_Total,22)
  SELF.AddHistoryField(?ret:Postcode,27)
  SELF.AddHistoryField(?ret:Company_Name,28)
  SELF.AddHistoryField(?ret:Building_Name,29)
  SELF.AddHistoryField(?ret:Address_Line1,30)
  SELF.AddHistoryField(?ret:Address_Line2,31)
  SELF.AddHistoryField(?ret:Address_Line3,32)
  SELF.AddHistoryField(?ret:Telephone_Number,33)
  SELF.AddHistoryField(?ret:Fax_Number,34)
  SELF.AddHistoryField(?ret:Postcode_Delivery,35)
  SELF.AddHistoryField(?ret:Company_Name_Delivery,36)
  SELF.AddHistoryField(?ret:Building_Name_Delivery,37)
  SELF.AddHistoryField(?ret:Address_Line1_Delivery,38)
  SELF.AddHistoryField(?ret:Address_Line2_Delivery,39)
  SELF.AddHistoryField(?ret:Address_Line3_Delivery,40)
  SELF.AddHistoryField(?ret:Telephone_Delivery,41)
  SELF.AddHistoryField(?ret:Fax_Number_Delivery,42)
  SELF.AddHistoryField(?ret:Delivery_Text,43)
  SELF.AddHistoryField(?ret:Invoice_Text,44)
  SELF.AddUpdateFile(Access:RETSALES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RETSALES.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETSALES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateRETSALES')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateRETSALES')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Stock_Enquiry PROCEDURE (func:OrderNumber)            !Generated from procedure template - Window

CurrentTab           STRING(80)
save_res_id          USHORT,AUTO
Use_Euro             BYTE
save_ret_ali_id      USHORT,AUTO
save_orp_id          USHORT,AUTO
save_ope_id          USHORT,AUTO
vat_rate_temp        REAL
vat_rate_parts_temp  REAL
Location_Temp        STRING(30)
accessory_temp       STRING('ALL')
vat_temp             REAL
total_temp           REAL
line_cost_temp       REAL
Courier_Euro         REAL
Item_Euro            REAL
Sub_Euro             REAL
Vat_Euro             REAL
Total_Euro           REAL
tmp:OrderNumber      LONG
tmp:ShelfLocation    STRING(30)
tmp:SecondLocation   STRING(30)
tmp:Quantity         LONG
tmp:Sundry           STRING('NO {1}')
tmp:Accessory        STRING('NO {1}')
tmp:ExchangeUnit     STRING('NO {1}')
tmp:Suspend          BYTE(0)
tmp:WebORderNumber   LONG
tmp:BackOrderTotal   REAL
locFalse             BYTE(0)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Location_Temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOCK)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Description)
                       PROJECT(sto:Shelf_Location)
                       PROJECT(sto:Second_Location)
                       PROJECT(sto:Quantity_Stock)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Location)
                       PROJECT(sto:Suspend)
                       PROJECT(sto:Accessory)
                       PROJECT(sto:ExchangeUnit)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Shelf_Location     LIKE(sto:Shelf_Location)       !List box control field - type derived from field
sto:Second_Location    LIKE(sto:Second_Location)      !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Primary key field - type derived from field
sto:Location           LIKE(sto:Location)             !Browse key field - type derived from field
sto:Suspend            LIKE(sto:Suspend)              !Browse key field - type derived from field
sto:Accessory          LIKE(sto:Accessory)            !Browse key field - type derived from field
sto:ExchangeUnit       LIKE(sto:ExchangeUnit)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(RETSTOCK)
                       PROJECT(res:Part_Number)
                       PROJECT(res:Description)
                       PROJECT(res:Item_Cost)
                       PROJECT(res:Quantity)
                       PROJECT(res:Record_Number)
                       PROJECT(res:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
res:Part_Number        LIKE(res:Part_Number)          !List box control field - type derived from field
res:Part_Number_NormalFG LONG                         !Normal forground color
res:Part_Number_NormalBG LONG                         !Normal background color
res:Part_Number_SelectedFG LONG                       !Selected forground color
res:Part_Number_SelectedBG LONG                       !Selected background color
res:Description        LIKE(res:Description)          !List box control field - type derived from field
res:Description_NormalFG LONG                         !Normal forground color
res:Description_NormalBG LONG                         !Normal background color
res:Description_SelectedFG LONG                       !Selected forground color
res:Description_SelectedBG LONG                       !Selected background color
res:Item_Cost          LIKE(res:Item_Cost)            !List box control field - type derived from field
res:Item_Cost_NormalFG LONG                           !Normal forground color
res:Item_Cost_NormalBG LONG                           !Normal background color
res:Item_Cost_SelectedFG LONG                         !Selected forground color
res:Item_Cost_SelectedBG LONG                         !Selected background color
res:Quantity           LIKE(res:Quantity)             !List box control field - type derived from field
res:Quantity_NormalFG  LONG                           !Normal forground color
res:Quantity_NormalBG  LONG                           !Normal background color
res:Quantity_SelectedFG LONG                          !Selected forground color
res:Quantity_SelectedBG LONG                          !Selected background color
line_cost_temp         LIKE(line_cost_temp)           !List box control field - type derived from local data
line_cost_temp_NormalFG LONG                          !Normal forground color
line_cost_temp_NormalBG LONG                          !Normal background color
line_cost_temp_SelectedFG LONG                        !Selected forground color
line_cost_temp_SelectedBG LONG                        !Selected background color
res:Record_Number      LIKE(res:Record_Number)        !Primary key field - type derived from field
res:Ref_Number         LIKE(res:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(ORDITEMS)
                       PROJECT(ori:partno)
                       PROJECT(ori:partdiscription)
                       PROJECT(ori:qty)
                       PROJECT(ori:recordnumber)
                       PROJECT(ori:ordhno)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
ori:partno             LIKE(ori:partno)               !List box control field - type derived from field
ori:partdiscription    LIKE(ori:partdiscription)      !List box control field - type derived from field
ori:qty                LIKE(ori:qty)                  !List box control field - type derived from field
sto:Second_Location    LIKE(sto:Second_Location)      !Browse hot field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !Browse hot field - type derived from field
sto:Shelf_Location     LIKE(sto:Shelf_Location)       !Browse hot field - type derived from field
sto:Suspend            LIKE(sto:Suspend)              !Browse hot field - type derived from field
sto:ExchangeUnit       LIKE(sto:ExchangeUnit)         !Browse hot field - type derived from field
sto:Accessory          LIKE(sto:Accessory)            !Browse hot field - type derived from field
sto:Sundry_Item        LIKE(sto:Sundry_Item)          !Browse hot field - type derived from field
ori:recordnumber       LIKE(ori:recordnumber)         !Primary key field - type derived from field
ori:ordhno             LIKE(ori:ordhno)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK14::sto:Accessory       LIKE(sto:Accessory)
HK14::sto:ExchangeUnit    LIKE(sto:ExchangeUnit)
HK14::sto:Location        LIKE(sto:Location)
HK14::sto:Suspend         LIKE(sto:Suspend)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB5::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Pick Stock'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Pick Stock'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,260,672,120),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Items Requested - Please note list of prices is NOT in Euros'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(8,274,416,68),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M*~Part Number~@s30@120L(2)|M*~Description~@s30@56L(2)|M*~Retail Cost~@n' &|
   '14.2@32L(2)|M*~Quantity~@n8@40R(2)|M*~Line Cost~L@n10.2@'),FROM(Queue:Browse),DROPID('FromList')
                           PROMPT('Courier Cost'),AT(432,274),USE(?RET:Courier_Cost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(512,274,52,12),USE(ret:Courier_Cost),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('<128>'),AT(572,274),USE(?Courier_Euro:Prompt),HIDE
                           ENTRY(@n10.2),AT(580,274,52,12),USE(Courier_Euro),SKIP,HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           PROMPT('<128>'),AT(572,290),USE(?Item_Euro:Prompt),HIDE
                           ENTRY(@n10.2),AT(580,290,52,12),USE(Item_Euro),SKIP,HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           PROMPT('Items Cost'),AT(432,290),USE(?RET:Parts_Cost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(512,290,52,12),USE(ret:Parts_Cost),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('<128>'),AT(572,310),USE(?Sub_Euro:Prompt),HIDE
                           ENTRY(@n10.2),AT(580,310,52,12),USE(Sub_Euro),SKIP,HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           PROMPT('Sub Total'),AT(432,308),USE(?RET:Sub_Total:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(512,308,52,12),USE(ret:Sub_Total),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('<128>'),AT(572,322),USE(?Vat_Euro:Prompt),HIDE
                           ENTRY(@n10.2),AT(580,322,52,12),USE(Vat_Euro),SKIP,HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           PROMPT('V.A.T.'),AT(432,324),USE(?vat_temp:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(512,324,52,12),USE(vat_temp),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('<128>'),AT(572,338),USE(?Total_Euro:Prompt),HIDE
                           ENTRY(@n10.2),AT(580,338,52,12),USE(Total_Euro),SKIP,HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           BUTTON,AT(8,350),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(76,350),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           BOX,AT(156,356,10,10),USE(?Box1),FILL(COLOR:Green)
                           PROMPT('- Awaiting Ordering'),AT(176,356),USE(?Prompt3),FONT(,,,FONT:bold,CHARSET:ANSI)
                           BOX,AT(252,356,10,10),USE(?Box1:2),COLOR(COLOR:Black),FILL(COLOR:Black)
                           PROMPT('- Cancelled'),AT(272,356),USE(?Prompt3:2),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Cost Of Back Order Parts (Ex VAT)'),AT(372,362),USE(?tmp:BackOrderTotal:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(512,360,52,12),USE(tmp:BackOrderTotal),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Cost Of Back Order Parts'),TIP('Cost Of Back Order Parts'),UPR,READONLY
                           PROMPT('Total (Inc V.A.T.)'),AT(432,342),USE(?total_temp:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(512,340,52,12),USE(total_temp),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                         END
                       END
                       BUTTON,AT(8,226),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       PROMPT('Press ''Insert'', Double Click or Drag the Stock Item to add to Items Request'),AT(88,234),USE(?Prompt2),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       SHEET,AT(36,64,120,222),USE(?Sheet3),NOSHEET
                         TAB('Tab 4'),USE(?NormalStockTab)
                           PROMPT('Site Location'),AT(140,46),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(8,78,596,142),USE(?Browse:1),IMM,HVSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),ALRT(4),ALRT(EnterKey),ALRT(InsertKey),FORMAT('120L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@120L(2)|M~Shelf Location~@' &|
   's30@120L(2)|M~Second Location~@s30@32D(2)|M~Stock Qty~L@N8@'),FROM(Queue:Browse:1),DRAGID('FromList')
                           COMBO(@s30),AT(8,46,124,10),USE(Location_Temp),IMM,DISABLE,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           OPTION('Stock Type'),AT(416,46,192,28),USE(accessory_temp),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All'),AT(580,57),USE(?accessory_temp:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                             RADIO('Non-Accessories'),AT(420,57),USE(?accessory_temp:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('NO')
                             RADIO('Accessories'),AT(508,57),USE(?accessory_temp:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES')
                           END
                           BUTTON,AT(608,196),USE(?Generate_Stock_Order),TRN,FLAT,LEFT,ICON('genstop.jpg')
                           SHEET,AT(4,28,672,228),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                             TAB('By Part Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(8,62,124,10),USE(sto:Part_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             END
                             TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(8,66,124,10),USE(sto:Description),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             END
                           END
                         END
                         TAB('Tab 5'),USE(?WebStockTab)
                           LIST,AT(8,48,392,174),USE(?List:2),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(InsertKey),FORMAT('87L(2)|M~Part Number~@s20@173L(2)|M~Description~@s60@40R(2)|M~Quantity~L@s8@'),FROM(Queue:Browse:2),DRAGID('FromList')
                           SHEET,AT(4,28,672,228),USE(?Sheet4),COLOR(0D6E7EFH),SPREAD
                             TAB('By Part Number'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               PROMPT('Part Details'),AT(448,101),USE(?Prompt22),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               PROMPT('Shelf Location'),AT(448,122),USE(?sto:Shelf_Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                               ENTRY(@s30),AT(516,122,124,10),USE(sto:Shelf_Location),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                               PROMPT('Sundry Item'),AT(448,173),USE(?sto:Shelf_Location:Prompt:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                               PROMPT('2nd Location'),AT(448,138),USE(?sto:Second_Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                               ENTRY(@s30),AT(516,138,124,10),USE(sto:Second_Location),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                               PROMPT('Quantity In Stock'),AT(448,154),USE(?sto:Quantity_Stock:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                               ENTRY(@N8),AT(516,154,64,10),USE(sto:Quantity_Stock),SKIP,DECIMAL(14),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                               CHECK,AT(516,173),USE(sto:Sundry_Item),SKIP,DISABLE,VALUE('YES','NO')
                               PROMPT('Accessory'),AT(448,189),USE(?sto:Shelf_Location:Prompt:3),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                               CHECK,AT(516,189),USE(sto:Accessory),SKIP,DISABLE,VALUE('YES','NO')
                               PROMPT('Exchange Unit'),AT(448,205),USE(?sto:Shelf_Location:Prompt:4),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                               CHECK,AT(516,205),USE(sto:ExchangeUnit),SKIP,DISABLE,RIGHT,VALUE('YES','NO')
                               PROMPT('Suspend Part'),AT(448,221),USE(?sto:Shelf_Location:Prompt:5),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                               CHECK,AT(516,221),USE(sto:Suspend),SKIP,DISABLE,MSG('Suspend Part'),TIP('Suspend Part'),VALUE('1','0')
                             END
                           END
                         END
                       END
                       BUTTON,AT(608,384),USE(?Close),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort6:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort7:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(ret:Payment_Method) = 'EXC'
BRW1::Sort8:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(ret:Payment_Method) = 'EXC'
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Show_fields     ROUTINE
  IF Use_Euro = TRUE
    ?Courier_Euro{PROP:Hide} = FALSE
    ?Courier_Euro:Prompt{PROP:Hide} = FALSE
    ?Item_Euro{PROP:Hide} = FALSE
    ?Item_Euro:Prompt{PROP:Hide} = FALSE
    ?Sub_Euro{PROP:Hide} = FALSE
    ?Sub_Euro:Prompt{PROP:Hide} = FALSE
    ?Vat_Euro{PROP:Hide} = FALSE
    ?Vat_Euro:Prompt{PROP:Hide} = FALSE
    ?Total_Euro{PROP:Hide} = FALSE
    ?Total_Euro:Prompt{PROP:Hide} = FALSE
  END


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020334'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Stock_Enquiry')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Relate:ORDITEMS.Open
  Relate:RETSALES.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:VATCODE.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  If ret:Payment_Method = 'EXC'
      Accessory_Temp = 'YES'
  Else !ret:Payment_Method = 'EXC'
  
  End !ret:Payment_Method = 'EXC'
  
  tmp:OrderNumber = func:OrderNumber
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCK,SELF)
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:RETSTOCK,SELF)
  BRW7.Init(?List:2,Queue:Browse:2.ViewPosition,BRW7::View:Browse,Queue:Browse:2,Relate:ORDITEMS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  SET(Defaults)
  Access:Defaults.Next()
  Use_Euro = FALSE
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = ret:account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          IF tra:EuroApplies = 1 OR Sub:EuroApplies = 1
            Use_Euro = TRUE
            DO Show_Fields
          END
          if tra:invoice_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = sub:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = sub:parts_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_parts_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          else!if tra:use_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = tra:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = tra:parts_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_parts_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  
  If ret:Payment_Method = 'EXC'
      ?Accessory_Temp{prop:Disable} = True
  Else !ret:Payment_Method = 'EXC'
  
  End !ret:Payment_Method = 'EXC'
  If func:OrderNumber <> 0
      ?WebStockTab{prop:Hide} = 0
      ?NormalStockTab{prop:Hide} = 1
  Else !func:OrderNumber <> 0
      ?WebStockTab{prop:Hide} = 1
      ?NormalStockTab{prop:Hide} = 0
  End !func:OrderNumber <> 0
  ?Sheet3{prop:wizard} = 1
  ! Save Window Name
   AddToLog('Window','Open','Stock_Enquiry')
  ?List{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,sto:LocPartSuspendKey)
  BRW1.AddRange(sto:Suspend)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Part_Number_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Part_Number_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:LocDescSuspendKey)
  BRW1.AddRange(sto:Suspend)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:ExchangeAccPartKey)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.SetFilter('(sto:Suspend = 0)')
  BRW1.AddSortOrder(,sto:ExchangeAccDescKey)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?sto:Description,sto:Description,1,BRW1)
  BRW1.SetFilter('(sto:Suspend = 0)')
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sto:Location_Part_Description_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,sto:Location,1,BRW1)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(sto:Shelf_Location,BRW1.Q.sto:Shelf_Location)
  BRW1.AddField(sto:Second_Location,BRW1.Q.sto:Second_Location)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Suspend,BRW1.Q.sto:Suspend)
  BRW1.AddField(sto:Accessory,BRW1.Q.sto:Accessory)
  BRW1.AddField(sto:ExchangeUnit,BRW1.Q.sto:ExchangeUnit)
  BRW8.Q &= Queue:Browse
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,res:Part_Number_Key)
  BRW8.AddRange(res:Ref_Number,ret:Ref_Number)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,res:Part_Number,1,BRW8)
  BIND('line_cost_temp',line_cost_temp)
  BRW8.AddField(res:Part_Number,BRW8.Q.res:Part_Number)
  BRW8.AddField(res:Description,BRW8.Q.res:Description)
  BRW8.AddField(res:Item_Cost,BRW8.Q.res:Item_Cost)
  BRW8.AddField(res:Quantity,BRW8.Q.res:Quantity)
  BRW8.AddField(line_cost_temp,BRW8.Q.line_cost_temp)
  BRW8.AddField(res:Record_Number,BRW8.Q.res:Record_Number)
  BRW8.AddField(res:Ref_Number,BRW8.Q.res:Ref_Number)
  BRW7.Q &= Queue:Browse:2
  BRW7.RetainRow = 0
  BRW7.AddSortOrder(,ori:Keyordhno)
  BRW7.AddRange(ori:ordhno,tmp:OrderNumber)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,ori:ordhno,1,BRW7)
  BRW7.AddField(ori:partno,BRW7.Q.ori:partno)
  BRW7.AddField(ori:partdiscription,BRW7.Q.ori:partdiscription)
  BRW7.AddField(ori:qty,BRW7.Q.ori:qty)
  BRW7.AddField(sto:Second_Location,BRW7.Q.sto:Second_Location)
  BRW7.AddField(sto:Quantity_Stock,BRW7.Q.sto:Quantity_Stock)
  BRW7.AddField(sto:Shelf_Location,BRW7.Q.sto:Shelf_Location)
  BRW7.AddField(sto:Suspend,BRW7.Q.sto:Suspend)
  BRW7.AddField(sto:ExchangeUnit,BRW7.Q.sto:ExchangeUnit)
  BRW7.AddField(sto:Accessory,BRW7.Q.sto:Accessory)
  BRW7.AddField(sto:Sundry_Item,BRW7.Q.sto:Sundry_Item)
  BRW7.AddField(ori:recordnumber,BRW7.Q.ori:recordnumber)
  BRW7.AddField(ori:ordhno,BRW7.Q.ori:ordhno)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW8.AskProcedure = 1
  FDCB5.Init(Location_Temp,?Location_Temp,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(loc:Location_Key)
  FDCB5.AddField(loc:Location,FDCB5.Q.loc:Location)
  FDCB5.AddField(loc:RecordNumber,FDCB5.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
    Relate:ORDITEMS.Close
    Relate:RETSALES.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:VATCODE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Stock_Enquiry')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  !Has this part already been entered
  do_update# = 1
  tmp:WebOrderNumber = 0

  If request  = Insertrecord
      brw1.UpdateBuffer()
      brw7.UpdateBuffer()
      Case Choice(?Sheet3)
          Of 1
          Of 2
              tmp:WebOrderNumber = ori:RecordNumber
              Access:STOCK.ClearKey(sto:Location_Key)
              sto:Location    = MainStoreLocation()
              sto:Part_Number = ori:PartNo
              If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  !Found

              Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      End !Case Choice(?Sheet3)

      If sto:Suspend
          do_update# = 0
          Access:RETSTOCK.Cancelautoinc()
          Case Missive('The part cannot be used. It has been suspended.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !If sto:Suspend
          If ret:Payment_Method <> 'EXC' and sto:ExchangeUnit = 'YES'
              Case Missive('Warning! This part is an Exchange Accessory.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to use it?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                  Of 1 ! No Button
                      do_update# = 0
                      Access:RETSTOCK.CancelAutoinc()
              End ! Case Missive
          Else !If ret:PaymentMethod = 'EXC' and sto:ExchangeUnit = 'YES'

          End !If ret:PaymentMethod = 'EXC' and sto:ExchangeUnit = 'YES'

          If do_Update# = 1
              save_ret_ali_id = access:retstock_alias.savefile()
              access:retstock_alias.clearkey(ret_ali:part_number_key)
              ret_ali:ref_number  = ret:ref_number
              ret_ali:part_number = sto:part_number
              set(ret_ali:part_number_key,ret_ali:part_number_key)
              loop
                  if access:retstock_alias.next()
                     break
                  end !if
                  if ret_ali:ref_number  <> ret:ref_number      |
                  or ret_ali:part_number <> sto:part_number      |
                      then break.  ! end if
                  found# = 1
                  Break
              end !loop
              access:retstock_alias.restorefile(save_ret_ali_id)

              If found# = 1
                  Case Missive('The selected part has already been attached to this sale.'&|
                    '<13,10>'&|
                    '<13,10>Are you sure you want to add another one?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          do_update# = 0
                          access:retstock.cancelautoinc()
                  End ! Case Missive
              End!If found# = 1

          End !If do_Update# = 1
      End !If sto:Suspend

  End!If request  = Insertrecord

  If request  = Deleterecord
      do_update# = 0
      do_delete# = 0
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number = res:part_ref_number
      if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
          If ret:Payment_Method = 'EXC' and sto:Accessory <> 'YES'

          Else !If ret:Payment_Method = 'EXC' and sto:Accessory <> 'YES'

          End !If ret:Payment_Method = 'EXC' and sto:Accessory <> 'YES'
          If sto:sundry_item = 'YES'
              do_Delete# = 1
          Else!If sto:sundry_item = 'YES'
              If res:Despatched = 'PEN'

                  !Try and find if a Pending Order exists for this part number,
                  !if so decrement the quantity, otherwise cancel the part.

                  Found# = 0
                  Save_ope_ID = Access:ORDPEND.SaveFile()
                  Access:ORDPEND.ClearKey(ope:Supplier_Key)
                  ope:Supplier    = res:Supplier
                  ope:Part_Number = res:Part_Number
                  Set(ope:Supplier_Key,ope:Supplier_Key)
                  Loop
                      If Access:ORDPEND.NEXT()
                         Break
                      End !If
                      If ope:Supplier    <> res:Supplier  |
                      Or ope:Part_Number <> res:Part_Number      |
                          Then Break.  ! End If
                      !Only affect Pending Orders that have enough quantity,
                      !other wise we need to cancel the part.
                      If ope:Description = res:Description and ope:Quantity >= res:Quantity
                          Found# = 1
                          Case Missive('This part has been requested for ordering.'&|
                            '<13,10>'&|
                            '<13,10>Do you wish to remove this quantity from the order request?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
                                  If ope:Quantity = res:Quantity
                                      Delete(ORDPEND)
                                  Else !If ope:Quanity = res:Quantity
                                      ope:Quantity -= res:Quantity
                                      Access:ORDPEND.Update()
                                  End !If ope:Quanity = res:Quantity
                                  Do_Delete# = 1
                              Of 1 ! No Button
                                  Do_Delete# = 0
                          End ! Case Missive
                          Break
                      End !If ope:Description = res:Description
                  End !Loop
                  Access:ORDPEND.RestoreFile(Save_ope_ID)

                  If Found# = 0
                      Case Missive('This part has been request for Order, but a Pending Order cannot be found. This usually means that the order has already been raised.'&|
                        '<13,10>If you continue this part request will be cancelled.'&|
                        '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              res:Despatched = 'CAN'
                              Access:RETSTOCK.Update()
                              Do_Delete# = 0
                          Of 1 ! No Button
                              Do_Delete# = 0
                      End ! Case Missive
                  End !If Found# = 0
              Else !If res:Despatch = 'PEN'
                  !Do not allow to delete Cancelled Parts
                  If res:Despatched = 'CAN'
                      Case Missive('You cannot delete a cancelled part.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Do_Delete# = 0
                  Else !If res:Despatch = 'CAN'
                      Case Missive('Do you want to delete this part?'&|
                        '<13,10>'&|
                        '<13,10>The stock will be recredited?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              sto:Quantity_Stock += res:Quantity
                              If access:stock.update() = Level:Benign
                                  do_delete# = 1
! Changing (DBH 08/03/2006) #6971 - New Procedure
!                                   get(stohist,0)
!                                   if access:stohist.primerecord() = level:benign
!                                       shi:ref_number           = sto:ref_number
!                                       access:users.clearkey(use:password_key)
!                                       use:password              =glo:password
!                                       access:users.fetch(use:password_key)
!                                       shi:user                  = use:user_code
!                                       shi:date                 = today()
!                                       shi:transaction_type     = 'ADD'
!                                       shi:despatch_note_number = ''
!                                       shi:job_number           = ret:ref_number
!                                       shi:quantity             = res:quantity
!                                       shi:purchase_cost        = res:purchase_cost
!                                       shi:sale_cost            = res:sale_cost
!                                       shi:retail_cost           = res:retail_cost
!                                       shi:information          = 'RETAIL PART REMOVED FROM SALE'
!                                       shi:notes                = 'PART EXISTS ON ORDER NUMBER: ' & res:order_number
!                                       ! Inserting (DBH 07/03/2006) #6971 - Show current stock in history
!                                       shi:StockOnHand          = sto:Quantity_Stock
!                                       ! End (DBH 07/03/2006) #6971
!                                       access:stohist.insert()
!                                   end!if access:stohist.primerecord() = level:benign
! to (DBH 08/03/2006) #6971
                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                     'ADD', | ! Transaction_Type
                                                     '', | ! Depatch_Note_Number
                                                     ret:Ref_Number, | ! Job_Number
                                                     0, | ! Sales_Number
                                                     res:Quantity, | ! Quantity
                                                     res:Purchase_Cost, | ! Purchase_Cost
                                                     res:Sale_Cost, | ! Sale_Cost
                                                     res:Retail_Cost, | ! Retail_Cost
                                                     'PART EXISTS ON ORDER NUMBER: ' & res:Order_Number, | ! Notes
                                                     'RETAIL PART REMOVED FROM SALE') ! Information
                                    ! Added OK

                                  Else ! AddToStockHistory
                                    ! Error
                                  End ! AddToStockHistory
! End (DBH 08/03/2006) #6971
                              End!If access:stock.update() = Level:Benign

                          Of 1 ! No Button
                      End ! Case Missive
                  End !If res:Despatch = 'CAN'
              End !If res:Despatch = 'PEN'

          End!If sto:sundry_item = 'YES'
      end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
      If do_delete# = 1
          Delete(retstock)
      End!If do_delete# = 1


  End!If request  = Deleterecord

  IF do_update# = 1

      glo:select1    = sto:ref_number
      glo:select2  = ''
      glo:select3  = ''
      glo:select4  = ''
      glo:select5  = ''
      glo:select6 = ''
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Retail_Stock(tmp:WebOrderNumber,0)
    ReturnValue = GlobalResponse
  END
  End!IF do_update# = 1
  !glo:Select2 = Flag
  !glo:Select3 = Stock Ref number
  !glo:Select4 = RETSTOCK record number
  !glo:Select5 = Quantity
  !glo:Select6 = OPDPEND record number, not needed anymore
  If glo:select2   = 'NEW PENDING' or glo:Select2 = 'CANCELLED'
      UpdateRetailPartRoutine()
   End !If glo:select2   = 'NEW PENDING'
  If glo:Select7 = 'DELETE'
      Access:RETSTOCK.ClearKey(res:Record_Number_Key)
      res:Record_Number = glo:Select4
      If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
          !Found
          Delete(RETSTOCK)
      Else!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
  End !If glo:Select7 = 'DELETE'
  glo:select1     = ''
  glo:Select2     = ''
  glo:Select3     = ''
  glo:Select5     = ''
  glo:Select4     = ''
  glo:Select7     = ''
  Select(?browse:1)
  BRW1.ResetSort(1)
  BRW7.ResetSort(1)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020334'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020334'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020334'&'0')
      ***
    OF ?Location_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?accessory_temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?Generate_Stock_Order
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Generate_Stock_Order, Accepted)
      check_access('STOCK - ORDER STOCK',x")
      if x" = false
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.sto:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case Missive('Cannot Order. '&|
                    '<13,10>'&|
                    '<13,10>This item has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  error# = 1
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              glo:select1 = brw1.q.sto:ref_number
              Stock_Order
              glo:select1 = ''
          End!If error# = 0
      
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Generate_Stock_Order, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:Drag
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, Drag)
      If dragid()
          setdropid(1)
      End!If dragid()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, Drag)
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:Drop
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, Drop)
      If dropid()
          post(event:accepted,?Insert)
      End!If dropid()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, Drop)
    END
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      If keycode() = EnterKey Or keycode() = MouseLeft2 Or keycode() = Insertkey
          Post(Event:accepted,?insert)
      End!If keycode() = EnterKey Or keycode() = MouseLeft2
      If keycode() = HomeKey
          Select(?Browse:1)
          Presskey(ctrlhome)
          Select(?browse:1)
      End!If keycode() = DeleteKey
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
      If keycode() = EnterKey Or keycode() = MouseLeft2 Or keycode() = Insertkey
          Post(Event:accepted,?insert)
      End!If keycode() = EnterKey Or keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Location_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      set(defstock)
      access:defstock.next()
      If DST:Use_Site_Location = 'YES'
          location_temp   = dst:site_location
          Display()
          brw1.resetsort(1)
      Else
          Select(?location_temp)
      End!If dst:site_location <> ''
      
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 1 And Upper(ret:Payment_Method) = 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 'YES'
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(ret:Payment_Method) = 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 'YES'
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSE
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(ret:Payment_Method) = 'EXC'
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(ret:Payment_Method) = 'EXC'
    RETURN SELF.SetSort(8,Force)
  ELSE
    RETURN SELF.SetSort(9,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.ResetFromView PROCEDURE

ret:Parts_Cost:Sum   REAL
tmp:BackOrderTotal:Sum REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:RETSTOCK.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    IF (res:Despatched = 'PIK')
      ret:Parts_Cost:Sum += line_cost_temp
    END
    IF (res:Despatched <> 'PIK')
      tmp:BackOrderTotal:Sum += line_cost_temp
    END
  END
  ret:Parts_Cost = ret:Parts_Cost:Sum
  tmp:BackOrderTotal = tmp:BackOrderTotal:Sum
  PARENT.ResetFromView
  Relate:RETSTOCK.SetQuickScan(0)
  SETCURSOR()


BRW8.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ResetSort, (BYTE Force),BYTE)
  ReturnValue = PARENT.ResetSort(Force)
  Select(?browse:1)
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ResetSort, (BYTE Force),BYTE)
  RETURN ReturnValue


BRW8.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, SetQueueRecord, ())
  line_cost_temp = Round(Round(RES:Item_Cost,.01) * RES:Quantity,.01)
  PARENT.SetQueueRecord
  IF (res:despatched = 'PEN')
    SELF.Q.res:Part_Number_NormalFG = 32768
    SELF.Q.res:Part_Number_NormalBG = 16777215
    SELF.Q.res:Part_Number_SelectedFG = 16777215
    SELF.Q.res:Part_Number_SelectedBG = 32768
  ELSE
    SELF.Q.res:Part_Number_NormalFG = -1
    SELF.Q.res:Part_Number_NormalBG = -1
    SELF.Q.res:Part_Number_SelectedFG = -1
    SELF.Q.res:Part_Number_SelectedBG = -1
  END
  IF (res:despatched = 'PEN')
    SELF.Q.res:Description_NormalFG = 32768
    SELF.Q.res:Description_NormalBG = 16777215
    SELF.Q.res:Description_SelectedFG = 16777215
    SELF.Q.res:Description_SelectedBG = 32768
  ELSE
    SELF.Q.res:Description_NormalFG = -1
    SELF.Q.res:Description_NormalBG = -1
    SELF.Q.res:Description_SelectedFG = -1
    SELF.Q.res:Description_SelectedBG = -1
  END
  IF (res:despatched = 'PEN')
    SELF.Q.res:Item_Cost_NormalFG = 32768
    SELF.Q.res:Item_Cost_NormalBG = 16777215
    SELF.Q.res:Item_Cost_SelectedFG = 16777215
    SELF.Q.res:Item_Cost_SelectedBG = 32768
  ELSE
    SELF.Q.res:Item_Cost_NormalFG = -1
    SELF.Q.res:Item_Cost_NormalBG = -1
    SELF.Q.res:Item_Cost_SelectedFG = -1
    SELF.Q.res:Item_Cost_SelectedBG = -1
  END
  IF (res:despatched = 'PEN')
    SELF.Q.res:Quantity_NormalFG = 32768
    SELF.Q.res:Quantity_NormalBG = 16777215
    SELF.Q.res:Quantity_SelectedFG = 16777215
    SELF.Q.res:Quantity_SelectedBG = 32768
  ELSE
    SELF.Q.res:Quantity_NormalFG = -1
    SELF.Q.res:Quantity_NormalBG = -1
    SELF.Q.res:Quantity_SelectedFG = -1
    SELF.Q.res:Quantity_SelectedBG = -1
  END
  IF (res:despatched = 'PEN')
    SELF.Q.line_cost_temp_NormalFG = 32768
    SELF.Q.line_cost_temp_NormalBG = 16777215
    SELF.Q.line_cost_temp_SelectedFG = 16777215
    SELF.Q.line_cost_temp_SelectedBG = 32768
  ELSE
    SELF.Q.line_cost_temp_NormalFG = -1
    SELF.Q.line_cost_temp_NormalBG = -1
    SELF.Q.line_cost_temp_SelectedFG = -1
    SELF.Q.line_cost_temp_SelectedBG = -1
  END
   
   
   IF (res:Despatched = 'PEN')
     SELF.Q.res:Part_Number_NormalFG = 32768
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 32768
   ELSIF(res:Despatched = 'CAN')
     SELF.Q.res:Part_Number_NormalFG = 16777215
     SELF.Q.res:Part_Number_NormalBG = 0
     SELF.Q.res:Part_Number_SelectedFG = 0
     SELF.Q.res:Part_Number_SelectedBG = 16777215
   ELSE
     SELF.Q.res:Part_Number_NormalFG = 0
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 8388608
   END
   IF (res:Despatched = 'PEN')
     SELF.Q.res:Description_NormalFG = 32768
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 32768
   ELSIF(res:Despatched = 'CAN')
     SELF.Q.res:Description_NormalFG = 16777215
     SELF.Q.res:Description_NormalBG = 0
     SELF.Q.res:Description_SelectedFG = 0
     SELF.Q.res:Description_SelectedBG = 16777215
   ELSE
     SELF.Q.res:Description_NormalFG = 0
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 8388608
   END
   IF (res:Despatched = 'PEN')
     SELF.Q.res:Item_Cost_NormalFG = 32768
     SELF.Q.res:Item_Cost_NormalBG = 16777215
     SELF.Q.res:Item_Cost_SelectedFG = 16777215
     SELF.Q.res:Item_Cost_SelectedBG = 32768
   ELSIF(res:Despatched = 'CAN')
     SELF.Q.res:Item_Cost_NormalFG = 16777215
     SELF.Q.res:Item_Cost_NormalBG = 0
     SELF.Q.res:Item_Cost_SelectedFG = 0
     SELF.Q.res:Item_Cost_SelectedBG = 16777215
   ELSE
     SELF.Q.res:Item_Cost_NormalFG = 0
     SELF.Q.res:Item_Cost_NormalBG = 16777215
     SELF.Q.res:Item_Cost_SelectedFG = 16777215
     SELF.Q.res:Item_Cost_SelectedBG = 8388608
   END
   IF (res:Despatched = 'PEN')
     SELF.Q.res:Quantity_NormalFG = 32768
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 32768
   ELSIF(res:Despatched = 'CAN')
     SELF.Q.res:Quantity_NormalFG = 16777215
     SELF.Q.res:Quantity_NormalBG = 0
     SELF.Q.res:Quantity_SelectedFG = 0
     SELF.Q.res:Quantity_SelectedBG = 16777215
   ELSE
     SELF.Q.res:Quantity_NormalFG = 0
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 8388608
   END
   IF (res:Despatched = 'PEN')
     SELF.Q.line_cost_temp_NormalFG = 32768
     SELF.Q.line_cost_temp_NormalBG = 16777215
     SELF.Q.line_cost_temp_SelectedFG = 16777215
     SELF.Q.line_cost_temp_SelectedBG = 32768
   ELSIF(res:Despatched = 'CAN')
     SELF.Q.line_cost_temp_NormalFG = 16777215
     SELF.Q.line_cost_temp_NormalBG = 0
     SELF.Q.line_cost_temp_SelectedFG = 0
     SELF.Q.line_cost_temp_SelectedBG = 16777215
   ELSE
     SELF.Q.line_cost_temp_NormalFG = 0
     SELF.Q.line_cost_temp_NormalBG = 16777215
     SELF.Q.line_cost_temp_SelectedFG = 16777215
     SELF.Q.line_cost_temp_SelectedBG = 8388608
   END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, SetQueueRecord, ())


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(7, SetQueueRecord, ())
  Access:STOCK.ClearKey(sto:Location_Key)
  sto:Location    = MainStoreLocation()
  sto:Part_Number = ori:PartNo
  If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      !Found
      tmp:ShelfLocation   = sto:Shelf_Location
      tmp:SecondLocation  = sto:Second_Location
      tmp:Sundry          = sto:Sundry_Item
      tmp:Accessory       = sto:Accessory
      tmp:ExchangeUnit    = sto:ExchangeUnit
      tmp:Suspend         = sto:Suspend
  Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
      tmp:ShelfLocation   = ''
      tmp:SecondLocation  = ''
      tmp:Sundry          = ''
      tmp:Accessory       = ''
      tmp:ExchangeUnit    = ''
      tmp:Suspend         = ''
  End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(7, SetQueueRecord, ())


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


FDCB5.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

EmployeePerformanceCriteria PROCEDURE                 !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:tag              STRING(1)
tmp:StartDate        DATE
tmp:EndDate          DATE
BRW4::View:Browse    VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:User_Code)
                       PROJECT(use:Active)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
use:User_Code          LIKE(use:User_Code)            !List box control field - type derived from field
use:Active             LIKE(use:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Employee Performance Export Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Employee Performance Export Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Select the User'),AT(203,113),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select the Sales Date Range'),AT(248,250),USE(?Prompt1:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Start Date'),AT(248,264),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(320,266,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(388,262),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(248,286),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(320,286,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(388,282),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           LIST,AT(204,124,204,120),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11LI@s1@92L(2)|M~Surname~@s30@120L(2)|M~Forename~@s30@12L(2)|M~User Code~@s3@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(206,171,50,13),USE(?DASREVTAG)
                           BUTTON('sho&W tags'),AT(214,203,70,13),USE(?DASSHOWTAG)
                           BUTTON,AT(415,148),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(415,178),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(415,208),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(380,332),USE(?Button9),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = use:User_Code
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = use:User_Code
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = use:User_Code
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::5:QUEUE = glo:Queue
    ADD(DASBRW::5:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer = use:User_Code
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = use:User_Code
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020347'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('EmployeePerformanceCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:USERS.Open
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:USERS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:StartDate   = Deformat('1/1/1990',@d6)
  tmp:EndDate     = Today()
  ! Save Window Name
   AddToLog('Window','Open','EmployeePerformanceCriteria')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,use:Surname_Active_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,use:Active,1,BRW4)
  BIND('tmp:tag',tmp:tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:tag,BRW4.Q.tmp:tag)
  BRW4.AddField(use:Surname,BRW4.Q.use:Surname)
  BRW4.AddField(use:Forename,BRW4.Q.use:Forename)
  BRW4.AddField(use:User_Code,BRW4.Q.use:User_Code)
  BRW4.AddField(use:Active,BRW4.Q.use:Active)
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','EmployeePerformanceCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020347'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020347'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020347'&'0')
      ***
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button9
      ThisWindow.Update
      EmployeePerformanceExport(tmp:StartDate,tmp:EndDate)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Post(Event:Accepted,?DASUNTAGALL)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = use:User_Code
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = use:User_Code
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue

EmployeePerformanceExport PROCEDURE (func:StartDate,func:EndDate) !Generated from procedure template - Process

Progress:Thermometer BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
SheetDesc            CSTRING(41)
tmp:Orders           LONG
tmp:OrderValue       REAL
tmp:Invoice          LONG
tmp:InvoiceValue     REAL
tmp:StartDate        DATE
tmp:EndDate          DATE
Process:View         VIEW(RETSALES)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,148,60),FONT('Tahoma',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(4,4,140,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(4,30,140,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(44,40,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
LastPctValue        LONG(0)                           !---ClarioNET 20
ClarioNET:PW:UserString   STRING(40)
ClarioNET:PW:PctText      STRING(40)
ClarioNET:PW WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
               PROGRESS,USE(Progress:Thermometer,,?ClarioNET:Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
               STRING(''),AT(0,3,141,10),USE(ClarioNET:PW:UserString),CENTER
               STRING(''),AT(0,30,141,10),USE(ClarioNET:PW:PctText),CENTER
             END
?F1SS                EQUATE(1000)
!# SW2 Extension            STRING(3)
f1Action             BYTE
f1FileName           CSTRING(256)
RowNumber            USHORT
StartRow             LONG
StartCol             LONG
Template             CSTRING(129)
f1Disabled           BYTE(0)
ssFieldQ              QUEUE(tqField).
CQ                    QUEUE(tqField).
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!---------------------
ssInit         ROUTINE
!---------------------
  DO ssBuildQueue                                     ! Initialize f1 field queue
  IF NOT UsBrowse(SsFieldQ,'EmployeePerformanceExport',,,,CQ,SheetDesc,StartRow,StartCol,Template)
    ThisWindow.Kill
    RETURN
  END

!---------------------
ssBuildQueue   ROUTINE
!---------------------
    ssFieldQ.Name = 'use:Surname'
    ssFieldQ.Desc = 'Surname'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'use:Forename'
    ssFieldQ.Desc = 'Forename'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Orders'
    ssFieldQ.Desc = 'Orders'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:OrderValue'
    ssFieldQ.Desc = 'Value'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Invoice'
    ssFieldQ.Desc = 'Invoice'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:InvoiceValue'
    ssFieldQ.Desc = 'Invoice Value'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
  SORT(ssFieldQ,+ssFieldQ.Desc)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  LastPctValue = 0                                    !---ClarioNET 37
  ClarioNET:PW:PctText = ?Progress:PctText{Prop:Text}
  ClarioNET:PW:UserString{Prop:Text} = ?Progress:UserString{Prop:Text}
  ClarioNET:OpenPushWindow(ClarioNET:PW)
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('EmployeePerformanceExport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
    BIND('tmp:Invoice',tmp:Invoice)
    BIND('tmp:InvoiceValue',tmp:InvoiceValue)
    BIND('tmp:OrderValue',tmp:OrderValue)
    BIND('tmp:Orders',tmp:Orders)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  DO ssInit
  Relate:RETSALES.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','EmployeePerformanceExport')
  Bryan.CompFieldColour()
  IF ThisWindow.Response <> RequestCancelled
    UsInitDoc(CQ,?F1ss,StartRow,StartCol,Template)
    ?F1SS{'Sheets("Sheet1").Name'} = SheetDesc
    IF Template = ''
      RowNumber = StartRow + 1
    ELSE
      RowNumber = StartRow
    END
  END
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:RETSALES, ?Progress:PctText, Progress:Thermometer, ProgressMgr, ret:date_booked)
  ThisProcess.AddSortOrder(ret:Date_Booked_Key)
  ThisProcess.AddRange(ret:date_booked,tmp:StartDate,tmp:EndDate)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(RETSALES,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','EmployeePerformanceExport')
  Bryan.CompFieldColour()


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','EmployeePerformanceExport')
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Progress:Cancel
                                                      ! SW2 This section changed
      IF Message('Would you like to view the partially created spreadsheet?','Process Cancelled',ICON:Question,Button:Yes+Button:No,Button:No)|
      = Button:Yes
        UsDeInit(f1FileName, ?F1SS, 1)
      END
      ReturnValue = Level:Fatal
      RETURN ReturnValue
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ThisWindow.Response = RequestCompleted
    f1FileName = 'Employee Performance'
    f1Action = 2
  
    IF UsGetFileName( f1FileName, 1, f1Action )
      UsDeInit( f1FileName, ?F1SS, f1Action )
    END
    ?F1SS{'quit()'}
  ELSE
    IF RowNumber THEN ?F1SS{'quit()'}.
  END
  ReturnValue = PARENT.TakeCloseEvent()
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:ClosePushWindow(ClarioNET:PW)         !---ClarioNET 88
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF LastPctValue <> Progress:Thermometer             !---ClarioNET 99
    IF INLIST(Progress:Thermometer,'5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95')
      LastPctValue = Progress:Thermometer
      ClarioNET:UpdatePushWindow(ClarioNET:PW)
    END
  END
  ReturnValue = PARENT.TakeRecord()
  UsFillRow(CQ,?F1SS,RowNumber,StartCol)
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
DuplicatePurchaseOrderList PROCEDURE (func:PurchaseOrderNumber) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:OrderNumber      STRING(30)
BRW4::View:Browse    VIEW(RETSALES_ALIAS)
                       PROJECT(res_ali:Ref_Number)
                       PROJECT(res_ali:Account_Number)
                       PROJECT(res_ali:Purchase_Order_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
res_ali:Ref_Number     LIKE(res_ali:Ref_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
res_ali:Account_Number LIKE(res_ali:Account_Number)   !List box control field - type derived from field
res_ali:Purchase_Order_Number LIKE(res_ali:Purchase_Order_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Duplicate Purchase Order Number'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Duplicate Purchase Order Number'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Order Number:'),AT(266,115,56,12),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(326,115,88,12),USE(func:PurchaseOrderNumber),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('This order number already exists on the following Sales Transactions'),AT(216,132,244,20),USE(?Prompt2),CENTER,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(218,155,246,149),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('50L(2)|M~Sales Number~@s8@113L(2)|M~Company Name~@s30@60L(2)|M~Account Number~@s' &|
   '15@'),FROM(Queue:Browse)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Button1),TRN,FLAT,LEFT,ICON('closep.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020346'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('DuplicatePurchaseOrderList')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:RETSALES_ALIAS.Open
  Relate:SUBTRACC.Open
  SELF.FilesOpened = True
  tmp:OrderNumber = func:PurchaseOrderNumber
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:RETSALES_ALIAS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','DuplicatePurchaseOrderList')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,res_ali:Purchase_Number_Key)
  BRW4.AddRange(res_ali:Purchase_Order_Number,tmp:OrderNumber)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,res_ali:Purchase_Order_Number,1,BRW4)
  BRW4.AddField(res_ali:Ref_Number,BRW4.Q.res_ali:Ref_Number)
  BRW4.AddField(sub:Company_Name,BRW4.Q.sub:Company_Name)
  BRW4.AddField(res_ali:Account_Number,BRW4.Q.res_ali:Account_Number)
  BRW4.AddField(res_ali:Purchase_Order_Number,BRW4.Q.res_ali:Purchase_Order_Number)
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES_ALIAS.Close
    Relate:SUBTRACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','DuplicatePurchaseOrderList')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020346'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020346'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020346'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(4, SetQueueRecord, ())
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number  = res_ali:Account_Number
  If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      !Found
  
  Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(4, SetQueueRecord, ())


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

