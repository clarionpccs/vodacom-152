

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABEIP.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBG01009.INC'),ONCE        !Local module procedure declarations
                     END


TurnaroundPerformanceCriteria PROCEDURE               !Generated from procedure template - Window

tmp:IncludeNonAccessories BYTE(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:IncludeAccessories BYTE(0)
ExportQueue          QUEUE,PRE(tmpque)
Day                  STRING(30)
HitRate              LONG
QtyOrders            LONG
Value                REAL
                     END
savepath             STRING(255)
save_ret_id          USHORT,AUTO
tmp:BackOrders       BYTE(0)
save_res_id          USHORT,AUTO
window               WINDOW('Turnaround Performance Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Turnaround Performance Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Turnaround Performance Criteria'),USE(?Tab1)
                           PROMPT('Start Date'),AT(221,166),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(297,166,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(365,162),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(221,188),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(297,188,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(365,184),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           CHECK('Include Exchange Accessory Sales'),AT(297,208),USE(tmp:IncludeAccessories),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Include Accessories'),TIP('Include Accessories'),VALUE('1','0')
                           CHECK('Include Non-Exchange Accessory Sales'),AT(297,228),USE(tmp:IncludeNonAccessories),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Include Non-Accessories'),TIP('Include Non-Accessories'),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?Button4),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020351'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('TurnaroundPerformanceCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RETSALES.Open
  Access:RETSTOCK.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020351'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020351'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020351'&'0')
      ***
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      savepath = path()
      glo:file_name = 'TURNPERM.CSV'
      If not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !Failed
          setpath(savepath)
      else!If not filedialog
          !Found
          setpath(savepath)
      
          Setcursor(Cursor:Wait)
      
      !Build up the temp queue first
          Clear(ExportQueue)
          Free(ExportQueue)
          Loop x# = 0 To 15
              If x# <> 15
                  tmpque:Day = x#
              Else !If x# <> 15
                  tmpque:Day = 'Over 14'
              End !If x# <> 15
              tmpque:HitRate      = 0
              tmpque:QtyOrders    = 0
              tmpque:Value        = 0
              Add(ExportQueue)
          End !Loop x# = 1 To 16
      
          Save_ret_ID = Access:RETSALES.SaveFile()
          Access:RETSALES.ClearKey(ret:Date_Booked_Key)
          ret:date_booked = tmp:StartDate
          Set(ret:Date_Booked_Key,ret:Date_Booked_Key)
          Loop
              If Access:RETSALES.NEXT()
                 Break
              End !If
              If ret:date_booked > tmp:EndDate      |
                  Then Break.  ! End If
      
      !Check Critiria
              If tmp:IncludeNonAccessories And ret:Payment_Method = 'EXC'
                  Cycle
              End !If tmp:IncludeNonAccessories And ret:Payment_Method = 'EXC'
      
              If tmp:IncludeAccessories And ret:Payment_Method = 'EXC'
                  Cycle
              End !If tmp:InvoiceAccessories And ret:Payment_Method = 'EXC'
      
      !Need to exclude Back Orders. Lets try and find some
              tmp:BackOrders = 0
              Save_res_ID = Access:RETSTOCK.SaveFile()
              Access:RETSTOCK.ClearKey(res:Part_Number_Key)
              res:Ref_Number  = ret:Ref_Number
              Set(res:Part_Number_Key,res:Part_Number_Key)
              Loop
                  If Access:RETSTOCK.NEXT()
                     Break
                  End !If
                  If res:Ref_Number  <> ret:Ref_Number      |
                      Then Break.  ! End If
                  If res:Order_Number <> ''
                      tmp:BackORders = 1
                      Break
                  End !If res:Order_Number <> ''
              End !Loop
              Access:RETSTOCK.RestoreFile(Save_res_ID)
      
              If tmp:BackOrders
                  Cycle
              End !If tmp:BackOrders
      
      !Lets do some exporting
      
              If ret:Date_Despatched = ret:Date_Booked
                  tmpque:Day  = '0'
                  Get(ExportQueue,tmpque:Day)
                  If ~Error()
      
                  End !If ~Error()
      
              End !If ret:Date_Despatched = ret:Date_Booked
      
      
          End !Loop
          Access:RETSALES.RestoreFile(Save_ret_ID)
          Setcursor()
      End!If not filedialog
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
DailySparesAnalysisCriteria PROCEDURE                 !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:SiteLocation     STRING(30)
tmp:Supplier         STRING(30)
savepath             STRING(255)
save_ret_id          USHORT,AUTO
tmp:SalesTotal       REAL
tmp:BackOrderTotal   REAL
tmp:OldestDate       DATE
tmp:StockValue       REAL
tmp:OldestOrder      DATE
save_res_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
save_ord_id          USHORT,AUTO
window               WINDOW('Daily Spares Analysis Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Daily Spares Analysis Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Daily Spares Analysis Criteria'),USE(?Tab1)
                           PROMPT('Start Date'),AT(225,166),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6),AT(301,166,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(369,164),USE(?LookupStartDate),SKIP,TRN,FLAT,FONT('Tahoma',8,,,CHARSET:ANSI),ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(225,186),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6),AT(301,186,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(369,182),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Site Location'),AT(225,206),USE(?tmp:SiteLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(301,206,124,10),USE(tmp:SiteLocation),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Site Location'),TIP('Site Location'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(429,202),USE(?LookupLocation),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Supplier'),AT(225,226),USE(?tmp:Supplier:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(301,226,124,10),USE(tmp:Supplier),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Supplier'),TIP('Supplier'),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                           BUTTON,AT(429,222),USE(?LookupSupplier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?Button6),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:SiteLocation                Like(tmp:SiteLocation)
look:tmp:Supplier                Like(tmp:Supplier)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020344'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DailySparesAnalysisCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:RETSALES.Open
  Access:SUPPLIER.UseFile
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:ORDERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:SiteLocation{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?tmp:SiteLocation{Prop:Tip}
  END
  IF ?tmp:SiteLocation{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?tmp:SiteLocation{Prop:Msg}
  END
  IF ?tmp:Supplier{Prop:Tip} AND ~?LookupSupplier{Prop:Tip}
     ?LookupSupplier{Prop:Tip} = 'Select ' & ?tmp:Supplier{Prop:Tip}
  END
  IF ?tmp:Supplier{Prop:Msg} AND ~?LookupSupplier{Prop:Msg}
     ?LookupSupplier{Prop:Msg} = 'Select ' & ?tmp:Supplier{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:RETSALES.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Locations
      PickSuppliers
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020344'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020344'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020344'&'0')
      ***
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:SiteLocation
      IF tmp:SiteLocation OR ?tmp:SiteLocation{Prop:Req}
        loc:Location = tmp:SiteLocation
        !Save Lookup Field Incase Of error
        look:tmp:SiteLocation        = tmp:SiteLocation
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:SiteLocation = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:SiteLocation = look:tmp:SiteLocation
            SELECT(?tmp:SiteLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loc:Location = tmp:SiteLocation
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:SiteLocation = loc:Location
          Select(?+1)
      ELSE
          Select(?tmp:SiteLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:SiteLocation)
    OF ?tmp:Supplier
      IF tmp:Supplier OR ?tmp:Supplier{Prop:Req}
        sup:Company_Name = tmp:Supplier
        !Save Lookup Field Incase Of error
        look:tmp:Supplier        = tmp:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            tmp:Supplier = look:tmp:Supplier
            SELECT(?tmp:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSupplier
      ThisWindow.Update
      sup:Company_Name = tmp:Supplier
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?tmp:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Supplier)
    OF ?Button6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
      savepath = path()
      glo:file_name = 'EXPJOBS.CSV'
      If not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !Failed
          setpath(savepath)
      else!If not filedialog
          !Found
          setpath(savepath)
      
          Loop x# = tmp:StartDate To tmp:EndDate
              Clear(gen:Record)
              gen:Line1   = Format(x#,@d6)
              Access:EXPGEN.Insert()
      
      !Find the invoiced sales
              Save_ret_ID = Access:RETSALES.SaveFile()
              Access:RETSALES.ClearKey(ret:Date_Booked_Key)
              ret:date_booked = x#
              Set(ret:Date_Booked_Key,ret:Date_Booked_Key)
              Loop
                  If Access:RETSALES.NEXT()
                     Break
                  End !If
                  If ret:date_booked <> x#      |
                      Then Break.  ! End If
      
                  If ret:Invoice_Number = ''
                      Cycle
                  End !If ret:Invoice_Number = ''
      
                  tmp:SalesTotal      = 0
                  tmp:BackOrderTotal  = 0
                  tmp:OldestDate      = 0
      
                  Save_res_ID = Access:RETSTOCK.SaveFile()
                  Access:RETSTOCK.ClearKey(res:Part_Number_Key)
                  res:Ref_Number  = ret:Ref_Number
                  Set(res:Part_Number_Key,res:Part_Number_Key)
                  Loop
                      If Access:RETSTOCK.NEXT()
                         Break
                      End !If
                      If res:Ref_Number  <> ret:Ref_Number      |
                          Then Break.  ! End If
                      If res:Despatched = 'YES'
                          !Non-BackOrder
                          tmp:SalesTotal += res:Item_Cost * res:Quantity
                      Else !If res:Despathced = 'YES'
                          tmp:BackOrderTotal += res:Item_cost * res:Quantity
                          If res:Date_Ordered <> ''
                              If tmp:OldestDate = 0
                                  tmp:OldestDate  = res:Date_Ordered
                              End !If tmp:OldestDate = 0
                              If res:Date_Ordered < tmp:OldestDate
                                  tmp:OldestDate  = res:Date_Ordered
                              End !If res:Date_Ordered < tmp:OldestDate
                          End !If res:Date_Ordered <> ''
                      End !If res:Despathced = 'YES'
                  End !Loop
                  Access:RETSTOCK.RestoreFile(Save_res_ID)
      
                  gen:Line1   = Clip(gen:Line1) & ',' & tmp:SalesTotal
                  gen:Line1   = Clip(gen:Line1) & ',' & tmp:BackOrderTotal
                  gen:Line1   = Clip(gen:Line1) & ',' & tmp:SalesTotal + tmp:BackOrderTotal
      
                  tmp:StockValue = 0
      
                  Save_sto_ID = Access:STOCK.SaveFile()
                  If tmp:SiteLocation <> ''
                      Access:STOCK.ClearKey(sto:Location_Key)
                      sto:Location    = tmp:SiteLocation
                      Set(sto:Location_Key,sto:Location_Key)
                  Else !If tmp:Location <> ''
                      Set(sto:Ref_Number_Key)
                  End !If tmp:Location <> ''
                  Loop
                      If Access:STOCK.NEXT()
                         Break
                      End !If
                      If tmp:SiteLocation <> ''
                          If sto:Location    <> tmp:SiteLocation      |
                              Then Break.  ! End If
                      End !If tmp:SiteLocation <> ''
                      tmp:StockValue += sto:Retail_Cost * sto:Quantity_stock
                  End !Loop
                  Access:STOCK.RestoreFile(Save_sto_ID)
      
                  gen:Line1   = Clip(gen:Line1) & ',' & tmp:StockValue
                  gen:Line1   = Clip(gen:Line1) & ',' & Format(tmp:OldestDate,@d6)
      
      ! Oldest Order From Supplier?!?!
                  tmp:OldestOrder = 0
                  Save_ord_ID = Access:ORDERS.SaveFile()
      
                  If tmp:Supplier <> ''
      
                      Access:ORDERS.ClearKey(ord:Supplier_Key)
                      ord:Supplier     = tmp:Supplier
                      Set(ord:Supplier_Key,ord:Supplier_Key)
                  Else!If tmp:Supplier <> ''
                      Set(ord:Order_Number_Key)
                  End!If tmp:Supplier <> ''
                  Loop
                      If Access:ORDERS.NEXT()
                         Break
                      End !If
                      If tmp:Supplier <> ''
                          If ord:Supplier     <> tmp:Supplier      |
                              Then Break.  ! End If
                      End !If tmp:Supplier <> ''
                      If ord:All_Received <> 'YES'
                          tmp:OldestOrder = ord:Date
                          Break
                      End !If ord:All_Received <> 'YES'
                  End !Loop
                  Access:ORDERS.RestoreFile(Save_ord_ID)
      
                  gen:Line1   = Clip(gen:Line1) & ',' & tmp:OldestOrder
                  gen:Line1   = Clip(gen:Line1) & ',,'
                  Access:EXPGEN.Insert()
      
              End !Loop
              Access:RETSALES.RestoreFile(Save_ret_ID)
          End !Loop x# = tmp:StartDate To tmp:EndDate
      
      End!If not filedialog
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:SiteLocation
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, AlertKey)
    END
  OF ?tmp:Supplier
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Supplier, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Supplier, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()






BackOrderProcessing PROCEDURE                         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::25:TAGFLAG         BYTE(0)
DASBRW::25:TAGMOUSE        BYTE(0)
DASBRW::25:TAGDISPSTATUS   BYTE(0)
DASBRW::25:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_orp_id          USHORT,AUTO
save_tratmp_id       USHORT,AUTO
save_ret_ali_id      USHORT,AUTO
tmp:StockBelow       LONG(1)
save_res_id          USHORT,AUTO
save_retpar_id       USHORT,AUTO
save_retacc_id       USHORT,AUTO
tmp:CurrentStock     LONG
tmp:TotalOrdered     LONG
tmp:TotalToShip      LONG
tmp:CloseWindow      BYTE(0)
tmp:CurrentStockAfter LONG
tmp:QuantityAvailable LONG
tmp:QuantityOnOrder  LONG
tmp:Location         STRING(30)
tmp:VatRate          REAL
tmp:UseDeliveryAddress BYTE(0)
tmp:UseInvoice       BYTE(0)
tmp:PaymentMethod    STRING('0 {2}')
AccountQueue         QUEUE,PRE(acctmp)
AccountNumber        STRING(30),NAME('acctmp:AccountNumber')
SaleNumber           LONG,NAME('acctmp:SaleNumber')
PaymentMethod        STRING('0 {2}'),NAME('acctmp:PaymentMethod')
PostcodeDelivery     STRING(10),NAME('acctmp:PostcodeDelivery')
CompanyNameDelivery  STRING(30),NAME('acctmp:CompanyNameDelivery')
BuildingNameDelivery STRING(30),NAME('acctmp:BuildingNameDelivery')
AddressLine1Delivery STRING(30),NAME('acctmp:AddressLine1Delivery')
AddressLine2Delivery STRING(30),NAME('acctmp:AddressLine2Delivery')
AddressLine3Delivery STRING(30),NAME('acctmp:AddressLine3Delivery')
TelephoneDelivery    STRING(15),NAME('acctmp:TelephoneDelivery')
FaxNumberDelivery    STRING(15),NAME('acctmp:FaxNumberDelivery')
                     END
tmp:ShelfLocation    STRING(30)
tmp:Tag              STRING(1)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Location
tratmp:Company_Name    LIKE(tratmp:Company_Name)      !List box control field - type derived from field
tratmp:RefNumber       LIKE(tratmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(RETPARTSLIST)
                       PROJECT(retpar:PartNumber)
                       PROJECT(retpar:Description)
                       PROJECT(retpar:QuantityBackOrder)
                       PROJECT(retpar:QuantityOnOrder)
                       PROJECT(retpar:QuantityStock)
                       PROJECT(retpar:RecordNumber)
                       PROJECT(retpar:Location)
                       PROJECT(retpar:Processed)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
retpar:PartNumber      LIKE(retpar:PartNumber)        !List box control field - type derived from field
retpar:PartNumber_Tip  STRING(80)                     !Field tooltip
retpar:Description     LIKE(retpar:Description)       !List box control field - type derived from field
retpar:Description_Tip STRING(80)                     !Field tooltip
tmp:ShelfLocation      LIKE(tmp:ShelfLocation)        !List box control field - type derived from local data
tmp:ShelfLocation_Tip  STRING(80)                     !Field tooltip
retpar:QuantityBackOrder LIKE(retpar:QuantityBackOrder) !List box control field - type derived from field
retpar:QuantityOnOrder LIKE(retpar:QuantityOnOrder)   !List box control field - type derived from field
retpar:QuantityStock   LIKE(retpar:QuantityStock)     !List box control field - type derived from field
retpar:RecordNumber    LIKE(retpar:RecordNumber)      !Primary key field - type derived from field
retpar:Location        LIKE(retpar:Location)          !Browse key field - type derived from field
retpar:Processed       LIKE(retpar:Processed)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(RETACCOUNTSLIST)
                       PROJECT(retacc:CompanyName)
                       PROJECT(retacc:AccountNumber)
                       PROJECT(retacc:SaleNumber)
                       PROJECT(retacc:DateOrdered)
                       PROJECT(retacc:QuantityOrdered)
                       PROJECT(retacc:QuantityToShip)
                       PROJECT(retacc:RecordNumber)
                       PROJECT(retacc:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
retacc:CompanyName     LIKE(retacc:CompanyName)       !List box control field - type derived from field
retacc:AccountNumber   LIKE(retacc:AccountNumber)     !List box control field - type derived from field
retacc:SaleNumber      LIKE(retacc:SaleNumber)        !List box control field - type derived from field
retacc:DateOrdered     LIKE(retacc:DateOrdered)       !List box control field - type derived from field
retacc:QuantityOrdered LIKE(retacc:QuantityOrdered)   !List box control field - type derived from field
retacc:QuantityToShip  LIKE(retacc:QuantityToShip)    !List box control field - type derived from field
retacc:RecordNumber    LIKE(retacc:RecordNumber)      !Primary key field - type derived from field
retacc:RefNumber       LIKE(retacc:RefNumber)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK13::retacc:RefNumber    LIKE(retacc:RefNumber)
! ---------------------------------------- Higher Keys --------------------------------------- !
! ---------------------------------------- Higher Keys --------------------------------------- !
HK18::retpar:Location     LIKE(retpar:Location)
HK18::retpar:Processed    LIKE(retpar:Processed)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB17::View:FileDropCombo VIEW(TRADETMP)
                       PROJECT(tratmp:Company_Name)
                       PROJECT(tratmp:RefNumber)
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask4          DECIMAL(10,0,0)                  !Xplore
XploreMask14          DECIMAL(10,0,0)                 !Xplore
XploreTitle4         STRING(' ')                      !Xplore
xpInitialTab4        SHORT                            !Xplore
XploreMask5          DECIMAL(10,0,0)                  !Xplore
XploreMask15          DECIMAL(10,0,0)                 !Xplore
XploreTitle5         STRING(' ')                      !Xplore
xpInitialTab5        SHORT                            !Xplore
window               WINDOW('Back Order Processing'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),IMM,WALLPAPER('sbback.jpg'),TILED,TIMER(50),GRAY,DOUBLE
                       PROMPT('Back Order Processing'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,396,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(604,396),USE(?Cancel),TRN,FLAT,ICON('closep.jpg')
                       PANEL,AT(4,26,672,32),USE(?Panel1:2),FILL(09A6A7CH)
                       PROMPT('Hide if "In Stock" is below'),AT(8,36),USE(?tmp:StockBelow:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s8),AT(116,36,64,10),USE(tmp:StockBelow),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Hide if "In Stock" is below'),TIP('Hide if "In Stock" is below'),UPR
                       SHEET,AT(4,62,356,332),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Un-Processed Orders'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(368,360),USE(?CancelPart),TRN,FLAT,LEFT,ICON('cantagp.jpg')
                         END
                         TAB('Processed Orders'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(8,364),USE(?GeneratePickNotes),TRN,FLAT,LEFT,ICON('genpickp.jpg')
                         END
                       END
                       LIST,AT(8,106,348,256),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),TIP('Browse Parts'),FORMAT('72L(2)|MP~Part Number~@s30@84L(2)|MP~Description~@s30@68L(2)|MP~Shelf Location~@' &|
   's30@37L(2)|M~Bck Order~@s8@38L(2)|M~On Order~@s8@45L(2)|M~In Stock~@s8@'),FROM(Queue:Browse)
                       LIST,AT(368,104,304,226),USE(?List:2),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)|J@s1@90L(2)|~Company Name~@s30@60L(2)|~Account Number~@s30@32R(2)|~Sale N' &|
   'o~@s8@39R(2)|~Requested~@d6@27R(2)|~Req~@s5@32R(2)|~To Ship~@s5@'),FROM(Queue:Browse:1)
                       BUTTON,AT(544,360),USE(?ProcessItems),TRN,FLAT,LEFT,ICON('proitmsp.jpg')
                       BUTTON,AT(608,360),USE(?AutoAllocate),TRN,FLAT,LEFT,ICON('autoallp.jpg')
                       BUTTON,AT(188,28),USE(?Recalculate),TRN,FLAT,LEFT,ICON('calcbakp.jpg')
                       COMBO(@s20),AT(8,82,124,10),USE(tmp:Location),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                       ENTRY(@s30),AT(8,92,124,10),USE(retpar:PartNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Part Number'),TIP('Part Number'),UPR
                       SHEET,AT(364,62,312,332),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('By Trade Account'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON('&Insert'),AT(524,82,42,12),USE(?Insert),HIDE
                           BUTTON('&Change'),AT(564,82,42,12),USE(?Change),HIDE,DEFAULT
                           BUTTON('&Delete'),AT(604,82,42,12),USE(?Delete),HIDE
                           BUTTON('&Rev tags'),AT(452,190,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(452,214,70,13),USE(?DASSHOWTAG),HIDE
                           PROMPT('Actual Stock Value'),AT(368,334),USE(?RETPAR:QuantityStock:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(460,334,40,10),USE(retpar:QuantityStock),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Quantity In Stock'),TIP('Quantity In Stock'),UPR,READONLY
                           ENTRY(@s8),AT(628,346,40,10),USE(tmp:TotalToShip),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Total To Ship'),TIP('Total To Ship'),UPR,READONLY
                           BUTTON,AT(504,78),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON,AT(368,76),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(436,78),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           PROMPT('Stock After Processing'),AT(368,346),USE(?tmp:CurrentStockAfter:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(460,346,40,10),USE(tmp:CurrentStockAfter),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Current Stock After'),TIP('Current Stock After'),UPR,READONLY
                           PROMPT('Totals'),AT(544,346),USE(?Prompt4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(580,346,40,10),USE(tmp:TotalOrdered),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Total Ordered'),TIP('Total Ordered'),UPR,READONLY
                         END
                       END
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW4::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet1) = 2
BRW5                 CLASS(BrowseClass)               !Browse using ?List:2
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW5::EIPManager     BrowseEIPManager                 !Browse EIP Manager for Browse using ?List:2
EditInPlace::RETACC:QuantityToShip EditEntryClass     !Edit-in-place class for field retacc:QuantityToShip
FDCB17               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore4              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore4Step1         StepStringClass !STRING          !Xplore: Column displaying retpar:PartNumber
Xplore4Locator1      StepLocatorClass                 !Xplore: Column displaying retpar:PartNumber
Xplore4Step2         StepStringClass !STRING          !Xplore: Column displaying retpar:Description
Xplore4Locator2      StepLocatorClass                 !Xplore: Column displaying retpar:Description
Xplore4Step3         StepCustomClass !                !Xplore: Column displaying tmp:ShelfLocation
Xplore4Locator3      StepLocatorClass                 !Xplore: Column displaying tmp:ShelfLocation
Xplore4Step4         StepLongClass   !LONG            !Xplore: Column displaying retpar:QuantityBackOrder
Xplore4Locator4      StepLocatorClass                 !Xplore: Column displaying retpar:QuantityBackOrder
Xplore4Step5         StepLongClass   !LONG            !Xplore: Column displaying retpar:QuantityOnOrder
Xplore4Locator5      StepLocatorClass                 !Xplore: Column displaying retpar:QuantityOnOrder
Xplore4Step6         StepLongClass   !LONG            !Xplore: Column displaying retpar:QuantityStock
Xplore4Locator6      StepLocatorClass                 !Xplore: Column displaying retpar:QuantityStock
Xplore4Step7         StepLongClass   !LONG            !Xplore: Column displaying retpar:RecordNumber
Xplore4Locator7      StepLocatorClass                 !Xplore: Column displaying retpar:RecordNumber
Xplore4Step8         StepStringClass !STRING          !Xplore: Column displaying retpar:Location
Xplore4Locator8      StepLocatorClass                 !Xplore: Column displaying retpar:Location
Xplore4Step9         StepCustomClass !BYTE            !Xplore: Column displaying retpar:Processed
Xplore4Locator9      StepLocatorClass                 !Xplore: Column displaying retpar:Processed
Xplore5              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore5Step1         StepCustomClass !                !Xplore: Column displaying tmp:Tag
Xplore5Locator1      StepLocatorClass                 !Xplore: Column displaying tmp:Tag
Xplore5Step2         StepStringClass !STRING          !Xplore: Column displaying retacc:CompanyName
Xplore5Locator2      StepLocatorClass                 !Xplore: Column displaying retacc:CompanyName
Xplore5Step3         StepStringClass !STRING          !Xplore: Column displaying retacc:AccountNumber
Xplore5Locator3      StepLocatorClass                 !Xplore: Column displaying retacc:AccountNumber
Xplore5Step4         StepLongClass   !LONG            !Xplore: Column displaying retacc:SaleNumber
Xplore5Locator4      StepLocatorClass                 !Xplore: Column displaying retacc:SaleNumber
Xplore5Step5         StepCustomClass !DATE            !Xplore: Column displaying retacc:DateOrdered
Xplore5Locator5      StepLocatorClass                 !Xplore: Column displaying retacc:DateOrdered
Xplore5Step6         StepLongClass   !LONG            !Xplore: Column displaying retacc:QuantityOrdered
Xplore5Locator6      StepLocatorClass                 !Xplore: Column displaying retacc:QuantityOrdered
Xplore5Step7         StepLongClass   !LONG            !Xplore: Column displaying retacc:QuantityToShip
Xplore5Locator7      StepLocatorClass                 !Xplore: Column displaying retacc:QuantityToShip
Xplore5Step8         StepLongClass   !LONG            !Xplore: Column displaying retacc:RecordNumber
Xplore5Locator8      StepLocatorClass                 !Xplore: Column displaying retacc:RecordNumber
Xplore5Step9         StepLongClass   !LONG            !Xplore: Column displaying retacc:RefNumber
Xplore5Locator9      StepLocatorClass                 !Xplore: Column displaying retacc:RefNumber

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::25:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW5.UpdateBuffer
   glo:Queue.Pointer = retacc:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = retacc:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse:1.tmp:Tag = tmp:Tag
  IF (tmp:Tag = '*')
    Queue:Browse:1.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = retacc:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::25:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::25:QUEUE = glo:Queue
    ADD(DASBRW::25:QUEUE)
  END
  FREE(glo:Queue)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::25:QUEUE.Pointer = retacc:RecordNumber
     GET(DASBRW::25:QUEUE,DASBRW::25:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = retacc:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASSHOWTAG Routine
   CASE DASBRW::25:TAGDISPSTATUS
   OF 0
      DASBRW::25:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::25:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::25:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
GetItemCost     Routine
    !Get the Trade Account from the Sale to determine the price
    !of the parts
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = ret:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:RetailZeroParts = 1
                res:Item_Cost = 0
            Else !If tra:RetailZeroParts = 1
                !Check the Trade Account Retail Price Structure
                !Only applies if it is a new record
                !'RET' = Retail Price
                !Other = Trade Price
                If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                    Case sub:Retail_Price_Structure
                        Of 'RET'
                            res:Item_Cost   = res:Retail_Cost
                        Of 'PUR'
                            res:Item_Cost   = res:Purchase_Cost
                        Else
                            res:Item_Cost   = res:Sale_Cost
                    End !Case sub:Retail_Price_Structure
                Else !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                    Case tra:Retail_Price_Structure
                        Of 'RET'
                            res:Item_Cost   = res:Retail_Cost
                        Of 'PUR'
                            res:Item_Cost   = res:Purchase_Cost
                        Else
                            res:Item_Cost   = res:Sale_Cost
                    End !Case tra:Retail_Price_Structure
                End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
            End !If tra:RetailZeroParts = 1
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
AddRetailPart       Routine
    !Now take part from stock and attach to sale
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number  = retpar:PartRefNumber
    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        !If the ship quantity is still less that the quantity in stock,
        !or the quantity required, then create a "Pending Part" for this amount.
        Do GetItemCost

        !Create the normal part
        If Access:RETSTOCK.PrimeRecord() = Level:Benign
            res:Ref_Number      = ret:Ref_Number
            res:Part_Number     = retpar:PartNumber
            res:Description     = retpar:Description
            res:Supplier        = sto:Supplier
            res:Purchase_Cost   = sto:Purchase_Cost
            res:Sale_Cost       = sto:Sale_Cost
            res:Retail_Cost     = sto:Retail_Cost
            res:AccessoryCost   = sto:AccessoryCost

            Do GetItemCost

            res:Quantity        = retacc:QuantityToShip
            res:Despatched      = 'PIK'
            res:Part_Ref_Number = sto:Ref_Number
            res:Date_Received   = Today()
            res:Previous_Sale_Number = retacc:SaleNumber
            Access:RETSALES_ALIAS.ClearKey(res_ali:Ref_Number_Key)
            res_ali:Ref_Number = retacc:SaleNumber
            If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
                !Found
                !If this is back order, from a back order try and find the original part
                !on the original sale, and look for the purchase order number
                If res_ali:Purchase_Order_Number <> 'VARIOUS'
                    res:Purchase_Order_Number   = res_ali:Purchase_Order_Number
                Else !If res_ali:Purchase_Order_Number <> 'VARIOUS'
                    Access:RETSTOCK_ALIAS.ClearKey(ret_ali:Record_Number_Key)
                    ret_ali:Record_Number = retacc:OrigRecordNumber
                    If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                        !Found
                        res:Purchase_Order_Number   = ret_ali:Purchase_Order_Number
                    Else!If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                End !If res_ali:Purchase_Order_Number <> 'VARIOUS'

            Else!If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign

            If Access:RETSTOCK.TryInsert() = Level:Benign
                !Insert Successful
                !Create a "Pending Part" if the original SaLe request has not been fulfilled

                If retacc:QuantityToShip < retacc:QuantityOrdered
                    If Access:RETSTOCK.PrimeRecord() = Level:Benign
                        res:Ref_Number      = ret:Ref_Number
                        res:Part_Number     = retpar:PartNumber
                        res:Description     = retpar:Description
                        res:Supplier        = sto:Supplier
                        res:Purchase_Cost   = sto:Purchase_Cost
                        res:Sale_Cost       = sto:Sale_Cost
                        res:Retail_Cost     = sto:Retail_Cost
                        res:AccessoryCost   = sto:AccessoryCost

                        Do GetItemCost

                        res:Quantity        = retacc:QuantityOrdered - retacc:QuantityToShip
                        res:Despatched      = 'PEN'
                        res:Part_Ref_Number = sto:Ref_Number
                        res:Date_Received   = Today()
                        res:Previous_Sale_Number    = retacc:SaleNumber
                        Access:RETSALES_ALIAS.ClearKey(res_ali:Ref_Number_Key)
                        res_ali:Ref_Number = retacc:SaleNumber
                        If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
                            !Found
                            !If this is back order, from a back order try and find the original part
                            !on the original sale, and look for the purchase order number
                            If res_ali:Purchase_Order_Number <> 'VARIOUS'
                                res:Purchase_Order_Number   = res_ali:Purchase_Order_Number
                            Else !If res_ali:Purchase_Order_Number <> 'VARIOUS'
                                Access:RETSTOCK_ALIAS.ClearKey(ret_ali:Record_Number_Key)
                                ret_ali:Record_Number = retacc:OrigRecordNumber
                                If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                                    !Found
                                    res:Purchase_Order_Number   = ret_ali:Purchase_Order_Number
                                Else!If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                            End !If res_ali:Purchase_Order_Number <> 'VARIOUS'
                        Else!If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
                        If Access:RETSTOCK.TryInsert() = Level:Benign
                            !Insert Successful
                        Else !If Access:RETSTOCK.TryInsert() = Level:Benign
                            !Insert Failed
                        End !If Access:RETSTOCK.TryInsert() = Level:Benign
                    End !If Access:RETSTOCK.PrimeRecord() = Level:Benign
                End !If retacc:QuantityToShip < retacc:QuantityOrdered

                Save_ret_ali_ID = Access:RETSTOCK_ALIAS.SaveFile()
                Access:RETSTOCK_ALIAS.ClearKey(ret_ali:Despatched_Only_Key)
                ret_ali:Ref_Number = retacc:SaleNumber
                ret_ali:Despatched = 'PEN'
                Set(ret_ali:Despatched_Only_Key,ret_ali:Despatched_Only_Key)
                Loop
                    If Access:RETSTOCK_ALIAS.NEXT()
                       Break
                    End !If
                    If ret_ali:Ref_Number <> retacc:SaleNumber      |
                    Or ret_ali:Despatched <> 'PEN'      |
                        Then Break.  ! End If
                    !I've changed a key element, but I'm breaking out straight
                    !away, so it's shouldn't matter that I'm not saving the
                    !position.
                    If ret_ali:Part_Number = retpar:PartNumber And ret_ali:Quantity = retacc:QuantityOrdered
                        Access:RETSTOCK.ClearKey(res:Record_Number_Key)
                        res:Record_Number = ret_ali:Record_Number
                        If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                            !Found
                            res:Despatched = 'OLD'
                            res:Previous_Sale_Number = ret:Ref_Number
                            Access:RETSTOCK.TryUpdate()
                            Break

                        Else!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                    End !If res:Part_Number = retpar:PartNumber
                End !Loop
                Access:RETSTOCK_ALIAS.RestoreFile(Save_res_ID)

                !Take Part From Stock
                sto:Quantity_Stock -= retacc:QuantityToShip
                If sto:Quantity_Stock < 0
                    sto:Quantity_Stock = 0
                End !If sto:Quantity_Stock < 0
                If Access:STOCK.TryUpdate() = Level:Benign
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'DEC', | ! Transaction_Type
                                         '', | ! Depatch_Note_Number
                                         ret:Ref_Number, | ! Job_Number
                                         0, | ! Sales_Number
                                         retacc:QuantityToShip, | ! Quantity
                                         sto:Purchase_Cost, | ! Purchase_Cost
                                         sto:Sale_Cost, | ! Sale_Cost
                                         sto:Retail_Cost, | ! Retail_Cost
                                         'RETAIL ITEM SOLD', | ! Notes
                                         'COMPANY: ' & Clip(ret:company_name) & '<13,10>PURCHASE ORDER NO: ' & Clip(RET:Purchase_Order_Number) & |
                                                    '<13,10>CONTACT: ' & CLip(RET:Contact_Name)) ! Information
                        ! Added OK
                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                End !If Acccess:STOCK.TryUpdate() = Level:Benign

            Else !If Access:RETSTOCK.TryInsert() = Level:Benign
                !Insert Failed
            End !If Access:RETSTOCK.TryInsert() = Level:Benign
        End !If Access:RETSTOCK.PrimeRecord() = Level:Benign

    Else! If Access:.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore4.GetBbSize('BackOrderProcessing','?List')    !Xplore
  BRW4.SequenceNbr = 0                                !Xplore
  Xplore5.GetBbSize('BackOrderProcessing','?List:2')  !Xplore
  BRW5.SequenceNbr = 0                                !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020326'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BackOrderProcessing')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:ORDERS.Open
  Relate:RETACCOUNTSLIST.Open
  Relate:RETDESNO.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:TRADETMP.Open
  Relate:VATCODE.Open
  Access:RETSTOCK.UseFile
  Access:RETSALES.UseFile
  Access:STOCK.UseFile
  Access:ORDPARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:RETPARTSLIST,SELF)
  BRW5.Init(?List:2,Queue:Browse:1.ViewPosition,BRW5::View:Browse,Queue:Browse:1,Relate:RETACCOUNTSLIST,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore4.Init(ThisWindow,BRW4,Queue:Browse,window,?List,'sbg01app.INI','>Header',0,BRW4.ViewOrder,Xplore4.RestoreHeader,BRW4.SequenceNbr,XploreMask4,XploreMask14,XploreTitle4,BRW4.FileSeqOn)
  Xplore5.Init(ThisWindow,BRW5,Queue:Browse:1,window,?List:2,'sbg01app.INI','>Header',0,BRW5.ViewOrder,Xplore5.RestoreHeader,BRW5.SequenceNbr,XploreMask5,XploreMask15,XploreTitle5,BRW5.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,retpar:LocPartNumberKey)
  BRW4.AddRange(retpar:Processed)
  BRW4.AddLocator(BRW4::Sort1:Locator)
  BRW4::Sort1:Locator.Init(?retpar:PartNumber,retpar:PartNumber,1,BRW4)
  BRW4.AddSortOrder(,retpar:LocPartNumberKey)
  BRW4.AddRange(retpar:Processed)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(?retpar:PartNumber,retpar:PartNumber,1,BRW4)
  BIND('tmp:ShelfLocation',tmp:ShelfLocation)
  BRW4.AddField(retpar:PartNumber,BRW4.Q.retpar:PartNumber)
  BRW4.AddField(retpar:Description,BRW4.Q.retpar:Description)
  BRW4.AddField(tmp:ShelfLocation,BRW4.Q.tmp:ShelfLocation)
  BRW4.AddField(retpar:QuantityBackOrder,BRW4.Q.retpar:QuantityBackOrder)
  BRW4.AddField(retpar:QuantityOnOrder,BRW4.Q.retpar:QuantityOnOrder)
  BRW4.AddField(retpar:QuantityStock,BRW4.Q.retpar:QuantityStock)
  BRW4.AddField(retpar:RecordNumber,BRW4.Q.retpar:RecordNumber)
  BRW4.AddField(retpar:Location,BRW4.Q.retpar:Location)
  BRW4.AddField(retpar:Processed,BRW4.Q.retpar:Processed)
  BRW5.Q &= Queue:Browse:1
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,retacc:AccountNumberKey)
  BRW5.AddRange(retacc:RefNumber)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,retacc:AccountNumber,1,BRW5)
  BIND('tmp:Tag',tmp:Tag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(tmp:Tag,BRW5.Q.tmp:Tag)
  BRW5.AddField(retacc:CompanyName,BRW5.Q.retacc:CompanyName)
  BRW5.AddField(retacc:AccountNumber,BRW5.Q.retacc:AccountNumber)
  BRW5.AddField(retacc:SaleNumber,BRW5.Q.retacc:SaleNumber)
  BRW5.AddField(retacc:DateOrdered,BRW5.Q.retacc:DateOrdered)
  BRW5.AddField(retacc:QuantityOrdered,BRW5.Q.retacc:QuantityOrdered)
  BRW5.AddField(retacc:QuantityToShip,BRW5.Q.retacc:QuantityToShip)
  BRW5.AddField(retacc:RecordNumber,BRW5.Q.retacc:RecordNumber)
  BRW5.AddField(retacc:RefNumber,BRW5.Q.retacc:RefNumber)
  FDCB17.Init(tmp:Location,?tmp:Location,Queue:FileDropCombo.ViewPosition,FDCB17::View:FileDropCombo,Queue:FileDropCombo,Relate:TRADETMP,ThisWindow,GlobalErrors,0,1,0)
  FDCB17.Q &= Queue:FileDropCombo
  FDCB17.AddSortOrder(tratmp:CompanyNameKey)
  FDCB17.AddField(tratmp:Company_Name,FDCB17.Q.tratmp:Company_Name)
  FDCB17.AddField(tratmp:RefNumber,FDCB17.Q.tratmp:RefNumber)
  ThisWindow.AddItem(FDCB17.WindowComponent)
  FDCB17.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:ORDERS.Close
    Relate:RETACCOUNTSLIST.Close
    Relate:RETDESNO.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:TRADETMP.Close
    Relate:VATCODE.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore4.EraseVisual()                             !Xplore
    Xplore5.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore4.sq.Col = Xplore4.CurrentCol
    GET(Xplore4.sq,Xplore4.sq.Col)
    IF Xplore4.ListType = 1 AND BRW4.ViewOrder = False !Xplore
      BRW4.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore4.PutInix('BackOrderProcessing','?List',BRW4.SequenceNbr,Xplore4.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisWindow.Opened                                !Xplore
    Xplore5.sq.Col = Xplore5.CurrentCol
    GET(Xplore5.sq,Xplore5.sq.Col)
    IF Xplore5.ListType = 1 AND BRW5.ViewOrder = False !Xplore
      BRW5.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore5.PutInix('BackOrderProcessing','?List:2',BRW5.SequenceNbr,Xplore5.sq.AscDesc) !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore4.Kill()                                      !Xplore
  Xplore5.Kill()                                      !Xplore
  Remove(RETACCOUNTSLIST)
  Remove(RETPARTSLIST)
  Remove(TRADETMP)
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateRETACCOUNTSLIST
    ReturnValue = GlobalResponse
  END
  brw4.updateviewrecord()
  retpar:QuantityStock -= tmp:TotalToShip
  Access:RETPARTSLIST.TryUpdate()
  Brw4.ResetSort(1)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore4.Upper = True                                !Xplore
  Xplore5.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore4.AddField(retpar:PartNumber,BRW4.Q.retpar:PartNumber)
  Xplore4.AddField(retpar:Description,BRW4.Q.retpar:Description)
  Xplore4.AddField(retpar:QuantityBackOrder,BRW4.Q.retpar:QuantityBackOrder)
  Xplore4.AddField(retpar:QuantityOnOrder,BRW4.Q.retpar:QuantityOnOrder)
  Xplore4.AddField(retpar:QuantityStock,BRW4.Q.retpar:QuantityStock)
  Xplore4.AddField(retpar:RecordNumber,BRW4.Q.retpar:RecordNumber)
  Xplore4.AddField(retpar:Location,BRW4.Q.retpar:Location)
  Xplore4.AddField(retpar:Processed,BRW4.Q.retpar:Processed)
  BRW4.FileOrderNbr = BRW4.AddSortOrder(,retpar:LocPartNumberKey) !Xplore Sort Order for File Sequence
  BRW4.AddRange(retpar:Processed)                     !Xplore
  BRW4.SetOrder('')                                   !Xplore
  BRW4.ViewOrder = True                               !Xplore
  Xplore4.AddAllColumnSortOrders(1)                   !Xplore
  BRW4.ViewOrder = False                              !Xplore
  Xplore5.AddField(retacc:CompanyName,BRW5.Q.retacc:CompanyName)
  Xplore5.AddField(retacc:AccountNumber,BRW5.Q.retacc:AccountNumber)
  Xplore5.AddField(retacc:SaleNumber,BRW5.Q.retacc:SaleNumber)
  Xplore5.AddField(retacc:DateOrdered,BRW5.Q.retacc:DateOrdered)
  Xplore5.AddField(retacc:QuantityOrdered,BRW5.Q.retacc:QuantityOrdered)
  Xplore5.AddField(retacc:QuantityToShip,BRW5.Q.retacc:QuantityToShip)
  Xplore5.AddField(retacc:RecordNumber,BRW5.Q.retacc:RecordNumber)
  Xplore5.AddField(retacc:RefNumber,BRW5.Q.retacc:RefNumber)
  BRW5.FileOrderNbr = BRW5.AddSortOrder(,retacc:AccountNumberKey) !Xplore Sort Order for File Sequence
  BRW5.AddRange(retacc:RefNumber)                     !Xplore
  BRW5.SetOrder('')                                   !Xplore
  BRW5.ViewOrder = True                               !Xplore
  Xplore5.AddAllColumnSortOrders(1)                   !Xplore
  BRW5.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:CloseWindow = True
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020326'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020326'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020326'&'0')
      ***
    OF ?CancelPart
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelPart, Accepted)
      ! Changing (DBH 22/08/2006) # 7963 - Cancel Tagged Parts
      !Case Missive('Are you sure you want to cancel the selected Back Order Request?','ServiceBase 3g',|
      !               'mquest.jpg','\No|/Yes')
      !    Of 2 ! Yes Button
      !
      !        Access:RETPARTSLIST.Clearkey(retpar:RecordNumberKey)
      !        retpar:RecordNumber = brw4.q.retpar:RecordNumber
      !        If Access:RETPARTSLIST.Tryfetch(retpar:RecordNumberKey) = Level:Benign
      !            !Found
      !            Access:RETSTOCK.ClearKey(res:DespatchedPartKey)
      !            res:Ref_Number  = brw5.q.retacc:SaleNumber
      !            res:Despatched  = 'PEN'
      !            res:Part_Number = retpar:PartNumber
      !            If Access:RETSTOCK.TryFetch(res:DespatchedPartKey) = Level:Benign
      !                !Found
      !                res:Despatched = 'CAN'
      !                If Access:RETSTOCK.Update() = Level:Benign
      !                    Delete(RETPARTSLIST)
      !                End !If Access:RESTOCK.Update() = Level:Benign
      !
      !            Else!If Access:RETSTOCK.TryFetch(res:DespatchedPartKey) = Level:Benign
      !                !Error
      !                Case Missive('Unable to fund the selected part on Sale Number ' & brw5.q.retacc:SaleNumber & '.','ServiceBase 3g',|
      !                               'mstop.jpg','/OK')
      !                    Of 1 ! OK Button
      !                End ! Case Missive
      !            End!If Access:RETSTOCK.TryFetch(res:DespatchedPartKey) = Level:Benign
      !
      !        Else ! If Access:RETPARTSLIST.Tryfetch(retpar:RecordNumberKey) = Level:Benign
      !            !Error
      !        End !If Access:RETPARTSLIST.Tryfetch(retpar:RecordNumberKey) = Level:Benign
      !
      !    Of 1 ! No Button
      !End ! Case Missive
      ! to (DBH 22/08/2006) # 7963
      Case Missive('Are you sure you want to cancel the Tagged Back Order Requests?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              If ~Records(glo:Queue)
                  Case Missive('You have not tagged any parts.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              End ! If ~Records(glo:Queue)
      
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  Access:RETACCOUNTSLIST.ClearKey(retacc:RecordNumberKey)
                  retacc:RecordNumber = glo:Pointer
                  If Access:RETACCOUNTSLIST.TryFetch(retacc:RecordNumberKey) = Level:Benign
                      !Found
                      If retacc:RefNumber = retpar:RecordNumber
                          Access:RETSTOCK.ClearKey(res:Record_Number_Key)
                          res:Record_Number = retacc:OrigRecordNumber
                          If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                              !Found
                              res:Despatched = 'CAN'
                              If Access:RETSTOCK.Update() = Level:Benign
                                  Access:RETPARTSLIST.ClearKey(retpar:RecordNumberKey)
                                  retpar:RecordNumber = brw4.q.retpar:RecordNumber
                                  If Access:RETPARTSLIST.TryFetch(retpar:RecordNumberKey) = Level:Benign
                                      !Found
                                      retpar:QuantityBackOrder -= retacc:QuantityOrdered
                                      Access:RETPARTSLIST.Update()
                                  Else ! If Access:RETPARTSLIST.TryFetch(retpar:RecordNumberKey) = Level:Benign
                                      !Error
                                  End ! If Access:RETPARTSLIST.TryFetch(retpar:RecordNumberKey) = Level:Benign
      
                                  Delete(RETACCOUNTSLIST)
      
                                  If retpar:QuantityBackOrder <= 0
                                      Delete(RETPARTSLIST)
                                  End ! If retpar:QuantityBackOrder <= 0
                                  Brw5.ResetSort(1)
                                  Brw4.ResetSort(1)
                              End ! If Access:RETSTOCK.Update() = Level:Benign
      
                          Else ! If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                              !Error
                          End ! If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
      
                      End ! If retacc:RefNumber = retpar:RecordNumber
                  Else ! If Access:RETACCOUNTSLIST.TryFetch(retacc:RecordNumberKey) = Level:Benign
                      !Error
                  End ! If Access:RETACCOUNTSLIST.TryFetch(retacc:RecordNumberKey) = Level:Benign
              End ! Loop x# = 1 To Records(glo:Queue)
          Of 1 ! No Button
      End ! Case Missive
      ! End (DBH 22/08/2006) #7963
      BRW4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelPart, Accepted)
    OF ?GeneratePickNotes
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GeneratePickNotes, Accepted)
      Case Missive('Do you wish to GENERATE Sales and Pick Notes for the processed parts?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              Clear(AccountQueue)
              Free(AccountQueue)
              Save_retacc_ID = Access:RETACCOUNTSLIST.SaveFile()
              Set(retacc:RecordNumberKey)
              Loop
                  If Access:RETACCOUNTSLIST.NEXT()
                     Break
                  End !If
      
                  If retacc:QuantityToShip = 0
                      Cycle
                  End !If retacc:QuantityToShip <> 0
      
                  Access:RETPARTSLIST.ClearKey(retpar:RecordNumberKey)
                  retpar:RecordNumber = retacc:RefNumber
                  If Access:RETPARTSLIST.TryFetch(retpar:RecordNumberKey) = Level:Benign
                      !Found
                      If retpar:Processed <> 1
                          Cycle
                      End !If retpar:Processed <> 1
                      !Is there still enough in Stock
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = retpar:PartRefNumber
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          !Found
                          If sto:Quantity_Stock < retacc:QuantityToShip
                              Cycle
                          End !If sto:Quantity_Stock < retacc:QuantityToShip
                      Else! If Access:.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                  Else!If Access:RETPARTSLIST.TryFetch(retpar:RecordNumberKey) = Level:Benign
                      !Error
                      Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:RETPARTSLIST.TryFetch(retpar:RecordNumberKey) = Level:Benign
      
                  !Get the original sale from the part to see
                  !if it's an Exchange Accesory Sale
                  Access:RETSALES.Clearkey(ret:Ref_Number_Key)
                  ret:Ref_Number  = retacc:SaleNumber
                  If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
                      !Found
                  Else! If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
      
                  Sort(AccountQueue,acctmp:AccountNumber)
                  acctmp:AccountNumber        = retacc:AccountNumber
                  acctmp:PaymentMethod        = ret:Payment_Method
                  acctmp:PostcodeDelivery     = ret:Postcode_Delivery
                  acctmp:CompanyNameDelivery  = ret:Company_Name_Delivery
                  acctmp:BuildingNameDelivery = ret:Building_Name_Delivery
                  acctmp:AddressLine1Delivery = ret:Address_Line1_Delivery
                  acctmp:AddressLine2Delivery = ret:Address_Line2_Delivery
                  acctmp:AddressLine3Delivery = ret:Address_Line3_Delivery
                  acctmp:TelephoneDelivery    = ret:Telephone_Delivery
                  acctmp:FaxNumberDelivery    = ret:Fax_Number_Delivery
                  tmp:PaymentMethod           = ret:Payment_Method
      
                  Get(AccountQueue,'acctmp:AccountNumber,acctmp:PaymentMethod,acctmp:PostcodeDelivery,acctmp:CompanyNameDelivery,acctmp:BuildingNameDelivery,acctmp:AddressLine1Delivery,acctmp:AddressLine2Delivery,acctmp:AddressLine3Delivery,acctmp:TelephoneDelivery,acctmp:FaxNumberDelivery')
                  If Error()
                      !Create New Sale For This Account
                      If Access:RETSALES.PrimeRecord() = Level:Benign
                          Access:USERS.ClearKey(use:password_key)
                          use:Password = glo:Password
                          If Access:USERS.TryFetch(use:password_key) = Level:Benign
                              !Found
                              ret:Who_Booked  = use:User_Code
                          End!If Access:USERS.TryFetch(use:password_key) = Level:Benign
      
                          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                          sub:Account_Number  = retacc:AccountNumber
                          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                              !Found
                              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                              tra:Account_Number  = sub:Main_Account_Number
                              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                  !Found
                                  !Get the Retail Vat Rate
                                  If tra:Invoice_Sub_Accounts = 'YES'
                                      Access:VATCODE.Clearkey(vat:Vat_Code_Key)
                                      vat:Vat_Code    = sub:Retail_Vat_Code
                                      If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
                                          !Found
                                          tmp:VatRate = vat:Vat_Rate
                                      Else! If Access:.Tryfetch(vat:Vat_Code_Key) = Level:Benign
                                          !Error
                                          Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
      
                                  Else !If tra:Invoice_Sub_Accounts = 'YES'
                                      Access:VATCODE.Clearkey(vat:Vat_Code_Key)
                                      vat:Vat_Code    = tra:Retail_Vat_Code
                                      If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
                                          !Found
                                          tmp:VatRate = vat:Vat_Rate
                                      Else! If Access:.Tryfetch(vat:Vat_Code_Key) = Level:Benign
                                          !Error
                                          Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
      
                                  End !If tra:Invoice_Sub_Accounts = 'YES'
      
                                  !Work out which Invoice and Delivery addresses to use
                                  !Also find out the Retail Payment Method
                                  If tra:Use_Sub_Accounts <> 'YES'
                                      ret:Courier_Cost    = tra:Courier_Cost
                                      ret:Courier         = tra:Courier_Outgoing
                                      If tra:Use_Delivery_Address = 'YES'
                                          tmp:UseDeliveryAddress = 1
                                      End !If tra:Use_Delivery_Address = 'YES'
                                      tmp:UseInvoice = 1
                                  Else !If tra:Use_Sub_Accounts = 'YES'
                                      ret:Courier_Cost    = sub:Courier_Cost
                                      ret:Courier         = sub:Courier_Outgoing
      
                                      If tra:Invoice_Sub_Accounts = 'YES'
                                          tmp:UseInvoice = 2
                                          If sub:Use_Delivery_Address = 'YES'
                                              tmp:UseDeliveryAddress = 2
                                          End !If sub:Use_Delivery_Address = 'YES'
                                      Else !If tra:Invoice_Sub_Accounts = 'YES'
                                          tmp:UseInvoice = 1
      
                                      End !If tra:Invoice_Sub_Accounts = 'YES'
                                  End !If tra:Use_Sub_Accounts = 'YES'
      
                                  !Make the new sale the same as the original sale
                                  ret:Payment_Method = tmp:PaymentMethod
      
                                  Case tmp:UseInvoice
                                      Of 1 !Use Trade Address
                                          ret:Postcode            = tra:Postcode
                                          ret:Company_Name        = tra:Company_Name
                                          ret:Building_Name       = ''
                                          ret:Address_Line1       = tra:Address_Line1
                                          ret:Address_Line2       = tra:Address_Line2
                                          ret:Address_Line3       = tra:Address_Line3
                                          ret:Telephone_Number    = tra:Telephone_Number
                                          ret:Fax_Number          = tra:Fax_Number
                                      Of 2 !Use Sub Account Address
                                          ret:Postcode            = sub:Postcode
                                          ret:Company_Name        = sub:Company_Name
                                          ret:Building_Name       = ''
                                          ret:Address_Line1       = sub:Address_Line1
                                          ret:Address_Line2       = sub:Address_Line2
                                          ret:Address_Line3       = sub:Address_Line3
                                          ret:Telephone_Number    = sub:Telephone_Number
                                          ret:Fax_Number          = sub:Fax_Number
                                  End !Case tmp:UseInvoice
      !                            Case tmp:UseDeliveryAddress
      !                                Of 1 !Use Trade Address for Delivery
      !                                    ret:Postcode_Delivery       = tra:Postcode
      !                                    ret:Company_Name_Delivery   = tra:Company_Name
      !                                    ret:Building_Name_Delivery  = ''
      !                                    ret:Address_Line1_Delivery  = tra:Address_Line1
      !                                    ret:Address_Line2_Delivery  = tra:Address_Line2
      !                                    ret:Address_Line3_Delivery  = tra:Address_Line3
      !                                    ret:Telephone_Delivery      = tra:Telephone_Number
      !                                    ret:Fax_Number_Delivery     = tra:Fax_Number
      !                                Of 2 !Use Sub Account Address For Delivery
      !                                    ret:Postcode_Delivery       = sub:Postcode
      !                                    ret:Company_Name_Delivery   = sub:Company_Name
      !                                    ret:Building_Name_Delivery  = ''
      !                                    ret:Address_Line1_Delivery  = sub:Address_Line1
      !                                    ret:Address_Line2_Delivery  = sub:Address_Line2
      !                                    ret:Address_Line3_Delivery  = sub:Address_Line3
      !                                    ret:Telephone_Delivery      = sub:Telephone_Number
      !                                    ret:Fax_Number_Delivery     = sub:Fax_Number
      !                            End !Case tmp:UseDelivery
                                          ret:Postcode_Delivery       = acctmp:PostcodeDelivery
                                          ret:Company_Name_Delivery   = acctmp:CompanyNameDelivery
                                          ret:Building_Name_Delivery  = acctmp:BuildingNameDelivery
                                          ret:Address_Line1_Delivery  = acctmp:AddressLine1Delivery
                                          ret:Address_Line2_Delivery  = acctmp:AddressLine2Delivery
                                          ret:Address_Line3_Delivery  = acctmp:AddressLine3Delivery
                                          ret:Telephone_Delivery      = acctmp:TelephoneDelivery
                                          ret:Fax_Number_Delivery     = acctmp:FaxNumberDelivery
      
                                  ret:Account_Number  = retacc:AccountNumber
                                  ret:Purchase_Order_Number   = 'VARIOUS'
                              Else! If Access:.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                  !Error
                                  Assert(0,'<13,10>Fetch Error<13,10>')
                              End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
                          Else! If Access:.Tryfetch(sub:Account_Number_Key) = Level:Benign
                              !Error
                              Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
                          If Access:RETSALES.TryInsert() = Level:Benign
                              !Insert Successful
                              !Add Account to the local queue, so that you can add to this sale later
                              acctmp:AccountNumber        = retacc:AccountNumber
                              acctmp:SaleNumber           = ret:Ref_Number
                              acctmp:PaymentMethod        = tmp:PaymentMethod
                              acctmp:PostcodeDelivery     = ret:Postcode_Delivery
                              acctmp:CompanyNameDelivery  = ret:Company_Name_Delivery
                              acctmp:BuildingNameDelivery = ret:Building_Name_Delivery
                              acctmp:AddressLine1Delivery = ret:Address_Line1_Delivery
                              acctmp:AddressLine2Delivery = ret:Address_Line2_Delivery
                              acctmp:AddressLine3Delivery = ret:Address_Line3_Delivery
                              acctmp:TelephoneDelivery    = ret:Telephone_Delivery
                              acctmp:FaxNumberDelivery    = ret:Fax_Number_Delivery
                              Add(AccountQueue)
      
                              Do AddRetailPart
      
                          Else !If Access:RETSALES.TryInsert() = Level:Benign
                              !Insert Failed
                          End !If Access:RETSALES.TryInsert() = Level:Benign
                      End !If Access:RETSALES.PrimeRecord() = Level:Benign
      
                  Else !If Error()
                      !Sale Already Created
                      Access:RETSALES.ClearKey(ret:Ref_Number_Key)
                      ret:Ref_Number = acctmp:SaleNumber
                      If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                          !Found
      
                          Do AddRetailPart
                      Else!If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                          !Error
                          Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                  End !If Error()
      
              End !Loop
              Access:RETACCOUNTSLIST.RestoreFile(Save_retacc_ID)
              !Set(DEFAULTS)
              !Access:DEFAULTS.Next()
              Clear(AccountQueue)
              Sort(AccountQueue,acctmp:SaleNumber)
              Loop x# = 1 to Records(AccountQueue)
                  Get(AccountQueue,x#)
                  !MESSAGE(x#)
                  glo:select1  = AccountQueue.acctmp:SaleNumber
                  !Case def:RetBackOrders
                  !Of 1
                      ! Inserting (DBH 07/12/2006) # 8553 - Force two copies
                      glo:ReportCopies = 2
                      ! End (DBH 07/12/2006) #8553
                      Retail_Picking_Note('PIK')
      
              END
              tmp:CloseWindow = True
          Of 1 ! No Button
      End ! Case Missive
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GeneratePickNotes, Accepted)
    OF ?List
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      BRW5.ApplyRange
      BRW5.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?ProcessItems
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessItems, Accepted)
      Case Missive('Are you sure you want to process this part?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Brw4.UpdateViewRecord()
      
              If retpar:Processed = 1
                  Case Missive('This part has already been processed.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else !If retpar:Process = 0
                  If tmp:CurrentStockAfter < 0
                      Case Missive('There are insufficient items in stock to process this item.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else !If tmp:CurrentStockAfter < 0
                      retpar:Processed = 1
                      Access:RETPARTSLIST.Update()
                      Brw4.ResetSort(1)
                      Brw4.TakeScroll(Event:ScrollTop)
                  End !If tmp:CurrentStockAfter < 0
              End !If retpar:Process = 0
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessItems, Accepted)
    OF ?AutoAllocate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AutoAllocate, Accepted)
      brw4.UpdateViewRecord()
      
      If retpar:Processed = 1
          Case Missive('Cannot Auto Allocate. This part has already been processed.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !retpar:Processed = 1
      
          tmp:QuantityAvailable   = retpar:QuantityStock
      
          Setcursor(Cursor:Wait)
          Save_retacc_ID = Access:RETACCOUNTSLIST.SaveFile()
          Access:RETACCOUNTSLIST.ClearKey(retacc:AccountNumberKey)
          retacc:RefNumber     = retpar:RecordNumber
          Set(retacc:AccountNumberKey,retacc:AccountNumberKey)
          Loop
              If Access:RETACCOUNTSLIST.NEXT()
                 Break
              End !If
              If retacc:RefNumber     <> retpar:RecordNumber      |
                  Then Break.  ! End If
      
              If retacc:QuantityToShip <> 0
                  Cycle
              End !If retacc:QuantityToShip <> 0
      
              If retacc:QuantityOrdered <> 0
                  If tmp:CurrentStockAfter => retacc:QuantityOrdered
                      retacc:QuantityToShip   = retacc:QuantityOrdered
                      tmp:CurrentStockAfter   -= retacc:QuantityToShip
                      Access:RETACCOUNTSLIST.TryUpdate()
                  Else !If tmp:CurrentStockAfter => retacc:QuantityOrdered
                      If tmp:CurrentStockAfter > 0
                          retacc:QuantityToShip = tmp:CurrentStockAfter
                          tmp:CurrentStockAfter = 0
                          Access:RETACCOUNTSLIST.TryUpdate()
                      End !If tmp:CurrentStockAfter > 0
                  End !If tmp:CurrentStockAfter => retacc:QuantityOrdered
      
              End !If retacc:QuantityToShip <> 0
          End !Loop
          Access:RETACCOUNTSLIST.RestoreFile(Save_retacc_ID)
          Setcursor()
      End !retpar:Processed = 1
      BRW5.ResetSort(1)
      BRW4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AutoAllocate, Accepted)
    OF ?Recalculate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Recalculate, Accepted)
    !Are there records?
    error# = 0
    Set(RETPARTSLIST)
    If Access:RETPARTSLIST.Next() = Level:Benign
        Case Missive('Warning! This will recalculate all Back Order information.'&|
          '<13,10>'&|
          '<13,10>Any changes you have made will be lost. Are you sure?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
            Of 1 ! No Button
                error# = 1
        End ! Case Missive

    End !If Access:RETPARTSLIST.Next() = Level:Benign

    If error# = 0

        !Loop through all Back Order parts

        !Using Tradetmp file for the list of locations.
        !Delete list so I can build a new one
        Save_tratmp_ID = Access:TRADETMP.SaveFile()
        Set(TRADETMP,0)
        Loop
            If Access:TRADETMP.NEXT()
               Break
            End !If
            relate:TRADETMP.Delete(0)
        End !Loop
        Access:TRADETMP.RestoreFile(Save_tratmp_ID)

        Save_retpar_ID = Access:RETPARTSLIST.SaveFile()
        Set(RETPARTSLIST,0)
        Loop
            If Access:RETPARTSLIST.Next()
               Break
            End !If
            relate:RETPARTSLIST.Delete(0)
        End !Loop
        Access:RETPARTSLIST.RestoreFile(Save_retpar_ID)


        !Search through all parts with 'PEN', i.e. a request was made
        !I used to use 'ORD', but as I'm now using Summary Ordering For
        !Retail Stock this is no longer needed
        Setcursor(Cursor:Wait)
        !Record Count
        RecordCount# = 0
        Save_res_ID = Access:RETSTOCK.SaveFile()
        Access:RETSTOCK.ClearKey(res:DespatchedKey)
        res:Despatched           = 'PEN'
        Set(res:DespatchedKey,res:DespatchedKey)
        Loop
            If Access:RETSTOCK.NEXT()
               Break
            End !If
            If res:Despatched           <> 'PEN'      |
                Then Break.  ! End If
            RecordCount# += 1
        End !Loop
        Access:RETSTOCK.RestoreFile(Save_res_ID)
        Setcursor()

        Prog.ProgressSetup(RecordCount#)

        Save_res_ID = Access:RETSTOCK.SaveFile()
        Access:RETSTOCK.ClearKey(res:DespatchedKey)
        res:Despatched = 'PEN'
        Set(res:DespatchedKey,res:DespatchedKey)
        Loop
            If Access:RETSTOCK.NEXT()
               Break
            End !If
            If res:Despatched <> 'PEN'      |
                Then Break.  ! End If
            IF Prog.InsideLoop()
                Break
            End ! IF Prog.InsideLoop()

            !If back order part found, then add to the list

            Access:RETSALES.Clearkey(ret:Ref_Number_Key)
            ret:Ref_Number  = res:Ref_Number
            If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
                !Found
                !Work Out Parts On Order. Just look for all parts numbers that match.
                tmp:QuantityOnOrder = 0
                Save_orp_ID = Access:ORDPARTS.SaveFile()
                Access:ORDPARTS.ClearKey(orp:Received_Part_Number_Key)
                orp:All_Received = 'NO'
                orp:Part_Number  = res:Part_Number
                Set(orp:Received_Part_Number_Key,orp:Received_Part_Number_Key)
                Loop
                    If Access:ORDPARTS.NEXT()
                       Break
                    End !If
                    If orp:All_Received <> 'NO'      |
                    Or orp:Part_Number  <> res:Part_Number      |
                        Then Break.  ! End If
                    !Only count parts that have come from the same stock location
                    If orp:Part_Ref_Number <> res:Part_Ref_Number
                        Cycle
                    End !If orp:Part_Ref_Number <> res:Part_Ref_Number

                    If orp:order_number <> ''
                        access:orders.clearkey(ord:order_number_key)
                        ord:order_number = orp:order_number
                        if access:orders.fetch(ord:order_number_key) = Level:Benign
                            If ord:all_received = 'NO'
                                If orp:all_received = 'NO'
                                    tmp:QuantityOnOrder += orp:quantity
                                End!If orp:all_received <> 'YES'
                            End!If ord:all_received <> 'YES'
                        end!if access:orders.fetch(ord:order_number_key) = Level:Benign
                    End!If orp:order_number <> ''

                End !Loop
                Access:ORDPARTS.RestoreFile(Save_orp_ID)

                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = res:Part_Ref_Number
                If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    !Build the Parts List
                    Access:RETPARTSLIST.ClearKey(retpar:LocPartNumberKey)
                    retpar:Location   = sto:Location
                    retpar:Processed  = 0
                    retpar:PartNumber = res:Part_Number
                    If Access:RETPARTSLIST.TryFetch(retpar:LocPartNumberKey) = Level:Benign
                        !Found
                        Retpar:QuantityBackOrder += res:Quantity
                        Access:RETPARTSLIST.Update()
                    Else!If Access:RETPARTSLIST.TryFetch(retpar:LocPartNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        If Access:RETPARTSLIST.Primerecord() = Level:Benign
                            retpar:PartRefNumber    = sto:Ref_Number
                            retpar:PartNumber       = res:Part_Number
                            retpar:Description      = res:Description
                            retpar:QuantityStock    = sto:Quantity_Stock
                            retpar:Location         = sto:Location
                            retpar:QuantityOnOrder  = tmp:QuantityOnOrder
                            retpar:QuantityBackOrder = res:Quantity
                            If Access:RETPARTSLIST.Tryinsert()
                                Access:RETPARTSLIST.Cancelautoinc()
                            End!If Access:RETPARTSLIST.Tryinsert()l:Benign
                        End!If Access:RETPARTSLIST.Primerecord() = Level:Benign

                        !Build the Location list
                        Access:TRADETMP.ClearKey(tratmp:CompanyNameKey)
                        tratmp:Company_Name = sto:Location
                        If Access:TRADETMP.TryFetch(tratmp:CompanyNameKey) = Level:Benign
                            !Found

                        Else!If Access:TRADETMP.TryFetch(tratmp:CompanyNameKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            If Access:TRADETMP.Primerecord() = Level:Benign
                                tratmp:Company_Name  = sto:Location
                                If Access:TRADETMP.Tryinsert()
                                    Access:TRADETMP.Cancelautoinc()
                                End!If Access:TRADETMP.Tryinsert()nign
                            End!If Access:TRADETMP.Primerecord() = Level:Benign
                        End!If Access:TRADETMP.TryFetch(tratmp:CompanyNameKey) = Level:Benign
                     End!If Access:RETPARTSLIST.TryFetch(retpar:LocPartNumberKey) = Level:Benign

    !                Access:RETACCOUNTSLIST.ClearKey(retacc:DateOrderedKey)
    !                retacc:RefNumber     = retpar:RecordNumber
    !                retacc:DateOrdered   = res:Date_Ordered
    !                retacc:AccountNumber = ret:Account_Number
    !                If Access:RETACCOUNTSLIST.TryFetch(retacc:DateOrderedKey)
                        If Access:RETACCOUNTSLIST.Primerecord() = Level:Benign
                            retacc:AccountNumber   = ret:Account_Number
                            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                            sub:Account_Number  = ret:Account_Number
                            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                !Found
                                retacc:CompanyName     = sub:Company_Name
                            Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                            retacc:DateOrdered     = ret:date_booked
                            retacc:QuantityOrdered = res:Quantity
                            retacc:QuantityToShip  = 0
                            retacc:RefNumber       = retpar:RecordNumber
                            retacc:SaleNumber       = ret:Ref_Number
                            retacc:OrigRecordNumber = res:Record_Number
                            If Access:RETACCOUNTSLIST.Tryinsert()
                                Access:RETACCOUNTSLIST.Cancelautoinc()
                            End!If Access:RETACCOUNTSLIST.Tryinsert()evel:Benign
                        End!If Access:RETACCOUNTSLIST.Primerecord() = Level:Benign
    !                Else !If Access:RETACCOUNTSLIST.TryFetch(retacc:DateOrderedKey) = Level:Benign
    !                    retacc:QuantityOrdered += res:Quantity
    !                    Access:RETACCOUNTSLIST.Update()
    !                End !If Access:RETACCOUNTSLIST.TryFetch(retacc:DateOrderedKey) = Level:Benign
                Else!If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

            End! If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Beni = Level:Benign
        End !Loop
        Access:RETSTOCK.RestoreFile(Save_res_ID)
        !Remove any entries below the "Is Below" figure

        Save_RETPAR_ID = Access:RETPARTSLIST.SaveFile()
        Set(RETPARTSLIST,0)
        Loop
            If Access:RETPARTSLIST.NEXT()
               Break
            End !If
            If retpar:QuantityStock < tmp:StockBelow
                relate:RETPARTSLIST.Delete(0)
            End !If retpar:Quantity_STock < tmp:StockBelow
        End !Loop
        Access:RETPARTSLIST.RestoreFile(Save_RETPAR_ID)

        Setcursor()

        Prog.ProgressFinish()
    End !If error# = 0
      BRW4.ResetSort(1)
      BRW5.ResetSort(1)
      FDCB17.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Recalculate, Accepted)
    OF ?tmp:Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW4.ApplyRange
      BRW4.ResetSort(1)
      Select(?List)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore4.IgnoreEvent = True                       !Xplore
     Xplore4.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore5.IgnoreEvent = True                       !Xplore
     Xplore5.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  OF ?tmp:Location
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      BRW5.ApplyRange
      BRW5.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::25:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?tmp:Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW4.ApplyRange
      BRW4.ResetSort(1)
      Select(?List)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If ~tmp:CloseWindow
          Cycle
      End !tmp:CloseWindow
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F6Key
              Post(Event:Accepted,?Recalculate)
          Of F7Key
              Post(Event:Accepted,?ProcessItems)
          Of F8Key
              Post(Event:Accepted,?AutoAllocate)
          Of F10Key
              If Choice(?Sheet1) = 2
                  Post(Event:Accepted,?GeneratePickNotes)
              End !If Choice(?Tab2) = 2
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Select(?tmp:StockBelow)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 4)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      POST(xpEVENT:Xplore + 5)                        !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore4.GetColumnInfo()                         !Xplore
      Xplore5.GetColumnInfo()                         !Xplore
    OF EVENT:Timer
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
      tmp:CurrentStockAfter = brw4.q.retpar:QuantityStock - tmp:TotalToShip
      If tmp:CurrentStockAfter < 0
          ?tmp:CurrentStockAfter{prop:fontcolor} = color:Red
      Else !tmp:CurrentStockAfter < 0
          ?tmp:CurrentStockAfter{prop:fontcolor} = color:none
      End !tmp:CurrentStockAfter < 0
      Display(?tmp:CurrentStockAfter)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore4.GetColumnInfo()                         !Xplore
      Xplore5.GetColumnInfo()                         !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 4                           !Xplore
        Xplore4.GetColumnInfo()                       !Xplore
      OF xpEVENT:Xplore + 5                           !Xplore
        Xplore5.GetColumnInfo()                       !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?Sheet1) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW4.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,4,'GetFreeElementName'
  IF BRW4.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW4.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW4.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,4,'GetFreeElementPosition'
  IF BRW4.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW4.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,4
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore4.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore4.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore4.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW4.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore4.sq)                    !Xplore
    Xplore4.SetupOrder(BRW4.SortOrderNbr)             !Xplore
    GET(Xplore4.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW4.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW4.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW4.FileOrderNbr,Force)      !Xplore
  END                                                 !Xplore
  IF Choice(?Sheet1) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW4.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.retpar:PartNumber_Tip = retpar:PartNumber
  SELF.Q.retpar:Description_Tip = retpar:Description
  SELF.Q.tmp:ShelfLocation_Tip = tmp:ShelfLocation


BRW4.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,4,'TakeEvent'
  Xplore4.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?List                                  !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore4.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore4.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore4.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore4.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore4.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW4.FileSeqOn = False                         !Xplore
       Xplore4.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW4.SavePosition()                           !Xplore
       Xplore4.HandleMyEvents()                       !Xplore
       !BRW4.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List                             !Xplore
  PARENT.TakeEvent


BRW4.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore4.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(4, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  ! Inserting (DBH 01/08/2006) # 7694 - Display the Main Store Shelf Location
  Access:STOCK.ClearKey(sto:Location_Key)
  sto:Location    = 'MAIN STORE'
  sto:Part_Number = retpar:PartNumber
  If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      tmp:ShelfLocation = sto:Shelf_Location
  Else ! If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      tmp:ShelfLocation = ''
  End ! If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
  ! End (DBH 01/08/2006) #7694
  
  BRW4::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(4, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW5.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = retpar:RecordNumber
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW5.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,5,'GetFreeElementName'
  IF BRW5.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW5.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW5.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,5,'GetFreeElementPosition'
  IF BRW5.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.EIP &= BRW5::EIPManager
  SELF.AddEditControl(EditInPlace::RETACC:QuantityToShip,7)
  SELF.AddEditControl(,5)
  SELF.AddEditControl(,6)
  SELF.AddEditControl(,2)
  SELF.AddEditControl(,3)
  SELF.AddEditControl(,4)
  SELF.AddEditControl(,1)
  SELF.ArrowAction = EIPAction:Default+EIPAction:Remain+EIPAction:RetainColumn
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW5.ResetFromView PROCEDURE

tmp:TotalOrdered:Sum REAL
tmp:TotalToShip:Sum  REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:RETACCOUNTSLIST.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    tmp:TotalOrdered:Sum += retacc:QuantityOrdered
    tmp:TotalToShip:Sum += retacc:QuantityToShip
  END
  tmp:TotalOrdered = tmp:TotalOrdered:Sum
  tmp:TotalToShip = tmp:TotalToShip:Sum
  PARENT.ResetFromView
  Relate:RETACCOUNTSLIST.SetQuickScan(0)
  SETCURSOR()


BRW5.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,5
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore5.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore5.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore5.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW5.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore5.sq)                    !Xplore
    Xplore5.SetupOrder(BRW5.SortOrderNbr)             !Xplore
    GET(Xplore5.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW5.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW5.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW5.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = retacc:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW5.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,5,'TakeEvent'
  Xplore5.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?List:2                                !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore5.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore5.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore5.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore5.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore5.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW5.FileSeqOn = False                         !Xplore
       Xplore5.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW5.SavePosition()                           !Xplore
       Xplore5.HandleMyEvents()                       !Xplore
       !BRW5.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List:2                           !Xplore
  PARENT.TakeEvent


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore5.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = retacc:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::25:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW4.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW4.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW4.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW4.ResetPairsQ PROCEDURE()
  CODE
Xplore4.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore4.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore4.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW4.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !retpar:PartNumber
  OF 2 !retpar:Description
  OF 3 !tmp:ShelfLocation
  OF 4 !retpar:QuantityBackOrder
  OF 5 !retpar:QuantityOnOrder
  OF 6 !retpar:QuantityStock
  OF 7 !retpar:RecordNumber
  OF 8 !retpar:Location
  OF 9 !retpar:Processed
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore4.SetNewOrderFields PROCEDURE()
  CODE
  BRW4.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore4.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore4Step9,retpar:LocPartNumberKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,retpar:LocPartNumberKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore4Step1.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore4Locator1)
      Xplore4Locator1.Init(?retpar:PartNumber,retpar:PartNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore4Locator2)
      Xplore4Locator2.Init(,retpar:Description,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step3.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore4Locator3)
      Xplore4Locator3.Init(,tmp:ShelfLocation,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step4.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore4Locator4)
      Xplore4Locator4.Init(,retpar:QuantityBackOrder,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step5.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore4Locator5)
      Xplore4Locator5.Init(,retpar:QuantityOnOrder,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step6.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore4Locator6)
      Xplore4Locator6.Init(?retpar:QuantityStock,retpar:QuantityStock,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step7.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore4Locator7)
      Xplore4Locator7.Init(,retpar:RecordNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step8.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore4Locator8)
      Xplore4Locator8.Init(,retpar:Location,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step9.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore4Locator9)
      Xplore4Locator9.Init(,retpar:Processed,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(retpar:Processed)
  RETURN
!================================================================================
Xplore4.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore4.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW4.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(RETPARTSLIST)
  END
  RETURN TotalRecords
!================================================================================
Xplore4.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('retpar:PartNumber')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Part Number')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Part Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:Description')         !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Description')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Description')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('tmp:ShelfLocation')          !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ShelfLocation')          !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ShelfLocation')          !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:QuantityBackOrder')   !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Bck Order')                  !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Quantity On Back Order')     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:QuantityOnOrder')     !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('On Order')                   !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Quantity On Order')          !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:QuantityStock')       !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('In Stock')                   !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Quantity In Stock')          !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:RecordNumber')        !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Record Number')              !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:Location')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Location')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Location')                   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:Processed')           !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Processed')                  !Header
                PSTRING('@n1')                        !Picture
                PSTRING('Processed')                  !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(9)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)
BRW5.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW5.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW5.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW5.ResetPairsQ PROCEDURE()
  CODE
Xplore5.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore5.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore5.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW5.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !tmp:Tag
  OF 3 !retacc:CompanyName
  OF 4 !retacc:AccountNumber
  OF 5 !retacc:SaleNumber
  OF 6 !retacc:DateOrdered
  OF 7 !retacc:QuantityOrdered
  OF 8 !retacc:QuantityToShip
  OF 9 !retacc:RecordNumber
  OF 10 !retacc:RefNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore5.SetNewOrderFields PROCEDURE()
  CODE
  BRW5.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore5.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore5Step9,retacc:AccountNumberKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,retacc:AccountNumberKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore5Step1.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore5Locator1)
      Xplore5Locator1.Init(,tmp:Tag,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore5Locator2)
      Xplore5Locator2.Init(,retacc:CompanyName,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore5Locator3)
      Xplore5Locator3.Init(,retacc:AccountNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step4.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore5Locator4)
      Xplore5Locator4.Init(,retacc:SaleNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step5.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore5Locator5)
      Xplore5Locator5.Init(,retacc:DateOrdered,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step6.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore5Locator6)
      Xplore5Locator6.Init(,retacc:QuantityOrdered,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step7.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore5Locator7)
      Xplore5Locator7.Init(,retacc:QuantityToShip,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step8.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore5Locator8)
      Xplore5Locator8.Init(,retacc:RecordNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step9.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore5Locator9)
      Xplore5Locator9.Init(,retacc:RefNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(retacc:RefNumber)
  RETURN
!================================================================================
Xplore5.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore5.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW5.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(RETACCOUNTSLIST)
  END
  RETURN TotalRecords
!================================================================================
Xplore5.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('tmp:Tag')                    !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:Tag')                    !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:Tag')                    !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:CompanyName')         !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Company Name')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Company Name')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:AccountNumber')       !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Account Number')             !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Account Number')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:SaleNumber')          !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Sale No')                    !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Sale Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:DateOrdered')         !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Requested')                  !Header
                PSTRING('@d6')                        !Picture
                PSTRING('Date Ordered')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:QuantityOrdered')     !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Req')                        !Header
                PSTRING('@s5')                        !Picture
                PSTRING('Quantity Ordered')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:QuantityToShip')      !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('To Ship')                    !Header
                PSTRING('@s5')                        !Picture
                PSTRING('Quantity To Ship')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:RecordNumber')        !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Record Number')              !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:RefNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Reference To Part Entry')    !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Reference To Part Entry')    !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(9)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
UpdateRETACCOUNTSLIST PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::retacc:Record LIKE(retacc:RECORD),STATIC
QuickWindow          WINDOW('Update Order'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Order'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Account No'),AT(248,184),USE(?RETACC:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(316,184,116,10),USE(retacc:AccountNumber),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Account Number'),TIP('Account Number'),UPR,READONLY
                           PROMPT('Quantity Ordered'),AT(248,204),USE(?RETACC:QuantityOrdered:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(316,204,64,10),USE(retacc:QuantityOrdered),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Quantity Ordered'),TIP('Quantity Ordered'),UPR,READONLY
                           PROMPT('Quantity To Ship'),AT(248,224),USE(?RETACC:QuantityToShip:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(316,224,64,10),USE(retacc:QuantityToShip),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Quantity To Ship'),TIP('Quantity To Ship'),UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020327'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateRETACCOUNTSLIST')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(retacc:Record,History::retacc:Record)
  SELF.AddHistoryField(?retacc:AccountNumber,3)
  SELF.AddHistoryField(?retacc:QuantityOrdered,6)
  SELF.AddHistoryField(?retacc:QuantityToShip,7)
  SELF.AddUpdateFile(Access:RETACCOUNTSLIST)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RETACCOUNTSLIST.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETACCOUNTSLIST
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETACCOUNTSLIST.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020327'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020327'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020327'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateRETPARTSLIST PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::retpar:Record LIKE(retpar:RECORD),STATIC
QuickWindow          WINDOW('Update the RETPARTSLIST File'),AT(,,248,166),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateRETPARTSLIST'),SYSTEM,GRAY,NOFRAME
                       SHEET,AT(4,4,240,140),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?RETPAR:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(116,20,40,10),USE(retpar:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Reference To Accounts List'),AT(8,34),USE(?RETPAR:RefNumber:Prompt),TRN
                           PROMPT('Reference To Stock'),AT(8,48),USE(?RETPAR:PartRefNumber:Prompt),TRN
                           ENTRY(@s8),AT(116,48,40,10),USE(retpar:PartRefNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Reference To Stock'),TIP('Reference To Stock'),UPR
                           PROMPT('Part Number'),AT(8,62),USE(?RETPAR:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(116,62,124,10),USE(retpar:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Part Number'),TIP('Part Number'),UPR
                           PROMPT('Description'),AT(8,76),USE(?RETPAR:Description:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(116,76,124,10),USE(retpar:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('desc'),TIP('desc'),UPR
                           PROMPT('Quantity In Stock'),AT(8,90),USE(?RETPAR:QuantityStock:Prompt),TRN
                           ENTRY(@s8),AT(116,90,40,10),USE(retpar:QuantityStock),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity In Stock'),TIP('Quantity In Stock'),UPR
                           PROMPT('Quantity On Back Order'),AT(8,104),USE(?RETPAR:QuantityBackOrder:Prompt),TRN
                           ENTRY(@s8),AT(116,104,40,10),USE(retpar:QuantityBackOrder),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity In Back Order'),TIP('Quantity In Back Order'),UPR
                           PROMPT('Quantity On Order'),AT(8,118),USE(?RETPAR:QuantityOnOrder:Prompt),TRN
                           ENTRY(@s8),AT(116,118,40,10),USE(retpar:QuantityOnOrder),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity On Order'),TIP('Quantity On Order'),UPR
                           CHECK('Processed'),AT(116,132,70,8),USE(retpar:Processed),MSG('Processed'),TIP('Processed'),VALUE('1','0')
                         END
                       END
                       BUTTON('OK'),AT(150,148,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(199,148,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(199,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a RETPARTSLIST Record'
  OF ChangeRecord
    ActionMessage = 'Changing a RETPARTSLIST Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateRETPARTSLIST')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RETPAR:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(retpar:Record,History::retpar:Record)
  SELF.AddHistoryField(?retpar:RecordNumber,1)
  SELF.AddHistoryField(?retpar:PartRefNumber,2)
  SELF.AddHistoryField(?retpar:PartNumber,3)
  SELF.AddHistoryField(?retpar:Description,4)
  SELF.AddHistoryField(?retpar:QuantityStock,6)
  SELF.AddHistoryField(?retpar:QuantityBackOrder,7)
  SELF.AddHistoryField(?retpar:QuantityOnOrder,8)
  SELF.AddHistoryField(?retpar:Processed,9)
  SELF.AddUpdateFile(Access:RETPARTSLIST)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RETPARTSLIST.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETPARTSLIST
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETPARTSLIST.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults







BrowseWebOrders PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:tag              STRING(1)
tmp:Zero             BYTE(0)
tmp:AccountNumber    STRING(30)
save_ori_id          USHORT,AUTO
save_res_id          USHORT,AUTO
vat_rate_temp        REAL
tmp:Tag2             STRING(1)
LocalDate            DATE
LocalTime            TIME
BRW1::View:Browse    VIEW(ORDHEAD)
                       PROJECT(orh:CustName)
                       PROJECT(orh:account_no)
                       PROJECT(orh:CustOrderNumber)
                       PROJECT(orh:thedate)
                       PROJECT(orh:thetime)
                       PROJECT(orh:Order_no)
                       PROJECT(orh:procesed)
                       PROJECT(orh:SalesNumber)
                       PROJECT(orh:pro_date)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
orh:CustName           LIKE(orh:CustName)             !List box control field - type derived from field
orh:account_no         LIKE(orh:account_no)           !List box control field - type derived from field
orh:CustOrderNumber    LIKE(orh:CustOrderNumber)      !List box control field - type derived from field
orh:thedate            LIKE(orh:thedate)              !List box control field - type derived from field
orh:thetime            LIKE(orh:thetime)              !List box control field - type derived from field
orh:Order_no           LIKE(orh:Order_no)             !Primary key field - type derived from field
orh:procesed           LIKE(orh:procesed)             !Browse key field - type derived from field
orh:SalesNumber        LIKE(orh:SalesNumber)          !Browse key field - type derived from field
orh:pro_date           LIKE(orh:pro_date)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(ORDITEMS)
                       PROJECT(ori:partno)
                       PROJECT(ori:partdiscription)
                       PROJECT(ori:qty)
                       PROJECT(ori:OrderDate)
                       PROJECT(ori:OrderTime)
                       PROJECT(ori:recordnumber)
                       PROJECT(ori:ordhno)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
ori:partno             LIKE(ori:partno)               !List box control field - type derived from field
ori:partdiscription    LIKE(ori:partdiscription)      !List box control field - type derived from field
ori:qty                LIKE(ori:qty)                  !List box control field - type derived from field
ori:OrderDate          LIKE(ori:OrderDate)            !List box control field - type derived from field
ori:OrderTime          LIKE(ori:OrderTime)            !List box control field - type derived from field
ori:recordnumber       LIKE(ori:recordnumber)         !Primary key field - type derived from field
ori:ordhno             LIKE(ori:ordhno)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(ORDWEBPR)
                       PROJECT(orw:AccountNumber)
                       PROJECT(orw:PartNumber)
                       PROJECT(orw:Description)
                       PROJECT(orw:Quantity)
                       PROJECT(orw:RecordNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
orw:AccountNumber      LIKE(orw:AccountNumber)        !List box control field - type derived from field
orw:PartNumber         LIKE(orw:PartNumber)           !List box control field - type derived from field
orw:Description        LIKE(orw:Description)          !List box control field - type derived from field
orw:Quantity           LIKE(orw:Quantity)             !List box control field - type derived from field
orw:RecordNumber       LIKE(orw:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK13::orh:procesed        LIKE(orh:procesed)
! ---------------------------------------- Higher Keys --------------------------------------- !
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
XploreMask14         DECIMAL(10,0,0)                  !Xplore
XploreMask114         DECIMAL(10,0,48)                !Xplore
XploreTitle14        STRING(' ')                      !Xplore
xpInitialTab14       SHORT                            !Xplore
QuickWindow          WINDOW('Browse Web Orders'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(4,7,644,13),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,9),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Web Order File'),AT(8,9),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(412,348),USE(?RefreshButton),TRN,FLAT,LEFT,ICON('refviewp.jpg')
                       SHEET,AT(364,24,312,358),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Items Requested'),USE(?Tab2)
                         END
                       END
                       SHEET,AT(4,24,356,358),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Unprocessed Sales'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Items Requested'),AT(368,30),USE(?Prompt1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(372,48,296,288),USE(?List),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Part Number~@s20@85L(2)|M~Description~@s60@32R(2)|M~Qty~L@n10@[52L(2)|M' &|
   '~Date~@d17@20L(2)|M~Time~@t7@]|M~Updated~'),FROM(Queue:Browse)
                           LIST,AT(8,48,344,288),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Customer Name~@s30@76L(2)|M~Account No~@s40@97L(2)|M~Customer Order Num' &|
   'ber~@s30@[44L(2)|M~Date~@d6b@20L(2)|M~Time~@t1@]|M~Order Placed~'),FROM(Queue:Browse:1)
                           BUTTON,AT(292,348),USE(?CreateSale),TRN,FLAT,LEFT,ICON('crsalep.jpg')
                         END
                         TAB('Unauthorised Orders'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(16,74,124,10),USE(orw:AccountNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('AccountNumber'),TIP('AccountNumber'),UPR
                           LIST,AT(16,86,332,286),USE(?List:2),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('100L(2)|M~AccountNumber~@s30@92L(2)|M~Part Number~@s30@100L(2)|M~Description~@s3' &|
   '0@32L(2)|M~Qty~@s8@'),FROM(Queue:Browse:2)
                         END
                       END
                       BUTTON,AT(613,385),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW14                CLASS(BrowseClass)               !Browse using ?List:2
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:2                !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW14::Sort0:Locator EntryLocatorClass                !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepStringClass !STRING          !Xplore: Column displaying orh:CustName
Xplore1Locator1      StepLocatorClass                 !Xplore: Column displaying orh:CustName
Xplore1Step2         StepStringClass !STRING          !Xplore: Column displaying orh:account_no
Xplore1Locator2      StepLocatorClass                 !Xplore: Column displaying orh:account_no
Xplore1Step3         StepStringClass !STRING          !Xplore: Column displaying orh:CustOrderNumber
Xplore1Locator3      StepLocatorClass                 !Xplore: Column displaying orh:CustOrderNumber
Xplore1Step4         StepCustomClass !DATE            !Xplore: Column displaying orh:thedate
Xplore1Locator4      StepLocatorClass                 !Xplore: Column displaying orh:thedate
Xplore1Step5         StepCustomClass !TIME            !Xplore: Column displaying orh:thetime
Xplore1Locator5      StepLocatorClass                 !Xplore: Column displaying orh:thetime
Xplore1Step6         StepLongClass   !LONG            !Xplore: Column displaying orh:Order_no
Xplore1Locator6      StepLocatorClass                 !Xplore: Column displaying orh:Order_no
Xplore1Step7         StepCustomClass !BYTE            !Xplore: Column displaying orh:procesed
Xplore1Locator7      StepLocatorClass                 !Xplore: Column displaying orh:procesed
Xplore1Step8         StepLongClass   !LONG            !Xplore: Column displaying orh:SalesNumber
Xplore1Locator8      StepLocatorClass                 !Xplore: Column displaying orh:SalesNumber
Xplore1Step9         StepCustomClass !DATE            !Xplore: Column displaying orh:pro_date
Xplore1Locator9      StepLocatorClass                 !Xplore: Column displaying orh:pro_date
Xplore14             CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore14Step1        StepStringClass !STRING          !Xplore: Column displaying orw:AccountNumber
Xplore14Locator1     StepLocatorClass                 !Xplore: Column displaying orw:AccountNumber
Xplore14Step2        StepStringClass !STRING          !Xplore: Column displaying orw:PartNumber
Xplore14Locator2     StepLocatorClass                 !Xplore: Column displaying orw:PartNumber
Xplore14Step3        StepStringClass !STRING          !Xplore: Column displaying orw:Description
Xplore14Locator3     StepLocatorClass                 !Xplore: Column displaying orw:Description
Xplore14Step4        StepLongClass   !LONG            !Xplore: Column displaying orw:Quantity
Xplore14Locator4     StepLocatorClass                 !Xplore: Column displaying orw:Quantity
Xplore14Step5        StepLongClass   !LONG            !Xplore: Column displaying orw:RecordNumber
Xplore14Locator5     StepLocatorClass                 !Xplore: Column displaying orw:RecordNumber

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('BrowseWebOrders','?Browse:1')    !Xplore
  BRW1.SequenceNbr = 0                                !Xplore
  Xplore14.GetBbSize('BrowseWebOrders','?List:2')     !Xplore
  BRW14.SequenceNbr = 0                               !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020343'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseWebOrders')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  Relate:ORDPEND.Open
  Relate:ORDWEBPR.Open
  Relate:RETSALES.Open
  Access:SUBTRACC.UseFile
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ORDHEAD,SELF)
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:ORDITEMS,SELF)
  BRW14.Init(?List:2,Queue:Browse:2.ViewPosition,BRW14::View:Browse,Queue:Browse:2,Relate:ORDWEBPR,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbg01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  Xplore14.Init(ThisWindow,BRW14,Queue:Browse:2,QuickWindow,?List:2,'sbg01app.INI','>Header',0,BRW14.ViewOrder,Xplore14.RestoreHeader,BRW14.SequenceNbr,XploreMask14,XploreMask114,XploreTitle14,BRW14.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,orh:ProcessSaleNoKey)
  BRW1.AddRange(orh:procesed)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,orh:SalesNumber,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,orh:pro_cust_name_key)
  BRW1.AddRange(orh:procesed)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,orh:pro_date,1,BRW1)
  BRW1.AddField(orh:CustName,BRW1.Q.orh:CustName)
  BRW1.AddField(orh:account_no,BRW1.Q.orh:account_no)
  BRW1.AddField(orh:CustOrderNumber,BRW1.Q.orh:CustOrderNumber)
  BRW1.AddField(orh:thedate,BRW1.Q.orh:thedate)
  BRW1.AddField(orh:thetime,BRW1.Q.orh:thetime)
  BRW1.AddField(orh:Order_no,BRW1.Q.orh:Order_no)
  BRW1.AddField(orh:procesed,BRW1.Q.orh:procesed)
  BRW1.AddField(orh:SalesNumber,BRW1.Q.orh:SalesNumber)
  BRW1.AddField(orh:pro_date,BRW1.Q.orh:pro_date)
  BRW8.Q &= Queue:Browse
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,ori:Keyordhno)
  BRW8.AddRange(ori:ordhno,orh:Order_no)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,ori:ordhno,1,BRW8)
  BRW8.AddField(ori:partno,BRW8.Q.ori:partno)
  BRW8.AddField(ori:partdiscription,BRW8.Q.ori:partdiscription)
  BRW8.AddField(ori:qty,BRW8.Q.ori:qty)
  BRW8.AddField(ori:OrderDate,BRW8.Q.ori:OrderDate)
  BRW8.AddField(ori:OrderTime,BRW8.Q.ori:OrderTime)
  BRW8.AddField(ori:recordnumber,BRW8.Q.ori:recordnumber)
  BRW8.AddField(ori:ordhno,BRW8.Q.ori:ordhno)
  BRW14.Q &= Queue:Browse:2
  BRW14.RetainRow = 0
  BRW14.AddSortOrder(,orw:PartNumberKey)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(?orw:AccountNumber,orw:AccountNumber,1,BRW14)
  BRW14.AddField(orw:AccountNumber,BRW14.Q.orw:AccountNumber)
  BRW14.AddField(orw:PartNumber,BRW14.Q.orw:PartNumber)
  BRW14.AddField(orw:Description,BRW14.Q.orw:Description)
  BRW14.AddField(orw:Quantity,BRW14.Q.orw:Quantity)
  BRW14.AddField(orw:RecordNumber,BRW14.Q.orw:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW14.AskProcedure = 0
      CLEAR(BRW14.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
    Relate:ORDPEND.Close
    Relate:ORDWEBPR.Close
    Relate:RETSALES.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
    Xplore14.EraseVisual()                            !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('BrowseWebOrders','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisWindow.Opened                                !Xplore
    Xplore14.sq.Col = Xplore14.CurrentCol
    GET(Xplore14.sq,Xplore14.sq.Col)
    IF Xplore14.ListType = 1 AND BRW14.ViewOrder = False !Xplore
      BRW14.SequenceNbr = 0                           !Xplore
    END                                               !Xplore
    Xplore14.PutInix('BrowseWebOrders','?List:2',BRW14.SequenceNbr,Xplore14.sq.AscDesc) !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  Xplore14.Kill()                                     !Xplore
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  Xplore14.Upper = True                               !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(orh:CustName,BRW1.Q.orh:CustName)
  Xplore1.AddField(orh:account_no,BRW1.Q.orh:account_no)
  Xplore1.AddField(orh:CustOrderNumber,BRW1.Q.orh:CustOrderNumber)
  Xplore1.AddField(orh:thedate,BRW1.Q.orh:thedate)
  Xplore1.AddField(orh:thetime,BRW1.Q.orh:thetime)
  Xplore1.AddField(orh:Order_no,BRW1.Q.orh:Order_no)
  Xplore1.AddField(orh:procesed,BRW1.Q.orh:procesed)
  Xplore1.AddField(orh:SalesNumber,BRW1.Q.orh:SalesNumber)
  Xplore1.AddField(orh:pro_date,BRW1.Q.orh:pro_date)
  BRW1.FileOrderNbr = BRW1.AddSortOrder(,orh:pro_cust_name_key) !Xplore Sort Order for File Sequence
  BRW1.AddRange(orh:procesed)                         !Xplore
  BRW1.SetOrder('')                                   !Xplore
  BRW1.ViewOrder = True                               !Xplore
  Xplore1.AddAllColumnSortOrders(1)                   !Xplore
  BRW1.ViewOrder = False                              !Xplore
  Xplore14.AddField(orw:AccountNumber,BRW14.Q.orw:AccountNumber)
  Xplore14.AddField(orw:PartNumber,BRW14.Q.orw:PartNumber)
  Xplore14.AddField(orw:Description,BRW14.Q.orw:Description)
  Xplore14.AddField(orw:Quantity,BRW14.Q.orw:Quantity)
  Xplore14.AddField(orw:RecordNumber,BRW14.Q.orw:RecordNumber)
  BRW14.FileOrderNbr = BRW14.AddSortOrder()           !Xplore Sort Order for File Sequence
  BRW14.SetOrder('')                                  !Xplore
  Xplore14.AddAllColumnSortOrders(0)                  !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020343'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020343'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020343'&'0')
      ***
    OF ?RefreshButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RefreshButton, Accepted)
      BRW1.ResetSort(1)
      BRW8.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RefreshButton, Accepted)
    OF ?CreateSale
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateSale, Accepted)
    !Check if the account of the selected sale is on stop - 4489 (DBH: 19-07-2004)
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = brw1.q.orh:Account_No
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        If sub:Stop_Account = 'YES'
            Case Missive('Cannot create a sale.'&|
              '<13,10>'&|
              '<13,10>The selected account is "on stop".','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Cycle
        End !If sub:Stop_Account = 'YES'
    Else !If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End !If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Brw1.UpdateViewRecord()
    Case Missive('Are you sure you want to create a sale from the highlighted order?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button
            Update_Retail_Sales(orh:Order_No)
        Of 1 ! No Button
    End ! Case Missive

      BRW1.ResetSort(1)
      BRW8.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateSale, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore14.IgnoreEvent = True                      !Xplore
     Xplore14.IgnoreEvent = False                     !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F10Key
              If Choice(?CurrentTab) = 1
                  Post(Event:Accepted,?CreateSale)
              End !If Choice(?CurrentTab) = 1
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      POST(xpEVENT:Xplore + 14)                       !Xplore
    OF EVENT:GainFocus
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      Xplore14.GetColumnInfo()                        !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 1
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  END                                                 !Xplore
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,14,'GetFreeElementName'
  IF BRW14.ViewOrder = True                           !Xplore
     RETURN('UPPER(' & CLIP(BRW14.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW14.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,14,'GetFreeElementPosition'
  IF BRW14.ViewOrder = True                           !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW14.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,14
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore14.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore14.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore14.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW14.ViewOrder = True                           !Xplore
    SavePtr# = POINTER(Xplore14.sq)                   !Xplore
    Xplore14.SetupOrder(BRW14.SortOrderNbr)           !Xplore
    GET(Xplore14.sq,SavePtr#)                         !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW14.SortOrderNbr,Force)       !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW14.FileSeqOn = True                        !Xplore
    RETURN SELF.SetSort(BRW14.FileOrderNbr,Force)     !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW14.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,14,'TakeEvent'
  Xplore14.LastEvent = EVENT()                        !Xplore
  IF FOCUS() = ?List:2                                !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore14.AdjustAllColumns()                  !Xplore
      OF AltF12                                       !Xplore
         Xplore14.ResetToDefault()                    !Xplore
      OF AltR                                         !Xplore
         Xplore14.ToggleBar()                         !Xplore
      OF AltM                                         !Xplore
         Xplore14.InvokePopup()                       !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore14.RightButtonUp                         !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW14.FileSeqOn = False                        !Xplore
       Xplore14.LeftButtonUp(0)                       !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW14.SavePosition()                          !Xplore
       Xplore14.HandleMyEvents()                      !Xplore
       !BRW14.RestorePosition()                       !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List:2                           !Xplore
  PARENT.TakeEvent


BRW14.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore14.LeftButton2()                       !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !orh:CustName
  OF 2 !orh:account_no
  OF 3 !orh:CustOrderNumber
  OF 4 !orh:thedate
  OF 5 !orh:thetime
  OF 6 !orh:Order_no
  OF 7 !orh:procesed
  OF 8 !orh:SalesNumber
  OF 9 !orh:pro_date
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step9,orh:pro_cust_name_key)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,orh:pro_cust_name_key)
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(,orh:CustName,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,orh:account_no,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,orh:CustOrderNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(,orh:thedate,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(,orh:thetime,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(,orh:Order_no,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(,orh:procesed,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(,orh:SalesNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(,orh:pro_date,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(orh:procesed)
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(ORDHEAD)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('orh:CustName')               !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Customer Name')              !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Customer Name')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orh:account_no')             !Field Name
                SHORT(160)                            !Default Column Width
                PSTRING('Account No')                 !Header
                PSTRING('@s40')                       !Picture
                PSTRING('Account No')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orh:CustOrderNumber')        !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Customer Order Number')      !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Customer Order Number')      !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orh:thedate')                !Field Name
                SHORT(44)                             !Default Column Width
                PSTRING('Date')                       !Header
                PSTRING('@d6b')                       !Picture
                PSTRING('Date')                       !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orh:thetime')                !Field Name
                SHORT(20)                             !Default Column Width
                PSTRING('Time')                       !Header
                PSTRING('@t1')                        !Picture
                PSTRING('Time')                       !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orh:Order_no')               !Field Name
                SHORT(48)                             !Default Column Width
                PSTRING('Order no')                   !Header
                PSTRING('@n12')                       !Picture
                PSTRING('Order no')                   !Description
                STRING('R')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orh:procesed')               !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('procesed')                   !Header
                PSTRING('@n3')                        !Picture
                PSTRING('procesed')                   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orh:SalesNumber')            !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Sales Transaction Number')   !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Sales Transaction Number')   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orh:pro_date')               !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('pro date')                   !Header
                PSTRING('@d17')                       !Picture
                PSTRING('pro date')                   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(9)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)
BRW14.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW14.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW14.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW14.ResetPairsQ PROCEDURE()
  CODE
Xplore14.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore14.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore14.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW14.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !orw:AccountNumber
  OF 2 !orw:PartNumber
  OF 3 !orw:Description
  OF 4 !orw:Quantity
  OF 5 !orw:RecordNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore14.SetNewOrderFields PROCEDURE()
  CODE
  BRW14.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore14.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore14Step5)
  SELF.FQ.SortField = SELF.BC.AddSortOrder()
  EXECUTE SortQRecord
    BEGIN
      Xplore14Step1.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore14Locator1)
      Xplore14Locator1.Init(?orw:AccountNumber,orw:AccountNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore14Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore14Locator2)
      Xplore14Locator2.Init(?orw:AccountNumber,orw:PartNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore14Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore14Locator3)
      Xplore14Locator3.Init(?orw:AccountNumber,orw:Description,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore14Step4.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore14Locator4)
      Xplore14Locator4.Init(?orw:AccountNumber,orw:Quantity,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore14Step5.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore14Locator5)
      Xplore14Locator5.Init(?orw:AccountNumber,orw:RecordNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  RETURN
!================================================================================
Xplore14.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore14.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW14.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(ORDWEBPR)
  END
  RETURN TotalRecords
!================================================================================
Xplore14.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('orw:AccountNumber')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('AccountNumber')              !Header
                PSTRING('@s30')                       !Picture
                PSTRING('AccountNumber')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orw:PartNumber')             !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Part Number')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Part Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orw:Description')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Description')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Description')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orw:Quantity')               !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Qty')                        !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Quantity')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orw:RecordNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Record Number')              !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(5)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateWebORders PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
BRW9::View:Browse    VIEW(ORDITEMS)
                       PROJECT(ori:partno)
                       PROJECT(ori:qty)
                       PROJECT(ori:recordnumber)
                       PROJECT(ori:ordhno)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
ori:partno             LIKE(ori:partno)               !List box control field - type derived from field
ori:qty                LIKE(ori:qty)                  !List box control field - type derived from field
ori:recordnumber       LIKE(ori:recordnumber)         !Primary key field - type derived from field
ori:ordhno             LIKE(ori:ordhno)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::orh:Record  LIKE(orh:RECORD),STATIC
QuickWindow          WINDOW('Update the ordhead File'),AT(,,280,182),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateWebORders'),SYSTEM,GRAY,NOFRAME
                       SHEET,AT(4,4,272,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('&Order no:'),AT(8,20),USE(?orh:Order_no:Prompt)
                           STRING(@n12),AT(108,20,52,10),USE(orh:Order_no),RIGHT(1)
                           PROMPT('account no:'),AT(8,34),USE(?orh:account_no:Prompt)
                           ENTRY(@s40),AT(108,34,164,10),USE(orh:account_no)
                           PROMPT('&Cust Name:'),AT(8,48),USE(?orh:CustName:Prompt)
                           ENTRY(@s30),AT(108,48,124,10),USE(orh:CustName)
                           PROMPT('&Cust Add 1:'),AT(8,62),USE(?orh:CustAdd1:Prompt)
                           ENTRY(@s30),AT(108,62,124,10),USE(orh:CustAdd1)
                           PROMPT('&Cust Add 2:'),AT(8,76),USE(?orh:CustAdd2:Prompt)
                           ENTRY(@s30),AT(108,76,124,10),USE(orh:CustAdd2)
                           PROMPT('&Cust Add 3:'),AT(8,90),USE(?orh:CustAdd3:Prompt)
                           ENTRY(@s30),AT(108,90,124,10),USE(orh:CustAdd3)
                           PROMPT('&Cust Post Code:'),AT(8,104),USE(?orh:CustPostCode:Prompt)
                           ENTRY(@s10),AT(108,104,44,10),USE(orh:CustPostCode)
                           PROMPT('&Cust Tel:'),AT(8,118),USE(?orh:CustTel:Prompt)
                           ENTRY(@s20),AT(108,118,84,10),USE(orh:CustTel)
                           PROMPT('&Cust Fax:'),AT(8,132),USE(?orh:CustFax:Prompt)
                           ENTRY(@s20),AT(108,132,84,10),USE(orh:CustFax)
                           PROMPT('&d Name:'),AT(8,146),USE(?orh:dName:Prompt)
                           ENTRY(@s30),AT(108,146,124,10),USE(orh:dName)
                         END
                         TAB('General (cont.)'),USE(?Tab:2)
                           PROMPT('&d Add 1:'),AT(8,20),USE(?orh:dAdd1:Prompt)
                           ENTRY(@s30),AT(108,20,124,10),USE(orh:dAdd1)
                           PROMPT('&d Add 2:'),AT(8,34),USE(?orh:dAdd2:Prompt)
                           ENTRY(@s30),AT(108,34,124,10),USE(orh:dAdd2)
                           PROMPT('&d Add 3:'),AT(8,48),USE(?orh:dAdd3:Prompt)
                           ENTRY(@s30),AT(108,48,124,10),USE(orh:dAdd3)
                           PROMPT('&d Post Code:'),AT(8,62),USE(?orh:dPostCode:Prompt)
                           ENTRY(@s10),AT(108,62,44,10),USE(orh:dPostCode)
                           PROMPT('&d Tel:'),AT(8,76),USE(?orh:dTel:Prompt)
                           ENTRY(@s20),AT(108,76,84,10),USE(orh:dTel)
                           PROMPT('&d Fax:'),AT(8,90),USE(?orh:dFax:Prompt)
                           ENTRY(@s20),AT(108,90,84,10),USE(orh:dFax)
                           PROMPT('&date:'),AT(8,104),USE(?orh:thedate:Prompt)
                           ENTRY(@d8),AT(108,104,104,10),USE(orh:thedate),RIGHT(1)
                           PROMPT('&time:'),AT(8,118),USE(?orh:thetime:Prompt)
                           ENTRY(@t1),AT(108,118,104,10),USE(orh:thetime),RIGHT(1)
                           PROMPT('procesed:'),AT(8,132),USE(?orh:procesed:Prompt)
                           ENTRY(@n3),AT(108,132,40,10),USE(orh:procesed)
                           PROMPT('pro date:'),AT(8,146),USE(?orh:pro_date:Prompt)
                           ENTRY(@d17),AT(108,146,104,10),USE(orh:pro_date)
                         END
                         TAB('General (cont. 2)'),USE(?Tab:3)
                           PROMPT('Who Booked'),AT(8,20),USE(?orh:WhoBooked:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,20,124,10),USE(orh:WhoBooked),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Who Booked'),TIP('Who Booked'),UPR
                           PROMPT('Department'),AT(8,34),USE(?orh:Department:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,34,124,10),USE(orh:Department),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Department'),TIP('Department'),UPR
                           PROMPT('Customer Order Number'),AT(8,48),USE(?orh:CustOrderNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,48,124,10),USE(orh:CustOrderNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Customer Order Number'),TIP('Customer Order Number'),UPR
                           PROMPT('Date Despatched'),AT(8,62),USE(?orh:DateDespatched:Prompt)
                           ENTRY(@d6),AT(108,62,104,10),USE(orh:DateDespatched),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Date Despatched'),TIP('Date Despatched'),UPR
                           PROMPT('Sales Transaction Number'),AT(8,76),USE(?orh:SalesNumber:Prompt),TRN
                           ENTRY(@s8),AT(108,76,40,10),USE(orh:SalesNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Sales Transaction Number'),TIP('Sales Transaction Number'),UPR
                         END
                         TAB('Tab 4'),USE(?Tab4)
                           LIST,AT(12,20,150,100),USE(?List),IMM,MSG('Browsing Records'),FORMAT('80L(2)|M~partno~@s20@40R(2)|M~qty~L@n10@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(34,129,42,12),USE(?Insert)
                           BUTTON('&Change'),AT(78,129,42,12),USE(?Change)
                           BUTTON('&Delete'),AT(122,129,42,12),USE(?Delete)
                         END
                       END
                       BUTTON('OK'),AT(182,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(231,164,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(231,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateWebORders')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?orh:Order_no:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(orh:Record,History::orh:Record)
  SELF.AddHistoryField(?orh:Order_no,1)
  SELF.AddHistoryField(?orh:account_no,2)
  SELF.AddHistoryField(?orh:CustName,3)
  SELF.AddHistoryField(?orh:CustAdd1,4)
  SELF.AddHistoryField(?orh:CustAdd2,5)
  SELF.AddHistoryField(?orh:CustAdd3,6)
  SELF.AddHistoryField(?orh:CustPostCode,7)
  SELF.AddHistoryField(?orh:CustTel,8)
  SELF.AddHistoryField(?orh:CustFax,9)
  SELF.AddHistoryField(?orh:dName,10)
  SELF.AddHistoryField(?orh:dAdd1,11)
  SELF.AddHistoryField(?orh:dAdd2,12)
  SELF.AddHistoryField(?orh:dAdd3,13)
  SELF.AddHistoryField(?orh:dPostCode,14)
  SELF.AddHistoryField(?orh:dTel,15)
  SELF.AddHistoryField(?orh:dFax,16)
  SELF.AddHistoryField(?orh:thedate,17)
  SELF.AddHistoryField(?orh:thetime,18)
  SELF.AddHistoryField(?orh:procesed,19)
  SELF.AddHistoryField(?orh:pro_date,20)
  SELF.AddHistoryField(?orh:WhoBooked,21)
  SELF.AddHistoryField(?orh:Department,22)
  SELF.AddHistoryField(?orh:CustOrderNumber,23)
  SELF.AddHistoryField(?orh:DateDespatched,24)
  SELF.AddHistoryField(?orh:SalesNumber,25)
  SELF.AddUpdateFile(Access:ORDHEAD)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDHEAD
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:ORDITEMS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW9.Q &= Queue:Browse
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,ori:Keyordhno)
  BRW9.AddRange(ori:ordhno,orh:Order_no)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,ori:ordhno,1,BRW9)
  BRW9.AddField(ori:partno,BRW9.Q.ori:partno)
  BRW9.AddField(ori:qty,BRW9.Q.ori:qty)
  BRW9.AddField(ori:recordnumber,BRW9.Q.ori:recordnumber)
  BRW9.AddField(ori:ordhno,BRW9.Q.ori:ordhno)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  BRW9.AskProcedure = 1
  BRW9.AddToolbarTarget(Toolbar)
  BRW9.ToolbarItem.HelpButton = ?Help
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateOrdItems
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

UpdateOrdItems PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::ori:Record  LIKE(ori:RECORD),STATIC
QuickWindow          WINDOW('Update the orditems File'),AT(,,332,154),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateOrdItems'),SYSTEM,GRAY,NOFRAME
                       SHEET,AT(4,4,324,128),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?ori:recordnumber:Prompt),TRN
                           ENTRY(@s8),AT(80,20,40,10),USE(ori:recordnumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('&ordhno:'),AT(8,34),USE(?ori:ordhno:Prompt)
                           ENTRY(@n12),AT(80,34,52,10),USE(ori:ordhno),RIGHT(1)
                           PROMPT('manufact:'),AT(8,48),USE(?ori:manufact:Prompt)
                           ENTRY(@s40),AT(80,48,164,10),USE(ori:manufact)
                           PROMPT('&partno:'),AT(8,62),USE(?ori:partno:Prompt)
                           ENTRY(@s20),AT(80,62,84,10),USE(ori:partno)
                           PROMPT('&partdiscription:'),AT(8,76),USE(?ori:partdiscription:Prompt)
                           ENTRY(@s60),AT(80,76,244,10),USE(ori:partdiscription)
                           PROMPT('&qty:'),AT(8,90),USE(?ori:qty:Prompt)
                           ENTRY(@n10),AT(80,90,44,10),USE(ori:qty),RIGHT(1)
                           PROMPT('&itemcost:'),AT(8,104),USE(?ori:itemcost:Prompt)
                           ENTRY(@n10.2),AT(80,104,44,10),USE(ori:itemcost),DECIMAL(14)
                           PROMPT('&totalcost:'),AT(8,118),USE(?ori:totalcost:Prompt)
                           ENTRY(@n10.2),AT(80,118,44,10),USE(ori:totalcost),DECIMAL(14)
                         END
                       END
                       BUTTON('OK'),AT(234,136,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(283,136,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(283,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateOrdItems')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ori:recordnumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ori:Record,History::ori:Record)
  SELF.AddHistoryField(?ori:recordnumber,1)
  SELF.AddHistoryField(?ori:ordhno,2)
  SELF.AddHistoryField(?ori:manufact,3)
  SELF.AddHistoryField(?ori:partno,4)
  SELF.AddHistoryField(?ori:partdiscription,5)
  SELF.AddHistoryField(?ori:qty,6)
  SELF.AddHistoryField(?ori:itemcost,7)
  SELF.AddHistoryField(?ori:totalcost,8)
  SELF.AddUpdateFile(Access:ORDITEMS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDITEMS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDITEMS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDITEMS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateRetailPartRoutine PROCEDURE                     ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_res_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    access:stock.clearkey(sto:ref_number_key)
    sto:ref_number  = glo:select3
    If access:stock.fetch(sto:ref_number_key)
        Case Missive('An error has occured retrieving the details of this Stock Part.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    Else!If access:stock.fetch(sto:ref_number_key)
        Access:RETSTOCK_ALIAS.Clearkey(ret_ali:Record_Number_Key)
        ret_ali:Record_Number = glo:Select4
        If Access:RETSTOCK_ALIAS.Tryfetch(ret_ali:Record_Number_Key) = Level:Benign
            !Found
            If Access:RETSTOCK.PrimeRecord() = Level:Benign
                RecordNumber$           = res:Record_Number
                res:Record              :=: ret_ali:Record
                res:Record_Number       = RecordNumber$
                res:Quantity            = glo:Select5
                res:Pending_Ref_Number  = glo:Select6
                If glo:Select2 = 'NEW PENDING'
                    res:Despatched = 'PEN'
                End !If glo:Select2 = 'NEW PENDING'
                If glo:Select2= 'CANCELLED'
                    res:Despatched = 'CAN'
                End !If glo:Select2= 'CANCELLED'
                res:Date_Ordered        = ''
                res:Date_Received       = ''
                If Access:RETSTOCK.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:RETSTOCK.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:RETSTOCK.TryInsert() = Level:Benign
            End !If Access:RETSTOCK.PrimeRecord() = Level:Benign
        Else! If Access:RETSTOCK_ALIAS.Tryfetch(ret_ali:Record_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:RETSTOCK_ALIAS.Tryfetch(ret_ali:Record_Number_Key) = Level:Benign
    End !If access:stock.fetch(sto:ref_number_key)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
BrowsePickingReconciliation PROCEDURE                 !Generated from procedure template - Window

CurrentTab           STRING(80)
save_retstock_id     USHORT,AUTO
save_res_id          USHORT,AUTO
save_ret_ali_id      USHORT,AUTO
Account_Number       STRING(15)
RET_temp             STRING('RET')
no_value_temp        REAL
tag_temp             STRING(1)
stop_on_errors_temp  BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Zero             BYTE(0)
BRW1::View:Browse    VIEW(RETSALES)
                       PROJECT(ret:Ref_Number)
                       PROJECT(ret:Purchase_Order_Number)
                       PROJECT(ret:Account_Number)
                       PROJECT(ret:date_booked)
                       PROJECT(ret:who_booked)
                       PROJECT(ret:Courier)
                       PROJECT(ret:Invoice_Number)
                       PROJECT(ret:Despatch_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ret:Ref_Number         LIKE(ret:Ref_Number)           !List box control field - type derived from field
ret:Purchase_Order_Number LIKE(ret:Purchase_Order_Number) !List box control field - type derived from field
ret:Account_Number     LIKE(ret:Account_Number)       !List box control field - type derived from field
ret:date_booked        LIKE(ret:date_booked)          !List box control field - type derived from field
ret:who_booked         LIKE(ret:who_booked)           !List box control field - type derived from field
ret:Courier            LIKE(ret:Courier)              !List box control field - type derived from field
ret:Invoice_Number     LIKE(ret:Invoice_Number)       !List box control field - type derived from field
ret:Despatch_Number    LIKE(ret:Despatch_Number)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Sales Awaiting Picking'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse Sales Awaiting Picking'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(68,86,472,272),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('39R(2)|M~Sales No~L@s8@92L(2)|M~Purchase Order Number~@s30@69L(2)|M~Account Numb' &|
   'er~@s15@47R(2)|M~Date Raised~@d6b@22L(2)|M~User~@s3@108L(2)|M~Courier~@s30@32L(2' &|
   ')|M~Invoice No~@s8@'),FROM(Queue:Browse:1)
                       BUTTON('&Insert'),AT(344,260,42,12),USE(?Insert),HIDE
                       BUTTON,AT(548,330),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                       BUTTON,AT(548,132),USE(?Print),TRN,FLAT,LEFT,ICON('prnroutp.jpg')
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Browse Sales'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(68,70,64,10),USE(ret:Ref_Number),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
windowsdir      CString(260)
systemdir       String(260)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020329'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowsePickingReconciliation')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RETSALES.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:USELEVEL.Open
  Relate:VATCODE.Open
  Access:RETSTOCK.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RETSALES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ret:DespatchNoRefNoKey)
  BRW1.AddRange(ret:Despatch_Number,tmp:Zero)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?ret:Ref_Number,ret:Ref_Number,1,BRW1)
  BRW1.AddField(ret:Ref_Number,BRW1.Q.ret:Ref_Number)
  BRW1.AddField(ret:Purchase_Order_Number,BRW1.Q.ret:Purchase_Order_Number)
  BRW1.AddField(ret:Account_Number,BRW1.Q.ret:Account_Number)
  BRW1.AddField(ret:date_booked,BRW1.Q.ret:date_booked)
  BRW1.AddField(ret:who_booked,BRW1.Q.ret:who_booked)
  BRW1.AddField(ret:Courier,BRW1.Q.ret:Courier)
  BRW1.AddField(ret:Invoice_Number,BRW1.Q.ret:Invoice_Number)
  BRW1.AddField(ret:Despatch_Number,BRW1.Q.ret:Despatch_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  brw1.InsertControl = 0
  brw1.DeleteControl = 0
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RETSALES.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:USELEVEL.Close
    Relate:VATCODE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of deleterecord
          check_access('RETAIL SALES - DELETE',x")
          if x" = false
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
      of insertrecord
          do_update# = false
          Case Missive('You cannot insert from here.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
  end !case request
  
  if do_update# = true
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Process_Retail_Sale
    ReturnValue = GlobalResponse
  END
      ! Inserting (DBH 25/08/2006) # 7964 - Print waybill?
      If glo:Select20 = 'PRINT WAYBILL'
          If glo:Select21 > 0
              glo:Select1 = glo:Select21
! Deleting (DBH 20/10/2006) # 8381 - Only print despatched items
!              Found# = False
!              Save_RETSTOCK_ID = Access:RETSTOCK.SaveFile()
!              Access:RETSTOCK.Clearkey(res:Part_Number_Key)
!              res:Ref_Number = glo:Select22
!              Set(res:Part_Number_Key,res:Part_Number_Key)
!              Loop ! Begin Loop
!                  If Access:RETSTOCK.Next()
!                      Break
!                  End ! If Access:RETSTOCK.Next()
!                  If res:Ref_Number <> glo:Select22
!                      Break
!                  End ! If res:Ref_Number <> f_Ref_Number
!                  If res:Despatched <> 'YES'
!                      Found# = True
!                      Break
!                  End ! If res:Despatched <> 'YES'
!              End ! Loop
!              Access:RETSTOCK.RestoreFile(Save_RETSTOCK_ID)
!
!              If Found# = True
!                  WaybillRetail('PIK')
!              Else ! If Found# = True
! End (DBH 20/10/2006) #8381
                  WaybillRetail
!              End ! If Found# = True
          End ! If glo:Select21 > 0
          glo:Select1 = ''
          glo:Select20 = ''
          glo:Select21 = ''
          glo:Select22 = ''
      End ! If glo:Select20 = 'PRINT WAYBILL'
      ! End (DBH 25/08/2006) #7964
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020329'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020329'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020329'&'0')
      ***
    OF ?Print
      ThisWindow.Update
      Report_Type(ret:ref_number)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?ret:Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Ref_Number, Selected)
      Select(?browse:1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Ref_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  !Are there any unpicked items? If not, don't show is this browse - 3584 (DBH: 16-01-2004)
  
  Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
  res:Ref_Number = ret:Ref_Number
  res:Despatched = 'PIK'
  If Access:RETSTOCK.TryFetch(res:Despatched_Only_Key)
      Return Record:Filtered
  End !Access:RETSTOCK.TryFetch(res:Despatched_Only_Key)
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

