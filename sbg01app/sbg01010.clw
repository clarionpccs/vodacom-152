

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01010.INC'),ONCE        !Local module procedure declarations
                     END


RetailPaid           PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_rtp_id          USHORT,AUTO
tmp:VATRate          REAL
tmp:PaymentTaken     REAL
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Quick fix routine. Place this in a01 later.
    If ret:Payment_Method = 'CAS'

        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = ret:Account_Number
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                    access:vatcode.clearkey(vat:vat_code_key)
                    vat:vat_code = sub:retail_vat_code
                    if access:vatcode.fetch(vat:vat_code_key) = level:benign
                        tmp:VatRate = vat:vat_rate
                    end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign

                Else !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                    access:vatcode.clearkey(vat:vat_code_key)
                    vat:vat_code = tra:retail_vat_code
                    if access:vatcode.fetch(vat:vat_code_key) = level:benign
                        tmp:VatRate = vat:vat_rate
                    end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign

                End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
            Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

        Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

        tmp:PaymentTaken = 0
        setcursor(cursor:wait)                                                                  !Has the stock been paid for
        save_rtp_id = access:retpay.savefile()
        access:retpay.clearkey(rtp:date_key)
        rtp:ref_number   = ret:ref_number
        set(rtp:date_key,rtp:date_key)
        loop
            if access:retpay.next()
               break
            end !if
            if rtp:ref_number   <> ret:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            tmp:PaymentTaken += rtp:amount
        end !loop
        access:retpay.restorefile(save_rtp_id)
        setcursor()

        If Round((ret:Sub_Total + (ret:Sub_Total * tmp:VatRate/100)),.01) - Round(tmp:PaymentTaken,.01) > .01
            Return Level:Fatal
        End !If Round((ret:Sub_Total + (ret:Sub_Total * tmp:VatRate/100)),.01) - Round(tmp:PaymentTaken,.01) > .01
    End !If ret:Payment_Method = 'CAS'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
BrowseAwaitingPayment PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
save_retstock_id     USHORT,AUTO
save_res_id          USHORT,AUTO
save_ret_ali_id      USHORT,AUTO
Account_Number       STRING(15)
RET_temp             STRING('RET')
no_value_temp        REAL
tag_temp             STRING(1)
stop_on_errors_temp  BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Zero             BYTE(0)
tmp:PAY              STRING('PAY')
BRW1::View:Browse    VIEW(RETSALES)
                       PROJECT(ret:Ref_Number)
                       PROJECT(ret:Purchase_Order_Number)
                       PROJECT(ret:Account_Number)
                       PROJECT(ret:date_booked)
                       PROJECT(ret:who_booked)
                       PROJECT(ret:Courier)
                       PROJECT(ret:Invoice_Number)
                       PROJECT(ret:Despatched)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ret:Ref_Number         LIKE(ret:Ref_Number)           !List box control field - type derived from field
ret:Purchase_Order_Number LIKE(ret:Purchase_Order_Number) !List box control field - type derived from field
ret:Account_Number     LIKE(ret:Account_Number)       !List box control field - type derived from field
ret:date_booked        LIKE(ret:date_booked)          !List box control field - type derived from field
ret:who_booked         LIKE(ret:who_booked)           !List box control field - type derived from field
ret:Courier            LIKE(ret:Courier)              !List box control field - type derived from field
ret:Invoice_Number     LIKE(ret:Invoice_Number)       !List box control field - type derived from field
ret:Despatched         LIKE(ret:Despatched)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Sales Awaiting Payment'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse Sales Awaiting Payment'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(68,72,468,286),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('39R(2)|M~Sales No~L@s8@92L(2)|M~Purchase Order Number~@s30@69L(2)|M~Account Numb' &|
   'er~@s15@47R(2)|M~Date Raised~@d6b@22L(2)|M~User~@s3@108L(2)|M~Courier~@s30@32L(2' &|
   ')|M~Invoice No~@s8@'),FROM(Queue:Browse:1)
                       BUTTON('&Insert'),AT(412,232,42,12),USE(?Insert),HIDE
                       BUTTON,AT(548,298),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                       BUTTON,AT(548,330),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       BUTTON,AT(548,122),USE(?Print),TRN,FLAT,LEFT,ICON('prnroutp.jpg')
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Browse Sales'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
windowsdir      CString(260)
systemdir       String(260)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020328'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseAwaitingPayment')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RETSALES.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:USELEVEL.Open
  Relate:VATCODE.Open
  Access:RETSTOCK.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RETSALES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ret:Despatched_Ref_Key)
  BRW1.AddRange(ret:Despatched,tmp:PAY)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,ret:Ref_Number,1,BRW1)
  BRW1.AddField(ret:Ref_Number,BRW1.Q.ret:Ref_Number)
  BRW1.AddField(ret:Purchase_Order_Number,BRW1.Q.ret:Purchase_Order_Number)
  BRW1.AddField(ret:Account_Number,BRW1.Q.ret:Account_Number)
  BRW1.AddField(ret:date_booked,BRW1.Q.ret:date_booked)
  BRW1.AddField(ret:who_booked,BRW1.Q.ret:who_booked)
  BRW1.AddField(ret:Courier,BRW1.Q.ret:Courier)
  BRW1.AddField(ret:Invoice_Number,BRW1.Q.ret:Invoice_Number)
  BRW1.AddField(ret:Despatched,BRW1.Q.ret:Despatched)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RETSALES.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:USELEVEL.Close
    Relate:VATCODE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of deleterecord
          check_access('RETAIL SALES - DELETE',x")
          if x" = false
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
      of insertrecord
          do_update# = false
          Case Missive('You cannot insert from here.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
  end !case request
  
  if do_update# = true
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Process_Retail_Sale
    ReturnValue = GlobalResponse
  END
      ! Inserting (DBH 25/08/2006) # 7964 - Print waybill?
      If glo:Select20 = 'PRINT WAYBILL'
          If glo:Select21 > 0
              glo:Select1 = glo:Select21
! Deleting (DBH 20/10/2006) # 8381 - Only print despatched items
!              Found# = False
!              Save_RETSTOCK_ID = Access:RETSTOCK.SaveFile()
!              Access:RETSTOCK.Clearkey(res:Part_Number_Key)
!              res:Ref_Number = glo:Select22
!              Set(res:Part_Number_Key,res:Part_Number_Key)
!              Loop ! Begin Loop
!                  If Access:RETSTOCK.Next()
!                      Break
!                  End ! If Access:RETSTOCK.Next()
!                  If res:Ref_Number <> glo:Select22
!                      Break
!                  End ! If res:Ref_Number <> f_Ref_Number
!                  If res:Despatched <> 'YES'
!                      Found# = True
!                      Break
!                  End ! If res:Despatched <> 'YES'
!              End ! Loop
!              Access:RETSTOCK.RestoreFile(Save_RETSTOCK_ID)
!
!              If Found# = True
!                  WaybillRetail('PIK')
!              Else ! If Found# = True
! End (DBH 20/10/2006) #8381
                  WaybillRetail
!              End ! If Found# = True
          End ! If glo:Select21 > 0
          glo:Select1 = ''
          glo:Select20 = ''
          glo:Select21 = ''
          glo:Select22 = ''
      End ! If glo:Select20 = 'PRINT WAYBILL'
      ! End (DBH 25/08/2006) #7964
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020328'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020328'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020328'&'0')
      ***
    OF ?Print
      ThisWindow.Update
      Report_Type(ret:ref_number)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BrowsePendingWebOrders PROCEDURE                      !Generated from procedure template - Browse

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_ori_id          USHORT,AUTO
save_orh_id          USHORT,AUTO
tmp:tag              STRING(1)
tmp:AccountNumber    STRING(30)
tmp:AccountType      STRING(3)
tmp:WebOrderNumber   LONG
OrderDate            DATE
OrderTime            TIME
BRW4::View:Browse    VIEW(ORDWEBPR)
                       PROJECT(orw:PartNumber)
                       PROJECT(orw:Description)
                       PROJECT(orw:Quantity)
                       PROJECT(orw:RecordNumber)
                       PROJECT(orw:AccountNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
orw:PartNumber         LIKE(orw:PartNumber)           !List box control field - type derived from field
orw:Description        LIKE(orw:Description)          !List box control field - type derived from field
orw:Quantity           LIKE(orw:Quantity)             !List box control field - type derived from field
orw:RecordNumber       LIKE(orw:RecordNumber)         !Primary key field - type derived from field
orw:AccountNumber      LIKE(orw:AccountNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Browse Pending Web Orders'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Pending Web Order File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('By Part Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,103,124,10),USE(orw:PartNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Part Number'),TIP('Part Number'),UPR
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,103,124,10),USE(orw:Description),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Description'),TIP('Description'),UPR
                         END
                       END
                       BUTTON,AT(168,300),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                       BUTTON,AT(236,300),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                       BUTTON,AT(304,300),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                       LIST,AT(168,116,276,182),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11LI@s1@116L(2)|M~Part Number~@s30@112L(2)|M~Description~@s30@32L(2)|M~Qty~@s8@'),FROM(Queue:Browse)
                       BUTTON,AT(448,166),USE(?Button3),TRN,FLAT,LEFT,ICON('makordp.jpg')
                       BUTTON('&Rev tags'),AT(632,244,2,2),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(636,268,2,2),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(448,244),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                       BUTTON,AT(448,274),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW4::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet1) = 2
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = orw:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = orw:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = orw:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::7:QUEUE = glo:Queue
    ADD(DASBRW::7:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.Pointer = orw:RecordNumber
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = orw:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020342'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowsePendingWebOrders')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  Relate:ORDWEBPR.Open
  Relate:TRADEACC.Open
  SELF.FilesOpened = True
  !Set trade account
  If glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          If tra:StoresAccount <> ''
              tmp:AccountNumber   = tra:StoresAccount
              tmp:AccountType     = 'SUB'
          Else !If tmp:StoresAccount <> ''
              tmp:AccountNumber   = Clarionet:Global.Param2
              tmp:AccountType     = 'TRA'
          End !If tmp:StoresAccount <> ''
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  Else !glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          If tra:StoresAccount <> ''
              tmp:AccountNumber   = tra:StoresAccount
              tmp:AccountType     = 'SUB'
          Else !If tmp:StoresAccount <> ''
              tmp:AccountNumber   = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
              tmp:AccountType     = 'TRA'
          End !If tmp:StoresAccount <> ''
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  End !glo:WebJob
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:ORDWEBPR,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,orw:DescriptionKey)
  BRW4.AddRange(orw:AccountNumber,tmp:AccountNumber)
  BRW4.AddLocator(BRW4::Sort1:Locator)
  BRW4::Sort1:Locator.Init(?orw:Description,orw:Description,1,BRW4)
  BRW4.AddSortOrder(,orw:PartNumberKey)
  BRW4.AddRange(orw:AccountNumber,tmp:AccountNumber)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(?orw:PartNumber,orw:PartNumber,1,BRW4)
  BIND('tmp:tag',tmp:tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:tag,BRW4.Q.tmp:tag)
  BRW4.AddField(orw:PartNumber,BRW4.Q.orw:PartNumber)
  BRW4.AddField(orw:Description,BRW4.Q.orw:Description)
  BRW4.AddField(orw:Quantity,BRW4.Q.orw:Quantity)
  BRW4.AddField(orw:RecordNumber,BRW4.Q.orw:RecordNumber)
  BRW4.AddField(orw:AccountNumber,BRW4.Q.orw:AccountNumber)
  BRW4.AskProcedure = 1
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab1{PROP:TEXT} = 'By Part Number'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?List{PROP:FORMAT} ='11LI@s1@#1#116L(2)|M~Part Number~@s30@#3#112L(2)|M~Description~@s30@#4#32L(2)|M~Qty~@s8@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
    Relate:ORDWEBPR.Close
    Relate:TRADEACC.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    AmendPendingWebOrder
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020342'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020342'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020342'&'0')
      ***
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      If ~Records(glo:Queue)
          Case Missive('You have not tagged any parts.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !Records(glo:Queue)
          Case Missive('Are you sure you want to create an order for the tagged items?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  
      
                  !TB12359 - J - 13/02/14 - create a common date time stamp
                  OrderDate = today()
                  OrderTime = clock()
                  !end tb12359
      
                  PendingWebOrderReport(orderTime)
      
                  Loop x# = 1 To Records(glo:Queue)
                      !Does an order already exist for this account?
                      tmp:WebOrderNumber = 0
                      Save_orh_ID = Access:ORDHEAD.SaveFile()
                      Access:ORDHEAD.ClearKey(orh:ProcessSaleNoKey)
                      orh:procesed    = 0
                      Set(orh:ProcessSaleNoKey,orh:ProcessSaleNoKey)
                      Loop
                          If Access:ORDHEAD.NEXT()
                             Break
                          End !If
                          If orh:procesed    <> 0      |
                              Then Break.  ! End If
                          If orh:Account_No = tmp:AccountNumber
                              tmp:WebOrderNumber = orh:Order_no
                              Break
                          End !If orh:Account_No = job:Account_Number
                      End !Loop
                      Access:ORDHEAD.RestoreFile(Save_orh_ID)
      
                      Get(glo:Queue,x#)
                      Access:ORDWEBPR.ClearKey(orw:RecordNumberKey)
                      orw:RecordNumber = glo:Pointer
                      If Access:ORDWEBPR.TryFetch(orw:RecordNumberKey) = Level:Benign
                          !Found
                          If tmp:WebOrderNumber
                              !This is an order raised for this account,
                              !now lets see if the same part has been ordered and add to that
                              Found# = 0
                              Save_ori_ID = Access:ORDITEMS.SaveFile()
                              Access:ORDITEMS.ClearKey(ori:Keyordhno)
                              ori:ordhno = tmp:WebOrderNumber
                              Set(ori:Keyordhno,ori:Keyordhno)
                              Loop
                                  If Access:ORDITEMS.NEXT()
                                     Break
                                  End !If
                                  If ori:ordhno <> tmp:WebOrderNumber      |
                                      Then Break.  ! End If
                                  If ori:partno = orw:PartNumber And ori:partdiscription = orw:Description
                                      ori:qty += orw:Quantity
                                      !TB12359 - J - 13/02/14 - update the date time stamp of this order
                                      ori:OrderDate = OrderDate
                                      ori:OrderTime = OrderTime
                                      !end TB12359
                                      Access:ORDITEMS.Update()
                                      Found# = 1
                                      Break
                                  End !ori:partdiscription = wpr:Description
                              End !Loop
                              Access:ORDITEMS.RestoreFile(Save_ori_ID)
      
                              If Found# = 0
                                  !Make a new part line
                                  If Access:ORDITEMS.PrimeRecord() = Level:Benign
                                      ori:ordhno          = tmp:WebOrderNumber
                                      !ori:manufact        = job:Manufacturer
                                      ori:qty             = orw:Quantity
                                      ori:itemcost        = orw:ItemCost
                                      ori:totalcost       = orw:ItemCost * orw:Quantity
                                      ori:partno          = orw:PartNumber
                                      ori:partdiscription = orw:Description
                                      !TB12359 - J - 13/02/14 - update the date time stamp of this order
                                      ori:OrderDate       = OrderDate
                                      ori:OrderTime       = OrderTime
                                      !end TB12359
                                      If Access:ORDITEMS.TryInsert() = Level:Benign
                                          !Insert Successful
                                      Else !If Access:ORDITEMS.TryInsert() = Level:Benign
                                          !Insert Failed
                                      End !If Access:ORDITEMS.TryInsert() = Level:Benign
                                  End !If Access:ORDITEMS.PrimeRecord() = Level:Benign
                              End !If Found# = 0
                          Else !If tmp:WebOrderNumber
                              !There is no existing orders for this account
                              If Access:ORDHEAD.PrimeRecord() = Level:Benign
                                  orh:account_no      = tmp:AccountNumber
      
                                  !Get Sub or Trade Account details?
                                  Case tmp:AccountType
                                      Of 'TRA'
                                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                                          tra:Account_Number  = tmp:AccountNumber
                                          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                              !Found
                                              orh:CustName        = tra:Company_Name
                                              orh:CustAdd1        = tra:Address_Line1
                                              orh:CustAdd2        = tra:Address_Line2
                                              orh:CustAdd3        = tra:Address_Line3
                                              orh:CustPostCode    = tra:Postcode
                                              orh:CustTel         = tra:Telephone_Number
                                              orh:CustFax         = tra:Fax_Number
      
                                              orh:dName           = tra:Company_Name
                                              orh:dAdd1           = tra:Address_Line1
                                              orh:dAdd2           = tra:Address_Line2
                                              orh:dAdd3           = tra:Address_Line3
                                              orh:dPostCode       = tra:Postcode
                                              orh:dTel            = tra:Telephone_Number
                                              orh:dFax            = tra:Fax_Number
      
                                          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                              !Error
                                          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
                                      Of 'SUB'
                                          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                                          sub:Account_Number  = tmp:AccountNumber
                                          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                              !Found
                                              orh:CustName        = sub:Company_Name
                                              orh:CustAdd1        = sub:Address_Line1
                                              orh:CustAdd2        = sub:Address_Line2
                                              orh:CustAdd3        = sub:Address_Line3
                                              orh:CustPostCode    = sub:Postcode
                                              orh:CustTel         = sub:Telephone_Number
                                              orh:CustFax         = sub:Fax_Number
      
                                              orh:dName           = sub:Company_Name
                                              orh:dAdd1           = sub:Address_Line1
                                              orh:dAdd2           = sub:Address_Line2
                                              orh:dAdd3           = sub:Address_Line3
                                              orh:dPostCode       = sub:Postcode
                                              orh:dTel            = sub:Telephone_Number
                                              orh:dFax            = sub:Fax_Number
      
                                          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                              !Error
                                          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                  End !Case tmp:AccountType
      
                                  orh:thedate         = Today()
                                  orh:thetime         = Clock()
                                  orh:procesed        = 0
                                  Access:USERS.Clearkey(use:Password_Key)
                                  use:Password    = glo:Password
                                  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                      !Found
      
                                  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                      !Error
                                  End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
                                  orh:WhoBooked       = use:User_Code
                                  orh:Department      = ''
                                  orh:CustOrderNumber = ''
                                  If Access:ORDHEAD.TryInsert() = Level:Benign
                                      !Insert Successful
                                      !Make a new part line
                                      If Access:ORDITEMS.PrimeRecord() = Level:Benign
                                          ori:ordhno          = orh:Order_No
                                          !ori:manufact        = job:Manufacturer
                                          ori:qty             = orw:Quantity
                                          ori:itemcost        = orw:ItemCost
                                          ori:totalcost       = orw:ItemCost * orw:Quantity
                                          ori:partno          = orw:PartNumber
                                          ori:partdiscription = orw:Description
                                          !TB12359 - J - 13/02/14 - update the date time stamp of this order
                                          ori:OrderDate = OrderDate
                                          ori:OrderTime = OrderTime
                                          !end TB12359
                                          If Access:ORDITEMS.TryInsert() = Level:Benign
                                              !Insert Successful
                                          Else !If Access:ORDITEMS.TryInsert() = Level:Benign
                                              !Insert Failed
                                          End !If Access:ORDITEMS.TryInsert() = Level:Benign
                                      End !If Access:ORDITEMS.PrimeRecord() = Level:Benign
      
                                  Else !If Access:ORDHEAD.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End !If Access:ORDHEAD.TryInsert() = Level:Benign
                              End !If Access:ORDHEAD.PrimeRecord() = Level:Benign
                          End !If tmp:WebOrderNumber
                          Delete(ORDWEBPR)
                      Else!If Access:ORDWEBPR.TryFetch(orw:RecordNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:ORDWEBPR.TryFetch(orw:RecordNumberKey) = Level:Benign
                  End !Loop x# = 1 To Records(glo:Queue)
      
              Of 1 ! No Button
          End ! Case Missive
      End !Records(glo:Queue)
      BRW4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag)
          Cycle
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet1)
        OF 1
          ?List{PROP:FORMAT} ='11LI@s1@#1#116L(2)|M~Part Number~@s30@#3#112L(2)|M~Description~@s30@#4#32L(2)|M~Qty~@s8@#5#'
          ?Tab1{PROP:TEXT} = 'By Part Number'
        OF 2
          ?List{PROP:FORMAT} ='11LI@s1@#1#112L(2)|M~Description~@s30@#4#116L(2)|M~Part Number~@s30@#3#32L(2)|M~Qty~@s8@#5#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW4.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet1) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = orw:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = orw:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue

AmendPendingWebOrder PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
sav:Quantity         REAL
History::orw:Record  LIKE(orw:RECORD),STATIC
QuickWindow          WINDOW('Amend Pending Order'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Pending Order Item'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Part Number'),AT(248,182),USE(?orw:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(308,182,124,10),USE(orw:PartNumber),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Part Number'),TIP('Part Number'),UPR,READONLY
                           PROMPT('Description'),AT(248,204),USE(?orw:Description:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(308,204,124,10),USE(orw:Description),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Description'),TIP('Description'),UPR,READONLY
                           PROMPT('Quantity'),AT(248,224),USE(?orw:Quantity:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(308,224,64,10),USE(orw:Quantity),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Quantity'),TIP('Quantity'),UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020340'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AmendPendingWebOrder')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(orw:Record,History::orw:Record)
  SELF.AddHistoryField(?orw:PartNumber,3)
  SELF.AddHistoryField(?orw:Description,4)
  SELF.AddHistoryField(?orw:Quantity,5)
  SELF.AddUpdateFile(Access:ORDWEBPR)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDWEBPR.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDWEBPR
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  !Save quantity incase its changed.
  sav:Quantity    = orw:Quantity
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDWEBPR.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020340'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020340'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020340'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If orw:Quantity <> sav:Quantity
      Case Missive('Are you sure you want to change the quantity of this pending part?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
          Of 1 ! No Button
              orw:Quantity    = sav:Quantity
              Select(?orw:Quantity)
              Cycle
      End ! Case Missive
  End !orw:Quantity <> sav:Quantity
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BrowseOutstandingStockOrders PROCEDURE                !Generated from procedure template - Browse

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
save_ori_id          USHORT,AUTO
save_orh_id          USHORT,AUTO
tmp:tag              STRING(1)
tmp:AccountNumber    STRING(30)
tmp:AccountType      STRING(3)
tmp:WebOrderNumber   LONG
ListDisplayQ         QUEUE,PRE(ldq)
PartNumber           STRING(30)
Description          STRING(30)
Quantity             LONG
DateRaised           DATE
TheDate              DATE
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
stockQ queue(ListDisplayQ), pre(stq)        ! Stores the outstanding stock orders
       end

exchangeQ queue(ListDisplayQ), pre(exq)     ! Stores the outstanding exchange orders
          end

loanQ queue(ListDisplayQ),pre(loq)          ! Stores the outstanding loan orders
      end
window               WINDOW('Browse Outstanding Stock Orders'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Outstanding Stock Orders File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('By Part Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Exchange Models Ordered'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Loan Models Ordered'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       LIST,AT(168,98,344,228),USE(?List1),VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('106L(2)|M~Part Number~@s30@94L(2)|M~Description~@s30@30R(2)|M~Qty~@s8@49R(2)|M~R' &|
   'aised~@d6@48R(2)|M~Processed~@d6@'),FROM(ListDisplayQ)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

myProg        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
myProg:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(myProg.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(myProg.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(myProg.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?myProg:CancelButton)
     END

omit('***',ClarionetUsed=0)

myProg:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(myProg.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(myProg.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(myProg.CNPercentText),CENTER
     END
***

QBox       QBoxClass
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
CopyStock routine
    data
i       long
    code
    ! Copy the contents of the stock queue into the display queue
    free(ListDisplayQ)

    loop i = 1 to records(stockQ)
        get(stockQ, i)
        ListDisplayQ :=: stockQ
        add(ListDisplayQ)
    end
CopyExchange routine
    data
i       long
    code
    ! Copy the contents of the exchange queue into the display queue
    free(ListDisplayQ)

    loop i = 1 to records(exchangeQ)
        get(exchangeQ, i)
        ListDisplayQ :=: exchangeQ
        add(ListDisplayQ)
    end
CopyLoan routine
    data
i       long
    code
    ! Copy the contents of the loan queue into the display queue
    free(ListDisplayQ)

    loop i = 1 to records(loanQ)
        get(loanQ, i)
        ListDisplayQ :=: loanQ
        add(ListDisplayQ)
    end
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020341'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseOutstandingStockOrders')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCHORDR.Open
  Relate:LOAORDR.Open
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  Relate:RETSALES.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:TRADEACC.Open
  Access:RETSTOCK.UseFile
  SELF.FilesOpened = True
  ! Set trade account
  If glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          If tra:StoresAccount <> ''
              tmp:AccountNumber   = tra:StoresAccount
              tmp:AccountType     = 'SUB'
          Else !If tmp:StoresAccount <> ''
              tmp:AccountNumber   = Clarionet:Global.Param2
              tmp:AccountType     = 'TRA'
          End !If tmp:StoresAccount <> ''
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  Else !glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          If tra:StoresAccount <> ''
              tmp:AccountNumber   = tra:StoresAccount
              tmp:AccountType     = 'SUB'
          Else !If tmp:StoresAccount <> ''
              tmp:AccountNumber   = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
              tmp:AccountType     = 'TRA'
          End !If tmp:StoresAccount <> ''
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  End !glo:WebJob
  
  ! Changing (DBH 06/12/2007) # 4006 - Tidy up the code
  !! Load stock queue
  !Access:ORDHEAD.ClearKey(orh:AccountDateKey)
  !orh:account_no = tmp:AccountNumber
  !set(orh:AccountDateKey, orh:AccountDateKey)
  !loop until Access:ORDHEAD.Next()
  !    if orh:account_no <> tmp:AccountNumber then break.  ! Loop through all orders for this account
  !    !------------------------------------------------------------------------------------!
  !
  !    !If order has not been processed yet.
  !    If orh:procesed = 0
  !        Access:ORDITEMS.ClearKey(ori:Keyordhno)
  !        ori:ordhno = orh:Order_no
  !        set(ori:Keyordhno, ori:Keyordhno)
  !        loop until Access:ORDITEMS.Next()                   ! Loop through items on order
  !            if ori:ordhno <> orh:Order_no then break.
  !            if ori:qty = 0 then cycle.
  !            stockQ.stq:PartNumber  = ori:partno
  !            stockQ.stq:Description = ori:partdiscription
  !            stockQ.stq:DateRaised  = orh:TheDate
  !            get(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description, stockQ.stq:DateRaised)
  !            if error()
  !                stockQ.stq:Quantity    = ori:qty
  !                stockQ.stq:TheDate     = ''
  !                add(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description)
  !            else
  !                stockQ.stq:Quantity   += ori:qty
  !                put(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description)
  !            end
  !        end
  !        Cycle
  !    End !If orh:Processed = 0
  !    !------------------------------------------------------------------------------------!
  !
  !    !If the order HAS been processed
  !    Access:RETSALES.ClearKey(ret:Ref_Number_Key)
  !    ret:Ref_Number = orh:SalesNumber                    ! Fetch attached retail sale
  !    if not Access:RETSALES.Fetch(ret:Ref_Number_Key)
  !! Deleting (DBH 05/12/2007) # 4006 - Show the items until they have been received
  !!        !If invoiced, then sale has been despatched, so no longer outstanding
  !!        If ret:Invoice_Number <> ''
  !!            Cycle
  !!        End !If ret:Invoice_Number <> ''
  !! End (DBH 05/12/2007) #4006
  !
  !        ! Inserting (DBH 05/12/2007) # 4006 - All web orders that have been processed should habe the web order number.
  !        !This will filter out old entries
  !        If ret:WebOrderNumber = 0
  !            Cycle
  !        End ! If ret:WebOrderNumber = 0
  !        ! End (DBH 05/12/2007) #4006
  !
  !        Access:RETSTOCK.ClearKey(res:Part_Number_Key)
  !        res:Ref_Number = ret:Ref_Number
  !        set(res:Part_Number_Key, res:Part_Number_Key)
  !        loop until Access:RETSTOCK.Next()               ! Fetch items on sale
  !            if res:Ref_Number <> ret:Ref_Number then break.
  !
  !            ! Inserting (DBH 06/12/2007) # 4006 - Remove any cancelled parts
  !            If res:Despatched = 'CAN'
  !                stockQ.stq:PartNumber  = res:Part_Number
  !                stockQ.stq:Description = res:Description
  !                stockQ.stq:TheDate     = orh:pro_date
  !                stockQ.stq:DateRaised  = orh:TheDate
  !                get(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description, stockQ.stq:TheDate, stockQ.stq:DateRaised)
  !                if not error()
  !                    stockQ.stq:Quantity -= res:Quantity
  !                    if stockQ.stq:Quantity < 0 then stockQ.stq:Quantity = 0.
  !                    if stockQ.stq:Quantity = 0        ! Remove item from browse if qty < 1
  !                        delete(stockQ)
  !                        sort(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description)
  !                    else
  !                        put(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description)
  !                    end
  !                End ! if not error()
  !                Cycle
  !            End ! If res:Despatched = 'CAN'
  !            ! End (DBH 06/12/2007) #4006
  !
  !            !Was the part put on back order, and is now on a new order?
  !            If res:Despatched = 'OLD'
  !                !Is the new order despatched?
  !                Access:RETSALES_ALIAS.ClearKey(res_ali:Ref_Number_Key)
  !                res_ali:Ref_Number = res:Previous_Sale_Number
  !                If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
  !                    !Found
  !! Changing (DBH 05/12/2007) # 4006 - Check if the back order part is legitimate
  !!                    If res_ali:Invoice_Number <> ''
  !!                        Cycle
  !!                    End !If res_ali:Invoice_Number <> ''
  !! to (DBH 05/12/2007) # 4006
  !                    Access:RETSTOCK_ALIAS.Clearkey(ret_ali:Part_Number_Key)
  !                    ret_ali:Ref_Number = res_ali:Ref_Number
  !                    ret_ali:Part_Number = res:Part_Number
  !                    Set(ret_ali:Part_Number_Key,ret_ali:Part_Number_Key)
  !                    Loop
  !                        If Access:RETSTOCK_ALIAS.Next()
  !                            Break
  !                        End ! If Access:RETSTOCK_ALIAS.Next()
  !                        If ret_ali:Ref_Number <> res_ali:Ref_Number
  !                            Break
  !                        End ! If ret_ali:Ref_Number <> res_ali:Ref_Number
  !                        If ret_ali:Part_Number <> res:Part_Number
  !                            Break
  !                        End ! If ret_ali:Part_Number <> res:Part_Number
  !
  !                        stockQ.stq:PartNumber  = ret_ali:Part_Number
  !                        stockQ.stq:Description = ret_ali:Description
  !                        stockQ.stq:TheDate     = orh:pro_date
  !                        stockQ.stq:DateRaised  = orh:TheDate
  !                        get(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description, stockQ.stq:TheDate, stockQ.stq:DateRaised)
  !                        if not error()                              ! Item should already exist in queue
  !                            if ret_ali:QuantityReceived <> 0 And ret_ali:Received  then       ! If item has already been received, deduct it's quantity from the total outstanding
  !                                stockQ.stq:Quantity -= ret_ali:QuantityReceived
  !                            else
  !                                stockQ.stq:Quantity += ret_ali:Quantity
  !                            end
  !                            if stockQ.stq:Quantity < 0 then stockQ.stq:Quantity = 0.
  !                            if stockQ.stq:Quantity = 0        ! Remove item from browse if qty < 1
  !                                delete(stockQ)
  !                                sort(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description)
  !                            else
  !                                put(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description)
  !                            end
  !                        Else
  !                            If ret_ali:Received = 0
  !                                ! A new entry. Only add to list if it has not been received (DBH: 05/12/2007)
  !                                stockQ.stq:Quantity    = ret_ali:Quantity
  !                                add(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description)
  !                            End ! If ret_ali:Received = 0
  !                        end
  !                    End ! Loop
  !
  !! End (DBH 05/12/2007) #4006
  !                Else!If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
  !                    !Error
  !                    !Assert(0,'<13,10>Fetch Error<13,10>')
  !                End!If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
  !                Cycle
  !            End !If res:Despatched = 'ORD'
  !
  !            stockQ.stq:PartNumber  = res:Part_Number
  !            stockQ.stq:Description = res:Description
  !            stockQ.stq:TheDate     = orh:pro_date
  !            stockQ.stq:DateRaised  = orh:TheDate
  !            get(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description, stockQ.stq:TheDate, stockQ.stq:DateRaised)
  !            if not error()                              ! Item should already exist in queue
  !                if res:QuantityReceived <> 0 And res:Received  then       ! If item has already been received, deduct it's quantity from the total outstanding
  !                    stockQ.stq:Quantity -= res:QuantityReceived
  !                else
  !                    stockQ.stq:Quantity += res:Quantity
  !                end
  !                if stockQ.stq:Quantity < 0 then stockQ.stq:Quantity = 0.
  !                if stockQ.stq:Quantity = 0        ! Remove item from browse if qty < 1
  !                    delete(stockQ)
  !                    sort(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description)
  !                else
  !                    put(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description)
  !                end
  !            Else
  !                If res:Received = 0
  !                    ! A new entry. Only add to list if it has not been received (DBH: 05/12/2007)
  !                    stockQ.stq:Quantity    = res:Quantity
  !                    add(stockQ, stockQ.stq:PartNumber, stockQ.stq:Description)
  !                End ! If res:Received = 0
  !            end
  !        end
  !    end
  !    !------------------------------------------------------------------------------------!
  !end
  ! to (DBH 06/12/2007) # 4006
  
  myProg.INIT(RECORDS(ORDHEAD) + RECORDS(EXCHORDR) + RECORDS(LOAORDR))
  
  ! Loop Through Web Orders For This Account
  Access:ORDHEAD.Clearkey(orh:AccountDateKey)
  orh:Account_No = tmp:AccountNumber
  Set(orh:AccountDateKey,orh:AccountDateKey)
  Loop
      If Access:ORDHEAD.Next()
          Break
      End ! If Access:ORDHEAD.Next()
      If orh:Account_No <> tmp:AccountNumber
          Break
      End ! If orh:Account_No <> tmp:AccountNumber
  
        IF (myProg.Update('Building Part Order List...'))
            BREAK
        END
  
      ! Count unprocessed orders first (DBH: 06/12/2007)
      If orh:Procesed = 0
          ! Add up items on the unprocessed order (DBH: 06/12/2007)
          Access:ORDITEMS.Clearkey(ori:KeyOrdHNo)
          ori:OrdHNo = orh:Order_No
          Set(ori:KeyOrdHNo,ori:KeyOrdHNo)
          Loop
              If Access:ORDITEMS.Next()
                  Break
              End ! If Access:ORDITEMS.Next()
              If ori:OrdHNo <> orh:Order_No
                  Break
              End ! If ori:OrdHNo <> orh:Order_No
              If ori:Qty = 0
                  Cycle
              End ! If ori:Qty = 0
  
              stq:PartNumber = ori:PartNo
              stq:Description = ori:PartDiscription
              stq:DateRaised = orh:TheDate
              Get(StockQ,stq:PartNumber,stq:Description,stq:DateRaised)
              If Error()
                  stq:Quantity = ori:Qty
                  stq:TheDate = 0
                  Add(StockQ)
              Else ! If Error()
                  stq:Quantity += ori:Qty
                  Put(StockQ)
              End ! If Error()
          End ! Loop
  
      Else ! If orh:Procesed = 0
          ! Find the retail sale associated with this web order (DBH: 06/12/2007)
          Access:RETSALES.Clearkey(ret:Ref_Number_Key)
          ret:Ref_Number = orh:SalesNumber
          If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
              ! Double check the the sale has the corresponding web order number (DBH: 06/12/2007)
              If ret:WebOrderNumber = 0
                  Cycle
              End ! If ret:WebOrderNumber = 0
  
              ! Count the items on the sale (DBH: 06/12/2007)
              Access:RETSTOCK.Clearkey(res:Part_Number_Key)
              res:Ref_Number = ret:Ref_Number
              Set(res:Part_Number_Key,res:Part_Number_Key)
              Loop
                  If Access:RETSTOCK.Next()
                      Break
                  End ! If Access:RETSTOCK.Next()
                  If res:Ref_Number <> ret:Ref_Number
                      Break
                  End ! If res:Ref_Number <> ret:Ref_Number
                  ! Item should be on another sale, so don't count here. (DBH: 06/12/2007)
                  If res:Despatched = 'OLD'
                      Cycle
                  End ! If res:Despatched = 'OLD'
  
                  If res:Despatched = 'CAN'
                      CYCLE
                  END
  !                    ! Item has been cancelled, exclude from count (DBH: 06/12/2007)
  !                    stq:PartNumber = res:Part_Number
  !                    stq:Description = res:Description
  !                    stq:TheDate = orh:Pro_Date
  !                    stq:DateRaised = orh:TheDate
  !                    Get(StockQ,stq:PartNumber,stq:Description,stq:TheDate,stq:DateRaised)
  !                    If Error()
  !                        stq:Quantity = - res:Quantity
  !                        Add(StockQ)
  !                    Else ! If Error()
  !                        stq:Quantity -= res:Quantity
  !                        Add(StockQ)
  !                    End ! If Error()
  !                    Cycle
  !                End ! If res:Despatched = 'CAN'
  
                  ! Count the part if it hasn't arrived yet (DBH: 06/12/2007)
  
                  stq:PartNumber = res:Part_Number
                  stq:Description = res:Description
                  stq:TheDate = orh:Pro_Date
                  stq:DateRaised = orh:TheDate
                  Get(StockQ,stq:PartNumber,stq:Description,stq:TheDate,stq:DateRaised)
                  If Error()
                      If res:Received = 0
                          stq:Quantity = res:Quantity
                          Add(StockQ)
                      End ! If res:Received
  
                  Else ! If Error()
                      If res:Received = 0
                          stq:Quantity += res:Quantity
                          Put(StockQ)
                      End ! If res:Received
                  End ! If Error()
              End ! Loop
          End ! If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
      End ! If orh:Procesed = 0
  End ! Loop
  
  ! Remove any negative entries from the list (DBH: 06/12/2007)
  Sort(StockQ,stq:PartNumber)
  Loop x# = 1 To Records(StockQ)
      Get(StockQ,x#)
      If Error()
          Break
      End ! If Error()
      If stq:Quantity < 1
          Delete(StockQ)
          x# -= 1
          Cycle
      End ! If stq:Quantity <= 0
  End ! Loop x# = 1 To Records(StockQ)
  
  
  
  ! End (DBH 06/12/2007) #4006
  
  ! Load exchange queue
  set(exo:Ref_Number_Key)
  loop until Access:EXCHORDR.Next()
        IF (myProg.Update('Building Exchange Order List...'))
            BREAK
        END
      if exo:Qty_Received < exo:Qty_Required              ! This check is just for safety, when exchange orders are completed the EXCHORDR record is deleted
          !Only show exchange order for this site's location
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          If glo:WebJob
              tra:Account_Number  = Clarionet:Global.Param2
          Else !glo:WebJob
              tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
          End !glo:WebJob
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              If exo:Location <> tra:SiteLocation
                  Cycle
              End !If exo:Location <> tra:Location
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          exchangeQ.exq:PartNumber = exo:Model_Number
          exchangeQ.exq:Description = exo:Manufacturer
          exchangeQ.exq:Quantity = exo:Qty_Required - exo:Qty_Received
          exchangeQ.exq:DateRaised = exo:DateCreated
          add(exchangeQ, exchangeQ.exq:Description)
      end
  end
  
  ! Load loan queue
  set(lor:Ref_Number_Key)
  loop until Access:LOAORDR.Next()
        IF (myProg.Update('Building Loan Order List...'))
            BREAK
        END
      if lor:Qty_Received < lor:Qty_Required
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          If glo:WebJob
              tra:Account_Number  = Clarionet:Global.Param2
          Else !glo:WebJob
              tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
          End !glo:WebJob
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              If exo:Location <> tra:SiteLocation
                  Cycle
              End !If exo:Location <> tra:Location
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          loanQ.loq:PartNumber = lor:Model_Number
          loanQ.loq:Description = lor:Manufacturer
          loanQ.loq:Quantity = lor:Qty_Required - lor:Qty_Received
          loanQ.loq:DateRaised = lor:DateCreated
          add(loanQ, loanQ.loq:Description)
      end
  end
  
  do CopyStock
  
    myProg.Kill()
  
  sort(ListDisplayQ, ListDisplayQ.ldq:PartNumber)
  select(?List1, 1)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  QBox.Init(ListDisplayQ,?List1)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHORDR.Close
    Relate:LOAORDR.Close
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
    Relate:RETSALES.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:TRADEACC.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?List1
      QBox.Update()
    END
    ! Check for left mouse-click
    QBox.Update()
    QBox.TakeAccepted(1)
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020341'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020341'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020341'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List1
    CASE EVENT()
    OF EVENT:ScrollUp
      QBox.ScrollUp()
    OF EVENT:ScrollDown
      QBox.ScrollDown()
    OF EVENT:PageUp
      QBox.PageUp()
    OF EVENT:PageDown
      QBox.PageDown()
    OF EVENT:ScrollTop
      QBox.UpdatePosition(1)
    OF EVENT:ScrollBottom
      QBox.UpdatePosition(RECORDS(ListDisplayQ))
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?List1
      QBox.Update()
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Sheet1, NewSelection)
      ! Reformat browse
      case choice(?Sheet1)
          of 1
              ?List1{PROP:FORMAT} ='106L(2)|M~Part Number~@s30@108L(2)|M~Description~@s30@37R(2)|M~Quantity~@s8@42R(' &|
                 '2)|M~Raised~@d6@40R(2)|M~Processed~@d6@'
              do CopyStock
              sort(ListDisplayQ, ListDisplayQ.ldq:PartNumber)
          of 2
              ?List1{PROP:FORMAT} ='108L(2)|M~Description~@s30@106L(2)|M~Part Number~@s30@37R(2)|M~Quantity~@s8@42R(' &|
                 '2)|M~Raised~@d6@40R(2)|M~Processed~@d6@'
              do CopyStock
              sort(ListDisplayQ, ListDisplayQ.ldq:Description)
          of 3
              ?List1{PROP:FORMAT} = '106L(2)|M~Manufacturer~@s30@108L(2)|M~Model Number~@s30@37R(2)|M~Quantity~@s8@42R(' &|
                 '2)|M~Raised~@d6@'
              do CopyExchange
              sort(ListDisplayQ, ListDisplayQ.ldq:Description)
          of 4
              ?List1{PROP:FORMAT} = '106L(2)|M~Manufacturer~@s30@108L(2)|M~Model Number~@s30@37R(2)|M~Quantity~@s8@42R(' &|
                 '2)|M~Raised~@d6@'
              do CopyLoan
              sort(ListDisplayQ, ListDisplayQ.ldq:Description)
      end
      
      display()
      select(?List1, 1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Sheet1, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
myProg.Init                 PROCEDURE(LONG func:Records)
    CODE
        myProg.ProgressSetup(func:Records)
myProg.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Browse
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        myProg.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(myProg:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        myProg.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.ResetProgress      Procedure(Long func:Records)
CODE

    myProg.recordsToProcess = func:Records
    myProg.recordsprocessed = 0
    myProg.percentProgress = 0
    myProg.progressThermometer = 0
    myProg.CNprogressThermometer = 0
    myProg.skipRecords = 0
    myProg.userText = ''
    myProg.CNuserText = ''
    myProg.percentText = '0% Completed'
    myProg.CNpercentText = myProg.percentText


myProg.Update      Procedure(<String func:String>)
    CODE
        RETURN (myProg.InsideLoop(func:String))
myProg.InsideLoop     Procedure(<String func:String>)
CODE

    myProg.SkipRecords += 1
    If myProg.SkipRecords < 100
        myProg.RecordsProcessed += 1
        Return 0
    Else
        myProg.SkipRecords = 0
    End
    if (func:String <> '')
        myProg.UserText = Clip(func:String)
        myProg.CNUserText = myProg.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        myProg.NextRecord()
        ClarioNet:UpdatePushWindow(myProg:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        myProg.NextRecord()
        if (myProg.CancelLoop())
            return 1
        end ! if (myProg.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

myProg.ProgressText        Procedure(String    func:String)
CODE

    myProg.UserText = Clip(func:String)
    myProg.CNUserText = myProg.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.Kill     Procedure()
    CODE
        myProg.ProgressFinish()
myProg.ProgressFinish     Procedure()
CODE

    myProg.ProgressThermometer = 100
    myProg.CNProgressThermometer = 100
    myProg.PercentText = '100% Completed'
    myProg.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(myProg:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.NextRecord      Procedure()
CODE
    Yield()
    myProg.RecordsProcessed += 1
    !If myProg.percentprogress < 100
        myProg.percentprogress = (myProg.recordsprocessed / myProg.recordstoprocess)*100
        If myProg.percentprogress > 100 or myProg.percentProgress < 0
            myProg.percentprogress = 0
        End
        If myProg.percentprogress <> myProg.ProgressThermometer then
            myProg.ProgressThermometer = myProg.percentprogress
            myProg.PercentText = format(myProg:percentprogress,@n3) & '% Completed'
        End
    !End
    myProg.CNPercentText = myProg.PercentText
    myProg.CNProgressThermometer = myProg.ProgressThermometer
    Display()

myProg.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?myProg:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
BackOrderViewer PROCEDURE (f:PartNumber)              !Generated from procedure template - Window

save_retstock_alias_id USHORT,AUTO
tmp:Total            LONG
BRW10::View:Browse   VIEW(RETACCOUNTSLIST)
                       PROJECT(retacc:CompanyName)
                       PROJECT(retacc:AccountNumber)
                       PROJECT(retacc:SaleNumber)
                       PROJECT(retacc:DateOrdered)
                       PROJECT(retacc:QuantityOrdered)
                       PROJECT(retacc:RecordNumber)
                       PROJECT(retacc:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
retacc:CompanyName     LIKE(retacc:CompanyName)       !List box control field - type derived from field
retacc:AccountNumber   LIKE(retacc:AccountNumber)     !List box control field - type derived from field
retacc:SaleNumber      LIKE(retacc:SaleNumber)        !List box control field - type derived from field
retacc:DateOrdered     LIKE(retacc:DateOrdered)       !List box control field - type derived from field
retacc:QuantityOrdered LIKE(retacc:QuantityOrdered)   !List box control field - type derived from field
retacc:RecordNumber    LIKE(retacc:RecordNumber)      !Primary key field - type derived from field
retacc:RefNumber       LIKE(retacc:RefNumber)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       LIST,AT(168,86,344,224),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('131L(2)|M~Company Name~@s30@73L(2)|M~Account Number~@s30@38L(2)|M~Sale No~@s8@51' &|
   'R(2)|M~Date Ordered~@d6@32L(2)|M~Qty Ordered~@s8@'),FROM(Queue:Browse)
                       PROMPT('Total:'),AT(448,316),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@s8),AT(472,316),USE(tmp:Total),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Back Order Details'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW10                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetFromView          PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020659'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  ! Inserting (DBH 22/09/2006) # 7963 - Try and make the filename as unique as possible
  Access:USERS.ClearKey(use:Password_Key)
  use:Password = glo:Password
  If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Found
  Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Error
  End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
  glo:File_Name = 'P' & Clip(use:User_Code) & Day(Today()) & Clock() & RANDOM(1,1000) & '.TMP'
  glo:File_Name2 = 'Q' & Clip(use:User_Code) & Day(Today()) & Clock() & RANDOM(1,1000) & '.TMP'
  Remove(Glo:File_Name)
  Remove(Glo:File_Name2)
  ! End (DBH 22/09/2006) #7963
  
  GlobalErrors.SetProcedureName('BackOrderViewer')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RETACCOUNTSLIST.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:STOCK.Open
  SELF.FilesOpened = True
  Save_RETSTOCK_ALIAS_ID = Access:RETSTOCK_ALIAS.SaveFile()
  Access:RETSTOCK_ALIAS.Clearkey(ret_ali:DespatchPartKey)
  ret_ali:Despatched = 'PEN'
  ret_ali:Part_Number = f:PartNumber
  Set(ret_ali:DespatchPartKey,ret_ali:DespatchPartKey)
  Loop ! Begin Loop
      If Access:RETSTOCK_ALIAS.Next()
          Break
      End ! If Access:RETSTOCK_ALIAS.Next()
      If ret_ali:Despatched <> 'PEN'
          Break
      End ! If ret_ali:Despatched <> 'PEN'
      If ret_ali:Part_Number <> f:PartNumber
          Break
      End ! If ret_ali:Part_Number <> f:PartNumber
      Access:RETSALES_ALIAS.ClearKey(res_ali:Ref_Number_Key)
      res_ali:Ref_Number = ret_ali:Ref_Number
      If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
          !Found
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = ret_ali:Part_Ref_Number
          If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              If Access:RETACCOUNTSLIST.PrimeRecord() = Level:Benign
                  retacc:RefNumber = 1
                  retacc:AccountNumber = res_ali:Account_Number
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number = res_ali:Account_Number
                  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      retacc:CompanyName = sub:Company_Name
                  Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                  retacc:DateOrdered = res_ali:Date_Booked
                  retacc:QuantityOrdered = ret_ali:Quantity
                  retacc:SaleNumber = res_ali:Ref_Number
                  retacc:OrigRecordNumber = ret_ali:Record_Number
                  If Access:RETACCOUNTSLIST.TryInsert() = Level:Benign
                      !Insert
                  Else ! If Access:RETACCOUNTSLIST.TryInsert() = Level:Benign
                      Access:RETACCOUNTSLIST.CancelAutoInc()
                  End ! If Access:RETACCOUNTSLIST.TryInsert() = Level:Benign
              End ! If Access.RETACCOUNTSLIST.PrimeRecord() = Level:Benign
          Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
              !Error
          End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
      Else ! If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
          !Error
      End ! If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
  
  End ! Loop
  Access:RETSTOCK_ALIAS.RestoreFile(Save_RETSTOCK_ALIAS_ID)
  BRW10.Init(?List,Queue:Browse.ViewPosition,BRW10::View:Browse,Queue:Browse,Relate:RETACCOUNTSLIST,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW10.Q &= Queue:Browse
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,retacc:AccountNumberKey)
  BRW10.AddRange(retacc:RefNumber,)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,retacc:AccountNumber,1,BRW10)
  BRW10.AddField(retacc:CompanyName,BRW10.Q.retacc:CompanyName)
  BRW10.AddField(retacc:AccountNumber,BRW10.Q.retacc:AccountNumber)
  BRW10.AddField(retacc:SaleNumber,BRW10.Q.retacc:SaleNumber)
  BRW10.AddField(retacc:DateOrdered,BRW10.Q.retacc:DateOrdered)
  BRW10.AddField(retacc:QuantityOrdered,BRW10.Q.retacc:QuantityOrdered)
  BRW10.AddField(retacc:RecordNumber,BRW10.Q.retacc:RecordNumber)
  BRW10.AddField(retacc:RefNumber,BRW10.Q.retacc:RefNumber)
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETACCOUNTSLIST.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:STOCK.Close
  END
  GlobalErrors.SetProcedureName
  Remove(glo:File_Name)
  Remove(glo:File_Name2)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020659'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020659'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020659'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW10.ResetFromView PROCEDURE

tmp:Total:Sum        REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:RETACCOUNTSLIST.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    tmp:Total:Sum += retacc:QuantityOrdered
  END
  tmp:Total = tmp:Total:Sum
  PARENT.ResetFromView
  Relate:RETACCOUNTSLIST.SetQuickScan(0)
  SETCURSOR()


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

ScanParts PROCEDURE (fPartNumber)                     !Generated from procedure template - Window

locPartNumber        STRING(30)
locReturn            BYTE
window               WINDOW('Top Tip'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Scan Part Number'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Part Number'),AT(248,204),USE(?locPartNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold)
                       ENTRY(@s30),AT(308,204,124,10),USE(locPartNumber),COLOR(COLOR:White),MSG('Part Number'),TIP('Part Number'),UPR
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(locReturn)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020733'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ScanParts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?OK,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      if (locPartNumber = '')
          select(?locPartNumber)
          cycle
      end ! if (locPartNumber = '')
      locReturn = 1
      fPartNumber = locPartNumber
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      locReturn = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020733'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020733'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020733'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
