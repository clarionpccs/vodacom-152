

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01005.INC'),ONCE        !Local module procedure declarations
                     END


tmp:One   BYTE(1)
Delivery_Text PROCEDURE                               !Generated from procedure template - Window

FilesOpened          BYTE
window               WINDOW('Delivery Text'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Delivery Text'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                         END
                         TAB('Tab 2'),USE(?Tab2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       TEXT,AT(232,166,216,84),USE(ret:Delivery_Text),VSCROLL,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR
                       BUTTON,AT(448,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020345'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Delivery_Text')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:RETSALES.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Delivery_Text')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Delivery_Text')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020345'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020345'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020345'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Update_Retail_Sales PROCEDURE (func:OrderNumber)      !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:CreateInvoice    BYTE(0)
stop_on_errors_temp  BYTE
sav:ref_number       LONG
Parts_Queue_Temp     QUEUE,PRE(PARTMP)
Part_Ref_Number      REAL
Pending_Ref_Number   REAL
Quantity             REAL
                     END
ref_number_temp      LONG
save_res_ali_id      USHORT,AUTO
save_res_id          USHORT,AUTO
save_rtp_id          USHORT,AUTO
vat_rate_temp        REAL
Payment_Method_Temp  STRING(3)
ActionMessage        CSTRING(40)
History::ret:Record  LIKE(ret:RECORD),STATIC
hMenu                LONG
menuItemCount        LONG
hWnd                 LONG
pos                  LONG
mInfo                LIKE(MENUITEMINFO)
MenuType             CSTRING(260)
QuickWindow          WINDOW('Update the RETSALES File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert Retail Sale'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,436,88),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Prompt 1'),AT(68,58,256,12),USE(?Prompt1),FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 2'),AT(344,58,152,12),USE(?Prompt2),RIGHT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Account Number'),AT(68,74),USE(?RET:Account_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(144,74,124,10),USE(ret:Account_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(272,70),USE(?LookupAccountNumber),SKIP,TRN,FLAT,FONT('Tahoma',8,,,CHARSET:ANSI),ICON('lookupp.jpg')
                           PROMPT('Purchase Order No'),AT(68,98),USE(?RET:Purchase_Order_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(144,98,124,10),USE(ret:Purchase_Order_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Contact Name'),AT(68,122),USE(?RET:Contact_Name:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(144,122,124,10),USE(ret:Contact_Name),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           OPTION('Delivery Or Collection'),AT(300,74,124,28),USE(ret:Delivery_Collection),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Delivery'),AT(308,86),USE(?RET:Delivery_Collection:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('DEL')
                             RADIO('Collection'),AT(368,86),USE(?RET:Delivery_Collection:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('COL')
                           END
                           PROMPT('Courier Cost'),AT(432,78),USE(?RET:Courier_Cost:Prompt),TRN,HIDE,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(432,90,64,10),USE(ret:Courier_Cost),HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           OPTION('Payment Method'),AT(300,106,196,28),USE(ret:Payment_Method),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Trade'),AT(308,118),USE(?RET:Payment_Method:Radio1),SKIP,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('TRA')
                             RADIO('Cash'),AT(360,118),USE(?RET:Payment_Method:Radio2),SKIP,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('CAS')
                             RADIO('Exchange Accessory'),AT(404,118),USE(?ret:Payment_Method:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('EXC')
                           END
                         END
                       END
                       SHEET,AT(64,144,220,220),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Invoice Address'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Company Name'),AT(68,164),USE(?RET:Company_Name:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(144,164,124,10),USE(ret:Company_Name),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Postcode'),AT(68,176),USE(?RET:Postcode:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s10),AT(144,176,64,10),USE(ret:Postcode),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Building Name/No'),AT(68,188),USE(?RET:Building_Name:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(144,188,124,10),USE(ret:Building_Name),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Address'),AT(68,204),USE(?RET:Address_Line1:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(144,204,124,10),USE(ret:Address_Line1),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(144,216,124,10),USE(ret:Address_Line2),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(144,228,124,10),USE(ret:Address_Line3),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(68,244),USE(?RET:Telephone_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(144,244,124,10),USE(ret:Telephone_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Fax Number'),AT(68,260),USE(?RET:Fax_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(144,260,124,10),USE(ret:Fax_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       SHEET,AT(288,144,212,220),USE(?Delivery_Sheet),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Delivery Address'),USE(?Tab3)
                           BUTTON,AT(292,278),USE(?Replicate),TRN,FLAT,LEFT,ICON('repinvp.jpg')
                           PROMPT('Company Name'),AT(292,164),USE(?RET:Company_Name_Delivery:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(368,164,124,10),USE(ret:Company_Name_Delivery),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Postcode'),AT(292,176),USE(?RET:Postcode_Delivery:Prompt),TRN,LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s10),AT(368,176,64,10),USE(ret:Postcode_Delivery),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Building Name/No'),AT(292,188),USE(?RET:Building_Name_Delivery:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(368,188,124,10),USE(ret:Building_Name_Delivery),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Address'),AT(292,204),USE(?RET:Address_Line1_Delivery:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(368,204,124,10),USE(ret:Address_Line1_Delivery),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(368,216,124,10),USE(ret:Address_Line2_Delivery),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Delivery Address'),AT(292,148),USE(?Prompt19),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(368,228,124,10),USE(ret:Address_Line3_Delivery),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(292,244),USE(?RET:Telephone_Delivery:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(368,244,124,10),USE(ret:Telephone_Delivery),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Fax Number'),AT(292,260),USE(?RET:Fax_Number_Delivery:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(368,260,124,10),USE(ret:Fax_Number_Delivery),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       PANEL,AT(504,56,112,308),USE(?Panel5),FILL(09A6A7CH)
                       BUTTON,AT(548,238),USE(?StockEnquiry),TRN,FLAT,LEFT,ICON('stoenqp.jpg')
                       BUTTON,AT(548,268),USE(?Payment),TRN,FLAT,HIDE,LEFT,ICON('paydetp.jpg')
                       BUTTON,AT(548,238),USE(?WebStockRequests),TRN,FLAT,HIDE,ICON('webreqp.jpg')
                       BUTTON,AT(548,298),USE(?Delivery_Text),TRN,FLAT,LEFT,ICON('deltextp.jpg')
                       BUTTON,AT(548,328),USE(?Invoice_Text),TRN,FLAT,LEFT,ICON('invtextp.jpg')
                       BUTTON,AT(484,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:ret:Account_Number                Like(ret:Account_Number)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
StockEnquiry        Routine

  error# = 0
  IF ret:account_number   = ''
    Case Missive('You must select a Trade Account.','ServiceBase 3g',|
                   'mstop.jpg','/OK')
        Of 1 ! OK Button
    End ! Case Missive
      error# = 1
  End!IF ret:account_number   = ''
  If error# = 0 And ret:payment_method = ''
    Case Missive('You must select a Payment Method.','ServiceBase 3g',|
                   'mstop.jpg','/OK')
        Of 1 ! OK Button
    End ! Case Missive
      error# = 1
  End!If error# = 0 And ret:payment_method = ''
  If error# = 0 And ret:delivery_collection = ''
    Case Missive('You must select Delivery or Collection.','ServiceBase 3g',|
                   'mstop.jpg','/OK')
        Of 1 ! OK Button
    End ! Case Missive
      error# = 1
  End!If error# = 0 And ret:payment_method = ''
  If error# = 0
      Stock_Enquiry(func:OrderNumber)
  End!If error# = 0

! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Retail Sale'
  OF ChangeRecord
    GlobalErrors.Throw(Msg:UpdateIllegal)
    RETURN
  OF DeleteRecord
    GlobalErrors.Throw(Msg:DeleteIllegal)
    RETURN
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020338'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  Globalrequest   = Insertrecord
  
  GlobalErrors.SetProcedureName('Update_Retail_Sales')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ret:Record,History::ret:Record)
  SELF.AddHistoryField(?ret:Account_Number,5)
  SELF.AddHistoryField(?ret:Purchase_Order_Number,7)
  SELF.AddHistoryField(?ret:Contact_Name,6)
  SELF.AddHistoryField(?ret:Delivery_Collection,8)
  SELF.AddHistoryField(?ret:Courier_Cost,10)
  SELF.AddHistoryField(?ret:Payment_Method,9)
  SELF.AddHistoryField(?ret:Company_Name,28)
  SELF.AddHistoryField(?ret:Postcode,27)
  SELF.AddHistoryField(?ret:Building_Name,29)
  SELF.AddHistoryField(?ret:Address_Line1,30)
  SELF.AddHistoryField(?ret:Address_Line2,31)
  SELF.AddHistoryField(?ret:Address_Line3,32)
  SELF.AddHistoryField(?ret:Telephone_Number,33)
  SELF.AddHistoryField(?ret:Fax_Number,34)
  SELF.AddHistoryField(?ret:Company_Name_Delivery,36)
  SELF.AddHistoryField(?ret:Postcode_Delivery,35)
  SELF.AddHistoryField(?ret:Building_Name_Delivery,37)
  SELF.AddHistoryField(?ret:Address_Line1_Delivery,38)
  SELF.AddHistoryField(?ret:Address_Line2_Delivery,39)
  SELF.AddHistoryField(?ret:Address_Line3_Delivery,40)
  SELF.AddHistoryField(?ret:Telephone_Delivery,41)
  SELF.AddHistoryField(?ret:Fax_Number_Delivery,42)
  SELF.AddUpdateFile(Access:RETSALES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:ORDHEAD.Open
  Relate:ORDPEND.Open
  Relate:RETPAY.Open
  Relate:RETSALES_ALIAS.Open
  Relate:VATCODE.Open
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETSALES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If func:OrderNumber <> 0
      Access:ORDHEAD.ClearKey(orh:KeyOrder_no)
      orh:Order_no = func:OrderNumber
      If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
          !Found
          ret:Account_Number         = orh:Account_No
          ret:Contact_Name           = orh:Department
          ret:Purchase_Order_Number  = orh:CustOrderNumber
          ret:WebOrderNumber         = orh:Order_No
          ret:WebDateCreated         = orh:TheDate
          ret:WebTimeCreated         = orh:TheTime
          ret:WebCreatedUser         = orh:WhoBooked
          ret:Postcode               = orh:CustPostCode
          ret:Company_Name           = orh:CustName
          ret:Building_Name          = ''
          ret:Address_Line1          = orh:CustAdd1
          ret:Address_Line2          = orh:CustAdd2
          ret:Address_Line3          = orh:CustAdd3
          ret:Telephone_Number       = orh:CustTel
          ret:Fax_Number             = orh:CustFax
          ret:Postcode_Delivery      = orh:dPostCode
          ret:Company_Name_Delivery  = orh:dName
          ret:Building_Name_Delivery = ''
          ret:Address_Line1_Delivery = orh:dAdd1
          ret:Address_Line2_Delivery = orh:dAdd2
          ret:Address_Line3_Delivery = orh:dAdd3
          ret:Telephone_Delivery     = orh:dTel
          ret:Fax_Number_Delivery    = orh:dFax
          ret:Delivery_Text          = ''
          ret:Invoice_Text           = ''
  
      Else!If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
  End !func:OrderNumber <> 0
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF thiswindow.request   = Insertrecord
      access:users.clearkey(use:password_key)
      use:password =glo:password
      access:users.fetch(use:password_key)
      ret:who_booked  = use:user_code
  Else!IF thiswindow.request   = Insertrecord
      access:users.clearkey(use:user_code_key)
      use:user_code = ret:who_booked
      access:users.fetch(use:user_code_key)
  End!IF thiswindow.request   = Insertrecord
  ?prompt1{prop:text} = 'Sales No: ' & CLip(ret:ref_number) & '   User: ' & Clip(use:forename) & ' ' & CLip(use:surname)
  ?prompt2{prop:text} = 'Date Raised: ' & Format(ret:date_booked,@d6) & '  ' & Format(ret:time_booked,@t1)
  
  If func:OrderNumber <> 0
      Post(Event:Accepted,?ret:Account_Number)
      ?StockEnquiry{prop:Hide} = 1
      ?WebStockRequests{prop:Hide} = 0
      !?StockEnquiry{prop:Text} = 'Web Stock Requests'
  End !func:OrderNumber <> 0
  
  If GETINI('RETAIL','ForcePurchaseOrderNumber',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?ret:Purchase_Order_Number{prop:req} = 1
  End !GETINI('RETAIL','ForcePurchaseOrderNumber',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  
  
  
  ! Save Window Name
   AddToLog('Window','Open','Update_Retail_Sales')
     hWnd = 0{PROP:Handle}
     !get the system menu handle
     hMenu = GetSystemMenu(hwnd, 0)
  
     !loop backwards through the menu
     LOOP I# = GetMenuItemCount(hMenu) To 0 BY -1
        minfo.cbSize     = Size(mInfo)
        minfo.fMask      = BOR(MIIM_TYPE,MIIM_ID)
        minfo.fType      = MFT_STRING
        MenuType         = ALL(' ',255)
        minfo.dwTypeData = Address(MenuType)
        mInfo.cch        = Size(MenuType)
        !Retrieve the current MENUITEMINFO.
        !Specifying fByPosition=True indicates
        !uItem points to an item by position.
        !Returns 1 if successful
        If GetMenuItemInfo(hMenu, I#, True, mInfo) = 1 Then
          !The close command has an ID of 61536. Once
          !that has been deleted, in a MDI Child window
          !two separators would remain (in a SDI one
          !separator would remain).
          !
          !To assure that this code will cover both
          !MDI and SDI system menus, 1 is subtracted
          !from the item number (c) to remove either
          !the top separator (of the two that will
          !remain in a MDIChild), or the single SDI
          !one (that would remain if this code was
          !applied against a SDI form.)
           If (mInfo.wID = 61536) Then
              a# = RemoveMenu(hMenu, I#,BOR(MF_REMOVE,MF_BYPOSITION))
              b# = RemoveMenu(hMenu, I#-1,BOR(MF_REMOVE,MF_BYPOSITION))
           End
        End
     END !LOOP
  
     !force a redraw of the non-client
     !area of the form to cause the
     !disabled X to paint correctly
     c# = DrawMenuBar(hwnd)
     Display
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?ret:Account_Number{Prop:Tip} AND ~?LookupAccountNumber{Prop:Tip}
     ?LookupAccountNumber{Prop:Tip} = 'Select ' & ?ret:Account_Number{Prop:Tip}
  END
  IF ?ret:Account_Number{Prop:Msg} AND ~?LookupAccountNumber{Prop:Msg}
     ?LookupAccountNumber{Prop:Msg} = 'Select ' & ?ret:Account_Number{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  ref_number_temp = ret:ref_number
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:ORDHEAD.Close
    Relate:ORDPEND.Close
    Relate:RETPAY.Close
    Relate:RETSALES_ALIAS.Close
    Relate:VATCODE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Retail_Sales')
  If thiswindow.response  = 2
      If thiswindow.request   = Insertrecord
          access:stock.open()
          access:stock.usefile()
          access:stohist.open()
          access:stohist.usefile()
          access:ordpend.open()
          access:ordpend.usefile()
  
          Clear(parts_queue_temp)
          Loop x# = 1 To Records(parts_queue_temp)
              Get(parts_queue_temp,x#)
              If partmp:pending_ref_number <> ''
                  access:ordpend.clearkey(ope:ref_number_key)
                  ope:ref_number  = partmp:pending_Ref_number
                  If access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                      Delete(ordpend)
                  End!If access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
              Else!If partmp:pending_ref_number <> ''
                  If partmp:part_ref_number <> ''
                      access:stock.clearkey(sto:ref_number_key)
                      sto:ref_number  = partmp:part_ref_number
                      If access:stock.fetch(sto:ref_number_key) = Level:Benign
                          sto:quantity_stock  += partmp:quantity
                          access:stock.update()
                          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'REC', | ! Transaction_Type
                                             '', | ! Depatch_Note_Number
                                             ret:Ref_Number, | ! Job_Number
                                             0, | ! Sales_Number
                                             partmp:Quantity, | ! Quantity
                                             sto:Purchase_Cost, | ! Purchase_Cost
                                             sto:Sale_Cost, | ! Sale_Cost
                                             sto:Retail_Cost, | ! Retail_Cost
                                             'STOCK RECREDITED', | ! Notes
                                             'SALE CANCELLED') ! Information
                            ! Added OK
                          Else ! AddToStockHistory
                            ! Error
                          End ! AddToStockHistory
                      End!If access:stock.fetch(sto:ref_number_key) = Level:Benign
                  End!If access:part_ref_number <> ''
              End!If partmp:pending_ref_number <> ''
          End!Loop x# = 1 To Records(parts_queue_temp)
          access:stock.close()
          access:stohist.close()
          access:ordpend.close()
      End!If thiswindow.request   = Insertrecord
  End!If thiswindow.response  = 2
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(PrimeFields, ())
    ret:date_booked = Today()
    ret:time_booked = Clock()
  PARENT.PrimeFields
  access:retsales.primerecord()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(PrimeFields, ())


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickSubAccounts
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      !Save Fields In Case Of Cancel
      sav:ref_number  = ret:ref_number
      
      If thiswindow.request   = Insertrecord
          Clear(parts_queue_temp)
          Free(parts_queue_temp)
          save_res_id = access:retstock.savefile()
          access:retstock.clearkey(res:part_number_key)
          res:ref_number  = ret:ref_number
          set(res:part_number_key,res:part_number_key)
          loop
              if access:retstock.next()
                 break
              end !if
              if res:ref_number  <> ret:ref_number      |
                  then break.  ! end if
              IF (res:Despatched = 'PEN')
                  ! #11953 Don't include pending items (i.e. not taken from stock) (Bryan: 15/02/2011)
                  CYCLE
              END
              partmp:part_ref_number  = res:part_ref_number
              partmp:quantity        = res:quantity
              partmp:pending_ref_number   = res:pending_ref_number
              Add(Parts_queue_temp)
          end !loop
          access:retstock.restorefile(save_res_id)
      End!If thiswindow.request   = Insertrecord
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020338'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020338'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020338'&'0')
      ***
    OF ?ret:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Account_Number, Accepted)
      IF ret:Account_Number OR ?ret:Account_Number{Prop:Req}
        sub:Account_Number = ret:Account_Number
        !Save Lookup Field Incase Of error
        look:ret:Account_Number        = ret:Account_Number
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            ret:Account_Number = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            ret:Account_Number = look:ret:Account_Number
            SELECT(?ret:Account_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
          If ret:account_number <> ''
              stop# = 0
      
              !Check if the selected sub account is on stop. - 4489 (DBH: 20-07-2004  36)
              Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
              sub:Account_Number  = ret:Account_Number
              If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                  !Found
                  If sub:Stop_Account = 'YES'
                      stop# = 1
                  End !If sub:Stop_Account = 'YES'
              Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
              !Check if the Head Account is on stop - 4489 (DBH: 20-07-2004  36)
              access:tradeacc.clearkey(tra:account_number_key)
              tra:account_number = sub:main_account_number
              if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                  If tra:stop_account = 'YES'
                      stop# = 1
                  End!If tra:stop_account = 'YES'
              End!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
      
              If stop# = 1
                  Case Missive('The selected Trade Account is on STOP.'&|
                    '<13,10>'&|
                    '<13,10>A new transaction cannot be created for this customer.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  ret:account_number  = ''
              Else!If stop# = 1
                  if tra:invoice_sub_accounts = 'YES'
                      If SUB:Retail_Payment_Type = 'ACC'
                          ret:payment_method  = 'TRA'
                      End!If SUB:Retail_Payment_Type = 'ACC'
                      IF sub:retail_payment_type  = 'CAS'
                          ret:payment_method = 'CAS'
                      End!IF sub:retail_payment_type  = 'CAS'
      
                      If sub:use_customer_address <> 'YES'
                          RET:Postcode        = sub:postcode
                          RET:Company_Name    = sub:company_name
                          RET:Building_Name   = ''
                          RET:Address_Line1   = sub:address_line1
                          RET:Address_Line2   = sub:address_line2
                          RET:Address_Line3   = sub:address_line3
                          RET:Telephone_Number= sub:telephone_number
                          RET:Fax_Number      = sub:fax_number
                      End!If sub:use_customer_address <> 'YES'
                      RET:Courier_Cost = sub:Courier_Cost
      
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = sub:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = sub:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                  else!if tra:use_sub_accounts = 'YES'
                      If TRA:Retail_Payment_Type = 'ACC'
                          ret:payment_method  = 'TRA'
                      End!If SUB:Retail_Payment_Type = 'ACC'
                      IF TRA:retail_payment_type  = 'CAS'
                          ret:payment_method = 'CAS'
                      End!IF sub:retail_payment_type  = 'CAS'
      
                      RET:Courier_Cost = TRA:Courier_Cost
                      If tra:use_customer_address <> 'YES'
                          RET:Postcode        = tra:postcode
                          RET:Company_Name    = tra:company_name
                          RET:Building_Name   = ''
                          RET:Address_Line1   = tra:address_line1
                          RET:Address_Line2   = tra:address_line2
                          RET:Address_Line3   = tra:address_line3
                          RET:Telephone_Number= tra:telephone_number
                          RET:Fax_Number      = tra:fax_number
                      End!If tra:use_customer_address <> 'YES'
      
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = tra:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = tra:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                  end!if tra:use_sub_accounts = 'YES'
      
                  If tra:use_sub_accounts <> 'YES'
                      If tra:use_delivery_address = 'YES'
                          RET:Postcode_Delivery       = tra:postcode
                          RET:Company_Name_Delivery   = tra:company_name
                          RET:Building_Name_Delivery  = ''
                          RET:Address_Line1_Delivery  = tra:address_line1
                          RET:Address_Line2_Delivery  = tra:address_line2
                          RET:Address_Line3_Delivery  = tra:address_line3
                          RET:Telephone_Delivery      = tra:telephone_number
                          RET:Fax_Number_Delivery     = tra:fax_number
                      End !If tra:use_delivery_address = 'YES'
                      ret:courier = tra:courier_outgoing
      
                  Else!If tra:use_sub_accounts <> 'YES'
      
                      If sub:use_delivery_address = 'YES'
                          RET:Postcode_delivery        = sub:postcode
                          RET:Company_Name_delivery    = sub:company_name
                          RET:Building_Name_delivery   = ''
                          RET:Address_Line1_delivery   = sub:address_line1
                          RET:Address_Line2_delivery   = sub:address_line2
                          RET:Address_Line3_delivery   = sub:address_line3
                          RET:Telephone_delivery= sub:telephone_number
                          RET:Fax_Number_delivery      = sub:fax_number
                      End
                      ret:courier = sub:courier_outgoing
      
                  End!If tra:use_sub_accounts <> 'YES'
      
                  payment_method_temp = ret:payment_method
              End!If stop# = 1
              post(event:accepted,?ret:payment_method)
          End!If ret:account_number <> ''
      
          Display()
      End!If ~0{prop:acceptall}
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Account_Number, Accepted)
    OF ?LookupAccountNumber
      ThisWindow.Update
      sub:Account_Number = ret:Account_Number
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          ret:Account_Number = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?ret:Account_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?ret:Account_Number)
    OF ?ret:Purchase_Order_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Purchase_Order_Number, Accepted)
      !Neil 10/7/2001!
      !Routine to check if PO used before by this customer!
      
      !IF ret:Purchase_Order_Number <> ''    !make sure not blank!!!
      !  Access:RETSALES_ALIAS.ClearKey(res_ali:Despatched_Account_Key)
      !  res_ali:Despatched = 'YES'
      !  res_ali:Account_Number = ret:Account_Number
      !  res_ali:Purchase_Order_Number = ret:Purchase_Order_Number
      !  IF Access:RETSALES_ALIAS.Fetch(res_ali:Despatched_Account_Key)
      !    !Error - No match - Allow PO!
      !  ELSE
      !    Case MessageEx('This purchase order number has been used previously by this company, would you like to continue booking this retail sale?','ServiceBase 2000',|
      !                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      !      Of 1 ! &Yes Button
      !
      !      Of 2 ! &No Button
      !        ret:Purchase_Order_Number = ''
      !        SELECT(?ret:Purchase_Order_Number)
      !        CYCLE
      !    End!Case MessageEx
      !  END
      !END
      
      !Don't understand the above code, so here's mine
      If ~0{prop:AcceptAll}
          If ret:Purchase_Order_Number <> ''
              Save_res_ali_ID = Access:RETSALES_ALIAS.SaveFile()
              Access:RETSALES_ALIAS.ClearKey(res_ali:Purchase_Number_Key)
              res_ali:Purchase_Order_Number = ret:Purchase_Order_Number
              Set(res_ali:Purchase_Number_Key,res_ali:Purchase_Number_Key)
              Loop
                  If Access:RETSALES_ALIAS.NEXT()
                     Break
                  End !If
                  If res_ali:Purchase_Order_Number <> ret:Purchase_Order_Number      |
                      Then Break.  ! End If
                  If res_ali:Ref_Number <> ret:Ref_Number
                      DuplicatePurchaseOrderList(ret:Purchase_Order_Number)
                      Break
                  End !If res_ali:Ref_Number <> ret:Ref_Number
              End !Loop
              Access:RETSALES_ALIAS.RestoreFile(Save_res_ali_ID)
          End !If ret:Purchase_Order_Number
      End !0{prop:AcceptAll}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Purchase_Order_Number, Accepted)
    OF ?ret:Delivery_Collection
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Delivery_Collection, Accepted)
      If ret:delivery_collection <> ''
          Unhide(?ret:courier_cost)
          Unhide(?ret:courier_cost:prompt)
          Case ret:delivery_collection
              Of 'COL'
                  Disable(?Delivery_Sheet)
              Of 'DEL'
                  Enable(?Delivery_Sheet)
          End!Case ret:delivery_collection
      Else!If ret:delivery_collection <> ''
              Hide(?ret:courier_Cost)
              Hide(?ret:courier_cost:prompt)
      End!Case ret:delivery_collection
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Delivery_Collection, Accepted)
    OF ?ret:Payment_Method
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Payment_Method, Accepted)
      If  ~0{prop:acceptall}
          If ret:payment_method   <> payment_method_temp
              If payment_method_temp <> ''
                  If ret:Payment_Method = 'EXC'
                      Case Missive('Exchange Accessory.'&|
                        '<13,10>'&|
                        '<13,10>All parts on this sale transaction will be zero valued.'&|
                        '<13,10>Are you sure?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                          Of 1 ! No Button
                              ret:Payment_Method  = Payment_Method_Temp
                      End ! Case Missive
                  Else !If ret:Payment_Method = 'EXC'
                      account# = 0
                      cash# = 0
                      access:subtracc.clearkey(sub:account_number_key)                                !Check the account to see if
                      sub:account_number = ret:account_number                                         !it should be cash, or account
                      if access:subtracc.fetch(sub:account_number_key) = level:benign
                          access:tradeacc.clearkey(tra:account_number_key)
                          tra:account_number = sub:main_account_number
                          if access:tradeacc.fetch(tra:account_number_key) = level:benign
                              if tra:invoice_sub_accounts = 'YES'
                                  IF sub:retail_payment_type = 'ACC'
                                      account#    = 1
                                  End!IF sub:payment_type = 'ACC'
                                  IF sub:retail_payment_Type = 'CAS'
                                      cash#   = 1
                                  End!IF sub:payment_Type = 'CAS'
                              else!if tra:use_sub_accounts = 'YES'
                                  If tra:retail_payment_type = 'ACC'
                                      account#    = 1
                                  End!If tra:retail_payment_type = 'ACC'
                                  If tra:retail_payment_type  = 'CAS'
                                      cash# = 1
                                  End!If tra:retail_payment_type  = 'CAS'
      
                              end!if tra:use_sub_accounts = 'YES'
                          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                      end!if access:subtracc.fetch(sub:account_number_key) = level:benign
      
                      Case ret:payment_method                                                         !Check what the payment method has
                          Of 'TRA'                                                                    !been changed to.
                              If cash# = 1
                                  Case Missive('The selected Trade Account can only be used for a Cash Sale.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  ret:payment_method  = payment_method_temp
                              Else
                                  Case Missive('Are you sure you want to change the Payment Method to "Account"?','ServiceBase 3g',|
                                                 'mquest.jpg','\No|/Yes')
                                      Of 2 ! Yes Button
                                              payment_method_temp = ret:payment_method
                                      Of 1 ! No Button
                                              ret:payment_method  = payment_method_temp
                                  End ! Case Missive
      
                              End!If cash# = 1
                          Of 'CAS'
                              If account# = 1
                                  Case Missive('The selected Trade Account has not been setup as a Cash Account. '&|
                                    '<13,10>'&|
                                    '<13,10>Are you sure you want to change the Payment Method to "Cash"?','ServiceBase 3g',|
                                                 'mquest.jpg','\No|/Yes')
                                      Of 2 ! Yes Button
                                              payment_method_temp = ret:payment_method
                                      Of 1 ! No Button
                                              ret:payment_method  = payment_method_temp
                                  End ! Case Missive
                              Else!If account# = 1
                                  payment_method_temp = ret:payment_method
                              End!If account# = 1
                      End!Case ret:payment_method
                  End !If ret:Payment_Method = 'EXC'
      
              Else!If payment_method_temp <> ''
                  If ret:Payment_Method = 'EXC'
                      Case Missive('Exchange Accessory.'&|
                        '<13,10>'&|
                        '<13,10>All parts on this sale transaction will be zero valued.'&|
                        '<13,10>Are you sure?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              payment_method_temp = ret:payment_method
                          Of 1 ! No Button
                              ret:Payment_Method  = Payment_Method_Temp
                      End ! Case Missive
                  End!If ret:Payment_Method = 'EXC'
              End!If payment_method_temp <> ''
      
          End!If ret:payment_method   <> payment_method_temp
          If ret:payment_method = 'CAS'
              Unhide(?Payment)
          Else!If ret:payment_method = 'CAS'
              Hide(?Payment)
          End!If ret:payment_method = 'CAS'
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Payment_Method, Accepted)
    OF ?Replicate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate, Accepted)
      RET:Postcode_Delivery=RET:Postcode
      RET:Company_Name_Delivery=RET:Company_Name
      RET:Building_Name_Delivery=RET:Building_Name
      RET:Address_Line1_Delivery=RET:Address_Line1
      RET:Address_Line2_Delivery=RET:Address_Line2
      RET:Address_Line3_Delivery=RET:Address_Line3
      RET:Telephone_Delivery=RET:Telephone_Number
      RET:Fax_Number_Delivery=RET:Fax_Number
      DIsplay()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate, Accepted)
    OF ?StockEnquiry
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockEnquiry, Accepted)
      Do StockEnquiry
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockEnquiry, Accepted)
    OF ?Payment
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payment, Accepted)
      error# = 0
      If error# = 0
          Browse_Retail_payments (ret:ref_number,ret:account_number,ret:courier_cost,'PIK')
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payment, Accepted)
    OF ?WebStockRequests
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?WebStockRequests, Accepted)
      Do StockEnquiry
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?WebStockRequests, Accepted)
    OF ?Delivery_Text
      ThisWindow.Update
      Delivery_Text
      ThisWindow.Reset
    OF ?Invoice_Text
      ThisWindow.Update
      Invoice_Text
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Error Checking
  IF ret:Delivery_Collection = ''
      Case Missive('You must select Delivery or Collection.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Select(?ret:Delivery_Collection)
      Cycle
  End!IF ret:Delivery_Collection = ''
  
  If ret:delivery_collection  = 'DEL'
      If ret:Address_Line1 = ''
          Case Missive('This sale has been marked as a Delivery.'&|
            '<13,10>'&|
            '<13,10>You must enter a Delivery Address.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End!If RET:Address_Delivery_Group = ''
  End!If ret:delivery_collection  = 'DEL'
  
  count# = 0
  stock_amount$   = 0
  setcursor(cursor:wait)                                                                      !Has any stock been picked?
  save_res_id = access:retstock.savefile()
  access:retstock.clearkey(res:part_number_key)
  res:ref_number  = ret:ref_number
  set(res:part_number_key,res:part_number_key)
  loop
      if access:retstock.next()
         break
      end !if
      if res:ref_number  <> ret:ref_number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      count# += 1
      If res:Despatched = 'PIK'
          stock_amount$ += Round(res:quantity * res:item_cost,.01)
      End !If res:Despatch = 'PIK'
  
  end !loop
  access:retstock.restorefile(save_res_id)
  setcursor()
  
  If count# = 0
      Case Missive('You must pick Stock before you can complete a transaction.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Cycle
  Else!If count# = 0
      If ret:payment_method = 'CAS'
  
          access:subtracc.clearkey(sub:account_number_key)
          sub:account_number = ret:account_number
          if access:subtracc.fetch(sub:account_number_key) = Level:benign
              stop# = 0
              access:tradeacc.clearkey(tra:account_number_key)
              tra:account_number = sub:main_account_number
              if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                  if tra:invoice_sub_accounts = 'YES'
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = sub:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                  else!if tra:use_sub_accounts = 'YES'
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = tra:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                  end!if tra:use_sub_accounts = 'YES'
              End!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
          End!if access:subtracc.fetch(sub:account_number_key)
  
  
          payment_amount$ = 0
          setcursor(cursor:wait)                                                                  !Has the stock been paid for
          save_rtp_id = access:retpay.savefile()
          access:retpay.clearkey(rtp:date_key)
          rtp:ref_number   = ret:ref_number
          set(rtp:date_key,rtp:date_key)
          loop
              if access:retpay.next()
                 break
              end !if
              if rtp:ref_number   <> ret:ref_number      |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              payment_amount$ += rtp:amount
          end !loop
          access:retpay.restorefile(save_rtp_id)
          setcursor()
  
          ret:sub_total   = Round(stock_amount$ + ret:courier_cost,.01)
          vat$            = Round(stock_amount$ * vat_rate_temp/100,.01) + Round(ret:courier_cost * vat_rate_temp/100,.01)
          total$          = Round(ret:sub_total + vat$,.01)
  
          If total$ - payment_amount$ > .01
              Case Missive('A Balance of '&Format(Round(total$ - payment_amount$,.01),@n14.2)&' is still outstanding for this Sale.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End!If stock_amount$ + ret:courier_cost <> payment_amount$
  
      End!If ret:payment_method = 'CAS'
  
  End!If count# = 0
  If func:OrderNumber <> 0
      Access:ordhead.ClearKey(orh:KeyOrder_no)
      orh:Order_no = func:OrderNumber
      If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
          !Found
          orh:SalesNumber = ret:Ref_Number
          orh:procesed = 1
          orh:pro_date = Today()
          Access:ORDHEAD.TryUpdate()
      Else!If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
  End !func:OrderNumber <> 0
  ReturnValue = PARENT.TakeCompleted()
  glo:Select1 = ret:Ref_Number
  ! Inserting (DBH 20/10/2006) # 8381 - Print 2 copies
  glo:ReportCopies = 2
  ! End (DBH 20/10/2006) #8381
  Retail_Picking_Note('PIK')
  glo:Select1 = ''
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?ret:Account_Number
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Account_Number, AlertKey)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Account_Number, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      !Save Fields
      payment_method_temp = ret:payment_method
      If thiswindow.request   = Insertrecord
          ret:delivery_collection = 'DEL'
      End!If thiswindow.request   = Insertrecord
      Post(Event:accepted,?ret:delivery_collection)
      Post(Event:accepted,?ret:payment_method)
      
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Invoice_Text PROCEDURE                                !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Invoice Text'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Invoice Text'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                         END
                       END
                       TEXT,AT(232,162,216,80),USE(ret:Invoice_Text),VSCROLL,FONT(,,,FONT:bold),COLOR(COLOR:White),UPR
                       BUTTON,AT(448,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020349'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Invoice_Text')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:RETSALES.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Invoice_Text')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Invoice_Text')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020349'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020349'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020349'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Process_Retail_Stock PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
Quantity_temp        REAL
ActionMessage        CSTRING(40)
quantity_required_temp REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::res:Record  LIKE(res:RECORD),STATIC
QuickWindow          WINDOW('Update the RETSTOCK File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Stock Item'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Item Details'),USE(?Tab1)
                           PROMPT('Part Number'),AT(234,160),USE(?RES:Part_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(322,160,124,10),USE(res:Part_Number),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Description'),AT(234,180),USE(?RES:Description:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(322,180,124,10),USE(res:Description),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Quantity In Stock'),AT(234,200),USE(?STO:Quantity_Stock:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(322,200,64,10),USE(sto:Quantity_Stock),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Quantity Required'),AT(234,220),USE(?Prompt4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(322,220,64,10),USE(quantity_required_temp),SKIP,RIGHT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Quantity To Sell'),AT(234,240),USE(?RES:Quantity:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(322,240,64,10),USE(res:Quantity),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,RANGE(1,999999),STEP(1)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020336'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Process_Retail_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(res:Record,History::res:Record)
  SELF.AddHistoryField(?res:Part_Number,3)
  SELF.AddHistoryField(?res:Description,4)
  SELF.AddHistoryField(?res:Quantity,11)
  SELF.AddUpdateFile(Access:RETSTOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RETSTOCK.Open
  Relate:STOCK.Open
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  access:stock.clearkey(sto:ref_number_key)
  sto:ref_number = res:part_ref_number
  if access:stock.tryfetch(sto:ref_number_key)
      Return(Level:Fatal)
  end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
  quantity_required_temp  = res:quantity
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Process_Retail_Stock')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSTOCK.Close
    Relate:STOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Process_Retail_Stock')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020336'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020336'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020336'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  error# = 0
  If quantity_required_temp <> res:quantity
      Case Missive('Are you sure you want to change the quantity?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
          Of 1 ! No Button
                  error# = 1
              res:quantity    = quantity_required_temp
          End!Case MessageEx
  
  End!If quantity_required_temp <> res:quantity
  If error# = 0
      If res:quantity <= sto:quantity_stock
          res:date_ordered    = Today()
          res:date_received   = Today()
          res:despatched = 'YES'
  
          sto:quantity_stock -= res:quantity
          access:stock.update()
          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                             'DEC', | ! Transaction_Type
                             '', | ! Depatch_Note_Number
                             0, | ! Job_Number
                             ret:Ref_Number, | ! Sales_Number
                             res:Quantity, | ! Quantity
                             res:Purchase_Cost, | ! Purchase_Cost
                             res:Sale_Cost, | ! Sale_Cost
                             res:Retail_Cost, | ! Retail_Cost
                             'RETAIL ITEM SOLD', | ! Notes
                             '') ! Information
            ! Added OK
  
          Else ! AddToStockHistory
            ! Error
          End ! AddToStockHistory
      End!If res:quantity < sto:quantity_stock
  End!If error# = 0
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Update_Retail_Stock PROCEDURE (func:RecordNumber,func:Type) !Generated from procedure template - Window

CurrentTab           STRING(80)
save_sup_id          USHORT,AUTO
save_retstock_alias_id USHORT,AUTO
EuroRate             REAL
Use_Euro             BYTE
quantity_temp        LONG
save_ope_id          USHORT,AUTO
save_orp_id          USHORT,AUTO
vat_rate_temp        REAL
ActionMessage        CSTRING(40)
cost_temp            REAL
Item_Total_Temp      REAL
Quantity_Stock_Temp  LONG
Quantity_To_Order_Temp LONG
Quantity_On_Order_Temp REAL
Quantity_Required_Temp LONG
Euro_Item            REAL
Euro_Total           REAL
tmp:TakeFromStock    BYTE(0)
tmp:QuantityOnBackOrder LONG
History::res:Record  LIKE(res:RECORD),STATIC
QuickWindow          WINDOW('Update the RETSTOCK File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Retail Stock Item'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Part Number'),AT(210,114),USE(?Part_Number),FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s25),AT(298,114),USE(res:Part_Number),FONT(,12,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Description'),AT(210,128),USE(?Description),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(298,128),USE(res:Description),FONT(,10,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(298,140),USE(res:Supplier),FONT(,10,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Quantity In Stock'),AT(210,156),USE(?Description:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@N8),AT(298,156,60,8),USE(Quantity_Stock_Temp),RIGHT,FONT(,10,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@N8),AT(298,168,60,8),USE(Quantity_To_Order_Temp),RIGHT,FONT(,10,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(444,172),USE(?Button4),TRN,FLAT,ICON('backdetp.jpg')
                           PROMPT('Quantity To Order'),AT(210,168),USE(?Description:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Quantity On Order'),AT(210,180),USE(?Description:5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@N8),AT(298,180,60,8),USE(Quantity_On_Order_Temp),RIGHT,FONT(,10,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@n8),AT(298,192,60,8),USE(sto:Minimum_Level),RIGHT,FONT(,10,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Total Qty On Back Order'),AT(210,204),USE(?Prompt18),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n8),AT(298,204,60,8),USE(tmp:QuantityOnBackOrder),TRN,RIGHT,FONT(,10,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Accesory Cost'),AT(210,222),USE(?res:AccessoryCost:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(306,222,64,10),USE(res:AccessoryCost),HIDE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Accesory Cost'),TIP('Accesory Cost'),UPR
                           PROMPT('Minimum Stock Level'),AT(210,192),USE(?Description:6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Item Cost'),AT(210,238),USE(?Item_Cost),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(306,238,64,10),USE(res:Item_Cost),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('<128>'),AT(390,238),USE(?Euro_Item:Prompt),HIDE
                           ENTRY(@n10.2),AT(398,238,64,10),USE(Euro_Item),HIDE,DECIMAL(14),FONT(,,,FONT:bold)
                           PROMPT('<128>'),AT(390,222),USE(?Euro_Item:Prompt:2),HIDE
                           ENTRY(@n10.2),AT(398,222,64,10),USE(Euro_Item,,?Euro_Item:2),HIDE,DECIMAL(14),FONT(,,,FONT:bold)
                           PROMPT('Quantity Required'),AT(210,254),USE(?Quantity_Required_Temp:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(306,254,64,10),USE(Quantity_Required_Temp),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,RANGE(1,999999),STEP(1)
                           PROMPT('Supplier'),AT(210,140),USE(?Description:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Quantity To Ship'),AT(210,270),USE(?RES:Quantity:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(306,270,64,10),USE(res:Quantity),RIGHT,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR,RANGE(0,9999999),STEP(1)
                           PROMPT('<128>'),AT(390,286),USE(?Euro_Total:Prompt),HIDE
                           ENTRY(@n10.2),AT(398,286,64,10),USE(Euro_Total),HIDE,DECIMAL(14),FONT(,,,FONT:bold)
                           PROMPT('Item Total (Shipped)'),AT(210,286),USE(?Item_Total_Temp:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(306,286,64,10),USE(Item_Total_Temp),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),READONLY
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    Map
LocalCreatePendingPart          Procedure(Long  func:Quantity)
    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Item_Total      Routine
    If ?res:AccessoryCost{prop:Hide} = 1
        item_total_temp = res:item_cost * res:quantity
        IF Use_Euro = TRUE
            Euro_Item = res:item_cost * EuroRate
            Euro_Total = (res:item_cost * EuroRate) * res:quantity
        END
    Else !If ?res:AccessoryCost{prop:Hide} = 1
        item_total_temp = res:AccessoryCost * res:quantity
        IF Use_Euro = TRUE
            Euro_Item = res:AccessoryCost * EuroRate
            Euro_Total = (res:AccessoryCost * EuroRate) * res:quantity
        END
    End !If ?res:AccessoryCost{prop:Hide} = 1
    Display(?item_total_temp)
    DISPLAY()
Check_Fields    ROUTINE
    IF Use_Euro = TRUE
        Case ?res:AccessoryCost{prop:Hide}
            Of 0
                ?Euro_Item:2{PROP:Hide} = FALSE
                ?Euro_Item:Prompt:2{PROP:Hide} = FALSE
            Of 1
                ?Euro_Item{PROP:Hide} = FALSE
                ?Euro_Item:Prompt{PROP:Hide} = FALSE
        End !Case ?res:AccessoryCost{prop:Hide}
        ?Euro_Total{PROP:Hide} = FALSE
        ?Euro_Total:Prompt{PROP:Hide} = FALSE

    END
    DISPLAY()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'Viewing A Retail Item'
  OF InsertRecord
    ActionMessage = 'Insert A Retail Item'
  OF ChangeRecord
    ActionMessage = 'Changing A Retail Item'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020339'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Retail_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(res:Record,History::res:Record)
  SELF.AddHistoryField(?res:Part_Number,3)
  SELF.AddHistoryField(?res:Description,4)
  SELF.AddHistoryField(?res:Supplier,5)
  SELF.AddHistoryField(?res:AccessoryCost,9)
  SELF.AddHistoryField(?res:Item_Cost,10)
  SELF.AddHistoryField(?res:Quantity,11)
  SELF.AddUpdateFile(Access:RETSTOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:ORDITEMS.Open
  Relate:RETSALES.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:VATCODE.Open
  Access:STOCK.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:ORDPEND.UseFile
  Access:ORDPARTS.UseFile
  Access:LOCATION.UseFile
  Access:SUPPLIER.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Update_Retail_Stock')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  If thiswindow.request   = Insertrecord
      !If func:RecordNumber <> 0 then this is adding stock
      !that has been requested from a Web Order.
      !It uses the Record Number to get the original Web Order request
      !and fill in the part screen as necesary.
  
      If func:RecordNumber <> 0
          Access:ORDITEMS.ClearKey(ori:RecordNumberKey)
          ori:RecordNumber = func:RecordNumber
          If Access:ORDITEMS.TryFetch(ori:RecordNumberKey) = Level:Benign
              !Found
              Access:STOCK.ClearKey(sto:Location_Key)
              sto:Location    = MainStoreLocation()
              sto:Part_Number = ori:PartNo
              If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  !Found
                  Quantity_Required_Temp  = ori:Qty
                  !Used for the check at completion
                  glo:Select1 = sto:Ref_Number
              Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
          Else!If Access:ORDITEMS.TryFetch(ori:RecordNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:ORDITEMS.TryFetch(ori:RecordNumberKey) = Level:Benign
      Else !If func:RecordNumber <> 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = glo:select1
          if access:stock.fetch(sto:ref_number_key)
              Return(Level:Fatal)
          End!if access:stock.fetch(sto:ref_number_key)
  
      End !If func:RecordNumber <> 0
  
      res:Part_Number   = sto:Part_Number
      res:Description   = sto:Description
      res:Supplier      = sto:Supplier
      res:Purchase_Cost = sto:Purchase_Cost
      res:Sale_Cost     = sto:Sale_Cost
      res:Retail_Cost   = sto:Retail_Cost
      res:AccessoryCost   = sto:AccessoryCost
  
      RES:Part_Ref_Number = sto:ref_number
  Else!!If thiswindow.request   = Insertrecord
      !Amending an existing part. They cannot change the quantity required.
      !Only the quantity to ship can now be changed.
  
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number  = res:part_ref_number
      access:stock.fetch(sto:ref_number_key)
      Hide(?Quantity_Required_Temp)
      Hide(?Quantity_Required_Temp:prompt)
  End!If thiswindow.request   = Insertrecord
  
  Use_Euro = FALSE
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  
  !Check to see if the Retail Sales has been invoiced.
  !If so you the Invoice's Euro Rate, otherwise use the Default Rate
  IF ret:Invoice_Number <> ''
    Access:invoice.ClearKey(inv:Invoice_Number_Key)
    inv:Invoice_Number = ret:Invoice_Number
    IF Access:Invoice.Fetch(inv:Invoice_Number_Key)
      EuroRate = def:EuroRate
    ELSE
      EuroRate = inv:EuroExhangeRate
    END
  ELSE
    EuroRate = def:EuroRate
  END
  
  !Exchange Sales do not have a price.
  If ret:Payment_Method = 'EXC'
      ?res:Item_Cost{prop:Hide} = 1
      ?Item_Cost{prop:Hide} = 1
      ?res:AccessoryCost{prop:Hide} = 0
      ?res:AccessoryCost:Prompt{prop:Hide} = 0
      res:item_cost = 0
      Disable(?res:Item_Cost)
  Else !ret:Payment_Method = 'EXC'
  
      !Get the Trade Account from the Sale to determine the price
      !of the parts
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = ret:Account_Number
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Found
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = sub:Main_Account_Number
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              If tra:RetailZeroParts = 1
                  res:Item_Cost = 0
              Else !If tra:RetailZeroParts = 1
                  !Check the Trade Account Retail Price Structure
                  !Only applies if it is a new record
                  !'RET' = Retail Price
                  !Other = Trade Price
                  If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                      Case sub:Retail_Price_Structure
                          Of 'RET'
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Retail_Cost
                              End !If ThisWindow.Request = InsertRecord
                              ?Item_Cost{prop:Text} = 'Retail Price'
                          Of 'PUR'
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Purchase_Cost
                              End !If ThisWindow.Request = InsertRecord
                              ?Item_Cost{prop:Text} = 'Purchase Price'
                          Else
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Sale_Cost
                              End !If ThisWindows.Request = InsertRecord
                              ?Item_Cost{prop:Text} = 'Trade Price'
                      End !Case sub:Retail_Price_Structure
  
                      !Check whether Sub Account uses the Euro.
                      !This will change the screen displays to reflect this
                      If sub:EuroApplies = 1
                          use_Euro    = 1
                      End !If sub:EuroApplied = 1
  
                  Else !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                      Case tra:Retail_Price_Structure
                          Of 'RET'
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Retail_Cost
                              End !If ThisWindow.Reqest = InsertRecord
                              ?Item_Cost{prop:Text} = 'Retail Price'
                          Of 'PUR'
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Purchase_Cost
                              End !If ThisWindow.Request = InsertRecord
                              ?Item_Cost{prop:Text} = 'Purchase Price'
                          Else
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Sale_Cost
                              End !If ThisWindow.Request = InsertRecord
                              ?Item_Cost{prop:Text} = 'Trade Price'
                      End !Case tra:Retail_Price_Structure
  
                      If tra:EuroApplies = 1
                          Use_Euro = 1
                      End !If tra:EuroApplies = 1
                  End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
              End !If tra:RetailZeroParts = 1
  
          Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  End !If ret:Payment_Method = 'EXC'
  
  Do Check_Fields
  Do item_total
  
  !Work How Parts Pending Orders There Are For This Part Number
  
  Setcursor(Cursor:Wait)
  Save_sup_ID = Access:SUPPLIER.SaveFile()
  Set(sup:Company_Name_Key)
  Loop
      If Access:SUPPLIER.NEXT()
         Break
      End !If
      Save_ope_ID = Access:ORDPEND.SaveFile()
      Access:ORDPEND.ClearKey(ope:Supplier_Key)
      ope:Supplier    = sup:Company_Name
      ope:Part_Number = res:Part_Number
      Set(ope:Supplier_Key,ope:Supplier_Key)
      Loop
          If Access:ORDPEND.NEXT()
             Break
          End !If
          If ope:Supplier    <> sup:Company_Name      |
          Or ope:Part_Number <> res:Part_Number      |
              Then Break.  ! End If
          quantity_to_order_temp += ope:quantity
      End !Loop
      Access:ORDPEND.RestoreFile(Save_ope_ID)
  End !Loop
  Access:SUPPLIER.RestoreFile(Save_sup_ID)
  Setcursor()
  
  !Work out how many parts have been ordered but not received
  
  Setcursor(Cursor:Wait)
  Save_orp_ID = Access:ORDPARTS.SaveFile()
  Access:ORDPARTS.ClearKey(orp:Received_Part_Number_Key)
  orp:All_Received = 'NO'
  orp:Part_Number  = res:Part_Number
  Set(orp:Received_Part_Number_Key,orp:Received_Part_Number_Key)
  Loop
      If Access:ORDPARTS.NEXT()
         Break
      End !If
      If orp:All_Received <> 'NO'      |
      Or orp:Part_Number  <> res:Part_Number      |
          Then Break.  ! End If
      !Double check back to the Order File to see if it really hasn't all been received.
      Access:ORDERS.Clearkey(ord:Order_Number_Key)
      ord:Order_Number    = orp:Order_Number
      If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
          !Found
          If ord:All_Received <> 'YES'
              Quantity_On_Order_Temp += orp:Quantity
          End !If ord:All_Received <> 'YES'
      Else! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
  
  End !Loop
  Access:ORDPARTS.RestoreFile(Save_orp_ID)
  Setcursor()
  
  IF quantity_to_order_temp < 0
      quantity_to_order_temp = 0
  End!IF sto:quantity_to_order < 0
  If quantity_on_order_temp < 0
      quantity_on_order_temp = 0
  End!If sto:quantity_on_order < 0
  
  quantity_stock_temp   = sto:quantity_stock
  
  ! Inserting (DBH 22/08/2006) # 7963 - Calculate the amount on back order
  tmp:QuantityOnBackOrder = 0
  Save_RETSTOCK_ALIAS_ID = Access:RETSTOCK_ALIAS.SaveFile()
  Access:RETSTOCK_ALIAS.Clearkey(ret_ali:DespatchPartKey)
  ret_ali:Despatched = 'PEN'
  ret_ali:Part_Number = res:Part_Number
  Set(ret_ali:DespatchPartKey,ret_ali:DespatchPartKey)
  Loop ! Begin Loop
      If Access:RETSTOCK_ALIAS.Next()
          Break
      End ! If Access:RETSTOCK_ALIAS.Next()
      If ret_ali:Despatched <> 'PEN'
          Break
      End ! If ret_ali:Despatched <> 'PEN'
      If ret_ali:Part_Number <> res:Part_Number
          Break
      End ! If ret_ali:Part_Number <>
      tmp:QuantityOnBackOrder += ret_ali:Quantity
  End ! Loop
  Access:RETSTOCK_ALIAS.RestoreFile(Save_RETSTOCK_ALIAS_ID)
  ! End (DBH 22/08/2006) #7963
  !Hide/Show OK Button
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If ret:Invoice_Number <> 0 Or res:Despatched = 'CAN' !Or res:Despatched = 'YES'
      ?OK{prop:Hide} = 1
  End !res:Despatched = 'YES'
  IF SELF.Request = ViewRecord
    DISABLE(?ButtonHelp)
    ?res:Part_Number{PROP:ReadOnly} = True
    ?res:Description{PROP:ReadOnly} = True
    ?res:Supplier{PROP:ReadOnly} = True
    DISABLE(?Button4)
    ?res:AccessoryCost{PROP:ReadOnly} = True
    ?res:Item_Cost{PROP:ReadOnly} = True
    ?Euro_Item{PROP:ReadOnly} = True
    ?Euro_Item:2{PROP:ReadOnly} = True
    ?res:Quantity{PROP:ReadOnly} = True
    ?Euro_Total{PROP:ReadOnly} = True
    ?Item_Total_Temp{PROP:ReadOnly} = True
    HIDE(?OK)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:ORDITEMS.Close
    Relate:RETSALES.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:VATCODE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Retail_Stock')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    res:Purchase_Order_Number = ret:purchase_order_number
    Quantity_Required_Temp = 1
    res:Despatched = 'PIK'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020339'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020339'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020339'&'0')
      ***
    OF ?Button4
      ThisWindow.Update
      BackOrderViewer(res:Part_Number)
      ThisWindow.Reset
    OF ?res:AccessoryCost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:AccessoryCost, Accepted)
      Do item_total
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:AccessoryCost, Accepted)
    OF ?res:Item_Cost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Item_Cost, Accepted)
      Do item_total
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Item_Cost, Accepted)
    OF ?res:Quantity
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Quantity, Accepted)
      Do item_total
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Quantity, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !New Record
  glo:Select2 = ''
  glo:Select3 = ''
  glo:Select4 = ''
  glo:Select5 = ''
  glo:Select6 = ''
  glo:Select7 = ''
  Set(DEFAULTS)
  Access:DEFAULTS.Next()

  If thiswindow.request = Insertrecord
  !Reget Stock Record
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number = glo:select1
      access:stock.fetch(sto:ref_number_key)

      If quantity_required_temp < res:quantity
          Case Missive('The Quantity Required cannot be less than the Quantity To Ship.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End!If quantity_required_temp < res:quantity

      If res:quantity = 0
          Case Missive('You have selected not to ship any items.'&|
            '<13,10>'&|
            '<13,10>Are you sure?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
              Of 1 ! No Button
                  Cycle
          End ! Case Missive
      End!If res:quantity = 0

      If res:Quantity > sto:Quantity_Stock
          Case Missive('You cannot ship more items than there are in stock.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !If res:Quantity > sto:Quantity_Stock



      !Communicaid don't want to take anything from stock yet. They want to manually adjust the stock levels
      !So don't need Back Ordering

      !Neil 11/07/01
      !Show message if number to ship drops the level below the minimum!

      !Oops Neil. That is now how you do it!

  !    IF sto:Quantity_Stock+(sto:Quantity_To_Order+sto:Quantity_On_Order)-res:Quantity <= sto:Minimum_Level
  !      !Fallen below min level if items taken!
  !        Case MessageEx('The selling of this item will cause the stock level to fall below the minimum levels. '&|
  !        '<13,10>'&|
  !        '<13,10>This is an information only message.','ServiceBase 2000',|
  !                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,300)
  !          Of 1 ! &OK Button
  !        End!Case MessageEx
  !    END

      !If the res:Quantity = 0 it is filled in with the Required quantity.
      !Need the below flag to stop it then going on to take that quantity
      !from stock.

      tmp:TakeFromStock = 1

      If sto:sundry_item <> 'YES'        !If sundry, don't decrement
          !Take the "Quantity To Ship" from stock, and order the difference to "Quantity Required"

          If Quantity_Required_Temp > res:Quantity
              !You are only shipping some of the required, the rest will be ordered.
              !Am just create parts orders, the same as with Service Summary Ordering

                  If res:Quantity = 0
                      tmp:TakeFromStock = 0
                      !Make this part the "pending" part.
                      res:despatched = 'PEN'
                      res:date_ordered = ''
                      res:date_received = ''
                      res:Quantity    = Quantity_Required_Temp
! Deleting (DBH 23/08/2006) # 7963 - Do not automatically put this part on an order
!                      LocalCreatePendingPart(Quantity_Required_Temp)
!                      res:Pending_Ref_Number  = ope:Ref_Number
! End (DBH 23/08/2006) #7963
                  Else !If res:Quantity = 0
                      !Set the globals which will create a new part line
                      !for the "pending" part.

                      glo:select2  = 'NEW PENDING'
                      glo:select3  = sto:ref_number
                      glo:select4  = res:record_number
                      glo:select5  = quantity_required_temp - res:quantity
                      res:date_ordered    = Today()
                      res:date_received   = Today()
  !                        ope:quantity    = quantity_required_temp - res:quantity
  !                   MakePartsRequest(sto:Supplier,sto:Part_Number,sto:Description,Quantity_Required_Temp - res:Quantity)
! Deleting (DBH 23/08/2006) # 7963 - Do not automatically create an order
!                      LocalCreatePendingPart(Quantity_Required_Temp - res:Quantity)
! End (DBH 23/08/2006) #7963
                      glo:select6  = ope:ref_number

                      !Set the flag for the items to appear on the picking note.
                      res:Despatched = 'PIK'
                  End !If res:Quantity = 0
          End !If Quantity_Required_Temp > res:Quantity

          !Take the Quantity To Ship From Stock

          !Only take quantity from stock if this ISN'T a pending part.
          If sto:Quantity_Stock <> 0 and tmp:TakeFromStock
              sto:Quantity_stock  -= res:Quantity
              If sto:Quantity_Stock < 0
                  sto:Quantity_Stock = 0
              End !If sto:Quantity_Stock < 0
              If Access:STOCK.Update() = Level:Benign
! Changing (DBH 08/03/2006) #6971 - New procedure
!                   get(stohist,0)
!                   if access:stohist.primerecord() = level:benign
!                       shi:ref_number           = sto:ref_number
!                       access:users.clearkey(use:password_key)
!                       use:password              =glo:password
!                       access:users.fetch(use:password_key)
!                       shi:user                  = use:user_code
!                       shi:date                 = today()
!                       shi:transaction_type     = 'DEC'
!                       shi:despatch_note_number = ''
!                       shi:job_number           = ret:ref_number
!                       shi:quantity             = res:quantity
!                       shi:purchase_cost        = sto:purchase_cost
!                       shi:sale_cost            = sto:sale_cost
!                       shi:retail_cost          = sto:retail_cost
!                       shi:information          = 'COMPANY: ' & Clip(ret:company_name) & '<13,10>PURCHASE ORDER NO: ' & Clip(RET:Purchase_Order_Number) & |
!                                                   '<13,10>CONTACT: ' & CLip(RET:Contact_Name)
!                       shi:notes                = 'RETAIL ITEM SOLD'
!                       ! Inserting (DBH 07/03/2006) #6971 - Show current stock in history
!                       shi:StockOnHand          = sto:Quantity_Stock
!                       ! End (DBH 07/03/2006) #6971
!                       if access:stohist.insert()
!                          access:stohist.cancelautoinc()
!                       end
! to (DBH 08/03/2006) #6971
                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                     'DEC', | ! Transaction_Type
                                     '', | ! Depatch_Note_Number
                                     ret:Ref_Number, | ! Job_Number
                                     0, | ! Sales_Number
                                     res:Quantity, | ! Quantity
                                     res:Purchase_Cost, | ! Purchase_Cost
                                     res:Sale_Cost, | ! Sale_Cost
                                     res:Retail_Cost, | ! Retail_Cost
                                     'RETAIL ITEM SOLD', | ! Notes
                                     'COMPANY: ' & Clip(ret:company_name) & '<13,10>PURCHASE ORDER NO: ' & Clip(RET:Purchase_Order_Number) & |
                                                  '<13,10>CONTACT: ' & CLip(RET:Contact_Name)) ! Information
                    ! Added OK

                  Else ! AddToStockHistory
                    ! Error
                  End ! AddToStockHistory
               End !If Access:STOCK.Update() = Level:Benign
          End !If sto:Quantity_Stock <> 0


      End!If sto:sundry_item <> 'YES'
      If func:RecordNumber <> 0
          !Can't delete the parts, but there is no flag in the file to hide them

  !        Access:ORDITEMS.ClearKey(ori:recordnumberkey)
  !        ori:recordnumber = func:RecordNumber
  !        If Access:orditems.TryFetch(ori:recordnumberkey) = Level:Benign
  !            !Found
  !            Delete(ORDITEMS)
  !        Else!If Access:orditems.TryFetch(ori:recordnumberkey) = Level:Benign
  !            !Error
  !            !Assert(0,'<13,10>Fetch Error<13,10>')
  !        End!If Access:orditems.TryFetch(ori:recordnumberkey) = Level:Benign
      End !If func:RecordNumber <> 0
  End!If thiswindow.request = Insertrecord
  !Changing Record

  !Intial Stock Entry - func:Type = 0
  !Amending from Process Retail Sale - func:Type = 1

  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If ThisWindow.Request <> InsertRecord
  !Reget Stock Record
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number  = res:Part_Ref_Number
      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Found

          If res:Quantity <> Quantity_Temp
              !Ignore Sundry Items
              If sto:Sundry_Item <> 'YES'
                  If res:Despatched = 'PEN'
                      If res:Quantity > Quantity_Temp
                          !It doesn't really matter if they want to increase the quantity
                          !of a pending order. I either add some to any pending order I find,
                          !or create a new pending order if the order has already been raised.
                          Case Missive('You have increased the quantity of a part awaiting order.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to increase the quantity you wish to order?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
                                  !Need to see if there is a pending order
                                  !for this supplier and part number.

! Deleting (DBH 23/08/2006) # 7963 - A pending order will not have been created for this part
!                                  Access:ORDPEND.ClearKey(ope:Supplier_Key)
!                                  ope:Supplier    = sto:Supplier
!                                  ope:Part_Number = sto:Part_Number
!                                  If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
!                                      !Found
!                                      ope:Quantity += res:Quantity - Quantity_Temp
!                                      Access:ORDPEND.TryUpdate()
!                                  Else!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
!                                      !Error
!                                      !Assert(0,'<13,10>Fetch Error<13,10>')
!                                      !If an pending order doesn't actually exist. Although I'm not sure why,
!                                      !then ask if they want to create a new one. I don't think
!                                      !I can make it be any cleverer than that.
!                                      Case Missive('Error! Cannot find a Pending Back Order for this Part Number.'&|
!                                        '<13,10>'&|
!                                        '<13,10>Do you want to create a new Back Order for this part?','ServiceBase 3g',|
!                                                     'mquest.jpg','\No|/Yes')
!                                          Of 2 ! Yes Button
!                                              LocalCreatePendingPart(res:Quantity)
!                                          Of 1 ! No Button
!                                              res:Quantity = Quantity_Temp
!                                              Select(?res:Quantity)
!                                              Cycle
!                                      End ! Case Missive
!                                  End!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
! End (DBH 23/08/2006) #7963
                              Of 1 ! No Button
                                  res:Quantity = Quantity_Temp
                                  Select(?res:Quantity)
                                  Cycle
                          End ! Case Missive


                      Else !If res:Quantity > Quantity_Temp
                          !I should be able to make this more cleverer than the spec
                          !I can check to see if an pending order exists and adjust
                          !it's quantity. But if not, then I bring up the message
                          !and cancel the difference.

                          Case Missive('You have decreased the quantity of a part awaiting back order.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to decrease the quantity you wish to order?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
! Deleting (DBH 23/08/2006) # 7963 - A pending order will not have been created for this part
!                                  Access:ORDPEND.Clearkey(ope:Supplier_Key)
!                                  ope:Supplier    = sto:Supplier
!                                  ope:Part_Number = sto:Part_Number
!                                  If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
!                                      !Found
!                                      ope:Quantity -= (Quantity_Temp - res:Quantity)
!                                      If ope:Quantity <= 0
!                                          !If it falls less than zero, then someone has ordered too much
!                                          !Remove the pending order.
!                                          Delete(ORDPEND)
!                                      Else
!                                          Access:ORDPEND.TryUpdate()
!                                      End !If ope:Quantity <= 0
!                                  Else! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
!                                      !Error
!                                      !Assert(0,'<13,10>Fetch Error<13,10>')
!
!                                      !There is no point giving a yes/no because,
!                                      !they've already agreed to this course of action above.
!                                      Case Missive('Cannot find a Pending Back Order. The quantity may have already been ordered.'&|
!                                        '<13,10>'&|
!                                        '<13,10>The difference will not be cancelled from the sale.','ServiceBase 3g',|
!                                                     'mexclam.jpg','/OK')
!                                          Of 1 ! OK Button
!                                      End ! Case Missive
!
!                                      !Set globals to create a new line for
!                                      !the cancelled item
!                                      glo:select2  = 'CANCELLED'
!                                      glo:select3  = sto:ref_number
!                                      glo:select4  = res:record_number
!                                      glo:select5  = Quantity_Temp - res:Quantity
!                                      glo:select6  = ''
!
!                                      Quantity_Temp = res:Quantity
!                                  End! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
! End (DBH 23/08/2006) #7963
                              Of 1 ! No Button
                          End ! Case Missive
                      End !If res:Quantity > Quantity_Temp
                  Else !If res:Despatched = 'PEN'
                      !A normal stock part.
                      If res:Quantity > Quantity_temp
                          !Do not allow the user to increase the quantity.
                          !If it's a initial Stock Entry (func:Type = 0)
                          !then the user can delete it, or add another part

                          !If it's from Process (func:Type = 1), then the
                          !only way is to create another sale.
                          Case func:Type
                              Of 0
                                  Case Missive('You cannot increase the quantity of a stock part.'&|
                                    '<13,10>'&|
                                    '<13,10>You can either delete the part and re-insert it at a higher quantity, or insert the same part again.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  res:Quantity = Quantity_Temp
                                  Select(?res:Quantity)
                                  Cycle
                              Of 1
                                  Case Missive('You cannot increase the quantity of a stock part.'&|
                                    '<13,10>'&|
                                    '<13,10>To ship more parts for this customer you will need to create another sale.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  res:Quantity = Quantity_Temp
                                  Select(?res:Quantity)
                                  Cycle
                          End !Case func:Type
                      Else !If res:Quantity > Quantity_temp
                          !Return the difference to stock.

                          Case Missive('You have decreased the quantity required. The difference will be returned to stock.'&|
                            '<13,10>'&|
                            '<13,10>Do you also want to create a Back Order for this quantity?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
! Deleting (DBH 23/08/2006) # 7963 - Do not create a pending order
!                                  LocalCreatePendingPart(Quantity_Temp - res:Quantity)
!                                  glo:select6  = ope:ref_number
! End (DBH 23/08/2006) #7963
                                  glo:select2  = 'NEW PENDING'
                                  glo:select3  = sto:ref_number
                                  glo:select4  = res:record_number
                                  glo:select5  = Quantity_Temp - res:Quantity

                              Of 1 ! No Button
                          End ! Case Missive

                          sto:Quantity_Stock += Quantity_Temp - res:Quantity
                          If Access:STOCK.TryUpdate() = Level:Benign
! Changing (DBH 08/03/2006) #6971 - New procedure
!                               if access:stohist.primerecord() = level:benign
!                                   shi:ref_number           = sto:ref_number
!                                   access:users.clearkey(use:password_key)
!                                   use:password             = glo:password
!                                   access:users.fetch(use:password_key)
!                                   shi:user                 = use:user_code
!                                   shi:date                 = today()
!                                   shi:transaction_type     = 'REC'
!                                   shi:despatch_note_number = ''
!                                   shi:job_number           = ret:ref_number
!                                   shi:quantity             = Quantity_Temp - res:Quantity
!                                   shi:purchase_cost        = sto:purchase_cost
!                                   shi:sale_cost            = sto:sale_cost
!                                   shi:retail_cost          = sto:retail_cost
!                                   shi:information          = 'COMPANY: ' & Clip(ret:company_name) & '<13,10>PURCHASE ORDER NO: ' & |
!                                                           Clip(ret:purchase_order_number) & '<13,10>CONTACT: ' & Clip(ret:contact_name)
!                                   shi:notes                = 'RETAIL ITEM RECREDITED'
!                                   ! Inserting (DBH 07/03/2006) #6971 - Show current stock in history
!                                   shi:StockOnHand          = sto:Quantity_Stock
!                                   ! End (DBH 07/03/2006) #6971
!                                   if access:stohist.insert()
!                                      access:stohist.cancelautoinc()
!                                   end
!                               end!if access:stohist.primerecord() = level:benign
! to (DBH 08/03/2006) #6971
                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                 'REC', | ! Transaction_Type
                                                 '', | ! Depatch_Note_Number
                                                 ret:Ref_Number, | ! Job_Number
                                                 0, | ! Sales_Number
                                                 Quantity_Temp - res:Quantity, | ! Quantity
                                                 sto:Purchase_Cost, | ! Purchase_Cost
                                                 sto:Sale_Cost, | ! Sale_Cost
                                                 sto:Retail_Cost, | ! Retail_Cost
                                                 'RETAIL ITEM RECREDITED', | ! Notes
                                                 'COMPANY: ' & Clip(ret:company_name) & '<13,10>PURCHASE ORDER NO: ' & |
                                                          Clip(ret:purchase_order_number) & '<13,10>CONTACT: ' & Clip(ret:contact_name)) ! Information
                                ! Added OK
                              Else ! AddToStockHistory
                                ! Error
                              End ! AddToStockHistory
! End (DBH 08/03/2006) #6971

                          End !If Access:STOCK.TryUpdate() = Level:Benign

                      End !If res:Quantity > Quantity_temp
                  End !If res:Despatched = 'PEN'

              Else !If sto:Sundry_Item <> 'YES'

              End !If sto:Sundry_Item <> 'YES'
              If res:Quantity = 0
                  Case Missive('You have changed the quantity of this part to zero. If you continue this part will be deleted from the sale.'&|
                    '<13,10>'&|
                    '<13,10>Are you sure you want to delete this part?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          glo:select4  = res:record_number
                          glo:Select7 = 'DELETE'

                      Of 1 ! No Button
                          glo:Select7 = ''
                  End ! Case Missive
              Else
                  glo:Select7 = ''
              End !If res:Quantity = 0
          End !If res:Quantity <> Quantity_Temp
      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  End!If thiswindow.request <> Insertrecord
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?res:Quantity
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Quantity, NewSelection)
      Do item_total
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Quantity, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      If res:item_cost <> 0
          Select(?quantity_required_temp)
      End!If res:item_cost <> 0
      
      
      !Save Fields
      quantity_temp   = res:quantity
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LocalCreatePendingPart          Procedure(Long  func:Quantity)
    Code
    If Access:ORDPEND.PrimeRecord() = Level:Benign
        ope:Part_Ref_Number = sto:Ref_Number
        ope:Job_Number      = res:Ref_Number
        ope:Part_Type       = 'STO'
        ope:Supplier        = sto:Supplier
        ope:Part_Number     = res:Part_Number
        ope:Description     = res:Description
        ope:Quantity        = func:Quantity
        ope:Account_Number  = ret:Account_Number

        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !f Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Process_Retail_Sale PROCEDURE                         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
tmp:BrowseType       BYTE(0)
tmp:PrintDespatchNote BYTE(0)
tmp:PrintInvoice     BYTE(0)
tmp:tag              STRING(20)
stop_on_errors_temp  BYTE(0)
save_ret_id          USHORT,AUTO
save_ret_ali_id      USHORT,AUTO
save_rtp_id          USHORT,AUTO
transfer_temp        BYTE
ref_number_temp      LONG
save_res_ali_id      USHORT,AUTO
save_res_id          USHORT,AUTO
ActionMessage        CSTRING(40)
address_line1_temp   STRING(60)
raised_by_temp       STRING(50)
Search_Field_Temp    STRING(30)
despatched_tick      STRING(1)
payment_status_temp  STRING(20)
Despatch_Number_temp STRING(20)
tmp:VatRate          REAL
tmp:PaymentTaken     REAL
tmp:OutstandingQty   LONG
locScannedPartNumber STRING(30)
tagManualQueue       QUEUE,PRE(mantag)
RecordNumber         LONG
                     END
StockHistoryCheckQ   QUEUE,PRE()
StockPartNumber      STRING(30)
QtySold              LONG
Purchase_cost        REAL
Sale_cost            REAL
Retail_Cost          REAL
                     END
HistoryCount         LONG
tmp:RecordNumber     LONG
BRW3::View:Browse    VIEW(RETSTOCK)
                       PROJECT(res:Part_Number)
                       PROJECT(res:Description)
                       PROJECT(res:Quantity)
                       PROJECT(res:ScannedQty)
                       PROJECT(res:Purchase_Order_Number)
                       PROJECT(res:Previous_Sale_Number)
                       PROJECT(res:Record_Number)
                       PROJECT(res:Ref_Number)
                       PROJECT(res:Despatched)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_NormalFG       LONG                           !Normal forground color
tmp:tag_NormalBG       LONG                           !Normal background color
tmp:tag_SelectedFG     LONG                           !Selected forground color
tmp:tag_SelectedBG     LONG                           !Selected background color
tmp:tag_Icon           LONG                           !Entry's icon ID
res:Part_Number        LIKE(res:Part_Number)          !List box control field - type derived from field
res:Part_Number_NormalFG LONG                         !Normal forground color
res:Part_Number_NormalBG LONG                         !Normal background color
res:Part_Number_SelectedFG LONG                       !Selected forground color
res:Part_Number_SelectedBG LONG                       !Selected background color
res:Description        LIKE(res:Description)          !List box control field - type derived from field
res:Description_NormalFG LONG                         !Normal forground color
res:Description_NormalBG LONG                         !Normal background color
res:Description_SelectedFG LONG                       !Selected forground color
res:Description_SelectedBG LONG                       !Selected background color
res:Quantity           LIKE(res:Quantity)             !List box control field - type derived from field
res:Quantity_NormalFG  LONG                           !Normal forground color
res:Quantity_NormalBG  LONG                           !Normal background color
res:Quantity_SelectedFG LONG                          !Selected forground color
res:Quantity_SelectedBG LONG                          !Selected background color
res:ScannedQty         LIKE(res:ScannedQty)           !List box control field - type derived from field
res:ScannedQty_NormalFG LONG                          !Normal forground color
res:ScannedQty_NormalBG LONG                          !Normal background color
res:ScannedQty_SelectedFG LONG                        !Selected forground color
res:ScannedQty_SelectedBG LONG                        !Selected background color
tmp:OutstandingQty     LIKE(tmp:OutstandingQty)       !List box control field - type derived from local data
tmp:OutstandingQty_NormalFG LONG                      !Normal forground color
tmp:OutstandingQty_NormalBG LONG                      !Normal background color
tmp:OutstandingQty_SelectedFG LONG                    !Selected forground color
tmp:OutstandingQty_SelectedBG LONG                    !Selected background color
res:Purchase_Order_Number LIKE(res:Purchase_Order_Number) !List box control field - type derived from field
res:Purchase_Order_Number_NormalFG LONG               !Normal forground color
res:Purchase_Order_Number_NormalBG LONG               !Normal background color
res:Purchase_Order_Number_SelectedFG LONG             !Selected forground color
res:Purchase_Order_Number_SelectedBG LONG             !Selected background color
res:Previous_Sale_Number LIKE(res:Previous_Sale_Number) !List box control field - type derived from field
res:Previous_Sale_Number_NormalFG LONG                !Normal forground color
res:Previous_Sale_Number_NormalBG LONG                !Normal background color
res:Previous_Sale_Number_SelectedFG LONG              !Selected forground color
res:Previous_Sale_Number_SelectedBG LONG              !Selected background color
tmp:BrowseType         LIKE(tmp:BrowseType)           !List box control field - type derived from local data
tmp:BrowseType_NormalFG LONG                          !Normal forground color
tmp:BrowseType_NormalBG LONG                          !Normal background color
tmp:BrowseType_SelectedFG LONG                        !Selected forground color
tmp:BrowseType_SelectedBG LONG                        !Selected background color
res:Record_Number      LIKE(res:Record_Number)        !Primary key field - type derived from field
res:Ref_Number         LIKE(res:Ref_Number)           !Browse key field - type derived from field
res:Despatched         LIKE(res:Despatched)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK15::res:Despatched      LIKE(res:Despatched)
HK15::res:Ref_Number      LIKE(res:Ref_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Process Retail Sale'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Process Retail Sale'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,28,672,136),USE(?Sheet3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT(''),AT(80,33),USE(?Prompt1),FONT(,10,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Order Details'),AT(376,33),USE(?Prompt7),TRN,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LINE,AT(338,36,0,109),USE(?Line1),COLOR(COLOR:White)
                           PROMPT('Account No:'),AT(84,44),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s15),AT(148,44),USE(ret:Account_Number),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Date Raised:'),AT(364,44),USE(?Prompt8),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@d6b),AT(452,44),USE(ret:date_booked),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@t1),AT(512,44),USE(ret:time_booked),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Contact Name:'),AT(84,54),USE(?Prompt3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Delivery Address'),AT(84,70),USE(?Prompt4),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(148,54),USE(ret:Contact_Name),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Rasied By:'),AT(364,52),USE(?String8),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Payment Status:'),AT(364,62),USE(?String8:2),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Company Name:'),AT(84,86),USE(?Prompt5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(148,86),USE(ret:Company_Name_Delivery),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s50),AT(452,52),USE(raised_by_temp),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s20),AT(452,62),USE(payment_status_temp),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Invoice Number:'),AT(364,78),USE(?Prompt9),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Despatch Number'),AT(364,70),USE(?Prompt9:2),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8b),AT(452,78),USE(ret:Invoice_Number),TRN,LEFT,FONT(,8,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@d6b),AT(500,78),USE(ret:Invoice_Date),TRN,LEFT,FONT(,8,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s8b),AT(452,70),USE(ret:Despatch_Number),TRN,LEFT,FONT(,8,,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Address:'),AT(84,100),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(148,100),USE(address_line1_temp),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Delivery Text'),AT(364,92),USE(?ret:delivery_text:prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(452,92,124,24),USE(ret:Delivery_Text),VSCROLL,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           STRING(@s30),AT(148,110),USE(ret:Address_Line2_Delivery),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('OUTSTANDING PAYMENT'),AT(259,148),USE(?OutstandingPayment),HIDE,CENTER,FONT(,14,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(148,124),USE(ret:Address_Line3_Delivery),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s10),AT(148,134),USE(ret:Postcode_Delivery),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Date Despatched'),AT(364,118),USE(?RET:Date_Despatched:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(452,118,64,10),USE(ret:Date_Despatched),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Consignment Number'),AT(364,132),USE(?RET:Consignment_Number:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(452,132,124,10),USE(ret:Consignment_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(472,352),USE(?Change),TRN,FLAT,LEFT,ICON('edtqtyp.jpg')
                       SHEET,AT(4,166,672,216),USE(?Sheet4),COLOR(0D6E7EFH),SPREAD
                         TAB('Un-Picked Items'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON('&Rev tags'),AT(20,136,50,13),USE(?DASREVTAG),HIDE
                           BUTTON,AT(540,218),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           PROMPT('Remove Items'),AT(248,182),USE(?prompt:RemoveItem),FONT(,,,FONT:bold)
                           PROMPT('Add Items'),AT(184,182),USE(?prompt:AddItem),FONT(,,,FONT:bold)
                           BUTTON,AT(168,192),USE(?button:AddStartScanning),TRN,FLAT,ICON('stscanp.jpg')
                           BUTTON,AT(168,192),USE(?button:AddStopScanning),TRN,FLAT,HIDE,ICON('stpscanp.jpg')
                           BUTTON,AT(244,192),USE(?button:RemoveStartScanning),TRN,FLAT,ICON('stscanp.jpg')
                           BUTTON,AT(244,192),USE(?button:RemoveStopScanning),TRN,FLAT,HIDE,ICON('stpscanp.jpg')
                           PROMPT('Scan Part Number'),AT(328,200),USE(?locScannedPartNumber:Prompt),TRN,HIDE,FONT(,,,FONT:bold)
                           ENTRY(@s30),AT(408,200,124,10),USE(locScannedPartNumber),HIDE,COLOR(COLOR:White),MSG('Scanned Part Number'),TIP('Scanned Part Number'),UPR
                           BUTTON,AT(540,248),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(540,276),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           BUTTON,AT(540,312),USE(?ProcessTagged),TRN,FLAT,LEFT,ICON('picktagp.jpg')
                           BUTTON,AT(608,312),USE(?ReturnTagged),TRN,FLAT,HIDE,LEFT,ICON('ret2stop.jpg')
                           BUTTON('sho&W tags'),AT(68,136,70,13),USE(?DASSHOWTAG),HIDE
                           ENTRY(@s30),AT(8,208,124,10),USE(res:Part_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('Picked Items'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,208,124,10),USE(res:Part_Number,,?res:Part_Number:2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('Back Orders'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BOX,AT(392,208,11,10),USE(?Box1),COLOR(COLOR:Black),FILL(COLOR:Black)
                           BOX,AT(292,208,11,10),USE(?Box1:3),COLOR(COLOR:Black),FILL(COLOR:Silver)
                           PROMPT('- Cancelled Back Order'),AT(408,208),USE(?Prompt14),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('- Placed On New Sale'),AT(308,208),USE(?Prompt14:2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BOX,AT(393,208,11,10),USE(?Box1:2),COLOR(COLOR:Black),FILL(COLOR:Black)
                         END
                       END
                       LIST,AT(8,220,524,128),USE(?List:2),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)*J@n1@125L(2)|M*~Part Number~@s30@126L(2)|M*~Description~@s30@[35R(2)|M*~T' &|
   'otal~@n8@39R(2)|M*~Scanned~@s8@44R(2)|M*~Outstanding~@s8@]|M~Quantity~92L(2)|M*~' &|
   'Purchase Order Number~@s30@97R(2)|M*~Sales No~L@s8b@4R(2)|M*~Browse Type For Col' &|
   'ouring~L@n1@'),FROM(Queue:Browse:1),DROPID('FromList')
                       BUTTON,AT(608,384),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(8,352),USE(?Payments),TRN,FLAT,LEFT,ICON('paydetp.jpg')
                       BUTTON,AT(76,352),USE(?PrintInvoice),TRN,FLAT,LEFT,ICON('invoicep.jpg')
                       BUTTON,AT(1072,402),USE(?PrintDespatchNote),TRN,FLAT,LEFT,ICON('prndesp.jpg')
                       BUTTON,AT(540,384),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW3                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW3::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet4) = 1
BRW3::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet4) = 2
BRW3::Sort3:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet4) = 3
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
local       Class
allPicked  Procedure(),Byte
startScanning   Procedure(Byte fType)
stopScanning    Procedure()
            End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  ! Before Embed Point: %DasTagBeforeTagOnOff) DESC(Tagging Before Tag On / Off) ARG(9)
  if (?dastag{prop:hide} = 1)
      ! Don't allow to tag if all picked
      exit
  end ! if (?dastag{prop:hide} = 1)
  ! After Embed Point: %DasTagBeforeTagOnOff) DESC(Tagging Before Tag On / Off) ARG(9)
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW3.UpdateBuffer
   glo:Queue.Pointer = res:Record_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = res:Record_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse:1.tmp:tag = tmp:tag
  Queue:Browse:1.tmp:tag_NormalFG = -1
  Queue:Browse:1.tmp:tag_NormalBG = -1
  Queue:Browse:1.tmp:tag_SelectedFG = -1
  Queue:Browse:1.tmp:tag_SelectedBG = -1
  IF (tmp:tag = '*')
    Queue:Browse:1.tmp:tag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
  ! Before Embed Point: %DasTagAfterTagOnOff) DESC(Tagging After Tag On / Off) ARG(9)
  if (tmp:tag = '*')
      if (field() = ?dastag or keycode() = mouseleft)
          ! The user has manually tagged (DBH: 03/06/2009) #10311
          mantag:RecordNumber = res:Record_Number
          get(tagManualQueue,mantag:RecordNumber)
          if (error())
              mantag:RecordNumber = res:Record_Number
              add(tagManualQueue)
              !TB13443 - J - 02/12/14 Update the number scanned to the full
              tmp:RecordNumber = Res:Record_number
              !find the record
              Access:RetStock.clearkey(res:Record_Number_Key)
              res:Record_Number = tmp:RecordNumber
              If access:RetStock.fetch(res:Record_Number_Key) = level:Benign
                      res:ScannedQty = res:Quantity
                      Access:RetStock.update()
              END !if fetch worked
          end ! if (~error())
      end ! if (ield() = ?dastag or keycode() = mouseleft)
  else ! if (tmp:tag = '*')
      mantag:RecordNumber = res:Record_Number
      get(tagManualQueue,mantag:RecordNumber)
      if (~error())
          delete(tagManualQueue)
      end ! if (~error())
      !TB13443 - J - 02/12/14 Update the number scanned to the zero when untagged
      tmp:RecordNumber = Res:Record_number
      !find the record
      Access:RetStock.clearkey(res:Record_Number_Key)
      res:Record_Number = tmp:RecordNumber
      If access:RetStock.fetch(res:Record_Number_Key) = level:Benign
              res:ScannedQty = 0
              Access:RetStock.update()
      END !if fetch worked
  
  end ! if (tmp:tag = '*')
  
  brw3.resetsort(1)
  ! After Embed Point: %DasTagAfterTagOnOff) DESC(Tagging After Tag On / Off) ARG(9)
DASBRW::9:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = res:Record_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
  ! Before Embed Point: %DasTagAfterTagAll) DESC(Tagging After Tag All) ARG(9)
  !TB13443 - schema changes
  !Now the "Tag All" Button will tag all that haven't been partly scanned already
  !    AND
  !the "number scanned" must be updated with the full amount when manually tagged
  
  free(tagManualQueue)
  tmp:RecordNumber = 0    !using this as a flag if any were partly scanned
  
  
  loop x# = 1 to records(glo:Queue)
  
      Get(Glo:queue,x#)
  
      !find the record
      Access:RetStock.clearkey(res:Record_Number_Key)
      res:Record_Number = glo:Queue.Pointer
      If access:RetStock.fetch(res:Record_Number_Key) = level:Benign
          If res:ScannedQty > 0 and res:ScannedQty < res:Quantity
              !this has been partly scanned and needs to remain "not manually scanned"
              tmp:RecordNumber += 1     !set the flag to show the message about partly scanned lines
          ELSE
              !this need to be marked as manually scanned
              mantag:RecordNumber = glo:Queue.Pointer
              add(tagManualQueue)
              res:ScannedQty = res:Quantity
              Access:RetStock.update()
          END !if partly scanned (ie res:ScannedQty > 0 and res:ScannedQty < res:Quantity)
      END
  
  
  end ! loop x# = 1 to records(glo:Queue)
  
  Case tmp:RecordNumber
      of  0
          !do nothing
      of 1
          Miss# = Missive('Please note that "Tag All" has not altered the one line that had been partly scanned.',|
                          'ServiceBase 3g','mexclam.jpg','OK')
      ELSE
          Miss# = Missive('Please note that "Tag All" has not altered the '&clip(tmp:RecordNumber)&' lines that had been partly scanned.',|
                          'ServiceBase 3g','mexclam.jpg','OK')
  
  END !Case tmp:Record number
  
  brw3.resetsort(1)
  
  !=======================================================================================
  ! Have manually tagged all. All records are counted (DBH: 03/06/2009) #10311
  !free(tagManualQueue)
  !loop x# = 1 to records(glo:Queue)
  !
  !    !TB13443 - J - 02/12/14 added missing line because this was not working
  !    Get(Glo:queue,x#)        !Even the best sometimes forget ;-)
  !    !End TB13443 missing part
  !
  !    mantag:RecordNumber = glo:Queue.Pointer
  !    get(tagManualQueue,mantag:RecordNumber)
  !    if (error())
  !        mantag:RecordNumber = glo:Queue.Pointer
  !        add(tagManualQueue)
  !    end ! if (~error())
  !
  !end ! loop x# = 1 to records(glo:Queue)
  !
  !Miss# = Missive('This has marked all lines as picked in full. If you do not want this you must edit the lines where this does not apply.',|
  !                'ServiceBase 3g','mwarn.jpg','OK')
  
  
  ! After Embed Point: %DasTagAfterTagAll) DESC(Tagging After Tag All) ARG(9)
DASBRW::9:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
  ! Before Embed Point: %DasTagAfterUnTagAll) DESC(Tagging After UnTag All) ARG(9)
  ! Have manually untagged all. All records are counted (DBH: 03/06/2009) #10311
  free(tagManualQueue)
  
  !TB13443 - J - 02/12/14 Update the number scanned to the zero when untagged
  Access:RetStock.clearkey(res:Part_Number_Key)   !has ref_number as first component
  res:Ref_Number = ret:Ref_Number
  set(res:Part_Number_Key,res:Part_Number_Key)
  Loop
      if access:RetStock.next() then break.
      if res:Ref_Number <> ret:Ref_Number then break.
      res:ScannedQty = 0
      Access:RetStock.update()
  END !loop through retstock
  
  brw3.resetsort(1)
  ! After Embed Point: %DasTagAfterUnTagAll) DESC(Tagging After UnTag All) ARG(9)
DASBRW::9:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = res:Record_Number
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = res:Record_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
CheckStockHistory       Routine

!TB13116 - J - 24/01/14
!sometimes stock history is missing on some sale items.
!This is a belt and braces approach to ensure all stock is historied

    !clear variables
    free(StockHistoryCheckQ)
    clear(StockHistoryCheckQ)

    !First create a queue saying how much of each stock has been sold on this sale
    !there may be several items for the same stock part_number
    Access:RETSTOCK.Clearkey(res:Part_Number_Key)    !has ref_number as first part
    res:Ref_Number  = ret:Ref_Number
    res:Part_Number = ''
    Set(res:Part_Number_Key,res:Part_Number_Key)
    Loop
        If Access:RETSTOCK.next() then break.
        if res:Ref_Number  <> ret:Ref_Number then break.

        if res:Despatched <> 'YES' then cycle.  !these parts have not been received yet

        !Write this to a queue with StockPartNumber, QtySold etc
        StockHistoryCheckQ.StockPartNumber = res:Part_Number
        get(StockHistoryCheckQ,StockHistoryCheckQ.StockPartNumber)
        if error() then
            !not found
            StockHistoryCheckQ.StockPartNumber = Res:Part_Number
            StockHistoryCheckQ.QtySold         = res:Quantity
            StockHistoryCheckQ.Purchase_Cost   = res:Purchase_Cost  
            StockHistoryCheckQ.Sale_Cost       = res:Sale_Cost     
            StockHistoryCheckQ.Retail_Cost     = res:Retail_Cost  
            Add(StockHistoryCheckQ)
        ELSE
            !found - update this
            StockHistoryCheckQ.QtySold        += res:Quantity
            PUT(StockHistoryCheckQ)
        END
    END !loop through items on this sale


    !now from that queue check how much history has been written for each item
    Loop X# = 1 to records(StockHistoryCheckQ)

        get(StockHistoryCheckQ, x#)

        HistoryCount = StockHistoryCheckQ.QtySold       !this should be the total sold on this order

        !need stock to swap to get stock.ref_number
        Access:STOCK.ClearKey(sto:Location_Key)
        sto:Location    = MainStoreLocation()
        sto:Part_Number = StockHistoryCheckQ.StockPartNumber
        If Access:STOCK.TryFetch(sto:Location_Key)
            !not found
            cycle
        END

        Access:StoHist.clearkey(shi:Ref_Number_Key)       !shi:Ref_Number_Key is ref_number then date
        shi:Ref_Number = sto:Ref_Number
        shi:Date       = ret:date_booked                  !Suitable starting date?
        Set(shi:Ref_Number_Key,shi:Ref_Number_Key)
        Loop

            if access:SToHist.next() then break.
            if shi:Ref_Number <> sto:Ref_Number then break.

            !Is this record from this order?
            if shi:Job_Number <> ret:Ref_number then cycle.

            !decrement the count
            HistoryCount -= shi:Quantity

        END !Loop through StoHist

        !have we got any left?
        if HistoryCount > 0 then
            !not enough stock history has been written - this may replace several bits of history, but the qty will be right
            !Stock is already open at the right place
            If AddToStockHistory(sto:Ref_Number,  | ! Ref_Number
                               'DEC',             | ! Transaction_Type
                               '',                | ! Depatch_Note_Number
                               ret:Ref_Number,    | ! Job_Number
                               0,                 | ! Sales_Number
                               HistoryCount,      | ! Quantity
                               StockHistoryCheckQ.Purchase_Cost, | ! Purchase_Cost - remembered from above
                               StockHistoryCheckQ.Sale_Cost,     | ! Sale_Cost     - remembered from above
                               StockHistoryCheckQ.Retail_Cost,   | ! Retail_Cost   - remembered from above
                               'RETAIL ITEM SOLD',| ! Notes
                               'COMPANY: ' & Clip(ret:company_name) & '<13,10>PURCH ORDER NO: ' & Clip(RET:Purchase_Order_Number) & |
                                            '<13,10>CONTACT: ' & CLip(RET:Contact_Name)) ! Information
                ! Added OK
            ELSE
                Miss# = Missive('ERROR - Stock history has failed to update - please report this error ref:TB13116','ServiceBase 3g','Mstop.jpg','OK')
            End ! AddToStockHistory

        END !if Count was still showing a left over

    END !loop through stock history check queue

    EXIT
ShowHideUnPicking       Routine
    Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
    res:Ref_Number = ret:Ref_Number
    res:Despatched = 'PIK'
    Set(res:Despatched_Only_Key,res:Despatched_Only_Key)
    If Access:RETSTOCK.NEXT()
        ?Tab4{prop:Hide} = 1
    Else !If Access:RETSTOCK.NEXT()
        If res:Ref_Number <> ret:Ref_Number Or |
            res:Despatched <> 'PIK'
            ?tab4{prop:Hide} = 1
        Else !If res:Ref_Number <> ref:Ref_Number Or |
            ?Tab4{prop:Hide} = 0
        End !If res:Ref_Number <> ref:Ref_Number Or |
    End !Access:RETSTOCK.NEXT()
    Brw3.ResetSort(1)
CheckPayment        Routine
    If RetailPaid() = Level:Benign
        ?OutstandingPayment{prop:Hide} = 1
    Else !If SalePaid() = Level:Benign
        ?OutstandingPayment{prop:Hide} = 0
    End !If SalePaid() = Level:Benign
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020335'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Process_Retail_Sale')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  SELF.AddUpdateFile(Access:RETSALES)
  Relate:COURIER.Open
  Relate:DEFAULTS.Open
  Relate:RETDESNO.Open
  Relate:RETSALES_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WAYBCONF.Open
  Relate:WAYBILLJ.Open
  Access:RETSALES.UseFile
  Access:USERS.UseFile
  Access:INVOICE.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:RETSTOCK.UseFile
  Access:WAYBILLS.UseFile
  Access:RETPAY.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETSALES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW3.Init(?List:2,Queue:Browse:1.ViewPosition,BRW3::View:Browse,Queue:Browse:1,Relate:RETSTOCK,SELF)
  ! Insert --- Clear scanned value (DBH: 03/06/2009) #10311
  Access:RETSTOCK.Clearkey(res:Part_Number_Key)
  res:Ref_Number    = ret:Ref_Number
  set(res:Part_Number_Key,res:Part_Number_Key)
  loop
      if (Access:RETSTOCK.Next())
          Break
      end ! if (Access:RETSTOCK.Next())
      if (res:Ref_Number    <> ret:Ref_Number)
          Break
      end ! if (res:Ref_Number    <> ret:Ref_Number)
      if (res:ScannedQty <> 0)
          res:ScannedQty = 0
          Access:RETSTOCK.TryUpdate()
      end ! if (res:ScannedQty <> 0)
  end ! loop
  ! end --- (DBH: 03/06/2009) #10311
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Do CheckPayment
  
  if glo:WebJob
      hide(?Change)
  
      hide(?OK)
      hide(?Payments)
      hide(?ReturnTagged)
      hide(?PrintInvoice)
  
      hide(?DASTAG)
      hide(?DASTAGAll)
      hide(?DASUNTAGALL)
      hide(?ProcessTagged)
  end
  ! Save Window Name
   AddToLog('Window','Open','Process_Retail_Sale')
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW3.Q &= Queue:Browse:1
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,res:DespatchedPartKey)
  BRW3.AddRange(res:Despatched)
  BRW3.AddLocator(BRW3::Sort1:Locator)
  BRW3::Sort1:Locator.Init(?res:Part_Number,res:Part_Number,1,BRW3)
  BRW3.AddSortOrder(,res:DespatchedPartKey)
  BRW3.AddRange(res:Despatched)
  BRW3.AddLocator(BRW3::Sort2:Locator)
  BRW3::Sort2:Locator.Init(?res:Part_Number:2,res:Part_Number,1,BRW3)
  BRW3.AddSortOrder(,res:Part_Number_Key)
  BRW3.AddRange(res:Ref_Number)
  BRW3.AddLocator(BRW3::Sort3:Locator)
  BRW3::Sort3:Locator.Init(,res:Part_Number,1,BRW3)
  BRW3.SetFilter('(Upper(res:Despatched) = ''PEN'' Or Upper(res:Despatched) = ''CAN'' or Upper(res:Despatched) = ''OLD'')')
  BRW3.AddSortOrder(,res:Part_Number_Key)
  BRW3.AddRange(res:Ref_Number,ret:Ref_Number)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,res:Part_Number,1,BRW3)
  BIND('tmp:tag',tmp:tag)
  BIND('tmp:OutstandingQty',tmp:OutstandingQty)
  BIND('tmp:BrowseType',tmp:BrowseType)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(tmp:tag,BRW3.Q.tmp:tag)
  BRW3.AddField(res:Part_Number,BRW3.Q.res:Part_Number)
  BRW3.AddField(res:Description,BRW3.Q.res:Description)
  BRW3.AddField(res:Quantity,BRW3.Q.res:Quantity)
  BRW3.AddField(res:ScannedQty,BRW3.Q.res:ScannedQty)
  BRW3.AddField(tmp:OutstandingQty,BRW3.Q.tmp:OutstandingQty)
  BRW3.AddField(res:Purchase_Order_Number,BRW3.Q.res:Purchase_Order_Number)
  BRW3.AddField(res:Previous_Sale_Number,BRW3.Q.res:Previous_Sale_Number)
  BRW3.AddField(tmp:BrowseType,BRW3.Q.tmp:BrowseType)
  BRW3.AddField(res:Record_Number,BRW3.Q.res:Record_Number)
  BRW3.AddField(res:Ref_Number,BRW3.Q.res:Ref_Number)
  BRW3.AddField(res:Despatched,BRW3.Q.res:Despatched)
  BRW3.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab4{PROP:TEXT} = 'Un-Picked Items'
    ?Tab3{PROP:TEXT} = 'Picked Items'
    ?Tab5{PROP:TEXT} = 'Back Orders'
    ?List:2{PROP:FORMAT} ='11L(2)*J@n1@#1#125L(2)|M*~Part Number~@s30@#7#126L(2)|M*~Description~@s30@#12#35R(2)|M*~Total~@n8@#17#39R(2)|M*~Scanned~@s8@#22#44R(2)|M*~Outstanding~@s8@#27#92L(2)|M*~Purchase Order Number~@s30@#32#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  Ref_Number_Temp = ret:Ref_Number
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
    Relate:RETDESNO.Close
    Relate:RETSALES_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WAYBCONF.Close
    Relate:WAYBILLJ.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Process_Retail_Sale')
      If transfer_temp = 1
          access:retstock.open()
          access:retstock.usefile()
          access:retsales.open()
          access:retsales.usefile()
          access:retstock_alias.open()
          access:retstock_alias.usefile()
          access:retsales_alias.open()
          access:retsales_alias.usefile()
  
          access:retsales_alias.clearkey(res_ali:ref_number_key)
          res_ali:ref_number = ref_number_temp
          if access:retsales_alias.tryfetch(res_ali:ref_number_key) = Level:Benign
              Get(retsales,0)
              If access:retsales.primerecord() = Level:Benign
                  ref_number$ = ret:ref_number
                  ret:record  :=: res_ali:record
                  ret:ref_number  = ref_number$
                  ret:despatched = 'RET'
                  access:retsales.insert()
  
                  setcursor(cursor:wait)
                  save_ret_ali_id = access:retstock_alias.savefile()
                  access:retstock_alias.clearkey(ret_ali:despatched_only_key)
                  ret_ali:ref_number = ref_number_temp
                  ret_ali:despatched = 'NO'
                  set(ret_ali:despatched_only_key,ret_ali:despatched_only_key)
                  loop
                      if access:retstock_alias.next()
                         break
                      end !if
                      if ret_ali:ref_number <> ref_number_temp      |
                      or ret_ali:despatched <> 'NO'      |
                          then break.  ! end if
                      Get(retstock,0)
                      If access:retstock.primerecord() = Level:Benign
                          record_number$  = res:record_number
                          res:record  :=: ret_ali:record
                          res:record_number   = record_number$
                          res:ref_number  = ret:ref_number
                          access:retstock.insert()
                      End!If access:retstock.primerecord() = Level:Benign
                  end !loop
                  access:retstock_alias.restorefile(save_ret_ali_id)
                  setcursor()
              End!If access:retsales.primerecord() = Level:Benign
  
              save_res_id = access:retstock.savefile()
              access:retstock.clearkey(res:despatched_only_key)
              res:ref_number = ref_number_temp
              res:despatched = 'NO'
              set(res:despatched_only_key,res:despatched_only_key)
              loop
                  if access:retstock.next()
                     break
                  end !if
                  if res:ref_number <> ref_number_temp      |
                  or res:despatched <> 'NO'      |
                      then break.  ! end if
                  Delete(retstock)
              end !loop
              access:retstock.restorefile(save_res_id)
          end!if access:retsales_alias.tryfetch(res_ali:ref_number_key) = Level:Benign
          access:retstock_alias.close()
          access:retsales.close()
          access:retstock.close()
          access:retsales_alias.close()
  
      End!If transfer_temp = 1
  GlobalErrors.SetProcedureName
  glo:Select20 = ''
  glo:Select21 = ''
  glo:Select22 = ''
  If thiswindow.response <> requestcancelled
      Access:DEFAULTS.Open()
      Access:DEFAULTS.UseFile()
  
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
  
      glo:select1  = Ref_number_temp
      relate:retsales.open()
      access:retdesno.open()
      access:retdesno.usefile()
      relate:tradeacc.open()
      relate:invoice.open()
      access:vatcode.open()
      access:vatcode.usefile()
      Relate:WAYBILLS.Open()
      Relate:WAYBCONF.Open()
      Relate:WAYBILLJ.Open()
  
      CheckPayment# = 0
  
      Access:RETSALES.Clearkey(ret:Ref_Number_Key)
      ret:Ref_Number  = Ref_Number_Temp
      If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
          !Found
          If tmp:PrintDespatchNote = 1 AND glo:WebJob = 0
              !Create a New Despatch Batch Number, and assign this to the job
              If ret:Despatch_Number = 0
                  if access:retdesno.primerecord() = Level:Benign
                      RDN:Consignment_Number  = ''
                      RDN:Courier             = ret:courier
                      rdn:sale_number         = ret:ref_number
                      access:retdesno.insert()
  
                      ret:despatched = 'PRO'
  
                      CheckPayment# = 1
  
                      ret:despatch_number = rdn:despatch_number
                      access:retsales.update()
                  End!if access:retdesno.primerecord() = Level:Benign
              End !If ret:Despatch_Number = 0
              ! Inserting (DBH 20/10/2006) # 8381 - Print two copies of the despatch note
              glo:ReportCopies = 2
              ! End (DBH 20/10/2006) #8381
              Retail_Despatch_Note(ret:Despatch_Number)
  
              ! Inserting (DBH 23/08/2006) # 7964 - Print a retail waybill
              If ret:WaybillNumber = 0
                  ret:WaybillNumber = NextWayBillNumber()
                  If ret:WayBillNumber > 0
                      Access:RETSALES.Update()
                      Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
                      way:WaybillNumber = ret:WayBillNumber
                      If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
                          !Found
                          way:AccountNumber = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                          way:WaybillID = 200
                          way:WayBillType = 10
                          way:FromAccount = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                          way:ToAccount = ret:Account_Number
                          If Access:WAYBILLS.Update() = Level:Benign
                              If Access:WAYBILLJ.PrimeRecord() = Level:Benign
                                  waj:WayBillNumber = way:WayBillNumber
                                  waj:JobNumber = ret:Ref_Number
                                  waj:OrderNumber = ret:Purchase_Order_Number
                                  waj:JobType = 'RET'
                                  If Access:WAYBILLJ.TryInsert() = Level:Benign
                                      !Insert
                                  Else ! If Access:WAYBILLJ.TryInsert() = Level:Benign
                                      Access:WAYBILLJ.CancelAutoInc()
                                  End ! If Access:WAYBILLJ.TryInsert() = Level:Benign
                              End ! If Access.WAYBILLJ.PrimeRecord() = Level:Benign
  
                              !TB13370 - J - 15/04/15 - adding a waybillconf record
                              Access:WaybConf.primerecord()
                              WAC:AccountNumber    = way:AccountNumber
                              WAC:WaybillNo        = way:WayBillNumber
                              WAC:GenerateDate     = today()
                              WAC:GenerateTime     = clock()
                              WAC:ConfirmationSent = 0
                              Access:Waybconf.update()
  
                          End ! If Access:WAYBILLS.Update() = Level:Benign
  
                      Else ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
                          !Error
                      End ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
                  End ! If tmp:NewWayBillNumber > 0
              End ! If ret:WaybillNumber = 0
              glo:Select20 = 'PRINT WAYBILL'
              glo:Select21 = ret:WayBillNumber
              glo:Select22 = ret:Ref_Number
  
              ! End (DBH 23/08/2006) #7964
              thiswindow.reset(1)
          End!If tmp:PrintDespatchNote = 1
  
          If tmp:PrintInvoice = 1
  
              Do CheckStockHistory        !TB03116 - J - 24/01/14
  
              fetch_error# = 0
              despatch# = 0
              UseEuro# = 0
              access:subtracc.clearkey(sub:account_number_key)
              sub:account_number = ret:account_number
              if access:subtracc.fetch(sub:account_number_key)
              Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
                  access:tradeacc.clearkey(tra:account_number_key)
                  tra:account_number = sub:main_account_number
                  if access:tradeacc.fetch(tra:account_number_key)
                      fetch_error# = 1
                      If stop_on_errors_temp = 0
                          Case Missive('Cannot create Invoice for Sale Number ' & Clip(ret:Ref_Number) & '.'&|
                            '<13,10>'&|
                            '<13,10>Cannot find Trade/Sub Account.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
  
                      End!If stop_on_errors_temp = 0
  
                  Else!if access:tradeacc.fetch(tra:account_number_key)
                      If tra:use_sub_accounts = 'YES'
                          If sub:despatch_invoiced_jobs = 'YES'
                              If sub:despatch_paid_jobs = 'YES'
                              Else
                                  despatch# = 1
                              End!If sub:despatch_paid_jobs = 'YES' And ret:paid = 'YES'
  
                          End!If sub:despatch_invoiced_jobs = 'YES'
                      End!If tra:use_sub_accounts = 'YES'
                      if tra:invoice_sub_accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                          If sub:EuroApplies
                              UseEuro# = 1
                          End !If sub:EuroApplies
                          vat_number" = sub:vat_number
                          access:vatcode.clearkey(vat:vat_code_key)
                          vat:vat_code = sub:retail_vat_code
                          if access:vatcode.fetch(vat:vat_code_key)
                              fetch_error# = 1
                              If stop_on_errors_temp = 0
                                  Case Missive('Cannot create Invoice for Sale Number ' & Clip(ret:Ref_Number) & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Cannot find Sub Trade Account V.A.T. Rates.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                              End!If stop_on_errors_temp = 0
  
                          Else!if access:vatcode.fetch(vat:vat_code_key)
                             retail_rate$ = vat:vat_rate
                          end!if access:vatcode.fetch(vat:vat_code_key)
  
                      else!if tra:use_sub_accounts = 'YES'
                          If tra:EuroApplies
                              UseEuro# = 1
                          End !If tra:EuroApplies
                          If tra:despatch_invoiced_jobs = 'YES'
                              If tra:despatch_paid_jobs = 'YES'
                              Else
                                  despatch# = 1
                              End!If tra:despatch_paid_jobs = 'YES' and ret:paid = 'YES'
                          End!If tra:despatch_invoiced_jobs = 'YES'
                          vat_number" = tra:vat_number
                          access:vatcode.clearkey(vat:vat_code_key)
                          vat:vat_code = tra:retail_vat_code
                          if access:vatcode.fetch(vat:vat_code_key)
                              fetch_error# = 1
                              If stop_on_errors_temp = 0
                                  Case Missive('Cannot create Invoice for Sale Number ' & Clip(ret:Ref_Number) & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Cannot find Trade Account V.A.T. Rates.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                              End!If stop_on_errors_temp = 0
  
                          Else!if access:vatcode.fetch(vat:vat_code_key)
                             retail_rate$ = vat:vat_rate
                          end
  
                      end!if tra:use_sub_accounts = 'YES'
                  end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
              end!if access:subtracc.fetch(sub:account_number_key) = level:benign
              If fetch_error# = 0
                  ret:parts_cost  = 0
  
                  save_res_id = access:retstock.savefile()
                  access:retstock.clearkey(res:despatched_only_key)
                  res:ref_number = ret:ref_number
                  res:despatched = 'YES'
                  set(res:despatched_only_key,res:despatched_only_key)
                  loop
                      if access:retstock.next()
                         break
                      end !if
                      if res:ref_number <> ret:ref_number       |
                      or res:despatched <> 'YES'      |
                          then break.  ! end if
  
                      ret:parts_cost  += Round(res:item_cost * res:quantity,.01)
                  end !loop
                  access:retstock.restorefile(save_res_id)
  
                  ret:sub_total   = Round(ret:parts_cost,.01) + ret:courier_cost
                  access:retsales.update()
  
                  If sage_error# = 0
                      invoice_error# = 1
                      get(invoice,0)
                      if access:invoice.primerecord() = level:benign
                          inv:invoice_type       = 'RET'
                          inv:job_number         = ret:ref_number
                          inv:date_created       = Today()
                          If tra:invoice_sub_accounts = 'YES' and tra:Use_Sub_accounts = 'YES'
                              inv:AccountType = 'SUB'
                          Else!If tra:invoice_sub_accounts = 'YES'
                              inv:AccountType = 'MAI'
                          End!If tra:invoice_sub_accounts = 'YES'
                          inv:account_number     = ret:account_number
                          inv:total              = ret:sub_total
                          inv:vat_rate_labour    = ''
                          inv:vat_rate_parts     = ''
                          inv:vat_rate_retail    = Round(retail_rate$,.01)
                          inv:vat_number         = def:vat_number
                          INV:Courier_Paid       = ret:courier_cost
                          inv:parts_paid         = ret:parts_cost
                          inv:labour_paid        = ''
                          inv:invoice_vat_number = vat_number"
                          invoice_error# = 0
                          If UseEuro#
                              inv:UseAlternativeAddress   = 1
                              inv:EuroExhangeRate = def:EuroRate
                          Else
                              inv:UseAlternativeAddress   = 0
                              inv:EuroExhangeRate = def:EuroRate
                          End !If UseEuro#
                          If access:invoice.insert()
                              invoice_error# = 1
                              access:invoice.cancelautoinc()
                          End!If access:invoice.insert()
                      end!if access:invoice.primerecord() = level:benign
                      If Invoice_error# = 0
                          ret:invoice_number        = inv:invoice_number
                          ret:invoice_date          = Today()
                          ret:invoice_courier_cost  = ret:courier_cost
                          ret:invoice_parts_cost    = ret:parts_cost
                          ret:invoice_sub_total     = ret:sub_total
                          access:retsales.update()
  
                          ! Moved BEFORE the invoice to try and avoid losing information - TrkBs: 5110 (DBH: 26-08-2005)
                          Line500_XML('STF')
  
                            ! #13608 Put the logging in the correct place. (DBH: 28/09/2015)
                            MakeInvoiceLog('STF')
  
                          Retail_Single_Invoice(0,inv:Invoice_Number)   !0 implies first time invoice
  
                      End!If Invoice_error# = 0
                  End!If def:use_sage = 'YES' and sage_error# = 1
              End!If fetch_error# = 0
  
          End!If tmp:createinvoice = 1
  
          If CheckPayment#
              If ret:Payment_Method = 'CAS'
                  If RetailPaid() = Level:Fatal
                      ret:Despatched = 'PAY'
                      Access:RETSALES.Update()
                  End !If RetailPaid() = Level:Benign
              End !If ret:Payment_Method = 'CAS'
          End !If CheckPayment#
  
      Else! If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
  
  
      access:vatcode.close()
      relate:retsales.close()
      access:retdesno.close()
      relate:tradeacc.close()
      relate:invoice.close()
      Relate:WAYBILLS.Close()
      Relate:WAYBCONF.Close()
      Relate:WAYBILLJ.Close()
  
  
      glo:select1  = ''
  
      Access:DEFAULTS.Close()
  End!IF thiswindow.request <> requestcancelled
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  Case request
      Of insertrecord
          do_update# = False
          access:RETSTOCK.cancelautoinc()
      Of changerecord
  !        If res:despatched <> 'YES'
  !            request = Viewrecord
  !        End!If res:despatched <> 'YES'
      Of deleterecord
          do_update# = False
  End!Case request
  
  If do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Retail_Stock(0,1)
    ReturnValue = GlobalResponse
  END
  End!If do_update# = true
      !glo:Select2 = Flag
      !glo:Select3 = Stock Ref number
      !glo:Select4 = RETSTOCK record number
      !glo:Select5 = Quantity
      !glo:Select6 = OPDPEND record number, not needed anymore
      If glo:select2   = 'NEW PENDING' or glo:Select2 = 'CANCELLED'
          UpdateRetailPartRoutine()
      End !If glo:select2   = 'NEW PENDING'
      If glo:Select7 = 'DELETE'
          Access:RETSTOCK.ClearKey(res:Record_Number_Key)
          res:Record_Number = glo:Select4
          If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
              !Found
              Delete(RETSTOCK)
          Else!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
      End !If glo:Select7 = 'DELETE'
      glo:select1     = ''
      glo:Select2     = ''
      glo:Select3     = ''
      glo:Select5     = ''
      glo:Select4     = ''
      glo:Select7     = ''
  
  BRW3.ResetSort(1)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020335'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020335'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020335'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?button:AddStartScanning
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?button:AddStartScanning, Accepted)
      local.StartScanning(0)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?button:AddStartScanning, Accepted)
    OF ?button:AddStopScanning
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?button:AddStopScanning, Accepted)
      local.StopScanning()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?button:AddStopScanning, Accepted)
    OF ?button:RemoveStartScanning
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?button:RemoveStartScanning, Accepted)
      local.StartScanning(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?button:RemoveStartScanning, Accepted)
    OF ?button:RemoveStopScanning
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?button:RemoveStopScanning, Accepted)
      local.StopScanning()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?button:RemoveStopScanning, Accepted)
    OF ?locScannedPartNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locScannedPartNumber, Accepted)
      if (?locScannedPartNumber{prop:Hide} = 0 and clip(locScannedPartNumber) <> '')
          if (?prompt:AddItem{prop:Hide} = 0)
      
              found# = 0
              avail# = 0
              Access:RETSTOCK.Clearkey(res:DespatchedPartKey)
              res:Ref_Number    = ret:Ref_Number
              res:Despatched    = 'PIK'
              res:Part_Number    = locScannedPartNumber
              set(res:DespatchedPartKey,res:DespatchedPartKey)
              loop
                  if (Access:RETSTOCK.Next())
                      Break
                  end ! if (Access:RETSTOCK.Next())
                  if (res:Ref_Number    <> ret:Ref_Number)
                      Break
                  end ! if (res:Ref_Number    <> ret:Ref_Number)
                  if (res:Despatched    <> 'PIK')
                      Break
                  end ! if (res:Despatched    <> 'PIK')
                  if (res:Part_Number    <> locScannedPartNumber)
                      Break
                  end ! if (res:Part_Number    <> locScannedPartNumber)
                  found# = 1
                  glo:Pointer = res:Record_Number
                  Add(glo:Queue)
      
                  ! User has scanned part. So remove any flag that says it was manually tagged (DBH: 03/06/2009) #10311
                  mantag:RecordNumber = res:Record_Number
                  get(tagManualQueue,mantag:RecordNumber)
                  if (~error())
                      delete(tagManualQueue)
                  end ! if (~error())
      
                  if (res:ScannedQty >= res:Quantity)
                      Cycle
                  end ! if (res:ScannedQty >= res:Quantity)
                  avail# = 1
      
      
      
                  res:ScannedQty += 1
                  access:RETSTOCK.TryUPdate()
                  locScannedPartNumber = ''
                  break
              end ! loop
              brw3.resetsort(1)
      
              if (found# = 0)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Cannot find the selected Part Number.','ServiceBase',|
                                 'mstop.jpg','\&Stop|/&Continue')
                      Of 2 ! &Continue Button
                      Of 1 ! &Stop Button
                          local.StopScanning()
                          cycle
                  End!Case Message
              else ! if (found# = 0)
                  if (avail# = 0)
                      Beep(Beep:SystemHand)  ;  Yield()
                      Case Missive('There are no more items left to be scanned for the selected part.','ServiceBase',|
                                     'mstop.jpg','/&OK')
                          Of 1 ! &OK Button
                      End!Case Message
                  end ! if (found# = 0)
              end ! if (found# = 0)
      
              if (local.allPicked())
                  cycle
                  !break
              else
                  select(?locScannedPartNumber)
              end !if (allPicked())
      
      
          end ! if (?prompt:AddItem{prop:Hide} = 0)
      
          if (?prompt:RemoveItem{prop:Hide} = 0)
              found# = 0
              avail# = 0
              Access:RETSTOCK.Clearkey(res:DespatchedPartKey)
              res:Ref_Number    = ret:Ref_Number
              res:Despatched    = 'PIK'
              res:Part_Number    = locScannedPartNumber
              set(res:DespatchedPartKey,res:DespatchedPartKey)
              loop
                  if (Access:RETSTOCK.Next())
                      Break
                  end ! if (Access:RETSTOCK.Next())
                  if (res:Ref_Number    <> ret:Ref_Number)
                      Break
                  end ! if (res:Ref_Number    <> ret:Ref_Number)
                  if (res:Despatched    <> 'PIK')
                      Break
                  end ! if (res:Despatched    <> 'PIK')
                  if (res:Part_Number    <> locScannedPartNumber)
                      Break
                  end ! if (res:Part_Number    <> locScannedPartNumber)
                  found# = 1
                  ! User has scanned part. So remove any flag that says it was manually tagged (DBH: 03/06/2009) #10311
                  mantag:RecordNumber = res:Record_Number
                  get(tagManualQueue,mantag:RecordNumber)
                  if (~error())
                      delete(tagManualQueue)
                  end ! if (~error())
      
                  if (res:ScannedQty = 0)
                      Cycle
                  end ! if (res:ScannedQty >= res:Quantity)
                  avail# = 1
      
                  res:ScannedQty -= 1
                  if (res:ScannedQty < 0)
                      res:ScannedQty = 0
                  end ! if (res:ScannedQty < 0)
                  access:RETSTOCK.TryUPdate()
                  locScannedPartNumber = ''
                  if (res:ScannedQty = 0)
                      ! Remove the tag
                      glo:pointer = res:Record_Number
                      get(glo:Queue,glo:pointer)
                      if (~error())
                          delete(glo:Queue)
                      end ! if (~error())
                  end ! if (res:ScannedQty = 0)
                  break
              end ! loop
              brw3.resetsort(1)
      
      
              if (found# = 0)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Cannot find the selected Part Number.','ServiceBase',|
                                 'mstop.jpg','\&Stop|/&Continue')
                      Of 2 ! &Continue Button
                      Of 1 ! &Stop Button
                          local.StopScanning()
                          cycle
                  End!Case Message
              else ! if (found# = 0)
                  if (avail# = 0)
                      Beep(Beep:SystemHand)  ;  Yield()
                      Case Missive('All items have been unselected for this part.','ServiceBase',|
                                     'mstop.jpg','/&OK')
                      Of 1 ! &OK Button
                      End!Case Message
                  end ! if (found# = 0)
              end ! if (found# = 0)
              select(?locScannedPartNumber)
          End !     if (?prompt:RemoveItem{prop:Hide} = 0)
      end ! if (?locScannedPartNumber{prop:Hide} = 0)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?locScannedPartNumber, Accepted)
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ProcessTagged
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessTagged, Accepted)
      Case Missive('Are you sure you want to marked the tagged items as picked?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              backOrder# = 0
              backOrderFound# = 0
              foundScanned# = 0
              loop x# = 1 to records(glo:Queue)
                  get(glo:Queue,x#)
                  Access:RETSTOCK.Clearkey(res:Record_Number_Key)
                  res:Record_Number    = glo:Pointer
                  if (Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign)
                      if (res:Despatched = 'PIK')
                          mantag:RecordNumber = glo:Pointer
                          get(tagManualQueue,mantag:RecordNumber)
                          if (error())
                              foundScanned# = 1
                              ! Found
                              if (res:ScannedQty < res:Quantity)
                                  if (backOrderFound# = 0)
                                      Beep(Beep:SystemQuestion)  ;  Yield()
                                      Case Missive('One or more of the parts have not had the full quantity scanned.'&|
                                          '|The difference will be returned to stock.'&|
                                          '|'&|
                                          '|Do you wish to create a back order for this quantity?','ServiceBase',|
                                                     'mquest.jpg','\&No|/&Yes')
                                      Of 2 ! &Yes Button
                                          backOrder# = 1
                                      Of 1 ! &No Button
                                      End!Case Message
                                  end ! if (backOrderFound# = 0)
                                  backOrderFound# = 1
      
                                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                  sto:Ref_Number    = res:Part_Ref_Number
                                  if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                                      ! Found
                                      sto:Quantity_Stock += (res:Quantity - res:ScannedQty)
                                      if (Access:STOCK.TryUpdate() = Level:Benign)
                                          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                             'REC', | ! Transaction_Type
                                                             '', | ! Depatch_Note_Number
                                                             ret:Ref_Number, | ! Job_Number
                                                             0, | ! Sales_Number
                                                             (res:Quantity - res:ScannedQty), | ! Quantity
                                                             sto:Purchase_Cost, | ! Purchase_Cost
                                                             sto:Sale_Cost, | ! Sale_Cost
                                                             sto:Retail_Cost, | ! Retail_Cost
                                                             'RETAIL ITEM RECREDITED', | ! Notes
                                                             'COMPANY: ' & Clip(ret:company_name) & '<13,10>PURCHASE ORDER NO: ' & |
                                                                      Clip(ret:purchase_order_number) & '<13,10>CONTACT: ' & Clip(ret:contact_name)) ! Information
                                            ! Added OK
                                          Else ! AddToStockHistory
                                            ! Error
                                          End ! AddToStockHistory
      
                                          ! Update item with new "Scanned" quantity (DBH: 03/06/2009) #10311
                                          oldQty# = (res:Quantity - res:ScannedQty)
                                          res:Quantity = res:ScannedQty
                                          Access:RETSTOCK.TryUpdate()
                                          if (backOrder# = 1)
                                              glo:select2  = 'NEW PENDING'
                                              glo:select3  = sto:ref_number
                                              glo:select4  = res:record_number
                                              glo:select5  = oldQty#
                                              UpdateRetailPartRoutine()
                                              glo:Select2 = ''
                                              glo:Select3 = ''
                                              glo:Select4 = ''
                                              glo:Select5 = ''
                                          end ! if (backOrder# = 1)
                                      end !if (Access:STOCK.TryUpdate() = Level:Benign)
                                  else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                                      ! Error
                                  end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                              end ! if (res:ScannedQty < res:Quantity)
      
                          end  ! if (~error())
      
      
                          Access:RETSTOCK.Clearkey(res:Record_Number_Key)
                          res:Record_Number    = glo:Pointer
                          if (Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign)
                              ! Found
                              ! Reget and update (DBH: 03/06/2009) #10311
                              res:Despatched = 'YES'
                              res:QuantityReceived    = res:Quantity
                              res:ScannedQty = 0
                              Access:RETSTOCK.Update()
      
                          else ! if (Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign)
                              ! Error
                          end ! if (Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign)
      
                      end!If res:Despatched = 'PIK'
                  else ! if (Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign)
      
              end !loop x# = 1 to records(glo:Queue)
      ! Delete --- Replaced (DBH: 03/06/2009) #10311
      !        Loop x# = 1 To Records(glo:Queue)
      !            Get(glo:Queue,x#)
      !            Access:RETSTOCK.ClearKey(res:Record_Number_Key)
      !            res:Record_Number = glo:Pointer
      !            If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
      !                If res:Despatched = 'PIK'
      !                    res:Despatched = 'YES'
      !                    res:QuantityReceived    = res:Quantity
      !                    Access:RETSTOCK.Update()
      !                End!If res:Despatched = 'PIK'
      !            End!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
      !        End!Loop x# = 1 To Records(glo:Queue)
      !
      ! end --- (DBH: 03/06/2009) #10311
          Of 1 ! No Button
      End ! Case Missive
      Do ShowHideUnPicking
      Do DASBRW::9:DASUNTAGALL
      BRW3.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessTagged, Accepted)
    OF ?ReturnTagged
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnTagged, Accepted)
      Case Missive('Are you sure you want to return the tagged (non-picked) items to stock?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  Access:RETSTOCK.ClearKey(res:Record_Number_Key)
                  res:Record_Number = glo:Pointer
                  If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                      If res:Despatched = 'PIK'
                          Access:STOCK.ClearKey(sto:Ref_Number_Key)
                          sto:Ref_Number = res:Part_Ref_Number
                          If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                              sto:Quantity_Stock += res:Quantity
                              Access:STOCK.Update()
                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                 'ADD', | ! Transaction_Type
                                                 '', | ! Depatch_Note_Number
                                                 res:Ref_Number, | ! Job_Number
                                                 0, | ! Sales_Number
                                                 res:Quantity, | ! Quantity
                                                 res:Purchase_Cost, | ! Purchase_Cost
                                                 res:Sale_Cost, | ! Sale_Cost
                                                 res:Retail_Cost, | ! Retail_Cost
                                                 'RETAIL ITEM RESTOCKED', | ! Notes
                                                 '') ! Information
                                ! Added OK
                              Else ! AddToStockHistory
                                ! Error
                              End ! AddToStockHistory
                              res:Despatched = 'RES'
                              Access:RETSTOCK.Update()
                          End!If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                      End!If res:Despatched = 'PIK'
                  End!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
              End!Loop x# = 1 To Records(glo:Queue)
          Of 1 ! No Button
      End ! Case Missive
      BRW3.ResetSort(1)
      Do DASBRW::9:DASUNTAGALL
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnTagged, Accepted)
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Payments
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payments, Accepted)
      Browse_Retail_payments (ret:ref_number,ret:account_number,ret:courier_cost,'YES')
      Do CheckPayment
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payments, Accepted)
    OF ?PrintInvoice
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintInvoice, Accepted)
      tmp:PrintInvoice = 0
      If ret:Invoice_Number <> ''
          Case Missive('This sale has already been invoiced.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If ret:Invoice_Number <> ''
          ! Only allow to print the invoice, if all the sales have been picked (DBH: 30-08-2005)
      
          !Picking Finished? Autoprint Despatch Note?
          Finished# = 0
      
          Access:RETSTOCK.Clearkey(res:Despatched_Only_Key)
          res:Ref_Number  = ret:Ref_Number
          res:Despatched  = 'PIK'
          If Access:RETSTOCK.Tryfetch(res:Despatched_Only_Key) = Level:Benign
              ! Found
              Case Missive('You cannot create an invoice until all the sale items have been picked, or placed on back order.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Else ! If Access:RETSTOCK.Tryfetch(res:Despatched_Only_Key) = Level:Benign
              ! Error
              Case Missive('Are you sure you want to create an Invoice for this sale?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Case Missive('A Invoice will be created when you complete this screen.','ServiceBase 3g',|
                                     'midea.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
      
                      tmp:PrintInvoice = 1
                      
                  Of 1 ! No Button
              End ! Case Missive
      
          End ! If Access:RETSTOCK.Tryfetch(res:Despatched_Only_Key) = Level:Benign
      End!If ret:Invoice_Number <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintInvoice, Accepted)
    OF ?PrintDespatchNote
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintDespatchNote, Accepted)
      tmp:PrintDespatchNote = 0
      If ret:Consignment_Number <> ''
          Case Missive('This sale has already been despatched.'&|
            '<13,10>'&|
            '<13,10>Do you want to reprint the Despatch Note?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  tmp:PrintDespatchNote = 1
              Of 1 ! No Button
          End ! Case Missive
      Else!If ret:Despatch_Number <> ''
          IF glo:WebJob = 0 THEN
              Case Missive('Are you sure you want to Despatch this sale?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                    tmp:PrintDespatchNote = 1
                  Of 1 ! No Button
              End ! Case Missive
          ELSE
              tmp:PrintDespatchNote = 1
          END !IF
      End!If ret:Despatch_Number <> ''
      
      
      if glo:WebJob = 1
          if tmp:PrintDespatchNote = 1
              if ret:despatch_number <> 0
                  ! Inserting (DBH 20/10/2006) # 8381 - Print two copies of the despatch note
                  glo:ReportCopies = 2
                  ! End (DBH 20/10/2006) #8381
                  Retail_Despatch_Note(ret:Despatch_Number)
              end
          end
      else
          If tmp:PrintDespatchNote = 1
              Case Missive('A Despatch Note will be printed when the complete this screen.','ServiceBase 3g',|
                             'midea.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          End !If tmp:PrintDespatchNote = 1
      end
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintDespatchNote, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Check Invoice And Change It
  access:invoice.clearkey(inv:invoice_number_key)
  inv:invoice_number = ret:invoice_number
  if access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
      cost$ = 0
      setcursor(cursor:wait)
      save_res_id = access:retstock.savefile()
      access:retstock.clearkey(res:despatched_only_key)
      res:ref_number = ret:ref_number
      res:despatched = 'YES'
      set(res:despatched_only_key,res:despatched_only_key)
      loop
          if access:retstock.next()
             break
          end !if
          if res:ref_number <> ret:ref_number      |
          or res:despatched <> 'YES'      |
              then break.  ! end if
          cost$  += Round(res:item_cost * res:quantity,.01)
      end !loop
      access:retstock.restorefile(save_res_id)
      setcursor()
  
      If inv:parts_paid <> cost$
          ret:parts_cost = Round(cost$,.01)
          ret:sub_total   = Round(ret:parts_cost,.01) + ret:courier_cost
          inv:parts_paid = ret:parts_cost
          inv:total      = ret:sub_total
          access:invoice.update()
          access:retsales.update()
      End!If inv:parts_paid <> ret:parts_cost
  end!if access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
  !Picking Finished? Autoprint Despatch Note?
      Finished# = 0
      Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
      res:Ref_Number = ret:Ref_Number
      res:Despatched = 'PIK'
      Set(res:Despatched_Only_Key,res:Despatched_Only_Key)
      If Access:RETSTOCK.NEXT()
          Finished# = 1
      Else !If Access:RETSTOCK.NEXT()
          If res:Ref_Number <> ret:Ref_Number Or |
              res:Despatched <> 'PIK'
              Finished# = 1
          Else !If res:Ref_Number <> ref:Ref_Number Or |
              Finished# = 0
          End !If res:Ref_Number <> ref:Ref_Number Or |
      End !Access:RETSTOCK.NEXT()
      If Finished# = 1
  !Only print an invoice/despatch note if there are picked items
          Save_res_ID = Access:RETSTOCK.SaveFile()
          Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
          res:Ref_Number = ret:Ref_Number
          res:Despatched = 'YES'
          Set(res:Despatched_Only_Key,res:Despatched_Only_Key)
          If Access:RETSTOCK.NEXT() = Level:Benign
              If res:Despatched = 'YES' And res:Ref_Number = ret:Ref_Number
                  If ret:Invoice_Number = 0
                      tmp:PrintInvoice = 1
                  End !If ret:Invoice_Number = 0
                  If ret:Despatch_Number = 0
                      tmp:PrintDespatchNote = 1
                  End !If ret:Despatch_Number = 0
              End !If res:Despatched = 'YES' And res:Ref_Number = ret:Ref_Number
          End !If Access:RETSTOCK.NEXT() = Level:Benign
      End !If Finished# = 1
  !Sale Paid?
      If RetailPaid() = Level:Benign
          If ret:Despatched = 'PAY'
              ret:Despatched = 'PRO'
          End !If ret:Despatched = 'PAY'
      Else!If RetailPaid() = Level:Benign
          ret:Despatched = 'PAY'
      End !If RetailPaid() = Level:Benign
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet4
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet4)
        OF 1
          ?List:2{PROP:FORMAT} ='11L(2)*J@n1@#1#125L(2)|M*~Part Number~@s30@#7#126L(2)|M*~Description~@s30@#12#35R(2)|M*~Total~@n8@#17#39R(2)|M*~Scanned~@s8@#22#44R(2)|M*~Outstanding~@s8@#27#92L(2)|M*~Purchase Order Number~@s30@#32#'
          ?Tab4{PROP:TEXT} = 'Un-Picked Items'
        OF 2
          ?List:2{PROP:FORMAT} ='125L(2)|M*~Part Number~@s30@#7#126L(2)|M*~Description~@s30@#12#35R(2)|M*~Total~@n8@#17#92L(2)|M*~Purchase Order Number~@s30@#32#'
          ?Tab3{PROP:TEXT} = 'Picked Items'
        OF 3
          ?List:2{PROP:FORMAT} ='125L(2)|M*~Part Number~@s30@#7#126L(2)|M*~Description~@s30@#12#35R(2)|M*~Total~@n8@#17#92L(2)|M*~Purchase Order Number~@s30@#32#97R(2)|M*~Sales No~L@s8b@#37#'
          ?Tab5{PROP:TEXT} = 'Back Orders'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ?prompt1{prop:text} = 'Customer Details.  Sale No: ' & ret:ref_number
      
      If RET:Building_Name_Delivery   = ''
          address_line1_temp  = Clip(ret:address_line1_delivery)
      Else!If RET:Building_Name_Delivery   = ''
          address_line1_temp  = Clip(ret:building_name_delivery) & ' ' & Clip(ret:address_line1_delivery)
      End!If RET:Building_Name_Delivery   = ''
      access:users.clearkey(use:user_code_key)
      use:user_code = ret:who_booked
      if access:users.tryfetch(use:user_code_key) = Level:Benign
          raised_by_temp  = Clip(use:forename) & ' ' & Clip(use:surname)
      end!if access:users.tryfetch(use:user_code_key) = Level:Benign
      
      Case ret:payment_method
          OF 'TRA'
              payment_status_temp = 'Trade'
          Of 'CAS'
              payment_status_temp = 'Cash'
          Of 'EXC'
              Payment_Status_Temp = 'Exchange Accessory'
      End!Case ret:payment_method
      
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      
      Do DASBRW::9:DASUNTAGALL
      Do ShowHideUnPicking
      IF glo:WebJob AND ?Tab4{PROP:Hide} = False THEN
         HIDE(?PrintDespatchNote)
      END !IF
      BRW3.ResetSort(1)
      Post(Event:NewSelection,?Sheet4)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
local.allPicked     Procedure()
save_RETSTOCK_id        UShort(),Auto
Code
    save_RETSTOCK_ID = Access:RETSTOCK.SaveFile()
    Access:RETSTOCK.Clearkey(res:DespatchedPartKey)
    res:Ref_Number    = ret:Ref_Number
    res:Despatched    = 'PIK'
    set(res:DespatchedPartKey,res:DespatchedPartKey)
    loop
        if (Access:RETSTOCK.Next())
            Break
        end ! if (Access:RETSTOCK.Next())
        if (res:Ref_Number    <> ret:Ref_Number)
            Break
        end ! if (res:Ref_Number    <> ret:Ref_Number)
        if (res:Despatched    <> 'PIK')
            Break
        end ! if (res:Despatched    <> 'PIK')

        glo:Pointer = res:Record_Number
        get(glo:Queue,glo:Pointer)
        if (error())
            !Item Hasn't Been Tagged
            found# = 1
            break
        end ! if (error())

        mantag:RecordNumber = res:Record_Number
        get(tagManualQueue,mantag:RecordNumber)
        if (error())
            !Item Hasn't Been Manually Taged. Have all the items been scanned
            if (res:ScannedQty < res:Quantity)
                found# = 1
                break
            end ! if (res:ScannedQty < res:Quantity)
        end ! if (error())
    end ! loop
    Access:RETSTOCK.RestoreFile(save_RETSTOCK_ID)

    ! Have all the items been picked?
    if (found# = 1)

        return 0
    else !if (found# = 0)
        local.StopScanning()
        return 1
    end ! if (found# = 0)
local.StartScanning   Procedure(Byte fType)
code
    ?dasTag{prop:Hide} = 1
    ?dasTagAll{prop:Hide} = 1
    ?dasUnTagAll{prop:Hide} = 1
    ?change{prop:Hide} = 1
    ?processTagged{prop:Hide} = 1
    ?returnTagged{prop:Hide} = 1
    ?payments{prop:Hide} = 1
    ?printInvoice{prop:Hide} = 1
    ?ok{prop:Hide} = 1
    ?close{prop:Hide} = 1

    ?locScannedPartNumber{prop:Hide} = 0
    ?locScannedPartNumber:Prompt{prop:Hide} = 0

    case fType
    of 0 ! Add
        ?prompt:RemoveItem{prop:Hide} = 1
        ?button:AddStartScanning{prop:Hide} = 1
        ?button:AddStopScanning{prop:Hide} = 0
        ?button:RemoveStartScanning{prop:Hide} = 1
        ?button:RemoveStopScanning{prop:Hide} = 1
    of 1 ! Delete
        ?prompt:AddItem{prop:Hide} = 1
        ?button:RemoveStartScanning{prop:Hide} = 1
        ?button:RemoveStopScanning{prop:Hide} = 0
        ?button:AddStartScanning{prop:Hide} = 1
        ?button:AddStopScanning{prop:Hide} = 1
    end ! case fType
    select(?locScannedPartNumber)
local.StopScanning   Procedure()
code
    ?dasTag{prop:Hide} = 0
    ?dasTagAll{prop:Hide} = 0
    ?dasUnTagAll{prop:Hide} = 0
    ?change{prop:Hide} = 0
    ?processTagged{prop:Hide} = 0
    ?returnTagged{prop:Hide} = 0
    ?payments{prop:Hide} = 0
    ?printInvoice{prop:Hide} = 0
    ?ok{prop:Hide} = 0
    ?close{prop:Hide} = 0

    ?locScannedPartNumber{prop:Hide} = 1
    ?locScannedPartNumber:Prompt{prop:Hide} = 1

    ?prompt:AddItem{prop:Hide} = 0
    ?prompt:RemoveItem{prop:Hide} = 0

    ?button:AddStartScanning{prop:Hide} = 0
    ?button:AddStopScanning{prop:Hide} = 1
    ?button:RemoveStartScanning{prop:Hide} = 0
    ?button:RemoveStopScanning{prop:Hide} = 1
    locScannedPartNumber = ''
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW3.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?Sheet4) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = ret:Ref_Number
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'PIK'
  ELSIF Choice(?Sheet4) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = ret:Ref_Number
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'YES'
  ELSIF Choice(?Sheet4) = 3
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = ret:Ref_Number
  ELSE
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW3.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW3.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet4) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?Sheet4) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?Sheet4) = 3
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW3.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(3, SetQueueRecord, ())
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = res:Record_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  tmp:OutstandingQty = res:Quantity - res:ScannedQty
  PARENT.SetQueueRecord
  SELF.Q.tmp:tag_NormalFG = -1
  SELF.Q.tmp:tag_NormalBG = -1
  SELF.Q.tmp:tag_SelectedFG = -1
  SELF.Q.tmp:tag_SelectedBG = -1
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END
  SELF.Q.res:Part_Number_NormalFG = -1
  SELF.Q.res:Part_Number_NormalBG = -1
  SELF.Q.res:Part_Number_SelectedFG = -1
  SELF.Q.res:Part_Number_SelectedBG = -1
  SELF.Q.res:Description_NormalFG = -1
  SELF.Q.res:Description_NormalBG = -1
  SELF.Q.res:Description_SelectedFG = -1
  SELF.Q.res:Description_SelectedBG = -1
  SELF.Q.res:Quantity_NormalFG = -1
  SELF.Q.res:Quantity_NormalBG = -1
  SELF.Q.res:Quantity_SelectedFG = -1
  SELF.Q.res:Quantity_SelectedBG = -1
  SELF.Q.res:ScannedQty_NormalFG = -1
  SELF.Q.res:ScannedQty_NormalBG = -1
  SELF.Q.res:ScannedQty_SelectedFG = -1
  SELF.Q.res:ScannedQty_SelectedBG = -1
  SELF.Q.tmp:OutstandingQty_NormalFG = -1
  SELF.Q.tmp:OutstandingQty_NormalBG = -1
  SELF.Q.tmp:OutstandingQty_SelectedFG = -1
  SELF.Q.tmp:OutstandingQty_SelectedBG = -1
  SELF.Q.res:Purchase_Order_Number_NormalFG = -1
  SELF.Q.res:Purchase_Order_Number_NormalBG = -1
  SELF.Q.res:Purchase_Order_Number_SelectedFG = -1
  SELF.Q.res:Purchase_Order_Number_SelectedBG = -1
  SELF.Q.res:Previous_Sale_Number_NormalFG = -1
  SELF.Q.res:Previous_Sale_Number_NormalBG = -1
  SELF.Q.res:Previous_Sale_Number_SelectedFG = -1
  SELF.Q.res:Previous_Sale_Number_SelectedBG = -1
  SELF.Q.tmp:BrowseType_NormalFG = -1
  SELF.Q.tmp:BrowseType_NormalBG = -1
  SELF.Q.tmp:BrowseType_SelectedFG = -1
  SELF.Q.tmp:BrowseType_SelectedBG = -1
  SELF.Q.tmp:OutstandingQty = tmp:OutstandingQty      !Assign formula result to display queue
  tmp:BrowseType = 0
  If res:Despatched = 'PEN'
      tmp:BrowseType = 1 !Green
  End!If res:Despatched = 'PEN'
  If res:Despatched = 'ORD'
      tmp:BrowseType = 2 !Red
  End!If res:Despatched = 'ORD'
  If res:Despatched = 'OLD'
      tmp:BrowseType = 3 !Gray
  End!If res:Despatched = 'OLD'
  If res:Despatched = 'CAN'
      tmp:BrowseType = 4 !Black, black!
  End !If res:Despatched = 'CAN'
  If res:Despatched = 'PIK'
      tmp:BrowseType = 5 !Blue
  End !If res:Despatched = 'PIK'
  
   
   
   IF (tmp:BrowseType = 0)
     SELF.Q.tmp:tag_NormalFG = 0
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.tmp:tag_NormalFG = 32768
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.tmp:tag_NormalFG = 255
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.tmp:tag_NormalFG = 8421504
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.tmp:tag_NormalFG = 16777215
     SELF.Q.tmp:tag_NormalBG = 0
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.tmp:tag_NormalFG = 8388736
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 8388736
   ELSE
     SELF.Q.tmp:tag_NormalFG = 0
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.res:Part_Number_NormalFG = 0
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.res:Part_Number_NormalFG = 32768
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.res:Part_Number_NormalFG = 255
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.res:Part_Number_NormalFG = 8421504
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.res:Part_Number_NormalFG = 16777215
     SELF.Q.res:Part_Number_NormalBG = 0
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.res:Part_Number_NormalFG = 8388736
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 8388736
   ELSE
     SELF.Q.res:Part_Number_NormalFG = 0
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.res:Description_NormalFG = 0
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.res:Description_NormalFG = 32768
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.res:Description_NormalFG = 255
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.res:Description_NormalFG = 8421504
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.res:Description_NormalFG = 16777215
     SELF.Q.res:Description_NormalBG = 0
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.res:Description_NormalFG = 8388736
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 8388736
   ELSE
     SELF.Q.res:Description_NormalFG = 0
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.res:Quantity_NormalFG = 0
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.res:Quantity_NormalFG = 32768
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.res:Quantity_NormalFG = 255
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.res:Quantity_NormalFG = 8421504
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.res:Quantity_NormalFG = 16777215
     SELF.Q.res:Quantity_NormalBG = 0
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.res:Quantity_NormalFG = 8388736
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 8388736
   ELSE
     SELF.Q.res:Quantity_NormalFG = 0
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.res:ScannedQty_NormalFG = 0
     SELF.Q.res:ScannedQty_NormalBG = 16777215
     SELF.Q.res:ScannedQty_SelectedFG = 16777215
     SELF.Q.res:ScannedQty_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.res:ScannedQty_NormalFG = 32768
     SELF.Q.res:ScannedQty_NormalBG = 16777215
     SELF.Q.res:ScannedQty_SelectedFG = 16777215
     SELF.Q.res:ScannedQty_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.res:ScannedQty_NormalFG = 255
     SELF.Q.res:ScannedQty_NormalBG = 16777215
     SELF.Q.res:ScannedQty_SelectedFG = 16777215
     SELF.Q.res:ScannedQty_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.res:ScannedQty_NormalFG = 8421504
     SELF.Q.res:ScannedQty_NormalBG = 16777215
     SELF.Q.res:ScannedQty_SelectedFG = 16777215
     SELF.Q.res:ScannedQty_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.res:ScannedQty_NormalFG = 16777215
     SELF.Q.res:ScannedQty_NormalBG = 0
     SELF.Q.res:ScannedQty_SelectedFG = 16777215
     SELF.Q.res:ScannedQty_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.res:ScannedQty_NormalFG = 8388736
     SELF.Q.res:ScannedQty_NormalBG = 16777215
     SELF.Q.res:ScannedQty_SelectedFG = 16777215
     SELF.Q.res:ScannedQty_SelectedBG = 8388736
   ELSE
     SELF.Q.res:ScannedQty_NormalFG = 0
     SELF.Q.res:ScannedQty_NormalBG = 16777215
     SELF.Q.res:ScannedQty_SelectedFG = 16777215
     SELF.Q.res:ScannedQty_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.tmp:OutstandingQty_NormalFG = 0
     SELF.Q.tmp:OutstandingQty_NormalBG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedFG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.tmp:OutstandingQty_NormalFG = 32768
     SELF.Q.tmp:OutstandingQty_NormalBG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedFG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.tmp:OutstandingQty_NormalFG = 255
     SELF.Q.tmp:OutstandingQty_NormalBG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedFG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.tmp:OutstandingQty_NormalFG = 8421504
     SELF.Q.tmp:OutstandingQty_NormalBG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedFG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.tmp:OutstandingQty_NormalFG = 16777215
     SELF.Q.tmp:OutstandingQty_NormalBG = 0
     SELF.Q.tmp:OutstandingQty_SelectedFG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.tmp:OutstandingQty_NormalFG = 8388736
     SELF.Q.tmp:OutstandingQty_NormalBG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedFG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedBG = 8388736
   ELSE
     SELF.Q.tmp:OutstandingQty_NormalFG = 0
     SELF.Q.tmp:OutstandingQty_NormalBG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedFG = 16777215
     SELF.Q.tmp:OutstandingQty_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 0
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 32768
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 255
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 8421504
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 16777215
     SELF.Q.res:Purchase_Order_Number_NormalBG = 0
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 8388736
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 8388736
   ELSE
     SELF.Q.res:Purchase_Order_Number_NormalFG = 0
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 0
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 32768
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 255
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 8421504
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 16777215
     SELF.Q.res:Previous_Sale_Number_NormalBG = 0
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 8388736
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 8388736
   ELSE
     SELF.Q.res:Previous_Sale_Number_NormalFG = 0
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.tmp:BrowseType_NormalFG = 0
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.tmp:BrowseType_NormalFG = 32768
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.tmp:BrowseType_NormalFG = 255
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.tmp:BrowseType_NormalFG = 8421504
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.tmp:BrowseType_NormalFG = 16777215
     SELF.Q.tmp:BrowseType_NormalBG = 0
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.tmp:BrowseType_NormalFG = 8388736
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 8388736
   ELSE
     SELF.Q.tmp:BrowseType_NormalFG = 0
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 8388608
   END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(3, SetQueueRecord, ())


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = res:Record_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Insert_Consignment_Note_Number PROCEDURE (f_ref_number,f_number,f_consignment_note_number) !Generated from procedure template - Window

FilesOpened          BYTE
consignment_note_number_temp STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Despatch Routine'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert Consignment Note Number'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Consigment Note Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(248,208,184,10),USE(consignment_note_number_temp),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(248,258),USE(?Address_Label),TRN,FLAT,LEFT,ICON('prnaddp.jpg')
                       BUTTON,AT(312,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(372,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020348'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Insert_Consignment_Note_Number')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Insert_Consignment_Note_Number')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Insert_Consignment_Note_Number')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020348'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020348'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020348'&'0')
      ***
    OF ?Address_Label
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Address_Label, Accepted)
      Set(defaults)
      access:defaults.next()
      glo:select1 = f_ref_number
      Loop x# = 1 to f_number
          Case def:label_printer_type
              Of 'TEC B-440 / B-442'
                  Address_Label_Retail
              Of 'TEC B-452'
                  Address_Label_Retail_B452
          End!Case def:themal_printer_type
      End!Loop x# = 1 to f_number
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Address_Label, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If consignment_note_number_temp <> ''
          f_consignment_note_number   = consignment_note_number_temp
          Post(event:closewindow)
      Else
          Select(?consignment_note_number_temp)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
          Case Missive('Warning! You have chosen to CANCEL the Despatch Procedure.'&|
            '<13,10>'&|
            '<13,10>Only continue if you are NOT going to despatch this unit at this time.','ServiceBase 3g',|
                         'mexclam.jpg','Cancel|Continue')
              Of 2 ! Continue Button
                  f_consignment_note_number = ''
                  Post(event:closewindow)
              Of 1 ! Cancel Button
          End ! Case Missive
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Browse_Retail_Sales PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
Account_Number       STRING(15)
RET_temp             STRING('RET')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Account_Number
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(RETSALES)
                       PROJECT(ret:Ref_Number)
                       PROJECT(ret:Purchase_Order_Number)
                       PROJECT(ret:Account_Number)
                       PROJECT(ret:date_booked)
                       PROJECT(ret:Courier)
                       PROJECT(ret:Despatched)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ret:Ref_Number         LIKE(ret:Ref_Number)           !List box control field - type derived from field
ret:Purchase_Order_Number LIKE(ret:Purchase_Order_Number) !List box control field - type derived from field
ret:Account_Number     LIKE(ret:Account_Number)       !List box control field - type derived from field
ret:date_booked        LIKE(ret:date_booked)          !List box control field - type derived from field
ret:Courier            LIKE(ret:Courier)              !List box control field - type derived from field
ret:Despatched         LIKE(ret:Despatched)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
QuickWindow          WINDOW('Sales Order Processing'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Sale Order Processing'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(164,82,352,272),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(4),FORMAT('50R(2)|M~Sales Number~L@s8@92L(2)|M~Purchase Order Number~@s30@69L(2)|M~Account ' &|
   'Number~@s15@47R(2)|M~Date Raised~@d6b@120L(2)|M~Courier~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(548,132),USE(?process_sale),TRN,FLAT,LEFT,ICON('prosalep.jpg')
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Sales Number'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(164,70,64,10),USE(ret:Ref_Number),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Purchase Order No'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(164,70,124,10),USE(ret:Purchase_Order_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Account Number'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(164,70,124,10),USE(ret:Purchase_Order_Number,,?RET:Purchase_Order_Number:2),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           COMBO(@s15),AT(292,70,124,10),USE(Account_Number),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('60L(2)|M@s15@120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2
BRW1::Sort2:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 3
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020332'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Retail_Sales')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RETSALES.Open
  Relate:SUBTRACC.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RETSALES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Retail_Sales')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,ret:Despatched_Ref_Key)
  BRW1.AddRange(ret:Despatched,RET_temp)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?ret:Ref_Number,ret:Ref_Number,1,BRW1)
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,ret:Despatched_Purchase_Key)
  BRW1.AddRange(ret:Despatched,RET_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?RET:Purchase_Order_Number,ret:Purchase_Order_Number,1,BRW1)
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,ret:Despatched_Account_Key)
  BRW1.AddRange(ret:Account_Number,Account_Number)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?RET:Purchase_Order_Number:2,ret:Purchase_Order_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ret:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?RET:Purchase_Order_Number,ret:Ref_Number,1,BRW1)
  BRW1.AddField(ret:Ref_Number,BRW1.Q.ret:Ref_Number)
  BRW1.AddField(ret:Purchase_Order_Number,BRW1.Q.ret:Purchase_Order_Number)
  BRW1.AddField(ret:Account_Number,BRW1.Q.ret:Account_Number)
  BRW1.AddField(ret:date_booked,BRW1.Q.ret:date_booked)
  BRW1.AddField(ret:Courier,BRW1.Q.ret:Courier)
  BRW1.AddField(ret:Despatched,BRW1.Q.ret:Despatched)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB7.Init(Account_Number,?Account_Number,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(sub:Account_Number_Key)
  FDCB7.AddField(sub:Account_Number,FDCB7.Q.sub:Account_Number)
  FDCB7.AddField(sub:Company_Name,FDCB7.Q.sub:Company_Name)
  FDCB7.AddField(sub:RecordNumber,FDCB7.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab3{PROP:TEXT} = 'By Sales Number'
    ?Tab:2{PROP:TEXT} = 'By Purchase Order No'
    ?Tab2{PROP:TEXT} = 'By Account Number'
    ?Browse:1{PROP:FORMAT} ='50R(2)|M~Sales Number~L@s8@#1#92L(2)|M~Purchase Order Number~@s30@#2#69L(2)|M~Account Number~@s15@#3#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
    Relate:SUBTRACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Retail_Sales')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020332'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020332'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020332'&'0')
      ***
    OF ?process_sale
      ThisWindow.Update
      Process_Retail_Sale
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?process_sale, Accepted)
      ThisWindow.Reset(1)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?process_sale, Accepted)
    OF ?Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      IF keycode() = mouseleft2
          Post(event:accepted,?process_sale)
      End!IF keycode() = mouseleft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='50R(2)|M~Sales Number~L@s8@#1#92L(2)|M~Purchase Order Number~@s30@#2#69L(2)|M~Account Number~@s15@#3#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#'
          ?Tab3{PROP:TEXT} = 'By Sales Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='92L(2)|M~Purchase Order Number~@s30@#2#50R(2)|M~Sales Number~L@s8@#1#69L(2)|M~Account Number~@s15@#3#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#'
          ?Tab:2{PROP:TEXT} = 'By Purchase Order No'
        OF 3
          ?Browse:1{PROP:FORMAT} ='92L(2)|M~Purchase Order Number~@s30@#2#50R(2)|M~Sales Number~L@s8@#1#69L(2)|M~Account Number~@s15@#3#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#'
          ?Tab2{PROP:TEXT} = 'By Account Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?ret:Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Ref_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Ref_Number, Selected)
    OF ?ret:Purchase_Order_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Purchase_Order_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Purchase_Order_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      FDCB7.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1::Sort2:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 2, Init, (BYTE Controls,BYTE Mode))
  ret:despatched  = 'RET'
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 2, Init, (BYTE Controls,BYTE Mode))


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_To_Despatch PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
Account_Number       STRING(15)
RET_temp             STRING('PRO')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(RETSALES)
                       PROJECT(ret:Ref_Number)
                       PROJECT(ret:Purchase_Order_Number)
                       PROJECT(ret:Account_Number)
                       PROJECT(ret:date_booked)
                       PROJECT(ret:Courier)
                       PROJECT(ret:Despatched)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ret:Ref_Number         LIKE(ret:Ref_Number)           !List box control field - type derived from field
ret:Purchase_Order_Number LIKE(ret:Purchase_Order_Number) !List box control field - type derived from field
ret:Account_Number     LIKE(ret:Account_Number)       !List box control field - type derived from field
ret:date_booked        LIKE(ret:date_booked)          !List box control field - type derived from field
ret:Courier            LIKE(ret:Courier)              !List box control field - type derived from field
ret:Despatched         LIKE(ret:Despatched)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK16::ret:Account_Number  LIKE(ret:Account_Number)
HK16::ret:Despatched      LIKE(ret:Despatched)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Despatch Process'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Despatch Process'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(164,84,352,274),USE(?Browse:1),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),ALRT(4),FORMAT('50R(2)|M~Sales Number~L@s8@92L(2)|M~Purchase Order Number~@s30@69L(2)|M~Account ' &|
   'Number~@s15@47R(2)|M~Date Raised~@d6b@120L(2)|M~Courier~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(548,238),USE(?Process_Despatch),TRN,FLAT,LEFT,ICON('inddesp.jpg')
                       BUTTON,AT(548,136),USE(?Change_Courier),TRN,FLAT,LEFT,ICON('chancoup.jpg')
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Sales Number'),USE(?Tab3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(164,70,64,10),USE(ret:Ref_Number),RIGHT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Purchase Order No'),USE(?Tab:2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(164,70,124,10),USE(ret:Purchase_Order_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Account Number'),USE(?Tab2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Account Number'),AT(300,72),USE(?Account_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold)
                           ENTRY(@s15),AT(364,72,124,10),USE(Account_Number),SKIP,LEFT,FONT('Tahoma',8,01010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(492,68),USE(?FldLookup),TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(164,72,124,10),USE(ret:Purchase_Order_Number,,?RET:Purchase_Order_Number:2),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Date Raised'),USE(?Tab4)
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort4:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 4
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2
BRW1::Sort2:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:Account_Number                Like(Account_Number)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Update_City_Link    Routine
    epc:account_number    = Left(Sub(cou:account_number,1,8))
    rdn:despatch_number = Format(rdn:despatch_number,@p<<<<<<<#p)
    epc:ref_number        = '|R' & Clip(Format(ret:ref_number,@s9)) & '/D' & Clip(Format(rdn:despatch_number,@s9))
    IF ret:Contact_Name <> ''
        epc:contact_name      = '|' & Left(Sub(ret:contact_name,1,30))
    Else !IF ret:Contact_Name <> ''
        epc:contact_name      = '|N/A'
    End !IF ret:Contact_Name <> ''

    epc:address_line1     = '|' & Left(Sub(ret:company_name_delivery,1,30))
    epc:address_line2     = '|' & Left(Sub(ret:address_line1_delivery,1,30))
    epc:town              = '|' & Left(Sub(ret:address_line2_delivery,1,30))
    epc:county            = '|' & Left(Sub(ret:address_line3_delivery,1,30))
    epc:postcode          = '|' & Left(Sub(ret:postcode_delivery,1,8))

    epc:customer_name     = '|' & Left(Sub(ret:account_number,1,30))
    epc:city_service      = '|' & Left(Sub(cou:service,1,2))
    epc:city_instructions = '|' & Left(Sub(ret:delivery_text,1,30))
    epc:pudamt            = '|' & Left(Sub('0.00',1,4))
    epc:return_it         = '|' & Left('N')
    epc:saturday          = '|' & Left('N')
    epc:dog               = '|' & Left(Sub('Mobile Phone Goods',1,30))
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020333'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_To_Despatch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:COURIER.Open
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  Relate:RETDESNO.Open
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RETSALES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_To_Despatch')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?Account_Number{Prop:Tip} AND ~?FldLookup{Prop:Tip}
     ?FldLookup{Prop:Tip} = 'Select ' & ?Account_Number{Prop:Tip}
  END
  IF ?Account_Number{Prop:Msg} AND ~?FldLookup{Prop:Msg}
     ?FldLookup{Prop:Msg} = 'Select ' & ?Account_Number{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,ret:Despatched_Ref_Key)
  BRW1.AddRange(ret:Despatched)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?ret:Ref_Number,ret:Ref_Number,1,BRW1)
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,ret:Despatched_Purchase_Key)
  BRW1.AddRange(ret:Despatched)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?ret:Purchase_Order_Number,ret:Purchase_Order_Number,1,BRW1)
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,ret:Despatched_Account_Key)
  BRW1.AddRange(ret:Account_Number)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?RET:Purchase_Order_Number:2,ret:Purchase_Order_Number,1,BRW1)
  BRW1.AddSortOrder(,ret:DespatchedPurchDateKey)
  BRW1.AddRange(ret:Despatched)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(,ret:Purchase_Order_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ret:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?ret:Ref_Number,ret:Ref_Number,1,BRW1)
  BRW1.AddField(ret:Ref_Number,BRW1.Q.ret:Ref_Number)
  BRW1.AddField(ret:Purchase_Order_Number,BRW1.Q.ret:Purchase_Order_Number)
  BRW1.AddField(ret:Account_Number,BRW1.Q.ret:Account_Number)
  BRW1.AddField(ret:date_booked,BRW1.Q.ret:date_booked)
  BRW1.AddField(ret:Courier,BRW1.Q.ret:Courier)
  BRW1.AddField(ret:Despatched,BRW1.Q.ret:Despatched)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab3{PROP:TEXT} = 'By Sales Number'
    ?Tab:2{PROP:TEXT} = 'By Purchase Order No'
    ?Tab2{PROP:TEXT} = 'By Account Number'
    ?Tab4{PROP:TEXT} = 'By Date Raised'
    ?Browse:1{PROP:FORMAT} ='50R(2)|M~Sales Number~L@s8@#1#92L(2)|M~Purchase Order Number~@s30@#2#69L(2)|M~Account Number~@s15@#3#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
    Relate:RETDESNO.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_To_Despatch')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickSubAccounts
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020333'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020333'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020333'&'0')
      ***
    OF ?Process_Despatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Process_Despatch, Accepted)
      brw1.resetsort(1)
      access:retsales.clearkey(ret:ref_number_key)
      ret:ref_number  = brw1.q.ret:ref_number
      If access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
          If RetailPaid() = Level:Fatal
              Case Missive('Cannot Despatch! This sale has an outstanding payment.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Else !If RetailPaid() = Level:Fatal
              access:retdesno.clearkey(rdn:despatch_number_key)
              rdn:despatch_number = ret:despatch_number
              If access:retdesno.tryfetch(rdn:despatch_number_key) = Level:Benign
                  no_of_items(items")
                  access:courier.clearkey(cou:courier_key)                                    !City Link Export
                  cou:courier = ret:courier
                  If access:courier.tryfetch(cou:courier_key) = Level:benign
                      If cou:courier_type = 'CITY LINK'
                          glo:file_name   = 'C:\CITYOUT.TXT'
                          Remove(expcity)
                          access:expcity.open()
                          access:expcity.usefile()
                          Do Update_City_Link
                          epc:nol               = '|' & Format(items",@n02)
                          access:expcity.insert()
                          access:expcity.close()
                          copy(expcity,Clip(cou:export_path))
                      End!If cou:courier_type = 'CITY LINK'
                  End!If access:courier.tryfetch(cou:courier_key) = Level:benign
                  consignment_number" = ''
                  Insert_Consignment_Note_number(ret:ref_number,items",consignment_number")
                  If consignment_number" <> ''
                      RDN:Consignment_Number  = consignment_number"
                      RDN:Courier             = ret:courier
                      access:retdesno.update()
                      ret:despatched = 'YES'
                      ret:consignment_number = consignment_number"
                      ret:date_despatched = Today()
                      access:retsales.update()
                      Access:ordhead.ClearKey(orh:SalesNumberKey)
                      orh:SalesNumber = ret:Ref_Number
                      If Access:ordhead.TryFetch(orh:SalesNumberKey) = Level:Benign
                          !Found
                          orh:DateDespatched  = Today()
                          Access:ORDHEAD.Update()
                      Else!If Access:ordhead.TryFetch(orh:SalesNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:ordhead.TryFetch(orh:SalesNumberKey) = Level:Benign
                  end!If consignment_number" <> ''
              End!If access:retdesno.tryfetch(rdn:despatch_number_key) = Level:Benign
      
          End !If RetailPaid() = Level:Fatal
      
      End!If access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Process_Despatch, Accepted)
    OF ?Change_Courier
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change_Courier, Accepted)
      thiswindow.reset(1)
      access:retsales.clearkey(ret:ref_number_key)
      ret:ref_number  = brw1.q.ret:ref_number
      If access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_courier
          if globalresponse = requestcompleted
              Case Missive('This will change the Courier of the transaction to ' & Clip(cou:Courier) & '. '&|
                '<13,10>'&|
                '<13,10>Are you sure?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      ret:courier = cou:courier
                      access:retsales.update()
                  Of 1 ! No Button
              End ! Case Missive
          end
          globalrequest     = saverequest#
      
      End!If access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change_Courier, Accepted)
    OF ?Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number, Accepted)
      BRW1.ResetSort(1)
      IF QuickWindow{Prop:AcceptAll} = False
        IF Account_Number OR ?Account_Number{Prop:Req}
          sub:Account_Number = Account_Number
          !Save Lookup Field Incase Of error
          look:Account_Number        = Account_Number
          IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              Account_Number = sub:Account_Number
            ELSE
              !Restore Lookup On Error
              Account_Number = look:Account_Number
              SELECT(?Account_Number)
              CYCLE
            END
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number, Accepted)
    OF ?FldLookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FldLookup, Accepted)
      ! Lookup button pressed. Call select procedure if required
      sub:Account_Number = Account_Number             ! Move value for lookup
      IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) ! IF record not found
         sub:Account_Number = Account_Number          ! Move value for lookup
         ! Based on ?Account_Number
         SET(sub:Account_Number_Key,sub:Account_Number_Key) ! Prime order for browse
         Access:SUBTRACC.TryNext()
      END
      GlobalRequest = SelectRecord
      PickSubAccounts                                 ! Allow user to select
      IF GlobalResponse <> RequestCompleted           ! User cancelled
         SELECT(?FldLookup)                           ! Reselect control
         CYCLE                                        ! Resume accept loop
      .
      CHANGE(?Account_Number,sub:Account_Number)
      GlobalResponse = RequestCancelled               ! Reset global response
      SELECT(?FldLookup+1)                            ! Select next control in sequence
      SELF.Request = SELF.OriginalRequest             ! Reset local request
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FldLookup, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      IF keycode() = mouseleft2
          Post(event:accepted,?process_despatch)
      End!IF keycode() = mouseleft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='50R(2)|M~Sales Number~L@s8@#1#92L(2)|M~Purchase Order Number~@s30@#2#69L(2)|M~Account Number~@s15@#3#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#'
          ?Tab3{PROP:TEXT} = 'By Sales Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='92L(2)|M~Purchase Order Number~@s30@#2#50R(2)|M~Sales Number~L@s8@#1#69L(2)|M~Account Number~@s15@#3#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#'
          ?Tab:2{PROP:TEXT} = 'By Purchase Order No'
        OF 3
          ?Browse:1{PROP:FORMAT} ='92L(2)|M~Purchase Order Number~@s30@#2#50R(2)|M~Sales Number~L@s8@#1#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#69L(2)|M~Account Number~@s15@#3#'
          ?Tab2{PROP:TEXT} = 'By Account Number'
        OF 4
          ?Browse:1{PROP:FORMAT} ='92L(2)|M~Purchase Order Number~@s30@#2#47R(2)|M~Date Raised~@d6b@#4#50R(2)|M~Sales Number~L@s8@#1#69L(2)|M~Account Number~@s15@#3#120L(2)|M~Courier~@s30@#5#'
          ?Tab4{PROP:TEXT} = 'By Date Raised'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?ret:Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Ref_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Ref_Number, Selected)
    OF ?ret:Purchase_Order_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Purchase_Order_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Purchase_Order_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = RET_temp
  ELSIF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = RET_temp
  ELSIF Choice(?CurrentTab) = 3
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = RET_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Account_Number
  ELSIF Choice(?CurrentTab) = 4
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = RET_temp
  ELSE
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 4
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

no_of_items PROCEDURE (f_number)                      !Generated from procedure template - Window

items_temp           LONG(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('No Of Packages'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Number Of Packages'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,09A6A7CH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('No Of Packages'),USE(?Tab1)
                           PROMPT('No Of Packages'),AT(252,206),USE(?items_temp:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(336,206,64,10),USE(items_temp),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,RANGE(1,99999999),STEP(1)
                         END
                       END
                       BUTTON,AT(368,258),USE(?Button2),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020593'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('no_of_items')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','no_of_items')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','no_of_items')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020593'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020593'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020593'&'0')
      ***
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
      f_number    = items_temp
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
