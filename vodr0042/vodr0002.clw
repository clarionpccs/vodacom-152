

   MEMBER('vodr0042.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


OverdueStatusReport PROCEDURE                         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
    MAP
!-----------------------------------------------
WorkingDaysBetween          PROCEDURE  (DATE, DATE, STRING, STRING), LONG
WorkingHoursBetween         PROCEDURE  (DATE, DATE, TIME, TIME, STRING, STRING), LONG
HoursBetween                    PROCEDURE  (TIME, TIME),LONG
!-----------------------------------------------
LoadAUDSTATS            PROCEDURE(STRING), LONG ! BOOL
LoadJOBSE                   PROCEDURE(), LONG ! BOOL
LoadSTATUS                  PROCEDURE(STRING), LONG ! BOOL
LoadSUBTRACC PROCEDURE(STRING), LONG ! BOOL
LoadTRADEACC                    PROCEDURE(STRING), LONG ! BOOL
LoadWEBJOB      PROCEDURE( LONG ), LONG ! BOOL
!-----------------------------------------------
AddToHeadAccountQueue                PROCEDURE(STRING), LONG ! BOOL
AddToSubAccountQueue                    PROCEDURE(STRING), LONG ! BOOL
CheckStatus PROCEDURE(STRING), LONG ! BOOL
UpdateHeadAccountQueue PROCEDURE()
UpdateSubAccountQueue  PROCEDURE(STRING)
UpdateSummaryQueue              PROCEDURE()
!-----------------------------------------------
After                       PROCEDURE (STRING, STRING), STRING
AppendString                    PROCEDURE( STRING, STRING, STRING ), STRING
Before                      PROCEDURE (STRING, STRING), STRING
GetARCLocation          PROCEDURE(), LONG ! BOOL
GetRRCLocation          PROCEDURE(), LONG ! BOOL
NumberToColumn PROCEDURE( LONG ), STRING
ParseParameters PROCEDURE(), STRING ! BOOL
AddParamter PROCEDURE(STRING), LONG ! BOOL
GetParameter    PROCEDURE(STRING), LONG ! BOOL
AddUserName     PROCEDURE(STRING), STRING ! BOOL
WriteDebug      PROCEDURE( STRING )
!-----------------------------------------------
    END !MAP
tmp:VersionNumber    STRING(30)
Parameter_Group      GROUP,PRE()
LOC:Courier          STRING(30)
LOC:EndDate          DATE
LOC:StartDate        DATE
                     END
ParameterQueue       QUEUE,PRE(param)
Name                 STRING(30)
Value                STRING(255)
SubParameters        STRING(255)
                     END
Local_Group          GROUP,PRE(LOC)
AccountNumber        STRING(15)
Account_Name         STRING(100)
ApplicationName      STRING('ServiceBase')
ARCFound             LONG
ARCLocation          STRING(30)
RRCLocation          STRING(30)
Clock                TIME
CurrentStatus        STRING(30)
DesktopPath          STRING(255)
FileName             STRING(255)
IncomingMSN          STRING(30)
IncomingIMEI         STRING(30)
JobType              STRING(3)
OutgoingIMEI         STRING(30)
OutgoingMSN          STRING(30)
Path                 STRING(255)
ProgramName          STRING(100)
SectionName          STRING(100)
SiteLocation         STRING(50)
TotalJobs            LONG
TotalHours           LONG
Text                 STRING(255)
Today                DATE
UserCode             STRING(3)
UserName             STRING(100)
                     END
DebugGroup           GROUP,PRE(debug)
Active               LONG
Count                LONG
Name                 STRING(30)
                     END
Misc_Group           GROUP,PRE()
AUDSTATS_OK          BYTE
OPTION1              SHORT
CONST:OverdueJobs    STRING('Overdue Jobs')
CONST:OverdueExchanges STRING('Overdue Exchanges')
CONST:OverdueLoans   STRING('Overdue Loans')
JOBSE_OK             BYTE
OverdueJobCount      LONG
OverdueExchangeCount LONG
OverdueLoanCount     LONG
OverdueJobs_Cols     LONG
OverdueExchanges_Cols LONG
OverdueLoans_Cols    LONG
RecordCount          LONG
Result               BYTE
WebJOB_OK            BYTE
                     END
MessageEx_Group      GROUP,PRE(messageex)
Delay                LONG
Text                 STRING(255)
                     END
AccountQueue_Group   GROUP,PRE()
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
                     END
UserResultsQueue     QUEUE,PRE(uq)
UserCode             STRING(3)
Forename             STRING(30)
Surname              STRING(30)
JobsBooked           LONG
                     END
LOC:HeadAccountNumber STRING(15)
HeadAccountQueue     QUEUE,PRE(hq)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
EmailAddress         STRING(255)
RepairCentreType     STRING(3)
UseTimingsFrom       STRING(3)
WorkingHours         LONG
SiteLocation         STRING(30)
JobsBooked           LONG
JobsDespatched       LONG
BranchIdentification STRING(2)
                     END
LOC:SubAccountNumber STRING(15)
SubAccountQueue      QUEUE,PRE(sq)
HeadAccountNumber    STRING(15)
HeadRepairCentreType STRING(3)
SubAccountNumber     STRING(15)
SubAccountName       STRING(30)
JobsBooked           LONG
JobsDespatched       LONG
                     END
Progress:Text        STRING(100)
TotalRow             LONG
SheetExtents_Group   GROUP,PRE()
data:LastCol         STRING('Z')
summary:LastCol      STRING('G')
summary:SummaryRow   LONG
temp:LastCol         STRING(1)
                     END
Worksheet_Group      GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('E')
HeadSummaryRow       LONG(7)
DataLastCol          STRING('Q')
DataSectionRow       LONG(7)
DataHeaderRow        LONG(9)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(200)
                     END
Excel_Group          GROUP,PRE()
Excel                CSTRING(20)
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
SummaryQueue         QUEUE,PRE(summq)
CurrentStatus        STRING(30),NAME('summq_CurrentStatus')
StatusType           BYTE,DIM(3),NAME('summq_StatusType')
TurnaroundTime       DECIMAL(10,4)
NumberOfJobs         LONG,DIM(3)
OverdueHours         LONG,DIM(3)
                     END
HeaderQueue          QUEUE,PRE(head)
SheetName            STRING(32),NAME('head_SheetName')
ColumnPos            LONG,NAME('head_ColumnPos')
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
Calculations_Group   GROUP,PRE(calc)
OverdueHours         LONG
                     END
CurrentStatusQueue   QUEUE,PRE(status)
Status               STRING(30)
UseTurnaroundTime    STRING(3)
                     END
JobsQueueIndex       LONG
JobsQueue            QUEUE,PRE(jobQ)
MainAccountNumber    STRING(15)
JobNumber            LONG
                     END
LocalTag             STRING(1)
GUIMode              BYTE
DoAll                STRING(1)
LocalHeadAccount     STRING(30)
Count                LONG
BRW5::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Overdue Status Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           STRING('Job Booked Start Date'),AT(245,114,84,12),USE(?String4),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(344,114,64,10),USE(LOC:StartDate),IMM,FONT('Arial',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest despatched date to show on report'),REQ
                           BUTTON,AT(412,110),USE(?StartDateCalendar),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           BUTTON('&Rev tags'),AT(297,290,72,12),USE(?DASREVTAG),DISABLE,HIDE
                           STRING('Job Booked End Date'),AT(245,136,84,12),USE(?String5),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(344,136,64,10),USE(LOC:EndDate),IMM,FONT('Arial',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest despatched date to show on report'),REQ
                           BUTTON,AT(412,132),USE(?EndDateCalendar),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           BUTTON('sho&W tags'),AT(297,306,70,13),USE(?DASSHOWTAG),DISABLE,HIDE
                           BUTTON,AT(496,308),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           CHECK('All Repair Centres'),AT(192,160),USE(DoAll),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                           CHECK('Show Excel'),AT(432,160),USE(excel:Visible),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are experiencing pr' &|
   'oblems <13,10>with Excel reports.'),VALUE('1','0')
                           LIST,AT(192,174,296,184),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('12L(2)|FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(496,240),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(496,274),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,374),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
! Excel EQUATES

!----------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic  EQUATE(0FFFFEFF7h) ! Constants.
xlSolid      EQUATE(        1 ) ! Constants.
xlLeft       EQUATE(0FFFFEFDDh) ! Constants.
xlRight      EQUATE(0FFFFEFC8h) ! Constants.
xlLastCell   EQUATE(       11 ) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.

xlCenter     EQUATE(0FFFFEFF4h) ! Excel.Constants
xlBottom     EQUATE(0FFFFEFF5h) ! Excel.Constants
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!----------------------------------------------------

! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = tra:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = '*'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = '*')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::6:QUEUE = glo:Queue2
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer2 = tra:RecordNumber
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!-----------------------------------------------
OKButtonPressed                                         ROUTINE
    DATA
    CODE
        !Debug:Active = true
        !-----------------------------------------------------------------
        WriteDebug('OKButtonPressed 1')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO ProgressBar_Setup
        DO XL_Setup

        DO FillHeadAccountQueue
        IF CancelPressed = True
            GOTO OKButtonPressed_EXIT_1
        END !IF

        DO FillJobsQueue
        IF CancelPressed = True
            GOTO OKButtonPressed_EXIT_1
        END !IF

        WriteDebug('OKButtonPressed 2')

        DO InitColumns
        !-----------------------------------------------------------------
        LOOP Count = 1 TO RECORDS(HeadAccountQueue)
            !------------------------------------------------------------
            IF CancelPressed = True
                GOTO OKButtonPressed_EXIT_1
            END !IF

            GET(HeadAccountQueue, Count)
            IF hq:RepairCentreType = 'NO'
                WriteDebug('OKButtonPressed x#="' & Count & '", "' & CLIP(hq:HeadAccountNumber) & '" hq:RepairCentreType = "NO"')

                CYCLE
            END !IF

            jobQ:MainAccountNumber = hq:HeadAccountNumber
            GET(JobsQueue, +jobQ:MainAccountNumber)
            IF ERRORCODE() <> 0
                WriteDebug('OKButtonPressed x#="' & Count & '", "' & CLIP(hq:HeadAccountNumber) & '" NOT IN(JobsQueue)')

                CYCLE
            END !IF
            !------------------------------------------------------------
            DO WriteHeadAccountReport
            !------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
OKButtonPressed_EXIT_1
        WriteDebug('RECORDS(JobsQueue)="' & RECORDS(JobsQueue) & '"')

        DO XL_Finalize
        DO ProgressBar_Finalise

OKButtonPressed_EXIT
        POST(Event:CloseWindow)

        EXIT
        !-----------------------------------------------------------------
!-----------------------------------------------
FillHeadAccountQueue ROUTINE
    DATA
    CODE
        !------------------------------------------
        WriteDebug('FillHeadAccountQueue ')

        IF CancelPressed
            EXIT
        END !IF
        !------------------------------------------
        Progress:Text    = 'Loading Trade Accounts'
        RecordsToProcess = 1000

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOC:ARCFound = False

        !Account_Number_Key       KEY(tra:Account_Number),NOCASE,PRIMARY
        !Company_Name_Key         KEY(tra:Company_Name),DUP,NOCASE
        !
        Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
            tra_ali:Account_Number = ''
        SET(tra_ali:Account_Number_Key, tra_ali:Account_Number_Key)
        LOOP WHILE Access:TRADEACC_ALIAS.NEXT() = Level:Benign
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            WriteDebug('FillHeadAccountQueue LOOP WHILE Access:TRADEACC.NEXT(' & CLIP(tra_ali:Account_Number) & ')')

            hq:HeadAccountNumber    = tra_ali:Account_Number
            hq:HeadAccountName      = tra_ali:Company_Name
            hq:EmailAddress         = tra_ali:EmailAddress
            hq:BranchIdentification = tra_ali:BranchIdentification
            IF (LOC:ARCFound = False) AND (tra_ali:RemoteRepairCentre = False)
                LOC:ARCFound        = True
                WriteDebug('FillHeadAccountQueue "' & CLIP(tra_ali:Account_Number) & '" ARC')

                hq:RepairCentreType = 'ARC' ! 'NO', 'ARC', 'RRC'
                hq:SiteLocation         = LOC:ARCLocation
            ELSE
                WriteDebug('FillHeadAccountQueue "' & CLIP(tra_ali:Account_Number) & '" RRC')
                hq:RepairCentreType = 'RRC' ! 'NO', 'ARC', 'RRC'
                hq:SiteLocation         = LOC:RRCLocation
            END !IF


            IF tra_ali:Account_Number = 'XXXRRC' THEN CYCLE.
            IF DoAll <> 'Y' THEN
               glo:Queue2.Pointer2 = tra_ali:RecordNumber
               GET(glo:Queue2,glo:Queue2.Pointer2)
               IF ERROR() THEN CYCLE.
            END !IF

            IF tra_ali:UseTimingsFrom = 'TRA'
                hq:WorkingHours = (  tra_ali:EndWorkHours - tra_ali:StartWorkHours)   / (60 * 60 * 100) ! 360,000
            ELSE
                hq:WorkingHours = (def:End_Work_Hours - def:Start_Work_Hours) / (60 * 60 * 100) ! 360,000
            END !IF

            hq:JobsBooked           = 0
            hq:JobsDespatched       = 0

            ADD(HeadAccountQueue)
        END !LOOP

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
    EXIT
FillJobsQueue                       ROUTINE
    DATA
Location LIKE(job:Location)
    CODE
        !-----------------------------------------------------------------
        WriteDebug('FillJobsQueue')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! Date_Booked_Key          KEY( job:date_booked    ),DUP,NOCASE
        ! DateCompletedKey         KEY( job:Date_Completed ),DUP,NOCASE
        !
        Access:JOBS.ClearKey(job:Date_Booked_Key)
            job:date_booked = LOC:StartDate
        SET(job:Date_Booked_Key, job:Date_Booked_Key)
        !------------------------------------------
        Progress:Text    = 'Finding Jobs'
        RecordsToProcess = 1000 * (LOC:EndDate - LOC:StartDate + 1)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP UNTIL Access:JOBS.Next()
            WriteDebug('FillJobsQueue LOOP UNTIL Access:JOBS.Next(' & job:Ref_Number & ')')
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF job:date_booked > LOC:EndDate
                WriteDebug('FillJobsQueue LOOP job:date_booked > LOC:EndDate')
                BREAK
            END !IF

            IF LOC:Today < job:Status_End_Date
                WriteDebug('IF LOC:Today < job:Status_End_Date JOB Status Date in future')
                CYCLE
            ELSIF job:Status_End_Date = LOC:Today
                IF LOC:Clock < job:Status_End_Time
                    WriteDebug('IF LOC:Clock < job:Status_End_Time JOB Status Time later today')
                    CYCLE
                END !IF
            END !IF
            !-------------------------------------------------------------
            IF    job:Exchange_Unit_Number > 0
                LOC:JobType = 'EXC'
                LOC:CurrentStatus = job:Exchange_Status

            ELSIF job:Loan_Unit_Number     > 0
                LOC:JobType = 'LOA'
                LOC:CurrentStatus = job:Loan_Status

            ELSE
                LOC:JobType = 'JOB'
                LOC:CurrentStatus = job:Current_Status

            END !IF

            IF NOT CheckStatus(LOC:CurrentStatus)
                WriteDebug('FillJobsQueue LOOP IF NOT CheckSTATUS(' & CLIP(LOC:CurrentStatus) & ')')

                CYCLE
            END !IF
            !-------------------------------------------------------------
            IF    NOT AddToSubAccountQueue(job:Account_Number)
                WriteDebug('FillJobsQueue IF NOT AddToSubAccountQueue(' & CLIP(job:Account_Number) & ')')

                CYCLE
            END !IF

            IF job:Location <> hq:SiteLocation
                WriteDebug('job:Location(' & CLIP(job:Location) & ') <> hq:SiteLocation"' & CLIP(hq:SiteLocation) & '"')

                CYCLE
            END !CASE
            !=============================================================
            WriteDebug('FillJobsQueue SUCCESS RecordCount="' & RecordCount & '"')
            RecordCount += 1

            jobQ:MainAccountNumber = sub:Main_Account_Number
            jobQ:JobNumber         = job:Ref_Number
            ADD(JobsQueue)
            !-------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
        WriteDebug('FillJobsQueue SORT(JobsQueue, +jobQ:MainAccountNumber, +jobQ:JobNumber) START')
            SORT(JobsQueue, +jobQ:MainAccountNumber, +jobQ:JobNumber)
        WriteDebug('FillJobsQueue SORT(JobsQueue, +jobQ:MainAccountNumber, +jobQ:JobNumber) END RECORDS(' & RECORDS(JobsQueue) & ')')
        !-----------------------------------------------------------------
        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
    EXIT
WriteHeadAccountReport                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('WriteHeadAccountReport')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        JobsQueueIndex       = POSITION(JobsQueue)
        !
        FREE(SummaryQueue)
        IF ERRORCODE() <> 0
            WriteDebug('WriteHeadAccountReport ERROR FREE(SummaryQueue)<13,10>ERRORCODE(' & ERRORCODE() & ')<13,10>ERROR(' & ERROR() & ')')
            CancelPressed = True

            EXIT
        END !IF
        !
        OverdueJobCount      = 0
        OverdueExchangeCount = 0
        OverdueLoanCount     = 0
        LOC:TotalJobs        = 0
        LOC:TotalHours       = 0

        RecordCount          = 0
        !IF LOC:FileName <> '' THEN
           !-----------------------------------------------------------------
           DO ExportSetup
           DO ExportBody
           DO ExportFinalize
           !-----------------------------------------------------------------
        !END !IF
    EXIT
!-----------------------------------------------
ExportSetup                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportSetup')

        CancelPressed = False
        SETCURSOR(CURSOR:Wait)

        LOC:Today = TODAY()
        LOC:Clock = CLOCK()
        !-----------------------------------------------------------------
        DO GetFileName

!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again',LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,500)
!                Of 1 ! &OK Button
!                    CancelPressed = True
!
!                    EXIT
!            End!Case MessageEx
!
!            EXIT
!        END !IF LOC:FileName = ''
        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
              LOC:FileName = CLIP(LOC:FileName) & '.xls'
           END !IF

           DO XL_AddWorkbook
           !-----------------------------------------------------------------
        END !IF
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportBody')

        IF CancelPressed
            EXIT
        END !IF

        DO CreateTitleSheet
        DO CreateDataSheet
        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportFinalize')

        IF CancelPressed
            DO XL_DropAllWorksheets

            SETCURSOR()

            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! 4 Dec 2001 John
        ! Show summary on entry
        !
        Excel{'Sheets("Sheet3").Delete'}

        Excel{'Sheets("Summary").Select'}
         DO XL_SetWorksheetLandscape

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO ResetClipboard
        !-----------------------------------------------------------------
        SETCURSOR()

!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export ' & CLIP(hq:HeadAccountName) & ' Completed.' & |
!                           '<13,10>'                                            & |
!                           '<13,10>Spreadsheet saved to your Desktop'           & |
!                           '<13,10>'                                            & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),     |
!                         LOC:ApplicationName,                                     |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,500)
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export ' & CLIP(hq:HeadAccountName) & ' Completed.' & |
!                           '<13,10>'                                            & |
!                           '<13,10>Spreadsheet saved to '                       & |
!                           '<13,10>'                                            & |
!                           '<13,10>' & CLIP(LOC:Filename),                        |
!                         LOC:ApplicationName,                                     |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,500)
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateTitleSheet')

        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol 

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetLandscape

        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = '$' & sheet:HeadSummaryRow & ':$' & sheet:HeadSummaryRow
        !-----------------------------------------------------------------
        Excel{'Range("A:A").ColumnWidth'}                         = 30.00
        Excel{'Range("B:' & sheet:HeadLastCol & '").ColumnWidth'} = 15.00
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateDataSheet')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        jobQ:MainAccountNumber = hq:HeadAccountNumber
        GET(JobsQueue, jobQ:MainAccountNumber)

        IF ERRORCODE() <> 0
            WriteDebug('CreateDataSheet IF ERRORCODE() <> 0 "' & ERRORCODE() & '"')
            EXIT
        END !IF

        IF jobQ:MainAccountNumber <> hq:HeadAccountNumber
            WriteDebug('CreateDataSheet IF jobQ:MainAccountNumber"' & CLIP(jobQ:MainAccountNumber) & '" <> hq:HeadAccountNumber"' & CLIP(hq:HeadAccountNumber) & '"')
            EXIT
        END !IF

        JobsQueueIndex = POINTER(JobsQueue)
        !-----------------------------------------------------------------
        Progress:Text    = 'Creating Report For "' & CLIP(hq:HeadAccountName) & '"'
        RecordsToProcess = RECORDS(JobsQueue)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOC:Text          = CONST:OverdueJobs
        sheet:TempLastCol = NumberToColumn(OverdueJobs_Cols)
        DO CreateJobTypeSheet

        LOC:Text          = CONST:OverdueExchanges
        sheet:TempLastCol = NumberToColumn(OverdueExchanges_Cols)
        DO CreateJobTypeSheet

        LOC:Text          = CONST:OverdueLoans
        sheet:TempLastCol = NumberToColumn(OverdueLoans_Cols)
        DO CreateJobTypeSheet
        !------------------------------------------
        LOOP WHILE JobsQueueIndex <= RECORDS(JobsQueue)
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            WriteDebug('CreateDataSheet LOOP JobsQueue WHILE JobsQueueIndex <= RECORDS(JobsQueue)')

            GET(JobsQueue, JobsQueueIndex)

            IF ERRORCODE() <> 0
                WriteDebug('CreateDataSheet LOOP JobsQueue IF ERRORCODE() <> 0')
                BREAK
            END !IF

            IF jobQ:MainAccountNumber <> hq:HeadAccountNumber
                WriteDebug('CreateDataSheet LOOP JobsQueue IF jobQ:MainAccountNumber"' & CLIP(jobQ:MainAccountNumber) & '" <> hq:HeadAccountNumber"' & CLIP(hq:HeadAccountNumber) & '"')
                BREAK
            END !IF
            !------------------------------------------
            !
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = jobQ:JobNumber
            IF Access:JOBS.Fetch(job:Ref_Number_Key) = Level:Benign
                !---------------------------------------------------------
                WriteDebug('CreateDataSheet LOOP JOBS IF Access:JOBS.Fetch(job:Ref_Number_Key) = Level:Benign')

                IF NOT job:Ref_Number = jobQ:JobNumber
                    WriteDebug('CreateDataSheet LOOP JOBS FAIL')
                    BREAK
                END !IF
                WebJOB_OK = LoadWEBJOB(job:Ref_Number)
                !=========================================================
                WriteDebug('CreateDataSheet LOOP JOBS SUCCESS')
                DO WriteColumns
                !---------------------------------------------------------
            END !LOOP

            JobsQueueIndex += 1
            !------------------------------------------
        END !LOOP

        WriteDebug('CreateDataSheet POST JobQueue LOOP')
        !-----------------------------------------------------------------
        LOC:SectionName = CONST:OverdueJobs
        RecordCount     = OverdueJobCount
        DO WriteDataSummary

        LOC:SectionName = CONST:OverdueExchanges
        RecordCount     = OverdueExchangeCount
        DO WriteDataSummary

        LOC:SectionName = CONST:OverdueLoans
        RecordCount     = OverdueLoanCount
        DO WriteDataSummary

        RecordCount = OverdueJobCount + OverdueExchangeCount + OverdueLoanCount

        WriteDebug('CreateDataSheet EXIT')
        !-----------------------------------------------------------------
        Progress:Text = Progress:Text & ' Complete'

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
    EXIT


!-----------------------------------------------
CreateJobTypeSheet                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateJobTypeSheet')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO XL_AddSheet
        !-----------------------------------------------------------------
        DO CreateSectionHeader

        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = '$' & sheet:DataHeaderRow & ':$' & sheet:DataHeaderRow
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateSectionHeader                         ROUTINE
    DATA
    CODE
        WriteDebug('CreateSectionHeader')
        !-----------------------------------------------
        !LOC:Text          = CLIP(LOC:SectionName)
        !sheet:TempLastCol = sheet:DataLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:TempLastCol & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'
                Excel{'Range("B' & sheet:DataSectionRow & '").Select'}
                    Excel{'ActiveCell.Formula'} = CLIP(LOC:Text)
                    DO XL_SetBold
                    DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        DO SetColumns
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(2)
    CODE
        WriteDebug('CreateWorksheetHeader')
        !-----------------------------------------------
        Excel{'ActiveSheet.Name'} = CLIP(LOC:Text)
        !-----------------------------------------------       
        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Formula'}   = 'Overdue Status By ' & CLIP(hq:HeadAccountName)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 16
        !-----------------------------------------------
        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}      = 'Date Created'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_SetBold
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:Today, @D8))

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}      = 'Time Created'
                DO XL_ColRight
                    DO XL_SetBold
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:Clock, @T1))

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Site Location'
                DO XL_ColRight
                    DO XL_SetBold
                    Excel{'ActiveCell.Formula'}      = CLIP(hq:HeadAccountNumber)

        Excel{'Range("A1:' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
FormatColumns                                                   ROUTINE
    DATA
ColumnPos LONG(1)
    CODE
        !-----------------------------------------------
        WriteDebug('FormatColumns')

        head:SheetName = CLIP(LOC:SectionName)
        head:ColumnPos = 1
        GET(HeaderQueue, '+head_SheetName, +head_ColumnPos')
        ColumnPos = POSITION(HeaderQueue)

        LOOP WHILE CLIP(head:SheetName) = CLIP(LOC:SectionName)
            !-------------------------------------------
            !Message('Range("' & CHR(64+head:ColumnPos) & sheet:DataHeaderRow+1 & ':' & CHR(64+head:ColumnPos) & sheet:DataHeaderRow+RecordCount & '").Select')
            Excel{'Range("' & CHR(64+head:ColumnPos) & sheet:DataHeaderRow+1 & ':' & CHR(64+head:ColumnPos) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                DO XL_HorizontalAlignmentRight

                !DO XL_HighLight ! DEBUG
                DO XL_ColRight
            !-------------------------------------------
            ColumnPos += 1
            IF ColumnPos > RECORDS(HeaderQueue)
                BREAK
            END !IF

            GET(HeaderQueue, ColumnPos)
            IF ERRORCODE()
                BREAK
            END !IF
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
InitColumns                                                     ROUTINE
    DATA
Cols    LONG
    CODE
        !-----------------------------------------------
        WriteDebug('InitColumns')

        FREE(HeaderQueue)
        !-----------------------------------------------
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 1
            head:ColumnName   = 'SB Job Number'
            head:ColumnWidth  = 15.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

            head:SheetName    = CONST:OverdueExchanges
            ADD(HeaderQueue)

            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 2
            head:ColumnName   = 'Franchise Branch Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

            head:SheetName    = CONST:OverdueExchanges
            ADD(HeaderQueue)

            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 3
            head:ColumnName   = 'Franchise Job Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

            head:SheetName    = CONST:OverdueExchanges
            ADD(HeaderQueue)

            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 4
            head:ColumnName   = 'Date Booked'
            head:ColumnWidth  = 11.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 5
            head:ColumnName   = 'Previous Status'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

       !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 6
            head:ColumnName   = 'Current Status'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 7
            head:ColumnName   = 'Date Changed'
            head:ColumnWidth  = 11.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 8
            head:ColumnName   = 'Location'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 9
            head:ColumnName   = 'Hours Overdue'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            head:ColumnPos    = 8
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        !Head Account Name
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 10
            head:ColumnName   = 'Head Account Name'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            head:ColumnPos    = 9
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 11
            head:ColumnName   = 'Head Account Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            head:ColumnPos    = 10
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 12
            head:ColumnName   = 'Sub Account Name'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            head:ColumnPos    = 11
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 13
            head:ColumnName   = 'Sub Account Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            head:ColumnPos    = 12
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 14
            head:ColumnName   = 'Manufacturer'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            head:ColumnPos    = 13
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 15
            head:ColumnName   = 'Model'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            head:ColumnPos    = 14
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 16
            head:ColumnName   = 'Engineer'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 17
            head:ColumnName   = 'Engineer Level'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '#0'
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 18
            head:ColumnName   = 'Repair Level'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '#0'
            ADD(HeaderQueue)
            Cols += 1

        !
        head:SheetName        = CONST:OverdueJobs
            head:ColumnPos    = 19
            head:ColumnName   = 'IMEI Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueExchanges
            head:ColumnPos    = 15
            ADD(HeaderQueue)
            head:SheetName    = CONST:OverdueLoans
            ADD(HeaderQueue)
            Cols += 1
        !-----------------------------------------------
        OverdueJobs_Cols      = Cols
        OverdueExchanges_Cols = Cols - 4
        OverdueLoans_Cols     = Cols - 4

        SORT(HeaderQueue, '+head_SheetName, +head_ColumnPos')
        !-----------------------------------------------
    EXIT
SetColumns                                                 ROUTINE
    DATA
ColumnPos LONG(1)
    CODE
        !-----------------------------------------------
        WriteDebug('SetColumns')

        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:TempLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow & '").Select'}
        !-----------------------------------------------
        head:SheetName = CLIP(LOC:Text) 
        head:ColumnPos = 1
        GET(HeaderQueue, '+head_SheetName, +head_ColumnPos')
        ColumnPos = POSITION(HeaderQueue)

        LOOP WHILE CLIP(head:SheetName) = CLIP(LOC:Text)
            !-------------------------------------------
            ! MESSAGE(CLIP(head:SheetName) & '<13,10,13,10>head:ColumnName="' & CLIP(head:ColumnName) & '"<13,10,13,10>ColumnPos=' & ColumnPos & '<13,10,13,10>head:ColumnPos=' & head:ColumnPos)
            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
            !-------------------------------------------
            ColumnPos += 1
            IF ColumnPos > RECORDS(HeaderQueue)
                BREAK
            END !IF

            GET(HeaderQueue, ColumnPos)
            IF ERRORCODE()
                BREAK
            END !IF
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1 & '").Select'}
            Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns START')

        IF job:Cancelled = 'YES'
            EXIT
        END !IF
        !-----------------------------------------------
        IF NOT LoadJOBSE()
            !
        END !IF
        !-----------------------------------------------
        DO GetUserDetailsFromQueue
        !-----------------------------------------------
! Checked during Fill Jobs
!        IF LOC:Today < job:Status_End_Date
!            EXIT
!        ELSIF job:Status_End_Date = LOC:Today
!            IF LOC:Clock < job:Status_End_Time
!                EXIT
!            END !IF
!        END !IF
        !-----------------------------------------------
        IF    job:Exchange_Unit_Number > 0
            LOC:JobType = 'EXC'
            LOC:CurrentStatus = job:Exchange_Status

        ELSIF job:Loan_Unit_Number     > 0
            LOC:JobType = 'LOA'
            LOC:CurrentStatus = job:Loan_Status

        ELSE
            LOC:JobType = 'JOB'
            LOC:CurrentStatus = job:Current_Status

        END !IF

! Checked during Fill Jobs
!        IF NOT LoadSTATUS(LOC:CurrentStatus)
!            EXIT
!        ELSIF sts:Use_Turnaround_Time <> 'YES'
!            EXIT
!        END !IF

        IF NOT LoadAUDSTATS(LOC:JobType)
            ! Sets AUDSTATS_OK as side effect, used to choose value to sent to Excel
            !
            WriteDebug('WriteColumns IF NOT LoadAUDSTATS(LOC:JobType"' & CLIP(LOC:JobType) & '")')
        END !IF
        !===============================================
        RecordCount += 1
            !-------------------------------------------
            UpdateHeadAccountQueue()
            UpdateSubAccountQueue(job:Account_Number)
            UpdateSummaryQueue()
            !-------------------------------------------
            CASE LOC:JobType
            OF 'EXC'
                OverdueExchangeCount += 1
                LOC:SectionName       = CONST:OverdueExchanges

                DO WriteColumns_Exchanges

            OF 'LOA'
                OverdueLoanCount     += 1
                LOC:SectionName       = CONST:OverdueLoans

                DO WriteColumns_Loans

            ELSE
                OverdueJobCount      += 1
                LOC:SectionName       = CONST:OverdueJobs

                DO WriteColumns_Jobs

            END !IF
            !-------------------------------------------
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown

        WriteDebug('WriteColumns EXIT')
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteColumns_Exchanges                                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns_Exchanges')

        Excel{'ActiveWorkBook.Sheets("' & CLIP(LOC:SectionName) & '").Select'}

        DO WriteColumn_JobNumber              ! Job Number
        DO WriteColumn_FranchiseBranchNumber !Franchise Branch Number
        DO WriteColumn_FranchiseJobNumber    !Franchise Job Number
        DO WriteColumn_DateBooked             ! Date Booked

        DO WriteColumn_Audit_PreviousStatus   ! Previous Status
        DO WriteColumn_CurrentStatus_Exchange ! Current Status
        DO WriteColumn_Audit_DateChanged     ! Date Changed
        DO WriteColumn_HoursOverdue          ! Hours Overdue

        DO WriteColumn_HeadAccountName        ! Head Account Name (Head Account No.)
        DO WriteColumn_HeadAccountNumber      ! Head Account No.
        DO WriteColumn_TradeAccountName       ! Sub Account Name
        DO WriteColumn_TradeAccount           ! Sub Account Number

        DO WriteColumn_Manufacturer           ! Manufacturer
        DO WriteColumn_ModelNumber            ! Model
        
        DO WriteColumn_ESN                    ! IMEI Number
        !-----------------------------------------------
    EXIT
WriteColumns_Loans                                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns_Loans')

        Excel{'ActiveWorkBook.Sheets("' & CLIP(LOC:SectionName) & '").Select'}

        DO WriteColumn_JobNumber            ! Job Number
        DO WriteColumn_FranchiseBranchNumber !Franchise Branch Number
        DO WriteColumn_FranchiseJobNumber    !Franchise Job Number
        DO WriteColumn_DateBooked           ! Date Booked

        DO WriteColumn_Audit_PreviousStatus ! Previous Status
        DO WriteColumn_CurrentStatus_Loan   ! Current Status
        DO WriteColumn_Audit_DateChanged   ! Date Changed
        DO WriteColumn_HoursOverdue        ! Hours Overdue

        DO WriteColumn_HeadAccountName      ! Head Account Name (Head Account No.)
        DO WriteColumn_HeadAccountNumber    ! Head Account No.
        DO WriteColumn_TradeAccountName     ! Sub Account Name
        DO WriteColumn_TradeAccount         ! Sub Account Number

        DO WriteColumn_Manufacturer         ! Manufacturer
        DO WriteColumn_ModelNumber          ! Model

        DO WriteColumn_ESN                  !IMEI Number
        !-----------------------------------------------
    EXIT
WriteColumns_Jobs                                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns_Jobs')

        Excel{'ActiveWorkBook.Sheets("' & CLIP(LOC:SectionName) & '").Select'}

        DO WriteColumn_JobNumber            ! Job Number
        DO WriteColumn_FranchiseBranchNumber !Franchise Branch Number
        DO WriteColumn_FranchiseJobNumber    !Franchise Job Number
        DO WriteColumn_DateBooked           ! Date Booked

        DO WriteColumn_Audit_PreviousStatus ! Previous Status
        DO WriteColumn_CurrentStatus_Job    ! Current Status
        DO WriteColumn_Audit_DateChanged   ! Date Changed
        DO WriteColumn_BinLocation         ! Location
        DO WriteColumn_HoursOverdue        ! Hours Overdue

        DO WriteColumn_HeadAccountName      ! Head Account Name (Head Account No.)
        DO WriteColumn_HeadAccountNumber    ! Head Account No.
        DO WriteColumn_TradeAccountName     ! Sub Account Name
        DO WriteColumn_TradeAccount         ! Sub Account Number

        DO WriteColumn_Manufacturer         ! Manufacturer
        DO WriteColumn_ModelNumber          ! Model

        DO WriteColumn_Engineer     
        DO WriteColumn_EngineerLevel
        DO WriteColumn_JobLevel

        DO WriteColumn_ESN                  !IMEI Number
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteColumn_JobNumber                   ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Ref_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Ref_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Audit_PreviousStatus                      ROUTINE  
    DATA
Temp LIKE(aus:NewStatus)
    CODE
        !-----------------------------------------------
        IF AUDSTATS_OK
            Temp = aus:OldStatus
        ELSE
            Temp = ''
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Audit_DateChanged                         ROUTINE  !
    DATA
Temp STRING(20)
    CODE
        !-----------------------------------------------
        IF AUDSTATS_OK
            Temp = LEFT(FORMAT(aus:DateChanged, @D8))
        ELSE
            Temp = ''
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_BinLocation                                    ROUTINE ! Location
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Location

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Location

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ChargeableChargeType                    ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Charge_Type

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Charge_Type

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ConsignmentNumber           ROUTINE     
    DATA
    CODE
        !-----------------------------------------------
        ! Consignment Number
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Consignment_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Consignment_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_CurrentStatus_Job                       ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Current_Status

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Current_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_CurrentStatus_Exchange                      ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Exchange_Status

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Exchange_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_CurrentStatus_Loan                      ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Loan_Status

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Loan_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DateBooked                      ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        ! DateBooked
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(job:date_booked, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(job:date_booked, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DateCompleted                   ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        ! DateCompleted
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(job:Date_Completed, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(job:Date_Completed, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DateDespatched                  ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(job:Date_Despatched, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(job:Date_Despatched, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Engineer                    ROUTINE !
    DATA
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = AppendString(uq:Surname, uq:Forename, ', ')
        Temp = AppendString(      Temp, uq:UserCode, ', ')
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & CLIP(Temp)

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = CLIP(Temp)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_EngineerLevel                    ROUTINE !
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & use:SkillLevel

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = use:SkillLevel

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ESN                         ROUTINE ! IMEI
    DATA
    CODE
        !-----------------------------------------------
        ! IMEI
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:ESN

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:ESN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_HeadAccountName                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & hq:HeadAccountName

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = hq:HeadAccountName

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_HeadAccountNumber              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = hq:HeadAccountNumber

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = hq:HeadAccountNumber

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_HoursOverdue                              ROUTINE !
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & calc:OverdueHours

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = calc:OverdueHours

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_IncomingConsignmentNumber           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Consignment Number
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Incoming_Consignment_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Incoming_Consignment_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_IncomingCourier             ROUTINE     
    DATA
    CODE
        !-----------------------------------------------
        ! Incoming Courier
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Incoming_Courier

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Incoming_Courier

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_IncomingIMEI                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! IMEI
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LOC:IncomingIMEI

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = LOC:IncomingIMEI

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_IncomingMSN                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! ! Incoming MSN
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LOC:IncomingMSN

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = LOC:IncomingMSN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_JobLevel                    ROUTINE !
    DATA
Temp LIKE(jobe:SkillLevel)
    CODE
        !-----------------------------------------------
        IF JOBSE_OK
            Temp = 0
        ELSE
            Temp = jobe:SkillLevel
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_JobType                     ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        ! Job Number
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Internal_Status

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Internal_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Manufacturer                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Make
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Manufacturer

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Manufacturer

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_MSN                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! MSN
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:MSN

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:MSN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ModelNumber                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Model
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Model_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = job:Model_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_OutgoingConsignmentNumber               ROUTINE
    DATA
TempConsignmentNumber LIKE(job:Consignment_Number)
    CODE
        !-----------------------------------------------
        ! Outgoing
        !-----------------------------------------------
        TempConsignmentNumber = job:Consignment_Number

        IF CLIP(TempConsignmentNumber) = '' AND CLIP(job:Exchange_Consignment_Number) <> ''
            TempConsignmentNumber = job:Exchange_Consignment_Number
        END !IF

        IF CLIP(TempConsignmentNumber) = '' AND CLIP(job:Loan_Consignment_Number) <> ''
            TempConsignmentNumber = job:Loan_Consignment_Number
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & TempConsignmentNumber

            EXIT
        END !IF

        Excel{'ActiveCell.Formula'}      = TempConsignmentNumber

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_OutgoingCourier             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Outgoing Courier
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Current_Courier

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Current_Courier ! eqiv Job_Courier ala Loan_Courier, Exchange_Courier

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_OutgoingIMEI                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! IMEI
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LOC:OutgoingIMEI

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = LOC:OutgoingIMEI

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_OutgoingMSN                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Outgoing MSN
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LOC:OutgoingMSN

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = LOC:OutgoingMSN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_TradeAccount                   ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & sq:SubAccountNumber

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = sq:SubAccountNumber

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_TradeAccountName            ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & sq:SubAccountName

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = sq:SubAccountName

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_TransitType                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Initial Transit Type
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Transit_Type

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Transit_Type

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_UserForename                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & uq:Forename

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = uq:Forename

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_UserSurname                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & uq:Surname

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = uq:Surname

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_UnitType                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Unit Type
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Unit_Type

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Unit_Type

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_WarrantyChargeType                  ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Warranty_Charge_Type

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Warranty_Charge_Type

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_WhoBooked                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Who_Booked

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Who_Booked

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_FranchiseBranchNumber  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = hq:BranchIdentification

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = hq:BranchIdentification

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_FranchiseJobNumber      ROUTINE
    DATA
Temp STRING(10)
    CODE
        !-----------------------------------------------
        IF WEBJOB_OK
            Temp = wob:JobNumber
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(Temp)

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = CLIP(Temp)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteDataSummary                    ROUTINE
    DATA
HoursOverdueColumn STRING('E')
SubTotalColumn     STRING('F')
    CODE
        !-----------------------------------------------------------------
        WriteDebug('WriteDataSummary')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! summary details
        !                                                                             
        Excel{'ActiveWorkBook.Sheets("' & CLIP(LOC:SectionName) & '").Select'}

        DO FormatColumns
        !-----------------------------------------------------------------
        IF LOC:SectionName = CONST:OverdueJobs
            HoursOverdueColumn = 'F'
            SubTotalColumn     = 'G'
        END !IF

        Excel{'Range("' & HoursOverdueColumn & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}      = 'Total Hours:'
            DO XL_HorizontalAlignmentRight

        DO XL_ColRight
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Formula'}      = '=SUBTOTAL(9, ' & SubTotalColumn & sheet:DataHeaderRow+1 & ':' & SubTotalColumn & sheet:DataHeaderRow+RecordCount & ')'

        DO XL_ColRight
            Excel{'ActiveCell.Formula'}      = 'Total Records:'
            DO XL_HorizontalAlignmentRight

        DO XL_ColRight
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Formula'}      = RecordCount

        DO XL_ColRight
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}      = 'Showing'

        DO XL_ColRight
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Formula'}      = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
        DO AddTitleTotal
            Excel{'ActiveCell.Offset(1, 1).Formula'} = RecordCount
            Excel{'ActiveCell.Offset(2, 1).Formula'} = '=SUBTOTAL(9, ' & SubTotalColumn & sheet:DataHeaderRow+1 & ':' & SubTotalColumn & sheet:DataHeaderRow+RecordCount & ')'
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        WriteDebug('WriteHeadSummary')

        Excel{'ActiveWorkBook.Sheets("Summary").Select'}

        WriteDebug('WriteHeadSummary 1')
        SORT(SummaryQueue, '+summq_CurrentStatus, +summq_StatusType')
        WriteDebug('WriteHeadSummary 2')
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        WriteDebug('WriteHeadSummary 3')
        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Summary'
            DO XL_SetBold

        WriteDebug('WriteHeadSummary 4')
        DO XL_RowDown
        WriteDebug('WriteHeadSummary 5')
        !-----------------------------------------------------------------
        ! Summary Jobs
        LOC:JobType = 'JOB'
        DO WriteJobTypeSummary

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! Summary Exchanges
        LOC:JobType = 'EXC'
        DO WriteJobTypeSummary

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! Summary Loans
        LOC:JobType = 'LOA'
        DO WriteJobTypeSummary

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO AddTitleTotal
            Excel{'ActiveCell.Offset(1, 1).Formula'} = LOC:TotalJobs
            Excel{'ActiveCell.Offset(2, 1).Formula'} = LOC:TotalHours
        !-----------------------------------------------------------------
        Excel{'Range("A1").Select'}
        !-----------------------------------------------------------------
    EXIT
WriteJobTypeSummary                             ROUTINE
    DATA
QueueIndex    LONG
JobTypeOffset BYTE

CurrentRow LONG
HeadRow    LONG
    CODE
        !-----------------------------------------------------------------
        WriteDebug('WriteJobTypeSummary(' & LOC:JobType & ')')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        CASE LOC:JobType
        OF 'JOB'
            Progress:Text    = 'Writing Summary For ' & CONST:OverdueJobs
            JobTypeOffset    = 1
        OF 'EXC'
            Progress:Text    = 'Writing Summary For ' & CONST:OverdueExchanges
            JobTypeOffset    = 2
        OF 'LOA'
            Progress:Text    = 'Writing Summary For ' & CONST:OverdueLoans
            JobTypeOffset    = 3
        END !CASE
        RecordsToProcess = RECORDS(SummaryQueue)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        CurrentRow = Excel{'ActiveCell.Row'}

        Excel{'Range("A' & CurrentRow & ':' & sheet:HeadLastCol & CurrentRow & '").Select'}
            DO XL_SetBorder
            DO XL_SetBold
            DO XL_SetTitle

        Excel{'Range("A' & CurrentRow & '").Select'}
            CASE LOC:JobType
            OF 'JOB'
                Excel{'ActiveCell.Formula'} = 'Summary ' & CONST:OverdueJobs
            OF 'EXC'
                Excel{'ActiveCell.Formula'} = 'Summary ' & CONST:OverdueExchanges
            OF 'LOA'
                Excel{'ActiveCell.Formula'} = 'Summary ' & CONST:OverdueLoans
            END !CASE

        CurrentRow += 1
        HeadRow     = CurrentRow

        Excel{'Range("A' & HeadRow & ':' & sheet:HeadLastCol & HeadRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeadRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Current Status'
            Excel{'ActiveCell.Offset(0, 1).Formula'} = 'Status Turnaroud Time'
            Excel{'ActiveCell.Offset(0, 2).Formula'} = 'Number Of Jobs'
            Excel{'ActiveCell.Offset(0, 3).Formula'} = 'Overdue Hours'
            Excel{'ActiveCell.Offset(0, 4).Formula'} = 'Average Overdue Hours'

        CurrentRow += 1

        RecordCount = 0
        LOOP QueueIndex = 1 TO RECORDS(SummaryQueue)
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            GET(SummaryQueue, QueueIndex)

            IF    (summq:StatusType[JobTypeOffset]  = False)
                WriteDebug('WriteJobTypeSummary(' & CLIP(summq:CurrentStatus) & ') IF    (summq:StatusType[JobTypeOffset]  = False)')
                CYCLE

            ELSIF summq:NumberOfJobs[JobTypeOffset] = 0
                WriteDebug('WriteJobTypeSummary(' & CLIP(summq:CurrentStatus) & ') ELSIF summq:NumberOfJobs[JobTypeOffset] = 0')
                CYCLE

            ELSE
                WriteDebug('WriteJobTypeSummary(' & CLIP(summq:CurrentStatus) & ') OK')
                LOC:TotalJobs        += summq:NumberOfJobs[JobTypeOffset]
                LOC:TotalHours       += summq:OverdueHours[JobTypeOffset]

            END !IF
            !-------------------------------------------------------------
            RecordCount += 1

            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Offset(0, 0).Formula'} = summq:CurrentStatus
                Excel{'ActiveCell.Offset(0, 1).Formula'} = summq:TurnaroundTime
                Excel{'ActiveCell.Offset(0, 2).Formula'} = summq:NumberOfJobs[JobTypeOffset]
                Excel{'ActiveCell.Offset(0, 3).Formula'} = summq:OverdueHours[JobTypeOffset]
                Excel{'ActiveCell.Offset(0, 4).Formula'} = '=IF(C' & CurrentRow & '=0,0, D' & CurrentRow & '/C' & CurrentRow & ')'

                Excel{'ActiveCell.Offset(0, 1).NumberFormat'} = '#,##0.00'
                Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '#,##0'
                Excel{'ActiveCell.Offset(0, 3).NumberFormat'} = '#,##0'
                Excel{'ActiveCell.Offset(0, 4).NumberFormat'} = '#,##0'

            CurrentRow += 1
            !-------------------------------------------------------------
        END !LOOP

        IF RecordCount = 0
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'} = 'None Found'
                CurrentRow += 1

        END !IF

        Excel{'Range("A' & CurrentRow & ':' & sheet:HeadLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Totals'
            DO XL_SetBold

        IF RecordCount > 0
            !Excel{'ActiveCell.Offset(0, 1).Formula'}      = '

            Excel{'ActiveCell.Offset(0, 2).Formula'}      = '=SUBTOTAL(9, C' & HeadRow+1 & ':C' & CurrentRow-1 & ')'
            Excel{'ActiveCell.Offset(0, 3).Formula'}      = '=SUBTOTAL(9, D' & HeadRow+1 & ':D' & CurrentRow-1 & ')'
            Excel{'ActiveCell.Offset(0, 4).Formula'}      = '=IF(C' & CurrentRow & '=0,0, D' & CurrentRow & '/C' & CurrentRow & ')'

            Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0, 3).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0, 4).NumberFormat'} = '#,##0'
        END !IF

        DO XL_RowDown
        DO XL_RowDown
        !-----------------------------------------------------------------
        Progress:Text = Progress:Text & ' Complete'

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
    EXIT
AddTitleTotal                           ROUTINE
    DATA
    CODE
        WriteDebug('AddTitleTotal')
        !-----------------------------------------------
        ! Put totals info on title sheet
        !
        Excel{'Range("D1").Select'}
            Excel{'ActiveCell.Offset(0, 1).Formula'}   = 'Total'
            Excel{'ActiveCell.Offset(1, 0).Formula'}   = 'Jobs'
            Excel{'ActiveCell.Offset(2, 0).Formula'}   = 'Hours'

        Excel{'Range("E2").Select'}
            Excel{'ActiveCell.NumberFormat'} = '#,##0'
            DO XL_SetBold

        Excel{'Range("E3").Select'}
            Excel{'ActiveCell.NumberFormat'} = '#,##0'
            DO XL_SetBold

        Excel{'Range("D1").Select'}
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
ActiveWorkSheet CSTRING(20)
ActiveCell      CSTRING(20)
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        CLEAR(clip:Value)
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,500)
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'midea.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Cannot find the user''s details.','ServiceBase 3g',|
                           'midea.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Overdue Status Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & CLIP(excel:PRogramName) & ' ' & Format(Today(),@d12)
!
!
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!!        IF CLIP(LOC:FileName) <> ''
!!            ! Already generated 
!!            EXIT
!!        END !IF
!!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!                DeskTopExists = True
!            END !IF
!        END !IF
!
!        LOC:FileName = CLIP(SHORTPATH(LOC:Path)) & CLIP(hq:HeadAccountNumber) & ' VODR0042 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT

!-----------------------------------------------
GetUserDetailsFromQueue                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! on entry LOC:UserCode = User code to look up and
        !   store the details in the queue
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        uq:UserCode = LOC:UserCode
        GET(UserResultsQueue, +uq:UserCode)
        IF ERRORCODE()
            ! Not in queue - ADD
            DO LoadUSERS
            IF Result
                uq:UserCode   = LOC:UserCode
                uq:Forename   = use:Forename
                uq:Surname    = use:Surname
            ELSE
                uq:UserCode   = LOC:UserCode
                uq:Forename   = '' !'<' & CLIP(LOC:UserCode) & '>'
                uq:Surname    = '' !'<Missing>'
            END ! IF

            uq:JobsBooked = 1

            ADD(UserResultsQueue, +uq:UserCode)

            EXIT ! 
        END !IF

        ! In queue - AMEND
        uq:JobsBooked += 1
        PUT(UserResultsQueue, +uq:UserCode)
        !-----------------------------------------------------------------
    EXIT
LoadHeadAccount                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeadAccountQueue)

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        !tra:Account_Number = ''
        SET(tra:Account_Number_Key, tra:Account_Number_Key)

        LOOP UNTIL Access:TRADEACC.Next()
            hq:HeadAccountNumber = tra:Account_Number
            hq:HeadAccountName   = tra:Company_Name
            hq:JobsBooked        = 0
            hq:BranchIdentification = tra:BranchIdentification
            ADD(HeadAccountQueue)
        END !LOOP

        SORT(HeadAccountQueue, +hq:HeadAccountNumber)
        !-----------------------------------------------
    EXIT
LoadSubAccount                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(SubAccountQueue)

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        !sub:Account_Number = ''
        SET(sub:Account_Number_Key, sub:Account_Number_Key)

        LOOP UNTIL Access:SUBTRACC.Next()
            !IF
            sq:HeadAccountNumber = sub:Main_Account_Number
            sq:SubAccountNumber  = sub:Account_Number
            sq:SubAccountName    = sub:Company_Name
            sq:JobsBooked        = 0

            ADD(SubAccountQueue, +sq:SubAccountNumber)
        END !LOOP

        SORT(SubAccountQueue, +sq:SubAccountNumber)
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
LoadUSERS                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = LOC:UserCode

        IF Access:USERS.TryFetch(use:User_Code_Key)
            EXIT
        END !IF

        IF use:User_Code <> LOC:UserCode
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            recordspercycle      = 25
            recordsprocessed     = 0
            recordstoprocess     = 10 !***The Number Of Records, or at least a guess***
            percentprogress      = 0
            progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        recordsprocessed = 0
        RecordCount      = 0

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        Result            = False

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            close(progresswindow)
        End!If tmp:cancel = 1

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

    ?progress:pcttext{prop:text} = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

    Display()
 
ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'midea.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName ! 'ServiceBase 2000'
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment SIGNED
    CODE
        !-----------------------------------------------
        xlComment = Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = excel:DateFormat ! 'dd Mmm yyyy'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("A11"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT
XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT
XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0 ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020605'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('OverdueStatusReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDSTATS.Open
  Relate:DEFAULTS.Open
  Relate:REPMAIL.Open
  Relate:STATUS.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName       = 'Overdue Status By Repair Centre'                   ! Job=2046         Cust=
      MainWindow{PROP:Text} = LOC:ProgramName
      ?Tab1{PROP:Text}      = CLIP(LOC:ProgramName) & ' Criteria'
  
      excel:Visible         = False
  
      SET(defaults)
      access:defaults.next()
  
      IF NOT GetARCLocation()
          Case Missive('The ARC Location is not setup.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
  
          POST(Event:CloseWindow)
      END !IF
  
      IF NOT GetRRCLocation()
          Case Missive('The RRC Location is not setup.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
  
          POST(Event:CloseWindow)
      END !IF
  
  
      messageex:Text = ParseParameters()
      IF CLIP(messageex:Text) <> ''
          Case Missive('' & Clip(messageex:Text) & '.','ServiceBase 3g',|
                         'midea.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
  
          POST(Event:CloseWindow)
      END !IF
  
  !    IF NOT GetParameter('Mode')
  !        Case MessageEx('Program not called correctly', LOC:ApplicationName, 'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,500)
  !        Of 1 ! &OK Button
  !        End!Case MessageEx
  !
  !        POST(Event:CloseWindow)
  !    ELSE
  !        CASE CLIP(param:Value)
  !        OF 'GUI'
              LOC:StartDate         = TODAY() !
              LOC:EndDate           = TODAY() !
  
              LOC:Today             = TODAY()
              LOC:Clock             = CLOCK()
  !
  !        OF 'CONFIG'
  !            Case MessageEx('Program not called correctly', LOC:ApplicationName, 'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,500)
  !            Of 1 ! &OK Button
  !            End!Case MessageEx
  !
  !            POST(Event:CloseWindow)
  !        OF 'INSTALL'
  !            Case MessageEx('Program not called correctly', LOC:ApplicationName, 'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,500)
  !            Of 1 ! &OK Button
  !            End!Case MessageEx
  !
  !            POST(Event:CloseWindow)
  !        OF 'SILENT'
  !            Case MessageEx('Program not called correctly', LOC:ApplicationName, 'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,500)
  !            Of 1 ! &OK Button
  !            End!Case MessageEx
  !
  !            POST(Event:CloseWindow)
  !        END !CASE
  !    END !IF
  
      debug:Active          = False
  
      IF GUIMode = 1 THEN
          WriteDebug('WindowManager.Init(IF GUIMode = 1 THEN)')
  
         MainWindow{PROP:ICONIZE} = TRUE
         DoAll = 'Y'
      END !IF
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?LOC:StartDate{Prop:Alrt,255} = MouseLeft2
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,tra:Account_Number_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,tra:Account_Number,1,BRW5)
  BRW5.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND (tra:Account_Number <<> ''XXXRRC''))')
  BIND('LocalTag',LocalTag)
  BIND('LocalHeadAccount',LocalHeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(LocalTag,BRW5.Q.LocalTag)
  BRW5.AddField(tra:Account_Number,BRW5.Q.tra:Account_Number)
  BRW5.AddField(tra:Company_Name,BRW5.Q.tra:Company_Name)
  BRW5.AddField(tra:RecordNumber,BRW5.Q.tra:RecordNumber)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDSTATS.Close
    Relate:DEFAULTS.Close
    Relate:REPMAIL.Close
    Relate:STATUS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         DISABLE(?List)
         DISABLE(?DASTAG)
         DISABLE(?DASTAGALL)
         DISABLE(?DASUNTAGALL)
      ELSE
          ENABLE(?List)
          ENABLE(?DASTAG)
          ENABLE(?DASTAGALL)
          ENABLE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
          DO OKButtonPressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020605'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020605'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020605'&'0')
      ***
    OF ?StartDateCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?EndDateCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?StartDateCalendar)
      CYCLE
    END
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?EndDateCalendar)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!-----------------------------------------------
WorkingDaysBetween PROCEDURE  (IN:StartDate,IN:EndDate,IN:IncludeSaturday,IN:IncludeSunday)!,long ! Declare Procedure
Weeks        LONG

DaysDiff     LONG
Days         LONG(0)

DaysPerWeek  LONG(5)
TempDate1    DATE
TempDate2    DATE
    CODE
        !-------------------------------------------
        WriteDebug('WorkingDaysBetween' & FORMAT(IN:StartDate,@D17) & 'AND ' & FORMAT(IN:EndDate,@D17))

        IF (IN:StartDate > IN:EndDate)
            RETURN WorkingDaysBetween(IN:EndDate, IN:StartDate,IN:IncludeSaturday,IN:IncludeSunday)
        END !IF
        !-------------------------------------------
        IF (IN:StartDate = IN:EndDate)
            RETURN Days
        END !IF
        !-------------------------------------------
        IF IN:IncludeSaturday = 'YES'
            DaysPerWeek += 1
        END !IF

        IF def:include_sunday = 'YES'
            DaysPerWeek += 1
        END !IF
        !-------------------------------------------
        TempDate1 = IN:StartDate
        IF IN:StartDate = '' THEN
           RETURN Days
        END !IF
        
        LOOP
            CASE (TempDate1 % 7)
            OF 0
                If IN:IncludeSunday = 'YES'
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday = 'YES'
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate1 += 1
            !WriteDebug(FORMAT(TempDate1,@D17))
            If (TempDate1 = IN:EndDate) Then
                RETURN Days
            End !If

        END !LOOP
        !-------------------------------------------
        TempDate2     = IN:EndDate
        !WriteDebug('Second loop')
        
        LOOP
            CASE (TempDate2 % 7)
            OF 0
                If IN:IncludeSunday = 'YES'
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday = 'YES'
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate2 -= 1
            If (TempDate1 = TempDate2) Then
                RETURN Days
            End !If

        END !LOOP
        !WriteDebug('End 2nd loop')
        !-------------------------------------------
        Weeks = (TempDate2 - TempDate1) / 7
        !-------------------------------------------
!        message('DaysBefore           =' & Days                  & '<13,10>' & |
!                '(Weeks * DaysPerWeek)=' & (Weeks * DaysPerWeek) & '<13,10>' & |
!                ' ===================== <13,10>'                             & |
!                'TOTAL                =' & Days + (Weeks * DaysPerWeek) )

        RETURN Days + (Weeks * DaysPerWeek)
        !-------------------------------------------

WorkingHoursBetween PROCEDURE  (StartDate, EndDate, StartTime, EndTime, IN:IncludeSaturday, IN:IncludeSunday) ! LONG
DaysBetween   LONG
Hours         LONG
    CODE
        !-------------------------------------------
        WriteDebug('WorkingHoursBetween')

        DaysBetween = WorkingDaysBetween(StartDate, EndDate, IN:IncludeSaturday, IN:IncludeSunday)
        Hours       = 0

        IF DaysBetween = 0
            Hours += HoursBetween(StartTime, EndTime)
        ELSIF DaysBetween = 1
            Hours += HoursBetween(StartTime,            def:End_Work_Hours)
            Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
        ELSE
            Hours  = (DaysBetween - 1) * hq:WorkingHours

            Hours += HoursBetween(StartTime,            def:End_Work_Hours)
            Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
        END !IF

        RETURN Hours
        !-------------------------------------------
HoursBetween    PROCEDURE  (StartTime, EndTime)!LONG
Hours LONG
    CODE
        !-------------------------------------------
        WriteDebug('HoursBetween')

        IF StartTime = EndTime
            RETURN 0
        ELSIF StartTime > EndTime
            RETURN HoursBetween(EndTime, StartTime)
        END !IF

        Hours = (EndTime - StartTime) / (60 * 60 * 100)  ! 100 100ths = 1sec, 60x1sec = 1min, 60x1min=1 hour

        RETURN Hours
        !-------------------------------------------
!-----------------------------------------------
LoadAUDSTATS PROCEDURE(IN:JobType)! LONG ! BOOL
TempStatus LIKE(aus:NewStatus)
    CODE
        WriteDebug('LoadAUDSTATS')

        !-----------------------------------------------
        !RecordNumberKey KEY(                          aus:RecordNumber),NOCASE,PRIMARY
        !DateChangedKey  KEY( aus:RefNumber, aus:Type, aus:DateChanged ),DUP,NOCASE
        !NewStatusKey    KEY( aus:RefNumber, aus:Type, aus:NewStatus   ),DUP,NOCASE
        !
        AUDSTATS_OK = False

        CASE IN:JobType
        OF 'JOB'
            TempStatus   = job:Current_Status
        OF 'EXC'
            TempStatus   = job:Exchange_Status
        OF 'LOA'
            TempStatus   = job:Loan_Status
        ELSE
            RETURN False
        END !CASE

        Access:AUDSTATS.ClearKey(aus:NewStatusKey)
            aus:RefNumber   = job:Ref_Number
            aus:Type        = IN:JobType
            aus:NewStatus   = TempStatus
        SET(aus:NewStatusKey, aus:NewStatusKey)

        IF Access:AUDSTATS.NEXT()
            RETURN False
        END !IF

        IF NOT aus:RefNumber = job:Ref_Number
            RETURN False
        END !IF

        IF NOT aus:Type      = IN:JobType
            RETURN False
        END !IF

        IF NOT aus:NewStatus = TempStatus
            RETURN False
        END !IF

        AUDSTATS_OK = True
        RETURN True
        !-----------------------------------------------
LoadJOBSE  PROCEDURE()! LONG ! BOOL
    CODE
        WriteDebug('LoadJOBSE ')
        !-----------------------------------------------
        JOBSE_OK = False

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
        SET(jobe:RefNumberKey, jobe:RefNumberKey)

        IF Access:JOBSE.NEXT()
            RETURN False
        END !IF

        IF jobe:RefNumber <> job:Ref_Number
            RETURN False
        END !IF

        JOBSE_OK = True
        RETURN True
        !-----------------------------------------------
LoadSTATUS  PROCEDURE(IN:Status)! LOMG ! BOOL
    CODE
        !-----------------------------------------------
        Access:STATUS.ClearKey(sts:Status_Key)
        sts:Status = IN:Status

        IF Access:STATUS.TryFetch(sts:Status_Key)
            WriteDebug('LoadSTATUS(' & CLIP(IN:Status) & ') False')

            RETURN False
        END !IF

        IF NOT sts:Status = IN:Status
            WriteDebug('LoadSTATUS(' & CLIP(IN:Status) & ') False')

            RETURN False
        END !IF

        WriteDebug('LoadSTATUS(' & CLIP(IN:Status) & ') True')
        RETURN True
        !-----------------------------------------------
LoadSUBTRACC PROCEDURE(IN:SubAccountNumber)! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('LoadSUBTRACC')

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:SubAccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:SubAccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC                    PROCEDURE(IN:HeadAccountNumber)! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('LoadTRADEACC(' & CLIP(IN:HeadAccountNumber) & ')')

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:HeadAccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF


        IF NOT tra:Account_Number = IN:HeadAccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadWEBJOB      PROCEDURE( IN:JobNumber )! LONG
    CODE
        !-----------------------------------------------
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = IN:JobNumber

        IF Access:WEBJOB.TryFetch(wob:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        IF NOT wob:RefNumber = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
!-----------------------------------------------
AddToHeadAccountQueue PROCEDURE(IN:HeadAccountNumber)! LONG ! BOOL
    CODE
        !-----------------------------------------------------------------
        WriteDebug('AddToHeadAccountQueue(' & CLIP(IN:HeadAccountNumber) & ')')

        IF CancelPressed
            RETURN False
        END !IF
        !-----------------------------------------------------------------
        hq:HeadAccountNumber = IN:HeadAccountNumber
        GET(HeadAccountQueue, +hq:HeadAccountNumber)

        CASE ERRORCODE()
        OF 00
            !------------------------------------------------------------
            ! Found
            IF hq:HeadAccountNumber <> IN:HeadAccountNumber
                WriteDebug('AddToHeadAccountQueue(' & CLIP(IN:HeadAccountNumber) & '), Partial Match(' & CLIP(hq:HeadAccountNumber) & ')')

                RETURN False
            END !IF
            !------------------------------------------------------------
            WriteDebug('AddToHeadAccountQueue FOUND="' & CLIP(hq:HeadAccountNumber) & '", hq:RepairCentreType="' & CLIP(hq:RepairCentreType) & '"')
                         
            CASE hq:RepairCentreType
            OF 'NO'
                RETURN False
            OF 'ARC'
                RETURN True
            OF 'RRC'
                RETURN True
            ELSE
                CancelPressed = True

                RETURN False
            END !CASE
            !------------------------------------------------------------
        OF 30 ! Entry Not Found
            ! Not in queue - ADD
            WriteDebug('AddToHeadAccountQueue(' & CLIP(IN:HeadAccountNumber) & '), NOT FOUND in HeadAccountQueue')

            RETURN False
        ELSE
            CancelPressed = True

            RETURN False
        END !IF
        !-----------------------------------------------------------------
AddToSubAccountQueue  PROCEDURE(IN:SubAccountNumber)! LONG ! BOOL
    CODE
        !-----------------------------------------------------------------
        WriteDebug('AddToSubAccountQueue(' & CLIP(IN:SubAccountNumber) & ')')

        IF CancelPressed
            RETURN False
        END !IF
        !-----------------------------------------------------------------
        sq:SubAccountNumber  = IN:SubAccountNumber
        GET(SubAccountQueue, +sq:SubAccountNumber)

        CASE ERRORCODE()
        OF 00
            ! Found
            IF sq:SubAccountNumber = IN:SubAccountNumber
                ! In case GET() matches a sub string
                !   ie if LOC:SubAccountNumber='VODA' and 'VODAPHONE1' is in queue
                !   a false match is generated
                !
                RETURN True
            END !IF

        OF 30 ! Entry Not Found
            ! Not in queue - ADD

        ELSE
            CancelPressed = True

            RETURN False
        END !IF

        IF NOT LoadSUBTRACC(IN:SubAccountNumber)
            sq:HeadAccountNumber    = ''
            sq:SubAccountNumber     = IN:SubAccountNumber
            sq:SubAccountName       = ''
            sq:HeadRepairCentreType = 'NO' ! default to exclude
            sq:JobsBooked           = 0
            sq:JobsDespatched       = 0

            ADD(SubAccountQueue, +sq:SubAccountNumber)

            RETURN False
        END ! IF

        IF AddToHeadAccountQueue(sub:Main_Account_Number)
            sq:HeadRepairCentreType = hq:RepairCentreType
        ELSE
            sq:HeadRepairCentreType = 'NO' ! default to exclude
        END ! IF

        sq:HeadAccountNumber = sub:Main_Account_Number
        sq:SubAccountNumber  = IN:SubAccountNumber
        sq:SubAccountName    = sub:Company_Name
        sq:JobsBooked        = 0
        sq:JobsDespatched    = 0

        ADD(SubAccountQueue, +sq:SubAccountNumber)

        WriteDebug('AddToSubAccountQueue ADD(' & CLIP(sq:HeadRepairCentreType) & ')')
        RETURN True
        !-----------------------------------------------------------------
CheckStatus PROCEDURE(IN:Status)! LONG ! BOOL
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CheckStatus(' & CLIP(IN:Status) & ')')

        IF CancelPressed
            RETURN False
        END !IF
        !-----------------------------------------------------------------
        status:Status = IN:Status
        GET(CurrentStatusQueue, +status:Status)

        CASE ERRORCODE()
        OF 00
            !------------------------------------------------------------
            ! Found
            IF status:Status <> IN:Status
                ! NULL (ADD)
            ELSIF status:UseTurnaroundTime = 'YES'
                WriteDebug('CheckStatus FOUND="' & CLIP(IN:Status) & '" OK')

                RETURN True
            ELSE
                WriteDebug('CheckStatus FOUND="' & CLIP(IN:Status) & '" FAIL')

                RETURN False
            END !IF
            !------------------------------------------------------------
        OF 30 ! Entry Not Found
            ! Not in queue - ADD
            ! NULL (ADD)

        ELSE
            CancelPressed = True

            RETURN False
        END !IF
        !-----------------------------------------------------------------
        IF LoadSTATUS(IN:Status)
            status:Status            = IN:Status
            status:UseTurnaroundTime = sts:Use_Turnaround_Time
        ELSE
            status:Status            = IN:Status
            status:UseTurnaroundTime = 'NO'
        END !IF

        ADD(CurrentStatusQueue, +status:Status)

        WriteDebug('CheckStatus(' & CLIP(IN:Status) & '), ADD status:Use_Turnaround_Time = "' & CLIP(status:UseTurnaroundTime) & '"')
        IF status:UseTurnaroundTime = 'YES'
            RETURN True
        ELSE
            RETURN False
        END !IF
        !-----------------------------------------------------------------
UpdateHeadAccountQueue PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        WriteDebug('UpdateHeadAccountQueue (' & CLIP(hq:HeadAccountNumber) &  ')')

        hq:JobsBooked += 1
        IF job:Date_Despatched <> 0
            hq:JobsDespatched += 1
        END !IF

        PUT(HeadAccountQueue)

        WriteDebug('UpdateHeadAccountQueue EXIT')
        !-----------------------------------------------------------------
UpdateSubAccountQueue  PROCEDURE(IN:SubAccountNumber)
    CODE
        !-----------------------------------------------------------------
        ! Sub acounts previously loaded during FillJobsQueue()
        !-----------------------------------------------------------------
        WriteDebug('UpdateSubAccountQueue')

        IF CancelPressed
            RETURN 
        END !IF
        !-----------------------------------------------------------------
        sq:SubAccountNumber  = IN:SubAccountNumber
        GET(SubAccountQueue, +sq:SubAccountNumber)

        CASE ERRORCODE()
        OF 00
            ! Found
            sq:JobsBooked         += 1

            IF job:Date_Despatched <> 0
                sq:JobsDespatched += 1
            END !IF

            PUT(SubAccountQueue, +sq:SubAccountNumber)

            RETURN 

        OF 30 ! Entry Not Found
            RETURN 

        ELSE
            CancelPressed = True

            RETURN 
        END !IF
        !-----------------------------------------------------------------
UpdateSummaryQueue                        PROCEDURE()
JobTypeOffset  BYTE(1)
Action         STRING(3)
    CODE
        !-----------------------------------------------------------------
        WriteDebug('UpdateSummaryQueue CurrentStatus="' & CLIP(LOC:CurrentStatus) & '"')

        IF CancelPressed = True
            RETURN
        END !IF
        !-----------------------------------------------------------------
        CASE LOC:JobType
        OF 'JOB'
            JobTypeOffset     = 1
        OF 'EXC'
            JobTypeOffset     = 2
        OF 'LOA'
            JobTypeOffset     = 3
        ELSE
            JobTypeOffset     = 1
        END !IF
        !-----------------------------------------------------------------
        summq:CurrentStatus = LOC:CurrentStatus
        GET(SummaryQueue, +summq:CurrentStatus)

        CASE ERRORCODE()
        OF 00 ! Found
            IF summq:CurrentStatus = LOC:CurrentStatus
                Action = 'PUT'
            ELSE
                Action = 'ADD'
            END !IF

        OF 30 ! Not Found
            Action = 'ADD'

        ELSE
            CancelPressed = True

            RETURN 
        END !CASE
        !-----------------------------------------------------------------
        IF Action = 'ADD'
            WriteDebug('UpdateSummaryQueue ADD')

            CLEAR(SummaryQueue)
                ! Not in queue - ADD
                summq:CurrentStatus        = LOC:CurrentStatus
!                summq:TurnaroundTime       = 0
!                summq:StatusType[1]        = False
!                summq:StatusType[2]        = False
!                summq:StatusType[3]        = False
!
!                LOOP x# = 1 TO 3
!                    summq:NumberOfJobs[x#] = 0
!                    summq:OverdueHours[x#] = 0
!                END !LOOP

                IF CheckStatus(LOC:CurrentStatus)
                    !-----------------------------------------------------
                    summq:TurnaroundTime      = (sts:Turnaround_Days * hq:WorkingHours) + sts:Turnaround_Hours

                    IF (sts:Job = 'YES')
                        WriteDebug('UpdateSummaryQueue ADD StatusJob=True')
                        summq:StatusType[1]       = TRUE
                    ELSE
                        WriteDebug('UpdateSummaryQueue ADD StatusJob=False')
                    END !IF

                    IF (sts:Exchange = 'YES')
                        WriteDebug('UpdateSummaryQueue ADD StatusExchange=True')
                        summq:StatusType[2]       = TRUE
                    ELSE
                        WriteDebug('UpdateSummaryQueue ADD StatusExchange=False')
                    END !IF

                    IF (sts:Loan = 'YES')
                        WriteDebug('UpdateSummaryQueue ADD StatusLoan=True')
                        summq:StatusType[3]       = TRUE
                    ELSE
                        WriteDebug('UpdateSummaryQueue ADD StatusLoan=False')
                    END !IF
                    !-----------------------------------------------------
                END ! IF
            ADD(SummaryQueue, +summq:CurrentStatus)
        END !IF Action = 'ADD'
        !-----------------------------------------------------------------
        ! In queue - AMEND
        !
        IF hq:UseTimingsFrom = 'TRA'
            calc:OverdueHours = WorkingHoursBetween(job:Status_End_Date, LOC:Today, job:Status_End_Time, LOC:Clock,  tra:IncludeSaturday,  tra:IncludeSunday)
        ELSE
            calc:OverdueHours = WorkingHoursBetween(job:Status_End_Date, LOC:Today, job:Status_End_Time, LOC:Clock, def:Include_Saturday, def:Include_Sunday)
        END !IF

        summq:NumberOfJobs[JobTypeOffset] += 1
        summq:OverdueHours[JobTypeOffset] += calc:OverdueHours

        PUT(SummaryQueue, +summq:CurrentStatus)
        !-----------------------------------------------------------------
!-----------------------------------------------
After   PROCEDURE (LookFor, LookIn) ! STRING
FirstChar     LONG
LEN_LookFor   LONG
    CODE
        !-----------------------------------------------------------------
        WriteDebug('After(' & LookFor & ', ' & CLIP(LookIn) & ')')

        FirstChar = INSTRING( CLIP(LEFT(LookFor)), LookIn )
        IF FirstChar = 0
            RETURN ''
        END !IF

        LEN_LookFor  = LEN(CLIP(LEFT(LookFor)))
        FirstChar   += LEN_LookFor

        RETURN SUB(LookIn, FirstChar, LEN_LookFor - FirstChar)
        !-----------------------------------------------------------------
AppendString                    PROCEDURE( IN:Left, IN:Right, IN:Separator)
    CODE
        !-----------------------------------------------------------------
        IF CLIP(IN:Left)=''
            WriteDebug('AppendString(' & CLIP(IN:Left) & ', ' & CLIP(IN:Right) & ', ' & IN:Separator & ')="' & CLIP(IN:Right) & '"')
            RETURN IN:Right

        ELSIF CLIP(IN:Right)=''
            WriteDebug('AppendString(' & CLIP(IN:Left) & ', ' & CLIP(IN:Right) & ', ' & IN:Separator & ')="' & CLIP(IN:Left) & '"')
            RETURN IN:Left

        END !IF

        WriteDebug('AppendString(' & CLIP(IN:Left) & ', ' & CLIP(IN:Right) & ', ' & IN:Separator & ')="' & CLIP(IN:Left) & IN:Separator & CLIP(IN:Right) & '"')
        RETURN CLIP(IN:Left) & IN:Separator & CLIP(IN:Right)
        !-----------------------------------------------------------------
Before   PROCEDURE (LookFor, LookIn) ! STRING
FirstChar     LONG
    CODE
        !-----------------------------------------------------------------
        WriteDebug('Before(' & LookFor & ', ' & CLIP(LookIn) & ')')

        FirstChar = INSTRING( CLIP(LEFT(LookFor)), LookIn )
        IF FirstChar = 0
            RETURN ''
        END !IF

        RETURN SUB(LookIn, 1, FirstChar - 1)
        !-----------------------------------------------------------------
GetARCLocation          PROCEDURE()! LONG ! BOOL
tmp:ARCLocation LIKE(LOC:ARCLocation)
    CODE
        !-----------------------------------------------
        WriteDebug('GetARCLocation')

        LOC:ARCLocation = GETINI('RRC', 'ARCLocation', '', '.\SB2KDEF.INI')

        IF LOC:ARCLocation = ''
            RETURN False
        ELSE
            RETURN True
        END !IF
        !-----------------------------------------------

GetRRCLocation          PROCEDURE()! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('GetRRCLocation')

        LOC:RRCLocation = GETINI('RRC','RRCLocation', '', '.\SB2KDEF.INI')

        IF LOC:RRCLocation = ''
            RETURN False
        ELSE
            RETURN True
        END !IF
        !-----------------------------------------------

NumberToColumn PROCEDURE(IN:Long)!STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        WriteDebug('NumberToColumn')

        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
ParseParameters PROCEDURE()! STRING ! BOOL
Cmd STRING(255)
Tmp STRING(255)
    CODE
        !-----------------------------------------------
        cmd = COMMAND()

        param:Name  = 'UserCode'
        param:Value = LEFT(CLIP(After('%', cmd)))
        Tmp = AddUserName(param:Value)
        IF CLIP(Tmp) <> ''
            RETURN Tmp
        END !IF
        PUT(ParameterQueue, +param:Name)
        !-----------------------------------------------
        IF NOT AddParamter('Mode')
            ! Set default mode to GUI (interactive with user)
            param:Name  = 'Mode'
            param:Value = 'GUI'
            PUT(ParameterQueue, +param:Name)
        ELSE
            CASE UPPER(param:Value)
            OF 'CONFIG' OROF 'INSTALL' OROF 'GUI' OROF 'SILENT'
                ! NULL
            ELSE
                RETURN 'Report Called Using Unknown Mode "' & param:Value & '"'
            END !CASE 
        END !IF
        !-----------------------------------------------
        RETURN ''
        !-----------------------------------------------
AddParamter PROCEDURE(IN:ParamName)! LONG ! BOOL
Cmd              STRING(255)
ParamString      STRING(255)
SubParametersPos LONG
    CODE
        !-----------------------------------------------
        Cmd = COMMAND()

        IF NOT MATCH( ' ' & IN:ParamName & ' *= *\(', Cmd, Match:Regular)
            WriteDebug('AddParamter(' & IN:ParamName & ') NO MATCH')

            RETURN False
        END !IF

        ParamString = LEFT( After(' ' & IN:ParamName, Cmd))
        ParamString = LEFT( After(               '=', Cmd))
        ParamString = LEFT( After(               '(', Cmd))
        ParamString = CLIP(Before(               ')', Cmd))

        SubParametersPos = INSTRING(',', ParamString, 1, 1) 
        IF SubParametersPos > 0
            ParamString         = CLIP(Before(',', ParamString))
            param:SubParameters = LEFT( After(',', ParamString))
        ELSE
            param:SubParameters = ''
        END !IF

        param:Name  = UPPER(IN:ParamName)
        param:Value = ParamString
        PUT(ParameterQueue, +param:Name)

        WriteDebug('AddParamter(' & IN:ParamName & '), Name="' & IN:ParamName & '", Value="' & param:Value & '", Params="' & param:SubParameters & '"')
        !-----------------------------------------------
GetParameter    PROCEDURE(IN:Name)! LONG ! BOOL
    CODE
        !-----------------------------------------------
        CLEAR(ParameterQueue)
            param:Name  = UPPER(IN:Name)
        GET(ParameterQueue, +param:Name)
        !-----------------------------------------------
        CASE ERRORCODE()
        OF 00
            RETURN True
        OF 30
            RETURN False
        ELSE
            CancelPressed = True

            RETURN False
        END !CASE
        !-----------------------------------------------
AddUserName     PROCEDURE(IN:UserCode)! STRING ! BOOL
tmpPos      LONG
    CODE
        !-----------------------------------------------
        IF CLIP(IN:UserCode) = ''
            RETURN 'Attempting to use ' & CLIP(LOC:ProgramName) & '<10,13>'              & |
                   '   without using ' & LOC:ApplicationName & '.<10,13>'                & |
                   '   Start ' & LOC:ApplicationName & ' and run the report from there.'   |
        END !IF CLIP(IN:UserCode) = ''
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(IN:UserCode)
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
            RETURN 'Unable to find your logged in user details.'
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = AppendString(use:Forename, use:Surname, ' ')

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF

        RETURN ''
        !-----------------------------------------------

WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        IF debug:Active = False
            RETURN
        END ! IF

        debug:Count += 1
        PUTINI( debug:Name, debug:Count, IN:Message, 'C:\DEBUG.ini')
        !-----------------------------------------------
!-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = '*')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue

