

   MEMBER('vodr0078.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


RRC_TATReport PROCEDURE                                    ! Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
TempFilePath         CSTRING(255)                          !
tmp:VersionNumber    STRING(30)                            !Version Number
save_locatlog_id     USHORT,AUTO                           !
save_aus_id          USHORT                                !
save_aud_id          USHORT                                !
save_wob_id          USHORT                                !
save_joc_id          USHORT                                !
save_joboutfl_id     USHORT,AUTO                           !
tmp:StartDate        DATE                                  !Jobs Completed From
tmp:EndDate          DATE                                  !Jobs Completed To
tmp:HeadAccountNumber STRING(30)                           !Head Account Number
tmp:ARCLocation      STRING(30)                            !ARC Location
tmp:CurrentRow       LONG                                  !Current Row
tmp:CurrentColumn    LONG                                  !Current Column
Progress:Thermometer BYTE(0)                               !Moving Bar
StatusQueue          QUEUE,PRE(staque)                     !
StatusMessage        STRING(60)                            !Status Box
Order                LONG                                  !Order
                     END                                   !
TotalRepairQueue     QUEUE,PRE(totrepque)                  !
Date                 DATE                                  !Date
Time                 TIME                                  !Time
Type                 STRING(1)                             !Type
Location             STRING(3)                             !Location
                     END                                   !
JobHistoryQueue      QUEUE,PRE(jobhis)                     !
TheDate              DATE                                  !Date
TheDay               STRING(30)                            !Day
StartTime            TIME                                  !Start Time
StopTime             TIME                                  !Stop Time
UnitStatus           STRING(30)                            !Unit Status
ControlTime          TIME                                  !Control Time
RealTime             TIME                                  !Real Time
                     END                                   !
tmp:TotalTime        LONG                                  !Total Time
tmp:ARCRealTotalTime LONG                                  !Real Time RCP
tmp:RRCRealTotalTime LONG                                  !RCP Total Time
tmp:CustomerTotalTime LONG                                 !Customer Perspective
TotalGroup           GROUP,PRE()                           !
tmp:TotalRRCInControl LONG                                 !Total RRC In Control
tmp:TotalARCWaitTime LONG                                  !Total Wait Time
tmp:TotalRRCWaitTime LONG                                  !Total RRC Wait Time
tmp:TotalARCCourierTime LONG                               !Total Courier Time
tmp:TotalRRCCourierTime LONG                               !Total RRC Courier Time
tmp:TotalARCInControl LONG                                 !Total ARC In Control
tmp:Total3rdParty    LONG                                  !Total 3rd Party
tmp:TotalReadyToDespatch LONG                              !Total Ready To Despatch
tmp:TotalCustomerCollection LONG                           !Customer Collection
tmp:TotalRRCOutOfControl LONG                              !Total RRC Out Of Control
tmp:TotalARCOutOfControl LONG                              !Total ARC Out Of Control
tmp:RealTotalRRCInControl LONG                             !RealTotalRRCInControl
tmp:RealTotalARCWaitTime LONG                              !Real Total Wait Time
tmp:RealTotalRRCWaitTime LONG                              !Real Total RRC Wait Time
tmp:RealTotalARCCourierTime LONG                           !Real Total Courier Time
tmp:RealTotalRRCCourierTime LONG                           !Real Total RRC Courier Time
tmp:RealTotalARCInControl LONG                             !Real Total ARC In Control
tmp:RealTotal3rdParty LONG                                 !Real Total 3rd Party
tmp:RealTotalReadyToDespatch LONG                          !Real Total Ready To Despatch
tmp:RealTotalCustomerCollection LONG                       !Real Total Customer Collection
tmp:RealTotalRRCOutOfControl LONG                          !Real Total RRC Out Of Control
tmp:RealTotalARCOutOfControl LONG                          !Real Total ARC Out Of Control
tmp:CustomerTotalARCOutOfControl LONG                      !Customer Total ARC Out Of Control
tmp:CustomerTotalRRCOutOfControl LONG                      !Customer Total RRC Out Of Control
tmp:ARC1hrTotalTime  LONG                                  !ARC 1 hr Total Time
tmp:RRC1hrTotalTime  LONG                                  !RRC 1 Hr Total Time
tmp:RealARC1hrTotalTime LONG                               !Real ARC 1hr Total Time
tmp:RealRRC1hrTotalTime LONG                               !Real RRC 1hr Total Time
tmp:CustARC1HrTotalTime LONG                               !Cust ARC 1Hr Total Time
tmp:CustRRC1HrTotalTime LONG                               !Cust RRC 1Hr Total Time
tmp:TotalRealARCEstimateTime LONG                          !Total Real ARC Estimate Time
tmp:TotalRealRRCEstimateTime LONG                          !Total Real RRC Estimate Time
tmp:TotalARCEstimateTime LONG                              !Total ARC Estimate Time
tmp:TotalRRCEstimateTime LONG                              !Total RRC Estimate Time
tmp:RealTotalARCEstimateTime LONG                          !Real Total ARC Estimate Time
tmp:RealTotalRRCEstimateTime LONG                          !Real Total RRC Estimate Time
tmp:CustomerTotalARCEstimateTime LONG                      !Customer Total ARC Estimate Time
tmp:CustomerTotalRRCEstimateTime LONG                      !Customer Total RRC Estimate Time
tmp:CustomerTotalARCUnlockCodeTime LONG                    !
tmp:CustomerTotalRRCUnlockCodeTime LONG                    !
                     END                                   !
CurrentActionGroup   GROUP,PRE()                           !
tmp:RRCInControl     BYTE(0)                               !RRC In Control
tmp:ARCWaitTime      BYTE(0)                               !Wait Time
tmp:RRCWaitTime      BYTE(0)                               !RRC Wait Time
tmp:ARCCourierTime   BYTE(0)                               !Courier Time
tmp:RRCCourierTime   BYTE(0)                               !RRC Courier Time
tmp:ARCInControl     BYTE(0)                               !ARC In Control
tmp:3rdParty         BYTE(0)                               !3rd Party
tmp:ReadyToDespatch  BYTE(0)                               !Ready To Despatch
tmp:CustomerCollection BYTE(0)                             !Customer Collection
tmp:RRCOutOfControl  BYTE(0)                               !RRC Out Of Control
tmp:ARCOutOfControl  BYTE(0)                               !ARC Out Of Control
tmp:ARCEstimateTime  BYTE(0)                               !ARC Estimate Time
tmp:RRCEstimateTime  BYTE(0)                               !RRC Estimate TIme
tmp:ARCUnlockCodeTime BYTE                                 !
tmp:RRCUnlockCodeTime BYTE                                 !
                     END                                   !
CurrentActionDatesGroup GROUP,PRE()                        !
tmp:RRCInControlStartDate DATE                             !RRC In Control Start Date
tmp:RRCInControlStartTime TIME                             !RRC In Control Start Time
tmp:ARCWaitTimeStartDate DATE                              !Wait Time Start Date
tmp:ARCWaitTimeStartTime TIME                              !Wait Time Start Time
tmp:RRCWaitTimeStartDate DATE                              !RRC Wait Time Start Date
tmp:RRCWaitTimeStartTime TIME                              !RRC Wait Time Start Time
tmp:ARCCourierTimeStartDate DATE                           !Courier Time Start Date
tmp:ARCCourierTimeStartTime TIME                           !Courier Time Start Time
tmp:RRCCourierTimeStartDate DATE                           !RRC Courier Start Date
tmp:RRCCourierTimeStartTime TIME                           !RRC Courier Time Start Time
tmp:ARCInControlStartDate DATE                             !ARC In Control Start Date
tmp:ARCInControlStartTime TIME                             !ARC In Control Start Time
tmp:3rdPartyStartDate DATE                                 !3rd Party Start Date
tmp:3rdPartyStartTime TIME                                 !3rd Party Start Time
tmp:ReadyToDespatchStartDate DATE                          !Ready To Despatch Start Date
tmp:ReadyToDespatchStartTime TIME                          !Ready To Despatch Start Time
tmp:CustomerCollectionStartDate DATE                       !Customer Collection Start Date
tmp:CustomerCollectionStartTime TIME                       !Customer Collection Start Time
tmp:RRCOutOfControlStartDate DATE                          !RRC Out Of Control Start Date
tmp:RRCOutOfControlStartTime TIME                          !RRC Out Of Control Start Time
tmp:ARCOutOfControlStartDate DATE                          !ARC Out Of Control Start Date
tmp:ARCOutOfControlStartTime TIME                          !ARC Out Of Control Start Time
tmp:ARCEstimateTimeStartDate DATE                          !ARC Estimate Start Date
tmp:ARCEstimateTimeStartTime TIME                          !ARC Estimate Time Start Time
tmp:RRCEstimateTimeStartDate DATE                          !RRC Estimate Time Start Date
tmp:RRCEstimateTimeStartTime TIME                          !RRC Estimate Time Start Time
tmp:ARCUnlockCodeStartDate DATE                            !
tmp:ARCUnlockCodeStartTime TIME                            !
tmp:RRCUnlockCodeStartDate DATE                            !
tmp:RRCUnlockCodeStartTime TIME                            !
                     END                                   !
tmp:SentToARCDate    DATE                                  !Sent To ARC Date
tmp:SentToARCTime    TIME                                  !Time Sent To ARC
tmp:SentToRRCDate    DATE                                  !Date Sent To RRC
tmp:SentToRRCTime    TIME                                  !Time Sent To RRC
tmp:LastCompletedDate DATE                                 !Last Completed Date
tmp:LastCompletedTime TIME                                 !Last Completed Time
tmp:ARCAccount       BYTE(0)                               !Reporting on ARC?
tmp:ARCCompanyName   STRING(30)                            !Company Name
tmp:ARCUser          BYTE(0)                               !ARC User
tmp:ExchangeUnitAttached BYTE(0)                           !Exchange Unit Attached
tmp:ExchangeAttachedDate DATE                              !Exchange Attached Date
tmp:ExchangeAttachedTime TIME                              !Exchange Attached Time
tmp:2ndExchangeUnitAttached BYTE(0)                        !2nd Exchange Unit Attached
tmp:2ndExchangeAttachedDate DATE                           !Second Exchange Unit Attached Date
tmp:2ndExchangeAttachedTime TIME                           !Second Exchange Unit Attached TIME
tmp:LoanUnitAttached BYTE(0)                               !Loan Unit Attached
tmp:LoanAttachedDate DATE                                  !Date Loan First Attached
tmp:LoanAttachedTime TIME                                  !Time Loan First Attached
tmp:LoanTime         LONG                                  !Time To Attach A Loan
RepairCentreQueue    QUEUE,PRE(rep1)                       !
AccountNumber        STRING(30)                            !Account Number
CompanyName          STRING(30)                            !Company Name
Type                 STRING(30)                            !2
Count                LONG                                  !Count
                     END                                   !
RepairCentreRealQueue QUEUE,PRE(rep2)                      !
AccountNumber        STRING(30)                            !Account Number
CompanyName          STRING(30)                            !Company Name
Type                 STRING(2)                             !Type
Count                LONG                                  !Count
                     END                                   !
CustomerPerspectiveQueue QUEUE,PRE(cus)                    !
AccountNumber        STRING(30)                            !Account Number
CompanyName          STRING(30)                            !Company Name
Type                 STRING(2)                             !Type
Count                LONG                                  !Count
                     END                                   !
LoanCountQueue1      QUEUE,PRE(loaque1)                    !Not Using "Real Time"
AccountNumber        STRING(30)                            !Account Number
LoanType             STRING(2)                             !Loan Type
LoanCount            LONG                                  !Count
                     END                                   !
LoanCountQueue2      QUEUE,PRE(loaque2)                    !Using "Real Time"
AccountNumber        STRING(30)                            !Account Number
LoanType             STRING(2)                             !Loan Type
LoanCount            LONG                                  !Count
                     END                                   !
LoanCountQueue3      QUEUE,PRE(loaque3)                    !
AccountNumber        STRING(30)                            !AccountNumber
LoanType             STRING(2)                             !LoanType
LoanCount            LONG                                  !LoanCount
                     END                                   !
TotalJobsQueue       QUEUE,PRE(totque)                     !
AccountNumber        STRING(30)                            !Account Number
TotalCompletedJobs   LONG                                  !Total Completed Jobs
TotalBookedJobs      LONG                                  !Total Booked Jobs
TotalOpenJobs        LONG                                  !Open Jobs
                     END                                   !
TempFileQueue        QUEUE,PRE(tmpque)                     !
AccountNumber        STRING(30)                            !Account Number
CompanyName          STRING(30)                            !Company Name
Count                LONG                                  !Count
FileName             STRING(255)                           !FileName
JobsBooked           LONG                                  !Jobs Booked
JobsOpen             LONG                                  !Jobs Open
                     END                                   !
tmp:Clipboard        ANY                                   !
GrandARCTotalGroup   GROUP,PRE(tmparc)                     !
GrandTotalRepair     LONG                                  !Grand Total Repair
GrandARCInControl    LONG                                  !Grand Total RRC In Control
GrandReadyToDespatch LONG                                  !Grand Total Ready To Despatch
GrandCustomerCollection LONG                               !Grand Total Customer Collection
GrandWaitTime        LONG                                  !Grand Total Wait Time
GrandCourierTime     LONG                                  !Grand Total Courier Time
Grand3rdParty        LONG                                  !GrandTotal3rdParty
                     END                                   !
GrandRRCTotalGroup   GROUP,PRE(tmprrc)                     !
GrandTotalRepair     LONG                                  !Grand TotalRepair
GrandRRCInControl    LONG                                  !Grand Total RRC In Repair
GrandReadyToDespatch LONG                                  !Grand Total Ready To Despatch
GrandCustomerCollection LONG                               !Grand Total Customer Collection
GrandWaitTime        LONG                                  !Grand Total Wait Time
GrandCourierTime     LONG                                  !Grand Total Courier Time
Grand3rdParty        LONG                                  !Grand Total 3rd Party
                     END                                   !
tmp:ARCJobs          LONG                                  !Count Of ARC Jobs
tmp:RRCJobs          LONG                                  !Count Of RRC Jobs
Automatic            BYTE(0)                               !Automated Report
tmp:AllAccounts      BYTE(0)                               !All Accounts
tmp:Tag              STRING(1)                             !Tag
tmp:SummaryOnly      BYTE(0)                               !Summary Only
tmp:ARCExchangeSentToRRC BYTE(0)                           !ARC Exchange Sent To RRC
tmp:WhoDespatchedTheJob STRING(3)                          !Who Despatched To Job
tmp:ARCJobFinished   BYTE(0)                               !ARC Job Finished
tmp:RRCJobFinished   BYTE(0)                               !RRC Job Finished
tmp:ARCCustomerPerspective LONG                            !ARC Customer Perspective
tmp:RRCCustomerPerspective LONG                            !RRC Customer Perspective
excel:ProgramName    STRING(255)                           !
excel:Filename       STRING(255)                           !
loc:Version          STRING(30)                            !
BRW8::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Vodacare TAT Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('RRC TAT Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,57,552,308),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Criteria'),USE(?Tab1),FONT(,,COLOR:White,,CHARSET:ANSI)
                           PROMPT('Jobs Completed From'),AT(245,74),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(342,74,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           PROMPT('Jobs Completed To'),AT(245,93),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(342,94,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Jobs Completed To'),TIP('Jobs Completed To'),REQ,UPR
                           CHECK('All Accounts'),AT(245,106),USE(tmp:AllAccounts),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Accounts'),TIP('All Accounts'),VALUE('1','0')
                           PROMPT(''),AT(72,313),USE(?StatusText)
                           CHECK('Summary Only'),AT(468,301),USE(tmp:SummaryOnly),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Summary Only'),TIP('Summary Only'),VALUE('1','0')
                           BUTTON('&Rev tags'),AT(146,166,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(150,190,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(460,185),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(460,217),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(460,249),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                         END
                       END
                       LIST,AT(228,122,224,236),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,09A6A7CH),MSG('Browsing Records'),FORMAT('10L(2)I@s1@60L(2)~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                       BUTTON,AT(472,366),USE(?Print),TRN,FLAT,ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(544,366),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       PROMPT('Report Version:'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
ExportFile    File,Driver('BASIC'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
JobNumber           String(30)
BranchNumber        String(30)
HeadAccount         String(30)
SubAccount          String(30)
Manufacturer        String(30)
ModelNumber         String(30)
Warranty            String(3)
JobsbookedInDate    String(30)
JobsBookedInTime    String(30)
JobsCompletedDate   String(30)
JobsCOmpletedTime   String(30)
TotalRepairTimeDays String(30)
TotalRepairTimeTime String(30)
InControlDays       String(30)
InControlTime       String(30)
ReadyToDespatchDays String(30)
ReadyToDespatchTime String(30)
CustomerCollectionDays  String(30)
CustomerCollectionTime  String(30)
WaitTimeDays        String(30)
WaitTimeTime        String(30)
CourierMovementDays String(30)
CourierMovementTime String(30)
ThirdPartyTimeDays  String(30)
ThirdPartyTimeTime  String(30)
LoanIssued          String(3)
RCP                 String(15) !Repair Centre Perspective
RTRCP               String(15) !Real Time Repair Center Perspective
CP                  String(15) !Customer Perspective

                        End
                    End

ARCExportFile    File,Driver('BASIC'),Pre(arcexp),Name(glo:ARCExportFile),Create,Bindable,Thread
Record                  Record
JobNumber           String(30)
BranchNumber        String(30)
HeadAccount         String(30)
SubAccount          String(30)
Manufacturer        String(30)
ModelNumber         String(30)
Warranty            String(3)
JobsbookedInDate    String(30)
JobsBookedInTime    String(30)
JobsCompletedDate   String(30)
JobsCOmpletedTime   String(30)
TotalRepairTimeDays String(30)
TotalRepairTimeTime String(30)
InControlDays       String(30)
InControlTime       String(30)
ReadyToDespatchDays String(30)
ReadyToDespatchTime String(30)
CustomerCollectionDays  String(30)
CustomerCollectionTime  String(30)
WaitTimeDays        String(30)
WaitTimeTime        String(30)
CourierMovementDays String(30)
CourierMovementTime String(30)
ThirdPartyTimeDays  String(30)
ThirdPartyTimeTime  String(30)
LoanIssued          String(3)
RCP                 String(15) !Repair Centre Perspective
RTRCP               String(15) !Real Time Repair Center Perspective
CP                  String(15) !Customer Perspective

                        End
                    End
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
progressThermometer Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
ProgressSetup      Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.UserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW8                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Fetch                  PROCEDURE(BYTE Direction),DERIVED   ! Method added to host embed code
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeNewSelection       PROCEDURE(),DERIVED                 ! Method added to host embed code
ValidateRecord         PROCEDURE(),BYTE,DERIVED            ! Method added to host embed code
                     END

BRW8::Sort0:Locator  StepLocatorClass                      ! Default Locator
Local                      CLASS

DrawBox                        Procedure(String func:TL, String func:TR, String func:BL, String func:BR, Long func:Colour)
DrawColumns                    Procedure (Byte func:Type)
TimeDifference                 Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime, String func:AccountNumber),Long
RealTimeDifference             Procedure(Date f:StartDate, Date f:EndDate, Time f:StartTime, Time f:EndTime, Byte f:Type),Long
RealTimeDifferenceCourier      Procedure(Date f:StartDate, Date f:EndDate, Time f:StartTime, Time f:EndTime, String f:Courier),Long
CustomerPerspective            Procedure(Date f:StartDate, Date f:EndDate, Time f:StartTime, Time f:EndTime, Byte f:Type),Long
ARCUser                        Procedure(String func:User),Byte
AtARC                          Procedure(Date func:NowDate, Time func:NowTime),Byte
WriteLine                      Procedure(String func:Type)
GetSummaryType                 Procedure(Long func:Time),String
GetSummaryTypeLoan             Procedure(Long func:Time),String
GetSummaryTypeName             Procedure(Long func:Time),String
SummaryTitles                  Procedure(String func:Title, Long func:Position)
DrawSummaryLineTotalBar        Procedure(Long func:Row)
WriteSummaryLineTotals         Procedure(Long func:Row)
UpdateProgressWindow           Procedure(String  func:Text)
SubAccountExcludedFromReport   Procedure(String func:AccountNumber),Byte
WriteSummarySectionTotal       Procedure(Long func:Row, Long func:StartRow)
WriteSummaryLoanLine           Procedure(String func:AccountNumber, Long func:Row, Byte func:Type)
GetStartDate                   Procedure(*Date f:StartDate,*Time f:StartTime,Byte f:Type)

                           END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

RecolourWindow      Routine
    Do RecolourWindow:Window
    If ?PanelFalse{prop:Fill} < 0
        If ?PanelFalse{prop:Fill} <> -1
            ?PanelFalse{prop:Fill} = -1
        End
    End
    If ?SRNNumber{Prop:FontColor} < 0
        If ?SRNNumber{prop:FontColor} <> -1
            ?SRNNumber{prop:FontColor} = -1
        End
    End
    If ?SRNNumber{Prop:Color} < 0
        If ?SRNNumber{prop:Color} <> -1
            ?SRNNumber{prop:Color} = -1
        End
    End
    If ?WindowTitle{Prop:FontColor} < 0
        If ?WindowTitle{prop:FontColor} <> -1
            ?WindowTitle{prop:FontColor} = -1
        End
    End
    If ?WindowTitle{Prop:Color} < 0
        If ?WindowTitle{prop:Color} <> -1
            ?WindowTitle{prop:Color} = -1
        End
    End
    If ?PanelButton{prop:Fill} < 0
        If ?PanelButton{prop:Fill} <> -1
            ?PanelButton{prop:Fill} = -1
        End
    End
    If ?Sheet1{prop:Trn} <> 0
        ?Sheet1{prop:Trn} = 0
    End
    If ?Sheet1{prop:Color} < 0
        If ?Sheet1{prop:Color} <> -1
            ?Sheet1{prop:Color} = -1
        End
    End
    If ?Tab1{prop:Trn} <> 0
        ?Tab1{prop:Trn} = 0
    End
    If ?Tab1{prop:Color} < 0
        If ?Tab1{prop:Color} <> -1
            ?Tab1{prop:Color} = -1
        End
    End
    If ?tmp:StartDate:Prompt{Prop:FontColor} < 0
        If ?tmp:StartDate:Prompt{prop:FontColor} <> -1
            ?tmp:StartDate:Prompt{prop:FontColor} = -1
        End
    End
    If ?tmp:StartDate:Prompt{Prop:Color} < 0
        If ?tmp:StartDate:Prompt{prop:Color} <> -1
            ?tmp:StartDate:Prompt{prop:Color} = -1
        End
    End
        ?tmp:StartDate{prop:Trn} = 0
    If ?tmp:StartDate{prop:ReadOnly} = True
        If ?tmp:StartDate{prop:FontColor} <> 65793
            ?tmp:StartDate{prop:FontColor} = 65793
        End
        If ?tmp:StartDate{prop:Color} <> 12632256
            ?tmp:StartDate{prop:Color} = 12632256
        End
        If ?tmp:StartDate{prop:Trn} <> 0
            ?tmp:StartDate{prop:Trn} = 0
        End
    Elsif ?tmp:StartDate{prop:Req} = True
        If ?tmp:StartDate{prop:FontColor} <> 0
            ?tmp:StartDate{prop:FontColor} = 0
        End
        If ?tmp:StartDate{prop:Color} <> 11206655
            ?tmp:StartDate{prop:Color} = 11206655
        End
    Else ! If ?tmp:StartDate{prop:Req} = True
        If ?tmp:StartDate{prop:FontColor} <> 0
            ?tmp:StartDate{prop:FontColor} = 0
        End
        If ?tmp:StartDate{prop:Color} <> 16777215
            ?tmp:StartDate{prop:Color} = 16777215
        End
    End ! If ?tmp:StartDate{prop:Req} = True

    If ?tmp:EndDate:Prompt{Prop:FontColor} < 0
        If ?tmp:EndDate:Prompt{prop:FontColor} <> -1
            ?tmp:EndDate:Prompt{prop:FontColor} = -1
        End
    End
    If ?tmp:EndDate:Prompt{Prop:Color} < 0
        If ?tmp:EndDate:Prompt{prop:Color} <> -1
            ?tmp:EndDate:Prompt{prop:Color} = -1
        End
    End
        ?tmp:EndDate{prop:Trn} = 0
    If ?tmp:EndDate{prop:ReadOnly} = True
        If ?tmp:EndDate{prop:FontColor} <> 65793
            ?tmp:EndDate{prop:FontColor} = 65793
        End
        If ?tmp:EndDate{prop:Color} <> 12632256
            ?tmp:EndDate{prop:Color} = 12632256
        End
        If ?tmp:EndDate{prop:Trn} <> 0
            ?tmp:EndDate{prop:Trn} = 0
        End
    Elsif ?tmp:EndDate{prop:Req} = True
        If ?tmp:EndDate{prop:FontColor} <> 0
            ?tmp:EndDate{prop:FontColor} = 0
        End
        If ?tmp:EndDate{prop:Color} <> 11206655
            ?tmp:EndDate{prop:Color} = 11206655
        End
    Else ! If ?tmp:EndDate{prop:Req} = True
        If ?tmp:EndDate{prop:FontColor} <> 0
            ?tmp:EndDate{prop:FontColor} = 0
        End
        If ?tmp:EndDate{prop:Color} <> 16777215
            ?tmp:EndDate{prop:Color} = 16777215
        End
    End ! If ?tmp:EndDate{prop:Req} = True

    If ?tmp:AllAccounts{prop:Font,3} < 0
        If ?tmp:AllAccounts{prop:Font,3} <> -1
            ?tmp:AllAccounts{prop:Font,3} = -1
        End
    End ! If ?tmp:AllAccounts{prop:FontColor} < 0
    IF ?tmp:AllAccounts{prop:Color} < 0
        If ?tmp:AllAccounts{prop:Color} <> -1
            ?tmp:AllAccounts{prop:Color} = -1
        End
    End ! IF ?tmp:AllAccounts{prop:Color} < 0
    If ?tmp:AllAccounts{prop:Trn} <> 1
        ?tmp:AllAccounts{prop:Trn} = 1
    End
    If ?StatusText{Prop:FontColor} < 0
        If ?StatusText{prop:FontColor} <> -1
            ?StatusText{prop:FontColor} = -1
        End
    End
    If ?StatusText{Prop:Color} < 0
        If ?StatusText{prop:Color} <> -1
            ?StatusText{prop:Color} = -1
        End
    End
    If ?tmp:SummaryOnly{prop:Font,3} < 0
        If ?tmp:SummaryOnly{prop:Font,3} <> -1
            ?tmp:SummaryOnly{prop:Font,3} = -1
        End
    End ! If ?tmp:SummaryOnly{prop:FontColor} < 0
    IF ?tmp:SummaryOnly{prop:Color} < 0
        If ?tmp:SummaryOnly{prop:Color} <> -1
            ?tmp:SummaryOnly{prop:Color} = -1
        End
    End ! IF ?tmp:SummaryOnly{prop:Color} < 0
    If ?tmp:SummaryOnly{prop:Trn} <> 1
        ?tmp:SummaryOnly{prop:Trn} = 1
    End
    If ?List{prop:FontColor} <> 0
        ?List{prop:FontColor} = 0
    End
    If ?List{prop:Color}    <> 16777215
        ?List{prop:Color}    = 16777215
    End
    If ?List{prop:Color,2}  <> 16777215
        ?List{prop:Color,2}  = 16777215
    End
    If ?List{prop:Color,3}  <> 16750899
        ?List{prop:Color,3}  = 16750899
    End
    If ?ReportVersion{Prop:FontColor} < 0
        If ?ReportVersion{prop:FontColor} <> -1
            ?ReportVersion{prop:FontColor} = -1
        End
    End
    If ?ReportVersion{Prop:Color} < 0
        If ?ReportVersion{prop:Color} <> -1
            ?ReportVersion{prop:Color} = -1
        End
    End

RecolourWindow:Window       Routine
    If 0{prop:Color} > -1; Exit.
    If 0{prop:Color} <> -1
        0{prop:Color} = -1
    End
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:Tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Reporting       Routine
Data
local:CurrentAccount        String(30)
local:AccountJobs           Long()
local:ReportStartDate       Date()
local:ReportStartTime       Time()
local:LocalPath             String(255)

local:SummaryRow            Long()
local:SummaryColumn         String(2)
local:SummaryAccount        String(30)
local:TotalJobsForAccount   Long()

local:JobsBooked            Long()
local:JobsOpen              Long()

local:SummaryStartRow       Long()

local:BlankClipboard        CString(2)
local:Desktop               Cstring(255)
local:FinalFileName         CString(255)
Code

    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    !Set the folder for the excel file (DBH: 10-03-2004)
    excel:ProgramName = 'Turnaround Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occured finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(local:Desktop) & |
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occured finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(local:Desktop) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    excel:FileName = Clip(local:Desktop) & '\~' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12) & '.xls'
    local:FinalFileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12) & '.xls'


    if (exists(clip(excel:Filename)))
        remove(excel:Filename)
        if exists(excel:Filename)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('Cannot get access to the Excel Document. Make sure it is not open, then try again.','ServiceBase',|
                           Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            Exit
        end ! if error()
    end ! if (exists(clip(excel:Filename))


    if (exists(local:FinalFileName))
        remove(local:FinalFileName)
        if error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('Cannot get access to the Excel Document. Make sure it is not open, then try again.','ServiceBase',|
                           Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            Exit
        end ! if error()
    end ! if (exists(local:FinalFileName))

    If COMMAND('/DEBUG')
        Remove('c:\tat.log')
    End !If COMMAND('/DEBUG')


    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()
    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Creating Export Files...')

    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________

    tmp:ARCJobs = 0
    Clear(GrandARCTotalGroup)
    Clear(GrandRRCTotalGroup)
    !Go though all accounts (DBH: 10-03-2004)
    Set(tra:Account_Number_Key)
    Loop
        If Access:TRADEACC.Next()
            Break
        End !If Access:TRADEACC.Next()

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel = 1

        !Add the trade account criteria.
        !Only limited by tagged accounts - 4087 (DBH: 14-07-2004)

        If tmp:AllAccounts <> 1
            glo:Pointer = tra:Account_Number
            Get(glo:Queue,glo:Pointer)
            If Error()
                Cycle
            End !If Error()
        End !If tmp:AllAccounts <> 1

        If tra:BranchIdentification = ''
            Cycle
        End !If tra:BranchIdentification = ''

        If local:CurrentAccount = ''
            local:CurrentAccount = tra:Account_Number
        End !If local:CurrentAccount = ''

        !Is this the ARC, or RRC? (DBH: 10-03-2004)
        If tra:Account_Number = tmp:HeadAccountNumber
            tmp:ARCAccount = 1
            glo:ARCExportFile = Clip(local:LocalPath) & 'TAT-' & Clip(tmp:HeadAccountNumber) & '.csv'
            Remove(ARCExportFile)
            Create(ARCExportFile)
            Open(ARCExportFile)
            local:CurrentAccount = tra:Account_Number
        Else !If tra:Account_Number = tmp:HeadAccountNumber
            Clear(GrandRRCTotalGroup)
            tmp:ARCAccount = 0
            glo:ExportFile = Clip(local:LocalPath) & 'TAT-' & Clip(tra:Account_Number) & '.csv'
            Remove(ExportFile)
            Create(ExportFile)
            Open(ExportFile)
            local:CurrentACcount = tra:Account_Number
        End !If tra:Account_Number = tmp:HeadAccountNumber

        !Open And Booked Jobs (DBH: 15-03-2004)
        local.UpdateProgressWindow('Reading Account: ' & Clip(tra:Account_Number) & ' - ' & Clip(tra:Company_Name))
    !_____________________________________________________________________


        local:JobsOpen = 0
        local:JobsBooked = 0
        tmp:RRCJobs = 0

        Access:WEBJOB.ClearKey(wob:DateCompletedKey)
        wob:HeadAccountNumber = tra:Account_Number
        wob:DateCompleted     = 0
        Set(wob:DateCompletedKey,wob:DateCompletedKey)
        Loop
            If Access:WEBJOB.NEXT()
               Break
            End !If
            If Upper(wob:HeadAccountNumber) <> Upper(tra:Account_Number)      |
            Or wob:DateCompleted     <> 0      |
                Then Break.  ! End If

            ! Inserting (DBH 03/07/2008) # 10198 - Don't include cancelled jobs
            If wob:Current_Status = '799 JOB CANCELLED'
                Cycle
            End ! If wob:CurrentStatus = '799 JOB CANCELLED'
            ! End (DBH 03/07/2008) #10198

            Do GetNextRecord2
            Do CancelCheck
            If tmp:Cancel
                Break
            End !If tmp:Cancel
            If local.SubAccountExcludedFromReport(wob:SubAcountNumber)= True
                Cycle
            End !If local.SubAccountExcludedFromReport(wob:SubAcountNumber)= True

            local:JobsOpen += 1
        End !Loop

        If tmp:Cancel
            Break
        End !If tmp:Cancel

        local.UpdateProgressWindow('Open Jobs: ' & local:JobsOpen)

        Access:WEBJOB.ClearKey(wob:DateBookedKey)
        wob:HeadAccountNumber = tra:Account_Number
        wob:DateBooked        = tmp:StartDate
        Set(wob:DateBookedKey,wob:DateBookedKey)
        Loop
            If Access:WEBJOB.NEXT()
               Break
            End !If
            If Upper(wob:HeadAccountNumber) <> Upper(tra:Account_Number)      |
            Or wob:DateBooked        > tmp:EndDate       |
                Then Break.  ! End If
            local:JobsBooked += 1
            Do GetNextRecord2
            Do CancelCheck
            If tmp:Cancel
                Break
            End !If tmp:Cancel
            If local.SubAccountExcludedFromReport(wob:SubAcountNumber) = True
                Cycle
            End !If local.SubAccountExcludedFromReport(wob:SubAcountNumber) = True
        End !Loop

        tmpque:AccountNumber = tra:Account_Number
        Get(TempFileQueue,tmpque:AccountNumber)
        If Error()
            tmpque:AccountNumber = tra:Account_Number
            tmpque:CompanyName  = tra:Company_Name
            tmpque:Count    = 0
            tmpque:FileName = ''
            tmpque:JobsBooked = local:JobsBooked
            tmpque:JobsOpen     = local:JobsOpen
            Add(TempFileQueue)
        Else !If Error()
            tmpque:JobsBooked = local:JobsBooked
            tmpque:JobsOpen     = local:JobsOpen
            Put(TempFileQueue)
        End !If Error()

        If tmp:Cancel
            Break
        End !If tmp:Cancel

        local.UpdateProgressWindow('Booked Jobs: ' & local:JobsBooked)

    !_____________________________________________________________________

        !Go Through WEBJOBS in date range (DBH: 10-03-2004)
        Access:WEBJOB.Clearkey(wob:DateCompletedKey)
        wob:HeadAccountNumber    = tra:Account_Number
        wob:DateCompleted        = tmp:StartDate
        Set(wob:DateCompletedKey,wob:DateCompletedKey)
        Loop
            If Access:WEBJOB.Next()
                Break
            End !If Access:WEBJOB.Next()
            If Upper(wob:HeadAccountNumber) <> Upper(tra:Account_Number)
                Break
            End !If wob:HeadAccountNumber <> tra:Account_Number
            If wob:DateCompleted > tmp:EndDate
                Break
            End !If wob:DateCompleted > tmp:EndDate
            If local.SubAccountExcludedFromReport(wob:SubAcountNumber) = True
                Cycle
            End !If local.SubAccountExcludedFromReport(wob:SubAcountNumber) = True

            Do GetNextRecord2
            Do GetNextRecord2
            Do CancelCheck
            If tmp:Cancel
                Break
            End !If tmp:Cancel

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = wob:RefNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found

            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
                Cycle
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

            ! Inserting (DBH 27/09/2006) # 8184 - Do not include cancelled jobs (confirmed via MSN)
            If job:Cancelled = 'YES'
                Cycle
            End ! If job:Cancelled = 'YES'
            ! End (DBH 27/09/2006) #8184

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = wob:RefNumber
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found

            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
                Cycle
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
    !_____________________________________________________________________
            !Is there First Exchange Unit Attached? (DBH: 18-03-2004)
            tmp:ExchangeUnitAttached = False
            tmp:ExchangeAttachedDate = 0
            tmp:ExchangeAttachedTime = 0

            tmp:ARCExchangeSentToRRC = False

            If job:Exchange_Unit_Number <> 0
                !When was the exchange attached?  (DBH: 27-02-2004)
                Save_aud_ID = Access:AUDIT.SaveFile()
                Access:AUDIT.ClearKey(aud:TypeActionKey)
                aud:Ref_Number = job:Ref_Number
                aud:Type       = 'EXC'
                aud:Action     = 'EXCHANGE UNIT ATTACHED TO JOB'
                Set(aud:TypeActionKey,aud:TypeActionKey)
                Loop
                    If Access:AUDIT.NEXT()
                       Break
                    End !If
                    If aud:Ref_Number <> job:Ref_Number      |
                    Or aud:Type       <> 'EXC'      |
                    Or aud:Action     <> 'EXCHANGE UNIT ATTACHED TO JOB'      |
                        Then Break.  ! End If

                    Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                    aud2:AUDRecordNumber = aud:Record_Number
                    IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

                    END ! IF
                    !Make sure that this entry is for the exchange
                    !unit attached to the job  (DBH: 27-02-2004))
                    If Instring('UNIT NUMBER: ' & Clip(job:Exchange_Unit_Number),aud2:Notes,1,1)
                        tmp:ExchangeUnitAttached = True
                        tmp:ExchangeAttachedDate = aud:Date
                        tmp:ExchangeAttachedTime = aud:Time
                        Break
                    End !If Instring('UNIT NUMBER: ' & Clip(job:Exchange_Unit_Number),1,1)
                End !Loop
                Access:AUDIT.RestoreFile(Save_aud_ID)

                ! Inserting (DBH 28/07/2006) # 8022 - Was the ARC exchange unit sent to the RRC?
                If jobe:ExchangedATRRC = False
                    Access:AUDSTATS.ClearKey(aus:DateChangedKey)
                    aus:RefNumber   = job:Ref_Number
                    aus:Type        = 'EXC'
                    Set(aus:DateChangedKey,aus:DateChangedKey)
                    Loop
                        If Access:AUDSTATS.NEXT()
                           Break
                        End !If
                        If aus:RefNumber   <> job:Ref_Number      |
                        Or aus:Type        <> 'EXC'      |
                            Then Break.  ! End If
                        If Sub(aus:NewStatus,1,3) = '459'
                            tmp:ARCExchangeSentToRRC = True
                            Break
                        End ! If Sub(aus:NewStatus,1,3) = '459'
                    End !Loop
                End ! If jobe:ExchangedATRRC = False
                ! End (DBH 28/07/2006) #8022
            End !If job:Exchange_Unit_Number <> 0
    !_____________________________________________________________________
            !Is There A 2nd Exchange Unit Attached? (DBH: 18-03-2004)
            tmp:2ndExchangeUnitAttached = False
            tmp:2ndExchangeAttachedDate = 0
            tmp:2ndExchangeAttachedTime = 0

            If jobe:SecondExchangeNumber <> 0
                !When was the exchange attached?  (DBH: 27-02-2004)
                Save_aud_ID = Access:AUDIT.SaveFile()
                Access:AUDIT.ClearKey(aud:TypeActionKey)
                aud:Ref_Number = job:Ref_Number
                aud:Type       = '2NE'
                aud:Action     = 'SECOND EXCHANGE UNIT ATTACHED TO JOB'
                Set(aud:TypeActionKey,aud:TypeActionKey)
                Loop
                    If Access:AUDIT.NEXT()
                       Break
                    End !If
                    If aud:Ref_Number <> job:Ref_Number      |
                    Or aud:Type       <> '2NE'      |
                    Or aud:Action     <> 'SECOND EXCHANGE UNIT ATTACHED TO JOB'      |
                        Then Break.  ! End If
                    Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                    aud2:AUDRecordNumber = aud:Record_Number
                    IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

                    END ! IF
                    !Make sure that this entry is for the exchange
                    !unit attached to the job  (DBH: 27-02-2004))
                    If Instring('UNIT NUMBER: ' & Clip(jobe:SecondExchangeNumber),aud2:Notes,1,1)
                        tmp:2ndExchangeUnitAttached = True
                        tmp:2ndExchangeAttachedDate = aud:Date
                        tmp:2ndExchangeAttachedTime = aud:Time
                        Break
                    End !If Instring('UNIT NUMBER: ' & Clip(job:Exchange_Unit_Number),1,1)
                End !Loop
                Access:AUDIT.RestoreFile(Save_aud_ID)
            End !If job:Exchange_Unit_Number <> 0
    !_____________________________________________________________________
            tmp:LoanAttachedDate = 0
            tmp:LoanAttachedTime = 0
            tmp:LoanUnitAttached = False
            tmp:LoanTime = 0
            !When was the loan attached? (DBH: 17-03-2004)
            Save_aud_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.ClearKey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type       = 'LOA'
            aud:Action     = 'LOAN UNIT ATTACHED TO JOB'
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop
                If Access:AUDIT.NEXT()
                   Break
                End !If
                If aud:Ref_Number <> job:Ref_Number      |
                Or aud:Type       <> 'LOA'      |
                Or aud:Action     <> 'LOAN UNIT ATTACHED TO JOB'      |
                    Then Break.  ! End If
                tmp:LoanUnitAttached = True
                tmp:LoanAttachedDate = aud:Date
                tmp:LoanAttachedTime = aud:Time
                Break
            End !Loop
            Access:AUDIT.RestoreFile(Save_aud_ID)

            Do WorkOutTimes

    !_____________________________________________________________________
            If tmp:ARCAccount = 1
                local.WriteLine('ARC')
                tmp:ARCJobs += 1
            Else !If tmp:ARCAccount = 1
                ! Inserting (DBH 02/02/2007) # 8719 - Only count if job is completed
                If tmp:RRCJobFinished = True
                ! End (DBH 02/02/2007) #8719
                    local.WriteLine('RRC')
                    local:AccountJobs += 1
                    tmp:RRCJobs += 1
                End ! If tmp:RRCJobFinished = True
            End !If tmp:ARCAccount = 1

            !If this is an RRC job, check to see if it has gone to the ARC
            ! and if it has make an entry in the ARC list -  (DBH: 10-03-2004)
            If tmp:ARCAccount = 0
                ! Inserting (DBH 02/10/2006) # N/A - Only show ARC tab if the ARC is ticked in the criteria screen
                ARCTicked# = 0
                If tmp:AllAccounts = 1
                    ARCTicked# = 1
                Else ! If tmp:AllAccounts = 1
! Changing (DBH 23/04/2007) # 8939 - Use default head account
!                    glo:Pointer = 'AA20'
! to (DBH 23/04/2007) # 8939
                    glo:Pointer = tmp:HeadAccountNumber
! End (DBH 23/04/2007) #8939
                    Get(glo:Queue,glo:Pointer)
                    If ~Error()
                        ARCTicked# = 1
                    End ! If ~Error()
                End ! If tmp:AllAccounts = 1
                If ARCTicked# = 1
                    If SentToHub(job:Ref_Number)
                        local.WriteLine('ARC')
                        tmp:ARCJobs += 1
                    End !If SentToHub(job:Ref_Number)
                End ! If ARCTicked# = 1
                ! End (DBH 02/10/2006) #N/A
            End !If tmp:ARCAccount = 0

        End !Loop WEBJOB

        If tmp:ARCAccount
            local.UpdateProgressWindow('Completed Jobs: ' & tmp:ARCJobs)
 !           local.WriteLine('TOTALARC')
        Else !If tmp:ARCAccount
            local.UpdateProgressWindow('Completed Jobs: ' & tmp:RRCJobs)
  !          local.WriteLine('TOTALRRC')
        End !If tmp:ARCAccount
        local.UpdateProgressWindow('')

        If tmp:ARCAccount = 0
            tmpque:AccountNumber = tra:Account_Number
            Get(TempFileQueue,tmpque:AccountNumber)
            If Error()
                tmpque:AccountNumber = tra:Account_Number
                tmpque:CompanyName  = tra:Company_Name
                tmpque:Count        = tmp:RRCJobs
                tmpque:FileName     = glo:ExportFile
                Add(TempFileQueue)
            Else !If Error()
                tmpque:Count    = tmp:RRCJobs
                tmpque:FileName = glo:ExportFile
                Put(TempFileQueue)
            End !If Error()
        End !If tmp:ARCAccount = 0

        ! Inserting (DBH 02/10/2006) # N/A - Only show ARC tab if the ARC is ticked in the criteria screen
        ARCTicked# = 0
        If tmp:AllAccounts = 1
            ARCTicked# = 1
        Else ! If tmp:AllAccounts = 1
            glo:Pointer = tmp:HeadAccountNumber
            Get(glo:Queue,glo:Pointer)
            If ~Error()
                ARCTicked# = 1
            End ! If ~Error()
        End ! If tmp:AllAccounts = 1
        If ARCTicked# = 1
            tmpque:AccountNumber = tmp:HeadAccountNumber
            Get(TempFileQueue,tmpque:AccountNumber)
            If Error()
                tmpque:AccountNumber = tmp:HeadAccountNumber
                tmpque:CompanyName = tmp:ARCCompanyName
                tmpque:Count    = tmp:ARCJobs
                tmpque:FileName = glo:ARCExportFile
                Add(TempFileQueue)
            Else !If Error()
                tmpque:FileName = glo:ARCExportFile
                tmpque:Count    = tmp:ARCJobs
                Put(TempFileQueue)
            End !If Error()
        End ! If ARCTicked# = 1
        ! End (DBH 02/10/2006) #N/A

        Close(ExportFile)

        If tmp:Cancel
            Break
        End !If tmp:Cancel
    End !Loop TRADEACC
    Close(ARCExportFile)
    !_____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Hide} = True

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Excel Document..')
    !_____________________________________________________________________

    !BLANK SHEETS FOR EACH ACCOUNT
    !_____________________________________________________________________

!        ExcelSetup(0)
!        ExcelMakeWorkBook('Turnaround Time Report','','Turnaround Time Report')
!        ExcelSheetName('Summary')

        If E1.Init(0,0) = 0
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occured communicating with Excel.'&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If E1.Init(0,0,1) = 0

        e1.NewWorkbook()

        !Do not add all the tabs, if Summary - 4087 (DBH: 14-07-2004)
        If ~tmp:SummaryOnly
            sort(TempFileQueue,-tmpque:AccountNumber)
            Loop x# = 1 To Records(TempFileQueue)
                Get(TempFileQueue,x#)
                Do GetNextRecord2
                If tmpque:Count = 0
                    Cycle
                End !If tmpque:Count = 0

                e1.RenameWorksheet(SUB(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName),1,30)) ! #12068 Excel only supports 30 character tabs (Bryan: 05/04/2011)

                e1.WriteToCell('SB Job No','A11')
                e1.WriteToCell('BranchSpecific','B11')
                e1.WriteToCell('Head Account','C11')
                e1.WriteToCell('Sub Account','D11')
                e1.WriteToCell('Manufacturer','E11')
                e1.WriteToCell('Model Number','F11')
                e1.WriteToCell('Warranty','G11')
                e1.WriteToCell('Date','H11')
                e1.WriteToCell('Time','I11')
                e1.WriteToCell('Date','J11')
                e1.WriteToCell('Time','K11')
                e1.WriteToCell('Days','L11')
                e1.WriteToCell('HH:MM','M11')
                e1.WriteToCell('Days','N11')
                e1.WriteToCell('HH:MM','O11')
                e1.WriteToCell('Days','P11')
                e1.WriteToCell('HH:MM','Q11')
                e1.WriteToCell('Days','R11')
                e1.WriteToCell('HH:MM','S11')
                e1.WriteToCell('Days','T11')
                e1.WriteToCell('HH:MM','U11')
                e1.WriteToCell('Days','V11')
                e1.WriteToCell('HH:MM','W11')
                e1.WriteToCell('Days','X11')
                e1.WriteToCell('HH:MM','Y11')
                e1.WriteToCell('Loan Issued','Z11')
                e1.WriteToCell('Repair Centre Perspective','AA11')
                e1.WriteToCell('Real Time Repair Centre Perspective','AB11')
                e1.WriteToCell('Customer Perspective','AC11')
                e1.SelectCells('B12')
                e1.FreezePanes(1)
                e1.InsertWorksheet()
!                ExcelMakeSheet()
!                ExcelSheetName(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName))
!                ExcelSelectRange('A11')
!                ExcelCell('SB Job No',1)
!                ExcelCell('BranchSpecific',1)
!                ExcelCell('Head Account',1)
!                ExcelCell('Sub Account',1)
!                ExcelCell('Manufacturer',1)
!                ExcelCell('Model Number',1)
!                ExcelCell('Warranty',1)
!                ExcelCell('Date',1)
!                ExcelCell('Time',1)
!                ExcelCell('Date',1)
!                ExcelCell('Time',1)
!                ExcelCell('Days',1)
!                ExcelCell('HH:MM',1)
!                ExcelCell('Days',1)
!                ExcelCell('HH:MM',1)
!                ExcelCell('Days',1)
!                ExcelCell('HH:MM',1)
!                ExcelCell('Days',1)
!                ExcelCell('HH:MM',1)
!                ExcelCell('Days',1)
!                ExcelCell('HH:MM',1)
!                ExcelCell('Days',1)
!                ExcelCell('HH:MM',1)
!                ExcelCell('Days',1)
!                ExcelCell('HH:MM',1)
!                ExcelCell('Loan Issued',1)
!                ExcelCell('Repair Centre Perspective',1)
!                ExcelCell('Real Time Repair Centre Perspective',1)
!                ExcelCell('Customer Perspective',1)
!                ExcelAutoFilter('A11:AC11')
!                ExcelFreeze('B12')
            End !Loop x# = 1 To Records(TempFileQueue)
        End !If ~tmp:SummaryOnly
        e1.RenameWorksheet('Summary')
        e1.saveas(excel:FileName)
        e1.closeWorkbook()

!        ExcelSaveWorkBook(excel:FileName)
!        ExcelClose()

        local.UpdateProgressWindow('Creating Excel Sheets...')
        local.UpdateProgressWindow('')


        If ~tmp:SummaryOnly
            Loop x# = 1 To Records(TempFileQueue)
                Get(TempFileQueue,x#)
                Do GetNextRecord2
                Yield()
                If tmpque:Count = 0
                    Cycle
                End !If tmpque:Count = 0

                local.UpdateProgressWindow('Writing Account: ' & Clip(tmpque:AccountNumber) & ' - ' & Clip(tmpque:CompanyName))

                E1.OpenWorkBook(Clip(tmpque:FileName))
                E1.Copy('A1','AC' & tmpque:Count)

                tmp:Clipboard = ClipBoard()

                E1.Copy('A1')

                E1.CloseWorkBook(3)

                E1.OpenWorkBook(excel:FileName)
                E1.SelectWorkSheet(SUB(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName),1,30)) ! #12068 Excel only supports 30 character tabs (Bryan: 05/04/2011)

                E1.SelectCells('A12')
                SetClipBoard(tmp:ClipBoard)
                E1.Paste()
                E1.SelectCells('B12')
                E1.Copy('A1')
                E1.CloseWorkBook(3)
                local.UpdateProgressWindow('Done.')
                local.UpdateProgressWindow('')
            End !Loop x# = 1 To Records(TempFileQueue)
        End !If tmp:Summary
        E1.OpenWorkBook(excel:FileName)
    !_____________________________________________________________________

    !SUMMARY
    !_____________________________________________________________________

        !Fill In Summary Sheet  (DBH: 03-03-2004)
        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Summary..')
        local.UpdateProgressWindow('')

        local:SummaryRow = 11
        local:SummaryAccount = ''
        E1.SelectWorkSheet('Summary')

        Do DrawSummaryTitle

        Local.SummaryTitles('Repair Centre Perspective',8)
        local:SummaryStartRow = 11

        Sort(RepairCentreQueue,rep1:AccountNumber)
        !Repair Center Perspective  (DBH: 03-03-2004)
        Loop x# = 1 To Records(RepairCentreQueue)
            Get(RepairCentreQueue,x#)

            Do GetNextRecord2

            If local:SummaryAccount = ''
                !This is the first time in the queue. Save the account]  (DBH: 03-03-2004)
                local:SummaryAccount= rep1:AccountNumber

                Local.WriteSummaryLoanLine(local:SummaryAccount,local:SummaryRow,0)
            End !If Account" = ''
            If local:SummaryAccount <> rep1:AccountNumber
                !Write Loan Information (DBH: 18-03-2004)
                Local.WriteSummaryLoanLine(local:SummaryAccount,local:SummaryRow,0)
                !Draw box under line for totals (DBH: 03-03-2004)
                local.DrawSummaryLineTotalBar(local:SummaryRow + 1)
                !Write total formulae  (DBH: 03-03-2004)
                local.WriteSummaryLineTotals(local:SummaryRow)
                !Write Total Completed Jobs  (DBH: 03-03-2004)
                tmpque:AccountNumber    = local:SummaryAccount
                Get(TempFileQueue,tmpque:AccountNumber)
                E1.WriteToCell(tmpque:Count,'B' & local:SummaryRow)
                E1.WriteToCell(tmpque:JobsBooked,'C' & local:SummaryRow)
                E1.WriteToCell(tmpque:JobsOpen,'D' & local:SummaryRow)

                !Save new account and move down 2 lines  (DBH: 03-03-2004)
                local:SummaryAccount = rep1:AccountNumber
                local:SummaryRow += 2
            End !If Account" <> rep1:Account_Number
            E1.WriteToCell(rep1:CompanyName,'A' & local:SummaryRow)
            E1.WriteToCell(rep1:Count,Clip(rep1:Type) & Clip(local:SummaryRow))
            local:TotalJobsForAccount += rep1:Count


        End !Loop x# = 1 To Records(RepairCentreQueue)
        !Write Loan Information (DBH: 18-03-2004)
        Local.WriteSummaryLoanLine(local:SummaryAccount,local:SummaryRow,0)
        !Draw box under line  (DBH: 03-03-2004)
        local.DrawSummaryLineTotalBar(local:SummaryRow + 1)
        !Insert Total formulas  (DBH: 03-03-2004)
        local.WriteSummaryLineTotals(local:SummaryRow)
        !Write Total Completed Jobs  (DBH: 03-03-2004)
        tmpque:AccountNumber    = rep1:AccountNumber
        Get(TempFileQueue,tmpque:AccountNumber)
        E1.WriteToCell(tmpque:Count,'B' & local:SummaryRow)
        E1.WriteToCell(tmpque:JobsBooked,'C' & local:SummaryRow)
        E1.WriteToCell(tmpque:JobsOpen,'D' & local:SummaryRow)

        !Draw Section Totals and Sub Totals
        Local.WriteSummarySectionTotal(local:SummaryRow,local:SummaryStartRow)
    !_____________________________________________________________________


        !Move down to start next section  (DBH: 03-03-2004)
        local:SummaryRow += 7

        !Real Time Repair Center Perspective  (DBH: 03-03-2004)
        Local.SummaryTitles('Real Time Repair Centre Perspective',local:SummaryRow)

        local:SummaryRow += 3

        local:SummaryStartRow = local:SummaryRow

        Sort(RepaircentreRealQueue,rep2:AccountNumber)
        local:SummaryAccount = ''
        Loop x# = 1 To Records(RepairCentreRealQueue)
            Get(RepairCentreRealQueue,x#)

            Do GetNextRecord2

            If local:SummaryAccount = ''
                !This is the first time in the queue. Save the account]  (DBH: 03-03-2004)
                local:SummaryAccount= rep2:AccountNumber
                !Write Loan Information (DBH: 18-03-2004)
                Local.WriteSummaryLoanLine(local:SummaryAccount,local:SummaryRow,1)
            End !If Account" = ''
            If local:SummaryAccount <> rep2:AccountNumber
                !Write Loan Information (DBH: 18-03-2004)
                Local.WriteSummaryLoanLine(local:SummaryAccount,local:SummaryRow,1)
                !Draw box under line for totals (DBH: 03-03-2004)
                local.DrawSummaryLineTotalBar(local:SummaryRow + 1)
                !Write total formulae  (DBH: 03-03-2004)
                local.WriteSummaryLineTotals(local:SummaryRow)
                !Write Total Completed Jobs  (DBH: 03-03-2004)
                tmpque:AccountNumber    = local:SummaryAccount
                Get(TempFileQueue,tmpque:AccountNumber)
                E1.WriteToCell(tmpque:Count,'B' & local:SummaryRow)
                E1.WriteToCell(tmpque:JobsBooked,'C' & local:SummaryRow)
                E1.WriteToCell(tmpque:JobsOpen,'D' & local:SummaryRow)

                !Save new account and move down 2 lines  (DBH: 03-03-2004)
                local:SummaryAccount = rep2:AccountNumber
                local:SummaryRow += 2
            End !If Account" <> rep1:Account_Number
            E1.WriteToCell(rep2:CompanyName,'A' & local:SummaryRow)
            E1.WriteToCell(rep2:Count,Clip(rep2:Type) & Clip(local:SummaryRow))
            local:TotalJobsForAccount += rep2:Count

        End !Loop x# = 1 To Records(RepairCentreQueue)
        !Write Loan Information (DBH: 18-03-2004)
        Local.WriteSummaryLoanLine(local:SummaryAccount,local:SummaryRow,1)
        !Draw box under line  (DBH: 03-03-2004)
        local.DrawSummaryLineTotalBar(local:SummaryRow + 1)
        !Insert Total formulas  (DBH: 03-03-2004)
        local.WriteSummaryLineTotals(local:SummaryRow)
        !Write Total Completed Jobs  (DBH: 03-03-2004)
        tmpque:AccountNumber    = rep2:AccountNumber
        Get(TempFileQueue,tmpque:AccountNumber)
        E1.WriteToCell(tmpque:Count,'B' & local:SummaryRow)
        E1.WriteToCell(tmpque:JobsBooked,'C' & local:SummaryRow)
        E1.WriteToCell(tmpque:JobsOpen,'D' & local:SummaryRow)

        !Draw Section Totals and Sub Totals
        Local.WriteSummarySectionTotal(local:SummaryRow,local:SummaryStartRow)

    !_____________________________________________________________________



        !Move down to next section (DBH: 03-03-2004)
        local:SummaryRow += 7

        !Customer Perspective (DBH: 03-03-2004)
        Local.SummaryTitles('Customer Perspective',local:SummaryRow)

        local:SummaryRow += 3
        local:SummaryStartRow = local:SummaryRow

        local:SummaryAccount = ''
        E1.SelectWorkSheet('Summary')
        Sort(CustomerPerspectiveQueue,cus:AccountNumber)
        Loop x# = 1 To Records(CustomerPerspectiveQueue)
            Get(CustomerPerspectiveQueue,x#)

            Do GetNextRecord2

            If local:SummaryAccount = ''
                !This is the first time in the queue. Save the account]  (DBH: 03-03-2004)
                local:SummaryAccount= cus:AccountNumber
                !Write Loan Information (DBH: 18-03-2004)
                Local.WriteSummaryLoanLine(local:SummaryAccount,local:SummaryRow,2)
            End !If Account" = ''
            If local:SummaryAccount <> cus:AccountNumber
                !Write Loan Information (DBH: 18-03-2004)
                Local.WriteSummaryLoanLine(local:SummaryAccount,local:SummaryRow,2)
                !Draw box under line for totals (DBH: 03-03-2004)
                local.DrawSummaryLineTotalBar(local:SummaryRow + 1)
                !Write total formulae  (DBH: 03-03-2004)
                local.WriteSummaryLineTotals(local:SummaryRow)
                !Write Total Completed Jobs  (DBH: 03-03-2004)
                tmpque:AccountNumber    = local:SummaryAccount
                Get(TempFileQueue,tmpque:AccountNumber)
                E1.WriteToCell(tmpque:Count,'B' & local:SummaryRow)
                E1.WriteToCell(tmpque:JobsBooked,'C' & local:SummaryRow)
                E1.WriteToCell(tmpque:JobsOpen,'D' & local:SummaryRow)

                !Save new account and move down 2 lines  (DBH: 03-03-2004)
                local:SummaryAccount = cus:AccountNumber
                local:SummaryRow += 2
            End !If Account" <> rep1:Account_Number
            E1.WriteToCell(cus:CompanyName,'A' & local:SummaryRow)
            E1.WriteToCell(cus:Count,Clip(cus:Type) & Clip(local:SummaryRow))
            local:TotalJobsForAccount += cus:Count

        End !Loop x# = 1 To Records(RepairCentreQueue)
        !Write Loan Information (DBH: 18-03-2004)
        Local.WriteSummaryLoanLine(local:SummaryAccount,local:SummaryRow,2)
        !Draw box under line  (DBH: 03-03-2004)
        local.DrawSummaryLineTotalBar(local:SummaryRow + 1)
        !Insert Total formulas  (DBH: 03-03-2004)
        local.WriteSummaryLineTotals(local:SummaryRow)
        !Write Total Completed Jobs  (DBH: 03-03-2004)
        tmpque:AccountNumber    = cus:AccountNumber
        Get(TempFileQueue,tmpque:AccountNumber)
        E1.WriteToCell(tmpque:Count,'B' & local:SummaryRow)
        E1.WriteToCell(tmpque:JobsBooked,'C' & local:SummaryRow)
        E1.WriteToCell(tmpque:JobsOpen,'D' & local:SummaryRow)

        !Draw Section Totals and Sub Totals
        Local.WriteSummarySectionTotal(local:SummaryRow,local:SummaryStartRow)


        !Set Format Of Number Cells
        E1.SetCellFontStyle('Bold','A' & local:SummaryRow + 2,'AK' & local:SummaryRow + 4)

        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'I11','I' & local:SummaryRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'M11','M' & local:SummaryRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'Q11','Q' & local:SummaryRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'U11','U' & local:SummaryRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'Y11','Y' & local:SummaryRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'AC11','AC' & local:SummaryRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'AG11','AG' & local:SummaryRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'AK11','AK' & local:SummaryRow + 4)

        E1.SetCellFontName('Tahoma','A1','AK' & local:SummaryRow + 6)
        E1.SetCellFontSize(8,'A3','AK' & local:SummaryRow + 6)
        E1.SetCellAlignment(oix:WrapText,1,'B8','D' & local:SummaryRow + 6)

    !_____________________________________________________________________

    !FORMATTING
    !_____________________________________________________________________

        local.UpdatePRogressWindow('Finishing Off Formatting..')

        !Do not add to tabs if Summary - 4087 (DBH: 14-07-2004)
        If ~tmp:SummaryOnly
            Loop x# = 1 To Records(TempFileQueue)
                Get(TempFileQueue,x#)
                If tmpque:Count = 0
                    Cycle
                End !If tmpque:Count = 0
                Do GetNextRecord2
                E1.SelectWorkSheet(SUB(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName),1,30)) ! #12068 Excel only supports 30 character tabs (Bryan: 05/04/2011)
                 If tmpque:AccountNumber = tmp:HeadAccountNumber
                    tmp:ARCAccount = 1
                Else !If tmpque:AccountNumber = tmp:HeadAccountNumber
                    tmp:ARCAccount = 0
                End !If tmpque:AccountNumber = tmp:HeadAccountNumber
                Do DrawDetailTitle
                Do DrawDetailColumns

                E1.SetCellFontSize(12,'A1')
                E1.SetCellFontStyle('Bold','A1')
                E1.WriteToCell('Turnaround Report (' & Clip(tmpque:CompanyName) & ')'   ,'A1')

                E1.WriteToCell('=SUBTOTAL(2, A12:A' & tmpque:Count + 11,'E4')
                E1.WriteToCell(tmpque:Count,'D4')

                local.DrawBox('A'& tmpque:Count + 12,'AC' & tmpque:Count + 12,'A' & tmpque:Count + 12,'AC' & tmpque:Count + 12,color:Silver)
                local.DrawBox('A'& tmpque:Count + 13,'AC' & tmpque:Count + 13,'A' & tmpque:Count + 13,'AC' & tmpque:Count + 13,color:Silver)

                E1.WriteToCell('TOTAL','A' & tmpque:Count + 12)
                E1.WriteToCell('AVERAGE','A' & tmpque:Count + 13)
                E1.WriteToCell('TOTAL','K4')
                E1.WriteToCell('AVERAGE','K5')

                !Total Repair Time
                E1.WriteToCell('=SUBTOTAL(9,L12:L' & tmpque:Count + 11 & ') + INT(SUBTOTAL(9,M12:M' & tmpque:Count+11 & '))','L' & tmpque:Count + 12)
                E1.WriteToCell('=SUBTOTAL(9,M12:M' & tmpque:Count+11 & ')','M' & tmpque:Count + 12)
                E1.WriteToCell('=L' & tmpque:Count + 12,'L4')
                E1.WriteToCell('=M' & tmpque:Count + 12,'M4')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'L' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'L4')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'M' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'M4')

                E1.WriteToCell('=INT(L' & tmpque:Count + 12 & '/E4)','=L' & tmpque:Count + 13)
                E1.WriteToCell('=(M' & tmpque:Count + 12 & '/E4)','=M' & tmpque:Count + 13)
                E1.WriteToCell('=L' & tmpque:Count + 13,'L5')
                E1.WriteToCell('=M' & tmpque:Count + 13,'M5')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'L' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'L5')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'M' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'M5')

                !In Control
                E1.WriteToCell('=SUBTOTAL(9,N12:N' & tmpque:Count + 11 & ') + INT(SUBTOTAL(9,O12:O' & tmpque:Count+11 & '))','N' & tmpque:Count + 12)
                E1.WriteToCell('=SUBTOTAL(9,O12:O' & tmpque:Count+11 & ')','O' & tmpque:Count + 12)
                E1.WriteToCell('=N' & tmpque:Count + 12,'N4')
                E1.WriteToCell('=O' & tmpque:Count + 12,'O4')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'N' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'N4')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'O' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'O4')

                E1.WriteToCell('=INT(N' & tmpque:Count + 12 & '/E4)','=N' & tmpque:Count + 13)
                E1.WriteToCell('=(O' & tmpque:Count + 12 & '/E4)','=O' & tmpque:Count + 13)
                E1.WriteToCell('=N' & tmpque:Count + 13,'N5')
                E1.WriteToCell('=O' & tmpque:Count + 13,'O5')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'N' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'N5')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'O' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'O5')


                !Ready To Despatch
                E1.WriteToCell('=SUBTOTAL(9,P12:P' & tmpque:Count + 11 & ') + INT(SUBTOTAL(9,Q12:Q' & tmpque:Count+11 & '))','P' & tmpque:Count + 12)
                E1.WriteToCell('=SUBTOTAL(9,Q12:Q' & tmpque:Count+11 & ')','Q' & tmpque:Count + 12)
                E1.WriteToCell('=P' & tmpque:Count + 12,'P4')
                E1.WriteToCell('=Q' & tmpque:Count + 12,'Q4')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'P' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'P4')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'Q' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'Q4')

                E1.WriteToCell('=INT(P' & tmpque:Count + 12 & '/E4)','=P' & tmpque:Count + 13)
                E1.WriteToCell('=(Q' & tmpque:Count + 12 & '/E4)','=Q' & tmpque:Count + 13)
                E1.WriteToCell('=P' & tmpque:Count + 13,'P5')
                E1.WriteToCell('=Q' & tmpque:Count + 13,'Q5')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'P' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'P5')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'Q' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'Q5')


                !Customer Collection
                E1.WriteToCell('=SUBTOTAL(9,R12:R' & tmpque:Count + 11 & ') + INT(SUBTOTAL(9,S12:S' & tmpque:Count+11 & '))','R' & tmpque:Count + 12)
                E1.WriteToCell('=SUBTOTAL(9,S12:S' & tmpque:Count+11 & ')','S' & tmpque:Count + 12)
                E1.WriteToCell('=R' & tmpque:Count + 12,'R4')
                E1.WriteToCell('=S' & tmpque:Count + 12,'S4')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'R' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'R4')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'S' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'S4')


                E1.WriteToCell('=INT(R' & tmpque:Count + 12 & '/E4)','=R' & tmpque:Count + 13)
                E1.WriteToCell('=(S' & tmpque:Count + 12 & '/E4)','=S' & tmpque:Count + 13)
                E1.WriteToCell('=R' & tmpque:Count + 13,'R5')
                E1.WriteToCell('=S' & tmpque:Count + 13,'S5')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'R' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'R5')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'S' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'S5')

                !Wait Time
                E1.WriteToCell('=SUBTOTAL(9,T12:T' & tmpque:Count + 11 & ') + INT(SUBTOTAL(9,U12:U' & tmpque:Count+11 & '))','T' & tmpque:Count + 12)
                E1.WriteToCell('=SUBTOTAL(9,U12:U' & tmpque:Count+11 & ')','U' & tmpque:Count + 12)
                E1.WriteToCell('=T' & tmpque:Count + 12,'T4')
                E1.WriteToCell('=U' & tmpque:Count + 12,'U4')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'T' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'T4')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'U' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'U4')

                E1.WriteToCell('=INT(T' & tmpque:Count + 12 & '/E4)','=T' & tmpque:Count + 13)
                E1.WriteToCell('=(U' & tmpque:Count + 12 & '/E4)','=U' & tmpque:Count + 13)
                E1.WriteToCell('=T' & tmpque:Count + 13,'T5')
                E1.WriteToCell('=U' & tmpque:Count + 13,'U5')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'T' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'T5')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'U' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'U5')

                !Courier Time
                E1.WriteToCell('=SUBTOTAL(9,V12:V' & tmpque:Count + 11 & ') + INT(SUBTOTAL(9,W12:W' & tmpque:Count+11 & '))','V' & tmpque:Count + 12)
                E1.WriteToCell('=SUBTOTAL(9,W12:W' & tmpque:Count+11 & ')','W' & tmpque:Count + 12)
                E1.WriteToCell('=V' & tmpque:Count + 12,'V4')
                E1.WriteToCell('=W' & tmpque:Count + 12,'W4')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'V' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'V4')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'W' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'W4')

                E1.WriteToCell('=INT(V' & tmpque:Count + 12 & '/E4)','=V' & tmpque:Count + 13)
                E1.WriteToCell('=(W' & tmpque:Count + 12 & '/E4)','=W' & tmpque:Count + 13)
                E1.WriteToCell('=V' & tmpque:Count + 13,'V5')
                E1.WriteToCell('=W' & tmpque:Count + 13,'W5')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'V' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'V5')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'W' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'W5')

                !3rd Party
                E1.WriteToCell('=SUBTOTAL(9,X12:X' & tmpque:Count + 11 & ') + INT(SUBTOTAL(9,Y12:Y' & tmpque:Count+11 & '))','X' & tmpque:Count + 12)
                E1.WriteToCell('=SUBTOTAL(9,Y12:Y' & tmpque:Count+11 & ')','Y' & tmpque:Count + 12)
                E1.WriteToCell('=X' & tmpque:Count + 12,'X4')
                E1.WriteToCell('=Y' & tmpque:Count + 12,'Y4')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'X' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'X4')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'Y' & tmpque:Count + 12)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'Y4')

                E1.WriteToCell('=INT(X' & tmpque:Count + 12 & '/E4)','=X' & tmpque:Count + 13)
                E1.WriteToCell('=(Y' & tmpque:Count + 12 & '/E4)','=Y' & tmpque:Count + 13)
                E1.WriteToCell('=X' & tmpque:Count + 13,'X5')
                E1.WriteToCell('=Y' & tmpque:Count + 13,'Y5')
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'X' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,'X5')
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'Y' & tmpque:Count + 13)
                E1.SetCellNumberFormat(oix:NumberFormatTime,,,1,'Y5')

                E1.SetCellFontStyle('Bold', 'A' & tmpque:Count + 12, 'AC' & tmpque:Count + 13)
                E1.SetCellFontStyle('Bold','K4','Z5')
                E1.SetCellFontName('Tahoma','A1','AC' & tmpque:Count + 13)
                E1.SetCellFontSize(8,'A2','AC' & tmpque:Count + 13)
                E1.SetCellAlignment(oix:WrapText,1,'AA11','AC11')
                E1.SetColumnWidth('A', '', '20')
                E1.SetColumnWidth('B', 'Z', '')
                E1.SetColumnWidth('AA','AC','14')

            End !Loop x# = 1 To Records(TempFileQueue)
        End !If ~tmp:SummaryOnly
        E1.Copy('A1')
        E1.SelectWorkSheet('Summary')

        e1.SaveAs(local:FinalFileName,oix:xlWorkbookNormal)
        E1.CloseWorkBook(3)
        E1.Kill()
        remove(clip(excel:FileName))

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))
    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)

    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    !?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    If Automatic <> True
        ?Button:OpenReportFolder{Prop:Hide} = 0
        Accept
            Case Field()
                Of ?Finish
                    Case Event()
                        Of Event:Accepted
                            Break

                    End !Case Event()
                Of ?Button:OpenReportFolder
                    Case Event()
                    Of Event:Accepted
                        RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                    End ! Case Event()
            End !Case Field()
        End !Accept
    Else
        If Command('/SCHEDULE')
            If Access:REPSCHLG.PrimeRecord() = Level:Benign
                rlg:REPSCHEDRecordNumber = rpd:RecordNumber
                rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                  '<13,10>Report Finished'
                If Access:REPSCHLG.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                    Access:REPSCHLG.CancelAutoInc()
                End ! If Access:REPSCHLG.TryInsert() = Level:Benign
            End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
        End ! If Command('/SCHEDULE')
    End !If Automatic <> True

    Close(ProgressWindow)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)
WorkOutTimes        Routine
Data
local:CountExchange     Byte(0)
local:Count2ndExchange  Byte(0)
local:LastDespatchDate      Date()
local:LastDespatchTime      Time()

local:TimeToComplete        Long()
local:TimeToDespatch        Long()

! Changing (DBH 01/12/2005) #6804 - Record the ARC and RRC ends seperately
! local:EndOfRepairTimeDate   Date()
! local:EndOfRepairTimeTime   Time()
! local:JobFinished           Byte(0)
! to (DBH 01/12/2005) #6804
local:ARCEndOfRepairTimeDate   Date()
local:ARCEndOfRepairTimeTime   Time()
local:RRCEndOfRepairTimeDate   Date()
local:RRCEndOfRepairTimeTime   Time()
! End (DBH 01/12/2005) #6804
! Inserting (DBH 17/06/2006) #7133 - Record the ARC/RRC start time seperately
local:ARCStartOfRepairTimeDate  Date()
local:ARCStartOfRepairTimeTime  Time()
local:ARCJobStarted             Byte(0)
local:RRCStartOfRepairTimeDate  Date()
local:RRCStartOfRepairTimeTime  Time()
local:RRCJobStarted             Byte(0)
! End (DBH 17/06/2006) #7133

local:JobDespatched         Byte(0)
local:StartDate             Date()
local:StartTime             Time()

local:RRCStartTime     Time()
local:RRCEndTime       Time()
local:RRCSaturdayStartTime    Time()
local:RRCSaturdayEndTime      Time()
local:ARCStartTime     Time()
local:ARCEndTime       Time()

local:ARCExchangeDespatchedToRRC        Byte(0)

Code

    local:RRCStartTime = Deformat('08:30',@t1)
    local:RRCEndTime   = Deformat('17:00',@t1)
    local:RRCSaturdayStartTime = Deformat('09:00',@t1)
    local:RRCSaturdayEndTime = Deformat('13:00',@t1)
    local:ARCStartTime = Deformat('08:00',@t1)
    local:ARCEndTime = Deformat('17:00',@t1)

    tmp:SentToARCDate = 0
    tmp:SentToARCTime = 0
    tmp:SentToRRCDate = 0
    tmp:SentToRRCTime = 0
    tmp:LastCompletedDate = job:Date_Completed
    tmp:LastCompletedTime = job:Time_Completed

    local:LastDespatchDate  = 0
    local:LastDespatchTime  = 0
    local:StartDate = job:Date_Booked
    local:StartTime = job:Time_Booked

    tmp:RRCRealTotalTime = 0
    tmp:ARCRealTotalTime = 0

    tmp:ARCJobFinished = 0
    tmp:RRCJobFinished = 0

    tmp:WhoDespatchedTheJob = ''

    Clear(TotalGroup)
    If Command('/DEBUG')
        LinePrint('','c:\tat.log')
        LinePrint('===========','c:\tat.log')
        LinePrint('Job Number: ' & job:Ref_Number,'c:\tat.log')
        LinePrint('','c:\tat.log')
        LinePrint('OBF Validated: ' & jobe:OBFvalidated)
    End ! If Command('/DEBUG')

    ! Inserting (DBH 16/06/2006) #7133 - Set the initial values incase there ISN'T a 000 NEW JOB BOOKING
    If jobe:WebJob
        !Job was booked at the RRC (DBH: 24/09/2006)

        tmp:RRCInControlStartDate = job:Date_Booked
        tmp:RRCInControlStartTime = job:Time_Booked

        local.GetStartDate(tmp:RRCInControlStartDate,tmp:RRCInControlStartTime,1)

        tmp:RRCInControl             = True
        local:RRCStartOfRepairTimeDate = job:Date_Booked
        local:RRCStartOfRepairTimeTime = job:Time_Booked

        local.GetStartDate(local:RRCStartOfRepairTimeDate,local:RRCStartOfRepairTimeTime,1)
        local:RRCJobStarted = True

    Else ! If jobe:WebJob
        ! Job was booked at the ARC (DBH: 24/09/2006)
        tmp:ARCInControlStartDate    = job:Date_Booked
        tmp:ARCInControlStartTime    = job:Time_Booked

        local.GetStartDate(tmp:ARCInControlStartDate,tmp:ARCInControlStartTime,0)

        tmp:ARCInControl = True
        local:ARCStartOfRepairTimeDate = job:Date_Booked
        local:ARCStartOfRepairTImeTime = job:Time_Booked

        local.GetStartDate(local:ARCStartOfRepairTimeDate,local:ARCStartOfRepairTimeTime,1)

        local:ARCJobStarted = True
    End ! If jobe:WebJob
    ! End (DBH 16/06/2006) #7133

!TB13205 - J - 30/12/13 though it took me days to spot this!
!TB13205 - This is the wrong key - because PUP records come late they are out of order by RecordNumber (part of RefDateRecordKey)
!    Access:AUDSTATS.ClearKey(aus:RefDateRecordKey)
!    aus:RefNumber    = job:Ref_Number
!    aus:DateChanged  = 0
!    Set(aus:RefDateRecordKey,aus:RefDateRecordKey)
!TB13205 - change the key to run by refnumber, date and time and it all comes in the right order
    Access:AUDSTATS.clearkey(aus:StatusDateKey)       !only elements are RefNumber, date ascending, time ascending
    aus:RefNumber   = job:Ref_Number
    aus:DateChanged = 0
    aus:TimeChanged = 0
    Set(aus:StatusDateKey,aus:StatusDateKey)
!TB13205 - end of changes!
    Loop
        If Access:AUDSTATS.Next()
            Break
        End ! If Access:AUDSTATS.Next()
        If aus:RefNumber <> job:Ref_Number
            Break
        End ! If aus:RefNumber <> job:Ref_Number

        !What this entry done by an ARC, RRC engineer? -  (DBH: 27-02-2004)
        tmp:ARCUser = local.ARCUser(aus:UserCode)

        If Command('/DEBUG')
            LinePrint('Status Change: ' & Clip(aus:NewStatus) & '. User: ' & Clip(aus:UserCode) & ' (' & tmp:ARCUser & ') - ' & |
                            Format(aus:DateChanged,@d6) & ' ' & Format(aus:TimeChanged,@t1),'c:\tat.log')
        End ! If Command('/DEBUG')


    !_____________________________________________________________________
    !Job Status
    !_____________________________________________________________________

        If aus:Type = 'JOB'
            Case Sub(aus:NewStatus,1,3)
                Of '000' !New Job Booking
                    ! Inserting (DBH 16/06/2006) #7133 - Set the initial values incase there ISN'T a 000 NEW JOB BOOKING
                    If jobe:WebJob
                        ! Job was booked at the RRC (DBH: 24/09/2006)
                        tmp:RRCInControlStartDate = aus:DateChanged
                        tmp:RRCInControlStartTime = aus:TimeChanged

                        local.GetStartDate(tmp:RRCInControlStartDate,tmp:RRCInControlStartTime,1)

                        tmp:RRCInControl             = True
                        local:RRCStartOfRepairTimeDate = aus:DateChanged
                        local:RRCStartOfRepairTimeTime = aus:TimeChanged
                        local.GetStartDate(local:RRCStartOfRepairTimeDate,local:RRCStartOfRepairTimeTime,1)
                        local:RRCJobStarted = True
                    Else ! If jobe:WebJob
                        ! Job was booked at the ARC (DBH: 24/09/2006)
                        tmp:ARCInControlStartDate    = aus:DateChanged
                        tmp:ARCInControlStartTime    = aus:TimeChanged

                        local.GetStartDate(tmp:ARCInControlStartDate,tmp:ARCInControlStartTime,0)

                        tmp:ARCInControl = True
                        local:ARCStartOfRepairTimeDate = job:Date_Booked
                        local:ARCStartOfRepairTImeTime = job:Time_Booked
                        local.GetStartDate(local:ARCStartOfRepairTimeDate,local:ARCStartOfRepairTimeTime,1)
                        local:ARCJobStarted = True
                    End ! If jobe:WebJob
                    ! End (DBH 16/06/2006) #7133


    !_____________________________________________________________________

                !ARC/RRC In Control

                Of '305'    |   !Awaiting Allocation
                Orof '310'  |   !Allocated To Engineer
                Orof '315'  |   !In Repair
                Orof '330'  |   !Spares Requested
                Orof '345'  |   !SparesReceived
                Orof '510'  |   !Estimate Ready
                Orof '535'  |   !Estimate Accepted
                Orof '540'  |   !Estimate Refused
                Orof '615'      !Manual QA REjection
                    If tmp:ARCUser
                        If tmp:ARCJobFinished = False
                             If tmp:ARCInControl = False
                                Do ResetARCCounters
                                Do ResetTHirdPartyCounters
                                Do StartARCInControl
                            End !If tmp:ARCInControl = False
                        End ! If tmp:ARCJobFinished = False
                        If local:ARCJobStarted = False
                            local:ARCJobStarted = True
                            local:ARCStartOfRepairTimeDate = aus:DateChanged
                            local:ARCStartOfRepairTimeTime = aus:TimeChanged
                        End ! If local:ARCJobStarted = False
                        ! Inserting (DBH 27/07/2006) # 8022 - Unit is in transit to ARC from RRC
                        If tmp:RRCCourierTime = True
                            Do ResetRRCCounters
                        End ! If tmp:RRCCourierTime = True
                        ! End (DBH 27/07/2006) #8022

                    Else !If Local.AtARC(aus:DateChanged, aus:TimeChanged)
                        If tmp:RRCJobFinished = False
                            If tmp:RRCInControl = False
                                Do ResetRRCCounters
                                Do StartRRCInControl
                                Do ResetThirdPartyCounters
                            End !If tmp:RRCInControl = False
                        End ! If tmp:RRCJobFinished = False
                        ! Inserting (DBH 27/07/2006) # 8022 - Unit is in transit to ARC from RRC
                        If tmp:ARCCourierTime = True
                            Do ResetARCCounters
                        End ! If tmp:RRCCourierTime = True
                        ! End (DBH 27/07/2006) #8022

                    End !If Local.AtARC(aus:DateChanged, aus:TimeChanged)
    !_____________________________________________________________________

                !ARC/RRC Out Of Control
                Of '307'       !Awaiting Unlock
                    If tmp:ARCUser
                        If tmp:ARCOutOfControl = False And tmp:ARCJobFinished = False
                            Do ResetARCCounters
                            Do StartARCOutOfControl
                        End !If tmp:ARCOutOfControl = False
                        ! Insert --- Count Unlock Code Time For Cust Perpective (DBH: 23/07/2009) #10944
                        if (tmp:ARCUnlockCodeTime = 0 and tmp:ARCJobFinished = 0)
                            do StartARCUnlockCode
                        end ! if (tmp:ARCUnlockCodeTime = 0 and tmp:ARCJobFinished = 0)
                        ! end --- (DBH: 23/07/2009) #10944
                    Else !If Local.ARCUser(aus:UserCode)
                        If tmp:RRCOutOfControl = False And tmp:RRCJobFinished = False
                            Do ResetRRCCounters
                            Do StartRRCOutOfControl
                        End !If tmp:RRCOutOfControl = False
                        ! Insert --- Count Unlock Code Time For Cust Perpective (DBH: 23/07/2009) #10944
                        if (tmp:RRCUnlockCodeTime = 0 and tmp:RRCJobFinished = 0)
                            do StartRRCUnlockCode
                        end ! if (tmp:ARCUnlockCodeTime = 0 and tmp:ARCJobFinished = 0)
                        ! end --- (DBH: 23/07/2009) #10944
                    End !If Local.ARCUser(aus:UserCode)
                Of '340'     !Back Order Spares
                    If tmp:ARCUser
                        If tmp:ARCOutOfControl = False And tmp:ARCJobFinished = False
                            Do ResetARCCounters
                            Do StartARCOutOfControl
                        End !If tmp:ARCOutOfControl = False
                    Else !If Local.ARCUser(aus:UserCode)
                        If tmp:RRCOutOfControl = False And tmp:RRCJobFinished = False
                            Do ResetRRCCounters
                            Do StartRRCOutOfControl
                        End !If tmp:RRCOutOfControl = False
                    End !If Local.ARCUser(aus:UserCode)

                Of '520'      !Estimate Sent
                    If tmp:ARCUser
                        If tmp:ARCEstimateTime = False And tmp:ARCJobFinished = False
                            Do ResetARCCounters
                            Do StartARCEstimateTime
                        End !If tmp:ARCOutOfControl = False
                    Else !If Local.ARCUser(aus:UserCode)
                        If tmp:RRCEstimateTime = False And tmp:RRCJobFinished = False
                            Do ResetRRCCounters
                            Do StartRRCEstimateTime
                        End !If tmp:RRCOutOfControl = False
                    End !If Local.ARCUser(aus:UserCode)

    !_____________________________________________________________________
                !Third Party Time
                Of '410' !Despatch To Third Party
                    If tmp:ARCUser
                        If tmp:3rdParty = False And tmp:ARCJobFinished = False
                            Do ResetARCCounters
                            Do ResetThirdPartyCounters
                            Do Start3rdParty
                        End !If tmp:3rdParty = False
                    Else ! If tmp:ARCUser
                    ! Inserting (DBH 17/10/2006) # 8367 - Count the 3rd party if the RRC sends a job there
                        If tmp:3rdParty = False and tmp:RRCJobFinished = False
                            Do ResetRRCCounters
                            DO ResetThirdPartyCounters
                            DO Start3rdParty
                        End ! If tmp:3rdParty = False and tmp:RRCJobFinished = False
                    End ! IF tmp:RRCUser
                    ! End (DBH 17/10/2006) #8367
    !_____________________________________________________________________
                !Wait Time
                Of '405'        !Send To 3rd Party
                    If tmp:ARCUser
                        If job:Exchange_Unit_Number = '' Or |
                            (job:Exchange_Unit_Number <> '' And jobe:WebJob = True)
                            !Only count if exchange unit is for the RRC, or no exchange at all.
                            If tmp:ARCWaitTime = False And tmp:ARCJobFinished = False
                                Do ResetARCCounters
                                Do StartARCWaitTime
                            End ! If tmp:ARCWaitTime = False
                        End ! (job:Exchange_Unit_Number <> '' And jobe:ExchangedATRRC)
                    Else ! If tmp:ARCUser
                    ! Inserting (DBH 17/10/2006) # 8367 - Count the 3rd party in the RRC sends a job there
                        If tmp:RRCWaitTime = False and tmp:RRCJobFInished = False
                            Do ResetRRCCounters
                            Do StartRRCWaitTIme
                        End ! If tmp:RRCWaitTime = False and tmp:RRCJobFInished = False
                    End ! IF tmp:RRCUser
                    ! End (DBH 17/10/2006) #8367
                Of '450'        !Send To ARC
                    If tmp:ARCUser = False And tmp:ARCJobFinished = False
                        If jobe:OBFvalidated
                            Do ResetRRCCounters
                            tmp:RRC1hrTotalTime = tmp:TotalRRCInControl
                            !Save the Real Time - 4087 (DBH: 14-07-2004)
                            tmp:RealRRC1hrTotalTime = tmp:RealTotalRRCInControl
                            If tmp:RRCJobFinished = False
                                local:RRCEndOfRepairTimeDate   = aus:DateChanged
                                local:RRCEndOfRepairTimeTime   = aus:TimeChanged
                                tmp:RRCJobFinished = True
                            End ! If job:Finished = False
                        Else ! If jobe:OBFvalidated
                            If job:Exchange_Unit_Number = '' Or |
                                (job:Exchange_Unit_Number <> '' And jobe:ExchangedATRRC = False)
                                !Only counf if no exchange, or exchange issued at ARC
                                If tmp:RRCWaitTime = False
                                    Do ResetRRCCounters
                                    Do StartRRCWaitTime
                                End ! If tmp:RRCWaitTime = False
                            End !
                        End ! If jobe:OBFvalidated
                    End ! If tmp:ARCUser = False

                Of '355'        !Exchange Requested
                    If tmp:ARCUser
                        If tmp:ARCWaitTime = False And tmp:ARCJobFinished = False
                            Do ResetARCCounters
                            Do StartARCWaitTime
                        End ! If tmp:ARCWaitTime = False
                    Else ! If tmp:ARCUser
                        If tmp:RRCWaitTime = False And tmp:RRCJobFinished = False
                            Do ResetRRCCounters
                            Do StartRRCWaitTime
                        End ! If tmp:RRCWaitTime = False
                    End ! If tmp:ARCUser       #
                Of '453'        !Send To RRC
! Changing (DBH 18/10/2006) # 8367 - Assume correct user
!                    If tmp:ARCUser And tmp:ARCJobFinished = True
! to (DBH 18/10/2006) # 8367
                    If tmp:ARCJobFinished = True
! End (DBH 18/10/2006) #8367
                        If job:Exchange_Unit_Number = '' Or |
                            (job:Exchange_Unit_Number <> '' And jobe:ExchangedATRRC = True)
                            !Only set if no exchange, or exchange issued at ARC
                            Do ResetARCCounters
                            Do StartARCWaitTime
                        End !
                    End ! If tmp:ARCUser = False

    !_____________________________________________________________________

                !Courier Time
                Of '451'      !Despatch To ARC
! Changing (DBH 18/10/2006) # 8367 - Assume the correct user
!                    If tmp:ARCUser = False And jobe:OBFValidated = False And tmp:ARCJobFinished = False
! to (DBH 18/10/2006) # 8367
                    If jobe:OBFValidated = False And tmp:ARCJobFinished = False And tmp:RRCJobFInished = False
! End (DBH 18/10/2006) #8367
                        If job:Exchange_Unit_Number = '' Or|
                            (job:Exchange_Unit_Number <> '' And jobe:ExchangedAtRRC = False)
                            ! Only count if no exchange, or exchanged at ARC
                            Do ResetRRCCounters
                            Do StartRRCCourierTime
                        End ! (job:Exchange_Unit_Number <> '' And jobe:ExchangedAtRRC = False)e
                    End ! If tmp:ARCUser = False
                Of '454'        !Despatched To RRC
! Changing (DBH 18/10/2006) # 8367 - Assume the correct user
!                    If tmp:ARCUser And tmp:RRCJobFinished = False
! to (DBH 18/10/2006) # 8367
                    If tmp:RRCJobFinished = False
! End (DBH 18/10/2006) #8367
                        If job:Exchange_Unit_Number = '' Or |
                            (job:Exchange_Unit_Number <> '' And jobe:ExchangedAtRRC = True)
                            ! Only count if no exchange, or exchanged at RRC
                            Do ResetARCCounters
                            Do StartARCCourierTime
                        End ! (job:Exchange_Unit_Number <> '' And jobe:ExchangedAtRRC = True)
                    End ! If tmp:ARCUser
    !_____________________________________________________________________

                !ARC In Control
                Of '452'    |   !Received At ARC
                OrOf '456'      !Received At ARC (Query)
                    If tmp:ARCJobFinished = False
                        If tmp:ARCInControl = False
                            Do ResetARCCounters
                            Do StartARCInControl
                            If tmp:3rdParty = True
                                Do ResetThirdPartyCounters
                            End ! If tmp:ThirdParty = True
                            If tmp:RRCCourierTime = True
                                Do ResetRRCCounters
                            End ! If tmp:RRCCourierTime = True
                        End ! If tmp:ARCInControl = False
                        !Unit now at the ARC -  (DBH: 17-02-2004)
                        If tmp:SentToARCDate = 0
                            tmp:SentToARCDate = aus:DateChanged
                            tmp:SentToARCTime = aus:TimeChanged
                        End !If tmp:SentToARCDate = 0
                        If local:ARCJobStarted = False
                            local:ARCJobStarted = True
                            local:ARCStartOfRepairTimeDate = aus:DateChanged
                            local:ARCStartOfRepairTimeTime = aus:TimeChanged
                        End ! If local:ARCJobStarted = False
                        ! Inserting (DBH 28/09/2006) # 8184 - In case some prat has put the job back in RRC control before it arrives at the ARC
                        If tmp:RRCInControl = True And job:Exchange_unit_Number = 0
                            Do ResetRRCCounters
                        End ! If tmp:RRCInControl = True And jobe:ExchangedATRRC = False
                        ! End (DBH 28/09/2006) #8184
                    End ! If tmp:ARCJobFinished = False

    ! ____________________________________________________________________________________
                Of '462' ! Received From PUP
                    ! Inserting (DBH 16/06/2006) #7133 - Set the initial values incase there ISN'T a 000 NEW JOB BOOKING
                    If jobe:WebJob
                        ! Job was booked at the RRC (DBH: 24/09/2006)
                        tmp:RRCInControlStartDate = aus:DateChanged
                        tmp:RRCInControlStartTime = aus:TimeChanged

                        local.GetStartDate(tmp:RRCInControlStartDate,tmp:RRCInControlStartTime,1)
                        tmp:RRCInControl             = True
                        local:RRCStartOfRepairTimeDate = aus:DateChanged
                        local:RRCStartOfRepairTimeTime = aus:TimeChanged
                        local.GetStartDate(local:RRCStartOfRepairTimeDate,local:RRCStartOfRepairTimeTime,1)
                        local:RRCJobStarted = True
                    Else ! If jobe:WebJob
                        ! Job was booked at the ARC (DBH: 24/09/2006)
                        tmp:ARCInControlStartDate    = aus:DateChanged
                        tmp:ARCInControlStartTime    = aus:TimeChanged

                        local.GetStartDate(tmp:ARCInControlStartDate,tmp:ARCInControlStartTime,0)
                        tmp:ARCInControl = True
                        local:ARCStartOfRepairTimeDate = job:Date_Booked
                        local:ARCStartOfRepairTImeTime = job:Time_Booked
                        local.GetStartDate(local:ARCStartOfRepairTimeDate,local:ARCStartOfRepairTimeTime,0)
                        local:ARCJobStarted = True
                    End ! If jobe:WebJob
                    ! End (DBH 16/06/2006) #7133
    !_____________________________________________________________________
                Of '463' ! Send To PUP

                    ! Inserting (DBH 25/09/2006) # 8184 - Send To PUP is the end of the line for VCP jobs
                    Do ResetRRCCounters

                    If tmp:CustomerCollection = True
                        Do ResetCustomerCollectionCounters
                    End ! If tmp:CustomerCollection = True
                    If tmp:ReadyToDespatch = True
                        Do ResetReadyToDespatchCounters
                    End ! If tmp:ReadyToDespatch = True
                    ! End (DBH 25/09/2006) #8184
    ! ____________________________________________________________________________________
                Of '464' ! Despatched To PUP
    ! ____________________________________________________________________________________
                Of '465' ! Received At PUP
    ! ____________________________________________________________________________________

                Of '605' !QA Check Required
                    If tmp:ARCUser
                        If tmp:ARCInControl = False And tmp:ARCJobFinished = False
                            Do ResetARCCounters
                            Do StartARCInControl
                            Do ResetThirdPartyCounters
                        End ! If tmp:ARCInControl = False And tmp:ARCJobFinished = False
                    Else ! If tmp:ARCUser
                        If tmp:RRCInControl = False
                            If (tmp:ARCCourierTime = True Or tmp:RRCCourierTime = True) And tmp:RRCJobFinished = False
                                ! Job is either coming back from ARC, or was never at the ARC. (DBH: 26/09/2006)
                                Do ResetRRCCounters
                                Do StartRRCInControl
                                tmp:SentToRRCDate = aus:DateChanged
                                tmp:SentToRRCTime = aus:TimeChanged
                            End ! If tmp:ARCCourierTime = True Or tmp:RRCourierTime = True
                            If local:ARCJobStarted = False And tmp:RRCJobFinished = False
                                ! Job is either coming back from ARC, or was never at the ARC. (DBH: 26/09/2006)
                                Do ResetRRCCounters
                                Do StartRRCInControl
                                tmp:SentToRRCDate = aus:DateChanged
                                tmp:SentToRRCTime = aus:TimeChanged
                            End ! If local:ARCJobStarted = False And tmp:RRCJobFinished = False
                        End ! If tmp:RRCInControl = False
                        If tmp:ARCCourierTime = True And tmp:RRCJobFinished = False
                            Do ResetARCCounters
                        End ! If tmp:ARCCourierTime = True And tmp:RRCJobFinished = False
                    End ! If tmp:ARCUser

!                    If (tmp:ARCCourierTime = True Or tmp:RRCCourierTime) And tmp:ARCUser = False And tmp:RRCInControl = False
!                        !Unit is returning from the ARC -  (DBH: 18-02-2004)
!                        Do ResetRRCCounters
!                        Do StartRRCInControl
!                        tmp:SentToRRCDate = aus:DateChanged
!                        tmp:SentToRRCDate = aus:TimeChanged
!                    End !If NowAtRRC# = True
!                    If tmp:ARCUser and tmp:ARCInControl = False And tmp:ARCJobFinished = False
!                        Do ResetARCCounters
!                        Do StartARCInControl
!                    End !If tmp:ARCUser and tmp:ARCInControl = False
!                    ! Inserting (DBH 27/07/2006) # 8022 - Unit is in transit to ARC from RRC
!                    If ~tmp:ARCUser And tmp:ARCCourierTime = True And tmp:RRCJobFinished = False
!                        Do ResetARCCounters
!                    End ! If tmp:RRCCourierTime = True
!                    ! End (DBH 27/07/2006) #8022
    !_____________________________________________________________________

                Of '705' | !Job Completed
                OrOf '710'  !Job Completed (OBF)
                    If tmp:ARCUser
                        !If this job is not going back to RRC (DBH: 19-03-2004)
                        If jobe:WebJob = 0
                            Do ResetARCCounters
                            DO ResetReadyToDespatchCounters
                            Do StartReadyToDespatch
                            !End Of Total Repair Time for exchange (DBH: 19-03-2004)
                            If tmp:ARCJobFinished = False
                                local:ARCEndOfRepairTimeDate   = aus:DateChanged
                                local:ARCEndOfRepairTimeTime   = aus:TimeChanged
                                tmp:ARCJobFinished = True
                            End ! If job:Finished = False
                        Else !If jobe:WebJob = 0
                            ! Inserting (DBH 15/06/2006) #7133 - If RRC exchange, then stop ARC count at completion
                            If job:Exchange_Unit_Number <> 0 And jobe:ExchangedATRRC = True
                                If tmp:ARCJobFinished = False
                                    local:ARCEndOfRepairTimeDate = aus:DateChanged
                                    local:ARCEndOfrepairTimeTime = aus:TimeChanged
                                    tmp:ARCJobFinished = True
                                End ! If local.ARCJobFinished = False
                                Do ResetARCCounters
! Deleting (DBH 17/10/2006) # 8367 - Not interersted in wait time after the job has finished
!                                Do StartARCWaitTime
! End (DBH 17/10/2006) #8367
! Inserting (DBH 02/02/2007) # 8719 - Should be following the ARC exchange. So ignore this status
                            Elsif job:Exchange_Unit_Number <> 0 And jobe:ExchangedATRRC = False
! End (DBH 02/02/2007) #8719
                            Else ! If job:Exchange_Unit_Number <> 0 And jobe:ExchangeATRRC = True
                            ! End (DBH 15/06/2006) #7133
                                If jobe:OBFValidated = True
                                    DO ResetARCCounters
                                    If tmp:ARCJobFinished = False
                                        local:ARCEndOfRepairTimeDate = aus:DateChanged
                                        local:ARCEndOfrepairTimeTime = aus:TimeChanged
                                        tmp:ARCJobFinished = True
                                    End ! If local.ARCJobFinished = False
                                    If jobe:WebJob
                                        ! Inserting (DBH 29/09/2006) # 8184 - This RRC OBF. If credited, don't send it back to the RRC
                                        Found# = False
                                        Save_JOBOUTFL_ID = Access:JOBOUTFL.SaveFile()
                                        Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                                        joo:JobNumber = job:Ref_Number
                                        Set(joo:JobNumberKey,joo:JobNumberKey)
                                        Loop ! Begin Loop
                                            If Access:JOBOUTFL.Next()
                                                Break
                                            End ! If Access:JOBOUTFL.Next()
                                            If joo:JobNumber <> job:Ref_Number
                                                Break
                                            End ! If joo:JobNumber <> job:Ref_Number
                                            If Instring('OBF CREDITED',joo:Description,1,1)
                                                Found# = True
                                                Break
                                            End ! If Instring('OBF CREDITED',joo:Description,1,1)
                                        End ! Loop
                                        Access:JOBOUTFL.RestoreFile(Save_JOBOUTFL_ID)

                                        If Found# = True
                                            If tmp:RRCJobFinished = False
                                                local:RRCEndOfRepairTimeDate   = aus:DateChanged
                                                local:RRCEndOfRepairTimeTime   = aus:TimeChanged
                                                tmp:RRCJobFinished = True
                                            End ! If job:Finished = False\
                                            ! Insert --- OBF job not going back to RRC. Count as despatched. (DBH: 10/07/2009) #10936
                                            If local:JobDespatched = False
                                                local:LastDespatchDate = aus:DateChanged
                                                local:LastDespatchTime  = aus:TimeChanged
                                                local:JobDespatched = True
                                            End ! If local:JobDespatched = False
                                            ! end --- (DBH: 10/07/2009) #10936
                                        End ! If Found# = True
                                        ! End (DBH 29/09/2006) #8184
                                    End ! If jobe:WebJob
                                Else ! If jobe:OBFValidated = True
! Changing (DBH 27/07/2006) # 8022 - Don't know why this is being set back to "In Control". Will use "Wait Time" instead
!                                    ! Inserting (DBH 29/11/2005) #6804 - Reset the counters and return to ARC In Control. This is so the RTRCP is correct
!                                    Do ResetARCCounters
!                                    Do StartARCInControl
!                                    ! End (DBH 29/11/2005) #6804
! to (DBH 27/07/2006) # 8022
                                    If tmp:ARCJobFinished = False
                                        local:ARCEndOfRepairTimeDate = aus:DateChanged
                                        local:ARCEndOfrepairTimeTime = aus:TimeChanged
                                        tmp:ARCJobFinished = True
                                    End ! If local.ARCJobFinished = False
                                    Do ResetARCCounters
! Deleting (DBH 17/10/2006) # 8367 - Not interested in wait time after the job has finished
!                                    Do StartARCWaitTime
! End (DBH 17/10/2006) #8367
!                                    ! If the ARC has sent an Exchange to the RRC. Finish the job here. (DBH: 28/07/2006)
!                                    If tmp:ARCExchangeSentToRRC = True And job:Exchange_Unit_Number <> 0
!                                        If tmp:ARCJobFinished = False
!                                            local:ARCEndOfRepairTimeDate = aus:DateChanged
!                                            local:ARCEndOfrepairTimeTime = aus:TimeChanged
!                                            tmp:ARCJobFinished = True
!                                        End ! If local.ARCJobFinished = False
!                                        Do ResetARCCounters
!                                        Do StartARCWaitTime
!                                    Else ! If tmp:ARCExchangeSentToRRC = True
!                                        If tmp:ARCJobFinished = False
!                                            local:ARCEndOfRepairTimeDate = aus:DateChanged
!                                            local:ARCEndOfrepairTimeTime = aus:TimeChanged
!                                            tmp:ARCJobFinished = True
!                                        End ! If local.ARCJobFinished = False
!                                        Do ResetARCCounters
!                                        Do StartARCInControl
!                                    End ! If tmp:ARCExchangeSentToRRC = True
! End (DBH 27/07/2006) #8022
                                End ! If jobe:OBFValidated = True
                            End ! If job:Exchange_Unit_Number <> 0 And jobe:ExchangeATRRC = True
                         End !If jobe:WebJob = 0
                        !Save the total RRC In Control Time for the 1hr summary -  (DBH: 03-03-2004)
                        tmp:ARC1hrTotalTime = tmp:TotalARCInControl
                        !Save the Real Time - 4087 (DBH: 14-07-2004)
                        tmp:RealARC1hrTotalTime = tmp:RealTotalARCInControl
                    Else !If tmp:ARCUser

                        If tmp:ReadyToDespatch = False
                            Do ResetRRCCounters
                            Do ResetReadyToDespatchCounters
                            Do StartReadyToDespatch
                            Do ResetCustomerCollectionCounters
                        End !If tmp:ReadyToDespatch = False
                        tmp:RRC1hrTotalTime = tmp:TotalRRCInControl
                        !Save the Real Time - 4087 (DBH: 14-07-2004)
                        tmp:RealRRC1hrTotalTime = tmp:RealTotalRRCInControl
                        If tmp:RRCJobFinished = False
                            local:RRCEndOfRepairTimeDate   = aus:DateChanged
                            local:RRCEndOfRepairTimeTime   = aus:TimeChanged
                            tmp:RRCJobFinished = True
                        End ! If job:Finished = False

                    End !If local.ARCUser(aus:UserCode)

                    !Save the completed date, because the report shows the "last" date -  (DBH: 18-02-2004)
                    tmp:LastCompletedDate   = aus:DateChanged
                    tmp:LastCompletedTime   = aus:TimeChanged
   !_____________________________________________________________________
                !Customer Collection
                Of '810'    |   !Ready To Despatch
                Orof '916'      !Paid Awaiting Despatch
                    If tmp:ARCUser
                        If tmp:ExchangeUnitAttached Or jobe:WebJob
                            !Do not count this if exchange unit attached.
                            !As the job will not be going back to the customer -  (DBH: 24-02-2004)
                        Else !If tmp:ExchangeUnitAttached
                            If local:JobDespatched = False
                                local:LastDespatchDate = aus:DateChanged
                                local:LastDespatchTime  = aus:TimeChanged
                                local:JobDespatched = True
                            End ! If local:JobDespatched = False
                        End !If tmp:ExchangeUnitAttached
                    Else !If local.ARCUser(aus:UserCode)
                        If tmp:CustomerCollection = False
                            Do ResetRRCCounters
                            Do ResetReadyToDespatchCounters
                            Do ResetCustomerCollectionCounters
                            Do StartCustomerCollection
                        End !If tmp:CustomerCollection = False
                        If ~tmp:ExchangeUnitAttached
                            !Count when the exchange unit is despatched instead - TrkBs: 4714 (DBH: 08-11-2004 61)
                            If local:JobDespatched = False
                                local:LastDespatchDate = aus:DateChanged
                                local:LastDespatchTime  = aus:TimeChanged
                                local:JobDespatched = True
                            End ! If local:JobDespatched = False
                        End ! If ~tmp:Exchange UnitAttached
                    End !If local.ARCUser(aus:UserCode)
    !_____________________________________________________________________

                !End Of Job
                Of '815'        ! Returned To Exch. Stock
                    If tmp:ARCUser
                        ! Inserting (DBH 02/02/2007) # 8719 - Don't count this status change if the exchange is going back to the RRC
                        If local:ARCExchangeDEspatchedToRRC = False
                        ! End (DBH 02/02/2007) #8719
                            Do ResetARCCounters
                            If tmp:CustomerCollection = True
                                Do ResetCustomerCollectionCOunters
                            End ! If tmp:CustomerCollection = True
                            If tmp:ReadyToDespatch = True
                                Do ResetReadyToDespatchCounters
                            End ! If tmp:ReadyToDespatch = True
                        End ! If local:ARCExchangeDEspatchedToRRC = False
                    Else ! If tmp:ARCUser
                        Do ResetRRCCounters
                        If tmp:CustomerCollection = True
                            Do ResetCustomerCollectionCOunters
                        End ! If tmp:CustomerCollection = True
                        If tmp:ReadyToDespatch = True
                            Do ResetReadyToDespatchCounters
                        End ! If tmp:ReadyToDespatch = True
                    End ! If tmp:ARCUser

                Of '901'        !Despatched
                OrOf '905'      !Despatch Unpaid
                Orof '910'      !Despatch Paid
                    IF tmp:ARCUser
                        Do ResetARCCounters
                        If tmp:CustomerCollection = True
                            Do ResetCustomerCollectionCOunters
                        End ! If tmp:CustomerCollection = True
                        If tmp:ReadyToDespatch = True
                            Do ResetReadyToDespatchCounters
                        End ! If tmp:ReadyToDespatch = True
                        ! Inserting (DBH 28/07/2006) # 8022 - Whoever despatched the job gets the "Customer Collection" count
                        tmp:WhoDespatchedTheJob = 'ARC'
                        ! End (DBH 28/07/2006) #8022
                    Else ! IF tmp:ARCUser
                        Do ResetRRCCounters
                        If tmp:CustomerCollection = True
                            Do ResetCustomerCollectionCOunters
                        End ! If tmp:CustomerCollection = True
                        If tmp:ReadyToDespatch = True
                            Do ResetReadyToDespatchCounters
                        End ! If tmp:ReadyToDespatch = True
                        ! Inserting (DBH 28/07/2006) # 8022 - Whoever despatched the job gets the "Customer Collection" count
                        tmp:WhoDespatchedTheJob = 'RRC'
                        ! End (DBH 28/07/2006) #8022
                    End ! IF tmp:ARCUser
            End !Case Sub(aus:NewStatus,1,3)
        End ! If aus:Type = 'JOB'
    !_____________________________________________________________________
            !Exchange Status
    !_____________________________________________________________________
        If aus:Type = 'EXC'! Or aus:Type = '2NE'
            Case Sub(aus:NewStatus,1,3)
                Of '110'        !Despatch Exchange Unit
                    If tmp:ARCUser And local:ARCExchangeDespatchedToRRC = False
                        !At the ARC. Is the unit going to a customer, or the RRC? -  (DBH: 27-02-2004)
                        If jobe:WebJob = True
!                            Do ResetARCCounters
                            Do ResetARCCounters
                            Do StartARCWaitTime
                            If tmp:ARCJobFinished = False
                                !Start - Job is now finished as far as TAT is concerned - TrkBs: 4714 (DBH: 04-11-2004 61)
                                tmp:ARC1hrTotalTime = tmp:TotalARCInControl
                                tmp:RealARC1hrTotalTime = tmp:RealTotalARCInControl
                                !End   - Job is now finished as far as TAT is concerned - TrkBs: 4714 (DBH: 04-11-2004 61)
                                local:ARCEndOfRepairTimeDate   = aus:DateChanged
                                local:ARCEndOfRepairTimeTime   = aus:TimeChanged
                                tmp:ARCJobFinished = True
                                local:ARCExchangeDespatchedToRRC = True
                            End ! If tmp:ARCJobFinished = True
                        Else !If jobe:WebJob = True
                            Do ResetARCCounters
                            Do ResetReadyToDespatchCounters
                            Do ResetCustomerCollectionCounters
                            Do StartCustomerCollection
                            If tmp:ARCJobFinished = False
                                !Start - Job is now finished as far as TAT is concerned - TrkBs: 4714 (DBH: 04-11-2004 61)
                                tmp:ARC1hrTotalTime = tmp:TotalARCInControl
                                tmp:RealARC1hrTotalTime = tmp:RealTotalARCInControl
                                !End   - Job is now finished as far as TAT is concerned - TrkBs: 4714 (DBH: 04-11-2004 61)
                                local:ARCEndOfRepairTimeDate   = aus:DateChanged
                                local:ARCEndOfRepairTimeTime   = aus:TimeChanged
                                tmp:ARCJobFinished = True
                            End ! If job:Finished = False
                        End !If jobe:WebJob = True
                    Else !If local.ARCUser(aus:UserCode)
                        Do ResetRRCCounters
                        Do ResetReadyToDespatchCounters
                        Do ResetCustomerCollectionCounters
                        Do StartCustomerCollection
                        !Start - Job is now finished as far as TAT is concerned - TrkBs: 4714 (DBH: 04-11-2004 61)
                        If tmp:RRCJobFinished = False
                            tmp:RRC1hrTotalTime = tmp:TotalRRCInControl
                            tmp:RealRRC1hrTotalTime = tmp:RealTotalRRCInControl
                            local:RRCEndOfRepairTimeDate   = aus:DateChanged
                            local:RRCEndOfRepairTimeTime   = aus:TimeChanged
                            tmp:RRCJobFinished = True
                        End ! If job:Finished = False
                        !End   - Job is now finished as far as TAT is concerned - TrkBs: 4714 (DBH: 04-11-2004 61)

                    End !If local.ARCUser(aus:UserCode)
    !_________________________________________-___________________________
                Of '458'        !Exchange Despatched To RRC
                    Do ResetARCCounters
                    ! Inserting (DBH 18/10/2006) # 8367 - This still needs to be counted against courier time, even though the ARC job has technically finished
                    Do StartRRCCourierTime
                    ! End (DBH 18/10/2006) #8367
    !_____________________________________________________________________

                Of '459'        !Exchange Received AT RRC
                    ! Inserting (DBH 27/07/2006) # 8022 - Unit is in transit to ARC from RRC
                    If tmp:RRCJobFinished = False
                        If tmp:ARCCourierTime = True
                            Do ResetARCCounters
                        End ! If tmp:RRCCourierTime = True
                        ! End (DBH 27/07/2006) #8022

                        Do ResetRRCCounters
                        Do StartRRCInControl
                        ! End (DBH 28/07/2006) #8022

                        ! Deleted (DBH 30/11/2005) #6804 - The job shouldn't finish here
                        ! If Local:jobFinished = False
                        !                         local:EndOfRepairTimeDate   = aus:DateChanged
                        !                         local:EndOfRepairTimeTime   = aus:TimeChanged
                        !                         Local:jobFinished = True
                        !                     End ! If job:Finished = False
                        ! End (DBH 30/11/2005) #6804

                    End ! If tmp:RRCJobFinished = False
    !_____________________________________________________________________

                Of '605'        !QA Check Required
                    If tmp:ARCUser
                        If tmp:ARCJobFinished = False
                            Do ResetARCCounters
                            Do StartARCInControl
                        End ! If tmp:ARCJobFinished = False
                    Else !If local.ARCUser(aus:UserCode)
                        If tmp:RRCJobFinished = False
                            Do ResetRRCCounters
                            Do StartRRCInControl
                        End ! If tmp:RRCJobFinished = False
                    End !If local.ARCUser(aus:UserCode)
    !_____________________________________________________________________

                Of '901'        !Despatched
!                    cont# = False
!                    If tmp:ARCUser
!                        If job:Exchange_Unit_Number <> '' And jobe:ExchangedATRRC = False
!                            !Only count despatch if the ARC attached the exchange
!                            cont# = True
!                        End ! If job:Exchange_Unit_Number <> '' And jobe:ExchangedATRRC = False
!                    Else ! If tmp:ARCUser
!                        If job:Exchange_Unit_Number <> '' And jobe:ExchangedAtRRC = True
!                            !Only count despatch if the RRC attached the exchange
!                            cont# = True
!                        End ! If job:Exchange_Unit_Number <> '' And jobe:ExchangedAtRRC = True
!                    End ! If tmp:ARCUser
!                    If cont# = True                                                0
                        ! Inserting (DBH 01/12/2005) #6804 - The job should of "finished" before now, but just incase there are any missing status changes
                        If tmp:ARCUser And local:ARCExchangeDespatchedToRRC = False
                            If jobe:WebJob = False
                                ! Do not count this despatch because the unit will be going back to the RRC (DBH: 27/09/2006)
                                Do ResetARCCounters
                                If tmp:CustomerCollection = True
                                    Do ResetCustomerCollectionCounters
                                End ! If tmp:CustomerCollection = True
                                If tmp:ReadyToDespatch = True
                                    Do ResetReadyToDespatchCounters
                                End ! If tmp:ReadyToDespatch = True
                                If local:JobDespatched = False
                                    local:LastDespatchDate = aus:DateChanged
                                    local:LastDespatchTime  = aus:TimeChanged
                                    local:JobDespatched = True
                                End ! If local:JobDespatched = False

                                If tmp:ARCJobFinished = False
                                    !Start - Job is now finished as far as TAT is concerned - TrkBs: 4714 (DBH: 04-11-2004 61)
                                    tmp:ARC1hrTotalTime = tmp:TotalARCInControl
                                    tmp:RealARC1hrTotalTime = tmp:RealTotalARCInControl
                                    !End   - Job is now finished as far as TAT is concerned - TrkBs: 4714 (DBH: 04-11-2004 61)
                                    local:ARCEndOfRepairTimeDate   = aus:DateChanged
                                    local:ARCEndOfRepairTimeTime   = aus:TimeChanged
                                    tmp:ARCJobFinished = True
                                End ! If job:Finished = False
                                ! Inserting (DBH 28/07/2006) # 8022 - Whoever despatched the job gets the "Customer Collection" count
                                tmp:WhoDespatchedTheJob = 'ARC'
                                ! End (DBH 28/07/2006) #8022
                            End ! If jobe:WebJob = False
                        Else ! If tmp:ARCUser
                            Do ResetRRCCounters
! Deleting (DBH 22/02/2007) # 8794 - No need to reset the ARC here
!                            DO REsetARCCounters
! End (DBH 22/02/2007) #8794
                            If tmp:CustomerCollection = True
                                Do ResetCustomerCollectionCounters
                            End ! If tmp:CustomerCollection = True
                            If tmp:ReadyToDespatch = True
                                Do ResetReadyToDespatchCounters
                            End ! If tmp:ReadyToDespatch = True
                            If local:JobDespatched = False
                                local:LastDespatchDate = aus:DateChanged
                                local:LastDespatchTime  = aus:TimeChanged
                                local:JobDespatched = True
                            End ! If local:JobDespatched = False

                            If tmp:RRCJobFinished = False
                                tmp:RRC1hrTotalTime = tmp:TotalRRCInControl
                                tmp:RealRRC1hrTotalTime = tmp:RealTotalRRCInControl
                                local:RRCEndOfRepairTimeDate   = aus:DateChanged
                                local:RRCEndOfRepairTimeTime   = aus:TimeChanged
                                tmp:RRCJobFinished = True
                            End ! If job:Finished = False
                            ! Inserting (DBH 28/07/2006) # 8022 - Whoever despatched the job gets the "Customer Collection" count
                            tmp:WhoDespatchedTheJob = 'RRC'
                            ! End (DBH 28/07/2006) #8022
                        End ! If tmp:ARCUser
                        ! End (DBH 01/12/2005) #6804
!                    End ! If cont# = True
            End !Case Sub(aus:NewStatus,1,3)
        End ! If aus:Type = 'EXC' Or aus:Type = '2NE'
    End !Loop

    If Command('/DEBUG')
        LinePrint('ARC Job Finished:' & tmp:ARCJobFinished,'c:\tat.log')
        LinePrint('RRC Job Finished:' & tmp:RRCJobFinished,'c:\tat.log')
    End ! If Command('/DEBUG')

!.... Continues ...

    !Incase the job has not been closed properly, reset the counters based on NOW -  (DBH: 19-02-2004)
    aus:DateChanged = Today()
    aus:TimeChanged = Clock()
    Do ResetARCCounters
    Do ResetRRCCounters
    Do ResetThirdPartyCounters
    Do ResetCustomerCollectionCOunters
    Do ResetReadyToDespatchCounters

! Changing (DBH 01/12/2005) #6804 - Finish off each section, if it hasn't already been done above
!     If Local:jobFinished = False
!         local:EndOfRepairTimeDate = Today()
!         local:EndOFRepairTimeTime = Clock()
!     End ! If local:EndOfRepairTimeDate = 0
! to (DBH 01/12/2005) #6804

    If tmp:ARCJobFinished = False
        If local:ARCStartOfRepairTimeDate <> ''
            local:ARCEndOfRepairTimeDate = Today()
            local:ARCEndOfRepairTimeTime = Clock()
            !Save the total RRC In Control Time for the 1hr summary -  (DBH: 03-03-2004)
            tmp:ARC1hrTotalTime = tmp:TotalARCInControl
            !Save the Real Time - 4087 (DBH: 14-07-2004)
            tmp:RealARC1hrTotalTime = tmp:RealTotalARCInControl
        End ! If local:ARCStartOfRepairTimeDate <> ''

    End ! If tmp:ARCJobFinished = False

    If tmp:RRCJobFinished = False !
! Deleting (DBH 02/02/2007) # 8719 - Don't count if the RRC hasn't completed it's half
!        If local:RRCStartOfRepairTimeDate <> ''
!            local:RRCEndOfRepairTimeDate = Today()
!            local:RRCEndOfRepairTimeTime = Clock()
!            !Save the total RRC In Control Time for the 1hr summary -  (DBH: 03-03-2004)
!            tmp:RRC1hrTotalTime = tmp:TotalRRCInControl
!            !Save the Real Time - 4087 (DBH: 14-07-2004)
!            tmp:RealRRC1hrTotalTime = tmp:RealTotalRRCInControl
!        End ! If local:RRCStartOfRepairTimeDate <> ''
! End (DBH 02/02/2007) #8719
    End ! If tmp:RRCJobFinished = False
    ! End (DBH 01/12/2005) #6804

    If local:JobDespatched = False
        local:LastDespatchDate = Today()
        local:LastDespatchTime  = Clock()
        local:JobDespatched = True
    End ! If local:JobDespatched = False


    !Total Repair Time is time to Exchange Received at RRC
    !or time to completion.
    !Work out time based on who booked the job -  (DBH: 19-03-2004)

    !RCP - TrkBs: 4714 (DBH: 08-11-2004 61)
    ! Deleted (DBH 01/12/2005) #6804 - I don't think tmp:TOtalTime is used
    ! tmp:TotalTime   = local.TimeDifference(local:StartDate,local:EndOfRepairTimeDate,local:StartTime,local:EndOfRepairTimeTime,tmp:ARCAccount)
    ! End (DBH 01/12/2005) #6804



    If Command('/DEBUG')
        LinePrint('== Totals ==','c:\tat.log')
    End !If Command('/DEBUG')
    IF jobe:WebJob
! Changing (DBH 23/02/2007) # 8794 - Add the seperate estimate time
!        tmp:TotalTime   -= tmp:TotalRRCOutOfControl
! to (DBH 23/02/2007) # 8794
        tmp:TotalTime -= (tmp:TOtalRRCOutOfControl + tmp:TotalRRCEstimateTime)
! End (DBH 23/02/2007) #8794
    Else ! IF jobe:WebJob
        tmp:TotalTime   -= (tmp:TotalARCOutOfControl + tmp:TotalARCEstimateTime)
    End ! IF jobe:WebJob

    ! Start - Detail shows the Real Time RCP - TrkBs: 5934 (DBH: 08-07-2005)
    ! Changing (DBH 17/06/2006) #7133 - Only start counting ARC, when job arrives there
    ! tmp:ARCRealTotalTime = local.RealTimeDifference(local:StartDate, local:ARCEndOfRepairTimeDate, local:StartTime, local:ARCEndOfRepairTimeTime, tmp:ARCAccount)
    !     tmp:RRCRealTotalTime = local.RealTimeDifference(local:StartDate, local:RRCEndOfRepairTimeDate, local:StartTime, local:RRCEndOfRepairTimeTime, tmp:ARCAccount)
    ! to (DBH 17/06/2006) #7133
    If local:ARCStartOfRepairTimeDate <> ''
        tmp:ARCRealTotalTime = local.RealTimeDifference(local:ARCStartOfRepairTimeDate, local:ARCEndOfRepairTimeDate, local:ARCStartOfRepairTimeTime, local:ARCEndOfRepairTimeTime, 1)
        ! Inserting (DBH 17/10/2006) # 8367 - Total Repair Time becomes Customer Perspective
        If jobe:WebJob = True
            ! Change --- Count weekend when applicable (DBH: 24/07/2009) #10944
            !Tmp:ARCCustomerPerspective = BHTimeDifference24Hr(local:ARCStartOfRepairTimeDate, local:ARCEndOfRepairTimeDate, local:ARCStartOfRepairTimeTime, local:ARCEndOfRepairTimeTime)
            ! To --- (DBH: 24/07/2009) #10944
            tmp:ARCCustomerPerspective = local.CustomerPerspective(local:ARCStartOfRepairTimeDate, local:ARCEndOfRepairTimeDate, local:ARCStartOfRepairTimeTime, local:ARCEndOfRepairTimeTime,1)
            ! end --- (DBH: 24/07/2009) #10944
        Else ! If jobe:WebJob = True
            ! Change --- Count weekends when applicable (DBH: 24/07/2009) #10944
            !Tmp:ARCCustomerPerspective = BHTimeDifference24Hr(job:Date_Booked, local:ARCEndOfRepairTimeDate, job:Time_Booked, local:ARCEndOfRepairTimeTime)
            ! To --- (DBH: 24/07/2009) #10944
            tmp:ARCCustomerPerspective = local.CustomerPerspective(job:Date_Booked, local:ARCEndOfRepairTimeDate, job:Time_Booked, local:ARCEndOfRepairTimeTime,1)
            ! end --- (DBH: 24/07/2009) #10944
        End ! If jobe:WebJob = True


        ! End (DBH 17/10/2006) #8367
    End ! If local:ARCStartOfRepairTimeDate <> ''
! Changing (DBH 02/02/2007) # 8719 - Only count if RRC hasn't finished job
!    If local:RRCStartOfRepairTimeTime <> ''
! to (DBH 02/02/2007) # 8719
    If local:RRCStartOfRepairTimeTime <> '' And tmp:RRCJobFinished
! End (DBH 02/02/2007) #8719
        tmp:RRCRealTotalTime = local.RealTimeDifference(local:RRCStartOfRepairTimeDate, local:RRCEndOfRepairTimeDate, local:RRCStartOfRepairTimeTime, local:RRCEndOfRepairTimeTime, 0)
        ! Inserting (DBH 17/10/2006) # 8367 - Total Repair Time becomes Customer Perspective
! Changing (DBH 06/11/2006) # 8367 - If the job is OBF validated (not coming back to RRC) count cust perspective too ARC completion
!        tmp:RRCCustomerPerspective = BHTimeDifference24Hr(job:Date_Booked, local:RRCEndOfRepairTimeDate, job:Time_Booked, local:RRCEndOfRepairTimeTime)
! to (DBH 06/11/2006) # 8367
        ! Change --- Count weekends when applicable (DBH: 24/07/2009) #10944
        !Tmp:RRCCustomerPerspective = BHTimeDifference24Hr(job:Date_Booked, local:RRCEndOfRepairTimeDate, job:Time_Booked, local:RRCEndOfRepairTimeTime)
        ! To --- (DBH: 24/07/2009) #10944
        tmp:RRCCustomerPerspective = local.CustomerPerspective(job:Date_Booked, local:RRCEndOfRepairTimeDate, job:Time_Booked, local:RRCEndOfRepairTimeTime,0)
        ! end --- (DBH: 24/07/2009) #10944
        If jobe:OBFvalidated
            ! Change --- Count weekends when applicable (DBH: 24/07/2009) #10944
            !Tmp:RRCCustomerPerspective = BHTimeDifference24Hr(job:Date_Booked, local:ARCEndOfRepairTimeDate, job:Time_Booked, local:ARCEndOfRepairTimeTime)
            ! To --- (DBH: 24/07/2009) #10944
            tmp:RRCCustomerPerspective = local.CustomerPerspective(job:Date_Booked, local:ARCEndOfRepairTimeDate, job:Time_Booked, local:ARCEndOfRepairTimeTime,0)
            ! end --- (DBH: 24/07/2009) #10944
        End ! If jobe:OBFvalidated
! End (DBH 06/11/2006) #8367

        ! End (DBH 17/10/2006) #8367
    End ! If local:RRCStartOfReparTimeTime <> ''
    ! End (DBH 17/06/2006) #7133

    If Command('/DEBUG')
        LinePrint('ARC Start Of Repair: ' & Format(local:ARCStartOfRepairTimeDate,@d6) & ' ' & Format(local:ARCStartOfRepairTimeTime,@t1),'c:\tat.log')
        LinePrint('ARC End Of Repair: ' & Format(local:ARCEndOfRepairTimeDate,@d6) & ' ' & Format(local:ARCEndOfRepairTimeTime,@t1),'c:\tat.log')
        LinePrint('RRC Start Of Repair: ' & Format(local:RRCStartOfRepairTimeDate,@d6) & ' ' & Format(local:RRCStartOfRepairTimeTime,@t1),'c:\tat.log')
        LinePrint('RRC End Of Repair: ' & Format(local:RRCEndOfRepairTimeDate,@d6) & ' ' & Format(local:RRCEndOfRepairTimeTime,@t1),'c:\tat.log')
        BHReturnDaysHoursMins(tmp:ARCRealTotalTime, Days#, Hours#, Mins#)
        LinePRint('ARC Total Time Real (Before): ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RRCRealTotalTime, Days#, Hours#, Mins#)
        LinePrint('RRC Total Time Real (Before): ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:ARCCustomerPerspective, Days#, Hours#, Mins#)
        LinePRint('ARC Customer Perspective (Before): ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RRCCustomerPerspective, Days#, Hours#, Mins#)
        LinePRint('RRC Customer Perspective (Before): ' & clip(wob:HeadAccountNumber) & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RealTotalARCWaitTime, Days#, Hours#, Mins#)
        LinePRint('ARC Wait Time: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RealTotalRRCWaitTime, Days#, Hours#, Mins#)
        LinePRint('RRC Wait Time: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RealTotalRRCWAitTime + tmp:RealTotalARCWaitTime, Days#, Hours#, Mins#)
        LinePRint('Wait Time: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RealTotalARCCourierTime, Days#, Hours#, Mins#)
        LinePRint('ARC Courier Time: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RealTotalRRCCOurierTime, Days#, Hours#, Mins#)
        LinePRint('RRC Courier Time: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RealTotalRRCCOurierTime + tmp:RealTotalARCCOurierTime, Days#, Hours#, Mins#)
        LinePRint('Courier Time: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
    End ! If Command('/DEBUG')

! Changing (DBH 07/11/2006) # N/A - Should always take away the out of control time
!    If jobe:WebJob
!        tmp:RRCRealTotalTime -= tmp:RealTotalRRCOutOfControl
!    Else ! If jobe:WebJob
!        tmp:ARCRealTotalTime -= tmp:RealTotalARCOutOfControl
!    End ! If jobe:WebJob
! to (DBH 07/11/2006) # N/A
    ! Inserting (DBH 02/02/2007) # 8719 - Only count if the RRC has finished it's job
    If tmp:RRCJobFinished
    ! End (DBH 02/02/2007) #8719
! Changing (DBH 23/02/2007) # 8794 - Add the estimate "out of control" time
!        tmp:RRCRealTotalTime -= tmp:RealTotalRRCOutOfControl
! to (DBH 23/02/2007) # 8794
        tmp:RRCRealTotalTime -= (tmp:RealTotalRRCOutOfControl + tmp:RealTotalRRCEstimateTime)
! End (DBH 23/02/2007) #8794
    ! Inserting (DBH 02/02/2007) # 8719 - Don't count if the RRC hasn't finished
    Else
        tmp:RRCRealTOtalTime = 0
    ! End (DBH 02/02/2007) #8719
    End ! If tmp:RRCJobFinished
! Changing (DBH 23/02/2007) # 8794 - Add the estimate "out of control" time
!    tmp:ARCRealTotalTime -= tmp:RealTotalARCOutOfControl
! to (DBH 23/02/2007) # 8794
    tmp:ARCRealTotalTime -= (tmp:RealTotalARCOutOfControl + tmp:RealTotalARCEstimateTime)
! End (DBH 23/02/2007) #8794
! End (DBH 07/11/2006) #N/A

! Changing (DBH 23/02/2007) # 8794 - Only take away the estimate "out of control" time
!    tmp:ARCCustomerPerspective -= (tmp:CustomerTotalARCOutOfControl + tmp:CustomerTotalRRCOutOfControl)
!    ! Inserting (DBH 02/02/2007) # 8719 - Only count if the RRC has finished it's job
!    If tmp:RRCJobFinished
!    ! End (DBH 02/02/2007) #8719
!        tmp:RRCCustomerPerspective -= (tmp:CustomerTotalRRCOutOfControl + tmp:CustomerTotalARCOutOfControl)
!    End ! If tmp:RRCJobFinished
! to (DBH 23/02/2007) # 8794
    ! Change --- Count unlock time (DBH: 24/07/2009) #10944
    !Tmp:ARCCustomerPerspective -= (tmp:CustomerTotalARCEstimateTime + tmp:CustomerTotalRRCEstimateTime)
    ! To --- (DBH: 24/07/2009) #10944
    tmp:ARCCustomerPerspective -= (tmp:CustomerTotalARCEstimateTime + tmp:CustomerTotalRRCEstimateTime + tmp:CustomerTotalARCUnlockCodeTime + tmp:CustomerTotalRRCUnlockCodeTime)
    ! end --- (DBH: 24/07/2009) #10944

    ! Inserting (DBH 02/02/2007) # 8719 - Only count if the RRC has finished it's job
    If tmp:RRCJobFinished
    ! End (DBH 02/02/2007) #8719
        ! Change --- Count unlock time (DBH: 24/07/2009) #10944
        !Tmp:RRCCustomerPerspective -= (tmp:CustomerTotalRRCEstimateTime + tmp:CustomerTotalARCEstimateTime)
        ! To --- (DBH: 24/07/2009) #10944
        tmp:RRCCustomerPerspective -= (tmp:CustomerTotalRRCEstimateTime + tmp:CustomerTotalARCEstimateTime + tmp:CustomerTotalARCUnlockCodeTime + tmp:CustomerTotalRRCUnlockCodeTime)
        ! end --- (DBH: 24/07/2009) #10944
    End ! If tmp:RRCJobFinished
! End (DBH 23/02/2007) #8794

    If Command('/DEBUG')
        BHReturnDaysHoursMins(tmp:RealTotalARCOutOfControl, Days#, Hours#, Mins#)
        LinePrint('ARC Out Of Control Real: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RealTOtalRRCOutOfControl, Days#, Hours#, Mins#)
        LinePrint('RRC Out Of Control Real: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RealTotalARCEstimateTime, Days#, Hours#, Mins#)
        LinePrint('ARC Estimate Time Real: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RealTotalRRCEstimateTime, Days#, Hours#, Mins#)
        LinePrint('RRC Estimate Time Real: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:CustomerTotalARCUnlockCodeTime, Days#, Hours#, Mins#)
        LinePrint('ARC Unlock Time: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:CustomerTotalRRCUnlockCodeTime, Days#, Hours#, Mins#)
        LinePrint('RRC Unlock Time: ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')

        BHReturnDaysHoursMins(tmp:ARCRealTotalTime, Days#, Hours#, Mins#)
        LinePrint('ARC Total Time Real (After): ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RRCRealTotalTime, Days#, Hours#, Mins#)
        LinePrint('RRC Total Time Real (After): ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:ARCCustomerPerspective, Days#, Hours#, Mins#)
        LinePRint('ARC Customer Perspective (After): ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RRCCustomerPerspective, Days#, Hours#, Mins#)
        LinePRint('RRC Customer Perspective (After): ' & Days# & ' ' & Hours# & ':' & Mins#, 'c:\tat.log')

    End ! If Command('/DEBUG')
    ! End   - Detail shows the Real Time RCP - TrkBs: 5934 (DBH: 08-07-2005)

    !Customer Perspective - TrkBs: 4714 (DBH: 08-11-2004 61)
    local:TimeToDespatch = BHTimeDifference24Hr(local:StartDate,local:LastDespatchDate,local:StartTime,local:LastDespatchTime)
    If Command('/DEBUG')
        BHReturnDaysHoursMins(local:TimeToDespatch,Days#,Hours#,Mins#)
        LinePrint('Total Time Customer (Before): ' & Days# & ' ' & Hours# & ':' & Mins#,'c:\tat.log')
    End !If Command('/DEBUG')

! Changing (DBH 23/02/2007) # 8794 - Count the estimate "out of control" time
!    If jobe:WebJob
!        local:TimeToDespatch -= tmp:CustomerTotalRRCOutOfControl
!    Else ! If jobe:WebJOb
!        local:TimeToDespatch -= tmp:CustomerTotalARCOutOfControl
!    End ! If jobe:WebJOb
! to (DBH 23/02/2007) # 8794
! Change --- Don't include out of control for customer perspective, only estimate (DBH: 03/07/2009) #10912
!    If jobe:WebJob
!        local:TimeToDespatch -= (tmp:CustomerTotalRRCOutOfControl + tmp:CustomerTotalRRCEstimateTime)
!    Else ! If jobe:WebJOb
!        local:TimeToDespatch -= (tmp:CustomerTotalARCOutOfControl + tmp:CustomerTotalARCEstimateTime)
!    End ! If jobe:WebJOb
! To --- (DBH: 03/07/2009) #10912
    If jobe:WebJob
        local:TimeToDespatch -= tmp:CustomerTotalRRCEstimateTime
    Else ! If jobe:WebJOb
        local:TimeToDespatch -= tmp:CustomerTotalARCEstimateTime
    End ! If jobe:WebJOb
! end --- (DBH: 03/07/2009) #10912
! End (DBH 23/02/2007) #8794

    If Command('/DEBUG')
        BHReturnDaysHoursMins(tmp:CustomerTotalARCOutOfControl,Days#,Hours#,Mins#)
        LinePrint('ARC Out Of Control Customer: ' & Days# & ' ' & Hours# & ':' & Mins#,'c:\tat.log')
        BHReturnDaysHoursMins(tmp:CustomerTotalRRCOutOfControl,Days#,Hours#,Mins#)
        LinePrint('RRC Out Of Control Customer: ' & Days# & ' ' & Hours# & ':' & Mins#,'c:\tat.log')
        BHReturnDaysHoursMins(tmp:CustomerTotalARCEstimateTime,Days#,Hours#,Mins#)
        LinePrint('ARC Estimate Time Customer: ' & Days# & ' ' & Hours# & ':' & Mins#,'c:\tat.log')
        BHReturnDaysHoursMins(tmp:CustomerTotalRRCEstimateTime,Days#,Hours#,Mins#)
        LinePrint('RRC Estimate Time Customer: ' & Days# & ' ' & Hours# & ':' & Mins#,'c:\tat.log')

        BHReturnDaysHoursMins(local:TimeToDespatch,Days#,Hours#,Mins#)
        LinePrint('Total Time Customer (After): ' & Days# & ' ' & Hours# & ':' & Mins#,'c:\tat.log')
    End !If Command('/DEBUG')

    ! Start - Save the Customer Perspective time, to appear on the detail section - TrkBs: 5934 (DBH: 11-07-2005)
    tmp:CustomerTotalTime = local:TimeToDespatch
    ! End   - Save the Customer Perspective time, to appear on the detail section - TrkBs: 5934 (DBH: 11-07-2005)

    !Work out the time to attached the loan
    !based on the user's site who added the loan -  (DBH: 17-03-2004)
    If tmp:LoanUnitAttached = True
        tmp:LoanTime = local.TimeDifference(local:StartDate, |
                                                            tmp:LoanAttachedDate, |
                                                            local:StartTime, |
                                                            tmp:LoanAttachedTime, |
                                                            0)
    End !If tmp:LoanTime > 0

    !_____________________________________________________________________

    !Repair Centre Persepective
    If tmp:ARCAccount = 0
        ! Inserting (DBH 26/02/2007) # 8794 - Don't count "excluded" jobs
        If tmp:RRCJobFinished
        ! End (DBH 26/02/2007) #8794
            !Repair Centre Perspective (DBH: 18-03-2004)
            rep1:AccountNumber  = tra:Account_Number
            rep1:Type = Local.GetSummaryType(tmp:RRC1hrTotalTime)
            Get(RepairCentreQueue,rep1:AccountNumber,rep1:Type)
            If Error()
                rep1:AccountNumber = tra:Account_Number
                rep1:CompanyName    = tra:Company_Name
                rep1:Type   = Local.GetSummaryType(tmp:RRC1hrTotalTime)
                rep1:Count  = 1
                Add(RepairCentreQueue)
            Else !If Error()
                rep1:Count += 1
                Put(RepairCentreQueue)
            End !If Error()

            !Loan Count (DBH: 18-03-2004)
            If tmp:LoanUnitAttached = True
                loaque1:AccountNumber = tra:Account_Number
                loaque1:LoanType = Local.GetSummaryTypeLoan(tmp:LoanTime)
                Get(LoanCountQueue1,loaque1:AccountNumber,loaque1:LoanType)
                If Error()
                    loaque1:AccountNumber = tra:Account_Number
                    loaque1:LoanType        = Local.GetSummaryTypeLoan(tmp:LoanTime)
                    loaque1:LoanCount       = 1
                    Add(LoanCountQueue1)
                Else !If Error()
                    loaque1:LoanCount += 1
                    Put(LoanCountQueue1)
                End !If tmp:LoanTime > 0
            End !If Error()

        End ! If tmp:RRCJobFinished


        !If Job has been sent to ARC, copy the results into the ARC list (DBH: 18-03-2004)
        If SentToHub(job:Ref_Number)
            ! Inserting (DBH 02/10/2006) # N/A - Only show ARC tab if the ARC is ticked in the criteria screen
            ARCTicked# = 0
            If tmp:AllAccounts = 1
                ARCTicked# = 1
            Else ! If tmp:AllAccounts = 1
                glo:Pointer = tmp:HeadAccountNumber
                Get(glo:Queue,glo:Pointer)
                If ~Error()
                    ARCTicked# = 1
                End ! If ~Error()
            End ! If tmp:AllAccounts = 1
            If ARCTicked# = 1
                rep1:AccountNumber  = tmp:HeadAccountNumber
                rep1:Type = Local.GetSummaryType(tmp:ARC1hrTotalTime)
                Get(RepairCentreQueue,rep1:AccountNumber,rep1:Type)
                If Error()
                    rep1:AccountNumber = tmp:HeadAccountNumber
                    rep1:CompanyName    = tmp:ARCCompanyName
                    rep1:Type   = Local.GetSummaryType(tmp:ARC1hrTotalTime)
                    rep1:Count  = 1
                    Add(RepairCentreQueue)
                Else !If Error()
                    rep1:Count += 1
                    Put(RepairCentreQueue)
                End !If Error()
                !Loan Count (DBH: 18-03-2004)
                If tmp:LoanTime > 0
                    loaque1:AccountNumber = tmp:HeadAccountNumber
                    loaque1:LoanType = Local.GetSummaryTypeLoan(tmp:LoanTime)
                    Get(LoanCountQueue1,loaque1:AccountNumber,loaque1:LoanType)
                    If Error()
                        loaque1:AccountNumber = tmp:HeadAccountNumber
                        loaque1:LoanType        = Local.GetSummaryTypeLoan(tmp:LoanTime)
                        loaque1:LoanCount       = 1
                        Add(LoanCountQueue1)
                    Else !If Error()
                        loaque1:LoanCount += 1
                        Put(LoanCountQueue1)
                    End !If Error()
                End !If tmp:LoanTime > 0
            End ! If ARCTicked# = 1
            ! End (DBH 02/10/2006) #N/A
        End !If SentToHub(job:Ref_Number)
    Else
        !Repair Centre Perspective (DBH: 18-03-2004)
        rep1:AccountNumber  = tmp:HeadAccountNumber
        rep1:Type = Local.GetSummaryType(tmp:ARC1hrTotalTime)
        Get(RepairCentreQueue,rep1:AccountNumber,rep1:Type)
        If Error()
            rep1:AccountNumber = tmp:HeadAccountNumber
            rep1:CompanyName    = tmp:ARCCompanyName
            rep1:Type   = Local.GetSummaryType(tmp:ARC1hrTotalTime)
            rep1:Count  = 1
            Add(RepairCentreQueue)
        Else !If Error()
            rep1:Count += 1
            Put(RepairCentreQueue)
        End !If Error()
        !Loan Count (DBH: 18-03-2004)
        If tmp:LoanUnitAttached = True
            loaque1:AccountNumber = tmp:HeadAccountNumber
            loaque1:LoanType = Local.GetSummaryTypeLoan(tmp:LoanTime)
            Get(LoanCountQueue1,loaque1:AccountNumber,loaque1:LoanType)
            If Error()
                loaque1:AccountNumber = tmp:HeadAccountNumber
                loaque1:LoanType        = Local.GetSummaryTypeLoan(tmp:LoanTime)
                loaque1:LoanCount       = 1
                Add(LoanCountQueue1)
            Else !If Error()
                loaque1:LoanCount += 1
                Put(LoanCountQueue1)
            End !If Error()
        End !If tmp:LoanTime > 0
    End !If tmp:ARCAccount = 0

    local:TimeToComplete = BHTimeDifference24Hr(local:StartDate,tmp:LastCompletedDate,local:StartTime,tmp:LastCompletedTime)

    !_____________________________________________________________________

    !Real Time Repair Center Perspective

    !Work out the time to attached the loan
    !based on the user's site who added the loan -  (DBH: 17-03-2004)
    If tmp:LoanUnitAttached = True
        tmp:LoanTime = local.RealTimeDifference(local:StartDate, |
                                                            tmp:LoanAttachedDate, |
                                                            local:StartTime, |
                                                            tmp:LoanAttachedTime, |
                                                            0)
    End !If tmp:LoanTime > 0


    If tmp:ARCAccount = 0
        ! Inserting (DBH 26/02/2007) # 8794 - Don't count "excluded" jobs
        If tmp:RRCJobFinished
        ! End (DBH 26/02/2007) #8794
            !Real Time Repair Centre Perspective (DBH: 18-03-2004)
            rep2:AccountNumber  = tra:Account_Number
            rep2:Type = Local.GetSummaryType(tmp:RealRRC1hrTotalTime)
            Get(RepairCentreRealQueue,rep2:AccountNumber,rep2:Type)
            If Error()
                rep2:AccountNumber = tra:Account_Number
                rep2:CompanyName    = tra:Company_Name
                rep2:Type   = Local.GetSummaryType(tmp:RealRRC1hrTotalTime)
                rep2:Count  = 1
                Add(RepairCentreRealQueue)
            Else !If Error()
                rep2:Count += 1
                Put(RepairCentreRealQueue)
            End !If Error()

            !Loan Count (DBH: 18-03-2004)
            If tmp:LoanUnitAttached = True
                loaque2:AccountNumber       = tra:Account_Number
                loaque2:LoanType            = Local.GetSummaryTypeLoan(tmp:LoanTime)
                Get(LoanCountQueue2,loaque2:AccountNumber,loaque2:LoanType)
                If Error()
                    loaque2:AccountNumber = tra:Account_Number
                    loaque2:LoanType        = Local.GetSummaryTypeLoan(tmp:LoanTime)
                    loaque2:LoanCount       = 1
                    Add(LoanCountQueue2)
                Else !If Error()
                    loaque2:LoanCount       += 1
                    Put(LoanCountQueue2)
                End !If Error()
            End !If tmp:LoanTime > 0
        End ! If tmp:RRCJobFinished

        If SentToHub(job:Ref_Number)
            ! Inserting (DBH 02/10/2006) # N/A - Only show ARC tab if the ARC is ticked in the criteria screen
            ARCTicked# = 0
            If tmp:AllAccounts = 1
                ARCTicked# = 1
            Else ! If tmp:AllAccounts = 1
                glo:Pointer = tmp:HeadAccountNumber
                Get(glo:Queue,glo:Pointer)
                If ~Error()
                    ARCTicked# = 1
                End ! If ~Error()
            End ! If tmp:AllAccounts = 1
            If ARCTicked# = 1
                rep2:AccountNumber  = tmp:HeadAccountNumber
                rep2:Type = Local.GetSummaryType(tmp:RealARC1hrTotalTime)
                Get(RepairCentreRealQueue,rep2:AccountNumber,rep2:Type)
                If Error()
                    rep2:AccountNumber = tmp:HeadAccountNumber
                    rep2:CompanyName = tmp:ARCCompanyName
                    rep2:Type   = Local.GetSummaryType(tmp:RealARC1hrTotalTime)
                    rep2:Count  = 1
                    Add(RepairCentreRealQueue)
                Else !If Error()
                    rep2:Count += 1
                    Put(RepairCentreRealQueue)
                End !If Error()
                !Loan Count (DBH: 18-03-2004)
                If tmp:LoanUnitAttached = True
                    loaque2:AccountNumber       = tmp:HeadAccountNumber
                    loaque2:LoanType            = Local.GetSummaryTypeLoan(tmp:LoanTime)
                    Get(LoanCountQueue2,loaque2:AccountNumber,loaque2:LoanType)
                    If Error()
                        loaque2:AccountNumber = tmp:HeadAccountNumber
                        loaque2:LoanType        = Local.GetSummaryTypeLoan(tmp:LoanTime)
                        loaque2:LoanCount       = 1
                        Add(LoanCountQueue2)
                    Else !If Error()
                        loaque2:LoanCount       += 1
                        Put(LoanCountQueue2)
                    End !If Error()
                End !If tmp:LoanTime > 0
            End ! If ARCTicked# = 1
        End !If SentToHub(job:Ref_Number)
    Else ! If tmp:ARCAccount = 0
        rep2:AccountNumber  = tmp:HeadAccountNumber
        rep2:Type = Local.GetSummaryType(tmp:RealARC1hrTotalTime)
        Get(RepairCentreRealQueue,rep2:AccountNumber,rep2:Type)
        If Error()
            rep2:AccountNumber = tmp:HeadAccountNumber
            rep2:CompanyName = tmp:ARCCompanyName
            rep2:Type   = Local.GetSummaryType(tmp:RealARC1hrTotalTime)
            rep2:Count  = 1
            Add(RepairCentreRealQueue)
        Else !If Error()
            rep2:Count += 1
            Put(RepairCentreRealQueue)
        End !If Error()
        !Loan Count (DBH: 18-03-2004)
        If tmp:LoanUnitAttached = True
            loaque2:AccountNumber       = tmp:HeadAccountNumber
            loaque2:LoanType            = Local.GetSummaryTypeLoan(tmp:LoanTime)
            Get(LoanCountQueue2,loaque2:AccountNumber,loaque2:LoanType)
            If Error()
                loaque2:AccountNumber = tmp:HeadAccountNumber
                loaque2:LoanType        = Local.GetSummaryTypeLoan(tmp:LoanTime)
                loaque2:LoanCount       = 1
                Add(LoanCountQueue2)
            Else !If Error()
                loaque2:LoanCount       += 1
                Put(LoanCountQueue2)
            End !If Error()
        End !If tmp:LoanTime > 0

    End ! If tmp:ARCAccount = 0

    !_____________________________________________________________________

    !Customer Perspective

    !Work out the time to attached the loan
    !based on the user's site who added the loan -  (DBH: 17-03-2004)
    If tmp:LoanUnitAttached = True
        tmp:LoanTime = BHTimeDifference24Hr(local:StartDate, |
                                                            tmp:LoanAttachedDate, |
                                                            local:StartTime, |
                                                            tmp:LoanAttachedTime)
    End !If tmp:LoanTime > 0


    If tmp:ARCAccount = 0
        ! Inserting (DBH 26/02/2007) # 8794 - Don't count "excluded" jobs
        If tmp:RRCJobFinished
        ! End (DBH 26/02/2007) #8794
            cus:AccountNumber  = tra:Account_Number
            cus:Type = Local.GetSummaryType(local:TimeToDespatch)
            Get(CustomerPerspectiveQueue,cus:AccountNumber,cus:Type)
            If Error()
                cus:AccountNumber = tra:Account_Number
                cus:CompanyName = tra:Company_Name
                cus:Type   = Local.GetSummaryType(local:TimeToDespatch)
                cus:Count  = 1
                Add(CustomerPerspectiveQueue)
            Else !If Error()
                cus:Count += 1
                Put(CustomerPerspectiveQueue)
            End !If Error()

            !Loan Count (DBH: 18-03-2004)
            If tmp:LoanUnitAttached = True
                loaque3:AccountNumber = tra:Account_Number
                loaque3:LoanType = Local.GetSummaryTypeLoan(tmp:LoanTime)
                Get(LoanCountQueue3,loaque3:AccountNumber,loaque3:LoanType)
                If Error()
                    loaque3:AccountNumber = tra:Account_Number
                    loaque3:LoanType        = Local.GetSummaryTypeLoan(tmp:LoanTime)
                    loaque3:LoanCount       = 1
                    Add(LoanCountQueue1)
                Else !If Error()
                    loaque3:LoanCount += 1
                    Put(LoanCountQueue3)
                End !If tmp:LoanTime > 0
            End !If Error()
        End ! If tmp:RRCJobFinished

        If SentToHub(job:Ref_Number)
            ! Inserting (DBH 02/10/2006) # N/A - Only show ARC tab if the ARC is ticked in the criteria screen
            ARCTicked# = 0
            If tmp:AllAccounts = 1
                ARCTicked# = 1
            Else ! If tmp:AllAccounts = 1
                glo:Pointer = tmp:HeadAccountNumber
                Get(glo:Queue,glo:Pointer)
                If ~Error()
                    ARCTicked# = 1
                End ! If ~Error()
            End ! If tmp:AllAccounts = 1
            If ARCTicked# = 1
                cus:AccountNumber  = tmp:HeadAccountNumber
                cus:Type = Local.GetSummaryType(local:TimeToDespatch)
                Get(CustomerPerspectiveQueue,cus:AccountNumber,cus:Type)
                If Error()
                    cus:AccountNumber = tmp:HeadAccountNumber
                    cus:CompanyName  = tmp:ARCCompanyName
                    cus:Type   = Local.GetSummaryType(local:TimeToDespatch)
                    cus:Count  = 1
                    Add(CustomerPerspectiveQueue)
                Else !If Error()
                    cus:Count += 1
                    Put(CustomerPerspectiveQueue)
                End !If Error()
                If tmp:LoanUnitAttached = True
                    loaque3:AccountNumber = tmp:HeadAccountNumber
                    loaque3:LoanType = Local.GetSummaryTypeLoan(tmp:LoanTime)
                    Get(LoanCountQueue3,loaque3:AccountNumber,loaque3:LoanType)
                    If Error()
                        loaque3:AccountNumber = tmp:HeadAccountNumber
                        loaque3:LoanType        = Local.GetSummaryTypeLoan(tmp:LoanTime)
                        loaque3:LoanCount       = 1
                        Add(LoanCountQueue1)
                    Else !If Error()
                        loaque3:LoanCount += 1
                        Put(LoanCountQueue3)
                    End !If tmp:LoanTime > 0
                End !If Error()
            End ! If ARCTicked# = 1
        End !If SentToHub(job:Ref_Number)

    Else ! If tmp:ARCAccount = 0
        cus:AccountNumber  = tmp:HeadAccountNumber
        cus:Type = Local.GetSummaryType(local:TimeToDespatch)
        Get(CustomerPerspectiveQueue,cus:AccountNumber,cus:Type)
        If Error()
            cus:AccountNumber = tmp:HeadAccountNumber
            cus:CompanyName  = tmp:ARCCompanyName
            cus:Type   = Local.GetSummaryType(local:TimeToDespatch)
            cus:Count  = 1
            Add(CustomerPerspectiveQueue)
        Else !If Error()
            cus:Count += 1
            Put(CustomerPerspectiveQueue)
        End !If Error()
        If tmp:LoanUnitAttached = True
            loaque3:AccountNumber = tmp:HeadAccountNumber
            loaque3:LoanType = Local.GetSummaryTypeLoan(tmp:LoanTime)
            Get(LoanCountQueue3,loaque3:AccountNumber,loaque3:LoanType)
            If Error()
                loaque3:AccountNumber = tmp:HeadAccountNumber
                loaque3:LoanType        = Local.GetSummaryTypeLoan(tmp:LoanTime)
                loaque3:LoanCount       = 1
                Add(LoanCountQueue1)
            Else !If Error()
                loaque3:LoanCount += 1
                Put(LoanCountQueue3)
            End !If tmp:LoanTime > 0
        End !If Error()

    End ! If tmp:ARCAccount = 0

    !_____________________________________________________________________

    If Command('/DEBUG')
        BHReturnDaysHoursMins(tmp:ARC1hrTotalTime,Days#,Hours#,Mins#)
        LinePrint('ARC Repair Centre Perspective: ' & Days# & ' ' & Hours# & ':' & Mins#,'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RealARC1hrTotalTime,Days#,Hours#,Mins#)
        LinePrint('ARC Real TimeRepair Centre Perspective: ' & Days# & ' ' & Hours# & ':' & Mins#,'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RRC1hrTotalTime,Days#,Hours#,Mins#)
        LinePrint('RRC Repair Centre Perspective: ' & Days# & ' ' & Hours# & ':' & Mins#,'c:\tat.log')
        BHReturnDaysHoursMins(tmp:RealRRC1hrTotalTime,Days#,Hours#,Mins#)
        LinePrint('RRC Real TimeRepair Centre Perspective: ' & Days# & ' ' & Hours# & ':' & Mins#,'c:\tat.log')
        BHReturnDaysHoursMins(tmp:CustomerTotalTime,Days#,Hours#,Mins#)
        LinePrint('ARC Repair Centre Perspective: ' & Days# & ' ' & Hours# & ':' & Mins#,'c:\tat.log')
    End ! If Command('/DEBUG')
ResetARCCounters           Routine
Data
local:DespatchingCourier        String(30)
Code
    If tmp:ARCWaitTime = True
        tmp:ARCWaitTime = False
        tmp:TotalARCWaitTime += local.TimeDifference(tmp:ARCWaitTimeStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:ARCWaitTimeStartTime, |
                                                    aus:TimeChanged, |
                                                    local.ARCUser(aus:UserCode))
        !Real Time RRC - 4087 (DBH: 14-07-2004)
        tmp:RealTotalARCWaitTime += local.RealTimeDifference(tmp:ARCWaitTimeStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:ARCWaitTimeStartTime, |
                                                    aus:TimeChanged, |
                                                    local.ARCUser(aus:UserCode))

        If Command('/DEBUG')
            LinePrint('ARC Wait Time Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')

    End !tmp:WaitTime = True

    If tmp:ARCCourierTime = True
        tmp:ARCCourierTime = False

        !Default the courier to the ARC (DBH: 19-03-2004)
        local:DespatchingCourier = 1
        !Look for entry in consignment history, for the date
        !of despatch. This should give the courier -  (DBH: 19-03-2004)
        Save_joc_ID = Access:JOBSCONS.SaveFile()
        Access:JOBSCONS.ClearKey(joc:DateKey)
        joc:RefNumber = job:Ref_number
        joc:TheDate   = tmp:ARCCourierTimeStartDate
        joc:TheTime  = 0
        Set(joc:DateKey,joc:DateKey)
        Loop
            If Access:JOBSCONS.NEXT()
               Break
            End !If
            If joc:RefNumber <> job:Ref_Number      |
            Or joc:TheDate   <> tmp:ARCCourierTimeStartDate      |
                Then Break.  ! End If
            Hrs# = Sub(Format(tmp:ARCCourierTimeStartTime,@t02),1,2)
            Mins# = Sub(Format(tmp:ARCCourierTimeStartTime,@t02),3,2)

            If Sub(Format(joc:TheTime,@t02),1,2) = Hrs#  And |
                Sub(Format(joc:TheTime,@t02),3,2) = Mins#
                !Confirm the entry is a REAL courier (DBH: 19-03-2004)
                Access:COURIER.ClearKey(cou:Courier_Key)
                cou:Courier = joc:Courier
                If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Found
                    local:DespatchingCourier = cou:Courier
                    Break
                Else !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Error
                End !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
            End !Sub(Format(aus:TimeChanged,@t02),3,2) = Mins#
        End !Loop
        Access:JOBSCONS.RestoreFile(Save_joc_ID)

        tmp:TotalARCCourierTime += local.TimeDifference(tmp:ARCCourierTimeStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:ARCCourierTimeStartTime, |
                                                    aus:TimeChanged, |
                                                    local:DespatchingCourier)
        !Real Time RRC - 4087 (DBH: 14-07-2004)

        ! Inserting (DBH 17/10/2006) # 8367 - Courer time based on the courier hours
        tmp:RealTotalARCCourierTime += local.RealTimeDifferenceCourier(tmp:ARCCourierTimeStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:ARCCourierTimeStartTime, |
                                                    aus:TimeChanged, |
                                                    local:DespatchingCourier)
        ! End (DBH 17/10/2006) #8367

        If Command('/DEBUG')
            LinePrint('ARC Courier Time Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
            LinePrint('ARC Courier Time Difference:  '& local.TimeDifference(tmp:ARCCourierTimeStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:ARCCourierTimeStartTime, |
                                                    aus:TimeChanged, |
                                                    local:DespatchingCourier) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
            LinePrint('ARC Courier Type: ' & Clip(local:DespatchingCourier) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')

    End !tmp:CourierTime = True

    If tmp:ARCInControl = True
        tmp:ARCInControl = False
        tmp:TotalARCInControl += local.TimeDifference(tmp:ARCInControlStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:ARCInControlStartTime, |
                                                        aus:TimeChanged, |
                                                        1)
        !Real Time RRC - 4087 (DBH: 14-07-2004)
        tmp:RealTotalARCInControl += local.RealTimeDifference(tmp:ARCInControlStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:ARCInControlStartTime, |
                                                        aus:TimeChanged, |
                                                        1)
        !Customer RRC In Control (For Summary)
        tmp:CustARC1HrTotalTime += BHTimeDifference24Hr(tmp:ARCInControlStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:ARCInControlStartTime, |
                                                        aus:TimeChanged)
        If Command('/DEBUG')
            LinePrint('ARC In Control Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')

    End !tmp:ARCInControl = True



    If tmp:ARCOutOfControl = True
        tmp:ARCOutOfControl = False
        tmp:TotalARCOutOfControl += local.TimeDifference(tmp:ARCOutOfControlStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:ARCOutOfCOntrolStartTime, |
                                                        aus:TimeChanged, |
                                                        1)
        !Real Time RRC - 4087 (DBH: 14-07-2004)
        tmp:RealTotalARCOutOfControl += local.RealTimeDifference(tmp:ARCOutOfControlStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:ARCOutOfCOntrolStartTime, |
                                                        aus:TimeChanged, |
                                                        1)

        !Customer Perspective Out Of Control - TrkBs: 4714 (DBH: 08-11-2004 61)
! Change --- Use new customer perspective count (DBH: 26/08/2009) #11038
!        tmp:CustomerTotalARCOutOfControl += BHTimeDifference24Hr(tmp:ARCOutOfControlStartDate, |
!                                                        aus:DateChanged, |
!                                                        tmp:ARCOutOfControlStartTime, |
!                                                        aus:TimeChanged)
! To --- (DBH: 26/08/2009) #11038
        tmp:CustomerTotalARCOutOfControl += local.CustomerPerspective(tmp:ARCOutOfControlStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:ARCOutOfControlStartTime, |
                                                        aus:TimeChanged,1)
! end --- (DBH: 26/08/2009) #11038

        If Command('/DEBUG')
            LinePrint('ARC Out Of Control Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')

    End !If tmp:ARCOutOfControl = True

    ! Inserting (DBH 23/02/2007) # 8794 - Count the Estimate "out of control" seperately
    If tmp:ARCEstimateTime = True
        tmp:ARCEstimateTIme = False
        tmp:TotalARCEstimateTime += local.TimeDifference(tmp:ARCEstimateTimeStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:ARCEstimateTimeStartTime, |
                                                        aus:TimeChanged, |
                                                        1)
        !Real Time RRC - 4087 (DBH: 14-07-2004)
        tmp:RealTotalARCEstimateTime += local.RealTimeDifference(tmp:ARCEstimateTimeStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:ARCEstimateTimeStartTime, |
                                                        aus:TimeChanged, |
                                                        1)

        !Customer Perspective Out Of Control - TrkBs: 4714 (DBH: 08-11-2004 61)
! Change --- Use new customer perspective count (DBH: 26/08/2009) #11046
!        tmp:CustomerTotalARCEstimateTime += BHTimeDifference24Hr(tmp:ARCEstimateTimeStartDate, |
!                                                        aus:DateChanged, |
!                                                        tmp:ARCEstimateTimeStartTime, |
!                                                        aus:TimeChanged)
!
! To --- (DBH: 26/08/2009) #11046
        tmp:CustomerTotalARCEstimateTime += local.CustomerPerspective(tmp:ARCEstimateTimeStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:ARCEstimateTimeStartTime, |
                                                        aus:TimeChanged,1)

! end --- (DBH: 26/08/2009) #11046
        If Command('/DEBUG')
            LinePrint('ARC Estimate Time Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')

    End ! If tmp:ARCEstimateTime = True
    ! End (DBH 23/02/2007) #8794

     ! Insert --- Count Unlock Code Time (DBH: 23/07/2009) #10944
    if (tmp:ARCUnlockCodeTime = 1)
        tmp:ARCUnlockCodeTime = 0
        tmp:CustomerTotalARCUnlockCodeTime += local.CustomerPerspective(tmp:ARCUnlockCodeStartDate, |
                                                            aus:DateChanged, |
                                                            tmp:ARCUnlockCodeStartTime, |
                                                            aus:TimeChanged,1)
        If Command('/DEBUG')
            LinePrint('ARC Unlock Code Time Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')
    end ! if (tmp:UnlockCodeTime = 1)
    ! end --- (DBH: 23/07/2009) #10944
ResetRRCCounters           Routine
Data
local:DespatchingCourier        String(30)
Code
    If tmp:RRCInControl = True
        tmp:RRCInControl = False
        tmp:TotalRRCInControl += local.TimeDifference(tmp:RRCInControlStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:RRCInControlStartTime, |
                                                        aus:TimeChanged, |
                                                        0)
        !Real Time RRC - 4087 (DBH: 14-07-2004)
        tmp:RealTotalRRCInControl += local.RealTimeDifference(tmp:RRCInControlStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:RRCInControlStartTime, |
                                                        aus:TimeChanged, |
                                                        0)
        !Customer RRC In Control (For Summary)
        tmp:CustRRC1HrTotalTime += BHTimeDifference24Hr(tmp:RRCInControlStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:RRCInControlStartTime, |
                                                        aus:TimeChanged)
        If Command('/DEBUG')
            LinePrint('RRC In Control Start: ' & Format(tmp:RRCInControlSTartDate,@d6b) & ' ' & Format(tmp:RRCInControlSTartTime,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
            LinePrint('RRC In Control Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')

            LinePrint('RRC Real Time: ' & local.RealTimeDifference(tmp:RRCInControlStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:RRCInControlStartTime, |
                                                        aus:TimeChanged, |
                                                        0),'c:\tat.log')
        End !If Command('/DEBUG')
    End !tmp:RRCInControl = True

    If tmp:RRCWaitTime = True
        tmp:RRCWaitTime = False
        tmp:TotalRRCWaitTime += local.TimeDifference(tmp:RRCWaitTimeStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:RRCWaitTimeStartTime, |
                                                    aus:TimeChanged, |
                                                    local.ARCUser(aus:UserCode))
        !Real Time RRC - 4087 (DBH: 14-07-2004)
        tmp:RealTotalRRCWaitTime += local.RealTimeDifference(tmp:RRCWaitTimeStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:RRCWaitTimeStartTime, |
                                                    aus:TimeChanged, |
                                                    local.ARCUser(aus:UserCode))

        If Command('/DEBUG')
            LinePrint('RRC Wait Time Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')
    End !tmp:WaitTime = True

    If tmp:RRCCourierTime = True
        tmp:RRCCourierTime = False

        !Default the courier to the ARC (DBH: 19-03-2004)
        local:DespatchingCourier = 1
        !Look for entry in consignment history, for the date
        !of despatch. This should give the courier -  (DBH: 19-03-2004)
        Save_joc_ID = Access:JOBSCONS.SaveFile()
        Access:JOBSCONS.ClearKey(joc:DateKey)
        joc:RefNumber = job:Ref_number
        joc:TheDate   = tmp:RRCCourierTimeStartDate
        joc:TheTime  = 0
        Set(joc:DateKey,joc:DateKey)
        Loop
            If Access:JOBSCONS.NEXT()
               Break
            End !If
            If joc:RefNumber <> job:Ref_Number      |
            Or joc:TheDate   <> tmp:RRCCourierTimeStartDate      |
                Then Break.  ! End If
            Hrs# = Sub(Format(tmp:RRCCourierTimeStartTime,@t02),1,2)
            Mins# = Sub(Format(tmp:RRCCourierTimeStartTime,@t02),3,2)

            If Sub(Format(joc:TheTime,@t02),1,2) = Hrs#  And |
                Sub(Format(joc:TheTime,@t02),3,2) = Mins#
                !Confirm the entry is a REAL courier (DBH: 19-03-2004)
                Access:COURIER.ClearKey(cou:Courier_Key)
                cou:Courier = joc:Courier
                If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Found
                    local:DespatchingCourier = cou:Courier
                    Break
                Else !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Error
                End !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
            End !Sub(Format(aus:TimeChanged,@t02),3,2) = Mins#
        End !Loop
        Access:JOBSCONS.RestoreFile(Save_joc_ID)

        tmp:TotalRRCCourierTime += local.TimeDifference(tmp:RRCCourierTimeStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:RRCCourierTimeStartTime, |
                                                    aus:TimeChanged, |
                                                    local:DespatchingCourier)
        !Real Time RRC - 4087 (DBH: 14-07-2004)
        ! Inserting (DBH 17/10/2006) # 8367 - Courier time based on courier hours
        tmp:RealTotalRRCCourierTime += local.RealTimeDifferenceCourier(tmp:RRCCourierTimeStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:RRCCourierTimeStartTime, |
                                                    aus:TimeChanged, |
                                                    local:DespatchingCourier)
                                                    ! End (DBH 17/10/2006) #8367

        If Command('/DEBUG')
            LinePrint('RRC Courier Time Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
            LinePrint('RRC Courier Time Difference: ' & local.TimeDifference(tmp:RRCCourierTimeStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:RRCCourierTimeStartTime, |
                                                    aus:TimeChanged, |
                                                    local:DespatchingCourier) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
            LinePrint('RRC Courier Type: ' & Clip(local:DespatchingCourier) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')

    End !tmp:CourierTime = True

    If tmp:RRCOutOfCOntrol = True
        tmp:RRCOutOfControl = False
        tmp:TotalRRCOutOfControl += local.TimeDifference(tmp:RRCOutOfControlStartDate, |
                                                            aus:DateChanged, |
                                                            tmp:RRCOutOfControlStartTime, |
                                                            aus:TimeChanged, |
                                                            0)
        !Real Time RRC - 4087 (DBH: 14-07-2004)
        tmp:RealTotalRRCOutOfControl += local.RealTimeDifference(tmp:RRCOutOfControlStartDate, |
                                                            aus:DateChanged, |
                                                            tmp:RRCOutOfControlStartTime, |
                                                            aus:TimeChanged, |
                                                            0)
        !Customer Perspective Out Of Control - TrkBs: 4714 (DBH: 08-11-2004 61)
! Change --- Use new customer perspective count (DBH: 26/08/2009) #11038
!        tmp:CustomerTotalRRCOutOfControl += BHTimeDifference24Hr(tmp:RRCOutOfControlStartDate, |
!                                                        aus:DateChanged, |
!                                                        tmp:RRCOutOfControlStartTime, |
!                                                        aus:TimeChanged)
! To --- (DBH: 26/08/2009) #11038
        tmp:CustomerTotalRRCOutOfControl += local.CustomerPerspective(tmp:RRCOutOfControlStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:RRCOutOfControlStartTime, |
                                                        aus:TimeChanged,0)
! end --- (DBH: 26/08/2009) #11038

        If Command('/DEBUG')
            LinePrint('RRC Out Of Control Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')

    End !If tmp:RRCOutOfCOntrol = True

    ! Inserting (DBH 23/02/2007) # 8794 - Count the Estimate "out of control" seperately
    If tmp:RRCEstimateTime = True
        tmp:RRCEstimateTIme = False
        tmp:TotalRRCEstimateTime += local.TimeDifference(tmp:RRCEstimateTimeStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:RRCEstimateTimeStartTime, |
                                                        aus:TimeChanged, |
                                                        1)
        !Real Time RRC - 4087 (DBH: 14-07-2004)
        tmp:RealTotalRRCEstimateTime += local.RealTimeDifference(tmp:RRCEstimateTimeStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:RRCEstimateTimeStartTime, |
                                                        aus:TimeChanged, |
                                                        1)

        !Customer Perspective Out Of Control - TrkBs: 4714 (DBH: 08-11-2004 61)
! Change --- Use new customer perspective count (DBH: 26/08/2009) #11038
!        tmp:CustomerTotalRRCEstimateTime += BHTimeDifference24Hr(tmp:RRCEstimateTimeStartDate, |
!                                                        aus:DateChanged, |
!                                                        tmp:RRCEstimateTimeStartTime, |
!                                                        aus:TimeChanged)
! To --- (DBH: 26/08/2009) #11038
        tmp:CustomerTotalRRCEstimateTime += local.CustomerPerspective(tmp:RRCEstimateTimeStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:RRCEstimateTimeStartTime, |
                                                        aus:TimeChanged,0)
! end --- (DBH: 26/08/2009) #11038

        If Command('/DEBUG')
            LinePrint('RRC Estimate Time Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')

    End ! If tmp:ARCEstimateTime = True
    ! End (DBH 23/02/2007) #8794

    ! Insert --- Count Unlock Code Time (DBH: 23/07/2009) #10944
    if (tmp:RRCUnlockCodeTime = 1)
        tmp:RRCUnlockCodeTime = 0
        tmp:CustomerTotalRRCUnlockCodeTime += local.CustomerPerspective(tmp:RRCUnlockCodeStartDate, |
                                                            aus:DateChanged, |
                                                            tmp:RRCUnlockCodeStartTime, |
                                                            aus:TimeChanged,0)
        If Command('/DEBUG')
            LinePrint('RRC Unlock Code Time Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')
    end ! if (tmp:UnlockCodeTime = 1)
    ! end --- (DBH: 23/07/2009) #10944
ResetThirdPartyCounters         Routine

    If tmp:3rdParty = True
        tmp:3rdParty = False
        tmp:Total3rdParty += local.TimeDifference(tmp:3rdPartyStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:3rdPartyStartTime, |
                                                    aus:TimeChanged, |
                                                    local.ARCUser(aus:UserCode))
        !Real Time RRC - 4087 (DBH: 14-07-2004)
        tmp:RealTotal3rdParty += local.RealTimeDifference(tmp:3rdPartyStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:3rdPartyStartTime, |
                                                    aus:TimeChanged, |
                                                    local.ARCUser(aus:UserCode))
        If Command('/DEBUG')
            LinePrint('3rd Party Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')

    End !tmp:3rdParty = True

ResetCustomerCollectionCounters     Routine

    If tmp:CustomerCollection = True
        tmp:CustomerCollection = False
        tmp:TotalCustomerCollection += local.TimeDifference(tmp:CustomerCollectionStartDate,|
                                                            aus:DateChanged, |
                                                            tmp:CustomerCollectionStartTime, |
                                                            aus:TimeChanged, |
                                                            local.ARCUser(aus:UserCode))
        !Real Time RRC - 4087 (DBH: 14-07-2004)
! Changing (DBH 18/10/2006) # 8367 - Use 24 hours for customer collection time
!        tmp:RealTotalCustomerCollection += local.RealTimeDifference(tmp:CustomerCollectionStartDate,|
!                                                            aus:DateChanged, |
!                                                            tmp:CustomerCollectionStartTime, |
!                                                            aus:TimeChanged, |
!                                                            local.ARCUser(aus:UserCode))
! to (DBH 18/10/2006) # 8367
        tmp:RealTotalCustomerCollection += BHTimeDifference24Hr(tmp:CustomerCollectionStartDate,|
                                                            aus:DateChanged, |
                                                            tmp:CustomerCollectionStartTime, |
                                                            aus:TimeChanged)

! End (DBH 18/10/2006) #8367
        If Command('/DEBUG')
            LinePrint('Customer Collection Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')

    End !tmp:CustomerCollection = True

ResetReadyToDespatchCOunters        Routine

    If tmp:ReadyToDespatch = True
        tmp:ReadyToDespatch = False
        tmp:TotalReadyToDespatch += local.TimeDifference(tmp:ReadyToDespatchStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:ReadyToDespatchStartTime, |
                                                        aus:TimeChanged, |
                                                        local.ARCUser(aus:UserCode))
        !Real Time RRC - 4087 (DBH: 14-07-2004)
        tmp:RealTotalReadyToDespatch += local.RealTimeDifference(tmp:ReadyToDespatchStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:ReadyToDespatchStartTime, |
                                                        aus:TimeChanged, |
                                                        local.ARCUser(aus:UserCode))
        If Command('/DEBUG')
            LinePrint('Ready To Despatch Stop: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
        End !If Command('/DEBUG')

    End !tmp:ReadyToDespatch = True

StartRRCInControl        Routine

    tmp:RRCInControl = True
    tmp:RRCInControlStartDate = aus:DateChanged
    tmp:RRCInControlStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('RRC In Control Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartARCWaitTime           Routine
    tmp:ARCWaitTime = True
    tmp:ARCWaitTimeStartDate = aus:DateChanged
    tmp:ARCWaitTimeStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('ARC Wait Time Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartRRCWaitTime           Routine
    tmp:RRCWaitTime = True
    tmp:RRCWaitTimeStartDate = aus:DateChanged
    tmp:RRCWaitTimeStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('RRC Wait Time Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartARCCourierTime        Routine
    tmp:ARCCourierTime = True
    tmp:ARCCourierTimeStartDate = aus:DateChanged
    tmp:ARCCourierTimeStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('ARC Courier Time Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartRRCCourierTime        Routine
    tmp:RRCCourierTime = True
    tmp:RRCCourierTimeStartDate = aus:DateChanged
    tmp:RRCCourierTimeStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('RRC Courier Time Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartARCInControl       Routine
    tmp:ARCInControl = True
    tmp:ARCInControlStartDate = aus:DateChanged
    tmp:ARCInControlStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('ARC In Control Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

Start3rdParty           Routine
    tmp:3rdParty = True
    tmp:3rdPartyStartDate = aus:DateChanged
    tmp:3rdPartyStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('3rd Party Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartReadyToDespatch    Routine
    tmp:ReadyToDespatch = True
    tmp:ReadyToDespatchStartDate = aus:DateChanged
    tmp:ReadyToDespatchStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('Ready To Despatch Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartCustomerCollection Routine
    tmp:CustomerCollection = True
    tmp:CustomerCollectionStartDate = aus:DateChanged
    tmp:CustomerCollectionStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('Customer Collection Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartRRCOutOfControl Routine
    tmp:RRCOutOfControl = True
    tmp:RRCOutOfControlStartDate = aus:DateChanged
    tmp:RRCOutOfControlStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('RRC Out Of Control Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartARCOutOfControl Routine
    tmp:ARCOutOfControl = True
    tmp:ARCOutOfCOntrolStartDate = aus:DateChanged
    tmp:ARCOutOfCOntrolStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('ARC Out Of Control Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartARCEstimateTime Routine
    tmp:ARCEstimateTime = True
    tmp:ARCEstimateTimeStartDate = aus:DateChanged
    tmp:ARCEstimateTimeStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('ARC Estimate Time Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartRRCEstimateTime Routine
    tmp:RRCEstimateTime = True
    tmp:RRCEstimateTimeStartDate = aus:DateChanged
    tmp:RRCEstimateTimeStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('RRC Estimate Time Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')


! Insert --- Count Unlock Code Time (DBH: 23/07/2009) #10944
StartARCUnlockCode    Routine
    tmp:ARCUnlockCodeTime = 1
    tmp:ARCUnlockCodeStartDate = aus:DateChanged
    tmp:ARCUnlockCodeStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('ARC Unlock Code Time Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')

StartRRCUnlockCode    Routine
    tmp:RRCUnlockCodeTime = 1
    tmp:RRCUnlockCodeStartDate = aus:DateChanged
    tmp:RRCUnlockCodeStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('RRC Unlock Code Time Start: ' & Format(aus:DateChanged,@d6b) & ' ' & Format(aus:TimeChanged,@t1b) & ' - ' & Sub(aus:NewStatus,1,3),'c:\tat.log')
    End !If Command('/DEBUG')
! end --- (DBH: 23/07/2009) #10944
DrawSummaryTitle        Routine
    E1.SetColumnWidth('A','','30')
    E1.SetColumnWidth('B','Z','15')
    E1.WriteToCell('Turnaround Report (Summary)')
    E1.SetCellFontStyle('Bold','A1')
    E1.SetCellFontSize(12,'A1')



    E1.WriteToCell('Jobs Completed From','A3')
    E1.WriteToCell(Format(tmp:StartDate,@d18),'B3')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B3')
    E1.WriteToCell('Jobs Completed To','A4')
    E1.WriteToCell(Format(tmp:EndDate,@d18),'B4')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B4')
    E1.WriteTocell('Date Created','A5')
    E1.WriteToCell(Format(Today(),@d18),'B5')
    E1.SetCellFontStyle('Bold','B3','E5')

    !Colour
    Local.DrawBox('A1','E1','A1','E1',color:Silver)
    Local.DrawBox('A3','E3','A5','E5',color:Silver)

    E1.WriteToCell('VODR0078 - ' & loc:Version  ,'D3')
DrawDetailTitle     Routine
   E1.WriteToCell('Jobs Completed From', 'A3')
   E1.WriteToCell(Format(tmp:StartDate, @d18), 'B3')
   E1.SetCellNumberFormat(oix:NumberFormatDate,,,, 'B3')
   E1.WriteToCell('Jobs Completed To', 'A4')
   E1.WriteToCell(Format(tmp:EndDate, @d18), 'B4')
   E1.SetCellNumberFormat(oix:NumberFormatDate,,,, 'B4')
   E1.WriteTocell('Date Created', 'A5')
   E1.WriteToCell(Format(Today(), @d18), 'B5')
   E1.SetCellFontStyle('Bold', 'B3', 'E5')

   Local.DrawBox('A1', 'AC1', 'A1', 'AC1', color:Silver)
   Local.DrawBox('A3', 'AC3', 'A5', 'AC5', color:Silver)

   E1.WriteToCell('In Report', 'D3')
   E1.WriteToCell('Shown', 'E3')

   E1.SetCellFontStyle('Bold', 'A8', 'AC11')


   Local.DrawBox('A8', 'AC8', 'A11', 'AC11', color:Silver)

   If tmp:ARCAccount
       Local.DrawColumns(1) ! ARC
   Else ! If tra:Account_Number = tmp:HeadAccountNumber
       Local.DrawColumns(0) ! RRC
   End ! If tra:Account_Number = tmp:HeadAccountNumber

DrawDetailColumns       Routine

    E1.SetCellFontStyle('Bold', 'A8', 'AC11')

    Local.DrawBox('A8', 'AC8', 'A11', 'AC11', color:Silver)

    If tmp:ARCAccount
        Local.DrawColumns(1) !ARC
    Else !If tra:Account_Number = tmp:HeadAccountNumber
        Local.DrawColumns(0) !RRC
    End !If tra:Account_Number = tmp:HeadAccountNumber
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Do you wish to FINISH building the Excel Document with the data you have compiled so far, or just QUIT now?','ServiceBase',|
                       Icon:Question,'&Finish|&Quit|&Cancel',3)
        Of 1 ! &Finish Button
            tmp:Cancel = 2
        Of 2 ! &Quit Button
            tmp:Cancel = 1
        Of 3 ! &Cancel Button
        End!Case Message
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          Access:SRNTExt.Clearkey(srn:KeySRN)
          srn:SRNumber = '020623'&'0'
          If Access:SRNText.Fetch(srn:KeySRN)
              !Error
              SRN:TipText = 'Press F1 to access detailed help'
          End !Access:SRNText.Fetch(stn:KeySRN) = Level:Benign
          ?SRNNumber{Prop:Text}= 'SRN:020623'&'0'
          if clip(SRN:TipText)='' then SRN:TipText = 'Press F1 to access detailed help'.
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RRC_TATReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('tmp:Tag',tmp:Tag)                                  ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:SRNText.Open()
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:REPSCHAC.SetOpenRelated()
  Relate:REPSCHAC.Open                                     ! File REPSCHAC used by this procedure, so make sure it's RelationManager is open
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:TRADEACC_ALIAS.Open                               ! File TRADEACC_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:AUDSTATS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOCATLOG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:COURIER.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:COUBUSHR.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSCONS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHED.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHCR.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHLG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:AUDIT2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !TB13262 - J - 21/03/14 - report not running in Automatic mode. The following lines had been commented - now restored
  !!Check to see if the report has been run with one of the auto switches  (DBH: 07-05-2004)
  !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY') Or COmmand('/SCHEDULE')
  !    Automatic = True
  !End !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
  Automatic = false
  
  !Check to see if the report has been run with one of the auto switches  (DBH: 07-05-2004)
  If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY') Or COmmand('/SCHEDULE')
      Automatic = True
  End !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
  
  
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:TRADEACC,SELF) ! Initialize the browse manager
  SELF.Open(window)                                        ! Open window
  !========== Set The Version Number ============
  Include('..\ReportVersion.inc')
  
  tmp:VersionNumber = Clip(tmp:VersionNumber) & '5024'
  loc:Version = tmp:VersionNumber
  
  ?ReportVersion{Prop:Text} = 'Report Version: ' & Clip(loc:Version)
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List{Prop:LineHeight} = 0
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
      Alert(0078H) !F9
      Alert(0070H) !F1
  INIMgr.Fetch('RRC_TATReport',window)                     ! Restore window settings from non-volatile store
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,tra:Account_Number_Key)               ! Add the sort order for tra:Account_Number_Key for sort order 1
  BRW8.AddLocator(BRW8::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW8::Sort0:Locator.Init(,tra:Account_Number,1,BRW8)     ! Initialize the browse locator using  using key: tra:Account_Number_Key , tra:Account_Number
  BRW8.SetFilter('(tra:BranchIdentification <<> '''' and tra:stop_Account <<> ''YES'')') ! Apply filter expression to browse
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tmp:Tag,BRW8.Q.tmp:Tag)                    ! Field tmp:Tag is a hot field or requires assignment from browse
  BRW8.AddField(tra:Account_Number,BRW8.Q.tra:Account_Number) ! Field tra:Account_Number is a hot field or requires assignment from browse
  BRW8.AddField(tra:Company_Name,BRW8.Q.tra:Company_Name)  ! Field tra:Company_Name is a hot field or requires assignment from browse
  BRW8.AddField(tra:RecordNumber,BRW8.Q.tra:RecordNumber)  ! Field tra:RecordNumber is a hot field or requires assignment from browse
  IF ?tmp:AllAccounts{Prop:Checked} = True
    DISABLE(?List)
  END
  IF ?tmp:AllAccounts{Prop:Checked} = False
    ENABLE(?List)
  END
  BRW8.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  SELF.SetAlerts()
      !Set the report to automatic run  (DBH: 07-05-2004)
  
      IF Automatic = TRUE
          IF tmp:startdate = ''
            tmp:StartDate = TODAY()
            tmp:EndDate = TODAY()
          END
  
          !Daily Report from first of the month to today  (DBH: 13-05-2004)
  
  
          If Command('/DAILY')
              tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
              tmp:EndDate = Today()
          End !If Command('/DAILY')
  
          !Weekly report is not specified  (DBH: 13-05-2004)
  
          If Command('/WEEKLY')
              Loop day# = Today() To Today()-7 By -1
                  If day# %7 = 1
                      tmp:StartDate = day#
                      Break
                  End !If day# %7 = 2
              End !Loop day# = Today() To Today()-7 By -1
              Loop day# = Today() To Today() + 7
                  IF day# %7 = 0
                      tmp:EndDate = day#
                      Break
                  End !IF day# %7 = 0
              End !Loop day# = Today() To Today() + 7 By -1
          End !If Command('/WEEKLY')
          !Monthly - Start Date is 1st of Month, End Date is last of month  (DBH: 07-05-2004)
          If Command('/MONTHLY')
              tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
              tmp:EndDate = Deformat('1/' & Month(Today()) + 1 & '/' & Year(Today()),@d6) - 1
          End !If Command('/MONTHLY')
  
  
          tmp:Allaccounts = 1
  
          If Command('/SCHEDULE')
  
              x# = Instring('%',Command(),1,1)
              Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
              rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
              If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
                  !Found
                  Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
                  rpc:ReportName = rpd:ReportName
                  rpc:ReportCriteriaType = rpd:ReportCriteriaType
                  If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
                      !Found
                      tmp:Allaccounts = rpc:AllAccounts
  
                      Access:REPSCHAC.Clearkey(rpa:AccountNumberKey)
                      rpa:REPSCHCRRecordNumber = rpc:RecordNumber
                      Set(rpa:AccountNumberKey,rpa:AccountNumberKey)
                      Loop ! Begin Loop
                          If Access:REPSCHAC.Next()
                              Break
                          End ! If Access:REPSCHAC.Next()
                          If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
                              Break
                          End ! If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
                          glo:Pointer = rpa:AccountNumber
                          Add(glo:Queue)
                      End ! Loop
  
                      Case rpc:DateRangeType
                      Of 1 ! Today Only
                          tmp:StartDate = Today()
                          tmp:EndDate = Today()
                      Of 2 ! 1st Of Month
                          tmp:StartDate = Date(Month(Today()),1,Year(Today()))
                          tmp:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Allow for change of year
  !                tmp:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  !                tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
  ! to (DBH 31/01/2008) # 9711
                  tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  tmp:StartDate = Date(Month(tmp:EndDate),1,Year(tmp:EndDate))
  ! End (DBH 31/01/2008) #9711
                      End ! Case rpc:DateRangeType
                  Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
                      !Error
                  End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
              Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
                  !Error
              End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          End ! If Command('/SCHEDULE')
  
          DO Reporting
          POST(Event:CloseWindow)
      END
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  Relate:SRNText.Close()
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:REPSCHAC.Close
    Relate:SRNTEXT.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('RRC_TATReport',window)                  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020623'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020623'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020623'&'0')
      ***
    OF ?tmp:AllAccounts
      IF ?tmp:AllAccounts{Prop:Checked} = True
        DISABLE(?List)
      END
      IF ?tmp:AllAccounts{Prop:Checked} = False
        ENABLE(?List)
      END
      ThisWindow.Reset
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Print
      ThisWindow.Update
      If tmp:AllAccounts = 0
      
          If Records(Glo:Queue) = 0
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('You have not tagged any accounts.','ServiceBase',|
                             Icon:Hand,'&OK',1)
              Of 1 ! &OK Button
              End!Case Message
              Cycle
          End !If Records(Glo:Queue) = 0
      
      End !tmp:AllAcounts = 0
      
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
  !Of Event:AlertKey
      If KeyCode() = 0078H
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020623'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020623'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020623'&'0')
      ***
      End !If KeyCode() F9
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.GetStartDate         Procedure(*Date f:StartDate, *Time f:StartTime, Byte f:Type)
local:RRCStartTime         Time()
local:RRCEndTime           Time()
local:RRCSaturdayStartTime Time()
local:RRCSaturdayEndTime   Time()
local:ARCStartTime         Time()
local:ARCEndTime           Time()
   Code
   local:RRCStartTime         = Deformat('08:30', @t1)
   local:RRCEndTime           = Deformat('17:00', @t1)
   local:RRCSaturdayStartTime = Deformat('09:00', @t1)
   local:RRCSaturdayEndTime   = Deformat('13:00', @t1)
   local:ARCStartTime         = Deformat('08:00', @t1)
   local:ARCEndTime           = Deformat('17:00', @t1)

   Case f:Type
   Of 1 ! RRC
       Case f:StartDate % 7
       Of 0 ! Sunday
           ! Start counting in control from Monday 8:30 (DBH: 24/09/2006)
           f:StartDate += 1
           f:StartTime  = local:RRCStartTime
       Of 5 ! Friday
           If f:StartTime < local:RRCStartTime
               ! Booked before 8:30. Start counting at 8:30 (DBH: 24/09/2006)
               f:StartTIme = local:RRCStartTime
           End ! If tmp:RRCInControlStartTime < local:RRCStartTime
           If f:StartTime > local:RRCEndTime
               ! Booked after 17:00. Start counting 9:00 on Saturday (DBH: 24/09/2006)
               f:StartDate += 1
               f:StartTime  = local:RRCSaturdayStartTime
           End ! If tmp:RRCInControlStartTime > local:RRCEndTime
       Of 6 ! Saturday
           If f:StartTime < local:RRCSaturdayStartTime
               f:StartTime = local:RRCSaturdayStartTime
           End ! If tmp:RRCInControlStartTime < local:RRCSaturdayStartTime
           If f:StartTime > local:RRCSaturdayEndTime
               f:StartDate += 2
               f:StartTime  = local:RRCStartTime
           End ! If tmp:RRCInControlStartTime > local:RRCSaturdayEndTime
       Else
           IF f:StartTime < local:RRCStartTime
               ! Booked before 8:30. Start counting at 8:30 (DBH: 24/09/2006)
               f:StartTime = local:RRCStartTime
           END ! IF tmp:RRCInControlStartTime < local:RRCStartTime
           If f:StartTime > local:RRCEndTime
               ! Booked after 17:00. Start counting at 8:30 next day (DBH: 24/09/2006)
               f:StartDate += 1
               f:StartTime  = local:RRCStartTime
           End ! If tmp:RRCInControlStartTime > local:RRCEndTime
       End ! Case job:Date_Booked %7
   Of 0 ! ARC
       Case f:StartDate % 7
       Of 0 ! Sunday
           ! Booked on Sunday. Start counting at 8:00 on Monday (DBH: 24/09/2006)
           f:StartDate += 1
           f:startTime  = local:ARCStartTime
       Of 5 ! Friday
           If f:StartTime < local:ARCStartTime
               ! Booked before 8:00. Start counting at 8:00 (DBH: 24/09/2006)
               f:StartTime = local:ARCStartTime
           End ! If tmp:ARCInControlStartTime < local:ARCStartTime
           If f:StartTime > local:ARCEndTime
               ! Booked after 17:00. Start counting at 8:00 on Monday (DBH: 24/09/2006)
               f:StartDate += 3
               f:StartTime  = local:ARCStartTime
           End ! If tmp:ARCInControlStartTime > local:ARCEndTime
       Of 6 ! Saturday
           ! Booked on Saturday. Start counting 8:00 on Monday (DBH: 24/09/2006)
           f:StartDate += 2
           f:StartTime  = local:ARCStartTime
       Else
           IF f:StartTime < local:ARCStartTime
               ! Booked before 8:00. Start counting at 8:00 (DBH: 24/09/2006)
               f:StartTime = local:ARCStartTime
           END ! IF tmp:RRCInControlStartTime < local:RRCStartTime
           If f:StartTime > local:ARCEndTime
               ! Booked after 17:00. Start counting at 8:00 next day (DBH: 24/09/2006)
               f:StartDate += 1
               f:StartTime  = local:ARCStartTime
           End ! If tmp:RRCInControlStartTime > local:RRCEndTime
       End ! Case job:Date_Booked
   End ! Case f:Type
Local.WriteSummaryLoanLine    Procedure(String func:AccountNumber,Long func:Row,Byte func:Type)
Code
    Case func:Type
        Of 0 !Not Real Time
            Sort(LoanCountQueue1,loaque1:AccountNumber)
            Loop loanqueue# = 1 To Records(LoanCountQueue1)
                Get(LoanCountQueue1,loanqueue#)
                If loaque1:AccountNumber <> func:AccountNumber
                    Cycle
                End !If loaque1:AccountNumber <> func:AccountNumber
                E1.WriteToCell(loaque1:LoanCount,Clip(loaque1:LoanType) & Clip(func:Row))
                !Loan Total (DBH: 18-03-2004)
                E1.WriteToCell('=H' & func:Row & ' + L' & func:Row & |
                                        ' + P' & func:Row & ' + T' & func:Row & |
                                        ' + X' & func:Row & ' + AB' & func:Row & |
                                        ' + AF' & func:Row & ' + AJ' & func:Row,'E' & func:Row)
            End !Loop loanqueue# = 1 To Records(LoanCountQueue1)

        Of 1 !Real Time
            Sort(LoanCountQueue2,loaque2:AccountNumber)
            Loop loanqueue# = 1 To Records(LoanCountQueue2)
                Get(LoanCountQueue2,loanqueue#)
                If loaque2:AccountNumber <> func:AccountNumber
                    Cycle
                End !If loaque1:AccountNumber <> func:AccountNumber
                E1.WriteToCell(loaque2:LoanCount,Clip(loaque2:LoanType) & Clip(func:Row))
                !Loan Total (DBH: 18-03-2004)
                E1.WriteToCell('=H' & func:Row & ' + L' & func:Row & |
                                        ' + P' & func:Row & ' + T' & func:Row & |
                                        ' + X' & func:Row & ' + AB' & func:Row & |
                                        ' + AF' & func:Row & ' + AJ' & func:Row,'E' & func:Row)
            End !Loop loanqueue# = 1 To Records(LoanCountQueue1)
        Of 2 !Customer Time
            Sort(LoanCountQueue3,loaque3:AccountNumber)
            Loop loanqueue# = 1 To Records(LoanCountQueue3)
                Get(LoanCountQueue3,loanqueue#)
                If loaque3:AccountNumber <> func:AccountNumber
                    Cycle
                End !If loaque1:AccountNumber <> func:AccountNumber
                E1.WriteToCell(loaque3:LoanCount,Clip(loaque3:LoanType) & Clip(func:Row))
                !Loan Total (DBH: 18-03-2004)
                E1.WriteToCell('=H' & func:Row & ' + L' & func:Row & |
                                        ' + P' & func:Row & ' + T' & func:Row & |
                                        ' + X' & func:Row & ' + AB' & func:Row & |
                                        ' + AF' & func:Row & ' + AJ' & func:Row,'E' & func:Row)
            End !Loop loanqueue# = 1 To Records(LoanCountQueue1)

    End !Case func:Type
local.SubAccountExcludedFromReport      Procedure(String func:AccountNumber)
Code
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        If sub:ExcludeFromTATReport
            Return True
        End !If sub:ExcludeFromTATReport
    Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Return False
local.WriteSummaryLineTotals        Procedure(Long  func:Row)
Code
    !Complete Totals -  (DBH: 03-03-2004)
    E1.WriteToCell('=SUM(G' & func:Row,'G' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + K' & func:Row,'K' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + K' & func:Row & ' + O' & func:Row,'O' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + K' & func:Row & ' + O' & func:Row & ' + S' & func:Row,'S' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + K' & func:Row & ' + O' & func:Row & ' + S' & func:Row & ' + W' & func:Row,'W' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + K' & func:Row & ' + O' & func:Row & ' + S' & func:Row & ' + W' & func:Row & ' + AA' & func:Row,'AA' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + K' & func:Row & ' + O' & func:Row & ' + S' & func:Row & ' + W' & func:Row & ' + AA' & func:Row & ' + AE' & func:Row,'AE' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + K' & func:Row & ' + O' & func:Row & ' + S' & func:Row & ' + W' & func:Row & ' + AA' & func:Row & ' + AE' & func:Row & ' + AI' & func:Row,'AI' & func:Row + 1)

    !Loan Totals -  (DBH: 03-03-2004)
    E1.WriteToCell('=SUM(H' & func:Row,'H' & func:Row + 1)
    E1.WriteToCell('=SUM(H' & func:Row & ' + L' & func:Row,'L' & func:Row + 1)
    E1.WriteToCell('=SUM(H' & func:Row & ' + L' & func:Row & ' + P' & func:Row,'P' & func:Row + 1)
    E1.WriteToCell('=SUM(H' & func:Row & ' + L' & func:Row & ' + P' & func:Row & ' + T' & func:Row,'T' & func:Row + 1)
    E1.WriteToCell('=SUM(H' & func:Row & ' + L' & func:Row & ' + P' & func:Row & ' + T' & func:Row & ' + X' & func:Row,'X' & func:Row + 1)
    E1.WriteToCell('=SUM(H' & func:Row & ' + L' & func:Row & ' + P' & func:Row & ' + T' & func:Row & ' + X' & func:Row & ' + AB' & func:Row,'AB' & func:Row + 1)
    E1.WriteToCell('=SUM(H' & func:Row & ' + L' & func:Row & ' + P' & func:Row & ' + T' & func:Row & ' + X' & func:Row & ' + AB' & func:Row & ' + AF' & func:Row,'AF' & func:Row + 1)
    E1.WriteToCell('=SUM(H' & func:Row & ' + L' & func:Row & ' + P' & func:Row & ' + T' & func:Row & ' + X' & func:Row & ' + AB' & func:Row & ' + AF' & func:Row & ' + AJ' & func:Row,'AJ' & func:Row + 1)

    !Percent Totals -  (DBH: 03-03-2004)
    E1.WriteToCell('=SUM(I' & func:Row,'I' & func:Row + 1)
    E1.WriteToCell('=SUM(I' & func:Row & ' + M' & func:Row,'M' & func:Row + 1)
    E1.WriteToCell('=SUM(I' & func:Row & ' + M' & func:Row & ' + Q' & func:Row,'Q' & func:Row + 1)
    E1.WriteToCell('=SUM(I' & func:Row & ' + M' & func:Row & ' + Q' & func:Row & ' + U' & func:Row,'U' & func:Row + 1)
    E1.WriteToCell('=SUM(I' & func:Row & ' + M' & func:Row & ' + Q' & func:Row & ' + U' & func:Row & ' + Y' & func:Row,'Y' & func:Row + 1)
    E1.WriteToCell('=SUM(I' & func:Row & ' + M' & func:Row & ' + Q' & func:Row & ' + U' & func:Row & ' + Y' & func:Row & ' + AC' & func:Row,'AC' & func:Row + 1)
    E1.WriteToCell('=SUM(I' & func:Row & ' + M' & func:Row & ' + Q' & func:Row & ' + U' & func:Row & ' + Y' & func:Row & ' + AC' & func:Row & ' + AG' & func:Row,'AG' & func:Row + 1)
    E1.WriteToCell('=SUM(I' & func:Row & ' + M' & func:Row & ' + Q' & func:Row & ' + U' & func:Row & ' + Y' & func:Row & ' + AC' & func:Row & ' + AG' & func:Row & ' + AK' & func:Row,'AK' & func:Row + 1)

    E1.SetCellFontStyle('Bold','G' & func:Row + 1,'AK' & func:Row + 1)

    !Satisfied Total
    E1.WriteToCell('=(G' & func:Row & '/B' & func:Row & ')* 100','I' & func:Row)
    E1.WriteToCell('=(K' & func:Row & '/B' & func:Row & ')* 100','M' & func:Row)
    E1.WriteToCell('=(O' & func:Row & '/B' & func:Row & ')* 100','Q' & func:Row)
    E1.WriteToCell('=(S' & func:Row & '/B' & func:Row & ')* 100','U' & func:Row)
    E1.WriteToCell('=(W' & func:Row & '/B' & func:Row & ')* 100','Y' & func:Row)
    E1.WriteToCell('=(AA' & func:Row & '/B' & func:Row & ')* 100','AC' & func:Row)
    E1.WriteToCell('=(AE' & func:Row & '/B' & func:Row & ')* 100','AG' & func:Row)
    E1.WriteToCell('=(AI' & func:Row & '/B' & func:Row & ')* 100','AK' & func:Row)

Local.DrawSummaryLineTotalBar     Procedure(Long    func:Row)

Code

    local.DrawBox('G' & func:Row,'I' & func:Row,'G' & func:Row,'I' & func:Row,color:Silver)
    local.DrawBox('K' & func:Row,'M' & func:Row,'K' & func:Row,'M' & func:Row,color:Silver)
    local.DrawBox('O' & func:Row,'Q' & func:Row,'O' & func:Row,'Q' & func:Row,color:Silver)
    local.DrawBox('S' & func:Row,'U' & func:Row,'S' & func:Row,'U' & func:Row,color:Silver)
    local.DrawBox('W' & func:Row,'Y' & func:Row,'W' & func:Row,'Y' & func:Row,color:Silver)
    local.DrawBox('AA' & func:Row,'AC' & func:Row,'AA' & func:Row,'AC' & func:Row,color:Silver)
    local.DrawBox('AE' & func:Row,'AG' & func:Row,'AE' & func:Row,'AG' & func:Row,color:Silver)
    local.DrawBox('AI' & func:Row,'AK' & func:Row,'AI' & func:Row,'AK' & func:Row,color:Silver)

local.WriteSummarySectionTotal  Procedure(Long func:Row,Long func:StartRow)
local:FormulaString     String(255)
local:FormulaString2    String(255)
Code
    E1.WriteToCell('Sub Totals','A' & func:Row + 2)
    E1.WriteToCell('=SUM(B' & func:StartRow & ':B' & func:Row & ')','B' & func:Row + 2)
    E1.WriteToCell('=SUM(C' & func:StartRow & ':C' & func:Row & ')','C' & func:Row + 2)
    E1.WriteToCell('=SUM(D' & func:StartRow & ':D' & func:Row & ')','D' & func:Row + 2)
    E1.WriteToCell('=SUM(E' & func:StartRow & ':E' & func:Row & ')','E' & func:Row + 2)

    E1.WriteToCell('Average Inc. ARC','A' & func:Row + 3)
    E1.WriteToCell('Average Exc. ARC','A' & func:Row + 4)

    local.DrawSummaryLineTotalBar(func:Row + 3)
    local.DrawSummaryLineTotalBar(func:Row + 4)
    E1.SetCellFontStyle('Bold','A' & func:Row + 2,'AK' & func:Row + 4)

    Loop column# = 7 to 37 By 4
        !Summary Totals Inc ARC
        local:FormulaString = E1.GetColumnAddressFromColumnNumber(column#) & (func:StartRow + 1)
        local:FormulaString2 = 'B' & func:StartRow
        Loop x# = (func:StartRow + 3) to (func:Row+1) By 2
            local:FormulaString = Clip(local:FormulaString) & ' + ' & E1.GetColumnAddressFromColumnNumber(column#) & x#
            local:FormulaString2 = Clip(local:FormulaString2) & ' + B' & (x# - 1)
        End !Loop x# = 11 to func:Row Step 2
        E1.WriteToCell('=((' & Clip(local:FormulaString) & ')/(' & Clip(local:FormulaString2) & ')) * 100',E1.GetColumnAddressFromColumnNumber(column# + 2) & func:Row + 3)

        !Summary Totals Ex ARC
        local:FormulaString = E1.GetColumnAddressFromColumnNumber(column#) & func:StartRow + 3
        local:FormulaString2 = 'B' & func:StartRow + 2
        Loop x# = (func:StartRow + 5) to (func:Row+1) By 2
            local:FormulaString = Clip(local:FormulaString) & ' + ' & E1.GetColumnAddressFromColumnNumber(column#) & x#
            local:FormulaString2 = Clip(local:FormulaString2) & ' + B' & (x# - 1)
        End !Loop x# = 11 to func:Row Step 2
        E1.WriteToCell('=((' & Clip(local:FormulaString) & ')/(' & Clip(local:FormulaString2) & ')) * 100',E1.GetColumnAddressFromColumnNumber(column# + 2) & func:Row + 4)

    End !x# = 7 to 37
    local.DrawBox('A' & func:Row + 2,'E' & func:Row + 2,'A' & func:Row + 2,'E' & func:Row + 2,color:Silver)
    local.DrawBox('A' & func:Row + 3,'E' & func:Row + 3,'A' & func:Row + 3,'E' & func:Row + 3,color:Silver)
    local.DrawBox('A' & func:Row + 4,'E' & func:Row + 4,'A' & func:Row + 4,'E' & func:Row + 4,color:Silver)
local.SummaryTitles     Procedure(String func:Title,Long func:Position)
Code
    E1.WriteToCell(Clip(func:Title),'A' & func:Position)
    Local.DrawBox('A' & func:Position,'E' & func:Position,'A' & func:Position + 2,'E' & func:Position + 2,color:Silver)

    E1.SetColumnWidth('F','','1')

    E1.WriteToCell('1 Hour','G' & func:Position)
    E1.WriteToCell('Completed','G' & func:Position + 2)
    E1.WriteToCell('Loans','H' & func:Position + 2)
    E1.WriteToCell('%Satisfied','I' & func:Position + 2)
    Local.DrawBox('G' & func:Position,'I' & func:Position,'G' & func:Position + 2,'I' & func:Position + 2,color:Silver)
    E1.SetColumnWidth('J','','1')
    E1.SetColumnWidth('G','I','9')

    E1.WriteToCell('24 Hours','K' & func:Position)
    E1.WriteToCell('Completed','K' & func:Position + 2)
    E1.WriteToCell('Loans','L' & func:Position + 2)
    E1.WriteToCell('%Satisfied','M' & func:Position + 2)
    Local.DrawBox('K' & func:Position,'M' & func:Position,'K' & func:Position + 2,'M' & func:Position + 2,color:Silver)
    E1.SetColumnWidth('N','','1')
    E1.SetColumnWidth('K','M','9')

    E1.WriteToCell('48 Hours','O' & func:Position)
    E1.WriteToCell('Completed','O' & func:Position + 2)
    E1.WriteToCell('Loans','P' & func:Position + 2)
    E1.WriteToCell('%Satisfied','Q' & func:Position + 2)
    Local.DrawBox('O' & func:Position,'Q' & func:Position,'O' & func:Position + 2,'Q' & func:Position + 2,color:Silver)
    E1.SetColumnWidth('R','','1')
    E1.SetColumnWidth('O','Q','9')

    E1.WriteToCell('3 Days','S' & func:Position)
    E1.WriteToCell('Completed','S' & func:Position + 2)
    E1.WriteToCell('Loans','T' & func:Position + 2)
    E1.WriteToCell('%Satisfied','U' & func:Position + 2)
    Local.DrawBox('S' & func:Position,'U' & func:Position,'S' & func:Position + 2,'U' & func:Position + 2,color:Silver)
    E1.SetColumnWidth('V','','1')
    E1.SetColumnWidth('S','U','9')

    E1.WriteToCell('5 Days','W' & func:Position)
    E1.WriteToCell('Completed','W' & func:Position + 2)
    E1.WriteToCell('Loans','X' & func:Position + 2)
    E1.WriteToCell('%Satisfied','Y' & func:Position + 2)
    Local.DrawBox('W' & func:Position,'Y' & func:Position,'W' & func:Position + 2,'Y' & func:Position + 2,color:Silver)
    E1.SetColumnWidth('Z','','1')
    E1.SetColumnWidth('W','Y','9')

    E1.WriteToCell('7 Days','AA' & func:Position)
    E1.WriteToCell('Completed','AA' & func:Position + 2)
    E1.WriteToCell('Loans','AB' & func:Position + 2)
    E1.WriteToCell('%Satisfied','AC' & func:Position + 2)
    Local.DrawBox('AA' & func:Position,'AC' & func:Position,'AA' & func:Position + 2,'AC' & func:Position + 2,color:Silver)
    E1.SetColumnWidth('AD','','1')
    E1.SetColumnWidth('AA','AC','9')

    E1.WriteToCell('9 Days','AE' & func:Position)
    E1.WriteToCell('Completed','AE' & func:Position + 2)
    E1.WriteToCell('Loans','AF' & func:Position + 2)
    E1.WriteToCell('%Satisfied','AG' & func:Position + 2)
    Local.DrawBox('AE' & func:Position,'AG' & func:Position,'AE' & func:Position + 2,'AG' & func:Position + 2,color:Silver)
    E1.SetColumnWidth('AH','','1')
    E1.SetColumnWidth('AE','AG','9')

    E1.WriteToCell('Over 9 Days','AI' & func:Position)
    E1.WriteToCell('Completed','AI' & func:Position + 2)
    E1.WriteToCell('Loans','AJ' & func:Position + 2)
    E1.WriteToCell('%Satisfied','AK' & func:Position + 2)
    Local.DrawBox('AI' & func:Position,'AK' & func:Position,'AI' & func:Position + 2,'AK' & func:Position + 2,color:Silver)
    E1.SetColumnWidth('AL','','1')
    E1.SetColumnWidth('AI','AK','9')

    E1.WriteToCell('Account Name','A' & func:Position + 2)
    E1.WriteToCell('Jobs Completed Within Dates','B' & func:Position + 2)
    E1.WriteToCell('Jobs Booked Within Dates','C' & func:Position + 2)
    E1.WriteToCell('Open Jobs','D' & func:Position + 2)
    E1.WriteToCell('Loans Issued','E' & func:Position + 2)

    E1.SetCellFontStyle('Bold','A' & func:Position,'AK' & func:Position + 2)
local.GetSummaryType        Procedure(Long func:Time)
Code
    If func:Time < 60
        !1 Hour
        Return 'G'
    Elsif func:Time >= 60 And func:Time < 1440
        !24 Hours
        Return 'K'
    Elsif func:Time >= 1440 And func:Time < 2880
        !48 Hours
        Return 'O'
    Elsif func:Time >= 2880 And func:Time < 4320
        !3 days
        Return 'S'
    Elsif func:Time >= 4320 And func:Time < 7200
        !5 Days
        Return 'W'
    Elsif func:Time >= 7200 And func:Time < 10080
        !7 Days
        Return 'AA'
    Elsif func:Time >= 10080 And func:Time < 12960
        !9 Days
        Return 'AE'
    Elsif func:Time >= 12960
        !Over 9 Days
        Return 'AI'
    End !If func:Time < 60
local.GetSummaryTypeLoan        Procedure(Long func:Time)
Code
    If func:Time < 60
        !1 Hour
        Return 'H'
    Elsif func:Time >= 60 And func:Time < 1440
        !24 Hours
        Return 'L'
    Elsif func:Time >= 1440 And func:Time < 2880
        !48 Hours
        Return 'P'
    Elsif func:Time >= 2880 And func:Time < 4320
        !3 Days
        Return 'T'
    Elsif func:Time >= 4320 And func:Time < 7200
        !5 Days
        Return 'X'
    Elsif func:Time >= 7200 And func:Time < 10080
        !7 Days
        Return 'AB'
    Elsif func:Time >= 10080 And func:Time < 12960
        !9 Days
        Return 'AF'
    Elsif func:Time >= 12960
        !Over 9 Days
        Return 'AJ'
    End !If func:Time < 60
local.GetSummaryTypeName         Procedure(Long func:Time)
   Code
   If func:Time < 60
       ! 1 Hour
       Return '1 Hour'
   Elsif func:Time >= 60 And func:Time < 1440
       ! 24 Hours
       Return '24 Hours'
   Elsif func:Time >= 1440 And func:Time < 2880
       ! 48 Hours
       Return '48 Hours'
   Elsif func:Time >= 2880 And func:Time < 4320
       ! 3 days
       Return '3 Days'
   Elsif func:Time >= 4320 And func:Time < 5760
       ! 4 days
       Return '4 Days'
   Elsif func:Time >= 5760 And func:Time < 7200
       ! 5 Days
       Return '5 Days'
   Elsif func:Time >= 7200 And func:Time < 8640
       ! 3 days
       Return '6 Days'
   Elsif func:Time >= 8640 And func:Time < 10080
       ! 7 Days
       Return '7 Days'
   Elsif func:Time >= 10080 And func:Time < 11520
       ! 7 Days
       Return '8 Days'
   Elsif func:Time >= 11520 And func:Time < 12960
       ! 9 Days
       Return '9 Days'
   Elsif func:Time >= 12960
       ! Over 9 Days
       Return 'Over 9 Days'
   End ! If func:Time < 60
Local.WriteLine              Procedure(String func:Type)
local:JobCompanyName         String(30)
   Code
   Case func:Type
   Of 'ARC'
       Clear(arcexp:Record)
       ! SB Job Number
       arcexp:JobNumber               = job:Ref_Number
       ! Branch Specific Number
       arcexp:BranchNumber            = wob:JobNumber
       ! Head Account
       Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
       tra_ali:Account_Number  = wob:HeadAccountNumber
       If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
           ! Found
           arcexp:HeadAccount             =  tra_ali:Company_Name
       Else ! If Access:TRADE_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
           ! Error
           arcexp:HeadAccount             =  wob:HeadAccountNumber
       End ! If Access:TRADE_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
       ! Sub Account
       arcexp:SubAccount              = job:Account_number
       ! Manufacturer
       arcexp:Manufacturer            = job:Manufacturer
       ! Model Number
       arcexp:ModelNumber             = job:Model_Number
       ! Warranty
       If job:Warranty_Job = 'YES'
           arcexp:Warranty                = 'YES'
       Else ! job:Warranty_Job = 'YES'
           arcexp:Warranty                = 'NO'
       End ! job:Warranty_Job = 'YES'
       ! Jobs Booked in Date/Time
       arcexp:JobsbookedInDate = Format(job:Date_Booked, @d18)
       arcexp:JobsBookedInTime = Format(job:Time_Booked, @t01)
       ! jobs Completed Date/Time
       arcexp:JobsCompletedDate = Format(tmp:LastCompletedDate, @d18)
       arcexp:JobsCompletedTime = Format(tmp:LastCompletedTime, @t01)
       ! Total Repair Time
! Changing (DBH 17/10/2006) # 8367 - Use Customer Perspective
!       BHReturnDaysHoursMins(tmp:ARCRealTotalTime, Days#, Hours#, Mins#)
! to (DBH 17/10/2006) # 8367
       BHReturnDaysHoursMins(tmp:ARCCustomerPerspective, Days#, Hours#, Mins#)
! End (DBH 17/10/2006) #8367
       arcexp:TotalRepairTimeDays = Days#
       arcexp:TotalRepairTimeTime = Hours# & ':' & Mins#
       ! ARC/RRC In Control
       BHReturnDaysHoursMins(tmp:RealTotalARCInControl, Days#, Hours#, Mins#)
       arcexp:InControlDays = Days#
       arcexp:InControlTime = Hours# & ':' & Mins#
       ! Ready To Despatch
       BHReturnDaysHoursMins(tmp:RealTotalReadyToDespatch, Days#, Hours#, Mins#)
       arcexp:ReadyToDespatchDays = Days#
       arcexp:ReadyToDespatchTime = Hours# & ':' & Mins#
       ! Customer Colleciont
       BHReturnDaysHoursMins(tmp:RealTotalCustomerCollection, Days#, Hours#, Mins#)
       arcexp:CustomerCollectionDays = Days#
       arcexp:CustomerCollectionTime = Hours# & ':' & Mins#
       ! Wait Time
! Changing (DBH 17/10/2006) # 8367 - Combine ARC and RRC time
!       BHReturnDaysHoursMins(tmp:RealTotalARCWaitTime, Days#, Hours#, Mins#)
! to (DBH 17/10/2006) # 8367
        ! Inserting (DBH 02/02/2007) # 8719 - Don't count RRC if not finished
        If tmp:RRCJobFinished = False
            tmp:REalTotalRRCWaitTime = 0
        End ! If tmp:RRCJobFinished = False
        ! End (DBH 02/02/2007) #8719
        BHReturnDaysHoursMins(tmp:RealTotalARCWaitTime + tmp:RealTotalRRCWaitTime, Days#, Hours#, Mins#)
! End (DBH 17/10/2006) #8367
       arcexp:WaitTimeDays = Days#
       arcexp:WaitTimeTime = Hours# & ':' & Mins#
       ! Courier Movement
! Changing (DBH 17/10/2006) # 8367 - Combine ARC and RRC times
!       BHReturnDaysHoursMins(tmp:RealTotalARCCourierTime, Days#, Hours#, Mins#)
! to (DBH 17/10/2006) # 8367
        ! Inserting (DBH 02/02/2007) # 8719 - Don't count the RRC if it's not finished
        If tmp:RRCJobFinished = False
            tmp:RealTotalRRCCourierTime = 0
        End ! If tmp:RRCJobFinished = False
        ! End (DBH 02/02/2007) #8719
        BHReturnDaysHoursMins(tmp:RealTotalARCCourierTime + tmp:RealTotalRRCCourierTime, Days#, Hours#, Mins#)
! End (DBH 17/10/2006) #8367
       arcexp:CourierMovementDays = Days#
       arcexp:CourierMovementTime = Hours# & ':' & Mins#
            ! 3rd Party Time
            ! 3rd party should include RRC in control (if RRC job) - 3263 (DBH: 02-04-2004)
! Changing (DBH 28/07/2006) # 8022 - Use the RRC Total Repair Time. RRC real Time already includes 3rd party time.
!       BHReturnDaysHoursMins(tmp:RealTotal3rdParty + tmp:RealTotalRRCInControl, Days#, Hours#, Mins#)
! to (DBH 28/07/2006) # 8022
        ! Inserting (DBH 02/02/2007) # 8719 - DOn't count the RRC if it's not finishewd
        If tmp:RRCJobFInished = False
            tmp:RRCRealTOtalTime = 0
        End ! If tmp:RRCJobFInished = False
        ! End (DBH 02/02/2007) #8719

        BHReturnDaysHoursMins(tmp:RRCRealTotalTime, Days#, Hours#, Mins#)
! End (DBH 28/07/2006) #8022
        arcexp:ThirdPartyTimeDays = Days#
        arcexp:ThirdPartyTimeTime = Hours# & ':' & Mins#

       ! Loan Issued
        If tmp:LoanUnitAttached = True
            arcexp:LoanIssued              = 'YES'
        Else ! LoanAttachedTojob(job:Ref_Number
            arcexp:LoanIssued              = 'NO'
        End ! LoanAttachedTojob(job:Ref_Number
        !Repair Centre Perspective
        arcexp:RCP = local.GetSummaryTypeName(tmp:ARC1hrTotalTime)
        !Real Time Repair Center Perspective
        arcexp:RTRCP = local.GetSummaryTypeName(tmp:RealARC1hrTotalTime)
        !Customer Perspective
        ! Change --- Use the same time as the Cust Per column (DBH: 22/07/2009) #10944
        !Arcexp:CP = local.GetSummaryTypeName(tmp:CustomerTotalTime)
        ! To --- (DBH: 22/07/2009) #10944
        arcexp:CP = local.GetSummaryTypeName(tmp:ARCCustomerPerspective)
        ! end --- (DBH: 22/07/2009) #10944
        Add(ARCExportFile)




    Of 'RRC'
        Clear(exp:Record)
        ! SB Job Number
        exp:JobNumber               = job:Ref_Number
        ! Branch Specific Number
        exp:BranchNumber            = wob:JobNumber
        ! Head Account
        Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
        tra_ali:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
            ! Found
            exp:HeadAccount             =  tra_ali:Company_Name
        Else ! If Access:TRADE_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
            ! Error
            exp:HeadAccount             =  wob:HeadAccountNumber
        End ! If Access:TRADE_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
        ! Sub Account
        exp:SubAccount              = job:Account_number
        ! Manufacturer
        exp:Manufacturer            = job:Manufacturer
        ! Model Number
        exp:ModelNumber             = job:Model_Number
        ! Warranty
        If job:Warranty_Job = 'YES'
            exp:Warranty                = 'YES'
        Else ! job:Warranty_Job = 'YES'
            exp:Warranty                = 'NO'
        End ! job:Warranty_Job = 'YES'
        ! Jobs Booked in Date/Time
        exp:JobsbookedInDate = Format(job:Date_Booked, @d18)
        exp:JobsBookedInTime = Format(job:Time_Booked, @t01)
        ! jobs Completed Date/Time
        exp:JobsCompletedDate = Format(tmp:LastCompletedDate, @d18)
        exp:JobsCompletedTime = Format(tmp:LastCompletedTime, @t01)
        ! Total Repair Time
 ! Changing (DBH 17/10/2006) # 8367 - Use Customer Perspective
 !       BHReturnDaysHoursMins(tmp:RRCRealTotalTime, Days#, Hours#, Mins#)
 ! to (DBH 17/10/2006) # 8367
        BHReturnDaysHoursMins(tmp:RRCCustomerPerspective, Days#, Hours#, Mins#)
 ! End (DBH 17/10/2006) #8367

        exp:TotalRepairTimeDays = Days#
        exp:TotalRepairTimeTime = Hours# & ':' & Mins#
        ! ARC/RRC In Control
        BHReturnDaysHoursMins(tmp:RealTotalRRCInControl, Days#, Hours#, Mins#)
        exp:InControlDays = Days#
        exp:InControlTime = Hours# & ':' & Mins#
        ! Ready To Despatch
        BHReturnDaysHoursMins(tmp:RealTotalReadyToDespatch, Days#, Hours#, Mins#)
        exp:ReadyToDespatchDays = Days#
        exp:ReadyToDespatchTime = Hours# & ':' & Mins#
        ! Customer Colleciont
        BHReturnDaysHoursMins(tmp:RealTotalCustomerCollection, Days#, Hours#, Mins#)
        exp:CustomerCollectionDays = Days#
        exp:CustomerCollectionTime = Hours# & ':' & Mins#
        ! Wait Time
 ! Changing (DBH 17/10/2006) # 8367 - Use combined ARC and RRC times
 !       BHReturnDaysHoursMins(tmp:RealTotalRRCWaitTime, Days#, Hours#, Mins#)
 ! to (DBH 17/10/2006) # 8367
        BHReturnDaysHoursMins(tmp:RealTotalARCWaitTime + tmp:RealTotalRRCWaitTime, Days#, Hours#, Mins#)
 ! End (DBH 17/10/2006) #8367
        exp:WaitTimeDays = Days#
        exp:WaitTimeTime = Hours# & ':' & Mins#
        ! Courier Movement
 ! Changing (DBH 17/10/2006) # 8367 - Use combined ARC and RRC Times
 !       BHReturnDaysHoursMins(tmp:RealTotalRRCCourierTime, Days#, Hours#, Mins#)
 ! to (DBH 17/10/2006) # 8367
         BHReturnDaysHoursMins(tmp:RealTotalARCCourierTIme + tmp:RealTotalRRCCourierTime, Days#, Hours#, Mins#)
 ! End (DBH 17/10/2006) #8367
        exp:CourierMovementDays = Days#
        exp:CourierMovementTime = Hours# & ':' & Mins#
        ! 3rd Party Time
        ! ARC 3rd party should include RRC In Control - 3263 (DBH: 02-04-2004)

 ! Changing (DBH 28/07/2006) # 8022 - Use the ARC Total Repair Time. (ARC Real TOTal TIme included 3rd Party TIme)
 !       BHReturnDaysHoursMins(tmp:RealTotal3rdParty + tmp:RealTotalARCInControl, Days#, Hours#, Mins#)
 ! to (DBH 28/07/2006) # 8022
        BHReturnDaysHoursMins(tmp:ARCRealTotalTime, Days#, Hours#, Mins#)
 ! End (DBH 28/07/2006) #8022
        exp:ThirdPartyTimeDays = Days#
        exp:ThirdPartyTimeTime = Hours# & ':' & Mins#
        ! Loan Issued
        If tmp:LoanUnitAttached = True
            exp:LoanIssued              = 'YES'
        Else ! LoanAttachedTojob(job:Ref_Number
            exp:LoanIssued              = 'NO'
        End ! LoanAttachedTojob(job:Ref_Number
        !Repair Centre Perspective
        exp:RCP = local.GetSummaryTypeName(tmp:RRC1hrTotalTime)
        !Real Time Repair Center Perspective
        exp:RTRCP = local.GetSummaryTypeName(tmp:RealRRC1hrTotalTime)
        !Customer Perspective
        ! Change --- Use the same time as the Cust Per column (DBH: 22/07/2009) #10944
        !exp:CP = local.GetSummaryTypeName(tmp:CustomerTotalTime)
        ! To --- (DBH: 22/07/2009) #10944
        exp:CP = local.GetSummaryTypeName(tmp:RRCCustomerPerspective)
        ! end --- (DBH: 22/07/2009) #10944
        Add(ExportFile)
    End ! Case func:Type
Local.ARCUser       Procedure(String func:User)
local:CurrentLocation       String(30)
local:1Time         String(4)
local:2Time         String(4)
Code
    If aus:Type = 'JOB'
        local:CurrentLocation = ''
        Save_LOCATLOG_ID = Access:LOCATLOG.SaveFile()
        Access:LOCATLOG.Clearkey(lot:DateKey)
        lot:RefNumber = job:Ref_Number
        lot:TheDate    = aus:DateChanged
        Set(lot:DateKey,lot:DateKey)
        Loop ! Begin Loop
            If Access:LOCATLOG.Next()
                Break
            End ! If Access:LOCATLOG.Next()
            If lot:RefNumber <> job:Ref_Number
                Break
            End ! If lot:RefNumber <> job:Ref_Number
            If lot:TheDate < aus:DateChanged
                Break
            End ! If lot:TheDate < aus:DateChanged
            If lot:TheDate = aus:DateChanged
                local:1Time = Format(lot:TheTime,@t02)
                local:2Time = Format(aus:TimeChanged,@t02)

                lot:TheTime = Deformat(local:1Time & ':00',@t5)
                aus:TimeChanged = Deformat(local:2Time & ':00',@t5)
                If lot:TheTime < aus:TimeChanged
                    Cycle
                End ! If lot:TheTime < aus:TimeChanged
! Changing (DBH 22/02/2007) # 894 - Can't compare the actual times incase the partial seconds are out
!                If lot:TheTime = aus:TimeChanged
! to (DBH 22/02/2007) # 894
                If local:1Time = local:2Time
! End (DBH 22/02/2007) #894
                    local:CurrentLocation = lot:NewLocation
                    Break
                End ! If lot:TheTime = aus:TimeChanged
            End ! If lot:TheDate = aus:DateChanged
            local:CurrentLocation = lot:PreviousLocation
            Break
        End ! Loop
        Access:LOCATLOG.RestoreFile(Save_LOCATLOG_ID)

        If Clip(local:CurrentLocation) <> ''
            Case local:CurrentLocation
            Of 'AT FRANCHISE'
                Return False
            Of 'SENT TO 3RD PARTY'
                Return True
            Of 'RECEIVED AT ARC'
                Return True
            Of 'DESPATCHED'
            Of 'IN-TRANSIT TO ARC'
                Return False
            Of 'IN-TRANSIT TO RRC'
                Return True
            End ! Case local:CurrentLocation
        End ! If Clip(local:CurrentLocation) <> ''
    End ! If aus:Type = 'JOB'

    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = func:User
    If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Found
        ! See if the engineer is in the audit trail. His location will then be in the notes (DBH: 07/11/2006)
        ARC#   = False
        RRC#   = False
        Found# = False
        Save_aud_ID = Access:AUDIT.SaveFile()
        Access:AUDIT.Clearkey(aud:Action_Key)
        aud:Ref_Number = job:Ref_Number
        aud:Action     = 'ENGINEER ALLOCATED: ' & Clip(use:User_Code)
        Set(aud:Action_Key,aud:Action_Key)
        Loop ! Begin Loop
            If Access:AUDIT.Next()
                Break
            End ! If Access:AUDIT.Next()
            If aud:Ref_Number <> job:Ref_Number
                Break
            End ! If aud:Ref_Number = job:Ref_Number
            Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
            aud2:AUDRecordNumber = aud:Record_Number
            IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

            END ! IF


            If aud:Action <> 'ENGINEER ALLOCATED: ' & Clip(use:User_Code)
                Break
            End ! If aud:Action <> 'ENGINEER ALLOCATED: ' & Clip(use:User_Code)
            If Instring(Clip(tmp:HeadAccountNumber),aud2:Notes,1,1)
                Found# = True
                ARC#   = True
                Break
            End ! If Instring('AA20',aud:Notes,1,1)
            If Instring(Clip(wob:HeadAccountNumber),aud2:Notes,1,1)
                Found# = True
                RRC#   = True
                Break
            End ! If Instring(wob:HeadAccountNumber,aud:Notes,1,1)
        End ! Loop
        Access:AUDIT.RestoreFile(Save_aud_ID)

        If Found# = True
            If ARC#
                Return True
            End ! If ARC#
            If RRC#
                Return False
            End ! If RRC#
        End ! If Found# = True

        If use:Location = tmp:ARCLocation
            Return True
        End !If use:Location = tmp:ARCLocation
    Else !If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Error
        ! Inserting (DBH 05/12/2005) #6804 - If can't find engineer, and not an RRC job.. can assume it's an ARC engineer. Otherwise try and work it out. Mmmm.
        If jobe:WebJob = False
            Return True
        Else ! If jobe:WebJob = False
            Return local.ATARC(aus:DateChanged,aus:TimeChanged)
        End ! If jobe:WebJob = False
        ! End (DBH 05/12/2005) #6804
    End !If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
    Return False
Local.ATARC         Procedure(Date func:NowDate,Time func:NowTime)
Code
    If tmp:SentToARCDate = 0
        !Job Not Sent To ARC -  (DBH: 18-02-2004)
        Return False
    End !If tmp:SentToARCDate = 0


    If func:NowDate > tmp:SentToARCDate
        If tmp:SentToRRCDate > 0
            If func:NowDate < tmp:SentToRRCDate
                Return True
            ElsIf func:NowDate = tmp:SentToRRCDate
                If func:NowTime < tmp:SentToRRCTime
                    Return True
                End !If tmp:NowTime < tmp:SentToRRCTime
            End !If func:NowDate = tmp:SentToRRCDate
        Else !If tmp:SentToRRCDate > 0
            Return True
        End !If tmp:SentToRRCDate > 0
    End !If func:NowDate > tmp:SentToARCDate

    If func:NowDate = tmp:SentToARCDate
        If func:NowTime > tmp:SentToARCTime
            If tmp:SentToRRCDate > 0
                If func:NowDate < tmp:SentToRRCDate
                    Return True
                ElsIf func:NowDate = tmp:SentToRRCDate
                    If func:NowTime < tmp:SentToRRCTime
                        Return True
                    End !If func:NowTime < tmp:SentToRRCTime
                End !If func:NowDate = tmp:SentToRRCDate
            Else !If tmp:SentToRRCDate > 0
                Return True
            End !If tmp:SentToRRCDate > 0
        End !If func:NowTime > tmp:SentToARCTime
    End !If func:NowDate = tmp:SentToARCDate

    Return False
Local.TimeDifference        Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime, String func:AccountNumber)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

local:Type      Byte(0)

Code
    Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
    Case func:AccountNumber
        Of 0 !RRC
            tra_ali:Account_Number = wob:HeadAccountNumber
            If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
                !Found

            Else !If Access:TRADEACC.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
                !Error
                Return 0
            End !If Access:TRADEACC.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
            local:Type = 0
        Of 1 !ARC
            tra_ali:Account_Number = tmp:HeadAccountNumber
            If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
                !Found

            Else !If Access:TRADEACC.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
                !Error
                Return 0
            End !If Access:TRADEACC.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
            local:Type = 0
        Else
            local:Type = 1
            Access:COURIER.ClearKey(cou:Courier_Key)
            cou:Courier = func:AccountNumber
            If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                !Found

            Else !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                !Error
                Return 0
            End !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign

    End !Case func:AccountNumber


    Loop x# = func:StartDate to func:EndDate
        If local:Type = 1
            Access:COUBUSHR.ClearKey(cbh:TypeDateKey)
            cbh:Courier = cou:Courier
            cbh:TheDate = x#
            If Access:COUBUSHR.TryFetch(cbh:TypeDateKey) = Level:Benign
                !Found
                Case cbh:ExceptionType
                    Of 0 !Exception
                        local:StartTime   = cbh:StartTime
                        local:EndTime     = cbh:EndTime
                        If x# = func:StartDate
                            If func:StartTime > cbh:StartTime
                                If func:StartTime > cbh:EndTime
                                    local:StartTime = 0
                                    local:EndTime = 0
                                Else !If func:StartTime > tbh:EndTime
                                    local:StartTime = func:StartTime
                                End !If func:StartTime > tbh:EndTime
                            End !If local:StartTime < tbh:StartTime
                        End !If x# = func:StartDate

                        If x# = func:EndDate
                            If func:EndTime < cbh:EndTime
                                If func:EndTime < cbh:StartTime
                                    local:StartTime = 0
                                    local:EndTime = 0
                                Else !If func:EndTime < tbh:StarTime
                                    local:EndTime = func:EndTime
                                End !If func:EndTime < tbh:StarTime
                            End !If local:EndTime > tbh:EndTime
                        End !If x# = func:EndDate
                    Of 1 !Free Day
                        local:StartTime   = 0
                        local:EndTime     = 0
                End !Case cbh:ExceptionType
            Else !If Access:COUBUSHR.TryFetch(cbh:TypeDateKey) = Level:Benign
                !Error
                Case x# % 7
                    Of 0 !Sunday
                        local:StartTime   = cou:SunStartWorkHours
                        local:EndTime     = cou:SunEndWorkHours
                        If cou:IncludeSunday = 'YES'
                            If x# = func:StartDate
                                If func:StartTime > cou:SunStartWorkHours
                                    If func:StartTime > cou:SunEndWorkHours
                                        local:EndTime = 0
                                        local:StartTime = 0
                                    Else !If func:StartTime > tra_ali:EndWorkHours
                                        local:StartTime = func:StartTime
                                    End !If func:StartTime > tra_ali:EndWorkHours
                                End !If local:StartTime > tra_ali:StartWorkHours
                            End !If x# = func:StartDate
                            If x# = func:EndDate
                                If func:EndTime < cou:SunEndWorkHours
                                    If func:EndTime < cou:SunStartWorkHours
                                        local:EndTime = 0
                                        local:StartTime = 0
                                    Else !If func:EndTime < tra_ali:StartWorkHours
                                        local:EndTime = func:EndTime
                                    End !If func:EndTime < tra_ali:StartWorkHours
                                End !If local:EndTime < tra_ali:EndWorkHours
                            End !If x# = func:EndDate

                        Else !If tra_ali:IncludeSunday = 'YES'
                            local:StartTime   = 0
                            local:EndTime    = 0
                        End !If tra_ali:IncludeSunday = 'YES'
                    Of 6 !Saturday
                        local:StartTime   = cou:SatStartWorkHours
                        local:EndTime     = cou:SatEndWorkHours
                        If cou:IncludeSaturday = 'YES'
                            If x# = func:StartDate
                                If func:StartTime > cou:SatStartWorkHours
                                    If func:StartTime > cou:SatEndWorkHours
                                        local:EndTime = 0
                                        local:StartTime = 0
                                    Else !If func:StartTime > tra_ali:EndWorkHours
                                        local:StartTime = func:StartTime
                                    End !If func:StartTime > tra_ali:EndWorkHours
                                End !If local:StartTime > tra_ali:StartWorkHours
                            End !If x# = func:StartDate
                            If x# = func:EndDate
                                If func:EndTime < cou:SatEndWorkHours
                                    If func:EndTime < cou:SatStartWorkHours
                                        local:EndTime = 0
                                        local:StartTime = 0
                                    Else !If func:EndTime < tra_ali:StartWorkHours
                                        local:EndTime = func:EndTime
                                    End !If func:EndTime < tra_ali:StartWorkHours
                                End !If local:EndTime < tra_ali:EndWorkHours
                            End !If x# = func:EndDate

                        Else !If tra_ali:IncludeSaturday = 'YES'
                            local:StartTime   = 0
                            local:EndTime     = 0
                        End !If tra_ali:IncludeSaturday = 'YES'
                    Else
                        local:StartTime   = cou:StartWorkHours
                        local:EndTime     = cou:EndWorkHours

                        If x# = func:StartDate
                            If func:StartTime > cou:StartWorkHours
                                If func:StartTime > cou:EndWorkHours
                                    local:EndTime = 0
                                    local:StartTime = 0
                                Else !If func:StartTime > tra_ali:EndWorkHours
                                    local:StartTime = func:StartTime
                                End !If func:StartTime > tra_ali:EndWorkHours
                            End !If local:StartTime > tra_ali:StartWorkHours
                        End !If x# = func:StartDate
                        If x# = func:EndDate
                            If func:EndTime < cou:EndWorkHours
                                If func:EndTime < cou:StartWorkHours
                                    local:EndTime = 0
                                    local:StartTime = 0
                                Else !If func:EndTime < tra_ali:StartWorkHours
                                    local:EndTime = func:EndTime
                                End !If func:EndTime < tra_ali:StartWorkHours
                            End !If local:EndTime < tra_ali:EndWorkHours
                        End !If x# = func:EndDate
                End !Case func:EndDate

            End !If Access:COUBUSHR.TryFetch(cbh:TypeDateKey) = Level:Benign
        Else !If local:Type = 0

            Access:TRABUSHR.ClearKey(tbh:TypeDateKey)
            tbh:RefNumber = tra_ali:RecordNumber
            tbh:TheDate   = x#
            If Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign
                !Found
                Case tbh:ExceptionType
                    Of 0 !Exception
                        local:StartTime   = tbh:StartTime
                        local:EndTime     = tbh:EndTime
                        If x# = func:StartDate
                            If func:StartTime > tbh:StartTime
                                If func:StartTime > tbh:EndTime
                                    local:StartTime = 0
                                    local:EndTime = 0
                                Else !If func:StartTime > tbh:EndTime
                                    local:StartTime = func:StartTime
                                End !If func:StartTime > tbh:EndTime
                            End !If local:StartTime < tbh:StartTime
                        End !If x# = func:StartDate

                        If x# = func:EndDate
                            If func:EndTime < tbh:EndTime
                                If func:EndTime < tbh:StartTime
                                    local:StartTime = 0
                                    local:EndTime = 0
                                Else !If func:EndTime < tbh:StarTime
                                    local:EndTime = func:EndTime
                                End !If func:EndTime < tbh:StarTime
                            End !If local:EndTime > tbh:EndTime
                        End !If x# = func:EndDate
                    Of 1 !Free Day
                        local:StartTime   = 0
                        local:EndTime     = 0
                End !Case tbh:ExceptionType
            Else !If Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign
                !Error
                Case x# % 7
                    Of 0 !Sunday
                        local:StartTime   = tra_ali:SunStartWorkHours
                        local:EndTime     = tra_ali:SunEndWorkHours
                        If tra_ali:IncludeSunday = 'YES'
                            If x# = func:StartDate
                                If func:StartTime > tra_ali:SunStartWorkHours
                                    If func:StartTime > tra_ali:SunEndWorkHours
                                        local:EndTime = 0
                                        local:StartTime = 0
                                    Else !If func:StartTime > tra_ali:EndWorkHours
                                        local:StartTime = func:StartTime
                                    End !If func:StartTime > tra_ali:EndWorkHours
                                End !If local:StartTime > tra_ali:StartWorkHours
                            End !If x# = func:StartDate
                            If x# = func:EndDate
                                If func:EndTime < tra_ali:SunEndWorkHours
                                    If func:EndTime < tra_ali:SunStartWorkHours
                                        local:EndTime = 0
                                        local:StartTime = 0
                                    Else !If func:EndTime < tra_ali:StartWorkHours
                                        local:EndTime = func:EndTime
                                    End !If func:EndTime < tra_ali:StartWorkHours
                                End !If local:EndTime < tra_ali:EndWorkHours
                            End !If x# = func:EndDate

                        Else !If tra_ali:IncludeSunday = 'YES'
                            local:StartTime   = 0
                            local:EndTime    = 0
                        End !If tra_ali:IncludeSunday = 'YES'
                    Of 6 !Saturday
                        local:StartTime   = tra_ali:SatStartWorkHours
                        local:EndTime     = tra_ali:SatEndWorkHours
                        If tra_ali:IncludeSaturday = 'YES'
                            If x# = func:StartDate
                                If func:StartTime > tra_ali:SatStartWorkHours
                                    If func:StartTime > tra_ali:SatEndWorkHours
                                        local:EndTime = 0
                                        local:StartTime = 0
                                    Else !If func:StartTime > tra_ali:EndWorkHours
                                        local:StartTime = func:StartTime
                                    End !If func:StartTime > tra_ali:EndWorkHours
                                End !If local:StartTime > tra_ali:StartWorkHours
                            End !If x# = func:StartDate
                            If x# = func:EndDate
                                If func:EndTime < tra_ali:SatEndWorkHours
                                    If func:EndTime < tra_ali:SatStartWorkHours
                                        local:EndTime = 0
                                        local:StartTime = 0
                                    Else !If func:EndTime < tra_ali:StartWorkHours
                                        local:EndTime = func:EndTime
                                    End !If func:EndTime < tra_ali:StartWorkHours
                                End !If local:EndTime < tra_ali:EndWorkHours
                            End !If x# = func:EndDate

                        Else !If tra_ali:IncludeSaturday = 'YES'
                            local:StartTime   = 0
                            local:EndTime     = 0
                        End !If tra_ali:IncludeSaturday = 'YES'
                    Else
                        local:StartTime   = tra_ali:StartWorkHours
                        local:EndTime     = tra_ali:EndWorkHours
                        If x# = func:StartDate
                            If func:StartTime > tra_ali:StartWorkHours
                                If func:StartTime > tra_ali:EndWorkHours
                                    local:EndTime = 0
                                    local:StartTime = 0
                                Else !If func:StartTime > tra_ali:EndWorkHours
                                    local:StartTime = func:StartTime
                                End !If func:StartTime > tra_ali:EndWorkHours
                            End !If local:StartTime > tra_ali:StartWorkHours
                        End !If x# = func:StartDate
                        If x# = func:EndDate
                            If func:EndTime < tra_ali:EndWorkHours
                                If func:EndTime < tra_ali:StartWorkHours
                                    local:EndTime = 0
                                    local:StartTime = 0
                                Else !If func:EndTime < tra_ali:StartWorkHours
                                    local:EndTime = func:EndTime
                                End !If func:EndTime < tra_ali:StartWorkHours
                            End !If local:EndTime < tra_ali:EndWorkHours
                        End !If x# = func:EndDate
                End !Case func:EndDate
            End !If Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign
        End !If func:AccountNumber <> 0 And func:AccountNumber <> 1

        local:1Time = Format(local:EndTime,@t2)
        local:2Time = Format(local:StartTime,@t2)

        local:1a = Sub(local:1Time,4,1)
        local:1b = Sub(local:1Time,3,1)
        local:1c = Sub(local:1time,2,1)
        local:1d = Sub(local:1time,1,1)

        local:2a = Sub(local:2Time,4,1)
        local:2b = Sub(local:2Time,3,1)
        local:2c = Sub(local:2time,2,1)
        local:2d = Sub(local:2time,1,1)

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)

local.RealTimeDifference             Procedure(Date f:StartDate, Date f:EndDate, Time f:StartTime, Time f:EndTime, Byte f:Type)
local:StartTime    Time
local:EndTime      Time
local:MidNight     Byte(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()
local:TotalTime         Long()

Code
    If f:StartDate > f:EndDate
        Return 0
    End ! If f:StartDate > f:EndDate

    Loop x# = f:StartDate to f:EndDate
        ! Ignore Sundays (DBH: 25/09/2006)
        If x# % 7 = 0
            Cycle
        End ! If x# % 7 = 0

        Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
        If f:Type = 1
            tra_ali:Account_Number    = tmp:HeadAccountNumber
        Else ! If f:Type = 1
            tra_ali:Account_Number    = wob:HeadAccountNumber
        End ! If f:Type = 1
        If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
            !Found
            Access:TRABUSHR.ClearKey(tbh:TypeDateKey)
            tbh:RefNumber    = tra_ali:RecordNumber
            tbh:TheDate      = x#
            If Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign
                !Found
                If tbh:ExceptionType = 1
                    ! This day has been marked as a "Day Off" for this account. (DBH: 25/09/2006)
                    Cycle
                End ! If tbh:ExceptionType = 1
            Else ! If Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign
                !Error
            End ! If Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign
        Else ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign

        local:StartTime = Deformat('00:00:00',@t4)
        local:EndTime = Deformat('23:59:59',@t4)

        Case f:Type
        Of 0 !RRC
            local:Midnight = True
            Case x# % 7
            Of 1 ! Monday
                local:StartTime = Deformat('08:30:00',@t4)
            Of 6 !Saturday
                local:EndTime = Deformat('13:00:00',@t4)
                local:MidNight = False
            End ! Case x# % 7
        Of 1 !ARC
            local:Midnight = True
            Case x# % 7
            Of 1 ! Monday
                local:StartTime = Deformat('08:00:00',@t4)
            Of 5 ! Friday
                local:EndTime = Deformat('17:00:00',@t4)
                local:MidNight = False
            Of 6 ! Saturday
                Cycle
            End ! Case x# % 7
        End ! Case f:Type

        If x# = f:StartDate
            If f:StartTime > local:StartTime
                If f:StartTime > local:EndTime
                    local:StartTime = 0
                    local:EndTime = 0
                    ! Inserting (DBH 22/02/2007) # 8794 - Overriding midnight
                    local:MidNight = False
                    ! End (DBH 22/02/2007) #8794
                Else ! If f:StartTime > local:EndTime
                    local:StartTime = f:StartTime
                End ! If f:StartTime > local:EndTime
            End ! If f:StartTime > local:StartTime
        End ! If x# = f:StartDate

        If x# = f:EndDate
            If f:EndTime < local:EndTime
                If f:EndTime < local:StartTime
                    local:StartTime = 0
                    local:EndTime = 0
                    ! Inserting (DBH 22/02/2007) # 8794 - Overriding midnight
                    local:MidNight = False
                    ! End (DBH 22/02/2007) #8794
                Else ! If f:EndTime < local:StartTime
                    local:EndTime = f:EndTime
                    local:Midnight = False
                End ! If f:EndTime < local:StartTime
            End ! If f:EndTime < local:EndTime
        End ! If x# = f:EndDate

        If local:Midnight = True
            local:1Time = Deformat('23:59:59',@t4)
            local:1a = 0
            local:1b = 0
            local:1c = 4
            local:1d = 2
        Else ! If local:Midnight = True
            local:1Time = Format(local:EndTime,@t2)
            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End ! If local:Midnight = True

        local:2Time = Format(local:StartTime,@t2)
        local:2a = Sub(local:2Time,4,1)
        local:2b = Sub(local:2Time,3,1)
        local:2c = Sub(local:2time,2,1)
        local:2d = Sub(local:2time,1,1)

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End ! Loop x# = f:StartDate to f:EndDate
    Return INT(local:TotalTime)

local.RealTimeDifferenceCourier             Procedure(Date f:StartDate, Date f:EndDate, Time f:StartTime, Time f:EndTime, String f:Courier)
local:StartTime    Time
local:EndTime      Time
local:MidNight     Byte(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()
local:TotalTime         Long()

Code
    If f:StartDate > f:EndDate
        Return 0
    End ! If f:StartDate > f:EndDate

    Access:COURIER.ClearKey(cou:Courier_Key)
    cou:Courier = f:Courier
    If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
        !Found
        ! Guaranteed that Vodacom won't set these up even though they asked for it. Default to ARC hours (DBH: 19/10/2006)!
        If cou:StartWorkHours = 0 And cou:EndWorkHours = 0
            cou:StartWorkHours = Deformat('08:00:00',@t4)
            cou:EndWorkHours = Deformat('17:00:00',@t4)
        End ! If cou:StartWorkHours = 0 And cou:EndWorkHours = 0
    Else ! If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
        !Error
        ! Inserting (DBH 18/10/2006) # 8327 - Incase the courier can't be found, assume ARC hours
        cou:StartWorkHours = Deformat('08:00:00',@t4)
        cou:EndWorkHours = Deformat('17:00:00',@t4)
        ! End (DBH 18/10/2006) #8327
    End ! If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign

    Loop x# = f:StartDate to f:EndDate
        ! Ignore Sundays (DBH: 25/09/2006)
        If x# % 7 = 0
            Cycle
        End ! If x# % 7 = 0

        Access:COUBUSHR.ClearKey(cbh:TypeDateKey)
        cbh:Courier = f:Courier
        cbh:TheDate = x#
        If Access:COUBUSHR.TryFetch(cbh:TypeDateKey) = Level:Benign
            !Found
            If cbh:ExceptionType = 1
                Cycle
            End ! If cbh:ExceptionType = 0
        Else ! If Access:COUBUSHR.TryFetch(cbh:TypeDateKey) = Level:Benign
            !Error
        End ! If Access:COUBUSHR.TryFetch(cbh:TypeDateKey) = Level:Benign

        local:StartTime = Deformat('00:00:00',@t4)
        local:EndTime = Deformat('23:59:59',@t4)

        local:Midnight = True
        Case x# % 7
        Of 1 ! Monday
            local:StartTime = cou:StartWorkHours
        Of 5 ! Friday
            local:EndTime = cou:EndWorkHours
            local:MidNight = False
        Of 6 ! Saturday
            Cycle
        End ! Case x# % 7

        If x# = f:StartDate
            If f:StartTime > local:StartTime
                If f:StartTime > local:EndTime
                    local:StartTime = 0
                    local:EndTime = 0
                    ! Inserting (DBH 22/02/2007) # 8794 - Overriding midnight
                    local:MidNight = False
                    ! End (DBH 22/02/2007) #8794
                Else ! If f:StartTime > local:EndTime
                    local:StartTime = f:StartTime
                End ! If f:StartTime > local:EndTime
            End ! If f:StartTime > local:StartTime
        End ! If x# = f:StartDate

        If x# = f:EndDate
            If f:EndTime < local:EndTime
                If f:EndTime < local:StartTime
                    local:StartTime = 0
                    local:EndTime = 0
                    ! Inserting (DBH 22/02/2007) # 8794 - Overriding midnight
                    local:MidNight = False
                    ! End (DBH 22/02/2007) #8794
                Else ! If f:EndTime < local:StartTime
                    local:EndTime = f:EndTime
                    local:Midnight = False
                End ! If f:EndTime < local:StartTime
            End ! If f:EndTime < local:EndTime
        End ! If x# = f:EndDate

        If local:Midnight = True
            local:1Time = Deformat('23:59:59',@t4)
            local:1a = 0
            local:1b = 0
            local:1c = 4
            local:1d = 2
        Else ! If local:Midnight = True
            local:1Time = Format(local:EndTime,@t2)
            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End ! If local:Midnight = True

        local:2Time = Format(local:StartTime,@t2)
        local:2a = Sub(local:2Time,4,1)
        local:2b = Sub(local:2Time,3,1)
        local:2c = Sub(local:2time,2,1)
        local:2d = Sub(local:2time,1,1)

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End ! Loop x# = f:StartDate to f:EndDate
    Return INT(local:TotalTime)

local.CustomerPerspective    Procedure(Date f:StartDate, Date f:EndDate, Time f:StartTime, Time f:EndTime, Byte f:Type)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)
local:SatStartTime          Time()
local:SatEndTime            Time()

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

locTakeAwayTime     Long()

Code
    If f:EndDate < f:StartDate
        Return 0
    End !If f:EndDate < f:StartDate

    If f:EndDate = f:StartDate
        If f:EndTime < f:StartTime
            Return 0
        End !If f:EndTime < f:StartTime
    End !If f:EndDate = f:StartDate

    local:SatStartTime = Deformat('08:30:00',@t4)
    local:SatEndTime = Deformat('17:00:00',@t4)

    Loop x# = f:StartDate to f:EndDate
        local:StartTime   = Deformat('00:00:00',@t4)
        local:EndTime     = Deformat('23:59:59',@t4)

        Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
        if (f:Type = 1)
            tra_ali:Account_Number    = tmp:HeadAccountNumber
        else ! if (f:Type = 1)
            tra_ali:Account_Number    = wob:HeadAccountNumber
        end ! if (f:Type = 1)
        if (Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign)
            ! Found
            Access:TRABUSHR.Clearkey(tbh:TypeDateKey)
            tbh:RefNumber    = tra_ali:RecordNumber
            tbh:TheDate    = x#
            if (Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign)
                ! Found
                if (tbh:ExceptionType = 1)
                    ! Don't count Whole Days Off
                    cycle
                end ! if (tbh:ExceptionType = 1)
            else ! if (Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign)
                ! Error
            end ! if (Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign)
        else ! if (Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign)

        If (x# % 7) = 0 ! Sunday
            if (tra_ali:IncludeSunday <> 'YES')
                Cycle
            end ! if (tra_Ali:IncludeSunday <> 'YES')
        End
        If (x# % 7) = 6 ! Saturday
            if (tra_ali:IncludeSaturday <> 'YES')
                Cycle
            end ! if (tra_ali:IncludeSaturday <> 'YES')

        End

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2

        local:2a = 0
        local:2b = 0
        local:2c = 0
        local:2d = 0

        If x# = f:StartDate
            local:StartTime = f:StartTime

            local:2Time = Format(local:StartTime,@t2)

            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)
        End !If x# = f:StartDate
        If x# = f:EndDate
            local:EndTime = f:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = f:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a

        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b

        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10
        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        locTakeAwayTime = 0
!        if (x# <> f:StartDate And x# <> f:endDate And (x# % 7) = 6 and tra:IncludeSaturday = 'YES')
!            ! It's a Saturday, take away time if the shop opens after 8.30
!            if (tra_ali:SatStartWorkHours > local:SatStartTime
!                if (x# = f:StartDate)
!                    ! start counting today
!                    if (local:EndTime => tra_ali:SatStartWorkHours)
!                        ! ends after the official opening time
!                        if (local:StartTime > local:SatStartTime and local:StartTime < tra_ali:SatStartWorkHours)
!                            ! starts after official open, but before trade opens
!                            locTakeAwayTime += (tra_ali:SatStartWorkHours - local:StartTime)
!                        elsif (local:StartTime < local:SatStartTime)
!                            ! starts before official open
!                            locTakeAwayTime += (tra_ali:SatStartWorkHours - local:SatStartTime)
!                        end ! if (local:StartTime > Deformat('08:30:00',@t4) and local:StartTime < tra_ali:SatStartWorkHours)
!                    elsif (local:EndTime < tra_ali:SatStartWorkHours)
!                        ! ends before the trade opening time
!                        if (local:StartTime > local:SatStartTime and local:StartTime < local:EndTime)
!                            ! starts after official open, but before end time
!                            locTakeAwayTime += (local:EndTime - local:StartTime)
!                        elsif (local:StartTime < local:SatStartTime)
!                            ! starts before official open
!                            locTakeAwayTime += (local:EndTime - local:SatStartTime)
!                        end ! if (local:StartTime > Deformat('08:30:00',@t4) and local:StartTime < tra_ali:SatStartWorkHours)
!                    end !if (local:EndTime > local:SatStartTime)
!                end ! if (x# = f:StartDate)
!                if (x# = f:EndDate)
!                    if (local:StartTime => tra_ali:SatEndWorkHours)
!
!                end ! if (x# = f:EndDate)
!            end ! if (tra_ali:SatStartWorkHours > Deformat('08:30:00',@t4))
!            ! Take away time if shop closes before 17.00
!            if (tra_ali:SatEndWorkHours < Deformat('17:00:00',@t4))
!                locTakeAwayTime += (Deformat('17:00:00',@t4) - tra_ali:SatEndWorkHours)
!            end !if (tra_ali:SatEndWorkHours < Deformat('17:00:00',@t4))
!        end !if (x# <> f:StartDate And x# <> f:endDate And (x% % 7) = 6)
        if ((x# % 7) = 6 and tra_ali:IncludeSaturday = 'YES')
            if (tra_ali:SatStartWorkHours > local:SatStartTime)
                ! clock starts after official start time
                if (local:StartTime => tra_ali:SatStartWorkHours)
                    ! starts after trade hours. No deductions necessary
                end ! if (local:StartTime > tra_ali:SatStartWorkHours)
                if (local:StartTime <= local:SatStartTime)
                    ! start before official time
                    if (local:EndTime <= local:SatStartTime)
                        ! finish before official time. No time deduction
                    end ! if (local:EndTime < local:SatStartTime)
                    if (local:EndTime > tra_ali:SatStartWorkHours)
                        ! finish after start time.
                        ! deduct difference between trade start and official
                        locTakeAwayTime += (tra_ali:SatStartWorkHours - local:SatStartTime)
                    end ! if (local:EndTime > tra_ali:SatStartWorkHours)
                    if (local:EndTime <= tra_ali:SatStartWorkHours)
                        ! finish after official hours, but before trade start
                        ! deduct time from offical start to end time
                        locTakeAwayTime += (local:EndTime - local:SatStartTime)
                    end ! if (local:EndTime < tra_ali:SatStartWorkHours)
                end ! if (local:StartTime < local:SatStartTime)
                if (local:StartTime > local:SatStartTime and local:StartTime < tra_ali:SatStartWorkHours)
                    ! starts after official start, but before trade start
                    if (local:EndTime >= tra_ali:SatStartWorkHours)
                        ! finish after trade start time.
                        ! deduct actual start time to trade start time
                        locTakeAwayTime += (tra_ali:SatStartWorkHours - local:StartTime)
                    end ! if (local:EndTime >= tra_ali:SatStartWorkHours)
                    if (local:EndTime < tra_ali:SatStartWorkHours)
                        ! finish before trade start time
                        ! deduct everything
                        locTakeAwayTime += (local:EndTime - local:StartTime)
                    end ! if (local:EndTime < tra_ali:SatStartWorkHours)
                end ! if (local:StartTime > local:SatStartTime and local:StartTime < tra_ali:SatStartWorkHours)
            end ! if (tra_ali:SatStartWorkHours > local:SatStartTime)

            if (tra_ali:SatEndWorkHours < local:SatEndTime)
                ! Ends before official time
                if (local:EndTime <= tra_ali:SatEndWorkHours)
                    ! ends before trade hours. no deductions
                end ! if (local:EndTime <= tra_ali:SatEndWorkHours)
                if (local:EndTime >= local:SatEndTime)
                    ! ends after official time
                    if (local:StartTime => local:SatEndTime)
                        ! starts after official time. no deductions
                    end ! if (local:StartTime > local:SatEndTime)
                    if (local:StartTime => tra_ali:SatEndWorkHours)
                        ! starts after trade hours, but before official end
                        ! deduct time from official end to trade end
                        ! Change --- Count from end of trade hours to end of official hours (DBH: 01/10/2009) #10944
                        !LocTakeAwayTime += (local:SatEndTime - tra_ali:SatEndWorkHours)
                        ! To --- (DBH: 01/10/2009) #10944
                        locTakeAwayTime += (local:SatEndTime - tra_ali:SatEndWorkHours)
                        ! end --- (DBH: 01/10/2009) #10944
                    end ! if (local:StartTime > tra_ali:SatEndWorkHours)
                    if (local:StartTime < tra_ali:SatEndWorkHours)
                        ! start before trade end hours
                        ! deduct time from trade end to official end
                        locTakeAwayTime += (local:SatEndTime - tra_ali:SatEndWorkHours)
                    end ! if (local:StartTime <= tra_ali:SatEndWorkHours)
                end ! if (local:EndTime >= local:SatEndTime)
                if (local:EndTime < local:SatEndTime and local:EndTime > tra_ali:SatEndWorkHours)
                    ! ends after trade end, but before official end
                    if (local:StartTime <= tra_ali:SatEndWorkHours)
                        ! start before trade end time
                        ! deduct actual end time from trade end
                        locTakeAwayTime += (local:EndTime - tra_ali:SatEndWorkHours)
                    end ! if (local:StartTime <= tra_ali:SatEndWorkHours)
                    if (local:StartTime > tra_ali:SatEndWorkHours)
                        ! start after trade hours and end before official end
                        ! deduct everything
                        locTakeAwayTime += (local:EndTime - local:StartTime)
                    end ! if (local:StartTime > tra_ali:SatEndWorkHours)
                end ! if (local:EndTime < local:SatEndTime and local:EndTime < tra_ali:SatEndWorkHours)
            end ! if (tra_ali:SatEndWorkHours < local:SatEndTime)
        end ! if ((x# % 6) = 6 and tra_ali:IncludeSaturday = 'YES')
        locTakeAwayTime = ABS(locTakeAwayTime / 6000)

        local:TimeDifference -= (locTakeAwayTime)
        if (local:TimeDifference < 0)
            local:TimeDifference = 0
        end !if (local:TimeDifference < 0)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = f:StartDate to f:EndDate

    If Command('/DEBUG')
        if (locTakeAwayTime > 0)
            LinePrint('Take Away Time: ' & locTakeAwayTime,'c:\tat.log')
        end ! if (locTakeAwayTime > 0)
    End



    Return INT(local:TotalTime)
Local.DrawColumns       Procedure(Byte func:Type)
Code
!    E1.WriteToCell('SB Job No','A11')
!    E1.WriteToCell('BranchSpecific','B11')
!    E1.WriteToCell('Head Account','C11')
!    E1.WriteToCell('Sub Account','D11')
!    E1.WriteToCell('Manufacturer','E11')
!    E1.WriteTocell('Model Number','F11')
!    E1.WriteToCell('Warranty','G11')
!    E1.WriteToCell('Date','H11')
!    E1.WriteToCell('Time','I11')
!    E1.WriteToCell('Date','J11')
!    E1.WriteToCell('Time','K11')
!    E1.WriteToCell('Days','L11')
!    E1.WriteToCell('HH:MM','M11')
!    E1.WriteToCell('Days','N11')
!    E1.WriteToCell('HH:MM','O11')
!    E1.WriteToCell('Days','P11')
!    E1.WriteToCell('HH:MM','Q11')
!    E1.WriteToCell('Days','R11')
!    E1.WriteToCell('HH:MM','S11')
!    E1.WriteToCell('Days','T11')
!    E1.WriteToCell('HH:MM','U11')
!    E1.WriteToCell('Days','V11')
!    E1.WriteToCell('HH:MM','W11')
!    E1.WriteToCell('Days','X11')
!    E1.WriteToCell('HH:MM','Y11')
!    E1.WriteToCell('Loan Issued','Z11')

    E1.WriteToCell('Jobs Booked In','H9')
    E1.WriteToCell('Jobs Completed 705','J9')
    E1.WriteToCell('Customer Perspective','L9')

    Case func:Type
        Of 0 !RRC
            E1.WriteToCell('RRC In Control','N9')
            E1.WriteToCell('ARC/3RD Party Repair','X9')
            E1.WriteToCell('RRC','P8')
        Of 1 !ARC
            E1.WriteToCell('ARC In Control','N9')
            E1.WriteToCell('RRC/3RD Party Repair','X9')
            E1.WriteToCell('ARC','P8')

    End !Case func:Type
    E1.WriteToCell('Ready To Despatch 810/110','P9')
    E1.WriteToCell('Customer Collection','R9')
    E1.WriteToCell('Wait Time','T9')
    E1.WriteToCell('Courier Time','V9')

    E1.WriteToCell('OUT OF CONTROL','V8')

    Local.DrawBox('H9','I9','H9','I9',color:Silver)
    Local.DrawBox('J9','K9','J9','K9',color:Silver)
    Local.DrawBox('L9','M9','L9','M9',color:Silver)

    Local.DrawBox('N9','O9','N9','O9',color:Silver)
    Local.DrawBox('P9','Q9','P9','Q9',color:Silver)
    Local.DrawBox('R9','S9','R9','S9',color:Silver)
    Local.DrawBox('T9','U9','T9','U9',color:Silver)
    Local.DrawBox('V9','W9','V9','W9',color:Silver)
    Local.DrawBox('X9','Y9','X9','Y9',color:Silver)
    Local.DrawBox('N8','S8','N8','S8',color:Silver)
    Local.DrawBox('T8','Y8','T8','Y8',color:Silver)
Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Window
Prog.ProgressSetup        Procedure(Long func:Records)
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
Code
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}
        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
Code
    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.percentText = '0% Completed'

Prog.InsideLoop         Procedure(<String func:String>)
Code
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
Code
    Prog.UserText = Clip(func:String)
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWinow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ProgressFinish     Procedure()
Code
    Prog.ProgressThermometer = 100
    Prog.PercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
    Code
    Yield()
    Prog.RecordsProcessed += 1
    If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    End
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            return 1
        Of Button:No
        End !CASE
    End!If cancel# = 1

    return 0


BRW8.Fetch PROCEDURE(BYTE Direction)

GreenBarIndex   LONG
  CODE
  PARENT.Fetch(Direction)


BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 2                                ! Set icon from icon list
  ELSE
    SELF.Q.tmp:Tag_Icon = 1                                ! Set icon from icon list
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue

XFiles2 PROCEDURE                                          ! Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(100,72,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

RecolourWindow      Routine
    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    If 0{prop:Color} > -1; Exit.
    If 0{prop:Color} <> -1
        0{prop:Color} = -1
    End
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles2')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do RecolourWindow
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('XFiles2',QuickWindow)                      ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  IF SELF.Opened
    INIMgr.Update('XFiles2',QuickWindow)                   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

SentToHub            PROCEDURE  (func:RefNumber)           ! Declare Procedure
Bryan       Class
CompFieldColour       Procedure()
            End
  CODE
    RETURN  vod.JobSentToHub(func:RefNumber)



!If jobe:HubRepair Or jobe:HubRepairDate <> ''
!    Return Level:Fatal
!Else !If jobe:HubRepair
!    Access:LOCATLOG.ClearKey(lot:NewLocationKey)
!    lot:RefNumber   = func:RefNumber
!    lot:NewLocation = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
!    If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
!        Return Level:Fatal
!    End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
!End !If jobe:HubRepair



!Return Level:Benign
RecolourWindow      Routine
    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    If 0{prop:Color} > -1; Exit.
    If 0{prop:Color} <> -1
        0{prop:Color} = -1
    End
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
Bryan       Class
CompFieldColour       Procedure()
            End
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)
  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
RecolourWindow      Routine
    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    If 0{prop:Color} > -1; Exit.
    If 0{prop:Color} <> -1
        0{prop:Color} = -1
    End
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
Bryan       Class
CompFieldColour       Procedure()
            End
TempNoLogging         long(0)
  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
RecolourWindow      Routine
    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    If 0{prop:Color} > -1; Exit.
    If 0{prop:Color} <> -1
        0{prop:Color} = -1
    End
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE) ! Generated from procedure template - Window

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

RecolourWindow      Routine
    Do RecolourWindow:Window
    If ?MainTextPrompt{Prop:FontColor} < 0
        If ?MainTextPrompt{prop:FontColor} <> -1
            ?MainTextPrompt{prop:FontColor} = -1
        End
    End
    If ?MainTextPrompt{Prop:Color} < 0
        If ?MainTextPrompt{prop:Color} <> -1
            ?MainTextPrompt{prop:Color} = -1
        End
    End
    If ?HALink{Prop:FontColor} < 0
        If ?HALink{prop:FontColor} <> -1
            ?HALink{prop:FontColor} = -1
        End
    End
    If ?HALink{Prop:Color} < 0
        If ?HALink{prop:Color} <> -1
            ?HALink{prop:Color} = -1
        End
    End
    If ?TimerCounter{Prop:FontColor} < 0
        If ?TimerCounter{prop:FontColor} <> -1
            ?TimerCounter{prop:FontColor} = -1
        End
    End
    If ?TimerCounter{Prop:Color} < 0
        If ?TimerCounter{prop:Color} <> -1
            ?TimerCounter{prop:Color} = -1
        End
    End
    If ?DontShowThisAgain{prop:Font,3} < 0
        If ?DontShowThisAgain{prop:Font,3} <> -1
            ?DontShowThisAgain{prop:Font,3} = -1
        End
    End ! If ?DontShowThisAgain{prop:FontColor} < 0
    IF ?DontShowThisAgain{prop:Color} < 0
        If ?DontShowThisAgain{prop:Color} <> -1
            ?DontShowThisAgain{prop:Color} = -1
        End
    End ! IF ?DontShowThisAgain{prop:Color} < 0
    If ?DontShowThisAgain{prop:Trn} <> 1
        ?DontShowThisAgain{prop:Trn} = 1
    End

RecolourWindow:Window       Routine
    If 0{prop:Color} > -1; Exit.
    If 0{prop:Color} <> -1
        0{prop:Color} = -1
    End
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
  SELF.Open(window)                                        ! Open window
  Do RecolourWindow
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
HelpBrowser PROCEDURE (func:Path)                          ! Generated from procedure template - Window

fepath               STRING(255)                           !
Window               WINDOW('Online Help'),AT(522,0,154,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(09A6A7CH),TILED,SYSTEM,GRAY,MAX,RESIZE,IMM
                       BUTTON,AT(0,0),USE(?Button1),TRN,FLAT,ICON('closep.jpg'),STD(STD:Close)
                       PROMPT('Close Online Help'),AT(72,8),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(-3,25,159,404),USE(?feControl)
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Open                   PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
Resize                 PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END


                     ! Start: FE Class Declaration
ThisViewer1          class(FeBrowser)
                     END
                     !End: FE Class Declaration


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

RecolourWindow      Routine
    Do RecolourWindow:Window
    If ?Prompt1{prop:FontColor} < 0
        If ?Prompt1{prop:FontColor} <> -1
            ?Prompt1{prop:FontColor} = -1
        End
    End
    If ?Prompt1{prop:Color} < 0
        If ?Prompt1{prop:Color} <> -1
            ?Prompt1{prop:Color} = -1
        End
    End
    If ?feControl{prop:Fill} < 0
        If ?feControl{prop:Fill} <> -1
            ?feControl{prop:Fill} = -1
        End
    End

RecolourWindow:Window       Routine
    If 0{prop:Color} > -1; Exit.
    If 0{prop:Color} <> -1
        0{prop:Color} = -1
    End
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
feGetFileFromUser_ThisViewer1  routine
  ThisViewer1.fe_AppPath = Path()           ! Used to restore current path after selection
  if ThisViewer1.fe_LastPath <> ''          ! Var set below
    SetPath (ThisViewer1.fe_LastPath)       ! Set path to location of last selected file
  end
  ThisViewer1.fe_RestorePath = ThisViewer1.fe_Path  ! current document's path
  ThisViewer1.fe_Path = ''
  if not FileDialog('Choose File to View', ThisViewer1.fe_Path, 'HTML Files|*.htm;*.html;*.mht|Text Files|*.txt|Image Files|*.gif;*.jpeg;*.jpg;*.png;*.art;*.au;*.aiff;*.xbm|XML Files|*.xml|EML Files|*.eml|All Files|*.*', 10000b)
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path to app path if cancelled
    ThisViewer1.fe_Path = ThisViewer1.fe_RestorePath
  else
    ThisViewer1.fe_LastPath = Path()        ! Path of selected file
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path
    ThisViewer1.Load (ThisViewer1.fe_Path)  ! Load the selected file
  end
 exit

fePathControlAccepted_ThisViewer1  routine
  ThisViewer1.fe_Path = ''
  ThisViewer1.Load (ThisViewer1.fe_Path)
  exit

feWindowClosing_ThisViewer1  routine
  ThisViewer1.Kill(2)  ! GJxxyy123 WIP test code, 5 Oct 2005
 exit

feRecoveryCode_ThisViewer1  routine
 ! This routine is obsolete.  Please read the docs or contact support@capesoft.com
 exit

feLoadInitialDocument_ThisViewer1  routine
  ! FileExplorer - Loading initial document as window opens
  ThisViewer1.fe_Path = fepath
  ThisViewer1.Load (ThisViewer1.fe_Path)
 exit

feWindowTakeFieldEvent_fePathControl_EventAlertKey_ThisViewer1  routine
  case keycode()
    of EnterKey
      post(event:accepted, )
    of CtrlEnter
      post(event:accepted, )
  end
 exit



ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('HelpBrowser')
  fepath = Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & Clip(func:Path) & '.htm'
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  ThisViewer1.TempDirLocation = 0    ! temp files stored with the exe
  
   ! The feBrowser object uses "Early Binding" at this stage.  At some point we might
   ! make it optional to use "Late Binding" instead.  This switch sets which approach
   ! we use.  Don't change it for now...
   ! ThisViewer1.UseLateBinding = Glo:UseLateBinding
  
  
   ! Uncomment this line of code to override the above code where the property is set...
   ! For testing purposes...  Set the switch in the ProgramOptions screen (disabled by default).
   ! ThisViewer1.AutoDialDefaultInternetConnection = Glo:DUNAutoDialOption
   ! 0 - do not dial the default dial up connection
   ! 1 - dial the "Windows Default" connection
   ! 2 - establish the default connection, then dial it
  
   ThisViewer1.BetaCallbacksOn = 0  ! GJxxyy FILEEXPLORER TEST CODE
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do RecolourWindow
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread)                         ! Controls will spread out as the window gets bigger
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('HelpBrowser',Window)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('HelpBrowser',Window)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  PARENT.Open
  ThisViewer1.Init (?feControl, , , , 1)


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
   ThisViewer1.TakeEvent()
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  
    OF EVENT:feCallback
      ! The File Explorer dll will post this event if you return fe:DivertNavigation from the
      ! (virtual) method ThisViewer::EventCallback.
  
    OF EVENT:feDelayedClose  ! GJxxyy123 WIP added 051005
      ThisViewer1.Kill()
  
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
        ThisViewer1._WindowClosing()  ! GJ added 280205
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      do feWindowClosing_ThisViewer1
    OF EVENT:OpenWindow
      do feLoadInitialDocument_ThisViewer1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Button1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Button1
  SELF.SetStrategy(?feControl, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom) ! Override strategy for ?feControl
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Prompt1


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
  ! Auto-Resizing the File Explorer COM Object
  ThisViewer1.Resize ()
  RETURN ReturnValue


