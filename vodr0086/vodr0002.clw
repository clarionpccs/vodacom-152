

   MEMBER('vodr0086.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('VODR0001.INC'),ONCE        !Req'd for module callout resolution
                     END


SwapIMEIReport PROCEDURE                              !Generated from procedure template - Window

tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:ExportFile       STRING(255),STATIC
tmp:SNDType          STRING(1)
tmp:VersionNumber    STRING(30)
JobsQueue            QUEUE,PRE(jobs)
JobNumber            LONG
ReportDate           DATE
                     END
window               WINDOW('Swap IMEI Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(292,258),USE(?OK),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(364,258),USE(?Close),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(632,8,36,32),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Help.jpg')
                       PANEL,AT(244,164,192,92),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Start Date'),AT(248,190),USE(?tmp:StartDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(320,188,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Start Date'),TIP('Start Date'),UPR
                       BUTTON,AT(388,184),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('End Date'),AT(248,206),USE(?tmp:EndDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(320,206,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('End Date'),TIP('End Date'),UPR
                       PROMPT('Report Version'),AT(248,246),USE(?ReportVersion),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(388,202),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Swap IMEI Export'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
local       Class
ValidateIMEIOK      Procedure(String f:IMEINumber),Byte
            End ! local       Class
ExportFile    File,Driver('ASCII'),Pre(exp),Name(tmp:ExportFile),Create,Bindable,Thread
Record              Record
OutGroup                Group
Line1                       String(500)
                        End ! OutGroup                Group
                    End
                End

ExportHeader    Group,Over(exp:OutGroup),Pre(head)
ID                  String(2)
Space1              String(1)
FileName            String(40)
Space2              String(1)
FileCreationDate    String(8)
Space3              String(1)
                End ! ExportHeader    Group,Over(exp:OutGroup),Pre(head)

ExportData      Group,Over(exp:OutGroup),Pre(data)
ID                  String(2)
Space1              String(1)
OriginalIMEI        String(15)
Space2              String(1)
NewIMEI             String(15)
Space3              String(1)
SwapDate            String(8)
Space4              String(1)
SNDType             String(1)
                End ! ExportData      Group,Over(exp:Outfroup),Pre(data)
 !TB13287 - J - 12/01/15 - add new field to end - SNDType to be either 'S' or blank

ExportTrailer   Group,Over(exp:OutGroup),Pre(trail)
ID                  String(2)
Space1              String(1)
FileName            String(40)
Space2              String(1)
NumberOfRecords     String(6)
Space3              String(1)
                End ! ExportTrailer   Group,Over(exp:OutGroup),Pre(trail)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

Reporting       Routine
Data
local:Path          CString(255)
local:FileName      CString(255)
local:RecordCount   Long()
local:ExchangeJobs  Long()
local:ThirdPartyJobs    Long()
local:CancelPressed Byte(0)
Code

    !If folder doesn't exist. Create it
    local:Path = Path() & '\Exchange Export'
    If ~Exists(local:Path)
        x# = MkDir(local:Path)
    End ! If ~Exists(tmp:Path)

    !TB13287 - J - 12/01/15 - change export file to new subfolder'SND IMEI'
    local:Path = Path() & '\Exchange Export\SND IMEI'
    If ~Exists(local:Path)
        x# = MkDir(local:Path)
    End ! If ~Exists(tmp:Path)

    !TB13287 - J - 12/01/15 - also check for the exporting path as well
    glo:IMEIExceptionPath = GETINI('IMEIFTP','Exception',,clip(path())&'\SB2KDEF.INI')
    if ~exists(glo:IMEIExceptionPath)
        x# = MkDir(glo:IMEIExceptionPath)
    END
    !End TB13287

    If Access:SWAPIMEI.PrimeRecord() = Level:Benign
        If Access:SWAPIMEI.TryInsert() = Level:Benign
            ! Insert Successful
            !TB13251 - J - 17/04/15 - change name to include "SB"
            ! #13551 Name change: "IMEI_SND_SB_" (DBH: 29/05/2015)
            !SND Exchange IMEIDB Report file name which is specced & delivered as IMEISWAP_ttttt_yyyymmdd.TXT could be changed to: IMEISWAP_SB_ttttt_yyyymmdd.TXT.
            local:FileName = 'IMEI_SND_SB_' & Format(swp:RecordNumber,@n06) & '_' & Format(Today(),@d012) & '.TXT'
            tmp:ExportFile = local:Path & '\' & local:FileName
        Else ! If Access:SWAPIMEI.TryInsert() = Level:Benign
            ! Insert Failed
            Access:SWAPIMEI.CancelAutoInc()
            If glo:Automatic
                ErrorLog('Error: Unable to autonumber file')
            Else ! If glo:Automatic
                Case Missive('An error occured when creating a auto number for the file.','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive
            End ! If glo:Automatic
            Exit
        End ! If Access:SWAPIMEI.TryInsert() = Level:Benign
    Else !If Access:SWAPIMEI.PrimeRecord() = Level:Benign
        If glo:Automatic
            ErrorLog('Error: Unable to autonumber file')
        Else ! If glo:Automatic
            Case Missive('An error occured when creating a auto number for the file.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
        End ! If glo:Automatic
        Exit
    End !If Access:SWAPIMEI.PrimeRecord() = Level:Benign

    Remove(ExportFile)
    Create(ExportFile)
    Open(ExportFile)

    Prog.ProgressSetup(Records(AUDIT))

    ErrorLog('=== Begin Export ===')
    ErrorLog('Export Filename: ' & local:Path & '\' & local:FileName)

    Clear(exp:Record)
    head:ID     = 'HR'
    !head:Space1 = ' '
    head:Space1 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
    head:FileName = local:FileName
    !head:Space2 = ' '
    head:Space2 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
    head:FileCreationDate = Format(Today(),@d012)
    !head:Space3 = ' '
    head:Space3 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
    Add(ExportFile)

    Free(JobsQueue)

    Access:AUDIT.ClearKey(aud:ActionOnlyKey)
    aud:Action = 'EXCHANGE UNIT ATTACHED TO JOB'
    aud:Date   = tmp:StartDate
    Set(aud:ActionOnlyKey,aud:ActionOnlyKey)
    Loop
        If Access:AUDIT.NEXT()
           Break
        End !If
        If aud:Action <> 'EXCHANGE UNIT ATTACHED TO JOB'      |
        Or aud:Date   > tmp:EndDate      |
            Then Break.  ! End If

        If Prog.InsideLoop()
            local:CancelPressed = True
            Break
        End ! If Prog.InsideLoop()

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = aud:Ref_Number
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            ! Found

        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            ! Error
            Cycle
        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

!from Skype messages
![15:52:48] Jeremy Cole: Some time ago it was decided to move the audit notes from the AUDIT.DAT file to the AUDIT2.DAT file
![15:53:44] Jeremy Cole: This programme was never ammended to look in AUDIT2.DAT for the notes, and it is looking in AUDIT for the field and not finding it
        !get the notes
        Access:Audit2.clearkey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:record_number
        if access:Audit2.fetch(aud2:AUDRecordNumberKey)
            !error
            If glo:Automatic
                ErrorLog('Error: Unable to trace audit notes for job record number '&clip(aud:Ref_Number)&' ommitting from report')
            Else ! If glo:Automatic
                Case Missive('Unable to trace audit notes for job record number '&clip(aud:Ref_Number)&' ommitting from report','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive
            End ! If glo:Automatic
        END

        If Instring('UNIT NUMBER: ' & job:Exchange_Unit_Number,aud2:Notes,1,1)
            ! If this entry relates to the exchange unit currently attached (DBH: 26-06-2006)
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                ! Found
                If local.ValidateIMEIOK(job:ESN) = False
                    Cycle
                End ! If local.ValidateIMEIOK(job:ESN) = False
                If local.ValidateIMEIOK(xch:ESN) = False
                    Cycle
                End ! If local.ValidateIMEIOK(xch:ESN) = False

                ! Inserting (DBH 26/06/2006) #7386 - Check to see if this job has already been reported, and if it's later
                jobs:JobNumber = job:Ref_Number
                Get(JobsQueue,jobs:JobNumber)
                If ~Error()
                    If jobs:ReportDate < aud:Date
                        Delete(JobsQueue)
                    Else ! If jobs:ReportDate < aud:Date
                        Cycle
                    End ! If jobs:ReportDate < aud:Date
                End ! If ~Error()
                ! End (DBH 26/06/2006) #7386

                Do SetSNDType

                Clear(exp:Record)
                data:ID     = 'DR'
                !data:Space1 = ' '
                data:Space1 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
                data:OriginalIMEI = job:ESN
                !data:Space2 = ' '
                data:Space2 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
                data:NewIMEI = xch:ESN
                !data:Space3 = ' '
                data:Space3 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
                data:SwapDate = Format(aud:Date,@d012)
                !data:Space4 = ' '
                data:Space4 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
                data:SNDType = tmp:SNDType      !TB13287 - J - 12/01/15 - new field showing if this is SND
                Add(ExportFile)

                ! Inserting (DBH 26/06/2006) #7386 - Add the job to a queue to make sure it isn't picked up twice
                jobs:JobNumber = job:Ref_Number
                jobs:ReportDate  = aud:Date
                Add(JobsQueue)
                ! End (DBH 26/06/2006) #7386

                local:RecordCount += 1
                local:ExchangeJobs += 1
                Prog.ProgressText('Record(s) Found: ' & local:RecordCount)
            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                ! Error
                Cycle
            End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        End ! If Insring('UNIT NUMBER: ' & job:Exchange_Unit_Number,aud:Notes,1,1)
    End !Loop

    Free(JobsQueue)

    ErrorLog('Exchange Jobs Found: ' & local:ExchangeJobs)

    If local:CancelPressed = False
        Access:AUDIT.ClearKey(aud:ActionOnlyKey)
        aud:Action = '3RD PARTY AGENT: UNIT RETURNED'
        aud:Date   = tmp:StartDate
        Set(aud:ActionOnlyKey,aud:ActionOnlyKey)
        Loop
            If Access:AUDIT.NEXT()
               Break
            End !If
            If aud:Action <> '3RD PARTY AGENT: UNIT RETURNED'      |
            Or aud:Date   > tmp:EndDate      |
                Then Break.  ! End If
            If Prog.InsideLoop()
                Break
            End ! If Prog.LoopInside()

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = aud:Ref_Number
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                ! Found

            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                ! Error
                Cycle
            End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign


            !get the notes
            Access:Audit2.clearkey(aud2:AUDRecordNumberKey)
            aud2:AUDRecordNumber = aud:record_number
            if access:Audit2.fetch(aud2:AUDRecordNumberKey)
                !error
                If glo:Automatic
                    ErrorLog('Error: Unable to trace audit notes for job record number '&clip(aud:Ref_Number)&' ommitting from report')
                Else ! If glo:Automatic
                    Case Missive('Unable to trace audit notes for job record number '&clip(aud:Ref_Number)&' ommitting from report','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                End ! If glo:Automatic
            END


            If Instring('NEW I.M.E.I. NO.: ' & Clip(job:ESN),aud2:Notes,1,1)
                ! If this entry relates to the current IMEI Number (DBH: 26-06-2006)
                Access:JOBTHIRD.Clearkey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                Loop ! Begin JOBTHIRD Loop
                    If Access:JOBTHIRD.Next()
                        Break
                    End ! If !Access
                    If jot:RefNumber <> job:Ref_Number
                        Break
                    End ! If
                    If jot:InIMEI = job:ESN
                        If local.ValidateIMEIOK(jot:OriginalIMEI) = False
                            Cycle
                        End ! If local:ValidateIMEIOK(jot:OriginalIMEI) = False
                        If local.ValidateIMEIOK(job:ESN) = False
                            Cycle
                        End ! If local:ValidateIMEIOK(job:ESN) = False

                        ! Inserting (DBH 26/06/2006) #7386 - Check to see if this job has already been reported, and if it's later
                        jobs:JobNumber = job:Ref_Number
                        Get(JobsQueue,jobs:JobNumber)
                        If ~Error()
                            If jobs:ReportDate < aud:Date
                                Delete(JobsQueue)
                            Else ! If jobs:ReportDate < aud:Date
                                Cycle
                            End ! If jobs:ReportDate < aud:Date
                        End ! If ~Error()
                        ! End (DBH 26/06/2006) #7386

                        Do SetSNDType

                        Clear(exp:Record)
                        data:ID     = 'DR'
                        !data:Space1 = ' '
                        data:Space1 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
                        data:OriginalIMEI = jot:OriginalIMEI
                        !data:Space2 = ' '
                        data:Space2 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
                        data:NewIMEI = job:ESN
                        !data:Space3 = ' '
                        data:Space3 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
                        data:SwapDate = Format(aud:Date,@d012)
                        !data:Space4 = ' '
                        data:Space4 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
                        data:SNDType = tmp:SNDType      !TB13287 - J - 12/01/15 - new field showing if this is SND
                        Add(ExportFile)

                        ! Inserting (DBH 26/06/2006) #7386 - Add the job to a queue to make sure it isn't picked up twice
                        jobs:JobNumber = job:Ref_Number
                        jobs:ReportDate  = aud:Date
                        Add(JobsQueue)
                        ! End (DBH 26/06/2006) #7386

                        local:RecordCount += 1
                        local:ThirdPartyJobs += 1
                        Prog.ProgressText('Record(s) Found: ' & local:RecordCount)
                        Break
                    End ! If jot:InIMEI = job:ESN
                End ! End JOBTHIRD Loop
            End ! If Instring('PREVIOUS I.M.E.I.',aus:Notes,1,1)
        End !Loop
    End ! If local:CancelPressed = False
    ErrorLog('Third Party Exchanges: ' & local:ThirdPartyJobs)

    Clear(exp:Record)
    trail:ID        = 'TR'
    !trail:Space1    = ' '
    trail:Space1 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
    trail:FileName  = local:FileName
    !trail:Space2    = ' '
    trail:Space2 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
    trail:NumberOfRecords = Format(local:RecordCount,@n06)
    !trail:Space3    = ' '
    trail:Space3 = '<9>'   !TB13287 - J - 12/01/15 - change space deliniation to tab
    Add(ExportFile)
    Close(ExportFile)
    Prog.ProgressFinish()

    If local:RecordCount > 0
        ErrorLog('Begin FTP Process')
        If FTP_JumpStart(tmp:ExportFile,local:FileName) = True
            ErrorLog('FTP Completed Successfully')
        End ! If FTP_JumpStart(tmp:ExportFile,local:FileName) = True
    End ! If local:RecordCount > 0

    ErrorLog('Export Completed. Total Lines: ' & local:RecordCount)
SetSNDType      Routine

    !TB13287 - J - 12/01/15 - is this a specific needs job?
    tmp:SNDType = ' '

    Access:jobse3.clearkey(jobe3:KeyRefNumber)
    jobe3:RefNumber = job:Ref_number
    if access:jobse3.fetch(jobe3:KeyRefNumber)
        !error
    ELSe
        if jobe3:SpecificNeeds then
            tmp:SNDType = 'S'
        END !if specific needs
    END !if record found

    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020649'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SwapIMEIReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Relate:JOBSE3.Open
  Relate:SWAPIMEI.Open
  Access:JOBS.UseFile
  Access:JOBTHIRD.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  tmp:StartDate = Today()
  tmp:EndDate = Today()
  
  ! Inserting (DBH 26/06/2006) #7386 - Has this report been run from ServiceBase, i.e. not automatic
  glo:Automatic = True
  If Instring('%',Command(),1,1)
      glo:Automatic = False
  End ! If Instring('%',Command(),1,1)
  ! End (DBH 26/06/2006) #7386
  
  OPEN(window)
  SELF.Opened=True
  !========== Set Verison Number ===============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = Clip(tmp:VersionNumber) & '5004'
  ?ReportVersion{prop:Text} = 'Report Version: ' & Clip(tmp:VersionNumber)
  If glo:Automatic = True
      Post(Event:Accepted,?OK)
  End ! If glo:Automatic = True
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
    Relate:JOBSE3.Close
    Relate:SWAPIMEI.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      Do Reporting
      Post(Event:CloseWindow)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020649'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020649'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020649'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.ValidateIMEIOK        Procedure(String f:IMEINumber)
Code
    If Len(Clip(f:IMEINumber)) <> 15
        Return False
    End ! If Len(Clip(f:IMEINumber)) <> 15

    Loop x# = 1 To Len(Clip(f:IMEINumber))
        If Val(Sub(f:IMEINumber,x#,1)) < 48 Or Val(Sub(f:IMEINumber,x#,1)) > 57
            Return False
        End ! If Val(Sub(f:IMEINumber,x#,1)) < 48 Or Val(Sub(f:IMEINumber,x#,1) > 57))
    End ! Loop x# = 1 To Len(Clip(f:IMEINumber))

    Return True
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
