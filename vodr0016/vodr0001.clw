

   MEMBER('vodr0016.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


XFiles PROCEDURE                                      !Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Ok,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:SRNTEXT.Open
  SELF.FilesOpened = True
  OPEN(QuickWindow)
  SELF.Opened=True
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

PLReportByManufacturer PROCEDURE                      !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
ParameterStuff       GROUP,PRE()
LOC:EndDate          DATE
LOC:StartDate        DATE
                     END
Local_Stuff          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase')
CommentText          STRING(100)
CompanyName          STRING(30)
Courier              STRING(30)
DesktopPath          STRING(255)
FileName             STRING(255)
FranchiseJobNumber   STRING(20)
JobNumber            LONG
LastColumn           STRING('L')
LineCost             DECIMAL(7,2)
Path                 STRING('C:\ {252}')
ProgramName          STRING(100)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
VATCode              STRING(2)
                     END
ProgressBar__Stuff   GROUP,PRE()
Progress:Text        STRING(100)
RecordCount          LONG
                     END
Misc_Group           GROUP,PRE()
AccountChange        BYTE
InvoiceOK            BYTE
OPTION1              SHORT
Result               BYTE
StockOK              BYTE
tmp:FirstModel       STRING(30)
WebJOB_OK            BYTE
                     END
SummaryQueue         QUEUE,PRE(sq)
Manufacturer         STRING(30)
TotalUnitsRepaired   LONG
WarrantyCount        LONG
WarrRevenueCount     LONG
WarrNonRevenueCount  LONG
WarrSparesCost       DECIMAL(7,2)
WarrAccessoriesCost  DECIMAL(7,2)
WarrRevenue          DECIMAL(7,2)
WarrProfit           DECIMAL(7,2)
NonWarrantyCount     LONG
NonWRevenueCount     LONG
NonWNonRevenueCount  LONG
NonWSparesCost       DECIMAL(7,2)
NonWAccessoriesCost  DECIMAL(7,2)
NonWRevenue          DECIMAL(7,2)
NonWProfit           DECIMAL(7,2)
                     END
EngineerQueue        QUEUE,PRE(eq)
Engineer             STRING(3)
Name                 STRING(100)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED
excel:DateFormat     STRING('dd mmm yyyy')
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('o')
DataLastCol          STRING('L')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(1024)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
WarParts_Group       GROUP,PRE(wp)
NonWAccessoriesCost  DECIMAL(7,2)
NonWProfit           DECIMAL(7,2)
NonWRevenue          DECIMAL(7,2)
NonWSparesCost       DECIMAL(7,2)
WarrAccessoriesCost  DECIMAL(7,2)
WarrProfit           DECIMAL(7,2)
WarrRevenue          DECIMAL(7,2)
WarrSparesCost       DECIMAL(7,2)
                     END
Stuart_Group         GROUP,PRE()
DoAll                STRING(1)
GUIMode              BYTE
LocalHeadAccount     STRING(30)
LocalTag             STRING(1)
LocalTimeOut         LONG
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
VATCode              LONG
VATRate              REAL
                     END
BRW5::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RemoteRepairCentre)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RemoteRepairCentre LIKE(tra:RemoteRepairCentre)   !Browse hot field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Export'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('P && L Report By Manufacturer Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           STRING('Start Date'),AT(263,122,40,8),USE(?String2),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(327,122,64,10),USE(LOC:StartDate),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest booking date'),REQ
                           BUTTON,AT(395,118),USE(?StartPopCalendar),IMM,TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                           BUTTON('sho&W tags'),AT(264,204,70,13),USE(?DASSHOWTAG),DISABLE,HIDE
                           STRING('End Date'),AT(263,144,40,8),USE(?String2:2),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(327,144,64,10),USE(LOC:EndDate),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest booking date'),REQ
                           BUTTON,AT(395,140),USE(?EndPopCalendar),IMM,TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                           BUTTON('&Rev tags'),AT(244,216,50,13),USE(?DASREVTAG),DISABLE,HIDE
                           BUTTON,AT(496,240),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(496,274),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(496,308),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           CHECK('All Repair Centres'),AT(192,168),USE(DoAll),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                           CHECK('Show Excel'),AT(432,168),USE(excel:Visible),TRN,LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are having problems' &|
   ' <13,10>with Excel reports.'),VALUE('1','0')
                           LIST,AT(192,180,296,178),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                         END
                         TAB('Repair Categories'),USE(?Tab2)
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
    MAP
GetHeadAccount PROCEDURE( STRING )
GetSubAccount PROCEDURE( STRING )
GetWebJobNumber PROCEDURE( LONG ), STRING
LoadSTOCK PROCEDURE( LONG ), LONG ! BOOL
LoadSUBTRACC   PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC   PROCEDURE( STRING ), LONG ! BOOL
LoadVATCODE     PROCEDURE( STRING ), LONG ! BOOL
LoadWEBJOB      PROCEDURE( LONG ), LONG ! BOOL
NumberToColumn PROCEDURE( LONG ), STRING
WriteColumn PROCEDURE( STRING, LONG=False )
    END !MAP
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = tra:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = 'Y'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = 'Y')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::6:QUEUE = glo:Queue2
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer2 = tra:RecordNumber
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!-----------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False

        IF LOC:StartDate > LOC:EndDate
            Case Missive('Invalid date range.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
            CancelPressed = True

            EXIT
        END !IF
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again', LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            CancelPressed = True
!
!            EXIT
!        END !IF LOC:FileName = ''
        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
              LOC:FileName = CLIP(LOC:FileName) & '.xls'
           END !IF
           !-----------------------------------------------------------------
           SETCURSOR(CURSOR:Wait)

           DO ProgressBar_Setup
           !-----------------------------------------------------------------
           DO XL_Setup

           DO XL_AddWorkbook
           !-----------------------------------------------------------------
        END !IF
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF

        DO CreateTitleSheet
        DO CreateDataSheet
        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()

!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'}                         = 18
        Excel{'ActiveSheet.Columns("b:' & sheet:HeadLastCol & '").ColumnWidth'} = 12
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        FREE(SubAccount_Queue)
        CLEAR(SubAccount_Queue)

        FREE(SummaryQueue)
        CLEAR(SummaryQueue)

        FREE(EngineerQueue)
        CLEAR(EngineerQueue)

        FREE(HeaderQueue)
        CLEAR(HeaderQueue)

        DO InitColumns

        DO XL_AddSheet
        DO XL_SetWorksheetLandscape
        !-----------------------------------------------------------------
        Progress:Text    = 'Checking Jobs'
        RecordsToProcess = 1000 * (LOC:EndDate - LOC:StartDate)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOC:Text        = LEFT(CLIP(LOC:ProgramName), 30)
        LOC:SectionName = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        LOC:CommentText = ''

        DO CreateSectionHeader
        !-----------------------------------------------------------------
        ! DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE
        !
        Access:JOBS.ClearKey(job:DateCompletedKey)
        job:Date_Completed = LOC:StartDate
        SET(job:DateCompletedKey, job:DateCompletedKey)
        !------------------------------------------
        LOOP UNTIL Access:JOBS.Next()
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            Access:SUBTRACC.CLEARKEY(sub:Account_Number_Key)
            sub:Account_Number = job:Account_Number
            IF Access:SUBTRACC.Fetch(sub:Account_Number_Key) THEN
               IF job:Account_Number <> tra_ali:Account_Number THEN CYCLE.
            ELSE
                IF sub:Main_Account_Number <> tra_ali:Account_Number THEN CYCLE.
            END !IF
            !-------------------------------------------------------------
            IF    job:Date_Completed < LOC:StartDate
                CYCLE
            ELSIF job:Date_Completed > LOC:EndDate
                BREAK
            END !IF
            
            DO WriteColumns
            !-------------------------------------------------------------
        END !LOOP

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !LOC:Text          = CLIP(LOC:SectionName)
        sheet:TempLastCol = sheet:DataLastCol ! 'L'

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow &'").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        DO SetColumns
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurentRow LONG(4)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = LEFT(CLIP(LOC:Text), 30)
        !-----------------------------------------------       
        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Font.Size'}        = 14
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}          = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        !-----------------------------------------------
        Excel{'Range("A3:' & sheet:TempLastCol & '3").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A3").Select'}
            Excel{'ActiveCell.Formula'}          = 'Criteria'
            DO XL_SetBold
        !-----------------------------------------------
        CurentRow = 4
            Excel{'Range("A' & CurentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Complete Date From:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:StartDate, @D8))
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft

        CurentRow += 1
            Excel{'Range("A' & CurentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Complete Date To:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:EndDate, @D8))
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft

        CurentRow += 1
            Excel{'Range("A' & CurentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LOC:UserName
                    DO XL_HorizontalAlignmentLeft

        CurentRow += 1
            Excel{'Range("A' & CurentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created Date:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(TODAY(), @D8))
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft

        Excel{'Range("A4:' & sheet:TempLastCol & CurentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

        !-----------------------------------------------
        ! 18 Sep 2002 John - Change column "Job Number" to "SB Job Number"
        !
        head:ColumnName       = 'SB Job Number'
            head:ColumnWidth  = 18.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 18 Sep 2002 John - Add column "Franchise Job Number" for Web Jobs
        !
        head:ColumnName       = 'Franchise Job Number'
            head:ColumnWidth  = 15.00
            head:NumberFormat = '00'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 18 Sep 2002 John - 1. Include a new column for the Franchise Branch Number
        !
        head:ColumnName       = 'Franchise Branch Number'
            head:ColumnWidth  = 10.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------

        head:ColumnName       = 'Manufacturer'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Model'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'IMEI Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        head:ColumnName       = 'Date Booked'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Date Completed'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Engineer'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !-----------------------------------------------
        ! 18 Sep 2002 John - Add column "Job Type" to remove ambiguety when
        !   ("Total Warranty Jobs" + "Total Chargeable Jobs") > "Number Of Jobs"
        !   due to any jobs being "Warranty / Chargeable"
        !
        head:ColumnName       = 'Job Type'
            head:ColumnWidth  = 15.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        !-----------------------------------------------

        head:ColumnName       = 'Charge Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Repair Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Spares Cost'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)

        head:ColumnName       = 'Revenue'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)

        head:ColumnName       = 'Profit'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)
        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !PutIniCount += 1
        !PUTINI(CLIP(LOC:ProgramName),PutIniCount, 'SetColumns','C:\' & LOC:ApplicationName & '_Debug.INI')

        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                                                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
 !putini('CreateDataSheet_AllPartsOrdered', format(CLOCK(), @t1), '0', 'c:\debug.ini')
        IF CancelPressed
            EXIT
        END !IF

        IF job:Date_Completed = 0
            EXIT
        END !IF
        !-----------------------------------------------------------------
        GetSubAccount(job:Account_Number)
        DO GetEngineer
        DO UpdateSummaryQueue

        LOC:FranchiseJobNumber = GetWebJobNumber(job:Ref_Number)
        !=================================================================
        IF job:Warranty_Job = 'YES'
            DO WriteColumns_Warranty
        END ! IF

        IF job:Chargeable_Job = 'YES'
            DO WriteColumns_Chargeable
        END ! IF
        !-----------------------------------------------------------------
    EXIT


WriteColumns_Warranty                                                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        RecordCount += 1

        DO WriteColumn_JobNumber        ! Job Number

        !-----------------------------------------------
        ! 18 Sep 2002 John - Add column "Franchise Job Number" for Web Jobs
        !
        WriteColumn( CLIP(LOC:FranchiseJobNumber) )       ! Web Job Number
        !-----------------------------------------------

        !-----------------------------------------------
        ! 26 Sep 2002 John - 1. Include a new column for the Franchise Branch Number
        !
        WriteColumn( saQ:BranchIdentification )
        !-----------------------------------------------

        DO WriteColumn_Manufacturer     ! Manufacturer
        DO WriteColumn_Model            ! Model
        DO WriteColumn_IMEINumber       ! IMEI Number
        DO WriteColumn_DateBooked       ! Date Booked
        DO WriteColumn_DateCompleted    ! Date Completed
        DO WriteColumn_Engineer         ! Engineer

        !-----------------------------------------------
        ! 18 Sep 2002 John - Add column "Job Type" to remove ambiguety when
        !   ("Total Warranty Jobs" + "Total Chargeable Jobs") > "Number Of Jobs"
        !   due to any jobs being "Warranty / Chargeable"
        !
        WriteColumn( 'Warranty'               )
        WriteColumn( job:Warranty_Charge_Type )
        WriteColumn( job:Repair_Type_Warranty )
        WriteColumn( wp:WarrSparesCost        ) ! Spares Cost
        WriteColumn( wp:WarrRevenue           ) ! Revenue
        WriteColumn( wp:WarrProfit            ) ! Profit
        !-----------------------------------------------

        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------------------------
    EXIT


WriteColumns_Chargeable                                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        RecordCount += 1

        DO WriteColumn_JobNumber        ! Job Number

        !-----------------------------------------------
        ! 18 Sep 2002 John - Add column "Franchise Job Number" for Web Jobs
        !
        WriteColumn( CLIP(LOC:FranchiseJobNumber) )       ! Web Job Number
        !-----------------------------------------------

        !-----------------------------------------------
        ! 26 Sep 2002 John - 1. Include a new column for the Franchise Branch Number
        !
        WriteColumn( saQ:BranchIdentification )
        !-----------------------------------------------

        DO WriteColumn_Manufacturer     ! Manufacturer
        DO WriteColumn_Model            ! Model
        DO WriteColumn_IMEINumber       ! IMEI Number
        DO WriteColumn_DateBooked       ! Date Booked
        DO WriteColumn_DateCompleted    ! Date Completed
        DO WriteColumn_Engineer         ! Engineer

        !-----------------------------------------------
        ! 18 Sep 2002 John - Add column "Job Type" to remove ambiguety when
        !   ("Total Warranty Jobs" + "Total Chargeable Jobs") > "Number Of Jobs"
        !   due to any jobs being "Warranty / Chargeable"
        !
        WriteColumn( 'Chargeable'      )            
        WriteColumn( job:Charge_Type   )
        WriteColumn( job:Repair_Type   )
        WriteColumn( wp:NonWSparesCost ) ! Spares Cost
        WriteColumn( wp:NonWRevenue    ) ! Revenue
        WriteColumn( wp:NonWProfit     ) ! Profit
        !-----------------------------------------------

        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
WriteColumn_JobNumber                                ROUTINE ! Job Number
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = job:Ref_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Ref_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_FranchiseBranchNumber  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = haQ:BranchIdentification

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = haQ:BranchIdentification

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_FranchiseJobNumber      ROUTINE
    DATA
Temp STRING(10)
    CODE
        !-----------------------------------------------
        IF WEBJOB_OK
            Temp = wob:JobNumber
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(Temp)

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = CLIP(Temp)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_WebJobNumber                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! 18 Sep 2002 John - Add column "Franchise Job Number" for Web Jobs
        !
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = GetWebJobNumber(job:Ref_Number)

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = GetWebJobNumber(job:Ref_Number)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_JobType                                ROUTINE ! Job Type
    DATA
Temp STRING('Chargeable/Warranty')
    CODE
        !-----------------------------------------------
        ! 18 Sep 2002 John - Add column "Job Type" to remove ambiguety when
        !   ("Total Warranty Jobs" + "Total Chargeable Jobs") > "Number Of Jobs"
        !   due to any jobs being "Warranty / Chargeable"
        !
        !-----------------------------------------------
        IF job:Warranty_Job = 'YES'
            IF job:Chargeable_Job = 'YES'
                Temp = 'Chargeable/Warranty'
            ELSE
                Temp = 'Warranty'
            END !IF
        ELSIF job:Chargeable_Job = 'YES'
            Temp = 'Chargeable'
        ELSE
            Temp = 'Unspecfied'
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Manufacturer                                ROUTINE ! Manufacturer
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Manufacturer

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Manufacturer

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Model                                ROUTINE ! Model
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Model_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Model_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_IMEINumber                                ROUTINE ! IMEI Number
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:ESN

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:ESN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DateBooked                                ROUTINE ! Date Booked
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(job:date_booked, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(job:date_booked, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DateCompleted                                 ROUTINE ! Date Completed
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(job:Date_Completed, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(job:Date_Completed, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Engineer                                ROUTINE ! Engineer
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & eq:Name

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = eq:Name

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ChargeType                                ROUTINE ! Charge Type
    DATA
Temp       LIKE(job:Charge_Type)
AddComment BYTE(False)
    CODE
        !-----------------------------------------------
        IF  (job:Chargeable_Job = 'YES')
            Temp = job:Charge_Type
            IF (job:Warranty_Job = 'YES')
                AddComment = True
            END !IF

        ELSIF job:Warranty_Job = 'YES'
            Temp = job:Warranty_Charge_Type

        ELSE
            Temp = 'Unspecified'

        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp
            IF AddComment
                clip:Value = CLIP(clip:Value) & '/' & job:Warranty_Charge_Type
            END !IF

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'} = Temp

        IF AddComment
            LOC:CommentText = job:Warranty_Charge_Type
            DO XL_AddComment
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_RepairType                                ROUTINE ! Repair Type
    DATA
Temp       LIKE(job:Repair_Type)
AddComment Byte(False)
    CODE
        !-----------------------------------------------
        IF (job:Chargeable_Job = 'YES')
            Temp = job:Repair_Type

            IF (job:Warranty_Job = 'YES') 
                AddComment = True
            END !IF

        ELSIF job:Warranty_Job = 'YES'
            Temp = job:Repair_Type_Warranty

        ELSE
            Temp = ''

        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            IF AddComment
                clip:Value = CLIP(clip:Value) & '/' & job:Repair_Type_Warranty
            END !IF

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        IF AddComment
            LOC:CommentText = 'Repair Type<13,10>' & job:Repair_Type_Warranty
            DO XL_AddComment
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_SparesCost                            ROUTINE ! Spares Cost
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & wp:WarrSparesCost + wp:NonWSparesCost

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = wp:WarrSparesCost + wp:NonWSparesCost

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Revenue                                    ROUTINE ! Revenue
    DATA
    CODE
        !-----------------------------------------------
        !Don't understand why were using Profit = Revenue
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & wp:WarrRevenue + wp:NonWRevenue!wp:WarrProfit + wp:NonWProfit

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = wp:WarrRevenue + wp:NonWRevenue!wp:WarrProfit + wp:NonWProfit

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Profit                                ROUTINE ! Profit
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & wp:WarrProfit + wp:NonWProfit

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = wp:WarrProfit + wp:NonWProfit

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
!WriteColumn_BatchNumber                                ROUTINE ! Batch Number
!    DATA
!    CODE
!        !-----------------------------------------------
!        DO XL_HorizontalAlignmentRight
!        Excel{'ActiveCell.Formula'}  = job:EDI_Batch_Number ! job:Batch_Number
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
!WriteColumn_ClaimRaised                                ROUTINE ! Claim Raised
!    DATA
!temp STRING(3)
!    CODE
!        !-----------------------------------------------
!        ! WarrantyStatus
!        !
!        ! Pending     No Batch Number
!        ! Processed   Batch Number exists
!        ! Accepted    Invoice Number
!        ! Rejected    job:EDI_Batch_Number = 'EXC'
!        !-----------------------------------------------
!        DO XL_HorizontalAlignmentRight
!
!        IF job:Warranty_Job = 'YES'
!            IF job:Batch_Number <> 0
!                temp = 'YES'
!
!            ELSE
!                temp = 'NO'
!
!            END !IF
!        END !IF
!
!        Excel{'ActiveCell.Formula'}  = temp
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
!WriteColumn_Reconciled                                ROUTINE ! Reconciled
!    DATA
!temp STRING(3)
!    CODE
!        !-----------------------------------------------
!        ! WarrantyStatus
!        !
!        ! Pending     No Batch Number
!        ! Processed   Batch Number exists
!        ! Accepted    Invoice Number
!        ! Rejected    job:EDI_Batch_Number = 'EXC'
!        !-----------------------------------------------
!        DO XL_HorizontalAlignmentRight
!
!        IF (job:Chargeable_Job = 'YES')
!            IF (job:Invoice_Number <> 0)
!                temp = 'YES'
!            ELSE
!                temp = 'NO'
!            END !IF
!
!        ELSE (job:Warranty_Job = 'YES')
!            IF (job:Invoice_Number_Warranty <> 0)
!                temp = 'YES'
!            ELSE
!                temp = 'NO'
!            END !IF
!
!        END !IF
!
!        Excel{'ActiveCell.Formula'}  = temp
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
!WriteColumn_ClaimRejected                                ROUTINE ! Claim Rejected
!    DATA
!temp STRING(3)
!    CODE
!        !-----------------------------------------------
!        ! WarrantyStatus
!        !
!        ! Pending     No Batch Number
!        ! Processed   Batch Number exists
!        ! Accepted    Invoice Number
!        ! Rejected    job:EDI_Batch_Number = 'EXC'
!        !-----------------------------------------------
!        DO XL_HorizontalAlignmentRight
!
!        IF job:Warranty_Job = 'YES'
!            IF (job:EDI = 'EXC')
!                temp = 'YES'
!
!            ELSE
!                temp = 'NO'
!
!            END !IF
!        END !IF
!
!        Excel{'ActiveCell.Formula'}  = temp
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
!WriteColumn_SparesCharge                                ROUTINE ! Spares Charge
!    DATA
!    CODE
!        !-----------------------------------------------
!        DO XL_HorizontalAlignmentRight
!
!        Excel{'ActiveCell.Formula'}  = wp:NonWSparesCost
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
!WriteColumn_WarrantyAccepted                      ROUTINE ! Warranty Accepted
!    DATA
!temp STRING(3)
!    CODE
!        !-----------------------------------------------
!        ! WarrantyStatus
!        !
!        ! Pending     No Batch Number
!        ! Processed   Batch Number exists
!        ! Accepted    Invoice Number
!        ! Rejected    job:EDI_Batch_Number = 'EXC'
!        !-----------------------------------------------
!        DO XL_HorizontalAlignmentRight
!
!        IF (job:Chargeable_Job = 'YES')
!            IF (job:Invoice_Number <> 0)
!                temp = 'YES'
!            ELSE
!                temp = 'NO'
!            END !IF
!
!        ELSE (job:Warranty_Job = 'YES')
!            IF (job:Invoice_Number_Warranty <> 0)
!                temp = 'YES'
!            ELSE
!                temp = 'NO'
!            END !IF
!
!        END !IF
!
!        Excel{'ActiveCell.Formula'}  = temp
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
!WriteColumn_AccessoriesCost                            ROUTINE ! Accessories Cost
!    DATA
!    CODE
!        !-----------------------------------------------
!        DO XL_HorizontalAlignmentRight
!
!        Excel{'ActiveCell.Formula'}  = wp:WarrAccessoriesCost + wp:NonWAccessoriesCost
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
!-----------------------------------------------
WriteDataSummary                                              ROUTINE
    DATA
TotalRow       LONG
    CODE
 !putini('WriteSummary_AllPartsOrdered', format(CLOCK(), @t1), '0', 'c:\debug.ini')
        !-----------------------------------------------------------------
        ! summary details
        !
        IF RecordCount < 1
            Excel{'ActiveCell.Formula'} = 'No Complete Jobs Found'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO FormatColumns

        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = RecordCount

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}         = 'Showing'

        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.NumberFormat'}    = '0'
            Excel{'ActiveCell.Formula'}         = '=SUBTOTAL(2, a' & sheet:DataHeaderRow+1 & ':a' & sheet:DataHeaderRow+RecordCount & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
        TotalRow = sheet:DataHeaderRow+RecordCount+1

        Excel{'Range("A' & TotalRow & ':' & sheet:DataLastCol & TotalRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & TotalRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Sub Totals'
            DO XL_SetBold

            LOOP x# = RECORDS(HeaderQueue)-3 TO RECORDS(HeaderQueue)-1 ! 11 TO 13
                Excel{'ActiveCell.Offset(0, ' & x# & ').FormulaR1C1'}  = '=SUBTOTAL(9,r' & sheet:DataHeaderRow+1 & 'C' & x#+1 & ':r' & sheet:DataHeaderRow+RecordCount & 'c' & x#+1 & ')'
                Excel{'ActiveCell.Offset(0, ' & x# & ').NumberFormat'} = '#,##0.00'
            END !LOOP
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary                                              ROUTINE
    DATA
TopRow       LONG
CurrentRow   LONG

Records_SummaryQueue LONG
    CODE
        !-----------------------------------------------------------------
        ! putini('WriteSummary_AllPartsOrdered', format(CLOCK(), @t1), '0', 'c:\debug.ini')
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
            DO XL_ColFirst

        IF RecordCount < 1
            Excel{'ActiveCell.Formula'} = 'No Complete Jobs Found'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        !
        TopRow = Excel{'ActiveCell.Row'}

        Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Manufacturer'

        Excel{'ActiveCell.Offset(0, 1).Formula'} = 'Warranty Total Units In'
        Excel{'ActiveCell.Offset(0, 2).Formula'} = 'Warranty Total Non Revenue Generating'
        Excel{'ActiveCell.Offset(0, 3).Formula'} = 'Warranty Total Revenue Generating'
        Excel{'ActiveCell.Offset(0, 4).Formula'} = 'Warranty Spares Cost'
        Excel{'ActiveCell.Offset(0, 5).Formula'} = 'Warranty Accessories Cost'
        Excel{'ActiveCell.Offset(0, 6).Formula'} = 'Warranty Revenue'
        Excel{'ActiveCell.Offset(0, 7).Formula'} = 'Warranty Profit'

        Excel{'ActiveCell.Offset(0, 8).Formula'} = 'Non Warranty Total Units In'
        Excel{'ActiveCell.Offset(0, 9).Formula'} = 'Non Warranty Total Non Revenue Generating'
        Excel{'ActiveCell.Offset(0,10).Formula'} = 'Non Warranty Total Revenue Generating'
        Excel{'ActiveCell.Offset(0,11).Formula'} = 'Non Warranty Spares Cost'
        Excel{'ActiveCell.Offset(0,12).Formula'} = 'Non Warranty Accessories Cost'
        Excel{'ActiveCell.Offset(0,13).Formula'} = 'Non Warranty Revenue'
        Excel{'ActiveCell.Offset(0,14).Formula'} = 'Non Warranty Profit'

        Excel{'Range("A' & TopRow & ':' & sheet:HeadLastCol & TopRow & '").Select'}
                DO XL_SetColumnHeader

        Excel{'Range("A' & TopRow+1 & '").Select'}
        !-----------------------------------------------------------------
        Records_SummaryQueue = RECORDS(SummaryQueue)
        IF Records_SummaryQueue < 1
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'No Complete Jobs Found'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        SORT(SummaryQueue, +sq:Manufacturer)
        LOOP x# = 1 TO Records_SummaryQueue
            !-------------------------------------------------------------
            GET(SummaryQueue, x#)
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0, 0).NumberFormat'} = chr(64) ! Text (AT) Sign

            Excel{'ActiveCell.Offset(0, 1).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0, 3).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0, 4).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0, 5).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0, 6).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0, 7).NumberFormat'} = '#,##0.00'

            Excel{'ActiveCell.Offset(0, 8).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0, 9).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,10).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,11).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0,12).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0,13).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0,14).NumberFormat'} = '#,##0.00'
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = sq:Manufacturer

            Excel{'ActiveCell.Offset(0, 1).Formula'}      = sq:WarrantyCount
            Excel{'ActiveCell.Offset(0, 2).Formula'}      = sq:WarrNonRevenueCount
            Excel{'ActiveCell.Offset(0, 3).Formula'}      = sq:WarrRevenueCount
            Excel{'ActiveCell.Offset(0, 4).Formula'}      = sq:WarrSparesCost
            Excel{'ActiveCell.Offset(0, 5).Formula'}      = sq:WarrAccessoriesCost
            Excel{'ActiveCell.Offset(0, 6).Formula'}      = sq:WarrRevenue
            Excel{'ActiveCell.Offset(0, 7).Formula'}      = sq:WarrProfit

            Excel{'ActiveCell.Offset(0, 8).Formula'}      = sq:NonWarrantyCount
            Excel{'ActiveCell.Offset(0, 9).Formula'}      = sq:NonWNonRevenueCount
            Excel{'ActiveCell.Offset(0,10).Formula'}      = sq:NonWRevenueCount
            Excel{'ActiveCell.Offset(0,11).Formula'}      = sq:NonWSparesCost
            Excel{'ActiveCell.Offset(0,12).Formula'}      = sq:NonWAccessoriesCost
            Excel{'ActiveCell.Offset(0,13).Formula'}      = sq:NonWRevenue
            Excel{'ActiveCell.Offset(0,14).Formula'}      = sq:NonWProfit
            !-------------------------------------------------------------
            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("a' & TopRow & ':' & sheet:HeadLastCol & (TopRow+Records_SummaryQueue) & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
        CurrentRow = TopRow+Records_SummaryQueue+1

        Excel{'Range("a' & CurrentRow & ':' & sheet:HeadLastCol & (CurrentRow) & '").Select'}
            DO XL_SetTitle
            DO XL_SetGrid
        !-----------------------------------------------------------------
        Excel{'Range("a' & CurrentRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Offset(0, 0).NumberFormat'} = chr(64) ! Text (AT) Sign

            Excel{'ActiveCell.Offset(0, 1).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0, 3).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0, 4).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0, 5).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0, 6).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0, 7).NumberFormat'} = '#,##0.00'

            Excel{'ActiveCell.Offset(0, 8).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0, 9).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,10).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,11).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0,12).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0,13).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0,14).NumberFormat'} = '#,##0.00'
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Totals'

            Excel{'ActiveCell.Offset(0, 1).Formula'} = '=SUBTOTAL(9,b' & TopRow & ':b' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0, 2).Formula'} = '=SUBTOTAL(9,c' & TopRow & ':c' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0, 3).Formula'} = '=SUBTOTAL(9,d' & TopRow & ':d' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0, 4).Formula'} = '=SUBTOTAL(9,e' & TopRow & ':e' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0, 5).Formula'} = '=SUBTOTAL(9,f' & TopRow & ':f' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0, 6).Formula'} = '=SUBTOTAL(9,g' & TopRow & ':g' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0, 7).Formula'} = '=SUBTOTAL(9,h' & TopRow & ':h' & TopRow+Records_SummaryQueue & ')'

            Excel{'ActiveCell.Offset(0, 8).Formula'} = '=SUBTOTAL(9,i' & TopRow & ':i' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0, 9).Formula'} = '=SUBTOTAL(9,j' & TopRow & ':j' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0,10).Formula'} = '=SUBTOTAL(9,k' & TopRow & ':k' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0,11).Formula'} = '=SUBTOTAL(9,l' & TopRow & ':l' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0,12).Formula'} = '=SUBTOTAL(9,m' & TopRow & ':m' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0,13).Formula'} = '=SUBTOTAL(9,n' & TopRow & ':n' & TopRow+Records_SummaryQueue & ')'
            Excel{'ActiveCell.Offset(0,14).Formula'} = '=SUBTOTAL(9,o' & TopRow & ':o' & TopRow+Records_SummaryQueue & ')'
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG', |
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Cannot find the user''s details.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Profit & Loss By Manufacturer'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(tra_ali:Account_Number) & ' ' & Format(Today(),@d12)
!
!
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                !DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0016 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
UpdateSummaryQueue                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        sq:Manufacturer  = job:Manufacturer
        GET(SummaryQueue, +sq:Manufacturer)
        IF ERRORCODE()
            ! Not in queue - ADD
            sq:Manufacturer              = job:Manufacturer

            sq:WarrantyCount       = 0
            sq:WarrRevenueCount    = 0
            sq:WarrNonRevenueCount = 0
            sq:WarrSparesCost      = 0
            sq:WarrAccessoriesCost = 0
            sq:WarrRevenue         = 0
            sq:WarrProfit          = 0

            sq:NonWarrantyCount    = 0
            sq:NonWRevenueCount    = 0
            sq:NonWNonRevenueCount = 0
            sq:NonWSparesCost      = 0
            sq:NonWAccessoriesCost = 0
            sq:NonWRevenue         = 0
            sq:NonWProfit          = 0

            ADD(SummaryQueue, +sq:Manufacturer)
        END !IF
        !-----------------------------------------------------------------
        DO CalcParts
        !DO CalcLabour

        sq:TotalUnitsRepaired  += 1
        !-----------------------------------------------------------------
        IF    (job:Warranty_Job   = 'YES')
            sq:WarrantyCount           += 1

            IF (job:Sub_Total_Warranty <= 0)
                sq:WarrNonRevenueCount += 1
            ELSE
                sq:WarrRevenueCount    += 1
            END !IF

            sq:WarrSparesCost          += wp:WarrSparesCost
            sq:WarrAccessoriesCost     += wp:WarrAccessoriesCost
            sq:WarrRevenue             += wp:WarrRevenue
            sq:WarrProfit              += wp:WarrProfit
        END !IF
        !-----------------------------------------------------------------
        IF (job:Chargeable_Job = 'YES')
            sq:NonWarrantyCount        += 1

            IF (job:Sub_Total <= 0)
                sq:NonWNonRevenueCount += 1
            ELSE
                sq:NonWRevenueCount    += 1
            END !IF

            sq:NonWSparesCost          += wp:NonWSparesCost
            sq:NonWAccessoriesCost     += wp:NonWAccessoriesCost
            sq:NonWRevenue             += wp:NonWRevenue
            sq:NonWProfit              += wp:NonWProfit
        END !IF
        !-----------------------------------------------------------------
        PUT(SummaryQueue, +sq:Manufacturer)
        !-----------------------------------------------------------------
    EXIT
CalcParts                                                                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        wp:WarrAccessoriesCost = 0
        wp:WarrSparesCost      = 0
        wp:WarrProfit          = 0
        wp:WarrRevenue         = 0

        IF job:Warranty_Job = 'YES'
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:ref_number  = job:Ref_Number
            SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
            LOOP UNTIL access:warparts.next()
                IF wpr:Ref_Number <> job:Ref_Number
                    BREAK
                END !IF

                IF LoadSTOCK(wpr:Part_Ref_Number)
                    IF (sto:accessory = 'YES')
                        wp:WarrAccessoriesCost += wpr:quantity * wpr:purchase_cost
                    ELSE
                        wp:WarrSparesCost      += wpr:quantity * wpr:purchase_cost
                    END !IF
                END !IF
            END !LOOP

            wp:WarrRevenue = job:Sub_Total_Warranty
            wp:WarrProfit  = wp:WarrRevenue - wp:WarrAccessoriesCost - wp:WarrSparesCost

        END !IF
        !-----------------------------------------------
        wp:NonWAccessoriesCost = 0
        wp:NonWSparesCost      = 0
        wp:NonWProfit          = 0
        wp:NonWRevenue         = 0

        IF job:Chargeable_Job = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
                par:Ref_Number = job:Ref_Number
            SET(par:Part_Number_Key,par:Part_Number_Key)

            LOOP UNTIL Access:PARTS.NEXT()
                IF par:Ref_Number <> job:Ref_Number
                    BREAK
                END !IF

                IF LoadSTOCK(par:Part_Ref_Number)
                    IF (sto:accessory = 'YES')
                        wp:NonWAccessoriesCost += par:quantity * par:purchase_cost
                    ELSE
                        wp:NonWSparesCost      += par:quantity * par:purchase_cost
                    END !IF
                END !IF
            END !LOOP

            wp:NonWRevenue = job:Sub_Total
            wp:NonWProfit  = wp:NonWRevenue - wp:NonWAccessoriesCost - wp:NonWSparesCost

        END !IF
        !-----------------------------------------------
    EXIT
!CalcParts                                                ROUTINE ! Calls Pricing_Routine PROCEDURE SBA01APP
!    DATA
!Labour"   STRING(20)
!Parts"    STRING(20)
!Pass"     STRING(20)
!Discount" STRING(20)
!    CODE
!        !-----------------------------------------------
!        ! Uses
!        ! Pricing_Routine PROCEDURE(String f_type, *String f_labour, *String f_parts, *String f_pass, *String f_discount)
!        !-----------------------------------------------
!        wp:WarrSparesCost      = 0
!        wp:WarrAccessoriesCost = 0
!        wp:WarrRevenue         = 0
!        wp:WarrProfit          = 0
!
!        wp:NonWSparesCost      = 0
!        wp:NonWAccessoriesCost = 0
!        wp:NonWRevenue         = 0
!        wp:NonWProfit          = 0
!        !-----------------------------------------------
!        ! 14 Aug 2002 John some totals getting doubled
!        !
!        !        job:Sub_Total_Estimate = 0
!        !
!        !        IF job:Estimate = 'YES' ! job:Estimate_Accepted = 'YES'
!        !            Pricing_Routine('E', Labour", Parts", Pass", Discount")
!        !            IF pass" = True
!        !                job:Labour_Cost_Estimate = Round(Labour", .01)
!        !                job:Parts_Cost_Estimate  = Round( Parts", .01)
!        !
!        !                job:Sub_Total_Estimate = job:Labour_Cost_Estimate + job:Parts_Cost_Estimate
!        !            End!If pass" = True
!        !
!        !        END !IF
!        !-----------------------------------------------
!        IF job:Warranty_Job = 'YES'
!            IF job:Invoice_Number_Warranty = ''
!                Pricing_Routine('W', Labour", Parts", Pass", Discount")
!                IF Pass" = True
!                    job:labour_cost_warranty = Round(Labour", .01)
!                    job:parts_cost_warranty  = Round( Parts", .01)
!                    job:sub_total_warranty   = Round(job:labour_cost_warranty,.01) + Round(job:parts_cost_warranty,.01) + Round(job:courier_cost_warranty,.01)
!                Else
!                    job:labour_cost_warranty = 0
!                    job:parts_cost_warranty  = 0
!                End!If pass" = False
!
!            END !IF
!
!            IF job:Ignore_Warranty_Charges = 'YES'
!                wp:WarrRevenue += job:Sub_Total_Warranty
!            ELSE
!                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
!                    wpr:ref_number  = job:Ref_Number
!                SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
!
!                LOOP UNTIL access:warparts.next()
!                    IF wpr:Ref_Number <> job:Ref_Number
!                        BREAK
!                    END !IF
!
!                    DO SyncSTOCK
!                    IF Result
!                        IF (sto:accessory = 'YES')
!                            wp:WarrAccessoriesCost += wpr:quantity * wpr:purchase_cost
!
!                        ELSE
!                            wp:WarrSparesCost      += wpr:quantity * wpr:purchase_cost
!
!                        END !IF
!                        
!                    END !IF
!                END !LOOP
!
!            END !IF
!        END !IF
!        !-----------------------------------------------
!        IF job:Chargeable_Job = 'YES'
!            IF job:Invoice_Number = ''
!                Pricing_Routine('C', Labour", Parts", Pass", Discount")
!                IF Pass" = True
!                    job:labour_cost = Round(Labour", .01)
!                    job:parts_cost  = Round( Parts", .01)
!                    job:sub_total   = Round(job:labour_cost_warranty,.01) + Round(job:parts_cost_warranty,.01) + Round(job:courier_cost_warranty,.01)
!                Else
!                    job:labour_cost = 0
!                    job:parts_cost  = 0
!                End!If pass" = False
!
!            END !IF
!
!            IF job:Ignore_Chargeable_Charges = 'YES'
!                wp:NonWRevenue += job:Sub_Total
!            ELSE
!                Access:PARTS.ClearKey(par:Part_Number_Key)
!                par:Ref_Number = job:Ref_Number
!                SET(par:Part_Number_Key,par:Part_Number_Key)
!                LOOP UNTIL Access:PARTS.NEXT()
!                    IF par:Ref_Number <> job:Ref_Number
!                        BREAK
!                    END !IF
!
!                    DO SyncSTOCK
!                    IF Result
!                        IF (sto:accessory = 'YES')
!                            wp:NonWAccessoriesCost += par:quantity * par:purchase_cost
!
!                        ELSE
!                            wp:NonWSparesCost      += par:quantity * par:purchase_cost
!
!                        END !IF
!                    END !IF
!                END !LOOP
!
!            END !IF
!        END !IF
!        !-----------------------------------------------
!        wp:WarrRevenue    = job:Sub_Total_Warranty
!        wp:WarrProfit     = wp:WarrRevenue - wp:WarrAccessoriesCost - wp:WarrSparesCost
!
!        wp:NonWRevenue    = job:Sub_Total ! + job:Sub_Total_Estimate ! 14 Aug 2002 John some totals getting doubled
!        wp:NonWProfit     = wp:NonWRevenue - wp:NonWAccessoriesCost - wp:NonWSparesCost
!        !-----------------------------------------------
!    EXIT
!CalcLabour                                              ROUTINE
!    DATA
!    CODE
!        !-----------------------------------------------------------------
!        wp:LabourAmount    = 0
!
!        IF job:chargeable_job <> 'YES' 
!            EXIT
!        END ! IF
!
!        IF job:ignore_chargeable_charges = 'YES'
!            EXIT
!        END ! IF
!
!        access:chartype.clearkey(cha:charge_type_key)
!        IF job:warranty_job = 'YES'
!            cha:charge_type = job:warranty_charge_type
!        ELSE
!            cha:charge_type = job:charge_type 
!        END !IF
!        
!        If access:chartype.fetch(cha:charge_type_key)
!            EXIT
!        End !If access:chartype.clearkey(cha:charge_type_key)
!
!        If cha:no_charge = 'YES'
!            EXIT
!        End !If cha:no_charge = 'YES'
!
!        DO SyncSUBTRACCfromJOBS
!        IF Result <> True
!            GOTO Final
!        END !IF
!
!        DO SyncTRADEACCfromSUBTRACC
!        IF Result <> True
!            GOTO Final
!        END !IF
!
!        If     tra:invoice_sub_accounts = 'YES' 
!            access:subchrge.clearkey(suc:model_repair_type_key)
!            suc:account_number = job:account_number
!            suc:model_number   = job:model_number
!            suc:charge_type    = job:charge_type
!            suc:unit_type      = job:unit_type
!            suc:repair_type    = job:repair_type
!            if access:subchrge.fetch(suc:model_repair_type_key)
!                access:trachrge.clearkey(trc:account_charge_key)
!                trc:account_number = sub:main_account_number
!                trc:model_number   = job:model_number
!                trc:charge_type    = job:charge_type
!                trc:unit_type      = job:unit_type
!                trc:repair_type    = job:repair_type
!                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                    wp:LabourAmount = trc:cost
!
!                    EXIT
!                End!if access:trachrge.fetch(trc:account_charge_key)
!            Else
!                wp:LabourAmount = suc:cost
!
!                EXIT
!            End!if access:subchrge.fetch(suc:model_repair_type_key)
!
!        Else!If tra:invoice_sub_accounts = 'YES'
!            access:trachrge.clearkey(trc:account_charge_key)
!            trc:account_number = sub:main_account_number
!            trc:model_number   = job:model_number
!            trc:charge_type    = job:charge_type
!            trc:unit_type      = job:unit_type
!            trc:repair_type    = job:repair_type
!            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                wp:LabourAmount = trc:cost
!
!                EXIT
!            End!if access:trachrge.fetch(trc:account_charge_key)
!
!        End!If tra:invoice_sub_accounts = 'YES'
!
!Final   access:stdchrge.clearkey(sta:model_number_charge_key)
!        sta:model_number = job:model_number
!        IF job:warranty_job = 'YES'
!            sta:charge_type  = job:warranty_charge_type
!        ELSE
!            sta:charge_type  = job:charge_type
!        END !IF
!        
!        sta:unit_type    = job:unit_type
!        sta:repair_type  = job:repair_type
!        if access:stdchrge.fetch(sta:model_number_charge_key) = Level:Benign
!            wp:LabourAmount = sta:cost
!        end !if access:stdchrge.fetch(sta:model_number_charge_key)
!        !-----------------------------------------------------------------
!    EXIT
GetEngineer                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------  
        eq:Engineer  = job:Engineer
        GET(EngineerQueue, +eq:Engineer)

        IF NOT ERRORCODE()
            ! In queue - return
            EXIT !
        END !IF
        !-----------------------------------------------------------------
        ! Not in queue - ADD
        !-----------------------------------------------------------------
        DO LoadEngineer
        IF Result
            eq:Name = CLIP(use:Surname) & ', ' & CLIP(use:Forename)
        ELSE
            eq:Name = job:Engineer
        END !IF

        eq:Engineer  = job:Engineer
        ADD(EngineerQueue, +eq:Engineer)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
LoadEngineer                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = job:Engineer

        IF Access:USERS.TryFetch(use:User_Code_Key)
            EXIT
        END !IF

        IF use:User_Code <> job:Engineer
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadSTOMODEL                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:STOMODEL.ClearKey(stm:Model_Number_Key)
        stm:Ref_Number    = shi:Ref_Number
        !stm:manufacturer = sto:manufacturer
        set(stm:Model_Number_Key, stm:Model_Number_Key)

        IF Access:STOMODEL.NEXT()
            EXIT
        END !IF
!        IF Access:STOMODEL.TryFetch(stm:Model_Number_Key)
!            EXIT
!        END !IF

        IF stm:Ref_Number <> shi:Ref_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadWARPARTS                                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = job:Ref_Number

        IF Access:WARPARTS.TryFetch(wpr:Part_Number_Key)
            EXIT
        END !IF

        IF wpr:Ref_Number <> job:Ref_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadUSERS                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = LOC:UserCode

        IF Access:USERS.TryFetch(use:User_Code_Key)
            EXIT
        END !IF

        IF use:User_Code <> LOC:UserCode
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
SyncINVOICE                                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result    = False
        InvoiceOK = False

        IF job:Invoice_Number = 0
            EXIT
        END !IF

        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job:Invoice_Number
        set(inv:Invoice_Number_Key, inv:Invoice_Number_Key)

        IF Access:INVOICE.NEXT()
            EXIT
        END !IF

        IF inv:Invoice_Number <> job:Invoice_Number
            EXIT
        END !IF

        Result    = True
        InvoiceOK = True
        !-----------------------------------------------
    EXIT
Load_StockHistory                                             ROUTINE
!    DATA
!    CODE
!        !-----------------------------------------------------------------
! !putini('CreateDataSheet_AllPartsOrdered', format(CLOCK(), @t1), '0', 'c:\debug.ini')
!        IF CancelPressed
!            EXIT
!        END !IF
!        !------------------------------------------
!        !
!        Access:STOHIST.ClearKey(shi:DateKey)
!        shi:Date = LOC:StartDate
!        SET(shi:DateKey, shi:DateKey)
!        !------------------------------------------
!        Progress:Text    = 'Loading Stock History Details'
!        RecordsToProcess = RECORDS(STOHIST)
!        RecordsProcessed = 0
!        RecordCount      = 0
!
!        DO ProgressBar_LoopPre
! !message('Start of main loop')
!        !-----------------------------------------------------------------
!        LOOP UNTIL Access:STOHIST.Next()
!            !-------------------------------------------------------------
!            DO ProgressBar_Loop
!
!            IF CancelPressed
!                BREAK
!            END !IF
!            !-------------------------------------------------------------
!            IF shi:Date < LOC:StartDate
!                CYCLE
!            ELSIF shi:Date > LOC:EndDate
!                BREAK
!            END !IF
!
!            DO SaveStockDetailsToQueue
!            !-------------------------------------------------------------
!        END !LOOP
! !message('End of main loop')
!
!        IF CancelPressed
!            EXIT
!        END !IF
!
!        DO ProgressBar_LoopPost
!        !-----------------------------------------------------------------
    EXIT

!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mstop.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    
    DISPLAY()
    !ForceRefresh = False
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A10").Select'}
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0    ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!-----------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('PLReportByManufacturer')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:DISCOUNT.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:STOCK.UseFile
  Access:STOMODEL.UseFile
  Access:JOBS.UseFile
  Access:INVOICE.UseFile
  Access:WARPARTS.UseFile
  Access:TRADEACC.UseFile
  Access:PARTS.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHAR.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName         = 'P & L Report By Manufacturer'  !         Job=1280            Cust=16
      excel:Visible           = False
  
      LOC:StartDate      = DATE( MONTH(TODAY()), 1, YEAR(TODAY()) )
      LOC:EndDate        = TODAY() !
  
      !DateTimeDifference( LOC:StartDate, deformat('12:00',@t1), LOC:EndDate, deformat('13:00',@t1) )
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName
      IF GUIMode = 1 THEN
         MainWindow{PROP:ICONIZE} = TRUE
         LocalTimeOut = 500
         DoAll = 'Y'
      END !IF
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,tra:Account_Number_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,tra:Account_Number,1,BRW5)
  BRW5.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND (tra:Account_Number <<> ''XXXRRC''))')
  BIND('LocalTag',LocalTag)
  BIND('LocalHeadAccount',LocalHeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(LocalTag,BRW5.Q.LocalTag)
  BRW5.AddField(tra:Account_Number,BRW5.Q.tra:Account_Number)
  BRW5.AddField(tra:Company_Name,BRW5.Q.tra:Company_Name)
  BRW5.AddField(tra:RemoteRepairCentre,BRW5.Q.tra:RemoteRepairCentre)
  BRW5.AddField(tra:RecordNumber,BRW5.Q.tra:RecordNumber)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:DISCOUNT.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         HIDE(?List)
         HIDE(?DASTAG)
         HIDE(?DASTAGALL)
         HIDE(?DASUNTAGALL)
      ELSE
          UNHIDE(?List)
          UNHIDE(?DASTAG)
          UNHIDE(?DASTAGALL)
          UNHIDE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
          Access:TRADEACC_ALIAS.CLEARKEY(tra_ali:Account_Number_Key)
          SET(tra_ali:Account_Number_Key,tra_ali:Account_Number_Key)
          LOOP
            IF Access:TRADEACC_ALIAS.NEXT() THEN BREAK.
            IF tra_ali:Account_Number <> LocalHeadAccount THEN
               IF tra_ali:RemoteRepairCentre = 0 THEN CYCLE.
            END !IF
            IF tra_ali:Account_Number = 'XXXRRC' THEN CYCLE.
            IF DoAll <> 'Y' THEN
               glo:Queue2.Pointer2 = tra_ali:RecordNumber
               GET(glo:Queue2,glo:Queue2.Pointer2)
               IF ERROR() THEN CYCLE.
            END !IF
            LOC:FileName = ''
            DO ExportSetup
            IF LOC:FileName = '' THEN CYCLE.
            DO ExportBody
            DO ExportFinalize
          END !LOOP
          IF NOT CancelPressed
              POST(Event:CloseWindow)
          END !IF
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?StartPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?EndPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?EndPopCalendar)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      IF RECORDS(BRW5) <> 0 AND DoAll <> 'Y' AND KeyCode() = MouseLeft2 THEN
         POST(Event:Accepted,?DASTAG)
      END !IF
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

GetHeadAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !
        OF 30 ! NOT Found
            ! Not in queue - ADD
            haq:AccountNumber            = IN:AccountNumber

            IF LoadTRADEACC(IN:AccountNumber)
                haq:AccountName          = tra:Company_Name
                haq:BranchIdentification = tra:BranchIdentification
            ELSE
                haq:AccountName          = '*T'
                haq:BranchIdentification = '*T'
            END !IF

            ADD(HeadAccount_Queue, +haq:AccountNumber)
        ELSE
            CancelPressed = True
        END !IF
        !-----------------------------------------------------------------
GetSubAccount PROCEDURE( IN:AccountNumber )
Temp          LONG
VAT_Rate_temp LIKE(vat:vat_rate)
    CODE
        !-----------------------------------------------------------------  
        saq:AccountNumber  = IN:AccountNumber
        GET(SubAccount_Queue, +saq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !
        OF 30 ! NOT Found
            !-------------------------------------------------------------
            ! Not in queue - ADD
            saq:AccountNumber            = job:Account_Number

            IF NOT LoadSUBTRACC(job:Account_Number)
                saq:AccountName          = ''
                saq:HeadAccountNumber    = ''
                saQ:HeadAccountName      = ''
                saQ:BranchIdentification = ''
                saQ:VATCode              = ''
                saQ:VATRate              = 0.00

                ADD(SubAccount_Queue, +saq:AccountNumber)

                RETURN
            END !IF
            !-------------------------------------------------------------
            GetHeadAccount(sub:Main_Account_Number)

            saq:AccountName          = sub:Company_Name
            saQ:HeadAccountNumber    = sub:Main_Account_Number
            saQ:HeadAccountName      = haQ:AccountName
            saQ:BranchIdentification = haQ:BranchIdentification

            If tra:Invoice_Sub_Accounts = 'YES'
                LOC:VATCode = sub:parts_vat_code
            Else
                LOC:VATCode = tra:parts_vat_code
            End!If tra:use_sub_accounts = 'YES'

            IF LoadVATCODE(LOC:VATCode)
                VAT_Rate_temp  = vat:vat_rate
            ELSE
                VAT_Rate_temp  = 0.00
            END ! IF

            saQ:VATCode              = LOC:VATCode
            saQ:VATRate              = VAT_Rate_temp

            ADD(SubAccount_Queue, +saq:AccountNumber)
            !-------------------------------------------------------------
        ELSE
            CancelPressed = True
        END !CASE
        !-----------------------------------------------------------------
GetWebJobNumber PROCEDURE( IN:JobNumber )! STRING
    CODE
        !-----------------------------------------------
        IF NOT LoadWEBJOB( job:Ref_Number)
            RETURN '*'
        END !IF

        RETURN IN:JobNumber & '-' & saQ:BranchIdentification & wob:JobNumber
        !-----------------------------------------------
LoadSTOCK       PROCEDURE(IN:PartNumber)! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = IN:PartNumber ! wpr:Part_Ref_Number

        IF Access:STOCK.TryFetch(sto:Ref_Number_Key)
            RETURN False
        END !IF

        IF NOT sto:Ref_Number = IN:PartNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadSUBTRACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:AccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF tra:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadVATCODE     PROCEDURE( IN:VATCode )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:VATCODE.ClearKey(vat:Vat_code_Key)
        vat:VAT_Code = IN:VATCode

        IF Access:VATCODE.TryFetch(vat:Vat_code_Key)
            RETURN False
        END !IF

        IF NOT vat:VAT_Code = IN:VATCode
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadWEBJOB      PROCEDURE( IN:JobNumber )! LONG
    CODE
        !-----------------------------------------------
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = IN:JobNumber

        IF Access:WEBJOB.TryFetch(wob:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        IF NOT wob:RefNumber = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = 'Y')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue

