

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01003.INC'),ONCE        !Local module procedure declarations
                     END


UpdateDEFWEB PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?dew:Account_Number
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?dew:Warranty_Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?dew:Transit_Type
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?dew:Job_Priority
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?dew:Status_Type
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?dew:Insurance_Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:8 QUEUE                           !Queue declaration for browse/combo box using ?dew:Chargeable_Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:6 QUEUE                           !Queue declaration for browse/combo box using ?dew:HandSet
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB7::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB8::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB9::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB10::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB11::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB14::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB12::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
History::dew:Record  LIKE(dew:RECORD),STATIC
QuickWindow          WINDOW('Update the DEFWEB File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           COMBO(@s15),AT(244,112,124,10),USE(dew:Account_Number),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,FORMAT('65L(2)|M@s15@120L(2)|M@s30@'),DROP(10,200),FROM(Queue:FileDropCombo)
                           PROMPT('Handset Type'),AT(168,128),USE(?dew:handset:prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(244,128,124,10),USE(dew:HandSet),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:6),MSG('Handset Type')
                           GROUP('Transit Type'),AT(168,142,204,60),USE(?Delivery),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           PROMPT('Delivery'),AT(172,152),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(244,152,124,10),USE(dew:Transit_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           PROMPT('Collection'),AT(172,168),USE(?Prompt4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(244,168,124,10),USE(dew:Job_Priority),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                           PROMPT('Exchange'),AT(172,184),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(244,184,124,10),USE(dew:Status_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:4)
                           GROUP('Charge Type'),AT(168,206,204,60),USE(?Warranty),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           PROMPT('Warranty'),AT(172,216),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(244,216,124,10),USE(dew:Warranty_Charge_Type),IMM,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Insurance'),AT(172,232),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(244,232,124,10),USE(dew:Insurance_Charge_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:5)
                           PROMPT('Chargeable'),AT(172,248),USE(?Prompt9),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(244,248,124,10),USE(dew:Chargeable_Charge_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:8)
                           GROUP('Allow Access To'),AT(376,108,136,186),USE(?Group3),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           CHECK('New Job Booking'),AT(384,120),USE(dew:AllowJobBooking),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('New Job Booking'),TIP('New Job Booking'),VALUE('1','0')
                           CHECK('Job Reports'),AT(384,134),USE(dew:Option6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Job Reports'),TIP('Job Reports'),VALUE('1','0')
                           CHECK('Job Progress'),AT(384,148),USE(dew:AllowJobProgress),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Job Progress'),TIP('Job Progress'),VALUE('1','0')
                           CHECK('Order Reports'),AT(384,178),USE(dew:Option7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Order Reports'),TIP('Order Reports'),VALUE('1','0')
                           CHECK('Order Parts'),AT(384,164),USE(dew:AllowOrderParts),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Order Parts'),TIP('Order Parts'),VALUE('1','0')
                           CHECK('Use I.M.E.I. No. Validation'),AT(384,224),USE(dew:Option8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use I.M.E.I. No. Validation'),TIP('Use I.M.E.I. No. Validation'),VALUE('1','0')
                           CHECK('Order Progress'),AT(384,192),USE(dew:AllowOrderProgress),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Order Progress'),TIP('Order Progress'),VALUE('1','0')
                           CHECK('Use I.M.E.I. No. Lookup'),AT(384,238),USE(dew:Option9),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use I.M.E.I. No. Lookup'),TIP('Use I.M.E.I. No. Lookup'),VALUE('1','0')
                           CHECK('Accept/Reject Estimates'),AT(384,208),USE(dew:Option5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Accept/Reject Estimates'),TIP('Accept/Reject Estimates'),VALUE('1','0')
                           CHECK('Option 10'),AT(384,252),USE(dew:Option10),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Option 10'),TIP('Option 10'),VALUE('1','0')
                           PROMPT('Account Number'),AT(168,112),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Web Default'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(388,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(452,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB9                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
                     END

FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

FDCB14               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:8         !Reference to browse queue type
                     END

FDCB12               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:6         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting The Web Defaults'
  OF ChangeRecord
    ActionMessage = 'Changing The Web Defaults'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020140'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateDEFWEB')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?dew:Account_Number
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(dew:Record,History::dew:Record)
  SELF.AddHistoryField(?dew:Account_Number,2)
  SELF.AddHistoryField(?dew:HandSet,9)
  SELF.AddHistoryField(?dew:Transit_Type,5)
  SELF.AddHistoryField(?dew:Job_Priority,6)
  SELF.AddHistoryField(?dew:Status_Type,8)
  SELF.AddHistoryField(?dew:Warranty_Charge_Type,3)
  SELF.AddHistoryField(?dew:Insurance_Charge_Type,11)
  SELF.AddHistoryField(?dew:Chargeable_Charge_Type,10)
  SELF.AddHistoryField(?dew:AllowJobBooking,12)
  SELF.AddHistoryField(?dew:Option6,17)
  SELF.AddHistoryField(?dew:AllowJobProgress,13)
  SELF.AddHistoryField(?dew:Option7,18)
  SELF.AddHistoryField(?dew:AllowOrderParts,14)
  SELF.AddHistoryField(?dew:Option8,19)
  SELF.AddHistoryField(?dew:AllowOrderProgress,15)
  SELF.AddHistoryField(?dew:Option9,20)
  SELF.AddHistoryField(?dew:Option5,16)
  SELF.AddHistoryField(?dew:Option10,21)
  SELF.AddUpdateFile(Access:DEFWEB)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:DEFWEB.Open
  Relate:TRANTYPE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFWEB
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateDEFWEB')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB6.Init(dew:Account_Number,?dew:Account_Number,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sub:Account_Number_Key)
  FDCB6.AddField(sub:Account_Number,FDCB6.Q.sub:Account_Number)
  FDCB6.AddField(sub:Company_Name,FDCB6.Q.sub:Company_Name)
  FDCB6.AddField(sub:RecordNumber,FDCB6.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB7.Init(dew:Warranty_Charge_Type,?dew:Warranty_Charge_Type,Queue:FileDropCombo:1.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo:1,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo:1
  FDCB7.AddSortOrder(cha:Warranty_Ref_Number_Key)
  FDCB7.SetFilter('Upper(cha:warranty) = ''YES''')
  FDCB7.AddField(cha:Charge_Type,FDCB7.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  FDCB8.Init(dew:Transit_Type,?dew:Transit_Type,Queue:FileDropCombo:2.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:2,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:2
  FDCB8.AddSortOrder(trt:Transit_Type_Key)
  FDCB8.AddField(trt:Transit_Type,FDCB8.Q.trt:Transit_Type)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  FDCB9.Init(dew:Job_Priority,?dew:Job_Priority,Queue:FileDropCombo:3.ViewPosition,FDCB9::View:FileDropCombo,Queue:FileDropCombo:3,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB9.Q &= Queue:FileDropCombo:3
  FDCB9.AddSortOrder(trt:Transit_Type_Key)
  FDCB9.AddField(trt:Transit_Type,FDCB9.Q.trt:Transit_Type)
  ThisWindow.AddItem(FDCB9.WindowComponent)
  FDCB9.DefaultFill = 0
  FDCB10.Init(dew:Status_Type,?dew:Status_Type,Queue:FileDropCombo:4.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:4,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:4
  FDCB10.AddSortOrder(trt:Transit_Type_Key)
  FDCB10.AddField(trt:Transit_Type,FDCB10.Q.trt:Transit_Type)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  FDCB11.Init(dew:Insurance_Charge_Type,?dew:Insurance_Charge_Type,Queue:FileDropCombo:5.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo:5,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo:5
  FDCB11.AddSortOrder(cha:Warranty_Ref_Number_Key)
  FDCB11.SetFilter('Upper(cha:warranty) = ''NO''')
  FDCB11.AddField(cha:Charge_Type,FDCB11.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  FDCB14.Init(dew:Chargeable_Charge_Type,?dew:Chargeable_Charge_Type,Queue:FileDropCombo:8.ViewPosition,FDCB14::View:FileDropCombo,Queue:FileDropCombo:8,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB14.Q &= Queue:FileDropCombo:8
  FDCB14.AddSortOrder(cha:Warranty_Ref_Number_Key)
  FDCB14.SetFilter('Upper(cha:warranty) = ''NO''')
  FDCB14.AddField(cha:Charge_Type,FDCB14.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB14.WindowComponent)
  FDCB14.DefaultFill = 0
  FDCB12.Init(dew:HandSet,?dew:HandSet,Queue:FileDropCombo:6.ViewPosition,FDCB12::View:FileDropCombo,Queue:FileDropCombo:6,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB12.Q &= Queue:FileDropCombo:6
  FDCB12.AddSortOrder(uni:Unit_Type_Key)
  FDCB12.AddField(uni:Unit_Type,FDCB12.Q.uni:Unit_Type)
  ThisWindow.AddItem(FDCB12.WindowComponent)
  FDCB12.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFWEB.Close
    Relate:TRANTYPE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateDEFWEB')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020140'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020140'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020140'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Web_Defaults PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(DEFWEB)
                       PROJECT(dew:Account_Number)
                       PROJECT(dew:Record_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
dew:Account_Number     LIKE(dew:Account_Number)       !List box control field - type derived from field
dew:Record_Number      LIKE(dew:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Web Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('64L(2)|M~Account Number~@s15@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Account Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(264,98,124,10),USE(dew:Account_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(448,191),USE(?Insert:2),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(448,220),USE(?Change:2),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(448,247),USE(?Delete:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Web Default File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020139'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Web_Defaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFWEB.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:DEFWEB,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Web_Defaults')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,dew:Account_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?dew:Account_Number,dew:Account_Number,1,BRW1)
  BRW1.AddField(dew:Account_Number,BRW1.Q.dew:Account_Number)
  BRW1.AddField(dew:Record_Number,BRW1.Q.dew:Record_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFWEB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Web_Defaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateDEFWEB
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020139'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020139'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020139'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?dew:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dew:Account_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dew:Account_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Do_not_delete PROCEDURE                               !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Do_not_delete')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXPGEN.Open
  Relate:SUPPTEMP.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Do_not_delete')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXPGEN.Close
    Relate:SUPPTEMP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Do_not_delete')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
StockDefaults PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
tmp:NokiaEDIRate     REAL
tmp:UseStockLevels   BYTE(0)
tmp:IgnorePriceChange BYTE(0)
tmp:UseVirtualPrice  BYTE(0)
tmp:AveragePurchaseCost BYTE(0)
tmp:UseRapidStock    BYTE(0)
Tmp:UseScheduledMinStock BYTE(0)
tmp:MaxPartMarkup    LONG
tmp:OrderNumberPrefix STRING(3)
tmp:NextOrderNumber  LONG
locPartPriceVariant  LONG
locUseLoanReturnDays BYTE
LoanReturnDays       LONG
History::dst:Record  LIKE(dst:RECORD),STATIC
QuickWindow          WINDOW('Stock Control Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Control Defaults'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           GROUP,AT(192,298,168,10),USE(?GroupLoanReturnDays)
                             PROMPT('Loan Return Days'),AT(192,298),USE(?LoanReturnDays:Prompt),FONT(,,COLOR:White,FONT:bold)
                             ENTRY(@n_8),AT(296,298,64,10),USE(LoanReturnDays),RIGHT(1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           END
                           CHECK('Use Default Site Location'),AT(295,110,112,8),USE(dst:Use_Site_Location),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           GROUP,AT(172,112,264,24),USE(?Group1),DISABLE
                             PROMPT('Site Location'),AT(192,120),USE(?dst:Site_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(295,120,124,10),USE(dst:Site_Location),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             BUTTON,AT(423,116),USE(?LookupSiteLocation),SKIP,TRN,FLAT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),ICON('lookupp.jpg')
                           END
                           CHECK(' Force Requisition No.'),AT(295,136),USE(de2:UseRequisitionNumber),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0'),MSG('Use Requisition Number')
                           CHECK('Use Stock Access Levels (Webmaster)'),AT(295,146),USE(tmp:UseStockLevels),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use Stock Levels (Webmaster)'),TIP('Use Stock Levels (Webmaster)'),VALUE('1','0')
                           CHECK('Ignore Received Parts Price Adjustment'),AT(295,158),USE(tmp:IgnorePriceChange),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Ignore Received Parts Price Adjustment'),TIP('Ignore Received Parts Price Adjustment'),VALUE('1','0')
                           CHECK('Use Virtual Site Price Structure'),AT(295,170),USE(tmp:UseVirtualPrice),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use Virtual Site Price Structure'),TIP('Use Virtual Site Price Structure'),VALUE('1','0')
                           CHECK('Use Average Purchase Cost'),AT(295,182),USE(tmp:AveragePurchaseCost),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use Average Purchase Cost'),TIP('Use Average Purchase Cost'),VALUE('1','0')
                           CHECK('Use Rapid Stock Allocation'),AT(295,194),USE(tmp:UseRapidStock),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use Rapid Stock Allocation'),TIP('Use Rapid Stock Allocation'),VALUE('1','0')
                           CHECK('Use Scheduled Minimum Stock'),AT(295,206),USE(Tmp:UseScheduledMinStock),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Max Part Markup'),AT(192,222),USE(?tmp:MaxPartMarkup:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           SPIN(@n8),AT(295,222,64,10),USE(tmp:MaxPartMarkup),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Max Part Markup'),TIP('Max Part Markup'),UPR,STEP(1)
                           PROMPT(' % '),AT(363,222),USE(?Prompt4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT(' % '),AT(364,266),USE(?Prompt4:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           CHECK('Use Loan Return Days'),AT(296,286),USE(locUseLoanReturnDays),FONT(,,COLOR:White,FONT:bold),VALUE('1','0')
                           PROMPT('Parts Order Prefix'),AT(192,240),USE(?tmp:OrderPrefix:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s3),AT(296,240,64,10),USE(tmp:OrderNumberPrefix),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Order Prefix'),TIP('Order Prefix'),UPR
                           PROMPT('Next Order Number'),AT(372,240),USE(?tmp:NextOrderNumber:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(448,240,64,10),USE(tmp:NextOrderNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Next Order Number'),TIP('Next Order Number'),UPR
                           PROMPT('(Leave blank for ''SS'')'),AT(192,250),USE(?Prompt9),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('(Only used if higher than last order no)'),AT(368,252),USE(?Prompt10),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Receive Part Price Variant'),AT(192,266),USE(?locPartPriceVariant:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           SPIN(@n3),AT(296,266,64,10),USE(locPartPriceVariant),RIGHT(2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),RANGE(0,100),STEP(1)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       PROMPT('EDI Euro Rate'),AT(172,342),USE(?tmp:NokiaEDIRate:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@n23.11),AT(248,342,64,10),USE(tmp:NokiaEDIRate),HIDE,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:dst:Site_Location                Like(dst:Site_Location)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting Stock Defaults'
  OF ChangeRecord
    ActionMessage = 'Stock Control Defaults'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020135'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:DEFSTOCK.Open
    SET(DEFSTOCK)
    CASE Access:DEFSTOCK.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:DEFSTOCK.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('StockDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(dst:Record,History::dst:Record)
  SELF.AddHistoryField(?dst:Use_Site_Location,1)
  SELF.AddHistoryField(?dst:Site_Location,3)
  SELF.AddUpdateFile(Access:DEFSTOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SET(Default2)
  Access:Default2.Next()
  
  ! Inserting (DBH 30/11/2007) # 9034 - Allow to specify the order prefix
  tmp:OrderNumberPrefix = GETINI('STOCK','OrderNumberPrefix',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:NextOrderNumber = GETINI('STOCK','NextOrderNumber',,Clip(Path()) & '\SB2KDEF.INI')
  ! End (DBH 30/11/2007) #9034
  locPartPriceVariant = GETINI('STOCK','PartPriceVariant',,Clip(Path()) & '\SB2KDEF.INI') !! #11382 Part Price Variant Default (DBH: 06/08/2010)
  locUseLoanReturnDays = GETINI('STOCK','UseLoanReturnDays',,CLIP(PATH()) & '\SB2KDEF.INI')  ! #12341 Set Loan return time (DBH: 27/01/2012)
  LoanReturnDays = GETINI('STOCK','LoanReturnDays',,CLIP(PATH()) & '\SB2KDEF.INI')
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If Instring('COMMUNICAID',def:User_Name,1,1)
      ?tmp:NokiaEDIRate{prop:Hide} = 0
      ?tmp:NokiaEDIRate:Prompt{prop:Hide} = 0
  End !Instring('COMMUNICAID',def:User_Name,1,1)
  tmp:NokiaEDIRate = GETINI('EDI','PartsOrderEuroRate',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:UseStockLevels = GETINI('WEBMASTER','StockLevels',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:IgnorePriceChange = GETINI('STOCK','IgnorePriceChange',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:UseVirtualPrice = GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:AveragePurchaseCost = GETINI('STOCK','UseAveragePurchaseCost',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:UseRapidStock       =GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:UseScheduledMinStock       =GETINI('STOCK','UseScheduledMinStock',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:MaxPartMarkup   =GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
  ! Save Window Name
   AddToLog('Window','Open','StockDefaults')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?dst:Site_Location{Prop:Tip} AND ~?LookupSiteLocation{Prop:Tip}
     ?LookupSiteLocation{Prop:Tip} = 'Select ' & ?dst:Site_Location{Prop:Tip}
  END
  IF ?dst:Site_Location{Prop:Msg} AND ~?LookupSiteLocation{Prop:Msg}
     ?LookupSiteLocation{Prop:Msg} = 'Select ' & ?dst:Site_Location{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?dst:Use_Site_Location{Prop:Checked} = True
    ENABLE(?Group1)
  END
  IF ?dst:Use_Site_Location{Prop:Checked} = False
    DISABLE(?LookupSiteLocation)
  END
  IF ?tmp:UseVirtualPrice{Prop:Checked} = True
    UNHIDE(?tmp:AveragePurchaseCost)
  END
  IF ?tmp:UseVirtualPrice{Prop:Checked} = False
    HIDE(?tmp:AveragePurchaseCost)
  END
  IF ?locUseLoanReturnDays{Prop:Checked} = True
    ENABLE(?GroupLoanReturnDays)
  END
  IF ?locUseLoanReturnDays{Prop:Checked} = False
    DISABLE(?GroupLoanReturnDays)
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(DEFSTOCK)
      Access:DEFSTOCK.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','StockDefaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Location
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020135'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020135'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020135'&'0')
      ***
    OF ?dst:Use_Site_Location
      IF ?dst:Use_Site_Location{Prop:Checked} = True
        ENABLE(?Group1)
      END
      IF ?dst:Use_Site_Location{Prop:Checked} = False
        DISABLE(?LookupSiteLocation)
      END
      ThisWindow.Reset
    OF ?dst:Site_Location
      IF dst:Site_Location OR ?dst:Site_Location{Prop:Req}
        loc:Location = dst:Site_Location
        !Save Lookup Field Incase Of error
        look:dst:Site_Location        = dst:Site_Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            dst:Site_Location = loc:Location
          ELSE
            !Restore Lookup On Error
            dst:Site_Location = look:dst:Site_Location
            SELECT(?dst:Site_Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSiteLocation
      ThisWindow.Update
      loc:Location = dst:Site_Location
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          dst:Site_Location = loc:Location
          Select(?+1)
      ELSE
          Select(?dst:Site_Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dst:Site_Location)
    OF ?tmp:UseVirtualPrice
      IF ?tmp:UseVirtualPrice{Prop:Checked} = True
        UNHIDE(?tmp:AveragePurchaseCost)
      END
      IF ?tmp:UseVirtualPrice{Prop:Checked} = False
        HIDE(?tmp:AveragePurchaseCost)
      END
      ThisWindow.Reset
    OF ?locUseLoanReturnDays
      IF ?locUseLoanReturnDays{Prop:Checked} = True
        ENABLE(?GroupLoanReturnDays)
      END
      IF ?locUseLoanReturnDays{Prop:Checked} = False
        DISABLE(?GroupLoanReturnDays)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  Access:Default2.Update()
  PUTINIext('EDI','PartsOrderEuroRate',tmp:NokiaEDIRate,CLIP(PATH()) & '\SB2KDEF.INI')
  PUTINIext('WEBMASTER','StockLevels',tmp:UseStockLevels,CLIP(PATH()) & '\SB2KDEF.INI')
  PUTINIext('STOCK','IgnorePriceChange',tmp:IgnorePriceChange,CLIP(PATH()) & '\SB2KDEF.INI')
  PUTINIext('STOCK','UseVirtualPrice',tmp:UseVirtualPrice,CLIP(PATH()) & '\SB2KDEF.INI')
  PUTINIext('STOCK','UseAveragePurchaseCost',tmp:AveragePurchaseCost,CLIP(PATH()) & '\SB2KDEF.INI')
  PUTINIext('STOCK','UseRapidSTockAllocation',tmp:UseRapidStock,CLIP(PATH()) & '\SB2KDEF.INI')
  PUTINIext('STOCK','UseScheduledMinStock',tmp:UseScheduledMinStock,CLIP(PATH()) & '\SB2KDEF.INI')
  PUTINIext('STOCK','MaxPartMarkup',tmp:MaxPartMarkup,CLIP(PATH()) & '\SB2KDEF.INI')
  
  
  ! Inserting (DBH 30/11/2007) # 9034 - Allow to specify the order prefix
  PUTINIext('STOCK','OrderNumberPrefix',tmp:OrderNumberPrefix,Clip(Path()) & '\SB2KDEF.INI')
  PUTINIext('STOCK','NextOrderNumber',tmp:NextOrderNumber,Clip(Path()) & '\SB2KDEF.INI')
  ! End (DBH 30/11/2007) #9034
  PUTINIext('STOCK','PartPriceVariant',locPartPriceVariant,Clip(Path()) & '\SB2KDEF.INI') !! #11382 Part Price Variant Default (DBH: 06/08/2010)
  PUTINIext('STOCK','UseLoanReturnDays',locUseLoanReturnDays,CLIP(PATH()) & '\SB2KDEF.INI')  ! #12341 Set Loan return time (DBH: 27/01/2012)
  PUTINIext('STOCK','LoanReturnDays',LoanReturnDays,CLIP(PATH()) & '\SB2KDEF.INI')
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

RetailSalesDefaults PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
tmp:MaxMarkup        LONG
tmp:ForcePurchaseOrdNo BYTE(0)
History::def:Record  LIKE(def:RECORD),STATIC
QuickWindow          WINDOW('Retail Sales Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Retail Sales Defaults'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           CHECK('Retail Back Orders    '),AT(319,156),USE(def:RetBackOrders),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Retail Back Orders'),TIP('Retail Back Orders'),VALUE('1','0')
                           PROMPT('Euro Exchange Rate'),AT(223,177),USE(?def:EuroRate:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n23.11),AT(319,177,64,10),USE(def:EuroRate),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Euro Exchange Rate'),TIP('Euro Exchange Rate'),UPR
                           STRING('(Euros to Sterling)'),AT(387,177),USE(?String1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Max Purch Cost Markup'),AT(223,199),USE(?tmp:MaxMarkup:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(319,199,64,10),USE(tmp:MaxMarkup),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Maximum Purchase Cost Markup'),TIP('Maximum Purchase Cost Markup'),UPR
                           PROMPT('%'),AT(387,199),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Force Purch Ord No'),AT(319,215),USE(tmp:ForcePurchaseOrdNo),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Force Purch Ord No'),TIP('Force Purch Ord No'),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(384,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Retail Sales Defaults'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020131'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Open
    SET(DEFAULTS)
    CASE Access:DEFAULTS.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:DEFAULTS.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('RetailSalesDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(def:Record,History::def:Record)
  SELF.AddHistoryField(?def:RetBackOrders,126)
  SELF.AddHistoryField(?def:EuroRate,129)
  SELF.AddUpdateFile(Access:DEFAULTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFAULTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:MaxMarkup = GETINI('RETAIL','MaxMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ForcePurchaseOrdNo = GETINI('RETAIL','ForcePurchaseOrderNumber',,CLIP(PATH())&'\SB2KDEF.INI')
  ! Save Window Name
   AddToLog('Window','Open','RetailSalesDefaults')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(DEFAULTS)
      Access:DEFAULTS.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','RetailSalesDefaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020131'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020131'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020131'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  PUTINIext('RETAIL','MaxMarkup',tmp:MaxMarkup,CLIP(PATH()) & '\SB2KDEF.INI')
  PUTINIext('RETAIL','ForcePurchaseOrderNumber',tmp:ForcePurchaseOrdNo,CLIP(PATH()) & '\SB2KDEF.INI')
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

CompulsoryFields PROCEDURE                            !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ForceAccessories STRING('0')
tmp:ForceNetwork     STRING('0')
tmp:MobileLengthFrom LONG
tmp:MobileLengthTo   LONG
tmp:MobileNumberFormat STRING(30)
tmp:MobileValidationType BYTE(0)
window               WINDOW('General Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Compulsory Field Defaults'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('&Compulsory Fields'),USE(?Tab5)
                           GROUP,AT(224,303,220,10),USE(?Group:MobileNoFormat)
                             PROMPT('Mobile Number Format'),AT(224,303),USE(?tmp:MobileNumberFormat:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(320,303,124,10),USE(tmp:MobileNumberFormat),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Mobile Number Format'),TIP('Mobile Number Format'),UPR
                           END
                           GROUP,AT(224,277,161,22),USE(?Group:MobileNoLength)
                             PROMPT('From'),AT(320,277),USE(?tmp:MobileLengthFrom:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('To'),AT(356,277),USE(?tmp:MobileLengthTo:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Mobile No Length'),AT(224,288),USE(?Prompt27),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n2),AT(320,287,29,11),USE(tmp:MobileLengthFrom),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Mobile Number Length From'),TIP('Mobile Number Length From'),UPR
                             ENTRY(@n2),AT(356,287,29,11),USE(tmp:MobileLengthTo),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Mobile Number Length To'),TIP('Mobile Number Length To'),UPR
                           END
                           OPTION,AT(428,157,88,12),USE(def:Force_Repair_Type),TRN
                             RADIO,AT(432,157),USE(?DEF:Force_Repair_Type:Radio1),TRN,VALUE('I')
                             RADIO,AT(464,157),USE(?DEF:Force_Repair_Type:Radio2),TRN,VALUE('B')
                             RADIO,AT(496,157),USE(?DEF:Force_Repair_Type:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Incoming Courier'),AT(352,181),USE(?Prompt43),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(428,168,88,12),USE(def:Force_Authority_Number),TRN
                             RADIO,AT(432,168),USE(?DEF:Force_Authority_Number:Radio1),TRN,VALUE('I')
                             RADIO,AT(464,168),USE(?DEF:Force_Authority_Number:Radio2),TRN,VALUE('B')
                             RADIO,AT(496,168),USE(?DEF:Force_Authority_Number:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(428,181,88,12),USE(def:Force_Incoming_Courier),TRN
                             RADIO,AT(432,181),USE(?DEF:Force_Incoming_Courier:Radio1),TRN,VALUE('I')
                             RADIO,AT(464,181),USE(?DEF:Force_Incoming_Courier:Radio2),DISABLE,TRN,VALUE('B')
                             RADIO,AT(496,181),USE(?DEF:Force_Incoming_Courier:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Outgoing Courier'),AT(352,192),USE(?Prompt43:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(244,237,88,12),USE(def:Customer_Name),TRN
                             RADIO,AT(248,240),USE(?DEF:Customer_Name:Radio1),TRN,VALUE('I')
                             RADIO,AT(280,240),USE(?DEF:Customer_Name:Radio2),TRN,VALUE('B')
                             RADIO,AT(312,240),USE(?DEF:Customer_Name:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(244,133,88,12),USE(def:Force_Initial_Transit_Type),TRN
                             RADIO,AT(248,133),USE(?DEF:Force_Initial_Transit_Type:Radio1),TRN,VALUE('I')
                             RADIO,AT(280,133),USE(?DEF:Force_Initial_Transit_Type:Radio2),TRN,VALUE('B')
                             RADIO,AT(312,133),USE(?DEF:Force_Initial_Transit_Type:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(428,229,88,12),USE(def:ForceDelPostcode),TRN
                             RADIO,AT(432,229),USE(?DEF:Force_Initial_Transit_Type:Radio1:2),TRN,VALUE('I')
                             RADIO,AT(464,229),USE(?DEF:Force_Initial_Transit_Type:Radio2:2),TRN,VALUE('B')
                             RADIO,AT(496,229),USE(?DEF:Force_Initial_Transit_Type:Radio3:2),TRN,VALUE('C')
                           END
                           PROMPT('Job Accessories'),AT(352,240),USE(?Prompt25:4),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(428,239,88,12),USE(tmp:ForceAccessories)
                             RADIO,AT(432,240),USE(?tmp:ForceAccessories:Radio46),VALUE('0')
                             RADIO,AT(464,240),USE(?tmp:ForceAccessories:Radio47),VALUE('B')
                             RADIO,AT(496,240),USE(?tmp:ForceAccessories:Radio48),VALUE('C')
                           END
                           PROMPT('Network'),AT(352,253),USE(?Network),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           STRING('Completion'),AT(296,85,32,42),USE(?String2:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           STRING('Booking'),AT(448,95,32,36),USE(?String2:5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           STRING('Ignore'),AT(232,98,32,32),USE(?String2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           STRING('Booking'),AT(264,95,32,36),USE(?String2:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           STRING('Completion'),AT(480,87,32,42),USE(?String2:6),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           OPTION,AT(428,250,88,12),USE(tmp:ForceNetwork)
                             RADIO,AT(432,253),USE(?tmp:ForceNetwork:Radio43),VALUE('0')
                             RADIO,AT(464,253),USE(?tmp:ForceNetwork:Radio44),VALUE('B')
                             RADIO,AT(496,253),USE(?tmp:ForceNetwork:Radio45),VALUE('I')
                           END
                           OPTION('Mobile No Validation Type'),AT(68,274,132,54),USE(tmp:MobileValidationType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Mobile No Length'),AT(76,285),USE(?tmp:MobileValidationType:Radio43),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Mobile No Format'),AT(76,301),USE(?tmp:MobileValidationType:Radio44),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('No Validation'),AT(76,317),USE(?tmp:MobileValidationType:Radio45),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('99')
                           END
                           GROUP('Mobile Format Key'),AT(464,282,148,54),USE(?Group:FormatKey),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('0     - Any Number'),AT(468,293),USE(?Prompt40),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('*x* - Fixed Number'),AT(468,303),USE(?Prompt40:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('e.g. 10 digit number starting with 07:'),AT(468,314),USE(?Prompt42),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('*07*00000000'),AT(484,325),USE(?Prompt44)
                           END
                           PROMPT('Initial Transit Type'),AT(164,133),USE(?Prompt25),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Delivery Postcode'),AT(352,229),USE(?Prompt25:3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Mobile Number'),AT(164,144),USE(?Prompt29),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Model Number'),AT(164,157),USE(?Prompt30),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(244,157,88,12),USE(def:Force_Model_Number),TRN
                             RADIO,AT(248,157),USE(?DEF:Force_Model_Number:Radio1),TRN,VALUE('I')
                             RADIO,AT(280,157),USE(?DEF:Force_Model_Number:Radio2),TRN,VALUE('B')
                             RADIO,AT(312,157),USE(?DEF:Force_Model_Number:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(244,178,88,12),USE(def:Force_Fault_Description),TRN
                             RADIO,AT(248,181),USE(?DEF:Force_Fault_Description:Radio1),TRN,VALUE('I')
                             RADIO,AT(280,181),USE(?DEF:Force_Fault_Description:Radio2),TRN,VALUE('B')
                             RADIO,AT(312,181),USE(?DEF:Force_Fault_Description:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(428,205,88,12),USE(def:Force_Spares),TRN
                             RADIO,AT(432,205),USE(?DEF:Force_Spares:Radio1),TRN,VALUE('I')
                             RADIO,AT(464,205),USE(?DEF:Force_Spares:Radio2),TRN,VALUE('B')
                             RADIO,AT(496,205),USE(?DEF:Force_Spares:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Unit Type'),AT(164,168),USE(?Prompt31),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Fault Description'),AT(164,181),USE(?Prompt32),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('I.M.E.I. Number'),AT(164,192),USE(?Prompt33),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('M.S.N.'),AT(164,205),USE(?Prompt34),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Colour'),AT(164,216),USE(?Colour),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(244,144,88,12),USE(def:Force_Mobile_Number),TRN
                             RADIO,AT(248,144),USE(?DEF:Force_Mobile_Number:Radio1),TRN,VALUE('I')
                             RADIO,AT(280,144),USE(?DEF:Force_Mobile_Number:Radio2),TRN,VALUE('B')
                             RADIO,AT(312,144),USE(?DEF:Force_Mobile_Number:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Customer Postcode'),AT(352,216),USE(?Prompt25:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(428,216,88,12),USE(def:ForcePostcode)
                             RADIO,AT(432,216),USE(?DEF:ForcePostcode:Radio31),VALUE('I')
                             RADIO,AT(464,216),USE(?DEF:ForcePostcode:Radio32),VALUE('B')
                             RADIO,AT(496,216),USE(?DEF:ForcePostcode:Radio33),VALUE('C')
                           END
                           PROMPT('Job Type'),AT(164,253),USE(?Prompt35),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(244,191,88,12),USE(def:Force_ESN),TRN
                             RADIO,AT(248,192),USE(?DEF:Force_ESN:Radio1),TRN,VALUE('I')
                             RADIO,AT(280,192),USE(?DEF:Force_ESN:Radio2),TRN,VALUE('B')
                             RADIO,AT(312,192),USE(?DEF:Force_ESN:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Engineer'),AT(352,133),USE(?Prompt36),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(428,133,88,12),USE(def:Force_Engineer),TRN
                             RADIO,AT(432,133),USE(?DEF:Force_Engineer:Radio1),TRN,VALUE('I')
                             RADIO,AT(464,133),USE(?DEF:Force_Engineer:Radio2),TRN,VALUE('B')
                             RADIO,AT(496,133),USE(?DEF:Force_Engineer:Radio3),TRN,VALUE('C')
                           END
                           STRING('Ignore'),AT(416,98,32,32),USE(?String2:4),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           OPTION,AT(428,192,88,12),USE(def:Force_Outoing_Courier)
                             RADIO,AT(432,192),USE(?DEF:Force_Outoing_Courier:Radio22),VALUE('I')
                             RADIO,AT(464,192),USE(?DEF:Force_Outoing_Courier:Radio23),VALUE('B')
                             RADIO,AT(496,192),USE(?DEF:Force_Outoing_Courier:Radio24),VALUE('C')
                           END
                           OPTION,AT(244,250,88,12),USE(def:Force_Job_Type),TRN
                             RADIO,AT(248,253),USE(?DEF:Force_Job_Type:Radio1),TRN,VALUE('I')
                             RADIO,AT(280,253),USE(?DEF:Force_Job_Type:Radio2),TRN,VALUE('B')
                             RADIO,AT(312,253),USE(?DEF:Force_Job_Type:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(244,167,88,12),USE(def:Force_Unit_Type),TRN
                             RADIO,AT(248,168),USE(?DEF:Force_Unit_Type:Radio1),TRN,VALUE('I')
                             RADIO,AT(280,168),USE(?DEF:Force_Unit_Type:Radio2),TRN,VALUE('B')
                             RADIO,AT(312,168),USE(?DEF:Force_Unit_Type:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Invoice Text'),AT(352,144),USE(?Prompt37),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(244,200,88,12),USE(def:Force_MSN),TRN
                             RADIO,AT(248,205),USE(?DEF:Force_MSN:Radio1),TRN,VALUE('I')
                             RADIO,AT(280,205),USE(?DEF:Force_MSN:Radio2),TRN,VALUE('B')
                             RADIO,AT(312,205),USE(?DEF:Force_MSN:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(244,213,88,12),USE(def:ForceCommonFault),MSG('Force Common Fault')
                             RADIO,AT(248,216),USE(?def:ForceCommonFault:Radio10),VALUE('I')
                             RADIO,AT(280,216),USE(?def:ForceCommonFault:Radio11),VALUE('B')
                             RADIO,AT(312,216),USE(?def:ForceCommonFault:Radio12),VALUE('C')
                           END
                           PROMPT('Order Number'),AT(164,229),USE(?order_number:prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(244,224,88,12),USE(def:Order_Number),TRN
                             RADIO,AT(248,229),USE(?DEF:Order_Number:Radio1),TRN,VALUE('I')
                             RADIO,AT(280,229),USE(?DEF:Order_Number:Radio2),TRN,VALUE('B')
                             RADIO,AT(312,229),USE(?DEF:Order_Number:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('End User Name'),AT(164,240),USE(?order_number:prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Repair Type'),AT(352,157),USE(?Prompt38),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           OPTION,AT(428,144,88,12),USE(def:Force_Invoice_Text),TRN
                             RADIO,AT(432,144),USE(?DEF:Force_Invoice_Text:Radio1),TRN,VALUE('I')
                             RADIO,AT(464,144),USE(?DEF:Force_Invoice_Text:Radio2),TRN,VALUE('B')
                             RADIO,AT(496,144),USE(?DEF:Force_Invoice_Text:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Authority Number'),AT(352,168),USE(?Prompt39),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Spares'),AT(352,205),USE(?Prompt41),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       BUTTON,AT(484,366),USE(?OkButton),FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
ShowHide            Routine
    Case tmp:MobileValidationType
    Of 0
        ?Group:MobileNoLength{prop:Hide} = False
        ?Group:MobileNoFormat{prop:Hide} = True
        ?Group:FormatKey{prop:Hide} = True
    Of 1
        ?Group:MobileNoLength{prop:Hide} = True
        ?Group:MobileNoFormat{prop:Hide} = False
        ?Group:FormatKey{prop:Hide} = False
    Else
        ?Group:MobileNoLength{prop:Hide} = True
        ?Group:MobileNoFormat{prop:Hide} = True
        ?Group:FormatKey{prop:Hide} = True
    End ! Case tmp:MobileValidationType
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020125'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CompulsoryFields')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  
  tmp:ForceAccessories = Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:ForceNetwork = Clip(GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI'))
  
  If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
      ?Colour{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
  End !Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
  ! Set Mobile No Length - TrkBs: 6141 (DBH: 23-08-2005)
  tmp:MobileLengthFrom = GETINI('COMPULSORY','MobileLengthFrom',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:MobileLengthTo   = GETINI('COMPULSORY','MobileLengthTo',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:MobileNumberFormat = GETINI('MOBILENUMBER','Format',,Clip(Path()) & '\SB2KDEF.INI')
  ! Inserting (DBH 22/06/2006) #7597 - Show new option for Mobile Number Type
  tmp:MobileValidationType = GETINI('MOBILENUMBER','FormatType',,Clip(Path()) & '\SB2KDEF.INI')
  Do ShowHide
  ! End (DBH 22/06/2006) #7597
  
  
  ! Save Window Name
   AddToLog('Window','Open','CompulsoryFields')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','CompulsoryFields')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020125'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020125'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020125'&'0')
      ***
    OF ?tmp:MobileValidationType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MobileValidationType, Accepted)
      Do ShowHide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MobileValidationType, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:defaults.update()
      PUTINIext('COMPULSORY','JobAccessories',tmp:ForceAccessories,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('COMPULSORY','Network',tmp:ForceNetwork,CLIP(PATH()) & '\SB2KDEF.INI')
      ! Set Mobile Length - TrkBs: 6141 (DBH: 23-08-2005)
      If tmp:MobileLengthFrom > tmp:MobileLengthTo
          Case Missive('The entered "Mobile No Length" values are invalid.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End ! If tmp:MobileLengthFrom > tmp:MobileLengthTo
      PUTINIext('COMPULSORY','MobileLengthFrom',tmp:MobileLengthFrom,Clip(Path()) & '\SB2KDEF.INI')
      PUTINIext('COMPULSORY','MobileLengthTo',tmp:MobileLengthTo,Clip(Path()) & '\SB2KDEF.INI')
      PUTINIext('MOBILENUMBER','Format',tmp:MobileNumberFormat,Clip(Path()) & '\SB2KDEF.INI')
      PUTINIext('MOBILENUMBER','FormatType',tmp:MobileValidationType,Clip(Path()) & '\SB2KDEF.INI')
      
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
MainDefaults PROCEDURE                                !Generated from procedure template - Window

LocalRequest         LONG
SB2KDEFINI           STRING(255)
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
tmp:RenameColour     BYTE(0)
tmp:ColourFieldName  STRING(30)
tmp:AccCheckAtComp   BYTE(0)
tmp:StopLiveBouncers BYTE(0)
tmp:HideSkillLevel   BYTE(0)
tmp:BouncerType      BYTE(0)
tmp:UseBERComparison BYTE(0)
tmp:BERCostComparison LONG
tmp:CheckPartAvailability BYTE(0)
tmp:OverdueBackorderText STRING(255)
tmp:HideNetwork      BYTE(0)
tmp:HideHubRepair    BYTE(0)
tmp:AutowriteAccount BYTE(0)
tmp:GroupDespatch    BYTE(0)
tmp:ReplicateSubAccounts BYTE(0)
tmp:DefaultSubAccount STRING(30)
tmp:HideEngLocation  BYTE(0)
tmp:RenameCourierCost BYTE(0)
tmp:CourierCostName  STRING(30)
tmp:RenameAuthorityNo BYTE
tmp:AuthorityNoName  STRING(20)
tmp:DoNotDespatchLoan BYTE(0)
tmp:DoNotUseLoanAuthTable BYTE(0)
tmp:DisableLocation  BYTE(0)
tmp:UseBastionWarrantyChecking BYTE(0)
tmp:IgnoreChargeableBouncers BYTE(0)
tmp:IgnoreWarrantyJobs BYTE(0)
tmp:HelpURL          STRING(255)
tmp:PDFBatchExportPath STRING(255)
tmp:XMLRepositoryFolder STRING(255)
tmp:CreateLine500Messages BYTE(0)
tmp:CreateOracleMessages BYTE(0)
tmp:STFMessage       STRING(30)
tmp:RIVMessage       STRING(30)
tmp:AOWMessage       STRING(30)
tmp:PDFURL           STRING(255)
tmp:CreateCIDMessages BYTE(0)
tmp:IMEIValidation   BYTE(0)
tmp:SBMQIPath        STRING(255)
tmp:EstimateFolder   STRING(255)
tmp:IMEIFTPAddress   STRING(30)
tmp:IMEIFTPUserName  STRING(60)
tmp:IMEIFTPPassword  STRING(60)
tmp:IMEIFTPLocation  STRING(255)
tmp:IMEIValidationURL STRING(255)
tmp:IMEIExceptionPath STRING(255)
tmp:MQRequestFolder  STRING(255)
tmp:KeepProcessedFiles BYTE(0)
tmp:SBOnlineURL      STRING(255)
tmp:NMSFTPSite       STRING(255)
tmp:NMSUsername      STRING(100)
tmp:NMSPassword      STRING(100)
tmp:NMSUploadFolder  STRING(255)
tmp:NMSWorkFolder    STRING(255)
tmp:NMSXMLFolder     STRING(255)
tmp:NMSSiteID        STRING(30)
tmp:NMSUserID        STRING(30)
tmp:NMSAccessKey     STRING(30)
tmp:NMSSourceSiteID  STRING(30)
tmp:NMSDownloadFolder STRING(255)
tmp:NMSUploadResponseFolder STRING(255)
tmp:NMSDownloadPreAlertFolder STRING(255)
tmp:NMSUploadPrealertFolder STRING(255)
tmp:WaybillEmailFileFolder STRING(255)
locMQRequestTimeOut  LONG
locExchangeRateImportPath STRING(255)
locLine500CSVImportFolder STRING(255)
locExceptionExchangeRate STRING(30)
locLoanSellingPriceExceptionFolder STRING(255)
locSMSExceptionFolder STRING(255)
locStockImportFailureFolder STRING(255)
locExchangeExceptionFolder STRING(255)
locExchangeImportSaveFolder STRING(255)
locAuditImportExceptionFolder STRING(255)
ApiActive            STRING(1)
ApiDirectory         STRING(255)
ApiArchive           STRING(1)
ApiArchiveDir        STRING(255)
VPortalDays          LONG
EVO_CreateFiles      BYTE
EVO_Company_code     STRING(4)
EVO_XREF1_HD         STRING(240)
EVO_Tax_Code         STRING(2)
EVO_Tax_Code_Exempt  STRING(2)
EVO_Vendor_Line_No   STRING(10)
EVO_Tax_Line_No      STRING(10)
FMWUserName          STRING(100)
FMWPassword          STRING(100)
CustomerLookup       STRING(1)
IdentifyStack        STRING(1)
CustomerExceptionFolder STRING(255)
CallUsing            STRING(10)
PathToSiebel         STRING(100)
PathToSiebel2        STRING(100)
PathToExe            STRING(255)
TP_Use               STRING(1)
TP_Address           STRING(100)
TP_Key               STRING(100)
TP_PathToExe         STRING(255)
TP_Logging           STRING(1)
TP_LogFolder         STRING(255)
UseAccredUsers       STRING(1)
CC_UseSystem         STRING(1)
CC_PathToExe         STRING(255)
CC_Timer             LONG
window               WINDOW('General Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(560,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('General Defaults'),AT(72,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(548,366),USE(?CancelButton),FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                       BUTTON,AT(480,366),USE(?OKButton),TRN,FLAT,ICON('okp.jpg')
                       CHECK('Summary Parts Orders'),AT(72,376),USE(def:SummaryOrders),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Summary Parts Orders'),TIP('Summary Parts Orders'),VALUE('1','0')
                       SHEET,AT(64,54,552,310),USE(?Sheet1),COLOR(09A6A7CH),SPREAD
                         TAB('Display Defaults'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP('Autosearch'),AT(72,116,256,36),USE(?Autosearchgroup),BOXED,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Default Autosearch'),AT(148,124),USE(def:Automatic_Replicate),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             PROMPT('Replicate To Field'),AT(76,137),USE(?DEF:Automatic_Replicate_Field:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             COMBO(@s15),AT(148,137,124,10),USE(def:Automatic_Replicate_Field),LEFT(2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,DROP(10),FROM('SURNAME|MOBILE NUMBER|E.S.N./I.M.E.I.|M.S.N.|POSTCODE')
                           END
                           GROUP('Hide Fields On Job Screen'),AT(72,164,256,120),USE(?HideFieldsGroup),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Mobile Number'),AT(76,177),USE(def:Show_Mobile_Number),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             CHECK('Hide Electronic QA'),AT(236,177),USE(def:QAPreliminary),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use Preliminary QA Check'),TIP('Use Preliminary QA Check'),VALUE('YES','NO')
                             CHECK('Loan / Exchange Details'),AT(76,188),USE(def:Show_Loan_Exchange_Details),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             CHECK('Job Skill Level'),AT(236,188),USE(tmp:HideSkillLevel),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Job Skill Level'),TIP('Job Skill Level'),VALUE('1','0')
                             CHECK('Physical Damage'),AT(76,201),USE(def:Hide_Physical_Damage),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             CHECK('Network'),AT(236,201),USE(tmp:HideNetwork),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Network'),TIP('Network'),VALUE('1','0')
                             CHECK('Insurance'),AT(76,212),USE(def:Hide_Insurance),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             CHECK('Hub Repair'),AT(236,212),USE(tmp:HideHubRepair),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Hub Repair'),TIP('Hub Repair'),VALUE('1','0')
                             CHECK('Authority Number'),AT(76,225),USE(def:Hide_Authority_Number),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             CHECK('Incoming Courier'),AT(236,225),USE(def:HideInCourier),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Hide Incoming Courier'),TIP('Hide Incoming Courier'),VALUE('YES','NO')
                             CHECK('Colour'),AT(76,236),USE(def:HideColour),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             CHECK('Intermittant Fault'),AT(236,236),USE(def:HideIntFault),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Hide Intermittant Fault'),TIP('Hide Intermittant Fault'),VALUE('YES','NO')
                             CHECK('Internal Location'),AT(76,249),USE(def:HideLocation),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Hide Internal Location'),TIP('Hide Internal Location'),VALUE('1','0')
                             OPTION,AT(76,260,176,12),USE(tmp:DisableLocation),DISABLE
                               RADIO('Hide Location'),AT(84,260),USE(?Option3:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                               RADIO('Disable Location'),AT(160,260),USE(?tmp:DisableLocation:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                             END
                             CHECK('Hide Internal Location (Rapid Eng Only)'),AT(76,268),USE(tmp:HideEngLocation),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Hide Internal Location (Rapid Engineer Screen Only)'),TIP('Hide Internal Location (Rapid Engineer Screen Only)'),VALUE('1','0')
                           END
                           GROUP('Online Help'),AT(360,228,252,32),USE(?Group6),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Online Help URL'),AT(364,244),USE(?tmp:HelpURL:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             TEXT,AT(444,244,164,10),USE(tmp:HelpURL),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Help URL'),TIP('Help URL'),SINGLE
                           END
                           OPTION('Job Progress Address Display Type'),AT(72,286,256,24),USE(def:Browse_Option),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Which Address To Show On Main Browse')
                             RADIO('Invoice'),AT(84,296),USE(?DEF:Browse_Option:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('INV')
                             RADIO('Collection'),AT(173,296),USE(?DEF:Browse_Option:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('COL')
                             RADIO('Delivery'),AT(271,296),USE(?DEF:Browse_Option:Radio3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('DEL')
                           END
                           GROUP('ServiceBase Online'),AT(360,266,252,32),USE(?Group9),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('SB Online URL'),AT(364,279),USE(?tmp:SBOnlineURL:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             TEXT,AT(444,279,164,10),USE(tmp:SBOnlineURL),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('SB Online URL'),TIP('SB Online URL'),SINGLE
                           END
                           PROMPT('IMEI Validation URL'),AT(84,324),USE(?tmp:IMEIValidationURL:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(164,324,124,10),USE(tmp:IMEIValidationURL),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('IMEI Validation URL'),TIP('IMEI Validation URL'),SINGLE
                           CHECK(' Use Bastion Warranty Checking'),AT(148,84),USE(tmp:UseBastionWarrantyChecking),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Show Repair Type Costs'),AT(444,88),USE(def:ShowRepTypeCosts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Show Repair Type Costs'),TIP('Show Repair Type Costs'),VALUE('1','0')
                           CHECK('Force Skill Levels For Engineers'),AT(444,100),USE(de2:UserSkillLevel),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Allocate Skill Level To Users'),TIP('Allocate Skill Level To Users'),VALUE('1','0')
                           CHECK('Use Accredited Users'),AT(148,100),USE(UseAccredUsers),VALUE('Y','N')
                           CHECK('Password Required To Allocate Engineer'),AT(444,112),USE(de2:AllocateEngPassword),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Password Required To Allocate Engineer'),TIP('Password Required To Allocate Engineer'),VALUE('1','0')
                           CHECK('Rename Colour Field'),AT(444,124),USE(tmp:RenameColour),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Rename Colour'),TIP('Rename Colour'),VALUE('1','0')
                           PROMPT('Colour Field Name'),AT(360,136),USE(?tmp:ColourFieldName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(444,136,124,10),USE(tmp:ColourFieldName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Colour Field Name'),TIP('Colour Field Name'),CAP
                           CHECK('Rename Authority Number'),AT(444,148,148,10),USE(tmp:RenameAuthorityNo),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           ENTRY(@s20),AT(444,160,124,10),USE(tmp:AuthorityNoName),COLOR(COLOR:White)
                           PROMPT('Authority No Name'),AT(360,160),USE(?AuthNoNamePrompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Autowrite Head Acc No to Sub Acc No'),AT(444,176),USE(tmp:AutowriteAccount),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Autowrite Head Acc No to Sub Acc No'),TIP('Autowrite Head Acc No to Sub Acc No'),VALUE('1','0')
                           PROMPT('Default Sub Account'),AT(360,188),USE(?tmp:DefaultSubAccount:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(444,188,124,10),USE(tmp:DefaultSubAccount),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Default Sub Account'),TIP('Default Sub Account'),UPR
                           BUTTON,AT(564,188),USE(?LookupSubAccount),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           CHECK('Courier Cost Alternate Use'),AT(444,200),USE(tmp:RenameCourierCost),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Courier Cost Alternate Use'),TIP('Courier Cost Alternate Use'),VALUE('1','0')
                           PROMPT('Courier Cost Name'),AT(360,212),USE(?tmp:CourierCostName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(444,212,124,10),USE(tmp:CourierCostName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Courier Cost Name'),TIP('Courier Cost Name'),CAP
                         END
                         TAB('Closing Defaults'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP('Closing Defaults'),AT(216,82,252,108),USE(?DespatchDefaults),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Validate Accessories At Despatch'),AT(292,90),USE(def:Force_Accessory_Check),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             CHECK('Validate Accessories At Completion'),AT(292,104),USE(tmp:AccCheckAtComp),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Force Accessory Check At Completion'),TIP('Force Accessory Check At Completion'),VALUE('1','0')
                             CHECK('Validate I.M.E.I. Number At Closing'),AT(292,114),USE(def:ValidateESN),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Validate ESN Before Rapid Job Update'),TIP('Validate ESN Before Rapid Job Update'),VALUE('YES','NO')
                             CHECK('Validate I.M.E.I. Number At Despatch'),AT(292,128),USE(def:ValidateDesp),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Validate ESN At Despatch'),TIP('Validate ESN At Despatch'),VALUE('YES','NO')
                             CHECK('Remove From Workshop On Despatch'),AT(292,138),USE(def:RemoveWorkshopDespatch),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Remove Location On'),TIP('Remove Location On'),VALUE('1','0')
                             CHECK('Group Virtual Despatches'),AT(292,152),USE(tmp:GroupDespatch),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Group Virtual Despatches'),TIP('Group Virtual Despatches'),VALUE('1','0')
                             CHECK('Do Not Despatch If Loan Allocated'),AT(292,162),USE(tmp:DoNotDespatchLoan),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Do Not Despatch If Loan Attached'),TIP('Do Not Despatch If Loan Attached'),VALUE('1','0')
                             CHECK('Bypass Loan Authorisation Table'),AT(292,176),USE(tmp:DoNotUseLoanAuthTable),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           END
                           GROUP('Vodacom Portal Jobs'),AT(214,202,254,30),USE(?Group10),BOXED,TRN,FONT(,,080FFFFH,,CHARSET:ANSI)
                           END
                           PROMPT('Cancel Vodacom Portal jobs after'),AT(234,214),USE(?Prompt60),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n-14),AT(368,214,34,10),USE(VPortalDays),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('Days'),AT(406,214),USE(?Prompt61),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                         END
                         TAB('Import / Export / Report Paths'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Use Postcode Program'),AT(292,84),USE(def:Use_Postcode),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('DLL Version'),AT(408,84),USE(def:PostcodeDll),HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Use DLL Version Of Postcode'),TIP('Use DLL Version Of Postcode'),VALUE('YES','NO')
                           PROMPT('Postcode Folder'),AT(184,96),USE(?DEF:Postcode_Path:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(292,96,124,10),USE(def:Postcode_Path),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(420,94),USE(?Lookup_Postcode_Path),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Default Export Folder'),AT(184,118),USE(?DEF:ExportPath:Prompt),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(292,118,124,10),USE(def:ExportPath),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Default Export Path'),TIP('Default Path for all export files'),UPR
                           BUTTON,AT(420,116),USE(?LookupExportPath),SKIP,TRN,FLAT,FONT('Tahoma',8,,,CHARSET:ANSI),ICON('lookupp.jpg')
                           PROMPT('Email Estimate Alert Folder'),AT(184,142),USE(?EstimateFolder:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(292,142,124,10),USE(tmp:EstimateFolder),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Email Estimate Alert Folder'),TIP('Email Estimate Alert Folder'),UPR
                           PROMPT('Waybill Email File Folder'),AT(184,168),USE(?tmp:WaybillEmailFileFolder:Prompt),TRN,FONT(,,,FONT:bold)
                           ENTRY(@s255),AT(292,168,124,10),USE(tmp:WaybillEmailFileFolder),COLOR(COLOR:White),MSG('Waybill Email File Folder'),TIP('Waybill Email File Folder'),UPR
                           PROMPT('Exchange Rate Import File'),AT(184,194),USE(?locExchangeRateImportPath:Prompt),FONT(,,,FONT:bold)
                           ENTRY(@s255),AT(292,194,124,10),USE(locExchangeRateImportPath),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('Stock Import Failure Folder'),AT(184,218),USE(?locStockImportFailureFolder:Prompt)
                           ENTRY(@s255),AT(292,218,124,10),USE(locStockImportFailureFolder),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('Exchange IMEI CSV'),AT(184,239),USE(?Prompt64)
                           ENTRY(@s255),AT(292,241,124,10),USE(locExchangeImportSaveFolder),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(420,239),USE(?LookupFile:17),FLAT,ICON('lookupp.jpg')
                           PROMPT('Import File'),AT(184,248),USE(?Prompt65)
                           PROMPT('Defaults for the API system'),AT(184,286),USE(?Prompt59)
                           LINE,AT(100,274,455,0),USE(?Line1),COLOR(COLOR:Black)
                           BUTTON,AT(420,308),USE(?LookupFile:14),FLAT,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(292,314,124,10),USE(ApiDirectory),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('API Input Directory'),AT(184,314),USE(?Prompt57),TRN
                           CHECK('Archive API files'),AT(292,330),USE(ApiArchive),VALUE('Y','N')
                           PROMPT('API Archive Directory'),AT(184,346),USE(?Prompt58)
                           ENTRY(@s255),AT(292,346,124,10),USE(ApiArchiveDir),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(420,340),USE(?LookupFile:15),FLAT,ICON('lookupp.jpg')
                           CHECK('Allow API '),AT(292,298),USE(ApiActive),VALUE('Y','N')
                           BUTTON,AT(420,213),USE(?LookupFile:13),TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(420,190),USE(?LookupFile:8),FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(420,164),USE(?LookupFile:7),TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(420,138),USE(?LookupFile:4),TRN,FLAT,ICON('lookupp.jpg')
                         END
                         TAB('Bouncer Defaults'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP('Bouncers'),AT(212,82,256,96),USE(?BouncersGroup),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Allow Bouncers'),AT(216,90,72,12),USE(def:Allow_Bouncer),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                             CHECK('Do Not Allow "Live" Bouncers'),AT(324,90),USE(tmp:StopLiveBouncers),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Do Not Allow "Live" Bouncers'),TIP('Stop booking of a job <13,10>when the bouncer <13,10>has not been completed.'),VALUE('1','0')
                             PROMPT('Bouncer Period'),AT(216,106),USE(?DEF:BouncerTime:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n3),AT(288,103,64,10),USE(def:BouncerTime),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Bouncer Time'),TIP('Period, in days, for a job to be a bouncer.'),UPR
                             PROMPT('(Days)'),AT(356,103),USE(?Days),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             OPTION('Bouncer Taken From'),AT(216,119,248,20),USE(tmp:BouncerType),BOXED,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               RADIO('Booking Date'),AT(220,127),USE(?tmp:BouncerType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                               RADIO('Completed Date'),AT(292,127),USE(?tmp:BouncerType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                               RADIO('Despatched Date'),AT(380,127),USE(?tmp:BouncerType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             END
                             CHECK('Exclude Char Jobs From Bouncer Table'),AT(288,149),USE(tmp:IgnoreChargeableBouncers),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Char Job'),TIP('Ignore Char Job'),VALUE('1','0')
                             CHECK('Exclude Warr Jobs From Bouncer Table'),AT(288,162),USE(tmp:IgnoreWarrantyJobs),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Char Job'),TIP('Ignore Char Job'),VALUE('1','0')
                           END
                         END
                         TAB('Line 500 / Oracle / MQ / EVO Defaults'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(292,124),USE(?LookupFile),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Line 500 CSV Import Confirmation Folder'),AT(68,192,88,18),USE(?locLine500CSVImportFolder:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s255),AT(164,192,124,10),USE(locLine500CSVImportFolder),FONT(,,,FONT:bold),COLOR(COLOR:White),MSG('Line 500 CSV Import Confirmation Folder'),TIP('Line 500 CSV Import Confirmation Folder'),UPR
                           BUTTON,AT(292,188),USE(?LookupFile:9),FLAT,ICON('lookupp.jpg')
                           PROMPT('PDF Batch Export Folder'),AT(68,128,76,18),USE(?tmp:PDFBatchExportPath:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(164,128,124,10),USE(tmp:PDFBatchExportPath),LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('PDF Batch Export Path'),TIP('PDF Batch Export Path'),UPR,SINGLE
                           GROUP,AT(384,110,236,126),USE(?EVO_SubGroup),TRN,HIDE
                             PROMPT('Company code:'),AT(396,123),USE(?EVO_Company_code:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s4),AT(484,123,124,10),USE(EVO_Company_code),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             PROMPT('XREF_1 HD:'),AT(396,141),USE(?EVO_XREF1_HD:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s240),AT(484,141,124,10),USE(EVO_XREF1_HD),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             PROMPT('Tax Code:'),AT(396,158),USE(?EVO_Tax_Code:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s2),AT(484,158,124,10),USE(EVO_Tax_Code),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             PROMPT('Tax Code Exempt:'),AT(396,176),USE(?EVO_Tax_Code_Exempt:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s2),AT(484,176,124,10),USE(EVO_Tax_Code_Exempt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             PROMPT('Vendor Line No:'),AT(396,194),USE(?EVO_Vendor_Line_No:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@n10),AT(484,194,124,10),USE(EVO_Vendor_Line_No),LEFT(1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             PROMPT('Tax Line No:'),AT(396,212),USE(?EVO_Tax_Line_No:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@n10),AT(484,212,124,10),USE(EVO_Tax_Line_No),LEFT(1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           END
                           ENTRY(@s255),AT(164,148,124,10),USE(tmp:PDFURL),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('(e.g. http://pdffolder/)'),AT(292,148),USE(?Prompt19),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('URL To PDF Export Folder'),AT(68,148,80,16),USE(?tmp:PDFBatchExportPath:Prompt:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(292,164),USE(?LookupFile:2),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('XML Repository Folder'),AT(68,170,72,16),USE(?tmp:XMLRepositoryFolder:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(164,170,124,10),USE(tmp:XMLRepositoryFolder),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('XML Repository Folder'),TIP('XML Repository Folder'),UPR
                           GROUP('XML Service Code Description'),AT(68,222,264,66),USE(?Group7),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('STF Sales To Franchise'),AT(72,232),USE(?tmp:STFMessage:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(164,232,64,10),USE(tmp:STFMessage),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('STF Sales To Franchise'),TIP('STF Sales To Franchise'),UPR
                             PROMPT('RIV Retailer Invoice'),AT(72,251),USE(?tmp:RIVMessage:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(164,251,64,10),USE(tmp:RIVMessage),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('RIV Retailer Invoice'),TIP('RIV Retailer Invoice'),UPR
                             PROMPT('AOW Out Of Warranty'),AT(72,272),USE(?tmp:AOWMessage:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(164,272,64,10),USE(tmp:AOWMessage),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('AOW Out Of Warranty Message'),TIP('AOW Out Of Warranty Message'),UPR
                           END
                           CHECK('IMEI Validation At Booking'),AT(164,302),USE(tmp:IMEIValidation),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('IMEI Validation At Booking'),TIP('IMEI Validation At Booking'),VALUE('1','0')
                           BUTTON,AT(292,310),USE(?LookupFile:3),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Path To SBMQI.EXE Folder'),AT(68,314,84,16),USE(?tmp:SBMQIPath:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(164,314,124,10),USE(tmp:SBMQIPath),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Path To SBMQI.EXE Folder'),TIP('Path To SBMQI.EXE Folder'),UPR
                           PROMPT('Path To MQREQUEST.EXE Folder'),AT(68,336,92,18),USE(?MQRequestFolder:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(164,336,124,10),USE(tmp:MQRequestFolder),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Path To MQREQUEST.EXE Folder'),TIP('Path To MQREQUEST.EXE Folder'),UPR
                           PROMPT('Time Out (secs)'),AT(352,336,64,12),USE(?locMQRequestTimeOut:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(420,336,64,10),USE(locMQRequestTimeOut),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),UPR,STEP(1)
                           CHECK('Keep Processed Files (for debugging)'),AT(164,350),USE(tmp:KeepProcessedFiles),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Keep Processed Files'),TIP('Keep Processed Files'),VALUE('1','0')
                           PROMPT('0 = Default Timeout (20s)'),AT(396,350),USE(?Prompt50),FONT(,7,,)
                           BUTTON,AT(292,332),USE(?LookupFile:5),TRN,FLAT,ICON('lookupp.jpg')
                           CHECK('Create Line 500 Messages'),AT(164,84),USE(tmp:CreateLine500Messages),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Create Line 500 Messages'),TIP('Create Line 500 Messages'),VALUE('1','0')
                           CHECK('Create EVO Messages'),AT(396,90),USE(EVO_CreateFiles),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                           CHECK('Create Oracle Messages'),AT(164,98),USE(tmp:CreateOracleMessages),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Create Oracle Messages'),TIP('Create Oracle Messages'),VALUE('1','0')
                           CHECK('Create CID Messages'),AT(164,112),USE(tmp:CreateCIDMessages),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Create CID Messages'),TIP('Create CID Messages'),VALUE('1','0')
                           GROUP('EVO Defaults'),AT(384,104,228,130),USE(?EVO_Group),BOXED,TRN,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           END
                         END
                         TAB('FTP Details'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP('Swap IMEI Automation Defaults'),AT(200,94,340,116),USE(?Group8),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING('Used for both the Swap IMIE report and SND Exchange IMEIDB Report'),AT(208,108),USE(?String7)
                             PROMPT('FTP Address'),AT(241,124),USE(?tmp:IMEIFTPAddress:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(352,124,124,10),USE(tmp:IMEIFTPAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('FTP Address'),TIP('FTP Address')
                             PROMPT('Username'),AT(241,140),USE(?tmp:IMEIFTPUserName:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s60),AT(352,140,124,10),USE(tmp:IMEIFTPUserName),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Username'),TIP('Username')
                             PROMPT('Password'),AT(241,156),USE(?tmp:IMEIFTPPassword:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s60),AT(352,156,124,10),USE(tmp:IMEIFTPPassword),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Password'),TIP('Password')
                             PROMPT('Location Path'),AT(242,172),USE(?tmp:IMEIFTPLocation:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s255),AT(352,172,124,10),USE(tmp:IMEIFTPLocation),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Location Path'),TIP('Location Path')
                           END
                           PROMPT('Report Exception Path'),AT(239,188),USE(?tmp:IMEIExceptionPath:Prompt)
                           ENTRY(@s255),AT(352,188,124,10),USE(tmp:IMEIExceptionPath),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           STRING('TB13267 everything in Touchpoint frame is individually hidden. Unhide when reins' &|
   'tated'),AT(83,218),USE(?String8),HIDE,FONT(,,COLOR:Lime,FONT:bold,CHARSET:ANSI)
                           GROUP('Touchpoint NPS'),AT(200,230,344,110),USE(?Group12),BOXED,TRN,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                             STRING('UNC Path'),AT(487,292),USE(?String5)
                             STRING('UNC Path'),AT(487,324),USE(?String6)
                           END
                           PROMPT('Integrate Touchpoint NPS'),AT(242,246),USE(?Prompt78),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           CHECK,AT(352,246),USE(TP_Use),HIDE,VALUE('Y','N')
                           PROMPT('Webservice Address'),AT(242,260),USE(?TP_Address:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s100),AT(352,260,124,10),USE(TP_Address),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Posting address'),TIP('Posting address')
                           PROMPT('Syngro Key'),AT(242,276),USE(?TP_Key:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s100),AT(352,276,124,10),USE(TP_Key),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Syngro Key'),TIP('Syngro Key')
                           PROMPT('Path To Exe'),AT(242,292),USE(?TP_PathToExe:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s255),AT(352,292,124,10),USE(TP_PathToExe),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Path to our exe '),TIP('Path to our exe ')
                           PROMPT('Enable Logging'),AT(242,308),USE(?Prompt79),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           CHECK,AT(352,308),USE(TP_Logging),HIDE,VALUE('Y','N')
                           PROMPT('Log Files Directory'),AT(242,324),USE(?TP_LogFolder:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s255),AT(352,324,124,10),USE(TP_LogFolder),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Path to logging folder'),TIP('Path to logging folder')
                           STRING('No Trailing "\" or "/" on any paths'),AT(308,348),USE(?String2:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                         END
                         TAB('Nokia NMS'),USE(?Tab7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,86,124,10),USE(tmp:NMSFTPSite),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),SINGLE
                           PROMPT('NMS FTP Site'),AT(100,86),USE(?Prompt49),TRN,FONT(,,COLOR:White,FONT:bold)
                           PROMPT('Username'),AT(100,102),USE(?tmp:NMSUsername:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,102,124,10),USE(tmp:NMSUsername),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Username'),TIP('Username'),SINGLE
                           PROMPT('Password'),AT(100,118),USE(?tmp:NMSPassword:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,118,124,10),USE(tmp:NMSPassword),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Password'),TIP('Password'),SINGLE
                           PROMPT('Upload "Booking Request" Folder'),AT(100,134),USE(?tmp:NMSBuildFolder:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,134,124,10),USE(tmp:NMSUploadFolder),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('"Build" Folder'),TIP('"Build" Folder'),SINGLE
                           PROMPT('e.g. "live/ToNMS/shipmentorder"'),AT(380,136),USE(?Prompt35),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('e.g. "live/FromNMS/shipmentorder"'),AT(380,152),USE(?Prompt35:6),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Upload "Reponse Receipt" Folder'),AT(100,166),USE(?tmp:NMSUploadResponseFolder:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,166,124,10),USE(tmp:NMSUploadResponseFolder),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Upload "Reponse Receipt" Folder'),TIP('Upload "Reponse Receipt" Folder'),SINGLE
                           PROMPT('Download "Pre Alert" Folder'),AT(99,182),USE(?tmp:NMSDownloadPreAlertFolder:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,182,124,10),USE(tmp:NMSDownloadPreAlertFolder),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('"Pre Alert" Folder'),TIP('"Pre Alert" Folder'),SINGLE
                           PROMPT('Upload "Pre Alert Response" Folder'),AT(100,198),USE(?tmp:NMSDownloadPreAlertFolder:Prompt:2),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,198,124,10),USE(tmp:NMSUploadPrealertFolder),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('"Pre Alert" Folder'),TIP('"Pre Alert" Folder'),SINGLE
                           PROMPT('Download "Booking Response" Folder'),AT(100,150),USE(?tmp:NMSDownloadShipmentOrderFolder:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,150,124,10),USE(tmp:NMSDownloadFolder),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Download "Shipmentorder" Folder'),TIP('Download "Shipmentorder" Folder'),SINGLE
                           PROMPT('Default Site ID'),AT(100,214),USE(?SiteID:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,214,124,10),USE(tmp:NMSSiteID),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Default Site ID'),TIP('Default Site ID'),SINGLE
                           PROMPT('Default Values in the XML Files'),AT(380,216),USE(?Prompt35:2),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Default Values in the XML Files'),AT(380,232),USE(?Prompt35:3),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Default User ID'),AT(100,230),USE(?tmp:UserID:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,230,124,10),USE(tmp:NMSUserID),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Default UserID'),TIP('Default UserID'),SINGLE
                           PROMPT('Default Access Key'),AT(100,246),USE(?tmp:NMSAccessKey:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,246,124,10),USE(tmp:NMSAccessKey),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Default Access Key'),TIP('Default Access Key'),SINGLE
                           PROMPT('Default Values in the XML Files'),AT(380,248),USE(?Prompt35:4),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('XML Folder'),AT(100,264),USE(?tmp:NMSXMLFolder:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,264,124,10),USE(tmp:NMSXMLFolder),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('XML Folder'),TIP('XML Folder'),UPR,SINGLE
                           BUTTON,AT(376,260),USE(?LookupFile:6),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('XML Folder On Network'),AT(408,264),USE(?Prompt37),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('No Trailing "\" or "/" on any paths'),AT(275,296),USE(?Prompt48),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Exception Files'),USE(?Tab8),FONT(,,COLOR:White,FONT:bold)
                           PROMPT('Exchange Selling Price Exception Folder'),AT(148,90),USE(?locExceptionExchangeRate:Prompt),TRN,FONT(,,COLOR:White,FONT:bold)
                           ENTRY(@s30),AT(304,90,124,10),USE(locExceptionExchangeRate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Loan Selling Price Exception Folder'),AT(148,110),USE(?locLoanSellingPriceExceptionFolder:Prompt)
                           ENTRY(@s255),AT(304,110,124,10),USE(locLoanSellingPriceExceptionFolder),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('SMS Exception Folder'),AT(148,130),USE(?locSMSExceptionFolder:Prompt),TRN
                           ENTRY(@s255),AT(304,130,124,10),USE(locSMSExceptionFolder),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(432,126),USE(?LookupFile:12),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Import Received Exchange Orders'),AT(148,150),USE(?Prompt62)
                           ENTRY(@s255),AT(304,153,124,10),USE(locExchangeExceptionFolder),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(432,148),USE(?LookupFile:16),FLAT,ICON('lookupp.jpg')
                           PROMPT('IMEI Exception Folder'),AT(148,159),USE(?Prompt63)
                           BUTTON,AT(432,170),USE(?LookupFile:18),FLAT,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(304,176,124,10),USE(locAuditImportExceptionFolder),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('Customer 3D Exception Folder'),AT(148,200),USE(?CustomerExceptionFolder:Prompt),TRN
                           ENTRY(@s255),AT(304,200,124,10),USE(CustomerExceptionFolder),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(432,196),USE(?LookupFile:19),FLAT,ICON('lookupp.jpg')
                           PROMPT('Audit Import Exception Folder'),AT(148,176),USE(?Prompt66)
                           BUTTON,AT(432,106),USE(?LookupFile:11),TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(432,86),USE(?LookupFile:10),TRN,FLAT,ICON('lookupp.jpg')
                         END
                         TAB('Customer 3D Integration'),USE(?Tab9),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('FMW User Name:'),AT(164,98),USE(?FMWUserName:Prompt)
                           ENTRY(@s100),AT(292,98,124,10),USE(FMWUserName),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('FMW Password:'),AT(164,112),USE(?FMWPassword:Prompt)
                           ENTRY(@s100),AT(292,112,124,10),USE(FMWPassword),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           CHECK('Enable Customer Lookup'),AT(164,136),USE(CustomerLookup),VALUE('Y','N')
                           CHECK('Identify Stack for Customer Info.'),AT(164,152),USE(IdentifyStack),VALUE('Y','N')
                           OPTION('Make Customer Detail Call Using'),AT(164,174,228,28),USE(CallUsing),BOXED,TRN
                             RADIO(' MSISDN'),AT(188,185),USE(?CallUsing:Radio1),VALUE('MSISDN')
                             RADIO('Customer ID'),AT(294,185),USE(?CallUsing:Radio2),VALUE('CUSTOMERID')
                           END
                           PROMPT('HighSpeed Profile Lookup URL:'),AT(164,224),USE(?PathToSiebel:Prompt)
                           ENTRY(@s100),AT(292,224,124,10),USE(PathToSiebel),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('Customer Details URL:'),AT(164,240),USE(?PathToSiebel2:Prompt)
                           ENTRY(@s100),AT(292,240,124,10),USE(PathToSiebel2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('Path To Exe:'),AT(164,278),USE(?PathToExe:Prompt)
                           ENTRY(@s255),AT(292,278,124,10),USE(PathToExe),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           STRING('In UNC format \\Computer\Folder'),AT(420,278),USE(?String1)
                           STRING('Must be the same for Data, Cwicweb and SBOnline computers'),AT(292,294),USE(?String3)
                           STRING('No Trailing "\" or "/" on any paths'),AT(268,318),USE(?String2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                         END
                         TAB('Courier Confirmation'),USE(?Tab10),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           CHECK('Use Automated Courier Confirmation'),AT(180,110),USE(CC_UseSystem),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                           STRING('Unticking this will stop the system which will need to be restarted'),AT(180,130),USE(?String9),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           PROMPT('Timer:'),AT(76,164),USE(?CC_Timer:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n-14),AT(180,164,124,10),USE(CC_Timer),RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           STRING('100ths of a second. Should be between 100 and 6000 (1 second to 1 minute)'),AT(312,164),USE(?String11),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           PROMPT('Path To CourierClient.exe'),AT(76,184),USE(?CC_PathToExe:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s255),AT(180,184,124,10),USE(CC_PathToExe),FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White)
                           STRING('No Trailing "\" or "/" on any paths'),AT(312,184),USE(?String2:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(312,200),USE(?ButtonCourierClear),FLAT,ICON('clearp.jpg')
                           STRING('More defaults need to be set on each courier using the system.'),AT(180,144),USE(?String12),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup10         SelectFileClass
FileLookup13         SelectFileClass
FileLookup8          SelectFileClass
FileLookup14         SelectFileClass
FileLookup15         SelectFileClass
FileLookup16         SelectFileClass
FileLookup9          SelectFileClass
FileLookup17         SelectFileClass
FileLookup18         SelectFileClass
FileLookup19         SelectFileClass
FileLookup20         SelectFileClass
FileLookup21         SelectFileClass
FileLookup22         SelectFileClass
FileLookup23         SelectFileClass
FileLookup24         SelectFileClass
FileLookup25         SelectFileClass
FileLookup26         SelectFileClass
FileLookup27         SelectFileClass
FileLookup28         SelectFileClass
FileLookup29         SelectFileClass
FileLookup30         SelectFileClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:DefaultSubAccount                Like(tmp:DefaultSubAccount)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020129'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('MainDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:SUBTRACC.Open
  Relate:WAYBCONF.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  
  Set(DEFAULT2)
  If Access:DEFAULT2.Next()
      If Access:DEFAULT2.PrimeRecord() = Level:Benign
          If Access:DEFAULT2.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:DEFAULT2.TryInsert() = Level:Benign
              !Insert Failed
          End !If Access:DEFAULT2.TryInsert() = Level:Benign
      End !If Access:DEFAULT2.PrimeRecord() = Level:Benign
  End !Access:DEFAULT2.Next()
  
  
  SB2KDEFINI = CLIP(PATH())&'\SB2KDEF.INI'
  
  tmp:RenameColour = Clip(GETINI('RENAME','RenameColour',,SB2KDEFINI))
  tmp:ColourFieldName = Clip(GETINI('RENAME','ColourName',,SB2KDEFINI))
  
  tmp:RenameAuthorityNo=Clip(GETINI('RENAME','RenameAutorityNo',,SB2KDEFINI))
  tmp:AuthorityNoName    =Clip(GETINI('RENAME','AutorityNoName',,SB2KDEFINI))
  
  tmp:AccCheckAtComp = Clip(GETINI('VALIDATE','ForceAccCheckComp',,SB2KDEFINI))
  tmp:StopLiveBouncers = Clip(GETINI('BOUNCER','StopLive',,SB2KDEFINI))
  tmp:BouncerType = Clip(GETINI('BOUNCER','BouncerType',,SB2KDEFINI))
  tmp:IgnoreChargeableBouncers = Clip(GETINI('BOUNCER','IgnoreChargeable',,SB2KDEFINI))
  tmp:IgnoreWarrantyJobs = Clip(GETINI('BOUNCER','IgnoreWarranty',,SB2KDEFINI))
  
  tmp:HideSkillLevel = Clip(GETINI('HIDE','SkillLevel',,SB2KDEFINI))
  tmp:HideNetwork = Clip(GETINI('HIDE','HideNetwork',,SB2KDEFINI))
  tmp:HideHubRepair = Clip(GETINI('HIDE','HubRepair',,SB2KDEFINI))
  tmp:HideEngLocation = Clip(GETINI('HIDE','InternalEngLocation',,SB2KDEFINI))
  
  tmp:AutowriteAccount = GETINI('TRADE','AutowriteAccount',,SB2KDEFINI)
  tmp:DefaultSubAccount = GETINI('TRADE','DefaultSubAccount',,SB2KDEFINI)
  
  tmp:GroupDespatch = GETINI('DESPATCH','GroupVirtualBatches',,SB2KDEFINI)
  
  tmp:RenameCourierCost = Clip(GETINI('RENAME','RenameCourierCost',,SB2KDEFINI))
  tmp:CourierCostName = Clip(GETINI('RENAME','CourierCostName',,SB2KDEFINI))
  
  tmp:DoNotDespatchLoan  = Clip(GETINI('DESPATCH','DoNotDespatchLoan',,SB2KDEFINI))
  tmp:DoNotUseLoanAuthTable  = Clip(GETINI('DESPATCH','DoNotUseLoanAuthTable',,SB2KDEFINI))
  
  tmp:DisableLocation = GETINI('DISABLE','InternalLocation',,SB2KDEFINI)
  
  tmp:UseBastionWarrantyChecking = GETINI('BASTION','EnableChecking',,SB2KDEFINI)
  
  If tmp:RenameColour and tmp:ColourFieldName <> ''
      ?def:HideColour{prop:Text} = Clip(tmp:ColourFieldName)
  End !tmp:RenameColour and tmp:ColourFieldName <> ''
  
  If Not Instring('VODAC',def:User_Name,1,1)
      ?tmp:UseBastionWarrantyChecking{prop:Hide} = True
  End
  
  tmp:HelpURL = GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')
  
  
  tmp:PDFBatchExportPath = GETINI('PDF','ExportPath',,SB2KDEFINI)
  tmp:PDFURL = GETINI('PDF','URL',,SB2KDEFINI)
  tmp:XMLRepositoryFolder = GETINI('XML','RepositoryFolder',,SB2KDEFINI)
  tmp:CreateLine500Messages = GETINI('XML','CreateLine500',,SB2KDEFINI)
  tmp:CreateOracleMessages = GETINI('XML','CreateOracle',,SB2KDEFINI)
  ! Create CID Messages - TrkBs: 6141 (DBH: 30-08-2005)
  tmp:CreateCIDMessages = GETINI('XML','CreateCID',,SB2KDEFINI)
  
  ! Set the Service Code Description - TrkBs: 6178 (DBH: 01-08-2005)
  tmp:STFMessage  = GETINI('XML','STFDescription',,SB2KDEFINI)
  If tmp:STFMessage = ''
      tmp:STFMessage = 'STF'
  End ! If tmp:STFMessage = ''
  tmp:RIVMessage  = GETINI('XML','RIVDescription',,SB2KDEFINI)
  If tmp:RIVMessage = ''
      tmp:RIVMessage = 'RIV'
  End ! If tmp:RIVMessage = 'RIV'
  tmp:AOWMessage  = GETINI('XML','AOWDescription',,SB2KDEFINI)
  If tmp:AOWMessage = ''
      tmp:AOWMessage = 'AOW'
  End ! If tmp:AOWMessage = ''
  
  tmp:IMEIValidation = GETINI('MQ','IMEIValidation',,SB2KDEFINI)
  ! Allow to put the SBMQI.EXE in a seperate folder - TrkBs: 6613 (DBH: 04-11-2005)
  tmp:SBMQIPath = GETINI('MQ','SBMQIFolder',,SB2KDEFINI)
  
  ! Inserting (DBH 17/05/2006) #7597 - Save the location of the estimate wmfs
  tmp:EstimateFolder = GETINI('ALERTS','EstimateFolder',,SB2KDEFINI)
  ! End (DBH 17/05/2006) #7597
  
  ! Inserting (DBH 23/06/2006) #7386 - Add new defaults for Swap IMEI Ftp defaults
  tmp:IMEIFTPAddress = GETINI('IMEIFTP','FTPAddress',,SB2KDEFINI)
  tmp:IMEIFTPUserName = GETINI('IMEIFTP','Username',,SB2KDEFINI)
  tmp:IMEIFTPPassword = GETINI('IMEIFTP','Password',,SB2KDEFINI)
  tmp:IMEIFTPLocation = GETINI('IMEIFTP','Location',,SB2KDEFINI)
  !TB13287 - J - 12/01/15 new field of execption path
  tmp:IMEIExceptionPath = GETINI('IMEIFTP','Exception',,SB2KDEFINI)
  !end TB13287
  ! End (DBH 23/06/2006) #7386
  
  ! Inserting (DBH 01/12/2006) # 8540 - Add a default to link to IMEI Validation site
  tmp:IMEIValidationURL = GETINI('URL','IMEIValidation',,SB2KDEFINI)
  ! End (DBH 01/12/2006) #8540
  ! Inserting (DBH 07/11/2007) # 9200 - Add default for mqrequest.exe folder
  tmp:MQRequestFolder = GETINI('MQ','MQRequestFolder',,SB2KDEFINI)
  tmp:KeepProcessedFiles = GETINI('MQ','MSISDNKeepFiles',,SB2KDEFINI)
  ! End (DBH 07/11/2007) #9200
  ! Inserting (DBH 20/02/2008) # 9770 - Save the SB Online URL
  tmp:SBOnlineURL = GETINI('SBONLINE','URL',,SB2KDEFINI)
  ! End (DBH 20/02/2008) #9770
  
  ! -----------------------------------------
  ! DBH 29/10/2008 #10394
  ! Inserting: New Defaults
  tmp:NMSFTPSite      = GETINI('NMSFTP','Address',,SB2KDEFINI)
  tmp:NMSUsername     = GETINI('NMSFTP','Username',,SB2KDEFINI)
  tmp:NMSPassword     = GETINI('NMSFTP','Password',,SB2KDEFINI)
  
  tmp:NMSXMLFolder    = GETINI('NMSFTP','XMLFolder',,SB2KDEFINI)
  tmp:NMSSiteID       = GETINI('NMSFTP','SiteID',,SB2KDEFINI)
  tmp:NMSUserID       = GETINI('NMSFTP','UserID',,SB2KDEFINI)
  tmp:NMSAccessKey    = GETINI('NMSFTP','AccessKey',,SB2KDEFINI)
  tmp:NMSSourceSiteID = GETINI('NMSFTP','SourceSiteID',,SB2KDEFINI)
  
  tmp:NMSUploadFolder = GETINI('NMSFTP','UploadFolder',,SB2KDEFINI)
  tmp:NMSDownloadFolder = GETINI('NMSFTP','DownloadFolder',,SB2KDEFINI)
  tmp:NMSUploadResponseFolder = GETINI('NMSFTP','UploadResponseFolder',,SB2KDEFINI)
  tmp:NMSDownloadPreAlertFolder = GETINI('NMSFTP','DownloadPreAlertFolder',,SB2KDEFINI)
  tmp:NMSUploadPrealertFolder = GETINI('NMSFTP','UploadPreAlertFolder',,SB2KDEFINI)
  ! End: DBH 29/10/2008 #10394
  ! -----------------------------------------
  
  tmp:WaybillEmailFileFolder = GETINI('EMAIL','WaybillEmailFileFolder',,SB2KDEFINI)
  ! DBH #11388 - Set MQRequest Time Out
  locMQRequestTimeOut = GETINI('MQ','TimeOut',,SB2KDEFINI)
  locExchangeRateImportPath = GETINI('EXCHANGERATE','ImportPath',,SB2KDEFINI) ! #11382 New Default For Import Path (DBH: 06/08/2010)
  locLine500CSVImportFolder = GETINI('MQ','CSVImportFolder',,SB2KDEFINI)  ! #11983 New default. (Bryan: 18/02/2011)
  locExceptionExchangeRate = GETINI('EXCEPTION','ExchangeRate',,SB2KDEFINI)  ! #12127 New default (Bryan: 06/07/2011)
  locLoanSellingPriceExceptionFolder = GETINI('EXCEPTION','LoanRate',,SB2KDEFINI)  ! #12347 New default (DBH: 19/01/2012)
  locSMSExceptionFolder = GETINI('EXCEPTION','SMS',,SB2KDEFINI)  ! #12477 New default (JAC: 21/02/2012)
  locStockImportFailureFolder = GETINI('STOCK','STOCKFAIL',,SB2KDEFINI)  ! #011096 New default (JAC: 23/03/2012)
  locExchangeExceptionFolder  = GETINI('EXCEPTION','EXCHANGE',,SB2KDEFINI)  ! #012423 New default (JAC: 29/08/2012)
  locExchangeImportSaveFolder = GETINI('EXCEPTION','EXCHANGESAVE',,SB2KDEFINI)  ! #012423 New default (JAC: 29/08/2012)
  locAuditImportExceptionFolder = GETINI('EXCEPTION','AUDITIMPORT',,SB2KDEFINI)  ! #012462 New default (JAC: 28/09/2012)
  
  !API directory TB12627 JC
  ApiDirectory  = getini('API','DIRECTORY','',clip(path())&'\SB_API.INI')
  ApiActive     = GetIni('API','ACTIVE','',clip(path())&'\SB_API.INI')
  ApiArchive    = GetIni('API','ARCHIVE','',clip(path())&'\SB_API.INI')
  ApiArchiveDir = GetIni('API','ARCHIVEDIR','',clip(path())&'\SB_API.INI')
  
  
  !vodacom portal days
  VPortalDays   = GetIni('VPORTAL','DAYS','',SB2KDEFINI)
  
  !EVO defaults: 12777 JAC: 09/10/12)
  EVO_CreateFiles             = Getini('EVO','CREATE'                 ,0  ,SB2KDEFINI)          !new defaults needed for EVO Project
  EVO_Company_code            = Getini('EVO','COMPANY_CODE'           ,'' ,SB2KDEFINI)
  EVO_XREF1_HD                = Getini('EVO','XREF_HD'                ,'' ,SB2KDEFINI)
  EVO_Tax_Code                = Getini('EVO','TAX_CODE'               ,'' ,SB2KDEFINI)
  EVO_Tax_Code_Exempt         = Getini('EVO','TAX_CODE_EXEMPT'        ,'' ,SB2KDEFINI)
  EVO_Vendor_Line_No          = Getini('EVO','VENDOR_LINE_NO'         ,0  ,SB2KDEFINI)
  EVO_Tax_Line_No             = Getini('EVO','TAX_LINE_NO'            ,0  ,SB2KDEFINI)
  
  !TB13224 - J - 20/10/14 - add defaults for the Customer 3D Integration
  FMWUserName             = GetIni('C3D','FMWUserName',    '',        SB2KDEFINI)
  FMWPassword             = GetIni('C3D','FMWPassword',    '',        SB2KDEFINI)
  CustomerLookup          = GetIni('C3D','CustomerLookup', 'N',       SB2KDEFINI)
  IdentifyStack           = GetIni('C3D','IdentifyStack',  'N',       SB2KDEFINI)
  CustomerExceptionFolder = GetIni('C3D','ExceptionFolder','',        SB2KDEFINI)
  CallUsing               = GetIni('C3D','CallUsing',      'MSISDN',  SB2KDEFINI)
  PathToSiebel            = GetIni('C3D','PathToSiebel',   '',        SB2KDEFINI)
  PathToSiebel2           = GetIni('C3D','PathToSiebel2',  '',        SB2KDEFINI)
  PathToExe               = GetIni('C3D','PathToExe',      '',    SB2KDEFINI)
  
  !TB13267 - J - 19/12/14 - add defaults for the Touchpoing integratioin
  TP_Use        = GetIni('Touchpoint','Use'       ,'N',    SB2KDEFINI)
  TP_Address    = GetIni('Touchpoint','Address'   ,'' ,    SB2KDEFINI)
  TP_Key        = GetIni('Touchpoint','Key'       ,'' ,    SB2KDEFINI)
  TP_PathToExe  = GetIni('Touchpoint','PathToExe' ,'' ,    SB2KDEFINI)
  TP_Logging    = GetIni('Touchpoint','Log'       ,'N',    SB2KDEFINI)
  TP_LogFolder  = GetIni('Touchpoint','LogFolder' ,'' ,    SB2KDEFINI)
  
  !TB13393 Apple accredited users
  UseAccredUsers= GetIni('Apple','UseAccredUsers' ,'N' ,    SB2KDEFINI)
  
  !TB13370 - CourierConfirmation
  CC_UseSystem     = GetIni('CourierConf','UseSystem'     ,'N', SB2KDEFINI)
  !CC_UserName      = GetIni('CourierConf','UserName'      ,'' , SB2KDEFINI)
  !CC_UserPassword  = GetIni('CourierConf','UserPassword'  ,'' , SB2KDEFINI)
  !CC_URL           = GetIni('CourierConf','URL'           ,'' , SB2KDEFINI)
  CC_PathToExe     = GetIni('CourierConf','Path'          ,'' , SB2KDEFINI)
  CC_Timer         = GetIni('CourierConf','Timer'         ,500 , SB2KDEFINI)
  ! Save Window Name
   AddToLog('Window','Open','MainDefaults')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:DefaultSubAccount{Prop:Tip} AND ~?LookupSubAccount{Prop:Tip}
     ?LookupSubAccount{Prop:Tip} = 'Select ' & ?tmp:DefaultSubAccount{Prop:Tip}
  END
  IF ?tmp:DefaultSubAccount{Prop:Msg} AND ~?LookupSubAccount{Prop:Msg}
     ?LookupSubAccount{Prop:Msg} = 'Select ' & ?tmp:DefaultSubAccount{Prop:Msg}
  END
  IF ?def:HideLocation{Prop:Checked} = True
    DISABLE(?tmp:HideEngLocation)
    ENABLE(?tmp:DisableLocation)
  END
  IF ?def:HideLocation{Prop:Checked} = False
    ENABLE(?tmp:HideEngLocation)
    DISABLE(?tmp:DisableLocation)
  END
  IF ?tmp:RenameColour{Prop:Checked} = True
    UNHIDE(?tmp:ColourFieldName:Prompt)
    UNHIDE(?tmp:ColourFieldName)
  END
  IF ?tmp:RenameColour{Prop:Checked} = False
    HIDE(?tmp:ColourFieldName:Prompt)
    HIDE(?tmp:ColourFieldName)
  END
  IF ?tmp:RenameAuthorityNo{Prop:Checked} = True
    UNHIDE(?AuthNoNamePrompt)
    UNHIDE(?tmp:AuthorityNoName)
  END
  IF ?tmp:RenameAuthorityNo{Prop:Checked} = False
    HIDE(?AuthNoNamePrompt)
    HIDE(?tmp:AuthorityNoName)
  END
  IF ?tmp:RenameCourierCost{Prop:Checked} = True
    UNHIDE(?tmp:CourierCostName:Prompt)
    UNHIDE(?tmp:CourierCostName)
  END
  IF ?tmp:RenameCourierCost{Prop:Checked} = False
    HIDE(?tmp:CourierCostName:Prompt)
    HIDE(?tmp:CourierCostName)
  END
  IF ?def:Use_Postcode{Prop:Checked} = True
    UNHIDE(?DEF:PostcodeDll)
  END
  IF ?def:Use_Postcode{Prop:Checked} = False
    HIDE(?DEF:PostcodeDll)
  END
  IF ?def:Allow_Bouncer{Prop:Checked} = True
    UNHIDE(?tmp:StopLiveBouncers)
  END
  IF ?def:Allow_Bouncer{Prop:Checked} = False
    HIDE(?tmp:StopLiveBouncers)
  END
  IF ?tmp:IMEIValidation{Prop:Checked} = True
    UNHIDE(?tmp:SBMQIPath:Prompt)
    UNHIDE(?tmp:SBMQIPath)
    UNHIDE(?LookupFile:3)
  END
  IF ?tmp:IMEIValidation{Prop:Checked} = False
    HIDE(?tmp:SBMQIPath:Prompt)
    HIDE(?tmp:SBMQIPath)
    HIDE(?LookupFile:3)
  END
  IF ?EVO_CreateFiles{Prop:Checked} = True
    UNHIDE(?EVO_Group)
    UNHIDE(?EVO_SubGroup)
  END
  IF ?EVO_CreateFiles{Prop:Checked} = False
    HIDE(?EVO_Group)
    HIDE(?EVO_SubGroup)
  END
  IF ?CC_UseSystem{Prop:Checked} = True
    UNHIDE(?String12)
  END
  IF ?CC_UseSystem{Prop:Checked} = False
    HIDE(?String12)
  END
  FileLookup10.Init
  FileLookup10.Flags=BOR(FileLookup10.Flags,FILE:LongName)
  FileLookup10.Flags=BOR(FileLookup10.Flags,FILE:Directory)
  FileLookup10.SetMask('All Files','*.*')
  FileLookup10.WindowTitle='Select Folder For Postcode Program'
  FileLookup13.Init
  FileLookup13.Flags=BOR(FileLookup13.Flags,FILE:LongName)
  FileLookup13.Flags=BOR(FileLookup13.Flags,FILE:Directory)
  FileLookup13.SetMask('All Files','*.*')
  FileLookup13.WindowTitle='Default Export Folder'
  FileLookup8.Init
  FileLookup8.Flags=BOR(FileLookup8.Flags,FILE:LongName)
  FileLookup8.Flags=BOR(FileLookup8.Flags,FILE:Directory)
  FileLookup8.SetMask('All Files','*.*')
  FileLookup8.WindowTitle='PDF Batch Export Folder'
  FileLookup14.Init
  FileLookup14.Flags=BOR(FileLookup14.Flags,FILE:Directory)
  FileLookup14.SetMask('All Files','*.*')
  FileLookup14.WindowTitle='XML Repository Folder'
  FileLookup15.Init
  FileLookup15.Flags=BOR(FileLookup15.Flags,FILE:LongName)
  FileLookup15.Flags=BOR(FileLookup15.Flags,FILE:Directory)
  FileLookup15.SetMask('All Files','*.*')
  FileLookup15.WindowTitle='Path To SBMQI.EXE Folder'
  FileLookup16.Init
  FileLookup16.Flags=BOR(FileLookup16.Flags,FILE:LongName)
  FileLookup16.Flags=BOR(FileLookup16.Flags,FILE:Directory)
  FileLookup16.SetMask('All Files','*.*')
  FileLookup9.Init
  FileLookup9.Flags=BOR(FileLookup9.Flags,FILE:LongName)
  FileLookup9.Flags=BOR(FileLookup9.Flags,FILE:Directory)
  FileLookup9.SetMask('All Files','*.*')
  FileLookup17.Init
  FileLookup17.Flags=BOR(FileLookup17.Flags,FILE:LongName)
  FileLookup17.Flags=BOR(FileLookup17.Flags,FILE:Directory)
  FileLookup17.SetMask('All Files','*.*')
  FileLookup18.Init
  FileLookup18.Flags=BOR(FileLookup18.Flags,FILE:LongName)
  FileLookup18.Flags=BOR(FileLookup18.Flags,FILE:Directory)
  FileLookup18.SetMask('lookupp.jpg','*.*')
  FileLookup19.Init
  FileLookup19.Flags=BOR(FileLookup19.Flags,FILE:LongName)
  FileLookup19.SetMask('All Files','*.*')
  FileLookup20.Init
  FileLookup20.Flags=BOR(FileLookup20.Flags,FILE:LongName)
  FileLookup20.Flags=BOR(FileLookup20.Flags,FILE:Directory)
  FileLookup20.SetMask('All Files','*.*')
  FileLookup21.Init
  FileLookup21.Flags=BOR(FileLookup21.Flags,FILE:LongName)
  FileLookup21.Flags=BOR(FileLookup21.Flags,FILE:Directory)
  FileLookup21.SetMask('All Files','*.*')
  FileLookup22.Init
  FileLookup22.Flags=BOR(FileLookup22.Flags,FILE:LongName)
  FileLookup22.Flags=BOR(FileLookup22.Flags,FILE:Directory)
  FileLookup22.SetMask('All Files','*.*')
  FileLookup23.Init
  FileLookup23.Flags=BOR(FileLookup23.Flags,FILE:LongName)
  FileLookup23.Flags=BOR(FileLookup23.Flags,FILE:Directory)
  FileLookup23.SetMask('All Files','*.*')
  FileLookup23.WindowTitle='SMS Exception Folder'
  FileLookup24.Init
  FileLookup24.Flags=BOR(FileLookup24.Flags,FILE:LongName)
  FileLookup24.Flags=BOR(FileLookup24.Flags,FILE:Directory)
  FileLookup24.SetMask('All Files','*.*')
  FileLookup24.WindowTitle='Folder for Stock Import Failure'
  FileLookup25.Init
  FileLookup25.Flags=BOR(FileLookup25.Flags,FILE:LongName)
  FileLookup25.Flags=BOR(FileLookup25.Flags,FILE:Directory)
  FileLookup25.SetMask('All Files','*.*')
  FileLookup25.WindowTitle='Select API Directory'
  FileLookup26.Init
  FileLookup26.Flags=BOR(FileLookup26.Flags,FILE:LongName)
  FileLookup26.Flags=BOR(FileLookup26.Flags,FILE:Directory)
  FileLookup26.SetMask('All Files','*.*')
  FileLookup26.WindowTitle='Select Archive Directory'
  FileLookup27.Init
  FileLookup27.Flags=BOR(FileLookup27.Flags,FILE:LongName)
  FileLookup27.Flags=BOR(FileLookup27.Flags,FILE:Directory)
  FileLookup27.SetMask('All Files','*.*')
  FileLookup28.Init
  FileLookup28.Flags=BOR(FileLookup28.Flags,FILE:LongName)
  FileLookup28.Flags=BOR(FileLookup28.Flags,FILE:Directory)
  FileLookup28.SetMask('All Files','*.*')
  FileLookup29.Init
  FileLookup29.Flags=BOR(FileLookup29.Flags,FILE:LongName)
  FileLookup29.Flags=BOR(FileLookup29.Flags,FILE:Directory)
  FileLookup29.SetMask('All Files','*.*')
  FileLookup29.WindowTitle='Audit Import Exception Folder'
  FileLookup30.Init
  FileLookup30.Flags=BOR(FileLookup30.Flags,FILE:LongName)
  FileLookup30.Flags=BOR(FileLookup30.Flags,FILE:Directory)
  FileLookup30.SetMask('All Files','*.*')
  FileLookup30.WindowTitle='Customer 3D Exception Folder'
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:SUBTRACC.Close
    Relate:WAYBCONF.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','MainDefaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickSubAccounts
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CC_Timer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CC_Timer, Accepted)
      if CC_Timer < 100 then
          CC_Timer = 100
          miss# = Missive('Timer must be set to a minimum of 100, equivalent to running every second','ServiceBase 3g','mSTOP.jpg','OK')
          display()
      END
      
      if CC_Timer > 6000 then
          CC_Timer = 6000
          miss# = missive('Timer must be set to a maximum of 6000, equivalent to running every minute','ServiceBase 3g','mSTOP.jpg','OK')
          display()
      END
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CC_Timer, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020129'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020129'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020129'&'0')
      ***
    OF ?OKButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OKButton, Accepted)
      access:defaults.update()
      access:default2.update()
      PUTINI('RENAME','RenameColour',tmp:RenameColour,SB2KDEFINI)
      PUTINI('RENAME','ColourName',tmp:ColourFieldName,SB2KDEFINI)
      PUTINI('RENAME','RenameAthorityNo',tmp:RenameAuthorityNo,SB2KDEFINI)
      PUTINI('RENAME','AthorityNoName',tmp:AuthorityNoName,SB2KDEFINI)
      PUTINI('RENAME','RenameCourierCost',tmp:RenameCourierCost,SB2KDEFINI)
      PUTINI('RENAME','CourierCostName',tmp:CourierCostName,SB2KDEFINI)
      PUTINI('VALIDATE','ForceAccCheckComp',tmp:AccCheckAtComp,SB2KDEFINI)
      PUTINI('BOUNCER','StopLive',tmp:StopLiveBouncers,SB2KDEFINI)
      PUTINI('BOUNCER','BouncerType',tmp:BouncerType,SB2KDEFINI)
      PUTINI('BOUNCER','IgnoreChargeable',tmp:IgnoreChargeableBouncers,SB2KDEFINI)
      PUTINI('BOUNCER','IgnoreWarranty',tmp:IgnoreWarrantyJobs,SB2KDEFINI)
      PUTINI('HIDE','SkillLevel',tmp:HideSkillLevel,SB2KDEFINI)
      PUTINI('HIDE','HideNetwork',tmp:HideNetwork,SB2KDEFINI)
      PUTINI('HIDE','HubRepair',tmp:HideHubRepair,SB2KDEFINI)
      PUTINI('HIDE','InternalEngLocation',tmp:HideEngLocation,SB2KDEFINI)
      PUTINI('TRADE','AutowriteAccount',tmp:AutowriteAccount,SB2KDEFINI)
      PUTINI('TRADE','DefaultSubAccount',tmp:DefaultSubAccount,SB2KDEFINI)
      PUTINI('DESPATCH','GroupVirtualBatches',tmp:GroupDespatch,SB2KDEFINI)
      PUTINI('DESPATCH','DoNotDespatchLoan',tmp:DoNotDespatchLoan,SB2KDEFINI)
      PUTINI('DESPATCH','DoNotUseLoanAuthTable',tmp:DoNotUseLoanAuthTable,SB2KDEFINI)
      PUTINI('DISABLE','InternalLocation',tmp:DisableLocation,SB2KDEFINI)
      PUTINI('BASTION','EnableChecking',tmp:UseBastionWarrantyChecking,SB2KDEFINI)
      PUTINI('ONLINEHELP','URL',tmp:HelpURL,CLIP(Path()) & '\EXTRADEFS.INI')
      
      
      PUTINI('PDF','ExportPath',Clip(tmp:PDFBatchExportPath),SB2KDEFINI)
      PUTINI('PDF','URL',tmp:PDFURL,SB2KDEFINI)
      PUTINI('XML','RepositoryFolder',Clip(tmp:XMLRepositoryFolder),SB2KDEFINI)
      ! Update the PDF path into the repository ini file - TrkBs: 6178 (DBH: 01-08-2005)
      PUTINI('PDF','ExportPath',Clip(tmp:PDFBatchExportPath),Clip(tmp:XMLRepositoryFolder) & '\REPOSITORY.INI')
      PUTINI('PDF','URL',Clip(tmp:PDFURL),Clip(tmp:XMLRepositoryFolder) & '\REPOSITORY.INI')
      
      PUTINI('XML','CreateLine500',tmp:CreateLine500Messages,SB2KDEFINI)
      PUTINI('XML','CreateOracle',tmp:CreateOracleMessages,SB2KDEFINI)
      ! Create CID Messages? - TrkBs: 6141 (DBH: 30-08-2005)
      PUTINI('XML','CreateCID',tmp:CreateCIDMessages,SB2KDEFINI)
      PUTINI('XML','STFDescription',tmp:STFMessage,SB2KDEFINI)
      PUTINI('XML','RIVDescription',tmp:RIVMessage,SB2KDEFINI)
      PUTINI('XML','AOWDescription',tmp:AOWMessage,SB2KDEFINI)
      PUTINI('MQ','IMEIValidation',tmp:IMEIValidation,SB2KDEFINI)
      PUTINI('MQ','SBMQIFolder',Clip(tmp:SBMQIPath),SB2KDEFINI)
      
      ! Inserting (DBH 17/05/2006) #7597 - Save the location of the estimate wmfs
      PUTINI('ALERTS','EstimateFolder',tmp:EstimateFolder,SB2KDEFINI)
      ! End (DBH 17/05/2006) #7597
      
      ! Inserting (DBH 23/06/2006) #7386 - Add new defaults for Swap IMEI Ftp defaults
      PUTINI('IMEIFTP','FTPAddress',tmp:IMEIFTPAddress,SB2KDEFINI)
      PUTINI('IMEIFTP','Username',tmp:IMEIFTPUsername,SB2KDEFINI)
      PUTINI('IMEIFTP','Password',tmp:IMEIFTPPassword,SB2KDEFINI)
      PUTINI('IMEIFTP','Location',tmp:IMEIFTPLocation,SB2KDEFINI)
      !TB13287 - J - 12/01/15 new field of execption path
      PUTINI('IMEIFTP','Exception',tmp:IMEIExceptionPath,SB2KDEFINI)
      !end TB13287
      ! End (DBH 23/06/2006) #7386
      
      ! Inserting (DBH 01/12/2006) # 8540 - Add a default to link to IMEI Validation site
      PUTINI('URL','IMEIValidation',tmp:IMEIValidationURL,SB2KDEFINI)
      ! End (DBH 01/12/2006) #8540
      ! Inserting (DBH 07/11/2007) # 9200 - Add default for mqrequest folder
      PUTINI('MQ','MQRequestFolder',tmp:MQRequestFolder,SB2KDEFINI)
      PUTINI('MQ','MSISDNKeepFiles',tmp:KeepProcessedFiles,SB2KDEFINI)
      ! End (DBH 07/11/2007) #9200
      PUTINI('SBONLINE','URL',tmp:SBOnlineURL,SB2KDEFINI)
      
      ! -----------------------------------------
      ! DBH 29/10/2008 #10394
      ! Inserting: New Defaults
      PUTINI('NMSFTP','Address',tmp:NMSFTPSite,SB2KDEFINI)
      PUTINI('NMSFTP','Username',tmp:NMSUsername,SB2KDEFINI)
      PUTINI('NMSFTP','Password',tmp:NMSPassword,SB2KDEFINI)
      PUTINI('NMSFTP','UploadFolder',tmp:NMSUploadFolder,SB2KDEFINI)
      PUTINI('NMSFTP','XMLFolder',tmp:NMSXMLFolder,SB2KDEFINI)
      PUTINI('NMSFTP','SiteID',tmp:NMSSiteID,SB2KDEFINI)
      PUTINI('NMSFTP','UserID',tmp:NMSUserID,SB2KDEFINI)
      PUTINI('NMSFTP','AccessKey',tmp:NMSAccessKey,SB2KDEFINI)
      PUTINI('NMSFTP','SourceSiteID',tmp:NMSSourceSiteID,SB2KDEFINI)
      PUTINI('NMSFTP','DownloadFolder',tmp:NMSDownloadFolder,SB2KDEFINI)
      PUTINI('NMSFTP','UploadResponseFolder',tmp:NMSUploadResponseFolder,SB2KDEFINI)
      PUTINI('NMSFTP','DownloadPreAlertFolder',tmp:NMSDownloadPreAlertFolder,SB2KDEFINI)
      PUTINI('NMSFTP','UploadPreAlertFolder',tmp:NMSUploadPrealertFolder,SB2KDEFINI)
      ! End: DBH 29/10/2008 #10394
      ! -----------------------------------------
      
      
      ! Insert --- Save email file folder (DBH: 06/05/2009) #10190
      PUTINI('EMAIL','WaybillEmailFileFolder',tmp:WaybillEmailFileFolder,SB2KDEFINI)
      ! end --- (DBH: 06/05/2009) #10190
      ! DBH #11388 - Set MQRequest Time Out
      PUTINI('MQ','TimeOut',locMQRequestTimeOut,SB2KDEFINI)
      PUTINI('EXCHANGERATE','ImportPath',locExchangeRateImportPath,SB2KDEFINI) ! #11382 New Default For Import Path (DBH: 06/08/2010)
      PUTINI('MQ','CSVImportFolder',locLine500CSVImportFolder,SB2KDEFINI)  ! #11983 New default. (Bryan: 18/02/2011)
      PUTINI('EXCEPTION','ExchangeRate',locExceptionExchangeRate,SB2KDEFINI)  ! #12127 New default (Bryan: 06/07/2011)
      PUTINI('EXCEPTION','LoanRate',locLoanSellingPriceExceptionFolder,SB2KDEFINI)  ! #12347 New default (DBH: 19/01/2012)
      PUTINI('EXCEPTION','SMS',locSMSExceptionFolder,SB2KDEFINI)  ! #12477 New default (JAC: 21/02/2012)
      PUTINI('STOCK','STOCKFAIL',locStockImportFailureFolder,SB2KDEFINI)  ! #011096 New default (JAC: 23/03/2012)
      PUTINI('EXCEPTION','EXCHANGE',locExchangeExceptionFolder,SB2KDEFINI)  ! #012423 New default (JAC: 29/08/2012)
      PUTINI('EXCEPTION','EXCHANGESAVE',locExchangeImportSaveFolder,SB2KDEFINI)  ! #012423 New default (JAC: 29/08/2012)
      puTINI('EXCEPTION','AUDITIMPORT',locAuditImportExceptionFolder,SB2KDEFINI)  ! #012462 New default (JAC: 28/09/2012)
      
      !API directory TB12627 JC
      putini('API','DIRECTORY',ApiDirectory,clip(path())&'\SB_API.INI')
      putIni('API','ACTIVE',ApiActive,clip(path())&'\SB_API.INI')
      putIni('API','ARCHIVE',ApiArchive,clip(path())&'\SB_API.INI')
      putIni('API','ARCHIVEDIR',ApiArchiveDir,clip(path())&'\SB_API.INI')
      
      putIni('VPORTAL','DAYS',VPortalDays,SB2KDEFINI)
      
      !EVO defaults: 12777 JAC: 09/10/12)
      Putini('EVO','CREATE'                 ,EVO_CreateFiles          ,SB2KDEFINI)          !new defaults needed for EVO Project
      Putini('EVO','COMPANY_CODE'           ,EVO_Company_code         ,SB2KDEFINI)
      Putini('EVO','XREF_HD'                ,EVO_XREF1_HD             ,SB2KDEFINI)
      Putini('EVO','TAX_CODE'               ,EVO_Tax_Code             ,SB2KDEFINI)
      Putini('EVO','TAX_CODE_EXEMPT'        ,EVO_Tax_Code_Exempt      ,SB2KDEFINI)
      Putini('EVO','VENDOR_LINE_NO'         ,EVO_Vendor_Line_No       ,SB2KDEFINI)
      Putini('EVO','TAX_LINE_NO'            ,EVO_Tax_Line_No          ,SB2KDEFINI)
      
      !TB13224 - J - 20/10/14 - add defaults for the Customer 3D Integration
      PutIni('C3D','FMWUserName',     FMWUserName,            SB2KDEFINI)
      PutIni('C3D','FMWPassword',     FMWPassword,            SB2KDEFINI)
      PutIni('C3D','CustomerLookup',  CustomerLookup,         SB2KDEFINI)
      PutIni('C3D','IdentifyStack' ,  IdentifyStack,          SB2KDEFINI)
      PutIni('C3D','ExceptionFolder', CustomerExceptionFolder,SB2KDEFINI)
      PutIni('C3D','CallUsing',       CallUsing,              SB2KDEFINI)
      PutIni('C3D','PathToSiebel',    PathToSiebel,           SB2KDEFINI)
      PutIni('C3D','PathToSiebel2',   PathToSiebel2,          SB2KDEFINI)
      PutIni('C3D','PathToExe',       PathToExe,              SB2KDEFINI)
      
      !TB13267 - J - 19/12/14 - add defaults for the Touchpoing integratioin
      PutIni('Touchpoint','Use'       ,TP_Use,        SB2KDEFINI)
      PutIni('Touchpoint','Address'   ,TP_Address ,   SB2KDEFINI)
      PutIni('Touchpoint','Key'       ,TP_Key ,       SB2KDEFINI)
      PutIni('Touchpoint','PathToExe' ,TP_PathToExe , SB2KDEFINI)
      PutIni('Touchpoint','Log'       ,TP_Logging,    SB2KDEFINI)
      PutIni('Touchpoint','LogFolder' ,TP_LogFolder , SB2KDEFINI)
      
      !TB13393 Apple accredited users
      PutIni('Apple','UseAccredUsers' ,UseAccredUsers ,    SB2KDEFINI)
      
      !All ini files now written - update the INIDATA file to mimic all the entries
      PutIniUpdateAll
      
      !TB13370 - CourierConfirmation
      putIni('CourierConf','UseSystem'     ,CC_UseSystem      , SB2KDEFINI)
      !PutIni('CourierConf','UserName'      ,CC_UserName       , SB2KDEFINI)
      !PutIni('CourierConf','UserPassword'  ,CC_UserPassword   , SB2KDEFINI)
      !PutIni('CourierConf','URL'           ,CC_URL            , SB2KDEFINI)
      PutIni('CourierConf','Path'          ,CC_PathToExe      , SB2KDEFINI)
      PutIni('CourierConf','Timer'         ,CC_Timer          , SB2KDEFINI)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OKButton, Accepted)
    OF ?def:HideLocation
      IF ?def:HideLocation{Prop:Checked} = True
        DISABLE(?tmp:HideEngLocation)
        ENABLE(?tmp:DisableLocation)
      END
      IF ?def:HideLocation{Prop:Checked} = False
        ENABLE(?tmp:HideEngLocation)
        DISABLE(?tmp:DisableLocation)
      END
      ThisWindow.Reset
    OF ?tmp:RenameColour
      IF ?tmp:RenameColour{Prop:Checked} = True
        UNHIDE(?tmp:ColourFieldName:Prompt)
        UNHIDE(?tmp:ColourFieldName)
      END
      IF ?tmp:RenameColour{Prop:Checked} = False
        HIDE(?tmp:ColourFieldName:Prompt)
        HIDE(?tmp:ColourFieldName)
      END
      ThisWindow.Reset
    OF ?tmp:RenameAuthorityNo
      IF ?tmp:RenameAuthorityNo{Prop:Checked} = True
        UNHIDE(?AuthNoNamePrompt)
        UNHIDE(?tmp:AuthorityNoName)
      END
      IF ?tmp:RenameAuthorityNo{Prop:Checked} = False
        HIDE(?AuthNoNamePrompt)
        HIDE(?tmp:AuthorityNoName)
      END
      ThisWindow.Reset
    OF ?tmp:DefaultSubAccount
      IF tmp:DefaultSubAccount OR ?tmp:DefaultSubAccount{Prop:Req}
        sub:Account_Number = tmp:DefaultSubAccount
        !Save Lookup Field Incase Of error
        look:tmp:DefaultSubAccount        = tmp:DefaultSubAccount
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:DefaultSubAccount = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            tmp:DefaultSubAccount = look:tmp:DefaultSubAccount
            SELECT(?tmp:DefaultSubAccount)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSubAccount
      ThisWindow.Update
      sub:Account_Number = tmp:DefaultSubAccount
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:DefaultSubAccount = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?tmp:DefaultSubAccount)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:DefaultSubAccount)
    OF ?tmp:RenameCourierCost
      IF ?tmp:RenameCourierCost{Prop:Checked} = True
        UNHIDE(?tmp:CourierCostName:Prompt)
        UNHIDE(?tmp:CourierCostName)
      END
      IF ?tmp:RenameCourierCost{Prop:Checked} = False
        HIDE(?tmp:CourierCostName:Prompt)
        HIDE(?tmp:CourierCostName)
      END
      ThisWindow.Reset
    OF ?def:Use_Postcode
      IF ?def:Use_Postcode{Prop:Checked} = True
        UNHIDE(?DEF:PostcodeDll)
      END
      IF ?def:Use_Postcode{Prop:Checked} = False
        HIDE(?DEF:PostcodeDll)
      END
      ThisWindow.Reset
    OF ?Lookup_Postcode_Path
      ThisWindow.Update
      def:Postcode_Path = Upper(FileLookup10.Ask(1)  )
      DISPLAY
    OF ?LookupExportPath
      ThisWindow.Update
      def:ExportPath = Upper(FileLookup13.Ask(1)  )
      DISPLAY
    OF ?LookupFile:17
      ThisWindow.Update
      locExchangeImportSaveFolder = FileLookup28.Ask(1)
      DISPLAY
    OF ?LookupFile:14
      ThisWindow.Update
      ApiDirectory = FileLookup25.Ask(1)
      DISPLAY
    OF ?LookupFile:15
      ThisWindow.Update
      ApiArchiveDir = FileLookup26.Ask(1)
      DISPLAY
    OF ?LookupFile:13
      ThisWindow.Update
      locStockImportFailureFolder = FileLookup24.Ask(1)
      DISPLAY
    OF ?LookupFile:8
      ThisWindow.Update
      locExchangeRateImportPath = Upper(FileLookup19.Ask(1)  )
      DISPLAY
    OF ?LookupFile:7
      ThisWindow.Update
      tmp:WaybillEmailFileFolder = Upper(FileLookup18.Ask(1)  )
      DISPLAY
    OF ?LookupFile:4
      ThisWindow.Update
      tmp:EstimateFolder = Upper(FileLookup16.Ask(1)  )
      DISPLAY
    OF ?def:Allow_Bouncer
      IF ?def:Allow_Bouncer{Prop:Checked} = True
        UNHIDE(?tmp:StopLiveBouncers)
      END
      IF ?def:Allow_Bouncer{Prop:Checked} = False
        HIDE(?tmp:StopLiveBouncers)
      END
      ThisWindow.Reset
    OF ?LookupFile
      ThisWindow.Update
      tmp:PDFBatchExportPath = Upper(FileLookup8.Ask(1)  )
      DISPLAY
    OF ?LookupFile:9
      ThisWindow.Update
      locLine500CSVImportFolder = Upper(FileLookup20.Ask(1)  )
      DISPLAY
    OF ?LookupFile:2
      ThisWindow.Update
      tmp:XMLRepositoryFolder = Upper(FileLookup14.Ask(1)  )
      DISPLAY
    OF ?tmp:IMEIValidation
      IF ?tmp:IMEIValidation{Prop:Checked} = True
        UNHIDE(?tmp:SBMQIPath:Prompt)
        UNHIDE(?tmp:SBMQIPath)
        UNHIDE(?LookupFile:3)
      END
      IF ?tmp:IMEIValidation{Prop:Checked} = False
        HIDE(?tmp:SBMQIPath:Prompt)
        HIDE(?tmp:SBMQIPath)
        HIDE(?LookupFile:3)
      END
      ThisWindow.Reset
    OF ?LookupFile:3
      ThisWindow.Update
      tmp:SBMQIPath = Upper(FileLookup15.Ask(1)  )
      DISPLAY
    OF ?LookupFile:5
      ThisWindow.Update
      tmp:MQRequestFolder = Upper(FileLookup9.Ask(1)  )
      DISPLAY
    OF ?EVO_CreateFiles
      IF ?EVO_CreateFiles{Prop:Checked} = True
        UNHIDE(?EVO_Group)
        UNHIDE(?EVO_SubGroup)
      END
      IF ?EVO_CreateFiles{Prop:Checked} = False
        HIDE(?EVO_Group)
        HIDE(?EVO_SubGroup)
      END
      ThisWindow.Reset
    OF ?LookupFile:6
      ThisWindow.Update
      tmp:NMSXMLFolder = Upper(FileLookup17.Ask(1)  )
      DISPLAY
    OF ?LookupFile:12
      ThisWindow.Update
      locSMSExceptionFolder = FileLookup23.Ask(1)
      DISPLAY
    OF ?LookupFile:16
      ThisWindow.Update
      locExchangeExceptionFolder = FileLookup27.Ask(1)
      DISPLAY
    OF ?LookupFile:18
      ThisWindow.Update
      locAuditImportExceptionFolder = FileLookup29.Ask(1)
      DISPLAY
    OF ?LookupFile:19
      ThisWindow.Update
      CustomerExceptionFolder = FileLookup30.Ask(1)
      DISPLAY
    OF ?LookupFile:11
      ThisWindow.Update
      locLoanSellingPriceExceptionFolder = Upper(FileLookup22.Ask(1)  )
      DISPLAY
    OF ?LookupFile:10
      ThisWindow.Update
      locExceptionExchangeRate = Upper(FileLookup21.Ask(1)  )
      DISPLAY
    OF ?CC_UseSystem
      IF ?CC_UseSystem{Prop:Checked} = True
        UNHIDE(?String12)
      END
      IF ?CC_UseSystem{Prop:Checked} = False
        HIDE(?String12)
      END
      ThisWindow.Reset
    OF ?ButtonCourierClear
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCourierClear, Accepted)
      if Missive('Running this procedure will mark all waybills as being confirmed to the courier.'&|
                 '|This may be useful if you have stopped the service for a while and dont want historic data being sent',|
                 'ServiceBase 3g','mSTOP.jpg','OK|CANCEL') = 1 then
      
          Access:Waybconf.clearkey(WAC:KeyRecordNo)
          WAC:RecordNo = 1
          set(WAC:KeyRecordNo,WAC:KeyRecordNo)
          Loop
              if access:WAYBCONF.next() then break.
              WAC:ConfirmationSent = 1
              Access:Waybconf.update()
          END
          Miss# = missive('FINISHED.','ServiceBase 3g','mSTOP.jpg','OK')
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCourierClear, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
      If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
          ?def:SummaryOrders{prop:hide} = 0
      End !FullAccess(glo:PassAccount,glo:Password)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
StandardTexts PROCEDURE                               !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW5::View:Browse    VIEW(STANTEXT)
                       PROJECT(stt:Description)
                       PROJECT(stt:Text)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
stt:Description        LIKE(stt:Description)          !List box control field - type derived from field
stt:Text               LIKE(stt:Text)                 !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('General Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Standard Document Texts'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Standard &Document Texts'),USE(?Standard_Document_Text_Tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,87,124,10),USE(stt:Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           LIST,AT(168,100,148,226),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)~Description~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(320,191),USE(?Insert),FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(320,220),USE(?Change),FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(320,247),USE(?Delete),FLAT,LEFT,ICON('deletep.jpg')
                           TEXT,AT(320,100,192,88),USE(stt:Text),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                         END
                       END
                       BUTTON,AT(384,332),USE(?OkButton),FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  IncrementalLocatorClass          !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020134'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('StandardTexts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:STANTEXT.Open
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:STANTEXT,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  
  
  
  
  ! Save Window Name
   AddToLog('Window','Open','StandardTexts')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,stt:Description_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(?stt:Description,stt:Description,1,BRW5)
  BRW5.AddField(stt:Description,BRW5.Q.stt:Description)
  BRW5.AddField(stt:Text,BRW5.Q.stt:Text)
  BRW5.AskProcedure = 1
  BRW5.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:STANTEXT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','StandardTexts')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSTANTEXT
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020134'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020134'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020134'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:defaults.update()
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?stt:Description
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stt:Description, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stt:Description, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

PrintingDefaults PROCEDURE                            !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
SDS_Thermal          STRING(3)
tmp:ThirdPartyDetails BYTE(0)
tmp:ThirdPartyJobLabel BYTE(0)
tmp:ExchangeRepairLabel BYTE(0)
Tmp:PrintA5Invoice   BYTE(0)
tmp:DoNotPrintChargeableDespatchNotes BYTE(0)
tmp:SetWaybillPrefix BYTE(0)
tmp:WaybillPrefix    STRING(5)
locTermsText         STRING(255)
BRW8::View:Browse    VIEW(DEFPRINT)
                       PROJECT(dep:Printer_Name)
                       PROJECT(dep:Printer_Path)
                       PROJECT(dep:Copies)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
dep:Printer_Name       LIKE(dep:Printer_Name)         !List box control field - type derived from field
dep:Printer_Path       LIKE(dep:Printer_Path)         !List box control field - type derived from field
dep:Copies             LIKE(dep:Copies)               !List box control field - type derived from field
tmp:Background         LIKE(tmp:Background)           !List box control field - type derived from local data
tmp:Background_Icon    LONG                           !Entry's icon ID
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('General Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,54,552,310),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Printer Defaults'),USE(?printerPreferences)
                           PROMPT('Default Printers'),AT(116,60),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(116,72,388,92),USE(?List:2),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('127L(2)|M~Report Name~@s60@161L(2)|M~Printer Name~@s255@28R(2)|M~Copies~@n3@11L(' &|
   '2)J~Background~@s1@'),FROM(Queue:Browse:1)
                           BUTTON,AT(508,70),USE(?Insert:2),FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(508,104),USE(?Change:2),FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(508,138),USE(?Delete:2),FLAT,LEFT,ICON('deletep.jpg')
                           GROUP('Label Printing Defaults'),AT(388,172,220,142),USE(?LabelPrintingDefaults),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           PROMPT('Label Printer Type'),AT(396,184),USE(?Prompt44),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(476,184,124,10),USE(def:Label_Printer_Type),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(label_queue_temp)
                           CHECK('Print Job Label'),AT(396,200),USE(def:Use_Job_Label),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Use Small Label'),AT(440,200),USE(def:UseSmallLabel),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use Small Label'),TIP('Use Small Label'),VALUE('1','0')
                           CHECK('Print Job Part Label'),AT(396,212),USE(def:add_stock_label),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Print Loan/Exchange Label'),AT(396,224),USE(def:Use_Loan_Exchange_Label),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Print Exchange Repair Label'),AT(396,236),USE(tmp:ExchangeRepairLabel),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Print Exchange Repair Label'),TIP('Print Exchange Repair Label'),VALUE('1','0')
                           CHECK('Print Stock Label'),AT(396,248),USE(def:receive_stock_label),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Print QA Failed Label'),AT(396,260),USE(def:QA_Failed_Label),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Print QA Pass Label'),AT(500,260),USE(def:QAPassLabel),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Print QA Pass Label'),TIP('Print QA Pass Label'),VALUE('1','0')
                           CHECK('Print Retained Accessory Label'),AT(396,272),USE(de2:PrintRetainedAccLabel),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Print Retained Accessory Label'),TIP('Print Retained Accessory Label'),VALUE('1','0')
                           CHECK('Print Thermal SDS Labels'),AT(396,284),USE(SDS_Thermal),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Print 3rd Party Returns Label'),AT(396,296),USE(tmp:ThirdPartyJobLabel),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Print'),TIP('Print'),VALUE('1','0')
                           GROUP('"Terms && Conditions" Text'),AT(68,318,540,44),USE(?Group3),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                             TEXT,AT(76,330,304,28),USE(locTermsText),FONT(,,0101010H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             PROMPT('Text to appear on Job Card, Customer Receipt, Loan Collection Note and Job/Excha' &|
   'nge Despatch Note'),AT(384,330,216,24),USE(?Prompt6)
                           END
                           GROUP('General Printing Defaults'),AT(68,172,312,142),USE(?Group2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           PROMPT('Waybill Prefix'),AT(292,284),USE(?tmp:WaybillPrefix:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s5),AT(292,294,64,10),USE(tmp:WaybillPrefix),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Waybill Prefix'),TIP('Waybill Prefix'),UPR
                           CHECK('Override Default Waybill Number Prefix On Waybills'),AT(76,294),USE(tmp:SetWaybillPrefix),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Set Waybill Prefix'),TIP('Set Waybill Prefix'),VALUE('1','0')
                           CHECK('Remove Report Background From All Reports'),AT(76,184),USE(def:Remove_Backgrounds),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Print I.M.E.I. Number Barcodes On Reports'),AT(76,196),USE(def:PrintBarcode),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Print Barcode'),TIP('Print Barcode'),VALUE('1','0')
                           CHECK('Print Third Party Despatch Note'),AT(76,208),USE(def:ThirdPartyNote),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Print Single 3rd Party Despatch Note'),TIP('Print Single 3rd Party Despatch Note'),VALUE('YES','NO')
                           CHECK('Print QA Failed Report'),AT(76,220),USE(def:QA_Failed_Report),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Print Accessories On Job Label '),AT(76,232),USE(def:Job_Label_Accessories),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Show Euro Symbol on Financial Paperwork'),AT(76,244),USE(de2:CurrencySymbol),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO'),MSG('Currency Symbol')
                           CHECK('Print A5 Invoice'),AT(76,256),USE(Tmp:PrintA5Invoice),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                           CHECK('Hide 3rd Party Consignment Note Details'),AT(76,268),USE(tmp:ThirdPartyDetails),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Hide Third Party Consignment Details'),TIP('Hide Third Party Consignment Details'),VALUE('1','0')
                           CHECK('Do Not Print Despatch Notes For Chargeable Jobs'),AT(76,280),USE(tmp:DoNotPrintChargeableDespatchNotes),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Printing Defaults'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(480,366),USE(?OkButton),FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?CancelButton),FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020130'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('PrintingDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:DEFSTOCK.Open
  SELF.FilesOpened = True
  Clear(label_queue_temp)
  Free(label_queue_temp)
  
  label_printer_temp = 'TEC B-440 / B-442'
  Add(label_queue_temp)
  
  label_printer_temp = 'TEC B-452'
  Add(label_queue_Temp)
  
  ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
  locTermsText = GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI')
  BRW8.Init(?List:2,Queue:Browse:1.ViewPosition,BRW8::View:Browse,Queue:Browse:1,Relate:DEFPRINT,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  Set(DEFAULT2)
  If Access:DEFAULT2.Next()
      If Access:DEFAULT2.PrimeRecord() = Level:Benign
  
          If Access:DEFAULT2.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:DEFAULT2.TryInsert() = Level:Benign
              !Insert Failed
          End !If Access:DEFAULT2.TryInsert() = Level:Benign
      End !If Access:DEFAULT2.PrimeRecord() = Level:Benign
  End !Access:DEFAULT2.Next()
  SDS_Thermal = GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ThirdPartyJobLabel = GETINI('PRINTING','ThirdPartyReturnsLabel',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ExchangeRepairLabel = GETINI('PRINTING','ExchangeRepairLabel',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ThirdPartyDetails = GETINI('HIDE','ThirdPartyDetails',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:PrintA5Invoice = GETINI('PRINTING','PrintA5Invoice',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:DoNotPrintChargeableDespatchNotes = GETINI('PRINTING','NoChargeableDespatchNotes',,CLIP(PATH())&'\SB2KDEF.INI')
  ! Inserting (DBH 20/03/2007) # 8865 - Allow user to specify waybill number prefix
  tmp:SetWaybillPrefix = GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')
  tmp:WaybillPrefix = GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')
  ! End (DBH 20/03/2007) #8865
  ! Save Window Name
   AddToLog('Window','Open','PrintingDefaults')
  ?List:2{prop:vcr} = TRUE
  ?def:Label_Printer_Type{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?def:Label_Printer_Type{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse:1
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,)
  BIND('tmp:Background',tmp:Background)
  ?List:2{PROP:IconList,1} = '~bluetick.ico'
  BRW8.AddField(dep:Printer_Name,BRW8.Q.dep:Printer_Name)
  BRW8.AddField(dep:Printer_Path,BRW8.Q.dep:Printer_Path)
  BRW8.AddField(dep:Copies,BRW8.Q.dep:Copies)
  BRW8.AddField(tmp:Background,BRW8.Q.tmp:Background)
  IF ?def:Use_Job_Label{Prop:Checked} = True
    UNHIDE(?def:UseSmallLabel)
  END
  IF ?def:Use_Job_Label{Prop:Checked} = False
    HIDE(?def:UseSmallLabel)
  END
  IF ?def:Use_Loan_Exchange_Label{Prop:Checked} = True
    ENABLE(?tmp:ExchangeRepairLabel)
  END
  IF ?def:Use_Loan_Exchange_Label{Prop:Checked} = False
    DISABLE(?tmp:ExchangeRepairLabel)
  END
  IF ?tmp:SetWaybillPrefix{Prop:Checked} = True
    ENABLE(?tmp:WaybillPrefix:Prompt)
    ENABLE(?tmp:WaybillPrefix)
  END
  IF ?tmp:SetWaybillPrefix{Prop:Checked} = False
    DISABLE(?tmp:WaybillPrefix:Prompt)
    DISABLE(?tmp:WaybillPrefix)
  END
  BRW8.AskProcedure = 1
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:DEFSTOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','PrintingDefaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Printer_Defaults
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?def:Use_Job_Label
      IF ?def:Use_Job_Label{Prop:Checked} = True
        UNHIDE(?def:UseSmallLabel)
      END
      IF ?def:Use_Job_Label{Prop:Checked} = False
        HIDE(?def:UseSmallLabel)
      END
      ThisWindow.Reset
    OF ?def:Use_Loan_Exchange_Label
      IF ?def:Use_Loan_Exchange_Label{Prop:Checked} = True
        ENABLE(?tmp:ExchangeRepairLabel)
      END
      IF ?def:Use_Loan_Exchange_Label{Prop:Checked} = False
        DISABLE(?tmp:ExchangeRepairLabel)
      END
      ThisWindow.Reset
    OF ?tmp:SetWaybillPrefix
      IF ?tmp:SetWaybillPrefix{Prop:Checked} = True
        ENABLE(?tmp:WaybillPrefix:Prompt)
        ENABLE(?tmp:WaybillPrefix)
      END
      IF ?tmp:SetWaybillPrefix{Prop:Checked} = False
        DISABLE(?tmp:WaybillPrefix:Prompt)
        DISABLE(?tmp:WaybillPrefix)
      END
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020130'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020130'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020130'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:defaults.update()
      Access:DEFAULT2.Update()
      PUTINIext('PRINTING','SDS',SDS_Thermal,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('HIDE','ThirdPartyDetails',tmp:ThirdPartyDetails,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('PRINTING','ThirdPartyReturnsLabel',tmp:ThirdPartyJobLabel,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('PRINTING','ExchangeRepairLabel',tmp:ExchangeRepairLabel,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('PRINTING','PrintA5Invoice',tmp:PrintA5Invoice,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('PRINTING','NoChargeableDespatchNotes',tmp:DoNotPrintChargeableDespatchNotes,CLIP(PATH()) & '\SB2KDEF.INI')
      ! Inserting (DBH 20/03/2007) # 8865 - Allow user to specify waybill number prefix
      PUTINIext('PRINTING','SetWaybillPrefix',tmp:SetWaybillPrefix,Clip(Path()) & '\SB2KDEF.INI')
      PUTINIext('PRINTING','WaybillPrefix',tmp:WaybillPrefix,Clip(Path()) & '\SB2KDEF.INI')
      ! End (DBH 20/03/2007) #8865
      ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
      PUTINIext('PRINTING','TermsText',locTermsText,PATH() & '\SB2KDEF.INI')
      
      
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      CONVERTOEMTOANSI(de2:CurrencySymbol)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW8.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (dep:background <> 'YES')
    SELF.Q.tmp:Background_Icon = 1
  ELSE
    SELF.Q.tmp:Background_Icon = 0
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

BookingDefaults PROCEDURE                             !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:DefaultHeadAccount STRING(30)
tmp:DefaultLocation  STRING(30)
tmp:TradeOrder       BYTE(0)
window               WINDOW('d'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Booking Defaults'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Booking Defaults'),USE(?BookingDefaultsTab)
                           GROUP('Default Charge Types'),AT(204,134,272,86),USE(?DefaultChargeTypes),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('The following Charge Types will be automatically filled in when Chargeable/Warra' &|
   'nty Job is selected on a New Job'),AT(208,146,264,26),USE(?Prompt42),CENTER,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           END
                           PROMPT('Chargeable'),AT(220,182),USE(?def:ChaChargeType:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(308,182,124,10),USE(def:ChaChargeType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),MSG('Chargeable Charge Type'),TIP('Chargeable Charge Type'),ALRT(MouseRight),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(436,178),USE(?LookupChargeableChargeType),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Warranty'),AT(220,202),USE(?def:WarChargeType:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(308,202,124,10),USE(def:WarChargeType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),MSG('Warranty Charge Type'),TIP('Warranty Charge Type'),ALRT(EnterKey),ALRT(MouseRight),ALRT(MouseLeft2),UPR
                           BUTTON,AT(436,198),USE(?LookupWarrantyChargeType),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Trade Header Account'),AT(220,236),USE(?tmp:DefaultHeadAccount:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(308,236,124,10),USE(tmp:DefaultHeadAccount),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),MSG('Trade Header Account'),TIP('Trade Header Account'),UPR
                           BUTTON,AT(436,231),USE(?LookupAccountNumber),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Site Location'),AT(220,260),USE(?tmp:DefaultLocation:Prompt),TRN,LEFT,FONT(,8,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(308,260,124,10),USE(tmp:DefaultLocation),SKIP,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),MSG('Site Location'),TIP('Site Location'),UPR,READONLY
                           STRING('The default site location will be updated if you reselect the default head accou' &|
   'nt'),AT(192,274),USE(?String1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:def:ChaChargeType                Like(def:ChaChargeType)
look:def:WarChargeType                Like(def:WarChargeType)
look:tmp:DefaultHeadAccount                Like(tmp:DefaultHeadAccount)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020122'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BookingDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  
  tmp:DefaultHeadAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:TradeOrder  = GETINI('BOOKING','AccountOrder',,CLIP(PATH())&'\SB2KDEF.INI')
  
  !TB13240 - J - 23/05/14 - move default location setting here
  tmp:DefaultLocation = geTINI('BOOKING','HeadSiteLocation','',CLIP(PATH()) & '\SB2KDEF.INI')
  ! Save Window Name
   AddToLog('Window','Open','BookingDefaults')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?def:ChaChargeType{Prop:Tip} AND ~?LookupChargeableChargeType{Prop:Tip}
     ?LookupChargeableChargeType{Prop:Tip} = 'Select ' & ?def:ChaChargeType{Prop:Tip}
  END
  IF ?def:ChaChargeType{Prop:Msg} AND ~?LookupChargeableChargeType{Prop:Msg}
     ?LookupChargeableChargeType{Prop:Msg} = 'Select ' & ?def:ChaChargeType{Prop:Msg}
  END
  IF ?def:WarChargeType{Prop:Tip} AND ~?LookupWarrantyChargeType{Prop:Tip}
     ?LookupWarrantyChargeType{Prop:Tip} = 'Select ' & ?def:WarChargeType{Prop:Tip}
  END
  IF ?def:WarChargeType{Prop:Msg} AND ~?LookupWarrantyChargeType{Prop:Msg}
     ?LookupWarrantyChargeType{Prop:Msg} = 'Select ' & ?def:WarChargeType{Prop:Msg}
  END
  IF ?tmp:DefaultHeadAccount{Prop:Tip} AND ~?LookupAccountNumber{Prop:Tip}
     ?LookupAccountNumber{Prop:Tip} = 'Select ' & ?tmp:DefaultHeadAccount{Prop:Tip}
  END
  IF ?tmp:DefaultHeadAccount{Prop:Msg} AND ~?LookupAccountNumber{Prop:Msg}
     ?LookupAccountNumber{Prop:Msg} = 'Select ' & ?tmp:DefaultHeadAccount{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BookingDefaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickChargeableChargeTypes
      PickWarrantyChargeTypes
      BrowseMainAccount
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020122'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020122'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020122'&'0')
      ***
    OF ?def:ChaChargeType
      IF def:ChaChargeType OR ?def:ChaChargeType{Prop:Req}
        cha:Charge_Type = def:ChaChargeType
        !Save Lookup Field Incase Of error
        look:def:ChaChargeType        = def:ChaChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            def:ChaChargeType = cha:Charge_Type
          ELSE
            !Restore Lookup On Error
            def:ChaChargeType = look:def:ChaChargeType
            SELECT(?def:ChaChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupChargeableChargeType
      ThisWindow.Update
      cha:Charge_Type = def:ChaChargeType
      GLO:Select1 = 'NO'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          def:ChaChargeType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?def:ChaChargeType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?def:ChaChargeType)
    OF ?def:WarChargeType
      IF def:WarChargeType OR ?def:WarChargeType{Prop:Req}
        cha:Charge_Type = def:WarChargeType
        !Save Lookup Field Incase Of error
        look:def:WarChargeType        = def:WarChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            def:WarChargeType = cha:Charge_Type
          ELSE
            !Restore Lookup On Error
            def:WarChargeType = look:def:WarChargeType
            SELECT(?def:WarChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupWarrantyChargeType
      ThisWindow.Update
      cha:Charge_Type = def:WarChargeType
      GLO:Select1 = 'YES'
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          def:WarChargeType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?def:WarChargeType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?def:WarChargeType)
    OF ?tmp:DefaultHeadAccount
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:DefaultHeadAccount, Accepted)
      !Need to fetch the full details
      
      !TB13240 - J - 23/05/14 moving default site location to here
      Access:Tradeacc.clearkey(tra:Account_Number_Key)
      tra:Account_Number = tmp:DefaultHeadAccount
      if access:Tradeacc.fetch(tra:Account_Number_Key)
          !error
      END
      
      tmp:DefaultLocation = tra:SiteLocation
      display(?tmp:DefaultLocation)
      IF tmp:DefaultHeadAccount OR ?tmp:DefaultHeadAccount{Prop:Req}
        tra:Account_Number = tmp:DefaultHeadAccount
        !Save Lookup Field Incase Of error
        look:tmp:DefaultHeadAccount        = tmp:DefaultHeadAccount
        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tmp:DefaultHeadAccount = tra:Account_Number
          ELSE
            !Restore Lookup On Error
            tmp:DefaultHeadAccount = look:tmp:DefaultHeadAccount
            SELECT(?tmp:DefaultHeadAccount)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:DefaultHeadAccount, Accepted)
    OF ?LookupAccountNumber
      ThisWindow.Update
      tra:Account_Number = tmp:DefaultHeadAccount
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tmp:DefaultHeadAccount = tra:Account_Number
          Select(?+1)
      ELSE
          Select(?tmp:DefaultHeadAccount)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:DefaultHeadAccount)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      !If def:auto_logout <> 'NO'
      !    If def:auto_logout_time < 60 Or def:auto_logout_time > 3600
      !        beep(beep:systemhand)  ;  yield()
      !        message('The logout time must be between 60 seconds (1 minute) and '&|
      !               '|3,600 seconds (1 hour).', |
      !               'ServiceBase 2000', icon:hand)
      !        def:auto_logout_time = ''
      !        Select(?def:auto_logout_time)
      !        Cycle
      !    End
      !End
      access:defaults.update()
      PUTINIEXT('BOOKING','HeadAccount',tmp:DefaultHeadAccount,CLIP(PATH()) & '\SB2KDEF.INI')
      !TB13240 - J - 23/05/14 - move default location setting here
      PUTINIEXT('BOOKING','HeadSiteLocation',tmp:DefaultLocation,CLIP(PATH()) & '\SB2KDEF.INI')
      
      
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
