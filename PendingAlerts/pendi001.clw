

   MEMBER('pendingalerts.clw')                        ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('PENDI001.INC'),ONCE        !Local module procedure declarations
                     END






Main PROCEDURE                                        !Generated from procedure template - Browse

CurrentTab           STRING(80)
tmp:BrowseFilter     BYTE
BRW1::View:Browse    VIEW(SMSMAIL)
                       PROJECT(sms:DateInserted)
                       PROJECT(sms:TimeInserted)
                       PROJECT(sms:RefNumber)
                       PROJECT(sms:SendToSMS)
                       PROJECT(sms:SendToEmail)
                       PROJECT(sms:MSISDN)
                       PROJECT(sms:EmailAddress)
                       PROJECT(sms:SMSSent)
                       PROJECT(sms:DateSMSSent)
                       PROJECT(sms:TimeSMSSent)
                       PROJECT(sms:EmailSent)
                       PROJECT(sms:DateEmailSent)
                       PROJECT(sms:TimeEmailSent)
                       PROJECT(sms:SBUpdated)
                       PROJECT(sms:MSG)
                       PROJECT(sms:PathToEstimate)
                       PROJECT(sms:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sms:DateInserted       LIKE(sms:DateInserted)         !List box control field - type derived from field
sms:TimeInserted       LIKE(sms:TimeInserted)         !List box control field - type derived from field
sms:RefNumber          LIKE(sms:RefNumber)            !List box control field - type derived from field
sms:SendToSMS          LIKE(sms:SendToSMS)            !List box control field - type derived from field
sms:SendToEmail        LIKE(sms:SendToEmail)          !List box control field - type derived from field
sms:MSISDN             LIKE(sms:MSISDN)               !List box control field - type derived from field
sms:EmailAddress       LIKE(sms:EmailAddress)         !List box control field - type derived from field
sms:SMSSent            LIKE(sms:SMSSent)              !List box control field - type derived from field
sms:DateSMSSent        LIKE(sms:DateSMSSent)          !List box control field - type derived from field
sms:TimeSMSSent        LIKE(sms:TimeSMSSent)          !List box control field - type derived from field
sms:EmailSent          LIKE(sms:EmailSent)            !List box control field - type derived from field
sms:DateEmailSent      LIKE(sms:DateEmailSent)        !List box control field - type derived from field
sms:TimeEmailSent      LIKE(sms:TimeEmailSent)        !List box control field - type derived from field
sms:SBUpdated          LIKE(sms:SBUpdated)            !List box control field - type derived from field
sms:MSG                LIKE(sms:MSG)                  !List box control field - type derived from field
sms:PathToEstimate     LIKE(sms:PathToEstimate)       !List box control field - type derived from field
sms:RecordNumber       LIKE(sms:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,1073741823)         !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse the SMS / Email Log'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('SMS / Email Log'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(8,50,664,328),USE(?Browse:1),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browse Message Log'),TIP('Browse Message Log'),FORMAT('50R(2)|M~Date Inserted~@d6@26R(2)|M~Time~@t1b@36R(2)|M~Job No~@s8@50C(2)|M~Send ' &|
   'To SMS~@s1@51C(2)|M~Send To Email~@s1@54L(2)|M~Mobile Number~@s12@106L(2)|M~Emai' &|
   'l Address~@s255@39C(2)|M~SMS Sent~@s1@54R(2)|M~Date SMS Sent~@d6b@26R(2)|M~Time~' &|
   '@t1b@39C(2)|M~Email Sent~@s1@54R(2)|M~Date Email Sent~@d6b@27R(2)|M~Time~@t1b@29' &|
   'C(2)|M~SB Upd~@n1@200L(2)|M~Message~@s160@500L(2)|M~Path To Estimate~@s255@/'),FROM(Queue:Browse:1)
                       SHEET,AT(4,30,672,352),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('SMS / Email Log'),USE(?Tab:2)
                         END
                       END
                       BUTTON,AT(608,384),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepCustomClass !DATE            !Xplore: Column displaying sms:DateInserted
Xplore1Locator1      StepLocatorClass                 !Xplore: Column displaying sms:DateInserted
Xplore1Step2         StepCustomClass !TIME            !Xplore: Column displaying sms:TimeInserted
Xplore1Locator2      StepLocatorClass                 !Xplore: Column displaying sms:TimeInserted
Xplore1Step3         StepLongClass   !LONG            !Xplore: Column displaying sms:RefNumber
Xplore1Locator3      StepLocatorClass                 !Xplore: Column displaying sms:RefNumber
Xplore1Step4         StepStringClass !STRING          !Xplore: Column displaying sms:SendToSMS
Xplore1Locator4      StepLocatorClass                 !Xplore: Column displaying sms:SendToSMS
Xplore1Step5         StepStringClass !STRING          !Xplore: Column displaying sms:SendToEmail
Xplore1Locator5      StepLocatorClass                 !Xplore: Column displaying sms:SendToEmail
Xplore1Step6         StepStringClass !STRING          !Xplore: Column displaying sms:MSISDN
Xplore1Locator6      StepLocatorClass                 !Xplore: Column displaying sms:MSISDN
Xplore1Step7         StepStringClass !STRING          !Xplore: Column displaying sms:EmailAddress
Xplore1Locator7      StepLocatorClass                 !Xplore: Column displaying sms:EmailAddress
Xplore1Step8         StepStringClass !STRING          !Xplore: Column displaying sms:SMSSent
Xplore1Locator8      StepLocatorClass                 !Xplore: Column displaying sms:SMSSent
Xplore1Step9         StepCustomClass !DATE            !Xplore: Column displaying sms:DateSMSSent
Xplore1Locator9      StepLocatorClass                 !Xplore: Column displaying sms:DateSMSSent
Xplore1Step10        StepCustomClass !TIME            !Xplore: Column displaying sms:TimeSMSSent
Xplore1Locator10     StepLocatorClass                 !Xplore: Column displaying sms:TimeSMSSent
Xplore1Step11        StepStringClass !STRING          !Xplore: Column displaying sms:EmailSent
Xplore1Locator11     StepLocatorClass                 !Xplore: Column displaying sms:EmailSent
Xplore1Step12        StepCustomClass !DATE            !Xplore: Column displaying sms:DateEmailSent
Xplore1Locator12     StepLocatorClass                 !Xplore: Column displaying sms:DateEmailSent
Xplore1Step13        StepCustomClass !TIME            !Xplore: Column displaying sms:TimeEmailSent
Xplore1Locator13     StepLocatorClass                 !Xplore: Column displaying sms:TimeEmailSent
Xplore1Step14        StepCustomClass !BYTE            !Xplore: Column displaying sms:SBUpdated
Xplore1Locator14     StepLocatorClass                 !Xplore: Column displaying sms:SBUpdated
Xplore1Step15        StepStringClass !STRING          !Xplore: Column displaying sms:MSG
Xplore1Locator15     StepLocatorClass                 !Xplore: Column displaying sms:MSG
Xplore1Step16        StepStringClass !STRING          !Xplore: Column displaying sms:PathToEstimate
Xplore1Locator16     StepLocatorClass                 !Xplore: Column displaying sms:PathToEstimate
Xplore1Step17        StepLongClass   !LONG            !Xplore: Column displaying sms:RecordNumber
Xplore1Locator17     StepLocatorClass                 !Xplore: Column displaying sms:RecordNumber

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Main','?Browse:1')               !Xplore
  BRW1.SequenceNbr = 0                                !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020648'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      ! Close Splash Window (DBH: 24-05-2005)
      PUTINI('STARTING','Run',True,)
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:SMSMAIL.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:SMSMAIL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ?Browse:1{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'pendingalerts.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sms:EmailSentKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,sms:SendToEmail,1,BRW1)
  BRW1.AddField(sms:DateInserted,BRW1.Q.sms:DateInserted)
  BRW1.AddField(sms:TimeInserted,BRW1.Q.sms:TimeInserted)
  BRW1.AddField(sms:RefNumber,BRW1.Q.sms:RefNumber)
  BRW1.AddField(sms:SendToSMS,BRW1.Q.sms:SendToSMS)
  BRW1.AddField(sms:SendToEmail,BRW1.Q.sms:SendToEmail)
  BRW1.AddField(sms:MSISDN,BRW1.Q.sms:MSISDN)
  BRW1.AddField(sms:EmailAddress,BRW1.Q.sms:EmailAddress)
  BRW1.AddField(sms:SMSSent,BRW1.Q.sms:SMSSent)
  BRW1.AddField(sms:DateSMSSent,BRW1.Q.sms:DateSMSSent)
  BRW1.AddField(sms:TimeSMSSent,BRW1.Q.sms:TimeSMSSent)
  BRW1.AddField(sms:EmailSent,BRW1.Q.sms:EmailSent)
  BRW1.AddField(sms:DateEmailSent,BRW1.Q.sms:DateEmailSent)
  BRW1.AddField(sms:TimeEmailSent,BRW1.Q.sms:TimeEmailSent)
  BRW1.AddField(sms:SBUpdated,BRW1.Q.sms:SBUpdated)
  BRW1.AddField(sms:MSG,BRW1.Q.sms:MSG)
  BRW1.AddField(sms:PathToEstimate,BRW1.Q.sms:PathToEstimate)
  BRW1.AddField(sms:RecordNumber,BRW1.Q.sms:RecordNumber)
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SMSMAIL.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Main','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(sms:DateInserted,BRW1.Q.sms:DateInserted)
  Xplore1.AddField(sms:TimeInserted,BRW1.Q.sms:TimeInserted)
  Xplore1.AddField(sms:RefNumber,BRW1.Q.sms:RefNumber)
  Xplore1.AddField(sms:SendToSMS,BRW1.Q.sms:SendToSMS)
  Xplore1.AddField(sms:SendToEmail,BRW1.Q.sms:SendToEmail)
  Xplore1.AddField(sms:MSISDN,BRW1.Q.sms:MSISDN)
  Xplore1.AddField(sms:EmailAddress,BRW1.Q.sms:EmailAddress)
  Xplore1.AddField(sms:SMSSent,BRW1.Q.sms:SMSSent)
  Xplore1.AddField(sms:DateSMSSent,BRW1.Q.sms:DateSMSSent)
  Xplore1.AddField(sms:TimeSMSSent,BRW1.Q.sms:TimeSMSSent)
  Xplore1.AddField(sms:EmailSent,BRW1.Q.sms:EmailSent)
  Xplore1.AddField(sms:DateEmailSent,BRW1.Q.sms:DateEmailSent)
  Xplore1.AddField(sms:TimeEmailSent,BRW1.Q.sms:TimeEmailSent)
  Xplore1.AddField(sms:SBUpdated,BRW1.Q.sms:SBUpdated)
  Xplore1.AddField(sms:MSG,BRW1.Q.sms:MSG)
  Xplore1.AddField(sms:PathToEstimate,BRW1.Q.sms:PathToEstimate)
  Xplore1.AddField(sms:RecordNumber,BRW1.Q.sms:RecordNumber)
  BRW1.FileOrderNbr = BRW1.AddSortOrder()             !Xplore Sort Order for File Sequence
  BRW1.SetOrder('')                                   !Xplore
  Xplore1.AddAllColumnSortOrders(0)                   !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020648'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020648'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020648'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
    OF EVENT:GainFocus
      BRW1.ResetQueue(Reset:Queue)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  !Case tmp:BrowseFilter
  !Of 0 !Show All
  !Of 1 !ALl Sent
  !    If pem:DateSent = ''
  !        Return Record:Filtered
  !    End ! If pem:DateSent = ''
  !Of 2 !All Unsent
  !    If pem:DateSent <> ''
  !        Return Record:Filtered
  !    End ! If pem:DateSent <> ''
  !Of 3 !Unsent SMS
  !    If pem:SMSEmail = 'EMAIL'
  !        Return Record:Filtered
  !    Else ! If pem:SMSEmail = 'EMAIL'
  !        If pem:DateSent <> ''
  !            Return Record:Filtered
  !        End ! If pem:DateSent <> ''
  !    End ! If pem:SMSEmail = 'EMAIL'
  !Of 4 !Unsent Email
  !    If pem:SMSEmail = 'SMS'
  !        Return Record:Filtered
  !    Else ! If pem:SMSEmail = 'SMS'
  !        If pem:DateSent <> ''
  !            Return Record:Filtered
  !        End ! If pem:DateSent <> ''
  !    End ! If pem:SMSEmail = 'SMS'
  !Of 5 !All Errors
  !    If pem:ErrorMessage <> ''
  !        Return Record:Filtered
  !    End ! If pem:ErrorMessage <> ''
  !End ! Case tmp:BrowseFilter
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !sms:DateInserted
  OF 2 !sms:TimeInserted
  OF 3 !sms:RefNumber
  OF 4 !sms:SendToSMS
  OF 5 !sms:SendToEmail
  OF 6 !sms:MSISDN
  OF 7 !sms:EmailAddress
  OF 8 !sms:SMSSent
  OF 9 !sms:DateSMSSent
  OF 10 !sms:TimeSMSSent
  OF 11 !sms:EmailSent
  OF 12 !sms:DateEmailSent
  OF 13 !sms:TimeEmailSent
  OF 14 !sms:SBUpdated
  OF 15 !sms:MSG
  OF 16 !sms:PathToEstimate
  OF 17 !sms:RecordNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step17)
  SELF.FQ.SortField = SELF.BC.AddSortOrder()
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(,sms:DateInserted,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,sms:TimeInserted,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,sms:RefNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(,sms:SendToSMS,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(,sms:SendToEmail,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(,sms:MSISDN,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(,sms:EmailAddress,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(,sms:SMSSent,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(,sms:DateSMSSent,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step10.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator10)
      Xplore1Locator10.Init(,sms:TimeSMSSent,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step11.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator11)
      Xplore1Locator11.Init(,sms:EmailSent,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step12.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator12)
      Xplore1Locator12.Init(,sms:DateEmailSent,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step13.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator13)
      Xplore1Locator13.Init(,sms:TimeEmailSent,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step14.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator14)
      Xplore1Locator14.Init(,sms:SBUpdated,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step15.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator15)
      Xplore1Locator15.Init(,sms:MSG,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step16.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator16)
      Xplore1Locator16.Init(,sms:PathToEstimate,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step17.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator17)
      Xplore1Locator17.Init(,sms:RecordNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(SMSMAIL)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('sms:DateInserted')           !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Date Inserted')              !Header
                PSTRING('@d6')                        !Picture
                PSTRING('Date Inserted')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:TimeInserted')           !Field Name
                SHORT(20)                             !Default Column Width
                PSTRING('Time')                       !Header
                PSTRING('@t1b')                       !Picture
                PSTRING('Time Inserted')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:RefNumber')              !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job No')                     !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Ref Number')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:SendToSMS')              !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Send To SMS')                !Header
                PSTRING('@s1')                        !Picture
                PSTRING('Send To SMS')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:SendToEmail')            !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Send To Email')              !Header
                PSTRING('@s1')                        !Picture
                PSTRING('Send To Email')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:MSISDN')                 !Field Name
                SHORT(48)                             !Default Column Width
                PSTRING('Mobile Number')              !Header
                PSTRING('@s12')                       !Picture
                PSTRING('Mobile Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:EmailAddress')           !Field Name
                SHORT(1020)                           !Default Column Width
                PSTRING('Email Address')              !Header
                PSTRING('@s255')                      !Picture
                PSTRING('EmailAddress')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:SMSSent')                !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('SMS Sent')                   !Header
                PSTRING('@s1')                        !Picture
                PSTRING('SMS Sent')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:DateSMSSent')            !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Date SMS Sent')              !Header
                PSTRING('@d6b')                       !Picture
                PSTRING('Date SMS Sent')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:TimeSMSSent')            !Field Name
                SHORT(20)                             !Default Column Width
                PSTRING('Time')                       !Header
                PSTRING('@t1b')                       !Picture
                PSTRING('Time SMS Sent')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:EmailSent')              !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Email Sent')                 !Header
                PSTRING('@s1')                        !Picture
                PSTRING('Email Sent')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:DateEmailSent')          !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Date Email Sent')            !Header
                PSTRING('@d6b')                       !Picture
                PSTRING('Date Email Sent')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:TimeEmailSent')          !Field Name
                SHORT(20)                             !Default Column Width
                PSTRING('Time')                       !Header
                PSTRING('@t1b')                       !Picture
                PSTRING('Time Email Sent')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:SBUpdated')              !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('SB Upd')                     !Header
                PSTRING('@n1')                        !Picture
                PSTRING('ServiceBase Updated')        !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:MSG')                    !Field Name
                SHORT(1020)                           !Default Column Width
                PSTRING('Message')                    !Header
                PSTRING('@s160')                      !Picture
                PSTRING('Message')                    !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:PathToEstimate')         !Field Name
                SHORT(1020)                           !Default Column Width
                PSTRING('Path To Estimate')           !Header
                PSTRING('@s255')                      !Picture
                PSTRING('Path To Estimate')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sms:RecordNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Record Number')              !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(17)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom)

