

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01003.INC'),ONCE        !Local module procedure declarations
                     END


Update_Job_Exchange_Accessory PROCEDURE               !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::jea:Record  LIKE(jea:RECORD),STATIC
QuickWindow          WINDOW('Update the JOBEXACC File'),AT(,,208,112),FONT('MS Sans Serif',8,,),IMM,HLP('Update_Job_Exchange_Accessory'),SYSTEM,GRAY,NOFRAME
                       SHEET,AT(4,4,200,86),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number:'),AT(8,20),USE(?JEA:Record_Number:Prompt)
                           ENTRY(@n10.2),AT(76,20,44,10),USE(jea:Record_Number),DECIMAL(14)
                           PROMPT('Reference Number'),AT(8,34),USE(?JEA:Job_Ref_Number:Prompt)
                           ENTRY(@p<<<<<<<<#p),AT(76,34,44,10),USE(jea:Job_Ref_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Reference Number'),AT(8,48),USE(?JEA:Stock_Ref_Number:Prompt)
                           ENTRY(@p<<<<<<<<#p),AT(76,48,44,10),USE(jea:Stock_Ref_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Part Number'),AT(8,62),USE(?JEA:Part_Number:Prompt),TRN
                           ENTRY(@s30),AT(76,62,124,10),USE(jea:Part_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Description'),AT(8,76),USE(?JEA:Description:Prompt),TRN
                           ENTRY(@s30),AT(76,76,124,10),USE(jea:Description),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('OK'),AT(110,94,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(159,94,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(159,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Job_Exchange_Accessory')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?JEA:Record_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(jea:Record,History::jea:Record)
  SELF.AddHistoryField(?jea:Record_Number,1)
  SELF.AddHistoryField(?jea:Job_Ref_Number,2)
  SELF.AddHistoryField(?jea:Stock_Ref_Number,3)
  SELF.AddHistoryField(?jea:Part_Number,4)
  SELF.AddHistoryField(?jea:Description,5)
  SELF.AddUpdateFile(Access:JOBEXACC)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBEXACC.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBEXACC
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Update_Job_Exchange_Accessory')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBEXACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Job_Exchange_Accessory')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateSIMNumber PROCEDURE (func:RefNumber)            !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Update SIM Number'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Update SIM Number'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('SIM Number'),USE(?SIMNumberTAB),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(248,208,184,10),USE(jobe:SIMNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('SIM Number'),TIP('SIM Number'),UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?Button2),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020421'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSIMNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBSE.Open
  SELF.FilesOpened = True
  access:JOBSE.clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = func:RefNumber
  If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else! If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
      Assert(0,'<13,10>Fetch Error<13,10>')
  End! If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateSIMNumber')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBSE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateSIMNumber')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020421'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020421'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020421'&'0')
      ***
    OF ?Button2
      ThisWindow.Update
      access:JOBSE.Update()
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateJOBTHIRD PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::jot:Record  LIKE(jot:RECORD),STATIC
QuickWindow          WINDOW('Update the JOBTHIRD File'),AT(,,240,178),FONT('Tahoma',8,,),IMM,HLP('UpdateJOBTHIRD'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,232,152),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?JOT:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(108,20,64,10),USE(jot:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Ref Number'),AT(8,34),USE(?JOT:RefNumber:Prompt),TRN
                           ENTRY(@s8),AT(108,34,64,10),USE(jot:RefNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Link To Job''s File'),TIP('Link To Job''s File'),UPR
                           PROMPT('Original I.M.E.I. No'),AT(8,48),USE(?JOT:OriginalIMEI:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,48,124,10),USE(jot:OriginalIMEI),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Original IMEI Number'),TIP('Original IMEI Number'),UPR
                           PROMPT('Outgoing I.M.E.I. Number'),AT(8,62),USE(?JOT:OutIMEI:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,62,124,10),USE(jot:OutIMEI),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Outgoing I.M.E.I. Number'),TIP('Outgoing I.M.E.I. Number'),UPR
                           PROMPT('Incoming I.M.E.I. No'),AT(8,76),USE(?JOT:InIMEI:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,76,124,10),USE(jot:InIMEI),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Incoming I.M.E.I. Number'),TIP('Incoming I.M.E.I. Number'),UPR
                           PROMPT('Outgoing Date'),AT(8,90),USE(?JOT:DateOut:Prompt)
                           ENTRY(@d6),AT(108,90,64,10),USE(jot:DateOut),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Incoming Date'),AT(8,104),USE(?JOT:DateIn:Prompt)
                           ENTRY(@d6),AT(108,104,64,10),USE(jot:DateIn),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Dummy Field:'),AT(8,118),USE(?JOT:DummyField:Prompt)
                           PROMPT('Link To Third Party Entry'),AT(8,132),USE(?jot:ThirdPartyNumber:Prompt),TRN
                           ENTRY(@s8),AT(108,132,64,10),USE(jot:ThirdPartyNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Link To Third Party Entry'),TIP('Link To Third Party Entry'),UPR
                         END
                       END
                       BUTTON('OK'),AT(136,160,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(188,160,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(191,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateJOBTHIRD')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?JOT:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(jot:Record,History::jot:Record)
  SELF.AddHistoryField(?jot:RecordNumber,1)
  SELF.AddHistoryField(?jot:RefNumber,2)
  SELF.AddHistoryField(?jot:OriginalIMEI,3)
  SELF.AddHistoryField(?jot:OutIMEI,4)
  SELF.AddHistoryField(?jot:InIMEI,5)
  SELF.AddHistoryField(?jot:DateOut,6)
  SELF.AddHistoryField(?jot:DateIn,8)
  SELF.AddHistoryField(?jot:ThirdPartyNumber,9)
  SELF.AddUpdateFile(Access:JOBTHIRD)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBTHIRD.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBTHIRD
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateJOBTHIRD')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBTHIRD.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateJOBTHIRD')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowseJobThird PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBTHIRD)
                       PROJECT(jot:OutIMEI)
                       PROJECT(jot:OutMSN)
                       PROJECT(jot:DateOut)
                       PROJECT(jot:DateDespatched)
                       PROJECT(jot:InIMEI)
                       PROJECT(jot:InMSN)
                       PROJECT(jot:DateIn)
                       PROJECT(jot:RecordNumber)
                       PROJECT(jot:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
jot:OutIMEI            LIKE(jot:OutIMEI)              !List box control field - type derived from field
jot:OutMSN             LIKE(jot:OutMSN)               !List box control field - type derived from field
jot:DateOut            LIKE(jot:DateOut)              !List box control field - type derived from field
jot:DateDespatched     LIKE(jot:DateDespatched)       !List box control field - type derived from field
jot:InIMEI             LIKE(jot:InIMEI)               !List box control field - type derived from field
jot:InMSN              LIKE(jot:InMSN)                !List box control field - type derived from field
jot:DateIn             LIKE(jot:DateIn)               !List box control field - type derived from field
jot:RecordNumber       LIKE(jot:RecordNumber)         !Primary key field - type derived from field
jot:RefNumber          LIKE(jot:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Third Party Transactions'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Third Party Transaction File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON('&Select'),AT(532,162,76,20),USE(?Select:2),LEFT,ICON('select.ico')
                       BUTTON('&Insert'),AT(532,230,76,20),USE(?Insert:3),HIDE,LEFT,ICON('insert.ico')
                       BUTTON('&Change'),AT(532,254,76,20),USE(?Change:3),HIDE,LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(532,278,76,20),USE(?Delete:3),HIDE,LEFT,ICON('delete.ico')
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('By Ref Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(128,118,424,184),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('71L(2)|M~Outgoing I.M.E.I. No~@s16@68L(2)|M~Outgoing M.S.N.~@s16@40R(2)|M~Date~@' &|
   'd6@40R(2)|M~Date Desp~@d6@68L(2)|M~Incoming I.M.E.I. No~@s16@68L(2)|M~Incoming M' &|
   '.S.N.~@s16@80R(2)|M~Incoming Date~@d6@'),FROM(Queue:Browse:1)
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020399'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseJobThird')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBTHIRD,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
?   Unhide(?Insert:3)
?   Unhide(?Change:3)
?   Unhide(?Delete:3)
  ! Save Window Name
   AddToLog('Window','Open','BrowseJobThird')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,jot:OutDateKey)
  BRW1.AddRange(jot:RefNumber,Relate:JOBTHIRD,Relate:JOBS)
  BRW1.AddField(jot:OutIMEI,BRW1.Q.jot:OutIMEI)
  BRW1.AddField(jot:OutMSN,BRW1.Q.jot:OutMSN)
  BRW1.AddField(jot:DateOut,BRW1.Q.jot:DateOut)
  BRW1.AddField(jot:DateDespatched,BRW1.Q.jot:DateDespatched)
  BRW1.AddField(jot:InIMEI,BRW1.Q.jot:InIMEI)
  BRW1.AddField(jot:InMSN,BRW1.Q.jot:InMSN)
  BRW1.AddField(jot:DateIn,BRW1.Q.jot:DateIn)
  BRW1.AddField(jot:RecordNumber,BRW1.Q.jot:RecordNumber)
  BRW1.AddField(jot:RefNumber,BRW1.Q.jot:RefNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseJobThird')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  DoUpdate# = 0
  If DoUpdate# = 0 And Request = Insertrecord
      Access:JOBTHIRD.Cancelautoinc()
  End!If DoUpdate# = 0 And Request = Insertrecord
? DoUpdate# = 1
  If DoUpdate# = 1
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJOBTHIRD
    ReturnValue = GlobalResponse
  END
  End!If DoUpdate# = 1
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020399'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020399'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020399'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Fault_Description_Text PROCEDURE                      !Generated from procedure template - Window

FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Fault Description'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Fault Description'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           TEXT,AT(232,162,216,82),USE(jbn:Fault_Description),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       STRING(@s1),AT(180,338),USE(glo:Preview),FONT(,16,COLOR:White,,CHARSET:ANSI)
                       BUTTON('Button 4'),AT(236,338,56,16),USE(?Button4)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Changing Fault Description'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020415'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Fault_Description_Text')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:JOBNOTES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBNOTES.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBNOTES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      !TB13265 - J - 27/03/14 - don't allow them to change the Fault Description if in view only mode
      if glo:Preview = 'V' then ?OK{prop:disable} = true.
  ! Save Window Name
   AddToLog('Window','Open','Fault_Description_Text')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Fault_Description_Text')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020415'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020415'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020415'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Do_Not_Delete8 PROCEDURE                              !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Do_Not_Delete8')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXPCITY.Open
  Relate:EXPGEN.Open
  Relate:EXPGENDM.Open
  Relate:EXPLABG.Open
  Relate:PARAMSS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Do_Not_Delete8')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXPCITY.Close
    Relate:EXPGEN.Close
    Relate:EXPGENDM.Close
    Relate:EXPLABG.Close
    Relate:PARAMSS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Do_Not_Delete8')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Compare_Fault_Codes PROCEDURE (f_ref_number,f_alias_ref_number) !Generated from procedure template - Window

FilesOpened          BYTE
save_maf_id          USHORT
tmp:Field            STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Compare Fault Codes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Compare Fault Codes'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Current Job Number'),AT(68,58),USE(?JOB:Ref_Number:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(160,58,64,10),USE(job:Ref_Number),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),REQ,UPR,READONLY
                           LINE,AT(338,72,0,231),USE(?Line1),COLOR(COLOR:White)
                           PROMPT('Historic Job Number'),AT(368,58),USE(?JOB:Ref_Number:Prompt:2),TRN,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(456,58,64,10),USE(job_ali:Ref_Number),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),REQ,UPR,READONLY
                           PROMPT('Fault Code 1:'),AT(68,74),USE(?JOB:Fault_Code1:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,74,124,10),USE(job:Fault_Code1),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,70),USE(?Lookup1),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 1:'),AT(368,74),USE(?JOB_ALI:Fault_Code1:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,74,124,10),USE(job_ali:Fault_Code1),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,70),USE(?Lookup1a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(456,74,124,10),USE(job_ali:Fault_Code1,,?JOB_ALI:Fault_Code1:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@d6b),AT(160,74,124,10),USE(job:Fault_Code1,,?JOB:Fault_Code1:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 2:'),AT(68,94),USE(?JOB:Fault_Code2:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,94,124,10),USE(job:Fault_Code2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,90),USE(?Lookup2),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 2:'),AT(368,94),USE(?JOB_ALI:Fault_Code2:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,94,124,10),USE(job_ali:Fault_Code2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,90),USE(?Lookup2a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(456,94,124,10),USE(job_ali:Fault_Code2,,?JOB_ALI:Fault_Code2:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@d6b),AT(160,94,124,10),USE(job:Fault_Code2,,?JOB:Fault_Code2:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 3:'),AT(68,114),USE(?JOB:Fault_Code3:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,114,124,10),USE(job:Fault_Code3),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,110),USE(?Lookup3),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 3:'),AT(368,114),USE(?JOB_ALI:Fault_Code3:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,114,124,10),USE(job_ali:Fault_Code3),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,110),USE(?Lookup3a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(456,114,124,10),USE(job_ali:Fault_Code3,,?JOB_ALI:Fault_Code3:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@d6b),AT(160,114,124,10),USE(job:Fault_Code3,,?JOB:Fault_Code3:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 4:'),AT(68,134),USE(?JOB:Fault_Code4:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,134,124,10),USE(job:Fault_Code4),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,130),USE(?Lookup4),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 4:'),AT(368,134),USE(?JOB_ALI:Fault_Code4:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,134,124,10),USE(job_ali:Fault_Code4),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,130),USE(?Lookup4a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(160,134,124,10),USE(job:Fault_Code4,,?JOB:Fault_Code4:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 5:'),AT(68,154),USE(?JOB:Fault_Code5:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,154,124,10),USE(job:Fault_Code5),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,150),USE(?Lookup5),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(160,154,124,10),USE(job:Fault_Code5,,?JOB:Fault_Code5:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 6:'),AT(68,174),USE(?JOB:Fault_Code6:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,174,124,10),USE(job:Fault_Code6),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,170),USE(?Lookup6),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 5:'),AT(368,154),USE(?JOB_ALI:Fault_Code5:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,154,124,10),USE(job_ali:Fault_Code5),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,150),USE(?Lookup5a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(456,154,124,10),USE(job_ali:Fault_Code5,,?JOB_ALI:Fault_Code5:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@d6b),AT(456,134,124,10),USE(job_ali:Fault_Code4,,?JOB_ALI:Fault_Code4:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@d6b),AT(160,174,124,10),USE(job:Fault_Code6,,?JOB:Fault_Code6:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 7:'),AT(68,194),USE(?JOB:Fault_Code7:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,194,124,10),USE(job:Fault_Code7),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,190),USE(?Lookup7),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 6:'),AT(368,174),USE(?JOB_ALI:Fault_Code6:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,174,124,10),USE(job_ali:Fault_Code6),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,170),USE(?Lookup6a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(456,174,124,10),USE(job_ali:Fault_Code6,,?JOB_ALI:Fault_Code6:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 7:'),AT(368,194),USE(?JOB_ALI:Fault_Code7:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,194,124,10),USE(job_ali:Fault_Code7),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,190),USE(?Lookup7a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(456,194,124,10),USE(job_ali:Fault_Code7,,?JOB_ALI:Fault_Code7:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@d6b),AT(160,194,124,10),USE(job:Fault_Code7,,?JOB:Fault_Code7:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 8:'),AT(68,214),USE(?JOB:Fault_Code8:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,214,124,10),USE(job:Fault_Code8),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,210),USE(?Lookup8),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 8:'),AT(368,214),USE(?JOB_ALI:Fault_Code8:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,214,124,10),USE(job_ali:Fault_Code8),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,210),USE(?Lookup8a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(160,214,124,10),USE(job:Fault_Code8,,?JOB:Fault_Code8:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 9:'),AT(68,234),USE(?JOB:Fault_Code9:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,234,124,10),USE(job:Fault_Code9),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,230),USE(?Lookup9),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 9:'),AT(368,234),USE(?JOB_ALI:Fault_Code9:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,234,124,10),USE(job_ali:Fault_Code9),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,230),USE(?Lookup9a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(456,214,124,10),USE(job_ali:Fault_Code8,,?JOB_ALI:Fault_Code8:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@d6b),AT(456,234,124,10),USE(job_ali:Fault_Code9,,?JOB_ALI:Fault_Code9:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@d6b),AT(160,234,124,10),USE(job:Fault_Code9,,?JOB:Fault_Code9:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 10:'),AT(68,254),USE(?JOB:Fault_Code10:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(160,254,124,10),USE(job:Fault_Code10),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,250),USE(?Lookup10),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 10:'),AT(368,254),USE(?JOB_ALI:Fault_Code10:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(456,254,124,10),USE(job_ali:Fault_Code10),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,250),USE(?Lookup10a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(456,254,124,10),USE(job_ali:Fault_Code10,,?JOB_ALI:Fault_Code10:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@d6b),AT(160,254,124,10),USE(job:Fault_Code10,,?JOB:Fault_Code10:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 11:'),AT(68,274),USE(?JOB:Fault_Code11:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(160,274,124,10),USE(job:Fault_Code11),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,270),USE(?Lookup11),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 11:'),AT(368,274),USE(?JOB_ALI:Fault_Code11:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(456,274,124,10),USE(job_ali:Fault_Code11),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,270),USE(?Lookup11a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(456,274,124,10),USE(job_ali:Fault_Code11,,?JOB_ALI:Fault_Code11:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@d6b),AT(160,274,124,10),USE(job:Fault_Code11,,?JOB:Fault_Code11:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 12:'),AT(68,294),USE(?JOB:Fault_Code12:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(160,294,124,10),USE(job:Fault_Code12),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(288,290),USE(?Lookup12),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 12:'),AT(368,294),USE(?JOB_ALI:Fault_Code12:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(456,294,124,10),USE(job_ali:Fault_Code12),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(584,290),USE(?Lookup12a),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(456,294,124,10),USE(job_ali:Fault_Code12,,?JOB_ALI:Fault_Code12:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@d6b),AT(160,294,124,10),USE(job:Fault_Code12,,?JOB:Fault_Code12:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                         END
                       END
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,ICON('closep.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:job:Fault_Code1                Like(job:Fault_Code1)
look:job_ali:Fault_Code1                Like(job_ali:Fault_Code1)
look:job:Fault_Code2                Like(job:Fault_Code2)
look:job_ali:Fault_Code2                Like(job_ali:Fault_Code2)
look:job:Fault_Code3                Like(job:Fault_Code3)
look:job_ali:Fault_Code3                Like(job_ali:Fault_Code3)
look:job:Fault_Code4                Like(job:Fault_Code4)
look:job_ali:Fault_Code4                Like(job_ali:Fault_Code4)
look:job:Fault_Code5                Like(job:Fault_Code5)
look:job:Fault_Code6                Like(job:Fault_Code6)
look:job_ali:Fault_Code5                Like(job_ali:Fault_Code5)
look:job:Fault_Code7                Like(job:Fault_Code7)
look:job_ali:Fault_Code6                Like(job_ali:Fault_Code6)
look:job_ali:Fault_Code7                Like(job_ali:Fault_Code7)
look:job:Fault_Code8                Like(job:Fault_Code8)
look:job_ali:Fault_Code8                Like(job_ali:Fault_Code8)
look:job:Fault_Code9                Like(job:Fault_Code9)
look:job_ali:Fault_Code9                Like(job_ali:Fault_Code9)
look:job:Fault_Code10                Like(job:Fault_Code10)
look:job_ali:Fault_Code10                Like(job_ali:Fault_Code10)
look:job:Fault_Code11                Like(job:Fault_Code11)
look:job_ali:Fault_Code11                Like(job_ali:Fault_Code11)
look:job:Fault_Code12                Like(job:Fault_Code12)
look:job_ali:Fault_Code12                Like(job_ali:Fault_Code12)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020397'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Compare_Fault_Codes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBS.Open
  Access:JOBS_ALIAS.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Compare_Fault_Codes')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?job:Fault_Code3{Prop:Tip} AND ~?Lookup3{Prop:Tip}
     ?Lookup3{Prop:Tip} = 'Select ' & ?job:Fault_Code3{Prop:Tip}
  END
  IF ?job:Fault_Code3{Prop:Msg} AND ~?Lookup3{Prop:Msg}
     ?Lookup3{Prop:Msg} = 'Select ' & ?job:Fault_Code3{Prop:Msg}
  END
  IF ?job:Fault_Code2{Prop:Tip} AND ~?Lookup2{Prop:Tip}
     ?Lookup2{Prop:Tip} = 'Select ' & ?job:Fault_Code2{Prop:Tip}
  END
  IF ?job:Fault_Code2{Prop:Msg} AND ~?Lookup2{Prop:Msg}
     ?Lookup2{Prop:Msg} = 'Select ' & ?job:Fault_Code2{Prop:Msg}
  END
  IF ?job:Fault_Code1{Prop:Tip} AND ~?Lookup1{Prop:Tip}
     ?Lookup1{Prop:Tip} = 'Select ' & ?job:Fault_Code1{Prop:Tip}
  END
  IF ?job:Fault_Code1{Prop:Msg} AND ~?Lookup1{Prop:Msg}
     ?Lookup1{Prop:Msg} = 'Select ' & ?job:Fault_Code1{Prop:Msg}
  END
  IF ?job:Fault_Code5{Prop:Tip} AND ~?Lookup5{Prop:Tip}
     ?Lookup5{Prop:Tip} = 'Select ' & ?job:Fault_Code5{Prop:Tip}
  END
  IF ?job:Fault_Code5{Prop:Msg} AND ~?Lookup5{Prop:Msg}
     ?Lookup5{Prop:Msg} = 'Select ' & ?job:Fault_Code5{Prop:Msg}
  END
  IF ?job:Fault_Code6{Prop:Tip} AND ~?Lookup6{Prop:Tip}
     ?Lookup6{Prop:Tip} = 'Select ' & ?job:Fault_Code6{Prop:Tip}
  END
  IF ?job:Fault_Code6{Prop:Msg} AND ~?Lookup6{Prop:Msg}
     ?Lookup6{Prop:Msg} = 'Select ' & ?job:Fault_Code6{Prop:Msg}
  END
  IF ?job:Fault_Code7{Prop:Tip} AND ~?Lookup7{Prop:Tip}
     ?Lookup7{Prop:Tip} = 'Select ' & ?job:Fault_Code7{Prop:Tip}
  END
  IF ?job:Fault_Code7{Prop:Msg} AND ~?Lookup7{Prop:Msg}
     ?Lookup7{Prop:Msg} = 'Select ' & ?job:Fault_Code7{Prop:Msg}
  END
  IF ?job:Fault_Code8{Prop:Tip} AND ~?Lookup8{Prop:Tip}
     ?Lookup8{Prop:Tip} = 'Select ' & ?job:Fault_Code8{Prop:Tip}
  END
  IF ?job:Fault_Code8{Prop:Msg} AND ~?Lookup8{Prop:Msg}
     ?Lookup8{Prop:Msg} = 'Select ' & ?job:Fault_Code8{Prop:Msg}
  END
  IF ?job:Fault_Code9{Prop:Tip} AND ~?Lookup9{Prop:Tip}
     ?Lookup9{Prop:Tip} = 'Select ' & ?job:Fault_Code9{Prop:Tip}
  END
  IF ?job:Fault_Code9{Prop:Msg} AND ~?Lookup9{Prop:Msg}
     ?Lookup9{Prop:Msg} = 'Select ' & ?job:Fault_Code9{Prop:Msg}
  END
  IF ?job:Fault_Code10{Prop:Tip} AND ~?Lookup10{Prop:Tip}
     ?Lookup10{Prop:Tip} = 'Select ' & ?job:Fault_Code10{Prop:Tip}
  END
  IF ?job:Fault_Code10{Prop:Msg} AND ~?Lookup10{Prop:Msg}
     ?Lookup10{Prop:Msg} = 'Select ' & ?job:Fault_Code10{Prop:Msg}
  END
  IF ?job:Fault_Code11{Prop:Tip} AND ~?Lookup11{Prop:Tip}
     ?Lookup11{Prop:Tip} = 'Select ' & ?job:Fault_Code11{Prop:Tip}
  END
  IF ?job:Fault_Code11{Prop:Msg} AND ~?Lookup11{Prop:Msg}
     ?Lookup11{Prop:Msg} = 'Select ' & ?job:Fault_Code11{Prop:Msg}
  END
  IF ?job:Fault_Code12{Prop:Tip} AND ~?Lookup12{Prop:Tip}
     ?Lookup12{Prop:Tip} = 'Select ' & ?job:Fault_Code12{Prop:Tip}
  END
  IF ?job:Fault_Code12{Prop:Msg} AND ~?Lookup12{Prop:Msg}
     ?Lookup12{Prop:Msg} = 'Select ' & ?job:Fault_Code12{Prop:Msg}
  END
  IF ?job:Fault_Code4{Prop:Tip} AND ~?Lookup4{Prop:Tip}
     ?Lookup4{Prop:Tip} = 'Select ' & ?job:Fault_Code4{Prop:Tip}
  END
  IF ?job:Fault_Code4{Prop:Msg} AND ~?Lookup4{Prop:Msg}
     ?Lookup4{Prop:Msg} = 'Select ' & ?job:Fault_Code4{Prop:Msg}
  END
  IF ?job_ali:Fault_Code1{Prop:Tip} AND ~?Lookup1a{Prop:Tip}
     ?Lookup1a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code1{Prop:Tip}
  END
  IF ?job_ali:Fault_Code1{Prop:Msg} AND ~?Lookup1a{Prop:Msg}
     ?Lookup1a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code1{Prop:Msg}
  END
  IF ?job_ali:Fault_Code2{Prop:Tip} AND ~?Lookup2a{Prop:Tip}
     ?Lookup2a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code2{Prop:Tip}
  END
  IF ?job_ali:Fault_Code2{Prop:Msg} AND ~?Lookup2a{Prop:Msg}
     ?Lookup2a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code2{Prop:Msg}
  END
  IF ?job_ali:Fault_Code3{Prop:Tip} AND ~?Lookup3a{Prop:Tip}
     ?Lookup3a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code3{Prop:Tip}
  END
  IF ?job_ali:Fault_Code3{Prop:Msg} AND ~?Lookup3a{Prop:Msg}
     ?Lookup3a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code3{Prop:Msg}
  END
  IF ?job_ali:Fault_Code4{Prop:Tip} AND ~?Lookup4a{Prop:Tip}
     ?Lookup4a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code4{Prop:Tip}
  END
  IF ?job_ali:Fault_Code4{Prop:Msg} AND ~?Lookup4a{Prop:Msg}
     ?Lookup4a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code4{Prop:Msg}
  END
  IF ?job_ali:Fault_Code5{Prop:Tip} AND ~?Lookup5a{Prop:Tip}
     ?Lookup5a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code5{Prop:Tip}
  END
  IF ?job_ali:Fault_Code5{Prop:Msg} AND ~?Lookup5a{Prop:Msg}
     ?Lookup5a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code5{Prop:Msg}
  END
  IF ?job_ali:Fault_Code6{Prop:Tip} AND ~?Lookup6a{Prop:Tip}
     ?Lookup6a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code6{Prop:Tip}
  END
  IF ?job_ali:Fault_Code6{Prop:Msg} AND ~?Lookup6a{Prop:Msg}
     ?Lookup6a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code6{Prop:Msg}
  END
  IF ?job_ali:Fault_Code7{Prop:Tip} AND ~?Lookup7a{Prop:Tip}
     ?Lookup7a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code7{Prop:Tip}
  END
  IF ?job_ali:Fault_Code7{Prop:Msg} AND ~?Lookup7a{Prop:Msg}
     ?Lookup7a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code7{Prop:Msg}
  END
  IF ?job_ali:Fault_Code8{Prop:Tip} AND ~?Lookup8a{Prop:Tip}
     ?Lookup8a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code8{Prop:Tip}
  END
  IF ?job_ali:Fault_Code8{Prop:Msg} AND ~?Lookup8a{Prop:Msg}
     ?Lookup8a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code8{Prop:Msg}
  END
  IF ?job_ali:Fault_Code9{Prop:Tip} AND ~?Lookup9a{Prop:Tip}
     ?Lookup9a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code9{Prop:Tip}
  END
  IF ?job_ali:Fault_Code9{Prop:Msg} AND ~?Lookup9a{Prop:Msg}
     ?Lookup9a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code9{Prop:Msg}
  END
  IF ?job_ali:Fault_Code10{Prop:Tip} AND ~?Lookup10a{Prop:Tip}
     ?Lookup10a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code10{Prop:Tip}
  END
  IF ?job_ali:Fault_Code10{Prop:Msg} AND ~?Lookup10a{Prop:Msg}
     ?Lookup10a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code10{Prop:Msg}
  END
  IF ?job_ali:Fault_Code11{Prop:Tip} AND ~?Lookup11a{Prop:Tip}
     ?Lookup11a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code11{Prop:Tip}
  END
  IF ?job_ali:Fault_Code11{Prop:Msg} AND ~?Lookup11a{Prop:Msg}
     ?Lookup11a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code11{Prop:Msg}
  END
  IF ?job_ali:Fault_Code12{Prop:Tip} AND ~?Lookup12a{Prop:Tip}
     ?Lookup12a{Prop:Tip} = 'Select ' & ?job_ali:Fault_Code12{Prop:Tip}
  END
  IF ?job_ali:Fault_Code12{Prop:Msg} AND ~?Lookup12a{Prop:Msg}
     ?Lookup12a{Prop:Msg} = 'Select ' & ?job_ali:Fault_Code12{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Compare_Fault_Codes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
      Browse_Manufacturer_Fault_Lookup
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020397'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020397'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020397'&'0')
      ***
    OF ?job:Fault_Code1
      IF job:Fault_Code1 OR ?job:Fault_Code1{Prop:Req}
        mfo:Field = job:Fault_Code1
        !Save Lookup Field Incase Of error
        look:job:Fault_Code1        = job:Fault_Code1
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code1 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job:Fault_Code1 = look:job:Fault_Code1
            SELECT(?job:Fault_Code1)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup1
      ThisWindow.Update
      mfo:Field = job:Fault_Code1
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 1
      GLO:Select2 = 1
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code1 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code1)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code1)
    OF ?job_ali:Fault_Code1
      IF job_ali:Fault_Code1 OR ?job_ali:Fault_Code1{Prop:Req}
        mfo:Field = job_ali:Fault_Code1
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code1        = job_ali:Fault_Code1
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code1 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code1 = look:job_ali:Fault_Code1
            SELECT(?job_ali:Fault_Code1)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup1a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code1
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 1
      GLO:Select2 = 1
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code1 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code1)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code1)
    OF ?job:Fault_Code2
      IF job:Fault_Code2 OR ?job:Fault_Code2{Prop:Req}
        mfo:Field = job:Fault_Code2
        mfo:Manufacturer = glo:Select1
        mfo:Field_Number = 2
        !Save Lookup Field Incase Of error
        look:job:Fault_Code2        = job:Fault_Code2
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code2 = mfo:Field
          ELSE
            CLEAR(mfo:Manufacturer)
            CLEAR(mfo:Field_Number)
            !Restore Lookup On Error
            job:Fault_Code2 = look:job:Fault_Code2
            SELECT(?job:Fault_Code2)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup2
      ThisWindow.Update
      mfo:Field = job:Fault_Code2
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 2
      GLO:Select2 = 2
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code2 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code2)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code2)
    OF ?job_ali:Fault_Code2
      IF job_ali:Fault_Code2 OR ?job_ali:Fault_Code2{Prop:Req}
        mfo:Field = job_ali:Fault_Code2
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code2        = job_ali:Fault_Code2
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code2 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code2 = look:job_ali:Fault_Code2
            SELECT(?job_ali:Fault_Code2)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup2a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code2
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 2
      GLO:Select2 = 2
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code2 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code2)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code2)
    OF ?job:Fault_Code3
      IF job:Fault_Code3 OR ?job:Fault_Code3{Prop:Req}
        mfo:Field = job:Fault_Code3
        !Save Lookup Field Incase Of error
        look:job:Fault_Code3        = job:Fault_Code3
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code3 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job:Fault_Code3 = look:job:Fault_Code3
            SELECT(?job:Fault_Code3)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup3
      ThisWindow.Update
      mfo:Field = job:Fault_Code3
      mfo:Manufacturer = glo:Select1
      mfo:Field = 3
      GLO:Select2 = 3
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code3 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code3)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code3)
    OF ?job_ali:Fault_Code3
      IF job_ali:Fault_Code3 OR ?job_ali:Fault_Code3{Prop:Req}
        mfo:Field = job_ali:Fault_Code3
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code3        = job_ali:Fault_Code3
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code3 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code3 = look:job_ali:Fault_Code3
            SELECT(?job_ali:Fault_Code3)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup3a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code3
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 3
      GLO:Select2 = 3
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code3 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code3)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code3)
    OF ?job:Fault_Code4
      IF job:Fault_Code4 OR ?job:Fault_Code4{Prop:Req}
        mfo:Field = job:Fault_Code4
        !Save Lookup Field Incase Of error
        look:job:Fault_Code4        = job:Fault_Code4
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code4 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job:Fault_Code4 = look:job:Fault_Code4
            SELECT(?job:Fault_Code4)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup4
      ThisWindow.Update
      mfo:Field = job:Fault_Code4
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 4
      GLO:Select2 = 4
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code4 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code4)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code4)
    OF ?job_ali:Fault_Code4
      IF job_ali:Fault_Code4 OR ?job_ali:Fault_Code4{Prop:Req}
        mfo:Field = job_ali:Fault_Code4
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code4        = job_ali:Fault_Code4
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code4 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code4 = look:job_ali:Fault_Code4
            SELECT(?job_ali:Fault_Code4)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup4a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code4
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 4
      GLO:Select2 = 4
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code4 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code4)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code4)
    OF ?job:Fault_Code5
      IF job:Fault_Code5 OR ?job:Fault_Code5{Prop:Req}
        mfo:Field = job:Fault_Code5
        !Save Lookup Field Incase Of error
        look:job:Fault_Code5        = job:Fault_Code5
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code5 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job:Fault_Code5 = look:job:Fault_Code5
            SELECT(?job:Fault_Code5)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup5
      ThisWindow.Update
      mfo:Field = job:Fault_Code5
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 5
      GLO:Select2 = 5
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code5 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code5)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code5)
    OF ?job:Fault_Code6
      IF job:Fault_Code6 OR ?job:Fault_Code6{Prop:Req}
        mfo:Field = job:Fault_Code6
        !Save Lookup Field Incase Of error
        look:job:Fault_Code6        = job:Fault_Code6
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code6 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job:Fault_Code6 = look:job:Fault_Code6
            SELECT(?job:Fault_Code6)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup6
      ThisWindow.Update
      mfo:Field = job:Fault_Code6
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 6
      GLO:Select2 = 6
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code6 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code6)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code6)
    OF ?job_ali:Fault_Code5
      IF job_ali:Fault_Code5 OR ?job_ali:Fault_Code5{Prop:Req}
        mfo:Field = job_ali:Fault_Code5
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code5        = job_ali:Fault_Code5
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code5 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code5 = look:job_ali:Fault_Code5
            SELECT(?job_ali:Fault_Code5)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup5a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code5
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 5
      GLO:Select2 = 5
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code5 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code5)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code5)
    OF ?job:Fault_Code7
      IF job:Fault_Code7 OR ?job:Fault_Code7{Prop:Req}
        mfo:Field = job:Fault_Code7
        !Save Lookup Field Incase Of error
        look:job:Fault_Code7        = job:Fault_Code7
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code7 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job:Fault_Code7 = look:job:Fault_Code7
            SELECT(?job:Fault_Code7)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup7
      ThisWindow.Update
      mfo:Field = job:Fault_Code7
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 7
      GLO:Select2 = 7
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code7 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code7)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code7)
    OF ?job_ali:Fault_Code6
      IF job_ali:Fault_Code6 OR ?job_ali:Fault_Code6{Prop:Req}
        mfo:Field = job_ali:Fault_Code6
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code6        = job_ali:Fault_Code6
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code6 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code6 = look:job_ali:Fault_Code6
            SELECT(?job_ali:Fault_Code6)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup6a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code6
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 6
      GLO:Select2 = 6
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code6 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code6)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code6)
    OF ?job_ali:Fault_Code7
      IF job_ali:Fault_Code7 OR ?job_ali:Fault_Code7{Prop:Req}
        mfo:Field = job_ali:Fault_Code7
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code7        = job_ali:Fault_Code7
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code7 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code7 = look:job_ali:Fault_Code7
            SELECT(?job_ali:Fault_Code7)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup7a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code7
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 7
      GLO:Select2 = 7
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code7 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code7)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code7)
    OF ?job:Fault_Code8
      IF job:Fault_Code8 OR ?job:Fault_Code8{Prop:Req}
        mfo:Field = job:Fault_Code8
        !Save Lookup Field Incase Of error
        look:job:Fault_Code8        = job:Fault_Code8
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code8 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job:Fault_Code8 = look:job:Fault_Code8
            SELECT(?job:Fault_Code8)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup8
      ThisWindow.Update
      mfo:Field = job:Fault_Code8
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 8
      GLO:Select2 = 8
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code8 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code8)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code8)
    OF ?job_ali:Fault_Code8
      IF job_ali:Fault_Code8 OR ?job_ali:Fault_Code8{Prop:Req}
        mfo:Field = job_ali:Fault_Code8
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code8        = job_ali:Fault_Code8
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code8 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code8 = look:job_ali:Fault_Code8
            SELECT(?job_ali:Fault_Code8)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup8a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code8
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 8
      GLO:Select2 = 8
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code8 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code8)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code8)
    OF ?job:Fault_Code9
      IF job:Fault_Code9 OR ?job:Fault_Code9{Prop:Req}
        mfo:Field = job:Fault_Code9
        !Save Lookup Field Incase Of error
        look:job:Fault_Code9        = job:Fault_Code9
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code9 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job:Fault_Code9 = look:job:Fault_Code9
            SELECT(?job:Fault_Code9)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup9
      ThisWindow.Update
      mfo:Field = job:Fault_Code9
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 9
      GLO:Select2 = 9
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code9 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code9)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code9)
    OF ?job_ali:Fault_Code9
      IF job_ali:Fault_Code9 OR ?job_ali:Fault_Code9{Prop:Req}
        mfo:Field = job_ali:Fault_Code9
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code9        = job_ali:Fault_Code9
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code9 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code9 = look:job_ali:Fault_Code9
            SELECT(?job_ali:Fault_Code9)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup9a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code9
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 9
      GLO:Select2 = 9
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code9 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code9)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code9)
    OF ?job:Fault_Code10
      IF job:Fault_Code10 OR ?job:Fault_Code10{Prop:Req}
        mfo:Field = job:Fault_Code10
        !Save Lookup Field Incase Of error
        look:job:Fault_Code10        = job:Fault_Code10
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code10 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job:Fault_Code10 = look:job:Fault_Code10
            SELECT(?job:Fault_Code10)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup10
      ThisWindow.Update
      mfo:Field = job:Fault_Code10
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 10
      GLO:Select2 = 10
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code10 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code10)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code10)
    OF ?job_ali:Fault_Code10
      IF job_ali:Fault_Code10 OR ?job_ali:Fault_Code10{Prop:Req}
        mfo:Field = job_ali:Fault_Code10
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code10        = job_ali:Fault_Code10
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code10 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code10 = look:job_ali:Fault_Code10
            SELECT(?job_ali:Fault_Code10)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup10a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code10
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 10
      GLO:Select2 = 10
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code10 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code10)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code10)
    OF ?job:Fault_Code11
      IF job:Fault_Code11 OR ?job:Fault_Code11{Prop:Req}
        mfo:Field = job:Fault_Code11
        !Save Lookup Field Incase Of error
        look:job:Fault_Code11        = job:Fault_Code11
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code11 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job:Fault_Code11 = look:job:Fault_Code11
            SELECT(?job:Fault_Code11)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup11
      ThisWindow.Update
      mfo:Field = job:Fault_Code11
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 11
      GLO:Select2 = 11
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code11 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code11)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code11)
    OF ?job_ali:Fault_Code11
      IF job_ali:Fault_Code11 OR ?job_ali:Fault_Code11{Prop:Req}
        mfo:Field = job_ali:Fault_Code11
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code11        = job_ali:Fault_Code11
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code11 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code11 = look:job_ali:Fault_Code11
            SELECT(?job_ali:Fault_Code11)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup11a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code11
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 11
      GLO:Select2 = 11
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code11 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code11)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code11)
    OF ?job:Fault_Code12
      IF job:Fault_Code12 OR ?job:Fault_Code12{Prop:Req}
        mfo:Field = job:Fault_Code12
        !Save Lookup Field Incase Of error
        look:job:Fault_Code12        = job:Fault_Code12
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Fault_Code12 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job:Fault_Code12 = look:job:Fault_Code12
            SELECT(?job:Fault_Code12)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup12
      ThisWindow.Update
      mfo:Field = job:Fault_Code12
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 12
      GLO:Select2 = 12
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Fault_Code12 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job:Fault_Code12)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Fault_Code12)
    OF ?job_ali:Fault_Code12
      IF job_ali:Fault_Code12 OR ?job_ali:Fault_Code12{Prop:Req}
        mfo:Field = job_ali:Fault_Code12
        !Save Lookup Field Incase Of error
        look:job_ali:Fault_Code12        = job_ali:Fault_Code12
        IF Access:MANFAULO.TryFetch(mfo:Field_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job_ali:Fault_Code12 = mfo:Field
          ELSE
            !Restore Lookup On Error
            job_ali:Fault_Code12 = look:job_ali:Fault_Code12
            SELECT(?job_ali:Fault_Code12)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup12a
      ThisWindow.Update
      mfo:Field = job_ali:Fault_Code12
      mfo:Manufacturer = glo:Select1
      mfo:Field_Number = 12
      GLO:Select2 = 12
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job_ali:Fault_Code12 = mfo:Field
          Select(?+1)
      ELSE
          Select(?job_ali:Fault_Code12)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job_ali:Fault_Code12)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Job Fault Codes
      glo:Select1 = job:Manufacturer
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = f_ref_number
      If access:jobs.fetch(job:ref_number_key) = Level:Benign
      
          setcursor(cursor:wait)
          save_maf_id = access:manfault.savefile()
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          set(maf:field_number_key,maf:field_number_key)
          loop
              if access:manfault.next()
                 break
              end !if
              if maf:manufacturer <> job:manufacturer      |
                  then break.  ! end if
              Case maf:field_number
                  Of 1
                      Unhide(?job:fault_code1:prompt)
                      ?job:fault_code1:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code1:2)
      !                    ?job:fault_code1:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code1)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup1{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
                  Of 2
                      Unhide(?job:fault_code2:prompt)
                      ?job:fault_code2:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code2:2)
      !                    ?job:fault_code2:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code2)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup2{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
      
                  Of 3
                      Unhide(?job:fault_code3:prompt)
                      ?job:fault_code3:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code3:2)
      !                    ?job:fault_code3:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code3)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup3{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
      
                  Of 4
                      Unhide(?job:fault_code4:prompt)
                      ?job:fault_code4:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code4:2)
      !                    ?job:fault_code4:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code4)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup4{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
      
                  Of 5
                      Unhide(?job:fault_code5:prompt)
                      ?job:fault_code5:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code5:2)
      !                    ?job:fault_code5:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code5)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup5{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
      
                  Of 6
                      Unhide(?job:fault_code6:prompt)
                      ?job:fault_code6:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code6:2)
      !                    ?job:fault_code6:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code6)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup6{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
      
                  Of 7
                      Unhide(?job:fault_code7:prompt)
                      ?job:fault_code7:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code7:2)
      !                    ?job:fault_code7:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code7)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup7{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
      
                  Of 8
                      Unhide(?job:fault_code8:prompt)
                      ?job:fault_code8:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code8:2)
      !                    ?job:fault_code8:2{prop:xpos} = 84
                      Else
      
                          Unhide(?job:fault_code8)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup8{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
      
                  Of 9
                      Unhide(?job:fault_code9:prompt)
                      ?job:fault_code9:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code9:2)
      !                    ?job:fault_code9:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code9)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup9{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
      
                  Of 10
                      Unhide(?job:fault_code10:prompt)
                      ?job:fault_code10:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code10:2)
      !                    ?job:fault_code10:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code10)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup10{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
      
                  Of 11
                      Unhide(?job:fault_code11:prompt)
                      ?job:fault_code11:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code11:2)
      !                    ?job:fault_code11:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code11)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup11{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
      
                  Of 12
                      Unhide(?job:fault_code12:prompt)
                      ?job:fault_code12:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code12:2)
      !                    ?job:fault_code12:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code12)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup12{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
              End
          end !loop
          access:manfault.restorefile(save_maf_id)
          setcursor()
      End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      !Job Alias Fault Codes
      access:jobs_alias.clearkey(job_ali:ref_number_key)
      job_ali:ref_number  = f_alias_ref_number
      If access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
      
          setcursor(cursor:wait)
          save_maf_id = access:manfault.savefile()
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job_ali:manufacturer
          set(maf:field_number_key,maf:field_number_key)
          loop
              if access:manfault.next()
                 break
              end !if
              if maf:manufacturer <> job_ali:manufacturer      |
                  then break.  ! end if
              Case maf:field_number
                  Of 1
                      Unhide(?job_ali:fault_code1:prompt)
                      ?job_ali:fault_code1:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code1:2)
      !                    ?job_ali:fault_code1:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code1)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup1a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
      
                  Of 2
                      Unhide(?job_ali:fault_code2:prompt)
                      ?job_ali:fault_code2:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code2:2)
      !                    ?job_ali:fault_code2:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code2)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup2a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
                  Of 3
                      Unhide(?job_ali:fault_code3:prompt)
                      ?job_ali:fault_code3:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code3:2)
      !                    ?job_ali:fault_code3:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code3)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup3a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
                  Of 4
                      Unhide(?job_ali:fault_code4:prompt)
                      ?job_ali:fault_code4:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code4:2)
      !                    ?job_ali:fault_code4:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code4)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup4a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
                  Of 5
                      Unhide(?job_ali:fault_code5:prompt)
                      ?job_ali:fault_code5:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code5:2)
      !                    ?job_ali:fault_code5:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code5)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup5a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
                  Of 6
                      Unhide(?job_ali:fault_code6:prompt)
                      ?job_ali:fault_code6:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code6:2)
      !                    ?job_ali:fault_code6:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code6)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup6a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
                  Of 7
                      Unhide(?job_ali:fault_code7:prompt)
                      ?job_ali:fault_code7:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code7:2)
      !                    ?job_ali:fault_code7:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code7)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup7a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
                  Of 8
                      Unhide(?job_ali:fault_code8:prompt)
                      ?job_ali:fault_code8:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code8:2)
      !                    ?job_ali:fault_code8:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code8)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup8a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
                  Of 9
                      Unhide(?job_ali:fault_code9:prompt)
                      ?job_ali:fault_code9:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code9:2)
      !                    ?job_ali:fault_code9:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code9)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup9a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
                  Of 10
                      Unhide(?job_ali:fault_code10:prompt)
                      ?job_ali:fault_code10:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code10:2)
      !                    ?job_ali:fault_code10:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code10)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup10a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
                  Of 11
                      Unhide(?job_ali:fault_code11:prompt)
                      ?job_ali:fault_code11:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code11:2)
      !                    ?job_ali:fault_code11:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code11)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup11a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
                  Of 12
                      Unhide(?job_ali:fault_code12:prompt)
                      ?job_ali:fault_code12:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code12:2)
      !                    ?job_ali:fault_code12:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code12)
                      End
                      If maf:Lookup = 'YES'
                          ?Lookup12a{prop:Hide} = 0
                      End !If maf:Lookup = 'YES'
              End
          end !loop
          access:manfault.restorefile(save_maf_id)
          setcursor()
      End!If access:jobs.fetch(job_ali:ref_number_key) = Level:Benign
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Browse_Bouncers PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_bou_id          USHORT,AUTO
save_job_id          USHORT
alias_locator_temp   REAL
FilesOpened          BYTE
x_temp               STRING('X')
tmp:CurrentEngineer  STRING(30)
tmp:CurrentEngineer2 STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ExchangeUnit     STRING(60)
tmp:ExchangeUnit2    STRING(60)
tmp:SecondEntryReason STRING(3)
tmp:CurrentUnitDetails STRING(60)
tmp:HistoricUnitDetails STRING(60)
BRW1::View:Browse    VIEW(BOUNCER)
                       PROJECT(bou:Bouncer_Job_Number)
                       PROJECT(bou:Record_Number)
                       PROJECT(bou:Original_Ref_Number)
                       JOIN(job_ali:Ref_Number_Key,bou:Bouncer_Job_Number)
                         PROJECT(job_ali:date_booked)
                         PROJECT(job_ali:Account_Number)
                         PROJECT(job_ali:Company_Name)
                         PROJECT(job_ali:Date_Completed)
                         PROJECT(job_ali:Warranty_Charge_Type)
                         PROJECT(job_ali:Charge_Type)
                         PROJECT(job_ali:Repair_Type_Warranty)
                         PROJECT(job_ali:Repair_Type)
                         PROJECT(job_ali:Ref_Number)
                         PROJECT(job_ali:ESN)
                         PROJECT(job_ali:MSN)
                         PROJECT(job_ali:Engineer)
                         PROJECT(job_ali:Exchange_Unit_Number)
                         PROJECT(job_ali:Model_Number)
                         PROJECT(job_ali:Manufacturer)
                         PROJECT(job_ali:Warranty_Job)
                         PROJECT(job_ali:Chargeable_Job)
                         JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                           PROJECT(jbn_ali:Engineers_Notes)
                           PROJECT(jbn_ali:Invoice_Text)
                           PROJECT(jbn_ali:Fault_Description)
                         END
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
bou:Bouncer_Job_Number LIKE(bou:Bouncer_Job_Number)   !List box control field - type derived from field
job_ali:date_booked    LIKE(job_ali:date_booked)      !List box control field - type derived from field
job_ali:Account_Number LIKE(job_ali:Account_Number)   !List box control field - type derived from field
job_ali:Company_Name   LIKE(job_ali:Company_Name)     !List box control field - type derived from field
job_ali:Date_Completed LIKE(job_ali:Date_Completed)   !List box control field - type derived from field
job_ali:Warranty_Charge_Type LIKE(job_ali:Warranty_Charge_Type) !List box control field - type derived from field
job_ali:Charge_Type    LIKE(job_ali:Charge_Type)      !List box control field - type derived from field
job_ali:Repair_Type_Warranty LIKE(job_ali:Repair_Type_Warranty) !List box control field - type derived from field
job_ali:Repair_Type    LIKE(job_ali:Repair_Type)      !List box control field - type derived from field
job_ali:Ref_Number     LIKE(job_ali:Ref_Number)       !List box control field - type derived from field
jbn_ali:Engineers_Notes LIKE(jbn_ali:Engineers_Notes) !List box control field - type derived from field
jbn_ali:Invoice_Text   LIKE(jbn_ali:Invoice_Text)     !List box control field - type derived from field
job_ali:ESN            LIKE(job_ali:ESN)              !List box control field - type derived from field
job_ali:MSN            LIKE(job_ali:MSN)              !List box control field - type derived from field
job_ali:Engineer       LIKE(job_ali:Engineer)         !List box control field - type derived from field
job_ali:Exchange_Unit_Number LIKE(job_ali:Exchange_Unit_Number) !List box control field - type derived from field
jbn_ali:Fault_Description LIKE(jbn_ali:Fault_Description) !List box control field - type derived from field
job_ali:Model_Number   LIKE(job_ali:Model_Number)     !List box control field - type derived from field
job_ali:Manufacturer   LIKE(job_ali:Manufacturer)     !List box control field - type derived from field
job_ali:Warranty_Job   LIKE(job_ali:Warranty_Job)     !Browse hot field - type derived from field
job_ali:Chargeable_Job LIKE(job_ali:Chargeable_Job)   !Browse hot field - type derived from field
bou:Record_Number      LIKE(bou:Record_Number)        !Primary key field - type derived from field
bou:Original_Ref_Number LIKE(bou:Original_Ref_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:date_booked)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Company_Name)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Third_Party_Site)
                       PROJECT(job:Engineer)
                       PROJECT(job:Exchange_Unit_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Warranty_Job)
                       PROJECT(job:Chargeable_Job)
                       PROJECT(job:Bouncer)
                       JOIN(jbn:RefNumberKey,job:Ref_Number)
                         PROJECT(jbn:Engineers_Notes)
                         PROJECT(jbn:Fault_Description)
                         PROJECT(jbn:Invoice_Text)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:date_booked        LIKE(job:date_booked)          !List box control field - type derived from field
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Company_Name       LIKE(job:Company_Name)         !List box control field - type derived from field
job:Date_Completed     LIKE(job:Date_Completed)       !List box control field - type derived from field
job:Repair_Type        LIKE(job:Repair_Type)          !List box control field - type derived from field
job:Repair_Type_Warranty LIKE(job:Repair_Type_Warranty) !List box control field - type derived from field
job:Warranty_Charge_Type LIKE(job:Warranty_Charge_Type) !List box control field - type derived from field
job:Charge_Type        LIKE(job:Charge_Type)          !List box control field - type derived from field
jbn:Engineers_Notes    LIKE(jbn:Engineers_Notes)      !List box control field - type derived from field
jbn:Fault_Description  LIKE(jbn:Fault_Description)    !List box control field - type derived from field
jbn:Invoice_Text       LIKE(jbn:Invoice_Text)         !List box control field - type derived from field
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:MSN                LIKE(job:MSN)                  !List box control field - type derived from field
job:Third_Party_Site   LIKE(job:Third_Party_Site)     !List box control field - type derived from field
job:Engineer           LIKE(job:Engineer)             !List box control field - type derived from field
job:Exchange_Unit_Number LIKE(job:Exchange_Unit_Number) !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:Manufacturer       LIKE(job:Manufacturer)         !List box control field - type derived from field
job:Warranty_Job       LIKE(job:Warranty_Job)         !Browse hot field - type derived from field
job:Chargeable_Job     LIKE(job:Chargeable_Job)       !Browse hot field - type derived from field
job:Bouncer            LIKE(job:Bouncer)              !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(WARPARTS)
                       PROJECT(wpr:Description)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                       PROJECT(wpr:Part_Number)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:3
wpr:Description        LIKE(wpr:Description)          !List box control field - type derived from field
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
wpr:Part_Number        LIKE(wpr:Part_Number)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(PARTS)
                       PROJECT(par:Description)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                       PROJECT(par:Part_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
par:Part_Number        LIKE(par:Part_Number)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(PARTS_ALIAS)
                       PROJECT(par_ali:Description)
                       PROJECT(par_ali:Ref_Number)
                       PROJECT(par_ali:Part_Number)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?List:4
par_ali:Description    LIKE(par_ali:Description)      !List box control field - type derived from field
par_ali:Ref_Number     LIKE(par_ali:Ref_Number)       !Browse key field - type derived from field
par_ali:Part_Number    LIKE(par_ali:Part_Number)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(WARPARTS_ALIAS)
                       PROJECT(war_ali:Description)
                       PROJECT(war_ali:Ref_Number)
                       PROJECT(war_ali:Part_Number)
                     END
Queue:Browse:5       QUEUE                            !Queue declaration for browse/combo box using ?List:5
war_ali:Description    LIKE(war_ali:Description)      !List box control field - type derived from field
war_ali:Ref_Number     LIKE(war_ali:Ref_Number)       !Browse key field - type derived from field
war_ali:Part_Number    LIKE(war_ali:Part_Number)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Bouncers'),AT(0,0,680,429),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR,NOMERGE
                         ITEM('Bouncer Defaults'),USE(?BouncerDefaults)
                       END
                       PROMPT('Browse Bouncers'),AT(8,9),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,9),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       PROMPT('Current Job'),AT(8,31,112,9),USE(?Prompt1),TRN,FONT('Tahoma',10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       LIST,AT(8,59,328,80),USE(?List),IMM,VSCROLL,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('36R(2)|M~Job No~@s8@48R(2)|M~Date Booked~L@d6b@64L(2)|M~Account Number~@s15@70L(' &|
   '2)|M~Company Name~@s30@48R(2)|M~Date Comp~L@D6b@120L(2)|M~Repair Type~@s30@120L(' &|
   '2)|M~Repair Type~@s30@120L(2)|M~Charge Type~@s30@120L(2)|M~Charge Type~@s30@1020' &|
   'L(2)|M~Engineers Notes~@s255@1020L(2)|M~Fault Description~@s255@1020L(2)|M~Invoi' &|
   'ce Text~@s255@80L(2)|M~I.M.E.I. Number~@s20@80L(2)|M~MSN~@s20@120L(2)|M~Third Pa' &|
   'rty Repair Site~@s30@12L(2)|M~Engineer~@s3@32R(2)|M~Exchange Unit Number~L@s8@12' &|
   '0L(2)|M~Model Number~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse)
                       LIST,AT(348,59,328,80),USE(?Browse:1),IMM,VSCROLL,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('36R(2)|M~Job No~@s8@47R(2)|M~Date Booked~L@d6b@65L(2)|M~Account Number~@s15@68L(' &|
   '2)|M~Company Name~@s30@48R(2)|M~Date Comp~L@D6b@120L(2)|M~Charge Type~@s30@120L(' &|
   '2)|M~Charge Type~@s30@120L(2)|M~Repair Type~@s30@120L(2)|M~Repair Type~@s30@36L(' &|
   '2)|M~Job Number~@p<<<<<<<<<<<<<<<<#p@1020L(2)|M~Engineers Notes~@s255@1020L(2)|M~Invoice' &|
   ' Text~@s255@80L(2)|M~I.M.E.I. Number~@s20@80L(2)|M~MSN~@s20@12L(2)|M~Engineer~@s' &|
   '3@32R(2)|M~Exchange Unit Number~L@s8@1020R(2)|M~Fault Description~L@s255@120L(2)' &|
   '|M~Model Number~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(4,27,336,356),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 2'),USE(?Tab2)
                           BUTTON,AT(272,31),USE(?Change),TRN,FLAT,LEFT,ICON('viewcurp.jpg')
                           ENTRY(@s8),AT(8,43,44,10),USE(job:Ref_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Unit Details:'),AT(56,44),USE(?Prompt30),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(108,44),USE(tmp:CurrentUnitDetails),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fault Description'),AT(8,141),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Invoice Text'),AT(196,141),USE(?Prompt7:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(8,149,132,38),USE(jbn:Fault_Description),SKIP,VSCROLL,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           TEXT,AT(196,149,132,38),USE(jbn:Invoice_Text),SKIP,VSCROLL,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           PROMPT('Chargeable Parts'),AT(8,197),USE(?Prompt2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Warranty Parts'),AT(196,197),USE(?Prompt3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Transfer Parts'),AT(144,205),USE(?Prompt29),CENTER,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           LIST,AT(8,205,132,72),USE(?List:2),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse:2)
                           LIST,AT(196,205,132,72),USE(?List:3),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse:3)
                           BUTTON,AT(156,219),USE(?TransferCharToWarr),TRN,FLAT,ICON('rightp.jpg')
                           BUTTON,AT(156,241),USE(?TransferWarrToChar),TRN,FLAT,ICON('leftp.jpg')
                           PROMPT('Chargeable Charge Type'),AT(8,287),USE(?job:Charge_Type:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Warranty Charge Type'),AT(196,287),USE(?job:Warranty_Charge_Type:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(8,295),USE(job:Charge_Type),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(196,295),USE(job:Warranty_Charge_Type),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Warranty Repair Type'),AT(196,307),USE(?job:Repair_Type_Warranty:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(8,315),USE(job:Repair_Type),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(196,315),USE(job:Repair_Type_Warranty),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number'),AT(8,327),USE(?Prompt11),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Engineer'),AT(196,327),USE(?tmp:CurrentEngineer:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s20),AT(8,335),USE(job:ESN),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(196,335),USE(tmp:CurrentEngineer),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('M.S.N.'),AT(8,347),USE(?Prompt11:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Third Party Site'),AT(196,347),USE(?job:Third_Party_Site:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s20),AT(8,355),USE(job:MSN),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(196,355,100,12),USE(job:Third_Party_Site),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Exchange Unit:'),AT(8,371),USE(?tmp:ExchangeUnit:Prompt),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(68,371,212,12),USE(tmp:ExchangeUnit),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Chargeable Repair Type'),AT(8,307),USE(?job:Repair_Type:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(8,384),USE(?Submit_For_Invoice),TRN,FLAT,LEFT,ICON('subinvp.jpg')
                       BUTTON,AT(76,384),USE(?Reject_From_Invoice),TRN,FLAT,LEFT,ICON('rejinvp.jpg')
                       SHEET,AT(344,27,332,356),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('By Job Number'),USE(?Tab:2)
                           PROMPT('Historic Job'),AT(348,31),USE(?Prompt6),TRN,FONT('Tahoma',10,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(608,31),USE(?View_Historic_Job),TRN,FLAT,LEFT,ICON('viewhisp.jpg')
                           PROMPT('Unit Details:'),AT(348,44),USE(?Prompt30:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(396,44),USE(tmp:HistoricUnitDetails),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Chargeable Parts'),AT(348,197),USE(?Prompt2:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(348,205,132,72),USE(?List:4),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse:4)
                           PROMPT('Warranty Parts'),AT(528,197),USE(?Prompt3:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(528,205,132,72),USE(?List:5),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse:5)
                           PROMPT('Chargeable Charge Type'),AT(348,287),USE(?job_ali:Charge_Type:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Engineers Notes'),AT(348,140),USE(?Prompt7:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Invoice Text'),AT(528,140),USE(?Prompt7:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(348,148,136,38),USE(jbn_ali:Fault_Description),SKIP,VSCROLL,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           TEXT,AT(528,148,144,38),USE(jbn_ali:Invoice_Text),SKIP,VSCROLL,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           STRING(@s30),AT(348,295),USE(job_ali:Charge_Type),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(528,295),USE(job_ali:Warranty_Charge_Type),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Chargeable Repair Type'),AT(348,307),USE(?job_ali:Repair_Type:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Warranty Charge Type'),AT(528,287),USE(?job_ali:Warranty_Charge_Type:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(528,315),USE(job_ali:Repair_Type_Warranty),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number:'),AT(348,327),USE(?Prompt11:5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Engineer'),AT(528,327),USE(?tmp:CurrentEngineer2:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s20),AT(348,335),USE(job_ali:ESN),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('M.S.N.'),AT(348,347),USE(?Prompt11:7),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Third Party Site:'),AT(528,347),USE(?job_ali:Third_Party_Site:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s20),AT(348,355),USE(job_ali:MSN),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(528,355,100,12),USE(job_ali:Third_Party_Site),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Exchange Unit:'),AT(348,367),USE(?tmp:ExchangeUnit2:Prompt),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(408,367,212,12),USE(tmp:ExchangeUnit2),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(528,335),USE(tmp:CurrentEngineer2),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Warranty Repair Type'),AT(528,307),USE(?job_ali:Repair_Type_Warranty:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(348,315),USE(job_ali:Repair_Type),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(512,384),USE(?Compare_Fault_Codes),TRN,FLAT,LEFT,ICON('compfaup.jpg')
                       BUTTON,AT(608,384),USE(?Cancel),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(152,384),USE(?Submit_For_Invoice:2),TRN,FLAT,LEFT,ICON('subclaip.jpg')
                       BUTTON,AT(220,384),USE(?Reject_From_Invoice:2),TRN,FLAT,LEFT,ICON('rejclaip.jpg')
                       BUTTON,AT(440,384),USE(?Button11),TRN,FLAT,LEFT,ICON('couboup.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW8                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:3                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9                 CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:4                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:5
Q                      &Queue:Browse:5                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020396'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Bouncers')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:PARTS_ALIAS.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Relate:USERS_ALIAS.Open
  Relate:WARPARTS_ALIAS.Open
  Relate:WEBJOB.Open
  Access:JOBS_ALIAS.UseFile
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:MODELNUM.UseFile
  Access:CHARTYPE.UseFile
  Access:REPTYDEF.UseFile
  Access:JOBSE.UseFile
  Access:REPAIRTY.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSE2.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:BOUNCER,SELF)
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:JOBS,SELF)
  BRW8.Init(?List:3,Queue:Browse:3.ViewPosition,BRW8::View:Browse,Queue:Browse:3,Relate:WARPARTS,SELF)
  BRW7.Init(?List:2,Queue:Browse:2.ViewPosition,BRW7::View:Browse,Queue:Browse:2,Relate:PARTS,SELF)
  BRW9.Init(?List:4,Queue:Browse:4.ViewPosition,BRW9::View:Browse,Queue:Browse:4,Relate:PARTS_ALIAS,SELF)
  BRW10.Init(?List:5,Queue:Browse:5.ViewPosition,BRW10::View:Browse,Queue:Browse:5,Relate:WARPARTS_ALIAS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !added when viewed by thin client - J 28/11/02 - ref L328
  if glo:webjob then
      hide(?submit_for_Invoice)
      hide(?Reject_from_invoice)
      hide(?submit_for_Invoice:2)
      hide(?Reject_from_invoice:2)
  END !if glo:webjob
  !end addition
  ! Save Window Name
   AddToLog('Window','Open','Browse_Bouncers')
  ?List{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?List:5{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,bou:Bouncer_Job_Number_Key)
  BRW1.AddRange(bou:Original_Ref_Number,job:Ref_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,bou:Bouncer_Job_Number,1,BRW1)
  BRW1.AddField(bou:Bouncer_Job_Number,BRW1.Q.bou:Bouncer_Job_Number)
  BRW1.AddField(job_ali:date_booked,BRW1.Q.job_ali:date_booked)
  BRW1.AddField(job_ali:Account_Number,BRW1.Q.job_ali:Account_Number)
  BRW1.AddField(job_ali:Company_Name,BRW1.Q.job_ali:Company_Name)
  BRW1.AddField(job_ali:Date_Completed,BRW1.Q.job_ali:Date_Completed)
  BRW1.AddField(job_ali:Warranty_Charge_Type,BRW1.Q.job_ali:Warranty_Charge_Type)
  BRW1.AddField(job_ali:Charge_Type,BRW1.Q.job_ali:Charge_Type)
  BRW1.AddField(job_ali:Repair_Type_Warranty,BRW1.Q.job_ali:Repair_Type_Warranty)
  BRW1.AddField(job_ali:Repair_Type,BRW1.Q.job_ali:Repair_Type)
  BRW1.AddField(job_ali:Ref_Number,BRW1.Q.job_ali:Ref_Number)
  BRW1.AddField(jbn_ali:Engineers_Notes,BRW1.Q.jbn_ali:Engineers_Notes)
  BRW1.AddField(jbn_ali:Invoice_Text,BRW1.Q.jbn_ali:Invoice_Text)
  BRW1.AddField(job_ali:ESN,BRW1.Q.job_ali:ESN)
  BRW1.AddField(job_ali:MSN,BRW1.Q.job_ali:MSN)
  BRW1.AddField(job_ali:Engineer,BRW1.Q.job_ali:Engineer)
  BRW1.AddField(job_ali:Exchange_Unit_Number,BRW1.Q.job_ali:Exchange_Unit_Number)
  BRW1.AddField(jbn_ali:Fault_Description,BRW1.Q.jbn_ali:Fault_Description)
  BRW1.AddField(job_ali:Model_Number,BRW1.Q.job_ali:Model_Number)
  BRW1.AddField(job_ali:Manufacturer,BRW1.Q.job_ali:Manufacturer)
  BRW1.AddField(job_ali:Warranty_Job,BRW1.Q.job_ali:Warranty_Job)
  BRW1.AddField(job_ali:Chargeable_Job,BRW1.Q.job_ali:Chargeable_Job)
  BRW1.AddField(bou:Record_Number,BRW1.Q.bou:Record_Number)
  BRW1.AddField(bou:Original_Ref_Number,BRW1.Q.bou:Original_Ref_Number)
  BRW6.Q &= Queue:Browse
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,job:Bouncer_Key)
  BRW6.AddRange(job:Bouncer,x_temp)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(?job:Ref_Number,job:Ref_Number,1,BRW6)
  BRW6.AddField(job:Ref_Number,BRW6.Q.job:Ref_Number)
  BRW6.AddField(job:date_booked,BRW6.Q.job:date_booked)
  BRW6.AddField(job:Account_Number,BRW6.Q.job:Account_Number)
  BRW6.AddField(job:Company_Name,BRW6.Q.job:Company_Name)
  BRW6.AddField(job:Date_Completed,BRW6.Q.job:Date_Completed)
  BRW6.AddField(job:Repair_Type,BRW6.Q.job:Repair_Type)
  BRW6.AddField(job:Repair_Type_Warranty,BRW6.Q.job:Repair_Type_Warranty)
  BRW6.AddField(job:Warranty_Charge_Type,BRW6.Q.job:Warranty_Charge_Type)
  BRW6.AddField(job:Charge_Type,BRW6.Q.job:Charge_Type)
  BRW6.AddField(jbn:Engineers_Notes,BRW6.Q.jbn:Engineers_Notes)
  BRW6.AddField(jbn:Fault_Description,BRW6.Q.jbn:Fault_Description)
  BRW6.AddField(jbn:Invoice_Text,BRW6.Q.jbn:Invoice_Text)
  BRW6.AddField(job:ESN,BRW6.Q.job:ESN)
  BRW6.AddField(job:MSN,BRW6.Q.job:MSN)
  BRW6.AddField(job:Third_Party_Site,BRW6.Q.job:Third_Party_Site)
  BRW6.AddField(job:Engineer,BRW6.Q.job:Engineer)
  BRW6.AddField(job:Exchange_Unit_Number,BRW6.Q.job:Exchange_Unit_Number)
  BRW6.AddField(job:Model_Number,BRW6.Q.job:Model_Number)
  BRW6.AddField(job:Manufacturer,BRW6.Q.job:Manufacturer)
  BRW6.AddField(job:Warranty_Job,BRW6.Q.job:Warranty_Job)
  BRW6.AddField(job:Chargeable_Job,BRW6.Q.job:Chargeable_Job)
  BRW6.AddField(job:Bouncer,BRW6.Q.job:Bouncer)
  BRW8.Q &= Queue:Browse:3
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,wpr:Part_Number_Key)
  BRW8.AddRange(wpr:Ref_Number,job:Ref_Number)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,wpr:Part_Number,1,BRW8)
  BRW8.AddField(wpr:Description,BRW8.Q.wpr:Description)
  BRW8.AddField(wpr:Record_Number,BRW8.Q.wpr:Record_Number)
  BRW8.AddField(wpr:Ref_Number,BRW8.Q.wpr:Ref_Number)
  BRW8.AddField(wpr:Part_Number,BRW8.Q.wpr:Part_Number)
  BRW7.Q &= Queue:Browse:2
  BRW7.RetainRow = 0
  BRW7.AddSortOrder(,par:Part_Number_Key)
  BRW7.AddRange(par:Ref_Number,job:Ref_Number)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,par:Part_Number,1,BRW7)
  BRW7.AddField(par:Description,BRW7.Q.par:Description)
  BRW7.AddField(par:Record_Number,BRW7.Q.par:Record_Number)
  BRW7.AddField(par:Ref_Number,BRW7.Q.par:Ref_Number)
  BRW7.AddField(par:Part_Number,BRW7.Q.par:Part_Number)
  BRW9.Q &= Queue:Browse:4
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,par_ali:Part_Number_Key)
  BRW9.AddRange(par_ali:Ref_Number,job_ali:Ref_Number)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,par_ali:Part_Number,1,BRW9)
  BRW9.AddField(par_ali:Description,BRW9.Q.par_ali:Description)
  BRW9.AddField(par_ali:Ref_Number,BRW9.Q.par_ali:Ref_Number)
  BRW9.AddField(par_ali:Part_Number,BRW9.Q.par_ali:Part_Number)
  BRW10.Q &= Queue:Browse:5
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,war_ali:Part_Number_Key)
  BRW10.AddRange(war_ali:Ref_Number,job_ali:Ref_Number)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,war_ali:Part_Number,1,BRW10)
  BRW10.AddField(war_ali:Description,BRW10.Q.war_ali:Description)
  BRW10.AddField(war_ali:Ref_Number,BRW10.Q.war_ali:Ref_Number)
  BRW10.AddField(war_ali:Part_Number,BRW10.Q.war_ali:Part_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW6.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:PARTS_ALIAS.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
    Relate:USERS_ALIAS.Close
    Relate:WARPARTS_ALIAS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Bouncers')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJOBS
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Compare_Fault_Codes
      brw1.updatebuffer()
      brw6.updatebuffer()
    OF ?Button11
      Case Missive('Counting the jobs may take a very long time to completed.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
      
              count# = 0
              yldcnt# = 0
      
              Prog.ProgressSetup(Records(JOBS))
      
              save_job_id = access:jobs.savefile()
              Access:jobs.clearkey(job:Bouncer_Key)
              job:bouncer = 'X'
              set(job:Bouncer_Key,job:Bouncer_Key)
      
              loop
                  if access:jobs.next()
                    break
                  end !if
      
                  If Prog.InsideLoop()
                      Break
                  End ! If Prog.InsideLoop()
      
                  if job:bouncer <> 'X'
                    break
                  end !if run out of bouncers
      
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
      
                  if glo:webjob
                        !only count ones matching the trade head account
                        access:subtracc.clearkey(sub:Account_Number_Key)
                        sub:Account_Number = job:Account_Number
                        if access:subtracc.fetch(sub:Account_Number_Key) = level:benign then
                            if sub:Main_Account_Number = ClarioNET:Global.Param2
                                count#+=1
                            END !If account does not match
                        END!If access subtrac
                  Else  !if glo:webjob
                        !count em all
                        count# += 1
                  END !if glo webjob
              end !loop
              access:jobs.restorefile(save_job_id)
              Prog.ProgressFinish()
      
              Case Missive('There are ' & Clip(count#) & ' job in this browse.','ServiceBase 3g',|
                             'midea.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
      
          Of 1 ! No Button
      End ! Case Missive
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?BouncerDefaults
      ThisWindow.Update
      BouncerDefaults
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020396'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020396'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020396'&'0')
      ***
    OF ?Change
      ThisWindow.Update
      brw1.resetsort(1)
      brw6.resetsort(1)
    OF ?TransferCharToWarr
      ThisWindow.Update
       If SecurityCheck('JOBS - TRANSFER PARTS')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If SecurityCheck('JOBS - TRANSFER PARTS')
          Case Missive('Are you sure you want to transfer all the Chargeable Parts to Warranty Parts?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
      
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = brw6.q.job:Ref_Number
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
      
                      count# = 0
                      setcursor(cursor:wait)
                      save_par_id = access:parts.savefile()
                      access:parts.clearkey(par:part_number_key)
                      par:ref_number  = job:ref_number
                      set(par:part_number_key,par:part_number_key)
                      loop
                          if access:parts.next()
                             break
                          end !if
                          if par:ref_number  <> job:ref_number      |
                              then break.  ! end if
                          If par:pending_ref_number <> ''
                              access:ordpend.clearkey(ope:ref_number_key)
                              ope:ref_number = par:pending_ref_number
                              if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                                  ope:part_type = 'JOB'
                                  access:ordpend.update()
                              End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                          End!If wpr:pending_ref_number <> ''
                          If par:order_number <> ''
                              access:ordparts.clearkey(orp:record_number_key)
                              orp:record_number   = par:order_number
                              If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                                  orp:part_type = 'JOB'
                                  access:ordparts.update()
                              End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                          End!If wpr:order_number <> ''
                          get(warparts,0)
                          if access:warparts.primerecord() = Level:Benign
                              record_number$  = wpr:record_number
                              wpr:record      :=: par:record
                              wpr:record_number   = record_number$
                              if access:warparts.insert()
                                  access:warparts.cancelautoinc()
                              End!if access:parts.insert()
                          End!if access:parts.primerecord() = Level:Benign
                          Delete(parts)
                          count# += 1
                      end !loop
                      access:parts.restorefile(save_par_id)
                      setcursor()
                      if count# <> 0
      
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              aud:type          = 'JOB'
                              access:users.clearkey(use:password_key)
                              use:password = glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'CHARGEABLE PARTS TRANSFERRED TO WARRANTY'
                              access:audit.insert()
                          end!�if access:audit.primerecord() = level:benign
                          If job:warranty_job <> 'YES'
                              job:Warranty_Charge_Type = ''
                              Case Missive('This job has not been marked as "Warranty".'&|
                                '<13,10>Do you wish to make it a SPLIT job, or a WARRANTY only job?','ServiceBase 3g',|
                                             'mquest.jpg','Warranty|Split')
                                  Of 2 ! Split Button
                                      job:Warranty_job = 'YES'
      !                                warranty_job_temp = job:Warranty_job
      !                                Post(event:accepted,?job:Warranty_job)
      
                                  Of 1 ! Warranty Button
                                      job:chargeable_job = 'NO'
                                      job:warranty_job = 'YES'
      !                                warranty_job_temp = job:warranty_job
      !                                chargeable_job_temp = job:chargeable_job
      !                                Post(event:accepted,?job:chargeable_job)
      !                                Post(event:accepted,?job:warranty_job)
      
                              End ! Case Missive
                          Else!If job:chargeable_job <> 'YES'
                              Case Missive('Tranfer Complete.','ServiceBase 3g',|
                                             'midea.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                          End!If job:warranty_job <> 'YES'
                      End!if count# <> 0
                      Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                      mod:Model_Number    = job:Model_NUmber
                      If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                          !Found
                          job:EDI = PendingJob(mod:Manufacturer)
                          job:Manufacturer    = mod:Manufacturer
                      Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      Access:JOBS.Update()
                  Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
      
              Of 1 ! No Button
          End ! Case Missive
      End!If SecurityCheck('JOBS - TRANSFER PARTS')
      BRW6.ResetSort(1)
      BRW7.ResetSort(1)
      BRW8.ResetSort(1)
    OF ?TransferWarrToChar
      ThisWindow.Update
      If SecurityCheck('JOBS - TRANSFER PARTS')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If SecurityCheck('JOBS - TRANSFER PARTS')
          Case Missive('Are you sure you want to transfer all your Warranty Parts to Chargeable Parts?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = brw6.q.job:Ref_Number
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
      
      
                      count# = 0
                      setcursor(cursor:wait)
                      save_wpr_id = access:warparts.savefile()
                      access:warparts.clearkey(wpr:part_number_key)
                      wpr:ref_number  = job:ref_number
                      set(wpr:part_number_key,wpr:part_number_key)
                      loop
                          if access:warparts.next()
                             break
                          end !if
                          if wpr:ref_number  <> job:ref_number      |
                              then break.  ! end if
                          If wpr:pending_ref_number <> ''
                              access:ordpend.clearkey(ope:ref_number_key)
                              ope:ref_number = wpr:pending_ref_number
                              if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                                  ope:part_type = 'JOB'
                                  access:ordpend.update()
                              End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                          End!If wpr:pending_ref_number <> ''
                          If wpr:order_number <> ''
                              access:ordparts.clearkey(orp:record_number_key)
                              orp:record_number   = wpr:order_number
                              If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                                  orp:part_type = 'JOB'
                                  access:ordparts.update()
                              End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                          End!If wpr:order_number <> ''
                          get(parts,0)
                          if access:parts.primerecord() = Level:Benign
                              record_number$  = par:record_number
                              par:record      :=: wpr:record
                              par:record_number   = record_number$
                              if access:parts.insert()
                                  access:parts.cancelautoinc()
                              End!if access:parts.insert()
                          End!if access:parts.primerecord() = Level:Benign
                          Delete(warparts)
                          count# += 1
                      end !loop
                      access:warparts.restorefile(save_wpr_id)
                      setcursor()
                      if count# <> 0
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              aud:type          = 'JOB'
                              access:users.clearkey(use:password_key)
                              use:password = glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'WARRANTY PARTS TRANSFERRED TO CHARGEABLE'
                              access:audit.insert()
                          end!�if access:audit.primerecord() = level:benign
      
                          If job:chargeable_job <> 'YES'
                              Case Missive('This job has not been marked as "Chargeable".'&|
                                '<13,10>Do you wish to make this a SPLIT job, or a CHARGEABLE only job?','ServiceBase 3g',|
                                             'mquest.jpg','Chargeable|Split')
                                  Of 2 ! Split Button
                                      job:chargeable_job = 'YES'
                                  Of 1 ! Chargeable Button
                                      job:warranty_job = 'NO'
                                      job:chargeable_job = 'YES'
                              End ! Case Missive
                          Else!If job:chargeable_job <> 'YES'
                              Case Missive('Tranfer Complete.','ServiceBase 3g',|
                                             'midea.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                          End!If job:chargeable_job <> 'YES'
      
                      End!if count# <> 0
                      Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                      mod:Model_Number    = job:Model_NUmber
                      If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                          !Found
                          job:EDI = PendingJob(mod:Manufacturer)
                          job:Manufacturer    = mod:Manufacturer
                      Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      Access:JOBS.Update()
                  Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
      
              Of 1 ! No Button
          End ! Case Missive
      End!If SecurityCheck('JOBS - TRANSFER PARTS')
      BRW6.ResetSort(1)
      BRW7.ResetSort(1)
      BRW8.ResetSort(1)
    OF ?Submit_For_Invoice
      ThisWindow.Update
      brw1.updatebuffer()
      brw6.updatebuffer()
      Case Missive('Are  you sure you want to submit this job for Invoicing?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              do_audit# = 0
              Set(defaults)
              access:defaults.next()
              remove# = 0
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number  = brw6.q.job:ref_number
              If access:jobs.fetch(job:ref_number_key) = Level:Benign
                  If job:chargeable_job <> 'YES'
                      !If this is not a chargeable job. Error.
                      Case Missive('The selected job is not marked as "Chargeable".','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else!If job:chargeable_job <> 'YES'
      !                tmp:SecondEntryReason   = ConfirmSecondEntry()
      !
      !                If tmp:SecondEntryReason
      !                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
      !                    jobse:RefNumber = job:Ref_Number
      !                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !                        !Found
      !                        jobe:CConfirmSecondEntry    = tmp:SecondEntryReason
      !                        Access:JOBSE.Update()
      !                    Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !                        !Error
      !                        !Assert(0,'<13,10>Fetch Error<13,10>')
      !                    End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                          Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                          rtd:Manufacturer = job:Manufacturer
                          rtd:Chargeable   = 'YES'
                          rtd:Repair_Type  = GETINI('BOUNCER','ChargeableRepairType',,CLIP(PATH())&'\SB2KDEF.INI')
                          If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                              !Found
                              job:Repair_Type = GETINI('BOUNCER','ChargeableRepairType',,CLIP(PATH())&'\SB2KDEF.INI')
      
                              !Call the new Pricing Routine - L945 (DBH: 04-09-2003)
                              JobPricingRoutine(0)
                              Access:JOBSE.Update()
                              Access:JOBS.Update()
                          Else!If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End!If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
      
      
                          If job:warranty_job = 'YES'
                              !If this is a split warranty
                              If job:bouncer_type = 'BOT' or job:Bouncer_Type = 'WAR'
                                  !If BOT, then mark as warranty bouncer
                                  job:bouncer_type = 'WAR'
                                  access:jobs.update()
                                  do_audit# = 1
                              Else!If job:bouncer_type = 'BOT'
                                  !Otherwise remove from browse
                                  remove# = 1
                              End!If job:bouncer_type = 'BOT'
                          Else!If job:warranty_job = 'YES'
                              !Remove from browse
                              remove# = 1
                          End!If job:warranty_job = 'YES'
      
      !                End !If tmp:SecondEntryReason
                  End!If job:chargeable_job <> 'YES'
      
                  If remove# = 1
                      setcursor(cursor:wait)
                      save_bou_id = access:bouncer.savefile()
                      access:bouncer.clearkey(bou:bouncer_job_number_key)
                      bou:original_ref_number = job:ref_number
                      set(bou:bouncer_job_number_key,bou:bouncer_job_number_key)
                      loop
                          if access:bouncer.next()
                             break
                          end !if
                          if bou:original_ref_number <> job:ref_number      |
                              then break.  ! end if
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          Delete(bouncer)
                      end !loop
                      access:bouncer.restorefile(save_bou_id)
                      setcursor()
                      job:bouncer = ''
                      job:bouncer_type = ''
                      If def:qa_required = 'YES'
                          If JOB:QA_Passed = 'YES'
                              GetStatus(610,1,'JOB')
                          Else!If JOB:QA_Passed = 'YES'
                              If job:qa_rejected = 'YES' and job:qa_second_passed <> 'YES'
                                  GetStatus(615,1,'JOB')
                              End!If job:qa_rejected = 'YES' and job:qa_second_passed <> 'YES'
                              If job:qa_rejected = 'YES' and job:qa_second_passed = 'YES'
                                  GetStatus(610,1,'JOB')
                              End!If job:qa_rejected = 'YES' and job:qa_second_passed = 'YES'
                              If job:qa_rejected <> 'YES'
                                  GetStatus(605,1,'JOB')
                              End!If job:qa_rejected <> 'YES'
                          End!If JOB:QA_Passed = 'YES'
                      else!If def:qa_required = 'YES'
                          If ToBeExchanged()
                              !GetStatus(707,1,'JOB')    !Amended by Neil 13/07/2001
                          Else!If ToBeExchanged()
                              !GetStatus(705,1,'JOB')    !Amended by Neil 13/07/2001
                          End!If ToBeExchanged()
                      End!If def:qa_required = 'YES'
                      access:jobs.update()
                      do_audit# = 1
                  End!If remove# = 1
      
                  If do_audit# = 1
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
      !                    aud:Notes         = 'CONFIRMED SECOND ENTRY: '
      !                    Case tmp:SecondEntryReason
      !                        Of 'ENG'
      !                            aud:Notes = Clip(aud:Notes) & ' ENGINEER'
      !                        Of 'PAR'
      !                            aud:Notes = Clip(aud:Notes) & ' PARTS'
      !                        Of 'MAN'
      !                            aud:Notes = Clip(aud:Notes) & ' MANUFACTURER'
      !                        Of 'UNR'
      !                            aud:Notes = Clip(aud:Notes) & ' UNRELATED'
      !                    End !Case tmp:SecondEntryReason
      
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'BOUNCER SUBMITTED FOR INVOICING'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                  End!If do_audit# = 1
              End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      
      
              brw1.resetsort(1)
              brw6.resetsort(1)
          Of 1 ! No Button
      End ! Case Missive
    OF ?Reject_From_Invoice
      ThisWindow.Update
      brw1.updatebuffer()
      brw6.updatebuffer()
      Case Missive('Are you sure you want to REJECT this job from Invoicing?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              do_audit# = 0
              Set(Defaults)
              access:defaults.next()
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number  = brw6.q.job:ref_number
              If access:jobs.fetch(job:ref_number_key) = Level:Benign
                  If job:chargeable_job <> 'YES'
                      Case Missive('The selected job is not marked as "Chargeable".','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else!If job:chargeable_job <> 'YES'
                      If job:warranty_job = 'YES'
                          If job:bouncer_type = 'BOT'
                              job:bouncer_type = 'WAR'
                              access:jobs.update()
                              do_audit# = 1
                          Else!If job:bouncer_type = 'BOT'
                              remove# = 1
                          End!If job:bouncer_type = 'BOT'
                      Else!If job:warranty_job = 'YES'
                          remove# = 1
                      End!If job:warranty_job = 'YES'
                  End!If job:chargeable_job <> 'YES'
                  If remove# = 1
                      setcursor(cursor:wait)
                      save_bou_id = access:bouncer.savefile()
                      access:bouncer.clearkey(bou:bouncer_job_number_key)
                      bou:original_ref_number = job:ref_number
                      set(bou:bouncer_job_number_key,bou:bouncer_job_number_key)
                      loop
                          if access:bouncer.next()
                             break
                          end !if
                          if bou:original_ref_number <> job:ref_number      |
                              then break.  ! end if
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          Delete(bouncer)
                      end !loop
                      access:bouncer.restorefile(save_bou_id)
                      setcursor()
                      job:bouncer = 'R'
                      job:bouncer_type = ''
                      access:jobs.update()
                      do_audit# = 1
                  End!If remove# = 1
                  If do_audit# = 1
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'BOUNCER F.O.C. NO PAYMENT REQUIRED'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                  End!If do_audit# = 1
              End!ccess:jobs.fetch(job:ref_number_key) = Level:Benign
      
              brw1.resetsort(1)
              brw6.resetsort(1)
          Of 1 ! No Button
      End ! Case Missive
    OF ?View_Historic_Job
      ThisWindow.Update
      !view historic job
      
      !added when viewed by thin client - J 28/11/02 - ref L328
      if glo:webjob then
          access:subtracc.clearkey(sub:Account_Number_Key)
          sub:Account_Number = job_ali:Account_Number
          if access:subtracc.fetch(sub:Account_Number_Key) = level:benign then
              if sub:Main_Account_Number <> ClarioNET:Global.Param2
                 !not booked here - can't view it
                  Case Missive('You cannot view this historic job as it was not booked by your Trade Account.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                 Cycle
              END !If account does not match
          END!If access subtrac
      END !if glo:webjob
      !end of addition
      
      brw6.updatebuffer()
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = job_ali:ref_number
      access:jobs.fetch(job:ref_number_key)
      Globalrequest   = changerecord
      updatejobs
      brw1.resetsort(1)
      brw6.resetsort(1)
    OF ?Compare_Fault_Codes
      ThisWindow.Update
      Compare_Fault_Codes(job:ref_number,job_ali:ref_number)
      ThisWindow.Reset
    OF ?Submit_For_Invoice:2
      ThisWindow.Update
      brw1.updatebuffer()
      brw6.updatebuffer()
      Case Missive('Are you sure you want to submit this job for Claiming?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Set(defaults)
              access:defaults.next()
              do_audit# = 0
              Remove# = 0
      
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number  = brw6.q.job:ref_number
              If access:jobs.fetch(job:ref_number_key) = Level:Benign
                  If job:warranty_job <> 'YES'
                      Case Missive('The selected job is not marked as "Warranty".','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else!If job:warranty_job <> 'YES'
      !                tmp:SecondEntryReason = ConfirmSecondEntry()
      
      !                If tmp:SecondEntryReason <> ''
      !                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
      !                    jobse:RefNumber = job:Ref_Number
      !                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !                        !Found
      !                        jobe:WConfirmSecondEntry    = tmp:SecondEntryReason
      !                        jobe:BouncerClaim = 1
      !                        Access:JOBSE.Update()
      !                    Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !                        !Error
      !                        !Assert(0,'<13,10>Fetch Error<13,10>')
      !                    End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                          Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                          rtd:Manufacturer = job:Manufacturer
                          rtd:Warranty   = 'YES'
                          rtd:Repair_Type  = GETINI('BOUNCER','WarrantyRepairType',,CLIP(PATH())&'\SB2KDEF.INI')
                          If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                              !Found
                              !Do not reprice Warranty Jobs - L945 (DBH: 04-09-2003)
      
                          Else!If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End!If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
      
                          If job:chargeable_job = 'YES'
                              If job:bouncer_type = 'BOT' or job:Bouncer_Type = 'CHA'
                                  job:bouncer_type = 'CHA'
                                  Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                                  mod:Model_Number  = job:Model_Number
                                  If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                    !Found
                                      job:EDI   = PendingJob(mod:Manufacturer)
                                      job:Manufacturer  = mod:Manufacturer
                                  Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      
                                  access:jobs.update()
                                  do_audit# = 1
                              Else!If job:bouncer_type = 'BOT'
                                  remove# = 1
                              End!If job:bouncer_type = 'BOT'
                          Else!If job:chargeable_job = 'YES'
                              remove# = 1
                          End!If job:chargeable_job = 'YES'
      
      !                End !If tmp:SecondEntryReason <> ''
      
                  End!If job:warranty_job <> 'YES'
                  If remove# = 1
                      setcursor(cursor:wait)
                      save_bou_id = access:bouncer.savefile()
                      access:bouncer.clearkey(bou:bouncer_job_number_key)
                      bou:original_ref_number = job:ref_number
                      set(bou:bouncer_job_number_key,bou:bouncer_job_number_key)
                      loop
                          if access:bouncer.next()
                             break
                          end !if
                          if bou:original_ref_number <> job:ref_number      |
                              then break.  ! end if
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          Delete(bouncer)
                      end !loop
                      access:bouncer.restorefile(save_bou_id)
                      setcursor()
                      job:bouncer = ''
                      job:bouncer_type = ''
                      If job:edi = 'YEB'
                          job:edi = 'NO'
                      End!If job:edi = 'YEB'
                      If def:qa_required = 'YES'
                          If JOB:QA_Passed = 'YES'
                              GetStatus(610,1,'JOB')
                          Else!If JOB:QA_Passed = 'YES'
                              If job:qa_rejected = 'YES' and job:qa_second_passed <> 'YES'
                                  GetStatus(615,1,'JOB')
                              End!If job:qa_rejected = 'YES' and job:qa_second_passed <> 'YES'
                              If job:qa_rejected = 'YES' and job:qa_second_passed = 'YES'
                                  GetStatus(610,1,'JOB')
                              End!If job:qa_rejected = 'YES' and job:qa_second_passed = 'YES'
                              If job:qa_rejected <> 'YES'
                                  GetStatus(605,1,'JOB')
                              End!If job:qa_rejected <> 'YES'
                          End!If JOB:QA_Passed = 'YES'
                      else!If def:qa_required = 'YES'
                          If ToBeExchanged()
                              !GetStatus(707,1,'JOB')   !Amended by Neil 13/07/2001
                          Else!If ToBeExchanged()
                              !GetStatus(705,1,'JOB')   !Amended by Neil 13/07/2001
                          End!If ToBeExchanged()
                      End!If def:qa_required = 'YES'
      
                      Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                      mod:Model_Number  = job:Model_Number
                      If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                        !Found
                          job:EDI   = PendingJob(mod:Manufacturer)
                          job:Manufacturer  = mod:Manufacturer
                      Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      
                      ! Changing (DBH 10/04/2006) #7252 - Update "in pending browse" date
                      ! access:jobs.update()
                      ! to (DBH 10/04/2006) #7252
                      If Access:JOBS.Update() = Level:Benign
                          If job:EDI = 'NO'
                              Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                              jobe2:RefNumber   = job:Ref_Number
                              If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                  ! Found
                                  jobe2:InPendingDate = Today()
                                  Access:JOBSE2.TryUpdate()
                              Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                  ! Error
                                  If Access:JOBSE2.PrimeRecord() = Level:Benign
                                    jobe2:RefNumber = job:Ref_Number
                                    jobe2:InPendingDate = Today()
                                    If Access:JOBSE2.TryInsert() = Level:Benign
                                        ! Insert Successful
      
                                    Else ! If Access:JOBSE2.TryInsert() = Level:Benign
                                        ! Insert Failed
                                        Access:JOBSE2.CancelAutoInc()
                                    End ! If Access:JOBSE2.TryInsert() = Level:Benign
                                  End !If Access:JOBSE2.PrimeRecord() = Level:Benign
                              End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                          End ! If job:EDI = 'NO'
                      End ! If Access:JOBS.Update() = Level:Benign
                      ! End (DBH 10/04/2006) #7252
      
                      do_audit# = 1
                  End!Case job:bouncer_type
                  If do_audit# = 1
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
      !                    aud:Notes         = 'CONFIRMED SECOND ENTRY: '
      !                    Case tmp:SecondEntryReason
      !                        Of 'ENG'
      !                            aud:Notes = Clip(aud:Notes) & ' ENGINEER'
      !                        Of 'PAR'
      !                            aud:Notes = Clip(aud:Notes) & ' PARTS'
      !                        Of 'MAN'
      !                            aud:Notes = Clip(aud:Notes) & ' MANUFACTURER'
      !                        Of 'UNR'
      !                            aud:Notes = Clip(aud:Notes) & ' UNRELATED'
      !                    End !Case tmp:SecondEntryReason
      
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'BOUNCER SUBMITTED FOR CLAIMING'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                  End!If do_audit# = 1
              End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      
      
              brw1.resetsort(1)
              brw6.resetsort(1)
          Of 1 ! No Button
      End ! Case Missive
    OF ?Reject_From_Invoice:2
      ThisWindow.Update
      brw1.updatebuffer()
      brw6.updatebuffer()
      Case Missive('Are you sure you want to REJECT this job from Claiming?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              do_audit# = 0
              Set(DEFAULTS)
              Access:DEFAULTS.next()
              Access:JOBS.ClearKey(job:Ref_Number_Key)
              job:Ref_Number  = brw6.q.job:Ref_Number
              If access:jobs.fetch(job:ref_number_key) = level:benign
                  If job:warranty_job <> 'YES'
                      Case Missive('The selected job is not marked as "Warranty".','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else!If job:warranty_job <> 'YES'
                      If job:Chargeable_Job = 'YES'
                          If job:Bouncer_Type = 'BOT' or job:Bouncer_Type = 'CHA'
                              job:Bouncer_Type = 'CHA'
                              Access:JOBS.Update()
                              do_audit# = 1
                          Else!If job:bouncer_type = 'BOT'
                              remove# = 1
                          End!If job:bouncer_type = 'BOT'
                      Else!If job:chargeable_job = 'YES'
                          remove# = 1
                      End!If job:chargeable_job = 'YES'
                  End!If job:warranty_job <> 'YES'
                  If remove# = 1
                      setcursor(cursor:wait)
                      save_bou_id = access:bouncer.savefile()
                      access:bouncer.clearkey(bou:bouncer_job_number_key)
                      bou:original_ref_number = job:ref_number
                      set(bou:bouncer_job_number_key,bou:bouncer_job_number_key)
                      loop
                          if access:bouncer.next()
                             break
                          end !if
                          if bou:original_ref_number <> job:ref_number      |
                              then break.  ! end if
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          Delete(bouncer)
                      end !loop
                      access:bouncer.restorefile(save_bou_id)
                      setcursor()
                      job:bouncer = 'R'
                      job:bouncer_type = ''
      
                      !J - TB13473 - when rejecting a bouncer from claiming it actually adds it to the JOBSWARR file
                      !this is wrong - it calls pendingjob(Manufact) - but this is not being set as a pending job - it is being cancelled
                      !pendingJob does nothing useful in this case - because it should be setting to XXX
                      !job:EDI can stay whatever it was before.
      !                Access:MODELNUM.Clearkey(mod:Model_Number_Key)
      !                mod:Model_Number  = job:Model_Number
      !                If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      !                  !Found
      !                    job:EDI   = PendingJob(mod:Manufacturer)
      !                    job:Manufacturer  = mod:Manufacturer
      !                Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      !                  !Error
      !                  !Assert(0,'<13,10>Fetch Error<13,10>')
      !                End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      
                      access:jobs.update()
                      do_audit# = 1
      
                      If do_audit# = 1
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              access:users.clearkey(use:password_key)
                              use:password = glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'BOUNCER F.O.C. NO PAYMENT REQUIRED'
                              access:audit.insert()
                          end!�if access:audit.primerecord() = level:benign
      
                      End!If do_audit# = 1
                  End!If remove# = 1
              End!If access:jobs.fetch(job:ref_number_key) = level:benign
      
              brw1.resetsort(1)
              brw6.resetsort(1)
          Of 1 ! No Button
      End ! Case Missive
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of F3Key
              Post(Event:Accepted,?Change)
          Of F4Key
              Post(Event:Accepted,?View_Historic_Job)
      End !KeyCode()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection
  IF job_ali:Chargeable_Job <> 'YES'
      ?job_ali:Charge_Type{Prop:Hide} = TRUE
      ?job_ali:Charge_Type:prompt{prop:Hide} = 1
      ?job_ali:Repair_Type{Prop:Hide} = TRUE
      ?job_ali:Repair_Type:prompt{prop:Hide} = 1
  ELSE
      ?job_ali:Charge_Type{Prop:Hide} = FALSE
      ?job_ali:Charge_Type:Prompt{prop:Hide} = 0
      ?job_ali:Repair_Type{Prop:Hide} = FALSE
      ?job_ali:Repair_Type{prop:Hide} = 0
  END
  
  IF job_ali:Warranty_Job <> 'YES'
      ?job_ali:Warranty_Charge_Type{Prop:Hide} = TRUE
      ?job_ali:Warranty_Charge_Type:Prompt{prop:Hide} = 1
      ?job_ali:Repair_Type_Warranty{Prop:Hide} = TRUE
      ?job_ali:Repair_Type_Warranty:Prompt{prop:Hide} = 1
  ELSE
      ?job_ali:Warranty_Charge_Type{Prop:Hide} = FALSE
      ?job_ali:Warranty_Charge_Type:Prompt{prop:Hide} = 0
      ?job_ali:Repair_Type_Warranty{Prop:Hide} = FALSE
      ?job_ali:Repair_Type_Warranty:Prompt{prop:Hide} = 0
  END
  
  If job_ali:Engineer = ''
      ?tmp:CurrentEngineer2{prop:Hide} = 1
      ?tmp:CurrentEngineer2:Prompt{prop:Hide} = 1
  Else !job:Engineer <> ''
      ?tmp:CurrentEngineer2{prop:Hide} = 0
      ?tmp:CurrentEngineer2:Prompt{prop:Hide} = 0
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code    = job_ali:Engineer
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Found
          tmp:CurrentEngineer2 = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  
  End !job:Engineer <> ''
  
  If job_ali:Third_Party_Site <> ''
      ?job_ali:Third_Party_Site{prop:Hide} = 0
      ?job_ali:Third_Party_Site:Prompt{prop:Hide} = 0
  Else !job:Third_Party_Site <> ''
      ?job_ali:Third_Party_Site{prop:Hide} = 1
      ?job_ali:Third_Party_Site:Prompt{prop:Hide} = 1
  End !job:Third_Party_Site <> ''
  
  If job_ali:Exchange_Unit_Number <> 0
      ?tmp:ExchangeUnit2{prop:Hide} = 0
      ?tmp:ExchangeUnit2:Prompt{prop:Hide} = 0
      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
      xch:Ref_Number  = job_ali:Exchange_Unit_Number
      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Found
          If xch:MSN <> ''
              tmp:ExchangeUnit = Clip(xch:Model_Number) & '  -  ' & Clip(xch:ESN) & '  -  ' & Clip(xch:MSN)
          Else !If xch:MSN <> ''
              tmp:ExchangeUnit = Clip(xch:Model_Number) & '  -  ' & Clip(xch:ESN)
          End !If xch:MSN <> ''
  
      Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          tmp:ExchangeUnit2 = 'Cannot find unit!'
      End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
  
  Else !job:Exchange_Unit_Number <> 0
      ?tmp:ExchangeUnit2{prop:Hide} = 1
      ?tmp:ExchangeUnit2:Prompt{prop:Hide} = 1
      tmp:ExchangeUnit2 = ''
  End !job:Exchange_Unit_Number <> 0
  
  tmp:HistoricUnitDetails = Clip(job_ali:Model_Number) & ' ' & Clip(job_ali:Manufacturer)


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW6.SetQueueRecord PROCEDURE

  CODE
  alias_locator_temp = job:Ref_Number
  PARENT.SetQueueRecord


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection
  IF job:Chargeable_Job <> 'YES'
      ?job:Charge_Type{Prop:Hide} = TRUE
      ?job:Charge_Type:Prompt{prop:Hide} = 1
      ?job:Repair_Type{Prop:Hide} = TRUE
      ?job:Repair_Type:Prompt{prop:Hide} = 1
      ?TransferCharToWarr{prop:Disable} = 1
  
  ELSE
      ?job:Charge_Type{Prop:Hide} = FALSE
      ?job:Charge_Type:Prompt{prop:Hide} = 0
      ?job:Repair_Type{Prop:Hide} = FALSE
      ?job:Repair_Type:Prompt{prop:Hide} = 0
      ?TransferCharToWarr{prop:Disable} = 0
  END
  
  IF job:Warranty_Job <> 'YES'
      ?job:Warranty_Charge_Type{Prop:Hide} = TRUE
      ?job:Repair_Type_Warranty{Prop:Hide} = TRUE
      ?job:Warranty_Charge_Type:Prompt{Prop:Hide} = TRUE
      ?job:Repair_Type_Warranty:Prompt{Prop:Hide} = TRUE
      ?TransferWarrToChar{prop:Disable} = 1
  
  ELSE
      ?job:Warranty_Charge_Type{Prop:Hide} = FALSE
      ?job:Repair_Type_Warranty{Prop:Hide} = FALSE
      ?job:Warranty_Charge_Type:Prompt{Prop:Hide} = FALSE
      ?job:Repair_Type_Warranty:Prompt{Prop:Hide} = FALSE
      ?TransferWarrToChar{prop:Disable} = 0
  END
  
  If job:Engineer = ''
      ?tmp:CurrentEngineer{prop:Hide} = 1
      ?tmp:CurrentEngineer:Prompt{prop:Hide} = 1
  Else !job:Engineer <> ''
      ?tmp:CurrentEngineer{prop:Hide} = 0
      ?tmp:CurrentEngineer:prompt{prop:Hide} = 0
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code    = job:Engineer
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Found
          tmp:CurrentEngineer = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  
  End !job:Engineer <> ''
  
  If job:Third_Party_Site <> ''
      ?job:Third_Party_Site{prop:Hide} = 0
      ?job:Third_Party_Site:Prompt{prop:Hide} = 0
  Else !job:Third_Party_Site <> ''
      ?job:Third_Party_Site{prop:Hide} = 1
      ?job:Third_Party_Site:Prompt{prop:Hide} = 1
  End !job:Third_Party_Site <> ''
  
  If job:Exchange_Unit_Number <> 0
      ?tmp:ExchangeUnit{prop:Hide} = 0
      ?tmp:ExchangeUnit:Prompt{prop:Hide} = 0
      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
      xch:Ref_Number  = job:Exchange_Unit_Number
      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Found
          If xch:MSN <> ''
              tmp:ExchangeUnit = Clip(xch:Model_Number) & '  -  ' & Clip(xch:ESN) & '  -  ' & Clip(xch:MSN)
          Else !If xch:MSN <> ''
              tmp:ExchangeUnit = Clip(xch:Model_Number) & '  -  ' & Clip(xch:ESN)
          End !If xch:MSN <> ''
  
      Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          tmp:ExchangeUnit = 'Cannot find unit!'
      End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
  
  Else !job:Exchange_Unit_Number <> 0
      ?tmp:ExchangeUnit{prop:Hide} = 1
      ?tmp:ExchangeUnit:Prompt{prop:Hide} = 1
      tmp:ExchangeUnit = ''
  End !job:Exchange_Unit_Number <> 0
  
  tmp:CurrentUnitDetails  = Clip(job:Model_Number) & ' ' & Clip(job:Manufacturer)
  


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  !filter if thin client
  !added when viewed by thin client - J 28/11/02 - ref L328
  if glo:webjob then
      access:subtracc.clearkey(sub:Account_Number_Key)
      sub:Account_Number = job:Account_Number
      if access:subtracc.fetch(sub:Account_Number_Key) = level:benign then
          if sub:Main_Account_Number <> ClarioNET:Global.Param2
              return(record:filtered)
          END !If account does not match
      END!If access subtrac
  END !if glo:webjob
  !end of addition
  BRW6::RecordStatus=ReturnValue
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 20
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
Update_Contacts PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::con:Record  LIKE(con:RECORD),STATIC
QuickWindow          WINDOW('Update the CONTACTS File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Contact'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Name'),AT(231,130),USE(?CON:Name:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(319,130,124,10),USE(con:Name),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Postcode'),AT(231,152),USE(?CON:Postcode:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s10),AT(319,152,64,10),USE(con:Postcode),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(387,146),USE(?Clear_Address),SKIP,TRN,FLAT,LEFT,ICON('clearp.jpg')
                           PROMPT('Address'),AT(231,174),USE(?CON:Address_Line1:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(319,174,124,10),USE(con:Address_Line1),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(319,188,124,10),USE(con:Address_Line2),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(319,200,124,10),USE(con:Address_Line3),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(231,216),USE(?CON:Telephone_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(319,216,124,10),USE(con:Telephone_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Fax Number'),AT(231,232),USE(?CON:Fax_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(319,232,124,10),USE(con:Fax_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Contact Name 1'),AT(231,248),USE(?CON:Contact_Name1:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(319,248,124,10),USE(con:Contact_Name1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Contact Name 2'),AT(231,264),USE(?CON:Contact_Name2:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(319,264,124,10),USE(con:Contact_Name2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Email Address'),AT(231,280),USE(?con:EmailAddress:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(319,280,124,10),USE(con:EmailAddress),LEFT,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Contact'
  OF ChangeRecord
    ActionMessage = 'Changing A Contact'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020406'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Contacts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(con:Record,History::con:Record)
  SELF.AddHistoryField(?con:Name,1)
  SELF.AddHistoryField(?con:Postcode,2)
  SELF.AddHistoryField(?con:Address_Line1,3)
  SELF.AddHistoryField(?con:Address_Line2,4)
  SELF.AddHistoryField(?con:Address_Line3,5)
  SELF.AddHistoryField(?con:Telephone_Number,6)
  SELF.AddHistoryField(?con:Fax_Number,7)
  SELF.AddHistoryField(?con:Contact_Name1,8)
  SELF.AddHistoryField(?con:Contact_Name2,9)
  SELF.AddHistoryField(?con:EmailAddress,10)
  SELF.AddUpdateFile(Access:CONTACTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CONTACTS.Open
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:CONTACTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Update_Contacts')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CONTACTS.Close
    Relate:DEFAULTS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Contacts')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020406'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020406'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020406'&'0')
      ***
    OF ?con:Postcode
      Postcode_Routine(con:postcode,con:address_line1,con:address_line2,con:address_line3)
      If con:address_line1 <> ''
          Select(?con:address_line1,1)
      End
      Display()
    OF ?Clear_Address
      ThisWindow.Update
      Case Missive('Are you sure you want to clear the Address?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              CON:Postcode = ''
              CON:Address_Line1 = ''
              CON:Address_Line2 = ''
              CON:Address_Line3 = ''
              Select(?con:postcode)
              Display()
          Of 1 ! No Button
      End ! Case Missive
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Contacts PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(CONTACTS)
                       PROJECT(con:Name)
                       PROJECT(con:Telephone_Number)
                       PROJECT(con:Fax_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
con:Name               LIKE(con:Name)                 !List box control field - type derived from field
con:Telephone_Number   LIKE(con:Telephone_Number)     !List box control field - type derived from field
con:Fax_Number         LIKE(con:Fax_Number)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Contacts File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Contact File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,112,276,212),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('124L(2)|M~Name~@s30@65L(2)|M~Telephone No~@s15@66L(2)|M~Fax Number~@s15@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,236),USE(?Insert:2),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(448,268),USE(?Change:2),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(448,300),USE(?Delete:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Name'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,124,10),USE(con:Name),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020405'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Contacts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CONTACTS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:CONTACTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Contacts')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,con:Name_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?con:Name,con:Name,1,BRW1)
  BRW1.AddField(con:Name,BRW1.Q.con:Name)
  BRW1.AddField(con:Telephone_Number,BRW1.Q.con:Telephone_Number)
  BRW1.AddField(con:Fax_Number,BRW1.Q.con:Fax_Number)
  QuickWindow{PROP:MinWidth}=394
  QuickWindow{PROP:MinHeight}=118
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CONTACTS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Contacts')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of insertrecord
          check_access('CONTACTS - INSERT',x")
          if x" = false
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
      of changerecord
          check_access('CONTACTS - CHANGE',x")
          if x" = false
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
      of deleterecord
          check_access('CONTACTS - DELETE',x")
          if x" = false
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
  end !case request
  
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Contacts
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020405'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020405'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020405'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?con:Name
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CON:Name, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

