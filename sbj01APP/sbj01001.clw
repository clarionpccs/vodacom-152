

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01001.INC'),ONCE        !Local module procedure declarations
                     END


Show_Addresses PROCEDURE (<Byte f:NewBooking>)        !Generated from procedure template - Window

save:IDNumber        STRING(13)
tmp:Hub              STRING(30)
tmp:ForceHub         BYTE(0)
tmp:HubType          BYTE(0)
save:SuburbCustomer  STRING(30)
save:SuburbCollection STRING(30)
save:SuburbDelivery  STRING(30)
save:HubCustomer     STRING(30)
save:HubDelivery     STRING(30)
LoyaltyStatus        STRING(250)
C3D_Generated        BYTE
!! ** Bryan Harrison (c)1998 **
temp_string         String(255)
window               WINDOW('Address Details'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('Address Details'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       GROUP,AT(64,54,552,310),USE(?Group:Whole)
                         PANEL,AT(64,54,552,310),USE(?Panel5),FILL(09A6A7CH)
                         PROMPT('Customer Address'),AT(68,58),USE(?InvoiceAddress),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Customer Name'),AT(68,70),USE(?JOB:CompanyName:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(148,70,124,10),USE(job:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         PROMPT('Address'),AT(68,82),USE(?JOB:AddressLine1:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(148,82,124,10),USE(job:Address_Line1),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         ENTRY(@s30),AT(148,94,124,10),USE(job:Address_Line2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         PROMPT('Suburb'),AT(68,106),USE(?JOB:AddressLine1:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(148,106,124,10),USE(job:Address_Line3),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                         BUTTON,AT(272,102),USE(?LookupSuburb1),TRN,FLAT,ICON('lookupp.jpg')
                         PROMPT('Postcode'),AT(68,124),USE(?JOB:Postcode:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s10),AT(148,124,64,10),USE(job:Postcode),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                         PROMPT('Hub'),AT(68,138),USE(?jobe2:HubCustomer:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(148,138,64,10),USE(jobe2:HubCustomer),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Hub'),TIP('Hub'),UPR,READONLY
                         PROMPT('Contact Numbers'),AT(68,160),USE(?JOB:ColTelephoneNo:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Telephone No'),AT(148,152),USE(?JOB:TelephoneNumber:Prompt),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         ENTRY(@s15),AT(148,160,56,10),USE(job:Telephone_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         PROMPT('Fax Number'),AT(216,152),USE(?JOB:FaxNumber:Prompt),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         ENTRY(@s15),AT(216,160,56,10),USE(job:Fax_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         PROMPT('Collection Address'),AT(300,58),USE(?CollectionAddress),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(300,70,124,10),USE(job:Company_Name_Collection),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         ENTRY(@s30),AT(300,82,124,10),USE(job:Address_Line1_Collection),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         ENTRY(@s30),AT(300,94,124,10),USE(job:Address_Line2_Collection),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         ENTRY(@s30),AT(300,106,124,10),USE(job:Address_Line3_Collection),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                         BUTTON,AT(424,102),USE(?LookupSuburb2),TRN,FLAT,ICON('lookupp.jpg')
                         BUTTON,AT(372,120),USE(?CollClearCopy),SKIP,TRN,FLAT,LEFT,ICON('clrcopp.jpg')
                         ENTRY(@s10),AT(300,124,64,10),USE(job:Postcode_Collection),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                         ENTRY(@s30),AT(300,138,64,10),USE(jobe2:HubCollection),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Hub'),TIP('Hub'),UPR,READONLY
                         PROMPT('Telephone Number'),AT(300,152),USE(?JOB:TelephoneNumber:Prompt:2),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         ENTRY(@s15),AT(300,160,56,10),USE(job:Telephone_Collection),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         PROMPT('Delivery Address'),AT(460,58),USE(?DeliveryAddress),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(460,70,124,10),USE(job:Company_Name_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         ENTRY(@s30),AT(460,82,124,10),USE(job:Address_Line1_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         ENTRY(@s30),AT(460,94,124,10),USE(job:Address_Line2_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         ENTRY(@s30),AT(460,106,124,10),USE(job:Address_Line3_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                         BUTTON,AT(584,102),USE(?LookupSuburb3),TRN,FLAT,ICON('lookupp.jpg')
                         ENTRY(@s10),AT(460,124,64,10),USE(job:Postcode_Delivery),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                         BUTTON,AT(528,120),USE(?DelClearCopy),SKIP,TRN,FLAT,LEFT,ICON('clrcopp.jpg')
                         ENTRY(@s30),AT(460,138,64,10),USE(jobe2:HubDelivery),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Hub'),TIP('Hub'),UPR,READONLY
                         PROMPT('Telephone Number'),AT(460,152),USE(?JOB:DelTelephoneNo:Prompt),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         ENTRY(@s15),AT(460,160,56,10),USE(job:Telephone_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         PROMPT('Email Address'),AT(68,174),USE(?jobe:EndUserEmailAddress:Prompt),TRN,FONT('Tahoma',8,COLOR:White,,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s255),AT(148,174,124,10),USE(jobe:EndUserEmailAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address')
                         PROMPT('Outgoing Courier'),AT(68,190),USE(?job:courier:prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(148,190,124,10),USE(job:Courier),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address'),READONLY
                         BUTTON,AT(272,186),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                         PROMPT('Courier Waybill No'),AT(68,204),USE(?jobe2:CourierWaybillNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(148,204,124,10),USE(jobe2:CourierWaybillNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Courier Waybill Number'),TIP('Courier Waybill Number'),UPR
                         PROMPT('End User Tel No'),AT(68,218),USE(?jobe:EndUserTelNo:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s15),AT(148,218,124,10),USE(jobe:EndUserTelNo),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         PROMPT('Vat Number'),AT(68,232),USE(?jobe:VatNumber:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(148,232,124,10),USE(jobe:VatNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Vat Number'),TIP('Vat Number'),UPR
                         PROMPT('I.D. Number'),AT(68,246),USE(?jobe2:IDNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s13),AT(148,246,124,10),USE(jobe2:IDNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('I.D. Number'),TIP('I.D. Number'),UPR
                         CHECK('SMS Notification'),AT(148,260),USE(jobe2:SMSNotification),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('SMS Notification'),TIP('SMS Notification'),VALUE('1','0')
                         CHECK('Email Notification'),AT(148,272),USE(jobe2:EmailNotification),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Email Notification'),TIP('Email Notification'),VALUE('1','0')
                         PROMPT('SMS Alert Mobile No'),AT(68,284),USE(?jobe2:SMSAlertNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(148,284,124,10),USE(jobe2:SMSAlertNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('SMS Alert Mobile Number'),TIP('SMS Alert Mobile Number'),UPR
                         PROMPT('Email Alert Address'),AT(68,298),USE(?jobe2:EmailAlertAddress:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s255),AT(148,298,124,10),USE(jobe2:EmailAlertAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Email Alert Address'),TIP('Email Alert Address')
                         TEXT,AT(148,312,124,48),USE(LoyaltyStatus),VSCROLL,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                         PROMPT('Loyalty Status'),AT(68,312),USE(?PromptLoyaltyStatus),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                         PROMPT('Account Number'),AT(460,180),USE(?jobe:Sub_Sub_Account:Prompt),TRN,FONT('Tahoma',7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         ENTRY(@s15),AT(460,188,124,10),USE(jobe:Sub_Sub_Account),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                         BUTTON,AT(460,204),USE(?BrowseAddresses),TRN,FLAT,HIDE,LEFT,ICON('accaddp.jpg')
                         PROMPT('Collection Text'),AT(300,230),USE(?jbn:Collection_Text:Prompt),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         TEXT,AT(300,238,124,42),USE(jbn:Collection_Text),SKIP,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                         BUTTON,AT(300,284),USE(?CollectionTextButton),TRN,FLAT,ICON('coltextp.jpg')
                         PROMPT('Delivery Text'),AT(460,230),USE(?JOB:TelephoneNumber:Prompt:4),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         TEXT,AT(460,238,124,42),USE(jbn:Delivery_Text),SKIP,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                         BUTTON,AT(460,284),USE(?DeliveryTextButton),TRN,FLAT,ICON('deltextp.jpg')
                       END
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(548,366),USE(?OK),TRN,FLAT,LEFT,KEY(F4Key),ALRT(F4Key),ICON('okp.jpg')
                       BUTTON,AT(72,366),USE(?ButtonAmendC3D),TRN,FLAT,HIDE,ICON('AmendC3D.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
szpostcode      cstring(15)
szpath          cstring(255)
szaddress       cstring(255)
!Save Entry Fields Incase Of Lookup
look:job:Courier                Like(job:Courier)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

Transit_Type:Display_Collection_Address Routine
    Access:TranType.ClearKey(trt:Transit_Type_Key)
    trt:Transit_Type = job:Transit_Type
    if Access:TranType.Fetch(trt:Transit_Type_Key) then exit.

    if trt:Collection_Address <> 'YES'
        hide(?CollectionAddress)
        hide(?job:Company_Name_Collection)
        hide(?job:Postcode_Collection)
        hide(?CollClearCopy)
        hide(?job:Address_Line1_Collection)
        hide(?job:Address_Line2_Collection)
        hide(?job:Address_Line3_Collection)
        hide(?JOB:TelephoneNumber:Prompt:2)
        hide(?job:Telephone_Collection)
        ?jbn:Collection_text{prop:Hide} = 1
        ?jbn:Collection_text:Prompt{prop:Hide} = 1
        ?jbn:Collection_text{prop:Hide} = 1
        ?CollectionTextButton{prop:Hide} = 1
        ?LookupSuburb2{prop:Hide} = 1
        ?jobe2:HubCollection{prop:Hide} = 1
    end

    ! #11880 The compulsory-ness of the waybill is determined by the Transit Type setup. (Bryan: 05/05/2011)
    IF (trt:WaybillComBook = 1 AND trt:WaybillComComp = 1)
        ?jobe2:CourierWaybillNumber{prop:req} = 1
    END ! trt:WaybillComComp

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020420'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Show_Addresses')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:C3DMONIT.Open
  Relate:COURIER.Open
  Relate:JOBSE3.Open
  Relate:SUBURB.Open
  Relate:TRANTYPE.Open
  Relate:USERS_ALIAS.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:SUBACCAD.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSE2.UseFile
  Access:TRAHUBS.UseFile
  SELF.FilesOpened = True
  Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Found
      save:IDNumber   = jobe2:IDNumber
      ! Inserting (DBH 06/02/2008) # 9613 - Autofill the hub for Cash Accounts (don't see the point of this)
      If Instring('CASH',job:Account_Number,1,1) And f:NewBooking = 1
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number = wob:HeadAccountNumber
          If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
          End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          If jobe2:HubCustomer = ''
              jobe2:HubCustomer = tra:Hub
          End ! If jobe2:HubCustomer = ''
          If jobe2:HubCollection = ''
              jobe2:HubCollection = tra:Hub
          End ! If jobe2:HubCollection = ''
          If jobe2:HubDelivery = ''
              jobe2:HubDelivery = tra:Hub
          End ! If jobe2:HubDelivery = ''
      End ! If Instring('CASH',job:Account_Number,1,1)
      ! End (DBH 06/02/2008) #9613
  End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
  
  
  !TB13224 - J - 21/10/14 - show loyalty status on this screen
  Access:jobse3.clearkey(jobe3:KeyRefNumber)
  jobe3:RefNumber = job:Ref_Number
  if access:jobse3.fetch(jobe3:KeyRefNumber)
      LoyaltyStatus = 'NOT CAPTURED'
  ELSE
      LoyaltyStatus = jobe3:LoyaltyStatus
  END
  !End TB13224
  
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Force Fields Check
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  If ForcePostcode('B')
      ?job:Postcode{prop:Req} = 1
  Else
      ?job:Postcode{prop:Req} = 0
  End !ForcePostcode('B')
  
  If ForceDeliveryPostcode('B')
      ?job:Postcode_Delivery{prop:Req} = 1
  Else !ForceDeliveryPostcode('B')
      ?job:Postcode_Delivery{prop:Req} = 0
  End !ForceDeliveryPostcode('B')
  
  If ForceCourier('B')
      ?job:Courier{prop:Req} = 1
      ?job:courier{prop:background} = 080FFFFH   !tb13225 - manually colour this read only field
  Else !ForceCourier('B')
      ?job:Courier{prop:Req} = 0
      ?job:Courier{prop:background} = color:white
  End !ForceCourier('B')
  
  If InvoiceSubAccounts(job:Account_Number)
      If SecurityCheck('ADDRESS MANIPULATION')
          If ~sub:ChangeInvAddress
              ?job:Company_Name{prop:Disable} = 1
              ?job:Address_Line1{prop:Disable} = 1
              ?job:Address_Line2{prop:Disable} = 1
              ?job:Address_Line3{prop:Disable} = 1
              ?job:Postcode{prop:Disable} = 1
              ?job:Telephone_Number{prop:Disable} = 1
              ?job:Fax_Number{prop:Disable} = 1
              ?jobe:EndUserTelNo{prop:Disable} = 1
              ?LookupSuburb1{prop:Disable} = 1
              ?jobe2:HubCustomer{prop:Disable} = 1
          End !If ~sub:ChangeInvAddress
  
          If ~sub:ChangeCollAddress
              ?job:Company_Name_Collection{prop:Disable} = 1
              ?job:Address_Line1_Collection{prop:Disable} = 1
              ?job:Address_Line2_Collection{prop:Disable} = 1
              ?job:Address_Line3_Collection{prop:Disable} = 1
              ?job:Postcode_Collection{prop:Disable} = 1
              ?job:Telephone_Collection{prop:Disable} = 1
              ?LookupSuburb2{prop:Disable} = 1
              ?jobe2:HubCollection{prop:Disable} = 1
          End !If ~sub:ChangeColAddress
  
          If ~sub:ChangeDelAddress
              ?job:Company_Name_Delivery{prop:Disable} = 1
              ?job:Address_Line1_Delivery{prop:Disable} = 1
              ?job:Address_Line2_Delivery{prop:Disable} = 1
              ?job:Address_Line3_Delivery{prop:Disable} = 1
              ?job:Postcode_Delivery{prop:Disable} = 1
              ?job:Telephone_Delivery{prop:Disable} = 1
              ?LookupSuburb3{prop:Disable} = 1
              ?jobe2:HubDelivery{prop:Disable} = 1
          End !If ~sub:ChangeDelAddress
      End ! If SecurityCheck('ADDRESS MANIPULATION')
  
  Else !InvoiceSubAccounts(job:Account_Number)
      If SecurityCheck('ADDRESS MANIPULATION')
          If ~tra:ChangeInvAddress
              ?job:Company_Name{prop:Disable} = 1
              ?job:Address_Line1{prop:Disable} = 1
              ?job:Address_Line2{prop:Disable} = 1
              ?job:Address_Line3{prop:Disable} = 1
              ?job:Postcode{prop:Disable} = 1
              ?job:Telephone_Number{prop:Disable} = 1
              ?job:Fax_Number{prop:Disable} = 1
              ?jobe:EndUserTelNo{prop:Disable} = 1
              ?LookupSuburb1{Prop:Disable} = 1
              ?jobe2:HubCustomer{prop:Disable} = 1
          End !If ~sub:ChangeInvAddress
  
          If ~tra:ChangeCollAddress
              ?job:Company_Name_Collection{prop:Disable} = 1
              ?job:Address_Line1_Collection{prop:Disable} = 1
              ?job:Address_Line2_Collection{prop:Disable} = 1
              ?job:Address_Line3_Collection{prop:Disable} = 1
              ?job:Postcode_Collection{prop:Disable} = 1
              ?job:Telephone_Collection{prop:Disable} = 1
              ?LookupSuburb2{prop:Disable} = 1
              ?jobe2:HubCollection{prop:Disable} = 1
          End !If ~sub:ChangeColAddress
  
          If ~tra:ChangeDelAddress
              ?job:Company_Name_Delivery{prop:Disable} = 1
              ?job:Address_Line1_Delivery{prop:Disable} = 1
              ?job:Address_Line2_Delivery{prop:Disable} = 1
              ?job:Address_Line3_Delivery{prop:Disable} = 1
              ?job:Postcode_Delivery{prop:Disable} = 1
              ?job:Telephone_Delivery{prop:Disable} = 1
              ?LookupSuburb3{prop:Disable} = 1
              ?jobe2:HubDelivery{prop:Disable} = 1
          End !If ~sub:ChangeDelAddress
  
      End ! If SecurityCheck('ADDRESS MANIPULATION')
  
  End !InvoiceSubAccounts(job:Account_Number)
  
  ! Start - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
  If sub:Vat_Number = '' AND sub:OverrideHeadVATNo = True
      ?jobe:VATNumber{prop:Hide} = False
      ?jobe:VATNumber:Prompt{prop:Hide} = False
  Else ! sub:Vat_Number = '' AND tra:Vat_Number = ''
      ?jobe:VATNumber{prop:Hide} = True
      ?jobe:VATNumber:Prompt{prop:Hide} = True
  End ! sub:Vat_Number = '' AND tra:Vat_Number = ''
  ! End   - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
  
  Do Transit_Type:Display_Collection_Address
  
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number  = job:Account_Number
  If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      !Found
      IF sub:UseAlternativeAdd THEN
         UNHIDE(?BrowseAddresses)
      END !IF
      IF sub:Force_Customer_Name
        ?job:Company_Name{prop:Req} = 1
      Else !ForceDeliveryPostcode('B')
        ?job:Company_Name{prop:Req} = 0
      End !ForceDeliveryPostcode('B')
  
      If sub:UseCustDespAdd = 'YES'
          tmp:HubType = 1
      Else ! If sub:UseCustDespAdd = 'YES'
          tmp:HubType = 2
      End ! If sub:UseCustDespAdd = 'YES'
  
  Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  
  ! #11880 The compulsory-ness of the waybill is determined by the Transit Type setup. (Bryan: 05/05/2011)
      ! Inserting (DBH 24/01/2008) # 9612 - Make field required
      !If job:Transit_Type = 'ARRIVED FRANCHISE'
      !    ?jobe2:CourierWaybillNumber{prop:Req} = True
      !End ! If job:Transit_Type = 'ARRIVED FRANCHISE'
      ! End (DBH 24/01/2008) #9612
  
  
  !View Only?
  If glo:Preview = 'V'
      ?Group:Whole{prop:Disable} = 1
  End !glo:Preview = 'V'
  !new booking?
  If f:NewBooking <> 1
      If SecurityCheck('AMEND COURIER WAYBILL NUMBER')
          ?jobe2:CourierWayBillNumber{prop:ReadOnly} = 1
          ?jobe2:CourierWayBillNumber{prop:Skip} = 1
      End ! If SecurityCheck('AMEND COURIER WAYBILL NUMBER')
      If f:NewBooking <> 1
          If SecurityCheck('AMEND HUB')
              tmp:ForceHub = 1
          End ! If SecurityCheck('AMEND HUB')
      End ! If f:NewBooking <> 1
  End ! If f:NewBooking = 1
  
  !TB13224 - J - 22/10/14 - If C3D Generated do disable customer fields
  !if this was generated by C3d then there is an OK record in C3dMonit
  Access:C3DMonit.clearkey(C3D:KeyRefNumber)
  C3D:RefNumber = Job:Ref_number
  set(C3D:KeyRefNumber,C3D:KeyRefNumber)
  Loop
      if access:C3DMonit.next() then break.
      if C3D:RefNumber <> Job:Ref_number then break.
      if C3D:ErrorCode = 'OK' then
  
          !show the button
          unhide(?ButtonAmendC3D)
  
          !disable the fields
          disable(?job:Company_Name)
          disable(?job:Address_Line1)
          disable(?job:Address_Line2)
          disable(?job:Address_Line3)
          disable(?job:Postcode)
          disable(?job:Telephone_Number)
          disable(?job:Fax_Number)
          disable(?jobe:EndUserEmailAddress)
  
          break
      END !if OK
  END !loop
  !End TB13224
  ! Inserting (DBH 04/08/2006) # 6643 - Find suburb? Was the postcode manually entered
  If ThisWindow.Request = ChangeRecord
      ! If edited the record and cannot find the suburb, assume that the postcode was manually entered. (DBH: 04/08/2006)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = job:Address_Line3
      If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Found
          ?job:Postcode{prop:Skip} = 1
          ?job:Postcode{prop:ReadOnly} = 1
          ?jobe2:HubCustomer{prop:Skip} = 1
          ?jobe2:HubCustomer{prop:ReadOnly} = 1
      Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Error
          ?job:Postcode{prop:Skip} = 0
          ?job:Postcode{prop:ReadOnly} = 0
          ?jobe2:HubCustomer{prop:Skip} = 0
          ?jobe2:HubCustomer{prop:ReadOnly} = 0
      End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = job:Address_Line3_Collection
      If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Found
          ?job:Postcode_Collection{prop:Skip} = 1
          ?job:Postcode_Collection{prop:ReadOnly} = 1
          ?jobe2:HubCollection{prop:Skip} = 1
          ?jobe2:HubCollection{prop:ReadOnly} = 1
      Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Error
          ?job:Postcode_Collection{prop:Skip} = 0
          ?job:Postcode_Collection{prop:ReadOnly} = 0
          ?jobe2:HubCollection{prop:Skip} = 0
          ?jobe2:HubCollection{prop:ReadOnly} = 0
      End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = job:Address_Line3_Delivery
      If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Found
          ?job:Postcode_Delivery{prop:Skip} = 1
          ?job:Postcode_Delivery{prop:ReadOnly} = 1
          ?jobe2:HubDelivery{prop:Skip} = 1
          ?jobe2:HubDelivery{prop:ReadOnly} = 1
      Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Error
          ?job:Postcode_Delivery{prop:Skip} = 0
          ?job:Postcode_Delivery{prop:ReadOnly} = 0
          ?jobe2:HubDelivery{prop:Skip} = 0
          ?jobe2:HubDelivery{prop:ReadOnly} = 0
      End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
  End ! If ThisWindow.Request = ChangeRecord
  ! End (DBH 04/08/2006) #6643
  !Save Fields
  save:SuburbCustomer = job:Address_Line3
  save:SuburbCollection = job:Address_line3_Collection
  save:SuburbDelivery = job:Address_line3_Delivery
  save:HubCustomer = jobe2:HubCustomer
  save:HubDelivery = jobe2:HubDelivery
  ! Save Window Name
   AddToLog('Window','Open','Show_Addresses')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?job:Courier{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?job:Courier{Prop:Tip}
  END
  IF ?job:Courier{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?job:Courier{Prop:Msg}
  END
  IF ?jobe2:SMSNotification{Prop:Checked} = True
    UNHIDE(?jobe2:SMSAlertNumber:Prompt)
    UNHIDE(?jobe2:SMSAlertNumber)
  END
  IF ?jobe2:SMSNotification{Prop:Checked} = False
    jobe2:SMSAlertNumber = ''
    HIDE(?jobe2:SMSAlertNumber:Prompt)
    HIDE(?jobe2:SMSAlertNumber)
  END
  IF ?jobe2:EmailNotification{Prop:Checked} = True
    UNHIDE(?jobe2:EmailAlertAddress:Prompt)
    UNHIDE(?jobe2:EmailAlertAddress)
  END
  IF ?jobe2:EmailNotification{Prop:Checked} = False
    jobe2:EmailAlertAddress = ''
    HIDE(?jobe2:EmailAlertAddress:Prompt)
    HIDE(?jobe2:EmailAlertAddress)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:C3DMONIT.Close
    Relate:COURIER.Close
    Relate:JOBSE3.Close
    Relate:SUBURB.Close
    Relate:TRANTYPE.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Show_Addresses')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Courier
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
      IF ?job:Company_Name{prop:Req} = 1
        !Required!
        IF job:Company_Name = ''
          SELECT(?job:Company_Name)
          CYCLE
        END
      END
      
      If jobe2:SMSNotification
          If jobe2:SMSAlertNumber = ''
              Case Missive('You must enter a SMS Alert Mobile Number.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Select(?jobe2:SMSAlertNumber)
              Cycle
          End ! If jobe2:SMSAlertNumber = ''
      End ! If jobe2:SMSNotification
      
      If jobe2:EmailNotification
          If jobe2:EmailAlertAddress = ''
              Case Missive('You must enter an Email Alert Address.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Select(?jobe2:EmailAlertAddress)
              Cycle
          End ! If jobe2:EmailAlertAddress = ''
      End ! If jobe2:EmailNotification
      
      ! Inserting (DBH 24/01/2008) # 9612 - Force waybill number
      If ?jobe2:CourierWaybillNumber{prop:Req} = 1 And ?jobe2:CourierWaybillNumber{prop:ReadOnly} = 0
          If jobe2:CourierWaybillNumber = ''
              Select(?jobe2:CourierWaybillNumber)
              Cycle
          End ! If jobe2:CourierWaybillNumber = ''
      End ! If jobe2:CourierWaybillNumber{prop:Req} = 1
      ! End (DBH 24/01/2008) #9612
      
      Access:JOBSE.TryUpdate()
      Access:JOBSE2.Update()
    OF ?ButtonAmendC3D
      !TB13224 - if the user has the right access level allow to ammend customer details
      
      If ~SecurityCheck('CHANGE SIEBEL CUSTOMER INFO')
      
              !hide the button
              hide(?ButtonAmendC3D)
      
              !enable the fields
              enable(?job:Company_Name)
              enable(?job:Address_Line1)
              enable(?job:Address_Line2)
              enable(?job:Address_Line3)
              enable(?job:Postcode)
              enable(?job:Telephone_Number)
              enable(?job:Fax_Number)
              enable(?jobe:EndUserEmailAddress)
      END !If SecurityCheck()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020420'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020420'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020420'&'0')
      ***
    OF ?job:Company_Name
      !SELECT(?job:PostCode)
    OF ?job:Address_Line1
      IF job:address_line2 <> ''
          Select(?job:address_line3)
      End!IF job:addressline2 <> ''
      If job:address_line3 <> ''
          Select(?job:telephone_number)
      End!If job:addressline3 <> ''
    OF ?job:Address_Line3
      If ~0{prop:AcceptAll}
          Access:SUBURB.ClearKey(sur:SuburbKey)
          sur:Suburb = job:Address_Line3
          If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Found
              If tmp:HubType = 1
                  If sur:Hub <> jobe2:HubCustomer
                      If HubOutOfRegion(wob:HeadAccountNumber,sur:Hub) = 1
                          If tmp:ForceHub = 1
                              Beep(Beep:SystemHand);  Yield()
                              Case Missive('The selected Hub is outside your Region.'&|
                                  '|'&|
                                  '|You do not have access to make this change.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              job:Address_Line3 = save:SuburbCustomer
                              Display()
                              Cycle
                          Else ! If tmp:ForceHub = 1
                              Beep(Beep:SystemExclamation);  Yield()
                              Case Missive('The selected Hub is outside your Region.'&|
                                  '|If you continue with this address you may not be able to despatch this job.'&|
                                  '|'&|
                                  '|Are you sure you want to continue?','ServiceBase 3g',|
                                             'mexclam.jpg','/No|\Yes')
                                  Of 2 ! Yes Button
                                  Of 1 ! No Button
                                      job:Address_Line3 = save:SuburbCustomer
                                      Display()
                                      Cycle
                              End ! Case Missive
                          End ! If tmp:ForceHub = 1
                      End ! If HubOutOfRegion(wob:HeadAccountNumber,sur:Hub) = 1
                  End ! If sur:Hub <> jobe2:HubCustomer
              End ! If tmp:HubType = 1
      
              job:Postcode = sur:Postcode
              jobe2:HubCustomer = sur:Hub
              ?job:Postcode{prop:Skip} = 1
              ?job:Postcode{prop:ReadOnly} = 1
              ?jobe2:HubCustomer{prop:Skip} = 1
              ?jobe2:HubCustomer{prop:ReadOnly} = 1
              Bryan.CompFieldColour()
              Display()
          Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Error
              Beep(Beep:SystemQuestion);  Yield()
              Case Missive('Cannot find the selected Suburb.'&|
                  '|'&|
                  '|Do you wish to RE-ENTER the Suburb, PICK from all available Suburbs, or CONTINUE and manually enter the address?','ServiceBase 3g',|
                             'mquest.jpg','Continue|Pick|/Re-Enter')
                  Of 3 ! Re-Enter Button
                      job:Address_Line3 = ''
                      job:Postcode = ''
                      jobe2:HubCustomer = ''
                      ?job:Postcode{prop:Skip} = 1
                      ?job:Postcode{prop:ReadOnly} = 1
                      ?jobe2:HubCustomer{prop:Skip} = 1
                      ?jobe2:HubCustomer{prop:ReadOnly} = 1
                      Bryan.CompFieldColour()
                      Display()
                      Select(?job:Address_Line3)
                  Of 2 ! Pick Button
                      job:Address_Line3 = ''
                      job:Postcode = ''
                      jobe2:HubCustomer = ''
                      Post(Event:Accepted,?LookupSuburb1)
                  Of 1 ! Continue Button
                      ?job:Postcode{prop:Skip} = 0
                      ?job:Postcode{prop:ReadOnly} = 0
                      If tmp:ForceHub = 0 Or tmp:HubType <> 1
                          ! Only make the Hub available if the user has access (DBH: 04/02/2008)
                          ?jobe2:HubCustomer{prop:Skip} = 0
                          ?jobe2:HubCustomer{prop:ReadOnly} = 0
                      End ! If tmp:ForceHub = 1
                      Bryan.CompFieldColour()
                      Select(?job:Postcode)
              End ! Case Missive
          End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          save:SuburbCustomer = job:Address_Line3
          save:HubCustomer = jobe2:HubCustomer
      
      End ! If ~0{prop:AcceptAll}
      
      
      
      
      
      
    OF ?LookupSuburb1
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseSuburbs
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              ! Inserting (DBH 04/02/2008) # 9613 - Do not allow the Hub to be changed
              If tmp:HubType = 1
                  If sur:Hub <> jobe2:HubCustomer
                      If HubOutOfRegion(wob:HeadAccountNumber,sur:Hub) = 1
                          If tmp:ForceHub = 1
                              Case Missive('The selected Hub is outside your region. You do not have access to do this.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              job:Address_Line3 = save:SuburbCustomer
                              Display()
                              Cycle
                          Else ! If tmp:ForceHub = 1
                              Case Missive('Warning! The selected Hub is outside your region. If you continue you may not be able to despatch this job.'&|
                                '||Are you sure you want to continue?','ServiceBase 3g',|
                                             'mexclam.jpg','/No|\Yes')
                                  Of 2 ! Yes Button
                                  Of 1 ! No Button
                                      job:Address_Line3 = save:SuburbCustomer
                                      Display()
                                      Cycle
                              End ! Case Missive
                          End ! If tmp:ForceHub = 1
                      End ! If HubOutOfRegion(wob:HeadAccountNumber,sur:Hub) = 1
                  End ! If sur:Hub <> jobe2:HubCustomer
              End ! If tmp:HubType = 1
      
              ! End (DBH 04/02/2008) #9613
              job:Address_Line3 = sur:Suburb
              job:Postcode = sur:Postcode
              jobe2:HubCustomer = sur:Hub
              save:SuburbCustomer = job:Address_Line3
              Select(?+2)
              ?job:Postcode{prop:Skip} = 1
              ?job:Postcode{prop:ReadOnly} = 1
              ?jobe2:HubCustomer{prop:ReadOnly} = 1
              ?jobe2:HubCustomer{prop:Skip} = 1
              Bryan.CompFieldColour()
          Of Requestcancelled
              Select(?-1)
      End!Case Globalreponse
      Display()
    OF ?job:Postcode
      Postcode_Routine(job:Postcode,job:Address_Line1,job:Address_Line2,job:Address_Line3)
      Display()
      Select(?job:Address_Line1,1)
    OF ?jobe2:HubCustomer
      If ~0{prop:AcceptAll}
          If tmp:HubType = 1
              ! This is the delivery address. Is it in range? (DBH: 06/02/2008)
              If HubOutOfRegion(wob:HeadAccountNumber,jobe2:HubCustomer) = 1
                  Beep(Beep:SystemExclamation);  Yield()
                  Case Missive('The selected Hub is outside your Region.'&|
                      '|If you continue with this address you may not be able to despatch jobs for this account.'&|
                      '|'&|
                      '|Are you sure you want to continue?','ServiceBase 3g',|
                                 'mexclam.jpg','/No|\Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          jobe2:HubCustomer = save:HubCustomer
                          Display()
                          Cycle
                  End ! Case Missive
              End ! If HubOutOfRegion(wob:HeadAccountNumber,jobe2:HubCustomer) = 1
          End ! If tmp:HubType = 1
          save:HubCustomer = jobe2:HubCustomer
      End ! If ~0{prop:AcceptAll}
    OF ?job:Telephone_Number
      
          temp_string = Clip(left(job:Telephone_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Telephone_Number    = temp_string
          Display(?job:Telephone_Number)
    OF ?job:Fax_Number
      
          temp_string = Clip(left(job:Fax_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Fax_Number    = temp_string
          Display(?job:Fax_Number)
    OF ?job:Address_Line1_Collection
      IF job:address_line2_collection <> ''
          Select(?job:address_line3_collection)
      End!IF job:addressline2 <> ''
      If job:address_line3_collection <> ''
          Select(?job:telephone_collection)
      End!If job:addressline3 <> ''
    OF ?job:Address_Line3_Collection
      If ~0{prop:AcceptAll}
          Access:SUBURB.ClearKey(sur:SuburbKey)
          sur:Suburb = job:Address_Line3_Collection
          If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Found
              job:Postcode_Collection = sur:Postcode
              jobe2:HubCollection = sur:Hub
              ?job:Postcode_Collection{prop:Skip} = 1
              ?job:Postcode_Collection{prop:ReadOnly} = 1
              ?jobe2:HubCollection{prop:Skip} = 1
              ?jobe2:HubCollection{prop:ReadOnly} = 1
              Bryan.CompFieldColour()
              Display()
          Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Error
              Beep(Beep:SystemQuestion);  Yield()
              Case Missive('Cannot find the selected Suburb.'&|
                  '|'&|
                  '|Do you wish to RE-ENTER the Suburb, PICK from all available Suburbs, or CONTINUE and manually enter the address?','ServiceBase 3g',|
                             'mquest.jpg','Continue|Pick|/Re-Enter')
                  Of 3 ! Re-Enter Button
                      job:Address_Line3_Collection = ''
                      job:Postcode_Collection = ''
                      jobe2:HubCollection = ''
                      ?job:Postcode_Collection{prop:Skip} = 1
                      ?job:Postcode_Collection{prop:ReadOnly} = 1
                      ?jobe2:hubCollection{prop:Skip} = 1
                      ?jobe2:HubCollection{prop:ReadOnly} = 1
                      Bryan.CompFieldColour()
                      Display()
                      Select(?job:Address_Line3_Collection)
                  Of 2 ! Pick Button
                      job:Address_Line3_Collection = ''
                      job:Postcode_Collection = ''
                      jobe2:HubCollection = ''
                      Post(Event:Accepted,?LookupSuburb2)
                  Of 1 ! Continue Button
                      ?job:Postcode_Collection{prop:Skip} = 0
                      ?job:Postcode_Collection{prop:ReadOnly} = 0
                      ?jobe2:HubCollection{prop:Skip} = 1
                      ?jobe2:HubCollection{prop:ReadOnly} = 1
                      Bryan.CompFieldColour()
                      Select(?job:Postcode_Collection)
              End ! Case Missive
          End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
      End ! If ~0{prop:AcceptAll}
      
      
      
      
      
      
    OF ?LookupSuburb2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseSuburbs
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              job:Address_Line3_Collection = sur:Suburb
              job:Postcode_Collection = sur:Postcode
              jobe2:HubCollection = sur:Hub
              Select(?+2)
              ?job:Postcode_Collection{prop:Skip} = 1
              ?job:Postcode_Collection{prop:ReadOnly} = 1
              ?jobe2:HubCollection{prop:Skip} = 1
              ?jobe2:HubCollection{prop:ReadOnly} = 1
              Bryan.CompFieldColour()
      
          Of Requestcancelled
              Select(?-1)
      End!Case Globalreponse
      Display()
    OF ?CollClearCopy
      ThisWindow.Update
      glo:G_Select1 = ''
      glo:select1 = 'COLLECTION'
      If job:address_line1_collection <> ''
          glo:select2 = '1'
      Else
          glo:select2 = '2'
      End
      If job:address_line1_delivery = ''
          glo:select3 = 'DELIVERY'
      End
      Clear_Address_Window()
      Case glo:select1
          Of '1'
              JOB:Postcode_Collection=''
              JOB:Company_Name_Collection=''
              JOB:Address_Line1_Collection=''
              JOB:Address_Line2_Collection=''
              JOB:Address_Line3_Collection=''
              JOB:Telephone_Collection=''
              jobe2:HubCollection = ''
              Select(?Job:postcode_collection)
          Of '2'
              job:postcode_collection      = job:postcode
              job:company_name_collection  = job:company_name
              job:address_line1_collection = job:address_line1
              job:address_line2_collection = job:address_line2
              job:address_line3_collection = job:address_line3
              job:telephone_collection     = job:telephone_number
              jobe2:HubCollection          = jobe2:HubCustomer
          Of '4'
              job:postcode_collection      = job:postcode_delivery
              job:company_name_collection  = job:company_name_delivery
              job:address_line1_collection = job:address_line1_delivery
              job:address_line2_collection = job:address_line2_delivery
              job:address_line3_collection = job:address_line3_delivery
              job:telephone_collection     = job:telephone_delivery
              jobe2:HubCollection          = jobe2:HubDelivery
          Of 'CANCEL'
      End
      Select(?job:postcode_collection)
      Display()
      glo:select1 = ''
      glo:select2 = ''
      
      glo:G_Select1 = ''
      
    OF ?job:Postcode_Collection
      Postcode_Routine(job:Postcode_Collection,job:Address_Line1_Collection,job:Address_Line2_Collection,job:Address_Line3_Collection)
      Display()
      Select(?job:Address_Line1_Collection,1)
    OF ?job:Telephone_Collection
      
          temp_string = Clip(left(job:Telephone_Collection))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Telephone_Collection    = temp_string
          Display(?job:Telephone_Collection)
    OF ?job:Address_Line1_Delivery
      IF job:address_line2_delivery <> ''
          Select(?job:address_line3_delivery)
      End!IF job:addressline2 <> ''
      If job:address_line3_delivery <> ''
          Select(?job:telephone_delivery)
      End!If job:addressline3 <> ''
    OF ?job:Address_Line3_Delivery
      If ~0{prop:AcceptAll}
          Access:SUBURB.ClearKey(sur:SuburbKey)
          sur:Suburb = job:Address_Line3_Delivery
          If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Found
              If tmp:HubType = 2
                  If sur:Hub <> jobe2:HubDelivery
                      If HubOutOfRegion(wob:HeadAccountNumber,sur:Hub) = 1
                          If tmp:ForceHub = 1
                              Beep(Beep:SystemHand);  Yield()
                              Case Missive('The selected Hub is outside your Region.'&|
                                  '|'&|
                                  '|You do not have access to make this change.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              job:Address_Line3_Delivery = save:SuburbDelivery
                              Display()
                              Cycle
                          Else ! If tmp:ForceHub = 1
                              Beep(Beep:SystemExclamation);  Yield()
                              Case Missive('The selected Hub is outside your Region.'&|
                                  '|If you continue with this address you may not be able to despatch this job.'&|
                                  '|'&|
                                  '|Are you sure you want to continue?','ServiceBase 3g',|
                                             'mexclam.jpg','/No|\Yes')
                                  Of 2 ! Yes Button
                                  Of 1 ! No Button
                                      job:Address_Line3_Delivery = save:SuburbDelivery
                                      Display()
                                      Cycle
                              End ! Case Missive
                          End ! If tmp:ForceHub = 1
                      End ! If HubOutOfRegion(wob:HeadAccountNumber,sur:Hub) = 1
                  End ! If sur:Hub <> jobe2:HubCustomer
              End ! If tmp:HubType = 1
              job:Postcode_Delivery = sur:Postcode
              jobe2:HubDelivery = sur:Hub
              ?job:Postcode_Delivery{prop:Skip} = 1
              ?job:Postcode_Delivery{prop:ReadOnly} = 1
              ?jobe2:HubDelivery{prop:Skip} = 1
              ?jobe2:HubDelivery{prop:ReadOnly} = 1
              Bryan.CompFieldColour()
              Display()
          Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Error
              Beep(Beep:SystemQuestion);  Yield()
              Case Missive('Cannot find the selected Suburb.'&|
                  '|'&|
                  '|Do you wish to RE-ENTER the Suburb, PICK from all available Suburbs, or CONTINUE and manually enter the address?','ServiceBase 3g',|
                             'mquest.jpg','Continue|Pick|/Re-Enter')
                  Of 3 ! Re-Enter Button
                      job:Address_Line3_Delivery = ''
                      job:Postcode_Delivery = ''
                      jobe2:HubDelivery = ''
                      ?job:Postcode_Delivery{prop:Skip} = 1
                      ?job:Postcode_Delivery{prop:ReadOnly} = 1
                      ?jobe2:HubDelivery{prop:Skip} = 1
                      ?jobe2:HubDelivery{prop:ReadOnly} = 1
                      Bryan.CompFieldColour()
                      Display()
                      Select(?job:Address_Line3_Delivery)
                  Of 2 ! Pick Button
                      job:Address_Line3_Delivery = ''
                      job:Postcode_Delivery = ''
                      jobe2:HubDelivery = ''
                      Post(Event:Accepted,?LookupSuburb3)
                  Of 1 ! Continue Button
                      ?job:Postcode_Delivery{prop:Skip} = 0
                      ?job:Postcode_Delivery{prop:ReadOnly} = 0
                      ?jobe2:HubDelivery{prop:Skip} = 0
                      ?jobe2:HubDelivery{prop:ReadOnly} = 0
                      Bryan.CompFieldColour()
                      Select(?job:Postcode_Delivery)
              End ! Case Missive
          End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          save:HubDelivery = jobe2:HubDelivery
          save:SuburbDelivery = job:Address_Line3_Delivery
      End ! If ~0{prop:AcceptAll}
      
      
      
      
      
      
    OF ?LookupSuburb3
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseSuburbs
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              job:Address_Line3_Delivery = sur:Suburb
              job:Postcode_Delivery = sur:Postcode
              jobe2:HubDelivery = sur:Hub
              Select(?+2)
              ?job:Postcode_Delivery{prop:Skip} = 1
              ?job:Postcode_Delivery{prop:ReadOnly} = 1
              ?jobe2:HubDelivery{prop:Skip} = 1
              ?jobe2:HubDelivery{prop:ReadOnly} = 1
              Bryan.CompFieldColour()
      
          Of Requestcancelled
              Select(?-1)
      End!Case Globalreponse
      Display()
    OF ?job:Postcode_Delivery
      Postcode_Routine(job:Postcode_Delivery,job:Address_Line1_Delivery,job:Address_Line2_Delivery,job:Address_Line3_Delivery)
      Display()
      Select(?job:Address_Line1_Delivery,1)
    OF ?DelClearCopy
      ThisWindow.Update
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      glo:select1 = 'DELIVERY'
      If job:address_line1_delivery <> ''
          glo:select2 = '1'
      Else
          glo:select2 = '2'
      End
      If job:address_line1_collection = ''
          glo:select3 = 'COLLECTION'
      End
      
      Clear_Address_Window
      
      Case glo:select1
          Of '1'
              job:postcode_delivery      = ''
              job:company_name_delivery  = ''
              job:address_line1_delivery = ''
              job:address_line2_delivery = ''
              job:address_line3_delivery = ''
              job:telephone_delivery     = ''
              jobe2:HubDelivery = ''
              Select(?Job:postcode_delivery)
          Of '2'
              job:postcode_delivery      = job:postcode
              job:company_name_delivery  = job:company_name
              job:address_line1_delivery = job:address_line1
              job:address_line2_delivery = job:address_line2
              job:address_line3_delivery = job:address_line3
              job:telephone_delivery     = job:telephone_number
              jobe2:HubDelivery = jobe2:HubCustomer
          Of '3'
              job:postcode_delivery      = job:postcode_collection
              job:company_name_delivery  = job:company_name_collection
              job:address_line1_delivery = job:address_line1_collection
              job:address_line2_delivery = job:address_line2_collection
              job:address_line3_delivery = job:address_line3_collection
              job:telephone_delivery     = job:telephone_collection
              jobe2:hubDelivery = jobe2:HubCollection
          Of 'CANCEL'
      End
      Select(?job:postcode_delivery)
      Display()
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
    OF ?jobe2:HubDelivery
      If ~0{prop:AcceptAll}
          If tmp:HubType = 2
              ! This is the delivery address. Is it in range? (DBH: 06/02/2008)
              If HubOutOfRegion(wob:HeadAccountNumber,jobe2:HubDelivery) = 1
                  Beep(Beep:SystemExclamation);  Yield()
                  Case Missive('The selected Hub is outside your Region.'&|
                      '|If you continue with this address you may not be able to despatch jobs for this account.'&|
                      '|'&|
                      '|Are you sure you want to continue?','ServiceBase 3g',|
                                 'mexclam.jpg','/No|\Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          jobe2:HubDelivery = save:HubDelivery
                          Display()
                          Cycle
                  End ! Case Missive
              End ! If HubOutOfRegion(wob:HeadAccountNumber,jobe2:HubCustomer) = 1
          End ! If tmp:HubType = 2
      
          save:HubDelivery = jobe2:HubDelivery
      End ! If ~0{prop:AcceptAll}
    OF ?job:Telephone_Delivery
      
          temp_string = Clip(left(job:Telephone_Delivery))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Telephone_Delivery    = temp_string
          Display(?job:Telephone_Delivery)
    OF ?job:Courier
      IF job:Courier OR ?job:Courier{Prop:Req}
        cou:Courier = job:Courier
        !Save Lookup Field Incase Of error
        look:job:Courier        = job:Courier
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Courier = cou:Courier
          ELSE
            !Restore Lookup On Error
            job:Courier = look:job:Courier
            SELECT(?job:Courier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      cou:Courier = job:Courier
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Courier = cou:Courier
          Select(?+1)
      ELSE
          Select(?job:Courier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Courier)
    OF ?jobe2:IDNumber
      ! Inserting (DBH 07/04/2006) #7305 - Record if the ID Number is changed
      If jobe2:IDNumber <> save:IDNumber
          If Clip(save:IDNumber) <> ''
              Case Missive('Are you sure you want to change the Customer ID Number?'&|
                '||(An entry will be recorded in the Audit Trail)','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      If Access:JOBSE2.Update() = Level:Benign
                          If AddToAudit(job:Ref_Number,'JOB','CUSTOMER ID NUMBER CHANGED','OLD ID NO: ' & Clip(save:IDNumber) & '<13,10>NEW ID NO: ' & Clip(jobe2:IDNumber))
                              save:IDNumber = jobe2:IDNumber
                          End ! If AddToAudit(job:Ref_Number,'JOB','CUSTOMER ID NUMBER CHANGED','OLD ID NO: ' & Clip(save:IDNumber) & '<13,10>NEW ID NO: ' & Clip(jobe2:IDNumber))
                      End ! If Access:JOBSE2.Update() = Level:Benign
                  Of 1 ! No Button
                      jobe2:IDNumber = save:IDNumber
              End ! Case Missive
          End ! If Clip(save:IDNumber <> '')
          ! Inserting (DBH 08/06/2006) #7305 - Replicate to the Loan ID Number
          If Clip(jobe2:LoanIDNumber) = ''
              jobe2:LoanIDNumber = jobe2:IDNumber
          End ! If jobe2:LoanIDNumber = ''
          ! End (DBH 08/06/2006) #7305
      Else ! If jobe2:IDNumber <> save:IDNumber
      End ! If jobe2:IDNumber <> save:IDNumber
      Display()
      ! End (DBH 07/04/2006) #7305
    OF ?jobe2:SMSNotification
      IF ?jobe2:SMSNotification{Prop:Checked} = True
        UNHIDE(?jobe2:SMSAlertNumber:Prompt)
        UNHIDE(?jobe2:SMSAlertNumber)
      END
      IF ?jobe2:SMSNotification{Prop:Checked} = False
        jobe2:SMSAlertNumber = ''
        HIDE(?jobe2:SMSAlertNumber:Prompt)
        HIDE(?jobe2:SMSAlertNumber)
      END
      ThisWindow.Reset
      ! Inserting (DBH 07/12/2007) # 9491 - Autofill the SMS Alert Number with the job mobile number
      If ~0{prop:AcceptAll}
          If jobe2:SMSNotification And jobe2:SMSAlertNumber = '' And job:Mobile_Number <> ''
              jobe2:SMSALertNumber = job:Mobile_Number
              Display()
          End ! If jobe2:SMSNotification And jobe2:SMSAlertNumber = ''
      End ! If 0{prop:AcceptAll}
      ! End (DBH 07/12/2007) #9491
    OF ?jobe2:EmailNotification
      IF ?jobe2:EmailNotification{Prop:Checked} = True
        UNHIDE(?jobe2:EmailAlertAddress:Prompt)
        UNHIDE(?jobe2:EmailAlertAddress)
      END
      IF ?jobe2:EmailNotification{Prop:Checked} = False
        jobe2:EmailAlertAddress = ''
        HIDE(?jobe2:EmailAlertAddress:Prompt)
        HIDE(?jobe2:EmailAlertAddress)
      END
      ThisWindow.Reset
    OF ?jobe2:SMSAlertNumber
      ! Inserting (DBH 08/05/2006) #7597 - Check the format of the Mobile Number
      If ~0{prop:AcceptAll}
          If PassMobileNumberFormat(jobe2:SMSAlertNumber,1) = False
              message('SMS alert number being set blank')
              jobe2:SMSAlertNumber = ''
              Select(?jobe2:SMSAlertNumber)
              Cycle
          End ! If PassMobileNumberFormat(jobe2:SMSAlertNumber,1) = False
      End ! If ~0{prop:AcceptAll}
      ! End (DBH 08/05/2006) #7597
    OF ?BrowseAddresses
      ThisWindow.Update
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = job:Account_Number
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Found
          GlobalRequest = SelectRecord
          Browse_Addresses(sub:RecordNumber)
          If GlobalResponse = RequestCompleted
              job:Postcode_Delivery       = sua:Postcode
              job:Address_Line1_Delivery  = sua:AddressLine1
              job:Company_Name_Delivery   = sua:CompanyName
              job:Address_Line2_Delivery  = sua:AddressLine2
              job:Address_Line3_Delivery  = sua:AddressLine3
              job:Telephone_Delivery      = sua:TelephoneNumber
              jobe:Sub_Sub_Account        = sua:AccountNumber
              Display()
          End !If GlobalRequest = RecordCompleted
      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    OF ?CollectionTextButton
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Collection_Text
      ThisWindow.Reset
    OF ?DeliveryTextButton
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Delivery_Text
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?job:Courier{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?job:Courier{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateJOBS PROCEDURE                                  !Generated from procedure template - Window

CurrentTab           STRING(80)
Local                CLASS
Pricing              Procedure(Byte func:Force)
                     END
tmp:JobNumber        STRING(20)
save_aus_id          USHORT,AUTO
save_jobsobf_id      USHORT,AUTO
save_joe_id          USHORT,AUTO
save_taf_id          USHORT,AUTO
save_cou_ali_id      USHORT,AUTO
save_cou_id          USHORT,AUTO
tmp:ConsignNo        STRING(30)
tmp:accountnumber    STRING(30)
tmp:OldConsignNo     STRING(30)
tmp:labelerror       LONG
account_number2_temp STRING(30)
tmp:DespatchClose    BYTE(0)
sav:ref_number       LONG
tmp:printjobcard     BYTE(0)
tmp:printjobreceipt  BYTE(0)
save_trb_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
save_cha_id          USHORT,AUTO
sav:path             STRING(255)
print_despatch_note_temp STRING(3)
error_type_temp      STRING(1),DIM(50)
error_queue_temp     QUEUE,PRE(err)
field                STRING(30)
                     END
error_message_temp   STRING(255)
Cancelling_Group     GROUP,PRE()
old_exchange_unit_number_temp REAL
new_exchange_unit_number_temp REAL
old_loan_unit_number_temp REAL
save_moc_id          USHORT,AUTO
new_loan_unit_number_temp REAL
saved_ref_number_temp REAL
                     END
parts_queue_temp     QUEUE,PRE(partmp)
Part_Ref_Number      REAL
Quantity             REAL
Exclude_From_Order   STRING(3)
Pending_Ref_Number   REAL
                     END
check_for_bouncers_temp STRING('0')
save_aud_id          USHORT,AUTO
save_job_id          USHORT,AUTO
saved_esn_temp       STRING(16)
saved_msn_temp       STRING(16)
engineer_sundry_temp STRING(3)
main_store_sundry_temp STRING(3)
engineer_ref_number_temp REAL
main_store_ref_number_temp REAL
engineer_quantity_temp REAL
main_store_quantity_temp REAL
save_ccp_id          USHORT
save_cwp_id          USHORT,AUTO
label_type_temp      BYTE
exchange_accessory_count_temp REAL
accessory_count_temp REAL
Ignore_Chargeable_Charges_Temp STRING('''NO''')
Ignore_Warranty_Charges_Temp STRING('''NO''')
Ignore_Estimate_Charges_Temp STRING('''NO''')
Force_Fault_Codes_Temp STRING('NO {1}')
balance_due_warranty_temp REAL
paid_warranty_temp   REAL
print_label_temp     STRING(3)
Courier_temp         STRING(30)
loan_Courier_temp    STRING(30)
exchange_Courier_temp STRING(30)
engineer_name_temp   STRING(60)
msn_fail_temp        STRING('NO {1}')
esn_fail_temp        STRING('NO {1}')
day_count_temp       REAL
day_number_temp      REAL
date_error_temp      STRING(3)
estimate_ready_temp  STRING(3)
in_repair_temp       STRING(3)
on_test_temp         STRING(3)
qa_passed_temp       STRING(3)
qa_rejected_temp     STRING(3)
qa_second_passed_temp STRING(3)
estimate_accepted_temp STRING(3)
estimate_rejected_temp STRING(3)
Third_Party_Site_temp STRING(30)
Chargeable_Job_temp  STRING(3)
Warranty_Job_Temp    STRING(3)
warranty_charge_type_temp STRING(30)
Partemp_group        GROUP,PRE()
Partemp:part_ref_number REAL
Partemp:pending_ref_number REAL
Partemp:despatch_note_number STRING(30)
Partemp:quantity     LONG
Partemp:purchase_cost REAL
Partemp:sale_cost    REAL
                     END
estimate_temp        STRING(3)
workshop_temp        STRING(3)
location_temp        STRING(30)
repair_type_temp     STRING(30)
warranty_repair_type_temp STRING(30)
model_number_temp    STRING(30)
History_Run_Temp     SHORT
transit_type_temp    STRING(30)
account_number_temp  STRING(30)
ESN_temp             STRING(30)
MSN_temp             STRING(30)
Unit_Type_temp       STRING(30)
Current_Status_Temp  STRING(30)
Order_Number_temp    STRING(30)
Surname_temp         STRING(30)
Date_Completed_temp  DATE
Time_Completed_Temp  TIME
LocalRequest         LONG
LocalResponse        LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
charge_type_Temp     STRING(30)
title_3_temp         STRING(46)
details_temp         STRING(31)
date_booked_temp     STRING(29)
name_temp            STRING(60)
Vat_Total_Temp       REAL
Total_Temp           REAL
engineer_temp        STRING(30)
loan_model_make_temp STRING(60)
loan_esn_temp        STRING(15)
loan_msn_temp        STRING(10)
Loan_Accessories_Temp STRING(30)
Exchange_Model_Make_temp STRING(60)
Exchange_Esn_temp    STRING(15)
Exchange_Msn_temp    STRING(10)
Exchange_Accessories_Temp STRING(30)
balance_due_temp     REAL
Accessories_Temp     STRING(30)
location_type_temp   STRING(30)
vat_total_estimate_temp REAL
total_estimate_temp  REAL
vat_total_warranty_temp REAL
Total_warranty_temp  REAL
chargeable_part_total_temp REAL
warranty_part_total_temp REAL
job_time_remaining_temp STRING(20)
status_time_remaining_temp STRING(20)
ordered_temp         STRING(18)
CPCSExecutePopUp     BYTE
ProcedureRunning     BYTE
warranty_ordered_temp STRING(18)
vat_estimate_temp    REAL
vat_chargeable_temp  REAL
vat_warranty_temp    REAL
paid_chargeable_temp REAL
estimate_parts_cost_temp REAL
show_booking_temp    STRING('NO {1}')
trade_account_string_temp STRING(60)
customer_name_string_temp STRING(60)
Exchange_Consignment_Number_temp STRING(30)
Exchange_Despatched_temp DATE
Exchange_Despatched_User_temp STRING(3)
Exchange_Despatch_Number_temp REAL
Loan_Consignment_Number_temp STRING(30)
Loan_Despatched_temp DATE
Loan_Despatched_User_temp STRING(3)
Loan_Despatch_Number_temp REAL
Date_Despatched_temp DATE
Despatch_Number_temp REAL
Despatch_User_temp   STRING(3)
Consignment_Number_temp STRING(30)
third_party_returned_temp DATE
tmp:FaultDescription STRING(255)
tmp:EngineerNotes    STRING(255)
tmp:InvoiceText      STRING(255)
SaveGroup            GROUP,PRE(sav)
ChargeType           STRING(30)
WarrantyChargeType   STRING(20)
RepairType           STRING(30)
WarrantyRepairType   STRING(30)
ChargeableJob        STRING(3)
WarrantyJob          STRING(3)
EstimateLabourCost   REAL
ChargeableLabourCost REAL
WarrantyLabourCost   REAL
EstimatePartsCost    REAL
ChargeablePartsCost  REAL
WarrantyPartsCost    REAL
EstimateCourierCost  REAL
ChargeableCourierCost REAL
WarrantyCourierCost  REAL
HubRepair            BYTE(0)
AccountNumber        STRING(30)
TransitType          STRING(30)
MSN                  STRING(30)
ModelNumber          STRING(30)
DOP                  DATE
OrderNumber          STRING(30)
FaultCode1           STRING(30)
FaultCode2           STRING(30)
FaultCode3           STRING(30)
FaultCode4           STRING(30)
FaultCode5           STRING(30)
FaultCode6           STRING(30)
FaultCode7           STRING(30)
FaultCode8           STRING(30)
FaultCode9           STRING(30)
FaultCode10          STRING(255)
FaultCode11          STRING(255)
FaultCode12          STRING(255)
InvoiceText          STRING(255)
FaultDescription     STRING(255)
                     END
ChangedGroup         GROUP,PRE(sav1)
cct                  BYTE(0)
wct                  BYTE(0)
crt                  BYTE(0)
wrt                  BYTE(0)
chj                  BYTE(0)
wrj                  BYTE(0)
elc                  BYTE(0)
clc                  BYTE(0)
wlc                  BYTE(0)
epc                  BYTE(0)
cpc                  BYTE(0)
wpc                  BYTE(0)
ecc                  BYTE(0)
ccc                  BYTE(0)
wcc                  BYTE(0)
hbr                  BYTE(0)
acc                  BYTE(0)
itt                  BYTE(0)
msn                  BYTE(0)
mdl                  BYTE(0)
dop                  BYTE(0)
ord                  BYTE(0)
fc1                  BYTE(0)
fc2                  BYTE(0)
fc3                  BYTE(0)
fc4                  BYTE(0)
fc5                  BYTE(0)
fc6                  BYTE(0)
fc7                  BYTE(0)
fc8                  BYTE(0)
fc9                  BYTE(0)
fc10                 BYTE(0)
fc11                 BYTE(0)
fc12                 BYTE(0)
ivt                  BYTE(0)
fdt                  BYTE(0)
                     END
tmp:PartQueue        QUEUE,PRE(tmp)
PartNumber           STRING(30)
                     END
tmp:ParcelLineName   STRING(255),STATIC
tmp:WorkstationName  STRING(30)
tmp:IncomingIMEI     STRING(30)
tmp:IncomingMSN      STRING(30)
tmp:ExchangeIMEI     STRING(30)
tmp:ExchangeMSN      STRING(30)
tmp:EstimateDetails  STRING(255)
tmp:SkillLevel       LONG
tmp:one              BYTE(1)
tmp:DateAllocated    DATE
tmp:FaultyUnit       BYTE(0)
tmp:BERConsignmentNumber STRING(30)
sav:ChaLabourCost    REAL
sav:WarLabourCost    REAL
sav:EstLabourCost    REAL
tmp:HUBRepair        BYTE(0)
tmp:HubRepairDate    DATE
tmp:HubRepairTime    TIME
tmp:SaveHubRepair    BYTE(0)
tmp:Network          STRING(30)
tmp:CColourFlag      BYTE(0)
tmp:WColourFlag      BYTE(0)
tmp:Claim            REAL
tmp:Handling         REAL
tmp:Exchange         REAL
tmp:RRCELabourCost   REAL
tmp:RRCEPartsCost    REAL
tmp:RRCCLabourCost   REAL
tmp:RRCCPartsCost    REAL
tmp:RRCWLabourCost   REAL
tmp:RRCWPartsCost    REAL
tmp:IgnoreClaimCosts BYTE(0)
tmp:IgnoreRRCChaCosts BYTE(0)
tmp:IgnoreRRCWarCosts BYTE(0)
tmp:IgnoreRRCEstCosts BYTE(0)
tmp:DisableLocation  BYTE(0)
tmp:POPType          STRING(30)
tmp:SaveAccessory    STRING(255),STATIC
SaveAccessoryQueue   QUEUE,PRE(savaccq)
Accessory            STRING(30)
Type                 BYTE(0)
                     END
tmp:EndUserTelNo     STRING(30)
tmp:2ndExchangeIMEI  STRING(30)
tmp:2ndExchangeMSN   STRING(30)
tmp:CurrentSiteLocation STRING(30)
tmp:VSACustomer      BYTE(0)
tmp:LoanUnitNumber   LONG
tmp:SpecificNeeds    BYTE
locAuditNotes        STRING(255)
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?job:Unit_Type
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:12 QUEUE                          !Queue declaration for browse/combo box using ?job:Turnaround_Time
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
tur:Days               LIKE(tur:Days)                 !List box control field - type derived from field
tur:Hours              LIKE(tur:Hours)                !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?job:Colour
moc:Colour             LIKE(moc:Colour)               !List box control field - type derived from field
moc:Record_Number      LIKE(moc:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?job:ProductCode
mop:ProductCode        LIKE(mop:ProductCode)          !List box control field - type derived from field
mop:RecordNumber       LIKE(mop:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
FDCB17::View:FileDropCombo VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                       PROJECT(tur:Days)
                       PROJECT(tur:Hours)
                     END
FDCB25::View:FileDropCombo VIEW(MODELCOL)
                       PROJECT(moc:Colour)
                       PROJECT(moc:Record_Number)
                     END
FDCB46::View:FileDropCombo VIEW(MODPROD)
                       PROJECT(mop:ProductCode)
                       PROJECT(mop:RecordNumber)
                     END
BRW21::View:Browse   VIEW(JOBSTAGE)
                       PROJECT(jst:Job_Stage)
                       PROJECT(jst:Date)
                       PROJECT(jst:Time)
                       PROJECT(jst:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
jst:Job_Stage          LIKE(jst:Job_Stage)            !List box control field - type derived from field
jst:Date               LIKE(jst:Date)                 !List box control field - type derived from field
jst:Time               LIKE(jst:Time)                 !List box control field - type derived from field
jst:Ref_Number         LIKE(jst:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::job:Record  LIKE(job:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string String(255)
QuickWindow          WINDOW('Main Frame'),AT(0,0,680,429),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(COLOR:White),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         MENU('Options'),USE(?Options)
                           ITEM('Allocate Job'),USE(?OptionsAllocateJob)
                           ITEM('Estimate'),USE(?OptionsEstimate)
                         END
                       END
                       PROMPT('Amend Job'),AT(8,9),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,7,644,13),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,9),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,384),USE(?Button:ReleaseForDespatch),TRN,FLAT,HIDE,ICON('reldespp.jpg')
                       PROMPT(''),AT(284,29,236,12),USE(?Batch_Number_Text),RIGHT,FONT(,10,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT(''),AT(8,29,312,14),USE(?Title_Text),FONT(,10,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                       SHEET,AT(4,29,520,258),USE(?Sheet4),BELOW,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB,USE(?Booking1_Tab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Auto Search'),AT(8,45),USE(?JOB:Auto_Search:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(84,45,70,10),USE(job:Auto_Search),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(AltF12),UPR
                           ENTRY(@n6),AT(160,45,16,10),USE(job:Last_Repair_Days),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR
                           PROMPT('Days From Last Repair'),AT(180,45,80,10),USE(?JOB:Last_Repair_Days:Prompt),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Trade Account Number'),AT(8,61,56,20),USE(?Job:Account_Number:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Trade Account Status'),AT(84,74,136,8),USE(?Trade_Account_Status),HIDE,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(84,61,136,10),USE(job:Account_Number),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),REQ,UPR,READONLY
                           BUTTON,AT(248,54),USE(?LookupTradeAccount),SKIP,DISABLE,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Order Number'),AT(8,85),USE(?job:order_number:prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(84,83,136,10),USE(job:Order_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(AltF12),UPR
                           BUTTON,AT(224,55),USE(?TradeAccountLookupButton),TRN,FLAT,ICON('lookupp.jpg')
                           GROUP,AT(8,97,220,20),USE(?NameGroup),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Title'),AT(84,97),USE(?Title:prompt),FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Initial'),AT(112,97),USE(?Initial:prompt),FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Surname'),AT(136,97),USE(?surname:prompt),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('End User Name'),AT(8,105,60,12),USE(?job:title:prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s4),AT(84,105,24,10),USE(job:Title),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                             ENTRY(@s1),AT(116,105,12,10),USE(job:Initial),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                             ENTRY(@s30),AT(136,105,84,11),USE(job:Surname),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                             PROMPT('End User Tel No'),AT(8,118),USE(?jobe:EndUserTelNo:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(84,118,136,10),USE(tmp:EndUserTelNo),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           END
                           PROMPT('Initial Transit Type'),AT(8,135),USE(?Prompt49),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(84,135,136,10),USE(job:Transit_Type),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(224,131),USE(?LookupTransitType),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Job Site'),AT(8,154),USE(?job:location_type:prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Workshop'),AT(84,154),USE(job:Workshop),DISABLE,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           GROUP,AT(4,165,236,20),USE(?Location_Group),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Internal Location'),AT(8,170),USE(?job:location:prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(84,170,136,10),USE(job:Location),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey)
                           END
                           PROMPT('At 3rd Party Site'),AT(160,155),USE(?Third_party_message),HIDE,FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(84,211,136,10),USE(job:Third_Party_Site),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           TEXT,AT(372,43,136,10),USE(job:ESN),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('MSN'),AT(284,57),USE(?JOB:MSN:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s16),AT(372,57,136,10),USE(job:MSN),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Product Code'),AT(284,69),USE(?job:ProductCode:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(372,69,136,10),USE(job:ProductCode),IMM,HIDE,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Model Number'),AT(284,82),USE(?JOB:Model_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(372,82,72,10),USE(job:Model_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(344,79),USE(?LookupModelNumber),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(460,82,48,10),USE(job:Manufacturer),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Unit Type'),AT(284,95,76,12),USE(?Prompt:Job:Unit_Type),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(372,95,84,10),USE(job:Unit_Type),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           COMBO(@s30),AT(460,95,48,10),USE(job:Colour),IMM,HIDE,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           CHECK,AT(372,109),USE(tmp:SpecificNeeds),VALUE('1','0')
                           PROMPT('Specific Needs Device'),AT(284,109),USE(?Prompt70)
                           BUTTON,AT(344,119),USE(?accessory_button),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Accessories'),AT(284,123),USE(?Accessories_Temp:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(372,123,70,10),USE(Accessories_Temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('DOP'),AT(444,123),USE(?JOB:DOP:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(460,123,48,10),USE(job:DOP),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('I.M.E.I. Number'),AT(284,43),USE(?job:esn:prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Mobile Number'),AT(284,138),USE(?JOB:Mobile_Number:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(372,138,136,10),USE(job:Mobile_Number),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Job Turnaround Time'),AT(284,154),USE(?Prompt101),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(372,154,136,10),USE(job:Turnaround_Time),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('100L(2)@s30@31L(2)@p<<<<# Dysp@8L(2)@p<<# Hrsp@'),DROP(10,164),FROM(Queue:FileDropCombo:12)
                           PROMPT('3rd Party '),AT(8,211),USE(?JOB:Third_Party_Site:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Charge Type'),AT(372,165),USE(?job:charge_type:prompt),TRN,FONT(,7,,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           CHECK('Chargeable Job'),AT(284,173,68,12),USE(job:Chargeable_Job),DISABLE,HIDE,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           ENTRY(@s30),AT(372,173,124,10),USE(job:Charge_Type),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(496,169),USE(?LookupChargeType),SKIP,DISABLE,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Despatched'),AT(8,186),USE(?JOB:Third_Party_Despatch_Date:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(84,186,64,10),USE(job:ThirdPartyDateDesp),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           CHECK('Warranty Job'),AT(284,191,68,12),USE(job:Warranty_Job),DISABLE,HIDE,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           ENTRY(@s30),AT(372,191,124,10),USE(job:Warranty_Charge_Type),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(EnterKey),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),REQ,UPR,READONLY
                           BUTTON,AT(496,187),USE(?LookupWarrantyChargeType),SKIP,DISABLE,TRN,FLAT,HIDE,FONT('Tahoma',8,,,CHARSET:ANSI),ICON('lookupp.jpg')
                           ENTRY(@d6b),AT(84,197,64,10),USE(job:Date_Paid),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Returned'),AT(8,197),USE(?job:date_paid:prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Estimate Required'),AT(284,209,88,12),USE(job:Estimate),DISABLE,HIDE,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           ENTRY(@n14.2),AT(432,209,32,12),USE(job:Estimate_If_Over),DISABLE,HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           CHECK('Accepted'),AT(468,205),USE(job:Estimate_Accepted),DISABLE,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Rejected'),AT(468,213),USE(job:Estimate_Rejected),DISABLE,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           STRING('If Over'),AT(404,209),USE(?job:Estimate_If_Over:prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB,USE(?Booking2_Tab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT(''),AT(260,34,82,12),USE(?ExchangeRepair),FONT('Tahoma',10,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(88,42,160,156),USE(?Group9),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s60),AT(92,50,152,11),USE(trade_account_string_temp),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(92,61),USE(job:Order_Number,,?JOB:Order_Number:2),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s60),AT(92,74,152,11),USE(customer_name_string_temp),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(92,93),USE(job:Transit_Type,,?JOB:Transit_Type:2),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s3),AT(92,114),USE(job:Workshop,,?JOB:Workshop:2),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(92,138),USE(job:Location,,?JOB:Location:2),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(92,125,124,10),USE(tmp:Network),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             BUTTON,AT(220,121),USE(?LookupNetwork),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             STRING(@s30),AT(92,149),USE(job:Third_Party_Site,,?JOB:Third_Party_Site:2),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(92,162),USE(job:Special_Instructions,,?JOB:Special_Instructions:2),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s60),AT(92,173),USE(tmp:EstimateDetails),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           END
                           GROUP,AT(360,42,160,147),USE(?Group9:2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@d6),AT(452,133),USE(job:DOP,,?job:DOP:2),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(364,145,152,10),USE(job:Turnaround_Time,,?job:Turnaround_Time:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(364,157,152,10),USE(job:Charge_Type,,?job:Charge_Type:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(364,169,152,10),USE(job:Warranty_Charge_Type,,?job:Warranty_Charge_Type:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('SIM Number:'),AT(452,109),USE(?SIMNumber),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(364,49,80,10),USE(tmp:IncomingIMEI),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(452,49,64,10),USE(tmp:IncomingMSN),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(452,61,64,10),USE(tmp:ExchangeMSN),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(452,73,64,10),USE(job:MSN,,?job:MSN:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s20),AT(452,85,64,10),USE(tmp:2ndExchangeMSN),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(452,97,64,10),USE(job:ProductCode,,?job:ProductCode:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(452,109,64,10),USE(jobe:SIMNumber),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(364,109,80,10),USE(job:Manufacturer,,?job:Manufacturer:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(364,97,80,10),USE(job:Model_Number,,?job:Model_Number:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s20),AT(364,85,80,10),USE(tmp:2ndExchangeIMEI),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(364,73,80,10),USE(job:ESN,,?job:ESN:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(364,61,80,10),USE(tmp:ExchangeIMEI),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             BUTTON,AT(464,109),USE(?LookupSIMNumber),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(364,121,80,10),USE(job:Unit_Type,,?job:Unit_Type:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(452,121,64,10),USE(job:Colour,,?job:Colour:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(364,133,80,10),USE(job:Mobile_Number,,?job:Mobile_Number:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           PROMPT('Trade Account:'),AT(8,50),USE(?Prompt107),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Final'),AT(264,74),USE(?Final),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Incoming'),AT(264,50),USE(?Incoming:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('IMEI / MSN:'),AT(308,50),USE(?IMEIMSN),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Exchanged'),AT(264,61),USE(?Exchanged),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('IMEI / MSN:'),AT(308,61),USE(?IMEIMSN:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Order Number:'),AT(8,61),USE(?Prompt108),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Model No / Product Code:'),AT(264,98),USE(?ModelNumberProductCode),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Manufacturer:'),AT(264,109),USE(?Manufacturer),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('IMEI / MSN:'),AT(308,74),USE(?IMEIMSN:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Customer Name:'),AT(8,74),USE(?Customer_Name_String_temp:Prompt),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Initial Transit Type:'),AT(8,93),USE(?Prompt110),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('2nd Exch'),AT(264,85),USE(?SecondExchange),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('IMEI / MSN:'),AT(308,85),USE(?IMEIMSN:4),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Job Turnaround Time'),AT(264,146),USE(?JobTurnaroundTime),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Unit Type / Colour:'),AT(264,122),USE(?UnitTypeColour),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Mobile No / D.O.P.'),AT(264,133),USE(?MobileNumberDOP),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('In Workshop:'),AT(8,114),USE(?Prompt111),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Network:'),AT(8,125),USE(?tmp:Network:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Internal Location:'),AT(8,138),USE(?location_string),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('3rd Party Site:'),AT(8,149),USE(?third_party_string),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Chargeable Charge Type:'),AT(264,157),USE(?chargeable_Type_string),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Special Instructions:'),AT(8,162),USE(?special_instructions_string),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Warranty Charge Type:'),AT(264,170),USE(?warranty_charge_Type_String),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Estimate:'),AT(8,173),USE(?EstimatePrompt),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT(''),AT(8,203,160,12),USE(?BouncerText),HIDE,FONT(,10,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(180,197),USE(?History:2),SKIP,TRN,FLAT,HIDE,LEFT,ICON('prevhisp.jpg')
                           CHECK('Amend Details'),AT(8,181,68,20),USE(show_booking_temp),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           PROMPT('JOB COMPLETED'),AT(224,223),USE(?jobcompleted),HIDE,FONT(,14,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT(''),AT(348,221,160,12),USE(?Exchanged_Title),RIGHT,FONT(,10,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(528,27,148,354),USE(?Sheet5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 13'),USE(?Tab13)
                           STRING('Work In Progress Details'),AT(532,32),USE(?String9),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           LIST,AT(532,43,140,68),USE(?List:2),IMM,FONT('Tahoma',7,,FONT:regular,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('69L(2)|M~Job Stage~@s20@43R(2)~Date & Time~@d6@/23L@t1@'),FROM(Queue:Browse:1)
                           PROMPT('Status'),AT(532,115),USE(?Prompt102),TRN,FONT(,,0D0FFD0H,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           STRING(@d6),AT(564,115),USE(job:Status_End_Date),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@t1),AT(628,115),USE(job:Status_End_Time),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Job'),AT(532,128),USE(?Prompt:Job:Current_Status),TRN,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(556,128,116,10),USE(job:Current_Status),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('Exch.'),AT(532,143),USE(?JOB:Exchange_Status:Prompt),TRN,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(556,143,116,10),USE(job:Exchange_Status),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Loan'),AT(532,159,,10),USE(?JOB:Loan_Status:Prompt),TRN,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(556,159,116,10),USE(job:Loan_Status),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Turnaround Time'),AT(532,171,56,8),USE(?Prompt79),FONT(,,0D0FFD0H,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           PROMPT('Job Time Remain'),AT(532,184,68,8),USE(?Prompt80),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s20),AT(612,184,60,11),USE(job_time_remaining_temp),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Status Time Remain'),AT(532,195),USE(?Prompt81),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s20),AT(612,195,64,10),USE(status_time_remaining_temp),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Hub Repair'),AT(532,208),USE(jobe:HubRepair),SKIP,DISABLE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0'),MSG('Hub Repair')
                           ENTRY(@d6),AT(532,221,55,9),USE(jobe:HubRepairDate),SKIP,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),READONLY,MSG('HubRepairDate')
                           ENTRY(@t1b),AT(592,221,52,9),USE(jobe:HubRepairTime),SKIP,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),READONLY,MSG('Hub Repair Time')
                           CHECK(' OBF Validated'),AT(532,237),USE(jobe:OBFvalidated),DISABLE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0'),MSG('Out of Box Failure - Validation')
                           ENTRY(@d6),AT(532,248,55,9),USE(jobe:OBFvalidateDate),SKIP,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),READONLY,MSG('OBF Validation Date')
                           ENTRY(@t1),AT(592,248,52,9),USE(jobe:OBFvalidateTime),SKIP,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),READONLY,MSG('OBF Validation Time')
                         END
                       END
                       BUTTON,AT(540,384),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(608,384),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(608,384),USE(?Close),TRN,FLAT,HIDE,ICON('closep.jpg'),STD(STD:Close)
                       SHEET,AT(4,288,520,92),USE(?Sheet6),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 10'),USE(?Tab10)
                           BUTTON,AT(448,293),USE(?accessory_button:2),TRN,FLAT,ICON('accessp.jpg')
                           BUTTON,AT(182,294),USE(?ConsignmentHistory),TRN,FLAT,LEFT,ICON('conhistp.jpg')
                           BUTTON,AT(272,294),USE(?ThirdPartyButton),TRN,FLAT,LEFT,ICON('thirdp.jpg')
                           BUTTON,AT(8,322),USE(?ViewCosts),TRN,FLAT,LEFT,ICON('costsp.jpg')
                           BUTTON,AT(95,322),USE(?Audit_Trail_Button),TRN,FLAT,LEFT,ICON('auditp.jpg')
                           BUTTON,AT(182,322),USE(?Contact_History:2),TRN,FLAT,LEFT,ICON('stachap.jpg')
                           BUTTON,AT(273,322),USE(?Contact_History:3),TRN,FLAT,LEFT,ICON('locchap.jpg')
                           BUTTON,AT(448,323),USE(?Button:ContactHistoryEmpty),TRN,FLAT,ICON('conthisp.jpg')
                           BUTTON,AT(356,322),USE(?Fault_Description_Text_Button),TRN,FLAT,LEFT,ICON('faudescp.jpg')
                           PROMPT('View Notes'),AT(456,351),USE(?Prompt:ViewNotes),HIDE,FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(8,350),USE(?ExchangeButton),TRN,FLAT,HIDE,LEFT,ICON('viewexcp.jpg')
                           BUTTON,AT(95,350),USE(?LoanButton),TRN,FLAT,HIDE,LEFT,ICON('viewloap.jpg')
                           BUTTON,AT(182,350),USE(?AmendAddresses),TRN,FLAT,LEFT,ICON('editaddp.jpg')
                           BUTTON,AT(272,350),USE(?Validate_Serial_Number),TRN,FLAT,LEFT,ICON('valserp.jpg')
                           BUTTON,AT(356,350),USE(?Print_Label),TRN,FLAT,LEFT,ICON('prnlabp.jpg')
                           BUTTON,AT(448,323),USE(?Button:ContactHistoryFilled),TRN,FLAT,HIDE,FONT(,,,,CHARSET:ANSI),ICON('conthisb.jpg')
                           BUTTON,AT(356,294),USE(?ValidatePOP),TRN,FLAT,LEFT,ICON('valpopp.jpg')
                           BUTTON,AT(8,294),USE(?Button49),TRN,FLAT,LEFT,ICON('engpartp.jpg')
                           BUTTON,AT(95,294),USE(?Fault_Codes_Lookup),TRN,FLAT,LEFT,ICON('faucodep.jpg')
                         END
                       END
                       STRING('JOB CANCELLED'),AT(4,19,516,186),USE(?CancelText),TRN,HIDE,FONT('Tahoma',36,COLOR:Red,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(250)
                       CHECK('VSA Customer'),AT(372,273),USE(tmp:VSACustomer),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('VSA Customer'),TIP('VSA Customer'),VALUE('1','0')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB17               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:12        !Reference to browse queue type
                     END

FDCB25               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB46               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

BRW21                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW21::Sort0:Locator StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_lac_id   ushort,auto
save_xca_id   ushort,auto
save_jac_id   ushort,auto
save_job_ali_id   ushort,auto
save_trr_id   ushort,auto
save_orp_id   ushort,auto
save_maf_id   ushort,auto
save_par_id   ushort,auto
save_wpr_id   ushort,auto
save_map_id   ushort,auto
save_par_ali_id   ushort,auto
save_epr_id   ushort,auto
save_jpt_id   ushort,auto
save_esn_id   ushort,auto
save_jea_id   ushort,auto
save_xch_ali_id   ushort,auto
    Map
LocalValidateAccessories    Procedure(),Byte
    End
!Save Account Address Details
accsav:AccountNumber    String(30)

InvoiceDetails  Group,Pre(accsa1)
AccountNumber       String(30)
CompanyName         String(30)
AddressLine1        String(30)
AddressLine2        String(30)
AddressLine3        String(30)
Postcode            String(30)
TelephoneNumber     String(30)
FaxNumber           String(30)
                End
DeliveryDetails     Group,Pre(accsa2)
CompanyName      String(30)
AddressLine1     String(30)
AddressLine2     String(30)
AddressLine3     String(30)
Postcode         String(30)
TelephoneNumber  String(30)
                End
SaveAccessory    File,Driver('TOPSPEED'),Pre(savacc),Name(tmp:SaveAccessory),Create,Bindable,Thread
AccessoryKey            Key(savacc:Accessory),NOCASE,DUP
Record                  Record
Accessory               String(30)
                        End
                    End
TempFilePath         CSTRING(255)

!Save Entry Fields Incase Of Lookup
look:job:Account_Number                Like(job:Account_Number)
look:job:Transit_Type                Like(job:Transit_Type)
look:job:Model_Number                Like(job:Model_Number)
look:job:Charge_Type                Like(job:Charge_Type)
look:job:Warranty_Charge_Type                Like(job:Warranty_Charge_Type)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

CheckContactHistory             Routine
    Found# = 0
    Access:CONTHIST.Clearkey(cht:Ref_Number_Key)
    cht:Ref_Number = job:Ref_Number
    Set(cht:Ref_Number_Key,cht:Ref_Number_Key)
    Loop ! Begin Loop
        If Access:CONTHIST.Next()
            Break
        End ! If Access:CONTHIST.Next()
        If cht:Ref_Number <> job:Ref_Number
            Break
        End ! If cht:Ref_Number <> job:Ref_Number
        Found# = 1
    End ! Loop
    If Found# = 0
        ?Button:ContactHistoryEmpty{prop:Hide} = False
        ?Button:ContactHistoryFilled{prop:Hide} = True
        ?Prompt:ViewNotes{prop:Hide} = True
    Else ! If Found# = 0
        ?Button:ContactHistoryEmpty{prop:Hide} = True
        ?Button:ContactHistoryFilled{prop:Hide} = False
        ?Prompt:ViewNotes{prop:Hide} = False
    End ! If Found# = 0
GetJOBSE                   Routine
   tmp:HUBRepair     = jobe:HUBRepair
   tmp:HubRepairDate = jobe:HubRepairDate
   tmp:HubRepairTime = jobe:HubRepairTime
   tmp:Network       = jobe:Network
   tmp:POPType       = jobe:POPType
   tmp:EndUserTelNo  = jobe:EndUserTelNo
   tmp:VSACustomer   = jobe:VSACustomer
!   ! Inserting (DBH 08/08/2006) # 8084 - Allow for costs being changed
!   tmp:IgnoreClaimCosts = jobe:IgnoreClaimCosts
!   tmp:Claim            = jobe:ClaimValue
!
!   tmp:Handling          = jobe:HandlingFee
!   tmp:IgnoreRRCChaCosts = jobe:IgnoreRRCChaCosts
!   tmp:RRCCLabourCost    = jobe:RRCCLabourCost
!   tmp:RRCCPartsCost     = jobe:RRCCPartsCost
!
!   tmp:IgnoreRRCWarCosts = jobe:IgnoreRRCWarCosts
!   tmp:RRCWPartsCost     = jobe:RRCWPartsCost
!   tmp:RRCWLabourCost    = jobe:RRCWLabourCost
!
!   tmp:IgnoreRRCEstCosts = jobe:IgnoreRRCEstCosts
!   tmp:RRCELabourCost    = jobe:RRCELabourCost
!   tmp:RRCEPartsCost     = jobe:RRCEPartsCost
!    ! End (DBH 08/08/2006) #8084



SetJOBSE                   Routine
   Access:JOBSE.Clearkey(jobe:RefNumberKey)
   jobe:RefNumber  = job:Ref_Number
   If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
       ! Found
       jobe:HUBRepair     = tmp:HUBRepair
       jobe:Network       = tmp:Network
       jobe:HubRepairDate = tmp:HubRepairDate
       jobe:HubRepairTime = tmp:HubRepairTime
       jobe:POPType       = tmp:POPType
       jobe:EndUserTelNo  = tmp:EndUserTelNo
       jobe:VSACustomer   = tmp:VSACustomer
!       ! Inserting (DBH 08/08/2006) # 8084 - Allow to costs being changed
!       jobe:IgnoreClaimCosts = tmp:IgnoreClaimCosts
!       jobe:ClaimValue       = tmp:Claim
!
!       jobe:HandlingFee       = tmp:Handling
!       jobe:IgnoreRRCChaCosts = tmp:IgnoreRRCChaCosts
!       jobe:RRCCLabourCost    = tmp:RRCCLabourCost
!       jobe:RRCCPartsCost     = tmp:RRCCPartsCost
!
!       jobe:IgnoreRRCWarCosts = tmp:IgnoreRRCWarCosts
!       jobe:RRCWPartsCost     = tmp:RRCWPartsCost
!       jobe:RRCWLabourCost    = tmp:RRCWLabourCost
!
!       jobe:IgnoreRRCEstCosts = tmp:IgnoreRRCEstCosts
!       jobe:RRCELabourCost    = tmp:RRCELabourCost
!       jobe:RRCEPartsCost     = tmp:RRCEPartsCost
!       ! End (DBH 08/08/2006) #8084

       Access:JOBSE.TryUpdate()
   Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
   ! Error
   End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

Validate_Motorola   ROUTINE

   IF job:POP = ''
     Glo:Select1 = ''
   ELSE
     Glo:Select1 = job:pop
   END

   IF job:dop = ''
     Glo:Select2 = ''
   ELSE
     Glo:Select2 = job:dop
   END

   !Glo:Select2 = ''
   glo:Select3 = 'NONE'

   x# = POP_Validate2()

   tmp:POPType = glo:Select3

   IF Glo:Select1 = ''
     !Nothing done!
     job:Warranty_Job = 'NO'
     job:Warranty_Charge_type = ''
   ELSE
    !If Status is 130 DOP Query, then set it back to the previous status

     If Sub(job:Current_Status,1,3) = '130'
        If job:Engineer = ''
            GetStatus(305,0,'JOB') !Awaiting Allocation
        Else !If job:Engineer = ''
            GetStatus(sub(job:PreviousStatus,1,3),0,'JOB')
        End !If job:Engineer = ''

     End !If Sub(job:Current_Status,1,3) = '130'
     job:POP = Glo:Select1
     IF Glo:Select1 = 'F'
       job:Warranty_Job = 'YES'
       Post(Event:Accepted,?job:Warranty_Job)
       job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
       !Post(Event:Accepted,?job:Warranty_Charge_Type)
       job:Chargeable_Job = 'NO'
       Post(Event:Accepted,?job:Chargeable_Job)
       job:Charge_Type = ''
       ENABLE(?Job:Chargeable_Job)
       ENABLE(?Job:Warranty_Job)
       Job:DOP = Glo:Select2
       Post(Event:Accepted,?Job:DOP)
       UPDATE()
       DISPLAY()
     END

     IF Glo:Select1 = 'S'
       job:Warranty_Job = 'YES'
       Post(Event:Accepted,?job:Warranty_Job)
       job:Warranty_Charge_Type = 'WARRANTY (2ND YR)'
       !Post(Event:Accepted,?job:Warranty_Charge_Type)
       job:Chargeable_Job = 'NO'
       Post(Event:Accepted,?job:Chargeable_Job)
       job:Charge_Type = ''
       ENABLE(?Job:Chargeable_Job)
       ENABLE(?Job:Warranty_Job)
       Job:DOP = Glo:Select2
       Post(Event:Accepted,?Job:DOP)
       UPDATE()
       DISPLAY()
     END

     IF Glo:Select1 = 'O'
       job:Warranty_Job = 'YES'
       Post(Event:Accepted,?job:Warranty_Job)
       job:Warranty_Charge_Type = 'WARRANTY (OBF)'
       !Post(Event:Accepted,?job:Warranty_Charge_Type)
       job:Chargeable_Job = 'NO'
       Post(Event:Accepted,?job:Chargeable_Job)
       job:Charge_Type = ''
       ENABLE(?Job:Chargeable_Job)
       ENABLE(?Job:Warranty_Job)
       Job:DOP = Glo:Select2
       Post(Event:Accepted,?Job:DOP)
       UPDATE()
       DISPLAY()
     END

     IF Glo:Select1 = 'C'
       job:Warranty_Job = 'NO'
       Post(Event:Accepted,?job:Warranty_Job)
       job:Chargeable_Job = 'YES'
       Post(Event:Accepted,?job:Chargeable_Job)
       job:Warranty_Charge_Type = ''
       DISABLE(?Job:Warranty_Job)
       ENABLE(?Job:Chargeable_Job)
       !Post(Event:Accepted,?job:Warranty_Charge_Type)
       job:Charge_Type = 'NON-WARRANTY'
       !Post(Event:Accepted,?job:Charge_Type)
       UPDATE()
       DISPLAY()
     END

   END

   Glo:Select1 = ''
   Glo:Select2 = ''
OutFaultList     Routine
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = job:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found

        If man:ForceParts
            !?ChargeableAdjustment{prop:Hide} = 0
            !?WarrantyAdjustment{prop:Hide} = 0
        Else !If man:ForceParts
            !?ChargeableAdjustment{prop:Hide} = 1
            !?WarrantyAdjustment{prop:Hide} = 1
        End !If man:ForceParts
    Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End !If AccessMANUFACT.Tryfetch(manManufacturer_Key) = LevelBenign
CheckRequiredFields     Routine
    If ForceTransitType('B')
!        ?job:Transit_Type{prop:color} = 0BDFFFFH
        ?job:Transit_Type{prop:Req} = 1
    Else !If ForceTransitType('B')
 !       ?job:Transit_Type{prop:color} = color:white
        ?job:Transit_Type{prop:Req} = 0
    End !If ForceTransitType('B')

    If ForceIMEI('B')
!        ?job:ESN{prop:color} = 0BDFFFFH
        ?job:ESN{prop:Req} = 1
    Else !If ForceIMEI('B')
!        ?job:ESN{prop:color} = color:white
        ?job:ESN{prop:Req} = 0
    End !If ForceIMEI('B')

    If ForceMSN(job:Manufacturer,'B')
!        ?job:MSN{prop:color} = 0BDFFFFH
        ?job:MSN{prop:Req} = 1
    Else !If ForceMSN('B')
!        ?job:MSN{prop:color} = color:white
        ?job:MSN{prop:Req} = 0
    End !If ForceMSN('B')

    If ForceModelNumber('B')
!        ?job:Model_Number{prop:color} = 0BDFFFFH
        ?job:Model_Number{prop:Req} = 1
    Else !If ForceModelNumber('B')
!        ?job:Model_Number{prop:color} = color:white
        ?job:Model_Number{prop:Req} = 0
    End !If ForceModelNumber('B')

    If ForceUnitType('B')
!        ?job:Unit_Type{prop:color} = 0BDFFFFH
        ?job:Unit_Type{prop:Req} = 1
    Else !If ForceUnitType{'B')
!        ?job:Unit_Type{prop:color} = color:white
        ?job:Unit_Type{prop:Req} = 0
    End !If ForceUnitType{'B')

    If ForceColour('B')
!        ?job:Colour{prop:color} = 0BDFFFFH
        ?job:Colour{prop:Req} = 1
    Else !If ForceColour('B')
!        ?job:Colour{prop:color} = color:white
        ?job:Colour{prop:Req} = 0
    End !If ForceColour('B')

    If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')
!        ?job:DOP{prop:color} = 0BDFFFFH
        ?job:DOP{prop:Req} = 1
    Else !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')
!        ?job:DOP{prop:color} = color:white
        ?job:DOP{prop:Req} = 0
    End !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')

    If ForceMobileNumber('B')
!        ?job:Mobile_Number{prop:color} = 0BDFFFFH
        ?job:Mobile_Number{prop:Req} = 1
    Else !If ForceMobileNumber('B')
!        ?job:Mobile_Number{prop:color} = color:white
        ?job:Mobile_Number{prop:Req} = 0
    End !If ForceMobileNumber('B')

    If ForceLocation(job:Transit_Type,job:Workshop,'B')
!        ?job:Location{prop:color} = 0BDFFFFH
        ?job:Location{prop:Req} = 1
    Else !If ForceLocation(job:Transit_Type,job:Workshop,'B')
 !       ?job:Location{prop:color} = color:white
        ?job:Location{prop:Req} = 0
    End !If ForceLocation(job:Transit_Type,job:Workshop,'B')


    If ForceOrderNumber(job:Account_Number,'B')
!        ?job:Order_Number{prop:color} = 0BDFFFFH
        ?job:Order_Number{prop:Req} = 1
    Else !If ForceOrderNumber(job:Account_Number,'B')
!        ?job:Order_Number{prop:color} = color:white
        ?job:Order_Number{prop:Req} = 0
    End !If ForceOrderNumber(job:Account_Number,'B')

    !TB13307 - J - 01/07/14 - now have three returns to show, force or hide customer name
    Case ForceCustomerName(job:Account_Number,'B')
    of 2
        !show
        ?job:Surname{prop:color} = color:white
        ?job:Surname{prop:Req} = 0
        ?NameGroup{prop:hide} = 0

    of 1
        !force
        ?job:Surname{prop:color} = 0BDFFFFH
        ?job:Surname{prop:Req} = 1
        ?NameGroup{prop:hide} = 0

    of 0
        !hide
        ?job:Surname{prop:Req} = 0
        ?NameGroup{prop:hide} = true

    END !Case Force CustomerName

!        ?job:Initial{prop:color} = 0BDFFFFH
!        ?job:Surname{prop:color} = 0BDFFFFH
!        ?job:Title{prop:color} = 0BDFFFFH
!        ?job:Surname{prop:Req} = 1
!    Else !If ForceCustomerName(job:Account_Number,'B')
!        ?job:Initial{prop:color} = color:white
!        ?job:Surname{prop:color} = color:white
!        ?job:Title{prop:color} = color:white
!        ?job:Surname{prop:Req} = 0
!    End !If ForceCustomerName(job:Account_Number,'B')

AddEngineer     Routine
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:Engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Found
        Engineer_Name_temp  = Clip(Use:Forename) & ' ' & Clip(use:Surname)
        tmp:SkillLevel   = use:SkillLevel
    Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

    Do_Status# = 0

    If job:Engineer <> Engineer_Temp
        IF job:Date_Completed <> ''
            Error# = 1
            Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !IF job:Date_Completed <> ''
        If job:Invoice_Number <> ''
            Error# = 1
            Case Missive('Cannot change! This job has been invoiced.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !If job:Invoice_Number <> ''
        If Error# = 0
            If Engineer_Temp = ''
                IF (AddToAudit(job:ref_number,'JOB','ENGINEER ALLOCATED: ' & CLip(job:engineer),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel)))
                END ! IF

                do_status# = 1
                engineer_temp = job:engineer

                If Access:JOBSENG.PrimeRecord() = Level:Benign
                    joe:JobNumber     = job:Ref_Number
                    joe:UserCode      = job:Engineer
                    joe:DateAllocated = Today()
                    joe:TimeAllocated = Clock()
                    joe:AllocatedBy   = use:User_Code
                    access:users.clearkey(use:User_Code_Key)
                    use:User_Code   = job:Engineer
                    access:users.fetch(use:User_Code_Key)

                    joe:EngSkillLevel = use:SkillLevel
                    joe:JobSkillLevel = use:SkillLevel

                    If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:JOBSENG.TryInsert() = Level:Benign
                End !If Access:JOBSENG.PrimeRecord() = Level:Benign

            Else !If Engineer_Temp = ''
                Case Missive('Are you sure you want to change the Engineer?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                    Of 2 ! Yes Button
                        IF (AddToAudit(job:ref_number,'JOB','ENGINEER CHANGED TO ' & CLip(job:engineer),'NEW ENGINEER: ' & CLip(job:Engineer) & |
                                                '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
                                                '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(engineer_temp)))
                        END ! IF

                        Engineer_Temp   = job:Engineer

                        If Access:JOBSENG.PrimeRecord() = Level:Benign
                            joe:JobNumber     = job:Ref_Number
                            joe:UserCode      = job:Engineer
                            joe:DateAllocated = Today()
                            joe:TimeAllocated = Clock()
                            joe:AllocatedBy   = use:User_Code
                            access:users.clearkey(use:User_Code_Key)
                            use:User_Code   = job:Engineer
                            access:users.fetch(use:User_Code_Key)

                            joe:EngSkillLevel = use:SkillLevel
                            joe:JobSkillLevel = jobe:SkillLevel

                            If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:JOBSENG.TryInsert() = Level:Benign
                        End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                    Of 1 ! No Button
                        job:Engineer    = Engineer_Temp
                End ! Case Missive
            End !If Engineer_Temp = ''
        Else !If Error# = 0
            job:Engineer    = engineer_temp
        End !If Error# = 0

    End !If job:Engineer <> Engineer_Temp

    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:Engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Found
        Engineer_Name_temp  = Clip(Use:Forename) & ' ' & Clip(use:Surname)
        tmp:SkillLevel   = use:SkillLevel
    Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

    If do_status# = 1
        GetStatus(310,thiswindow.request,'JOB') !allocated to engineer
        brw21.resetsort(1)
        Do time_remaining
    End!If do_status# = 1

    Do Update_Engineer
AreThereNotes       Routine
    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:Ref_Number
    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Found
!        If Clip(jbn:Fault_Description) <> ''
!            ?Fault_Description_Text_Button:2{prop:fontstyle} = font:bold
!            ?Fault_Description_Text_Button{prop:text} = 'Fault Description Text - Filled'
!            ?Fault_Description_Text_Button:2{prop:text} = 'Fault Desc.'
!        Else!If jbn:FaultDescription <> ''
!            ?Fault_Description_Text_Button{prop:text} = 'Fault Description Text - Empty'
!            ?Fault_Description_Text_Button:2{prop:text} = 'Fault Description'
!            ?Fault_Description_Text_Button:2{prop:fontstyle} = font:regular
!        End!If jbn:FaultDescription <> ''
    Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
CountParts      Routine
!Bog off no longer wanted
!    Clear(tmp:PartQueue)
!    Free(tmp:PartQueue)
!
!    Save_par_Id = Access:PARTS.Savefile()
!    Access:PARTS.Clearkey(par:Part_Number_Key)
!    par:Ref_Number  = job:Ref_Number
!    Set(par:Part_Number_Key,par:Part_Number_Key)
!    Loop
!        If Access:PARTS.Next()
!           Break
!        End !If Access:PARTS.Next()
!        If par:Ref_Number  <> job:Ref_Number
!            Break
!        End!If par:Ref_Number  <> job:Ref_Number
!
!        tmp:PartNumber  = par:Part_Number
!        Get(tmp:PartQueue,tmp:PartNumber)
!        If Error()
!            tmp:PartNumber  = par:Part_Number
!            Add(tmp:PartQueue)
!        End!If Error()
!    End !loop
!    Access:PARTS.Restorefile(Save_par_Id)
!
!    ?chargeable_details_tab{prop:text} = '&Chargeable Parts (' & CLip(Records(tmp:Partqueue)) & ')'
!
!    Clear(tmp:PartQueue)
!    Free(tmp:PartQueue)
!
!    save_wpr_id = access:warparts.savefile()
!    access:warparts.clearkey(wpr:part_number_key)
!    wpr:ref_number  = job:ref_number
!    set(wpr:part_number_key,wpr:part_number_key)
!    loop
!        if access:warparts.next()
!           break
!        end !if
!        if wpr:ref_number  <> job:ref_number      |
!            then break.  ! end if
!        tmp:PartNumber  = wpr:part_number
!        Get(tmp:PartQueue,tmp:PartNumber)
!        If Error()
!            tmp:Partnumber  = wpr:Part_number
!            Add(tmp:PartQueue)
!        End!If Error()
!    end !loop
!    access:warparts.restorefile(save_wpr_id)
!
!    ?warranty_details_tab{prop:text} = '&Warranty Parts (' & CLip(Records(tmp:Partqueue)) & ')'
!    Clear(tmp:PartQueue)
!    Free(tmp:PartQueue)
!
!    save_epr_id = access:estparts.savefile()
!    access:estparts.clearkey(epr:part_number_key)
!    epr:ref_number  = job:ref_number
!    set(epr:part_number_key,epr:part_number_key)
!    loop
!        if access:estparts.next()
!           break
!        end !if
!        if epr:ref_number  <> job:ref_number      |
!            then break.  ! end if
!        tmp:PartNumber  = epr:part_number
!        Get(tmp:PartQueue,tmp:PartNumber)
!        If Error()
!            tmp:Partnumber  = epr:Part_number
!            Add(tmp:PartQueue)
!        End!If Error()
!    end !loop
!    access:estparts.restorefile(save_epr_id)
!
!    ?estimate_details_tab{prop:text} = '&Estimate Parts (' & CLip(Records(tmp:Partqueue)) & ')'
!    Clear(tmp:PartQueue)
!    Free(tmp:PartQueue)
!
!
CityLink_Service        Routine

UPS_Service        Routine
stock_history       Routine        !Do The Prime, and Set The Quantity First
!    shi:ref_number      = sto:ref_number
!    access:users.clearkey(use:password_key)
!    use:password        =glo:password
!    access:users.fetch(use:password_key)
!    shi:user            = use:user_code
!    shi:date            = Today()
!    shi:transaction_type    = 'DEC'
!    shi:despatch_note_number    = par:despatch_note_number
!    shi:job_number      = job:ref_number
!    shi:purchase_cost   = par:purchase_cost
!    shi:sale_cost       = par:sale_cost
!    shi:retail_cost     = par:retail_cost
!    shi:notes           = 'STOCK DECREMENTED'
!    access:stohist.insert()
Skip_Despatch       Routine

Check_For_Despatch        Routine
    Set(defaults)
    access:defaults.next()

    despatch# = 0

    if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        despatch# = 1
    Else!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If sub:despatch_invoiced_jobs <> 'YES' And sub:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                Else!If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If tra:despatch_invoiced_jobs <> 'YES' And tra:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                End!If tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
    End!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'

    If despatch# = 1
        tmp:DespatchClose = 0
        If job:despatched <> 'YES'
            If ToBeLoaned() = 1
                restock# = 0
                If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                    Case Missive('A Loan Unit has been issed to this job but the Initial Transit Type has been set_up so that an Exchange Unit is required,'&|
                      '<13,10>Do you wish to CONTINUE and mark this unit for Despatch back to the Customer, or do you want to RESTOCK it?','ServiceBase 3g',|
                                   'mquest.jpg','\Restock|/Continue')
                        Of 2 ! Continue Button
                        Of 1 ! Restock Button
                            ForceDespatch()
                            restock# = 1
                    End ! Case Missive
                End!If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                If restock# = 0
                    job:despatched  = 'LAT'
                    job:despatch_Type = 'JOB'
                End!If restock# = 0
            Else!If ToBeLoaned() = 1
                If ToBeExchanged()
                    ForceDespatch()
                Else!If ToBeExchanged()
                    job:date_despatched = DespatchANC(job:courier,'JOB')
                    access:courier.clearkey(cou:courier_key)
                    cou:courier = job:courier
                    if access:courier.tryfetch(cou:courier_key) = Level:benign
                        If cou:despatchclose = 'YES'
                            tmp:despatchclose = 1
                        Else!If cou:despatchclose = 'YES'
                            tmp:despatchclose = 0
                        End!If cou:despatchclose = 'YES'
                    End!if access:courier.tryfetch(cou:courier_key) = Level:benign
                End!If ToBeExchanged()
            End!If ToBeLoaned() = 1
        End!If job:despatched <> 'YES'
    End
Transit_Type_Bit        Routine
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = job:transit_type
    if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        error# = 0
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                If trt:loan_unit    = 'YES' And tra:allow_loan <> 'YES'
                    error# = 1
                    Case Missive('Loan Units are not authorised for this Trade Account.'&|
                      '<13,10>Select another Initial Transit Type or amend the Trade Account defaults.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    job:transit_type    = transit_type_temp
                End!If trt:loan_unit    = 'YES' And tra:allow_loan <> 'YES'
                If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES' And error# = 0
                    Case Missive('Exchange Units are not authorised for this Trade Account.'&|
                      '<13,10>Select another Initial Transit Type or amend the Trade Account defaults.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    job:transit_Type    = transit_type_temp
                    error# = 1
                End!If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES'
            end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
        If error# = 0

            access:trantype.clearkey(trt:transit_type_key)
            trt:transit_type = job:transit_type
            If access:trantype.fetch(trt:transit_type_key) = Level:Benign
                hide# = 0
                If trt:collection_address <> 'YES'
                    If job:address_line1 <> ''
                        Case Missive('The selected Transit Type will remove the Collection Address.'&|
                          '<13,10>'&|
                          '<13,10>Are you sure?','ServiceBase 3g',|
                                       'mquest.jpg','\No|/Yes')
                            Of 2 ! Yes Button

                                transit_type_temp   = job:transit_type
                                hide# = 1
                            Of 1 ! No Button

                                job:transit_type = transit_type_temp
                        End ! Case Missive
                    Else
                        hide# = 1
                    End
                    If hide# = 1
                        job:postcode         = ''
                        job:company_name     = ''
                        job:address_line1    = ''
                        job:address_line2    = ''
                        job:address_line3    = ''
                        job:telephone_number = ''
                        job:fax_number       = ''
                    End
                Else
                End
                hide# = 0
                If trt:delivery_address <> 'YES'
                    If job:address_line1_delivery <> ''
                        Case Missive('The selected Transit Type will remove the Delivery Address.'&|
                          '<13,10>'&|
                          '<13,10>Are you sure?','ServiceBase 3g',|
                                       'mquest.jpg','\No|/Yes')
                            Of 2 ! Yes Button

                                transit_type_temp   = job:transit_type
                                hide# = 1
                            Of 1 ! No Button
                                job:transit_type    = transit_type_temp
                        End ! Case Missive
                    Else
                        hide# = 1
                    End
                    If hide# = 1
                        job:postcode_delivery         = ''
                        job:company_name_delivery     = ''
                        job:address_line1_delivery    = ''
                        job:address_line2_delivery    = ''
                        job:address_line3_delivery    = ''
                        job:telephone_delivery = ''
                    End
                Else
                End
            end !if
            If trt:InWorkshop = 'YES'
                job:workshop = 'YES'
            Else!If trt:InWorkshop = 'YES'
                job:workshop = 'NO'
            End!If trt:InWorkshop = 'YES'

            If trt:location <> 'YES'
                Hide(?location_group)
                ?job:location{prop:req} = 0
            Else
                If ~def:HideLocation
                    If job:workshop = 'YES'
                        ?Location_Group{Prop:Hide} = 0
                    End
                    If trt:force_location = 'YES' And job:workshop = 'YES'
                        ?job:location{prop:req} = 1
                    Else!If trt:force_location = 'YES'
                        ?job:location{prop:req} = 0
                    End!If trt:force_location = 'YES'
                Else
                    tmp:DisableLocation = GETINI('DISABLE','InternalLocation',,CLIP(PATH())&'\SB2KDEF.INI')
                    if tmp:DisableLocation
                        If job:workshop = 'YES'
                            ?Location_Group{Prop:Hide} = 0
                            ?Location_Group{Prop:Disable} = 1
                        End
                        ?job:location{prop:req} = 0
                        ?job:location{prop:Disable} = 1
                    end
                End !If ~def:HideLocation
            End
            If ThisWindow.Request = Insertrecord
                GetStatus(Sub(trt:initial_status,1,3),1,'JOB')

                Brw21.resetsort(1)
                Do time_remaining

                GetStatus(Sub(trt:Exchangestatus,1,3),1,'EXC')

                GetStatus(Sub(trt:loanstatus,1,3),1,'LOA')

                If trt:skip_workshop <> 'YES'
                    Unhide(?job:workshop)
                    Select(?job:workshop)
                End
                If trt:workshop_label = 'YES' and job:workshop = 'YES'
                    print_label_temp = 'YES'
                Else
                    print_label_temp = 'NO'
                End

                access:subtracc.clearkey(sub:account_number_key)
                sub:account_number  = job:account_number
                access:subtracc.fetch(sub:account_number_key)
                access:tradeacc.clearkey(tra:account_number_key)
                tra:account_number  = sub:main_account_number
                access:tradeacc.fetch(tra:account_number_key)

                IF tra:refurbcharge = 'YES' and TRT:Exchange_Unit = 'YES'
                    job:chargeable_job = 'YES'
                    job:warranty_job    = 'YES'
                    job:charge_type = tra:ChargeType
                    job:Warranty_Charge_Type    = tra:WarChargeType
                    Unhide(?job:charge_type)
                    Unhide(?job:warranty_charge_type)
                    Unhide(?lookupwarrantychargetype)
                    Unhide(?lookupChargeType)
                End!IF tra:refurbcharge = 'YES'

                If trt:job_card = 'YES'
                    tmp:printjobcard    = 1
                Else!If trt:job_card = 'YES'
                    tmp:printjobcard    = 0
                End!If trt:job_card = 'YES'
                If trt:jobReceipt = 'YES'
                    tmp:printjobreceipt = 1
                Else!If trt:job_Receipt = 'YES'
                    tmp:printjobreceipt = 0
                End!If trt:job_Receipt = 'YES'

                error# = 0
                If job:turnaround_time <> '' and job:turnaround_time <> trt:initial_priority
                    Case Missive('The selected Transit Type required a Turnaround Time of ' & Clip(trt:Initial_Priority) & '.'&|
                      '<13,10>'&|
                      '<13,10>A Turnaround Time has already been assigned to this job. Do you wish to change it?','ServiceBase 3g',|
                                   'mquest.jpg','\No|/Yes')
                        Of 2 ! Yes Button
                        Of 1 ! No Button
                            error# = 1
                    End ! Case Missive
                End!If job:turnaround_time <> ''
                If error# = 0
                    job:turnaround_time = trt:initial_priority
                    access:turnarnd.clearkey(tur:turnaround_time_key)
                    tur:turnaround_time = job:turnaround_time
                    if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
                        Turnaround_Routine(tur:days,tur:hours,end_date",end_time")
                        job:turnaround_end_date = Clip(end_date")
                        job:turnaround_end_time = Clip(end_time")
                        Do Time_Remaining
                    end!if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
                End!If error# = 0
            End
        End!If error# = 0
    end!if access:trantype.fetch(trt:transit_type_key) = Level:Benign
Location_Bit     Routine
        job:location = loi:location
        If location_temp <> ''
    !Add To Old Location
            access:locinter.clearkey(loi:location_key)
            loi:location = location_temp
            If access:locinter.fetch(loi:location_key) = Level:Benign
                If loi:allocate_spaces = 'YES'
                    loi:current_spaces+= 1
                    loi:location_available = 'YES'
                    access:locinter.update()
                End
            end !if
        End!If location_temp <> ''
    !Take From New Location
        access:locinter.clearkey(loi:location_key)
        loi:location = job:location
        If access:locinter.fetch(loi:location_key) = Level:Benign
            If loi:allocate_spaces = 'YES'
                loi:current_spaces -= 1
                If loi:current_spaces< 1
                    loi:current_spaces = 0
                    loi:location_available = 'NO'
                End
                access:locinter.update()
            End
        end !if
        location_temp  = job:location

        If job:date_completed = ''
            If job:engineer <> ''
                GetStatus(310,1,'JOB') !allocated to engineer

                Brw21.resetsort(1)
                Do time_remaining
            Else!If job:engineer <> ''
                GetStatus(305,1,'JOB') !awaiting allocation

                Brw21.resetsort(1)
                Do time_remaining
            End!If job:engineer <> ''
        End!If job:date_completed = ''

        brw21.resetsort(1)



Show_Hide_Tabs      Routine
    check_access('JOBS - SHOW COSTS',x")
    If x" = False
    Else!If x" = False

    End!If x" = False
    Thiswindow.reset(1)
Repair_Type_Bit     Routine
    price_error# = 0
    no_error# = 0
    If job:chargeable_job = 'YES'
        Pricing_Routine('C',labour",parts",pass",claim",handling",exchange",RRCRate",RRCParts")
        if pass" = False
            price_error# = 1
            Case Missive('A pricing structure has not been setup for this Repair Type.'&|
              '<13,10>'&|
              '<13,10>If you choose to continue you must inform your System Supervisor.'&|
              '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                           'mquest.jpg','\No|/Yes')
                Of 2 ! Yes Button
                    no_error# = 2
                Of 1 ! No Button
                    no_error# = 0
            End ! Case Missive
        End!if pass" = False

        If price_error# = 0
            If repair_type_temp = ''
                no_error# = 1
            Else!If repair_type_temp = ''
                If job:labour_cost <> labour" Or job:parts_cost <> parts"
                    Case Missive('The selected Repair Type has a different pricing structure. If you choose to continue this job will be re-valued accordingly.'&|
                      '<13,10>'&|
                      '<13,10>Are you sure?','ServiceBase 3g',|
                                   'mquest.jpg','\No|/Yes')
                        Of 2 ! Yes Button
                            no_error# = 3 !pricing Change
                        Of 1 ! No Button
                            no_error# = 0
                    End ! Case Missive
                Else!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
                    Case Missive('Are you sure you want to change the Repair Type?','ServiceBase 3g',|
                                   'mquest.jpg','\No|/Yes')
                        Of 2 ! Yes Button
                             no_error# = 2 !Repair Type Change Only
                        Of 1 ! No Button
                             no_error# = 0
                    End ! Case Missive
                End!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
            End!If repair_type_temp = ''
        End!price_error# = 0

        If no_error# = 0
            job:repair_type = repair_type_temp
        Else!If no_error# = 0
            Case no_error#
            Of 2
                IF (AddToAudit(job:ref_number,'JOB','CHARGEABLE REPAIR TYPE CHANGED: ' & Clip(job:repair_type),'NEW CHARGEABLE REPAIR TYPE: ' & Clip(job:repair_type) & '<13,10>OLD CHARGEABLE REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)))
                END ! IF

            Of 3
                IF (AddToAudit(job:ref_number,'JOB','REPRICE JOB - CHARGEABLE REPAIR TYPE CHANGED: ' & Clip(job:repair_type),'NEW CHARGEABLE REPAIR TYPE: ' & Clip(job:repair_type) & '<13,10>OLD CHARGEABLE REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)))
                END ! IF
            End
            If no_error# = 1 Or no_error# = 3
                Local.Pricing(0)
            End!If no_error# = 1 Or no_error# = 3
            repair_type_temp    = job:repair_type

            Do third_party_repairer
            Post(Event:accepted,?job:charge_type)
        End!If no_error# = 0
    End!If job:chargeable_job = 'YES'
Repair_Type_Warranty_Bit     Routine
    price_error# = 0
    no_error# = 0
    If job:warranty_job = 'YES'
        Pricing_Routine('W',labour",parts",pass",claim",handling",exchange",RRCRate",RRCParts")
        if pass" = False
            price_error# = 1
            Case Missive('A pricing structure has not been setup for this Repair Type.'&|
              '<13,10>'&|
              '<13,10>If you choose to continue you must inform your System Supervisor.'&|
              '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                           'mquest.jpg','\No|/Yes')
                Of 2 ! Yes Button
                    no_error# = 2
                Of 1 ! No Button
                    no_error# = 0
            End ! Case Missive
        End!if pass" = False


        If price_error# = 0
            If repair_type_temp = ''
                no_error# = 1
            Else!If repair_type_temp = ''
                If job:labour_cost_warranty <> labour" Or job:parts_cost_warranty <> parts"
                    Case Missive('The selected Repair Type has a different pricing structure. If you choose to continue this job will be re-valued accordingly.'&|
                      '<13,10>'&|
                      '<13,10>Are you sure?','ServiceBase 3g',|
                                   'mquest.jpg','\No|/Yes')
                        Of 2 ! Yes Button
                            no_error# = 3 !pricing Change
                        Of 1 ! No Button
                            no_error# = 0
                    End ! Case Missive
                Else!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
                    Case Missive('Are you sure you want to change the Repair Type?','ServiceBase 3g',|
                                   'mquest.jpg','\No|/Yes')
                        Of 2 ! Yes Button
                             no_error# = 2 !Repair Type Change Only
                        Of 1 ! No Button
                             no_error# = 0
                    End ! Case Missive
                End!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
            End!If repair_type_temp = ''
        End!price_error# = 0

        If no_error# = 0
            job:repair_type_warranty = repair_type_temp
        Else!If no_error# = 0
            Case no_error#
                Of 2
                    IF (AddToAudit(job:Ref_Number,'JOB','WARRANTY REPAIR TYPE CHANGED: ' & Clip(job:repair_type_warranty),'NEW WARRANTY REPAIR TYPE: ' & Clip(job:repair_type_warranty) & '<13,10>OLD WARRANTY REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)))
                    END ! IF
                Of 3
                    IF (AddToAudit(job:Ref_Number,'JOB','REPRICE JOB - WARRANTY REPAIR TYPE CHANGED: ' & Clip(job:repair_type_warranty),'NEW WARRANTY REPAIR TYPE: ' & Clip(job:repair_type_warranty) & '<13,10>OLD WARRANTY REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)))
                    END ! IF
            End
            If no_error# = 1 Or no_error# = 3
                Local.Pricing(0)
            End!If no_error# = 1 Or no_error# = 3
            repair_type_temp    = job:repair_type_warranty

            Do third_party_repairer
            Post(Event:accepted,?job:charge_type)
        End!If no_error# = 0
    End!If job:chargeable_job = 'YES'
Trade_Account_Lookup_Bit        Routine
Check_Parts     Routine
    If job:Date_Completed = ''
        !Only change Status if job is NOT completed.

        found_requested# = 0
        found_ordered# = 0
        found_received# = 0
        setcursor(cursor:wait)
        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number  <> job:ref_number      |
                then break.  ! end if
            If wpr:pending_ref_number <> ''
                found_requested# = 1
                Break
            End
            If wpr:order_number <> ''
                If wpr:date_received = ''
                    found_ordered# = 1
                    Break
                Else!If wpr:date_received = ''
                    found_received# = 1
                    Break
                End!If wpr:date_received = ''
            End
        end !loop
        access:warparts.restorefile(save_wpr_id)
        setcursor()

        If found_requested# = 0
            setcursor(cursor:wait)
            save_par_id = access:parts.savefile()
            access:parts.clearkey(par:part_number_key)
            par:ref_number  = job:ref_number
            set(par:part_number_key,par:part_number_key)
            loop
                if access:parts.next()
                   break
                end !if
                if par:ref_number  <> job:ref_number      |
                    then break.  ! end if
                If par:pending_ref_number <> ''
                    found_requested# = 1
                    Break
                End
                If par:order_number <> ''
                    If par:date_received = ''
                        found_ordered# = 1
                        Break
                    Else!If par:date_received = ''
                        found_received# = 1
                        Break
                    End!If par:date_received = ''
                End
            end !loop
            access:parts.restorefile(save_par_id)
            setcursor()
        End!If found_requested# = 0

        If found_requested# = 1
            GetStatus(330,thiswindow.request,'JOB') !spares requested

            Do time_remaining

        Else!If found_requested# = 1
            If found_ordered# = 1
                GetStatus(335,thiswindow.request,'JOB') !spares ordered
                brw21.resetsort(1)
                Do time_remaining
            Else!If found_ordered# = 1
                If found_received# = 1
                    GetStatus(345,thiswindow.request,'JOB') !spares received

                    brw21.resetsort(1)
                    Do time_remaining
                End!If found_received# = 1
            End!If found_ordered# = 1
        End!If found_requested# = 1

    End !If job:Date_Completed = ''

    Display()

Trade_Account_Status        Routine  ! Trade Account Status Display
!    access:subtracc.clearkey(sub:account_number_key)
!    sub:account_number = job:account_number
!    if access:subtracc.fetch(sub:account_number_key) = Level:Benign
!        access:tradeacc.clearkey(tra:account_number_key)
!        tra:account_number = sub:main_account_Number
!        if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!            If tra:invoice_sub_accounts = 'YES'
!                If sub:stop_account = 'YES'
!                    If sub:allow_cash_sales = 'YES'
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP - ALLOW CASH TRANSACTIONS ONLY'
!                    Else
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP'
!                    End
!                End
!
!            Else !If tra:invoice_sub_accounts = 'YES'
!                If tra:stop_account = 'YES'
!                    If tra:allow_cash_sales = 'YES'
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP - ALLOW CASH TRANSACTIONS ONLY'
!                    Else
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP'
!                    End
!                End
!            End !If tra:invoice_sub_accounts = 'YES'
!
!            If tra:use_contact_name = 'YES'
!            End
!        end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!    end !if access:subtracc.fetch(sub:account_number_key) = Level:Benign
Show_Tabs       Routine   !Loan Exchange Tabs

    access:subtracc.clearkey(sub:account_number_key)
    sub:account_number = job:account_number
    if access:subtracc.fetch(sub:account_number_key) = Level:Benign
        access:tradeacc.clearkey(tra:account_number_key)
        tra:account_number = sub:main_account_number
        if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
            If tra:Allow_Loan = 'YES'
                ?LoanButton{prop:Hide} = 0
            Else !If tra:Allow_Loan = 'YES'
                ?LoanButton{prop:Hide} = 1
            End !If tra:Allow_Loan = 'YES'
            If tra:Allow_Exchange = 'YES'
                ?ExchangeButton{prop:Hide} = 0
            Else !If tra:Allow_Exchange = 'YES'
                ?ExchangeButton{prop:Hide} = 1
            End !If tra:Allow_Exchange = 'YES'
        end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
    end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign


Estimate_Check      Routine     !Estiamte Status Display
Check_Msn       Routine   !Does Manufacturer Use MSN Number
    esn_fail_temp = 'NO'
    msn_fail_temp = 'NO'
    If MSNRequired(job:Manufacturer) = Level:Benign
        ?job:MSN{prop:Hide} = 0
        ?job:MSN:Prompt{prop:Hide} = 0

        Set(DEFAULTS)
        Access:DEFAULTS.Next()
        If ThisWindow.Request = InsertRecord
            If def:Force_MSN = 'B'
                ?job:MSN{prop:Req} = 1
            Else !If def:Force_MSN = 'B'
                ?job:MSN{prop:Req} = 0
            End !If def:Force_MSN = 'B'
        Else !If ThisWindow.Request = InsertRecord
            If def:Force_MSN = 'I'
                ?job:MSN{prop:Req} = 0
            Else !If def:Force_MSN = 'I'
                ?job:MSN{prop:Req} = 1
            End !If def:Force_MSN = 'I'
        End !If ThisWindow.Request = InsertRecord


    Else !If MSNRequired(job:Manufacturer) = Level:Benign
        ?job:MSN{prop:Hide} = 1
        ?job:MSN:Prompt{prop:Hide} = 1
    End !If MSNRequired(job:Manufacturer) = Level:Benign

    If CheckLength('IMEI',job:Model_Number,job:ESN)
        ESN_Fail_Temp = 'YES'
        job:ESN = ''
    End !If CheckLength('IMEI',job:Model_Number,job:ESN)

    If ?job:MSN{prop:Hide} = 0
        If CheckLength('MSN',job:Model_Number,job:MSN)
            MSN_Fail_Temp = 'YES'
            job:MSN = ''
        End !If CheckLength('MSN',job:Model_Number,job:MSN)
    End !If ?job:MSN{prop:Hide} = 0


    Case ProductCodeRequired(job:Manufacturer)
        of Level:Benign
            ?job:ProductCode{prop:Hide} = 0
            ?job:ProductCode:Prompt{prop:Hide} = 0
        of Level:notify
            ?job:ProductCode{prop:Hide} = 0
            ?job:ProductCode:Prompt{prop:Hide} = 0
            ?job:ProductCode{prop:Req} = 1
        ELSE    !Level:Fatal            
            ?job:ProductCode{prop:Hide} = 1
            ?job:ProductCode:Prompt{prop:Hide} = 1
    End !case ProductCodeRequired(job:Manufacturer)

Titles      Routine     !Show Date Booked etc.
    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    access:webjob.clearkey(wob:RefNumberKey)
    wob:RefNumber = Job:Ref_number
    if access:webjob.fetch(wob:refNumberKey) then
        !error not found in web job
        tmp:JobNumber = job:ref_number
    Else
        If glo:WebJob
            access:tradeacc.clearkey(tra:Account_Number_Key)
            tra:Account_Number = ClarioNET:Global.Param2
            access:tradeacc.fetch(tra:Account_Number_Key)
            tmp:JobNumber = wob:refnumber & '-' & tra:BranchIdentification & wob:JobNumber
            ?Title_Text{prop:Text} = '  Head Acc: ' & Clip(tra:Account_Number)
        Else !If glo:WebJob
            access:tradeacc.clearkey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            access:tradeacc.fetch(tra:Account_Number_Key)
            tmp:JobNumber = wob:refnumber & '-' & tra:BranchIdentification & wob:JobNumber
            ?Title_Text{prop:Text} = '  Head Acc: ' & Clip(tra:Account_Number)
        End !If glo:WebJob
    End !if

    If job:batch_number <> ''
        ?title_text{prop:text} ='Repair Details - Job No: ' & Clip(tmp:JobNumber) & |
                                '  Batch No: ' & Clip(Format(job:batch_number,@p<<<<<<#p)) & |
                                 Clip(?Title_Text{prop:Text})
    Else!If job:batch_number <> ''
        ?title_text{prop:text} ='Repair Details - Job No: ' & Clip(tmp:JobNumber) & |
                                 Clip(?Title_Text{prop:Text})
    End!If job:batch_number <> ''

    ! Check if handset is exchange repair
    save_xch_ali_id = Access:EXCHANGE_ALIAS.SaveFile()
    Access:EXCHANGE_ALIAS.ClearKey(xch_ali:ESN_Only_Key)
    xch_ali:ESN = job:ESN
    if Access:EXCHANGE_ALIAS.Fetch(xch_ali:ESN_Only_Key) = Level:Benign
        ! if exchange unit is in repair / exchange repair display on title
        if (xch_ali:Available = 'REP') or (xch_ali:Available = 'INR')
            ?ExchangeRepair{Prop:Text} = 'EXCHANGE REPAIR'
        end
    end
    Access:EXCHANGE_ALIAS.RestoreFile(save_xch_ali_id)

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
!            If tra:Use_Sub_Accounts = 'YES'
!                ?Title_Text{prop:Text} = Clip(?Title_Text{prop:Text}) & '  Head Acc: ' & Clip(tra:Account_Number)
!            End !If tra:Use_Sub_Accounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign


    ?batch_number_text{prop:text} = 'Date Booked: ' &  Clip(Format(job:date_booked,@d6b)) & ' ' & |
                                    Clip(Format(job:time_booked,@t1b)) & ' - ' & Clip(job:who_booked)
    ?exchanged_title{prop:text} = ''
    If job:exchange_unit_number <> ''
        ?exchanged_title{prop:text} = 'Customer''s Unit Exchanged'
    End!If job:exchange_unit_number <> ''
    If job:loan_unit_number <> ''
        ?exchanged_title{prop:text} = 'Loan Unit Issued'
    End!If job:loan_unit_number <> ''

    Display()

    If job:date_completed <> ''
        job:completed = 'YES'
    Else
        job:completed = 'NO'
    End

    trade_account_string_temp = Clip(job:account_number) & ' - ' & Clip(job:company_name)
    If job:title <> '' and job:initial <> ''
        customer_name_String_temp = Clip(job:title) & ' ' & Clip(job:initial) & ' ' & Clip(job:surname)
    End!If job:title <> '' and job:initial <> ''
    If job:title = '' And job:initial <> ''
        customer_name_string_temp = Clip(job:initial) & ' ' & Clip(job:Surname)
    End!If job:title = '' And job:initial <> ''
    If job:title <> '' And job:initial = ''
        customer_name_String_temp = Clip(job:title) & ' ' & Clip(job:surname)
    End!If job:title <> '' And job:initial = ''
    If job:title = '' And job:initial = ''
        customer_name_string_temp = Clip(job:surname)
    End!If job:title = '' And job initial = ''

    If job:chargeable_job = 'YES'
        Unhide(?chargeable_type_string)
        Unhide(?job:charge_type:2)
    End
    If job:warranty_job = 'YES'
        Unhide(?Warranty_charge_Type_string)
        Unhide(?job:warranty_charge_type:2)
    End
    If job:location <> '' And job:workshop = 'YES'
        If ~def:HideLocation
            ?Location_String{prop:Hide} = 0
            ?job:Location:2{prop:Hide} = 0
        Else
            tmp:DisableLocation = GETINI('DISABLE','InternalLocation',,CLIP(PATH())&'\SB2KDEF.INI')
            if tmp:DisableLocation
                ?Location_String{prop:Hide} = 0
                ?job:Location:2{prop:Hide} = 0
            end
        End !If ~def:HideLocation
    End
    If job:workshop <> '' And job:third_party_site <> ''
        Unhide(?third_party_string)
        Unhide(?job:third_party_site:2)
    End!If job:workshop = '' And job:third_party_site <> ''

    If job:special_instructions <> ''
        Unhide(?special_instructions_string)
        Unhide(?job:special_instructions:2)
    End

! Changing (DBH 01/03/2007) # 8820 - Show same "history" button as on engineering screen
!    bouncers# = 0
!    bouncers# = CountBouncer(job:ref_number,job:date_booked,job:esn,|
!                job:Chargeable_job,job:Charge_Type,job:Repair_Type,|
!                job:Warranty_Job,job:Warranty_Charge_Type,job:Repair_Type_warranty)
!    If bouncers#
!        Unhide(?BouncerText)
!        ?bouncertext{prop:text} = Clip(bouncers#) & ' BOUNCER(S)'
!        ?History:2{prop:Hide} = 0
!    Else!If bouncers#
!        Hide(?BouncerText)
!        ?History:2{prop:Hide} = 1
!    End!If bouncers#
! to (DBH 01/03/2007) # 8820
    x# = CountHistory(job:ESN,job:Ref_Number,job:Date_Booked)
    If x# > 0
        ?BouncerText{prop:Hide} = 0
        ?History:2{prop:Hide} = 0
        ?BouncerText{prop:Text} = x# & ' PREV. JOB(S)'
    Else ! If x# > 0
        ?BouncerText{prop:Hide} = 1
        ?History:2{prop:Hide} = 1
    End ! If x# > 0
! End (DBH 01/03/2007) #8820

    !IMEIs
    !Exchange Unit
    tmp:ExchangeIMEI   = 'N/A'
    tmp:ExchangeMSN    = 'N/A'
    If job:Exchange_Unit_Number <> 0
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            !Found
            tmp:ExchangeIMEI   = xch:ESN
            tmp:ExchangeMSN    = xch:MSN
        Else!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
    End !If job:Exchange_Unit_Number <> 0

    !Third Party
    tmp:IncomingIMEI    = job:Esn       !default to be used if nothing else found
    tmp:IncomingMSN     = job:Msn
    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
    jot:RefNumber = job:Ref_Number
    Set(jot:RefNumberKey,jot:RefNumberKey)
    If Access:JOBTHIRD.NEXT() = Level:Benign
        If job:Ref_Number = jot:RefNumber
            tmp:IncomingIMEI    = jot:OriginalIMEI
            tmp:IncomingMSN     = jot:OriginalMSN
            !Not implimented yet
        End !If job:Ref_Number = jot:RefNumber
    End !If Access:JOBTHIRD.NEXT() = Level:Benign
    If tmp:IncomingIMEI = ''
        tmp:IncomingIMEI = 'N/A'
    End !If tmp:IncomingIMEI = ''
    If tmp:IncomingMSN  = ''
        tmp:IncomingMSN = 'N/A'
    End !If tmp:IncomingMSN  = ''

    tmp:2ndExchangeIMEI = 'N/A'
    tmp:2ndExchangeMSN  = 'N/A'
    If jobe:SecondExchangeNumber <> 0
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number  = jobe:SecondExchangeNumber
        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Found
            tmp:2ndExchangeIMEI = xch:ESN
            tmp:2ndExchangeMSN  = xch:MSN
        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End !If jobe:SecondExchangeNumber <> 0

    !Estimate Details
    If job:Estimate = 'YES' and ?job:Estimate{prop:Hide} = 0
        tmp:EstimateDetails = 'YES     If Over: ' & Clip(job:Estimate_If_Over) & '   Status: '
        If job:Estimate_Accepted = 'YES'
            tmp:EstimateDetails = Clip(tmp:EstimateDetails) & ' Accepted'
        Else !If job:EstimateAccepted = 'YES'
            If job:Estimate_Rejected = 'YES'
                tmp:EstimateDetails = Clip(tmp:EstimateDetails) & ' Rejected'
            Else !If job:Estimate_Rejected = 'YES'
                If job:Estimate_Ready = 'YES'
                    if Sub(job:Current_Status,1,3) = '520'
                        tmp:EstimateDetails = Clip(tmp:EstimateDetails) & ' Sent'
                    else
                        tmp:EstimateDetails = Clip(tmp:EstimateDetails) & ' Ready'
                    end
                Else !If job:Estimate_Ready = 'YES'
                    tmp:EstimateDetails = Clip(tmp:EstimateDetails) & ' In Progress'
                End !If job:Estimate_Ready = 'YES'
            End !If job:Estimate_Rejected = 'YES'
        End !If job:EstimateAccepted = 'YES'
    Else
        tmp:EstimateDetails = ''
    End !If job:Estimate = 'YES'
Totals      Routine   !Work Out The Financial Stuff

    Total_Price('C',vat",total",balance")
    job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
    total_temp = total"
    vat_chargeable_temp = vat"
    balance_due_temp = balance"

    Total_Price('E',vat",total",balance")
    job:sub_total_estimate = job:labour_cost_estimate + job:parts_cost_estimate + job:courier_cost_estimate
    total_estimate_temp = total"
    vat_estimate_temp = vat"

    Total_Price('W',vat",total",balance")
    job:sub_total_warranty  = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
    vat_warranty_temp = vat"
    total_warranty_temp = total"
    balance_due_warranty_temp = balance"
    Display()

Update_Engineer    Routine  !Engineers Full Name
    !Engineer
    Save_joe_ID = Access:JOBSENG.SaveFile()
    Access:JOBSENG.ClearKey(joe:UserCodeKey)
    joe:JobNumber     = job:Ref_Number
    joe:UserCode      = job:Engineer
    joe:DateAllocated = Today()
    Set(joe:UserCodeKey,joe:UserCodeKey)
    Loop
        If Access:JOBSENG.PREVIOUS()
           Break
        End !If
        If joe:JobNumber <> job:Ref_Number
            Break
        End !If joe:JobNumber <> job:Ref_Number

        If joe:UserCode      <> job:Engineer      |
        Or joe:DateAllocated > Today()      |
            Then Break.  ! End If
        tmp:SkillLevel      = joe:EngSkillLevel
        tmp:DateAllocated   = joe:DateAllocated
        Break
    End !Loop
    Access:JOBSENG.RestoreFile(Save_joe_ID)

    Access:users.Clearkey(use:user_code_key)
    use:user_code    = job:engineer
    If Access:users.Fetch(use:user_Code_Key) = Level:Benign
        engineer_name_temp   = Clip(use:forename) & ' ' & Clip(use:surname)
!        tmp:SkillLevel  = use:SkillLevel
    End
    Display()
Update_Loan     Routine     !Sort Out Loan And Exchange Information
    If job:loan_unit_number = ''
        job:loan_user = ''
        loan_model_make_temp = ''
        loan_esn_temp = ''
        loan_msn_temp = ''
        Loan_Accessories_Temp = ''
    Else
        count# = 0
        setcursor(cursor:wait)
        save_lac_id = access:loanacc.savefile()
        access:loanacc.clearkey(lac:ref_number_key)
        lac:ref_number = loa:ref_number
        set(lac:ref_number_key,lac:ref_number_key)
        loop
            if access:loanacc.next()
               break
            end !if
            if lac:ref_number <> loa:ref_number      |
                then break.  ! end if
            count# += 1
        end !loop
        access:loanacc.restorefile(save_lac_id)
        setcursor()

        If count# = 0
            Loan_Accessories_Temp = ''
        End!If count# = 0
        If count# > 1
            loan_accessories_temp = count#
        End!If count# > 1

        access:loan.clearkey(loa:ref_number_key)
        loa:ref_number = job:loan_unit_number
        if access:loan.fetch(loa:ref_number_key) = Level:Benign
            loan_model_make_temp    = Clip(loa:ref_number) & ': ' & Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
            loan_esn_temp           = loa:esn
            loan_msn_temp           = loa:msn
        end !if access:loan.fetch(loa:ref_number_key) = Level:Benign
    End

Update_Exchange     Routine
    If job:exchange_unit_number = ''
        job:exchange_user = ''
        exchange_model_make_temp = ''
        exchange_esn_temp = ''
        exchange_msn_temp = ''
        exchange_Accessories_Temp = ''
    End
    count# = 0
    save_jea_id = access:jobexacc.savefile()
    access:jobexacc.clearkey(jea:part_number_key)
    jea:job_ref_number = job:Ref_number
    set(jea:part_number_key,jea:part_number_key)
    loop
        if access:jobexacc.next()
           break
        end !if
        if jea:job_ref_number <> job:ref_number      |
            then break.  ! end if
        count# += 1
        Exchange_Accessories_Temp = Clip(jea:part_number) & '-' & Clip(jea:description)
    end !loop
    access:jobexacc.restorefile(save_jea_id)

    If count# = 0
        Exchange_Accessories_Temp = ''
    End!If count# = 0
    If count# > 1
        Exchange_Accessories_temp = count#
    End!If count# > 1
    access:exchange.clearkey(xch:ref_number_key)
    xch:ref_number = job:exchange_unit_number
    if access:exchange.fetch(xch:ref_number_key) = Level:Benign
        exchange_model_make_temp    = Clip(job:exchange_unit_number) & ': ' & Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
        exchange_esn_temp           = xch:esn
        exchange_msn_temp           = xch:msn
    end !if access:exchange.fetch(xch:ref_number_key) = Level:Benign

Count_accessories       Routine
    exchange_accessory_count_temp = 0
    save_jea_id = access:jobexacc.savefile()
    access:jobexacc.clearkey(jea:part_number_key)
    jea:job_ref_number = job:Ref_number
    set(jea:part_number_key,jea:part_number_key)
    loop
        if access:jobexacc.next()
           break
        end !if
        if jea:job_ref_number <> job:ref_number      |
            then break.  ! end if
        exchange_accessory_count_temp += 1
    end !loop
    access:jobexacc.restorefile(save_jea_id)
Third_Party_Repairer        Routine    !Don't Think This Is Used Anymore
    clear(trm:record)
    trm:model_number = job:model_number
    get(trdmodel,trm:model_number_only_key)
    if errorcode()
       clear(trm:record)
!       Hide(?3rd_Party_Option)
    Else
!        Unhide(?3rd_Party_Option)
    end !if
Repair_Type_Check       Routine      !Third Party Repair Type
Repair_type_Lookup      Routine      !Lookup Repair Type

    saverequest#      = globalrequest
    globalresponse    = requestcancelled
    globalrequest     = selectrecord
    glo:select1    = job:model_number
    browserepairty
    if globalresponse = requestcompleted
        job:repair_type = rep:repair_type
        glo:select1 = ''
        display()
    Else
        glo:select1 = 'END'
    end
    globalrequest     = saverequest#
Repair_type_Warranty_Lookup      Routine      !Lookup Repair Type

    saverequest#      = globalrequest
    globalresponse    = requestcancelled
    globalrequest     = selectrecord
    glo:select1    = job:model_number
    browserepairty
    if globalresponse = requestcompleted
        job:repair_type_warranty = rep:repair_type
        glo:select1 = ''
        display()
    Else
        glo:select1 = 'END'
    end
    globalrequest     = saverequest#
Transit_type        Routine     !Lookup Transit Type
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    Access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = job:transit_type
    If Access:trantype.Fetch(trt:transit_type_key) = Level:Benign
        If trt:location <> 'YES'
            Hide(?location_group)
        Else
            If ~def:HideLocation
                Unhide(?location_group)
            Else
                tmp:DisableLocation = GETINI('DISABLE','InternalLocation',,CLIP(PATH())&'\SB2KDEF.INI')
                if tmp:DisableLocation
                    Unhide(?location_group)
                    Disable(?location_group)
                end
            End !If ~def:HideLocation
        End
    end !if
    Display()
Location_Type       Routine     !Hide/Show If Workshop
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    Case job:workshop
        Of 'YES'
            access:trantype.clearkey(trt:transit_type_key)
            trt:transit_type = job:transit_type
            if access:trantype.fetch(trt:transit_type_key) = Level:Benign
                If trt:location <> 'YES'
                    Hide(?location_group)
                Else
                    If ~def:HideLocation
                        Unhide(?location_group)
                    Else
                        tmp:DisableLocation = GETINI('DISABLE','InternalLocation',,CLIP(PATH())&'\SB2KDEF.INI')
                        if tmp:DisableLocation
                            Unhide(?location_group)
                            Disable(?location_group)
                        end
                    End !If ~def:HideLocation
                End
            Else
                If ~def:HideLocation
                    Unhide(?location_group)
                Else
                    tmp:DisableLocation = GETINI('DISABLE','InternalLocation',,CLIP(PATH())&'\SB2KDEF.INI')
                    if tmp:DisableLocation
                        Unhide(?location_group)
                        Disable(?location_group)
                    end
                End !If ~def:HideLocation
            end
            Hide(?job:third_party_site)
            hide(?job:third_party_site:prompt)
            Hide(?third_party_message)
            Hide(?job:ThirdPartyDateDesp)
            Hide(?job:third_party_despatch_date:prompt)
            Hide(?job:date_paid)
            Hide(?job:date_paid:prompt)
            If job:third_party_site <> ''
                Unhide(?thirdpartybutton)
            Else!If job:third_party_site <> ''
                Hide(?thirdpartybutton)
            End!If job:third_party_site <> ''
        Of 'NO'
            Hide(?location_group)
            If job:third_party_site <> ''
                Unhide(?thirdpartybutton)
                Unhide(?job:third_party_site)
                Unhide(?job:third_party_site:prompt)
                Unhide(?third_party_message)
                Unhide(?job:ThirdPartyDateDesp)
                Unhide(?job:third_party_despatch_date:prompt)
                Unhide(?job:date_paid)
                Unhide(?job:date_paid:prompt)
            End
    ENd
Charge_Type     Routine      !Is this an estimate charge type
    If job:chargeable_job   = 'YES'
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:allow_estimate = 'YES'
                Unhide(?job:estimate)
                If job:estimate <> 'YES' and cha:force_estimate = 'YES'
                    Case Missive('The selected Charge Type means that this job must be an Estimate.','ServiceBase 3g',|
                                   'mexclam.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    job:estimate = 'YES'
                    Post(Event:accepted,?job:estimate)
                End!If job:estimate <> 'YES'
            Else
                Hide(?job:estimate)
            End
        end !If access:chartype.fetch(cha:charge_type_key) = Level:Benign
    End!If job:chargeable_job   = 'YES'
Estimate        Routine       !Hide/Show If Estimate
    If job:Chargeable_Job = 'YES'
        If job:estimate = 'YES' And ?job:Estimate{prop:Hide} = 0
            Unhide(?job:Estimate)
            Unhide(?job:estimate_if_over:prompt)
            Unhide(?job:estimate_if_over)
            Unhide(?job:estimate_accepted)
            Unhide(?job:estimate_rejected)
            set(defaults)
            access:defaults.next()
            If job:estimate_if_over = ''
                job:estimate_if_over    = def:estimate_if_over
            End
        Else
            Hide(?job:estimate_if_over:prompt)
            Hide(?job:estimate_if_over)
            Hide(?job:estimate_accepted)
            Hide(?job:estimate_rejected)
        End
    End!If job:Chargeable_Job = 'YES'
Update_accessories      Routine
    count# = 0
    setcursor(cursor:wait)
    save_jac_id = access:jobacc.savefile()
    access:jobacc.clearkey(jac:ref_number_key)
    jac:ref_number = job:ref_number
    set(jac:ref_number_key,jac:ref_number_key)
    loop
        if access:jobacc.next()
           break
        end !if
        if jac:ref_number <> job:ref_number      |
            then break.  ! end if
        count# += 1
    end !loop
    access:jobacc.restorefile(save_jac_id)
    setcursor()

    If count# = 1
        access:jobacc.clearkey(jac:ref_number_key)
        jac:ref_number = job:ref_number
        Set(jac:ref_number_key,jac:ref_number_key)
        access:jobacc.next()
        Accessories_Temp = jac:accessory
    Else
        accessories_temp = Clip(count#)
    End

    If Accessories_Temp > 0
        ?Accessory_Button:2{prop:fontstyle} = font:bold
    Else
        ?Accessory_Button:2{prop:fontstyle} = font:regular
    End !If Accessories_Temp > 0
Get_Audit       Routine
    Clear(aud:record)
    aud:ref_number  = job:ref_number
    aud:date        = Today()
    aud:time        = Clock()
    clear(use:record)
    use:password =glo:password
    get(users,use:password_key)
    aud:user        = use:user_code
Time_Remaining          Routine      !Turnaround Time Nightmare
    Set(defaults)
    access:defaults.next()
    Compile('***',Debug=1)
        Unhide(?job:status_end_date)
        Unhide(?job:status_end_time)
    ***
    If job:date_completed <> ''
        status_time_remaining_temp = 'Job Completed'
        job_time_remaining_temp = 'Job Completed'

    Else!If job:date_completed <> ''
!Status Time
        If job:status_end_date = Deformat('1/1/2050',@d6) Or job:status_end_date = ''
            ?status_time_remaining_temp{prop:fontcolor} = color:green
            status_time_remaining_temp = 'Never Expire'
        Else !If job:status_end_date = ''
            If job:status_end_date > Today()
                count# = 0
                day_count_temp = 0
                Loop
                    count# += 1
                    day_number_temp = (Today() + count#)
                    If def:include_saturday <> 'YES'
                        If day_number_temp % 7 = 6
                            Cycle
                        End!If day_number_temp % 7 = 6
                    End!If def:include_saturday <> 'YES'
                    If def:include_sunday <> 'YES'
                        If day_number_temp % 7 = 0
                            Cycle
                        End!If day_number_temp % 7 = 0
                    End!If def:include_sunday <> 'YES'
                    day_count_temp += 1
                    If day_number_temp >= job:status_end_date
                        Break
                    End!If day_number_temp = job:status_end_date
                End!Loop
                ?status_time_remaining_temp{prop:fontcolor} = color:navy
                If day_count_temp = 1
                    status_time_remaining_temp = Clip(day_count_temp) & ' Day'
                Else!If day_count_temp = 1
                    status_time_remaining_temp = Clip(day_count_temp) & ' Days'
                End!If day_count_temp = 1
            End!If job:status_end_date > Today()

            If job:status_end_date < Today()
                count# = 0
                day_count_temp = 0
                Loop
                    count# += 1
                    day_number_temp = (job:status_end_date + count#)
                    If def:include_saturday <> 'YES'
                        If day_number_temp % 7 = 6
                            Cycle
                        End!If day_number_temp % 7 = 6
                    End!If def:include_saturday <> 'YES'
                    If def:include_sunday <> 'YES'
                        If day_number_temp % 7 = 0
                            Cycle
                        End!If day_number_temp % 7 = 0
                    End!If def:include_sunday <> 'YES'
                    day_count_temp += 1
                    If day_number_temp >= Today()
                        Break
                    End!If day_number_temp = job:status_end_date
                End!Loop
                ?status_time_remaining_temp{prop:fontcolor} = color:red
                If day_count_temp = 1
                    status_time_remaining_temp = Clip(day_count_temp) & ' Day Over'
                Else!If day_count_temp = 1
                    status_time_remaining_temp = Clip(day_count_temp) & ' Days Over'
                End!If day_count_temp = 1
            End!If job:status_end_date < Today()

            If job:status_end_date = Today()
                If job:status_end_time > Clock()
                    ?status_time_remaining_temp{prop:fontcolor} = color:green
                    status_time_remaining_temp = Format(job:status_end_time - Clock(),@t1) & ' Hrs'
                End!If job:status_end_time > Clock()
                If job:status_end_time < Clock()
                    ?status_time_remaining_temp{prop:fontcolor} = color:red
                    status_time_remaining_temp = Format(Clock() - job:status_end_time,@t1) & ' Hrs Over'
                End!If job:status_end_time > Clock()
                If job:status_end_time = Clock()
                    ?status_time_remaining_temp{prop:fontcolor} = color:red
                    status_time_remaining_temp = 'Due Now'
                End
            End!If job:status_end_date = Today()
        End !If job:status_end_date = ''

!Turnaround Time
        If job:turnaround_end_date = Deformat('1/1/2050',@d6) Or job:turnaround_end_date = ''
            job_time_remaining_temp = 'Never Expire'
        Else !If job:turnaround_end_date = ''
            If job:turnaround_end_date > Today()
                count# = 0
                day_count_temp = 0
                Loop
                    count# += 1
                    day_number_temp = (Today() + count#)
                    If def:include_saturday <> 'YES'
                        If day_number_temp % 7 = 6
                            Cycle
                        End!If day_number_temp % 7 = 6
                    End!If def:include_saturday <> 'YES'
                    If def:include_sunday <> 'YES'
                        If day_number_temp % 7 = 0
                            Cycle
                        End!If day_number_temp % 7 = 0
                    End!If def:include_sunday <> 'YES'
                    day_count_temp += 1
                    If day_number_temp >= job:turnaround_end_date
                        Break
                    End!If day_number_temp = job:turnaround_end_date
                End!Loop
                If day_count_temp = 1
                    job_time_remaining_temp = Clip(day_count_temp) & ' Day'
                Else!If day_count_temp = 1
                    job_time_remaining_temp = Clip(day_count_temp) & ' Days'
                End!If day_count_temp = 1
            End!If job:turnaround_end_date > Today()

            If job:turnaround_end_date < Today()
                count# = 0
                day_count_temp = 0
                Loop
                    count# += 1
                    day_number_temp = (job:turnaround_end_date + count#)
                    If def:include_saturday <> 'YES'
                        If day_number_temp % 7 = 6
                            Cycle
                        End!If day_number_temp % 7 = 6
                    End!If def:include_saturday <> 'YES'
                    If def:include_sunday <> 'YES'
                        If day_number_temp % 7 = 0
                            Cycle
                        End!If day_number_temp % 7 = 0
                    End!If def:include_sunday <> 'YES'
                    day_count_temp += 1
                    If day_number_temp >= Today()
                        Break
                    End!If day_number_temp = job:turnaround_end_date
                End!Loop
                If day_count_temp = 1
                    job_time_remaining_temp = Clip(day_count_temp) & ' Day Over'
                Else!If day_count_temp = 1
                    job_time_remaining_temp = Clip(day_count_temp) & ' Days Over'
                End!If day_count_temp = 1
            End!If job:turnaround_end_date < Today()
            If job:turnaround_end_date = Today()
                If job:turnaround_end_time > Clock()
                    job_time_remaining_temp = Format(job:turnaround_end_time - Clock(),@t1) & ' Hrs'
                End!If job:turnaround_end_time > Clock()
                If job:turnaround_end_time < Clock()
                    job_time_remaining_temp = Format(Clock() - job:turnaround_end_time,@t1) & ' Hrs Over'
                End!If job:turnaround_end_time > Clock()
                If job:turnaround_end_time = Clock()
                    job_time_remaining_temp = 'Due Now'
                End
            End!If job:turnaround_end_date = Today()
        End !If job:turnaround_end_date = ''
    End!If job:date_completed <> ''
History_Search          Routine !AutoSearching
    Set(defaults)
    access:defaults.next()
    glo:select1 = ''
    glo:select2 = ''
    Do History_Search2
    If glo:select1 <> ''
        glo:select2 = job:auto_search
        glo:select12 = ''
        If ThisWindow.Request <> insertrecord
            glo:select12 = 'VIEW ONLY'
            glo:select11 = job:ref_number
        End

        !Auto_Search_Window
        glo:select11 = ''
        glo:select12 = ''
        If ThisWindow.Request = Insertrecord
            If glo:select3 <> ''
                access:jobs_alias.clearkey(job_ali:ref_number_key)
                job_ali:ref_number = glo:select3
                if access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
                    access:jobnotes_alias.clearkey(jbn_ali:RefNumberKey)
                    jbn_ali:RefNUmber   = jbn:RefNumber
                    access:jobnotes_alias.tryfetch(jbn_ali:RefNumberKey)
                    If glo:select5 = 'Y'
                        job:account_number  = job_ali:account_number
                        job:trade_account_name  = job_ali:trade_account_name
                        job:postcode         = job_ali:postcode
                        job:company_name     = job_ali:company_name
                        job:address_line1    = job_ali:address_Line1
                        job:address_line2    = job_ali:address_Line2
                        job:address_line3    = job_ali:address_line3
                        job:telephone_number = job_ali:telephone_number
                        job:fax_number       = job_ali:fax_number
                    End
                    If glo:select6 = 'Y'
                        job:postcode_collection         = job_ali:postcode_collection
                        job:company_name_collection     = job_ali:company_name_collection
                        job:address_line1_collection    = job_ali:address_Line1_collection
                        job:address_line2_collection    = job_ali:address_Line2_collection
                        job:address_line3_collection    = job_ali:address_line3_collection
                        job:telephone_collection = job_ali:telephone_collection
                        job:postcode_delivery         = job_ali:postcode_delivery
                        job:company_name_delivery     = job_ali:company_name_delivery
                        job:address_line1_delivery    = job_ali:address_Line1_delivery
                        job:address_line2_delivery    = job_ali:address_Line2_delivery
                        job:address_line3_delivery    = job_ali:address_line3_delivery
                        job:telephone_delivery = job_ali:telephone_delivery
                    End
                    If glo:select7 = 'Y'
                        job:model_number = job_ali:model_number
                        job:mobile_number = job_ali:mobile_number
                        job:manufacturer    = job_ali:manufacturer
                        job:unit_type       = job_ali:unit_type
                        job:esn             = job_ali:esn
                        job:msn             = job_ali:msn
                        job:dop             = job_ali:dop
                    End
                    If glo:select8 = 'Y'
                        job:title   = job_ali:title
                        job:initial = job_ali:initial
                        job:surname = job_ali:surname
                    End
                    access:jobnotes.clearkey(jbn:RefNumberKey)
                    jbn:RefNumber   = job:Ref_Number
                    access:jobnotes.tryfetch(Jbn:RefNumberKey)
                    If glo:select9 = 'Y'
                        jbn:engineers_notes = jbn_ali:engineers_notes
                    End
                    If glo:select10 = 'Y'
                        jbn:invoice_text    = jbn_ali:invoice_text
                    End
                    access:jobnotes.update()
                    IF glo:select11 = 'Y'
                    End
                    job:last_repair_days    = Today() - job_ali:date_booked
                    Do last_repair_days
                    If job:last_repair_days < def:warranty_period And glo:select7 = 'Y'
                        Case Missive('This unit was previously repaired within the Internal Warranty Period specified by the System Supervisor.'&|
                          '<13,10>Please check with your Supervisor to ascertain whether this unit should be repaired FREE OF CHARGE.','ServiceBase 3g',|
                                       'mexclam.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive

! Fill In Engineer's Notes With Previous Jobs

                        setcursor(cursor:wait)
                        save_job_ali_id = access:jobs_alias.savefile()
                        access:jobs_alias.clearkey(job_ali:esn_key)
                        job_ali:esn = job:esn
                        set(job_ali:esn_key,job_ali:esn_key)
                        loop
                            if access:jobs_alias.next()
                               break
                            end !if
                            if job_ali:esn <> job:esn      |
                                then break.  ! end if
                            If job_ali:ref_number = job:ref_number
                                Cycle
                            End
                            Jbn:engineers_notes = Clip(jbn:engineers_notes) & '<13,10>Previous Repair: <13,10>Job No: ' & Clip(Format(job_ali:ref_number,@n012))|
                                                     & ' Date: ' & Clip(Format(job_ali:date_booked,@d6b)) & ' Eng: ' & Clip(job_ali:engineer)
                            access:jobnotes.update()
                        end !loop
                        access:jobs_alias.restorefile(save_job_ali_id)
                        setcursor()
                    End

                    Clear(glo:G_Select1)
                    Clear(glo:G_Select1)

                end !if access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
            End!If glo:select3 <> ''
        End
    Else
        If ThisWindow.Request = insertrecord
            glo:select1 = 'NO MATCH'
            glo:select2 = job:auto_search
            Hide(?job:last_repair_days)
            Hide(?job:last_repair_days:prompt)
            If def:automatic_replicate <> 'YES'
                !New_Job_Screen
                Case glo:select2
                    Of 'SURNAME'
                        job:surname = job:auto_search
                    Of 'MOBILE NUMBER'
                        job:mobile_number = job:auto_search
                    Of 'ESN'
                        job:esn = job:auto_search
                        Post(Event:Accepted,?job:ESN)
                    Of 'MSN'
                        job:msn = job:auto_search
                    Of 'POSTCODE'
                        job:postcode = job:auto_search
                        Postcode_Routine(job:postcode,job:address_line1,job:address_line2,job:address_line3)
                        Display()
                    Of 'SEARCH AGAIN'
                        Clear(job:auto_search)
                        Select(?job:auto_search)
                        Unhide(?job:last_repair_days)
                        UnHide(?job:last_repair_days:prompt)
                End
            Else
                Case def:automatic_replicate_field
                    Of 'SURNAME'
                        job:surname = job:auto_search
                    Of 'MOBILE NUMBER'
                        job:mobile_number = job:auto_search
                    Of 'E.S.N./I.M.E.I.'
                        job:esn = job:auto_search
                        Post(Event:Accepted,?job:ESN)
                    Of 'M.S.N.'
                        job:msn = job:auto_search
                    Of 'POSTCODE'
                        job:postcode = job:auto_search
                        Postcode_Routine(job:postcode,job:address_line1,job:address_line2,job:address_line3)
                        Display()
                End
            End
        End
    End
    history# = 0
    Display()
    Clear(glo:select1)
History_search2     Routine
    glo:select1 = ''
    ! Surname
        clear(job_ali:record, -1)
        job_ali:surname       = job:auto_search
        set(job_ali:surname_key,job_ali:surname_key)
        loop
            next(jobs_alias)
            if errorcode()                       |
               or job_ali:surname       <> job:auto_search      |
               then break.  ! end if
            glo:select1 = 'SURNAME'
            Break
        end !loop
    !Mobile_Number
        If glo:select1 = ''
            clear(job_ali:record)
            job_ali:mobile_number = job:auto_search
            get(jobs_alias,job_ali:mobilenumberkey)
            if errorcode()
               clear(job_ali:record)
            Else
                glo:select1 = 'MOBILE'
            end !if
        End
    !ESN
        If glo:select1 = ''
            clear(job_ali:record)
            job_ali:esn = job:auto_search
            get(jobs_alias,job_ali:esn_key)
            if errorcode()
               clear(job_ali:record)
            Else
                glo:select1 = 'ESN'
            end !if
        End
    !MSN
        If glo:select1 = ''
            clear(job_ali:record)
            job_ali:msn = job:auto_search
            get(jobs_alias,job_ali:msn_key)
            if errorcode()
               clear(job_ali:record)
            Else
                glo:select1 = 'MSN'
            end !if
        End
    !Postcode
Last_Repair_Days        Routine   !Show/Hide Last Repair Days field
    If job:last_repair_days = 0
        Hide(?job:last_repair_days)
        Hide(?job:last_repair_days:prompt)
    Else
        UnHide(?job:last_repair_days)
        UnHide(?job:last_repair_days:prompt)
    End
    If job:last_repair_days < def:warranty_Period
        ?job:last_repair_days{prop:color} = color:red
        ?job:last_repair_days{prop:fontcolor} = color:yellow
    End
check_force_fault_codes    Routine
    force_fault_codes_temp = 'NO'
    If job:chargeable_job = 'YES'
        required# = 0
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:chargeable = 'YES'
    If job:warranty_job = 'YES' And force_fault_codes_temp <> 'YES'
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:warranty_charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:warranty_job = 'YES'
Date_Completed      Routine     !When Date Completed Filled in
!    access:jobnotes.clearkey(jbn:RefNumberKey)
!    jbn:RefNUmber   = job:ref_number
!    If access:jobnotes.tryfetch(jbn:RefNumberKey) = Level:Benign
!        jbn:Fault_Description    = tmp:FaultDescription
!        jbn:Invoice_Text        = tmp:InvoiceText
!        jbn:Engineers_notes     = tmp:EngineerNotes
!        access:jobnotes.update()
!    End!If access:jobnotes.tryfetch(jbn:RefNumberKey) = Level:Benign

!    Include('completed.inc')
!    Do Time_Remaining
!    Do skip_Despatch
!    If error# = 1
!        glo:errortext = 'This job cannot be completed due to the following error(s): <13,10>' & Clip(glo:errortext)
!        Error_Text
!        glo:errortext = ''
!    End!If error# = 1
!
!    brw21.resetsort(1)
!    loop error_count# = 1 to 50
!        If error_type_temp[x#] = 1
!            Case error_count#
!                OF 1
!                    ?job:transit_type{prop:req} = 1
!                OF 2
!                    ?job:mobile_number{prop:req} = 1
!                OF 3
!                    ?job:model_number{prop:req} = 1
!                OF 4
!                    ?job:unit_type{prop:req} = 1
!                OF 5
!                    ?tmp:FaultDescription{prop:req} = 1
!                OF 6
!                    ?job:esn{prop:req} = 1
!                OF 7
!                    ?job:incoming_courier{prop:req} = 1
!                OF 8
!                    ?job:msn{prop:req} = 1
!                OF 31
!                    ?job:courier{prop:req} = 1
!                OF 10
!                    ?job:engineer{prop:req} = 1
!                of 11
!                    ?tmp:InvoiceText{prop:req} = 1
!                OF 12
!                    ?job:repair_type{prop:req} = 1
!                OF 13
!                    ?job:repair_Type_warranty{prop:req} = 1
!                OF 14
!                    ?job:authority_number{prop:req} = 1
!                OF 27
!                OF 28
!                Of 29
!
!                Of 30
!
!            End!Case error#
!        End
!    End!loop error_count# = 1 to 30
!    If exchange_update# = 1
!        If job:exchange_unit_number <> ''
!            access:exchange.clearkey(xch:ref_number_key)
!            xch:ref_number = job:exchange_unit_number
!            if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!                If xch:audit_number <> ''
!                    access:excaudit.clearkey(exa:audit_number_key)
!                    exa:stock_type   = xch:stock_type
!                    exa:audit_number = xch:audit_number
!                    if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
!!Remove Stock Unit
!                        access:exchange.clearkey(xch:ref_number_key)
!                        xch:ref_number = exa:stock_unit_number
!                        if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!                            get(exchhist,0)
!                            if access:exchhist.primerecord() = level:benign
!                                exh:ref_number   = xch:ref_number
!                                exh:date          = today()
!                                exh:time          = clock()
!                                access:users.clearkey(use:password_key)
!                                use:password =glo:password
!                                access:users.fetch(use:password_key)
!                                exh:user = use:user_code
!                                exh:status        = 'REMOVED FROM AUDIT NUMBER: ' & Clip(xch:audit_number)
!                                access:exchhist.insert()
!                            end!if access:exchhist.primerecord() = level:benign
!                            xch:audit_number = ''
!                            access:exchange.update()
!                        end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!                        access:exchange.clearkey(xch:ref_number_key)
!                        xch:ref_number = exa:replacement_unit_number
!                        if access:exchange.fetch(xch:ref_number_key) = Level:benign     !Make new unit available
!                            xch:available = 'RTS'
!                            xch:job_number = ''
!                            access:exchange.update()
!                            get(exchhist,0)
!                            if access:exchhist.primerecord() = level:benign
!                                exh:ref_number   = xch:ref_number
!                                exh:date          = today()
!                                exh:time          = clock()
!                                access:users.clearkey(use:password_key)
!                                use:password =glo:password
!                                access:users.fetch(use:password_key)
!                                exh:user = use:user_code
!                                exh:status        = 'UNIT AVAILABLE'
!                                access:exchhist.insert()
!                            end!if access:exchhist.primerecord() = level:benign
!                        end!if access:exchange.fetch(xch:ref_number_key) = Level:benign
!                        exa:stock_unit_number = exa:replacement_unit_number             !Make new unit the stock item
!                        exa:replacement_unit_number = ''
!                        access:excaudit.update()
!                    end!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
!                Else!
!                End!If xch:audit_number <> ''
!            end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!        End!If job:exchange_unit_number <> ''
!
!    End!If exchange_update# = 1
!END Removing Exchange Unit From Stock, and putting replacement unit as stock item.
    Display()
QA_Group        Routine
Fill_Lists      Routine    !Fill the Drop Down Lists

   BRW21.ResetSort(1)
ValidateDateCode Routine

    if job:POP <> '' then exit.
    if job:DOP <> '' then exit.

    !Warranty Check!
    IF LEN(CLIP(job:MSN)) = 11 AND job:Manufacturer = 'ERICSSON'
      !Ericsson MSN!
      Job:MSN = SUB(job:MSN,2,10)
      UPDATE()
      Display()
    END

    Error# = 0
    If CheckLength('MSN',job:model_number,job:msn)
        Select(?job:msn)
        Error# = 1
    Else!If error# = 1
        Error# = 0
        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = job:Manufacturer
        If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Found
            If man:ApplyMSNFormat
                If CheckFaultFormat(job:Msn,man:MSNFormat)
                    Case Missive('M.S.N. failed format validation.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    Select(?job:msn)
                    Error# = 1
                End !If CheckFaultFormat(job:Msn,man:MSNFormat)
            End !If man:ApplyMSNFormat
        Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign


    End!If error# = 1

    If job:Manufacturer = 'MOTOROLA' AND Error# = 0

        If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          DO Validate_Motorola

        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)

          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
        !IF job:pop <> ''
          !UNHIDE(?Validate_POP)
        !ELSE
          !HIDE(?Validate_POP)
        !END
    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'SAMSUNG' AND Error# = 0

        If DateCodeValidation('SAMSUNG',job:MSN,job:Date_Booked)
          DO Validate_Motorola

        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)

          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
        !IF job:pop <> ''
        !  UNHIDE(?Validate_POP)
        !ELSE
        !  HIDE(?Validate_POP)
        !END
    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'PHILIPS' AND Error# = 0

        If DateCodeValidation('PHILIPS',job:MSN,job:Date_Booked)
          DO Validate_Motorola

        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)

          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
        !IF job:pop <> ''
        !  UNHIDE(?Validate_POP)
        !ELSE
        !  HIDE(?Validate_POP)
        !END
    End !If job:Manufacturer = 'MOTOROLA'

    !----------------------------------------------------------------------!
    If job:Manufacturer = 'ALCATEL' AND job:POP = ''
        IF job:Fault_Code3 <> ''
            If DateCodeValidation('ALCATEL',job:Fault_Code3,job:Date_Booked)
                DO Validate_Motorola

            Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                job:Warranty_Job = 'YES'
                Post(Event:Accepted,?job:Warranty_Job)

                job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                IF job:pop = ''
                  job:pop = 'F'
                END
            End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
            !IF job:pop <> ''
            !    UNHIDE(?Validate_POP)
            !ELSE
            !    HIDE(?Validate_POP)
            !END
        END
    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'ERICSSON' AND job:POP = ''
        IF job:fault_code7 <> ''
            If DateCodeValidation('ERICSSON',CLIP(CLIP(job:Fault_Code7)&CLIP(job:Fault_Code8)),job:Date_Booked)
                DO Validate_Motorola

            Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                job:Warranty_Job = 'YES'
                Post(Event:Accepted,?job:Warranty_Job)

                job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                IF job:pop = ''
                  job:pop = 'F'
                END
            End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
            !IF job:pop <> ''
            !    UNHIDE(?Validate_POP)
            !ELSE
            !    HIDE(?Validate_POP)
            !END
        END
    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'BOSCH' AND job:POP = ''
        IF job:fault_code7 <> ''
            If DateCodeValidation('BOSCH',job:Fault_Code7,job:Date_Booked)
                DO Validate_Motorola

            Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                job:Warranty_Job = 'YES'
                Post(Event:Accepted,?job:Warranty_Job)

                job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                IF job:pop = ''
                  job:pop = 'F'
                END
            End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
            !IF job:pop <> ''
            !    UNHIDE(?Validate_POP)
            !ELSE
            !    HIDE(?Validate_POP)
            !END
        END
    End !If job:Manufacturer = 'MOTOROLA'


    If job:Manufacturer = 'SIEMENS' AND job:POP = ''
        IF job:Fault_Code3 <> ''
            If DateCodeValidation('SIEMENS',job:Fault_Code3,job:Date_Booked)
                DO Validate_Motorola
            Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                job:Warranty_Job = 'YES'
                Post(Event:Accepted,?job:Warranty_Job)

                job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                IF job:pop = ''
                  job:pop = 'F'
               END
            End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
            !IF job:pop <> ''
            !    UNHIDE(?Validate_POP)
            !ELSE
            !    HIDE(?Validate_POP)
            !END
        End
    End !If job:Manufacturer = 'MOTOROLA'

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Job'
  OF ChangeRecord
    ActionMessage = 'Changing A Job'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020392'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateJOBS')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(job:Record,History::job:Record)
  SELF.AddHistoryField(?job:Auto_Search,4)
  SELF.AddHistoryField(?job:Last_Repair_Days,142)
  SELF.AddHistoryField(?job:Account_Number,39)
  SELF.AddHistoryField(?job:Order_Number,42)
  SELF.AddHistoryField(?job:Title,60)
  SELF.AddHistoryField(?job:Initial,61)
  SELF.AddHistoryField(?job:Surname,62)
  SELF.AddHistoryField(?job:Transit_Type,30)
  SELF.AddHistoryField(?job:Workshop,23)
  SELF.AddHistoryField(?job:Location,24)
  SELF.AddHistoryField(?job:Third_Party_Site,143)
  SELF.AddHistoryField(?job:ESN,16)
  SELF.AddHistoryField(?job:MSN,17)
  SELF.AddHistoryField(?job:ProductCode,18)
  SELF.AddHistoryField(?job:Model_Number,14)
  SELF.AddHistoryField(?job:Manufacturer,15)
  SELF.AddHistoryField(?job:Unit_Type,19)
  SELF.AddHistoryField(?job:Colour,20)
  SELF.AddHistoryField(?job:DOP,27)
  SELF.AddHistoryField(?job:Mobile_Number,70)
  SELF.AddHistoryField(?job:Turnaround_Time,168)
  SELF.AddHistoryField(?job:Chargeable_Job,13)
  SELF.AddHistoryField(?job:Charge_Type,36)
  SELF.AddHistoryField(?job:ThirdPartyDateDesp,149)
  SELF.AddHistoryField(?job:Warranty_Job,12)
  SELF.AddHistoryField(?job:Warranty_Charge_Type,37)
  SELF.AddHistoryField(?job:Date_Paid,88)
  SELF.AddHistoryField(?job:Estimate,144)
  SELF.AddHistoryField(?job:Estimate_If_Over,145)
  SELF.AddHistoryField(?job:Estimate_Accepted,146)
  SELF.AddHistoryField(?job:Estimate_Rejected,147)
  SELF.AddHistoryField(?JOB:Order_Number:2,42)
  SELF.AddHistoryField(?JOB:Transit_Type:2,30)
  SELF.AddHistoryField(?JOB:Workshop:2,23)
  SELF.AddHistoryField(?JOB:Location:2,24)
  SELF.AddHistoryField(?JOB:Third_Party_Site:2,143)
  SELF.AddHistoryField(?JOB:Special_Instructions:2,169)
  SELF.AddHistoryField(?job:DOP:2,27)
  SELF.AddHistoryField(?job:Turnaround_Time:2,168)
  SELF.AddHistoryField(?job:Charge_Type:2,36)
  SELF.AddHistoryField(?job:Warranty_Charge_Type:2,37)
  SELF.AddHistoryField(?job:MSN:2,17)
  SELF.AddHistoryField(?job:ProductCode:2,18)
  SELF.AddHistoryField(?job:Manufacturer:2,15)
  SELF.AddHistoryField(?job:Model_Number:2,14)
  SELF.AddHistoryField(?job:ESN:2,16)
  SELF.AddHistoryField(?job:Unit_Type:2,19)
  SELF.AddHistoryField(?job:Colour:2,20)
  SELF.AddHistoryField(?job:Mobile_Number:2,70)
  SELF.AddHistoryField(?job:Status_End_Date,164)
  SELF.AddHistoryField(?job:Status_End_Time,165)
  SELF.AddHistoryField(?job:Current_Status,38)
  SELF.AddHistoryField(?job:Exchange_Status,34)
  SELF.AddHistoryField(?job:Loan_Status,33)
  SELF.AddUpdateFile(Access:JOBS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS.Open
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:COMMONCP.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:DISCOUNT.Open
  Relate:ESNMODEL.Open
  Relate:EXCAUDIT.Open
  Relate:EXCCHRGE.Open
  Relate:JOBEXACC.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:JOBPAYMT.Open
  Relate:JOBS2_ALIAS.Open
  Relate:JOBSE3.Open
  Relate:MANUDATE.Open
  Relate:NETWORKS.Open
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  Relate:PARTS_ALIAS.Open
  Relate:STAHEAD.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WARPARTS_ALIAS.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:DEFCHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:MANUFACT.UseFile
  Access:LOAN.UseFile
  Access:LOANACC.UseFile
  Access:JOBLOHIS.UseFile
  Access:EXCHANGE.UseFile
  Access:JOBEXHIS.UseFile
  Access:EXCHACC.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBACC.UseFile
  Access:TRDMODEL.UseFile
  Access:TRDPARTY.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:WARPARTS.UseFile
  Access:STATUS.UseFile
  Access:LOANHIST.UseFile
  Access:EXCHHIST.UseFile
  Access:LOCATION.UseFile
  Access:LOCINTER.UseFile
  Access:COMMONFA.UseFile
  Access:COMMONWP.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:BOUNCER.UseFile
  Access:MODELCOL.UseFile
  Access:TRDBATCH.UseFile
  Access:REPAIRTY.UseFile
  Access:JOBNOTES.UseFile
  Access:MODELNUM.UseFile
  Access:STOCKTYP.UseFile
  Access:JOBSE.UseFile
  Access:TRAFAULT.UseFile
  Access:TRAFAULO.UseFile
  Access:CHARTYPE.UseFile
  Access:SUBCHRGE.UseFile
  Access:REPTYDEF.UseFile
  Access:JOBSENG.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBSE2.UseFile
  Access:CONTHIST.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If thiswindow.request = Insertrecord
      get(jobnotes,0)
      if access:jobnotes.primerecord() = Level:Benign
          jbn:refnumber         = job:ref_number
          if access:jobnotes.tryinsert()
              access:jobnotes.cancelautoinc()
          end
      end!if access:jobnotes.primerecord() = Level:Benign
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber    = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found
  
      Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
          If access:JOBSE.primerecord() = Level:Benign
              jobe:RefNumber  = job:Ref_Number
              If access:JOBSE.tryinsert()
                  access:JOBSE.cancelautoinc()
              End!If access:JOBSE.tryinsert()
          End!If access:JOBSE.primerecord() = Level:Benign
      End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  End
  access:jobnotes.clearkey(jbn:refnumberkey)
  jbn:refnumber = job:Ref_number
  if access:jobnotes.tryfetch(jbn:refnumberkey)
      If access:jobnotes.primerecord() = Level:Benign
          jbn:refnumber   = job:ref_number
          If access:jobnotes.tryinsert()
              access:jobnotes.cancelautoinc()
          End!If access:jobnotes.tryinsert()
      End!If access:jobnotes.primerecord() = Level:Benign
  Else
      tmp:FaultDescription    = jbn:Fault_Description
      tmp:EngineerNOtes       = jbn:Engineers_Notes
      tmp:InvoiceText         = jbn:Invoice_Text
  end!if access:jobnotes.tryfetch(jbn:refnumberkey) = Level:Benign
  
  access:JOBSE.clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else! If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
      If access:JOBSE.primerecord() = Level:Benign
          jobe:RefNumber  = job:Ref_Number
          If access:JOBSE.tryinsert()
              access:JOBSE.cancelautoinc()
          End!If access:JOBSE.tryinsert()
      End!If access:JOBSE.primerecord() = Level:Benign
      !Assert(0,'Fetch Error')
  End! If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
  
  Do GETJOBSE
  
  ! Inserting (DBH 07/04/2006) #7305 - Make sure a jobse2 record exists
  Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Found
  
  Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Error
      If Access:JOBSE2.PrimeRecord() = Level:Benign
          jobe2:RefNumber = job:Ref_Number
          If Access:JOBSE2.TryInsert() = Level:Benign
              ! Insert Successful
  
          Else ! If Access:JOBSE2.TryInsert() = Level:Benign
              ! Insert Failed
              Access:JOBSE2.CancelAutoInc()
          End ! If Access:JOBSE2.TryInsert() = Level:Benign
      End !If Access:JOBSE2.PrimeRecord() = Level:Benign
  End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
  ! End (DBH 07/04/2006) #7305
  
  !TB13287 - J - 06/01/15 - is there a specific needs tick?
  Access:jobse3.clearkey(jobe3:KeyRefNumber)
  jobe3:RefNumber = Job:Ref_number
  if access:jobse3.fetch(jobe3:KeyRefNumber)
    !no record - no problem
  ELSE
    tmp:SpecificNeeds = jobe3:SpecificNeeds
  END
  
  !Save Accesories incase they change
  If GetTempPathA(255,TempFilePath)
      If Sub(TempFilePath,-1,1) = '\'
          tmp:SaveAccessory = Clip(TempFilePath) & 'SAVACC' & Clock() & '.TMP'
      Else !If Sub(TempFilePath,-1,1) = '\'
          tmp:SaveAccessory = Clip(TempFilePath) & '\SAVACC' & Clock() & '.TMP'
      End !If Sub(TempFilePath,-1,1) = '\'
      Remove(tmp:SaveAccessory)
  
      Create(SaveAccessory)
      Open(SaveAccessory)
      If ~Error()
          Save_jac_ID = Access:JOBACC.SaveFile()
          Access:JOBACC.ClearKey(jac:Ref_Number_Key)
          jac:Ref_Number = job:Ref_Number
          Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
          Loop
              If Access:JOBACC.NEXT()
                 Break
              End !If
              If jac:Ref_Number <> job:Ref_Number      |
                  Then Break.  ! End If
              savacc:Accessory = jac:Accessory
              Add(SaveAccessory)
          End !Loop
          Access:JOBACC.RestoreFile(Save_jac_ID)
      End !If ~Error()
      Close(SaveAccessory)
  End
  
  
  BRW21.Init(?List:2,Queue:Browse:1.ViewPosition,BRW21::View:Browse,Queue:Browse:1,Relate:JOBSTAGE,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Set Defaults
  ?sheet4{prop:wizard} = 1
  Set(Defaults)
  access:defaults.next()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  access:users.clearkey(use:password_key)
  use:password    =glo:password
  access:users.fetch(use:password_key)
  
  If thiswindow.request = Insertrecord
      if def:force_initial_transit_type = 'B'
          ?job:transit_type{prop:req} = 1
      end !if def:force_initial transit type = 'B'
  
      if def:force_mobile_number = 'B'  and def:show_mobile_number <> 'YES'
          ?job:mobile_number{prop:req} = 1
      end !if def:force_mobile_number = 'B'
  
      if def:force_model_number = 'B'
          ?job:model_number{prop:req} = 1
      end !if def:force_model_number = 'B'
  
      if def:force_unit_type = 'B'
          ?job:unit_type{prop:req} = 1
      end !if def:force_unit_type = 'B'
  
  !    if def:force_fault_description = 'B'
  !        ?tmp:FaultDescription{prop:req} = 1
  !    end !if def:force_fault_description = 'B'
  
      if def:force_esn = 'B'
          ?job:esn{prop:req} = 1
      end !if def:force_esn = 'B'
  
  
      if def:force_invoice_text = 'B'
          !?tmp:InvoiceText{prop:req} = 1
      end !if def:force_invoice_text = 'B'
  
      If DEF:Order_Number = 'B'
          ?job:order_number{prop:req} = 1
      End!If DEF:Order_Number = 'B'
  End!If thiswindow.request = Insertrecord
  
  Do CityLink_service
  Do UPS_Service
  
  If GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?tmp:Network{prop:Hide} = 1
      ?tmp:Network:Prompt{prop:Hide} = 1
      ?LookupNetwork{prop:Hide} = 1
  Else !GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?tmp:Network{prop:Hide} = 0
      ?tmp:Network:Prompt{prop:Hide} = 0
      ?LookupNetwork{prop:Hide} = 0
  End !GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  
  ! Inserting (DBH 12/10/2006) # 8020 - Highlight if contact history has been entered
  Do CheckContactHistory
  ! End (DBH 12/10/2006) #8020
  
  ! Inserting (DBH 06/02/2008) # 9613 - Show the release button if the job is ready to despatch
  If wob:ReadyToDespatch = 1
      If ReleasedForDespatch(job:Ref_Number) <> 1
          If SecurityCheck('OUT OF REGION - ALLOW RELEASE') = 0
              ?Button:ReleaseForDespatch{prop:Hide} = 0
          End ! If SecurityCheck('OUT OF REGION - ALLOW RELEASE') = 0
      End ! If ReleasedFromDespatch(job:Ref_Number) <> 1
  End ! If wob:ReadyToDespatch = 1
  ! End (DBH 06/02/2008) #9613
  Do AreThereNotes
  !Cancelled Job?
  If job:cancelled = 'YES'
      Hide(?OK)
      Unhide(?canceltext)
  Else
      hide(?canceltext)
      ?CancelText{prop:text}=''
      If jobe:FailedDelivery
          ?Canceltext{prop:Hide} = 0
          ?CancelText{prop:Text} = 'FAILED DELIVERY'
      End !If jobe:FailedDelivery
  End!If job:cancelled = 'YES'
  
  thiswindow.update()
  display()
  !thismakeover.refresh()
  !Save Address/Account Details
  accsav:AccountNumber    = job:Account_Number
  
  accsa1:CompanyName      = job:Company_Name
  accsa1:AddressLine1     = job:Address_Line1
  accsa1:AddressLine2     = job:Address_Line2
  accsa1:AddressLine3     = job:Address_Line3
  accsa1:Postcode         = job:Postcode
  accsa1:TelephoneNumber  = job:Telephone_Number
  accsa1:FaxNumber        = job:Fax_Number
  
  accsa2:CompanyName      = job:Company_Name_Delivery
  accsa2:AddressLine1     = job:Address_Line1_Delivery
  accsa2:AddressLine2     = job:Address_Line2_Delivery
  accsa2:AddressLine3     = job:Address_Line3_Delivery
  accsa2:Postcode         = job:Postcode_Delivery
  accsa2:TelephoneNumber  = job:Telephone_Delivery
  !Check if job in RRC Control
  !or rejected warranty job - 3788 (DBH: 19-04-2004)
  glo:Preview = ''
  
  ! After window open initialization (DBH: 24-03-2005)
  ! Is this record in use? (DBH: 24-03-2005)
  ! Check to see if the job is in use. This will also lock it for other processes - TrkBs: 5873 (DBH: 02-06-2005)
  If ThisWindow.Request <> InsertRecord
      If JobInUse(job:Ref_Number,0)
          If SecurityCheck('EDIT LOCKED JOBS') = False
              Case Missive('This job is currently in use by another station.'&|
                '<13,10>'&|
                '<13,10>Do you wish to EDIT his job?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Release(JOBS)
                  Of 1 ! No Button
                      ThisWindow.Request = ViewRecord
                      ThisWindow.Request = ViewRecord
                      ?Show_Booking_Temp{prop:Hide} = True
                      ?LookupNetwork{prop:Hide} = True
                      ?OK{prop:Disable} = 1
                      glo:Preview = 'V'
              End ! Case Missive
          Else  ! If SecurityCheck('EDIT LOCKED JOBS') = False
              Case Missive('This job is currently in use by another station.'&|
                '<13,10>'&|
                '<13,10>This screen will be VIEW ONLY.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              ThisWindow.Request = ViewRecord
              ?Show_Booking_Temp{prop:Hide} = True
              ?LookupNetwork{prop:Hide} = True
              ?OK{prop:Disable} = 1
              glo:Preview = 'V'
          End ! If SecurityCheck('EDIT LOCKED JOBS') = False
      Else ! If JobInUse(job:Ref_Number,0)
      End ! If JobInUse(job:Ref_Number,0)If JobInUse(job:Ref_Number,0)
  End ! ThisWindow.Request <> InsertRecord
  
  if glo:WebJob AND glo:Preview <> 'V' then
      !Does the user have the correct access level
      !to amned a job that was sent to the hub? - 4348 (DBH: 22-07-2004  20)
      If SentToHub(job:Ref_Number)
          If SecurityCheck('JOBS - RRC AMEND ARC JOB')
              ?Show_Booking_Temp{prop:Hide} = True
              ?LookupNetwork{prop:Hide} = True
              glo:Preview = 'V'
              ?ValidatePOP{prop:Hide} = True
          End !If SecurityCheck('JOBS - RRC AMEND ARC JOB')
      End !If SentToHub(job:Ref_Number)
  
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number    = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
          tmp:CurrentSiteLocation = tra:SiteLocation
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !    message('in final webjob code')
      !Can the RRC view this job
          !Check the see if viewing job was booked by the
          !RRC viewing it - L910 (DBH: 01-09-2003)
      If (job:Location <> GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI') And |
          job:Location <> GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')) Or jobe:HubRepair = True Or |
          (wob:HeadAccountNumber <> Clarionet:Global.Param2)
  
          Case Missive('This job is not in this R.R.C.''s Control and cannot be amended.'&|
            '<13,10>'&|
            '<13,10>Click ''OK'' to view the job.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
  
          ?Show_Booking_Temp{prop:Hide} = True
          ?LookupNetwork{prop:Hide} = True
          glo:Preview = 'V'
      Else
          !Is this job a warranty job. If so has it been completed
          !at the RRC. If so, then it has been submitted and cannot be changed - 3788 (DBH: 14-04-2004)
          If job:Warranty_Job = 'YES' And wob:EDI <> 'XXX'
              If job:Date_Completed <> '' And ~SentToHub(job:Ref_Number)
                  If wob:EDI = 'NO' Or wob:EDI = 'YES' Or wob:EDI = 'PAY' or wob:EDI = 'APP'
                      ?Show_Booking_Temp{prop:Hide} = True
                      ?LookupNetwork{prop:Hide} = True
                      ?OK{prop:Disable} = 1
  
                      glo:Preview = 'V'
                      !message('Set glo:preview = '& glo:Preview)
                  Else !If wob:EDI = 'NO' Or wob:EDI = 'YES' Or wob:EDI = 'PAY' or wob:EDI = 'APP'
                      Case Missive('The selected Warranty Job has been rejected. '&|
                        '<13,10>Do you wish to EDIT (password required), or just VIEW the job details?','ServiceBase 3g',|
                                     'mquest.jpg','View|Edit')
                          Of 2 ! Edit Button
                              Error# =0
                              Access:USERS.Clearkey(use:User_Code_Key)
                              use:User_Code   = InsertEngineerPassword()
                              If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found
                                  Access:ACCAREAS.ClearKey(acc:Access_level_key)
                                  acc:User_Level  = use:User_Level
                                  acc:Access_Area = 'EDIT REJECTED WARRANTY JOB'
                                  If Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign
                                      !Found
  
                                  Else !If Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign
                                      !Error
                                      Error# = 1
                                  End !If Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign
                              Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                                  Error# = 1
                              End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                              If Error# = 1
                                  Case Missive('You do not have sufficient access to edit this job.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                              End !If Error# = 0
                          Of 1 ! View Button
                              Error# = 1
                      End ! Case Missive
                      If Error# = 1
                          ?Show_Booking_Temp{prop:Hide} = True
                          ?LookupNetwork{prop:Hide} = True
                          ?OK{prop:Disable} = 1
                          glo:Preview = 'V'
                          !message('Set glo:preview = '& glo:Preview)
  
                      End !If Error# = 1
                  End !If wob:EDI = 'NO' Or wob:EDI = 'YES' Or wob:EDI = 'PAY' or wob:EDI = 'APP'
              End !If job:Date_Completed <> '' And ~SentToHub(job:Ref_Number)
          End !If job:Warranty_Job = 'YES'
      End !job:Location <> GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')
  END
  
  ! Inserting (DBH 25/03/2008) # 9856 - Do not allow the user to change jobs booked at the VCP
  If SecurityCheck('CHANGE VCP ACCOUNT NUMBER')
      If Sub(job:account_Number,1,3) = 'VCP'
          ?job:Account_Number{prop:Disable} = 1
          ?LookupTradeAccount{Prop:Disable} = 1
      End ! If Sub(jo:accoun_Number,1,3) = 'VCP'
  End ! If SecurityCheck('CHANGE VCP ACCOUNT NUMBER')
  ! End (DBH 25/03/2008) #9856
      if glo:webjob = 0 then
          !TB12540 - Show contact history if there is a sticky note
          !need to find one that has not been cancelled, and is valid for this request
          Access:conthist.clearkey(cht:KeyRefSticky)
          cht:Ref_Number = job:ref_number
          cht:SN_StickyNote = 'Y'
          Set(cht:KeyRefSticky,cht:KeyRefSticky)      
          Loop
              if access:Conthist.next() then break.
              IF cht:Ref_Number <> job:ref_number then break.
              if cht:SN_StickyNote <> 'Y' then break.
              if cht:SN_Completed <> 'Y' and cht:SN_CustService = 'Y' then
                  glo:select12 = job:ref_number
                  Browse_Contact_Hist2
                  BREAK
              END
          END
      end !if gloWebjob
  ! Save Window Name
   AddToLog('Window','Open','UpdateJOBS')
    ReturnRequest" = ThisWindow.Request
    CASE ThisWindow.Request
    OF ViewRecord
        ReturnRequest" = 'ViewRecord'
    OF InsertRecord
        ReturnRequest" = 'InsertRecord'
    OF ChangeRecord
        ReturnRequest" = 'ChangeRecord'
    OF DeleteRecord 
        ReturnRequest" = 'DeleteRecord'
    END ! CASE
    AddToLog('Job Start','',ReturnRequest",job:Ref_Number)
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?job:ESN:2{prop:boxed} = False
  ?job:MSN:2{prop:boxed} = False
  ?tmp:IncomingIMEI{prop:boxed} = False
  ?tmp:IncomingMSN{prop:boxed} = False
  ?tmp:ExchangeIMEI{prop:boxed} = False
  ?tmp:ExchangeMSN{prop:boxed} = False
  ?job:Model_Number:2{prop:boxed} = False
  ?job:ProductCode:2{prop:boxed} = False
  ?job:Colour:2{prop:boxed} = False
  ?job:Mobile_Number:2{prop:boxed} = False
  ?job:Charge_Type:2{prop:boxed} = False
  ?job:Warranty_Charge_Type:2{prop:boxed} = False
  ?tmp:EstimateDetails{prop:boxed} = False
  ?jobe:SIMNumber{prop:boxed} = False
  ?Batch_Number_Text{prop:boxed} = False
  ?job:Manufacturer:2{prop:boxed} = False
  ?job:Unit_Type:2{prop:boxed} = False
  ?job:Turnaround_Time:2{prop:boxed} = False
  ?tmp:2ndExchangeIMEI{prop:boxed} = False
  ?tmp:2ndExchangeMSN{prop:boxed} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?job:Account_Number{Prop:Tip} AND ~?LookupTradeAccount{Prop:Tip}
     ?LookupTradeAccount{Prop:Tip} = 'Select ' & ?job:Account_Number{Prop:Tip}
  END
  IF ?job:Account_Number{Prop:Msg} AND ~?LookupTradeAccount{Prop:Msg}
     ?LookupTradeAccount{Prop:Msg} = 'Select ' & ?job:Account_Number{Prop:Msg}
  END
  IF ?job:Transit_Type{Prop:Tip} AND ~?LookupTransitType{Prop:Tip}
     ?LookupTransitType{Prop:Tip} = 'Select ' & ?job:Transit_Type{Prop:Tip}
  END
  IF ?job:Transit_Type{Prop:Msg} AND ~?LookupTransitType{Prop:Msg}
     ?LookupTransitType{Prop:Msg} = 'Select ' & ?job:Transit_Type{Prop:Msg}
  END
  IF ?job:Model_Number{Prop:Tip} AND ~?LookupModelNumber{Prop:Tip}
     ?LookupModelNumber{Prop:Tip} = 'Select ' & ?job:Model_Number{Prop:Tip}
  END
  IF ?job:Model_Number{Prop:Msg} AND ~?LookupModelNumber{Prop:Msg}
     ?LookupModelNumber{Prop:Msg} = 'Select ' & ?job:Model_Number{Prop:Msg}
  END
  IF ?job:Charge_Type{Prop:Tip} AND ~?LookupChargeType{Prop:Tip}
     ?LookupChargeType{Prop:Tip} = 'Select ' & ?job:Charge_Type{Prop:Tip}
  END
  IF ?job:Charge_Type{Prop:Msg} AND ~?LookupChargeType{Prop:Msg}
     ?LookupChargeType{Prop:Msg} = 'Select ' & ?job:Charge_Type{Prop:Msg}
  END
  IF ?job:Warranty_Charge_Type{Prop:Tip} AND ~?LookupWarrantyChargeType{Prop:Tip}
     ?LookupWarrantyChargeType{Prop:Tip} = 'Select ' & ?job:Warranty_Charge_Type{Prop:Tip}
  END
  IF ?job:Warranty_Charge_Type{Prop:Msg} AND ~?LookupWarrantyChargeType{Prop:Msg}
     ?LookupWarrantyChargeType{Prop:Msg} = 'Select ' & ?job:Warranty_Charge_Type{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW21.Q &= Queue:Browse:1
  BRW21.RetainRow = 0
  BRW21.AddSortOrder(,jst:Job_Stage_Key)
  BRW21.AddRange(jst:Ref_Number,Relate:JOBSTAGE,Relate:JOBS)
  BRW21.AddLocator(BRW21::Sort0:Locator)
  BRW21::Sort0:Locator.Init(,jst:Job_Stage,1,BRW21)
  BRW21.AddField(jst:Job_Stage,BRW21.Q.jst:Job_Stage)
  BRW21.AddField(jst:Date,BRW21.Q.jst:Date)
  BRW21.AddField(jst:Time,BRW21.Q.jst:Time)
  BRW21.AddField(jst:Ref_Number,BRW21.Q.jst:Ref_Number)
  IF ?job:Chargeable_Job{Prop:Checked} = True
    UNHIDE(?JOB:Charge_Type)
    UNHIDE(?LookupChargeType)
  END
  IF ?job:Chargeable_Job{Prop:Checked} = False
    HIDE(?JOB:Charge_Type)
    HIDE(?LookupChargeType)
  END
  IF ?job:Warranty_Job{Prop:Checked} = True
    UNHIDE(?job:charge_type:prompt)
    UNHIDE(?LookupWarrantyChargeType)
    UNHIDE(?job:Warranty_Charge_Type)
  END
  IF ?job:Warranty_Job{Prop:Checked} = False
    HIDE(?JOB:Warranty_Charge_Type)
    HIDE(?LookupWarrantyChargeType)
  END
  IF ?job:Estimate_Accepted{Prop:Checked} = True
    job:Estimate_Rejected = 'NO'
  END
  IF ?job:Estimate_Rejected{Prop:Checked} = True
    job:Estimate_Accepted = 'NO'
  END
  FDCB6.Init(job:Unit_Type,?job:Unit_Type,Queue:FileDropCombo:1.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo:1,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo:1
  FDCB6.AddSortOrder(uni:ActiveKey)
  FDCB6.AddRange(uni:Active,tmp:one)
  FDCB6.AddField(uni:Unit_Type,FDCB6.Q.uni:Unit_Type)
  FDCB6.AddUpdateField(uni:Unit_Type,job:Unit_Type)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB17.Init(job:Turnaround_Time,?job:Turnaround_Time,Queue:FileDropCombo:12.ViewPosition,FDCB17::View:FileDropCombo,Queue:FileDropCombo:12,Relate:TURNARND,ThisWindow,GlobalErrors,0,1,0)
  FDCB17.Q &= Queue:FileDropCombo:12
  FDCB17.AddSortOrder(tur:Turnaround_Time_Key)
  FDCB17.AddField(tur:Turnaround_Time,FDCB17.Q.tur:Turnaround_Time)
  FDCB17.AddField(tur:Days,FDCB17.Q.tur:Days)
  FDCB17.AddField(tur:Hours,FDCB17.Q.tur:Hours)
  ThisWindow.AddItem(FDCB17.WindowComponent)
  FDCB17.DefaultFill = 0
  FDCB25.Init(job:Colour,?job:Colour,Queue:FileDropCombo:2.ViewPosition,FDCB25::View:FileDropCombo,Queue:FileDropCombo:2,Relate:MODELCOL,ThisWindow,GlobalErrors,0,1,0)
  FDCB25.Q &= Queue:FileDropCombo:2
  FDCB25.AddSortOrder(moc:Colour_Key)
  FDCB25.AddRange(moc:Model_Number,job:Model_Number)
  FDCB25.AddField(moc:Colour,FDCB25.Q.moc:Colour)
  FDCB25.AddField(moc:Record_Number,FDCB25.Q.moc:Record_Number)
  ThisWindow.AddItem(FDCB25.WindowComponent)
  FDCB25.DefaultFill = 0
  FDCB46.Init(job:ProductCode,?job:ProductCode,Queue:FileDropCombo.ViewPosition,FDCB46::View:FileDropCombo,Queue:FileDropCombo,Relate:MODPROD,ThisWindow,GlobalErrors,0,1,0)
  FDCB46.Q &= Queue:FileDropCombo
  FDCB46.AddSortOrder(mop:ProductCodeKey)
  FDCB46.AddRange(mop:ModelNumber,job:Model_Number)
  FDCB46.AddField(mop:ProductCode,FDCB46.Q.mop:ProductCode)
  FDCB46.AddField(mop:RecordNumber,FDCB46.Q.mop:RecordNumber)
  ThisWindow.AddItem(FDCB46.WindowComponent)
  FDCB46.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW21.AskProcedure = 0
      CLEAR(BRW21.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  ! Fill In WebJobs again, just in case anything is lost - TrkBs: 6554 (DBH: 18-10-2005)
  ! Inserting (DBH 09/?6/2006) #7804 - Code moved here, so it still fills in even if cancel pressed
  Access:WEBJOB.Clearkey(wob:RefNumberKey)
  wob:RefNumber   = job:Ref_Number
  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      ! Found
      wob:SubAcountNumber  = job:account_number
      wob:OrderNumber      = job:Order_Number
      wob:IMEINumber       = job:ESN
      wob:MSN              = job:MSN
      wob:Manufacturer     = job:Manufacturer
      wob:ModelNumber      = job:Model_Number
      wob:MobileNumber     = job:Mobile_Number
      wob:Current_Status       = job:Current_Status
      wob:Exchange_Status      = job:Exchange_Status
      wob:Loan_Status          = job:Loan_Status
      Access:WEBJOB.Update()
  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  ! End (DBH 09/06/2006) #7804
  ! Inserting (DBH 12/03/2007) # 8718 - Make sure the child records reflect any imei change
  Relate:JOBSOBF.Open()
  Save_JOBSOBF_ID = Access:JOBSOBF.SaveFile()
  Access:JOBSOBF.ClearKey(jof:RefNumberKey)
  jof:RefNumber = job:Ref_Number
  If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
      !Found
      If jobe:OBFValidated = 0 And jof:Status <> 2 And jof:Status <> 3
          ! If this is no longer and OBF then delete the entry in OBF Processing (DBH: 14/03/2007)
          Relate:JOBSOBF.Delete(0)
      Else ! If jof:OBFValidated = 0 And jof:Status <> 2 And jof:Status <> 3
          If jof:HeadAccountNumber <> wob:HeadAccountNumber
              jof:HeadAccountNumber = wob:HeadAccountNumber
              Access:JOBSOBF.Update()
          End ! If jof:HeadAccountNumber <> wob:HeadAccountNumber
          If jof:DateCompleted <> job:Date_Completed
              jof:DateCompleted = job:Date_Completed
              jof:TimeCompleted = job:Time_Completed
              Access:JOBSOBF.Update()
          End ! If jof:DateCompleted <> job:Date_Completed
          If jof:IMEINumber <> job:ESN
              jof:IMEINumber = job:ESN
              Access:JOBSOBF.Update()
          End ! If jof:IMEINumber <> job:ESN
      End ! If jof:OBFValidated = 0 And jof:Status <> 2 And jof:Status <> 3
  Else ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
  Access:JOBSOBF.RestoreFile(Save_JOBSOBF_ID)
  Relate:JOBSOBF.Close()
  ! End (DBH 12/03/2007) #8718
  
  
  Release(JOBS)
  
  set(Defaults)
  access:defaults.next()
  IF def:use_job_label <> 'YES'
      print_label_temp = 'NO'
  END! IF def:use_job_label <> 'YES'
  
  
  Set(defaults)
  access:defaults.next()
  Case def:label_printer_type
  Of 'TEC B-440 / B-442'
      Label_Type_temp = 1
  Of 'TEC B-452'
      label_type_temp = 2
  End! Case def:themal_printer_type
  
  saved_ref_number_Temp = job:ref_number
  saved_esn_temp        = job:esn
  
    ReturnResponse" = ''
    CASE ThisWindow.Response
    OF RequestCompleted
        ReturnResponse" = 'RequestCompleted'
    OF RequestCancelled
        ReturnResponse" = 'RequestCancelled'
    END ! CASE
  
    ReturnRequest" = ThisWindow.Request
    CASE ThisWindow.Request
    OF ViewRecord
        ReturnRequest" = 'ViewRecord'
    OF InsertRecord
        ReturnRequest" = 'InsertRecord'
    OF ChangeRecord
        ReturnRequest" = 'ChangeRecord'
    OF DeleteRecord 
        ReturnRequest" = 'DeleteRecord'
    END ! CASE
  
    AddToLog('Job End','',ReturnRequest",job:Ref_Number,ReturnResponse")
  
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:COMMONCP.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:DISCOUNT.Close
    Relate:ESNMODEL.Close
    Relate:EXCAUDIT.Close
    Relate:EXCCHRGE.Close
    Relate:JOBEXACC.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:JOBSE3.Close
    Relate:MANUDATE.Close
    Relate:NETWORKS.Close
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
    Relate:PARTS_ALIAS.Close
    Relate:STAHEAD.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WARPARTS_ALIAS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateJOBS')
  If print_label_temp = 'YES' And thiswindow.response <> 2
      glo:select1 = job:Ref_number
      case label_type_temp
          Of 1
              If job:bouncer = 'B'
                  Thermal_Labels('SE')
              Else!If job:bouncer = 'B'
                  If ExchangeAccount(job:Account_Number)
                      Thermal_Labels('ER')
                  Else !If ExchangeAccount(job:Account_Number)
                      Thermal_Labels('')
                  End !If ExchangeAccount(job:Account_Number)
  
              End!If job:bouncer = 'B'
          Of 2
              Thermal_Labels_B452
      End!case label_type_temp
      glo:select1 = ''
  End!If print_label_temp = 'YES'
  
  If thiswindow.response = 2
      If thiswindow.request = Insertrecord
          access:stock.open()
          access:stock.usefile()
          access:stohist.open()
          access:stohist.usefile()
          access:ordpend.open()
          access:ordpend.usefile()
          Clear(parts_queue_temp)
          Loop x# = 1 To Records(parts_queue_temp)
              Get(parts_queue_temp,x#)
              If partmp:exclude_from_order <> 'YES'
                  If partmp:pending_ref_number <> ''
                      access:ordpend.clearkey(ope:ref_number_key)
                      ope:ref_number = partmp:pending_ref_number
                      if access:ordpend.fetch(ope:ref_number_key) = Level:benign
                          Delete(ordpend)
                      End!if access:ordpend.fetch(ope:ref_number_key) = Level:benign
                  Else!If partmp:pending_ref_number <> ''
                      If partmp:part_ref_number <> ''
                          access:stock.clearkey(sto:ref_number_key)
                          sto:ref_number  = partmp:part_ref_number
                          If access:stock.fetch(sto:ref_number_key) = Level:Benign
                              sto:quantity_stock += partmp:quantity
                              access:stock.update()
                          End!If access:stock.clearkey(sto:ref_number_key) = Level:Benign
                          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'REC', | ! Transaction_Type
                                             '', | ! Depatch_Note_Number
                                             job:Ref_Number, | ! Job_Number
                                             0, | ! Sales_Number
                                             partmp:Quantity, | ! Quantity
                                             sto:Purchase_Cost, | ! Purchase_Cost
                                             sto:Sale_Cost, | ! Sale_Cost
                                             sto:Retail_Cost, | ! Retail_Cost
                                             'STOCK RECREDITED', | ! Notes
                                             '') ! Information
                            ! Added OK
  
                          Else ! AddToStockHistory
                            ! Error
                          End ! AddToStockHistory
                      End!If partmp:part_ref_number <> ''
                  End!If partmp:pending_ref_number <> ''
              End!If par:exclude_from_order <> 'YES'
          End!Loop x# = 1 To Records(parts_queue_temp)
          access:stock.close()
          access:stohist.close()
          access:ordpend.close()
      End!If thiswindow.request = Insertrecord
      If old_exchange_unit_number_temp <> new_exchange_unit_number_temp
          access:jobs.open()
          access:jobs.usefile()
          access:jobs.clearkey(job:ref_number_key)
          access:exchange.open()
          access:Exchange.usefile()
          access:exchhist.open()
          access:exchhist.usefile()
          access:jobexhis.open()
          access:jobexhis.usefile()
          job:ref_number  = saved_ref_number_temp
          If access:jobs.fetch(job:ref_number_key) = Level:Benign
              If thiswindow.request <> Insertrecord
                  Case Missive('The Exchange Unit you attached has now been re-stocked.','ServiceBase 3g',|
                                 'midea.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              End!If thiswindow.request <> Insertrecord
              access:exchange.clearkey(xch:ref_number_key)
              xch:ref_number = new_exchange_unit_number_temp
              if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                  xch:available = 'AVL'
                  xch:job_number = ''
                  xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                  access:exchange.update()
                  get(exchhist,0)
                  if access:exchhist.primerecord() = level:benign
                      exh:ref_number    = xch:ref_number
                      exh:date          = Today()
                      exh:time          = Clock()
                      access:users.clearkey(use:password_key)
                      use:password    =glo:password
                      access:users.fetch(use:password_key)
                      exh:user          = use:user_code
                      exh:status        = 'UNIT RE-STOCKED, CANCELLED JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
                      if access:exchhist.insert()
                          access:exchhist.cancelautoinc()
                      end
                  end!if access:exchhist.primerecord() = level:benign
              end!if access:exchange.fetch(xch:ref_number_key)
              job:exchange_unit_number = ''
              get(jobexhis,0)
              if access:jobexhis.primerecord() = level:benign
                  jxh:ref_number       = job:ref_number
                  jxh:loan_unit_number = new_exchange_unit_number_temp
                  jxh:date             = Today()
                  jxh:time             = Clock()
                  jxh:user             = use:user_code
                  jxh:status           = 'UNIT RETURNED - JOB CANCELLED'
                  access:jobexhis.insert()
                  access:jobs.update()
              End!if access:jobexhis.primerecord() = level:benign
          End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
          access:jobs.close()
          access:exchange.close()
          access:exchhist.close()
          access:jobexhis.close()
      End!If old_exchange_unit_number_temp <> new_exchange_unit_number_temp
  Else!If ok_pressed_temp = 0
  End!If ok_pressed_temp = 0
  If thiswindow.response <> 2
      !Include('Bouncer.inc')
  Else!If ok not pressed
  End!If ok_pressed_temp = 1
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    job:date_booked = Today()
    job:Physical_Damage = 'NO'
    job:Insurance = 'NO'
    job:Intermittent_Fault = 'NO'
    job:Internal_Status = 'NEWJOB'
    job:time_booked = Clock()
    job:Loan_Status = '101 NOT ISSUED'
    job:Exchange_Status = '101 NOT ISSUED'
    job:Workshop = 'NO'
    job:Estimate = 'NO'
    job:Third_Party_Printed = 'NO'
    job:EDI = 'XXX'
    job:Completed = 'NO'
    job:Despatched = 'NO'
    job:Paid = 'NO'
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  jbn:RefNumber = job:Ref_Number                      ! Assign linking field value
  Access:JOBNOTES.Fetch(jbn:RefNumberKey)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  !What should be happenind when deleting a part from a job
  !
  !If this an external part
  !    Delete Part
  !Else!If this an external part
  !    If part has been ordered (part order number exists)
  !        If the part has been received (date received exists)
  !            If it is a stock part (stock ref number exists)
  !                If stock part is a sundry
  !                    Delete Part
  !                Else!            If stock part is a sundry
  !                    stock quantity += part quantity
  !                    Create Stock History (Add)
  !                    Delete Part
  !                End!            If stock part is a sundry
  !            Else!If it is a stock part (stock ref number exists)
  !                Create Stock Part
  !                Create Stock History (Add)
  !                Ask to pick locations
  !                Delete Part
  !            End!If it is a stock part (stock ref number exists)
  !        Else!If the part has been received (date received exists)
  !            Mark the Part on the order as received with quantity = 0
  !            Delete Part
  !        End!If the part has been received (date received exists)
  !    Else!If part has been ordered (part order number exists)
  !        If is a pending order (pending order number exists)
  !            Delete pending order
  !            Delete Part
  !        Else!If is a pending order (pending order number exists)
  !            Is it a stock part (stock ref number exists)
  !                If stock part is a sundry
  !                    Delete Part
  !                Else!            If stock part is a sundry
  !                    stock quantity += part quantity
  !                    Create Stock History (Re-credit)
  !                    Delete Part
  !                End!If stock part is a sundry
  !            Else!Is it a stock part (stock ref number exists)
  !                Delete Part
  !            End!Is it a stock part (stock ref number exists)
  !        End!If is a pending order (pending order number exists)
  !    End!If part has been ordered (part order number exists)
  !End!If this an external part
      continue# = 1
      If number = 7 or number = 8 or number = 9
          If job:model_number = ''
              Case Missive('You select a Model Number first.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              continue# = 0
          Else !If job:model_number = ''
              If job:charge_type = '' And number = 1
                  Case Missive('You must select a Charge Type first.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  continue# = 0
              End
              If job:warranty_charge_type = '' And number = 3 And continue# = 1
                  Case Missive('You must select a Warranty Charge Type first.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  continue# = 0
              End

          End !If job:model_number = ''
      End!If number = 5 or number = 6 or number = 7

      If job:date_completed <> ''
          Check_Access('AMEND COMPLETED JOBS',x")
          If x" = False
              Case Missive('Cannot Insert / Delete parts.'&|
                '<13,10>'&|
                '<13,10>This job has been completed.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              continue# = 0
          End
      End
      If job:invoice_Number <> ''
          Check_Access('AMEND INVOICED JOBS',x")
          If x" = False
              If request = InsertRecord Or request = DeleteRecord
                  continue# = 0
                  If number = 7
                      Case Missive('Cannot Insert / Delete parts.'&|
                        '<13,10>'&|
                        '<13,10>This job has been invoiced.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  End!If number = 1
                  If number = 9
                      Case Missive('Cannot Insert / Delete parts.'&|
                        '<13,10>'&|
                        '<13,10>This job has been invoiced.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  End!If number = 1

              End!If request = InsertRecord Or request = DeleteRecord

          End
      End

      If continue# = 1

  !        Case number
   !           Of 7    !Insert Chargeable Part
!                  If Field() = ?Insert
!                      glo:select11 = job:manufacturer
!                      glo:select12 = job:model_number
!                  End !If Field() = ?Insert
!                  If Field() = ?Delete
!                      If DeleteChargeablePart(0)
!                          Delete(PARTS)
!                      End !If DeleteChargeablePart()
!                  End!If Field() = ?Delete
!
!  !            Of 8    !Insert Estiamte Part
!                  If Field() = ?Insert:2
!                      glo:select11 = job:manufacturer
!                      glo:select12 = job:model_number
!                  End !If Field() = ?Insert:3
!                  If Field() = ?Delete:2
!                      If job:Estimate_Accepted <> 'YES' and epr:UsedOnRepair
!                          Include('delparte.inc')
!                      Else
!                          Case MessageEx('Are you sure you want to delete this part?','ServiceBase 2000',|
!                                         'Styles\Trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0)
!                              Of 1 ! &Yes Button
!                                  do_delete# = 1
!                              Of 2 ! &No Button
!                                  do_delete# = 0
!                          End !Case
!                      End !If job:Estimate_Accepted = 'YES'
!                      ThisWindow.Update
!                      !Stock Order
!
!                      If do_delete# = 1
!                          Delete(EstParts)
!                      End !If do_delete# = 1
!                  End!If Field() = ?Delete
!
!  !            Of 9    !Insert Warranty Part
!                  If Field() = ?Insert:3
!                      glo:select11 = job:manufacturer
!                      glo:select12 = job:model_number
!                  End !If Field() = ?Insert:3
!                  If Field() = ?Delete:3
!                      If DeleteWarrantyPart(0)
!                          Delete(WARPARTS)
!                      End !If DeleteWarrantyPart()
!                  End !If Field() = Delete:3


  !        End !Case number
          If request <> 3


  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickWebAccounts
      PickTransitTypes
      Browse_Model_Numbers
      PickChargeableChargeTypes
      PickWarrantyChargeTypes
    END
    ReturnValue = GlobalResponse
  END
      End !If request <> 3
  
      Case number
          Of 7 !Chargeable
              If glo:select1 = 'NEW PENDING' or glo:Select1 = 'NEW PENDING WEB'
                  access:stock.clearkey(sto:ref_number_key)
                  sto:ref_number = par:part_ref_number
                  If access:stock.fetch(sto:ref_number_key)
                      Case Missive('Error! Cannot retrieve the details of this Stock Part.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else!If access:stock.fetch(sto:ref_number_key)
                      sto:quantity_stock     = 0
                      access:stock.update()
  
                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'DEC', | ! Transaction_Type
                                         par:Despatch_Note_Number, | ! Depatch_Note_Number
                                         job:Ref_Number, | ! Job_Number
                                         0, | ! Sales_Number
                                         sto:Quantity_stock, | ! Quantity
                                         par:Purchase_Cost, | ! Purchase_Cost
                                         par:Sale_Cost, | ! Sale_Cost
                                         par:Retail_Cost, | ! Retail_Cost
                                         'STOCK DECREMENTED', | ! Notes
                                         '') ! Information
                        ! Added OK
  
                      Else ! AddToStockHistory
                        ! Error
                      End ! AddToStockHistory
  
                      access:parts_alias.clearkey(par_ali:refpartrefnokey)
                      par_ali:ref_number      = job:ref_number
                      par_ali:part_ref_number = glo:select2
                      If access:parts_alias.fetch(par_ali:RefPartRefNoKey) = Level:Benign
                          par:ref_number           = par_ali:ref_number
                          par:adjustment           = par_ali:adjustment
                          par:part_ref_number      = par_ali:part_ref_number
  
                          par:part_number     = par_ali:part_number
                          par:description     = par_ali:description
                          par:supplier        = par_ali:supplier
                          par:purchase_cost   = par_ali:purchase_cost
                          par:sale_cost       = par_ali:sale_cost
                          par:retail_cost     = par_ali:retail_cost
                          par:quantity             = glo:select3
                          par:warranty_part        = 'NO'
                          par:exclude_from_order   = 'NO'
                          par:despatch_note_number = ''
                          par:date_ordered         = ''
                          par:date_received        = ''
                          par:pending_ref_number   = glo:select4
                          par:order_number         = ''
                          par:fault_code1    = par_ali:fault_code1
                          par:fault_code2    = par_ali:fault_code2
                          par:fault_code3    = par_ali:fault_code3
                          par:fault_code4    = par_ali:fault_code4
                          par:fault_code5    = par_ali:fault_code5
                          par:fault_code6    = par_ali:fault_code6
                          par:fault_code7    = par_ali:fault_code7
                          par:fault_code8    = par_ali:fault_code8
                          par:fault_code9    = par_ali:fault_code9
                          par:fault_code10   = par_ali:fault_code10
                          par:fault_code11   = par_ali:fault_code11
                          par:fault_code12   = par_ali:fault_code12
                          par:Requested      = True
                          If glo:Select1 = 'NEW PENDING WEB'
                              par:WebOrder = 1
                          End !If glo:Select1 = 'NEW PENDING WEB'
                          access:parts.insert()
                      end !If access:parts_alias.clearkey(par_ali:RefPartRefNoKey) = Level:Benign
                  end !if access:stock.fetch(sto:ref_number_key = Level:Benign
              End!If glo:select1 = 'NEW PENDING'
              DO Check_Parts !Check parts to see if status change required?
          Of 9 !Warranty
              If glo:select1 = 'NEW PENDING' Or glo:Select1 = 'NEW PENDING WEB'
                  access:stock.clearkey(sto:ref_number_key)
                  sto:ref_number = wpr:part_ref_number
                  If access:stock.fetch(sto:ref_number_key)
                      Case Missive('Error! Cannot retrieve the details of this Stock Part.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else!If access:stock.fetch(sto:ref_number_key)
                      sto:quantity_stock     = 0
                      access:stock.update()
  
                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'DEC', | ! Transaction_Type
                                         wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                         wpr:Ref_Number, | ! Job_Number
                                         0, | ! Sales_Number
                                         sto:Quantity_Stock, | ! Quantity
                                         wpr:Purchase_Cost, | ! Purchase_Cost
                                         wpr:Sale_Cost, | ! Sale_Cost
                                         wpr:Retail_Cost, | ! Retail_Cost
                                         'STOCK DECREMENTED', | ! Notes
                                         '') ! Information
                        ! Added OK
                      Else ! AddToStockHistory
                        ! Error
                      End ! AddToStockHistory
  
                      access:warparts_alias.clearkey(war_ali:RefPartRefNoKey)
                      war_ali:ref_number      = job:ref_number
                      war_ali:part_ref_number = glo:select2
                      If access:warparts_alias.fetch(war_ali:RefPartRefNoKey) = Level:Benign
                          wpr:ref_number           = war_ali:ref_number
                          wpr:adjustment           = war_ali:adjustment
                          wpr:part_ref_number      = war_ali:part_ref_number
                          wpr:part_number     = war_ali:part_number
                          wpr:description     = war_ali:description
                          wpr:supplier        = war_ali:supplier
                          wpr:purchase_cost   = war_ali:purchase_cost
                          wpr:sale_cost       = war_ali:sale_cost
                          wpr:retail_cost     = war_ali:retail_cost
                          wpr:quantity             = glo:select3
                          wpr:warranty_part        = 'YES'
                          wpr:exclude_from_order   = 'NO'
                          wpr:despatch_note_number = ''
                          wpr:date_ordered         = ''
                          wpr:date_received        = ''
                          wpr:pending_ref_number   = glo:select4
                          wpr:order_number         = ''
                          wpr:fault_code1    = war_ali:fault_code1
                          wpr:fault_code2    = war_ali:fault_code2
                          wpr:fault_code3    = war_ali:fault_code3
                          wpr:fault_code4    = war_ali:fault_code4
                          wpr:fault_code5    = war_ali:fault_code5
                          wpr:fault_code6    = war_ali:fault_code6
                          wpr:fault_code7    = war_ali:fault_code7
                          wpr:fault_code8    = war_ali:fault_code8
                          wpr:fault_code9    = war_ali:fault_code9
                          wpr:fault_code10   = war_ali:fault_code10
                          wpr:fault_code11   = war_ali:fault_code11
                          wpr:fault_code12   = war_ali:fault_code12
                          wpr:Requested      = True
                          If glo:Select1 = 'NEW PENDING WEB'
                              wpr:WebOrder = 1
                          End !If glo:Select1 = 'NEW PENDING WEB'
                          access:warparts.insert()
                      end !If access:parts_alias.clearkey(par_ali:RefPartRefNoKey) = Level:Benign
                  end !if access:stock.fetch(sto:ref_number_key = Level:Benign
              End!If glo:select1 = 'NEW PENDING'
              DO Check_Parts !Check parts to see if status change required?
  
  
      End !Case number
      Else!!If continue# = 1
          Case number
              Of 7
                  access:parts.cancelautoinc()
                  Do check_parts
                  Local.Pricing(0)
  
              Of 8
                  access:estparts.cancelautoinc()
                  Do check_parts
                  Local.Pricing(0)
  
              Of 9
                  access:warparts.cancelautoinc()
                  Do check_parts
                  Local.Pricing(0)
  
          End!Case number
  
      End !If continue# = 1
      Do CountParts
      Clear(glo:G_Select1)
      Clear(glo:G_Select1)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?job:Chargeable_Job
      If ~0{prop:acceptall}
        If job:chargeable_job <> chargeable_job_temp
            If job:chargeable_job = 'YES'
                If job:warranty_job = 'YES'
      
                    Case Missive('This job has already been marked as a Warranty Job.'&|
                      '<13,10>'&|
                      '<13,10>Do you wish to make a SPLIT Warranty/Chargeable job, CHARGEABLE only, or WARRANTY Only?'&|
                      '<13,10>','ServiceBase 3g',|
                                   'mquest.jpg','Warranty|Chargeable|Split')
                        Of 3 ! Split Button
                          chargeable_job_temp  = job:chargeable_job
                        Of 2 ! Chargeable Button
                          job:chargeable_job  = 'YES'
                          job:warranty_job    = 'NO'
                        Of 1 ! Warranty Button
                          job:chargeable_job  = 'NO'
                    End ! Case Missive
      
      
                Else !!If job:warranty_job = 'YES'
                    chargeable_job_temp = job:chargeable_job
                End !If job:warranty_job = 'YES'
      
            Else !If job:chargeable_job = 'YES'
      
                found# = 0
                setcursor(cursor:wait)
                save_par_id = access:parts.savefile()
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = job:ref_number
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                       break
                    end !if
                    if par:ref_number  <> job:ref_number      |
                        then break.  ! end if
                    found# = 1
                    Break
                end !loop
                access:parts.restorefile(save_par_id)
                setcursor()
      
                If job:estimate = 'YES'
                    found# = 2
                End
      
                Case found#
                    Of 1    !Parts
                        Case Missive('Chargeable parts have been added to this job.'&|
                          '<13,10>'&|
                          '<13,10>You must remove these before you can mark this job as Warranty Only.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        job:chargeable_job  = chargeable_job_temp
      
                    Of 2    !Estimate
                        Case Missive('This job has been marked as an Estimate.'&|
                          '<13,10>'&|
                          '<13,10>You cannot make this a Warranty Only Job.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        job:chargeable_job  = chargeable_job_temp
                Else !Case found#
                    chargeable_job_temp = job:chargeable_job
                End !If found# <> 0
            End !If job:chargeable_job = 'YES'
        End !If job:chargeable_job <> chargeable_job_temp
      End !If ~0{prop:acceptall}
      Post(Event:accepted,?job:charge_type)
      
    OF ?job:Warranty_Job
      If ~0{prop:acceptall}
        If job:warranty_job <> warranty_job_temp
            If job:warranty_job = 'YES'
                error# = 0
                If job:chargeable_job = 'YES'
      
                    Case Missive('This job has alreay been marked as a Chargeable Job.'&|
                      '<13,10>'&|
                      '<13,10>Do you wish to make a SPLIT Warranty/Chargeable job, CHARGEABLE only, or WARRANTY only?','ServiceBase 3g',|
                                   'mquest.jpg','Warranty|Chargeable|Split')
                        Of 3 ! Split Button
                          warranty_job_temp  = job:warranty_job
                        Of 2 ! Chargeable Button
                          job:chargeable_job  = 'YES'
                          job:warranty_job    = 'NO'
                        Of 1 ! Warranty Button
                          job:chargeable_job  = 'NO'
                          job:warranty_job    = 'YES'
                    End ! Case Missive
      
                Else !!If job:chargeable_job = 'YES'
                    warranty_job_temp = job:warranty_job
                End !If job:chargeable_job = 'YES'
                charge_type"    = ''
                found# = 0
                save_cha_id = access:chartype.savefile()
                access:chartype.clearkey(cha:warranty_key)
                cha:warranty    = 'YES'
                set(cha:warranty_key,cha:warranty_key)
                loop
                    if access:chartype.next()
                       break
                    end !if
                    if cha:warranty    <> 'YES'      |
                        then break.  ! end if
                    found# += 1
                    charge_type"    = cha:charge_type
                    If found# > 1
                        Break
                    End!If found# > 1
      
                end !loop
                access:chartype.restorefile(save_cha_id)
                If found# = 1
                    job:warranty_Charge_type = charge_type"
                End!If found# = 1
      
      
            Else !If job:chargeable_job = 'YES'
      
                found# = 0
                setcursor(cursor:wait)
                save_wpr_id = access:warparts.savefile()
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = job:ref_number
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> job:ref_number      |
                        then break.  ! end if
                    found# = 1
                    Break
                end !loop
                access:warparts.restorefile(save_wpr_id)
                setcursor()
      
                If found# <> 0
                    Case Missive('Warranty parts have been added to this job.'&|
                      '<13,10>'&|
                      '<13,10>You must remove these before you can mark this job as Chargeable.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    job:warranty_job  = warranty_job_temp
                Else !!If found# <> 0
                    warranty_job_temp   = job:warranty_job
                End !If found# <> 0
            End !If job:warranty_job = 'YES'
        End !If job:warranty_job <> warranty_job_temp
      End !If ~0{prop:acceptall}
      Access:MODELNUM.Clearkey(mod:Model_Number_Key)
      mod:Model_Number    = job:Model_Number
      If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Found
        job:EDI = PendingJob(mod:Manufacturer)
        job:Manufacturer    = mod:Manufacturer
      Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
    OF ?Cancel
      Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
          Of 1 ! No Button
              cycle
      End ! Case Missive
      !Save Fields In Case Of Cancel
      saved_ref_number_temp   = job:Ref_number
      new_exchange_unit_number_temp   = job:exchange_unit_number
      new_loan_unit_number_temp       = job:loan_unit_number
      
      If thiswindow.request = Insertrecord
          Clear(parts_queue_temp)
          Free(parts_queue_temp)
          save_par_id = access:parts.savefile()
          access:parts.clearkey(par:part_number_key)
          par:ref_number  = job:ref_number
          set(par:part_number_key,par:part_number_key)
          loop
              if access:parts.next()
                 break
              end !if
              if par:ref_number  <> job:ref_number      |
                  then break.  ! end if
              partmp:Part_Ref_Number  = par:part_ref_number
              partmp:Quantity = par:quantity
              partmp:Exclude_From_Order   = par:exclude_from_order
              partmp:Pending_Ref_Number   = par:pending_ref_number
              add(parts_queue_temp)
          end !loop
          access:parts.restorefile(save_par_id)
          setcursor()
      End!If thiswindow.request = Insertrecord
    OF ?Audit_Trail_Button
      glo:select12 = job:ref_number
    OF ?Button:ContactHistoryEmpty
      glo:select12 = job:ref_number
    OF ?Fault_Description_Text_Button
      access:jobnotes.clearkey(JBN:RefNumberKey)
      jbn:refnumber   = job:ref_number
      If access:jobnotes.tryfetch(JBN:RefNumberKey)
          If access:jobnotes.primerecord() = Level:Benign
              jbn:refnumber   = job:ref_number
              If access:jobnotes.tryinsert()
                  access:jobnotes.cancelautoinc()
              End!If access:jobnotes.tryinsert()
          End!If access:jobnotes.primerecord() = Level:Benign
      End! If access:jobnotes.tryfetch(JBN:RefNumberKey) = Level:Benign
      
      glo:select1 = job:manufacturer
    OF ?ExchangeButton
      Do SetJOBSE
    OF ?LoanButton
      DO SETJOBSE
      
      !Paul 14/07/2009 Log No 10920
      tmp:LoanUnitNumber = job:Loan_Unit_Number
      !End Change
    OF ?AmendAddresses
      Do SETJOBSE
    OF ?Button:ContactHistoryFilled
      glo:Select12 = job:Ref_Number
    OF ?Button49
      DO SETJOBSE
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OptionsAllocateJob
      ThisWindow.Update
      AllocateEngineerToJob
      ThisWindow.Reset
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_Code   = job:Engineer
          If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Found
              Engineer_Name_temp  = Clip(Use:Forename) & ' ' & Clip(use:Surname)
              tmp:SkillLevel   = use:SkillLevel
          Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
      thiswindow.update()
      display()
    OF ?OptionsEstimate
      ThisWindow.Update
      EstimateDetails
      ThisWindow.Reset
      thiswindow.update()
      display()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020392'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020392'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020392'&'0')
      ***
    OF ?Button:ReleaseForDespatch
      ThisWindow.Update
      Beep(Beep:SystemQuestion);  Yield()
      Case Missive('Are you sure you want to release the current job for despatch?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              glo:EDI_Reason = ''
              Get_EDI_Reason
              If Clip(glo:EDI_Reason) <> ''
                  If AddToAudit(job:Ref_Number,'JOB','OUT OF REGION: RELEASED FOR DESPATCH',Clip(Upper(glo:EDI_Reason)))
      
                  End ! If AddToAudit(job:Ref_Number,'JOB','OUT OF REGION: RELEASED FOR DESPATCH',|
                  Beep(Beep:SystemAsterisk);  Yield()
                  Case Missive('You can now despatch this job.','ServiceBase 3g',|
                                 'midea.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  ?Button:ReleaseForDEspatch{Prop:Hide} = 1
              End ! If Clip(glo:EDI_Reason) <> ''
              glo:EDI_Reason = ''
          Of 1 ! No Button
      End ! Case Missive
    OF ?job:Auto_Search
      
       temp_string = Clip(left(job:Auto_Search))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       job:Auto_Search = temp_string
       Display(?job:Auto_Search)
      If ~0{prop:acceptall}
          If ThisWindow.Request = Insertrecord
              If job:auto_search <> ''
                  Do History_search
                  history_run_temp = 1
              End!If job:auto_search <> ''
          End
      
          Do check_msn
      
      End
    OF ?job:Account_Number
      If ~0{prop:acceptall}
      do_lookup# = 1
      If job:account_number <> account_number_temp
          error# = 0
          If job:date_completed <> ''
              Check_Access('AMEND COMPLETED JOBS',x")
              If x" = False
                  Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                  End ! Case Missive
                  error# = 1
              End
          End
          If job:invoice_Number <> ''
              Check_Access('AMEND INVOICED JOBS',x")
              If x" = False
                  Case Missive('Cannot change! This job has been invoiced.','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                  End ! Case Missive
                  error# = 1
              End
          End
          If error# = 1
              job:account_number = account_number_temp
              do_lookup# = 0
          Else!If job:date_Completed <> ''
              do_lookup# = 1
          End!If job:date_Completed <> ''
      
      End!If job:account_number <> account_number_temp
      Display()
      If do_lookup# = 1
          If glo:WebJob
              glo:Select1 = Clarionet:Global.Param2
          Else !If glo:WebJob
              glo:Select1 = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
          End !If glo:WebJob
      IF job:Account_Number OR ?job:Account_Number{Prop:Req}
        sub:Account_Number = job:Account_Number
        !Save Lookup Field Incase Of error
        look:job:Account_Number        = job:Account_Number
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Account_Number = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            job:Account_Number = look:job:Account_Number
            SELECT(?job:Account_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
          glo:Select1 = ''
          stop# = 0
          access:subtracc.clearkey(sub:account_number_key)
          sub:account_number = job:account_number
          if access:subtracc.fetch(sub:account_number_key) = level:benign
              access:tradeacc.clearkey(tra:account_number_key)
              tra:account_number = sub:main_account_number
              if access:tradeacc.fetch(tra:account_number_key) = level:benign
                  if tra:use_sub_accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
                      If sub:ForceOrderNumber
                          ?job:Order_Number{prop:req} = 1
                      Else !If tra:ForceOrderNumber
                          ?job:Order_Number{prop:req} = 0
                      End !If tra:ForceOrderNumber
      
                      If sub:stop_account = 'YES'
                          stop# = 1
                      End!If sub:stop_account = 'YES'
                  else!if tra:use_sub_accounts = 'YES'
                      If tra:ForceOrderNumber
                          ?job:Order_Number{prop:req} = 1
                      Else !If tra:ForceOrderNumber
                          ?job:Order_Number{prop:req} = 0
                      End !If tra:ForceOrderNumber
      
                      If tra:stop_account = 'YES'
                          stop# = 1
                      End!If tra:stop_account = 'YES'
                  end!if tra:use_sub_accounts = 'YES'
              end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
          end!if access:subtracc.fetch(sub:account_number_key) = level:benign
      
      ! Inserting (DBH 04/02/2008) # 9613 - Check that the account picked is in the hub?
          If Stop# = 0
              If tra:Invoice_Sub_Accounts <> 'YES'
                  If tra:Use_Customer_Address <> 'YES'
                      If HubOutOfRegion(wob:HeadAccountNumber,tra:Hub) = 1
                          Beep(Beep:SystemExclamation);  Yield()
                          Case Missive('Warning! The selected Account is outside your Hub.'&|
                              '|If you continue you may not be able to despatch this job.'&|
                              '|'&|
                              '|Are you sure you want to continue?','ServiceBase 3g',|
                                         'mexclam.jpg','/No|\Yes')
                              Of 2 ! Yes Button
                              Of 1 ! No Button
                                  job:Account_Number = Account_Number_Temp
                                  Select(?job:Account_Number)
                                  Display()
                                  Cycle
                          End ! Case Missive
                      End ! If HubOutOfRegion(wob:HeadAccountNumber,tra:Hub) = 1
                  End ! If tra:Use_Customer_Address <> 'YES'
              Else ! If tra:Invoice_Sub_Accounts <> 'YES'
                  If sub:Use_Customer_Address <> 'YES'
                      If HubOutOfRegion(wob:HeadAccountNumber,sub:Hub) = 1
                          Beep(Beep:SystemExclamation);  Yield()
                          Case Missive('Warning! The selected Account is outside your Hub.'&|
                              '|If you continue you may not be able to despatch this job.'&|
                              '|'&|
                              '|Are you sure you want to continue?','ServiceBase 3g',|
                                         'mexclam.jpg','/No|\Yes')
                              Of 2 ! Yes Button
                              Of 1 ! No Button
                                  job:Account_Number = Account_Number_Temp
                                  Select(?job:Account_Number)
                                  Display()
                                  Cycle
                          End ! Case Missive
                      End ! If HubOutOfRegion(wob:HeadAccountNumber,tra:Hub) = 1
                  End ! If sub:Use_Customer_Address <> 'YES'
              End ! If tra:Invoice_Sub_Accounts <> 'YES'
          End ! If Stop# = 0
      ! End (DBH 04/02/2008) #9613
      
      
          If stop# = 0
              error# = 0
              access:trantype.clearkey(trt:transit_type_key)
              trt:transit_type = job:transit_type
              if access:trantype.fetch(trt:transit_type_key) = Level:Benign
                  If trt:loan_unit = 'YES' And tra:allow_loan <> 'YES'
                      Case Missive('Loan Units are not authorised for this Trade Account.'&|
                        '<13,10>'&|
                        '<13,10>Amend the Trade Account details or select another Initial Transit Type.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  End !If trt:loan_unit = 'YES' And tra:allow_loan <> 'YES'
                  If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES' And error# = 0
                      Case Missive('Exchange Units are not authorised for this Trade Account.'&|
                        '<13,10>'&|
                        '<13,10>Amend the Trade Account details or select another Initial Transit Type.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  End !If trt:exchange_unit = 'YES' And tra:allow_loan <> 'YES'
              end!if access:trantype.fetch(trt:transit_type_key) = Level:Benign
      
              !Get New Costs
              error# = 0
              If CheckPricing('C') = 1 Or CheckPricing('W') = 1
                  error# = 1
              End!If CheckPricing('C') = 1 Or CheckPricing('W') = 1
      
              !Check to see if the IMEI matches the account number, if there is one.
              If Error# = 0 And job:ESN <> ''
                  If ExchangeAccount(job:Account_Number)
                      Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                      xch:ESN = job:ESN
                      If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                          !Found
                          !Write the relevant fields to say this unit is in repair and take completed.
                          !Otherwise the use could click cancel and screw everything up.
                      Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                          !Error
                          Case Missive('The selected Trade Account is setup to repair Exchange Units, but the entered I.M.E.I. Number cannot be found in the Exchange Stock.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          Error# = 1
                      End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                  Else
                      Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
                      xch:ESN = job:ESN
                      If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                          !Found
                          If xch:Available <> 'DES'
                              Case Missive('The entered I.M.E.I. matches an entry in Exchange Stock. This unit has not marked "Despatched" so may still exist in stock.'&|
                                '<13,10>If you continue this job will be a normal repair and not an "In House Exchanger Repair".'&|
                                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                             'mquest.jpg','\No|/Yes')
                                  Of 2 ! Yes Button
                                  Of 1 ! No Button
                                      Error# = 1
                              End ! Case Missive
                          End!Case MessageEx
                      Else! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
      
                  End !ExchangeAccount(job:Account_Number)
              End !Error# = 0
      
      
              If error# = 0
                  If tra:use_contact_name = 'YES'
                      Unhide(?NameGroup)
                  Else!If tra:use_contact_name = 'YES'
                      Hide(?NameGroup)
                  End!If tra:use_contact_name = 'YES'
                  overwrite# = 1
                  If account_number_temp <> '' and job:account_number <> account_number_temp
                      Case Missive('You have selected a NEW Trade Account. This account may assign different Prices, Address'', Turnaround Times and Couriers to this job.'&|
                        '<13,10>Are you sure you want to overwrite this information?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              overwrite# = 1
                          Of 1 ! No Button
                              overwrite# = 0
                      End ! Case Missive
                  End!If trade_account_temp <> ''
                  If overwrite# = 1
                      ! Inserting (DBH 04/02/2008) # 9613 - If "Cash" sale, default the hub to the RRC's. Although this will change
                      If Instring('CASH',Upper(job:Account_Number),1,1)
                          Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
                          tra_ali:Account_number = wob:HeadAccountNumber
                          If Access:TRADEACC_ALIAS.TryFetch(tra_Ali:Account_Number_Key) = Level:Benign
                              jobe2:HubCustomer   = tra:Hub
                              jobe2:HubCollection = tra:Hub
                              jobe2:HubDelivery   = tra:Hub
                          End ! If Access:TRADEACC_ALIAS.TryFetch(tra_Ali:Account_Number_Key) = Level:Benign
                      End ! If Instring('CASH',Upper(job:Account_Number),1,1)
                      ! End (DBH 04/02/2008) #9613
      
                      If tra:force_estimate = 'YES'
                          job:estimate_if_over    = tra:estimate_if_over
                          job:estimate        = 'YES'
                          Post(event:accepted,?job:estimate)
                      End!If tra:force_estimate = 'YES'
                      If tra:turnaround_time <> ''
                          job:turnaround_time = tra:turnaround_time
                          Post(event:accepted,?job:turnaround_time)
                      End!If tra:turnaround_time <> ''
                      If tra:invoice_sub_accounts <> 'YES'
                          If tra:use_customer_address <> 'YES'
                              job:postcode         = tra:postcode
                              job:company_name     = tra:company_name
                              job:address_line1    = tra:address_line1
                              job:address_line2    = tra:address_line2
                              job:address_line3    = tra:address_line3
                              job:telephone_number = tra:telephone_number
                              job:fax_number       = tra:fax_number
                              jobe:EndUserEmailAddress = tra:EmailAddress
                              jobe2:HubCustomer = tra:Hub
                          End!If tra:use_customer_address <> 'YES'
                          If tra:courier_cost <> ''
                              job:courier_cost            = sub:courier_cost
                              job:courier_cost_warranty   = sub:courier_cost
                          End!If tra:courier_cost <> '' and overwrite# = 1
                      Else!!If tra:invoice_sub_accounts <> 'YES'
                          If sub:use_customer_address <> 'YES'
                              job:postcode         = sub:postcode
                              job:company_name     = sub:company_name
                              job:address_line1    = sub:address_Line1
                              job:address_line2    = sub:address_line2
                              job:address_line3    = sub:address_line3
                              job:telephone_number = sub:telephone_number
                              job:fax_number       = sub:fax_number
                              jobe:EndUserEmailAddress = sub:EmailAddress
                              jobe2:HubCustomer = sub:Hub
                          End!If sub:use_customer_address <> 'YES'
                          If sub:courier_cost <> ''
                              job:courier_cost            = sub:courier_cost
                              job:courier_cost_warranty   = sub:courier_cost
                          End!If sub:courier_cost <> '' And overwrite# = 1
                      End!If tra:invoice_sub_accounts <> 'YES'
                      If tra:use_sub_accounts <> 'YES'
                          If tra:use_delivery_address = 'YES'
                              job:postcode_delivery   = tra:postcode
                              job:company_name_delivery   = tra:company_name
                              job:address_line1_delivery  = tra:address_line1
                              job:address_line2_delivery  = tra:address_line2
                              job:address_line3_delivery  = tra:address_line3
                              job:telephone_delivery      = tra:telephone_number
                              jobe2:HubDelivery = tra:Hub
                          End !If tra:use_delivery_address = 'YES'
                          If tra:use_collection_address = 'YES' and overwrite# = 1
                              job:postcode_collection = tra:postcode
                              job:company_name_collection = tra:company_Name
                              job:address_line1_collection    = tra:address_line1
                              job:address_line2_collection    = tra:address_line2
                              job:address_line3_collection    = tra:address_line3
                              job:telephone_collection    = tra:telephone_number
                              jobe2:HubCollection = tra:Hub
                          End!If tra:use_collection_address = 'YES'
                          If overwrite# = 1
                              job:courier = tra:courier_outgoing
                              job:incoming_courier = tra:courier_incoming
                              job:exchange_courier = job:courier
                              job:loan_courier = job:courier
                          End!If overwrite# = 1
                      Else!If tra:use_sub_accounts <> 'YES'
                          If sub:use_delivery_address = 'YES' and overwrite# = 1
                              job:postcode_delivery      = sub:postcode
                              job:company_name_delivery  = sub:company_name
                              job:address_line1_delivery = sub:address_line1
                              job:address_line2_delivery = sub:address_line2
                              job:address_line3_delivery = sub:address_line3
                              job:telephone_delivery     = sub:telephone_number
                              jobe2:HubDelivery = sub:Hub
                          End
                          If sub:use_collection_address = 'YES' and overwrite# = 1
                              job:postcode_collection = sub:postcode
                              job:company_name_collection = sub:company_Name
                              job:address_line1_collection    = sub:address_line1
                              job:address_line2_collection    = sub:address_line2
                              job:address_line3_collection    = sub:address_line3
                              job:telephone_collection    = sub:telephone_number
                              jobe2:HubCollection = sub:Hub
                          End!If tra:use_collection_address = 'YES'
                          if overwrite# = 1
                              job:courier = sub:courier_outgoing
                              job:incoming_courier = sub:courier_incoming
                              job:exchange_courier = job:courier
                              job:loan_courier = job:courier
                          End!if overwrite# = 1
                      End!If tra:use_sub_accounts <> 'YES'
                      If overwrite# = 1
                          access:courier.clearkey(cou:courier_key)
                          cou:courier = job:courier
                          access:courier.tryfetch(cou:courier_key)
                          IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                              job:loaservice  = cou:service
                              job:excservice  = cou:service
                              job:jobservice  = cou:service
                          Else!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                              job:loaservice  = ''
                              job:excservice  = ''
                              job:jobservice  = ''
                          End!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                      End!If overwrite# = 1
                      account_number_temp = job:account_number
      
                      Beep(Beep:SystemAsterisk);  Yield()
                      Case Missive('You have changed the Account Number.'&|
                          '|'&|
                          '|The job details have been saved.','ServiceBase 3g',|
                                     'midea.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Access:JOBS.TryUpdate()
                      Access:JOBSE.TryUpdate()
                      Access:WEBJOB.TryUpdate()
                      Access:JOBSE2.TryUpdate()
                  Else!If overwrite# = 1
                      job:account_number = account_number_temp
                  End!If overwrite# = 1
              Else!If error# = 0
                  job:account_number  = account_number_temp
              End!If error# = 0
          Else!If stop# = 0
              Case Missive('The selected Trade Account is on STOP.'&|
                '<13,10>'&|
                '<13,10>Please select another.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              job:account_number  = account_number_temp
          End!If stop# = 0
      
      End!If do_lookup# = 1
      Do Estimate
      Display()
      End!End!If ~0{prop:acceptall}
      Do CheckRequiredFields
    OF ?LookupTradeAccount
      ThisWindow.Update
      If glo:WebJob
        glo:Select1 = Clarionet:Global.Param2
      Else !If glo:WebJob
        glo:Select1 = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      End !If glo:WebJob
      sub:Account_Number = job:Account_Number
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Account_Number = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?job:Account_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Account_Number)
      glo:Select1 = ''
      
      message(GLO:Select13)
      
      If clip(GLO:Select13) <> ''  then
          job:Account_Number = clip(GLO:Select13)
      End
      
      Display()
      
      message(job:Account_Number & '/' & GLO:Select13)
    OF ?TradeAccountLookupButton
      ThisWindow.Update
      PickWebAccounts
      ThisWindow.Reset
      If clip(GLO:Select13) <> ''  then
          job:Account_Number = clip(GLO:Select13)
      End
      
      Display()
      
      Post(event:accepted,?job:Account_Number)
    OF ?job:Transit_Type
      If ~0{prop:acceptall}
      do_lookup# = 1
      If job:transit_type <> transit_type_temp
          error# = 0
          If job:date_completed <> ''
              Check_Access('AMEND COMPLETED JOBS',x")
              If x" = False
                  Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  error# = 1
                  job:transit_type = transit_type_temp
              End
          End
          If job:invoice_Number <> ''
              Check_Access('AMEND INVOICED JOBS',x")
              If x" = False
                  Case Missive('Cannot change! This job has been invoiced.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  job:transit_type = transit_Type_temp
                  error# = 1
              End
          End
          If error# = 1
              do_lookup# = 0
          End!If error# = 1
      End!If job:transit_type <> transit_type_temp
      If do_lookup# = 1
      
      IF job:Transit_Type OR ?job:Transit_Type{Prop:Req}
        trt:Transit_Type = job:Transit_Type
        !Save Lookup Field Incase Of error
        look:job:Transit_Type        = job:Transit_Type
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            job:Transit_Type = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            job:Transit_Type = look:job:Transit_Type
            SELECT(?job:Transit_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
          If job:transit_type <> transit_type_temp
              Set(DEFAULTS)
              Access:DEFAULTS.Next()
              access:trantype.clearkey(trt:transit_type_key)
              trt:transit_type = job:transit_type
              if access:trantype.fetch(trt:transit_type_key) = Level:Benign
                  error# = 0
                  access:subtracc.clearkey(sub:account_number_key)
                  sub:account_number = job:account_number
                  if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                      access:tradeacc.clearkey(tra:account_number_key)
                      tra:account_number = sub:main_account_number
                      if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                          If trt:loan_unit    = 'YES' And tra:allow_loan <> 'YES'
                              error# = 1
                              Case Missive('Loan Units are not authorised for this Trade Account.'&|
                                '<13,10>'&|
                                '<13,10>Select another Initial Transit Type or amend the Trade Account defaults.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              job:transit_type    = transit_type_temp
                          End!If trt:loan_unit    = 'YES' And tra:allow_loan <> 'YES'
                          If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES' And error# = 0
                              Case Missive('Exchange Units are not authorised for this Trade Account.'&|
                                '<13,10>'&|
                                '<13,10>Select another Initial Transit Type or amend the Trade Account defaults.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              job:transit_Type    = transit_type_temp
                              error# = 1
                          End!If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES'
                      end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                  end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                  If error# = 0
                      hide# = 0
                      If trt:collection_address <> 'YES'
                          If job:address_line1 <> ''
                              Case Missive('The selected Transit Type will remove the Collection Address.'&|
                                '<13,10>'&|
                                '<13,10>Are you sure?','ServiceBase 3g',|
                                             'mquest.jpg','\No|/Yes')
                                  Of 2 ! Yes Button
                                      hide# = 1
                                  Of 1 ! No Button
                              End ! Case Missive
                          Else
                              hide# = 1
                          End
                          If hide# = 1
                              job:postcode_collection         = ''
                              job:company_name_collection     = ''
                              job:address_line1_collection    = ''
                              job:address_line2_collection    = ''
                              job:address_line3_collection    = ''
                              job:telephone_collection = ''
                          End
                      Else
                      End
                      hide# = 0
                      If trt:delivery_address <> 'YES'
                          If job:address_line1_delivery <> ''
                              Case Missive('The selected Transit Type will remove the Delivery Address.'&|
                                '<13,10>'&|
                                '<13,10>Are you sure?','ServiceBase 3g',|
                                             'mquest.jpg','\No|/Yes')
                                  Of 2 ! Yes Button
                                      hide# = 1
                                  Of 1 ! No Button
                              End ! Case Missive
                          Else
                              hide# = 1
                          End
                          If hide# = 1
                              job:postcode_delivery         = ''
                              job:company_name_delivery     = ''
                              job:address_line1_delivery    = ''
                              job:address_line2_delivery    = ''
                              job:address_line3_delivery    = ''
                              job:telephone_delivery = ''
                          End
                      Else
                      End
                      If trt:InWorkshop = 'YES'
                          job:workshop = 'YES'
                      Else!If trt:InWorkshop = 'YES'
                          job:workshop = 'NO'
                      End!If trt:InWorkshop = 'YES'
      
                      If trt:InternalLocation <> ''
                          job:Location    = trt:InternalLocation
                      End !If trt:InternalLocation <> ''
      
                      If trt:location <> 'YES'
                          Hide(?location_group)
                          ?job:location{prop:req} = 0
                      Else
                          If ~def:HideLocation
                              If job:workshop = 'YES'
                                  Unhide(?location_group)
                              End
                              If trt:force_location = 'YES' And job:workshop = 'YES'
                                  ?job:location{prop:req} = 1
                              Else!If trt:force_location = 'YES'
                                  ?job:location{prop:req} = 0
                              End!If trt:force_location = 'YES'
                          Else
                              tmp:DisableLocation = GETINI('DISABLE','InternalLocation',,CLIP(PATH())&'\SB2KDEF.INI')
                              if tmp:DisableLocation
                                  If job:workshop = 'YES'
                                      Unhide(?location_group)
                                      Disable(?location_group)
                                  End
                                  ?job:location{prop:req} = 0
                                  ?job:location{prop:Disable} = 1
                              end
                          End !If ~def:HideLocation
                      End
                      If tmp:HubRepair{prop:Hide} = 0
                          tmp:HubRepair = trt:HubRepair
                      End !If tmp:HubRepair{prop:Hide} = 0
                      If ThisWindow.Request = Insertrecord
                          GetStatus(Sub(trt:initial_status,1,3),1,'JOB')
      
                          Brw21.resetsort(1)
                          Do time_remaining
      
                          GetStatus(Sub(trt:Exchangestatus,1,3),1,'EXC')
                          GetStatus(Sub(trt:loanstatus,1,3),1,'LOA')
      
                          If trt:skip_workshop <> 'YES'
                              Unhide(?job:workshop)
                              Select(?job:workshop)
                          End
                          If trt:workshop_label = 'YES' and job:workshop = 'YES'
                              print_label_temp = 'YES'
                          Else
                              print_label_temp = 'NO'
                          End
      
                          IF tra:refurbcharge = 'YES' and TRT:Exchange_Unit = 'YES'
                              job:chargeable_job = 'YES'
                              job:warranty_job    = 'YES'
                              job:charge_type = tra:ChargeType
                              job:Warranty_Charge_Type    = tra:WarChargeType
                              Unhide(?job:charge_type)
                              Post(Event:Accepted,?job:Charge_Type)
                              Unhide(?job:warranty_charge_type)
                              Unhide(?lookupwarrantychargetype)
                              Unhide(?lookupChargeType)
                          End!IF tra:refurbcharge = 'YES'
      
                          If trt:job_card = 'YES'
                              tmp:printjobcard    = 1
                          Else!If trt:job_card = 'YES'
                              tmp:printjobcard    = 0
                          End!If trt:job_card = 'YES'
                          If trt:jobReceipt = 'YES'
                              tmp:printjobreceipt = 1
                          Else!If trt:job_Receipt = 'YES'
                              tmp:printjobreceipt = 0
                          End!If trt:job_Receipt = 'YES'
      
                          turnerror# = 0
                          If job:turnaround_time <> '' and job:turnaround_time <> trt:initial_priority
                              Case Missive('The selected Transit Type requires a Turnaround Time of ' & Clip(trt:Initial_Priority) & '.'&|
                                '<13,10>'&|
                                '<13,10>A Turnaround Time has already been assigned to this job. Do you wish to change this?','ServiceBase 3g',|
                                             'mquest.jpg','\No|/Yes')
                                  Of 2 ! Yes Button
                                  Of 1 ! No Button
                                      turnerror# = 1
                              End ! Case Missive
                          End!If job:turnaround_time <> ''
                          If turnerror# = 0
                              job:turnaround_time = trt:initial_priority
                              Display(?job:Turnaround_Time)
                              Post(Event:Accepted,?job:Turnaround_Time)
      
                          End!If error# = 0
                      End
                  End!If error# = 0
              end!if access:trantype.fetch(trt:transit_type_key) = Level:Benign
              If error# = 1
                  job:transit_type = transit_type_temp
              Else
                  transit_type_temp   = job:transit_type
              End!If error# = 1
          End!If job:transit_type <> transit_type_temp
      
          End!If do_lookup# = 1
      End !If ~0{prop:acceptall}
      Do CheckRequiredFields
    OF ?LookupTransitType
      ThisWindow.Update
      trt:Transit_Type = job:Transit_Type
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          job:Transit_Type = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?job:Transit_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Transit_Type)
    OF ?job:Workshop
      If ~quickwindow{prop:acceptall}
      If job:workshop <> workshop_temp
          error# = 0
          If job:workshop = 'YES'
              If job:third_party_site <> ''
                  If SecurityCheck('RETURN UNIT FROM 3RD PARTY')
                      Case Missive('This unit is at a 3rd Party Site. You can only return it via 3rd Party Return in Rapid 3rd Party Despatch.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  Else!If SecurityCheck('RETURN UNIT FROM 3RD PARTY')
                      Case Missive('This unit is at a 3rd Party Site. It should be returned bia 3rd Party Retudn in Rapid 3rd Party Depsatch.'&|
                        '<13,10>You should only mark this unit as "In Workshop" in the event of an error. Are you sure?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                          Of 1 ! No Button
                              error# = 1
                      End ! Case Missive
                  End!If SecurityCheck('RETURN UNIT FROM 3RD PARTY')

              End!If job:third_party_site <> ''

              IF error# = 1
                  job:workshop = workshop_temp
              Else!IF error# = 1
                  workshop_temp = job:workshop

                  IF (AddToAudit(job:Ref_Number,'JOB','UNIT BROUGHT INTO WORKSHOP',CHOOSE(job:third_party_site <> '','FROM THIRD PARTY SITE: ' & Clip(job:Third_Party_Site),'')))
                  END ! IF

      !If there is an incoming consignment note number, mark that the unit has come in.
                  If job:exchange_unit_number <> '' And job:incoming_consignment_number <> ''
                      job:incoming_date = Today()
!                      Display(?job:incoming_date)
                  End!If job:exchange_unit_number <> '' And job:incoming_consignment_number <> ''

      !The customer unit is brought into the workshop. Mark the replacement unit as 'In Repair'

      !Get The Audit Number
                  access:exchange_alias.clearkey(xch_ali:ref_number_key)
                  xch_ali:ref_number = job:exchange_unit_number
                  if access:exchange_alias.fetch(xch_ali:ref_number_key) = Level:Benign
                      If xch:audit_number <> ''
                          access:excaudit.clearkey(exa:audit_number_key)
                          exa:stock_type   = xch_ali:stock_type
                          exa:audit_number = xch_ali:audit_number
                          if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
      !Get The Replacement Unit Detailts
                              access:exchange.clearkey(xch:ref_number_key)
                              xch:ref_number = exa:replacement_unit_number
                              if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                                  xch:available = 'REP'
                                  xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                                  access:exchange.update()
                                  get(exchhist,0)
                                  if access:exchhist.primerecord() = level:benign
                                      exh:ref_number   = xch:ref_number
                                      exh:date          = today()
                                      exh:time          = clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      exh:user = use:user_code
                                      exh:status        = 'UNIT PUT INTO REPAIR'
                                      access:exchhist.insert()
                                  end!if access:exchhist.primerecord() = level:benign

                              end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                          end!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
                      End!If xch:audit_number <> ''
                  end!if access:exchange_alias.fetch(xch_ali:ref_number_key) = Level:Benign

                  access:subtracc.clearkey(sub:account_number_key)
                  sub:account_number = job:account_number
                  if access:subtracc.fetch(sub:account_number_key) = level:benign
                      access:tradeacc.clearkey(tra:account_number_key)
                      tra:account_number = sub:main_account_number
                      if access:tradeacc.fetch(tra:account_number_key) = level:benign
                          If tra:refurbcharge = 'YES'
                              refurb# = 0
                              IF job:exchange_unit_number <> ''
                                  refurb# = 1
                              End!IF job:exchange_unit_number <> ''
                              If refurb# = 0
                                  access:trantype.clearkey(trt:transit_type_key)
                                  trt:transit_type = job:transit_type
                                  if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                                      If trt:exchange_unit = 'YES'
                                          refurb# = 1
                                      End!If trt:exchange_unit = 'YES'
                                  EnD!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                              End!If refurb# = 0
                              IF refurb# = 0
                                  IF Sub(job:exchange_status,1,3) = '108'
                                      refurb# = 1
                                  End!IF Sub(job:exchange_status,1,3) = '108'
                              End!IF refurb# = 0
                              IF refurb# = 1
                                  Case Missive('This Repair Unit must be refurbished.'&|
                                    '<13,10>'&|
                                    '<13,10>A Refurbishment lavel will be princed when you have completed this job.','ServiceBase 3g',|
                                                 'mexclam.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  print_label_temp = 'YES'
                              End!IF refurb# = 1
                          End!If tra:refurbcharge = 'YES'
                      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                  end!if access:subtracc.fetch(sub:account_number_key) = level:benign

             End!IF error# = 1
          End!If job:workshop = 'YES'
      End

      Do location_Type

      End     !If ~quickwindow{prop:acceptall}
      ! Status Change
      If ~0{prop:acceptall}
          If job:date_completed = ''
      !        job:location = ''
              job:in_repair = 'NO'
              job:engineer = ''
              job:on_test = 'NO'
              GetStatus(305,thiswindow.request,'JOB') !awaiting allocation
              Brw21.resetsort(1)
              Do time_remaining
          End
      End
      
      BRW21.ResetQueue(1)
      Do CheckRequiredFields
    OF ?job:Location
      If ~0{prop:acceptall}
      do_lookup# = 1
      
      If job:location <> location_temp
          error# = 0
          If job:date_completed <> ''
              If SecurityCheck('AMEND COMPLETED JOBS')
                  Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  error# = 1
                  job:location = location_temp
              End !If SecurityCheck('AMEND COMPLETED JOBS')
          End
          If job:invoice_Number <> ''
              Check_Access('AMEND INVOICED JOBS',x")
              If x" = False
                  Case Missive('Cannot change! This job has been invoiced.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  job:location = location_temp
                  error# = 1
              End
          End
      
          If error# = 1
              do_lookup# = 0
          End!"If error# = 1
      End!If job:location <> location_temp
      Do show_tabs
      If do_lookup# = 1 and job:location <> location_temp
      
      
              If job:location <> location_temp
                  If location_temp <> ''
              !Add To Old Location
                      access:locinter.clearkey(loi:location_key)
                      loi:location = location_temp
                      If access:locinter.fetch(loi:location_key) = Level:Benign
                          If loi:allocate_spaces = 'YES'
                              loi:current_spaces+= 1
                              loi:location_available = 'YES'
                              access:locinter.update()
                          End
                      end !if
                  End!If location_temp <> ''
              !Take From New Location
                  access:locinter.clearkey(loi:location_key)
                  loi:location = job:location
                  If access:locinter.fetch(loi:location_key) = Level:Benign
                      If loi:allocate_spaces = 'YES'
                          loi:current_spaces -= 1
                          If loi:current_spaces< 1
                              loi:current_spaces = 0
                              loi:location_available = 'NO'
                          End
                          access:locinter.update()
                      End
                  end !if
      
                  !Write To Log
                  If Access:LOCATLOG.PrimeRecord() = Level:Benign
                      lot:RefNumber   = job:Ref_Number
                      Access:USERS.Clearkey(use:Password_Key)
                      use:Password    = glo:Password
                      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                          !Found
                          lot:UserCode    = use:User_code
                      Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
                      lot:PreviousLocation    = Location_temp
                      lot:NewLocation         = job:Location
                      If Access:LOCATLOG.TryInsert() = Level:Benign
                          !Insert Successful
                      Else !If Access:LOCATLOG.TryInsert() = Level:Benign
                          !Insert Failed
                      End !If Access:LOCATLOG.TryInsert() = Level:Benign
                  End !If Access:LOCATLOG.PrimeRecord() = Level:Benign
                  location_temp  = job:location
      
                  brw21.resetsort(1)
      
              End!If job:location <> location_temp
          End!If do_lookup# = 1
      End!If ~0{prop:acceptall}
    OF ?job:ESN
      If ~0{prop:acceptall}
          IF LEN(CLIP(job:ESN)) = 18
            !Ericsson IMEI!
            Job:ESN = SUB(job:ESN,4,15)
            !ESN_Entry_Temp = Job:ESN
            UPDATE()
          ELSE
            !Job:ESN = ESN_Entry_Temp
          END
          Do check_msn
      
      
      Error# = 0
      If job:ESN <> ESN_Temp
          ! Inserting (DBH 01/06/2006) #6972 - Check access and validity of the IMEI Number
          If SecurityCheck('JOBS - AMEND IMEI NUMBER')
              Case Missive('You do not have sufficient access to amend the IMEI Number.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              job:ESN = ESN_Temp
              Display()
              Cycle
          End ! If SecurityCheck('JOBS - AMEND IMEI NUMBER')
      
          If IsIMEIValid(job:ESN,job:Model_Number,1) = False
              job:ESN = ESN_Temp
              Display()
              Cycle
          End ! If IsIMEIValid(job:ESN,job:Model_Number,1) = False
          ! End (DBH 01/06/2006) #6972
      
      
          If ESN_Temp <> ''
              Case Missive('Are you sure you want to change the I.M.E.I. Number?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                  Of 1 ! No Button
                      error# = 1
              End ! Case Missive
          End !If ESN_Temp <> ''
      
          If Error# = 0
              If job:ESN <> 'N/A'
                  Set(DEFAULTS)
                  Access:DEFAULTS.Next()
                  If def:Allow_Bouncer <> 'YES'
                      If CountBouncer()
                          Case Missive('The I.M.E.I. Number has been entered onto the system before.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          Error# = 1
                      End!If found# = 1
                  Else
                      If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                          If LiveBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
                              Case Missive('The selected I.M.E.I. is already entered onto a live job. You must complete this previous job before you can continue.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Error# = 1
                          End !If LiveBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
                      End !If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                  End !If def:Allow_Bouncers <> 'YES'
              End !If job:ESN <> 'N/A'
          End !If Error# = 0
      End !job:ESN <> ESN_Temp
      
      If Error# = 0
          If ExchangeAccount(job:Account_Number)
              Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
              xch:ESN = job:ESN
              If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                  !Found
                  !Write the relevant fields to say this unit is in repair and take completed.
                  !Otherwise the use could click cancel and screw everything up.
              Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
                  Case Missive('The selected Trade Account is setup to repair Exchange Units, but the entered I.M.E.I. number cannot be found in Exchange Stock.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Error# = 1
              End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
          Else !ExchangeAccount(job:Account_Number)
              Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
              xch:ESN = job:ESN
              If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                  !Found
                  If xch:Available <> 'DES'
                      Case Missive('The entered I.M.E.I. matches an entry in Exchange Stock. This unit has not marked "Despatched" so may still exist in stock.'&|
                        '<13,10>If you continue this job will be a normal repair and not an "In House Exchanger Repair".'&|
                        '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                          Of 1 ! No Button
                              Error# = 1
                      End ! Case Missive
                  End!Case MessageEx
              Else! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
      
      
          End !ExchangeAccount(job:Account_Number)
      End !Error# = 0
      
      Case Error#
          Of 0
              If ESN_Temp <> ''
                  IF (AddToAudit(job:Ref_Number,'JOB','I.M.E.I. NUMBER CHANGED TO ' & Clip(job:esn),'PREVIOUS I.M.E.I. NUMBER: ' & Clip(esn_temp) & |
                                          '<13,10>NEW I.M.E.I. NUMBER: ' & Clip(job:esn)))
                  END ! IF
      
              End !If ESN_Temp <> ''
              ESN_Temp = job:ESN
              Post(Event:Accepted,?job:Model_Number)
              ! Inserting (DBH 14/07/2006) # 6972 - Ignore if cancelled pressed. Save the record anyway
              Access:JOBS.Update()
              Case Missive('The IMEI number has successfully been changed.'&|
                '|The job record has now been saved.','ServiceBase 3g',|
                             'midea.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              ! End (DBH 14/07/2006) #6972
      
          Of 1
              job:ESN = ESN_Temp
      End !Error#
      Do Titles
      End!If ~0{prop:acceptall}
      Select(?+1)
      Do CheckRequiredFields
    OF ?job:MSN
      If ~0{prop:acceptall}
          IF LEN(CLIP(job:MSN)) = 11 AND job:Manufacturer = 'ERICSSON'
            !Ericsson MSN!
            Job:MSN = SUB(job:MSN,2,10)
            UPDATE()
          END
          Do check_msn
          !do ValidateDateCode
      End!If ~0{prop:acceptall}
      
    OF ?job:Model_Number
      do_lookup# = 1
      If ~0{prop:acceptall}
          If job:model_number <> model_number_temp
              error# = 0
              If job:date_completed <> ''
                  Check_Access('AMEND COMPLETED JOBS',x")
                  If x" = False
                      Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  End
              End
              If job:invoice_Number <> '' and error# = 0
                  Check_Access('AMEND INVOICED JOBS',x")
                  If x" = False
                      Case Missive('Cannot change! THis job has been invoiced.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  End
              End
              If error# = 1
                  do_lookup# = 0
              End!If error# = 0
          End!If job:model_number <> model_number_temp
          If do_lookup# = 1
      
      IF job:Model_Number OR ?job:Model_Number{Prop:Req}
        mod:Model_Number = job:Model_Number
        !Save Lookup Field Incase Of error
        look:job:Model_Number        = job:Model_Number
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            job:Model_Number = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            job:Model_Number = look:job:Model_Number
            SELECT(?job:Model_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
              Error# = 0
      
              job:Model_Number    = IMEIModelRoutine(job:ESN,job:Model_Number)
              !routine returns a blank if errors occured
              if job:model_number = '' then error#=1. !if job:model_number was blanked
      
              if error# = 0 then
                  If job:Model_Number <> Model_Number_Temp
                      If Model_Number_Temp <> ''
                          Case Missive('You have selected to change the Model Number.'&|
                            '<13,10>Warning! The Pricing Structure may change causing the Job''s costs to change.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
                              Of 1 ! No Button
                                  error# = 1
                          End ! Case Missive
                      End !If Model_Number_Temp <> ''
                  End!If job:Model_Number <> Model_Number_Temp
              ENd !if error# = 0
      
              If Error# = 0
                  If (CheckPricing('C') = 1 Or CheckPricing('W') = 1)
                      Error# = 1
                  End !If (CheckPricing('C') = 1 Or CheckPricing('W') = 1)
              End !If Error# = 0
      
              If Error# = 0
      
                  Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                  mod:Model_Number    = job:Model_Number
                  If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      !Found
                      job:Manufacturer    = mod:Manufacturer
      
                      If ProductCodeRequired(job:Manufacturer) = Level:Benign
                          ?job:ProductCode{prop:Hide} = 0
                          ?job:ProductCode:Prompt{prop:Hide} = 0
                      Else !If ProductCodeRequired(job:Manufacturer) = Level:Benign
                          ?job:ProductCode{prop:Hide} = 1
                          ?job:ProductCode:Prompt{prop:Hide} = 1
                      End !If ProductCodeRequired(job:Manufacturer) = Level:Benign
      
                      If mod:Specify_Unit_Type = 'YES'
                          job:Unit_Type   = mod:Unit_Type
                      End !If mod:Specify_Unit_Type = 'YES'
      
                      !Only autofil the Product Code Fault Code if it's Nokia and they
                      !are not using the other Product Code field.
                      If ?job:ProductCode{prop:Hide} = 1
                          If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA'  Or job:manufacturer = 'MOTOROLA'
                              job:fault_code1 = mod:product_type
                          End!If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA' Or job:manufacturer = 'MOTOROLA'
                      Else !If ?job:ProductCode{prop:Hide} = 1
                          If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA'  Or job:manufacturer = 'MOTOROLA'
                              job:fault_code1 = job:ProductCode
                          End!If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA' Or job:manufacturer = 'MOTOROLA'
                      End !If ?job:ProductCode{prop:Hide} = 1
      
                      If mod:Product_Type <> ''
                          job:Fault_code1 = mod:Product_Type
                      End !If mod:Product_Type <> ''
      
                      If MSNRequired(job:Manufacturer) = Level:Benign
                          ?job:MSN{prop:Hide} = 0
                          ?job:MSN:Prompt{prop:Hide} = 0
                      Else !If MSNRequired(job:Manufacturer) = Level:Benign
                          ?job:MSN{prop:Hide} = 1
                          ?job:MSN{prop:Hide} = 1
                      End !If MSNRequired(job:Manufacturer) = Level:Benign
      
                      If CheckLength('IMEI',job:Model_Number,job:ESN)
                          job:ESN = ''
                      End !If CheckLength('IMEI',job:Model_Number,job:ESN)
      
                      If ?job:MSN{prop:Hide} = 0
                          If CheckLength('MSN',job:Model_Number,job:MSN)
                              job:MSN = ''
                          End !If CheckLength('MSN',job:Model_Number,job:MSN)
                      End !If ?job:MSN{prop:Hide} = 0
      
                      If Model_Number_Temp <> ''
                          If (CheckPricing('C') = Level:Benign And CheckPricing('W') = Level:Benign)
                              IF (AddToAudit(job:Ref_Number,'JOB','MODEL NUMBER CHANGED: ' & Clip(job:model_number),'NEW MODEL NUMBER: ' & Clip(job:model_number) & '<13,10>OLD MODEL NUMBER: ' & Clip(model_number_temp)))
                              END ! IF
                          Else!If (CheckPricing('C') = Level:Benign And CheckPricing('W') = Level:Benign)
                              IF (AddToAudit(job:Ref_Number,'JOB','REPRICE JOB: MODEL NUMBER CHANGED ' & Clip(job:model_number),'NEW MODEL NUMBER: ' & Clip(job:model_number) & '<13,10>OLD MODEL NUMBER: ' & Clip(model_number_temp)))
                              END ! IF
                          End!If (CheckPricing('C') = Level:Benign And CheckPricing('W') = Level:Benign)                    Access:AUDIT.Insert()
      
                          !Force price change if model changed - L945 (DBH: 08-09-2003)
                          Local.Pricing(1)
      
                          Model_Number_Temp   = job:Model_Number
      
                          !Autofil Colour
                          count# = 0
                          setcursor(cursor:wait)                                                          !Count how many colours have
                          save_moc_id = access:modelcol.savefile()                                        !been entered for this model.
                          access:modelcol.clearkey(moc:colour_key)                                        !If there is only one, USE IT
                          moc:model_number = job:model_number
                          set(moc:colour_key,moc:colour_key)
                          loop
                              if access:modelcol.next()
                                 break
                              end !if
                              if moc:model_number <> job:model_Number      |
                                  then break.  ! end if
                              count# += 1
                              If count# > 1
                                  Break
                              End!If count# > 1
                          end !loop
                          access:modelcol.restorefile(save_moc_id)
                          setcursor()
                          If count# = 1
                              job:colour  = moc:colour
                          End!If count# = 1
      
                      End !If Model_Number_Temp <> ''
                  Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
              End !If Error# = 0
              If Error# = 1
                  job:Model_Number    = Model_Number_Temp
              End !If Error# = 1
              Post(Event:Accepted,?job:DOP)
              Display()
      
          End!If do_lookup# = 1
      End !If ~0{prop:acceptall}
      Do OutFaultList
      Do CheckRequiredFields
    OF ?LookupModelNumber
      ThisWindow.Update
      mod:Model_Number = job:Model_Number
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          job:Model_Number = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?job:Model_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Model_Number)
    OF ?job:Unit_Type
      If ~0{prop:acceptall}
          error# = 0
      
          If job:unit_type <> unit_type_temp
              If unit_Type_temp = ''
                  Case Missive('Are you sure you want to change the Unit Type?'&|
                    '<13,10>'&|
                    '<13,10>Warning! The value of this job may change.','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          error# = 1
                  End ! Case Missive
              End!If unit_Type_temp = ''
          End!If job:unit_type <> unit_type_temp
          If error# = 0
              access:unittype.clearkey(uni:unit_type_key)
              uni:unit_type = job:unit_type
              if access:unittype.fetch(uni:unit_type_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browseunittype
                  if globalresponse = requestcompleted
                      job:unit_type = uni:unit_type
                      display()
                  end
                  globalrequest     = saverequest#
              end !if access:unittype.fetch(uni:unit_type_key)
      
              If CheckPricing('W') Or CheckPricing('C')
                  error# = 1
              End!If CheckPricing('W') Or CheckPricing('C')
      
          End!If error# = 0
          If error# = 1
              job:unit_type    = unit_type_temp
          Else
              unit_type_temp  = job:unit_type
              !Update pricing if unit type is changed - L945 (DBH: 08-09-2003)
              Local.Pricing(1)
          End!If error# = 1
      
      End
    OF ?tmp:SpecificNeeds
      !TB13287 - J - 05/01/15 always add an audit trail when ticked/unticked
      IF ~0{Prop:AcceptAll} THEN
          if tmp:SpecificNeeds = true then
              If AddToAudit(job:Ref_Number,'JOB','SND TICKBOX TICKED','JOB HAS NOW BEEN MARKED AS A SPECIFIC NEEDS JOB')
                  !error
              END
          ELSE
              If AddToAudit(job:Ref_Number,'JOB','SND TICKBOX UNTICKED','JOB HAS NOW BE MARKED AS A NORMAL (NOT A SPECIFIC NEEDS) JOB.')
                  !error
              END
          END
      end !IF ~window{Prop:AcceptAll} THEN
    OF ?accessory_button
      ThisWindow.Update
      If job:model_number = ''
          Case Missive('You must enter a Model Number first.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else
          glo:select1  = job:model_number
          glo:select2  = job:ref_number
      
          Browse_job_accessories
      
          glo:select1  = ''
          glo:select2  = ''
      
          Do Update_Accessories
      End
      
      
    OF ?job:DOP
      If ~0{prop:acceptall}
          If job:dop > Today()
              Case Missive('Invalid Date.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              job:DOP = ''
              Select(?job:DOP)
          Else !If job:dop > Today()
              !Only check DOP is it's a Warranty Job
      
              If job:warranty_job = 'YES'
                  access:manufact.clearkey(man:manufacturer_key)
                  man:manufacturer = job:manufacturer
                  if access:manufact.fetch(man:manufacturer_key) = Level:Benign
                      !*WORKAROUND*
                      IF job:Warranty_Charge_Type = 'WARRANTY (2ND YR)'
                        man:warranty_period = 730
                      END
                      If job:dop < (job:date_booked - man:warranty_period) and man:warranty_period <> 0 and job:dop <> ''
                          Case Missive('The unit is outside the Manufacturer''s Warranty Period.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          GetStatus(130,thiswindow.request,'JOB') !DOP query
                          DO time_remaining
                          brw21.resetsort(1)
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'D.O.P. OVER WARRANTY PERIOD BY ' & (job:date_booked - (job:dop + man:warranty_period)) & 'DAY(S)'
                              access:audit.insert()
                          end!�if access:audit.primerecord() = level:benign
                      Else
                          !If the job is AT 130 DOP QUERY and the job is now ok,
                          !I revert back to the previous status
                          If Sub(job:Current_Status,1,3) = '130'
                              If job:Engineer = ''
                                  GetStatus(305,0,'JOB') !Awaiting Allocation
                              Else !If job:Engineer = ''
                                  GetStatus(Sub(job:PreviousStatus,1,3),0,'JOB')
                              End !If job:Engineer = ''
      
                          End !If Sub(job:Current_Status,1,3) = '130'
                      End!If job:dop > man:warranty_period + Today()
                  end!if access:manufact.fetch(man:manufacturer_key) = Level:Benign
              End!If job:warranty_job = 'YES'
          End !If job:dop > Today()
      End!If ~0{prop:acceptall}
    OF ?job:Mobile_Number
      
       temp_string = Clip(left(job:Mobile_Number))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       job:Mobile_Number = temp_string
       Display(?job:Mobile_Number)
      ! Inserting (DBH 03/05/2006) #7149 - Check the mobile number format
      If ~0{prop:AcceptAll}
          If PassMobileNumberFormat(job:Mobile_Number,1) = False
              job:Mobile_Number = ''
              Select(?job:Mobile_Number)
              Cycle
          End ! If PassMobileNumberFormat(job:Mobile_Number,1) = False
      End ! If ~0{prop:AcceptAll}
      ! End (DBH 03/05/2006) #7149
    OF ?job:Turnaround_Time
      If ~0{prop:acceptall}
      access:turnarnd.clearkey(tur:turnaround_time_key)
      tur:turnaround_time = job:turnaround_time
      if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
          Turnaround_Routine(tur:days,tur:hours,end_date",end_time")
          job:turnaround_end_date = Clip(end_date")
          job:turnaround_end_time = Clip(end_time")
          Do Time_Remaining
      end!if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
      End!If ~0{prop:acceptall}
    OF ?job:Chargeable_Job
      IF ?job:Chargeable_Job{Prop:Checked} = True
        UNHIDE(?JOB:Charge_Type)
        UNHIDE(?LookupChargeType)
      END
      IF ?job:Chargeable_Job{Prop:Checked} = False
        HIDE(?JOB:Charge_Type)
        HIDE(?LookupChargeType)
      END
      ThisWindow.Reset
      Do show_hide_tabs
      Post(Event:Accepted,?job:Estimate)
      Do Estimate
      Do CheckRequiredFields
    OF ?job:Charge_Type
      If job:Chargeable_Job = 'YES'
      IF job:Charge_Type OR ?job:Charge_Type{Prop:Req}
        cha:Charge_Type = job:Charge_Type
        cha:Warranty = 'NO'
        GLO:Select1 = 'NO'
        !Save Lookup Field Incase Of error
        look:job:Charge_Type        = job:Charge_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            job:Charge_Type = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            job:Charge_Type = look:job:Charge_Type
            SELECT(?job:Charge_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
          If job:chargeable_job = 'YES'
      
              no_change# = 0
              If job:charge_type <> charge_type_temp
                  error# = 0
                  If job:date_completed <> '' and error# = 0
                      If SecurityCheck('AMEND COMPLETED JOBS')
                          Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          error# = 1
                      End
                  End
                  If job:invoice_Number <> '' and error# = 0
                      If SecurityCheck('AMEND INVOICED JOBS')
                          Case Missive('Cannot change! This job has been invoiced.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          error# = 1
                      End
                  End
                  If error# = 1
                      job:charge_type = charge_type_temp
                  Else!If job:date_completed <> ''
      
                      If charge_type_temp <> ''
                          Case Missive('Are you sure you want to change the Charge Type?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
                                  no_change# = 0
                              Of 1 ! No Button
                                  job:charge_type     = charge_type_temp
                                  no_change# = 1
                          End ! Case Missive
                      End
                      If no_change# = 0
                          access:chartype.clearkey(cha:warranty_key)
                          cha:charge_type = job:charge_type
                          cha:warranty    = 'NO'
                          if access:chartype.fetch(cha:warranty_key) = Level:Benign
                              If cha:allow_estimate <> 'YES' And job:estimate = 'YES'
                                  Case Missive('The selected Charge Type cannot be used for an Estimated job.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  job:charge_type = charge_type_temp
                                  no_change# = 1
                              End
                          End !if access:chartype.fetch(cha:charge_type_key) = Level:Benign
                      End
      
                      If error# = 0
                          IF CheckPricing('C') = 1
                              error# = 1
                          End!IF CheckPricing('C')
                      End!If error# = 0
      
                      If no_change# = 0
                          charge_type_temp    = job:charge_type
                      End
                  End!If job:date_completed <> ''
              End
              If job:chargeable_job   = 'YES'
                  access:chartype.clearkey(cha:charge_type_key)
                  cha:charge_type = job:charge_type
                  If access:chartype.fetch(cha:charge_type_key) = Level:Benign
                      If cha:allow_estimate = 'YES'
                          Unhide(?job:estimate)
                          If job:estimate <> 'YES' and cha:force_estimate = 'YES'
                              Case Missive('The selected Charge Type means that this job must be an Estimate.','ServiceBase 3g',|
                                             'mexclam.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              job:estimate = 'YES'
                              Post(Event:accepted,?job:estimate)
                          End!If job:estimate <> 'YES'
                      Else
                          Hide(?job:estimate)
                      End
                  end !If access:chartype.fetch(cha:charge_type_key) = Level:Benign
                  If job:Estimate = 'YES'
                      Post(Event:Accepted,?job:Estimate)
                  End !If job:Estimate = 'YES'
              End!If job:chargeable_job   = 'YES'
          End!If job:chargeable_job = 'YES'
          Access:MODELNUM.Clearkey(mod:Model_Number_Key)
          mod:Model_Number    = job:Model_Number
          If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
              !Found
              job:EDI = PendingJob(mod:Manufacturer)
              job:Manufacturer = mod:Manufacturer
          Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      
      End!If ~0{prop:acceptall}
      Do Show_Tabs
      Local.Pricing(0)
      End !If job:Chargeable_Job = 'YES'
    OF ?LookupChargeType
      ThisWindow.Update
      cha:Charge_Type = job:Charge_Type
      GLO:Select1 = 'NO'
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          job:Charge_Type = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?job:Charge_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Charge_Type)
    OF ?job:Warranty_Job
      IF ?job:Warranty_Job{Prop:Checked} = True
        UNHIDE(?job:charge_type:prompt)
        UNHIDE(?LookupWarrantyChargeType)
        UNHIDE(?job:Warranty_Charge_Type)
      END
      IF ?job:Warranty_Job{Prop:Checked} = False
        HIDE(?JOB:Warranty_Charge_Type)
        HIDE(?LookupWarrantyChargeType)
      END
      ThisWindow.Reset
      Do show_hide_tabs
      If job:warranty_job = 'NO'
          job:edi = 'XXX'
      End!If job:warranty_job = 'NO'
      Post(event:accepted,?job:warranty_charge_type)
      Do CheckRequiredFields
      do ValidateDateCode
    OF ?job:Warranty_Charge_Type
      If job:Warranty_Job = 'YES'
      IF job:Warranty_Charge_Type OR ?job:Warranty_Charge_Type{Prop:Req}
        cha:Charge_Type = job:Warranty_Charge_Type
        cha:Warranty = 'YES'
        GLO:Select1 = 'YES'
        !Save Lookup Field Incase Of error
        look:job:Warranty_Charge_Type        = job:Warranty_Charge_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            job:Warranty_Charge_Type = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            job:Warranty_Charge_Type = look:job:Warranty_Charge_Type
            SELECT(?job:Warranty_Charge_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
          If job:warranty_job = 'YES'
              Error# = 0
              If job:Warranty_Charge_Type <> Warranty_Charge_Type_Temp
                  If job:Date_Completed <> '' And Error# = 0
                      If SecurityCheck('AMEND COMPLETED JOBS')
                          Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          Error# = 1
                      End !If SecurityCheck('AMEND INVOICED JOBS')
                  End !If job:Date_Completed <> ''
      
                  If job:Invoice_Number_Warranty <> '' And Error# = 0
                      If SecurityCheck('AMEND INVOICED JOBS')
                          Case Missive('Cannot change! This job has been invoiced.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          Error# = 1
                      End !If SecurityCheck('AMEND INVOICED JOBS')
                  End !If job:Invoice_Number_Warranty <> '' And Error# = 0
      
                  If Error# = 0
                      If CheckPricing('W') = 1
                          Error# = 1
                      End !If CheckPricing('W')
                  End !If Error# = 0
      
                  If Warranty_Charge_Type_Temp <> '' And Error# = 0
                      Case Missive('Are you sure you want to change the Charge Type?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                          Of 1 ! No Button
                              Error# = 0
                      End ! Case Missive
                  End !If Warranty_Charge_Type <> ''
                  If Error# = 0
                      Warranty_Charge_Type_Temp   = job:Warranty_Charge_Type
                  Else !If Error# = 0
                      job:Warranty_Charge_Type    = Warranty_Charge_Type_Temp
                  End !If Error# = 0
              End !If job:Warranty_Charge_Type <> Warranty_Charge_Type_Temp
              Display()
          End!If job:warranty_job = 'YES'
          Access:MODELNUM.Clearkey(mod:Model_Number_Key)
          mod:Model_Number    = job:Model_Number
          If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
              !Found
              job:EDI = PendingJob(mod:Manufacturer)
              job:Manufacturer = mod:Manufacturer
          Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      
      
      End!If ~0{prop:acceptall}
      Do Show_Tabs
      Local.Pricing(0)
      End !If job:Warranty_Job = 'YES'
    OF ?LookupWarrantyChargeType
      ThisWindow.Update
      cha:Charge_Type = job:Warranty_Charge_Type
      GLO:Select1 = 'YES'
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          job:Warranty_Charge_Type = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?job:Warranty_Charge_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Warranty_Charge_Type)
    OF ?job:Estimate
      If ~0{prop:acceptall}
          If job:estimate <> estimate_temp and job:Chargeable_Job = 'YES'
              If job:estimate = 'NO'
                  reset_status# = 1
                  If job:estimate_accepted = 'YES'
                      Case Missive('Warning! The estimate on this job has been accepted.'&|
                        '<13,10>'&|
                        '<13,10>Do you want to reset this estimate?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              estimate_temp   = job:estimate
                          Of 1 ! No Button
                              job:estimate    = estimate_temp
                              reset_status# = 0
                      End ! Case Missive
                  End
                  If reset_status# = 1
                      setcursor(cursor:wait)
                      save_aud_id = access:audit.savefile()
                      access:audit.clearkey(aud:ref_number_key)
                      aud:ref_number = job:ref_number
                      set(aud:ref_number_key,aud:ref_number_key)
                      loop
                          if access:audit.next()
                             break
                          end !if
                          if aud:ref_number <> job:ref_number  |
                              then break.  ! end if
      
                          If Instring('STATUS CHANGE',aud:action,1,1) And Instring('505',aud:action,1,1)
                              Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                              aud2:AUDRecordNumber = aud:Record_Number
                              IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                                      GetStatus(Clip(Sub(aud2:notes,18,3)),thiswindow.request,'JOB')
      
                                      Do time_remaining
                                      brw21.resetsort(1)
                                      Break
                              END ! IF
                          End!If Instring('STATUS CHANGE',aud:action,1,1) And Instring('505',aud:action,1,1)
                          
                      end !loop
                      access:audit.restorefile(save_aud_id)
                      setcursor()
                  End!If reset_status# = 1
              Else!If job:estimate = 'NO'
                  GetStatus(505,1,'JOB')
                  Brw21.resetsort(1)
                  Do time_remaining
              End!If job:estimate = 'NO'
          End
          Do Estimate
      End!If ~0{prop:acceptall}
      Do Show_Tabs
      Do show_hide_tabs
    OF ?job:Estimate_If_Over
      Do Estimate_Check
    OF ?job:Estimate_Accepted
      IF ?job:Estimate_Accepted{Prop:Checked} = True
        job:Estimate_Rejected = 'NO'
      END
      ThisWindow.Reset
      If ~0{prop:acceptall}
      If job:estimate_accepted <> estimate_accepted_temp
          If job:estimate_accepted = 'NO'
              check_access('RELEASE - ESTIMATE ACCEPTED',x")
              If x" = False
                  Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  job:estimate_accepted   = estimate_accepted_temp
              Else !If x" = False
                  estimate_accepted_temp  = job:estimate_accepted
              End !If x" = False
          Else !If job:in_repair = 'NO'
              error# = 0
              If job:estimate_ready <> 'YES'
                  error# = 1
                  Case Missive('The Estimate has not been marked as "Ready".','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  job:estimate_accepted = estimate_accepted_temp
              Else!If job:estimate_ready <> 'YES'
                  estimate_accepted_temp  = job:estimate_accepted
                  GetStatus(535,thiswindow.request,'JOB') !estimate accepted
                  Brw21.resetsort(1)
                  Do time_remaining
      
                  glo:select1  = 'ACCEPTED'
                  glo:select2  = ''
                  glo:select3  = ''
                  glo:select4  = ''
                  Estimate_Reason
                  glo:select1  = ''
      
                  IF (AddToAudit(job:Ref_Number,'JOB','ESTIMATE ACCEPTED FROM: ' & Clip(glo:select2),'COMMUNICATION METHOD: ' & Clip(glo:select3)))
                  END ! IF
      
                  glo:select2 = ''
                  glo:select3 = ''
                  glo:select4 = ''
                  job:COurier_Cost = job:Courier_Cost_Estimate
      
                  setcursor(cursor:wait)
      
                  save_epr_id = access:estparts.savefile()
                  access:estparts.clearkey(epr:part_number_key)
                  epr:ref_number  = job:ref_number
                  set(epr:part_number_key,epr:part_number_key)
                  loop
                      if access:estparts.next()
                         break
                      end !if
                      if epr:ref_number  <> job:ref_number      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
      
                      get(parts,0)
                      glo:select1 = ''
                      if access:parts.primerecord() = level:benign
                          par:ref_number           = job:ref_number
                          par:adjustment           = epr:adjustment
                          par:part_number     = epr:part_number
                          par:description     = epr:description
                          par:supplier        = epr:supplier
                          par:purchase_cost   = epr:purchase_cost
                          par:sale_cost       = epr:sale_cost
                          par:retail_cost     = epr:retail_cost
                          par:quantity             = epr:quantity
                          par:exclude_from_order   = epr:exclude_from_order
                          par:part_ref_number      = epr:part_ref_number
                          If par:exclude_from_order <> 'YES' and epr:UsedOnRepair <> 1
                             If par:part_ref_number <> ''
                                  access:stock.clearkey(sto:ref_number_key)
                                  sto:ref_number = par:part_ref_number
                                  if access:stock.fetch(sto:ref_number_key)
                                      Case Missive('The original Stock Part for this part: ' & Clip(par:Part_Number) & ' cannot be found.'&|
                                        '<13,10>It will therefore be inserted as an External Order.','ServiceBase 3g',|
                                                     'mexclam.jpg','/OK')
                                          Of 1 ! OK Button
                                      End ! Case Missive
                                      Get(ordpend,0)
                                      If access:ordpend.primerecord() = Level:Benign
                                          ope:part_ref_number = ''
                                          ope:job_number  = job:ref_number
                                          ope:part_type   = 'JOB'
                                          ope:supplier    = par:supplier
                                          ope:part_number = par:part_number
                                          ope:description = par:description
                                          ope:quantity    = par:quantity
                                          access:ordpend.insert()
                                          par:pending_ref_number  = ope:ref_number
                                          access:parts.insert()
                                      End!If access:ordpend.primerecord() = Today()
                                  Else!if access:stock.fetch(sto:ref_number_key)
                                      If sto:sundry_item <> 'YES'
                                         If par:quantity > sto:quantity_stock
                                              get(ordpend,0)
                                              if access:ordpend.primerecord() = level:benign
                                                  ope:part_ref_number = sto:ref_number
                                                  ope:job_number      = job:ref_number
                                                  ope:part_type       = 'JOB'
                                                  ope:supplier        = par:supplier
                                                  ope:part_number     = par:part_number
                                                  ope:description     = par:description
                                                  If sto:quantity_stock = 0
                                                      ope:quantity    = par:quantity
                                                      par:pending_ref_number = ope:ref_number
                                                      access:parts.insert()
                                                  Else!If sto:quantity_stock <= 0
                                                      ope:quantity    = par:quantity - sto:quantity_stock
                                                      par:quantity    = sto:quantity_stock
                                                      par:date_ordered    = Today()
                                                      access:parts.insert()
                                                      sto:quantity_stock  = 0
                                                      access:stock.update()
      
                                                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                         'DEC', | ! Transaction_Type
                                                                         par:Despatch_Note_Number, | ! Depatch_Note_Number
                                                                         job:Ref_Number, | ! Job_Number
                                                                         0, | ! Sales_Number
                                                                         par:Quantity, | ! Quantity
                                                                         par:Purchase_Cost, | ! Purchase_Cost
                                                                         par:Sale_Cost, | ! Sale_Cost
                                                                         par:Retail_Cost, | ! Retail_Cost
                                                                         'STOCK DECREMENTED', | ! Notes
                                                                         '') ! Information
                                                        ! Added OK
      
                                                      Else ! AddToStockHistory
                                                        ! Error
                                                      End ! AddToStockHistory
      
                                                      Get(parts,0)
                                                      If access:parts.primerecord() = Level:Benign
                                                          par:ref_number  = job:ref_number
                                                          par:adjustment  = epr:adjustment
                                                          par:part_ref_number = sto:ref_number
                                                          par:part_number     = epr:part_number
                                                          par:description     = epr:description
                                                          par:supplier        = epr:supplier
                                                          par:purchase_cost   = epr:purchase_cost
                                                          par:sale_cost       = epr:sale_cost
                                                          par:retail_cost     = epr:retail_cost
                                                          par:quantity    = ope:quantity
                                                          par:pending_ref_number  = ope:ref_number
                                                          access:parts.insert()
                                                      End!If access:parts.primerecord() = Level:Benign
                                                  End!If sto:quantity_stock = 0
                                                  access:ordpend.insert()
                                              end!if access:ordpend.primerecord() = level:benign
      
                                          Else !If par:quantity > sto:quantity_stock
                                              sto:quantity_stock -= par:quantity
                                              If sto:quantity_stock < 0
                                                  sto:quantity_stock = 0
                                              End
                                              Access:stock.update()
                                              par:date_ordered    = Today()
                                              access:parts.insert()
                                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                 'DEC', | ! Transaction_Type
                                                                 par:Despatch_Note_Number, | ! Depatch_Note_Number
                                                                 job:Ref_Number, | ! Job_Number
                                                                 0, | ! Sales_Number
                                                                 par:Quantity, | ! Quantity
                                                                 par:Purchase_Cost, | ! Purchase_Cost
                                                                 par:Sale_Cost, | ! Sale_Cost
                                                                 par:Retail_Cost, | ! Retail_Cost
                                                                 'STOCK DECREMENTED', | ! Notes
                                                                 '') ! Information
                                                ! Added OK
      
                                              Else ! AddToStockHistory
                                                ! Error
                                              End ! AddToStockHistory
                                          End !If par:quantity > sto:quantity_stock
                                      Else!If sto:sundry_item <> 'YES'
                                          par:date_ordered = Today()
                                          access:parts.insert()
                                      End!If sto:sundry_item <> 'YES'
                                  end!if access:stock.fetch(sto:ref_number_key)
                              Else !If par:part_ref_number <> ''
                                  get(ordpend,0)
                                  if access:ordpend.primerecord()= level:benign
                                      ope:part_ref_number = ''
                                      ope:job_number      = job:ref_number
                                      ope:part_type       = 'JOB'
                                      ope:supplier        = par:supplier
                                      ope:part_number     = par:part_number
                                      ope:description     = par:description
                                      ope:quantity        = par:quantity
                                      access:ordpend.insert()
                                      par:pending_ref_number = ope:ref_number
                                      access:parts.insert()
                                  end!if access:ordpend.primerecord()= level:benign
                              End !If par:part_ref_number <> ''
                          Else!If par:exclude_from_order <> 'YES'
                              par:date_ordered    = 'YES'
                              access:parts.insert()
                          End!If par:exclude_from_order <> 'YES'
                      end!if access:parts.primerecord() = level:benign
                  end !loop
                  access:estparts.restorefile(save_epr_id)
                  setcursor()
              End!If job:estimate_ready <> 'YES'
          End !If job:in_repair = 'NO'
          Local.Pricing(0)
          Do show_hide_tabs
          Do check_parts
      End !If job:in_repair <> in_repair_temp
      
      End !If ~0{prop:acceptall}
    OF ?job:Estimate_Rejected
      IF ?job:Estimate_Rejected{Prop:Checked} = True
        job:Estimate_Accepted = 'NO'
      END
      ThisWindow.Reset
      If ~0{prop:acceptall}
      If job:estimate_rejected <> estimate_rejected_temp
          If job:estimate_rejected = 'NO'
              check_access('RELEASE - ESTIMATE REJECTED',x")
              If x" = False
                  Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  job:estimate_rejected   = estimate_rejected_temp
              Else !If x" = False
                  estimate_rejected_temp  = job:estimate_rejected
              End !If x" = False
          Else !If job:in_repair = 'NO'
              estimate_rejected_temp  = job:estimate_rejected
              GetStatus(540,thiswindow.request,'JOB') !estimate refused
              Brw21.resetsort(1)
              Do time_remaining
      
              glo:select1  = 'REJECTED'
              glo:select2  = ''
              glo:select3  = ''
              glo:select4  = ''
              Estimate_Reason
              glo:select1  = ''
      
              IF (AddToAudit(job:Ref_Number,'JOB','ESTIMATE REJECTED FROM: ' & Clip(glo:select2),'COMMUNICATION METHOD: ' & Clip(glo:select3) & |
                               '<13,10>REASON: ' & Clip(glo:select4)))
              END ! IF
      
      
              glo:select2 = ''
              glo:select3 = ''
              glo:select4 = ''
      
      
          End !If job:in_repair = 'NO'
      End !If job:in_repair <> in_repair_temp
      End !If ~0{prop:acceptall}
    OF ?LookupNetwork
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickNetworks
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tmp:Network = net:Network
          Of Requestcancelled
      End!Case Globalreponse
      Display(?tmp:Network)
    OF ?LookupSIMNumber
      ThisWindow.Update
      UpdateSIMNumber(job:Ref_Number)
      ThisWindow.Reset
      access:JOBSE.clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
          Display(?jobe:SIMNumber)
      Else! If access:JOBSE.tryfetch(jobe:RefNumberKey.) = Level:Benign
          !Error
          !Assert(0,'Fetch Error')
      End! If access:JOBSE.tryfetch(jobe:RefNumberKey.) = Level:Benign
    OF ?History:2
      ThisWindow.Update
      ! Changing (DBH 01/03/2007) # 8820 - Call the same "History" screen as the engineering screen
      !glo:Select1 = 'ESN'
      !glo:select2 = job:esn
      !glo:select12 = 'VIEW ONLY'
      !glo:select11 = job:ref_number
      !Auto_Search_Window
      !glo:Select1 = ''
      !glo:Select2 = ''
      !glo:Select12 = ''
      !glo:Select11 = ''
      !
      ! to (DBH 01/03/2007) # 8820
      If GetTempPathA(255,TempFilePath)
          If Sub(TempFilePath,-1,1) = '\'
              glo:FileName = Clip(TempFilePath) & 'JOBBOUNCER' & Clock() & '.TMP'
          Else !If Sub(TempFilePath,-1,1) = '\'
              glo:FileName = Clip(TempFilePath) & '\JOBBOUNCER' & Clock() & '.TMP'
          End !If Sub(TempFilePath,-1,1) = '\'
      End
      x# = BrowseJobBouncers('V')
      Remove(glo:FileName)
      ! End (DBH 01/03/2007) #8820
    OF ?show_booking_temp
      If show_booking_temp = 'YES'
          Hide(?booking2_tab)
          Select(?Sheet4,1)
          Unhide(?booking1_tab)
      End
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?accessory_button:2
      ThisWindow.Update
      If job:model_number = ''
          Case Missive('You must enter a Model Number first.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else
          glo:select1  = job:model_number
          glo:select2  = job:ref_number
      
          Browse_job_accessories
      
          glo:select1  = ''
          glo:select2  = ''
      
          Do Update_Accessories
      End
      
      
    OF ?ConsignmentHistory
      ThisWindow.Update
      BrowseConsignmentHistory(job:Ref_Number)
      ThisWindow.Reset
    OF ?ThirdPartyButton
      ThisWindow.Update
      BrowseJobThird
      ThisWindow.Reset
    OF ?ViewCosts
      ThisWindow.Update
      ViewCosts
      ThisWindow.Reset
      ! Inserting (DBH 08/08/2006) # 8084 - Update the costs
      Do GetJOBSE
      ! End (DBH 08/08/2006) #8084
      
    OF ?Audit_Trail_Button
      ThisWindow.Update
      Browse_Audit
      ThisWindow.Reset
      glo:select12 = ''
    OF ?Contact_History:2
      ThisWindow.Update
      BrowseStatusChanges(job:Ref_Number)
      ThisWindow.Reset
    OF ?Contact_History:3
      ThisWindow.Update
      BrowseLocationChanges(job:Ref_Number)
      ThisWindow.Reset
    OF ?Button:ContactHistoryEmpty
      ThisWindow.Update
      Browse_Contact_History
      ThisWindow.Reset
      glo:select12 = ''
      ! Inserting (DBH 12/10/2006) # 8020 - Highlight if contact history has been entered
      Do CheckContactHistory
      ! End (DBH 12/10/2006) #8020
      
    OF ?Fault_Description_Text_Button
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Fault_Description
      ThisWindow.Reset
      glo:select1 = ''
      Do AreThereNotes
      
      
      
    OF ?ExchangeButton
      ThisWindow.Update
      ViewExchangeUnit
      ThisWindow.Reset
      Do Titles
      Do GETJOBSE
    OF ?LoanButton
      ThisWindow.Update
      ViewLoanUnit
      ThisWindow.Reset
      Do Titles
      DO GETJOBSE
      
      !Paul 14/07/2009 Log No 10920
      !has the loan unit number changed and is this screen locked???
      If tmp:LoanUnitNumber <> job:Loan_Unit_Number then
          If ?OK{prop:Hide} = True OR ?OK{prop:disable} = true
              ?Cancel{prop:Hide} = True
              ?Close{prop:Hide} = False
          End ! ?OK{prop:Hide} = True
      End !If tmp:LoanUnitNumber <> job:Loan_Unit_Number then
      
      display()
    OF ?AmendAddresses
      ThisWindow.Update
      Show_Addresses
      ThisWindow.Reset
      Do GETJOBSE
    OF ?Validate_Serial_Number
      ThisWindow.Update
      Serial_Number_Validation(serial_number")
      If serial_number" <> ''
          !Find what the serial number entered is equal to:
          found# = 1
          If job:esn <> serial_number"
              If job:msn <> serial_number"
                  found# = 0
              end!if access:jobs.fetch(job:msn_key)
              IF found# = 0
                  access:exchange.clearkey(xch:ref_number_key)
                  xch:ref_number = job:exchange_unit_number
                  if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                      If xch:esn = serial_number" Or xch:msn = serial_number"
                          found# = 2
                      End!If xch:esn = serial_number_temp Or xch:msn = serial_number_temp
                  end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
              End!IF found# = 0
          end!if access:jobs.fetch(job:esn_key)
          !If serial number doesn't match anything about the job: found = 0
          !If serial number DOES match ESN, or MSN of the job: found = 1
          !If serial number matches the exchange unit ESN: found = 2
          Case found#
              Of 0
                  Case Missive('Warning! Serial Number Mismatch.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  IF (AddToAudit(job:Ref_Number,'JOB','SERIAL NUMBER MISMATCHED','SERIAL NUMBER ENTERED: ' & Clip(serial_number")))
                  END ! IF
      
              Of 2
                  Case Missive('The Serial Number entered matched the Exchange Unit''s Serial Number on this job.'&|
                    '<13,10>'&|
                    '<13,10>Please bring this to the attention of you System Supervisor.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  IF (AddToAudit(job:Ref_Number,'JOB','SERIAL NUMBER VALIDATION ERROR','THE SERIAL NUMBER VALIDATED MATCHED THE EXCHANGE UNIT SERIAL NUMBER. ' & |
                                          '<13,10>SERIAL NUMBER ENTERED: ' & Clip(serial_number")))
                  END ! IF
      
      
          End!Case found#
      
      End!If serial_number" <> ''
    OF ?Print_Label
      ThisWindow.Update
      Case Missive('A Job Label will be printed when you have finished amending this job.','ServiceBase 3g',|
                     'mexclam.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      print_label_temp = 'YES'
    OF ?Button:ContactHistoryFilled
      ThisWindow.Update
      Browse_Contact_History
      ThisWindow.Reset
      glo:Select12 = ''
      ! Inserting (DBH 12/10/2006) # 8020 - Highlight if contact history has been entered
      Do CheckContactHistory
      ! End (DBH 12/10/2006) #8020
    OF ?ValidatePOP
      ThisWindow.Update
      If SecurityCheck('JOBS - AMEND POP DETAILS')
          Case Missive('You do not have sufficient access to amend this details.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
          !Validate Proof OF Purchase
          Glo:Select1 = job:pop
          Glo:Select2 = job:dop
      
          Error# = 0
          ! Check access level to determin if password screen should appear - TrkBs: 6685 (DBH: 11-11-2005)
          If SecurityCheck('VALIDATE POP WITHOUT PASSWORD')
              Access:USERS.Clearkey(use:Password_Key)
              use:Password    = glo:Password
              If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  ! Found
                  UserCode" = use:User_Code
                  If InsertEngineerPassword() <> UserCode"
                      Case Missive('Incorrect Password.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Error# = 1
                  End ! If InsertEngineerPassword() <> UserCode"
              Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  ! Error
              End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          End ! If Security_Check('VALIDATE POP WITHOUT PASSWORD')
      
          If Error# = 0
              Do Validate_Motorola
          End ! If Error# = 0
      
          Glo:Select1 = ''
          Glo:Select2 = ''
      END
    OF ?Button49
      ThisWindow.Update
      UpdateJOBS_Additional
      ThisWindow.Reset
      DO GETJOBSE
    OF ?Fault_Codes_Lookup
      ThisWindow.Update
      access:jobnotes.clearkey(jbn:refNumberKey)
      jbn:RefNumber   = job:ref_number
      access:jobnotes.tryfetch(jbn:refnumberkey)
      If job:Chargeable_Job = 'YES'
          JobType"    = 'C'
          ChargeType"   = job:Charge_Type
          RepairType"   = job:Repair_Type
      End !If job:Chargeable_Job = 'YES'
      If job:Warranty_Job = 'YES'
          JobType"    = 'W'
          ChargeType"   = job:Warranty_Charge_Type
          RepairType"   = job:Repair_Type_Warranty
      End !If job:Warranty_Job = 'YES'
      Access:WEBJOB.Clearkey(wob:RefNumberKey)
      wob:RefNumber   = job:Ref_Number
      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          ! Found
          ! Have to reget the webjob record, but this means that changes will not be cancelled - TrkBs: 5904 (DBH: 23-09-2005)
      
      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          ! Error
      End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      Clear(glo:FaultCodeGroup)
      glo:FaultCode1 = job:Fault_Code1
      glo:FaultCode2 = job:Fault_Code2
      glo:FaultCode3 = job:Fault_Code3
      glo:FaultCode4 = job:Fault_Code4
      glo:FaultCode5 = job:Fault_Code5
      glo:FaultCode6 = job:Fault_Code6
      glo:FaultCode7 = job:Fault_Code7
      glo:FaultCode8 = job:Fault_Code8
      glo:FaultCode9 = job:Fault_Code9
      glo:FaultCode10 = job:Fault_Code10
      glo:FaultCode11 = job:Fault_Code11
      glo:FaultCode12 = job:Fault_Code12
      glo:FaultCode13 = wob:FaultCode13
      glo:FaultCode14 = wob:FaultCode14
      glo:FaultCode15 = wob:FaultCode15
      glo:FaultCode16 = wob:FaultCode16
      glo:FaultCode17 = wob:FaultCode17
      glo:FaultCode18 = wob:FaultCode18
      glo:FaultCode19 = wob:FaultCode19
      glo:FaultCode20 = wob:FaultCode20
      GenericFaultCodes(job:Ref_Number,JobType",ThisWindow.Request,job:account_Number,job:Manufacturer,|
                          ChargeType",RepairType",job:Date_Completed)
      job:Fault_Code1   = glo:FaultCode1
      job:Fault_Code2   = glo:FaultCode2
      job:Fault_Code3   = glo:FaultCode3
      job:Fault_Code4   = glo:FaultCode4
      job:Fault_Code5   = glo:FaultCode5
      job:Fault_Code6   = glo:FaultCode6
      job:Fault_Code7   = glo:FaultCode7
      job:Fault_Code8   = glo:FaultCode8
      job:Fault_Code9   = glo:FaultCode9
      job:Fault_Code10  = glo:FaultCode10
      job:Fault_Code11  = glo:FaultCode11
      job:Fault_Code12  = glo:FaultCode12
      wob:FaultCode13   = glo:FaultCode13
      wob:FaultCode14   = glo:FaultCode14
      wob:FaultCode15   = glo:FaultCode15
      wob:FaultCode16   = glo:FaultCode16
      wob:FaultCode17   = glo:FaultCode17
      wob:FaultCode18   = glo:FaultCode18
      wob:FaultCode19   = glo:FaultCode19
      wob:FaultCode20   = glo:FaultCode20
      Clear(glo:FaultCodeGroup)
      Access:WEBJOB.Update()
      do ValidateDateCode
      Do AreThereNotes
    OF ?tmp:VSACustomer
      ! If the user ticks VSA.. send the CID Messages - TrkBs: 6141 (DBH: 26-08-2005)
      If ~0{prop:AcceptAll}
          IF tmp:VSACustomer = 1
              Case Missive('Are you sure you want to make this customer a "VSA Customer"?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      If CheckLength('MOBILE','',job:Mobile_Number)
                          tmp:VSACustomer = 0
                      Else
                          CID_XML(job:Mobile_Number,wob:HeadAccountNumber,1)
                      End ! If CheckLength('MOBILE',,job:MobileNumber)
                  Of 1 ! No Button
                  tmp:VSACUstomer = 0
              End ! Case Missive
          End ! IF tmp:VSACustomer = 0
      End ! If ~0{prop:AcceptAll}
      Display()
      ! End   - If the user ticks VSA.. send the CID Messages - TrkBs: 6141 (DBH: 26-08-2005)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Compulsory Field Check    New Booking Audit Trail
  !Right Into JOBSE
  Do SetJOBSE
  sav:ref_number    = job:Ref_number
  If ThisWindow.request   = Insertrecord
      Set(defaults)
      access:defaults.next()
      error# = 0
  
      If job:Date_Completed <> ''
          CompulsoryFieldCheck('C')
      Else!If job:Date_Completed <> ''
          CompulsoryFieldCheck('B')
      End!If job:Date_Completed <> ''
  
      If glo:ErrorText <> ''
          glo:errortext = 'You cannot complete booking due to the following error(s): <13,10>' & Clip(glo:errortext)
          Error_Text
          glo:errortext = ''
          Cycle
      Else!If error# = 1
          locAuditNotes = 'UNIT DETAILS: ' & Clip(job:manufacturer) & ' ' & Clip(job:model_number) & ' - ' & Clip(job:unit_type) & |
                                  '<13,10>I.M.E.I. NO.: ' & Clip(job:esn) & |
                                  '<13,10>M.S.N.: ' & Clip(job:msn)
          If job:chargeable_job = 'YES'
              locAuditNotes   = Clip(locAuditNotes) &  '<13,10,13,10>CHARGEABLE JOB: ' & Clip(job:chargeable_job) & |
                                              '<13,10>CHARGE TYPE: ' & Clip(job:charge_type)
              If job:repair_type <> ''
                  locAuditNotes = Clip(locAuditNotes) & '<13,10>REPAIR TYPE: ' & Clip(job:repair_type)
              End!If job:charge_type <> ''
          End!If job:chargeable_job = 'YES'
          If job:warranty_job = 'YES'
              locAuditNotes   = CLip(locAuditNotes) & '<13,10>WARRANTY JOB: ' & CLip(job:warranty_job) & |
                              '<13,10>CHARGE TYPE: ' & Clip(job:warranty_charge_type)
              If job:repair_type_warranty <> ''
                  locAuditNotes = Clip(locAuditNotes) & '<13,10>REPAIR TYPE: ' & Clip(job:repair_type_warranty)
              End!If job:repair_type_warranty <> ''
          End!If job:warranty_job = 'YES'
  
          If job:estimate = 'YES'
              locAuditNotes   = Clip(locAuditNotes) & '<13,10>ESTIMATE: ' & Clip(job:estimate) & |
                                              'VALUE: ' & Clip(job:estimate_if_over)
          End!If job:estimate = 'YES'
  
          If ExchangeAccount(job:Account_Number)
              locAuditNotes   = Clip(locAuditNotes) & '<13,10,13,10>EXCHANGE REPAIR'
          End !If ExchangeAccount(job:Account_Number)
  
          count# = 0
          save_jac_id = access:jobacc.savefile()
          access:jobacc.clearkey(jac:ref_number_key)
          jac:ref_number = job:ref_number
          set(jac:ref_number_key,jac:ref_number_key)
          loop
              if access:jobacc.next()
                 break
              end !if
              if jac:ref_number <> job:ref_number      |
                  then break.  ! end if
              count# += 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10>Accessory ' & Clip(count#) & ': ' & Clip(jac:accessory)
          end !loop
          access:jobacc.restorefile(save_jac_id)
  
  
          IF (AddToAudit(job:Ref_Number,'JOB','NEW JOB BOOKING INITIAL ENTRY',locAuditNotes))
          END ! IF
  
  
          !If this is a new job and it is for an Exchange Account, mark that the unit
          !assuming that it can be found in Exchange Database, that it is 'In Repair'
          If ExchangeAccount(job:Account_Number) And job:ESN <> ''
              Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
              xch:ESN = job:ESN
              If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                  !Found
                  xch:Available = 'REP'
                  xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                  xch:Job_Number  = job:Ref_Number
                  If Access:EXCHANGE.TryUpdate() = Level:Benign
                      get(exchhist,0)
                      if access:exchhist.primerecord() = Level:Benign
                          exh:ref_number      = xch:ref_number
                          exh:date            = Today()
                          exh:time            = Clock()
                          access:users.clearkey(use:password_key)
                          use:password        = glo:password
                          access:users.fetch(use:password_key)
                          exh:user = use:user_code
                          exh:status          = 'EXCHANGE UNIT IN REPAIR ON JOB NO: ' & Clip(job:Ref_Number)
                          access:exchhist.insert()
                      end!if access:exchhist.primerecord() = Level:Benign
                  End !If Access:EXCHANGE.TryUpdate()
  
              Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
          End !If ExchangeAccount(job:Account_Number)
  
      End!If error# = 1
  Else
      CompulsoryFieldCheck('B')
      If glo:ErrorText <> ''
          glo:errortext = 'You cannot complete editing this job because of the following errors: <13,10>' & Clip(glo:errortext)
          Error_Text
          glo:errortext = ''
          Cycle
      End!If glo:ErrorText <> ''
  End !If ThisWindow.request   = Insertrecord
  
    !TB13287 - J - 06/01/15 - is there a specific needs tick?
    Access:jobse3.clearkey(jobe3:KeyRefNumber)
    jobe3:RefNumber = Job:Ref_number
    if access:jobse3.fetch(jobe3:KeyRefNumber)
      !no record - should there be one?
      if tmp:SpecificNeeds = 1 then
          Access:jobse3.primerecord()
          jobe3:RefNumber = Job:Ref_number
          jobe3:SpecificNeeds = tmp:SpecificNeeds
          Access:jobse3.update()
      END !if box has been ticked
    ELSE
      jobe3:SpecificNeeds = tmp:SpecificNeeds
      Access:jobse3.update()
    END
  !Invoicing
  Set(defaults)
  access:Defaults.next()
  IF def:use_sage <> 'YES'
      Check_Access('AMEND INVOICED JOBS',x")
      If x" = True
          If job:invoice_number <> '' And job:chargeable_job = 'YES'
              If job:invoice_labour_cost <> job:labour_cost Or job:invoice_parts_cost <> job:parts_cost Or |
                  job:invoice_courier_cost <> job:courier_cost
                  Case Missive('Warning! The Chargeable Part of this job has been invoiced and you have changed the Chargeable Costs. If you continue Invoice Number ' & Clip(inv:Invoice_Number) & ' will be changed to these new values.'&|
                    '<13,10>Are you sure?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          IF (AddToAudit(job:Ref_Number,'JOB','CHARGEABLE INVOICED JOB AMENDED','NEW COSTS:-' & '<13,10>LABOUR: ' & Format(job:labour_cost,@n14.2) & |
                                                  '<13,10>PARTS: ' & Format(job:parts_cost,@n14.2) & |
                                                  '<13,10>COURIER: ' & Format(job:courier_cost,@n14.2) & |
                                                  '<13,10><13,10>PREVIOUS COSTS:-' & '<13,10>LABOUR: ' & Format(job:invoice_labour_cost,@n14.2) & |
                                                  '<13,10>PARTS: ' & Format(job:invoice_parts_cost,@n14.2) & |
                                                  '<13,10>COURIER: ' & Format(job:invoice_courier_cost,@n14.2)))
                          END ! IF
  
  
                          job:invoice_labour_cost = job:labour_cost
                          job:invoice_parts_cost  = job:parts_cost
                          job:invoice_courier_cost = job:courier_cost
                          access:invoice.clearkey(inv:invoice_number_key)
                          inv:invoice_number = job:invoice_number
                          if access:invoice.fetch(inv:invoice_number_key)
                              If inv:invoice_type = 'SIN'
                                  inv:total           = job:sub_total
                                  inv:labour_paid     = job:labour_cost
                                  inv:parts_paid      = job:parts_cost
                                  access:invoice.update()
                              End!If inv:invoice_type = 'SIN'
                          end
                      Of 1 ! No Button
                  End ! Case Missive
              End!If job:invoice_labour_cost <> job:labour_cost Or job:invoice_parts_cost <> job:parts_cost Or |
          End!If job:invoice_number <> ''
          If job:invoice_number_warranty <> '' And job:warranty_job = 'YES'
              If job:winvoice_labour_cost <> job:labour_cost_warranty Or job:winvoice_parts_cost <> job:parts_cost_warranty Or |
                  job:winvoice_courier_cost <> job:courier_cost_warranty
                  Case Missive('Warning! The Warranty Part of this job has been invoiced and you have changed the Warranty Costs. If you continue Invoice Number ' & Clip(job:Invoice_Number_Warranty) & ' will be changed to these new values.'&|
                    '<13,10>Are you sure?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          IF (AddToAudit(job:Ref_Number,'JOB','WARRANTY INVOICED JOB AMENDED','NEW COSTS:-' & '<13,10>LABOUR: ' & Format(job:labour_cost_warranty,@n14.2) & |
                                                  '<13,10>PARTS: ' & Format(job:parts_cost_warranty,@n14.2) & |
                                                  '<13,10>COURIER: ' & Format(job:courier_cost_warranty,@n14.2) & |
                                                  '<13,10><13,10>PREVIOUS COSTS:-' & '<13,10>LABOUR: ' & Format(job:winvoice_labour_cost,@n14.2) & |
                                                  '<13,10>PARTS: ' & Format(job:winvoice_parts_cost,@n14.2) & |
                                                  '<13,10>COURIER: ' & Format(job:winvoice_courier_cost,@n14.2)))
                          END ! IF
  
                          job:winvoice_labour_cost = job:labour_cost_warranty
                          job:winvoice_parts_cost  = job:parts_cost_warranty
                          job:winvoice_courier_cost = job:courier_cost_warranty
                      Of 1 ! No Button
                  End ! Case Missive
              End!If job:invoice_labour_cost <> job:labour_cost Or job:invoice_parts_cost <> job:parts_cost Or |
          End!If job:invoice_number <> ''
      End!If x" = False
  End!IF def:use_sage <> 'YES'
  
  !Fill Fields
  If job:msn = ''
      job:msn = 'N/A'
  End!If job:msn = ''
  !ANC Label
  If thiswindow.request = Insertrecord
      anc# = 0
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job:account_number
      if access:subtracc.fetch(sub:account_number_key) = level:benign
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = sub:main_account_number
          if access:tradeacc.fetch(tra:account_number_key) = level:benign
              if tra:use_sub_accounts = 'YES'
                  access:courier.clearkey(cou:courier_key)
                  cou:courier = sub:courier_outgoing
                  if access:courier.tryfetch(cou:courier_key) = Level:Benign
                      If cou:courier_type = 'ANC'
                          anc# = 1
                      End!If cou:courier_type = 'ANC'
                  end!if access:courier.tryfetch(cou:courier_key) = Level:Benign
              else!if tra:use_sub_accounts = 'YES'
                  access:courier.clearkey(cou:courier_key)
                  cou:courier = tra:courier_outgoing
                  if access:courier.tryfetch(cou:courier_key) = Level:Benign
                      If cou:courier_type = 'ANC'
                          anc# = 1
                      End!If cou:courier_type = 'ANC'
                  end!if access:courier.tryfetch(cou:courier_key) = Level:Benign
              end!if tra:use_sub_accounts = 'YES'
          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
      end!if access:subtracc.fetch(sub:account_number_key) = level:benign
      IF anc# = 1
          label# = 0
          access:trantype.clearkey(trt:transit_type_key)
          trt:transit_type = job:transit_type
          if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
              If trt:loan_unit = 'YES' or trt:exchange_unit = 'YES'
                  label# = 1
              End!If trt:loan_unit = 'YES' or trt:exchange_unit = 'YES'
          Else!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
              label# = 0
          End!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
      End!IF anc# = 1
  
      IF label# = 1
          glo:file_name = 'C:\ANC.TXT'
          Remove(expgen)
          If access:expgen.open()
              Stop(error())
              Return Level:Benign
          End!If access:expgen.open()
          access:expgen.usefile()
          Clear(gen:record)
          gen:line1   = job:ref_number
          gen:line1   = Sub(gen:line1,1,10) & job:company_name
          gen:line1   = Sub(gen:line1,1,40) & job:address_line1_delivery
          gen:line1   = Sub(gen:line1,1,70) & job:address_line2_delivery
          gen:line1   = Sub(gen:line1,1,100) & job:address_line3_delivery
          gen:line1   = Sub(gen:line1,1,130) & job:postcode_delivery
          gen:line1   = Sub(gen:line1,1,150) & job:telephone_delivery
          gen:line1   = Sub(gen:line1,1,175) & job:unit_type
          gen:line1   = Sub(gen:line1,1,195) & job:model_number
          gen:line1   = Sub(gen:line1,1,215) & cou:ContractNumber
          access:expgen.insert()
          Close(expgen)
          access:expgen.close()
          cou:anccount += 1
          If cou:ANCCount = 999
              cou:ANCCount = 1
          End
          access:courier.update()
          Set(Defaults)
          access:defaults.next()
          job:incoming_consignment_number = def:anccollectionno
          DEF:ANCCollectionNo += 10
          access:defaults.update()
          sav:path    = path()
          setpath(Clip(cou:ancpath))
          Run('ancpaper.exe',1)
          Setpath(sav:path)
  
      End!IF label# = 1
  End!If thiswindow.request = Insertrecord
  
  !Did Anything Change?
  If thiswindow.request <> Insertrecord
      Clear(ChangedGroup)
      If sav:ChargeableJob <> job:Chargeable_Job
          sav1:chj = 1
      End!If sav:ChargeableJob <> job:Chargeable_Job
      If sav:WarrantyJob <> job:Warranty_Job
          sav1:wrj = 1
      End!If sav:WarrantyJob <> job:Warranty_Job
      If sav:HubRepair  <> tmp:HubRepair
          sav1:hbr = 1
      End !If sav:HubRepair  <> jobe:HubRepair
  
  
  
      If sav1:cct Or sav1:wct or sav1:crt or sav1:wrt or sav1:chj or |
  sav1:wrj Or sav1:elc Or sav1:clc Or sav1:wlc Or sav1:epc Or |
  sav1:cpc  Or sav1:Wpc Or sav1:ecc Or sav1:ccc Or sav1:wcc Or sav1:hbr Or sav1:acc
          !Something has changed, so add an audit entry
  
          locAuditNotes         = 'JOB DETAILS CHANGED: '
  
          If sav1:cct = 1
          !Chargeable Charge Type
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>CHAR. CHARGE TYPE: ' & CLip(job:Charge_Type)
          End!If sav1:cct
  
          If sav1:wct = 1
          !Warranty Charge Type
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>WARR. CHARGE TYPE: ' & CLip(job:Warranty_Charge_Type)
          End!If sav1:cct
  
          If sav1:crt = 1
          !Chargeable Repair Type
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>CHAR. REPAIR TYPE: ' & CLip(job:Repair_Type)
          End!If sav1:cct
  
          If sav1:wrt = 1
          !Warranty Repair Type
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>WARR. REPAIR TYPE: ' & CLip(job:Repair_Type_Warranty)
          End!If sav1:cct
  
          If sav1:chj = 1
          !Chargable Job
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>CHARGEABLE JOB: ' &  Clip(job:Chargeable_Job)
          End!If sav1:chj = 1
  
          If sav1:wrj = 1
          !Warranty Job
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>WARRANTY JOB: ' & Clip(job:Warranty_Job)
          End!If sav1:wrj = 1
  
          If sav1:hbr = 1
              Case tmp:HubRepair
                  Of 0
                      locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>HUB REPAIR DE-SELECTED'
                  Of 1
                      locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>HUB REPAIR SELECTED'
              End !Case jobe:HubRepair
          End !If sav1:hbr = 1
  
          IF (AddToAudit(job:Ref_Number,'JOB','JOB UPDATED',locAuditNotes))
          END ! IF
  
      End!If ChangedGroup
  
      If sav:TransitType <> job:Transit_Type
          sav1:itt = 1
      End !If sav:TransitType <> job:Transit_Type
  
      !Do not record N/A in the notes - L900  (DBH: 29-07-2003)
      If job:MSN <> 'N/A'
          If sav:MSN <> job:MSN
              sav1:MSN = 1
          End !If sav:MSN <> job:MSN
      End !If job:MSN <> 'N/A' And job:MSN <> ''
  
      If sav:ModelNumber <> job:Model_Number
          sav1:Mdl = 1
      End !If sav:ModelNumber <> job:Model_Number
  
      If sav:DOP <> job:DOP
          sav1:DOP = 1
      End !If sav:DOP <> job:DOP
  
      If sav:OrderNumber <> job:Order_Number
          sav1:ord = 1
      End !If sav:OrderNumber <> job:Order_Number
  
      If sav:FaultCode1 <> job:Fault_Code1
          sav1:fc1 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode2 <> job:Fault_Code2
          sav1:fc2 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode3 <> job:Fault_Code3
          sav1:fc3 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode4 <> job:Fault_Code4
          sav1:fc4 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode5 <> job:Fault_Code5
          sav1:fc5 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode6 <> job:Fault_Code6
          sav1:fc6 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode7 <> job:Fault_Code7
          sav1:fc7 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode8 <> job:Fault_Code8
          sav1:fc8 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode9 <> job:Fault_Code9
          sav1:fc9 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode10 <> job:Fault_Code10
          sav1:fc10 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode11 <> job:Fault_Code11
          sav1:fc11 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If sav:FaultCode12 <> job:Fault_Code12
          sav1:fc12 = 1
      End !If sav:FaultCode1 <> job:Fault_Code1
  
      If job:Manufacturer = 'SAMSUNG'
          !Do not show Fault Code 12 for Samsung
          sav1:fc12 = 0
      End !If job:Manufacturer = 'SAMSUNG'
  
      !Do not show the Main Out Fault
      Access:MANFAULT.ClearKey(maf:MainFaultKey)
      maf:Manufacturer = job:Manufacturer
      maf:MainFault    = 1
      If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
          !Found
          Case maf:Field_Number
              Of 1
                  sav1:fc1 = 0
              Of 2
                  sav1:fc2 = 0
              Of 3
                  sav1:fc3 = 0
              Of 4
                  sav1:fc4 = 0
              Of 5
                  sav1:fc5 = 0
              Of 6
                  sav1:fc6 = 0
              Of 7
                  sav1:fc7 = 0
              Of 8
                  sav1:fc8 = 0
              Of 9
                  sav1:fc9 = 0
              Of 10
                  sav1:fc10 = 0
              Of 11
                  sav1:fc11 = 0
              Of 12
                  sav1:fc12 = 0
          End !Case maf:Field_Number
      Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End            !If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
  
      If sav:InvoiceText <> jbn:Invoice_Text
          sav1:ivt = 1
      End !If sav:InvoiceText <> jbn:Invoice_Text
  
      If sav:FaultDescription <> jbn:Fault_Description
          sav1:fdt = 1
      End !If sav:FaultDescription <> jbn:Fault_Description
  
      If sav1:itt Or sav1:MSN Or sav1:mdl Or sav1:DOP Or sav1:Ord
  
          locAuditNotes         = 'JOB DETAILS CHANGED: '
  
          If sav1:itt = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>TRANS TYPE: ' & CLip(job:Transit_Type)
          End !If sav1:itt = 1
  
          If sav1:MSN = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>MSN: ' & CLip(job:MSN)
          End !If sav1:MSN = 1
  
          If sav1:mdl = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>MODEL NO: ' & CLip(job:Model_Number)
          End !If sav1:mdl = 1
  
          If sav1:DOP = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>DOP: ' & CLip(Format(job:DOP,@d6))
          End !If sav1:DOP = 1
  
          If sav1:ord = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>ORDER NO: ' & CLip(job:Order_Number)
          End !If sav1:ord = 1
  
          IF (AddToAudit(job:Ref_Number,'JOB','JOB UPDATED',locAuditNotes))
          END ! IF
  
      End !sav1:ivt Or sav1:fd1
  
      If sav1:FC1 Or sav1:FC2 Or sav1:FC3 Or sav1:FC4 Or |
          sav1:FC5 Or sav1:FC6 Or sav1:FC7 Or sav1:fC8 Or |
          sav1:FC9 Or sav1:FC10 Or sav1:FC11 Or sav1:FC12
  
          locAuditNotes         = 'JOB DETAILS CHANGED: '
  
          If sav1:fc1 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE1: ' & CLip(job:Fault_Code1)
          End !If sav1:fc1 = 1
          If sav1:fc2 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE2: ' & CLip(job:Fault_Code2)
          End !If sav1:fc1 = 1
          If sav1:fc3 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE3: ' & CLip(job:Fault_Code3)
          End !If sav1:fc1 = 1
          If sav1:fc4 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE4: ' & CLip(job:Fault_Code4)
          End !If sav1:fc1 = 1
          If sav1:fc5 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE5: ' & CLip(job:Fault_Code5)
          End !If sav1:fc1 = 1
          If sav1:fc6 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE6: ' & CLip(job:Fault_Code6)
          End !If sav1:fc1 = 1
          If sav1:fc7 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE7: ' & CLip(job:Fault_Code7)
          End !If sav1:fc1 = 1
          If sav1:fc8 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE8: ' & CLip(job:Fault_Code8)
          End !If sav1:fc1 = 1
          If sav1:fc9 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE9: ' & CLip(job:Fault_Code9)
          End !If sav1:fc1 = 1
          If sav1:fc10 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE10: ' & CLip(job:Fault_Code10)
          End !If sav1:fc1 = 1
          If sav1:fc11 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE11: ' & CLip(job:Fault_Code11)
          End !If sav1:fc1 = 1
          If sav1:fc12 = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>FAULT CODE12: ' & CLip(job:Fault_Code12)
          End !If sav1:fc1 = 1
  
          IF (AddToAudit(job:Ref_Number,'JOB','JOB UPDATED: FAULT CODES',locAuditNotes))
          END ! IF
  
      End !sav1:FC9 Or sav1:FC10 Or sav1:FC11 Or sav1:FC12
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = job:Manufacturer
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Found
          If man:UseInvTextForFaults
              !Does not show Invoice Text
              sav1:ivt = 0
          End !If man:UseInvTextForFaults
      End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
      If sav1:ivt
          locAuditNotes         = 'JOB DETAILS CHANGED: '
          If sav1:ivt = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>' & CLip(jbn:Invoice_Text)
          End !If sav1:fc1 = 1
          IF (AddToAudit(job:Ref_Number,'JOB','JOB UPDATED: INVOICE TEXT',locAuditNotes))
          END ! IF
  
  
      End !If sav1:ivt
      If sav1:fdt
          locAuditNotes         = 'JOB DETAILS CHANGED: '
          If sav1:fdt = 1
              locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>' & CLip(jbn:Fault_Description)
          End !If sav1:fc1 = 1
          IF (AddToAudit(job:Ref_Number,'JOB','JOB UPDATED: FAULT DESCRIPTION',locAuditNotes))
          END ! IF
  
      End !If sav1:ivt
  
  
  End!If thiswindow.request <> Insertrecord
  
  
  !Write Address/Account Details
  If job:Account_Number <> accsav:AccountNumber
      IF (AddToAudit(job:Ref_Number,'JOB','ACCOUNT NUMBER CHANGED','PREVIOUS ACCOUNT: ' & Clip(accsav:AccountNumber) & |
                          '<13,10>NEW ACCOUNT: ' & Clip(job:Account_Number)))
      END ! IF
  End !job:Account_Number <> accsav:AccountNumber
  
  If job:Company_Name <> accsa1:CompanyName Or |
      job:Address_Line1 <> accsa1:AddressLine1 Or |
      job:Address_Line2 <> accsa1:AddressLine2 Or |
      job:Address_Line3 <> accsa1:AddressLine3 Or |
      job:Postcode <> accsa1:Postcode Or |
      job:Telephone_Number <> accsa1:TelephoneNumber Or |
      job:Fax_Number <> accsa1:FaxNumber
  
      IF (AddToAudit(job:Ref_Number,'JOB','ADDRESS DETAILS CHANGED - CUSTOMER','PREVIOUS: <13,10>' & Clip(accsa1:CompanyName) & |
                      '<13,10>' & Clip(accsa1:AddressLine1) & |
                      '<13,10>' & Clip(accsa1:AddressLine2) & |
                      '<13,10>' & Clip(accsa1:AddressLine3) & |
                      '<13,10>' & Clip(accsa1:Postcode) & |
                      '<13,10>TEL: ' & Clip(accsa1:TelephoneNumber) & |
                      '<13,10>FAX: ' & Clip(accsa1:FaxNumber)))
      END ! IF
  End !job:Fax_Number <> accsa1:FaxNumber
  
  If job:Company_Name_Delivery <> accsa2:CompanyName Or |
      job:Address_Line1_Delivery <> accsa2:AddressLine1 Or |
      job:Address_Line2_Delivery <> accsa2:AddressLine2 Or |
      job:Address_Line3_Delivery <> accsa2:AddressLine3 Or |
      job:Postcode_Delivery <> accsa2:Postcode Or |
      job:Telephone_Delivery <> accsa2:TelephoneNumber
  
      IF (AddToAudit(job:Ref_Number,'JOB','ADDRESS DETAILS CHANGED - DESPATCH','PREVIOUS: <13,10>' & Clip(accsa2:CompanyName) & |
                      '<13,10>' & Clip(accsa2:AddressLine1) & |
                      '<13,10>' & Clip(accsa2:AddressLine2) & |
                      '<13,10>' & Clip(accsa2:AddressLine3) & |
                      '<13,10>' & Clip(accsa2:Postcode) & |
                      '<13,10>TEL: ' & Clip(accsa1:TelephoneNumber)))
      END ! IF
  
  End !job:Fax_Number <> accsa1:FaxNumber
  !Check Accesories incase they change
  Free(SaveAccessoryQueue)
  Clear(SaveAccessoryQueue)
  
  Open(SaveAccessory)
  If ~Error()
      Save_jac_ID = Access:JOBACC.SaveFile()
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
             Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          Clear(savacc:Record)
          savacc:Accessory    = jac:Accessory
          Get(SaveAccessory,savacc:AccessoryKey)
          If Error()
              !New Accessory Added
              savaccq:Accessory   = jac:Accessory
              savaccq:Type        = 1
              Add(SaveAccessoryQueue)
          End !If Error()
      End !Loop
      Access:JOBACC.RestoreFile(Save_jac_ID)
  
      Clear(savacc:record)
      Set(SaveAccessory,0)
      Loop
          Next(SaveAccessory)
          If Error()
              Break
          End !If Error()
          Access:JOBACC.ClearKey(jac:Ref_Number_Key)
          jac:Ref_Number = job:Ref_Number
          jac:Accessory  = savacc:Accessory
          If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
              !Found
  
          Else!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              !Accessory Deleted
              savaccq:Accessory   = savacc:Accessory
              savaccq:Type        = 0
              Add(SaveAccessoryQueue)
          End!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
      End !Loop
  
      If Records(SaveAccessoryQueue)
          !Some accessories have been changed
          locAuditNotes = ''
          Sort(SaveAccessoryQueue,savaccq:Accessory,savaccq:Type)
          Loop x# = 1 To Records(SaveAccessoryQueue)
              Get(SaveAccessoryQueue,x#)
              locAuditNotes   = Clip(locAuditNotes) & '<13,10>' & Clip(savaccq:Accessory)
               Case savaccq:Type
                  Of 0
                      locAuditNotes = Clip(locAuditNotes) & ' -DELETED'
                  Of 1
                      locAuditNotes = Clip(locAuditNotes) & ' -ADDED'
               End !Case savaccq:Type
          End !Loop x# = 1 To Records(SaveAccessoryQueue)
  
          !Cut out the line break at the beginning
          locAuditNotes         = Sub(locAuditNotes,3,Len(locAuditNotes))
          IF (AddToAudit(job:Ref_Number,'JOB','JOB ACCESSORIES AMENDED',locAuditNotes))
          END ! IF
  
      End !If Records(SaveAccessoryQueue)
      Free(SaveAccessoryQueue)
  End !Error()
  Close(SaveAccessory)
  Remove(SaveAccessory)
  
  
  ReturnValue = PARENT.TakeCompleted()
  If thiswindow.request = Insertrecord
      If tmp:printjobcard = 1
          glo:select1  = sav:ref_number
          Job_card
          glo:select1  = ''
      End!If tmp:printjobcard = 1
      If tmp:printjobreceipt = 1
          glo:select1  = sav:ref_number
          Job_Receipt
          glo:select1  = ''
      End!If tmp:printjobreceipt = 1
  End!If thiswindow.request = Insertrecord
  !!Do the automatic despatch
  !If tmp:DespatchClose    = 1
  !    despatch# = 1
  !    access:subtracc.clearkey(sub:account_number_key)
  !    sub:account_number  = job:account_number
  !    If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
  !        access:tradeacc.clearkey(tra:account_number_key)
  !        tra:account_number  = sub:main_account_number
  !        If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
  !            If tra:skip_despatch = 'YES'
  !                despatch# = 0
  !            End!If tra:skip_despatch = 'YES'
  !        End!If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
  !    End!If access;subtracc.tryfetch(sub:account_number_key) = Level:Benign
  !    If despatch# = 1
  !        access:courier.clearkey(cou:courier_key)
  !        cou:courier = job:courier
  !        if access:courier.tryfetch(cou:courier_key)
  !            Case MessageEx('Cannot find the Courier attached to this job.','ServiceBase 2000',|
  !                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
  !                Of 1 ! &OK Button
  !            End!Case MessageEx
  !        Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
  !            If access:desbatch.primerecord() = Level:Benign
  !                If access:desbatch.tryinsert()
  !                    access:desbatch.cancelautoinc()
  !                Else!If access:despatch.tryinsert()
  !                    Include('despsing.inc')
  !                End!If access:despatch.tryinsert()
  !            End!If access:desbatch.primerecord() = Level:Benign
  !        End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
  !    End!If despatch# = 1
  !End!If tmp:DespatchClose    = 1
  !
  !If print_despatch_note_temp = 'YES'
  !
  !    restock# = 0
  !    glo:select1  = job:ref_number
  !    If ToBeExchanged()
  !    !Has this unit got an Exchange, or is it expecting an exchange
  !        restock# = 1
  !    Else!If job:exchange_unit_number
  !        !Does the transit type expect an exchange?
  !        access:trantype.clearkey(trt:transit_type_key)
  !        trt:transit_type = job:transit_type
  !        if access:trantype.tryfetch(trt:transit_type_key) = Level:benign
  !            if trt:exchange_unit = 'YES'
  !                restock# = 1
  !            End!if trt:exchange_unit = 'YES'
  !        End!if access:trantype.tryfetch(trt:transit_type_key) = Level:benign
  !    End!If job:exchange_unit_number <> ''
  !    !If there is an loan unit attached, assume that a despatch note is required.
  !    If job:loan_unit_number <> ''
  !!        If job:despatched <> 'LAT'
  !!            restock# = 1
  !!        Else!If job:despatched <> 'LAT'
  !            restock# = 0
  !!        End!If job:despatched <> 'LAT'
  !    End!If job:loan_unit_number <> ''
  !
  !    If restock# = 1
  !        restocking_note
  !    Else!If restock# = 1
  !        despatch_note
  !    End!If restock# = 1
  !    glo:select1  = ''
  !End!If print_despatch_note_temp = 'YES'
  !
  !
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?OptionsEstimate
    glo:select1 = 'INSERT'
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?job:Account_Number
    CASE EVENT()
    OF EVENT:AlertKey
      Post(event:accepted,?LookupTradeAccount)
    END
  OF ?job:Transit_Type
    CASE EVENT()
    OF EVENT:AlertKey
      Post(Event:Accepted,?LookupTransitType)
    END
  OF ?job:Model_Number
    CASE EVENT()
    OF EVENT:AlertKey
      Post(Event:accepted,?LookupModelNumber)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      quickwindow{prop:buffer} = 2
      If ThisWindow.Request = InsertRecord
          Select(?Sheet4,1)
          Unhide(?booking1_tab)
          access:users.clearkey(use:password_key)
          use:password    =glo:password
          access:users.fetch(use:password_key)
          job:who_booked  = use:user_code
          get(jobstage,0)
          if access:jobstage.primerecord() = level:benign
      
              jst:ref_number = job:ref_number
              jst:job_stage  = '000 NEW JOB'
              jst:date       = Today()
              jst:time       = Clock()
              access:users.clearkey(use:password_key)
              use:password =glo:password
              access:users.fetch(use:password_key)
              jst:user = use:user_code
              if access:jobstage.insert()
                  access:jobstage.cancelautoinc()
              end
              brw21.resetsort(1)
          end!if access:jobstage.primerecord() = level:benign
          Select(?job:auto_search)
      
      
      Else
          Select(?Sheet4,2)
          Unhide(?booking2_tab)
      End
      !Titles
      Do Transit_Type
      Do Location_Type
      Do Update_accessories
      Do Third_Party_Repairer
      Do Time_Remaining
      Do Trade_Account_Status
      Do Estimate_Check
      Do Show_Tabs
      Do CountParts
      Do Charge_Type
      Do Titles
      
      !Lookups
      Do Update_Engineer
      Do Update_Loan
      Do Update_Exchange
      Do Check_Msn
      !J removed the next 3 lines 07/10/02
      !don't know why they were here or why they solve the problem
      !only need to comment the last one - but may as well do them all!
      
      !Post(Event:accepted,?job:charge_type)
      !Post(event:accepted,?job:chargeable_job)
      !Post(event:accepted,?job:warranty_job)
      
      !not the temporary variables referenced in these embeds are not
      !set up at this stage - they come two embeds further on ...
      
      !message('inlookups')
      Do Estimate
      Do QA_Group
      
      !Price Job On Entry
      Local.Pricing(0)
      
      Do show_hide_tabs
      Do skip_despatch
      
      
      !Workshop Tick
            If ThisWindow.Request <> Insertrecord
                Unhide(?job:workshop)
                Unhide(?job:location_type:prompt)
            End
      
      !Security Check
      Check_Access('JOB LABOUR COSTS - EDIT',x")
      If x" = False
      Else!If x" = False

      End!"If x" = False
      Check_Access('JOB COURIER COSTS - EDIT',x")
      If x" = False

      Else!If x" = False

      End!If x" = False
      Check_Access('AMEND COMPLETED JOBS',x")
      If x" = False
          If job:date_completed <> ''
              Hide(?OK)
          End!If job:date_completed <> ''
      Else!If x" = False
          UnHide(?OK)
      End!If x" = False
      Set(defaults)
      access:Defaults.next()
      If def:use_sage <> 'YES'
          Check_Access('AMEND INVOICED JOBS',x")
          If x" = False
              If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
                  If job:invoice_number <> '' And job:invoice_number_warranty <> ''
                      Hide(?OK)
                  End!If job:invoice_number <> '' And job:invoice_number_warranty <> ''
              End!If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
              If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
                  If job:invoice_number <> ''
                      Hide(?OK)
                  End!If job:invoice_number <> ''
              End!If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
              If job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
                  If job:invoice_number_warranty <> ''
                      Hide(?OK)
                  End!If job:invoice_number_warranty <> ''
              End!If job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
          Else!If x" = False
              UnHide(?OK)
          End!If x" = False
      Else!If def:use_sage <> 'YES'
          If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
              If job:invoice_number <> '' And job:invoice_number_warranty <> ''
                  Hide(?OK)
              End!If job:invoice_number <> '' And job:invoice_number_warranty <> ''
          End!If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
          If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
              If job:invoice_number <> ''
                  Hide(?OK)
              End!If job:invoice_number <> ''
          End!If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
          If job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
              If job:invoice_number_warranty <> ''
                  Hide(?OK)
              End!If job:invoice_number_warranty <> ''
          End!If job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
      End!If def:use_sage <> 'YES'

      Check_Access('JOBS - AMEND DESPATCH',x")
      If x" = False
!          Hide(?amend_despatch:3)
      Else!If x" = False
          If job:consignment_number
!              UnHide(?amend_despatch:3)
          End!If job:consignment_number
      End!If x" = False
      !Save Fields
      account_number_temp     = job:account_number
      repair_type_temp        = job:repair_type
      warranty_repair_Type_temp   = job:repair_type_warranty
      transit_type_temp       = job:transit_type
      location_type_temp      = job:location_Type
      model_number_temp       = job:model_number
      location_temp           = job:location
      workshop_temp           = job:workshop
      charge_type_temp        = job:charge_type
      estimate_temp           = job:estimate
      msn_temp                = job:msn
      esn_temp                = job:esn
      unit_type_temp          = job:unit_type
      current_status_temp     = job:current_status
      order_number_temp       = job:order_number
      surname_temp            = job:surname
      date_completed_temp     = job:date_completed
      time_completed_temp     = job:time_completed
      engineer_temp           = job:engineer
      warranty_charge_type_temp = job:warranty_charge_type
      warranty_job_temp       = job:warranty_job
      chargeable_job_temp     = job:chargeable_job
      estimate_ready_temp     = job:estimate_ready
      in_repair_temp          = job:in_repair
      on_test_temp            = job:on_test
      qa_passed_temp          = job:qa_passed
      qa_rejected_temp        = job:qa_rejected
      qa_second_passed_temp   = job:qa_second_passed
      estimate_accepted_temp  = job:estimate_accepted
      estimate_rejected_temp  = job:estimate_rejected
      courier_temp            = job:courier
      loan_courier_temp       = job:loan_courier
      exchange_courier_temp   = job:exchange_courier
      Ignore_Chargeable_Charges_Temp  = job:Ignore_Chargeable_Charges
      Ignore_Warranty_Charges_Temp    = job:Ignore_Warranty_Charges
      Ignore_Estimate_Charges_Temp    = job:Ignore_Estimate_Charges
      exchange_despatched_temp         = JOB:Exchange_Despatched
      exchange_consignment_number_temp      = JOB:Exchange_Consignment_Number
      exchange_despatched_user_temp    = JOB:Exchange_Despatched_User
      exchange_despatch_number_temp  = JOB:Exchange_Despatch_Number
      loan_despatched_temp               = job:loan_despatched
      loan_consignment_number_temp     = job:loan_consignment_number
      loan_Despatch_number_temp      = job:loan_despatch_number
      loan_despatched_user_temp        = job:loan_despatched_user
      date_despatched_temp             = JOB:Date_Despatched
      despatch_number_temp             = JOB:Despatch_Number
      despatch_user_temp               = JOB:Despatch_User
      consignment_number_temp          = JOB:Consignment_Number
      sav:EstLabourCost                = job:Labour_Cost_Estimate
      sav:ChaLabourCost                = job:Labour_Cost
      sav:WarLabourCost                = job:Labour_Cost_Warranty
      tmp:SaveHubRepair                = tmp:HubRepair
      
      !Save Fields In Case Of Cancel
      old_exchange_unit_number_temp   = job:exchange_unit_number
      old_loan_unit_number_temp       = job:loan_unit_number
      
      
      !Will these fields be changed?
      sav:ChargeType          = job:Charge_Type
      sav:WarrantyChargeType  = job:Warranty_Charge_Type
      sav:RepairType          = job:Repair_Type
      sav:WarrantyRepairType  = job:Repair_Type_Warranty
      sav:ChargeableJob       = job:Chargeable_Job
      sav:WarrantyJob         = job:Warranty_Job
      sav:EstimateLabourCost       = job:Labour_Cost_Estimate
      sav:ChargeableLabourCost       = job:Labour_Cost
      sav:WarrantyLabourCost       = job:Labour_Cost_Warranty
      sav:EstimatePartsCost       = job:Parts_Cost_Estimate
      sav:ChargeablePartsCost       = job:Parts_Cost
      sav:WarrantyPartsCost       = job:Parts_Cost_Warranty
      sav:EstimateCourierCost       = job:Courier_Cost_Estimate
      sav:ChargeableCourierCost       = job:Courier_Cost
      sav:WarrantyCourierCost       = job:Courier_Cost_Warranty
      sav:HubRepair               = tmp:HubRepair
      sav:TransitType         = job:Transit_Type
      sav:MSN                 = job:MSN
      sav:ModelNumber         = job:Model_Number
      sav:DOP                 = job:DOP
      sav:OrderNumber         = job:Order_Number
      sav:FaultCode1          = job:Fault_Code1
      sav:FaultCode2          = job:Fault_Code2
      sav:FaultCode3          = job:Fault_Code3
      sav:FaultCode4          = job:Fault_Code4
      sav:FaultCode5          = job:Fault_Code5
      sav:FaultCode6          = job:Fault_Code6
      sav:FaultCode7          = job:Fault_Code7
      sav:FaultCode8          = job:Fault_Code8
      sav:FaultCode9          = job:Fault_Code9
      sav:FaultCode10         = job:Fault_Code10
      sav:FaultCode11         = job:Fault_Code11
      sav:FaultCode12         = job:Fault_Code12
      sav:InvoiceText         = jbn:Invoice_Text
      sav:FaultDescription    = jbn:Fault_Description
      
      !History Button
      history_run_temp = 0
      If ThisWindow.Request <> insertrecord
          Do History_search2
      End
      Do Last_repair_days
      !Show Fields
      Set(defaults)
      access:defaults.next()
      If def:show_mobile_number = 'YES'
          Hide(?job:mobile_number)
          Hide(?job:mobile_number:prompt)
          Hide(?job:mobile_number:2)
          Hide(?mobilenumberdop)
      Else
          Unhide(?job:mobile_number)
          Unhide(?job:mobile_number:prompt)
          unhide(?job:mobile_number:2)
          Unhide(?mobilenumberdop)
      End
      
      If def:HideColour   = 'YES'
          Hide(?job:Colour)
          ?UnitTypeColour{prop:Text} = 'Unit Type'
      Else!If def:HideColour   = 'YES'
          Unhide(?job:colour)
          ?Prompt:Job:Unit_Type{prop:text} = 'Unit Type / Colour'
      
          If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
              ?Prompt:Job:Unit_Type{prop:Text} = 'Unit Type / ' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
              ?UnitTypeColour{prop:Text} = 'Unit Type / ' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
          End !Case ThisWindow.Request
      End!If def:HideColour   = 'YES'
      
      If ProductCodeRequired(job:Manufacturer)
          ?job:ProductCode{prop:Hide} = 1
          ?job:ProductCode:Prompt{prop:Hide} = 1
          ?ModelNumberProductCode{prop:Text} = 'Model Number'
          ?job:ProductCode:2{prop:Hide} = 1
      Else !If ProductCodeRequired(job:Manufacutrer)
          ?job:ProductCode{prop:Hide} = 0
          ?job:ProductCode:Prompt{prop:Hide} = 0
          ?job:ProductCode:2{prop:Hide} = 0
      End!If def:HideProduct = 'YES'
      
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job:account_number
      if access:subtracc.fetch(sub:account_number_key) = level:benign
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = sub:main_account_number
          if access:tradeacc.fetch(tra:account_number_key) = level:benign
              if tra:use_contact_name = 'YES'
                  Unhide(?NameGroup)
                  Unhide(?Customer_Name_String_Temp:Prompt)
                  Unhide(?Customer_Name_String_Temp)
              else!if tra:use_sub_accounts = 'YES'
                  Hide(?NameGroup)
                  Hide(?Customer_Name_String_Temp:Prompt)
                  Hide(?Customer_Name_String_Temp)
              end!if tra:use_sub_accounts = 'YES'
          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
      end!if access:subtracc.fetch(sub:account_number_key) = level:benign
      
      !Paid To Date
      paid_chargeable_temp = ''
      paid_warranty_temp = ''
      setcursor(cursor:wait)
      save_jpt_id = access:jobpaymt.savefile()
      
      access:jobpaymt.clearkey(jpt:all_date_key)
      jpt:ref_number = job:ref_number
      set(jpt:all_date_key,jpt:all_date_key)
      loop
      
          if access:jobpaymt.next()
             break
          end !if
          if jpt:ref_number <> job:ref_number      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          paid_chargeable_temp += jpt:amount
      
      end !loop
      access:jobpaymt.restorefile(save_jpt_id)
      setcursor()
      !Invoiced
      If job:invoice_number <> ''
          job:courier_cost = job:invoice_courier_cost
          job:labour_cost  = job:invoice_labour_cost
          job:parts_cost   = job:invoice_parts_cost
      End!If job:invoice_number <> ''
      If job:invoice_number_warranty <> ''
          job:courier_cost_warranty = job:winvoice_courier_cost
          job:labour_cost_warranty  = job:winvoice_labour_cost
          job:parts_cost_warranty  =  job:winvoice_parts_cost
      End!If job:invoice_number_warranty <> ''
      
      Case job:edi
          Of 'EXC'
          Of 'YES'
          Of 'FIN'
      End!Case job:edi
      
      If job:invoiceStatus    = 'EXC'
      End!If job:invoiceStatus    = 'EXC'
      !!Cancelled Job?
      !If job:cancelled = 'YES'
      !    Hide(?OK)
      !    Unhide(?canceltext)
      !Else
      !    If jobe:FailedDelivery
      !        ?Canceltext{prop:Hide} = 0
      !        ?CancelText{prop:Text} = 'FAILED DELIVERY'
      !    End !If jobe:FailedDelivery
      !End!If job:cancelled = 'YES'
      !
      !thismakeover.refresh()
      !Date Completed Text
        If job:Date_completed = ''
           If job:Estimate = 'YES' and (job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES') And |
              ?job:Estimate{prop:hide} = 0
              ?JobCompleted{prop:Text} = 'ESTIMATE JOB'
              ?JobCompleted{prop:fontcolor} = 0D0FFD0H
              Unhide(?JobCompleted)
           End !If job:Estimate = 'YES' and (job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES')
        Else !If job:Date_completed = ''
            ?JobCompleted{prop:Text} = 'JOB COMPLETED'
            ?jobCompleted{prop:FontColor} = 080FFFFH
            Unhide(?jobcompleted)
        End!If job:Date_completed <> ''
      Do OutFaultList
      Do CheckRequiredFields
      !message('end of open window code')
    OF EVENT:GainFocus
      Do Fill_Lists
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.Pricing   Procedure(Byte func:Force)
Code

    Set(defaults)
    access:defaults.next()
    Do SetJOBSE

    !Add new prototype - L945 (DBH: 04-09-2003)
    JobPricingRoutine(func:Force)
    Access:JOBSE.Update()
    Do GetJOBSE

    Display()
LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
locAction STRING(30)
    Code

    local:ErrorCode  = 0
    If AccessoryCheck('JOB')
        If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 1
        Else!If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 2
        End !If AccessoryMismatch(job:Ref_Number,'JOB')
    End !If AccessoryCheck('JOB')


    locAuditNotes = ''

    Case local:Errorcode
        Of 0 Orof 2
            locAuditNotes         = 'ACCESSORIES:<13,10>'
        Of 1
            locAuditNotes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
    End !Case local:Errorcode

    !Add the list of accessories to the Audit Trail
    If job:Despatch_Type <> 'LOAN'
        Save_jac_ID = Access:JOBACC.SaveFile()
        Access:JOBACC.ClearKey(jac:Ref_Number_Key)
        jac:Ref_Number = job:Ref_Number
        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        Loop
            If Access:JOBACC.NEXT()
               Break
            End !If
            If jac:Ref_Number <> job:Ref_Number      |
                Then Break.  ! End If
            locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
        End !Loop
        Access:JOBACC.RestoreFile(Save_jac_ID)
    Else !If job:Despatch_Type <> 'LOAN'
        Save_lac_ID = Access:LOANACC.SaveFile()
        Access:LOANACC.ClearKey(lac:Ref_Number_Key)
        lac:Ref_Number = job:Ref_Number
        Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
        Loop
            If Access:LOANACC.NEXT()
               Break
            End !If
            If lac:Ref_Number <> job:Ref_Number      |
                Then Break.  ! End If
            locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(lac:Accessory)
        End !Loop
        Access:LOANACC.RestoreFile(Save_lac_ID)
    End !If job:Despatch_Type <> 'LOAN'

    Case local:Errorcode
        Of 1
            !If locAuditNotesError show the Accessories that were actually tagged
            locAuditNotes         = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).
    End!Case local:Errorcode

    Case local:ErrorCode
        Of 0
            locAction        = 'COMPLETION VALIDATION: PASSED ACCESSORIES'
        Of 1
            locAction        = 'COMPLETION VALIDATION: FAILED ACCESSORIES'
        Of 2
            locAction        = 'COMPLETION VALIDATION: PASSED ACCESSORIES (FORCED)'
    End !Case local:ErrorCode

    IF (AddToAudit(job:Ref_Number,'JOB',locAction,locAuditNotes))
    END ! IF

    !Do not complete
    If local:ErrorCode = 1
        Return Level:Fatal
    End !If local:ErrorCode = 1

    Return Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW21.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

UpdateJOBS_Additional PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:JobNumber        STRING(20)
save_joe_id          USHORT,AUTO
save_taf_id          USHORT,AUTO
save_cou_ali_id      USHORT,AUTO
save_cou_id          USHORT,AUTO
tmp:ConsignNo        STRING(30)
tmp:accountnumber    STRING(30)
tmp:OldConsignNo     STRING(30)
tmp:labelerror       LONG
account_number2_temp STRING(30)
tmp:DespatchClose    BYTE(0)
sav:ref_number       LONG
tmp:printjobcard     BYTE(0)
tmp:printjobreceipt  BYTE(0)
save_trb_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
save_cha_id          USHORT,AUTO
sav:path             STRING(255)
print_despatch_note_temp STRING(3)
error_type_temp      STRING(1),DIM(50)
error_queue_temp     QUEUE,PRE(err)
field                STRING(30)
                     END
error_message_temp   STRING(255)
Cancelling_Group     GROUP,PRE()
old_exchange_unit_number_temp REAL
new_exchange_unit_number_temp REAL
old_loan_unit_number_temp REAL
save_moc_id          USHORT,AUTO
new_loan_unit_number_temp REAL
saved_ref_number_temp REAL
                     END
parts_queue_temp     QUEUE,PRE(partmp)
Part_Ref_Number      REAL
Quantity             REAL
Exclude_From_Order   STRING(3)
Pending_Ref_Number   REAL
                     END
check_for_bouncers_temp STRING('0')
save_aud_id          USHORT,AUTO
save_job_id          USHORT,AUTO
saved_esn_temp       STRING(16)
saved_msn_temp       STRING(16)
engineer_sundry_temp STRING(3)
main_store_sundry_temp STRING(3)
engineer_ref_number_temp REAL
main_store_ref_number_temp REAL
engineer_quantity_temp REAL
main_store_quantity_temp REAL
save_ccp_id          USHORT
save_cwp_id          USHORT,AUTO
label_type_temp      BYTE
exchange_accessory_count_temp REAL
accessory_count_temp REAL
Ignore_Chargeable_Charges_Temp STRING('''NO''')
Ignore_Warranty_Charges_Temp STRING('''NO''')
Ignore_Estimate_Charges_Temp STRING('''NO''')
Force_Fault_Codes_Temp STRING('NO {1}')
balance_due_warranty_temp REAL
paid_warranty_temp   REAL
print_label_temp     STRING(3)
Courier_temp         STRING(30)
loan_Courier_temp    STRING(30)
exchange_Courier_temp STRING(30)
engineer_name_temp   STRING(60)
msn_fail_temp        STRING('NO {1}')
esn_fail_temp        STRING('NO {1}')
day_count_temp       REAL
day_number_temp      REAL
date_error_temp      STRING(3)
estimate_ready_temp  STRING(3)
in_repair_temp       STRING(3)
on_test_temp         STRING(3)
qa_passed_temp       STRING(3)
qa_rejected_temp     STRING(3)
qa_second_passed_temp STRING(3)
estimate_accepted_temp STRING(3)
estimate_rejected_temp STRING(3)
Third_Party_Site_temp STRING(30)
Chargeable_Job_temp  STRING(3)
Warranty_Job_Temp    STRING(3)
warranty_charge_type_temp STRING(30)
Partemp_group        GROUP,PRE()
Partemp:part_ref_number REAL
Partemp:pending_ref_number REAL
Partemp:despatch_note_number STRING(30)
Partemp:quantity     LONG
Partemp:purchase_cost REAL
Partemp:sale_cost    REAL
                     END
estimate_temp        STRING(3)
workshop_temp        STRING(3)
location_temp        STRING(30)
repair_type_temp     STRING(30)
warranty_repair_type_temp STRING(30)
model_number_temp    STRING(30)
History_Run_Temp     SHORT
transit_type_temp    STRING(30)
account_number_temp  STRING(30)
ESN_temp             STRING(30)
MSN_temp             STRING(30)
Unit_Type_temp       STRING(30)
Current_Status_Temp  STRING(30)
Order_Number_temp    STRING(30)
Surname_temp         STRING(30)
Date_Completed_temp  DATE
Time_Completed_Temp  TIME
LocalRequest         LONG
LocalResponse        LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
charge_type_Temp     STRING(30)
title_3_temp         STRING(46)
details_temp         STRING(31)
date_booked_temp     STRING(29)
name_temp            STRING(60)
Vat_Total_Temp       REAL
Total_Temp           REAL
engineer_temp        STRING(30)
loan_model_make_temp STRING(60)
loan_esn_temp        STRING(15)
loan_msn_temp        STRING(10)
Loan_Accessories_Temp STRING(30)
Exchange_Model_Make_temp STRING(60)
Exchange_Esn_temp    STRING(15)
Exchange_Msn_temp    STRING(10)
Exchange_Accessories_Temp STRING(30)
balance_due_temp     REAL
Accessories_Temp     STRING(30)
location_type_temp   STRING(30)
vat_total_estimate_temp REAL
total_estimate_temp  REAL
vat_total_warranty_temp REAL
Total_warranty_temp  REAL
chargeable_part_total_temp REAL
warranty_part_total_temp REAL
job_time_remaining_temp STRING(20)
status_time_remaining_temp STRING(20)
ordered_temp         STRING(18)
CPCSExecutePopUp     BYTE
ProcedureRunning     BYTE
warranty_ordered_temp STRING(18)
vat_estimate_temp    REAL
vat_chargeable_temp  REAL
vat_warranty_temp    REAL
paid_chargeable_temp REAL
estimate_parts_cost_temp REAL
show_booking_temp    STRING('NO {1}')
trade_account_string_temp STRING(60)
customer_name_string_temp STRING(60)
Exchange_Consignment_Number_temp STRING(30)
Exchange_Despatched_temp DATE
Exchange_Despatched_User_temp STRING(3)
Exchange_Despatch_Number_temp REAL
Loan_Consignment_Number_temp STRING(30)
Loan_Despatched_temp DATE
Loan_Despatched_User_temp STRING(3)
Loan_Despatch_Number_temp REAL
Date_Despatched_temp DATE
Despatch_Number_temp REAL
Despatch_User_temp   STRING(3)
Consignment_Number_temp STRING(30)
third_party_returned_temp DATE
tmp:FaultDescription STRING(255)
tmp:EngineerNotes    STRING(255)
tmp:InvoiceText      STRING(255)
SaveGroup            GROUP,PRE(sav)
ChargeType           STRING(30)
WarrantyChargeType   STRING(20)
RepairType           STRING(30)
WarrantyRepairType   STRING(30)
ChargeableJob        STRING(3)
WarrantyJob          STRING(3)
EstimateLabourCost   REAL
ChargeableLabourCost REAL
WarrantyLabourCost   REAL
EstimatePartsCost    REAL
ChargeablePartsCost  REAL
WarrantyPartsCost    REAL
EstimateCourierCost  REAL
ChargeableCourierCost REAL
WarrantyCourierCost  REAL
HubRepair            BYTE(0)
AccountNumber        STRING(30)
TransitType          STRING(30)
MSN                  STRING(30)
ModelNumber          STRING(30)
DOP                  DATE
OrderNumber          STRING(30)
FaultCode1           STRING(30)
FaultCode2           STRING(30)
FaultCode3           STRING(30)
FaultCode4           STRING(30)
FaultCode5           STRING(30)
FaultCode6           STRING(30)
FaultCode7           STRING(30)
FaultCode8           STRING(30)
FaultCode9           STRING(30)
FaultCode10          STRING(255)
FaultCode11          STRING(255)
FaultCode12          STRING(255)
InvoiceText          STRING(255)
FaultDescription     STRING(255)
                     END
ChangedGroup         GROUP,PRE(sav1)
cct                  BYTE(0)
wct                  BYTE(0)
crt                  BYTE(0)
wrt                  BYTE(0)
chj                  BYTE(0)
wrj                  BYTE(0)
elc                  BYTE(0)
clc                  BYTE(0)
wlc                  BYTE(0)
epc                  BYTE(0)
cpc                  BYTE(0)
wpc                  BYTE(0)
ecc                  BYTE(0)
ccc                  BYTE(0)
wcc                  BYTE(0)
hbr                  BYTE(0)
acc                  BYTE(0)
itt                  BYTE(0)
msn                  BYTE(0)
mdl                  BYTE(0)
dop                  BYTE(0)
ord                  BYTE(0)
fc1                  BYTE(0)
fc2                  BYTE(0)
fc3                  BYTE(0)
fc4                  BYTE(0)
fc5                  BYTE(0)
fc6                  BYTE(0)
fc7                  BYTE(0)
fc8                  BYTE(0)
fc9                  BYTE(0)
fc10                 BYTE(0)
fc11                 BYTE(0)
fc12                 BYTE(0)
ivt                  BYTE(0)
fdt                  BYTE(0)
                     END
tmp:PartQueue        QUEUE,PRE(tmp)
PartNumber           STRING(30)
                     END
tmp:ParcelLineName   STRING(255),STATIC
tmp:WorkstationName  STRING(30)
tmp:IncomingIMEI     STRING(30)
tmp:IncomingMSN      STRING(30)
tmp:ExchangeIMEI     STRING(30)
tmp:ExchangeMSN      STRING(30)
tmp:EstimateDetails  STRING(255)
tmp:SkillLevel       LONG
tmp:one              BYTE(1)
tmp:DateAllocated    DATE
tmp:FaultyUnit       BYTE(0)
tmp:BERConsignmentNumber STRING(30)
sav:ChaLabourCost    REAL
sav:WarLabourCost    REAL
sav:EstLabourCost    REAL
tmp:HUBRepair        BYTE(0)
tmp:HubRepairDate    DATE
tmp:HubRepairTime    TIME
tmp:SaveHubRepair    BYTE(0)
tmp:Network          STRING(30)
tmp:CColourFlag      BYTE(0)
tmp:WColourFlag      BYTE(0)
tmp:Claim            REAL
tmp:Handling         REAL
tmp:Exchange         REAL
tmp:RRCELabourCost   REAL
tmp:RRCEPartsCost    REAL
tmp:RRCCLabourCost   REAL
tmp:RRCCPartsCost    REAL
tmp:RRCWLabourCost   REAL
tmp:RRCWPartsCost    REAL
tmp:DisableLocation  BYTE(0)
tmp:POPType          STRING(30)
tmp:SaveAccessory    STRING(255),STATIC
SaveAccessoryQueue   QUEUE,PRE(savaccq)
Accessory            STRING(30)
Type                 BYTE(0)
                     END
tmp:EndUserTelNo     STRING(30)
tmp:CPartQuantity    STRING(4)
tmp:WPartQuantity    STRING(4)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo:11 QUEUE                          !Queue declaration for browse/combo box using ?job:Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?job:Incoming_Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:6 QUEUE                           !Queue declaration for browse/combo box using ?job:JobService
cit:Service            LIKE(cit:Service)              !List box control field - type derived from field
cit:Description        LIKE(cit:Description)          !List box control field - type derived from field
cit:RefNumber          LIKE(cit:RefNumber)            !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB8::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
FDCB10::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
FDCB30::View:FileDropCombo VIEW(CITYSERV)
                       PROJECT(cit:Service)
                       PROJECT(cit:Description)
                       PROJECT(cit:RefNumber)
                     END
BRW12::View:Browse   VIEW(PARTS)
                       PROJECT(par:Part_Number)
                       PROJECT(par:Description)
                       PROJECT(par:Order_Number)
                       PROJECT(par:Quantity)
                       PROJECT(par:Part_Ref_Number)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
par:Part_Number        LIKE(par:Part_Number)          !List box control field - type derived from field
par:Part_Number_NormalFG LONG                         !Normal forground color
par:Part_Number_NormalBG LONG                         !Normal background color
par:Part_Number_SelectedFG LONG                       !Selected forground color
par:Part_Number_SelectedBG LONG                       !Selected background color
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Description_NormalFG LONG                         !Normal forground color
par:Description_NormalBG LONG                         !Normal background color
par:Description_SelectedFG LONG                       !Selected forground color
par:Description_SelectedBG LONG                       !Selected background color
par:Order_Number       LIKE(par:Order_Number)         !List box control field - type derived from field
par:Order_Number_NormalFG LONG                        !Normal forground color
par:Order_Number_NormalBG LONG                        !Normal background color
par:Order_Number_SelectedFG LONG                      !Selected forground color
par:Order_Number_SelectedBG LONG                      !Selected background color
ordered_temp           LIKE(ordered_temp)             !List box control field - type derived from local data
ordered_temp_NormalFG  LONG                           !Normal forground color
ordered_temp_NormalBG  LONG                           !Normal background color
ordered_temp_SelectedFG LONG                          !Selected forground color
ordered_temp_SelectedBG LONG                          !Selected background color
tmp:CPartQuantity      LIKE(tmp:CPartQuantity)        !List box control field - type derived from local data
tmp:CPartQuantity_NormalFG LONG                       !Normal forground color
tmp:CPartQuantity_NormalBG LONG                       !Normal background color
tmp:CPartQuantity_SelectedFG LONG                     !Selected forground color
tmp:CPartQuantity_SelectedBG LONG                     !Selected background color
par:Quantity           LIKE(par:Quantity)             !List box control field - type derived from field
par:Quantity_NormalFG  LONG                           !Normal forground color
par:Quantity_NormalBG  LONG                           !Normal background color
par:Quantity_SelectedFG LONG                          !Selected forground color
par:Quantity_SelectedBG LONG                          !Selected background color
chargeable_part_total_temp LIKE(chargeable_part_total_temp) !List box control field - type derived from local data
chargeable_part_total_temp_NormalFG LONG              !Normal forground color
chargeable_part_total_temp_NormalBG LONG              !Normal background color
chargeable_part_total_temp_SelectedFG LONG            !Selected forground color
chargeable_part_total_temp_SelectedBG LONG            !Selected background color
par:Part_Ref_Number    LIKE(par:Part_Ref_Number)      !List box control field - type derived from field
par:Part_Ref_Number_NormalFG LONG                     !Normal forground color
par:Part_Ref_Number_NormalBG LONG                     !Normal background color
par:Part_Ref_Number_SelectedFG LONG                   !Selected forground color
par:Part_Ref_Number_SelectedBG LONG                   !Selected background color
tmp:CColourFlag        LIKE(tmp:CColourFlag)          !List box control field - type derived from local data
tmp:CColourFlag_NormalFG LONG                         !Normal forground color
tmp:CColourFlag_NormalBG LONG                         !Normal background color
tmp:CColourFlag_SelectedFG LONG                       !Selected forground color
tmp:CColourFlag_SelectedBG LONG                       !Selected background color
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW77::View:Browse   VIEW(ESTPARTS)
                       PROJECT(epr:Part_Number)
                       PROJECT(epr:Description)
                       PROJECT(epr:Quantity)
                       PROJECT(epr:Record_Number)
                       PROJECT(epr:Ref_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
epr:Part_Number        LIKE(epr:Part_Number)          !List box control field - type derived from field
epr:Part_Number_NormalFG LONG                         !Normal forground color
epr:Part_Number_NormalBG LONG                         !Normal background color
epr:Part_Number_SelectedFG LONG                       !Selected forground color
epr:Part_Number_SelectedBG LONG                       !Selected background color
epr:Description        LIKE(epr:Description)          !List box control field - type derived from field
epr:Description_NormalFG LONG                         !Normal forground color
epr:Description_NormalBG LONG                         !Normal background color
epr:Description_SelectedFG LONG                       !Selected forground color
epr:Description_SelectedBG LONG                       !Selected background color
epr:Quantity           LIKE(epr:Quantity)             !List box control field - type derived from field
epr:Quantity_NormalFG  LONG                           !Normal forground color
epr:Quantity_NormalBG  LONG                           !Normal background color
epr:Quantity_SelectedFG LONG                          !Selected forground color
epr:Quantity_SelectedBG LONG                          !Selected background color
estimate_parts_cost_temp LIKE(estimate_parts_cost_temp) !List box control field - type derived from local data
estimate_parts_cost_temp_NormalFG LONG                !Normal forground color
estimate_parts_cost_temp_NormalBG LONG                !Normal background color
estimate_parts_cost_temp_SelectedFG LONG              !Selected forground color
estimate_parts_cost_temp_SelectedBG LONG              !Selected background color
epr:Record_Number      LIKE(epr:Record_Number)        !Primary key field - type derived from field
epr:Ref_Number         LIKE(epr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW79::View:Browse   VIEW(WARPARTS)
                       PROJECT(wpr:Part_Number)
                       PROJECT(wpr:Description)
                       PROJECT(wpr:Order_Number)
                       PROJECT(wpr:Quantity)
                       PROJECT(wpr:Part_Ref_Number)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
wpr:Part_Number        LIKE(wpr:Part_Number)          !List box control field - type derived from field
wpr:Part_Number_NormalFG LONG                         !Normal forground color
wpr:Part_Number_NormalBG LONG                         !Normal background color
wpr:Part_Number_SelectedFG LONG                       !Selected forground color
wpr:Part_Number_SelectedBG LONG                       !Selected background color
wpr:Description        LIKE(wpr:Description)          !List box control field - type derived from field
wpr:Description_NormalFG LONG                         !Normal forground color
wpr:Description_NormalBG LONG                         !Normal background color
wpr:Description_SelectedFG LONG                       !Selected forground color
wpr:Description_SelectedBG LONG                       !Selected background color
wpr:Order_Number       LIKE(wpr:Order_Number)         !List box control field - type derived from field
wpr:Order_Number_NormalFG LONG                        !Normal forground color
wpr:Order_Number_NormalBG LONG                        !Normal background color
wpr:Order_Number_SelectedFG LONG                      !Selected forground color
wpr:Order_Number_SelectedBG LONG                      !Selected background color
warranty_ordered_temp  LIKE(warranty_ordered_temp)    !List box control field - type derived from local data
warranty_ordered_temp_NormalFG LONG                   !Normal forground color
warranty_ordered_temp_NormalBG LONG                   !Normal background color
warranty_ordered_temp_SelectedFG LONG                 !Selected forground color
warranty_ordered_temp_SelectedBG LONG                 !Selected background color
tmp:WPartQuantity      LIKE(tmp:WPartQuantity)        !List box control field - type derived from local data
tmp:WPartQuantity_NormalFG LONG                       !Normal forground color
tmp:WPartQuantity_NormalBG LONG                       !Normal background color
tmp:WPartQuantity_SelectedFG LONG                     !Selected forground color
tmp:WPartQuantity_SelectedBG LONG                     !Selected background color
wpr:Quantity           LIKE(wpr:Quantity)             !List box control field - type derived from field
wpr:Quantity_NormalFG  LONG                           !Normal forground color
wpr:Quantity_NormalBG  LONG                           !Normal background color
wpr:Quantity_SelectedFG LONG                          !Selected forground color
wpr:Quantity_SelectedBG LONG                          !Selected background color
warranty_part_total_temp LIKE(warranty_part_total_temp) !List box control field - type derived from local data
warranty_part_total_temp_NormalFG LONG                !Normal forground color
warranty_part_total_temp_NormalBG LONG                !Normal background color
warranty_part_total_temp_SelectedFG LONG              !Selected forground color
warranty_part_total_temp_SelectedBG LONG              !Selected background color
wpr:Part_Ref_Number    LIKE(wpr:Part_Ref_Number)      !List box control field - type derived from field
wpr:Part_Ref_Number_NormalFG LONG                     !Normal forground color
wpr:Part_Ref_Number_NormalBG LONG                     !Normal background color
wpr:Part_Ref_Number_SelectedFG LONG                   !Selected forground color
wpr:Part_Ref_Number_SelectedBG LONG                   !Selected background color
tmp:WColourFlag        LIKE(tmp:WColourFlag)          !List box control field - type derived from local data
tmp:WColourFlag_NormalFG LONG                         !Normal forground color
tmp:WColourFlag_NormalBG LONG                         !Normal background color
tmp:WColourFlag_SelectedFG LONG                       !Selected forground color
tmp:WColourFlag_SelectedBG LONG                       !Selected background color
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Job Details'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Job Details'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('A&ddress Details'),USE(?Tab4),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Customer Address'),AT(144,70),USE(?String6),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Company Name'),AT(68,83),USE(?Prompt128),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(144,83),USE(job:Company_Name),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s10),AT(144,118),USE(job:Postcode,,?job:Postcode:2),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s15),AT(220,131),USE(job:Telephone_Number),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s15),AT(220,139),USE(job:Fax_Number),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING('Email Address'),AT(68,154),USE(?String21:4),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s255),AT(144,155),USE(jobe:EndUserEmailAddress),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING('Fax Number'),AT(144,139),USE(?String21:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(144,94),USE(job:Address_Line1),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(144,102),USE(job:Address_Line2),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Address'),AT(68,94),USE(?JOB:Address:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(144,110),USE(job:Address_Line3),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING('Contact Numbers'),AT(68,131),USE(?String21),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Telephone Number'),AT(144,131),USE(?String21:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(308,75,141,102),USE(?Collection_Address_Group),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING('Collection Address'),AT(312,70),USE(?String7),FONT(,8,,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(312,83),USE(job:Company_Name_Collection),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(312,94),USE(job:Address_Line1_Collection),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(312,102),USE(job:Address_Line2_Collection),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(312,110),USE(job:Address_Line3_Collection),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s10),AT(312,118),USE(job:Postcode_Collection),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s15),AT(312,131),USE(job:Telephone_Collection),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             TEXT,AT(312,147,132,24),USE(jbn:Collection_Text),SKIP,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                             BUTTON,AT(312,178),USE(?Collection_Text_Button),TRN,FLAT,LEFT,ICON('coltextp.jpg')
                           END
                           GROUP,AT(464,75,141,102),USE(?delivery_Address_Group),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING('Delivery Address'),AT(468,70),USE(?String8),FONT(,8,,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(468,83),USE(job:Company_Name_Delivery),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(468,94),USE(job:Address_Line1_Delivery),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(468,102),USE(job:Address_Line2_Delivery),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(468,110),USE(job:Address_Line3_Delivery),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s10),AT(468,118),USE(job:Postcode_Delivery),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s15),AT(468,131),USE(job:Telephone_Delivery),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             TEXT,AT(468,147,132,24),USE(jbn:Delivery_Text),SKIP,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                             BUTTON,AT(468,178),USE(?Delivery_Text_Button),TRN,FLAT,LEFT,ICON('deltextp.jpg')
                           END
                         END
                         TAB('En&gineering Details'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(112,70),USE(?Lookup_Engineer),DISABLE,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Engineer'),AT(68,70),USE(?Job:Engineer:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(60,98,236,20),USE(?Repair_Type_Chargeable_Group),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Chargeable Type'),AT(68,104),USE(?Prompt105),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(144,106,136,10),USE(job:Repair_Type),DISABLE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR,READONLY
                             BUTTON,AT(284,102),USE(?LookupChargeableChargeType),SKIP,DISABLE,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           END
                           PROMPT('Repair Type'),AT(144,98),USE(?Job:Repair_Type:Prompt),TRN,HIDE,FONT(,7,0D0FFD0H,FONT:bold+FONT:underline),COLOR(09A6A7CH)
                           STRING(@d6b),AT(432,99),USE(job:Date_In_Repair),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@t1b),AT(492,99),USE(job:Time_In_Repair),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@d6b),AT(524,99),USE(job:Date_On_Test),DISABLE,HIDE,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@t1b),AT(584,99),USE(job:Time_On_Test),DISABLE,HIDE,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@t1b),AT(584,110),USE(tmp:HubRepairTime),DISABLE,HIDE,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@d6),AT(524,110),USE(tmp:HubRepairDate),DISABLE,HIDE,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Hub Repair'),AT(432,110),USE(tmp:HUBRepair),DISABLE,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Hub Repair'),TIP('Hub Repair'),VALUE('1','0')
                           PROMPT('Engineers Notes'),AT(68,154),USE(?JOB:Engineers_Notes:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(284,152),USE(?Engineers_Notes_Button),DISABLE,TRN,FLAT,HIDE,LEFT,ICON('lookupp.jpg')
                           TEXT,AT(144,154,136,40),USE(jbn:Engineers_Notes),SKIP,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Repair Status'),AT(372,70),USE(?Prompt85),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Estimate Ready'),AT(432,70,60,12),USE(job:Estimate_Ready),DISABLE,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           BUTTON,AT(68,196),USE(?Button59),TRN,FLAT,LEFT,ICON('enghistp.jpg')
                           STRING(@s2),AT(184,84),USE(tmp:SkillLevel),TRN,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Job Level:'),AT(200,84),USE(?JobLevel),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s2),AT(240,84),USE(jobe:SkillLevel),TRN,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Allocated:'),AT(260,84),USE(?Prompt126:4),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(144,70),USE(engineer_name_temp),TRN,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING(@d6),AT(300,84),USE(tmp:DateAllocated),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Eng Level:'),AT(144,84),USE(?Prompt126:2),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(64,128,236,20),USE(?Repair_Type_Warranty_Group),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Warranty Type'),AT(68,133),USE(?JOB:Repair_Type_Warranty:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(144,133,136,10),USE(job:Repair_Type_Warranty),DISABLE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR,READONLY
                             BUTTON,AT(284,130),USE(?LookupWarrantyRepairType),SKIP,DISABLE,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           END
                           CHECK('In Repair'),AT(432,86),USE(job:In_Repair),DISABLE,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('On Test'),AT(544,86),USE(job:On_Test),DISABLE,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           PROMPT('Date Completed'),AT(512,148),USE(?Prompt100),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D6b),AT(512,158,60,10),USE(job:Date_Completed),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@t1),AT(572,158,36,10),USE(job:Time_Completed),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           GROUP,AT(364,130,244,12),USE(?QA_Group),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('QA Status'),AT(372,130),USE(?Prompt99),DISABLE,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('QA Passed'),AT(432,130),USE(job:QA_Passed),DISABLE,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                             STRING(@d6b),AT(524,130),USE(job:Date_QA_Passed),DISABLE,HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@t1b),AT(584,130),USE(job:Time_QA_Passed),DISABLE,HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           END
                         END
                         TAB('E&stimate Parts'),USE(?Estimate_Details_Tab),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(156,72,456,102),USE(?List:3),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('107L(2)|M*~Part Number~@s30@237L(2)|M*~Description~@s30@20R(2)|M*~Qty~@n4@35R(2)' &|
   '|M*~Total~@n14.2@'),FROM(Queue:Browse:2)
                           PROMPT('- Used On Repair'),AT(84,70),USE(?Prompt124),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BOX,AT(68,70,10,9),USE(?Box1),COLOR(COLOR:Red),FILL(COLOR:Red)
                         END
                         TAB('&Chargeable Parts'),USE(?Chargeable_Details_Tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(68,70,544,112),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('104L(2)|M*~Part Number~@s30@113L(2)|M*~Description~@s30@61L(2)|M*~Order Number~@' &|
   'n012b@74L(2)|M*~Order Status~@s18@20R(2)|M*~Qty~@s4@0L(2)|M*~Qty~@n4@35R(2)|M*~T' &|
   'otal~@n8.2@500L(50)*~Part Ref Number~@n012@/0L(50)*~Chargeable Colour Flag~@n1@/'),FROM(Queue:Browse)
                           BUTTON,AT(68,186),USE(?ColourKey),TRN,FLAT,LEFT,ICON('colkeyp.jpg')
                         END
                         TAB('&Warranty Parts'),USE(?Warranty_Details_Tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(68,70,544,112),USE(?List:4),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('104L(2)|M*~Part Number~@s30@113L(2)|M*~Description~@s30@61D(2)|M*~Order Number~L' &|
   '@n012b@74L(2)|M*~Order Status~@s18@20R(2)|M*~Qty~@s4@0L(2)|M*~Qty~@n4@35R(2)|M*~' &|
   'Total~@n14.2@500L(50)|M*~Part Ref Number~@n012@/4L(50)|M*~Warranty Colour Flag~@' &|
   'n1@/'),FROM(Queue:Browse:3)
                           BUTTON,AT(68,186),USE(?ColourKey:2),TRN,FLAT,LEFT,ICON('colkeyp.jpg')
                         END
                         TAB('In&voice Details'),USE(?Tab11),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Advance Payment'),AT(68,70),USE(?job:advance_payment:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2b),AT(148,70,64,10),USE(job:Advance_Payment,,?JOB:Advance_Payment:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Warranty Invoice No'),AT(356,83),USE(?JOB:Invoice_Number_Warranty:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Authority Number'),AT(68,86),USE(?JOB:Authority_Number:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(148,86,64,10),USE(job:Authority_Number),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Outgoing'),AT(524,94),USE(?Prompt103),TRN,FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Incoming'),AT(452,94),USE(?Prompt104),TRN,FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Courier'),AT(356,102),USE(?job:courier:prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(524,102,64,10),USE(job:Courier),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:11)
                           COMBO(@s30),AT(452,102,64,10),USE(job:Incoming_Courier),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                           ENTRY(@d6b),AT(524,70,64,10),USE(job:Invoice_Date),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@p<<<<<<<#pb),AT(452,83,64,10),USE(job:Invoice_Number_Warranty),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Chargeable Invoice No'),AT(356,70),USE(?JOB:Invoice_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@p<<<<<<<#pb),AT(452,70,64,10),USE(job:Invoice_Number),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@d6b),AT(524,83,64,10),USE(job:Invoice_Date_Warranty),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@n6b),AT(148,158,64,10),USE(job:EDI_Batch_Number),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Insurance Ref No'),AT(68,102),USE(?JOB:Insurance_Reference_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(148,102,124,10),USE(job:Insurance_Reference_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Service'),AT(356,118),USE(?JOB:Despatch_Number:Prompt:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s1),AT(452,118,64,10),USE(jobe:UPSFlagCode),HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),FORMAT('4L|M~UPS Flag Code~L(2)@s1@'),DROP(10),FROM('1|2|3'),MSG('UPS Flag Code')
                           COMBO(@s1),AT(524,118,64,10),USE(job:JobService),IMM,VSCROLL,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('10L(2)|M@s1@120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:6),MSG('Job Service For City Link / Label G')
                           PROMPT('EDI Batch Number'),AT(68,158),USE(?JOB:EDI_Batch_Number:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(548,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:11        !Reference to browse queue type
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB30               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:6         !Reference to browse queue type
                     END

BRW12                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
BRW77                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW77::Sort0:Locator StepLocatorClass                 !Default Locator
BRW79                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW79::Sort0:Locator StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_lac_id   ushort,auto
save_xca_id   ushort,auto
save_jac_id   ushort,auto
save_job_ali_id   ushort,auto
save_trr_id   ushort,auto
save_orp_id   ushort,auto
save_maf_id   ushort,auto
save_par_id   ushort,auto
save_wpr_id   ushort,auto
save_map_id   ushort,auto
save_par_ali_id   ushort,auto
save_epr_id   ushort,auto
save_jpt_id   ushort,auto
save_esn_id   ushort,auto
save_jea_id   ushort,auto
save_xch_ali_id   ushort,auto
    Map
LocalValidateAccessories    Procedure(),Byte
    End
!Save Account Address Details
accsav:AccountNumber    String(30)

InvoiceDetails  Group,Pre(accsa1)
AccountNumber       String(30)
CompanyName         String(30)
AddressLine1        String(30)
AddressLine2        String(30)
AddressLine3        String(30)
Postcode            String(30)
TelephoneNumber     String(30)
FaxNumber           String(30)
                End
DeliveryDetails     Group,Pre(accsa2)
CompanyName      String(30)
AddressLine1     String(30)
AddressLine2     String(30)
AddressLine3     String(30)
Postcode         String(30)
TelephoneNumber  String(30)
                End
SaveAccessory    File,Driver('TOPSPEED'),Pre(savacc),Name(tmp:SaveAccessory),Create,Bindable,Thread
AccessoryKey            Key(savacc:Accessory),NOCASE,DUP
Record                  Record
Accessory               String(30)
                        End
                    End
TempFilePath         CSTRING(255)

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

GetJOBSE        Routine
!    tmp:HUBRepair   = jobe:HUBRepair
!    tmp:HubRepairDate   = jobe:HubRepairDate
!    tmp:HubRepairTime   = jobe:HubRepairTime
!    tmp:Network = jobe:Network
!    tmp:POPType   = jobe:POPType
!    tmp:EndUserTelNo    = jobe:EndUserTelNo

SetJOBSE        Routine
!    Access:JOBSE.Clearkey(jobe:RefNumberKey)
!    jobe:RefNumber  = job:Ref_Number
!    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!        !Found
!        jobe:HUBRepair  = tmp:HUBRepair
!        jobe:Network    = tmp:Network
!        jobe:HubRepairDate  = tmp:HubRepairDate
!        jobe:HubRepairTime  = tmp:HubRepairTime
!        jobe:POPType    = tmp:POPType
!        jobe:EndUserTelNo   = tmp:EndUserTelNo
!        Access:JOBSE.TryUpdate()
!    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!        !Error
!    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

Validate_Motorola   ROUTINE

!   IF job:POP = ''
!     Glo:Select1 = ''
!   ELSE
!     Glo:Select1 = job:pop
!   END
!
!   IF job:dop = ''
!     Glo:Select2 = ''
!   ELSE
!     Glo:Select2 = job:dop
!   END
!
!   !Glo:Select2 = ''
!   glo:Select3 = 'NONE'
!
!   POP_Validate2
!
!   tmp:POPType = glo:Select3
!
!   IF Glo:Select1 = ''
!     !Nothing done!
!     job:Warranty_Job = ''
!     job:Warranty_Charge_type = ''
!   ELSE
!     job:POP = Glo:Select1
!     IF Glo:Select1 = 'F'
!       job:Warranty_Job = 'YES'
!       Post(Event:Accepted,?job:Warranty_Job)
!       job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
!       !Post(Event:Accepted,?job:Warranty_Charge_Type)
!       job:Chargeable_Job = ''
!       Post(Event:Accepted,?job:Chargeable_Job)
!       job:Charge_Type = ''
!       ENABLE(?Job:Chargeable_Job)
!       ENABLE(?Job:Warranty_Job)
!       Job:DOP = Glo:Select2
!       Post(Event:Accepted,?Job:DOP)
!       UPDATE()
!       DISPLAY()
!     END
!
!     IF Glo:Select1 = 'S'
!       job:Warranty_Job = 'YES'
!       Post(Event:Accepted,?job:Warranty_Job)
!       job:Warranty_Charge_Type = 'WARRANTY (2ND YR)'
!       !Post(Event:Accepted,?job:Warranty_Charge_Type)
!       job:Chargeable_Job = ''
!       Post(Event:Accepted,?job:Chargeable_Job)
!       job:Charge_Type = ''
!       ENABLE(?Job:Chargeable_Job)
!       ENABLE(?Job:Warranty_Job)
!       Job:DOP = Glo:Select2
!       Post(Event:Accepted,?Job:DOP)
!       UPDATE()
!       DISPLAY()
!     END
!
!     IF Glo:Select1 = 'O'
!       job:Warranty_Job = 'YES'
!       Post(Event:Accepted,?job:Warranty_Job)
!       job:Warranty_Charge_Type = 'WARRANTY (OBF)'
!       !Post(Event:Accepted,?job:Warranty_Charge_Type)
!       job:Chargeable_Job = ''
!       Post(Event:Accepted,?job:Chargeable_Job)
!       job:Charge_Type = ''
!       ENABLE(?Job:Chargeable_Job)
!       ENABLE(?Job:Warranty_Job)
!       Job:DOP = Glo:Select2
!       Post(Event:Accepted,?Job:DOP)
!       UPDATE()
!       DISPLAY()
!     END
!
!     IF Glo:Select1 = 'C'
!       job:Warranty_Job = 'NO'
!       Post(Event:Accepted,?job:Warranty_Job)
!       job:Chargeable_Job = 'YES'
!       Post(Event:Accepted,?job:Chargeable_Job)
!       job:Warranty_Charge_Type = ''
!       DISABLE(?Job:Warranty_Job)
!       ENABLE(?Job:Chargeable_Job)
!       !Post(Event:Accepted,?job:Warranty_Charge_Type)
!       job:Charge_Type = 'NON-WARRANTY'
!       !Post(Event:Accepted,?job:Charge_Type)
!       UPDATE()
!       DISPLAY()
!     END
!
!   END
!
!   Glo:Select1 = ''
!   Glo:Select2 = ''
OutFaultList     Routine
    !Some code uncommented! NB 10.12.2003
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = job:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found
! Deleting (DBH 08/11/2007) # 9200 - Not needed
!        If man:UseInvTextForFaults
!            ?OutFaultList{prop:Hide} = 0
!            !?jbn:Invoice_Text{prop:Hide} = 1
!            !?Invoice_Text_Button{prop:Hide} = 1
!            !?jbn:Invoice_Text:Prompt{prop:Hide} = 1
!        Else !If man:UseInvTextForFaults
!            ?OutFaultList{prop:Hide} = 1
!            !?jbn:Invoice_Text{prop:Hide} = 0
!            !?Invoice_Text_Button{prop:Hide} = 0
!            !?jbn:Invoice_Text:Prompt{prop:Hide} = 0
!        End !If man:UseInvTextForFaults
! End (DBH 08/11/2007) #9200
        If man:ForceParts
            !?ChargeableAdjustment{prop:Hide} = 0
            !?WarrantyAdjustment{prop:Hide} = 0
        Else !If man:ForceParts
            !?ChargeableAdjustment{prop:Hide} = 1
            !?WarrantyAdjustment{prop:Hide} = 1
        End !If man:ForceParts
    Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End !If AccessMANUFACT.Tryfetch(manManufacturer_Key) = LevelBenign
CheckRequiredFields     Routine
    If ForceAuthorityNumber('B')
        ?job:Authority_Number{prop:color} = 0BDFFFFH
        ?job:Authority_Number{prop:Req} = 1
    Else !If ForceAuthorityNumber('B')
        ?job:Authority_Number{prop:color} = color:white
        ?job:Authority_Number{prop:Req} = 0
    End !If ForceAuthorityNumber('B')



    If ForceIncomingCourier('B')
        ?job:Incoming_Courier{prop:color} = 0BDFFFFH
!        ?job:incoming_consignment_number{prop:color} = 0BDFFFFH
!        ?job:incoming_date{prop:color} = 0BDFFFFH
        ?job:Incoming_Courier{prop:Req} = 1
!        ?job:Incoming_Consignment_Number{prop:Req} = 1
!        ?job:Incoming_Date{prop:Req} = 1
    Else !If ForceIncomingCourier('B')
        ?job:Incoming_Courier{prop:color} = color:white
!        ?job:incoming_consignment_number{prop:color} = color:white
!        ?job:incoming_date{prop:color} = color:white
        ?job:Incoming_Courier{prop:Req} = 0
!        ?job:Incoming_Consignment_Number{prop:Req} = 0
!        ?job:Incoming_Date{prop:Req} = 0
    End !If ForceIncomingCourier('B')

    If ForceCourier('B')
        ?job:Courier{prop:color} = 0BDFFFFH
        ?job:Courier{prop:Req} = 1
    Else !If ForceCourier('B')
        ?job:Courier{prop:color} = color:White
        ?job:Courier{prop:Req} = 0
    End !If ForceCourier('B')

    If ForceIncomingCourier('B')
        ?job:Incoming_Courier{prop:color} = 0BDFFFFH
        ?job:Incoming_Courier{prop:Req} = 1
    Else !If ForceIncomingCourier('B')
        ?job:Incoming_Courier{prop:color} = color:White
        ?job:Incoming_Courier{prop:Req} = 0
    End !If ForceIncomingCourier('B')

    If ForceRepairType('B')
!        If ?job:Repair_Type{prop:Hide} = 0
            ?job:Repair_Type{prop:color} = 0BDFFFFH
            ?job:Repair_Type{prop:Req} = 1
!        Else !If ?job:Repair_Type{prop:Hide} = 0
!            ?job:Repair_Type{prop:color} = color:White
!            ?job:Repair_Type{prop:Req} = 0
!        End !If ?job:Repair_Type{prop:Hide} = 0
!        If ?job:Repair_Type_Warranty{prop:Hide} = 0
            ?job:Repair_Type_Warranty{prop:color} = 0BDFFFFH
            ?job:Repair_Type_Warranty{prop:Req} = 1
!        Else !If ?job:Repair_Type_Warranty{prop:Hide} = 0
!            ?job:Repair_Type_Warranty{prop:color} = color:White
!            ?job:Repair_Type_Warranty{prop:Req} = 0
!        End !If ?job:Repair_Type_Warranty{prop:Hide} = 0
    Else !If ForceRepairType('B')
        ?job:Repair_Type{prop:color} = color:White
        ?job:Repair_Type{prop:Req} = 0
        ?job:Repair_Type_Warranty{prop:color} = color:White
        ?job:Repair_Type_Warranty{prop:Req} = 0
    End !If ForceRepairType('B')
AddEngineer     Routine
!    Access:USERS.Clearkey(use:User_Code_Key)
!    use:User_Code   = job:Engineer
!    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!        !Found
!        Engineer_Name_temp  = Clip(Use:Forename) & ' ' & Clip(use:Surname)
!        tmp:SkillLevel   = use:SkillLevel
!    Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!    End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!
!    Do_Status# = 0
!
!    If job:Engineer <> Engineer_Temp
!        IF job:Date_Completed <> ''
!            Error# = 1
!            Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
!                           'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!            End!Case MessageEx
!        End !IF job:Date_Completed <> ''
!        If job:Invoice_Number <> ''
!            Error# = 1
!            Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
!                           'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!            End!Case MessageEx
!
!        End !If job:Invoice_Number <> ''
!        If Error# = 0
!            If Engineer_Temp = ''
!                get(audit,0)
!                if access:audit.primerecord() = level:benign
!                    aud:Notes         = Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel)
!                    aud:ref_number    = job:ref_number
!                    aud:date          = today()
!                    aud:time          = clock()
!                    aud:type          = 'JOB'
!                    access:users.clearkey(use:password_key)
!                    use:password =glo:password
!                    access:users.fetch(use:password_key)
!                    aud:user = use:user_code
!                    aud:action        = 'ENGINEER ALLOCATED: ' & CLip(job:engineer)
!                    access:audit.insert()
!                end!�if access:audit.primerecord() = level:benign
!                do_status# = 1
!                engineer_temp = job:engineer
!
!                If Access:JOBSENG.PrimeRecord() = Level:Benign
!                    joe:JobNumber     = job:Ref_Number
!                    joe:UserCode      = job:Engineer
!                    joe:DateAllocated = Today()
!                    joe:TimeAllocated = Clock()
!                    joe:AllocatedBy   = use:User_Code
!                    access:users.clearkey(use:User_Code_Key)
!                    use:User_Code   = job:Engineer
!                    access:users.fetch(use:User_Code_Key)
!
!                    joe:EngSkillLevel = use:SkillLevel
!                    joe:JobSkillLevel = use:SkillLevel
!
!                    If Access:JOBSENG.TryInsert() = Level:Benign
!                        !Insert Successful
!                    Else !If Access:JOBSENG.TryInsert() = Level:Benign
!                        !Insert Failed
!                    End !If Access:JOBSENG.TryInsert() = Level:Benign
!                End !If Access:JOBSENG.PrimeRecord() = Level:Benign
!
!            Else !If Engineer_Temp = ''
!                Case MessageEx('Are you sure you want to change the Engineer?','ServiceBase 2000',|
!                               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                    Of 1 ! &Yes Button
!
!                        get(audit,0)
!                        if access:audit.primerecord() = level:benign
!                            aud:notes         = 'NEW ENGINEER: ' & CLip(job:Engineer) & |
!                                                '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
!                                                '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(engineer_temp)
!                            aud:ref_number    = job:ref_number
!                            aud:date          = today()
!                            aud:time          = clock()
!                            aud:type          = 'JOB'
!                            access:users.clearkey(use:password_key)
!                            use:password =glo:password
!                            access:users.fetch(use:password_key)
!                            aud:user = use:user_code
!                            aud:action        = 'ENGINEER CHANGED TO ' & CLip(job:engineer)
!                            access:audit.insert()
!                        end!�if access:audit.primerecord() = level:benign
!                        Engineer_Temp   = job:Engineer
!
!                        If Access:JOBSENG.PrimeRecord() = Level:Benign
!                            joe:JobNumber     = job:Ref_Number
!                            joe:UserCode      = job:Engineer
!                            joe:DateAllocated = Today()
!                            joe:TimeAllocated = Clock()
!                            joe:AllocatedBy   = use:User_Code
!                            access:users.clearkey(use:User_Code_Key)
!                            use:User_Code   = job:Engineer
!                            access:users.fetch(use:User_Code_Key)
!
!                            joe:EngSkillLevel = use:SkillLevel
!                            joe:JobSkillLevel = jobe:SkillLevel
!
!                            If Access:JOBSENG.TryInsert() = Level:Benign
!                                !Insert Successful
!                            Else !If Access:JOBSENG.TryInsert() = Level:Benign
!                                !Insert Failed
!                            End !If Access:JOBSENG.TryInsert() = Level:Benign
!                        End !If Access:JOBSENG.PrimeRecord() = Level:Benign
!
!                    Of 2 ! &No Button
!                        job:Engineer    = Engineer_Temp
!                End!Case MessageEx
!            End !If Engineer_Temp = ''
!        Else !If Error# = 0
!            job:Engineer    = engineer_temp
!        End !If Error# = 0
!
!    End !If job:Engineer <> Engineer_Temp
!
!    Access:USERS.Clearkey(use:User_Code_Key)
!    use:User_Code   = job:Engineer
!    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!        !Found
!        Engineer_Name_temp  = Clip(Use:Forename) & ' ' & Clip(use:Surname)
!        tmp:SkillLevel   = use:SkillLevel
!    Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!    End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!
!    If do_status# = 1
!        GetStatus(310,thiswindow.request,'JOB') !allocated to engineer
!        Do time_remaining
!    End!If do_status# = 1
!
!    Do Update_Engineer
AreThereNotes       Routine
    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:Ref_Number
    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Found
    Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
CountParts      Routine
!Bog off no longer wanted
!    Clear(tmp:PartQueue)
!    Free(tmp:PartQueue)
!
!    Save_par_Id = Access:PARTS.Savefile()
!    Access:PARTS.Clearkey(par:Part_Number_Key)
!    par:Ref_Number  = job:Ref_Number
!    Set(par:Part_Number_Key,par:Part_Number_Key)
!    Loop
!        If Access:PARTS.Next()
!           Break
!        End !If Access:PARTS.Next()
!        If par:Ref_Number  <> job:Ref_Number
!            Break
!        End!If par:Ref_Number  <> job:Ref_Number
!
!        tmp:PartNumber  = par:Part_Number
!        Get(tmp:PartQueue,tmp:PartNumber)
!        If Error()
!            tmp:PartNumber  = par:Part_Number
!            Add(tmp:PartQueue)
!        End!If Error()
!    End !loop
!    Access:PARTS.Restorefile(Save_par_Id)
!
!    ?chargeable_details_tab{prop:text} = '&Chargeable Parts (' & CLip(Records(tmp:Partqueue)) & ')'
!
!    Clear(tmp:PartQueue)
!    Free(tmp:PartQueue)
!
!    save_wpr_id = access:warparts.savefile()
!    access:warparts.clearkey(wpr:part_number_key)
!    wpr:ref_number  = job:ref_number
!    set(wpr:part_number_key,wpr:part_number_key)
!    loop
!        if access:warparts.next()
!           break
!        end !if
!        if wpr:ref_number  <> job:ref_number      |
!            then break.  ! end if
!        tmp:PartNumber  = wpr:part_number
!        Get(tmp:PartQueue,tmp:PartNumber)
!        If Error()
!            tmp:Partnumber  = wpr:Part_number
!            Add(tmp:PartQueue)
!        End!If Error()
!    end !loop
!    access:warparts.restorefile(save_wpr_id)
!
!    ?warranty_details_tab{prop:text} = '&Warranty Parts (' & CLip(Records(tmp:Partqueue)) & ')'
!    Clear(tmp:PartQueue)
!    Free(tmp:PartQueue)
!
!    save_epr_id = access:estparts.savefile()
!    access:estparts.clearkey(epr:part_number_key)
!    epr:ref_number  = job:ref_number
!    set(epr:part_number_key,epr:part_number_key)
!    loop
!        if access:estparts.next()
!           break
!        end !if
!        if epr:ref_number  <> job:ref_number      |
!            then break.  ! end if
!        tmp:PartNumber  = epr:part_number
!        Get(tmp:PartQueue,tmp:PartNumber)
!        If Error()
!            tmp:Partnumber  = epr:Part_number
!            Add(tmp:PartQueue)
!        End!If Error()
!    end !loop
!    access:estparts.restorefile(save_epr_id)
!
!    ?estimate_details_tab{prop:text} = '&Estimate Parts (' & CLip(Records(tmp:Partqueue)) & ')'
!    Clear(tmp:PartQueue)
!    Free(tmp:PartQueue)
!
!
CityLink_Service        Routine
    Disable(?job:jobservice)
    access:courier.clearkey(cou:courier_key)
    cou:courier = job:courier
    If access:courier.tryfetch(cou:courier_key) = Level:Benign
        If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
            check_access('AMEND CITY LINK SERVICE',x")
            if x" = true
                Enable(?job:jobService)
            End!if x" = true
        End!If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
    End!If access:courier.clearkey(cou:courier_key) = Level:Benign

UPS_Service        Routine
    HIDE(?jobe:UPSFlagCode)
    access:courier.clearkey(cou:courier_key)
    cou:courier = job:incoming_courier
    If access:courier.tryfetch(cou:courier_key) = Level:Benign
        If cou:courier_type = 'UPS'
            !check_access('AMEND CITY LINK SERVICE',x")
            !if x" = true
                UNHIDE(?jobe:UPSFlagCode)
            !End!if x" = true
        End!If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
    End!If access:courier.clearkey(cou:courier_key) = Level:Benign
stock_history       Routine        !Do The Prime, and Set The Quantity First
Skip_Despatch       Routine

Check_For_Despatch        Routine
!    Set(defaults)
!    access:defaults.next()
!
!    despatch# = 0
!
!    if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
!        despatch# = 1
!    Else!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
!        access:subtracc.clearkey(sub:account_number_key)
!        sub:account_number = job:account_number
!        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
!            access:tradeacc.clearkey(tra:account_number_key)
!            tra:account_number = sub:main_account_number
!            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!                If tra:use_sub_accounts = 'YES'
!                    If job:chargeable_job = 'YES'
!                        If sub:despatch_invoiced_jobs <> 'YES' And sub:despatch_paid_jobs <> 'YES'
!                            despatch# = 1
!                        End!If sub:despatch_invoiced_jobs <> 'YES'
!                    Else!If job:chargeable_job = 'YES'
!                        despatch# = 1
!                    End!If job:chargeable_job = 'YES'
!                Else!If tra:use_sub_accounts = 'YES'
!                    If job:chargeable_job = 'YES'
!                        If tra:despatch_invoiced_jobs <> 'YES' And tra:despatch_paid_jobs <> 'YES'
!                            despatch# = 1
!                        End!If sub:despatch_invoiced_jobs <> 'YES'
!                    Else!If job:chargeable_job = 'YES'
!                        despatch# = 1
!                    End!If job:chargeable_job = 'YES'
!                End!If tra:use_sub_accounts = 'YES'
!            end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
!    End!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
!
!    If despatch# = 1
!        tmp:DespatchClose = 0
!        If job:despatched <> 'YES'
!            If ToBeLoaned() = 1
!                restock# = 0
!                If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
!                    Case MessageEx('A Loan Unit has been issued to this job but the Initial Transit Type has been set-up so that an Exchange Unit is required.<13,10><13,10>Do you wish to continue and mark this uni for Despatch Back to the Customer, or do you want to Restock it?','ServiceBase 2000',|
!                                   'Styles\question.ico','|&Continue|&Restock Unit',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                        Of 1 ! &Continue Button
!                        Of 2 ! &Restock Unit Button
!                            ForceDespatch()
!                            restock# = 1
!                    End!Case MessageEx
!                End!If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
!                If restock# = 0
!                    job:despatched  = 'LAT'
!                    job:despatch_Type = 'JOB'
!                End!If restock# = 0
!            Else!If ToBeLoaned() = 1
!                If ToBeExchanged()
!                    ForceDespatch()
!                Else!If ToBeExchanged()
!                    job:date_despatched = DespatchANC(job:courier,'JOB')
!                    access:courier.clearkey(cou:courier_key)
!                    cou:courier = job:courier
!                    if access:courier.tryfetch(cou:courier_key) = Level:benign
!                        If cou:despatchclose = 'YES'
!                            tmp:despatchclose = 1
!                        Else!If cou:despatchclose = 'YES'
!                            tmp:despatchclose = 0
!                        End!If cou:despatchclose = 'YES'
!                    End!if access:courier.tryfetch(cou:courier_key) = Level:benign
!                End!If ToBeExchanged()
!            End!If ToBeLoaned() = 1
!        End!If job:despatched <> 'YES'
!    End
Transit_Type_Bit        Routine
!    Set(DEFAULTS)
!    Access:DEFAULTS.Next()
!    access:trantype.clearkey(trt:transit_type_key)
!    trt:transit_type = job:transit_type
!    if access:trantype.fetch(trt:transit_type_key) = Level:Benign
!        error# = 0
!        access:subtracc.clearkey(sub:account_number_key)
!        sub:account_number = job:account_number
!        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
!            access:tradeacc.clearkey(tra:account_number_key)
!            tra:account_number = sub:main_account_number
!            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!                If trt:loan_unit    = 'YES' And tra:allow_loan <> 'YES'
!                    error# = 1
!                    Case MessageEx('Loan Units are not authorised for this Trade Account.<13,10><13,10>Select another Initial Transit Type or amend the Trade Account defaults.','ServiceBase 2000',|
!                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                        Of 1 ! &OK Button
!                    End!Case MessageEx
!                    job:transit_type    = transit_type_temp
!                End!If trt:loan_unit    = 'YES' And tra:allow_loan <> 'YES'
!                If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES' And error# = 0
!                    Case MessageEx('Exchange Units are not authorised for this Trade Account.<13,10><13,10>Select another Initial Transit Type or amend the Trade Account defaults.','ServiceBase 2000',|
!                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                        Of 1 ! &OK Button
!                    End!Case MessageEx
!                    job:transit_Type    = transit_type_temp
!                    error# = 1
!                End!If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES'
!            end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
!        If error# = 0
!
!            access:trantype.clearkey(trt:transit_type_key)
!            trt:transit_type = job:transit_type
!            If access:trantype.fetch(trt:transit_type_key) = Level:Benign
!                hide# = 0
!                If trt:collection_address <> 'YES'
!                    If job:address_line1 <> ''
!                        Case MessageEx('The selected Transit Type will remove the Collection Address.<13,10><13,10>Are you sure?','ServiceBase 2000',|
!                                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                            Of 1 ! &Yes Button
!
!                                transit_type_temp   = job:transit_type
!                                hide# = 1
!                            Of 2 ! &No Button
!
!                                job:transit_type = transit_type_temp
!                        End!Case MessageEx
!                    Else
!                        hide# = 1
!                    End
!                    If hide# = 1
!                        job:postcode         = ''
!                        job:company_name     = ''
!                        job:address_line1    = ''
!                        job:address_line2    = ''
!                        job:address_line3    = ''
!                        job:telephone_number = ''
!                        job:fax_number       = ''
!                        Hide(?collection_address_group)
!                    End
!                Else
!                    Unhide(?collection_address_group)
!                End
!                hide# = 0
!                If trt:delivery_address <> 'YES'
!                    If job:address_line1_delivery <> ''
!                        Case MessageEx('The selected Transit Type will remove the Delivery Address.<13,10><13,10>Are you sure?','ServiceBase 2000',|
!                                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                            Of 1 ! &Yes Button
!
!                                transit_type_temp   = job:transit_type
!                                hide# = 1
!                            Of 2 ! &No Button
!
!                                job:transit_type    = transit_type_temp
!                        End!Case MessageExEnd!Case MessageEx
!                    Else
!                        hide# = 1
!                    End
!                    If hide# = 1
!                        job:postcode_delivery         = ''
!                        job:company_name_delivery     = ''
!                        job:address_line1_delivery    = ''
!                        job:address_line2_delivery    = ''
!                        job:address_line3_delivery    = ''
!                        job:telephone_delivery = ''
!                        Hide(?delivery_address_group)
!                    End
!                Else
!                    Unhide(?delivery_address_group)
!                End
!            end !if
!            If trt:InWorkshop = 'YES'
!                job:workshop = 'YES'
!            Else!If trt:InWorkshop = 'YES'
!                job:workshop = 'NO'
!            End!If trt:InWorkshop = 'YES'
!
!            If ThisWindow.Request = Insertrecord
!                GetStatus(Sub(trt:initial_status,1,3),1,'JOB')
!
!                Do time_remaining
!
!                GetStatus(Sub(trt:Exchangestatus,1,3),1,'EXC')
!
!                GetStatus(Sub(trt:loanstatus,1,3),1,'LOA')
!
!
!                access:subtracc.clearkey(sub:account_number_key)
!                sub:account_number  = job:account_number
!                access:subtracc.fetch(sub:account_number_key)
!                access:tradeacc.clearkey(tra:account_number_key)
!                tra:account_number  = sub:main_account_number
!                access:tradeacc.fetch(tra:account_number_key)
!
!                IF tra:refurbcharge = 'YES' and TRT:Exchange_Unit = 'YES'
!                    job:chargeable_job = 'YES'
!                    job:warranty_job    = 'YES'
!                    job:charge_type = tra:ChargeType
!                    job:Warranty_Charge_Type    = tra:WarChargeType
!                End!IF tra:refurbcharge = 'YES'
!
!                If trt:job_card = 'YES'
!                    tmp:printjobcard    = 1
!                Else!If trt:job_card = 'YES'
!                    tmp:printjobcard    = 0
!                End!If trt:job_card = 'YES'
!                If trt:jobReceipt = 'YES'
!                    tmp:printjobreceipt = 1
!                Else!If trt:job_Receipt = 'YES'
!                    tmp:printjobreceipt = 0
!                End!If trt:job_Receipt = 'YES'
!
!                error# = 0
!                If job:turnaround_time <> '' and job:turnaround_time <> trt:initial_priority
!                    Case MessageEx('The selected Transit Type requires a Turnaround Time of '&Clip(trt:initial_priority)&'.<13,10><13,10>A Turnaround Time has already been assigned to this job. Do you wish to change this?','ServiceBase 2000',|
!                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                        Of 1 ! &Yes Button
!                        Of 2 ! &No Button
!                            error# = 1
!                    End!Case MessageEx
!                End!If job:turnaround_time <> ''
!                If error# = 0
!                    job:turnaround_time = trt:initial_priority
!                    access:turnarnd.clearkey(tur:turnaround_time_key)
!                    tur:turnaround_time = job:turnaround_time
!                    if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
!                        Turnaround_Routine(tur:days,tur:hours,end_date",end_time")
!                        job:turnaround_end_date = Clip(end_date")
!                        job:turnaround_end_time = Clip(end_time")
!                        Do Time_Remaining
!                    end!if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
!                End!If error# = 0
!            End
!        End!If error# = 0
!    end!if access:trantype.fetch(trt:transit_type_key) = Level:Benign
Location_Bit     Routine
        job:location = loi:location
        If location_temp <> ''
    !Add To Old Location
            access:locinter.clearkey(loi:location_key)
            loi:location = location_temp
            If access:locinter.fetch(loi:location_key) = Level:Benign
                If loi:allocate_spaces = 'YES'
                    loi:current_spaces+= 1
                    loi:location_available = 'YES'
                    access:locinter.update()
                End
            end !if
        End!If location_temp <> ''
    !Take From New Location
        access:locinter.clearkey(loi:location_key)
        loi:location = job:location
        If access:locinter.fetch(loi:location_key) = Level:Benign
            If loi:allocate_spaces = 'YES'
                loi:current_spaces -= 1
                If loi:current_spaces< 1
                    loi:current_spaces = 0
                    loi:location_available = 'NO'
                End
                access:locinter.update()
            End
        end !if
        location_temp  = job:location

        If job:date_completed = ''
            If job:engineer <> ''
                GetStatus(310,1,'JOB') !allocated to engineer

                Do time_remaining
            Else!If job:engineer <> ''
                GetStatus(305,1,'JOB') !awaiting allocation

                Do time_remaining
            End!If job:engineer <> ''
        End!If job:date_completed = ''




Show_Hide_Tabs      Routine
!    check_access('JOBS - SHOW COSTS',x")
!    If x" = False
!    Else!If x" = False
!        If job:chargeable_job = 'YES'
!            Unhide(?chargeable_details_tab)
!            Unhide(?repair_type_chargeable_group)
!        Else
!            Hide(?chargeable_details_tab)
!            Hide(?repair_type_chargeable_group)
!        End!If job:chargeable_job = 'YES'
!        If job:warranty_job = 'YES'
!            Unhide(?warranty_details_tab)
!            Unhide(?repair_type_warranty_group)
!        Else
!            Hide(?warranty_details_tab)
!            Hide(?repair_type_warranty_group)
!        End!If job:waranty_job = 'YES'
!        If job:estimate = 'YES' and ?job:Estimate{prop:Hide} = 0
!            If job:Chargeable_job = 'YES'
!                Unhide(?EstimatePrompt)
!
!                Unhide(?estimate_details_tab)
!                IF job:estimate_accepted <> 'YES' AND job:estimate_rejected <> 'YES'
!                    Hide(?chargeable_details_tab)
!                    Unhide(?repair_type_chargeable_group)
!                End
!            Else
!                Hide(?Estimate_Details_Tab)
!                hide(?EstimatePrompt)
!
!            End!If job:Chargeable_job = 'YES'
!        Else
!            Hide(?estimate_details_tab)
!            hide(?EstimatePrompt)
!
!        End!If job:estimate = 'YES'
!    End!If x" = False
!    Thiswindow.reset(1)
Repair_Type_Bit     Routine
!    price_error# = 0
!    no_error# = 0
!    If job:chargeable_job = 'YES'
!        Pricing_Routine('C',labour",parts",pass",claim",handling",exchange",RRCRate",RRCParts")
!        if pass" = False
!            price_error# = 1
!            Case MessageEx('A pricing structure has not been setup for this Repair Type.<13,10><13,10>If you choose to continue you must inform your System Supervisor.<13,10>Are you sure?','ServiceBase 2000',|
!                           'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                Of 1 ! &Yes Button
!                    no_error# = 2
!                Of 2 ! &No Button
!                    no_error# = 0
!            End!Case MessageEx
!        End!if pass" = False
!
!
!        If price_error# = 0
!            If repair_type_temp = ''
!                no_error# = 1
!            Else!If repair_type_temp = ''
!                If job:labour_cost <> labour" Or job:parts_cost <> parts"
!                    Case MessageEx('The selected Repair Type has a different pricing structure. If you choose to continue this job will be re-valued accordingly.<13,10><13,10>Are you sure?','ServiceBase 2000',|
!                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                        Of 1 ! &Yes Button
!                            no_error# = 3 !pricing Change
!                        Of 2 ! &No Button
!                            no_error# = 0
!                    End!Case MessageEx
!                Else!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
!                    Case MessageEx('Are you sure you want to change the Repair Type?','ServiceBase 2000',|
!                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                        Of 1 ! &Yes Button
!                             no_error# = 2 !Repair Type Change Only
!                        Of 2 ! &No Button
!                             no_error# = 0
!                    End!Case MessageEx
!                End!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
!            End!If repair_type_temp = ''
!        End!price_error# = 0
!
!        If no_error# = 0
!            job:repair_type = repair_type_temp
!        Else!If no_error# = 0
!            Case no_error#
!                Of 2 orof 3
!                    get(audit,0)
!                    aud:ref_number = job:ref_number
!                    aud:date       = Today()
!                    aud:time       = Clock()
!                    access:users.clearkey(use:password_key)
!                    use:password =glo:password
!                    access:users.fetch(use:password_key)
!                    aud:user = use:user_code
!            End!Case no_error#
!            Case no_error#
!                Of 2
!                    aud:notes      = 'NEW CHARGEABLE REPAIR TYPE: ' & Clip(job:repair_type) & '<13,10>OLD CHARGEABLE REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)
!                    aud:action     = 'CHARGEABLE REPAIR TYPE CHANGED: ' & Clip(job:repair_type)
!                    access:audit.insert()
!                Of 3
!                    aud:notes      = 'NEW CHARGEABLE REPAIR TYPE: ' & Clip(job:repair_type) & '<13,10>OLD CHARGEABLE REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)
!                    aud:action     = 'REPRICE JOB - CHARGEABLE REPAIR TYPE CHANGED: ' & Clip(job:repair_type)
!                    access:audit.insert()
!            End
!            If no_error# = 1 Or no_error# = 3
!                Do pricing
!            End!If no_error# = 1 Or no_error# = 3
!            repair_type_temp    = job:repair_type
!
!            Do third_party_repairer
!        End!If no_error# = 0
!    End!If job:chargeable_job = 'YES'
Repair_Type_Warranty_Bit     Routine
!    price_error# = 0
!    no_error# = 0
!    If job:warranty_job = 'YES'
!        Pricing_Routine('W',labour",parts",pass",claim",handling",exchange",RRCRate",RRCParts")
!        if pass" = False
!            price_error# = 1
!            Case MessageEx('A pricing structure has not been setup for this Repair Type.<13,10><13,10>If you choose to continue you must inform your System Supervisor immediately.<13,10>Are you sure?','ServiceBase 2000',|
!                           'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                Of 1 ! &Yes Button
!                    no_error# = 2
!                Of 2 ! &No Button
!                    no_error# = 0
!            End!Case MessageEx
!        End!if pass" = False
!
!
!        If price_error# = 0
!            If repair_type_temp = ''
!                no_error# = 1
!            Else!If repair_type_temp = ''
!                If job:labour_cost_warranty <> labour" Or job:parts_cost_warranty <> parts"
!                    Case MessageEx('The selected Repair Type has a different pricing structure.<13,10><13,10>If you choose to continue this job will be re-valued accordingly.<13,10>Are you sure?','ServiceBase 2000',|
!                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                        Of 1 ! &Yes Button
!                            no_error# = 3 !pricing Change
!                        Of 2 ! &No Button
!                            no_error# = 0
!                    End!Case MessageEx
!                Else!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
!                    Case MessageEx('Are you sure you want to change the Repair Type?','ServiceBase 2000',|
!                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                        Of 1 ! &Yes Button
!                             no_error# = 2 !Repair Type Change Only
!                        Of 2 ! &No Button
!                             no_error# = 0
!                    End!Case MessageEx
!                End!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
!            End!If repair_type_temp = ''
!        End!price_error# = 0
!
!        If no_error# = 0
!            job:repair_type_warranty = repair_type_temp
!        Else!If no_error# = 0
!            Case no_error#
!                Of 2 orof 3
!                    get(audit,0)
!                    aud:ref_number = job:ref_number
!                    aud:date       = Today()
!                    aud:time       = Clock()
!                    access:users.clearkey(use:password_key)
!                    use:password =glo:password
!                    access:users.fetch(use:password_key)
!                    aud:user = use:user_code
!            End!Case no_error#
!            Case no_error#
!                Of 2
!                    aud:notes      = 'NEW WARRANTY REPAIR TYPE: ' & Clip(job:repair_type_warranty) & '<13,10>OLD WARRANTY REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)
!                    aud:action     = 'WARRANTY REPAIR TYPE CHANGED: ' & Clip(job:repair_type_warranty)
!                    access:audit.insert()
!                Of 3
!                    aud:notes      = 'NEW WARRANTY REPAIR TYPE: ' & Clip(job:repair_type_warranty) & '<13,10>OLD WARRANTY REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)
!                    aud:action     = 'REPRICE JOB - WARRANTY REPAIR TYPE CHANGED: ' & Clip(job:repair_type_warranty)
!                    access:audit.insert()
!            End
!            If no_error# = 1 Or no_error# = 3
!                Do pricing
!            End!If no_error# = 1 Or no_error# = 3
!            repair_type_temp    = job:repair_type_warranty
!
!            Do third_party_repairer
!        End!If no_error# = 0
!    End!If job:chargeable_job = 'YES'
Trade_Account_Lookup_Bit        Routine
Check_Parts     Routine
    If job:Date_Completed = ''
        !Only change Status if job is NOT completed.

        found_requested# = 0
        found_ordered# = 0
        found_received# = 0
        setcursor(cursor:wait)
        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number  <> job:ref_number      |
                then break.  ! end if
            If wpr:pending_ref_number <> ''
                found_requested# = 1
                Break
            End
            If wpr:order_number <> ''
                If wpr:date_received = ''
                    found_ordered# = 1
                    Break
                Else!If wpr:date_received = ''
                    found_received# = 1
                    Break
                End!If wpr:date_received = ''
            End
        end !loop
        access:warparts.restorefile(save_wpr_id)
        setcursor()

        If found_requested# = 0
            setcursor(cursor:wait)
            save_par_id = access:parts.savefile()
            access:parts.clearkey(par:part_number_key)
            par:ref_number  = job:ref_number
            set(par:part_number_key,par:part_number_key)
            loop
                if access:parts.next()
                   break
                end !if
                if par:ref_number  <> job:ref_number      |
                    then break.  ! end if
                If par:pending_ref_number <> ''
                    found_requested# = 1
                    Break
                End
                If par:order_number <> ''
                    If par:date_received = ''
                        found_ordered# = 1
                        Break
                    Else!If par:date_received = ''
                        found_received# = 1
                        Break
                    End!If par:date_received = ''
                End
            end !loop
            access:parts.restorefile(save_par_id)
            setcursor()
        End!If found_requested# = 0

        If found_requested# = 1
            GetStatus(330,thiswindow.request,'JOB') !spares requested

            Do time_remaining

        Else!If found_requested# = 1
            If found_ordered# = 1
                GetStatus(335,thiswindow.request,'JOB') !spares ordered
                Do time_remaining
            Else!If found_ordered# = 1
                If found_received# = 1
                    GetStatus(345,thiswindow.request,'JOB') !spares received

                    Do time_remaining
                End!If found_received# = 1
            End!If found_ordered# = 1
        End!If found_requested# = 1

    End !If job:Date_Completed = ''

    Display()

Trade_Account_Status        Routine  ! Trade Account Status Display
!    access:subtracc.clearkey(sub:account_number_key)
!    sub:account_number = job:account_number
!    if access:subtracc.fetch(sub:account_number_key) = Level:Benign
!        access:tradeacc.clearkey(tra:account_number_key)
!        tra:account_number = sub:main_account_Number
!        if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!            If tra:invoice_sub_accounts = 'YES'
!                If sub:stop_account = 'YES'
!                    If sub:allow_cash_sales = 'YES'
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP - ALLOW CASH TRANSACTIONS ONLY'
!                    Else
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP'
!                    End
!                End
!
!            Else !If tra:invoice_sub_accounts = 'YES'
!                If tra:stop_account = 'YES'
!                    If tra:allow_cash_sales = 'YES'
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP - ALLOW CASH TRANSACTIONS ONLY'
!                    Else
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP'
!                    End
!                End
!            End !If tra:invoice_sub_accounts = 'YES'
!
!            If tra:use_contact_name = 'YES'
!            End
!        end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!    end !if access:subtracc.fetch(sub:account_number_key) = Level:Benign
Show_Tabs       Routine   !Loan Exchange Tabs



Estimate_Check      Routine     !Estiamte Status Display
Check_Msn       Routine   !Does Manufacturer Use MSN Number
    esn_fail_temp = 'NO'
    msn_fail_temp = 'NO'

    If CheckLength('IMEI',job:Model_Number,job:ESN)
        ESN_Fail_Temp = 'YES'
        job:ESN = ''
    End !If CheckLength('IMEI',job:Model_Number,job:ESN)


Titles      Routine     !Show Date Booked etc.
    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    access:webjob.clearkey(wob:RefNumberKey)
    wob:RefNumber = Job:Ref_number
    if access:webjob.fetch(wob:refNumberKey) then
        !error not found in web job
        tmp:JobNumber = job:ref_number
    Else
        If glo:WebJob
            access:tradeacc.clearkey(tra:Account_Number_Key)
            tra:Account_Number = ClarioNET:Global.Param2
            access:tradeacc.fetch(tra:Account_Number_Key)
            tmp:JobNumber = wob:refnumber & '-' & tra:BranchIdentification & wob:JobNumber
        Else !If glo:WebJob
            access:tradeacc.clearkey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            access:tradeacc.fetch(tra:Account_Number_Key)
            tmp:JobNumber = wob:refnumber & '-' & tra:BranchIdentification & wob:JobNumber
        End !If glo:WebJob
    End !if


    ! Check if handset is exchange repair
    save_xch_ali_id = Access:EXCHANGE_ALIAS.SaveFile()
    Access:EXCHANGE_ALIAS.ClearKey(xch_ali:ESN_Only_Key)
    xch_ali:ESN = job:ESN
    if Access:EXCHANGE_ALIAS.Fetch(xch_ali:ESN_Only_Key) = Level:Benign
        ! if exchange unit is in repair / exchange repair display on title
        if (xch_ali:Available = 'REP') or (xch_ali:Available = 'INR')
        end
    end
    Access:EXCHANGE_ALIAS.RestoreFile(save_xch_ali_id)

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
!            If tra:Use_Sub_Accounts = 'YES'
!                ?Title_Text{prop:Text} = Clip(?Title_Text{prop:Text}) & '  Head Acc: ' & Clip(tra:Account_Number)
!            End !If tra:Use_Sub_Accounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign



    Display()

    If job:date_completed <> ''
        job:completed = 'YES'
    Else
        job:completed = 'NO'
    End

    trade_account_string_temp = Clip(job:account_number) & ' - ' & Clip(job:company_name)
    If job:title <> '' and job:initial <> ''
        customer_name_String_temp = Clip(job:title) & ' ' & Clip(job:initial) & ' ' & Clip(job:surname)
    End!If job:title <> '' and job:initial <> ''
    If job:title = '' And job:initial <> ''
        customer_name_string_temp = Clip(job:initial) & ' ' & Clip(job:Surname)
    End!If job:title = '' And job:initial <> ''
    If job:title <> '' And job:initial = ''
        customer_name_String_temp = Clip(job:title) & ' ' & Clip(job:surname)
    End!If job:title <> '' And job:initial = ''
    If job:title = '' And job:initial = ''
        customer_name_string_temp = Clip(job:surname)
    End!If job:title = '' And job initial = ''



    !IMEIs
    !Exchange Unit
    tmp:ExchangeIMEI   = 'N/A'
    tmp:ExchangeMSN    = 'N/A'
    If job:Exchange_Unit_Number <> 0
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            !Found
            tmp:ExchangeIMEI   = xch:ESN
            tmp:ExchangeMSN    = xch:MSN
        Else!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
    End !If job:Exchange_Unit_Number <> 0

    !Third Party
    tmp:IncomingIMEI    = job:Esn
    tmp:IncomingMSN     = job:Msn
    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
    jot:RefNumber = job:Ref_Number
    Set(jot:RefNumberKey,jot:RefNumberKey)
    If Access:JOBTHIRD.NEXT() = Level:Benign
        If job:Ref_Number = jot:RefNumber
            tmp:IncomingIMEI    = jot:OriginalIMEI
            tmp:IncomingMSN     = jot:OriginalMSN
            !Not implimented yet
        End !If job:Ref_Number = jot:RefNumber
    End !If Access:JOBTHIRD.NEXT() = Level:Benign
    If tmp:IncomingIMEI = ''
        tmp:IncomingIMEI = 'N/A'
    End !If tmp:IncomingIMEI = ''
    If tmp:IncomingMSN  = ''
        tmp:IncomingMSN = 'N/A'
    End !If tmp:IncomingMSN  = ''


    !Estimate Details
Totals      Routine   !Work Out The Financial Stuff

    Total_Price('C',vat",total",balance")
    job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
    total_temp = total"
    vat_chargeable_temp = vat"
    balance_due_temp = balance"

    Total_Price('E',vat",total",balance")
    job:sub_total_estimate = job:labour_cost_estimate + job:parts_cost_estimate + job:courier_cost_estimate
    total_estimate_temp = total"
    vat_estimate_temp = vat"

    Total_Price('W',vat",total",balance")
    job:sub_total_warranty  = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
    vat_warranty_temp = vat"
    total_warranty_temp = total"
    balance_due_warranty_temp = balance"
    Display()

Update_Engineer    Routine  !Engineers Full Name
    !Engineer
    Save_joe_ID = Access:JOBSENG.SaveFile()
    Access:JOBSENG.ClearKey(joe:UserCodeKey)
    joe:JobNumber     = job:Ref_Number
    joe:UserCode      = job:Engineer
    joe:DateAllocated = Today()
    Set(joe:UserCodeKey,joe:UserCodeKey)
    Loop
        If Access:JOBSENG.PREVIOUS()
           Break
        End !If
        If joe:JobNumber <> job:Ref_Number
            Break
        End !If joe:JobNumber <> job:Ref_Number

        If joe:UserCode      <> job:Engineer      |
        Or joe:DateAllocated > Today()      |
            Then Break.  ! End If
        tmp:SkillLevel      = joe:EngSkillLevel
        tmp:DateAllocated   = joe:DateAllocated
        Break
    End !Loop
    Access:JOBSENG.RestoreFile(Save_joe_ID)

    Access:users.Clearkey(use:user_code_key)
    use:user_code    = job:engineer
    If Access:users.Fetch(use:user_Code_Key) = Level:Benign
        engineer_name_temp   = Clip(use:forename) & ' ' & Clip(use:surname)
!        tmp:SkillLevel  = use:SkillLevel
    End
    Display()
Update_Loan     Routine     !Sort Out Loan And Exchange Information
    If job:loan_unit_number = ''
        job:loan_user = ''
        loan_model_make_temp = ''
        loan_esn_temp = ''
        loan_msn_temp = ''
        Loan_Accessories_Temp = ''
    Else
        count# = 0
        setcursor(cursor:wait)
        save_lac_id = access:loanacc.savefile()
        access:loanacc.clearkey(lac:ref_number_key)
        lac:ref_number = loa:ref_number
        set(lac:ref_number_key,lac:ref_number_key)
        loop
            if access:loanacc.next()
               break
            end !if
            if lac:ref_number <> loa:ref_number      |
                then break.  ! end if
            count# += 1
        end !loop
        access:loanacc.restorefile(save_lac_id)
        setcursor()

        If count# = 0
            Loan_Accessories_Temp = ''
        End!If count# = 0
        If count# > 1
            loan_accessories_temp = count#
        End!If count# > 1

        access:loan.clearkey(loa:ref_number_key)
        loa:ref_number = job:loan_unit_number
        if access:loan.fetch(loa:ref_number_key) = Level:Benign
            loan_model_make_temp    = Clip(loa:ref_number) & ': ' & Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
            loan_esn_temp           = loa:esn
            loan_msn_temp           = loa:msn
        end !if access:loan.fetch(loa:ref_number_key) = Level:Benign
    End

Update_Exchange     Routine
    If job:exchange_unit_number = ''
        job:exchange_user = ''
        exchange_model_make_temp = ''
        exchange_esn_temp = ''
        exchange_msn_temp = ''
        exchange_Accessories_Temp = ''
    End
    count# = 0
    save_jea_id = access:jobexacc.savefile()
    access:jobexacc.clearkey(jea:part_number_key)
    jea:job_ref_number = job:Ref_number
    set(jea:part_number_key,jea:part_number_key)
    loop
        if access:jobexacc.next()
           break
        end !if
        if jea:job_ref_number <> job:ref_number      |
            then break.  ! end if
        count# += 1
        Exchange_Accessories_Temp = Clip(jea:part_number) & '-' & Clip(jea:description)
    end !loop
    access:jobexacc.restorefile(save_jea_id)

    If count# = 0
        Exchange_Accessories_Temp = ''
    End!If count# = 0
    If count# > 1
        Exchange_Accessories_temp = count#
    End!If count# > 1
    access:exchange.clearkey(xch:ref_number_key)
    xch:ref_number = job:exchange_unit_number
    if access:exchange.fetch(xch:ref_number_key) = Level:Benign
        exchange_model_make_temp    = Clip(job:exchange_unit_number) & ': ' & Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
        exchange_esn_temp           = xch:esn
        exchange_msn_temp           = xch:msn
    end !if access:exchange.fetch(xch:ref_number_key) = Level:Benign

Count_accessories       Routine
    exchange_accessory_count_temp = 0
    save_jea_id = access:jobexacc.savefile()
    access:jobexacc.clearkey(jea:part_number_key)
    jea:job_ref_number = job:Ref_number
    set(jea:part_number_key,jea:part_number_key)
    loop
        if access:jobexacc.next()
           break
        end !if
        if jea:job_ref_number <> job:ref_number      |
            then break.  ! end if
        exchange_accessory_count_temp += 1
    end !loop
    access:jobexacc.restorefile(save_jea_id)
Third_Party_Repairer        Routine    !Don't Think This Is Used Anymore
    clear(trm:record)
    trm:model_number = job:model_number
    get(trdmodel,trm:model_number_only_key)
    if errorcode()
       clear(trm:record)
!       Hide(?3rd_Party_Option)
    Else
!        Unhide(?3rd_Party_Option)
    end !if
Repair_Type_Check       Routine      !Third Party Repair Type
Repair_type_Lookup      Routine      !Lookup Repair Type

    saverequest#      = globalrequest
    globalresponse    = requestcancelled
    globalrequest     = selectrecord
    glo:select1    = job:model_number
    browserepairty
    if globalresponse = requestcompleted
        job:repair_type = rep:repair_type
        glo:select1 = ''
        display()
    Else
        glo:select1 = 'END'
    end
    globalrequest     = saverequest#
Repair_type_Warranty_Lookup      Routine      !Lookup Repair Type

    saverequest#      = globalrequest
    globalresponse    = requestcancelled
    globalrequest     = selectrecord
    glo:select1    = job:model_number
    browserepairty
    if globalresponse = requestcompleted
        job:repair_type_warranty = rep:repair_type
        glo:select1 = ''
        display()
    Else
        glo:select1 = 'END'
    end
    globalrequest     = saverequest#
Transit_type        Routine     !Lookup Transit Type
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    Access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = job:transit_type
    If Access:trantype.Fetch(trt:transit_type_key) = Level:Benign
        If trt:collection_address <> 'YES'
            Hide(?collection_address_group)
        Else
            Unhide(?collection_address_group)
        End
        If trt:delivery_address <> 'YES'
            Hide(?delivery_address_group)
        Else
            Unhide(?delivery_address_group)
        End
    end !if
    Display()
Location_Type       Routine     !Hide/Show If Workshop
Charge_Type     Routine      !Is this an estimate charge type
Estimate        Routine       !Hide/Show If Estimate
Update_accessories      Routine
    count# = 0
    setcursor(cursor:wait)
    save_jac_id = access:jobacc.savefile()
    access:jobacc.clearkey(jac:ref_number_key)
    jac:ref_number = job:ref_number
    set(jac:ref_number_key,jac:ref_number_key)
    loop
        if access:jobacc.next()
           break
        end !if
        if jac:ref_number <> job:ref_number      |
            then break.  ! end if
        count# += 1
    end !loop
    access:jobacc.restorefile(save_jac_id)
    setcursor()

    If count# = 1
        access:jobacc.clearkey(jac:ref_number_key)
        jac:ref_number = job:ref_number
        Set(jac:ref_number_key,jac:ref_number_key)
        access:jobacc.next()
        Accessories_Temp = jac:accessory
    Else
        accessories_temp = Clip(count#)
    End

Get_Audit       Routine
    Clear(aud:record)
    aud:ref_number  = job:ref_number
    aud:date        = Today()
    aud:time        = Clock()
    clear(use:record)
    use:password =glo:password
    get(users,use:password_key)
    aud:user        = use:user_code
Time_Remaining          Routine      !Turnaround Time Nightmare
History_Search          Routine !AutoSearching
!    Set(defaults)
!    access:defaults.next()
!    glo:select1 = ''
!    glo:select2 = ''
!    Do History_Search2
!    If glo:select1 <> ''
!        glo:select2 = job:auto_search
!        glo:select12 = ''
!        If ThisWindow.Request <> insertrecord
!            glo:select12 = 'VIEW ONLY'
!            glo:select11 = job:ref_number
!        End
!
!        Auto_Search_Window
!        glo:select11 = ''
!        glo:select12 = ''
!        If ThisWindow.Request = Insertrecord
!            If glo:select3 <> ''
!                access:jobs_alias.clearkey(job_ali:ref_number_key)
!                job_ali:ref_number = glo:select3
!                if access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
!                    access:jobnotes_alias.clearkey(jbn_ali:RefNumberKey)
!                    jbn_ali:RefNUmber   = jbn:RefNumber
!                    access:jobnotes_alias.tryfetch(jbn_ali:RefNumberKey)
!                    If glo:select5 = 'Y'
!                        job:account_number  = job_ali:account_number
!                        job:trade_account_name  = job_ali:trade_account_name
!                        job:postcode         = job_ali:postcode
!                        job:company_name     = job_ali:company_name
!                        job:address_line1    = job_ali:address_Line1
!                        job:address_line2    = job_ali:address_Line2
!                        job:address_line3    = job_ali:address_line3
!                        job:telephone_number = job_ali:telephone_number
!                        job:fax_number       = job_ali:fax_number
!                    End
!                    If glo:select6 = 'Y'
!                        job:postcode_collection         = job_ali:postcode_collection
!                        job:company_name_collection     = job_ali:company_name_collection
!                        job:address_line1_collection    = job_ali:address_Line1_collection
!                        job:address_line2_collection    = job_ali:address_Line2_collection
!                        job:address_line3_collection    = job_ali:address_line3_collection
!                        job:telephone_collection = job_ali:telephone_collection
!                        job:postcode_delivery         = job_ali:postcode_delivery
!                        job:company_name_delivery     = job_ali:company_name_delivery
!                        job:address_line1_delivery    = job_ali:address_Line1_delivery
!                        job:address_line2_delivery    = job_ali:address_Line2_delivery
!                        job:address_line3_delivery    = job_ali:address_line3_delivery
!                        job:telephone_delivery = job_ali:telephone_delivery
!                    End
!                    If glo:select7 = 'Y'
!                        job:model_number = job_ali:model_number
!                        job:mobile_number = job_ali:mobile_number
!                        job:manufacturer    = job_ali:manufacturer
!                        job:unit_type       = job_ali:unit_type
!                        job:esn             = job_ali:esn
!                        job:msn             = job_ali:msn
!                        job:dop             = job_ali:dop
!                    End
!                    If glo:select8 = 'Y'
!                        job:title   = job_ali:title
!                        job:initial = job_ali:initial
!                        job:surname = job_ali:surname
!                    End
!                    access:jobnotes.clearkey(jbn:RefNumberKey)
!                    jbn:RefNumber   = job:Ref_Number
!                    access:jobnotes.tryfetch(Jbn:RefNumberKey)
!                    If glo:select9 = 'Y'
!                        jbn:engineers_notes = jbn_ali:engineers_notes
!                    End
!                    If glo:select10 = 'Y'
!                        jbn:invoice_text    = jbn_ali:invoice_text
!                    End
!                    access:jobnotes.update()
!                    IF glo:select11 = 'Y'
!                    End
!                    job:last_repair_days    = Today() - job_ali:date_booked
!                    Do last_repair_days
!                    If job:last_repair_days < def:warranty_period And glo:select7 = 'Y'
!                        Case MessageEx('This unit was previously repaired within the Internal Warranty Period specified by the System Supervisor.<13,10><13,10>Please check with your Supervisor to ascertain whether this  unit should be repaired FREE OF CHARGE.','ServiceBase 2000',|
!                                       'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
!                            Of 1 ! &OK Button
!                        End!Case MessageEx
!
!! Fill In Engineer's Notes With Previous Jobs
!
!                        setcursor(cursor:wait)
!                        save_job_ali_id = access:jobs_alias.savefile()
!                        access:jobs_alias.clearkey(job_ali:esn_key)
!                        job_ali:esn = job:esn
!                        set(job_ali:esn_key,job_ali:esn_key)
!                        loop
!                            if access:jobs_alias.next()
!                               break
!                            end !if
!                            if job_ali:esn <> job:esn      |
!                                then break.  ! end if
!                            If job_ali:ref_number = job:ref_number
!                                Cycle
!                            End
!                            Jbn:engineers_notes = Clip(jbn:engineers_notes) & '<13,10>Previous Repair: <13,10>Job No: ' & Clip(Format(job_ali:ref_number,@n012))|
!                                                     & ' Date: ' & Clip(Format(job_ali:date_booked,@d6b)) & ' Eng: ' & Clip(job_ali:engineer)
!                            access:jobnotes.update()
!                        end !loop
!                        access:jobs_alias.restorefile(save_job_ali_id)
!                        setcursor()
!                    End
!
!                    Clear(glo:G_Select1)
!                    Clear(glo:G_Select1)
!
!                end !if access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
!            End!If glo:select3 <> ''
!        End
!    Else
!        If ThisWindow.Request = insertrecord
!            glo:select1 = 'NO MATCH'
!            glo:select2 = job:auto_search
!            Hide(?job:last_repair_days)
!            Hide(?job:last_repair_days:prompt)
!            ?history_button{prop:text} = 'No History'
!            ?history_button{prop:fontcolor} = color:red
!            If def:automatic_replicate <> 'YES'
!                New_Job_Screen
!                Case glo:select2
!                    Of 'SURNAME'
!                        job:surname = job:auto_search
!                    Of 'MOBILE NUMBER'
!                        job:mobile_number = job:auto_search
!                    Of 'ESN'
!                        job:esn = job:auto_search
!                        Post(Event:Accepted,?job:ESN)
!                    Of 'MSN'
!                        job:msn = job:auto_search
!                    Of 'POSTCODE'
!                        job:postcode = job:auto_search
!                        Postcode_Routine(job:postcode,job:address_line1,job:address_line2,job:address_line3)
!                        Display()
!                    Of 'SEARCH AGAIN'
!                        Clear(job:auto_search)
!                        Select(?job:auto_search)
!                        Unhide(?job:last_repair_days)
!                        ?history_button{prop:text} = 'History'
!                        ?history_button{prop:text} = color:black
!                        UnHide(?job:last_repair_days:prompt)
!                End
!            Else
!                Case def:automatic_replicate_field
!                    Of 'SURNAME'
!                        job:surname = job:auto_search
!                    Of 'MOBILE NUMBER'
!                        job:mobile_number = job:auto_search
!                    Of 'E.S.N./I.M.E.I.'
!                        job:esn = job:auto_search
!                        Post(Event:Accepted,?job:ESN)
!                    Of 'M.S.N.'
!                        job:msn = job:auto_search
!                    Of 'POSTCODE'
!                        job:postcode = job:auto_search
!                        Postcode_Routine(job:postcode,job:address_line1,job:address_line2,job:address_line3)
!                        Display()
!                End
!            End
!        End
!    End
!    history# = 0
!    Display()
!    Clear(glo:select1)
History_search2     Routine
    glo:select1 = ''
    ! Surname
        clear(job_ali:record, -1)
        job_ali:surname       = job:auto_search
        set(job_ali:surname_key,job_ali:surname_key)
        loop
            next(jobs_alias)
            if errorcode()                       |
               or job_ali:surname       <> job:auto_search      |
               then break.  ! end if
            glo:select1 = 'SURNAME'
            Break
        end !loop
    !Mobile_Number
        If glo:select1 = ''
            clear(job_ali:record)
            job_ali:mobile_number = job:auto_search
            get(jobs_alias,job_ali:mobilenumberkey)
            if errorcode()
               clear(job_ali:record)
            Else
                glo:select1 = 'MOBILE'
            end !if
        End
    !ESN
        If glo:select1 = ''
            clear(job_ali:record)
            job_ali:esn = job:auto_search
            get(jobs_alias,job_ali:esn_key)
            if errorcode()
               clear(job_ali:record)
            Else
                glo:select1 = 'ESN'
            end !if
        End
    !MSN
        If glo:select1 = ''
            clear(job_ali:record)
            job_ali:msn = job:auto_search
            get(jobs_alias,job_ali:msn_key)
            if errorcode()
               clear(job_ali:record)
            Else
                glo:select1 = 'MSN'
            end !if
        End
    !Postcode
Last_Repair_Days        Routine   !Show/Hide Last Repair Days field
!    If job:last_repair_days = 0
!        Hide(?job:last_repair_days)
!        Hide(?job:last_repair_days:prompt)
!    Else
!        UnHide(?job:last_repair_days)
!        UnHide(?job:last_repair_days:prompt)
!    End
!    If job:last_repair_days < def:warranty_Period
!        ?job:last_repair_days{prop:color} = color:red
!        ?job:last_repair_days{prop:fontcolor} = color:yellow
!    End
check_force_fault_codes    Routine
!    force_fault_codes_temp = 'NO'
!    If job:chargeable_job = 'YES'
!        required# = 0
!        access:chartype.clearkey(cha:charge_type_key)
!        cha:charge_type = job:charge_type
!        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
!            If cha:force_warranty = 'YES'
!                force_fault_codes_temp = 'YES'
!            End
!        end !if
!    End!If job:chargeable = 'YES'
!    If job:warranty_job = 'YES' And force_fault_codes_temp <> 'YES'
!        access:chartype.clearkey(cha:charge_type_key)
!        cha:charge_type = job:warranty_charge_type
!        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
!            If cha:force_warranty = 'YES'
!                force_fault_codes_temp = 'YES'
!            End
!        end !if
!    End!If job:warranty_job = 'YES'
Date_Completed      Routine     !When Date Completed Filled in
!!    access:jobnotes.clearkey(jbn:RefNumberKey)
!!    jbn:RefNUmber   = job:ref_number
!!    If access:jobnotes.tryfetch(jbn:RefNumberKey) = Level:Benign
!!        jbn:Fault_Description    = tmp:FaultDescription
!!        jbn:Invoice_Text        = tmp:InvoiceText
!!        jbn:Engineers_notes     = tmp:EngineerNotes
!!        access:jobnotes.update()
!!    End!If access:jobnotes.tryfetch(jbn:RefNumberKey) = Level:Benign
!
!    Include('completed.inc')
!    Do Time_Remaining
!    Do skip_Despatch
!    If error# = 1
!        glo:errortext = 'This job cannot be completed due to the following error(s): <13,10>' & Clip(glo:errortext)
!        Error_Text
!        glo:errortext = ''
!    End!If error# = 1
!
!    brw21.resetsort(1)
!!    loop error_count# = 1 to 50
!!        If error_type_temp[x#] = 1
!!            Case error_count#
!!                OF 1
!!                    ?job:transit_type{prop:req} = 1
!!                OF 2
!!                    ?job:mobile_number{prop:req} = 1
!!                OF 3
!!                    ?job:model_number{prop:req} = 1
!!                OF 4
!!                    ?job:unit_type{prop:req} = 1
!!                OF 5
!!                    ?tmp:FaultDescription{prop:req} = 1
!!                OF 6
!!                    ?job:esn{prop:req} = 1
!!                OF 7
!!                    ?job:incoming_courier{prop:req} = 1
!!                OF 8
!!                    ?job:msn{prop:req} = 1
!!                OF 31
!!                    ?job:courier{prop:req} = 1
!!                OF 10
!!                    ?job:engineer{prop:req} = 1
!!                of 11
!!                    ?tmp:InvoiceText{prop:req} = 1
!!                OF 12
!!                    ?job:repair_type{prop:req} = 1
!!                OF 13
!!                    ?job:repair_Type_warranty{prop:req} = 1
!!                OF 14
!!                    ?job:authority_number{prop:req} = 1
!!                OF 27
!!                OF 28
!!                Of 29
!!
!!                Of 30
!!
!!            End!Case error#
!!        End
!!    End!loop error_count# = 1 to 30
!!    If exchange_update# = 1
!!        If job:exchange_unit_number <> ''
!!            access:exchange.clearkey(xch:ref_number_key)
!!            xch:ref_number = job:exchange_unit_number
!!            if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!!                If xch:audit_number <> ''
!!                    access:excaudit.clearkey(exa:audit_number_key)
!!                    exa:stock_type   = xch:stock_type
!!                    exa:audit_number = xch:audit_number
!!                    if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
!!!Remove Stock Unit
!!                        access:exchange.clearkey(xch:ref_number_key)
!!                        xch:ref_number = exa:stock_unit_number
!!                        if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!!                            get(exchhist,0)
!!                            if access:exchhist.primerecord() = level:benign
!!                                exh:ref_number   = xch:ref_number
!!                                exh:date          = today()
!!                                exh:time          = clock()
!!                                access:users.clearkey(use:password_key)
!!                                use:password =glo:password
!!                                access:users.fetch(use:password_key)
!!                                exh:user = use:user_code
!!                                exh:status        = 'REMOVED FROM AUDIT NUMBER: ' & Clip(xch:audit_number)
!!                                access:exchhist.insert()
!!                            end!if access:exchhist.primerecord() = level:benign
!!                            xch:audit_number = ''
!!                            access:exchange.update()
!!                        end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!!                        access:exchange.clearkey(xch:ref_number_key)
!!                        xch:ref_number = exa:replacement_unit_number
!!                        if access:exchange.fetch(xch:ref_number_key) = Level:benign     !Make new unit available
!!                            xch:available = 'RTS'
!!                            xch:job_number = ''
!!                            access:exchange.update()
!!                            get(exchhist,0)
!!                            if access:exchhist.primerecord() = level:benign
!!                                exh:ref_number   = xch:ref_number
!!                                exh:date          = today()
!!                                exh:time          = clock()
!!                                access:users.clearkey(use:password_key)
!!                                use:password =glo:password
!!                                access:users.fetch(use:password_key)
!!                                exh:user = use:user_code
!!                                exh:status        = 'UNIT AVAILABLE'
!!                                access:exchhist.insert()
!!                            end!if access:exchhist.primerecord() = level:benign
!!                        end!if access:exchange.fetch(xch:ref_number_key) = Level:benign
!!                        exa:stock_unit_number = exa:replacement_unit_number             !Make new unit the stock item
!!                        exa:replacement_unit_number = ''
!!                        access:excaudit.update()
!!                    end!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
!!                Else!
!!                End!If xch:audit_number <> ''
!!            end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!!        End!If job:exchange_unit_number <> ''
!!
!!    End!If exchange_update# = 1
!!END Removing Exchange Unit From Stock, and putting replacement unit as stock item.
!    Display()
QA_Group        Routine
    Hide(?qa_group)
    Set(defaults)
    access:defaults.next()
    If def:qa_required = 'YES' And job:date_completed <> ''
        Unhide(?qa_group)
    End!If def:qa_required = 'YES' And job:date_completed <> ''
    If def:qa_before_complete = 'YES'
        If job:date_completed = ''
            Disable(?qa_group)
            Unhide(?qa_group)
        Else!If job:date_completed = ''
            Unhide(?qa_group)
            Disable(?qa_group)
        End!If job:date_completed = ''
    End!If def:qa_before_complete = 'YES'
Pricing     Routine

    Set(defaults)
    access:defaults.next()
    Do SetJOBSE

    !Add new prototype - L945 (DBH: 04-09-2003)
    JobPricingRoutine(0)
    Access:JOBSE.Update()
    Do GetJOBSE
    Display()
Fill_Lists      Routine    !Fill the Drop Down Lists

ValidateDateCode Routine


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020393'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateJOBS_Additional')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:ACCAREAS.Open
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:CITYSERV.Open
  Relate:COMMONCP.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:DISCOUNT.Open
  Relate:ESNMODEL.Open
  Relate:EXCAUDIT.Open
  Relate:EXCCHRGE.Open
  Relate:JOBEXACC.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:JOBPAYMT.Open
  Relate:JOBS2_ALIAS.Open
  Relate:MANUDATE.Open
  Relate:NETWORKS.Open
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  Relate:PARTS_ALIAS.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WARPARTS_ALIAS.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:DEFCHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:MANUFACT.UseFile
  Access:LOAN.UseFile
  Access:LOANACC.UseFile
  Access:JOBLOHIS.UseFile
  Access:EXCHANGE.UseFile
  Access:JOBEXHIS.UseFile
  Access:EXCHACC.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBACC.UseFile
  Access:TRDMODEL.UseFile
  Access:TRDPARTY.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:WARPARTS.UseFile
  Access:STATUS.UseFile
  Access:LOANHIST.UseFile
  Access:EXCHHIST.UseFile
  Access:LOCATION.UseFile
  Access:LOCINTER.UseFile
  Access:COMMONFA.UseFile
  Access:COMMONWP.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:BOUNCER.UseFile
  Access:MODELCOL.UseFile
  Access:TRDBATCH.UseFile
  Access:REPAIRTY.UseFile
  Access:JOBNOTES.UseFile
  Access:MODELNUM.UseFile
  Access:STOCKTYP.UseFile
  Access:JOBSE.UseFile
  Access:TRAFAULT.UseFile
  Access:TRAFAULO.UseFile
  Access:CHARTYPE.UseFile
  Access:SUBCHRGE.UseFile
  Access:REPTYDEF.UseFile
  Access:JOBSENG.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  Access:AUDSTATS.UseFile
  SELF.FilesOpened = True
  BRW12.Init(?List,Queue:Browse.ViewPosition,BRW12::View:Browse,Queue:Browse,Relate:PARTS,SELF)
  BRW77.Init(?List:3,Queue:Browse:2.ViewPosition,BRW77::View:Browse,Queue:Browse:2,Relate:ESTPARTS,SELF)
  BRW79.Init(?List:4,Queue:Browse:3.ViewPosition,BRW79::View:Browse,Queue:Browse:3,Relate:WARPARTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Do AreThereNotes
  !Save Address/Account Details
  accsav:AccountNumber    = job:Account_Number
  
  accsa1:CompanyName      = job:Company_Name
  accsa1:AddressLine1     = job:Address_Line1
  accsa1:AddressLine2     = job:Address_Line2
  accsa1:AddressLine3     = job:Address_Line3
  accsa1:Postcode         = job:Postcode
  accsa1:TelephoneNumber  = job:Telephone_Number
  accsa1:FaxNumber        = job:Fax_Number
  
  accsa2:CompanyName      = job:Company_Name_Delivery
  accsa2:AddressLine1     = job:Address_Line1_Delivery
  accsa2:AddressLine2     = job:Address_Line2_Delivery
  accsa2:AddressLine3     = job:Address_Line3_Delivery
  accsa2:Postcode         = job:Postcode_Delivery
  accsa2:TelephoneNumber  = job:Telephone_Delivery
  ! Save Window Name
   AddToLog('Window','Open','UpdateJOBS_Additional')
  ?List:3{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW12.Q &= Queue:Browse
  BRW12.RetainRow = 0
  BRW12.AddSortOrder(,par:Part_Number_Key)
  BRW12.AddRange(par:Ref_Number,job:Ref_Number)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,par:Part_Number,1,BRW12)
  BIND('ordered_temp',ordered_temp)
  BIND('tmp:CPartQuantity',tmp:CPartQuantity)
  BIND('chargeable_part_total_temp',chargeable_part_total_temp)
  BIND('tmp:CColourFlag',tmp:CColourFlag)
  BRW12.AddField(par:Part_Number,BRW12.Q.par:Part_Number)
  BRW12.AddField(par:Description,BRW12.Q.par:Description)
  BRW12.AddField(par:Order_Number,BRW12.Q.par:Order_Number)
  BRW12.AddField(ordered_temp,BRW12.Q.ordered_temp)
  BRW12.AddField(tmp:CPartQuantity,BRW12.Q.tmp:CPartQuantity)
  BRW12.AddField(par:Quantity,BRW12.Q.par:Quantity)
  BRW12.AddField(chargeable_part_total_temp,BRW12.Q.chargeable_part_total_temp)
  BRW12.AddField(par:Part_Ref_Number,BRW12.Q.par:Part_Ref_Number)
  BRW12.AddField(tmp:CColourFlag,BRW12.Q.tmp:CColourFlag)
  BRW12.AddField(par:Record_Number,BRW12.Q.par:Record_Number)
  BRW12.AddField(par:Ref_Number,BRW12.Q.par:Ref_Number)
  BRW77.Q &= Queue:Browse:2
  BRW77.RetainRow = 0
  BRW77.AddSortOrder(,epr:Part_Number_Key)
  BRW77.AddRange(epr:Ref_Number,job:Ref_Number)
  BRW77.AddLocator(BRW77::Sort0:Locator)
  BRW77::Sort0:Locator.Init(,epr:Part_Number,1,BRW77)
  BIND('estimate_parts_cost_temp',estimate_parts_cost_temp)
  BRW77.AddField(epr:Part_Number,BRW77.Q.epr:Part_Number)
  BRW77.AddField(epr:Description,BRW77.Q.epr:Description)
  BRW77.AddField(epr:Quantity,BRW77.Q.epr:Quantity)
  BRW77.AddField(estimate_parts_cost_temp,BRW77.Q.estimate_parts_cost_temp)
  BRW77.AddField(epr:Record_Number,BRW77.Q.epr:Record_Number)
  BRW77.AddField(epr:Ref_Number,BRW77.Q.epr:Ref_Number)
  BRW79.Q &= Queue:Browse:3
  BRW79.RetainRow = 0
  BRW79.AddSortOrder(,wpr:Part_Number_Key)
  BRW79.AddRange(wpr:Ref_Number,Relate:WARPARTS,Relate:JOBS)
  BRW79.AddLocator(BRW79::Sort0:Locator)
  BRW79::Sort0:Locator.Init(,wpr:Part_Number,1,BRW79)
  BIND('warranty_ordered_temp',warranty_ordered_temp)
  BIND('tmp:WPartQuantity',tmp:WPartQuantity)
  BIND('warranty_part_total_temp',warranty_part_total_temp)
  BIND('tmp:WColourFlag',tmp:WColourFlag)
  BRW79.AddField(wpr:Part_Number,BRW79.Q.wpr:Part_Number)
  BRW79.AddField(wpr:Description,BRW79.Q.wpr:Description)
  BRW79.AddField(wpr:Order_Number,BRW79.Q.wpr:Order_Number)
  BRW79.AddField(warranty_ordered_temp,BRW79.Q.warranty_ordered_temp)
  BRW79.AddField(tmp:WPartQuantity,BRW79.Q.tmp:WPartQuantity)
  BRW79.AddField(wpr:Quantity,BRW79.Q.wpr:Quantity)
  BRW79.AddField(warranty_part_total_temp,BRW79.Q.warranty_part_total_temp)
  BRW79.AddField(wpr:Part_Ref_Number,BRW79.Q.wpr:Part_Ref_Number)
  BRW79.AddField(tmp:WColourFlag,BRW79.Q.tmp:WColourFlag)
  BRW79.AddField(wpr:Record_Number,BRW79.Q.wpr:Record_Number)
  BRW79.AddField(wpr:Ref_Number,BRW79.Q.wpr:Ref_Number)
  FDCB8.Init(job:Courier,?job:Courier,Queue:FileDropCombo:11.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:11,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:11
  FDCB8.AddSortOrder(cou:Courier_Key)
  FDCB8.AddField(cou:Courier,FDCB8.Q.cou:Courier)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  FDCB10.Init(job:Incoming_Courier,?job:Incoming_Courier,Queue:FileDropCombo:3.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:3,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:3
  FDCB10.AddSortOrder(cou:Courier_Key)
  FDCB10.AddField(cou:Courier,FDCB10.Q.cou:Courier)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  FDCB30.Init(job:JobService,?job:JobService,Queue:FileDropCombo:6.ViewPosition,FDCB30::View:FileDropCombo,Queue:FileDropCombo:6,Relate:CITYSERV,ThisWindow,GlobalErrors,0,1,0)
  FDCB30.Q &= Queue:FileDropCombo:6
  FDCB30.AddSortOrder(cit:ServiceKey)
  FDCB30.AddField(cit:Service,FDCB30.Q.cit:Service)
  FDCB30.AddField(cit:Description,FDCB30.Q.cit:Description)
  FDCB30.AddField(cit:RefNumber,FDCB30.Q.cit:RefNumber)
  ThisWindow.AddItem(FDCB30.WindowComponent)
  FDCB30.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW12.AskProcedure = 0
      CLEAR(BRW12.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW77.AskProcedure = 0
      CLEAR(BRW77.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW79.AskProcedure = 0
      CLEAR(BRW79.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:CITYSERV.Close
    Relate:COMMONCP.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:DISCOUNT.Close
    Relate:ESNMODEL.Close
    Relate:EXCAUDIT.Close
    Relate:EXCCHRGE.Close
    Relate:JOBEXACC.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:MANUDATE.Close
    Relate:NETWORKS.Close
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
    Relate:PARTS_ALIAS.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WARPARTS_ALIAS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateJOBS_Additional')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  !What should be happenind when deleting a part from a job
  !
  !If this an external part
  !    Delete Part
  !Else!If this an external part
  !    If part has been ordered (part order number exists)
  !        If the part has been received (date received exists)
  !            If it is a stock part (stock ref number exists)
  !                If stock part is a sundry
  !                    Delete Part
  !                Else!            If stock part is a sundry
  !                    stock quantity += part quantity
  !                    Create Stock History (Add)
  !                    Delete Part
  !                End!            If stock part is a sundry
  !            Else!If it is a stock part (stock ref number exists)
  !                Create Stock Part
  !                Create Stock History (Add)
  !                Ask to pick locations
  !                Delete Part
  !            End!If it is a stock part (stock ref number exists)
  !        Else!If the part has been received (date received exists)
  !            Mark the Part on the order as received with quantity = 0
  !            Delete Part
  !        End!If the part has been received (date received exists)
  !    Else!If part has been ordered (part order number exists)
  !        If is a pending order (pending order number exists)
  !            Delete pending order
  !            Delete Part
  !        Else!If is a pending order (pending order number exists)
  !            Is it a stock part (stock ref number exists)
  !                If stock part is a sundry
  !                    Delete Part
  !                Else!            If stock part is a sundry
  !                    stock quantity += part quantity
  !                    Create Stock History (Re-credit)
  !                    Delete Part
  !                End!If stock part is a sundry
  !            Else!Is it a stock part (stock ref number exists)
  !                Delete Part
  !            End!Is it a stock part (stock ref number exists)
  !        End!If is a pending order (pending order number exists)
  !    End!If part has been ordered (part order number exists)
  !End!If this an external part
!      continue# = 1
!      If number = 7 or number = 8 or number = 9
!          If job:model_number = ''
!              Case MessageEx('You must select a Model Number first.','ServiceBase 2000',|
!                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                  Of 1 ! &OK Button
!              End!Case MessageEx
!              continue# = 0
!          Else !If job:model_number = ''
!              If job:charge_type = '' And number = 1
!                  Case MessageEx('You must select a Charge Type first.','ServiceBase 2000',|
!                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                      Of 1 ! &OK Button
!                  End!Case MessageEx
!                  continue# = 0
!              End
!              If job:warranty_charge_type = '' And number = 3 And continue# = 1
!                  Case MessageEx('You must select a Warranty Charge Type first.','ServiceBase 2000',|
!                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                      Of 1 ! &OK Button
!                  End!Case MessageEx
!                  continue# = 0
!              End
!
!          End !If job:model_number = ''
!      End!If number = 5 or number = 6 or number = 7
!
!      If job:date_completed <> ''
!          Check_Access('AMEND COMPLETED JOBS',x")
!          If x" = False
!              Case MessageEx('Cannot Insert/Delete parts.<13,10><13,10>This job has been completed.','ServiceBase 2000',|
!                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                  Of 1 ! &OK Button
!              End!Case MessageEx
!              continue# = 0
!          End
!      End
!      If job:invoice_Number <> ''
!          Check_Access('AMEND INVOICED JOBS',x")
!          If x" = False
!              If request = InsertRecord Or request = DeleteRecord
!                  continue# = 0
!                  If number = 7
!                      Case MessageEx('Cannot Insert/Delete parts.<13,10><13,10>This job has been invoiced.','ServiceBase 2000',|
!                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                          Of 1 ! &OK Button
!                      End!Case MessageEx
!                  End!If number = 1
!                  If number = 9
!                      Case MessageEx('Cannot Insert/Delete parts.<13,10><13,10>This job has been invoiced.','ServiceBase 2000',|
!                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                          Of 1 ! &OK Button
!                      End!Case MessageEx
!                  End!If number = 1
!
!              End!If request = InsertRecord Or request = DeleteRecord
!
!          End
!      End
!
!      If continue# = 1
!
!  !        Case number
!   !           Of 7    !Insert Chargeable Part
!!                  If Field() = ?Insert
!!                      glo:select11 = job:manufacturer
!!                      glo:select12 = job:model_number
!!                  End !If Field() = ?Insert
!!                  If Field() = ?Delete
!!                      If DeleteChargeablePart(0)
!!                          Delete(PARTS)
!!                      End !If DeleteChargeablePart()
!!                  End!If Field() = ?Delete
!!
!!  !            Of 8    !Insert Estiamte Part
!!                  If Field() = ?Insert:2
!!                      glo:select11 = job:manufacturer
!!                      glo:select12 = job:model_number
!!                  End !If Field() = ?Insert:3
!!                  If Field() = ?Delete:2
!!                      If job:Estimate_Accepted <> 'YES' and epr:UsedOnRepair
!!                          Include('delparte.inc')
!!                      Else
!!                          Case MessageEx('Are you sure you want to delete this part?','ServiceBase 2000',|
!!                                         'Styles\Trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0)
!!                              Of 1 ! &Yes Button
!!                                  do_delete# = 1
!!                              Of 2 ! &No Button
!!                                  do_delete# = 0
!!                          End !Case
!!                      End !If job:Estimate_Accepted = 'YES'
!!                      ThisWindow.Update
!!                      !Stock Order
!!
!!                      If do_delete# = 1
!!                          Delete(EstParts)
!!                      End !If do_delete# = 1
!!                  End!If Field() = ?Delete
!!
!!  !            Of 9    !Insert Warranty Part
!!                  If Field() = ?Insert:3
!!                      glo:select11 = job:manufacturer
!!                      glo:select12 = job:model_number
!!                  End !If Field() = ?Insert:3
!!                  If Field() = ?Delete:3
!!                      If DeleteWarrantyPart(0)
!!                          Delete(WARPARTS)
!!                      End !If DeleteWarrantyPart()
!!                  End !If Field() = Delete:3
!
!
!  !        End !Case number
!          If request <> 3
!
!
  !    End !If request <> 3
  !
  !    Case number
  !        Of 7 !Chargeable
  !            If glo:select1 = 'NEW PENDING' or glo:Select1 = 'NEW PENDING WEB'
  !                access:stock.clearkey(sto:ref_number_key)
  !                sto:ref_number = par:part_ref_number
  !                If access:stock.fetch(sto:ref_number_key)
  !                    Case MessageEx('Error! Cannot retrieve the details of this Stock Part','ServiceBase 2000',|
  !                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
  !                        Of 1 ! &OK Button
  !                    End!Case MessageEx
  !                Else!If access:stock.fetch(sto:ref_number_key)
  !                    If Access:STOHIST.Primerecord() = Level:Benign
  !                        shi:Ref_Number              = sto:Ref_Number
  !                        shi:Transaction_Type        = 'DEC'
  !                        shi:Despatch_Note_Number    = par:Despatch_Note_Number
  !                        shi:Quantity                = sto:Quantity_stock
  !                        shi:Date                    = Today()
  !                        shi:Purchase_Cost           = par:Purchase_Cost
  !                        shi:Sale_Cost               = par:Sale_Cost
  !                        shi:Retail_Cost             = par:Retail_Cost
  !                        shi:Job_Number              = job:Ref_Number
  !                        Access:USERS.Clearkey(use:Password_Key)
  !                        use:Password                = glo:Password
  !                        Access:USERS.Tryfetch(use:Password_Key)
  !                        shi:User                    = use:User_Code
  !                        shi:Notes                   = 'STOCK DECREMENTED'
  !                        shi:Information             = ''
  !                        If Access:STOHIST.Tryinsert()
  !                            Access:STOHIST.Cancelautoinc()
  !                        End!If Access:STOHIST.Tryinsert()
  !                    End!If Access:STOHIST.Primerecord() = Level:Benign2
  !                    sto:quantity_stock     = 0
  !                    access:stock.update()
  !
  !                    access:parts_alias.clearkey(par_ali:refpartrefnokey)
  !                    par_ali:ref_number      = job:ref_number
  !                    par_ali:part_ref_number = glo:select2
  !                    If access:parts_alias.fetch(par_ali:RefPartRefNoKey) = Level:Benign
  !                        par:ref_number           = par_ali:ref_number
  !                        par:adjustment           = par_ali:adjustment
  !                        par:part_ref_number      = par_ali:part_ref_number
  !
  !                        par:part_number     = par_ali:part_number
  !                        par:description     = par_ali:description
  !                        par:supplier        = par_ali:supplier
  !                        par:purchase_cost   = par_ali:purchase_cost
  !                        par:sale_cost       = par_ali:sale_cost
  !                        par:retail_cost     = par_ali:retail_cost
  !                        par:quantity             = glo:select3
  !                        par:warranty_part        = 'NO'
  !                        par:exclude_from_order   = 'NO'
  !                        par:despatch_note_number = ''
  !                        par:date_ordered         = ''
  !                        par:date_received        = ''
  !                        par:pending_ref_number   = glo:select4
  !                        par:order_number         = ''
  !                        par:fault_code1    = par_ali:fault_code1
  !                        par:fault_code2    = par_ali:fault_code2
  !                        par:fault_code3    = par_ali:fault_code3
  !                        par:fault_code4    = par_ali:fault_code4
  !                        par:fault_code5    = par_ali:fault_code5
  !                        par:fault_code6    = par_ali:fault_code6
  !                        par:fault_code7    = par_ali:fault_code7
  !                        par:fault_code8    = par_ali:fault_code8
  !                        par:fault_code9    = par_ali:fault_code9
  !                        par:fault_code10   = par_ali:fault_code10
  !                        par:fault_code11   = par_ali:fault_code11
  !                        par:fault_code12   = par_ali:fault_code12
  !                        par:Requested      = True
  !                        If glo:Select1 = 'NEW PENDING WEB'
  !                            par:WebOrder = 1
  !                        End !If glo:Select1 = 'NEW PENDING WEB'
  !                        access:parts.insert()
  !                    end !If access:parts_alias.clearkey(par_ali:RefPartRefNoKey) = Level:Benign
  !                end !if access:stock.fetch(sto:ref_number_key = Level:Benign
  !            End!If glo:select1 = 'NEW PENDING'
  !            DO Check_Parts !Check parts to see if status change required?
  !        Of 9 !Warranty
  !            If glo:select1 = 'NEW PENDING' Or glo:Select1 = 'NEW PENDING WEB'
  !                access:stock.clearkey(sto:ref_number_key)
  !                sto:ref_number = wpr:part_ref_number
  !                If access:stock.fetch(sto:ref_number_key)
  !                    Case MessageEx('Error! Cannot retrieve the details of this Stock Part','ServiceBase 2000',|
  !                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
  !                        Of 1 ! &OK Button
  !                    End!Case MessageEx
  !                Else!If access:stock.fetch(sto:ref_number_key)
  !                    If Access:STOHIST.Primerecord() = Level:Benign
  !                        shi:Ref_Number              = sto:Ref_Number
  !                        shi:Transaction_Type        = 'DEC'
  !                        shi:Despatch_Note_Number    = wpr:Despatch_Note_Number
  !                        shi:Quantity                = sto:Quantity_Stock
  !                        shi:Date                    = Today()
  !                        shi:Purchase_Cost           = wpr:Purchase_Cost
  !                        shi:Sale_Cost               = wpr:Sale_cost
  !                        shi:Retail_Cost             = wpr:Retail_Cost
  !                        shi:Job_Number              = wpr:Ref_Number
  !                        Access:USERS.Clearkey(use:Password_Key)
  !                        use:Password                = glo:Password
  !                        Access:USERS.Tryfetch(use:Password_Key)
  !                        shi:User                    = use:User_Code
  !                        shi:Notes                   = 'STOCK DECREMENTED'
  !                        shi:Information             = ''
  !                        If Access:STOHIST.Tryinsert()
  !                            Access:STOHIST.Cancelautoinc()
  !                        End!If Access:STOHIST.Tryinsert()
  !                    End!If Access:STOHIST.Primerecord() = Level:Benign
  !
  !                    sto:quantity_stock     = 0
  !                    access:stock.update()
  !
  !                    access:warparts_alias.clearkey(war_ali:RefPartRefNoKey)
  !                    war_ali:ref_number      = job:ref_number
  !                    war_ali:part_ref_number = glo:select2
  !                    If access:warparts_alias.fetch(war_ali:RefPartRefNoKey) = Level:Benign
  !                        wpr:ref_number           = war_ali:ref_number
  !                        wpr:adjustment           = war_ali:adjustment
  !                        wpr:part_ref_number      = war_ali:part_ref_number
  !                        wpr:part_number     = war_ali:part_number
  !                        wpr:description     = war_ali:description
  !                        wpr:supplier        = war_ali:supplier
  !                        wpr:purchase_cost   = war_ali:purchase_cost
  !                        wpr:sale_cost       = war_ali:sale_cost
  !                        wpr:retail_cost     = war_ali:retail_cost
  !                        wpr:quantity             = glo:select3
  !                        wpr:warranty_part        = 'YES'
  !                        wpr:exclude_from_order   = 'NO'
  !                        wpr:despatch_note_number = ''
  !                        wpr:date_ordered         = ''
  !                        wpr:date_received        = ''
  !                        wpr:pending_ref_number   = glo:select4
  !                        wpr:order_number         = ''
  !                        wpr:fault_code1    = war_ali:fault_code1
  !                        wpr:fault_code2    = war_ali:fault_code2
  !                        wpr:fault_code3    = war_ali:fault_code3
  !                        wpr:fault_code4    = war_ali:fault_code4
  !                        wpr:fault_code5    = war_ali:fault_code5
  !                        wpr:fault_code6    = war_ali:fault_code6
  !                        wpr:fault_code7    = war_ali:fault_code7
  !                        wpr:fault_code8    = war_ali:fault_code8
  !                        wpr:fault_code9    = war_ali:fault_code9
  !                        wpr:fault_code10   = war_ali:fault_code10
  !                        wpr:fault_code11   = war_ali:fault_code11
  !                        wpr:fault_code12   = war_ali:fault_code12
  !                        wpr:Requested      = True
  !                        If glo:Select1 = 'NEW PENDING WEB'
  !                            wpr:WebOrder = 1
  !                        End !If glo:Select1 = 'NEW PENDING WEB'
  !                        access:warparts.insert()
  !                    end !If access:parts_alias.clearkey(par_ali:RefPartRefNoKey) = Level:Benign
  !                end !if access:stock.fetch(sto:ref_number_key = Level:Benign
  !            End!If glo:select1 = 'NEW PENDING'
  !            DO Check_Parts !Check parts to see if status change required?
  !
  !
  !    End !Case number
  !    Else!!If continue# = 1
  !        Case number
  !            Of 7
  !                access:parts.cancelautoinc()
  !                Do check_parts
  !                Do Pricing
  !
  !            Of 8
  !                access:estparts.cancelautoinc()
  !                Do check_parts
  !                Do Pricing
  !
  !            Of 9
  !                access:warparts.cancelautoinc()
  !                Do check_parts
  !                Do Pricing
  !
  !        End!Case number
  !
  !    End !If continue# = 1
  !    Do CountParts
  !    Clear(glo:G_Select1)
  !    Clear(glo:G_Select1)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:HUBRepair
      !If ~0{prop:AcceptAll}
      !    Error# = 0
      !
      !    If tmp:HubRepair <> tmp:SaveHubRepair
      !        Case tmp:HubRepair
      !            Of 0
      !                Case MessageEx('De-selecting Hub Repair will credit this job to a REMOTE Repair Centre.'&|
      !                  '<13,10>'&|
      !                  '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
      !                               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      !                    Of 1 ! &Yes Button
      !                    Of 2 ! &No Button
      !                        Error# = 1
      !                End!Case MessageEx
      !            Of 1
      !                Case MessageEx('Selecting Hub Repair will credit this job to a ADVANCED Repair Centre.'&|
      !                  '<13,10>'&|
      !                  '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
      !                               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      !                    Of 1 ! &Yes Button
      !                    Of 2 ! &No Button
      !                        Error# = 1
      !                End!Case MessageEx
      !        End !Case tmp:HubRepair
      !        If Error#
      !            tmp:HubRepair = tmp:SaveHubRepair
      !        End !If Error# = 0
      !    End !If tmp:HubRepair <> tmp:SavedHubRepair
      !
      !    If tmp:HubRepair
      !        tmp:HubRepairDate = Today()
      !        tmp:HubRepairTime = Clock()
      !    Else
      !        tmp:HubRepairDate   = ''
      !        tmp:HubRepairTime = ''
      !    End !If tmp:HubRepair
      !
      !End !0{prop:AcceptAll}
      !Display()
    OF ?job:Estimate_Ready
!      If ~0{prop:acceptall}
!      If job:estimate_ready <> estimate_ready_temp
!          If job:estimate_ready = 'NO'
!              check_access('RELEASE - ESTIMATE READY',x")
!              If x" = False
!                  Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
!                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                      Of 1 ! &OK Button
!                  End!Case MessageEx
!                  job:estimate_ready = estimate_temp
!              Else
!                  estimate_ready_temp = job:estimate_ready
!              End !If x" = False
!          Else!If job:estimate_ready = 'YES'
!              error# = 0
!              If total_estimate_temp < job:estimate_if_over
!                  Case MessageEx('The total value of this Estimate is less than the specified ''Estimate If Over'' value.<13,10><13,10>Are you sure you want to mark this Estimate as ''Ready''?','ServiceBase 2000',|
!                                 'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
!                      Of 1 ! &Yes Button
!                          error# = 0
!                      Of 2 ! &No Button
!                          error# = 1
!                  End!Case MessageEx
!              End!If total_estimate_temp < job:estimate_if_over
!              If Error# = 0
!                  If GETINI('ESTIMATE','UseBERComparison',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                      Access:MODELNUM.Clearkey(mod:Model_Number_Key)
!                      mod:Model_Number    = job:Model_Number
!                      If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!                          !Found
!                          If job:Sub_Total_Estimate > mod:ReplacementValue
!                              If ((job:Sub_Total_Estimate - mod:ReplacementValue) / mod:ReplacementValue) * 100 > GETINI('ESTIMATE','BERComparison',,CLIP(PATH())&'\SB2KDEF.INI')
!                                  Case MessageEx('This repair exceeds ' & Clip(GETINI('ESTIMATE','BERComparison',,CLIP(PATH())&'\SB2KDEF.INI')) & '% of the unit replacement value.'&|
!                                    '<13,10>You should now select a B.E.R. Repair Type.'&|
!                                    '<13,10>'&|
!                                    '<13,10>Do you wish to make this job a B.E.R. or continue with the estimate?','ServiceBase 2000',|
!                                                 'Styles\warn.ico','|&Continue|&Make B.E.R.',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
!                                      Of 1 ! &Continue Button
!
!                                      Of 2 ! &Make B.E.R. Button
!                                          job:Repair_Type = PickRepairTypes(job:Model_Number,job:Account_Number,job:Charge_Type,job:Unit_Type,'C',1,EngineerSkillLevel(job:Engineer),job:Repair_Type)
!                                          If job:Repair_Type = ''
!                                              job:Repair_Type = Repair_Type_Temp
!                                          End !job:Repair_Type = ''
!                                          Post(Event:Accepted,?job:Repair_Type)
!                                          Error# = 1
!                                  End!Case MessageEx
!                              End !If ((job:Sub_Total_Estimate - mod:ReplacementValue) / mod:ReplacementValue) * 100 > GETINI('ESTIMATE','BERComparison',,CLIP(PATH())&'\SB2KDEF.INI')
!                          End !If job:Sub_Total_Estimate > mod:ReplacementValue
!                      Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!                          !Error
!                          !Assert(0,'<13,10>Fetch Error<13,10>')
!                      End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!
!                  End !If GETINI('ESTIMATE','UseBERComparison',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!              End !If Error# = 0
!              If error# = 0
!                  If GETINI('ESTIMATE','CheckPartAvailable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                      !Check if parts are available, or outside supplier period
!                      FoundNotOverDue# = 0
!                      Save_epr_ID = Access:ESTPARTS.SaveFile()
!                      Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
!                      epr:Ref_Number  = job:Ref_Number
!                      Set(epr:Part_Number_Key,epr:Part_Number_Key)
!                      Loop
!                          If Access:ESTPARTS.NEXT()
!                             Break
!                          End !If
!                          If epr:Ref_Number  <> job:Ref_Number      |
!                              Then Break.  ! End If
!                          If epr:Exclude_From_Order <> 'YES'
!                              If epr:Part_Ref_Number <> ''
!                                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
!                                  sto:Ref_NUmber  = epr:Part_Ref_Number
!                                  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                                      !Found
!                                      If sto:Quantity_Stock = 0
!                                          Save_orp_ID = Access:ORDPARTS.SaveFile()
!                                          Access:ORDPARTS.ClearKey(orp:Received_Part_Number_Key)
!                                          orp:All_Received = 'NO'
!                                          orp:Part_Number  = epr:Part_Number
!                                          Set(orp:Received_Part_Number_Key,orp:Received_Part_Number_Key)
!                                          Loop
!                                              If Access:ORDPARTS.NEXT()
!                                                 Break
!                                              End !If
!                                              If orp:All_Received <> 'NO'      |
!                                              Or orp:Part_Number  <> epr:Part_Number      |
!                                                  Then Break.  ! End If
!                                              Access:ORDERS.ClearKey(ord:Order_Number_Key)
!                                              ord:Order_Number = orp:Order_Number
!                                              If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
!                                                  !Found
!                                                  Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
!                                                  sup:Company_Name = ord:Supplier
!                                                  If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
!                                                      !Found
!                                                      If Today() - ord:Date < sup:Normal_Supply_Period
!                                                          FoundNotOverdue# = 1
!                                                          Break
!                                                      End !If Today() - ord:Date > sup:Normal_Supply_Period
!                                                  Else!If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
!                                                      !Error
!                                                      !Assert(0,'<13,10>Fetch Error<13,10>')
!                                                  End!If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
!
!                                              Else!If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
!                                                  !Error
!                                                  !Assert(0,'<13,10>Fetch Error<13,10>')
!                                              End!If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
!                                          End !Loop
!                                          Access:ORDPARTS.RestoreFile(Save_orp_ID)
!                                          If FoundNotOverDue# = 1
!                                              Break
!                                          End !If FoundOverDue# = 1
!
!                                      End !If sto:Quantity_stock = 0
!                                  Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key.) = Level:Benign
!                                      !Error
!                                      !Assert(0,'<13,10>Fetch Error<13,10>')
!                                  End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key.) = Level:Benign
!                              End !If sto:Part_Ref_Number <> ''
!                          End !If epr:Exclude_From_Order <> 'YES
!                      End !Loop
!                      Access:ESTPARTS.RestoreFile(Save_epr_ID)
!
!                      If FoundNotOverDue# = 0
!                          Case MessageEx('One or more of the parts attached to this job in on an overdue backorder.'&|
!                            '<13,10>'&|
!                            '<13,10>Do you wish to notorise this on the estimate?','ServiceBase 2000',|
!                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!                              Of 1 ! &Yes Button
!                                  Set(DEFAULT2)
!                                  Access:DEFAULT2.Next()
!                                  Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
!                                  jbn:RefNumber   = job:Ref_Number
!                                  If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
!                                      !Found
!
!                                  Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
!                                      !Error
!                                      !Assert(0,'<13,10>Fetch Error<13,10>')
!                                  End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
!
!                                  If jbn:Invoice_Text = ''
!                                      jbn:Invoice_Text = de2:OverdueBackOrderText
!                                  Else !If jbn:Invoice_Text = ''
!                                      jbn:Invoice_Text = Clip(jbn:Invoice_Text) & '<13,10>' & de2:OverdueBackOrderText
!                                  End !If jbn:Invoice_Text = ''
!                                  Access:JOBNOTES.TryUpdate()
!                              Of 2 ! &No Button
!                          End!Case MessageEx
!                      End !If FoundOverDue# = 1
!                  End !If GETINI('ESTIMATE','CheckPartAvailable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!
!                  estimate_ready_temp   = job:estimate_ready
!                 GetStatus(510,thiswindow.request,'JOB') !estimate ready
!
!                  brw21.resetsort(1)
!                  Do time_remaining
!              Else!If error# = 0
!                  job:estimate_ready = estimate_ready_temp
!              End!If error# = 0
!
!          End !If job:estimate_ready = 'YES'
!      End !If job:estimate_ready <> estimate_temp

      !End !If ~0{prop:acceptall}
    OF ?job:In_Repair
      !If ~0{prop:acceptall}
      
      !If job:in_repair <> in_repair_temp
      !    If job:in_repair = 'NO'
      !        check_access('RELEASE - IN REPAIR',x")
      !        If x" = False
      !            Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
      !                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                Of 1 ! &OK Button
      !            End!Case MessageEx
      !            job:in_repair   = in_repair_temp
      !        Else !If x" = False
      !            in_repair_temp  = job:in_repair
      !        End !If x" = False
      !    Else !If job:in_repair = 'NO'
      !        error# = 0
      !        If job:date_completed <> ''
      !            Check_Access('AMEND COMPLETED JOBS',x")
      !            If x" = False
      !                Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job has been completed.','ServiceBase 2000',|
      !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                    Of 1 ! &OK Button
      !                End!Case MessageEx
      !                error# = 1
      !            End
      !        End
      !        If job:invoice_Number <> ''
      !            Check_Access('AMEND INVOICED JOBS',x")
      !            If x" = False
      !                Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job has been invoiced.','ServiceBase 2000',|
      !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                    Of 1 ! &OK Button
      !                End!Case MessageEx
      !                error# = 1
      !            End
      !        End
      !
      !        If error# = 0
      !            If job:workshop <> 'YES'
      !                Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job is not in the Workshop.','ServiceBase 2000',|
      !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                    Of 1 ! &OK Button
      !                End!Case MessageEx
      !                error# = 1
      !            End !If job:workshop <> 'YES'
      !        End !If error# = 0
      !        If error# = 0
      !            job:date_in_repair = Today()
      !            job:time_in_repair = Clock()
      !              GetStatus(315,thiswindow.request,'JOB') !in repair
      !
      !            Brw21.resetsort(1)
      !            Do time_remaining
      !        End !If error# = 0
      !        If error# = 1
      !            job:date_in_repair = ''
      !            job:time_in_repair = ''
      !            job:in_repair = in_repair_temp
      !        Else
      !            in_repair_temp  = job:in_repair
      !        End
      !        Display()
      !    End !If job:in_repair = 'NO'
      !End !If job:in_repair <> in_repair_temp
      !
      !
      !End !If ~0{prop:acceptall}
    OF ?job:On_Test
      !If ~0{prop:acceptall}
      !If job:on_test <> on_test_temp
      !    If job:on_test = 'NO'
      !        check_access('RELEASE - ON TEST',x")
      !        If x" = False
      !            Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
      !                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                Of 1 ! &OK Button
      !            End!Case MessageEx
      !            job:on_test = on_test_temp
      !        Else !If x" = False
      !            on_test_temp    = job:on_test
      !        End !If x" = False
      !    Else !If job:in_repair = 'NO'
      !        error# = 0
      !        If job:date_completed <> ''
      !            Check_Access('AMEND COMPLETED JOBS',x")
      !            If x" = False
      !                Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job has been completed.','ServiceBase 2000',|
      !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                    Of 1 ! &OK Button
      !                End!Case MessageEx
      !                error# = 1
      !            End
      !        End
      !        If job:invoice_Number <> ''
      !            Check_Access('AMEND INVOICED JOBS',x")
      !            If x" = False
      !                Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job has been invoiced.','ServiceBase 2000',|
      !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                    Of 1 ! &OK Button
      !                End!Case MessageEx
      !                error# = 1
      !            End
      !        End
      !        If error# = 0
      !            If job:workshop <> 'YES'
      !                Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job is not in the Workshop.','ServiceBase 2000',|
      !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                    Of 1 ! &OK Button
      !                End!Case MessageEx
      !                error# = 1
      !            End !If job:workshop <> 'YES'
      !        End !If error# = 0
      !        If error# = 0
      !            If job:in_repair <> 'YES'
      !                Case MessageEx('Cannot mark as On Test.<13,10><13,10>This job has not been marked as In Repair.','ServiceBase 2000',|
      !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                    Of 1 ! &OK Button
      !                End!Case MessageEx
      !                error# = 1
      !            End !If job:in_repair <> 'YES'
      !        End!If error# = 0
      !        If error# = 0
      !            job:date_on_test = Today()
      !            job:time_on_test = Clock()
      !              GetStatus(320,thiswindow.request,'JOB') !on test
      !
      !            Brw21.resetsort(1)
      !            Do time_remaining
      !        End !If error# = 0
      !
      !        If error# = 1
      !            job:date_on_test = ''
      !            job:time_on_test = ''
      !            job:on_test = on_test_temp
      !        Else
      !            on_test_temp    = job:on_test
      !        End !If error# = 1
      !
      !    End !If job:in_repair = 'NO'
      !End !If job:on_test <> on_test_temp
      !
      !
      !
      !
      !End !If ~0{prop:acceptall}
    OF ?job:QA_Passed
      !If ~0{prop:acceptall}
      
      !set(defaults)
      !access:defaults.next()
      !If job:qa_passed <> qa_passed_temp
      !    If job:qa_passed = 'NO'
      !        check_access('RELEASE - QA PASSED',x")
      !        If x" = False
      !            Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
      !                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                Of 1 ! &OK Button
      !            End!Case MessageEx
      !            job:qa_passed   = qa_passed_temp
      !        Else !If x" = False
      !            qa_passed_temp  = job:qa_passed
      !        End !If x" = False
      !    Else !If job:in_repair = 'NO'
      !        If def:qa_required = 'YES'
      !            Do Check_For_Despatch
      !        End!If def:qa_required = 'YES'
      !        pass# = 0
      !        Include('accjchk.inc')
      !        If pass# = 1
      !            qa_passed_temp  = job:qa_passed
      !            job:date_qa_passed = Today()
      !            job:time_qa_passed = Clock()
      !              GetStatus(610,thiswindow.request,'JOB') !manual qa passed
      !
      !        Else!
      !            job:qa_Passed  = qa_passed_temp
      !        End!If pass# = 1
      !        Brw21.resetsort(1)
      !       Do time_remaining
      !
      !    End !If job:in_repair = 'NO'
      !End !If job:in_repair <> in_repair_temp
      !
      !End !If ~0{prop:acceptall}
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020393'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020393'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020393'&'0')
      ***
    OF ?Collection_Text_Button
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Collection_Text
      ThisWindow.Reset
    OF ?Delivery_Text_Button
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Delivery_Text
      ThisWindow.Reset
    OF ?Lookup_Engineer
      ThisWindow.Update
      Set(DEFAULT2)
      Access:DEFAULT2.Next()
      
      GlobalRequest = SelectRecord
      If de2:UserSkillLevel
          PickEngineersSkillLevel(jobe:SkillLevel)
      Else !If de2:UserSkillLevel
          Browse_Users_Job_Assignment
      End !If de2:UserSkillLevel
      
      Case GlobalResponse
          Of Requestcompleted
              job:Engineer = use:User_Code
              Do AddEngineer
              Select(?+2)
          Of Requestcancelled
      !        job:Engineer = ''
              Select(?-1)
      End!Case Globalreponse
      
    OF ?job:Repair_Type
      If ~0{prop:acceptall}
          PricingError#  = 0
          If job:Repair_Type <> Repair_Type_Temp
          End !If job:Repair_Type <> Repair_Type_Temp
          !Before Lookup
          If PricingError# = 0
              do_lookup# = 1
      
              If job:date_completed <> ''
                  If SecurityCheck('AMEND COMPLETED JOBS')
                      Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      do_lookup# = 0
                  End
              End
              If job:invoice_Number <> '' and do_lookup# = 1
                  If SecurityCheck('AMEND INVOICED JOBS')
                      Case Missive('Cannot change! This job has been invoiced.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
      
                      do_lookup# = 0
                  End
              End
      
              If job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
                  Case Missive('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  do_lookup# = 0
              End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
      
              If do_lookup# = 1
      
                  IF job:Repair_Type
                      Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                      rtd:Manufacturer = job:Manufacturer
                      rtd:Chargeable   = 'YES'
                      rtd:Repair_Type  = job:Repair_Type
                      If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                          !Found
      
                      Else!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          job:Repair_Type = ''
                          Post(Event:Accepted,?LookupChargeableChargeType)
      
                      End!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                  End!IF job:Repair_Type OR ?job:Repair_Type{Prop:Req}
      
                  If job:repair_type <> repair_type_temp
                      error# = 0
                      Case CheckPricing('C')
                          Of 1
                              error# = 1
                          Of 2
                              Case Missive('The selected Repair Type has a different pricing structure.'&|
                                '<13,10>If you continue the job will be re-vaued accordingly.'&|
                                '<13,10>'&|
                                '<13,10>Are you sure?','ServiceBase 3g',|
                                             'mquest.jpg','\No|/Yes')
                                  Of 2 ! Yes Button
                                  Of 1 ! No Button
                                      error# = 1
                              End ! Case Missive
                          Else
                              If repair_type_temp <> ''
                                  Case Missive('Are you sure you want to change the repair type?','ServiceBase 3g',|
                                                 'mquest.jpg','\No|/Yes')
                                      Of 2 ! Yes Button
                                      Of 1 ! No Button
                                          error# = 1
                                  End ! Case Missive
                              End!If repair_type_temp <> ''
                      End!Case CheckPricing('C')
      
                      If error# = 1
                          job:repair_type = repair_type_temp
                      Else!If job:date_completed <> ''
                          repair_type_temp = job:repair_type
                          Do pricing
      
                          Do third_party_repairer
                      End!If job:date_completed <> ''
                  End!If job:repair_type <> repair_type_temp
                  Display()
              End !do_lookup# = 1
      
          End !If PricingError# = 0
      End !0{prop:acceptall}
    OF ?LookupChargeableChargeType
      ThisWindow.Update
      !If job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
      !    Case MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
      !                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !        Of 1 ! &OK Button
      !    End!Case MessageEx
      !Else !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
      !    job:Repair_Type = PickRepairTypes(job:Model_Number,job:Account_Number,job:Charge_Type,job:Unit_Type,'C',0,EngineerSkillLevel(job:Engineer),job:Repair_Type)
      !    If job:Repair_Type = ''
      !        job:Repair_Type = Repair_Type_Temp
      !    End !job:Repair_Type = ''
      !    Post(Event:Accepted,?job:Repair_Type)
      !
      !End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
    OF ?Engineers_Notes_Button
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      EngineersText
      ThisWindow.Reset
      access:jobnotes.clearkey(JBN:RefNumberKey)
      jbn:refnumber   = job:ref_number
      If access:jobnotes.tryfetch(JBN:RefNumberKey)
          If access:jobnotes.primerecord() = Level:Benign
              jbn:refnumber   = job:ref_number
              If access:jobnotes.tryinsert()
                  access:jobnotes.cancelautoinc()
              End!If access:jobnotes.tryinsert()
          End!If access:jobnotes.primerecord() = Level:Benign
      End! If access:jobnotes.tryfetch(JBN:RefNumberKey) = Level:Benign
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If tmp:engineernotes = ''
                  tmp:engineernotes = Clip(glo:notes_pointer)
              Else !If job:engineers_notes = ''
                  tmp:engineernotes = Clip(tmp:engineernotes) & '<13,10>' & Clip(glo:notes_pointer)
              End !If job:engineers_notes = ''
          End
          Display()
      End
    OF ?Button59
      ThisWindow.Update
      BrowseEngineersOnJob(job:Ref_Number)
      ThisWindow.Reset
    OF ?job:Repair_Type_Warranty
      !if ~0{prop:acceptall}
      !    PricingError# = 0
      !    If job:Repair_Type_Warranty <> Warranty_Repair_Type_Temp
      !    End !If job:Repair_Type_Warranty <> Warranty_Repair_Type_Temp
      !    If PricingError# = 0
      !        do_lookup# = 1
      !
      !        If job:date_completed <> ''
      !            If SecurityCheck('AMEND COMPLETED JOBS')
      !                Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
      !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                    Of 1 ! &OK Button
      !                End!Case MessageEx
      !                do_lookup# = 0
      !            End
      !        End
      !        If job:invoice_Number <> '' and do_lookup# = 1
      !            If SecurityCheck('AMEND INVOICED JOBS')
      !                Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
      !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                    Of 1 ! &OK Button
      !                End!Case MessageEx
      !                do_lookup# = 0
      !            End
      !        End
      !
      !        If job:Model_Number = '' Or job:Warranty_Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
      !            Case MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
      !                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !                Of 1 ! &OK Button
      !            End!Case MessageEx
      !            do_lookup# = 0
      !        End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
      !
      !        If do_lookup# = 1
      !
      !            IF job:Repair_Type_Warranty
      !                Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
      !                rtd:Manufacturer = job:Manufacturer
      !                rtd:Warranty   = 'YES'
      !                rtd:Repair_Type  = job:Repair_Type_Warranty
      !                If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
      !                    !Found
      !
      !                Else!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
      !
      !                    !Error
      !                    !Assert(0,'<13,10>Fetch Error<13,10>')
      !
      !                End!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
      !            End!IF job:Repair_Type OR ?job:Repair_Type{Prop:Req}
      !
      !            If job:Repair_Type_Warranty <> Warranty_Repair_Type_Temp
      !                error# = 0
      !                Case CheckPricing('W')
      !                    Of 1
      !                        error# = 1
      !                    Of 2
      !                        Case MessageEx('The selected Repair Type has a different pricing structure.<13,10><13,10>If you continue the job will be re-valued accordingly.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
      !                                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      !                            Of 1 ! &Yes Button
      !                            Of 2 ! &No Button
      !                                error# = 1
      !                        End!Case MessageEx
      !                    Else
      !                        If Warranty_Repair_Type_Temp <> ''
      !                            Case MessageEx('Are you sure you want to change the repair type?','ServiceBase 2000',|
      !                                           'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      !                                Of 1 ! &Yes Button
      !                                Of 2 ! &No Button
      !                                    error# = 1
      !                            End!Case MessageEx
      !                        End!If repair_type_temp <> ''
      !                End!Case CheckPricing('C')
      !
      !                If error# = 1
      !                    job:repair_type_Warranty = Warranty_repair_type_temp
      !
      !                Else!If job:date_completed <> ''
      !                    !Do we want an "Exchange Prompt" message to appear?
      !                    Access:REPTYDEF.ClearKey(rtd:Warranty_Key)
      !                    rtd:Warranty    = 'YES'
      !                    rtd:Repair_Type = job:Repair_Type_Warranty
      !                    If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
      !                        !Found
      !                        If rtd:PromptForExchange
      !                            Case MessageEx('You have requested an Exchange Unit for this repair.'&|
      !                              '<13,10>'&|
      !                              '<13,10>Do you wish to continue and process this request, or cancel it?','ServiceBase 2000',|
      !                                           'Styles\question.ico','|&Process Request|&Cancel Request',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      !                                Of 1 ! &Process Request Button
      !                                    GetStatus(108,0,'EXC')
      !                                Of 2 ! &Cancel Request Button
      !                            End!Case MessageEx
      !                        End !If rtd:ExchangePrompt
      !                    Else!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
      !                        !Error
      !                        !Assert(0,'<13,10>Fetch Error<13,10>')
      !                    End!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
      !
      !                    Warranty_repair_type_temp = job:repair_type_Warranty
      !                    Do pricing
      !
      !                    Do third_party_repairer
      !                End!If job:date_completed <> ''
      !            End!If job:repair_type <> repair_type_temp
      !            Display()
      !        End !do_lookup# = 1
      !
      !    End !If PricingError# = 0
      !End !0{prop:acceptall}
    OF ?LookupWarrantyRepairType
      ThisWindow.Update
      !If job:Model_Number = '' Or job:Warranty_Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
      !    Case MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
      !                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !        Of 1 ! &OK Button
      !    End!Case MessageEx
      !    do_lookup# = 0
      !Else !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
      !    job:Repair_Type_Warranty = PickRepairTypes(job:Model_Number,job:Account_Number,job:Warranty_Charge_Type,job:Unit_Type,'W',0,EngineerSkillLevel(job:Engineer),job:Repair_Type_Warranty)
      !    If job:Repair_Type_Warranty = ''
      !        job:Repair_Type = Warranty_Repair_Type_Temp
      !    End !job:Repair_Type = ''
      !    Post(Event:Accepted,?job:Repair_Type_Warranty)
      !End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
      !
    OF ?ColourKey
      ThisWindow.Update
      PartsKey
      ThisWindow.Reset
    OF ?ColourKey:2
      ThisWindow.Update
      PartsKey
      ThisWindow.Reset
    OF ?job:Courier
      If ~0{prop:acceptall}
      access:courier.clearkey(cou:courier_key)
      cou:courier = job:courier
      if access:courier.fetch(cou:courier_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_courier
          if globalresponse = requestcompleted
              job:courier = cou:courier
          else
              job:courier = courier_temp
              select(?-1)
          end
          display(?job:courier)
          globalrequest     = saverequest#
      end!if access:courier.fetch(cou:courier_key)
      If job:courier <> courier_temp
          If courier_temp <> ''
              error# = 0
              If job:date_completed <> ''
                  Check_Access('AMEND COMPLETED JOBS',x")
                  If x" = False
                      Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  End
              End
              If job:invoice_Number <> ''
                  Check_Access('AMEND INVOICED JOBS',x")
                  If x" = False
                      Case Missive('Cannot change! This job has been invoiced.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  End
              End
              If error# = 1
                  job:courier = courier_temp
              Else!If error# = 1
                  Case Missive('Are you sure you want to change the Courier?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          If thiswindow.request <> Insertrecord
                              IF (AddToAudit(job:Ref_Number,'JOB','JOB COURIER CHANGED: ' & Clip(job:courier),'NEW COURIER: ' & CLip(job:courier) & '<13,10>OLD COURIER: ' & CLip(courier_temp)))
                              END ! IF
      
                          End!If thiswindow.request <> Insertrecord
                          courier_temp = job:courier
                          If job:despatch_type = 'JOB'
                              job:current_courier = job:courier
                          End
                      Of 1 ! No Button
                          job:courier = courier_temp
                  End ! Case Missive
              End!If error# = 1
          Else!If courier_temp <> ''
              If job:despatch_type = 'JOB'
                  job:current_courier = job:courier
                  courier_Temp = job:courier
              End
          End!If courier_temp <> ''
      
      End!If job:courier <> courier_temp
      Display(?job:courier)
      End!If ~0{prop:acceptall}
    OF ?job:Incoming_Courier
      Do UPS_Service
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      quickwindow{prop:buffer} = 2
      !Titles
      Do Transit_Type
      Do Location_Type
      Do Update_accessories
      Do Third_Party_Repairer
      Do Time_Remaining
      Do Trade_Account_Status
      Do Estimate_Check
      Do Show_Tabs
      Do CountParts
      Do Charge_Type
      Do Titles
      
      !Lookups
      Do Update_Engineer
      Do Update_Loan
      Do Update_Exchange
      Do Check_Msn
      !J removed the next 3 lines 07/10/02
      !don't know why they were here or why they solve the problem
      !only need to comment the last one - but may as well do them all!
      
      !Post(Event:accepted,?job:charge_type)
      !Post(event:accepted,?job:chargeable_job)
      !Post(event:accepted,?job:warranty_job)
      
      !not the temporary variables referenced in these embeds are not
      !set up at this stage - they come two embeds further on ...
      
      !message('inlookups')
      Do Estimate
      Do QA_Group
      
      !Price Job On Entry
      Do Pricing
      
      Do show_hide_tabs
      Do skip_despatch
      
      
      !Select First Tab
      If Thiswindow.request <> Insertrecord
          Select(?Lookup_Engineer)
      End
      !Paid To Date
      paid_chargeable_temp = ''
      paid_warranty_temp = ''
      setcursor(cursor:wait)
      save_jpt_id = access:jobpaymt.savefile()
      access:jobpaymt.clearkey(jpt:all_date_key)
      jpt:ref_number = job:ref_number
      set(jpt:all_date_key,jpt:all_date_key)
      loop
          if access:jobpaymt.next()
             break
          end !if
          if jpt:ref_number <> job:ref_number      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          paid_chargeable_temp += jpt:amount
      end !loop
      access:jobpaymt.restorefile(save_jpt_id)
      setcursor()
      !Invoiced
      If job:invoice_number <> ''
          job:courier_cost = job:invoice_courier_cost
          job:labour_cost  = job:invoice_labour_cost
          job:parts_cost   = job:invoice_parts_cost
      End!If job:invoice_number <> ''
      If job:invoice_number_warranty <> ''
          job:courier_cost_warranty = job:winvoice_courier_cost
          job:labour_cost_warranty  = job:winvoice_labour_cost
          job:parts_cost_warranty  =  job:winvoice_parts_cost
      End!If job:invoice_number_warranty <> ''
      
      Case job:edi
          Of 'EXC'
          Of 'YES'
          Of 'FIN'
      End!Case job:edi
      
      If job:invoiceStatus    = 'EXC'
      End!If job:invoiceStatus    = 'EXC'
      Do OutFaultList
      Do CheckRequiredFields
      !message('end of open window code')
    OF EVENT:GainFocus
      Do Fill_Lists
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
locAuditNotes STRING(255)
locAction STRING(30)
    Code

    local:ErrorCode  = 0
    If AccessoryCheck('JOB')
        If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 1
        Else!If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 2
        End !If AccessoryMismatch(job:Ref_Number,'JOB')
    End !If AccessoryCheck('JOB')

    Case local:Errorcode
        Of 0 Orof 2
            locAuditNotes         = 'ACCESSORIES:<13,10>'
        Of 1
            locAuditNotes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
    End !Case local:Errorcode

    !Add the list of accessories to the Audit Trail
    If job:Despatch_Type <> 'LOAN'
        Save_jac_ID = Access:JOBACC.SaveFile()
        Access:JOBACC.ClearKey(jac:Ref_Number_Key)
        jac:Ref_Number = job:Ref_Number
        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        Loop
            If Access:JOBACC.NEXT()
               Break
            End !If
            If jac:Ref_Number <> job:Ref_Number      |
                Then Break.  ! End If
            locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
        End !Loop
        Access:JOBACC.RestoreFile(Save_jac_ID)
    Else !If job:Despatch_Type <> 'LOAN'
        Save_lac_ID = Access:LOANACC.SaveFile()
        Access:LOANACC.ClearKey(lac:Ref_Number_Key)
        lac:Ref_Number = job:Ref_Number
        Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
        Loop
            If Access:LOANACC.NEXT()
               Break
            End !If
            If lac:Ref_Number <> job:Ref_Number      |
                Then Break.  ! End If
            locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(lac:Accessory)
        End !Loop
        Access:LOANACC.RestoreFile(Save_lac_ID)
    End !If job:Despatch_Type <> 'LOAN'

    Case local:Errorcode
        Of 1
            !If Error show the Accessories that were actually tagged
            locAuditNotes         = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).
    End!Case local:Errorcode

    Case local:ErrorCode
        Of 0
            locAction        = 'COMPLETION VALIDATION: PASSED ACCESSORIES'
        Of 1
            locAction        = 'COMPLETION VALIDATION: FAILED ACCESSORIES'
        Of 2
            locAction        = 'COMPLETION VALIDATION: PASSED ACCESSORIES (FORCED)'
    End !Case local:ErrorCode

    IF (AddToAudit(job:Ref_Number,'JOB',locAction,locAuditNotes))
    END ! IF

    !Do not complete
    If local:ErrorCode = 1
        Return Level:Fatal
    End !If local:ErrorCode = 1

    Return Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW12.SetQueueRecord PROCEDURE

  CODE
  If par:WebOrder
      Ordered_Temp = 'WEB ORDER'
  Else !par:WebOrder
      If par:adjustment <> 'YES'
          If par:exclude_from_order = 'YES'
              ordered_temp = 'EXCLUDED'
          Else!If par:exclude_from_order = 'YES'
              If par:order_number <> ''
                  If par:date_received = ''
                      ordered_temp = 'ON ORDER'
                  Else!If par:date_received = ''
                      ordered_temp    = 'RECD: ' & Format(par:date_received,@d6b)
                  End!If par:date_received = ''
              Else!If par:order_number <> ''
                  If par:pending_ref_number <> ''
                      ordered_temp = 'PENDING'
                  Else!If par:pending_ref_number <> ''
                      If par:part_ref_number <> ''
                          ordered_temp = 'FROM STOCK'
                      Else!If par:part_ref_number <> ''
                          ordered_temp = 'EXCLUDED'
                      End!If par:part_ref_number <> ''
                  End!If par:pending_ref_number <> ''
              End!If par:order_number <> ''
          End!If par:exclude_from_order = 'YES'
      Else
          ordered_temp = 'N/A'
      End
  
  End !par:WebOrder
  
  chargeable_part_total_temp  = par:quantity * par:sale_cost
  
  tmp:CColourFlag = 0
  If par:WebOrder
      tmp:CColourFlag = 6
  Else !par:WebOrder
      If par:PartAllocated Or GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
          If par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = ''
              tmp:CColourFlag = 1
          End !If par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = ''
  
          If par:order_number <> '' And par:date_received =''
              tmp:CColourFlag = 3
          End !If par:order_number <> '' And par:date_received =''
  
          If par:order_number <> '' And par:date_received<> ''
              tmp:CColourFlag = 4
          End !If par:order_number <> '' And par:date_received<> ''
      Else !If par:PartAllocated Or GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
          Case par:Status
              Of ''
                  tmp:CColourFlag = 1
              Of 'PRO'
                  tmp:CColourFlag = 5
              Of 'PIK'
                  tmp:CColourFlag = 2
              Of 'RET'
                  tmp:CColourFlag = 7
          End !Case par:Status
      End !If par:PartAllocated Or GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
  End !par:WebOrder
  ! Start - Show 'COR' if the part is a correction - TrkBs: 5844 (DBH: 27-05-2005)
  If par:Correction = True
      tmp:CPartQuantity = 'COR'
  Else ! If par:Correction = True
      tmp:CPartQuantity = par:Quantity
  End ! If par:Correction = True
  ! End   - Show 'COR' if the part is a correction - TrkBs: 5844 (DBH: 27-05-2005)
  PARENT.SetQueueRecord
  SELF.Q.par:Part_Number_NormalFG = -1
  SELF.Q.par:Part_Number_NormalBG = -1
  SELF.Q.par:Part_Number_SelectedFG = -1
  SELF.Q.par:Part_Number_SelectedBG = -1
  SELF.Q.par:Description_NormalFG = -1
  SELF.Q.par:Description_NormalBG = -1
  SELF.Q.par:Description_SelectedFG = -1
  SELF.Q.par:Description_SelectedBG = -1
  SELF.Q.par:Order_Number_NormalFG = -1
  SELF.Q.par:Order_Number_NormalBG = -1
  SELF.Q.par:Order_Number_SelectedFG = -1
  SELF.Q.par:Order_Number_SelectedBG = -1
  SELF.Q.ordered_temp_NormalFG = -1
  SELF.Q.ordered_temp_NormalBG = -1
  SELF.Q.ordered_temp_SelectedFG = -1
  SELF.Q.ordered_temp_SelectedBG = -1
  SELF.Q.tmp:CPartQuantity_NormalFG = -1
  SELF.Q.tmp:CPartQuantity_NormalBG = -1
  SELF.Q.tmp:CPartQuantity_SelectedFG = -1
  SELF.Q.tmp:CPartQuantity_SelectedBG = -1
  SELF.Q.par:Quantity_NormalFG = -1
  SELF.Q.par:Quantity_NormalBG = -1
  SELF.Q.par:Quantity_SelectedFG = -1
  SELF.Q.par:Quantity_SelectedBG = -1
  SELF.Q.chargeable_part_total_temp_NormalFG = -1
  SELF.Q.chargeable_part_total_temp_NormalBG = -1
  SELF.Q.chargeable_part_total_temp_SelectedFG = -1
  SELF.Q.chargeable_part_total_temp_SelectedBG = -1
  SELF.Q.par:Part_Ref_Number_NormalFG = -1
  SELF.Q.par:Part_Ref_Number_NormalBG = -1
  SELF.Q.par:Part_Ref_Number_SelectedFG = -1
  SELF.Q.par:Part_Ref_Number_SelectedBG = -1
  SELF.Q.tmp:CColourFlag_NormalFG = -1
  SELF.Q.tmp:CColourFlag_NormalBG = -1
  SELF.Q.tmp:CColourFlag_SelectedFG = -1
  SELF.Q.tmp:CColourFlag_SelectedBG = -1
   
   
   IF (tmp:CColourFlag = 1)
     SELF.Q.par:Part_Number_NormalFG = 32768
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.par:Part_Number_NormalFG = 8388608
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.par:Part_Number_NormalFG = 255
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.par:Part_Number_NormalFG = 8421504
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.par:Part_Number_NormalFG = 16711935
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.par:Part_Number_NormalFG = 8388736
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.par:Part_Number_NormalFG = 8421631
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 8421631
   ELSE
     SELF.Q.par:Part_Number_NormalFG = 0
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.par:Description_NormalFG = 32768
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.par:Description_NormalFG = 8388608
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.par:Description_NormalFG = 255
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.par:Description_NormalFG = 8421504
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.par:Description_NormalFG = 16711935
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.par:Description_NormalFG = 8388736
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.par:Description_NormalFG = 8421631
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 8421631
   ELSE
     SELF.Q.par:Description_NormalFG = 0
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.par:Order_Number_NormalFG = 32768
     SELF.Q.par:Order_Number_NormalBG = 16777215
     SELF.Q.par:Order_Number_SelectedFG = 16777215
     SELF.Q.par:Order_Number_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.par:Order_Number_NormalFG = 8388608
     SELF.Q.par:Order_Number_NormalBG = 16777215
     SELF.Q.par:Order_Number_SelectedFG = 16777215
     SELF.Q.par:Order_Number_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.par:Order_Number_NormalFG = 255
     SELF.Q.par:Order_Number_NormalBG = 16777215
     SELF.Q.par:Order_Number_SelectedFG = 16777215
     SELF.Q.par:Order_Number_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.par:Order_Number_NormalFG = 8421504
     SELF.Q.par:Order_Number_NormalBG = 16777215
     SELF.Q.par:Order_Number_SelectedFG = 16777215
     SELF.Q.par:Order_Number_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.par:Order_Number_NormalFG = 16711935
     SELF.Q.par:Order_Number_NormalBG = 16777215
     SELF.Q.par:Order_Number_SelectedFG = 16777215
     SELF.Q.par:Order_Number_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.par:Order_Number_NormalFG = 8388736
     SELF.Q.par:Order_Number_NormalBG = 16777215
     SELF.Q.par:Order_Number_SelectedFG = 16777215
     SELF.Q.par:Order_Number_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.par:Order_Number_NormalFG = 8421631
     SELF.Q.par:Order_Number_NormalBG = 16777215
     SELF.Q.par:Order_Number_SelectedFG = 16777215
     SELF.Q.par:Order_Number_SelectedBG = 8421631
   ELSE
     SELF.Q.par:Order_Number_NormalFG = 0
     SELF.Q.par:Order_Number_NormalBG = 16777215
     SELF.Q.par:Order_Number_SelectedFG = 16777215
     SELF.Q.par:Order_Number_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.ordered_temp_NormalFG = 32768
     SELF.Q.ordered_temp_NormalBG = 16777215
     SELF.Q.ordered_temp_SelectedFG = 16777215
     SELF.Q.ordered_temp_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.ordered_temp_NormalFG = 8388608
     SELF.Q.ordered_temp_NormalBG = 16777215
     SELF.Q.ordered_temp_SelectedFG = 16777215
     SELF.Q.ordered_temp_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.ordered_temp_NormalFG = 255
     SELF.Q.ordered_temp_NormalBG = 16777215
     SELF.Q.ordered_temp_SelectedFG = 16777215
     SELF.Q.ordered_temp_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.ordered_temp_NormalFG = 8421504
     SELF.Q.ordered_temp_NormalBG = 16777215
     SELF.Q.ordered_temp_SelectedFG = 16777215
     SELF.Q.ordered_temp_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.ordered_temp_NormalFG = 16711935
     SELF.Q.ordered_temp_NormalBG = 16777215
     SELF.Q.ordered_temp_SelectedFG = 16777215
     SELF.Q.ordered_temp_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.ordered_temp_NormalFG = 8388736
     SELF.Q.ordered_temp_NormalBG = 16777215
     SELF.Q.ordered_temp_SelectedFG = 16777215
     SELF.Q.ordered_temp_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.ordered_temp_NormalFG = 8421631
     SELF.Q.ordered_temp_NormalBG = 16777215
     SELF.Q.ordered_temp_SelectedFG = 16777215
     SELF.Q.ordered_temp_SelectedBG = 8421631
   ELSE
     SELF.Q.ordered_temp_NormalFG = 0
     SELF.Q.ordered_temp_NormalBG = 16777215
     SELF.Q.ordered_temp_SelectedFG = 16777215
     SELF.Q.ordered_temp_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.tmp:CPartQuantity_NormalFG = 32768
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.tmp:CPartQuantity_NormalFG = 8388608
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.tmp:CPartQuantity_NormalFG = 255
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.tmp:CPartQuantity_NormalFG = 8421504
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.tmp:CPartQuantity_NormalFG = 16711935
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.tmp:CPartQuantity_NormalFG = 8388736
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.tmp:CPartQuantity_NormalFG = 8421631
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:CPartQuantity_NormalFG = 0
     SELF.Q.tmp:CPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:CPartQuantity_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.par:Quantity_NormalFG = 32768
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.par:Quantity_NormalFG = 8388608
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.par:Quantity_NormalFG = 255
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.par:Quantity_NormalFG = 8421504
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.par:Quantity_NormalFG = 16711935
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.par:Quantity_NormalFG = 8388736
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.par:Quantity_NormalFG = 8421631
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 8421631
   ELSE
     SELF.Q.par:Quantity_NormalFG = 0
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.chargeable_part_total_temp_NormalFG = 32768
     SELF.Q.chargeable_part_total_temp_NormalBG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedFG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.chargeable_part_total_temp_NormalFG = 8388608
     SELF.Q.chargeable_part_total_temp_NormalBG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedFG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.chargeable_part_total_temp_NormalFG = 255
     SELF.Q.chargeable_part_total_temp_NormalBG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedFG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.chargeable_part_total_temp_NormalFG = 8421504
     SELF.Q.chargeable_part_total_temp_NormalBG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedFG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.chargeable_part_total_temp_NormalFG = 16711935
     SELF.Q.chargeable_part_total_temp_NormalBG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedFG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.chargeable_part_total_temp_NormalFG = 8388736
     SELF.Q.chargeable_part_total_temp_NormalBG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedFG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.chargeable_part_total_temp_NormalFG = 8421631
     SELF.Q.chargeable_part_total_temp_NormalBG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedFG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedBG = 8421631
   ELSE
     SELF.Q.chargeable_part_total_temp_NormalFG = 0
     SELF.Q.chargeable_part_total_temp_NormalBG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedFG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.par:Part_Ref_Number_NormalFG = 32768
     SELF.Q.par:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.par:Part_Ref_Number_NormalFG = 8388608
     SELF.Q.par:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.par:Part_Ref_Number_NormalFG = 255
     SELF.Q.par:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.par:Part_Ref_Number_NormalFG = 8421504
     SELF.Q.par:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.par:Part_Ref_Number_NormalFG = 16711935
     SELF.Q.par:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.par:Part_Ref_Number_NormalFG = 8388736
     SELF.Q.par:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.par:Part_Ref_Number_NormalFG = 8421631
     SELF.Q.par:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedBG = 8421631
   ELSE
     SELF.Q.par:Part_Ref_Number_NormalFG = 0
     SELF.Q.par:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedBG = 0
   END
   IF (tmp:CColourFlag = 1)
     SELF.Q.tmp:CColourFlag_NormalFG = 32768
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 32768
   ELSIF(tmp:CColourFlag = 2)
     SELF.Q.tmp:CColourFlag_NormalFG = 8388608
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 8388608
   ELSIF(tmp:CColourFlag = 3)
     SELF.Q.tmp:CColourFlag_NormalFG = 255
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 255
   ELSIF(tmp:CColourFlag = 4)
     SELF.Q.tmp:CColourFlag_NormalFG = 8421504
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 8421504
   ELSIF(tmp:CColourFlag = 5)
     SELF.Q.tmp:CColourFlag_NormalFG = 16711935
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 16711935
   ELSIF(tmp:CColourFlag = 6)
     SELF.Q.tmp:CColourFlag_NormalFG = 8388736
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 8388736
   ELSIF(tmp:CColourFlag = 7)
     SELF.Q.tmp:CColourFlag_NormalFG = 8421631
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:CColourFlag_NormalFG = 0
     SELF.Q.tmp:CColourFlag_NormalBG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:CColourFlag_SelectedBG = 0
   END


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW77.SetQueueRecord PROCEDURE

  CODE
  estimate_parts_cost_temp  = epr:quantity * epr:sale_cost
  PARENT.SetQueueRecord
  SELF.Q.epr:Part_Number_NormalFG = -1
  SELF.Q.epr:Part_Number_NormalBG = -1
  SELF.Q.epr:Part_Number_SelectedFG = -1
  SELF.Q.epr:Part_Number_SelectedBG = -1
  SELF.Q.epr:Description_NormalFG = -1
  SELF.Q.epr:Description_NormalBG = -1
  SELF.Q.epr:Description_SelectedFG = -1
  SELF.Q.epr:Description_SelectedBG = -1
  SELF.Q.epr:Quantity_NormalFG = -1
  SELF.Q.epr:Quantity_NormalBG = -1
  SELF.Q.epr:Quantity_SelectedFG = -1
  SELF.Q.epr:Quantity_SelectedBG = -1
  SELF.Q.estimate_parts_cost_temp_NormalFG = -1
  SELF.Q.estimate_parts_cost_temp_NormalBG = -1
  SELF.Q.estimate_parts_cost_temp_SelectedFG = -1
  SELF.Q.estimate_parts_cost_temp_SelectedBG = -1
   
   
   IF (epr:UsedOnRepair = 1)
     SELF.Q.epr:Part_Number_NormalFG = 255
     SELF.Q.epr:Part_Number_NormalBG = 16777215
     SELF.Q.epr:Part_Number_SelectedFG = 16777215
     SELF.Q.epr:Part_Number_SelectedBG = 255
   ELSE
     SELF.Q.epr:Part_Number_NormalFG = -1
     SELF.Q.epr:Part_Number_NormalBG = -1
     SELF.Q.epr:Part_Number_SelectedFG = -1
     SELF.Q.epr:Part_Number_SelectedBG = -1
   END
   IF (epr:UsedOnRepair = 1)
     SELF.Q.epr:Description_NormalFG = 255
     SELF.Q.epr:Description_NormalBG = 16777215
     SELF.Q.epr:Description_SelectedFG = 16777215
     SELF.Q.epr:Description_SelectedBG = 255
   ELSE
     SELF.Q.epr:Description_NormalFG = -1
     SELF.Q.epr:Description_NormalBG = -1
     SELF.Q.epr:Description_SelectedFG = -1
     SELF.Q.epr:Description_SelectedBG = -1
   END
   IF (epr:UsedOnRepair = 1)
     SELF.Q.epr:Quantity_NormalFG = 255
     SELF.Q.epr:Quantity_NormalBG = 16777215
     SELF.Q.epr:Quantity_SelectedFG = 16777215
     SELF.Q.epr:Quantity_SelectedBG = 255
   ELSE
     SELF.Q.epr:Quantity_NormalFG = -1
     SELF.Q.epr:Quantity_NormalBG = -1
     SELF.Q.epr:Quantity_SelectedFG = -1
     SELF.Q.epr:Quantity_SelectedBG = -1
   END
   IF (epr:UsedOnRepair = 1)
     SELF.Q.estimate_parts_cost_temp_NormalFG = 255
     SELF.Q.estimate_parts_cost_temp_NormalBG = 16777215
     SELF.Q.estimate_parts_cost_temp_SelectedFG = 16777215
     SELF.Q.estimate_parts_cost_temp_SelectedBG = 255
   ELSE
     SELF.Q.estimate_parts_cost_temp_NormalFG = -1
     SELF.Q.estimate_parts_cost_temp_NormalBG = -1
     SELF.Q.estimate_parts_cost_temp_SelectedFG = -1
     SELF.Q.estimate_parts_cost_temp_SelectedBG = -1
   END


BRW77.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW79.SetQueueRecord PROCEDURE

  CODE
  If wpr:WebOrder
      Warranty_Ordered_Temp = 'WEB ORDER'
  Else !wpr:WebJob
      If wpr:adjustment <> 'YES'
          If wpr:exclude_from_order = 'YES'
              warranty_ordered_temp = 'EXCLUDED'
          Else!If wpr:exclude_from_order = 'YES'
              If wpr:order_number <> ''
                  If wpr:date_received = ''
                      warranty_ordered_temp = 'ON ORDER'
                  Else!If wpr:date_received = ''
                      warranty_ordered_temp    = 'RECD: ' & Format(wpr:date_received,@d6b)
                  End!If wpr:date_received = ''
              Else!If wpr:order_number <> ''
                  If wpr:pending_ref_number <> ''
                      warranty_ordered_temp = 'PENDING'
                  Else!If wpr:pending_ref_number <> ''
                      If wpr:part_ref_number <> ''
                          warranty_ordered_temp = 'FROM STOCK'
                      Else!If wpr:part_ref_number <> ''
                          warranty_ordered_temp = 'EXCLUDED'
                      End!If wpr:part_ref_number <> ''
                  End!If wpr:pending_ref_number <> ''
              End!If wpr:order_number <> ''
          End!If wpr:exclude_from_order = 'YES'
      Else
          warranty_ordered_temp = 'N/A'
      End
  
  End !wpr:WebJob
  
  warranty_part_total_temp  = wpr:quantity * wpr:purchase_cost
  
  tmp:WColourFlag = 0
  If wpr:WebOrder
      tmp:WColourFlag = 6
  Else !wpr:WebOrder
      If wpr:PartAllocated Or GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
          If wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = ''
              tmp:WColourFlag = 1
          End !If wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = ''
  
          If wpr:order_number <> '' And wpr:date_received =''
              tmp:WColourFlag = 3
          End !If wpr:order_number <> '' And wpr:date_received =''
  
          If wpr:order_number <> '' And wpr:date_received<> ''
              tmp:WColourFlag = 4
          End !If wpr:order_number <> '' And wpr:date_received<> ''
      Else !If wpr:PartAllocated Or GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
          Case wpr:Status
              Of ''
                  tmp:WColourFlag = 1
              Of 'PRO'
                  tmp:WColourFlag = 5
              Of 'PIK'
                  tmp:WColourFlag = 2
              Of 'RET'
                  tmp:WColourFlag = 7
          End !Case wpr:Status
      End !If wpr:PartAllocated Or GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
  End !wpr:WebOrder
  ! Start - Show 'COR' if the part is a correction - TrkBs: 5844 (DBH: 27-05-2005)
  If wpr:Correction = True
      tmp:WPartQuantity = 'COR'
  Else ! If par:Correction = True
      tmp:WPartQuantity = wpr:Quantity
  End ! If par:Correction = True
  ! End   - Show 'COR' if the part is a correction - TrkBs: 5844 (DBH: 27-05-2005)
  
  PARENT.SetQueueRecord
  SELF.Q.wpr:Part_Number_NormalFG = -1
  SELF.Q.wpr:Part_Number_NormalBG = -1
  SELF.Q.wpr:Part_Number_SelectedFG = -1
  SELF.Q.wpr:Part_Number_SelectedBG = -1
  SELF.Q.wpr:Description_NormalFG = -1
  SELF.Q.wpr:Description_NormalBG = -1
  SELF.Q.wpr:Description_SelectedFG = -1
  SELF.Q.wpr:Description_SelectedBG = -1
  SELF.Q.wpr:Order_Number_NormalFG = -1
  SELF.Q.wpr:Order_Number_NormalBG = -1
  SELF.Q.wpr:Order_Number_SelectedFG = -1
  SELF.Q.wpr:Order_Number_SelectedBG = -1
  SELF.Q.warranty_ordered_temp_NormalFG = -1
  SELF.Q.warranty_ordered_temp_NormalBG = -1
  SELF.Q.warranty_ordered_temp_SelectedFG = -1
  SELF.Q.warranty_ordered_temp_SelectedBG = -1
  SELF.Q.tmp:WPartQuantity_NormalFG = -1
  SELF.Q.tmp:WPartQuantity_NormalBG = -1
  SELF.Q.tmp:WPartQuantity_SelectedFG = -1
  SELF.Q.tmp:WPartQuantity_SelectedBG = -1
  SELF.Q.wpr:Quantity_NormalFG = -1
  SELF.Q.wpr:Quantity_NormalBG = -1
  SELF.Q.wpr:Quantity_SelectedFG = -1
  SELF.Q.wpr:Quantity_SelectedBG = -1
  SELF.Q.warranty_part_total_temp_NormalFG = -1
  SELF.Q.warranty_part_total_temp_NormalBG = -1
  SELF.Q.warranty_part_total_temp_SelectedFG = -1
  SELF.Q.warranty_part_total_temp_SelectedBG = -1
  SELF.Q.wpr:Part_Ref_Number_NormalFG = -1
  SELF.Q.wpr:Part_Ref_Number_NormalBG = -1
  SELF.Q.wpr:Part_Ref_Number_SelectedFG = -1
  SELF.Q.wpr:Part_Ref_Number_SelectedBG = -1
  SELF.Q.tmp:WColourFlag_NormalFG = -1
  SELF.Q.tmp:WColourFlag_NormalBG = -1
  SELF.Q.tmp:WColourFlag_SelectedFG = -1
  SELF.Q.tmp:WColourFlag_SelectedBG = -1
   
   
   IF (tmp:WColourFlag = 1)
     SELF.Q.wpr:Part_Number_NormalFG = 32768
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.wpr:Part_Number_NormalFG = 8388608
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.wpr:Part_Number_NormalFG = 255
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.wpr:Part_Number_NormalFG = 8421504
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.wpr:Part_Number_NormalFG = 16711935
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.wpr:Part_Number_NormalFG = 8388736
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.wpr:Part_Number_NormalFG = 8421631
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 8421631
   ELSE
     SELF.Q.wpr:Part_Number_NormalFG = 0
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.wpr:Description_NormalFG = 32768
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.wpr:Description_NormalFG = 8388608
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.wpr:Description_NormalFG = 255
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.wpr:Description_NormalFG = 8421504
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.wpr:Description_NormalFG = 16711935
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.wpr:Description_NormalFG = 8388736
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.wpr:Description_NormalFG = 8421631
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 8421631
   ELSE
     SELF.Q.wpr:Description_NormalFG = 0
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.wpr:Order_Number_NormalFG = 32768
     SELF.Q.wpr:Order_Number_NormalBG = 16777215
     SELF.Q.wpr:Order_Number_SelectedFG = 16777215
     SELF.Q.wpr:Order_Number_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.wpr:Order_Number_NormalFG = 8388608
     SELF.Q.wpr:Order_Number_NormalBG = 16777215
     SELF.Q.wpr:Order_Number_SelectedFG = 16777215
     SELF.Q.wpr:Order_Number_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.wpr:Order_Number_NormalFG = 255
     SELF.Q.wpr:Order_Number_NormalBG = 16777215
     SELF.Q.wpr:Order_Number_SelectedFG = 16777215
     SELF.Q.wpr:Order_Number_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.wpr:Order_Number_NormalFG = 8421504
     SELF.Q.wpr:Order_Number_NormalBG = 16777215
     SELF.Q.wpr:Order_Number_SelectedFG = 16777215
     SELF.Q.wpr:Order_Number_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.wpr:Order_Number_NormalFG = 16711935
     SELF.Q.wpr:Order_Number_NormalBG = 16777215
     SELF.Q.wpr:Order_Number_SelectedFG = 16777215
     SELF.Q.wpr:Order_Number_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.wpr:Order_Number_NormalFG = 8388736
     SELF.Q.wpr:Order_Number_NormalBG = 16777215
     SELF.Q.wpr:Order_Number_SelectedFG = 16777215
     SELF.Q.wpr:Order_Number_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.wpr:Order_Number_NormalFG = 8421631
     SELF.Q.wpr:Order_Number_NormalBG = 16777215
     SELF.Q.wpr:Order_Number_SelectedFG = 16777215
     SELF.Q.wpr:Order_Number_SelectedBG = 8421631
   ELSE
     SELF.Q.wpr:Order_Number_NormalFG = 0
     SELF.Q.wpr:Order_Number_NormalBG = 16777215
     SELF.Q.wpr:Order_Number_SelectedFG = 16777215
     SELF.Q.wpr:Order_Number_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.warranty_ordered_temp_NormalFG = 32768
     SELF.Q.warranty_ordered_temp_NormalBG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedFG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.warranty_ordered_temp_NormalFG = 8388608
     SELF.Q.warranty_ordered_temp_NormalBG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedFG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.warranty_ordered_temp_NormalFG = 255
     SELF.Q.warranty_ordered_temp_NormalBG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedFG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.warranty_ordered_temp_NormalFG = 8421504
     SELF.Q.warranty_ordered_temp_NormalBG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedFG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.warranty_ordered_temp_NormalFG = 16711935
     SELF.Q.warranty_ordered_temp_NormalBG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedFG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.warranty_ordered_temp_NormalFG = 8388736
     SELF.Q.warranty_ordered_temp_NormalBG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedFG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.warranty_ordered_temp_NormalFG = 8421631
     SELF.Q.warranty_ordered_temp_NormalBG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedFG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedBG = 8421631
   ELSE
     SELF.Q.warranty_ordered_temp_NormalFG = 0
     SELF.Q.warranty_ordered_temp_NormalBG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedFG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.tmp:WPartQuantity_NormalFG = 32768
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.tmp:WPartQuantity_NormalFG = 8388608
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.tmp:WPartQuantity_NormalFG = 255
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.tmp:WPartQuantity_NormalFG = 8421504
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.tmp:WPartQuantity_NormalFG = 16711935
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.tmp:WPartQuantity_NormalFG = 8388736
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.tmp:WPartQuantity_NormalFG = 8421631
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:WPartQuantity_NormalFG = 0
     SELF.Q.tmp:WPartQuantity_NormalBG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedFG = 16777215
     SELF.Q.tmp:WPartQuantity_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.wpr:Quantity_NormalFG = 32768
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.wpr:Quantity_NormalFG = 8388608
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.wpr:Quantity_NormalFG = 255
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.wpr:Quantity_NormalFG = 8421504
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.wpr:Quantity_NormalFG = 16711935
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.wpr:Quantity_NormalFG = 8388736
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.wpr:Quantity_NormalFG = 8421631
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 8421631
   ELSE
     SELF.Q.wpr:Quantity_NormalFG = 0
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.warranty_part_total_temp_NormalFG = 32768
     SELF.Q.warranty_part_total_temp_NormalBG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedFG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.warranty_part_total_temp_NormalFG = 8388608
     SELF.Q.warranty_part_total_temp_NormalBG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedFG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.warranty_part_total_temp_NormalFG = 255
     SELF.Q.warranty_part_total_temp_NormalBG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedFG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.warranty_part_total_temp_NormalFG = 8421504
     SELF.Q.warranty_part_total_temp_NormalBG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedFG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.warranty_part_total_temp_NormalFG = 16711935
     SELF.Q.warranty_part_total_temp_NormalBG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedFG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.warranty_part_total_temp_NormalFG = 8388736
     SELF.Q.warranty_part_total_temp_NormalBG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedFG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.warranty_part_total_temp_NormalFG = 8421631
     SELF.Q.warranty_part_total_temp_NormalBG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedFG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedBG = 8421631
   ELSE
     SELF.Q.warranty_part_total_temp_NormalFG = 0
     SELF.Q.warranty_part_total_temp_NormalBG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedFG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.wpr:Part_Ref_Number_NormalFG = 32768
     SELF.Q.wpr:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.wpr:Part_Ref_Number_NormalFG = 8388608
     SELF.Q.wpr:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.wpr:Part_Ref_Number_NormalFG = 255
     SELF.Q.wpr:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.wpr:Part_Ref_Number_NormalFG = 8421504
     SELF.Q.wpr:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.wpr:Part_Ref_Number_NormalFG = 16711935
     SELF.Q.wpr:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.wpr:Part_Ref_Number_NormalFG = 8388736
     SELF.Q.wpr:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.wpr:Part_Ref_Number_NormalFG = 8421631
     SELF.Q.wpr:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = 8421631
   ELSE
     SELF.Q.wpr:Part_Ref_Number_NormalFG = 0
     SELF.Q.wpr:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = 0
   END
   IF (tmp:WColourFlag = 1)
     SELF.Q.tmp:WColourFlag_NormalFG = 32768
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 32768
   ELSIF(tmp:WColourFlag = 2)
     SELF.Q.tmp:WColourFlag_NormalFG = 8388608
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 8388608
   ELSIF(tmp:WColourFlag = 3)
     SELF.Q.tmp:WColourFlag_NormalFG = 255
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 255
   ELSIF(tmp:WColourFlag = 4)
     SELF.Q.tmp:WColourFlag_NormalFG = 8421504
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 8421504
   ELSIF(tmp:WColourFlag = 5)
     SELF.Q.tmp:WColourFlag_NormalFG = 16711935
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 16711935
   ELSIF(tmp:WColourFlag = 6)
     SELF.Q.tmp:WColourFlag_NormalFG = 8388736
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 8388736
   ELSIF(tmp:WColourFlag = 7)
     SELF.Q.tmp:WColourFlag_NormalFG = 8421631
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 8421631
   ELSE
     SELF.Q.tmp:WColourFlag_NormalFG = 0
     SELF.Q.tmp:WColourFlag_NormalBG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:WColourFlag_SelectedBG = 0
   END


BRW79.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Collection_Text PROCEDURE                             !Generated from procedure template - Window

FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Collection Text'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Collection Text'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Collection Text'),AT(232,148),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(232,160,216,64),USE(jbn:Collection_Text),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Contact Name'),AT(232,232),USE(?JBN:ColContatName:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(324,232,124,10),USE(jbn:ColContatName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Contact Name'),TIP('Contact Name'),UPR
                           PROMPT('Department'),AT(232,248),USE(?JBN:ColDepartment:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(324,248,124,10),USE(jbn:ColDepartment),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Department'),TIP('Department'),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Changing Collection Text'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020410'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Collection_Text')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:JOBNOTES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBNOTES.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBNOTES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Collection_Text')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Collection_Text')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020410'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020410'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020410'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Delivery_Text PROCEDURE                               !Generated from procedure template - Window

FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Delivery Text'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Delivery Text'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Delivery Text'),AT(232,146),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(232,156,216,64),USE(jbn:Delivery_Text),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Contact Name'),AT(232,228),USE(?JBN:DelContactName:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(324,228,124,10),USE(jbn:DelContactName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Contact Name'),TIP('Contact Name'),UPR
                           PROMPT('Department'),AT(232,244),USE(?JBN:DelDepartment:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(324,244,124,10),USE(jbn:DelDepartment),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Department'),TIP('Department'),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Changing Delivery Text'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020411'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Delivery_Text')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:JOBNOTES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBNOTES.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBNOTES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Delivery_Text')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Delivery_Text')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020411'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020411'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020411'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
EngineersText PROCEDURE                               !Generated from procedure template - Window

FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Engineers Notes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Engineer''s Notes'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,246),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           BUTTON,AT(204,166),USE(?Lookup_Engineers_Notes),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           TEXT,AT(238,166,204,86),USE(jbn:Engineers_Notes),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting'
  OF ChangeRecord
    ActionMessage = 'Changing Fault Description'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020412'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('EngineersText')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:JOBNOTES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBNOTES.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBNOTES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','EngineersText')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','EngineersText')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Lookup_Engineers_Notes
      glo:select1 = job:manufacturer
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020412'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020412'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020412'&'0')
      ***
    OF ?Lookup_Engineers_Notes
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Engineers_Notes
      ThisWindow.Reset
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If jbn:Engineers_Notes = ''
                  jbn:Engineers_Notes = Clip(glo:notes_pointer)
              Else !If job:engineers_notes = ''
                  jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) & '. ' & Clip(glo:notes_pointer)
              End !If job:engineers_notes = ''
          End
          Display()
      End
      Clear(glo:Q_Notes)
      Free(glo:Q_Notes)
      glo:select1 = ''
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
FreeTextOutFault PROCEDURE                            !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
FreeTextOutFault_Entry STRING(255)
window               WINDOW('Free Text Out Fault'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Free Text Out Fault'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Free Text Out Fault'),USE(?Tab1)
                           PROMPT('Free Text Out Fault'),AT(256,154),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(256,164,172,82),USE(FreeTextOutFault_Entry),FONT(,,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020416'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('FreeTextOutFault')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBOUTFL.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','FreeTextOutFault')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBOUTFL.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','FreeTextOutFault')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020416'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020416'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020416'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      IF FreeTextOutFault_Entry = ''
        SELECT(?FreeTextOutFault_Entry)
        CYCLE
      END
      
      IF Access:JobOutFl.PrimeRecord()
        !Error!
      ELSE
        joo:JobNumber = job:Ref_Number
        joo:FaultCode = 0
        joo:Description = FreeTextOutFault_Entry
        joo:Level = 0
        Access:JobOutFl.Update()
      END
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
OutFaultList PROCEDURE (func:Manufacturer,func:JobNumber,LinkedFaultCodeQueue) !Generated from procedure template - Window

Local                CLASS
WarrantyCode         Procedure(Long func:FieldNumber,String func:Field,String func:Type)
                     END
save_par_id          USHORT,AUTO
save_epr_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_maf_id          USHORT,AUTO
save_mfo_id          USHORT,AUTO
tmp:Manufacturer     STRING(30)
tmp:FieldNumber      LONG
tmp:JobNumber        LONG
tmp:Description      STRING(255)
tmp:War_Desc         STRING(255)
tmp:EDescription     STRING(255)
Tmp:ERepair          LONG
Tmp:WRepair          LONG
tmp:MRepair          LONG
tmp:Field            STRING(30)
tmp:SkillLevel       LONG
WarPartsQueue        QUEUE,PRE(warque)
FaultCode            STRING(30),NAME('warqueFaultCode')
Description          STRING(30),NAME('warqueDescription')
Level                LONG
                     END
ChaPartsQueue        QUEUE,PRE(chaque)
FaultCode            STRING(30),NAME('chaqueFaultCode')
Description          STRING(30),NAME('chaqueDescription')
Level                LONG
                     END
EstPartsQueue        QUEUE,PRE(estque)
FaultCode            STRING(30),NAME('estqueFaultCode')
Description          STRING(30),NAME('estqueDescription')
Level                LONG
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW5::View:Browse    VIEW(MANFAULO)
                       PROJECT(mfo:Field)
                       PROJECT(mfo:Description)
                       PROJECT(mfo:ImportanceLevel)
                       PROJECT(mfo:SkillLevel)
                       PROJECT(mfo:RecordNumber)
                       PROJECT(mfo:NotAvailable)
                       PROJECT(mfo:Manufacturer)
                       PROJECT(mfo:Field_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
mfo:Field              LIKE(mfo:Field)                !List box control field - type derived from field
mfo:Description        LIKE(mfo:Description)          !List box control field - type derived from field
mfo:ImportanceLevel    LIKE(mfo:ImportanceLevel)      !List box control field - type derived from field
mfo:SkillLevel         LIKE(mfo:SkillLevel)           !List box control field - type derived from field
mfo:RecordNumber       LIKE(mfo:RecordNumber)         !Primary key field - type derived from field
mfo:NotAvailable       LIKE(mfo:NotAvailable)         !Browse key field - type derived from field
mfo:Manufacturer       LIKE(mfo:Manufacturer)         !Browse key field - type derived from field
mfo:Field_Number       LIKE(mfo:Field_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(JOBOUTFL)
                       PROJECT(joo:FaultCode)
                       PROJECT(joo:Description)
                       PROJECT(joo:Level)
                       PROJECT(joo:RecordNumber)
                       PROJECT(joo:JobNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
joo:FaultCode          LIKE(joo:FaultCode)            !List box control field - type derived from field
joo:Description        LIKE(joo:Description)          !List box control field - type derived from field
joo:Description_Tip    STRING(80)                     !Field tooltip
joo:Level              LIKE(joo:Level)                !List box control field - type derived from field
tmp:MRepair            LIKE(tmp:MRepair)              !Browse hot field - type derived from local data
joo:RecordNumber       LIKE(joo:RecordNumber)         !Primary key field - type derived from field
joo:JobNumber          LIKE(joo:JobNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK7::mfo:Field_Number     LIKE(mfo:Field_Number)
HK7::mfo:Manufacturer     LIKE(mfo:Manufacturer)
HK7::mfo:NotAvailable     LIKE(mfo:NotAvailable)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Attach Out Fault Codes To Job'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Insert / Amend Out Fault Codes On Job'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,28,332,224),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('By Fault Codes'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(8,48,324,172),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('58L(2)|M~Field~@s30@158L(2)|M~Description~@s60@47R(2)|M~Repair Index~L@n8@47R(2)' &|
   '|M~Skill Level~L@n8@'),FROM(Queue:Browse)
                           BUTTON,AT(268,224),USE(?AddFault),TRN,FLAT,RIGHT,ICON('addfaup.jpg')
                         END
                       END
                       SHEET,AT(340,28,336,224),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Main Out Faults'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(344,48,328,126),USE(?List:2),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('58L(2)|M~Fault Code~@s30@203L(2)|MP~Description~@s255@32R(2)|M~Repair Index~L@n8' &|
   '@'),FROM(Queue:Browse:1)
                           PROMPT('Description:'),AT(484,178),USE(?Prompt3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(484,188,188,60),USE(joo:Description),SKIP,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           BUTTON,AT(344,224),USE(?FreeText),TRN,FLAT,LEFT,ICON('freetxtp.jpg')
                           BUTTON,AT(412,224),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       SHEET,AT(4,256,332,124),USE(?Sheet3),COLOR(0D6E7EFH),SPREAD
                         TAB('Chargeable Part Out Faults'),USE(?ChargeableTab),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(8,272,324,104),USE(?List6),FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('62L(2)|M~Fault Code~@s30@200L(2)|M~Description~@s30@32L(2)|M~Level~@s8@'),FROM(ChaPartsQueue)
                         END
                         TAB('Estimate Part Out Faults'),USE(?EstimateTab),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(8,272,324,104),USE(?List7),FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('62L(2)|M~Fault Code~@s30@200L(2)|M~Description~@s30@32L(2)|M~Level~@s8@'),FROM(EstPartsQueue)
                         END
                       END
                       SHEET,AT(340,256,336,124),USE(?Sheet4),COLOR(0D6E7EFH),SPREAD
                         TAB('Warranty Part Out Faults'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(344,272,328,104),USE(?List5),FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('62L(2)|M~Fault Code~@s30@200L(2)|M~Description~@s30@32L(2)|M~Level~@s8@'),FROM(WarPartsQueue)
                         END
                       END
                       BUTTON,AT(608,384),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW6                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020390'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('OutFaultList')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBOUTFL.Open
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:WARPARTS.UseFile
  Access:USERS.UseFile
  Access:MANFAURL.UseFile
  SELF.FilesOpened = True
  tmp:Manufacturer    = func:Manufacturer
  tmp:JobNumber       = func:JobNumber
  
  tmp:FieldNumber = 0
  Save_maf_ID = Access:MANFAULT.SaveFile()
  Access:MANFAULT.ClearKey(maf:Field_Number_Key)
  maf:Manufacturer = tmp:Manufacturer
  Set(maf:Field_Number_Key,maf:Field_Number_Key)
  Loop
      If Access:MANFAULT.NEXT()
         Break
      End !If
      If maf:Manufacturer <> tmp:Manufacturer       |
          Then Break.  ! End If
      If maf:MainFault
          tmp:FieldNumber = maf:Field_Number
          Break
      End !If maf:MainFault
  End !Loop
  Access:MANFAULT.RestoreFile(Save_maf_ID)
  
  If tmp:FieldNumber = 0
      Case Missive('None of the fault codes attached to this Manufacturer have been marked as the "Out Fault".','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
  End !tmp:FieldNumber = 0
  
  If job:Engineer <> ''
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code   = job:Engineer
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Found
          tmp:SkillLevel = use:SkillLevel
      Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  Else !job:Engineer <> ''
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = glo:Password
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:SkillLevel = use:SkillLevel
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  End !job:Engineer <> ''
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:MANFAULO,SELF)
  BRW6.Init(?List:2,Queue:Browse:1.ViewPosition,BRW6::View:Browse,Queue:Browse:1,Relate:JOBOUTFL,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If job:Estimate = 'YES' and job:Chargeable_Job = 'YES' And |
      job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
      ?ChargeableTab{prop:Hide} = 1
      ?Estimatetab{prop:Hide} = 0
  Else !job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
      ?ChargeableTab{prop:Hide} = 0
      ?Estimatetab{prop:Hide} = 1
  End !job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
  
  
  !Let's Build the Temp Queues up, shall we?
  Save_wpr_ID = Access:WARPARTS.SaveFile()
  Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
  wpr:Ref_Number  = job:Ref_Number
  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
  Loop
      If Access:WARPARTS.NEXT()
         Break
      End !If
      If wpr:Ref_Number  <> job:Ref_Number      |
          Then Break.  ! End If
      If wpr:Fault_Code1 <> ''
          Local.WarrantyCode(1,wpr:fault_Code1,'W')
      End !If wpr:Fault_Code1
      If wpr:Fault_Code2 <> ''
          Local.WarrantyCode(2,wpr:fault_Code2,'W')
      End !If wpr:Fault_Code1
      If wpr:Fault_Code3 <> ''
          Local.WarrantyCode(3,wpr:fault_Code3,'W')
      End !If wpr:Fault_Code1
      If wpr:Fault_Code4 <> ''
          Local.WarrantyCode(4,wpr:fault_Code4,'W')
      End !If wpr:Fault_Code1
      If wpr:Fault_Code5 <> ''
          Local.WarrantyCode(5,wpr:fault_Code5,'W')
      End !If wpr:Fault_Code1
      If wpr:Fault_Code6 <> ''
          Local.WarrantyCode(6,wpr:fault_Code6,'W')
      End !If wpr:Fault_Code1
      If wpr:Fault_Code7 <> ''
          Local.WarrantyCode(7,wpr:fault_Code7,'W')
      End !If wpr:Fault_Code1
      If wpr:Fault_Code8 <> ''
          Local.WarrantyCode(8,wpr:fault_Code8,'W')
      End !If wpr:Fault_Code1
      If wpr:Fault_Code9 <> ''
          Local.WarrantyCode(9,wpr:fault_Code9,'W')
      End !If wpr:Fault_Code1
      If wpr:Fault_Code10 <> ''
          Local.WarrantyCode(10,wpr:fault_Code10,'W')
      End !If wpr:Fault_Code1
      If wpr:Fault_Code11 <> ''
          Local.WarrantyCode(11,wpr:fault_Code11,'W')
      End !If wpr:Fault_Code1
      If wpr:Fault_Code12 <> ''
          Local.WarrantyCode(12,wpr:fault_Code12,'W')
      End !If wpr:Fault_Code1
  
  End !Loop
  Access:WARPARTS.RestoreFile(Save_wpr_ID)
  
  Save_par_ID = Access:PARTS.SaveFile()
  Access:PARTS.ClearKey(par:Part_Number_Key)
  par:Ref_Number  = job:Ref_Number
  Set(par:Part_Number_Key,par:Part_Number_Key)
  Loop
      If Access:PARTS.NEXT()
         Break
      End !If
      If par:Ref_Number  <> job:Ref_Number      |
          Then Break.  ! End If
      If par:Fault_Code1 <> ''
          Local.WarrantyCode(1,par:fault_Code1,'C')
      End !If wpr:Fault_Code1
      If par:Fault_Code2 <> ''
          Local.WarrantyCode(2,par:fault_Code2,'C')
      End !If wpr:Fault_Code1
      If par:Fault_Code3 <> ''
          Local.WarrantyCode(3,par:fault_Code3,'C')
      End !If wpr:Fault_Code1
      If par:Fault_Code4 <> ''
          Local.WarrantyCode(4,par:fault_Code4,'C')
      End !If wpr:Fault_Code1
      If par:Fault_Code5 <> ''
          Local.WarrantyCode(5,par:fault_Code5,'C')
      End !If wpr:Fault_Code1
      If par:Fault_Code6 <> ''
          Local.WarrantyCode(6,par:fault_Code6,'C')
      End !If wpr:Fault_Code1
      If par:Fault_Code7 <> ''
          Local.WarrantyCode(7,par:fault_Code7,'C')
      End !If wpr:Fault_Code1
      If par:Fault_Code8 <> ''
          Local.WarrantyCode(8,par:fault_Code8,'C')
      End !If wpr:Fault_Code1
      If par:Fault_Code9 <> ''
          Local.WarrantyCode(9,par:fault_Code9,'C')
      End !If wpr:Fault_Code1
      If par:Fault_Code10 <> ''
          Local.WarrantyCode(10,par:fault_Code10,'C')
      End !If wpr:Fault_Code1
      If par:Fault_Code11 <> ''
          Local.WarrantyCode(11,par:fault_Code11,'C')
      End !If wpr:Fault_Code1
      If par:Fault_Code12 <> ''
          Local.WarrantyCode(12,par:fault_Code12,'C')
      End !If wpr:Fault_Code1
  
  End !Loop
  Access:PARTS.RestoreFile(Save_par_ID)
  
  Save_epr_ID = Access:ESTPARTS.SaveFile()
  Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
  epr:Ref_Number  = job:Ref_Number
  Set(epr:Part_Number_Key,epr:Part_Number_Key)
  Loop
      If Access:ESTPARTS.NEXT()
         Break
      End !If
      If epr:Ref_Number  <> job:Ref_Number      |
          Then Break.  ! End If
      If epr:Fault_Code1 <> ''
          Local.WarrantyCode(1,epr:fault_Code1,'E')
      End !If wpr:Fault_Code1
      If epr:Fault_Code2 <> ''
          Local.WarrantyCode(2,epr:fault_Code2,'E')
      End !If wpr:Fault_Code1
      If epr:Fault_Code3 <> ''
          Local.WarrantyCode(3,epr:fault_Code3,'E')
      End !If wpr:Fault_Code1
      If epr:Fault_Code4 <> ''
          Local.WarrantyCode(4,epr:fault_Code4,'E')
      End !If wpr:Fault_Code1
      If epr:Fault_Code5 <> ''
          Local.WarrantyCode(5,epr:fault_Code5,'E')
      End !If wpr:Fault_Code1
      If epr:Fault_Code6 <> ''
          Local.WarrantyCode(6,epr:fault_Code6,'E')
      End !If wpr:Fault_Code1
      If epr:Fault_Code7 <> ''
          Local.WarrantyCode(7,epr:fault_Code7,'E')
      End !If wpr:Fault_Code1
      If epr:Fault_Code8 <> ''
          Local.WarrantyCode(8,epr:fault_Code8,'E')
      End !If wpr:Fault_Code1
      If epr:Fault_Code9 <> ''
          Local.WarrantyCode(9,epr:fault_Code9,'E')
      End !If wpr:Fault_Code1
      If epr:Fault_Code10 <> ''
          Local.WarrantyCode(10,epr:fault_Code10,'E')
      End !If wpr:Fault_Code1
      If epr:Fault_Code11 <> ''
          Local.WarrantyCode(11,epr:fault_Code11,'E')
      End !If wpr:Fault_Code1
      If epr:Fault_Code12 <> ''
          Local.WarrantyCode(12,epr:fault_Code12,'E')
      End !If wpr:Fault_Code1
  
  End !Loop
  Access:ESTPARTS.RestoreFile(Save_epr_ID)
  !View Only?
  If glo:Preview = 'V'
      ?AddFault{prop:Hide} = 1
      !TB13265 - J - 07/04/14 must also stop the double clicking on the list to select.
      ?FreeText{prop:Hide} = 1
      ?Delete{prop:Hide} = 1
  End !glo:Select24 = 'RRCVIEWONLY'
  ! Save Window Name
   AddToLog('Window','Open','OutFaultList')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List6{prop:vcr} = TRUE
  ?List7{prop:vcr} = TRUE
  ?List5{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,mfo:HideFieldKey)
  BRW5.AddRange(mfo:Field_Number)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,mfo:Field,1,BRW5)
  BRW5.AddField(mfo:Field,BRW5.Q.mfo:Field)
  BRW5.AddField(mfo:Description,BRW5.Q.mfo:Description)
  BRW5.AddField(mfo:ImportanceLevel,BRW5.Q.mfo:ImportanceLevel)
  BRW5.AddField(mfo:SkillLevel,BRW5.Q.mfo:SkillLevel)
  BRW5.AddField(mfo:RecordNumber,BRW5.Q.mfo:RecordNumber)
  BRW5.AddField(mfo:NotAvailable,BRW5.Q.mfo:NotAvailable)
  BRW5.AddField(mfo:Manufacturer,BRW5.Q.mfo:Manufacturer)
  BRW5.AddField(mfo:Field_Number,BRW5.Q.mfo:Field_Number)
  BRW6.Q &= Queue:Browse:1
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,joo:JobNumberKey)
  BRW6.AddRange(joo:JobNumber,tmp:JobNumber)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,joo:FaultCode,1,BRW6)
  BIND('tmp:MRepair',tmp:MRepair)
  BRW6.AddField(joo:FaultCode,BRW6.Q.joo:FaultCode)
  BRW6.AddField(joo:Description,BRW6.Q.joo:Description)
  BRW6.AddField(joo:Level,BRW6.Q.joo:Level)
  BRW6.AddField(tmp:MRepair,BRW6.Q.tmp:MRepair)
  BRW6.AddField(joo:RecordNumber,BRW6.Q.joo:RecordNumber)
  BRW6.AddField(joo:JobNumber,BRW6.Q.joo:JobNumber)
  BRW6.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  BRW6.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBOUTFL.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','OutFaultList')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  ! Do not allow to delete the imei validation entries - TrkBs: 6690 (DBH: 11-11-2005)
  If Request = DeleteRecord
      If (SecurityCheck('SYSTEM OUT FAULTS - DELETE'))  ! #11609 Add access level to delete system entries (add exchange entry) (DBH: 12/08/2010)
          If (Instring('IMEI VALIDATION:',joo:Description,1,1) or Instring('EXCHANGED FOR',joo:Description,1,1))
              Case Missive('You cannot delete this automatically generated entry.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Return RequestCancelled
          End !If Instring('IMEI VALIDATION:',joo:Description,1,1)
      end ! If (SecurityCheck('SYSTEM OUT FAULTS - DELETE'))
  End !If Request = DeleteRecord
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateOutFaults
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020390'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020390'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020390'&'0')
      ***
    OF ?AddFault
      ThisWindow.Update
      If Access:JOBOUTFL.PrimeRecord() = Level:Benign
          joo:JobNumber   = tmp:JobNumber
          joo:FaultCode   = brw5.q.mfo:Field
          joo:Description = brw5.q.mfo:Description
          joo:Level       = brw5.q.mfo:ImportanceLevel
          If Access:JOBOUTFL.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:JOBOUTFL.TryInsert() = Level:Benign
              !Insert Failed
              Access:JOBOUTFL.CancelAutoInc()
          End !If Access:JOBOUTFL.TryInsert() = Level:Benign
      End !If Access:JOBOUTFL.PrimeRecord() = Level:Benign
      BRW6.ResetSort(1)
    OF ?FreeText
      ThisWindow.Update
      FreeTextOutFault
      ThisWindow.Reset
      BRW6.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      Case Keycode()
          Of MouseLeft2
            !TB13265 - J - 07/04/14 - do not allow double clicking if the button is hidden
            if ?AddFault{prop:hide}= false
              Post(Event:Accepted,?AddFault)
            END
      End !Keycode()
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.WarrantyCode      Procedure(Long func:FieldNumber,String func:Field,String func:Type)
Code
    !Is this fault code a "Main Fault"
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = job:Manufacturer
    map:Field_Number = func:FieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Found
        If map:MainFault

            !Ok, get the details from the Job "Main Fault"
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = job:Manufacturer
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                Save_mfo_ID = Access:MANFAULO.SaveFile()
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = job:Manufacturer
                mfo:Field_Number = maf:Field_Number
                mfo:Field        = func:Field
                Set(mfo:Field_Key,mfo:Field_Key)
                Loop
                    If Access:MANFAULO.NEXT()
                       Break
                    End !If
                    If mfo:Manufacturer <> job:Manufacturer      |
                    Or mfo:Field_Number <> maf:Field_Number      |
                    Or mfo:Field        <> func:Field      |
                        Then Break.  ! End If
                    If mfo:RelatedPartCode <> 0
                        If mfo:RelatedPartCode <> func:FieldNumber
                            Cycle
                        End !If mfo:RelatedPartCode <> func:FieldNumber
                    !This 'END' was at the bottom which would
                    !mean only Ericsson faults would ever appear - 234694 (DBH: 25-07-2003)
                    End !If mfo:RelatedPartCode <> 0

                        !Add to temp queue
                        !Had this code already been added?

                        Case func:Type
                            Of 'W' !Warranty
                                !Check the description because the same fault code,
                                !could appear - 234694 (DBH: 25-07-2003)
                                Sort(WarPartsQueue,warque:FaultCode)
                                warque:FaultCode    = func:Field
                                warque:Description  = mfo:Description
                                Get(WarPArtsQueue,'warqueFaultCode,warqueDescription')
                                If Error()
                                    warque:FaultCode    = func:Field
                                    warque:Description  = mfo:Description
                                    warque:Level        = mfo:ImportanceLevel
                                    Add(WarPartsQueue)
                                End !If Error()
                            Of 'C' !Chargeable
                                Sort(ChaPartsQueue,chaque:FaultCode)
                                chaque:FaultCode    = func:Field
                                chaque:Description  = mfo:Description
                                Get(ChaPArtsQueue,'chaqueFaultCode,chaqueDescription')
                                If Error()
                                    chaque:FaultCode    = func:Field
                                    chaque:Description  = mfo:Description
                                    chaque:Level        = mfo:ImportanceLevel
                                    Add(ChaPartsQueue)
                                End !If Error()
                            Of 'E' !Estimate
                                Sort(EstPartsQueue,estque:FaultCode)
                                estque:FaultCode    = func:Field
                                estque:Description  = mfo:Description
                                Get(EstPArtsQueue,'estqueFaultCode,estqueDescription')
                                If Error()
                                    estque:FaultCode    = func:Field
                                    estque:Description  = mfo:Description
                                    estque:Level        = mfo:ImportanceLevel
                                    Add(EstPartsQueue)
                                End !If Error()
                        End !Case func:Type
                        Break
                End !Loop
                Access:MANFAULO.RestoreFile(Save_mfo_ID)
            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Else !If map:MainFault

        End !If map:MainFault
    Else!If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End            !If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = 0
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = tmp:Manufacturer
  GET(SELF.Order.RangeList.List,3)
  Self.Order.RangeList.List.Right = tmp:FieldNumber
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  If mfo:SkillLevel > tmp:SkillLevel
      Return Record:Filtered
  End !mfo:SkillLevel > tmp:SkillLevel
  
  ! Key now contains "Not Available"instead of "Hide From Engineer" - TrkBs: 5904 (DBH: 22-09-2005)
  If mfo:HideFromEngineer
      Return Record:Filtered
  End ! If mfo:HideFromEngineer
  
  If Records(LinkedFaultCodeQueue)
      Found# = 0
      Loop x# = 1 To Records(LinkedFaultCodeQueue)
          Get(LinkedFaultCodeQueue,x#)
          Access:MANFAURL.Clearkey(mnr:LinkedRecordNumberKey)
          mnr:MANFAULORecordNumber = LinkedFaultCodeQueue.RecordNumber
          mnr:LinkedRecordNumber = mfo:RecordNumber
          If Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign
              Found# = 1
              Break
          Else ! If Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign
          End ! If Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign
      End ! Loop x# = 1 To Records(LinkedFaultCodeQueue)
      If Found# = 0
          Return Record:Filtered
      End ! If Found# = 0
  End ! If Records(LinkedFaultCodeQueue)
  BRW5::RecordStatus=ReturnValue
  RETURN ReturnValue


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW6.SetQueueRecord PROCEDURE

  CODE
              Access:ManFault.ClearKey(maf:MainFaultKey)
              maf:manufacturer = tmp:manufacturer
              maf:mainFault = TRUE
              IF Access:ManFault.Fetch(maf:MainFaultKey)
                !Error!
              END
              Access:ManFaulo.ClearKey(mfo:Field_Key)
              mfo:Manufacturer = tmp:manufacturer
              mfo:Field_Number = maf:Field_Number
              mfo:Field =joo:FaultCode
              IF Access:ManFaulo.Fetch(mfo:Field_Key)
                !Error!
                tmp:MRepair = 0
              ELSE
                tmp:MRepair = mfo:ImportanceLevel
              END
  PARENT.SetQueueRecord
  SELF.Q.joo:Description_Tip = joo:Description


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

UpdateOutFaults PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::joo:Record  LIKE(joo:RECORD),STATIC
QuickWindow          WINDOW('Update the JOBOUTFL File'),AT(,,316,112),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateOutFaults'),SYSTEM,GRAY,NOFRAME
                       SHEET,AT(4,4,308,86),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?joo:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,20,40,10),USE(joo:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Job Number'),AT(8,34),USE(?joo:JobNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,34,40,10),USE(joo:JobNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Number'),TIP('Job Number'),UPR
                           PROMPT('Fault Code'),AT(8,48),USE(?joo:FaultCode:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(64,48,124,10),USE(joo:FaultCode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Description'),AT(8,62),USE(?joo:Description:Prompt),COLOR(COLOR:Silver)
                           ENTRY(@s60),AT(64,62,244,10),USE(joo:Description),COLOR(COLOR:White),UPR
                           PROMPT('Level'),AT(8,76),USE(?joo:Level:Prompt),TRN
                           SPIN(@n8),AT(64,76,51,10),USE(joo:Level),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Level'),TIP('Level'),UPR,STEP(1)
                         END
                       END
                       BUTTON('OK'),AT(218,94,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(267,94,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(267,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateOutFaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?joo:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(joo:Record,History::joo:Record)
  SELF.AddHistoryField(?joo:RecordNumber,1)
  SELF.AddHistoryField(?joo:JobNumber,2)
  SELF.AddHistoryField(?joo:FaultCode,3)
  SELF.AddHistoryField(?joo:Description,4)
  SELF.AddHistoryField(?joo:Level,5)
  SELF.AddUpdateFile(Access:JOBOUTFL)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBOUTFL.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBOUTFL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateOutFaults')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBOUTFL.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateOutFaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Estimate_Reason PROCEDURE                             !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Estimate Process'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Estimate Process'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Estimate Accepted'),USE(?Estimate_Accepted),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Estimate Accepted From'),AT(252,184),USE(?glo:select2:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s40),AT(252,196,176,10),USE(GLO:Select2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Communication Method'),AT(252,220),USE(?glo:select3:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s40),AT(252,230,176,10),USE(GLO:Select3),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('Estimate Rejected'),USE(?Estimate_Rejected),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s40),AT(252,188,176,10),USE(GLO:Select2,,?glo:select2:2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Communication Method'),AT(252,204),USE(?Prompt4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Estimate Rejected By'),AT(252,178),USE(?Prompt3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s40),AT(252,214,176,10),USE(GLO:Select3,,?glo:select3:2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Rejection Reason'),AT(252,228),USE(?glo:select4:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s40),AT(252,238,148,10),USE(GLO:Select4),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(EnterKey),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),REQ,UPR
                           BUTTON,AT(404,234),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(368,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),STD(STD:Close),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:GLO:Select4                Like(GLO:Select4)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020413'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Estimate_Reason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ESREJRES.Open
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Case glo:select1
      Of 'ACCEPTED'
          Unhide(?estimate_accepted)
      Of 'REJECTED'
          Unhide(?estimate_rejected)
  End
  Display()
  ! Save Window Name
   AddToLog('Window','Open','Estimate_Reason')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?GLO:Select4{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?GLO:Select4{Prop:Tip}
  END
  IF ?GLO:Select4{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?GLO:Select4{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ESREJRES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Estimate_Reason')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseEstRejectionReasons
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020413'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020413'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020413'&'0')
      ***
    OF ?GLO:Select4
      IF GLO:Select4 OR ?GLO:Select4{Prop:Req}
        esr:RejectionReason = GLO:Select4
        !Save Lookup Field Incase Of error
        look:GLO:Select4        = GLO:Select4
        IF Access:ESREJRES.TryFetch(esr:RejectionReasonKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            GLO:Select4 = esr:RejectionReason
          ELSE
            !Restore Lookup On Error
            GLO:Select4 = look:GLO:Select4
            SELECT(?GLO:Select4)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      esr:RejectionReason = GLO:Select4
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          GLO:Select4 = esr:RejectionReason
          Select(?+1)
      ELSE
          Select(?GLO:Select4)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?GLO:Select4)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
