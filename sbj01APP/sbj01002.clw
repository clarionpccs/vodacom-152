

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01002.INC'),ONCE        !Local module procedure declarations
                     END


Serial_Number_Validation PROCEDURE (f_serial_number)  !Generated from procedure template - Window

FilesOpened          BYTE
serial_number_temp   STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Serial Number Validation'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Serial Number Validation'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Serial Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(248,204,184,10),USE(serial_number_temp),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020419'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Serial_Number_Validation')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Access:JOBS.UseFile
  Access:LOAN.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Serial_Number_Validation')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Serial_Number_Validation')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020419'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020419'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020419'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      If serial_number_temp = ''
          Select(?Serial_number_temp)
      Else
          f_serial_number = serial_number_temp
          post(Event:closewindow)
      End
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Validate_Loan_Accessories PROCEDURE                   !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::4:TAGFLAG          BYTE(0)
DASBRW::4:TAGMOUSE         BYTE(0)
DASBRW::4:TAGDISPSTATUS    BYTE(0)
DASBRW::4:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
save_lac_id          USHORT,AUTO
FilesOpened          BYTE
tag_temp             STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(LOANACC)
                       PROJECT(lac:Accessory)
                       PROJECT(lac:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
lac:Accessory          LIKE(lac:Accessory)            !List box control field - type derived from field
lac:Ref_Number         LIKE(lac:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Validate Accessories On Loan Unit'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Validate Accessories On Loan Unit'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(264,112,148,186),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)~Accessory~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Rev tags'),AT(600,302,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(600,326,70,13),USE(?DASSHOWTAG),HIDE
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Accessory'),USE(?Tab:2)
                           ENTRY(@s30),AT(264,98,124,10),USE(lac:Accessory),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,114),USE(?Validate_Accessories),TRN,FLAT,LEFT,ICON('valaccp.jpg')
                           BUTTON,AT(240,300),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(308,300),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(376,300),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::4:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:Queue.Pointer = lac:Accessory
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = lac:Accessory
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = lac:Accessory
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::4:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::4:QUEUE = GLO:Queue
    ADD(DASBRW::4:QUEUE)
  END
  FREE(GLO:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::4:QUEUE.Pointer = lac:Accessory
     GET(DASBRW::4:QUEUE,DASBRW::4:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = lac:Accessory
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASSHOWTAG Routine
   CASE DASBRW::4:TAGDISPSTATUS
   OF 0
      DASBRW::4:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::4:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::4:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020404'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Validate_Loan_Accessories')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOANACC.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOANACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Validate_Loan_Accessories')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,lac:Ref_Number_Key)
  BRW1.AddRange(lac:Ref_Number,)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?lac:Accessory,lac:Accessory,1,BRW1)
  BIND('tag_temp',tag_temp)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tag_temp,BRW1.Q.tag_temp)
  BRW1.AddField(lac:Accessory,BRW1.Q.lac:Accessory)
  BRW1.AddField(lac:Ref_Number,BRW1.Q.lac:Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOANACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Validate_Loan_Accessories')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020404'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020404'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020404'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Validate_Accessories
      ThisWindow.Update
      error# = 0
      setcursor(cursor:wait)
      save_lac_id = access:loanacc.savefile()
      access:loanacc.clearkey(lac:ref_number_key)
      lac:ref_number = glo:select1
      set(lac:ref_number_key,lac:ref_number_key)
      loop
          if access:loanacc.next()
             break
          end !if
          if lac:ref_number <> glo:select1      |
              then break.  ! end if
          Sort(glo:Queue,glo:pointer)
          glo:pointer  = xca:accessory
          Get(glo:Queue,glo:pointer)
          If Error()
              error# = 1
              Break
          End!If Error()
      end !loop
      access:loanacc.restorefile(save_lac_id)
      setcursor()
      
      
      If error# = 1
          glo:select2 = 'FAIL'
      Else!If error# = 1
          glo:select2 = ''
      End!If error# = 1
      Post(Event:CloseWindow)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::4:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?lac:Accessory
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Do DASBRW::4:DASUNTAGALL
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = lac:Accessory
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = lac:Accessory
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::4:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowseConsignmentHistory PROCEDURE (func:JobNumber)   !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:JobNumber        LONG
BRW1::View:Browse    VIEW(JOBSCONS)
                       PROJECT(joc:TheDate)
                       PROJECT(joc:TheTime)
                       PROJECT(joc:UserCode)
                       PROJECT(joc:DespatchFrom)
                       PROJECT(joc:DespatchTo)
                       PROJECT(joc:Courier)
                       PROJECT(joc:ConsignmentNumber)
                       PROJECT(joc:DespatchType)
                       PROJECT(joc:RecordNumber)
                       PROJECT(joc:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
joc:TheDate            LIKE(joc:TheDate)              !List box control field - type derived from field
joc:TheTime            LIKE(joc:TheTime)              !List box control field - type derived from field
joc:UserCode           LIKE(joc:UserCode)             !List box control field - type derived from field
joc:DespatchFrom       LIKE(joc:DespatchFrom)         !List box control field - type derived from field
joc:DespatchTo         LIKE(joc:DespatchTo)           !List box control field - type derived from field
joc:Courier            LIKE(joc:Courier)              !List box control field - type derived from field
joc:ConsignmentNumber  LIKE(joc:ConsignmentNumber)    !List box control field - type derived from field
joc:DespatchType       LIKE(joc:DespatchType)         !List box control field - type derived from field
joc:RecordNumber       LIKE(joc:RecordNumber)         !Primary key field - type derived from field
joc:RefNumber          LIKE(joc:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Consignment History'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Consignment History File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(96,74,492,282),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('42R(2)|M~Date~@d6@23R(2)|M~Time~@t1b@25L(2)|M~User~@s3@80L(2)|M~Despatch From~@s' &|
   '30@80L(2)|M~Despatch To~@s30@80L(2)|M~Courier~@s30@80L(2)|M~Consignment Number~@' &|
   's30@12L(2)|M~Despatch Type~@s3@'),FROM(Queue:Browse:1)
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),SPREAD
                         TAB('By Date'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020388'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseConsignmentHistory')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBSCONS.Open
  SELF.FilesOpened = True
  tmp:JobNumber   = func:JobNumber
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBSCONS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseConsignmentHistory')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,joc:DateKey)
  BRW1.AddRange(joc:RefNumber,tmp:JobNumber)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,joc:TheDate,1,BRW1)
  BRW1.AddField(joc:TheDate,BRW1.Q.joc:TheDate)
  BRW1.AddField(joc:TheTime,BRW1.Q.joc:TheTime)
  BRW1.AddField(joc:UserCode,BRW1.Q.joc:UserCode)
  BRW1.AddField(joc:DespatchFrom,BRW1.Q.joc:DespatchFrom)
  BRW1.AddField(joc:DespatchTo,BRW1.Q.joc:DespatchTo)
  BRW1.AddField(joc:Courier,BRW1.Q.joc:Courier)
  BRW1.AddField(joc:ConsignmentNumber,BRW1.Q.joc:ConsignmentNumber)
  BRW1.AddField(joc:DespatchType,BRW1.Q.joc:DespatchType)
  BRW1.AddField(joc:RecordNumber,BRW1.Q.joc:RecordNumber)
  BRW1.AddField(joc:RefNumber,BRW1.Q.joc:RefNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBSCONS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseConsignmentHistory')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020388'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020388'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020388'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults




BrowseStatusChanges PROCEDURE (func:JobNumber)        !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:UserName         STRING(40)
tmp:StatusType       STRING('JOB')
tmp:JobNumber        LONG
BRW1::View:Browse    VIEW(AUDSTATS)
                       PROJECT(aus:DateChanged)
                       PROJECT(aus:TimeChanged)
                       PROJECT(aus:OldStatus)
                       PROJECT(aus:NewStatus)
                       PROJECT(aus:RecordNumber)
                       PROJECT(aus:RefNumber)
                       PROJECT(aus:Type)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
aus:DateChanged        LIKE(aus:DateChanged)          !List box control field - type derived from field
aus:TimeChanged        LIKE(aus:TimeChanged)          !List box control field - type derived from field
tmp:UserName           LIKE(tmp:UserName)             !List box control field - type derived from local data
aus:OldStatus          LIKE(aus:OldStatus)            !List box control field - type derived from field
aus:NewStatus          LIKE(aus:NewStatus)            !List box control field - type derived from field
aus:RecordNumber       LIKE(aus:RecordNumber)         !Primary key field - type derived from field
aus:RefNumber          LIKE(aus:RefNumber)            !Browse key field - type derived from field
aus:Type               LIKE(aus:Type)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK7::aus:RefNumber        LIKE(aus:RefNumber)
HK7::aus:Type             LIKE(aus:Type)
! ---------------------------------------- Higher Keys --------------------------------------- !
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse Status Changes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Status Change File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(88,86,508,270),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('43R(2)|M~Date~@d6@22R(2)|M~Time~@t1@144L(2)|M~User Name~@s40@126L(2)|M~Old Statu' &|
   's~@s30@80L(2)|M~New Status~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('By Date Changed'),USE(?Tab:2)
                           PROMPT('By Date'),AT(68,58),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Status Type'),AT(352,58,224,24),USE(tmp:StatusType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Job'),AT(360,70),USE(?tmp:StatusType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('JOB')
                             RADIO('Exchange'),AT(404,70),USE(?tmp:StatusType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('EXC')
                             RADIO('2nd Exchange'),AT(464,70),USE(?tmp:StatusType:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2NE')
                             RADIO('Loan'),AT(536,70),USE(?tmp:StatusType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('LOA')
                           END
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
OrderInit              PROCEDURE(<Key K>)            !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepCustomClass !DATE            !Xplore: Column displaying aus:DateChanged
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying aus:DateChanged
Xplore1Step2         StepCustomClass !TIME            !Xplore: Column displaying aus:TimeChanged
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying aus:TimeChanged
Xplore1Step3         StepCustomClass !                !Xplore: Column displaying tmp:UserName
Xplore1Locator3      IncrementalLocatorClass          !Xplore: Column displaying tmp:UserName
Xplore1Step4         StepStringClass !STRING          !Xplore: Column displaying aus:OldStatus
Xplore1Locator4      IncrementalLocatorClass          !Xplore: Column displaying aus:OldStatus
Xplore1Step5         StepStringClass !STRING          !Xplore: Column displaying aus:NewStatus
Xplore1Locator5      IncrementalLocatorClass          !Xplore: Column displaying aus:NewStatus
Xplore1Step6         StepLongClass   !LONG            !Xplore: Column displaying aus:RecordNumber
Xplore1Locator6      IncrementalLocatorClass          !Xplore: Column displaying aus:RecordNumber
Xplore1Step7         StepLongClass   !LONG            !Xplore: Column displaying aus:RefNumber
Xplore1Locator7      IncrementalLocatorClass          !Xplore: Column displaying aus:RefNumber
Xplore1Step8         StepStringClass !STRING          !Xplore: Column displaying aus:Type
Xplore1Locator8      IncrementalLocatorClass          !Xplore: Column displaying aus:Type

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('BrowseStatusChanges','?Browse:1') !Xplore
  BRW1.SequenceNbr = 1                                !Xplore
  BRW1.ViewOrder   = True                             !Xplore
  Xplore1.RestoreHeader = True                        !Xplore
  Xplore1.AscDesc       = 0                           !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020401'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseStatusChanges')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDSTATS.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  tmp:JobNumber   = func:JobNumber
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:AUDSTATS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseStatusChanges')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbj01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,aus:StatusTypeRecordKey)
  BRW1.AddRange(aus:Type)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,aus:DateChanged,1,BRW1)
  BIND('tmp:UserName',tmp:UserName)
  BRW1.AddField(aus:DateChanged,BRW1.Q.aus:DateChanged)
  BRW1.AddField(aus:TimeChanged,BRW1.Q.aus:TimeChanged)
  BRW1.AddField(tmp:UserName,BRW1.Q.tmp:UserName)
  BRW1.AddField(aus:OldStatus,BRW1.Q.aus:OldStatus)
  BRW1.AddField(aus:NewStatus,BRW1.Q.aus:NewStatus)
  BRW1.AddField(aus:RecordNumber,BRW1.Q.aus:RecordNumber)
  BRW1.AddField(aus:RefNumber,BRW1.Q.aus:RefNumber)
  BRW1.AddField(aus:Type,BRW1.Q.aus:Type)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDSTATS.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('BrowseStatusChanges','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  ! Save Window Name
   AddToLog('Window','Close','BrowseStatusChanges')
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(aus:DateChanged,BRW1.Q.aus:DateChanged)
  Xplore1.AddField(aus:TimeChanged,BRW1.Q.aus:TimeChanged)
  Xplore1.AddField(tmp:UserName,BRW1.Q.tmp:UserName)
  Xplore1.AddField(aus:OldStatus,BRW1.Q.aus:OldStatus)
  Xplore1.AddField(aus:NewStatus,BRW1.Q.aus:NewStatus)
  Xplore1.AddField(aus:RecordNumber,BRW1.Q.aus:RecordNumber)
  Xplore1.AddField(aus:RefNumber,BRW1.Q.aus:RefNumber)
  Xplore1.AddField(aus:Type,BRW1.Q.aus:Type)
  BRW1.FileOrderNbr = BRW1.AddSortOrder(,aus:StatusTypeRecordKey) !Xplore Sort Order for File Sequence
  BRW1.AddRange(aus:Type)                             !Xplore
  BRW1.SetOrder('')                                   !Xplore
  BRW1.ViewOrder = True                               !Xplore
  Xplore1.AddAllColumnSortOrders(1)                   !Xplore
  BRW1.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:StatusType
      !Extension Templete Code:XploreOOPBrowse,ControlEventHandling,?tmp:StatusType,'NewSelection',PRIORITY(4000)
      BRW1.ViewOrder = False                          !Xplore
      BRW1.FileSeqOn = False                          !Xplore
      Xplore1.EraseVisual()                           !Xplore
      Xplore1.SetFilters()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020401'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020401'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020401'&'0')
      ***
    OF ?tmp:StatusType
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
    OF EVENT:GainFocus
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = tmp:JobNumber
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = tmp:StatusType
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !Display an error if unable to find user information - 4314 (DBH: 27-05-2004)
  access:users.clearkey(use:user_code_key)
  use:user_code   = aus:UserCode
  If access:users.tryfetch(use:user_code_key) = Level:Benign
      !Found
      tmp:UserName    = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else! If access:users.tryfetch(use:user_code_key) = Level:Benign
      !Error
      tmp:UserName    = '* ERROR - USER CODE: ' & Clip(aus:UserCode) & ' *'
  End! If access:users.tryfetch(use:user_code_key) = Level:Benign
  
  PARENT.SetQueueRecord


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.OrderInit PROCEDURE(<KEY K>)
!This method is located here (in BRW1) because SELF.ORDER queue is protected in the ViewManager
  CODE
  !0{PROP:Text} = 'SortOrder=' & POINTER(SELF.ORDER) & ' order=' & SELF.ORDER.Order & ' fe=' & SELF.ORDER.FreeElement
  CLEAR(SELF.ORDER.LimitType)                         !Xplore
  FREE(SELF.ORDER.Filter)                             !Xplore
  DISPOSE(SELF.ORDER.Filter)                          !Xplore
  SELF.ORDER.Filter &= NEW FilterQueue                !Xplore
  DISPOSE(SELF.ORDER.RangeList)                       !Xplore
  SELF.Order.RangeList &= NEW BufferedPairsClass      !Xplore
  SELF.ORDER.MainKey &= K                             !Xplore
  SELF.Order.RangeList.Init                           !Xplore
  DISPOSE(SELF.Order.Order)                           !Xplore
  PUT(SELF.Order)                                     !Xplore
  RETURN                                              !Xplore
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !aus:DateChanged
        ColumnString     = SUB(LEFT(BRW1.Q.aus:DateChanged),1,20)
  OF 2 !aus:TimeChanged
  OF 3 !tmp:UserName
  OF 4 !aus:OldStatus
  OF 5 !aus:NewStatus
  OF 6 !aus:RecordNumber
  OF 7 !aus:RefNumber
  OF 8 !aus:Type
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step8,aus:StatusTypeRecordKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,aus:StatusTypeRecordKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(,aus:DateChanged,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,aus:TimeChanged,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,tmp:UserName,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(,aus:OldStatus,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(,aus:NewStatus,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(,aus:RecordNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(,aus:RefNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(,aus:Type,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(aus:Type)
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
I     LONG
J     LONG
SaveQ LONG
  CODE
  SaveQ = POINTER(SELF.Fq)
  LOOP I = 1 to RECORDS(SELF.Fq)
    GET(SELF.Fq,I)
    IF NOT SELF.FQ.Sortfield
      CYCLE
    END
    LOOP J = 1 TO 2
      IF SELF.BC.SetSort(SELF.Fq.SortField + J - 1).
    END !LOOP J = 1 TO 2
  END !LOOP
  GET(SELF.Fq,SaveQ)
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(AUDSTATS)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('aus:DateChanged')            !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Date')                       !Header
                PSTRING('@d6')                        !Picture
                PSTRING('Date Changed')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('aus:TimeChanged')            !Field Name
                SHORT(20)                             !Default Column Width
                PSTRING('Time')                       !Header
                PSTRING('@t1')                        !Picture
                PSTRING('Time Changed')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tmp:UserName')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:UserName')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:UserName')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('aus:OldStatus')              !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Old Status')                 !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Old Status')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('aus:NewStatus')              !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('New Status')                 !Header
                PSTRING('@s30')                       !Picture
                PSTRING('New Status')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('aus:RecordNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Record Number')              !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('aus:RefNumber')              !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job Number')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Job Number')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('aus:Type')                   !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Type')                       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Type')                       !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(8)
!--------------------------
XpSortFields    GROUP,STATIC
                PSTRING('aus:DateChanged')            !Column Field
                SHORT(2)                              !Nos of Sort Fields
                PSTRING('aus:DateChanged')            !Sort Field Seq:1
                BYTE(1)                               !Descending
                PSTRING('@d6')                        !Picture
                PSTRING('aus:RecordNumber')           !Sort Field Seq:2
                BYTE(1)                               !Descending
                PSTRING('@s8')                        !Picture
                !-------------------------
                END
XpSortDim       SHORT(1)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults




BrowseLocationChanges PROCEDURE (func:JobNumber)      !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:UserName         STRING(40)
tmp:StatusType       STRING('JOB')
tmp:JobNumber        LONG
ExchangeType         STRING(1)
BRW1::View:Browse    VIEW(LOCATLOG)
                       PROJECT(lot:TheDate)
                       PROJECT(lot:TheTime)
                       PROJECT(lot:PreviousLocation)
                       PROJECT(lot:NewLocation)
                       PROJECT(lot:Exchange)
                       PROJECT(lot:RecordNumber)
                       PROJECT(lot:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
lot:TheDate            LIKE(lot:TheDate)              !List box control field - type derived from field
lot:TheTime            LIKE(lot:TheTime)              !List box control field - type derived from field
tmp:UserName           LIKE(tmp:UserName)             !List box control field - type derived from local data
lot:PreviousLocation   LIKE(lot:PreviousLocation)     !List box control field - type derived from field
lot:NewLocation        LIKE(lot:NewLocation)          !List box control field - type derived from field
lot:Exchange           LIKE(lot:Exchange)             !List box control field - type derived from field
lot:RecordNumber       LIKE(lot:RecordNumber)         !Primary key field - type derived from field
lot:RefNumber          LIKE(lot:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK7::lot:RefNumber        LIKE(lot:RefNumber)
! ---------------------------------------- Higher Keys --------------------------------------- !
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse Location Changes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Location Changes File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(88,106,508,238),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('49L(2)|M~Date~@d6@27L(2)|M~Time~@t1b@144L(2)|M~User Name~@s40@124L(2)|M~Previous' &|
   ' Location~@s30@120L(2)|M~New Location~@s30@4L(2)|M~Exch~@s1@'),FROM(Queue:Browse:1)
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('By Date Changed'),USE(?Tab:2)
                           PROMPT('By Date'),AT(68,58),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION,AT(444,72,160,30),USE(ExchangeType)
                             RADIO('Job'),AT(516,87),USE(?ExchangeType:Radio1),VALUE('N')
                             RADIO('Exchange'),AT(544,87),USE(?ExchangeType:Radio2),VALUE('Y')
                           END
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
OrderInit              PROCEDURE(<Key K>)            !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepCustomClass !DATE            !Xplore: Column displaying lot:TheDate
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying lot:TheDate
Xplore1Step2         StepCustomClass !TIME            !Xplore: Column displaying lot:TheTime
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying lot:TheTime
Xplore1Step3         StepCustomClass !                !Xplore: Column displaying tmp:UserName
Xplore1Locator3      IncrementalLocatorClass          !Xplore: Column displaying tmp:UserName
Xplore1Step4         StepStringClass !STRING          !Xplore: Column displaying lot:PreviousLocation
Xplore1Locator4      IncrementalLocatorClass          !Xplore: Column displaying lot:PreviousLocation
Xplore1Step5         StepStringClass !STRING          !Xplore: Column displaying lot:NewLocation
Xplore1Locator5      IncrementalLocatorClass          !Xplore: Column displaying lot:NewLocation
Xplore1Step6         StepStringClass !STRING          !Xplore: Column displaying lot:Exchange
Xplore1Locator6      IncrementalLocatorClass          !Xplore: Column displaying lot:Exchange
Xplore1Step7         StepLongClass   !LONG            !Xplore: Column displaying lot:RecordNumber
Xplore1Locator7      IncrementalLocatorClass          !Xplore: Column displaying lot:RecordNumber
Xplore1Step8         StepLongClass   !LONG            !Xplore: Column displaying lot:RefNumber
Xplore1Locator8      IncrementalLocatorClass          !Xplore: Column displaying lot:RefNumber

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('BrowseLocationChanges','?Browse:1') !Xplore
  BRW1.SequenceNbr = 1                                !Xplore
  BRW1.ViewOrder   = True                             !Xplore
  Xplore1.RestoreHeader = True                        !Xplore
  Xplore1.AscDesc       = 0                           !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020400'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseLocationChanges')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATLOG.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  tmp:JobNumber   = func:JobNumber
  ExchangeType = 'N'
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOCATLOG,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseLocationChanges')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbj01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,lot:DateKey)
  BRW1.AddRange(lot:RefNumber)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,lot:TheDate,1,BRW1)
  BIND('tmp:UserName',tmp:UserName)
  BRW1.AddField(lot:TheDate,BRW1.Q.lot:TheDate)
  BRW1.AddField(lot:TheTime,BRW1.Q.lot:TheTime)
  BRW1.AddField(tmp:UserName,BRW1.Q.tmp:UserName)
  BRW1.AddField(lot:PreviousLocation,BRW1.Q.lot:PreviousLocation)
  BRW1.AddField(lot:NewLocation,BRW1.Q.lot:NewLocation)
  BRW1.AddField(lot:Exchange,BRW1.Q.lot:Exchange)
  BRW1.AddField(lot:RecordNumber,BRW1.Q.lot:RecordNumber)
  BRW1.AddField(lot:RefNumber,BRW1.Q.lot:RefNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATLOG.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('BrowseLocationChanges','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  ! Save Window Name
   AddToLog('Window','Close','BrowseLocationChanges')
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(lot:TheDate,BRW1.Q.lot:TheDate)
  Xplore1.AddField(lot:TheTime,BRW1.Q.lot:TheTime)
  Xplore1.AddField(tmp:UserName,BRW1.Q.tmp:UserName)
  Xplore1.AddField(lot:PreviousLocation,BRW1.Q.lot:PreviousLocation)
  Xplore1.AddField(lot:NewLocation,BRW1.Q.lot:NewLocation)
  Xplore1.AddField(lot:Exchange,BRW1.Q.lot:Exchange)
  Xplore1.AddField(lot:RecordNumber,BRW1.Q.lot:RecordNumber)
  Xplore1.AddField(lot:RefNumber,BRW1.Q.lot:RefNumber)
  BRW1.FileOrderNbr = BRW1.AddSortOrder(,lot:DateKey) !Xplore Sort Order for File Sequence
  BRW1.AddRange(lot:RefNumber)                        !Xplore
  BRW1.SetOrder('')                                   !Xplore
  BRW1.ViewOrder = True                               !Xplore
  Xplore1.AddAllColumnSortOrders(1)                   !Xplore
  BRW1.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ExchangeType
      BRW1.resetsort(1)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020400'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020400'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020400'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
    OF EVENT:GainFocus
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = tmp:JobNumber
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  Access:USERS.Clearkey(use:User_Code_Key)
  use:User_Code   = lot:UserCode
  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Found
      tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  PARENT.SetQueueRecord


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  if ExchangeType = 'Y' then
      if lot:Exchange <> 'Y' then return record:filtered.
  ELSE
      if lot:Exchange = 'Y' then return record:filtered.
  END
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.OrderInit PROCEDURE(<KEY K>)
!This method is located here (in BRW1) because SELF.ORDER queue is protected in the ViewManager
  CODE
  !0{PROP:Text} = 'SortOrder=' & POINTER(SELF.ORDER) & ' order=' & SELF.ORDER.Order & ' fe=' & SELF.ORDER.FreeElement
  CLEAR(SELF.ORDER.LimitType)                         !Xplore
  FREE(SELF.ORDER.Filter)                             !Xplore
  DISPOSE(SELF.ORDER.Filter)                          !Xplore
  SELF.ORDER.Filter &= NEW FilterQueue                !Xplore
  DISPOSE(SELF.ORDER.RangeList)                       !Xplore
  SELF.Order.RangeList &= NEW BufferedPairsClass      !Xplore
  SELF.ORDER.MainKey &= K                             !Xplore
  SELF.Order.RangeList.Init                           !Xplore
  DISPOSE(SELF.Order.Order)                           !Xplore
  PUT(SELF.Order)                                     !Xplore
  RETURN                                              !Xplore
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !lot:TheDate
        ColumnString     = SUB(LEFT(BRW1.Q.lot:TheDate),1,20)
  OF 2 !lot:TheTime
  OF 3 !tmp:UserName
  OF 4 !lot:PreviousLocation
  OF 5 !lot:NewLocation
  OF 6 !lot:Exchange
  OF 7 !lot:RecordNumber
  OF 8 !lot:RefNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step8,lot:DateKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,lot:DateKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(,lot:TheDate,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,lot:TheTime,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,tmp:UserName,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(,lot:PreviousLocation,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(,lot:NewLocation,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(,lot:Exchange,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(,lot:RecordNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(,lot:RefNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(lot:RefNumber)
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
I     LONG
J     LONG
SaveQ LONG
  CODE
  SaveQ = POINTER(SELF.Fq)
  LOOP I = 1 to RECORDS(SELF.Fq)
    GET(SELF.Fq,I)
    IF NOT SELF.FQ.Sortfield
      CYCLE
    END
    LOOP J = 1 TO 2
      IF SELF.BC.SetSort(SELF.Fq.SortField + J - 1).
    END !LOOP J = 1 TO 2
  END !LOOP
  GET(SELF.Fq,SaveQ)
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(LOCATLOG)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('lot:TheDate')                !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Date')                       !Header
                PSTRING('@d6')                        !Picture
                PSTRING('Date')                       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('lot:TheTime')                !Field Name
                SHORT(20)                             !Default Column Width
                PSTRING('Time')                       !Header
                PSTRING('@t1b')                       !Picture
                PSTRING('The Time')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tmp:UserName')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:UserName')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:UserName')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('lot:PreviousLocation')       !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Previous Location')          !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Previous Location')          !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('lot:NewLocation')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('New Location')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('New Location')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('lot:Exchange')               !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Exch')                       !Header
                PSTRING('@s1')                        !Picture
                PSTRING('Exch')                       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('lot:RecordNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Record Number')              !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('lot:RefNumber')              !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Ref Number')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Ref Number')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(8)
!--------------------------
XpSortFields    GROUP,STATIC
                PSTRING('lot:TheDate')                !Column Field
                SHORT(2)                              !Nos of Sort Fields
                PSTRING('lot:TheDate')                !Sort Field Seq:1
                BYTE(1)                               !Descending
                PSTRING('@d6')                        !Picture
                PSTRING('lot:TheTime')                !Sort Field Seq:2
                BYTE(1)                               !Descending
                PSTRING('@t1b')                       !Picture
                !-------------------------
                END
XpSortDim       SHORT(1)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

ViewExchangeUnit PROCEDURE                            !Generated from procedure template - Window

Local                CLASS
AllocateExchangePart Procedure(String func:Status,Byte func:SecondUnit)
                     END
save_jobse_id        USHORT,AUTO
save_trb_id          USHORT,AUTO
save_webjob_id       USHORT,AUTO
save_joe_id          USHORT,AUTO
save_jea_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
tmp:Accessories      STRING(255)
tmp:FaultyUnit       BYTE(0)
tmp:BERConsignmentNumber STRING(30)
Print_Label_Temp     STRING(3)
Exchange_Accessory_Count_Temp LONG
Accessory_Count_Temp LONG
tmp:IMEI             STRING(30)
tmp:MSN              STRING(30)
tmp:ModelNumber      STRING(30)
tmp:ModelNumberDetails STRING(60)
tmp:SiteLocation     STRING(30)
tmp:StockType        STRING(30)
tmp:ExchangeDate     DATE
PartsCounter         LONG
WarrantyPartsCounter LONG
tmp:CurrentLocation  STRING(30)
tmp:ReplacementValue REAL
tmp:2ndExchangeDate  DATE
tmp:2ndIMEI          STRING(30)
tmp:2ndMSN           STRING(30)
tmp:2ndModelNumber   STRING(30)
tmp:2ndAccessories   STRING(255)
tmp:2ndSiteLocation  STRING(30)
tmp:2ndStockType     STRING(30)
tmp:2ndReplacementValue REAL
tmp:ProductCode      STRING(30)
tmp:SecondProductCode STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:HandsetReplacementValue REAL
tmp:SecondHandsetReplacementValue REAL
SaveExchangeLocation STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?job:ExcService
cit:Service            LIKE(cit:Service)              !List box control field - type derived from field
cit:RefNumber          LIKE(cit:RefNumber)            !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(CITYSERV)
                       PROJECT(cit:Service)
                       PROJECT(cit:RefNumber)
                     END
window               WINDOW('View Exchange Unit Details'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('View Exchange Unit Details'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,28,672,50),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Job Details'),AT(8,32),USE(?Prompt20),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number:'),AT(8,44),USE(?Prompt12),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('M.S.N.:'),AT(8,54),USE(?job:MSN:Prompt),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s20),AT(100,44,136,12),USE(job:ESN),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s20),AT(100,54,140,12),USE(job:MSN),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(436,42,140,12),USE(job:Charge_Type),HIDE,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Model Number:'),AT(8,65),USE(?Prompt12:3),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s60),AT(100,65,224,12),USE(tmp:ModelNumberDetails),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Char Charge Type:'),AT(352,42),USE(?CChargeType:Prompt),HIDE,FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Warr Charge Type:'),AT(352,65),USE(?WChargeType:Prompt),HIDE,FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(436,65,140,12),USE(job:Warranty_Charge_Type),HIDE,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(4,80,336,302),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('First Exchange Unit'),USE(?FirstExchangeTab),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           GROUP('Exchange Unit Details'),AT(8,94,328,148),USE(?Group1),BOXED,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Exchange Unit No:'),AT(12,106),USE(?Prompt5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('I.M.E.I. Number:'),AT(12,118),USE(?IMEI),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(100,118),USE(tmp:IMEI),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s8),AT(100,106),USE(job:Exchange_Unit_Number),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING('Exchange Date:'),AT(144,106),USE(?StrExchangeUnitDate),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@d6b),AT(208,106),USE(tmp:ExchangeDate),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(100,130),USE(tmp:MSN),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(100,142),USE(tmp:ModelNumber),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Accessories:'),AT(12,154),USE(?IMEI:4),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s255),AT(100,154,156,12),USE(tmp:Accessories),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(100,166,156,12),USE(tmp:SiteLocation),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Stock Type:'),AT(12,178),USE(?IMEI:5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(100,178,156,12),USE(tmp:StockType),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@n14.2),AT(100,190),USE(tmp:ReplacementValue),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Handset Part No:'),AT(12,202),USE(?tmp:ProductCode:Prompt),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(100,202),USE(tmp:ProductCode),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@n-14.2),AT(100,216),USE(tmp:HandsetReplacementValue),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Handset Replacement Value:'),AT(12,214,80,16),USE(?tmp:HandsetReplacementValue:Prompt),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             BUTTON,AT(268,212),USE(?Button:SelectHandsetPartNo),TRN,FLAT,HIDE,LEFT,ICON('handsetp.jpg')
                             BUTTON,AT(268,114),USE(?PickExchangeUnit),TRN,FLAT,LEFT,ICON('pickexcp.jpg')
                             BUTTON,AT(268,146),USE(?PickAccessory),TRN,FLAT,LEFT,ICON('pickaccp.jpg')
                             BUTTON,AT(268,178),USE(?NoAvailableUnit),TRN,FLAT,LEFT,ICON('nounitp.jpg')
                             PROMPT('Replacement Value:'),AT(12,190),USE(?tmp:ReplacementValue:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Site Location:'),AT(12,166),USE(?IMEI:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Model Number:'),AT(12,142),USE(?IMEI:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('M.S.N.:'),AT(12,130),USE(?tmp:MSN:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           GROUP('Despatch Details'),AT(8,242,328,94),USE(?DespatchDetails),BOXED,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Exchange Courier'),AT(12,252),USE(?job:Exchange_Courier:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(100,252,124,10),USE(job:Exchange_Courier),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                             BUTTON,AT(228,248),USE(?LookupExchangeCourier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Service'),AT(12,266),USE(?job:Exchange_Courier:Prompt:2),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             COMBO(@s20),AT(100,266,64,10),USE(job:ExcService),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('4L(2)|M@s1@'),DROP(10,64),FROM(Queue:FileDropCombo),MSG('Exchange Service For City Link / Label G')
                             PROMPT('Consignment Number'),AT(12,280),USE(?job:Exchange_Consignment_Number:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(100,280,124,10),USE(job:Exchange_Consignment_Number),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                             PROMPT('Despatched'),AT(12,296),USE(?job:Exchange_Despatched:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@d6b),AT(100,296,64,10),USE(job:Exchange_Despatched),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                             PROMPT('User'),AT(12,308),USE(?job:Exchange_Despatched_User:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s3),AT(100,308,64,10),USE(job:Exchange_Despatched_User),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                             BUTTON,AT(268,306),USE(?AmendDespatchDetails),TRN,FLAT,LEFT,ICON('edtdespp.jpg')
                             PROMPT('Despatch Number'),AT(12,320),USE(?job:Exchange_Despatch_Number:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s8),AT(100,320,64,10),USE(job:Exchange_Despatch_Number),SKIP,LEFT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           END
                         END
                       END
                       SHEET,AT(344,80,332,302),USE(?Sheet3),COLOR(0D6E7EFH),SPREAD
                         TAB('Second Exchange Unit'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           GROUP('Exchange Unit Details'),AT(348,96,324,146),USE(?Group3),BOXED,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Exchange Unit No:'),AT(352,106),USE(?Prompt5:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s8),AT(436,106),USE(jobe:SecondExchangeNumber),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING('Exchange Date:'),AT(480,106),USE(?StrExchangeUnitDate:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@d6),AT(544,106),USE(tmp:2ndExchangeDate),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('I.M.E.I. Number:'),AT(352,120),USE(?IMEI:6),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(436,120),USE(tmp:2ndIMEI),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('M.S.N.:'),AT(352,130),USE(?tmp:2ndMSN:Prompt),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(436,130),USE(tmp:2ndMSN),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Model Number:'),AT(352,144),USE(?IMEI:10),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(436,144),USE(tmp:2ndModelNumber),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Site Location:'),AT(352,154),USE(?IMEI:9),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(436,154,156,12),USE(tmp:2ndSiteLocation),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Stock Type:'),AT(352,168),USE(?IMEI:8),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(436,168,156,12),USE(tmp:2ndStockType),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Exchange Status:'),AT(352,192),USE(?IMEI:7),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(436,192,156,12),USE(jobe:SecondExchangeStatus),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Handset Part No:'),AT(352,202),USE(?tmp:SecondProductCode:Prompt),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(436,202),USE(tmp:SecondProductCode),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Handset Replacement Value:'),AT(352,214,80,16),USE(?tmp:SecondHandsetReplacementValue:Prompt),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@n-14.2),AT(436,216),USE(tmp:SecondHandsetReplacementValue),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             BUTTON,AT(604,212),USE(?Button:SecondSelectHandsetPartNo),FLAT,HIDE,LEFT,ICON('handsetp.jpg')
                             PROMPT('Replacement Value:'),AT(352,178),USE(?tmp:ReplacementValue:Prompt:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@n14.2),AT(436,178),USE(tmp:2ndReplacementValue),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             BUTTON,AT(604,114),USE(?PickSecondExchangeUnit),TRN,FLAT,LEFT,ICON('pickexcp.jpg')
                           END
                         END
                       END
                       BUTTON,AT(608,384),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:job:Exchange_Courier                Like(job:Exchange_Courier)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

MakeScreenViewOnly          Routine
    ?PickExchangeUnit{prop:Hide} = 1
    ?PickSecondExchangeUnit{prop:Hide} = 1
    ?PickAccessory{prop:Hide} = 1
    ?AmendDespatchDetails{prop:Hide} = 1
    ?NoAvailableUnit{prop:Hide} = 1
    ?LookupExchangeCourier{prop:Disable} = 1
    ?job:Exchange_Courier{prop:Disable} = 1
PickExchangeUnit        Routine
Data
local:SaveExchangeUnitNumber        Long()
local:48HourRecordNumber            Long()
local:PickedIMEINumber              String(30)
local:OldExchangeNumber             Long()

local:DoExchange                    Byte(0)
local:DoReturn                      Byte(0)
local:Remove                        Byte(0)
local:RestockFlag                   Byte(0)
local:ExchangeMade                  Byte(0)

local:AuditNotes                    String(255)
local:AuditAction                   String(80)

local:Error                         Byte(0)
Code
    local:SaveExchangeUnitNumber = job:Exchange_Unit_Number

    ! Does the user have access to add, or amend an exchange unit? (DBH: 10/09/2007)
    If job:Exchange_Unit_Number <> ''
        If SecurityCheck('JOBS - AMEND EXCHANGE UNIT')
            Case Missive('You do not have access to this option.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End ! If SecurityCheck('JOBS - AMEND EXCHANGE UNIT')
    Else ! If job:Exchange_Unit_Number <> ''
        If SecurityCheck('JOBS - ADD EXCHANGE UNIT')
            Case Missive('You do not have access to this option.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End ! If SecurityCheck('JOBS - ADD EXCHANGE UNIT')
    End ! If job:Exchange_Unit_Number <> ''

    tmp:FaultyUnit = 0
    glo:Select12 = job:Model_Number
    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    local:DoExchange = 1
    local:DoReturn = 0
    local:OldExchangeNumber = job:Exchange_Unit_Number

    !J - 17/02/15 - TB13484 - need to add account name and number to different Audit trails so opening the tradeacc file here ...
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    If glo:WebJob
        If COMMAND('WebAcc')
            Clarionet:Global.Param2 = Command('WebAcc')
        End !If COMMAND('WebAcc')
        tra:Account_Number  = Clarionet:Global.Param2
    ELSE
        !Not glo:webjob - must be arc
        tra:Account_Number  = 'AA20'
    End !glo:WebJob
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = LevelBenign
    !END TB13484
        

    ! Is there already an exchange unit attached. Need to remove before adding new (DBH: 10/09/2007)
    If job:Exchange_Unit_Number > 0
        local:DoReturn = 1
        glo:Select1 = ''

        ! If this is a 48 Hour job and there is already an exchange unit attached (DBH: 10/09/2007)
        If jobe:Engineer48HourOption = 1
            ! The only reason to be returning is for a faulty exchange (DBH: 10/09/2007)
            glo:Select2 = '48HOUR'
        Else ! If jobe:Engineer48HourOption = 1
            glo:Select2 = 'E'
        End ! If jobe:Engineer48HourOption = 1

        Loan_Changing_Screen

        If glo:Select1 = ''
            ! Nothing was picked (DBH: 10/09/2007)
            Exit
        End ! If glo:Select1 = ''

        If job:Exchange_Consignment_Number <> ''
            Case Missive('Warning! The Exchange Unit for this job has already been despatched.'&|
              '<13,10>If you continue the existing unit on this job will be removed and returned to Exchange Stock.'&|
              '<13,10>(An entry will be made in the audit trail)'&|
              '<13,10>Are you sure you want to remove the Exchange Unit?','ServiceBase 3g',|
                           'mquest.jpg','\No|/Yes')
                Of 2 ! Yes Button
                    local:Remove = 1
                Of 1 ! No Button
                    Exit
            End ! Case Missive
        End ! If job:Exchange_Consignment_Number <> ''

        If local:Remove = 1
            local:AuditNotes = 'UNIT HAD BEEN DESPATCHED: ' & |
                                '<13,10>UNIT NUMBER: ' & Clip(job:Exchange_unit_number) & |
                                '<13,10>COURIER: ' & CLip(job:exchange_Courier) & |
                                '<13,10>CONSIGNMENT NUMBER: ' & CLip(job:exchange_consignment_number) & |
                                '<13,10>DATE DESPATCHED: ' & Clip(Format(job:Exchange_despatched,@d6)) &|
                                '<13,10>DESPATCH USER: ' & CLip(job:Exchange_despatched_user) &|
                                '<13,10>DESPATCH NUMBER: ' & CLip(job:exchange_despatch_number)
        Else ! If local:Remove = 1
            !This was   local:AuditNotes = 'UNIT NUMBER: ' & CLip(job:exchange_unit_Number)
            !J - 07/12/15 - TB13484 - add details of MODEL NUMBER, I.M.E.I. and STOCK TYPE (like is shown when an exchange is attached)
            !at the same time add the account name and number of who is removing the exchange
            
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = job:Exchange_Unit_Number
            If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                local:AuditNotes = 'UNIT NUMBER: ' & CLip(xch:ref_number) & |
                       '<13,10>MODEL NUMBER: ' & CLip(xch:model_number) & |
                       '<13,10>I.M.E.I.: ' & CLip(xch:esn) & |
                       '<13,10>STOCK TYPE: ' & Clip(xch:Stock_Type)
            ELSE
                local:AuditNotes = 'UNIT NUMBER: ' & CLip(job:exchange_unit_Number) & ' Further exchange details not found in system.'
            END
            local:AuditNotes = clip(tra:Company_Name) &'; '& clip(tra:Account_Number)& '; REMOVED EXCHANGE<13,10>'&clip(local:AuditNotes)
            !END TB13484

        End ! If local:Remove = 1

        Case glo:Select1
        Of 1
            !aud:Action = 'REPLACED FAULTY EXCHANGE UNIT'               !removed - J - 17/02/15 - TB13484 - Audit not being actioned in this code
            local:AuditAction = 'REPLACED FAULTY EXCHANGE UNIT'         !Added   - J - 17/02/15 - TB13484 - Variable used in setting audit trail below
            tmp:FaultyUnit = 1
        Of 2
            !aud:Action = 'ALTERNATIVE EXCHANGE MODEL REQUIRED'         !removed - J - 17/02/15 - TB13484 - Audit not being actioned in this code
            local:AuditAction = 'ALTERNATIVE EXCHANGE MODEL REQUIRED'   !Added   - J - 17/02/15 - TB13484 - Variable used in setting audit trail below
        Of 3
            !aud:Action = 'EXCHANGE UNIT NOT REQUIRED: RE-STOCKED'      !removed - J - 17/02/15 - TB13484 - Audit not being actioned in this code
            local:AuditAction = 'EXCHANGE UNIT NOT REQUIRED: RE-STOCKED'!Added   - J - 17/02/15 - TB13484 - Variable used in setting audit trail below
            local:DoExchange = 0
        Else
            local:DoExchange = 0
            local:DoReturn = 0
            local:AuditAction = ''

        End ! Case glo:Select1


        If AddToAudit(job:Ref_Number,'EXC',Clip(local:AuditAction),Clip(local:AuditNotes))

        End ! If AddToAudit(job:Ref_Number,'EXC',local:AuditAction,local:AuditNotes)

        !TB13233 - update the exchange location
        UpdateExchLocation(job:Ref_number,Use:user_code,'REMOVED FROM JOB')

        ! Remove all despatch information (DBH: 10/09/2007)
        job:Exchange_Unit_Number = 0
        job:Exchange_Accessory = ''
        job:Exchange_Consignment_Number = ''
        job:Exchange_Despatched = ''
        job:Exchange_Despatched_User = ''
        job:Exchange_Issued_Date = ''
        job:Exchange_User = ''
        If job:Despatch_Type = 'EXC'
            job:Despatched = 'NO'
            job:Despatch_Type = ''
        Else ! If job:Despatch_Type = 'EXC'
            Save_JOBSE_ID = Access:JOBSE.SaveFile()
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Found
                If jobe:DespatchType = 'EXC'
                    jobe:DespatchType = ''
                    Access:JOBSE.TryUpdate()

                    Save_WEBJOB_ID = Access:WEBJOB.SaveFile()
                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_Number
                    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Found
                        wob:ReadyToDespatch = 0
                        Access:WEBJOB.TryUpdate()
                    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Error
                    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                    Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)
                End ! If jobe:DespatchType = 'EXC'
            Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            Access:JOBSE.RestoreFile(Save_JOBSE_ID)
        End ! If job:Despatch_Type = 'EXC'

        GetStatus(101,1,'EXC')

        ! Remove the "exchange" parts (DBH: 10/09/2007)
        Access:PARTS.ClearKey(par:Part_Number_Key)
        par:Ref_Number = job:Ref_Number
        par:Part_Number = 'EXCH'
        If Access:PARTS.TryFetch(par:Part_Number_Key) = Level:Benign
            !Found
            RemoveFromStockAllocation(par:Record_Number,'CHA')
            Delete(PARTS)
            local:RestockFlag = 1
        Else ! If Access:PARTS.TryFetch(par:Part_Number_Key) = Level:Benign
            !Error
        End ! If Access:PARTS.TryFetch(par:Part_Number_Key) = Level:Benign

        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = job:Ref_Number
        wpr:Part_Number = 'EXCH'
        If Access:WARPARTS.TryFetch(wpr:Part_Number_Key) = Level:Benign
            !Found
            RemoveFromStockAllocation(wpr:Record_Number,'WAR')
            Delete(WARPARTS)
            local:RestockFlag = 1
        Else ! If Access:WARPARTS.TryFetch(wpr:Part_Number_Key) = Level:Benign
            !Error
        End ! If Access:WARPARTS.TryFetch(wpr:Part_Number_Key) = Level:Benign

        glo:Select1 = ''
        glo:Select2 = ''
    End ! If job:Exchange_Unit_Number > 0

    local:Error = 0
    If local:DoExchange = 1
        ! Can't have a loan and an exchange ready to despatch at the same time (DBH: 10/09/2007)
        If job:loan_unit_number <> '' And ((glo:WebJob = 1 And jobe:Despatched = 'REA' And jobe:DespatchType = 'LOA') Or |
                                                (glo:WebJob = 0 And job:Despatched = 'REA' and job:Despatch_Type = 'LOA'))
            Case Missive('You cannot attached an Exchange Unit until the Loan Unit has been despatched.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            local:Error = 1
        End ! If job:loan_unit_number <> '' And ((glo:WebJob = 1 And jobe:Despatched = 'REA' And jobe:DespatchType = 'LOA') Or |

        job:Exchange_Unit_Number = SelectLoanExchangeUnit('EXC')

        local:48HourRecordNumber = 0

        If local:SaveExchangeUnitNumber = 0 And job:Exchange_Unit_Number > 0 And local:Error = 0
            If jobe:Engineer48HourOption = 1
                ! 48 Hour Job. Is there an outstanding order for this job? (DBH: 10/09/2007)
                Access:EXCHOR48.ClearKey(ex4:AttachedToJobKey)
                ex4:AttachedToJob = 0
                ex4:Location = tmp:CurrentLocation
                ex4:JobNumber = job:Ref_Number
                If Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign
                    !Found

                    local:48HourRecordNumber = ex4:RecordNumber

                    ! Is the new IMEI the same? (DBH: 10/09/2007)
                    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                    xch:Ref_Number = job:Exchange_Unit_Number
                    If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                        !Found
                        local:PickedIMEINumber = xch:ESN
                    Else ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                        !Error
                    End ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign

                    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                    xch:Ref_Number = ex4:OrderUnitNumber
                    If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                        !Found
                        If xch:ESN <> local:PickedIMEINumber
                            Case Missive('An Exchange Unit has been ordered for this job under the 48 Hour Exchange Process.'&|
                              '<13,10>The I.M.E.I. Number of the unit you have selected is NOT the unit that has been ordered.'&|
                              '<13,10>Are you sure you want to continue with the incorrect unit?','ServiceBase 3g',|
                                           'mquest.jpg','\No|/Yes')
                                Of 2 ! Yes Button
                                Of 1 ! No Button
                                    job:Exchange_Unit_Number = 0
                                    local:Error = 1
                            End ! Case Missive
                        End ! If xch:ESN <> local:PickedIMEINumber
                    Else ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                        !Error
                    End ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign

                Else ! If Access:EXCH0R48.TryFetch(ex4:AttachedToJobKey) = Level:Benign
                    !Error
                End ! If Access:EXCH0R48.TryFetch(ex4:AttachedToJobKey) = Level:Benign
            Else ! If jobe:Engineer48HourOption = 1
            End ! If jobe:Engineer48HourOption = 1
        End ! If local:SaveExchangeUnitNumber = 0 And job:Exchange_Unit_Number > 0

        If job:Exchange_Unit_Number > 0 And local:Error = 0
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = job:Exchange_Unit_Number
            If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                Access:STOCKTYP.ClearKey(stp:Stock_Type_Key)
                stp:Stock_Type = xch:Stock_Type
                If Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign
                    !Found
                    If stp:Available <> 1
                        Case Missive('The selected unit is NOT available.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        job:Exchange_Unit_Number = 0
                        local:Error = 1
                    End ! If stp:Available <> 1
                Else ! If Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign
                    !Error
                End ! If Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign

                If xch:Model_Number <> job:Model_Number and local:Error = 0
                    Case Missive('The selected Exchange Unit has a Model Number of : ' & Clip(xch:Model_Number) & '.'&|
                      '<13,10>'&|
                      '<13,10>Do you want to ignore this mismatch?','ServiceBase 3g',|
                                   'mquest.jpg','\Cancel|/Ignore')
                        Of 2 ! Ignore Button
                        Of 1 ! Cancel Button
                            job:Exchange_Unit_Number = 0
                            local:Error = 1
                    End ! Case Missive
                End ! If xch:Model_Number <> job:Model_Number and local:Error = 0

                If local:Error = 0
                    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                    man:Manufacturer = job:Manufacturer
                    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        !Found
                        ! Do we need to QA this before we can despatch it? (DBH: 10/09/2007)
                        If man:QALoanExchange
                            xch:Available = 'QA1'
                        Else ! If man:QALoanExchange
                            xch:Available = 'EXC'
                        End ! If man:QALoanExchange
                        xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                        xch:Job_Number = job:Ref_Number
                        Access:EXCHANGE.TryUpdate()

                        If Access:EXCHHIST.PrimeRecord() = Level:Benign
                            exh:Ref_Number = job:Exchange_Unit_Number
                            exh:Date       = Today()
                            exh:Time       = Clock()
                            Access:USERS.ClearKey(use:Password_Key)
                            use:Password = glo:Password
                            If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                                !Found
                            Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                                !Error
                            End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                            exh:User       = use:User_Code
                            If man:QALoanExchange
                                exh:status        = 'AWAITING QA. EXCHANGED ON JOB NO: ' & clip(job:ref_number)
                            Else ! If man:QALoanExchange
                                exh:status        = 'UNIT EXCHANGED ON JOB NO: ' & clip(job:ref_number)
                            End ! If man:QALoanExchange
                            If Access:EXCHHIST.TryInsert() = Level:Benign
                                !Insert
                            Else ! If Access:EXCHHIST.TryInsert() = Level:Benign
                                Access:EXCHHIST.CancelAutoInc()
                            End ! If Access:EXCHHIST.TryInsert() = Level:Benign
                        End ! If Access.EXCHHIST.PrimeRecord() = Level:Benign

                        local:AuditNotes = 'UNIT NUMBER: ' & CLip(xch:ref_number) & |
                                            '<13,10>MODEL NUMBER: ' & CLip(xch:model_number) & |
                                            '<13,10>I.M.E.I.: ' & CLip(xch:esn)
                        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                        mod:Model_Number = xch:Model_Number
                        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                            !Found
                        Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                            !Error
                        End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                        If MSNRequired(mod:Manufacturer) = Level:Benign
                            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>M.S.N.: ' & Clip(xch:MSN)
                        End ! If MSNRequired(mod:Manufacturer) = Level:Benign

                        local:AuditNotes = Clip(local:AuditNotes) & '<13,10>STOCK TYPE: ' & Clip(xch:Stock_Type)

                        !J - 17/02/15 - TB13484 - need to add name and account number of who is attaching the exchange
                        local:AuditNotes = clip(tra:Company_Name) &'; '& clip(tra:Account_Number)& '; ALLOCATED EXCHANGE<13,10>'&clip(local:AuditNotes)
                        !END TB13484

                        If AddToAudit(job:Ref_Number,'EXC','EXCHANGE UNIT ATTACHED TO JOB',Clip(local:AuditNotes))

                        End ! If AddToAudit(job:Ref_Number,'EXC','EXCHANGE UNIT ATTACHED TO JOB',Clip(local:AuditNotes))

                        Do GetExchangeUnitDate

                        local:ExchangeMade = 1

                        ! Where has the exchange been made? (DBH: 10/09/2007)
                        If glo:WebJob
                            jobe:ExchangedAtRRC = 1
                        Else ! If glo:WebJob
                            jobe:ExchangedAtRRC = 0
                        End ! If glo:WebJob
                        Access:JOBSE.TryUpdate()

                        !TB13233 - J - 10/12/14 - adding exchange location and history
                        Access:jobse3.clearkey(jobe3:KeyRefNumber)
                        jobe3:RefNumber = job:Ref_Number
                        if access:jobse3.fetch(jobe3:KeyRefNumber)
                            !not found
                            Access:jobse3.primerecord()
                            jobe3:RefNumber = job:Ref_Number
                            SaveExchangeLocation = 'N/A'
                        ELSE
                            !found
                            SaveExchangeLocation = jobe3:Exchange_IntLocation
                        END
                        
                        if glo:WebJob then
                            jobe3:Exchange_IntLocation = 'AT FRANCHISE'
                        ELSE
                            jobe3:Exchange_IntLocation = 'AT ARC'
                        END
                        Access:Jobse3.update()

                        Access:Locatlog.primerecord()
                        lot:RefNumber        = job:Ref_number
                        lot:TheDate          = today()
                        lot:TheTime          = clock()
                        lot:UserCode         = use:User_Code        !user fetched on open window
                        lot:PreviousLocation = SaveExchangeLocation
                        lot:NewLocation      = jobe3:Exchange_IntLocation
                        lot:Exchange         = 'Y'
                        Access:LocatLog.update()
                        !END TB13233 - J - 10/12/14 - adding exchange location and history

                        ! Add "exchange" part (DBH: 10/09/2007)
                        local.AllocateExchangePart('PIK',0)

                        ! Create a new exchange unit for the "incoming unit" (DBH: 10/09/2007)

                        Access:EXCHANGE_ALIAS.ClearKey(xch_ali:Ref_Number_Key)
                        xch_ali:Ref_Number = job:Exchange_Unit_Number
                        If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:Ref_Number_Key) = Level:Benign
                            !Found
                            ! Does this unit already exist? (DBH: 10/09/2007)
                            Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                            xch:ESN = job:ESN
                            If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                                !Found
                                ! A unit already exits with this IMEI Number. We'll use that one (DBH: 10/09/2007)
                                If job:Date_Completed <> ''
                                    ! Job has been completed. Make the exchange to Return To Stock (DBH: 10/09/2007)
                                    xch:Available = 'RTS'
                                Else ! If job:Date_Completed <> ''
                                    If job:Workshop = 'YES'
                                        xch:Available = 'REP'
                                    Else ! If job:Workshop = 'YES'
                                        xch:Available = 'INC'
                                    End ! If job:Workshop = 'YES'
                                End ! If job:Date_Completed <> ''
                                xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                                xch:Job_Number = job:Ref_Number
                                Access:EXCHANGE.TryUpdate()
                            Else ! If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                                !Error
                                ! The IMEI doesn't already exist. Create a new entry (DBH: 10/09/2007)
                                If Access:EXCHANGE.PrimeRecord() = Level:Benign
                                    RefNumber# = xch:Ref_Number
                                    xch:Record :=: xch_ali:Record
                                    xch:Ref_Number = RefNumber#

                                    If job:Date_Completed <> ''
                                        xch:Available = 'RTS'
                                    Else ! If job:Date_Completed <> ''
                                        If job:Workshop = 'YES'
                                            xch:Available = 'REP'
                                        Else ! If job:Workshop = 'YES'
                                            xch:Available = 'INC'
                                        End ! If job:Workshop = 'YES'
                                    End ! If job:Date_Completed <> ''

                                    xch:Job_Number = job:Ref_Number
                                    xch:ESN        = job:ESN
                                    xch:MSN        = job:MSN
                                    xch:Model_Number = job:Model_Number
                                    xch:Manufacturer = job:Manufacturer
                                    xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                                    If Access:EXCHANGE.TryInsert() = Level:Benign
                                        !Insert
                                    Else ! If Access:EXCHANGE.TryInsert() = Level:Benign
                                        Access:EXCHANGE.CancelAutoInc()
                                    End ! If Access:EXCHANGE.TryInsert() = Level:Benign
                                End ! If Access.EXCHANGE.PrimeRecord() = Level:Benign
                            End ! If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign

                            If xch_ali:Audit_Number <> ''
                                Access:EXCAUDIT.ClearKey(exa:Audit_Number_Key)
                                exa:Stock_Type = xch:Stock_Type
                                exa:Audit_Number = xch:Audit_Number
                                If Access:EXCAUDIT.TryFetch(exa:Audit_Number_Key) = Level:Benign
                                    !Found
                                    exa:Replacement_Unit_Number = xch:Ref_Number
                                    Access:EXCAUDIT.Update()
                                Else ! If Access:EXCAUDIT.TryFetch(exa:Audit_Number_Key) = Level:Benign
                                    !Error
                                End ! If Access:EXCAUDIT.TryFetch(exa:Audit_Number_Key) = Level:Benign
                            End ! If xch_ali:Audit_Number <> ''

                            ! Mark exchange order as fulfilled (DBH: 10/09/2007)
                            If local:48HourRecordNumber <> 0
                                Access:EXCHOR48.ClearKey(ex4:RecordNumberKey)
                                ex4:RecordNumber = local:48HourRecordNumber
                                If Access:EXCHOR48.TryFetch(ex4:RecordNumberKey) = Level:Benign
                                    !Found
                                    ex4:AttachedToJob = 1
                                    Access:EXCHOR48.Update()
                                Else ! If Access:EXCHOR48.TryFetch(ex4:RecordNumberKey) = Level:Benign
                                    !Error
                                End ! If Access:EXCHOR48.TryFetch(ex4:RecordNumberKey) = Level:Benign
                            End ! If local:48HourRecordNumber <> 0

                            If man:QALoanExchange
                                job:Despatched = ''
                                job:Despatch_Type = ''
                                job:Exchange_Status = 'QA REQUIRED'
                                job:Exchange_User = exh:User
                                GetStatus(605,ThisWindow.Request,'EXC')
                            Else ! If man:QALoanExchange
                                If glo:WebJob
                                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                                    jobe:RefNumber = job:Ref_Number
                                    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                        !Found
                                        jobe:DespatchType = 'EXC'
                                        jobe:Despatched = 'REA'
                                        Access:JOBSE.TryUpdate()

                                        Save_WEBJOB_ID = Access:WEBJOB.SaveFile()
                                        Access:WEBJOB.ClearKey(wob:RefNumberKey)
                                        wob:RefNumber = job:Ref_Number
                                        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                            !Found
                                            wob:ReadyToDespatch = 1
                                            wob:DespatchCourier = job:Exchange_Courier
                                            Access:WEBJOB.TryUpdate()
                                        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                            !Error
                                        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

                                        Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)
                                    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                        !Error
                                    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                Else ! If glo:WebJob
                                    job:Exchange_Status = 'AWAITING DESPATCH'
                                    job:Exchange_User = exh:User
                                    Access:COURIER.ClearKey(cou:Courier_Key)
                                    cou:courier = job:Exchange_Courier
                                    If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                                        !Found
                                        If cou:Courier_Type = 'CITY LINK' Or cou:Courier_Type = 'LABEL G'
                                            job:ExcService = cou:Service
                                        Else ! If cou:Courier_Type = 'CITY LINK' Or cou:Courier_Type = 'LABEL G'
                                            job:ExcService = ''
                                        End ! If cou:Courier_Type = 'CITY LINK' Or cou:Courier_Type = 'LABEL G'
                                        job:Exchange_Despatched = DespatchANC(job:Exchange_Courier,'EXC')
                                        GetStatus(110,ThisWindow.Request,'EXC')

                                    Else ! If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                                        !Error
                                    End ! If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                                End ! If glo:WebJob
                            End ! If man:QALoanExchange

                            If job:Exchange_Courier = ''
                                Case Missive('Warning! There is no courier attached to this Exchange Unit.'&|
                                  '<13,10>'&|
                                  '<13,10>It will not despatch correctly if you do not select one.','ServiceBase 3g',|
                                               'mexclam.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                            End ! If job:Exchange_Courier = ''

                            If def:Use_Loan_Exchange_Label = 'YES'
                                glo:Select1 = job:Ref_Number
                                If Clip(GETINI('PRINTING','ExchangeRepairLabel',,CLIP(PATH())&'\SB2KDEF.INI')) = 1 !and tmp:FaultyUnit
                                    ExchangeRepairLabel(job:Ref_Number)
                                Else !If Clip(GETINI('PRINTING','ExchangeRepairLabel',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                                    glo:select2 = job:exchange_unit_number
                                    Set(defaults)
                                    access:defaults.next()
                                    Case def:label_printer_type
                                        Of 'TEC B-440 / B-442'
                                            Thermal_Labels_Exchange
                                        Of 'TEC B-452'
                                            Thermal_Labels_Exchange_B452
                                    End!Case def:themal_printer_type
                                End !If Clip(GETINI('PRINTING','ExchangeRepairLabel',,CLIP(PATH())&'\SB2KDEF.INI')) = 1

                                glo:select1 = ''
                                glo:select2 = ''
                            End ! If def:Use_Loan_Exchange_Label = 'YES'

                            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                            sub:Account_Number = job:Account_Number
                            If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                                !Found
                                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                                tra:Account_Number = sub:Main_Account_Number
                                If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                                    !Found
                                    If tra:RefurbCharge = 'YES' And job:Workshop = 'YES'
                                        Case Missive('This Repair Unit must be refurbished.'&|
                                          '<13,10>Do you want to print a Refurbishment Label?'&|
                                          '<13,10>'&|
                                          '<13,10>(Note: It will be printed on completion of editing this job)','ServiceBase 3g',|
                                                       'mquest.jpg','\No|/Yes')
                                            Of 2 ! Yes Button
                                                print_label_temp = 'YES'
                                            Of 1 ! No Button
                                        End ! Case Missive
                                    End ! If tra:RefurbCharge = 'YES' And job:Workshop = 'YES'
                                Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                                    !Error
                                End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                            Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                                !Error
                            End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

                            ! Update Third Party Batch (DBH: 10/09/2007)
                            If job:Third_Party_Site <> ''
                                Access:TRDBATCH.Clearkey(trb:ESN_Only_Key)
                                trb:ESN = job:ESN
                                Set(trb:ESN_Only_Key,trb:ESN_Only_Key)
                                Loop ! Begin Loop
                                    If Access:TRDBATCH.Next()
                                        Break
                                    End ! If Access:TRDBATCH.Next()
                                    If trb:ESN <> job:ESN
                                        Break
                                    End ! If trb:ESN <> job:ESN
                                    If trb:Ref_Number = job:Ref_Number
                                        trb:Exchanged = 'YES'
                                        Access:TRDBATCH.Update()
                                    End ! If trb:Ref_Number = job:Ref_Number
                                End ! Loop

                            End ! If job:Third_Party_Site <> ''

                            If glo:WebJob
                                local:Error = 0
                                PartsCounter = 0
                                WarrantyPartsCounter = 0
                                Do CountParts

                                If PartsCounter > 0
                                    Case Missive('This job has parts attached. These must be removed before you can send it to the ARC.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                        Of 1 ! OK Button
                                    End ! Case Missive
                                    local:Error = 1
                                Else ! If PartsCounter > 0
                                    If WarrantyPartsCounter > 0
                                        ! Keep the parts if it's warranty and has an exchange. This should therefore be an RF Swap (hopefully) (DBH: 10/09/2007)
                                        Case Missive('Warning! This job has Warranty Parts attached.'&|
                                          '<13,10>'&|
                                          '<13,10>Do you wish to keep the parts attached to the job when it is sent to the ARC?','ServiceBase 3g',|
                                                       'mquest.jpg','\No|/Yes')
                                            Of 2 ! Yes Button
                                            Of 1 ! No Button
                                                local:Error = 1
                                        End ! Case Missive
                                    End ! If WarrantyPartsCounter > 0
                                End ! If PartsCounter > 0

                                If local:Error = 0
                                    Do HubRepair
                                End ! If local:Error = 0
                            End ! If glo:WebJob

                            jobe:ExchangeProductCode = ''
                            jobe:HandsetReplacmentValue = 0
                            Access:JOBSE.TryUpdate()

                            ! Pick Product Code if ARC and Setup In Manufacturer (DBH: 10/09/2007)
                            If job:Exchange_Unit_Number > 0 and ~glo:WebJob
                                If man:UseProdCodesForEXC = 1
                                    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                                    xch:Ref_Number = job:Exchange_Unit_Number
                                    If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                                        !Found
                                    Else ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                                        !Error
                                    End ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                                    GlobalResponse = RequestCancelled
                                    GlobalRequest = SelectRecord
                                    glo:Select50 = xch:Model_Number
                                    PickModelProductCodes
                                    glo:Select50 = ''
                                    If GlobalResponse = RequestCompleted
                                        jobe:ExchangeProductCode = prd:ProductCode
                                        jobe:HandsetReplacmentValue = prd:HandsetReplacementValue
                                    Else ! If GlobalResponse = RequestCompleted
                                        jobe:ExchangeProductCode = ''
                                        jobe:HandsetReplacmentValue = 0
                                    End ! If GlobalResponse = RequestCompleted
                                    Access:JOBSE.TryUpdate()
                                End ! If man:UseProdCodesForEXC = 1
                            End ! If job:Exchange_Unit_Number > 0 and ~glo:WebJob
                        Else ! If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:Ref_Number_Key) = Level:Benign
                            !Error
                        End ! If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:Ref_Number_Key) = Level:Benign

                    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        !Error
                        Case Missive('Error! Cannot find the Manufacturer attached to this job.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                End ! If local:Error = 0
            Else ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                !Error
            End ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign

        End ! If job:Exchange_Unit_Number > 0
        GlobalRequest = SaveRequest#
    End ! If local:DoExchange = 1
    If local:DoReturn = 2
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = local:OldExchangeNumber
        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
            !Found
            sto:Quantity_Stock += 1
            Access:STOCK.TryUpdate()
        Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
    End ! If local:DoReturn = 2
    If local:DoReturn = 1
        ! Delete incoming unit (DBH: 10/09/2007)
        Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
        xch:ESN = job:ESN
        Set(xch:ESN_Only_Key,xch:ESN_Only_Key)
        Loop ! Begin Loop
            If Access:EXCHANGE.Next()
                Break
            End ! If Access:EXCHANGE.Next()
            If xch:ESN <> job:ESN
                Break
            End ! If xch:ESN <> job:ESN
            If xch:Job_Number = job:Ref_Number
                Relate:EXCHANGE.Delete(0)
            End ! If xch:Job_Number = job:Ref_Number
        End ! Loop

        If local:RestockFlag = 0
            local.AllocateExchangePart('RTS',0)
        End ! If local:RestockFlag = 0

        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = local:OldExchangeNumber
        If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            !Found
            If tmp:FaultyUnit
                xch:Available = 'INR'
                xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                Case Missive('Please book the replaced Exchange unit as a new job.','ServiceBase 3g',|
                                       'mexclam.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            Else ! If tmp:FaultyUnit
                If xch:Available <> 'QAF'
                    xch:Available = 'AVL'
                    xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                End ! If xch:Available <> 'QAF'
            End ! If tmp:FaultyUnit
            xch:Job_Number = ''
            Access:EXCHANGE.TryUpdate()

            If Access:EXCHHIST.PrimeRecord() = Level:Benign
                exh:Ref_Number = local:OldExchangeNumber
                exh:Date    = Today()
                exh:Time    = Clock()
                exh:User    = use:User_Code
                If tmp:FaultyUnit
                    exh:Status = 'FAULTY UNIT RE-STOCKED FROM JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
                Else ! If tmp:FaultUnit
                    exh:Status = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
                End ! If tmp:FaultUnit
                If Access:EXCHHIST.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:EXCHHIST.TryInsert() = Level:Benign
                    Access:EXCHHIST.CancelAutoInc()
                End ! If Access:EXCHHIST.TryInsert() = Level:Benign
            End ! If Access.EXCHHIST.PrimeRecord() = Level:Benign

        Else ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            !Error
        End ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
    End ! If local:DoReturn = 1
    glo:Select12 = ''

    Do CityLinkService
    Do UpdateAccessories
    Do GetExchangeDetails
    ThisWindow.Reset(1)
PickSecondExchangeUnit          Routine
Data
local:SaveExchangeUnitNumber        Long
local:DoExchange                    Byte(0)
local:DoReturn                      Byte(0)
local:DoRestock                     Byte(0)
locAction STRING(30)
locAuditNotes STRING(255)
Code

    local:SaveExchangeUnitNumber = jobe:SecondExchangeNumber

    If jobe:SecondExchangeNumber <> ''
        If SecurityCheck('JOBS - AMEND EXCHANGE UNIT')
            Case Missive('You do not have access to this option.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If SecurityCheck('JOBS - AMEND EXCHANGE UNIT')
    Else !jobe:SecondExchangeNumber <> ''
        If SecurityCheck('JOBS - ADD EXCHANGE UNIT')
            Case Missive('You do not have access to this option.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If SecurityCheck('JOBS - ADD EXCHANGE UNIT')
    End !jobe:SecondExchangeNumber <> ''

    local:DoExchange = True
    If jobe:SecondExchangeNumber <> ''
        glo:Select1 = ''
        glo:Select2 = 'E'

        local:DoReturn  = True
        Loan_Changing_Screen

        Case glo:Select1
            Of 1
                locAction = 'REPLACED FAULTY SECOND EXCHANGE UNIT'
                tmp:FaultyUnit = 1
            Of 2
                locAction = 'ALTERNATIVE SECOND EXCHANGE MODEL REQUIRED'
            Of 3
                locAction = 'SECOND EXCHANGE UNIT NOT REQUIRED'
                GetStatus(101,0,'2NE')
                jobe:SecondExchangeNumber = 0

                Save_par_ID = Access:PARTS.SaveFile()
                Access:PARTS.ClearKey(par:Part_Number_Key)
                par:Ref_Number  = job:Ref_Number
                par:Part_Number = 'EXCH'
                Set(par:Part_Number_Key,par:Part_Number_Key)
                Loop
                    If Access:PARTS.NEXT()
                       Break
                    End !If
                    If par:Ref_Number  <> job:Ref_Number      |
                    Or par:Part_Number <> 'EXCH'      |
                        Then Break.  ! End If
                    If par:SecondExchangeUnit = True
                        RemoveFromStockAllocation(par:Record_Number,'CHA')
                        Relate:PARTS.Delete(0)
                        local:DoRestock = True
                        Break
                    End !If par:SecondExchangeUnit = True
                End !Loop
                Access:PARTS.RestoreFile(Save_par_ID)

                Save_wpr_ID = Access:WARPARTS.SaveFile()
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = job:Ref_Number
                wpr:Part_Number = 'EXCH'
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                       Break
                    End !If
                    If wpr:Ref_Number  <> job:Ref_Number      |
                    Or wpr:Part_Number <> 'EXCH'      |
                        Then Break.  ! End If
                    If wpr:SecondExchangeUnit = True
                        RemoveFromStockAllocation(wpr:Record_Number,'WAR')
                        Relate:WARPARTS.Delete(0)
                        local:DoRestock = True
                        Break
                    End !If wpr:SecondExchangeUnit = True
                End !Loop
                Access:WARPARTS.RestoreFile(Save_wpr_ID)

                local:DoExchange = False
            Else
                local:DoExchange = False
                local:Doreturn  = False
        End !Case glo:Select1
        glo:Select1 = ''
        glo:Select2 = ''

        IF (AddToAudit(job:Ref_Number,'2NE',locAction,'UNIT NUMBER: ' & Clip(jobe:SecondExchangeNumber)))
        END ! IF

    End !jobe:SecondExchangeNumber <> ''

    If local:DoExchange
        jobe:SecondExchangeNumber = SelectLoanExchangeUnit('EXC')
        If jobe:SecondExchangeNumber <> 0
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = jobe:SecondExchangeNumber
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                Access:STOCKTYP.Clearkey(stp:Stock_Type_Key)
                stp:Stock_Type   = xch:Stock_Type
                If Access:STOCKTYP.Tryfetch(stp:Stock_Type_Key) = Level:Benign
                    !Found
                    If stp:Available <> 1
                        Case Missive('The selected unit is not available.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        jobe:SecondExchangeNumber = 0
                        Exit
                    End !If stp:Available <> 1
                Else ! If Access:STOCKTYP.Tryfetch(stp:Stock_Type_Key) = Level:Benign
                    !Error
                End !If Access:STOCKTYP.Tryfetch(stp:Stock_Type_Key) = Level:Benign

                If xch:Model_Number <> job:Model_Number
                    Case Missive('The selected Exchange Unit has a Model Number of ' & Clip(xch:Model_Number) & '. '&|
                      '<13,10>'&|
                      '<13,10>Do you want to ignore this mismatch?','ServiceBase 3g',|
                                   'mquest.jpg','\Cancel|/Ignore')
                        Of 2 ! Ignore Button
                        Of 1 ! Cancel Button
                            jobe:SecondExchangeNumber = 0
                            Exit
                    End ! Case Missive

                End !If xch:Model_Number <> job:Model_Number

                Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                man:Manufacturer    = job:Manufacturer
                If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                    !Found
                    If man:QALoanExchange
                        xch:Available = 'QA1'
                    Else !If man:QALoanExchange
                        xch:Available = 'EXC'
                    End !If man:QALoanExchange
                    xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                    xch:Job_Number = job:Ref_Number
                    Access:EXCHANGE.Update()

                        if access:exchhist.primerecord() = level:benign
                            exh:ref_number    = jobe:SecondExchangeNumber
                            exh:date          = Today()
                            exh:time          = Clock()
                            access:users.clearkey(use:password_key)
                            use:password =glo:password
                            access:users.fetch(use:password_key)
                            exh:user = use:user_code
                            If man:QALoanExchange = 'YES'
                                exh:status        = 'AWAITING QA. EXCHANGED ON JOB NO: ' & clip(job:ref_number)
                            Else!If def:qaexchloan = 'YES'
                                exh:status        = 'UNIT EXCHANGED (2ND) ON JOB NO: ' & clip(job:ref_number)
                            End!If def:qaexchloan = 'YES'
                            if access:exchhist.insert()
                                access:exchhist.cancelautoinc()
                            end
                        end!if access:exchhist.primerecord() = level:benign
!Add Exchange Audit
                        locAuditNotes         = 'UNIT NUMBER: ' & CLip(xch:ref_number) & |
                                            '<13,10>MODEL NUMBER: ' & CLip(xch:model_number) & |
                                            '<13,10>I.M.E.I.: ' & CLip(xch:esn)
                        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                        mod:Model_Number = xch:Model_Number
                        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                            !Found

                        Else!If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign

                        If MSNRequired(mod:Manufacturer) = Level:Benign
                            locAuditNotes       = Clip(locAuditNotes) & '<13,10>M.S.N.: ' & Clip(xch:MSN)
                        End !If MSNRequired(job:Manufacturer) = Level:Benign
                        !Log 3777 Save the Stock Type incase it's needed when a second exchange is attached
                        !DBH 10/1/04
                        locAuditNotes         = Clip(locAuditNotes) & '<13,10>STOCK TYPE: ' & Clip(xch:Stock_Type)

                        IF (AddToAudit(job:Ref_Number,'2NE','SECOND EXCHANGE UNIT ATTACHED TO JOB',locAuditNotes))
                        END ! IF

                        Do GetExchangeUnitDate

                        ExchangeMade# = 1

                        Local.AllocateExchangePart('PIK',1)
                        GetStatus(605,0,'2NE')

                        ! Inserting (DBH 08/12/2005) #6644 - Blank the fields before selecting. This should hopefully prevent any spurious data
                        jobe:SecondExcProdCode = ''
                        jobe:SecondHandsetRepValue = 0
                        Access:JOBSE.Update()
                        ! End (DBH 08/12/2005) #6644

                        ! Start - Pick Product Code if ARC and setup in Manufacturer - TrkBs: 5365 (DBH: 27-04-2005)
                        If jobe:SecondExchangeNumber <> 0 And ~glo:WebJob
                            If man:UseProdCodesForEXC = True
                                ! Inserting (DBH 08/12/2005) #6644 - Reget exchange unit to make sure we are looking up the right manufacturer
                                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                xch:Ref_Number  = jobe:SecondExchangeNumber
                                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                    ! Found

                                Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                    ! Error
                                End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                ! End (DBH 08/12/2005) #6644
                                GlobalResponse    = RequestCancelled
                                GlobalRequest     = SelectRecord
                                glo:Select50      = xch:Model_Number
                                PickModelProductCodes
                                glo:Select50      = ''
                                If Globalresponse = RequestCompleted
                                    jobe:SecondExcProdCode = prd:ProductCode
                                    ! Inserting (DBH 07/12/2005) #6644 - Record Handset Replacement Value. Blank values if cancelled.
                                    jobe:SecondHandsetRepValue = prd:HandsetReplacementValue
                                Else
                                    jobe:SecondExcProdCode = ''
                                    jobe:SecondHandsetRepValue = 0
                                    ! End (DBH 07/12/2005) #6644
                                End
                                Access:JOBSE.TryUpdate()
                            End ! If man:UseProdCodesForEXC = True
                        End ! If job:Exchange_Unit_Number <> 0
                        ! End   - Pick Product Code is setup in Manufacturer - TrkBs: 5365 (DBH: 27-04-2005)

                Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                    !Error
                End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        Else !If jobe:SecondExchangeUnit <> 0

        End !If jobe:SecondExchangeUnit <> 0
    End !If local:DoExchange

    If local:DoReturn
        If local:DoRestock = False
            Local.AllocateExchangePart('RTS',1)
        End !If local:Restock = False

        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number  = local:SaveExchangeUnitNumber
        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Found
             If tmp:FaultyUnit
                xch:Available = 'INR'
                xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                Case Missive('Please book the replaced Exchange Unit as a new job.','ServiceBase 3g',|
                               'mexclam.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            Else !If tmp:FaultUnit
                If xch:available <> 'QAF'
                    xch:available = 'AVL'
                    xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                End!If xch:available <> 'QAF'

            End !If tmp:FaultUnit
            xch:job_number = ''
            access:exchange.update()

            if access:exchhist.primerecord() = level:benign
                exh:ref_number    = old_exchange_number#
                exh:date          = Today()
                exh:time          = Clock()
                exh:user          = use:user_code
                If tmp:FaultyUnit
                    exh:status        = 'FAULTY UNIT RE-STOCKED (2ND) FROM JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
                Else !If tmp:FaultyUnit
                    exh:status        = 'UNIT RE-STOCKED (2ND) FROM JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
                End !If tmp:FaultyUnit

                if access:exchhist.insert()
                    access:exchhist.cancelautoinc()
                end
            end!if access:exchhist.primerecord() = level:benign
        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End !If local:DoReturn
    Do GetExchangeDetails


    Display()
GetExchangeUnitDate     Routine
    tmp:ExchangeDate = 0
    !Look for the exact Exchange Unit entry
    !in case there has been more than one exchange -  (DBH: 24-07-2003)
    If job:Exchange_Unit_Number <> 0
        Save_aud_ID = Access:AUDIT.SaveFile()
        Access:AUDIT.ClearKey(aud:TypeActionKey)
        aud:Ref_Number = job:Ref_Number
        aud:Type       = 'EXC'
        aud:Action     = 'EXCHANGE UNIT ATTACHED TO JOB'
        Set(aud:TypeActionKey,aud:TypeActionKey)
        Loop
            If Access:AUDIT.NEXT()
               Break
            End !If
            If aud:Ref_Number <> job:Ref_Number      |
            Or aud:Type       <> 'EXC'      |
            Or aud:Action     <> 'EXCHANGE UNIT ATTACHED TO JOB'      |
                Then Break.  ! End If
            Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
            aud2:AUDRecordNumber = aud:Record_Number
            IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                If Instring(job:Exchange_Unit_Number,aud2:Notes,1,1)
                    tmp:ExchangeDate    = aud:Date
                    Break
                End !If Instring(job:Exchange_Unit_Number,aud:Notes,1,1)

            END ! IF
        End !Loop
        Access:AUDIT.RestoreFile(Save_aud_ID)
    End !If job:Exchange_Unit_Number <> 0

    tmp:2ndExchangeDate = 0
    !Look for the exact Exchange Unit entry
    !in case there has been more than one exchange -  (DBH: 24-07-2003)
    If jobe:SecondExchangeNumber <> 0
        Save_aud_ID = Access:AUDIT.SaveFile()
        Access:AUDIT.ClearKey(aud:TypeActionKey)
        aud:Ref_Number = job:Ref_Number
        aud:Type       = '2NE'
        aud:Action     = 'SECOND EXCHANGE UNIT ATTACHED TO JOB'
        Set(aud:TypeActionKey,aud:TypeActionKey)
        Loop
            If Access:AUDIT.NEXT()
               Break
            End !If
            If aud:Ref_Number <> job:Ref_Number      |
            Or aud:Type       <> '2NE'      |
            Or aud:Action     <> 'SECOND EXCHANGE UNIT ATTACHED TO JOB'      |
                Then Break.  ! End If
            Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
            aud2:AUDRecordNumber = aud:Record_Number
            IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                If Instring(jobe:SecondExchangeNumber,aud2:Notes,1,1)
                    tmp:2ndExchangeDate    = aud:Date
                    Break
                End !If Instring(job:Exchange_Unit_Number,aud:Notes,1,1)

            END ! IF
        End !Loop
        Access:AUDIT.RestoreFile(Save_aud_ID)
    End !If jobe:SecondExchangeNumber <> 0
HubRepair       Routine
!    Access:JOBSE.Clearkey(jobe:RefNumberKey)
!    jobe:RefNumber  = job:Ref_Number
!    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found
        jobe:HubRepair = 1
        jobe:HubRepairDate = Today()
        jobe:HubRepairTime = Clock()
        stanum# = Sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
        GetStatus(stanum#,0,'JOB')

        access:reptydef.clearkey(rtd:ManRepairTypeKey)
        rtd:Manufacturer = job:manufacturer
        set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
        loop
           if access:reptydef.next() then break.
           if rtd:Manufacturer <> job:manufacturer then break.
           if rtd:BER = 10 then
               !we have an exchange fee
               if (job:Chargeable_Job = 'YES' and job:Repair_Type = '') then job:Repair_Type = rtd:Repair_Type.
               if (job:Warranty_Job = 'YES' And job:Repair_Type_Warranty = '') |
                    then job:Repair_Type_Warranty = rtd:Repair_Type.
               break
           end !if ber = 10
        End!loop
        Access:JOBSE.Update()

        !Lookup current engineer and mark as escalated
        Save_joe_ID = Access:JOBSENG.SaveFile()
        Access:JOBSENG.ClearKey(joe:UserCodeKey)
        joe:JobNumber     = job:Ref_Number
        joe:UserCode      = job:Engineer
        joe:DateAllocated = Today()
        Set(joe:UserCodeKey,joe:UserCodeKey)
        Loop
            If Access:JOBSENG.PREVIOUS()
               Break
            End !If
            If joe:JobNumber     <> job:Ref_Number       |
            Or joe:UserCode      <> job:Engineer      |
            Or joe:DateAllocated > Today()       |
                Then Break.  ! End If
            joe:Status = 'HUB'
            joe:StatusDate = Today()
            joe:StatusTime = Clock()
            Access:JOBSENG.Update()
            Break
        End !Loop
        Access:JOBSENG.RestoreFile(Save_joe_ID)

!    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!        !Error
!    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
CountParts      Routine
    PartsCounter = 0
    Count# = 0
    Save_par_ID = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        Count# += 1
        If par:Part_Number <> 'EXCH'
            PartsCounter += 1
        End !If par:Part_Number <> 'EXCH'

    End !Loop
    Access:PARTS.RestoreFile(Save_par_ID)


    Count# = 0
    WarrantyPartsCounter = 0
    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        Count# += 1
        If wpr:Part_Number <> 'EXCH'
            WarrantyPartsCounter += 1
        End !If par:Part_Number <> 'EXCH'
    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)

CityLinkService        Routine
    Disable(?job:excservice)

    access:courier.clearkey(cou:courier_key)
    cou:courier = job:exchange_courier
    If access:courier.tryfetch(cou:courier_key) = Level:Benign
        If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
            check_access('AMEND CITY LINK SERVICE',x")
            if x" = true
                Enable(?job:excservice)
            End!if x" = true
        End!If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
    End!If access:courier.clearkey(cou:courier_key) = Level:Benign

UpdateAccessories       Routine
    Count# = 0
    save_jea_id = access:jobexacc.savefile()
    access:jobexacc.clearkey(jea:part_number_key)
    jea:job_ref_number = job:Ref_number
    set(jea:part_number_key,jea:part_number_key)
    loop
        if access:jobexacc.next()
           break
        end !if
        if jea:job_ref_number <> job:ref_number      |
            then break.  ! end if
        count# += 1
        tmp:accessories = Clip(jea:part_number) & '-' & Clip(jea:description)
    end !loop
    access:jobexacc.restorefile(save_jea_id)

    If Count# > 1
        tmp:accessories = Count#
    End !If Count# > 1
    If Count# = 0
        tmp:Accessories = ''
    End !If Count# = 0
    Display()
Count_accessories       Routine
    exchange_accessory_count_temp = 0
    save_jea_id = access:jobexacc.savefile()
    access:jobexacc.clearkey(jea:part_number_key)
    jea:job_ref_number = job:Ref_number
    set(jea:part_number_key,jea:part_number_key)
    loop
        if access:jobexacc.next()
           break
        end !if
        if jea:job_ref_number <> job:ref_number      |
            then break.  ! end if
        exchange_accessory_count_temp += 1
    end !loop
    access:jobexacc.restorefile(save_jea_id)
GetExchangeDetails      Routine
    tmp:IMEI    = ''
    tmp:MSN     = ''
    tmp:ModelNumber = ''
    tmp:SiteLocation    = ''
    tmp:StockType = ''
    If job:Exchange_Unit_Number <> ''
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number  = job:Exchange_Unit_Number
        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Found
            tmp:IMEI    = xch:ESN
            tmp:MSN     = xch:MSN
            tmp:ModelNumber = xch:Model_Number
            tmp:SiteLocation    = xch:Location
            tmp:StockType   = xch:Stock_Type
        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign

        ! Get the Handset Part Number, if required - TrkBs: 5365 (DBH: 27-04-2005)
        Access:MANUFACT.Clearkey(man:Manufacturer_Key)
        man:Manufacturer    = xch:Manufacturer
        If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
            ! Found
            If man:UseProdCodesForEXC = True
                ?tmp:ProductCode{prop:Hide} = False
                ?tmp:ProductCode:Prompt{prop:Hide} = False
                tmp:ProductCode = jobe:ExchangeProductCode
                ! Inserting (DBH 07/12/2005) #6644 - Show part no button
                tmp:HandsetReplacementValue = jobe:HandsetReplacmentValue
                ?tmp:HandsetReplacementValue{prop:Hide} = False
                ?tmp:HandsetReplacementValue:Prompt{prop:Hide} = False
                If ~glo:WebJob
                    ! Only the ARC can attached a Handset Part No - TrkBs: 6644 (DBH: 08-12-2005)
                    ?Button:SelectHandsetPartNo{prop:Hide} = False
                End ! If ~glo:WebJob
                ! End (DBH 07/12/2005) #6644
            Else ! If man:UseProdCodesForEXC = True
                ?tmp:ProductCode{prop:Hide} = True
                ?tmp:ProductCode:Prompt{prop:Hide} = True
                ?Button:SelectHandsetPartNo{prop:Hide} = True
                ?tmp:HandsetReplacementValue{prop:Hide} = True
                ?tmp:HandsetReplacementValue:Prompt{prop:Hide} = True
            End ! If man:UseProdCodesForEXC = True
        Else ! If Access:MANUFACTURER.Tryfetch(man:Manufacturer_Key) = Level:Benign
            ! Error
        End ! If Access:MANUFACTURER.Tryfetch(man:Manufacturer_Key) = Level:Benign
    Else ! If job:Exchange_Unit_Number <> ''
        ?tmp:ProductCode{prop:Hide} = True
        ?tmp:ProductCode:Prompt{prop:Hide} = True
        ?Button:SelectHandsetPartNo{Prop:Hide} = True
        ?tmp:HandsetReplacementValue{prop:Hide} = True
        ?tmp:HandsetReplacementValue:Prompt{prop:Hide} = True
    End !If job:Exchange_Unit_Number <> ''
    Access:MODELNUM.ClearKey(mod:Model_Number_Key)
    mod:Model_Number = tmp:ModelNumber
    If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
        !Found
        tmp:ReplacementValue    = mod:ReplacementValue
    Else!If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
        !Error
        tmp:ReplacementValue = 0
    End!If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
    If MSNRequired(mod:Manufacturer) = Level:Benign
        ?job:msn{prop:Hide} = 0
        ?job:MSN:Prompt{prop:Hide} = 0
        ?tmp:MSN{prop:Hide} = 0
        ?tmp:MSN:Prompt{prop:Hide} = 0
    Else !If MSNRequired(job:Manufacturer)
        ?job:msn{prop:Hide} = 1
        ?job:MSN:Prompt{prop:Hide} = 1
        ?tmp:MSN{prop:Hide} = 1
        ?tmp:MSN:Prompt{prop:Hide} = 1
    End !If MSNRequired(job:Manufacturer)

    tmp:2ndExchangeDate     = ''
    tmp:2ndIMEI             = ''
    tmp:2ndMSN              = ''
    tmp:2ndModelNumber      = ''
    tmp:2ndAccessories      = ''
    tmp:2ndSiteLocation     = ''
    tmp:2ndStockType        = ''
    tmp:2ndReplacementValue = ''

    If jobe:SecondExchangeNumber <> ''
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number  = jobe:SecondExchangeNumber
        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Found
            tmp:2ndIMEI = xch:ESN
            tmp:2ndMSN  = xch:MSN
            tmp:2ndModelNumber = xch:Model_Number
            tmp:2ndSiteLocation = xch:Location
            tmp:2ndStockType    = xch:Stock_Type
        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign

        ! Get the Handset Part Number, if required - TrkBs: 5365 (DBH: 27-04-2005)
        Access:MANUFACT.Clearkey(man:Manufacturer_Key)
        man:Manufacturer    = xch:Manufacturer
        If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
            ! Found
            If man:UseProdCodesForEXC = True
                ?tmp:SecondProductCode{prop:Hide} = False
                ?tmp:SecondProductCode:Prompt{prop:Hide} = False
                tmp:SecondProductCode = jobe:SecondExcProdCode
                ! Inserting (DBH 07/12/2005) #6644 - Show part number button
                tmp:SecondHandsetReplacementValue = jobe:SecondHandsetRepValue
                ?tmp:SecondHandsetReplacementValue{prop:Hide} = False
                ?tmp:SecondHandsetReplacementValue:Prompt{prop:Hide} = False
                If ~glo:WebJob
                    ! Only the ARC can attached a Handset Part No - TrkBs: 6644 (DBH: 08-12-2005)
                    ?Button:SecondSelectHandsetPartNo{prop:Hide} = False
                End ! If ~glo:WebJob
                ! End (DBH 07/12/2005) #6644
            Else ! If man:UseProdCodesForEXC = True
                ?tmp:SecondProductCode{prop:Hide} = True
                ?tmp:SecondProductCode:Prompt{prop:Hide} = True
                ?Button:SecondSelectHandsetPartNo{prop:Hide} = True
                ?tmp:SecondHandsetReplacementValue{prop:Hide} = True
                ?tmp:SecondHandsetReplacementValue:Prompt{prop:Hide} = True
            End ! If man:UseProdCodesForEXC = True
        Else ! If Access:MANUFACTURER.Tryfetch(man:Manufacturer_Key) = Level:Benign
            ! Error
        End ! If Access:MANUFACTURER.Tryfetch(man:Manufacturer_Key) = Level:Benign
    Else !If jobe:SecondExchangeNumber <> ''
        ?tmp:SecondProductCode{prop:Hide} = True
        ?tmp:SecondProductCode:Prompt{prop:Hide} = True
        ?Button:SecondSelectHandsetPartNo{prop:Hide} = True
        ?tmp:SecondHandsetReplacementValue{prop:Hide} = True
        ?tmp:SecondHandsetReplacementValue:Prompt{prop:Hide} = True
    End !If jobe:SecondExchangeNumber <> ''

    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = tmp:2ndModelNumber
    If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Found
        tmp:2ndReplacementValue = mod:ReplacementValue
    Else ! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Error
        tmp:2ndReplacementValue = 0
    End !If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
    If MSNRequired(mod:Manufacturer) = Level:Benign
        ?tmp:2ndMSN{prop:Hide} = 0
        ?tmp:2ndMSN:Prompt{prop:Hide} = 0
    Else !If MSNRequired(job:Manufacturer)
        ?tmp:2ndMSN{prop:Hide} = 1
        ?tmp:2ndMSN:Prompt{prop:Hide} = 1
    End !If MSNRequired(job:Manufacturer)


    !Only allow the ARC to attach a second unit
    !and only after the first one has been attached -  (DBH: 16-12-2003)
    If glo:WebJob
        ?Sheet3{prop:Disable} = 1
    Else !If glo:WebJob
        If job:Exchange_Unit_Number = 0
            If jobe:SecondExchangeNumber = 0
                ?Sheet3{prop:Disable} = 1
            Else !If job:Exchange_Unit_Number = 0
                ?Sheet3{prop:Disable} = 0
            End !If job:Exchange_Unit_Number = 0
        Else !If job:Exchange_Unit_Number = 0
            ?Sheet3{prop:Disable} = 0
        End !If job:Exchange_Unit_Number = 0
    End !If glo:WebJob
    Do GetExchangeUnitDate
    Display()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020592'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ViewExchangeUnit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:AUDIT.Open
  Relate:CITYSERV.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:EXCHOR48.Open
  Relate:JOBEXACC.Open
  Relate:JOBSE3.Open
  Relate:LOCATION_ALIAS.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:COURIER.UseFile
  Access:STOCK.UseFile
  Access:STOMODEL.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:LOCATION.UseFile
  Access:USERS.UseFile
  Access:JOBSE.UseFile
  Access:REPTYDEF.UseFile
  Access:JOBSENG.UseFile
  Access:MODELNUM.UseFile
  Access:MANUFACT.UseFile
  Access:PRODCODE.UseFile
  Access:AUDIT2.UseFile
  Access:LOCATLOG.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  Do CityLinkService
  Do UpdateAccessories
  Do GetExchangeDetails
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If glo:WebJob
      tra:Account_Number =  Clarionet:Global.Param2
  Else !glo:WebJob
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  End !glo:WebJob
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:CurrentLocation = tra:SiteLocation
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  
  
  If SecurityCheck('JOBS - AMEND DESPATCH') = Level:Benign  And job:Exchange_Consignment_Number <> ''
      ?AmendDespatchDetails{prop:Hide} = 0
  End !SecurityCheck('JOBS - AMEND DESPATCH') = Level:Benign
  
  
  tmp:ModelNumberDetails = job:Model_Number
  If job:Colour <> ''
      tmp:ModelNumberDetails = Clip(tmp:ModelNumberDetails) & ' - ' & Clip(job:Colour)
  End !job:Colour <> ''
  
  ! Deleted (DBH 08/12/2005) #6644 - This is not needed here.
  !if job:Date_Completed <> ''
  !    if SecurityCheck('AMEND COMPLETED JOBS')
  !        !Job has been completed but they have no access to change it!
  !        !So check they CAN do it if it's invoiced!
  !        if job:Invoice_Date <> ''
  !            if SecurityCheck('AMEND INVOICED JOBS')
  !                !invoiced and they have no access
  !                disable(?PickExchangeUnit)
  !                disable(?NoAvailableUnit)
  !                disable(?PickAccessory)
  !                Disable(?PickSecondExchangeUnit)
  !            end
  !        else
  !            ! Not invoiced, so assume they cannot amend
  !            disable(?PickExchangeUnit)
  !            disable(?NoAvailableUnit)
  !            disable(?PickAccessory)
  !            Disable(?PickSecondExchangeUnit)
  !        end
  !    end
  !end
  ! End (DBH 08/12/2005) #6644
  
  
  
  If job:Warranty_Job = 'YES'
      ?job:Warranty_Charge_Type{prop:Hide} = 0
      ?WChargeType:Prompt{prop:Hide} = 0
  End !job:Warranty_Job <> 'YES'
  
  If job:Chargeable_Job = 'YES'
      ?job:Charge_Type{prop:Hide} = 0
      ?CChargeType:Prompt{prop:Hide} = 0
  End !job:Chargeable_Job = 'YES'
  SET(Defaults)
  Access:Defaults.Next()
  If def:Force_Outoing_Courier = 'B' OR def:Force_Outoing_Courier = 'C'
    ?job:Exchange_Courier{Prop:Req} = TRUE
  END
  
  Do GetExchangeUnitDate
  
  Bryan.CompFieldColour()
  
  
  !View Only
  ! Inserting (DBH 06/12/2006) # 8559 - Check the user is from the same RRC
  Error# = 0
  Access:USERS.ClearKey(use:Password_Key)
  use:Password = glo:Password
  If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Found
  Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Error
  End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = wob:HeadAccountNumber
  If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      !Found
  Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  If glo:WebJob
      If use:Location <> tra:SiteLocation
          Error# = 1
          Do MakeScreenViewOnly
      End ! If use:Location <> tra:SiteLocation
  End ! If glo:WebJob
  ! End (DBH 06/12/2006) #8559
  
  If Error# = 0
      If job:Engineer <> ''
          Access:USERS.ClearKey(use:User_Code_Key)
          use:User_Code = job:Engineer
          If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
              !Found
          Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
              !Error
          End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      End ! If job:Engineer <> ''
  
      If job:Date_Completed <> ''
          ! Inserting (DBH 08/12/2005) #6644 - Allow user to amend exchanges if they have access
          If SecurityCheck('AMEND EXCHANGE ON COMPLETE JOB')
              Do MakeScreenViewOnly
          End ! If SecurityCheck('AMEND EXCHANGE ON COMPLETE JOB')
          ! End (DBH 08/12/2005) #6644
      Else ! If job:Date_Completed <> ''
          If glo:Preview <> 'V'
              !If not view only, then see if user got access to bypass
              !stock allocation
              If SecurityCheck('EXCHANGE ALLOCATE OPTIONS MENU') = Level:Benign
                  glo:Preview = 'R'
              End !If SecurityCheck('EXCHANGE ALLOCATION OPTIONS MENU')
          End !glo:Preview <> 'V'
  
          If glo:Preview <> 'R' !Will = R if called from Rapid Stock
              If glo:Preview = 'V' or (RapidLocation(tra:SiteLocation) And job:Exchange_Unit_Number = 0)
                  If glo:WebJob And jobe:Engineer48HourOption And job:Exchange_Unit_Number <> 0
  
                  Else !If glo:WebJob And jobe:Engineer48HourOption And job:Exchange_Unit_Number <> 0
                      Do MakeScreenViewOnly
                  End !If glo:WebJob And jobe:Engineer48HourOption And job:Exchange_Unit_Number <> 0
              End !glo:Select21
          End !glo:Preview <> 'R'
      End ! If job:Date_Completed <> ''
  End ! If Error# = 0
  ! Save Window Name
   AddToLog('Window','Open','ViewExchangeUnit')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?job:Exchange_Courier{Prop:Tip} AND ~?LookupExchangeCourier{Prop:Tip}
     ?LookupExchangeCourier{Prop:Tip} = 'Select ' & ?job:Exchange_Courier{Prop:Tip}
  END
  IF ?job:Exchange_Courier{Prop:Msg} AND ~?LookupExchangeCourier{Prop:Msg}
     ?LookupExchangeCourier{Prop:Msg} = 'Select ' & ?job:Exchange_Courier{Prop:Msg}
  END
  FDCB6.Init(job:ExcService,?job:ExcService,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:CITYSERV,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(cit:ServiceKey)
  FDCB6.AddField(cit:Service,FDCB6.Q.cit:Service)
  FDCB6.AddField(cit:RefNumber,FDCB6.Q.cit:RefNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:CITYSERV.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:EXCHOR48.Close
    Relate:JOBEXACC.Close
    Relate:JOBSE3.Close
    Relate:LOCATION_ALIAS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ViewExchangeUnit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickCouriers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020592'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020592'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020592'&'0')
      ***
    OF ?Button:SelectHandsetPartNo
      ThisWindow.Update
      ! Inserting (DBH 08/12/2005) #6644 - Replace the Handset Part Number
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          ! Found
          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
          xch:Ref_Number  = job:Exchange_Unit_Number
          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              ! Found
              GlobalResponse    = RequestCancelled
              GlobalRequest     = SelectRecord
              glo:Select50      = xch:Model_Number
              PickModelProductCodes
              glo:Select50      = ''
              If Globalresponse = RequestCompleted
                  jobe:ExchangeProductCode = prd:ProductCode
                  ! Inserting (DBH 08/12/2005) #6644 - Record Handset Replacement Value
                  jobe:HandsetReplacmentValue = prd:HandsetReplacementValue
                  ! End (DBH 08/12/2005) #6644
                  Access:JOBSE.TryUpdate()
                  Do GetExchangeDetails
              End ! If Globalresponse = RequestCompleted
          Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              ! Error
          End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          ! Error
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! End (DBH 08/12/2005) #6644
    OF ?PickExchangeUnit
      ThisWindow.Update
      Do PickExchangeUnit
    OF ?PickAccessory
      ThisWindow.Update
      If SecurityCheck('JOBS - PICK ACCESSORY')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Error# = 1
      Else
          glo:select12 = job:model_Number
          Do Count_accessories
          accessory_count_temp = exchange_accessory_count_temp
      
          Browse_Job_Exchange_Accessory(job:ref_Number)
      
          glo:select12 = ''
          error# = 0
          If job:despatch_type = 'LOA' And job:despatched = 'REA'
              error# = 1
          End
          If error# = 0
              Do Count_accessories
              If exchange_accessory_count_temp > accessory_count_temp
                  job:exchange_status = 'AWAITING DESPATCH'
                  job:despatched = 'REA'
                  job:despatch_type = 'EXC'
                  job:exchange_user = exh:user
                  job:current_courier = job:exchange_courier
                  GetStatus(110,thiswindow.request,'EXC') !depatch exchange unit
              End!If exchange_accessory_count_temp > accessory_count_temp
      
          End!If error# = 1
          Do UpdateAccessories
      
      End !If SecurityCheck('JOBS - AMEND EXCHANGE UNIT')
      
    OF ?NoAvailableUnit
      ThisWindow.Update
      If job:Exchange_Unit_Number <> 0
          Case Missive('There is an exchange unit attached to this job.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !job:Exchange_Unit_Number <> 0
          !Change Exchange Status
          !And this means it will appear in the Stock Allocation list
          GetStatus(350,0,'EXC') !Exchange Order Required
          GetStatus(355,0,'JOB') !Exchange Requested
          Local.AllocateExchangePart('ORD',0)
      
      
          Post(Event:Accepted,?OK)
      End !job:Exchange_Unit_Number <> 0
    OF ?job:Exchange_Courier
      IF job:Exchange_Courier OR ?job:Exchange_Courier{Prop:Req}
        cou:Courier = job:Exchange_Courier
        !Save Lookup Field Incase Of error
        look:job:Exchange_Courier        = job:Exchange_Courier
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Exchange_Courier = cou:Courier
          ELSE
            !Restore Lookup On Error
            job:Exchange_Courier = look:job:Exchange_Courier
            SELECT(?job:Exchange_Courier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      !Write the courier to webjobs again, if the job is in the despatch table - 4001 (DBH: 04-03-2004)
      If glo:WebJob and jobe:DespatchType = 'EXC'
          save_WEBJOB_id = Access:WEBJOB.SaveFile()
          Access:WEBJOB.Clearkey(wob:RefNumberKey)
          wob:RefNumber   = job:Ref_Number
          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
              !Found
              If wob:ReadyToDespatch = 1
                  wob:DespatchCourier = job:Exchange_Courier
                  Access:WEBJOB.TryUpdate()
              End !If wob:ReadyToDespatch = 1
          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
              !Error
          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          Access:WEBJOB.RestoreFile(save_WEBJOB_id)
      End !glo:WebJob
    OF ?LookupExchangeCourier
      ThisWindow.Update
      cou:Courier = job:Exchange_Courier
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Exchange_Courier = cou:Courier
          Select(?+1)
      ELSE
          Select(?job:Exchange_Courier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Exchange_Courier)
    OF ?AmendDespatchDetails
      ThisWindow.Update
      Case Missive('Are you sure you want to amend the Exchange Unit''s Despatch Details?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              ?job:Exchange_consignment_number{prop:readonly} = 0
              ?job:Exchange_consignment_number{prop:color} = color:yellow
              ?job:exchange_despatched{prop:readonly} = 0
              ?job:exchange_despatched{prop:color} = color:yellow
              ?job:exchange_despatched_user{prop:readonly} = 0
              ?job:exchange_despatched_user{prop:color} = color:yellow
              ?job:exchange_despatch_number{prop:readonly} = 0
              ?job:exchange_despatch_number{prop:color} = color:yellow
          Of 1 ! No Button
      End ! Case Missive
    OF ?Button:SecondSelectHandsetPartNo
      ThisWindow.Update
      ! Inserting (DBH 08/12/2005) #6644 - Replace the Handset Part Number
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          ! Found
          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
          xch:Ref_Number  = jobe:SecondExchangeNumber
          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              ! Found
              GlobalResponse    = RequestCancelled
              GlobalRequest     = SelectRecord
              glo:Select50      = xch:Model_Number
              PickModelProductCodes
              glo:Select50      = ''
              If Globalresponse = RequestCompleted
                  jobe:SecondExcProdCode = prd:ProductCode
                  ! Inserting (DBH 08/12/2005) #6644 - Record Handset Replacement Value
                  jobe:SecondHandsetRepValue = prd:HandsetReplacementValue
                  ! End (DBH 08/12/2005) #6644
                  Access:JOBSE.TryUpdate()
                  Do GetExchangeDetails
              End ! If Globalresponse = RequestCompleted
          Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              ! Error
          End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          ! Error
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! End (DBH 08/12/2005) #6644
    OF ?PickSecondExchangeUnit
      ThisWindow.Update
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
          Do PickSecondExchangeUnit
          Access:JOBSE.Update()
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.AllocateExchangePart        Procedure(String    func:Status,Byte func:SecondUnit)
local:FoundPart                 Byte(0)
Code
    If job:Warranty_Job = 'YES'
        Save_wpr_ID = Access:WARPARTS.SaveFile()
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = job:Ref_Number
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> job:Ref_Number      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            If func:SecondUnit
                If wpr:SecondExchangeUnit
                    local:FoundPart = True
                End !If wpr:SecondExchangeUnit
            Else !If func:SecondUnit
                local:FoundPart = True
            End !If func:SecondUnit
            If local:FoundPart = True
                wpr:Status = func:Status
                If func:Status = 'PIK'
                    wpr:PartAllocated = 1
                End !If func:Status = 'PIK'
                Access:WARPARTS.Update()
                !Remove the EXCH line from Stock Allocation,
                !just incase the Exchange isn't being added properly - L873 (DBH: 24-07-2003)
                RemoveFromStockAllocation(wpr:Record_Number,'WAR')
                Break
            End !If local:FoundPart = True
        End !Loop
        Access:WARPARTS.RestoreFile(Save_wpr_ID)
    End !If job:Warranty_Job = 'YES'


    If local:FoundPart = False
        If job:Chargeable_Job = 'YES'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit
                    If par:SecondExchangeUnit
                        local:FoundPart = True
                    End !If wpr:SecondExchangeUnit
                Else !If func:SecondUnit
                    local:FoundPart = True
                End !If func:SecondUnit
                If local:FoundPart = True
                    par:Status = func:Status
                    If func:Status = 'PIK'
                        par:PartAllocated = 1
                    End !If func:Status = 'PIK'
                    Access:PARTS.Update()
                    !Remove the EXCH line from Stock Allocation,
                    !just incase the Exchange isn't being added properly - L873 (DBH: 24-07-2003)
                    RemoveFromStockAllocation(par:Record_Number,'CHA')
                    Break
                End !If local:FoundPart = True

            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
        End !If job:Chargeable_Job = 'YES'
    End !If local:FounPart = False

    If local:FoundPart = False
        If job:Engineer = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = glo:Password
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = job:Engineer
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

        End !If job:Engineer = ''
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
!                    If ~loc:UseRapidStock
!                        Return
!                    End !If ~loc:UseRapidStock

            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If job:Warranty_Job = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = job:ref_number
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = Clip(job:Manufacturer) & ' EXCHANGE UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = func:Status
                   wpr:ExchangeUnit          = True
                   wpr:SecondExchangeUnit    = func:SecondUnit
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If job:Chargeable_Job = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = job:ref_number
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = Clip(job:Manufacturer) & ' EXCHANGE UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = func:Status
                        par:ExchangeUnit          = True
                        par:SecondExchangeUnit    = func:SecondUnit
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
            Case Missive('You must have a Part setup in Stock Control with a part number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If local:FounPart = False
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Fault_Description PROCEDURE                           !Generated from procedure template - Window

FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Fault Description'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Fault Description'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           BUTTON,AT(204,158),USE(?Lookup_Engineers_Notes),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           TEXT,AT(238,158,206,82),USE(jbn:Fault_Description),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting'
  OF ChangeRecord
    ActionMessage = 'Changing Fault Description'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020414'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Fault_Description')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:JOBNOTES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBNOTES.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBNOTES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      !TB13265 - J - 27/03/14 - don't allow them to change the Fault Description if in view only mode
      if glo:Preview = 'V' then ?OK{prop:disable} = true.
  ! Save Window Name
   AddToLog('Window','Open','Fault_Description')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Fault_Description')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Lookup_Engineers_Notes
      glo:select1 = job:manufacturer
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020414'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020414'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020414'&'0')
      ***
    OF ?Lookup_Engineers_Notes
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Fault_Description_Text
      ThisWindow.Reset
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If jbn:fault_description = ''
                  jbn:fault_description = Clip(glo:notes_pointer)
              Else !If job:engineers_notes = ''
                  jbn:fault_description = Clip(jbn:fault_description) & '. ' & Clip(glo:notes_pointer)
              End !If job:engineers_notes = ''
          End
          Display()
      End
      Clear(glo:Q_Notes)
      Free(glo:Q_Notes)
      glo:select1 = ''
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ViewLoanUnit PROCEDURE                                !Generated from procedure template - Window

save_lac_id          USHORT,AUTO
save_webjob_id       USHORT,AUTO
save_jobse_id        USHORT,AUTO
tmp:Accessories      STRING(255)
tmp:FaultyUnit       BYTE(0)
tmp:BERConsignmentNumber STRING(30)
Print_Label_Temp     STRING(3)
tmp:IMEI             STRING(30)
tmp:MSN              STRING(30)
tmp:ModelNumber      STRING(30)
tmp:ModelNumberDetails STRING(60)
tmp:Deposit          GROUP,PRE()
tmp:DepositAmount    REAL
tmp:DepositDate      DATE
                     END
tmp:Contact_History_Action STRING(80)
tmp:LoanLost         BYTE(0)
tmp:LoanValue        REAL
save:IDNumber        STRING(13)
TempDate             STRING(20)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?job:LoaService
cit:Service            LIKE(cit:Service)              !List box control field - type derived from field
cit:RefNumber          LIKE(cit:RefNumber)            !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(CITYSERV)
                       PROJECT(cit:Service)
                       PROJECT(cit:RefNumber)
                     END
window               WINDOW('View Loan Unit Details'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('View Loan Unit Details'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,52,552,310),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           BUTTON,AT(296,146),USE(?PickLoanUnit),TRN,FLAT,LEFT,ICON('pikloap.jpg')
                           BUTTON,AT(296,146),USE(?RestockLoanUnit),TRN,FLAT,ICON('qaloanp.jpg')
                           GROUP('Job Unit Details'),AT(68,56,536,80),USE(?Group3),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Loan I.D. Number'),AT(372,68),USE(?jobe2:IDNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s13),AT(448,68,124,10),USE(jobe2:LoanIDNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('I.D. Number'),TIP('I.D. Number'),REQ,UPR
                             PROMPT('I.M.E.I. Number:'),AT(72,68),USE(?Prompt12),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('M.S.N.:'),AT(72,80),USE(?job:MSN:Prompt),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s20),AT(148,68),USE(job:ESN),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s20),AT(148,80),USE(job:MSN),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Model Number:'),AT(72,92),USE(?Prompt12:3),FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s60),AT(148,92),USE(tmp:ModelNumberDetails),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Char Charge Type:'),AT(72,104),USE(?CChargeType:Prompt),HIDE,FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s60),AT(148,104),USE(job:Charge_Type),HIDE,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(148,116),USE(job:Warranty_Charge_Type),HIDE,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Warr Charge Type:'),AT(72,116),USE(?WChargeType:Prompt),HIDE,FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           GROUP('Loan Unit Details'),AT(68,140,296,92),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Loan Unit No:'),AT(72,152),USE(?Prompt5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('I.M.E.I. Number:'),AT(72,164),USE(?IMEI),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(148,164),USE(tmp:IMEI),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s8b),AT(148,152),USE(job:Loan_Unit_Number),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(148,176),USE(tmp:MSN),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(148,188),USE(tmp:ModelNumber),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Accessories:'),AT(72,200),USE(?IMEI:4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s255),AT(148,200,144,12),USE(tmp:Accessories),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             BUTTON,AT(296,196),USE(?BrowseLoanAccessories),SKIP,TRN,FLAT,LEFT,ICON('lookupp.jpg')
                             PROMPT('Replacement Value'),AT(72,216),USE(?tmp:LoanValue:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n14.2),AT(148,216,64,10),USE(tmp:LoanValue),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Replacement Value'),TIP('Replacement Value'),UPR
                             PROMPT('Model Number:'),AT(72,188),USE(?IMEI:3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('M.S.N.:'),AT(72,176),USE(?tmp:MSN:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           GROUP,AT(68,234,244,44),USE(?DepositGroup),TRN
                             PROMPT('Deposit Paid'),AT(76,238),USE(?Deposit:Prompt),FONT('Tahoma',12,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@n10.2),AT(156,238),USE(tmp:DepositAmount),RIGHT,FONT('Tahoma',12,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@d6),AT(76,252),USE(tmp:DepositDate),RIGHT,FONT('Tahoma',12,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           BUTTON,AT(68,280),USE(?Payment_Details),TRN,FLAT,LEFT,ICON('paydetp.jpg')
                           BUTTON,AT(139,280),USE(?SendSMSButton),TRN,FLAT,ICON('SndSMSp.jpg')
                           BUTTON,AT(214,280),USE(?SMSHistButton),TRN,FLAT,ICON('SMSHistP.jpg')
                           GROUP('Despatch Details'),AT(368,140,240,92),USE(?DespatchDetails),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Loan Courier'),AT(372,152),USE(?job:Exchange_Courier:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(448,152,124,10),USE(job:Loan_Courier),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             BUTTON,AT(576,148),USE(?LookupExchangeCourier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Service'),AT(372,164),USE(?job:Exchange_Courier:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             COMBO(@s20),AT(448,164,64,10),USE(job:LoaService),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('4L(2)|M@s1@'),DROP(10,64),FROM(Queue:FileDropCombo),MSG('Loan Service For City Link / LabelG')
                             PROMPT('Consignment No'),AT(372,180),USE(?job:Exchange_Consignment_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(448,180,124,10),USE(job:Loan_Consignment_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                             PROMPT('Despatched'),AT(372,196),USE(?job:Exchange_Despatched:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@d6b),AT(448,196,64,10),USE(job:Loan_Despatched),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                             PROMPT('User'),AT(372,208),USE(?job:Exchange_Despatched_User:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s3),AT(448,208,64,10),USE(job:Loan_Despatched_User),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                             BUTTON,AT(540,202),USE(?AmendDespatchDetails),TRN,FLAT,LEFT,ICON('edtdespp.jpg')
                             PROMPT('Despatch Number'),AT(372,220),USE(?job:Exchange_Despatch_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s8),AT(448,220,64,10),USE(job:Loan_Despatch_Number),SKIP,LEFT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           END
                         END
                       END
                       BUTTON,AT(548,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:job:Loan_Courier                Like(job:Loan_Courier)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

PickLoanUnit        Routine
    DATA
locAuditNotes   STRING(255)
locAction STRING(100)
    CODE
    !Security Check
    tmp:LoanLost = 0
    Error# = 0
    If job:Loan_Unit_Number <> ''
        If SecurityCheck('JOBS - AMEND LOAN UNIT')
            Case Missive('You do not have access to this option.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Error# = 1
        End !If SecurityCheck('JOBS - AMEND EXCHANGE UNIT')
    Else !If job:Exchange_Unit_Number <> ''
        If SecurityCheck('JOBS - ADD LOAN UNIT')
            Case Missive('You do not have access to this option.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Error# = 1
        End !If SecurityCheck('JOBS - ADD EXCHANGE UNIT')
    End !If job:Exchange_Unit_Number <> ''

    If Error# = 0

        glo:select12 = job:Model_number
        Set(defaults)
        access:defaults.next()
        error# = 0

        remove# = 0

        If error# = 0
            access:subtracc.clearkey(sub:account_number_key)
            sub:account_number = job:account_number
            access:subtracc.fetch(sub:account_number_key)
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            access:tradeacc.fetch(tra:account_number_key)
            If tra:Ignoredespatch = 'YES' and error# = 0
                Case Missive('The selected Trade Account has been marked as "Skip Despatch".'&|
                  '<13,10>'&|
                  '<13,10>A Loan Unit cannot be issued.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                error# = 1
            End!If tra:skip_despatch = 'YES'
        End!If error# = 0

        If error# = 0
            do_loan# = 1
            do_return# = 0
            old_loan_number# = job:loan_unit_number
            If job:loan_unit_number <> ''
                do_return# = 1
                glo:select2  = 'L'
                glo:select1 = ''
                Loan_Changing_Screen

                If glo:Select1 <> ''

                    If job:loan_consignment_number <> '' And Error# = 0
                        Case Missive('Warning! The Loan Unit for this job has already been despatched. If you continue the existing unit on this job will be removed and returned to Loan Stock.'&|
                          '<13,10>(An Entry will be made in the Audit Trail)'&|
                          '<13,10>Are you sure you want to remove the Loan Unit?','ServiceBase 3g',|
                                       'mquest.jpg','\No|/Yes')
                            Of 2 ! Yes Button
                                remove# = 1
                            Of 1 ! No Button
                                error# = 1
                        End ! Case Missive
                    End!If job:loan_consingment_number <> ''
                Else !If glo:Select1 <> ''
                    Error# = 1
                    !Code is still restocking the unit, even if cancel
                    !is pressed - L904 (DBH: 05-08-2003)
                    Exit

                End !If glo:Select1 <> ''


                If Error# = 0
                        If remove# = 1
                            locAuditNotes         = 'UNIT HAD BEEN DESPATCHED: ' & |
                                                '<13,10>UNIT NUMBER: ' & Clip(job:Loan_unit_number) & |
                                                '<13,10>COURIER: ' & CLip(job:loan_Courier) & |
                                                '<13,10>CONSIGNMENT NUMBER: ' & CLip(job:loan_consignment_number) & |
                                                '<13,10>DATE DESPATCHED: ' & Clip(Format(job:loan_despatched,@d6)) &|
                                                '<13,10>DESPATCH USER: ' & CLip(job:loan_despatched_user) &|
                                                '<13,10>DESPATCH NUMBER: ' & CLip(job:loan_despatch_number)
                        Else!If remove# = 1
                            locAuditNotes       = 'UNIT NUMBER: ' & CLip(job:loan_unit_Number)
                        End!If remove# = 1



                        Case glo:select1
                            Of 1
                                locAction    = 'REPLACED FAULTY LOAN'
                                GetStatus(101,1,'LOA')
                            Of 2
                                locAction    = 'ALTERNATVIE MODEL REQUIRED'
                                GetStatus(101,1,'LOA')
                            Of 3 OrOf 4
                                If glo:Select1 = 3
                                    locAction    = 'RETURN LOAN: RE-STOCKED'
                                    Loop
                                        If AccessoryCheck('LOAN')
                                            Case Missive('Do you wish to RE-ENTER the Accessories, CHARGE the customer for the mismatch, or IGNORE the mismatch?','ServiceBase 3g',|
                                                           'mquest.jpg','\Cancel|Ignore|Charge|Re-Enter')
                                                Of 4 ! Re-Enter Button
                                                Of 3 ! Charge Button
                                                    job:Courier_Cost += InsertLoanCharge()
                                                    locAction = Clip(locAction) & ' - ACCESSORY MISMATCH. CUSTOMER CHARGED'
                                                    Break
                                                Of 2 ! Ignore Button
                                                    locAction = Clip(locAction) & ' - ACCESSORY MISMATCH. IGNORED'
                                                    Break
                                                Of 1 ! Cancel Button
                                                    Access:AUDIT.CancelAutoInc()
                                                    EXIT
                                            End ! Case Missive
                                        Else
                                            Break
                                        End !If AccessoryCheck('LOAN')
                                    End !Loop

                                Else !If glo:Select1 = 3
                                    locAction    = 'RETURN LOAN: LOAN LOST'
                                    tmp:LoanLost = 1
                                End !If glo:Select1 = 3

                                do Loan:Return:Contact_History
                                ! Change loan status to 816 RETURN TO STOCK - TrkBs: 6570 (DBH: 31-10-2005)
                                GetStatus(816,1,'LOA')  ! Previously 101

!                                job:loan_unit_number = ''
!                                job:loan_accessory = ''
!                                job:loan_courier            =''
!                                job:loan_consignment_number =''
!                                job:loan_despatched         =''
!                                job:loan_despatched_user    =''
!                                job:loan_despatch_number    =''
!                                job:Loaservice              =''
!                                job:loan_issued_date = ''
!                                job:loan_user        = ''
!                                If job:despatch_type = 'LOA'
!                                    job:despatched = 'NO'
!                                    job:despatch_type = ''
!                                End
                                do_loan# = 0
                            Else
                                do_loan# = 0
                                do_return# = 0
                                GetStatus(101,1,'LOA')
                        End!Case glo:select1

                        IF (AddToAudit(job:Ref_Number,'LOA',locAction,locAuditNotes))
                        END ! IF

                        ! If job status is at "READY TO DESPATCH (LOAN)", revert to "READY TO DESPATCH" - TrkBs: 6570 (DBH: 31-10-2005)
                        If Sub(job:Current_Status,1,3) = '811'
                            GetStatus(810,1,'JOB')
                        End !If Sub(job:Current_Status,1,3) = '811'

                        !Remove the job from the despatch table.
                        !This is in case the user has picked "Replace" but
                        !then doesn't select a new unit. - 4485 (DBH: 20-07-2004)
                        job:loan_unit_number = ''
                        job:loan_accessory = ''
                        job:loan_consignment_number =''
                        job:loan_despatched         =''
                        job:loan_despatched_user    =''
                        job:loan_despatch_number    =''
                        job:Loaservice              =''
                        job:loan_issued_date        = ''
                        job:loan_user               = ''
                        !Remove from web, or fat despatch table - 4485 (DBH: 20-07-2004)
                        If job:despatch_type = 'LOA'
                            job:despatched = 'NO'
                            job:despatch_type = ''
                        Else
                            save_JOBSE_id = Access:JOBSE.SaveFile()
                            Access:JOBSE.Clearkey(jobe:RefNumberKey)
                            jobe:RefNumber  = job:Ref_Number
                            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                !Found
                                If jobe:DespatchType = 'LOA'
                                    jobe:DespatchType = ''
                                    Access:JOBSE.RestoreFile(save_JOBSE_id)
                                    save_WEBJOB_id = Access:WEBJOB.SaveFile()
                                    Access:WEBJOB.Clearkey(wob:RefNumberKey)
                                    wob:RefNumber   = job:Ref_Number
                                    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                        !Found
                                        wob:ReadyToDespatch = 0
                                        Access:WEBJOB.TryUpdate()
                                    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                        !Error
                                    End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                    Access:WEBJOB.RestoreFile(save_WEBJOB_id)

                                End !If jobe:DespatchType = 'LOA'
                                Access:JOBSE.TryUpdate()
                            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                !Error
                            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        End

                    glo:select1 = ''
                    glo:select2 = ''
                Else
                    do_loan# = 0
                    do_Return# = 0
                End !If Error# = 0

            End!If job:loan_unit_number <> ''
            If do_loan# = 1
                If job:Exchange_unit_number <> '' And ((glo:WebJob = 1 And jobe:Despatched = 'REA' And jobe:DespatchType = 'EXC') Or |
                                                (glo:WebJob = 0 And job:Despatched = 'REA' and job:Despatch_Type = 'EXC'))
                    Case Missive('Cannot attach a Loan Unit. The Exchange Unit attached to this job has not been despatched.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive

                Else !If job:exchange_unit_number <> '' and job:Exchange_Despatched = ''

                    job:Loan_Unit_Number = SelectLoanExchangeUnit('LOA')
                    If job:Loan_Unit_Number <> ''
                        access:loan.clearkey(loa:ref_number_key)
                        loa:ref_number = job:loan_unit_number
                        if access:loan.fetch(loa:ref_number_key) = Level:Benign
                            continue# = 1
                            IF loa:model_number <> job:model_number
                                Case Missive('The selected Loan Unit has a Model Number of ' & Clip(loa:Model_Number) & '.'&|
                                  '<13,10>This is different from the Model Number of the job.'&|
                                  '<13,10>'&|
                                  '<13,10>Do you want to ignore this mismatch?','ServiceBase 3g',|
                                               'mquest.jpg','\Cancel|/Ignore')
                                    Of 2 ! Ignore Button
                                    Of 1 ! Cancel Button
                                        job:loan_unit_number    = ''
                                        continue# = 0
                                End ! Case Missive
                            End!IF loa:model_number <> job:model_number
                            If continue# = 1
                                Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                                man:Manufacturer = job:Manufacturer
                                If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                    !Found

                                    If man:QALoan
                                        loa:available   = 'QA1'
                                    Else!If def:qaexchloan = 'YES'
                                        loa:available   = 'LOA'
                                    End!If def:qaexchloan = 'YES'
                                    loa:job_number    = job:ref_number
                                    !Paul 02/06/2009 Log No 10684
                                    loa:StatusChangeDate = today()
                                    access:loan.update()
                                    get(loanhist,0)
                                    if access:loanhist.primerecord() = level:benign
                                        loh:ref_number    = loa:ref_number
                                        loh:date          = Today()
                                        loh:time          = Clock()
                                        access:users.clearkey(use:password_key)
                                        use:password    =glo:password
                                        access:users.fetch(use:password_key)
                                        loh:user          = use:user_code
                                        JOB:Loan_User     = loh:user
                                        If def:qaexchloan = 'YES'
                                            loh:status  = 'AWAITING QA. LOANED ON JOB NO: ' & Clip(job:ref_number)
                                        Else
                                            loh:status        = 'UNIT LOANED ON JOB NO: ' & clip(job:ref_number)
                                        End!If def:qaexchloan = 'YES'
                                        if access:loanhist.insert()
                                            access:loanhist.cancelautoinc()
                                        end

                                        IF (AddToAudit(job:Ref_Number,'LOA','LOAN UNIT ATTACHED TO JOB','UNIT NUMBER: ' & Clip(loa:ref_number) & |
                                                                '<13,10>MODEL NUMBER: ' & CLip(loa:model_number) & |
                                                                '<13,10>E.S.N: ' & CLip(loa:esn)))
                                        END ! IF

                                        do Loan:Issued:Contact_History

                                        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                                        mod:Model_Number    = loa:Model_Number
                                        If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                            !Found

                                        Else ! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                            !Error
                                        End !If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign

                                        Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                        jobe:RefNumber  = job:Ref_Number
                                        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                            !Found
                                            tmp:LoanValue   = mod:LoanReplacementValue
                                            jobe:LoanReplacementValue   = mod:LoanReplacementValue
                                            Access:JOBSE.Update()
                                        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                            !Error
                                        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                                        If man:QALoan
                                            job:despatched = ''
                                            job:despatch_type = ''
                                            GetStatus(605,thiswindow.request,'LOA') !QA Required

                                        Else!If def:qaexchloan   = 'YES'
                                            If glo:webJob
                                                jobe:DespatchType = 'LOA'
                                                jobe:Despatched = 'REA'
                                                Access:JOBSE.Update()

                                                save_WEBJOB_id = Access:WEBJOB.SaveFile()
                                                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                                                wob:RefNumber   = job:Ref_Number
                                                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                    !Found
                                                    wob:ReadyToDespatch = 1
                                                    wob:DespatchCourier = job:Loan_Courier
                                                    Access:WEBJOB.TryUpdate()
                                                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                    !Error
                                                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                Access:WEBJOB.RestoreFile(save_WEBJOB_id)
                                            Else !If glo:webJob
                                                job:despatched = 'LAT'
                                                job:despatch_Type = 'LOA'
                                                job:loan_user   = loh:user
                                                job:current_courier = job:loan_courier
                                                access:courier.clearkey(cou:courier_key)
                                                cou:courier = job:Loan_courier
                                                access:courier.tryfetch(cou:courier_key)
                                                IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                                                    job:loaservice  = cou:service
                                                Else!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                                                    job:loaservice  = ''
                                                End!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'

                                                job:Loan_Despatched = DespatchANC(job:Loan_Courier,'LOA')

                                            End !If glo:webJob
                                            GetStatus(105,thiswindow.request,'LOA')

                                        End!If def:qaexchloan   = 'YES'

                                        If job:loan_courier = ''
                                            Case Missive('Warning! There is no Courier attached to this Loan Unit.'&|
                                              '<13,10>'&|
                                              '<13,10>It will not despatch correctly if you do not select one.','ServiceBase 3g',|
                                                           'mexclam.jpg','/OK')
                                                Of 1 ! OK Button
                                            End ! Case Missive
                                        End!If job:exchange_courier = ''


                                        Do CityLinkService

                                        Set(Defaults)
                                        access:defaults.next()
                                        If def:use_loan_exchange_label  = 'YES'
                                            glo:select1  = job:ref_number
                                            glo:select2  = job:loan_unit_number
                                            case def:label_printer_type
                                                Of 'TEC B-440 / B-442'
                                                    Thermal_Labels_Loan
                                                Of 'TEC B-452'
                                                    Thermal_Labels_Loan_B452
                                            End!case def:label_printer_type
                                            glo:select1  = ''
                                            glo:select2  = ''
                                        End!If def:use_loan_exchange_label  = 'YES'
                                    end!if access:exchhist.primerecord() = level:benign
                                Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                    !Error
                                    Case Missive('Error! Cannot find the Manufacturer attached to this job.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                        Of 1 ! OK Button
                                    End ! Case Missive
                                End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

                            End!If continue# = 1
                        end!if access:loan.fetch(loa:ref_number_key) = Level:Benign
                    End!If glo:select1 <> ''
                    globalrequest = saverequest#
                End !If job:exchange_unit_number <> '' and job:Exchange_Despatched = ''
            End!If do_Loan# = 1
            If do_return# = 1
                access:loan.clearkey(loa:ref_number_key)
                loa:ref_number = old_loan_number#
                if access:loan.fetch(loa:ref_number_key) = Level:Benign
                    If tmp:LoanLost
                        loa:Available = 'LOS'
                    Else !If tmp:LoanLost
                        loa:available = 'AVL'
                    End !If tmp:LoanLost
                    !Paul 02/06/2009 Log No 10684
                    loa:StatusChangeDate = today()
                    loa:job_number = ''
                    access:loan.update()
                    get(loanhist,0)
                    if access:loanhist.primerecord() = level:benign
                        loh:ref_number    = old_loan_number#
                        loh:date          = Today()
                        loh:time          = Clock()
                        loh:user          = use:user_code
                        loh:status        = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
                        if access:loanhist.insert()
                            access:loanhist.cancelautoinc()
                        end
                    end!if access:exchhist.primerecord() = level:benign
                end!if access:loan.fetch(loa:ref_number_key)
            End!If do_return# = 1
        End!If error# = 0
        glo:select12 = ''
    End !If Error# = 0
    !Paul 14/07/2009 Log No 10920
    !update the jobs file here so we can close the customer window
    access:jobs.tryupdate()
    !End Change

    Do UpdateLoanDetails
    Do ButtonText
ButtonText      Routine
    If job:Loan_Unit_Number <> 0
        ?RestockLoanUnit{prop:Hide} = 0
        ?PickLoanUnit{prop:Hide} = 1
    Else !job:Loan_Unit_Number <> 0
        ?RestockLoanUnit{prop:Hide} = 1
        ?PickLoanUnit{prop:Hide} = 0
    End !job:Loan_Unit_Number <> 0
CityLinkService        Routine
    Disable(?job:loaservice)

    access:courier.clearkey(cou:courier_key)
    cou:courier = job:loan_courier
    If access:courier.tryfetch(cou:courier_key) = Level:Benign
        If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
            check_access('AMEND CITY LINK SERVICE',x")
            if x" = true
                Enable(?job:loaservice)
            End!if x" = true
        End!If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
    End!If access:courier.clearkey(cou:courier_key) = Level:Benign

UpdateAccessories       Routine
    count# = 0
    setcursor(cursor:wait)
    save_lac_id = access:loanacc.savefile()
    access:loanacc.clearkey(lac:ref_number_key)
    lac:ref_number = loa:ref_number
    set(lac:ref_number_key,lac:ref_number_key)
    loop
        if access:loanacc.next()
           break
        end !if
        if lac:ref_number <> loa:ref_number      |
            then break.  ! end if
        count# += 1
        tmp:Accessories = Clip(lac:Accessory)
    end !loop
    access:loanacc.restorefile(save_lac_id)
    setcursor()

    If count# = 0
        tmp:Accessories = ''
    End!If count# = 0
    If count# > 1
        tmp:Accessories = count#
    End!If count# > 1
    Display()
UpdateLoanDetails      Routine
    tmp:IMEI    = ''
    tmp:MSN     = ''
    tmp:ModelNumber = ''
    If job:Loan_Unit_Number <> ''
        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number  = job:Loan_Unit_Number
        If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
            !Found
            tmp:IMEI    = loa:ESN
            tmp:MSN     = loa:MSN
            tmp:ModelNumber = loa:Model_Number

!            Access:JOBSE.Clearkey(jobe:RefNumberKey)
!            jobe:RefNumber  = job:Ref_Number
!            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                !Found
!                tmp:LoanValue   = jobe:LoanReplacementValue
!            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                !Error
!            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        Else ! If AccessLOAN.Tryfetch(loaRef_Number_Key) = LevelBenign
            !Error
        End !If AccessLOAN.Tryfetch(loaRef_Number_Key) = LevelBenign
    Else
        tmp:LoanValue = 0
    End !If job:Exchange_Unit_Number <> ''
    If MSNRequired(job:Manufacturer) = 0
        ?job:msn{prop:Hide} = 0
        ?job:MSN:Prompt{prop:Hide} = 0
        ?tmp:MSN{prop:Hide} = 0
        ?tmp:MSN:Prompt{prop:Hide} = 0
    Else !If MSNRequired(job:Manufacturer)
        ?job:msn{prop:Hide} = 1
        ?job:MSN:Prompt{prop:Hide} = 1
        ?tmp:MSN{prop:Hide} = 1
        ?tmp:MSN:Prompt{prop:Hide} = 1
    End !If MSNRequired(job:Manufacturer)


    Display()
DisplayDepositDetails Routine
    ! Display the deposit paid group if required

    hide(?DepositGroup)                                                 ! Hide fields

    tmp:DepositAmount = 0
    tmp:DepositDate = ''

    Access:JobPaymt.ClearKey(jpt:Loan_Deposit_Key)
    jpt:Ref_Number = job:Ref_Number
    jpt:Loan_Deposit = True
    set(jpt:Loan_Deposit_Key,jpt:Loan_Deposit_Key)
    loop until Access:JobPaymt.Next()                                   ! Loop through payments attached to job
        if jpt:Ref_Number <> job:Ref_Number then break.
        if jpt:Loan_Deposit <> True then break.
        tmp:DepositAmount += jpt:Amount
        if tmp:DepositDate = '' then tmp:DepositDate = jpt:Date.        ! There may be multiple deposit records,
                                                                        ! store date of first record found.
    end

    if tmp:DepositAmount <> 0
        unhide(?DepositGroup)                                           ! Display fields if a deposit was found
    end
Loan:Return:Contact_History Routine
    ! Add contact history entry when loan unit returned
    tmp:Contact_History_Action = 'LOAN UNIT RETURNED'
    do Loan:Contact_History
Loan:Issued:Contact_History Routine
    ! Add contact history entry when loan unit issued
    tmp:Contact_History_Action = 'LOAN UNIT ISSUED'
    do Loan:Contact_History
Loan:Contact_History Routine
    ! Add contact history entry
    data
Loan:Accessories string(255)
    code
    Loan:Accessories = tmp:Accessories              ! Save accessories string, as it is used elsewhere..

    Access:Loan.ClearKey(loa:ref_number_key)
    loa:ref_number = job:loan_unit_number
    if not Access:Loan.Fetch(loa:ref_number_key)    ! Fetch loan unit
        do Loan:Fetch_Accessories                   ! Get comma delimited list of accessories (stored in tmp:Accessories)
        if not Access:ContHist.PrimeRecord()        ! Insert new contact history record
            cht:Ref_Number  = job:ref_number
            cht:Date = Today()
            cht:Time = Clock()
            Access:Users.Clearkey(use:password_key)
            use:password = glo:password
            Access:Users.Fetch(use:password_key)
            cht:User = use:user_code
            cht:Action = tmp:Contact_History_Action
            cht:Notes = 'MANUFACTURER: ' & clip(loa:Manufacturer) & '<13,10>' & |
                        'MODEL: ' & clip(loa:Model_Number) & '<13,10>' & |
                        'IMEI: ' & clip(loa:ESN) & '<13,10>' & |
                        'ACCESSORIES: ' & clip(tmp:Accessories) & '<13,10>'
            Access:ContHist.TryUpdate()
        end
    end

    tmp:Accessories = Loan:Accessories
Loan:Fetch_Accessories Routine
    ! Load string variable with loan units accessories
    tmp:Accessories = ''

    save_lac_id = Access:LoanAcc.SaveFile()
    Access:LoanAcc.ClearKey(lac:ref_number_key)
    lac:ref_number = loa:ref_number
    set(lac:ref_number_key,lac:ref_number_key)
    loop until Access:LoanAcc.Next()                        ! Loop through loan accessories
        if lac:ref_number <> loa:ref_number then break.
        if tmp:Accessories <> ''
            tmp:Accessories = clip(tmp:Accessories) & ', '
        end
        tmp:Accessories = clip(tmp:Accessories) & clip(lac:Accessory)
    end !loop
    Access:LoanAcc.RestoreFile(save_lac_id)

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020395'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ViewLoanUnit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:AUDIT.Open
  Relate:CITYSERV.Open
  Relate:DEFAULTS.Open
  Relate:JOBPAYMT.Open
  Relate:LOAN.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:COURIER.UseFile
  Access:LOANACC.UseFile
  Access:CONTHIST.UseFile
  Access:JOBSE.UseFile
  Access:MODELNUM.UseFile
  Access:MANUFACT.UseFile
  Access:JOBSE2.UseFile
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Found
      save:IDNumber   = jobe2:LoanIDNumber
  Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Do CityLinkService
  Do UpdateAccessories
  Do UpdateLoanDetails
  Do DisplayDepositDetails    ! Added by Gary (7th Aug 2002)
  
  If SecurityCheck('JOBS - AMEND DESPATCH') = Level:Benign  And job:Loan_Consignment_Number <> ''
      ?AmendDespatchDetails{prop:Hide} = 0
  End !SecurityCheck('JOBS - AMEND DESPATCH') = Level:Benign
  
  if job:Date_Completed <> ''
      if SecurityCheck('AMEND COMPLETED JOBS')
          !Job has been completed but they have no access to change it!
          !So check they CAN do it if it's invoiced!
          if job:Invoice_Date <> ''
              if SecurityCheck('AMEND INVOICED JOBS')
                  !invoiced and they have no access
                  disable(?PickLoanUnit)
              end
          else
              ! Not invoiced, so assume they cannot amend
              disable(?PickLoanUnit)
          end
      end
  end
  
  tmp:ModelNumberDetails = job:Model_Number
  If job:Colour <> ''
      tmp:ModelNumberDetails = Clip(tmp:ModelNumberDetails) & ' - ' & Clip(job:Colour)
  End !job:Colour <> ''
  
  If job:Warranty_Job = 'YES'
      ?job:Warranty_Charge_Type{prop:Hide} = 0
      ?WChargeType:Prompt{prop:Hide} = 0
  End !job:Warranty_Job = 'YES'
  
  If job:Chargeable_Job = 'YES'
      ?job:Charge_Type{prop:Hide} = 0
      ?CChargeType:Prompt{prop:Hide} = 0
  End !job:Chargeable_Job = 'YES'
  
  SET(Defaults)
  Access:Defaults.Next()
  If def:Force_Outoing_Courier = 'B' OR def:Force_Outoing_Courier = 'C'
    ?job:Loan_Courier{Prop:Req} = TRUE
  END
  Bryan.CompFieldColour()
  
  Do ButtonText
  
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
      tmp:LoanValue   = jobe:LoanReplacementValue
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  !View Only
  !If glo:Preview = 'V'
  !    ?PickExchangeUnit{prop:Hide} = 1
  !    ?AmendDespatchDetails{prop:Hide} = 1
  !    ?Payment_Details{prop:Hide} = 1
  !    ?LookupExchangeCourier{prop:Disable} = 1
  !    ?job:Loan_Courier{prop:Disable} = 1
  !End !glo:Select21 = 'RRCVIEWONLY'
  
  ! Inserting (DBH 06/12/2006) # 8559 - If the job is being viewed by another RRC. Disable
  If glo:WebJob
      Access:WEBJOB.ClearKey(wob:RefNumberKey)
      wob:RefNumber = job:Ref_Number
      If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
          !Found
      Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
          !Error
      End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number =  wob:HeadAccountNumber
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Found
          Access:USERS.ClearKey(use:Password_Key)
          use:Password = glo:Password
          If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
              !Found
              If use:Location <> tra:SiteLocation
                  ?PickLoanUnit{prop:Hide} = 1
                  ?AmendDespatchDetails{prop:Hide} = 1
                  ?Payment_Details{prop:Hide} = 1
                  ?LookupExchangeCourier{prop:Disable} = 1
                  ?job:Loan_Courier{prop:Disable} = 1
                  ?BrowseLoanAccessories{prop:Hide} = 1
              End ! If use:Location <> tra:SiteLocation
          Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
              !Error
          End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  End ! If glo:WebJob
  ! End (DBH 06/12/2006) #8559
  ! Save Window Name
   AddToLog('Window','Open','ViewLoanUnit')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?job:Loan_Courier{Prop:Tip} AND ~?LookupExchangeCourier{Prop:Tip}
     ?LookupExchangeCourier{Prop:Tip} = 'Select ' & ?job:Loan_Courier{Prop:Tip}
  END
  IF ?job:Loan_Courier{Prop:Msg} AND ~?LookupExchangeCourier{Prop:Msg}
     ?LookupExchangeCourier{Prop:Msg} = 'Select ' & ?job:Loan_Courier{Prop:Msg}
  END
  FDCB6.Init(job:LoaService,?job:LoaService,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:CITYSERV,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(cit:ServiceKey)
  FDCB6.AddField(cit:Service,FDCB6.Q.cit:Service)
  FDCB6.AddField(cit:RefNumber,FDCB6.Q.cit:RefNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:CITYSERV.Close
    Relate:DEFAULTS.Close
    Relate:JOBPAYMT.Close
    Relate:LOAN.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ViewLoanUnit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickCouriers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?BrowseLoanAccessories
      GLO:Select1 = job:Loan_Unit_Number
    OF ?Payment_Details
      GLO:Select1 = job:Ref_Number
      GLO:Select2 = 'LOAN DEPOSIT'
      Browse_Payments()
      GLO:Select1 = ''
      GLO:Select2 = ''
      Do DisplayDepositDetails                        ! Recheck if a deposit exists for this job
    OF ?SendSMSButton
      !Log No 10321 PS 28/04/09
      
      If jobe2:SMSDate = 0 then
          !no loan sms sent previously
          jobe2:SMSDate = today() + 30
          access:JobSE2.update()
      
      End!If jobe2:SMSDate = 0 then
      
      !Format the date before sending
      TempDate = Format(jobe2:SMSDate,@d10)
      
      !now send the SMS message
      If jobe2:SMSNotification
          !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'LOAN','SMS',jobe2:SMSAlertNumber,'','',clip(TempDate))
          SendSMSText('L','Y','N')
      End ! If jobe2:SMSNotification
      If jobe2:EmailNotification
          AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'LOAN','EMAIL','',jobe2:EmailAlertAddress,'',clip(TempDate))
      End ! If jobe2:EmailNotification
    OF ?OK
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
          jobe:LoanReplacementValue   = tmp:LoanValue
          Access:JOBSE.Update()
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
      ! Inserting (DBH 07/04/2006) #7305 - Save the changes
      Access:JOBSE2.Update()
      ! End (DBH 07/04/2006) #7305
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020395'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020395'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020395'&'0')
      ***
    OF ?PickLoanUnit
      ThisWindow.Update
      Do PickLoanUnit
    OF ?RestockLoanUnit
      ThisWindow.Update
      Do PickLoanUnit
    OF ?jobe2:LoanIDNumber
      ! Changing (DBH 08/06/2006) #7305 - This field is now the Loan ID Number
      ! ! Inserting (DBH 07/04/2006) #7305 - The Customer ID Number has been changed
      ! If jobe2:IDNumber <> save:IDNumber
      !     If Clip(save:IDNumber) <> ''
      !         Case Missive('Are you sure you want to change the Customer ID Number?'&|
      !           '||(The change will be recorded in the Audit Trail)','ServiceBase 3g',|
      !                        'mquest.jpg','\No|/Yes')
      !             Of 2 ! Yes Button
      !                 If Access:JOBSE2.Update() = Level:Benign
      !                     If AddToAudit(job:Ref_Number,'JOB','CUSTOMER ID NUMBER CHANGED','OLD ID NO: ' & Clip(save:IDNumber) & |
      !                                                                                     '<13,10>NEW ID NO: ' & Clip(jobe2:IDNumber))
      !                         save:IDNumber = jobe2:IDNumber
      !                     End ! If AddToAudit
      !                 End ! If Access:JOBSE2.Update() = Level:Benign
      !             Of 1 ! No Button
      !                 jobe2:IDNumber = save:IDNumber
      !         End ! Case Missive
      !     Else ! If Clip(save:IDNumber) <> ''
      !
      !     End ! If Clip(save:IDNumber) <> ''
      ! End ! If jobe2:IDNumber <> save:IDNumber
      ! Display()
      ! ! End (DBH 07/04/2006) #7305
      ! to (DBH 08/06/2006) #7305
      ! Inserting (DBH 07/04/2006) #7305 - The Customer ID Number has been changed
      If jobe2:LoanIDNumber <> save:IDNumber
          If Clip(save:IDNumber) <> ''
              Case Missive('Would you like to change the ID Number associated to the loan unit only (Loan), or do you wish to change it here AND under the Address Details as well (Both)?'&|
                '|(The change will be recorded in the Audit Trail).','ServiceBase 3g',|
                             'mquest.jpg','\Cancel|Both|Loan')
                  Of 3 ! Loan Button
                      If Access:JOBSE2.Update() = Level:Benign
                          If AddToAudit(job:Ref_Number,'JOB','LOAN ID NUMBER CHANGED','OLD ID NO: ' & Clip(save:IDNumber) & |
                                                                                          '<13,10>NEW ID NO: ' & Clip(jobe2:LoanIDNumber))
                              save:IDNumber = jobe2:LoanIDNumber
                          End ! If AddToAudit
                      End ! If Access:JOBSE2.Update() = Level:Benign
                  Of 2 ! Both Button
                      CustId" = jobe2:IDNumber
                      jobe2:IDNumber = jobe2:LoanIDNumber
                      If Access:JOBSE2.Update() = Level:Benign
                          If AddToAudit(job:Ref_Number,'JOB','LOAN ID NUMBER CHANGED','OLD ID NO: ' & Clip(save:IDNumber) & |
                                                                                          '<13,10>NEW ID NO: ' & Clip(jobe2:LoanIDNumber))
                              save:IDNumber = jobe2:LoanIDNumber
                          End ! If AddToAudit
                          If AddToAudit(job:Ref_Number,'JOB','CUSTOMER ID NUMBER CHANGED','OLD ID NO: ' & Clip(CustID") & |
                                                                                          '<13,10>NEW ID NO: ' & Clip(jobe2:IDNumber))
                              save:IDNumber = jobe2:LoanIDNumber
                          End ! If AddToAudit
                      End ! If Access:JOBSE2.Update() = Level:Benign
                  Of 1 ! Cancel Button
              End ! Case Missive
          Else ! If Clip(save:IDNumber) <> ''
      
          End ! If Clip(save:IDNumber) <> ''
      End ! If jobe2:IDNumber <> save:IDNumber
      Display()
      ! End (DBH 07/04/2006) #7305
      
      ! End (DBH 08/06/2006) #7305
    OF ?BrowseLoanAccessories
      ThisWindow.Update
      Browse_Loan_Accessories(job:Loan_Unit_Number)
      ThisWindow.Reset
      glo:Select1 = ''
    OF ?SMSHistButton
      ThisWindow.Update
      BrowseSMSHistory
      ThisWindow.Reset
    OF ?job:Loan_Courier
      IF job:Loan_Courier OR ?job:Loan_Courier{Prop:Req}
        cou:Courier = job:Loan_Courier
        !Save Lookup Field Incase Of error
        look:job:Loan_Courier        = job:Loan_Courier
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Loan_Courier = cou:Courier
          ELSE
            !Restore Lookup On Error
            job:Loan_Courier = look:job:Loan_Courier
            SELECT(?job:Loan_Courier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      !Write the courier to webjobs again, if the job is in the despatch table - 4001 (DBH: 04-03-2004)
      If glo:WebJob and jobe:DespatchType = 'LOA'
          save_WEBJOB_id = Access:WEBJOB.SaveFile()
          Access:WEBJOB.Clearkey(wob:RefNumberKey)
          wob:RefNumber   = job:Ref_Number
          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
              !Found
              If wob:ReadyToDespatch = 1
                  wob:DespatchCourier = job:Loan_Courier
                  Access:WEBJOB.TryUpdate()
              End !If wob:ReadyToDespatch = 1
          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
              !Error
          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          Access:WEBJOB.RestoreFile(save_WEBJOB_id)
      End !glo:WebJob
    OF ?LookupExchangeCourier
      ThisWindow.Update
      cou:Courier = job:Loan_Courier
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Loan_Courier = cou:Courier
          Select(?+1)
      ELSE
          Select(?job:Loan_Courier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Loan_Courier)
    OF ?AmendDespatchDetails
      ThisWindow.Update
      Case Missive('Are you sure you want to amend the Loan Unit''s Despatch Details?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              ?job:Loan_consignment_number{prop:readonly} = 0
              ?job:Loan_consignment_number{prop:color} = color:yellow
              ?job:Loan_despatched{prop:readonly} = 0
              ?job:Loan_despatched{prop:color} = color:yellow
              ?job:Loan_despatched_user{prop:readonly} = 0
              ?job:Loan_despatched_user{prop:color} = color:yellow
              ?job:Loan_despatch_number{prop:readonly} = 0
              ?job:Loan_despatch_number{prop:color} = color:yellow
      
          Of 1 ! No Button
      End ! Case Missive
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Browse_Accessory_Stock PROCEDURE                      !Generated from procedure template - Window

CurrentTab           STRING(80)
model_number_temp    STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
location_temp        STRING(30)
no_temp              STRING('NO')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?location_temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOMODEL)
                       PROJECT(stm:Part_Number)
                       PROJECT(stm:Description)
                       PROJECT(stm:Ref_Number)
                       PROJECT(stm:RecordNumber)
                       PROJECT(stm:Accessory)
                       PROJECT(stm:Model_Number)
                       PROJECT(stm:Location)
                       JOIN(sto:Ref_Part_Description_Key,stm:Location,stm:Ref_Number,stm:Part_Number,stm:Description)
                         PROJECT(sto:Quantity_Stock)
                         PROJECT(sto:Location)
                         PROJECT(sto:Ref_Number)
                         PROJECT(sto:Part_Number)
                         PROJECT(sto:Description)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
stm:Part_Number        LIKE(stm:Part_Number)          !List box control field - type derived from field
stm:Description        LIKE(stm:Description)          !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
stm:Ref_Number         LIKE(stm:Ref_Number)           !List box control field - type derived from field
stm:RecordNumber       LIKE(stm:RecordNumber)         !Primary key field - type derived from field
stm:Accessory          LIKE(stm:Accessory)            !Browse key field - type derived from field
stm:Model_Number       LIKE(stm:Model_Number)         !Browse key field - type derived from field
stm:Location           LIKE(stm:Location)             !Browse key field - type derived from field
sto:Location           LIKE(sto:Location)             !Related join file key field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Related join file key field - type derived from field
sto:Part_Number        LIKE(sto:Part_Number)          !Related join file key field - type derived from field
sto:Description        LIKE(sto:Description)          !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Browse the Stock File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Stock File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,20),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Site Location'),AT(168,86),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s30),AT(224,86,124,10),USE(location_temp),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       LIST,AT(168,134,276,192),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('114L(2)|M~Part Number~@s30@109L(2)|M~Description~@s30@50D(2)|M~Qty In Stock~L@N8' &|
   '@48R(2)|M~Ref Number~@n012@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,184),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       SHEET,AT(164,104,352,226),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Part Number'),USE(?Tab:6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,120,124,10),USE(stm:Part_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,120,124,10),USE(stm:Description),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass CLASS(StepStringClass)          !Default Step Manager
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

BRW1::Sort1:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 2
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020402'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Accessory_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Access:MODELNUM.UseFile
  Access:STOCK.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOMODEL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Title
  Set(defstock)
  Access:defstock.next()
  If dst:site_location <> ''
      location_temp = dst:site_location
      Display()
  End
  
  if glo:webjob then
      access:users.clearkey(use:password_key)
      use:Password = glo:password
      if access:users.fetch(use:password_key)
          !error - let defaults stand
      ELSE
          location_temp= use:Location
          ?location_temp{prop:readonly}=true
          disable(?location_temp)
          display()
      END
  END !If glo:webjob
  
  
  !title
  ! Save Window Name
   AddToLog('Window','Open','Browse_Accessory_Stock')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,stm:Description_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STM:Description,stm:Description,1,BRW1)
  BRW1.AddResetField(location_temp)
  BRW1.AddSortOrder(,stm:Model_Part_Number_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?STM:Part_Number,stm:Part_Number,1,BRW1)
  BRW1.AddResetField(location_temp)
  BIND('GLO:Select12',GLO:Select12)
  BIND('location_temp',location_temp)
  BIND('no_temp',no_temp)
  BIND('model_number_temp',model_number_temp)
  BRW1.AddField(stm:Part_Number,BRW1.Q.stm:Part_Number)
  BRW1.AddField(stm:Description,BRW1.Q.stm:Description)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(stm:Ref_Number,BRW1.Q.stm:Ref_Number)
  BRW1.AddField(stm:RecordNumber,BRW1.Q.stm:RecordNumber)
  BRW1.AddField(stm:Accessory,BRW1.Q.stm:Accessory)
  BRW1.AddField(stm:Model_Number,BRW1.Q.stm:Model_Number)
  BRW1.AddField(stm:Location,BRW1.Q.stm:Location)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  QuickWindow{PROP:MinWidth}=584
  QuickWindow{PROP:MinHeight}=210
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  FDCB6.Init(location_temp,?location_temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(loc:Location_Key)
  FDCB6.AddField(loc:Location,FDCB6.Q.loc:Location)
  FDCB6.AddField(loc:RecordNumber,FDCB6.Q.loc:RecordNumber)
  FDCB6.AddUpdateField(loc:Location,location_temp)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:6{PROP:TEXT} = 'By Part Number'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='114L(2)|M~Part Number~@s30@#1#109L(2)|M~Description~@s30@#2#50D(2)|M~Qty In Stock~L@N8@#3#48R(2)|M~Ref Number~@n012@#4#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Accessory_Stock')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020402'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020402'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020402'&'0')
      ***
    OF ?location_temp
      stm:model_number = glo:select12
      stm:accessory = 'YES'
      brw1.addrange(stm:location,location_temp)
      brw1.applyrange
      thiswindow.reset(1)
      BRW1.ResetSort(1)
    OF ?Select:2
      ThisWindow.Update
      glo:select7 = stm:ref_number
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      stm:model_number = glo:select12
      stm:accessory = 'YES'
      brw1.addrange(stm:location,location_temp)
      brw1.applyrange
      thiswindow.reset(1)
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='114L(2)|M~Part Number~@s30@#1#109L(2)|M~Description~@s30@#2#50D(2)|M~Qty In Stock~L@N8@#3#48R(2)|M~Ref Number~@n012@#4#'
          ?Tab:6{PROP:TEXT} = 'By Part Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='109L(2)|M~Description~@s30@#2#114L(2)|M~Part Number~@s30@#1#50D(2)|M~Qty In Stock~L@N8@#3#48R(2)|M~Ref Number~@n012@#4#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ?WindowTitle{prop:text} = 'Available Stock For Model: ' & Clip(glo:select12)
      Select(?Browse:1)
      stm:model_number = glo:select12
      stm:accessory = 'YES'
      brw1.addrange(stm:location,location_temp)
      brw1.applyrange
      thiswindow.reset(1)
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
      FDCB6.ResetQueue(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1::Sort0:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  stm:model_number = Upper(glo:select12)
  stm:accessory = 'YES'
  PARENT.Init(Controls,Mode)


BRW1::Sort1:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  stm:model_number = Upper(glo:select12)
  stm:accessory = 'YES'
  PARENT.Init(Controls,Mode)


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?location_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?STM:Part_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?STM:Description, Resize:FixLeft+Resize:FixTop, Resize:LockSize)


FDCB6.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

Browse_Job_Exchange_Accessory PROCEDURE (f_ref_number) !Generated from procedure template - Window

CurrentTab           STRING(80)
stock_ref_number_temp REAL
FilesOpened          BYTE
Ref_Number_Temp      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBEXACC)
                       PROJECT(jea:Part_Number)
                       PROJECT(jea:Description)
                       PROJECT(jea:Record_Number)
                       PROJECT(jea:Job_Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
jea:Part_Number        LIKE(jea:Part_Number)          !List box control field - type derived from field
jea:Description        LIKE(jea:Description)          !List box control field - type derived from field
jea:Record_Number      LIKE(jea:Record_Number)        !Primary key field - type derived from field
jea:Job_Ref_Number     LIKE(jea:Job_Ref_Number)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Exchange Accessories'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Exchange Accessory File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,110,276,214),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('124L(2)|M~Part Number~@s30@80L(2)|M~Description~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Part Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,124,10),USE(jea:Part_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,204),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(448,247),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020403'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Job_Exchange_Accessory')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBEXACC.Open
  Relate:STOCK.Open
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  ref_number_temp = f_ref_number
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBEXACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Job_Exchange_Accessory')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,jea:Part_Number_Key)
  BRW1.AddRange(jea:Job_Ref_Number,Ref_Number_Temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?jea:Part_Number,jea:Part_Number,1,BRW1)
  BRW1.AddField(jea:Part_Number,BRW1.Q.jea:Part_Number)
  BRW1.AddField(jea:Description,BRW1.Q.jea:Description)
  BRW1.AddField(jea:Record_Number,BRW1.Q.jea:Record_Number)
  BRW1.AddField(jea:Job_Ref_Number,BRW1.Q.jea:Job_Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBEXACC.Close
    Relate:STOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Job_Exchange_Accessory')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  stock_ref_number_Temp = jea:stock_ref_number
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Job_Exchange_Accessory
    ReturnValue = GlobalResponse
  END
  If globalresponse = 1
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number  = stock_ref_number_temp
      If access:stock.fetch(sto:ref_number_key) = Level:Benign
          sto:quantity_stock += 1
          access:stock.update()
          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                             'REC', | ! Transaction_Type
                             '', | ! Depatch_Note_Number
                             Ref_Number_Temp, | ! Job_Number
                             0, | ! Sales_Number
                             1, | ! Quantity
                             sto:Purchase_Cost, | ! Purchase_Cost
                             sto:Sale_Cost, | ! Sale_Cost
                             sto:Retail_Cost, | ! Retail_Cost
                             'STOCK RECREDITED', | ! Notes
                             'REMOVED FROM EXCHANGE') ! Information
            ! Added OK
  
          Else ! AddToStockHistory
            ! Error
          End ! AddToStockHistory
      End!If access:stock.fetch(sto:ref_number_key)
  End!If globalresponse = 1
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Insert
      glo:select7 = ''
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020403'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020403'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020403'&'0')
      ***
    OF ?Insert
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Accessory_Stock
      ThisWindow.Reset
      If glo:select7 <> ''
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number  = glo:select7
          If access:stock.fetch(sto:ref_number_key)
          Else!If access:stock.fetch(sto:ref_number_key)
              If sto:quantity_stock <= 0
                  Case Missive('There is insufficient stock.'&|
                    '<13,10>'&|
                    '<13,10>Please select another Accessory.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!If sto:quantity_stock <= 0
                  get(jobexacc,0)
                  if access:jobexacc.primerecord() = Level:benign
                      jea:job_ref_number   = ref_number_temp
                      jea:stock_ref_number = sto:ref_number
                      jea:part_number      = sto:part_number
                      jea:description      = sto:description
                      access:jobexacc.insert()
      
                      sto:quantity_stock -= 1
                      If sto:quantity_stock < 0
                          sto:quantity_stock = 0
                      End!If sto:quantity_stock < 0
                      access:stock.update()
                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'DEC', | ! Transaction_Type
                                         '', | ! Depatch_Note_Number
                                         Ref_Number_Temp, | ! Job_Number
                                         0, | ! Sales_Number
                                         1, | ! Quantity
                                         sto:Purchase_Cost, | ! Purchase_Cost
                                         sto:Sale_Cost, | ! Sale_Cost
                                         sto:Retail_Cost, | ! Retail_Cost
                                         'ACCESSORY EXCHANGE ON JOB NUMBER: ' & Clip(Ref_Number_Temp), | ! Notes
                                         '') ! Information
                        ! Added OK
      
                      Else ! AddToStockHistory
                        ! Error
                      End ! AddToStockHistory
                  end!if access:jobexacc.primerecord() = Level:benign
              End!If sto:quantity_stock <= 0
          End!If access:stock.fetch(sto:ref_number_key)
      End!If glo:select7 <> ''
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?jea:Part_Number
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

