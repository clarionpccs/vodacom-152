

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01004.INC'),ONCE        !Local module procedure declarations
                     END


Update_Invoice PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
sav:couriercost      REAL
sav:labourcost       REAL
sav:partscost        REAL
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::inv:Record  LIKE(inv:RECORD),STATIC
QuickWindow          WINDOW('Update the INVOICE File'),AT(,,219,212),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('Update_Invoice'),TIMER(50),SYSTEM,GRAY,DOUBLE,IMM
                       PROMPT('Invoice Number'),AT(8,20),USE(?INV:Invoice_Number:Prompt),TRN
                       ENTRY(@s8),AT(84,20,56,10),USE(inv:Invoice_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                       PROMPT('Account Number'),AT(8,36),USE(?INV:Account_Number:Prompt),TRN
                       ENTRY(@s15),AT(84,36,124,10),USE(inv:Account_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                       SHEET,AT(4,4,212,176),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Batch Number'),AT(8,52),USE(?INV:Batch_Number:Prompt)
                           ENTRY(@s6),AT(84,52,60,10),USE(inv:Batch_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Courier Value'),AT(8,68),USE(?INV:Courier_Paid:Prompt)
                           ENTRY(@n14.2b),AT(84,68,60,10),USE(inv:Courier_Paid),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Labour Value'),AT(8,84),USE(?INV:Labour_Paid:Prompt)
                           ENTRY(@n14.2b),AT(84,84,60,10),USE(inv:Labour_Paid),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Parts Value'),AT(8,100),USE(?INV:Parts_Paid:Prompt)
                           ENTRY(@n14.2b),AT(84,100,60,10),USE(inv:Parts_Paid),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Total (Ex V.A.T.)'),AT(8,116),USE(?INV:Total:Prompt),TRN
                           ENTRY(@n14.2b),AT(84,116,60,10),USE(inv:Total),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Labour V.A.T. Rate'),AT(8,132),USE(?INV:Vat_Rate_Labour:Prompt),TRN
                           ENTRY(@N14.2B),AT(84,132,60,10),USE(inv:Vat_Rate_Labour),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Parts V.A.T. Rate'),AT(8,148),USE(?INV:Vat_Rate_Parts:Prompt),TRN
                           ENTRY(@N14.2B),AT(84,148,60,10),USE(inv:Vat_Rate_Parts),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Retail V.A.T. Rate'),AT(8,164),USE(?INV:Vat_Rate_Retail:Prompt),TRN
                           ENTRY(@n14.2B),AT(84,164,60,10),USE(inv:Vat_Rate_Retail),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('Chargeable Invoice'),USE(?Tab2),HIDE
                           PROMPT('Courier Cost'),AT(8,52),USE(?JOB:Invoice_Courier_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,52,64,10),USE(job:Invoice_Courier_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Labour Cost'),AT(8,68),USE(?JOB:Invoice_Labour_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,68,64,10),USE(job:Invoice_Labour_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Parts Cost'),AT(8,84),USE(?JOB:Invoice_Parts_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,84,64,10),USE(job:Invoice_Parts_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Total (Ex Vat)'),AT(8,100),USE(?JOB:Invoice_Sub_Total:Prompt),TRN
                           ENTRY(@n14.2),AT(84,100,64,10),USE(job:Invoice_Sub_Total),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                         END
                         TAB('Warranty Invoice'),USE(?Tab3)
                           PROMPT('EDI Batch Number'),AT(8,52),USE(?INV:Batch_Number:Prompt:2)
                           ENTRY(@n6),AT(84,52,60,10),USE(inv:Batch_Number,,?INV:Batch_Number:2),DECIMAL(14),FONT(,8,,FONT:bold),UPR
                           PROMPT('Claim Reference'),AT(8,68),USE(?INV:Claim_Reference:Prompt),TRN
                           ENTRY(@s30),AT(84,68,60,10),USE(inv:Claim_Reference),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Courier Cost'),AT(8,100),USE(?INV:Courier_Paid:Prompt:2)
                           ENTRY(@n14.2),AT(84,100,60,10),USE(inv:Courier_Paid,,?INV:Courier_Paid:2),DECIMAL(14),FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Labour Cost'),AT(8,116),USE(?INV:Labour_Paid:Prompt:2)
                           ENTRY(@n14.2),AT(84,116,60,10),USE(inv:Labour_Paid,,?INV:Labour_Paid:2),DECIMAL(14),FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Parts Cost'),AT(8,132),USE(?INV:Parts_Paid:Prompt:2)
                           ENTRY(@n14.2),AT(84,132,60,10),USE(inv:Parts_Paid,,?INV:Parts_Paid:2),DECIMAL(14),FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Total Claimed'),AT(8,84),USE(?INV:Total_Claimed:Prompt)
                           ENTRY(@n14.2),AT(84,84,60,10),USE(inv:Total_Claimed),DECIMAL(14),FONT(,,,FONT:bold),MSG('r'),TIP('r')
                         END
                       END
                       BUTTON('&OK'),AT(100,188,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,188,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,184,212,24),USE(?Panel1),FILL(0D6E7EFH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Amending An Invoice'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Invoice')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?INV:Invoice_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(inv:Record,History::inv:Record)
  SELF.AddHistoryField(?inv:Invoice_Number,1)
  SELF.AddHistoryField(?inv:Account_Number,5)
  SELF.AddHistoryField(?inv:Batch_Number,14)
  SELF.AddHistoryField(?inv:Courier_Paid,18)
  SELF.AddHistoryField(?inv:Labour_Paid,19)
  SELF.AddHistoryField(?inv:Parts_Paid,20)
  SELF.AddHistoryField(?inv:Total,7)
  SELF.AddHistoryField(?inv:Vat_Rate_Labour,8)
  SELF.AddHistoryField(?inv:Vat_Rate_Parts,9)
  SELF.AddHistoryField(?inv:Vat_Rate_Retail,10)
  SELF.AddHistoryField(?INV:Batch_Number:2,14)
  SELF.AddHistoryField(?inv:Claim_Reference,16)
  SELF.AddHistoryField(?INV:Courier_Paid:2,18)
  SELF.AddHistoryField(?INV:Labour_Paid:2,19)
  SELF.AddHistoryField(?INV:Parts_Paid:2,20)
  SELF.AddHistoryField(?inv:Total_Claimed,17)
  SELF.AddUpdateFile(Access:INVOICE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:INVOICE.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:INVOICE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Update_Invoice')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Case inv:invoice_Type
      Of 'SIN'
          Hide(?tab1)
          Hide(?tab3)
          Unhide(?tab2)
          access:jobs.clearkey(job:invoicenumberkey)
          job:invoice_number  = inv:invoice_number
          If access:jobs.tryfetch(job:invoicenumberkey) = Level:Benign
              !Found
              sav:labourcost  = job:invoice_courier_cost
              sav:partscost   = job:invoice_parts_cost
              sav:couriercost = job:invoice_courier_cost
          Else! If access:jobs.tryfetch(job:invoice_number_key) = Level:Benign
              !Error
          End! If access:.tryfetch(job:invoice_number_key) = Level:Benign
      Of 'WAR'
          hide(?tab1)
          hide(?tab2)
          Unhide(?tab3)
  End!Case inv:invoice_Type
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Invoice')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  Case inv:invoice_Type
      Of 'SIN'
          If job:invoice_courier_cost <> sav:couriercost Or |
              job:invoice_parts_cost <> sav:partscost Or |
              job:invoice_labour_cost <> sav:labourcost
              Case Missive('You have amended the invoice costs. The job will be updated with these values.'&|
                '<13,10>Are you sure?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      access:jobs.update()
                  Of 1 ! No Button
              End ! Case Missive
          End!job:invoice_labour_cost <> sav:labourcost
  End!Case inv:invoice_Type
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Invoice type and saved values
      !
      !Single Invoice (Chargeable)
      !
      !inv:job_number    = Job Number
      !inv:account_number    = Job Trade Account Number
      !inv:total    = Job Sub Total
      !inv:vat_rate....    = Trade VAT rates
      !
      !
      !Warranty Invoice
      !
      !inv:account_number    = Manufacturer Trade Account Number
      !inv:vat_rate...        = Trade VAT rates
      !inv:batch_number    = Batch Number
      !inv:claim_reference    = Claim Reference Number
      !inv:total_claimed    = Total Claimed
      !inv:courier_paid    = Courier Charge Paid
      !inv:labour_paid        = Labour Charge Paid
      !inv:parts_paid        = Parts Charge Paid
      !inv:jobs_count        = Number Of Jobs Invoiced
      !
      !
      !Multiple Invoice (Chargeable)
      !
      !inv:account_number    = Job Trade Account Number
      !inv:total            = Total of all invoices
      !inv:vat_rate ....    = Trade VAT Rates
      !inv:labour_paid        = Total Labour Paid
      !inv:parts_paid        = Total Parts Paid
      !inv:courier_paid    = Total Courier Paid
      !Invoice Update Type
      Case inv:invoice_type
          Of 'SIN'
              Disable(?inv:batch_number)
          Of 'CHA'
              Disable(?inv:batch_number)
          Of 'WAR'
              ?inv:total:prompt{prop:text} = 'Total Claimed'
      End!Case inv:invoice_type
    OF EVENT:Timer
      job:invoice_sub_total = job:invoice_courier_cost + job:invoice_parts_cost + job:invoice_labour_cost
      Display(?job:invoice_Sub_total)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults







Browse_Invoice PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
save_inv_id          USHORT,AUTO
save_wob_id          USHORT,AUTO
save_tradeacc_id     USHORT,AUTO
save_subtracc_id     USHORT,AUTO
pos                  STRING(255)
FilesOpened          BYTE
Invoice_Type_Temp    STRING(10)
account_number_temp  STRING(15)
invoice_Credit_temp  STRING(8)
tmp:filter           STRING(255)
tmp:order            STRING(255)
tmp:couriercost      REAL
tmp:LabourCost       REAL
tmp:PartsCost        REAL
tmp:RRCLabourCost    REAL
tmp:RRCPartsCost     REAL
tmp:ARCAccount       STRING(30)
tmp:InvoiceNumber    STRING(30)
tmp:InvoiceType      STRING(7)
tmp:TradeID          STRING(2)
BRW1::View:Browse    VIEW(INVOICE)
                       PROJECT(inv:Invoice_Number)
                       PROJECT(inv:Account_Number)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Courier_Paid)
                       PROJECT(inv:Labour_Paid)
                       PROJECT(inv:Parts_Paid)
                       PROJECT(inv:Invoice_Type)
                       PROJECT(inv:Total_Claimed)
                       PROJECT(inv:InvoiceCredit)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
inv:Invoice_Number     LIKE(inv:Invoice_Number)       !List box control field - type derived from field
inv:Invoice_Number_NormalFG LONG                      !Normal forground color
inv:Invoice_Number_NormalBG LONG                      !Normal background color
inv:Invoice_Number_SelectedFG LONG                    !Selected forground color
inv:Invoice_Number_SelectedBG LONG                    !Selected background color
inv:Account_Number     LIKE(inv:Account_Number)       !List box control field - type derived from field
inv:Account_Number_NormalFG LONG                      !Normal forground color
inv:Account_Number_NormalBG LONG                      !Normal background color
inv:Account_Number_SelectedFG LONG                    !Selected forground color
inv:Account_Number_SelectedBG LONG                    !Selected background color
inv:Date_Created       LIKE(inv:Date_Created)         !List box control field - type derived from field
inv:Date_Created_NormalFG LONG                        !Normal forground color
inv:Date_Created_NormalBG LONG                        !Normal background color
inv:Date_Created_SelectedFG LONG                      !Selected forground color
inv:Date_Created_SelectedBG LONG                      !Selected background color
Invoice_Type_Temp      LIKE(Invoice_Type_Temp)        !List box control field - type derived from local data
Invoice_Type_Temp_NormalFG LONG                       !Normal forground color
Invoice_Type_Temp_NormalBG LONG                       !Normal background color
Invoice_Type_Temp_SelectedFG LONG                     !Selected forground color
Invoice_Type_Temp_SelectedBG LONG                     !Selected background color
invoice_Credit_temp    LIKE(invoice_Credit_temp)      !List box control field - type derived from local data
invoice_Credit_temp_NormalFG LONG                     !Normal forground color
invoice_Credit_temp_NormalBG LONG                     !Normal background color
invoice_Credit_temp_SelectedFG LONG                   !Selected forground color
invoice_Credit_temp_SelectedBG LONG                   !Selected background color
tmp:couriercost        LIKE(tmp:couriercost)          !List box control field - type derived from local data
tmp:couriercost_NormalFG LONG                         !Normal forground color
tmp:couriercost_NormalBG LONG                         !Normal background color
tmp:couriercost_SelectedFG LONG                       !Selected forground color
tmp:couriercost_SelectedBG LONG                       !Selected background color
tmp:LabourCost         LIKE(tmp:LabourCost)           !List box control field - type derived from local data
tmp:LabourCost_NormalFG LONG                          !Normal forground color
tmp:LabourCost_NormalBG LONG                          !Normal background color
tmp:LabourCost_SelectedFG LONG                        !Selected forground color
tmp:LabourCost_SelectedBG LONG                        !Selected background color
tmp:PartsCost          LIKE(tmp:PartsCost)            !List box control field - type derived from local data
tmp:PartsCost_NormalFG LONG                           !Normal forground color
tmp:PartsCost_NormalBG LONG                           !Normal background color
tmp:PartsCost_SelectedFG LONG                         !Selected forground color
tmp:PartsCost_SelectedBG LONG                         !Selected background color
tmp:RRCLabourCost      LIKE(tmp:RRCLabourCost)        !List box control field - type derived from local data
tmp:RRCLabourCost_NormalFG LONG                       !Normal forground color
tmp:RRCLabourCost_NormalBG LONG                       !Normal background color
tmp:RRCLabourCost_SelectedFG LONG                     !Selected forground color
tmp:RRCLabourCost_SelectedBG LONG                     !Selected background color
tmp:RRCPartsCost       LIKE(tmp:RRCPartsCost)         !List box control field - type derived from local data
tmp:RRCPartsCost_NormalFG LONG                        !Normal forground color
tmp:RRCPartsCost_NormalBG LONG                        !Normal background color
tmp:RRCPartsCost_SelectedFG LONG                      !Selected forground color
tmp:RRCPartsCost_SelectedBG LONG                      !Selected background color
inv:Courier_Paid       LIKE(inv:Courier_Paid)         !List box control field - type derived from field
inv:Courier_Paid_NormalFG LONG                        !Normal forground color
inv:Courier_Paid_NormalBG LONG                        !Normal background color
inv:Courier_Paid_SelectedFG LONG                      !Selected forground color
inv:Courier_Paid_SelectedBG LONG                      !Selected background color
inv:Labour_Paid        LIKE(inv:Labour_Paid)          !List box control field - type derived from field
inv:Labour_Paid_NormalFG LONG                         !Normal forground color
inv:Labour_Paid_NormalBG LONG                         !Normal background color
inv:Labour_Paid_SelectedFG LONG                       !Selected forground color
inv:Labour_Paid_SelectedBG LONG                       !Selected background color
inv:Parts_Paid         LIKE(inv:Parts_Paid)           !List box control field - type derived from field
inv:Parts_Paid_NormalFG LONG                          !Normal forground color
inv:Parts_Paid_NormalBG LONG                          !Normal background color
inv:Parts_Paid_SelectedFG LONG                        !Selected forground color
inv:Parts_Paid_SelectedBG LONG                        !Selected background color
inv:Invoice_Type       LIKE(inv:Invoice_Type)         !List box control field - type derived from field
inv:Invoice_Type_NormalFG LONG                        !Normal forground color
inv:Invoice_Type_NormalBG LONG                        !Normal background color
inv:Invoice_Type_SelectedFG LONG                      !Selected forground color
inv:Invoice_Type_SelectedBG LONG                      !Selected background color
inv:Total_Claimed      LIKE(inv:Total_Claimed)        !List box control field - type derived from field
inv:Total_Claimed_NormalFG LONG                       !Normal forground color
inv:Total_Claimed_NormalBG LONG                       !Normal background color
inv:Total_Claimed_SelectedFG LONG                     !Selected forground color
inv:Total_Claimed_SelectedBG LONG                     !Selected background color
inv:InvoiceCredit      LIKE(inv:InvoiceCredit)        !List box control field - type derived from field
inv:InvoiceCredit_NormalFG LONG                       !Normal forground color
inv:InvoiceCredit_NormalBG LONG                       !Normal background color
inv:InvoiceCredit_SelectedFG LONG                     !Selected forground color
inv:InvoiceCredit_SelectedBG LONG                     !Selected background color
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW13::View:Browse   VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Invoice_Number)
                       PROJECT(job:Invoice_Number_Warranty)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:Unit_Type          LIKE(job:Unit_Type)            !List box control field - type derived from field
job:Invoice_Number     LIKE(job:Invoice_Number)       !Browse key field - type derived from field
job:Invoice_Number_Warranty LIKE(job:Invoice_Number_Warranty) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(RETSALES)
                       PROJECT(ret:Ref_Number)
                       PROJECT(ret:Purchase_Order_Number)
                       PROJECT(ret:Consignment_Number)
                       PROJECT(ret:Invoice_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
ret:Ref_Number         LIKE(ret:Ref_Number)           !List box control field - type derived from field
ret:Purchase_Order_Number LIKE(ret:Purchase_Order_Number) !List box control field - type derived from field
ret:Consignment_Number LIKE(ret:Consignment_Number)   !List box control field - type derived from field
ret:Invoice_Number     LIKE(ret:Invoice_Number)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW18::View:Browse   VIEW(JOBSINV)
                       PROJECT(jov:DateCreated)
                       PROJECT(jov:RecordNumber)
                       PROJECT(jov:InvoiceNumber)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:InvoiceNumber      LIKE(tmp:InvoiceNumber)        !List box control field - type derived from local data
tmp:InvoiceNumber_NormalFG LONG                       !Normal forground color
tmp:InvoiceNumber_NormalBG LONG                       !Normal background color
tmp:InvoiceNumber_SelectedFG LONG                     !Selected forground color
tmp:InvoiceNumber_SelectedBG LONG                     !Selected background color
tmp:InvoiceType        LIKE(tmp:InvoiceType)          !List box control field - type derived from local data
tmp:InvoiceType_NormalFG LONG                         !Normal forground color
tmp:InvoiceType_NormalBG LONG                         !Normal background color
tmp:InvoiceType_SelectedFG LONG                       !Selected forground color
tmp:InvoiceType_SelectedBG LONG                       !Selected background color
jov:DateCreated        LIKE(jov:DateCreated)          !List box control field - type derived from field
jov:DateCreated_NormalFG LONG                         !Normal forground color
jov:DateCreated_NormalBG LONG                         !Normal background color
jov:DateCreated_SelectedFG LONG                       !Selected forground color
jov:DateCreated_SelectedBG LONG                       !Selected background color
jov:RecordNumber       LIKE(jov:RecordNumber)         !Primary key field - type derived from field
jov:InvoiceNumber      LIKE(jov:InvoiceNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,538770663)          !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
XploreMask13         DECIMAL(10,0,538770663)          !Xplore
XploreMask113         DECIMAL(10,0,0)                 !Xplore
XploreTitle13        STRING(' ')                      !Xplore
xpInitialTab13       SHORT                            !Xplore
QuickWindow          WINDOW('Browse The Invoice File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Browse The Invoice File'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(8,56,592,170),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('43R(2)|M*~Invoice No~L@p<<<<<<<<<<<<<<#p@64L(2)|M*~Account Number~@s15@51R(2)|M*~Date C' &|
   'reated~C(0)@d6b@47L(2)|M*~Invoice Type~@s10@55L(2)|M*~Invoice / Credit~@s8@44R(2' &|
   ')|M*~Courier Cost~@n10.2@52R(2)|M*~ARC Lab Cost~@n10.2@55R(2)|M*~ARC Parts Cost~' &|
   '@n10.2@52R(2)|M*~RRC Lab Cost~@n14.2@55R(2)|M*~RRC Parts Cost~@n14.2@49R(2)|M*~C' &|
   'ourier Cost~@n14.2@47R(2)|M*~Labour Cost~@n14.2@42R(2)|M*~Parts Cost~@n14.2@42L(' &|
   '2)|M*~Invoice Type~@s3@43R(2)|M*~Total Cost~@n14.2@72L(2)|M*~Invoice Credit~@s3@'),FROM(Queue:Browse:1)
                       ENTRY(@s8),AT(8,44,64,10),USE(inv:Invoice_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                       SHEET,AT(4,28,672,234),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Invoice Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(8,230),USE(?Reprint_Invoice),TRN,FLAT,ICON('rprninvp.jpg')
                           BUTTON,AT(80,230),USE(?ReprintRRCInvoice),TRN,FLAT,ICON('rprnrrcp.jpg')
                           BUTTON,AT(152,230),USE(?Reprint_Invoice:2),TRN,FLAT,ICON('rprnbatp.jpg')
                           BUTTON,AT(224,230),USE(?ResubmitToLine500),TRN,FLAT,HIDE,ICON('relinep.jpg')
                           BUTTON,AT(296,230),USE(?buttonResubmitCSV),TRN,FLAT,HIDE,ICON('csv500p.jpg')
                           BUTTON,AT(604,130),USE(?AmendInvoice),TRN,FLAT,ICON('editp.jpg')
                           PROMPT('- Credit Note'),AT(500,44),USE(?Prompt2),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(604,162),USE(?Remove_Invoice),TRN,FLAT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(608,384),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       SHEET,AT(4,264,672,116),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Tab 3'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(420,272,252,102),USE(?Group:CreditNote),HIDE
                             PROMPT('Credit Notes / Credited Invoices'),AT(428,276),USE(?Prompt6),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             LIST,AT(428,284,176,90),USE(?List:3),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('71L(2)|M*~Invoice Number~@s30@33L(2)|M*~Type~@s7@40R|M*~Date Created~R(2)@d6@'),FROM(Queue:Browse:3)
                             BUTTON,AT(608,314),USE(?Button:PrintInvoice),TRN,FLAT,ICON('printp.jpg')
                           END
                           PROMPT('Jobs Attached To The Selected Invoice'),AT(8,276),USE(?Prompt3),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(356,314),USE(?Change:Job),TRN,FLAT,LEFT,ICON('editp.jpg')
                           LIST,AT(8,286,340,90),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('35R(2)|M~Job No~L@s8@125L(2)|M~Model Number~@s30@64L(2)|M~ESN/IMEI~@s16@120L(2)|' &|
   'M~Unit Type~@s30@'),FROM(Queue:Browse)
                         END
                         TAB('Tab 4'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Retail Sales Attached To The Selected Invoice'),AT(8,278),USE(?Prompt4),TRN,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Gray)
                           LIST,AT(8,288,428,88),USE(?List:2),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('37R(2)|M~Sales No~@s8@120L(2)|M~Purchase Order Number~@s30@120L(2)|M~Consignment' &|
   ' Number~@s30@'),FROM(Queue:Browse:2)
                         END
                       END
                       BOX,AT(488,44,10,8),USE(?Box1:2),HIDE,COLOR(COLOR:Black),FILL(COLOR:Red)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
BRW13                CLASS(BrowseClass)               !Browse using ?List
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse                  !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW14                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
BRW18                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:3                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW18::Sort0:Locator StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_job_id   ushort,auto
windowsdir      Cstring(144)
systemdir       Cstring(255)
! #11983 Declare Import File (Bryan: 18/02/2011)
locSavePath         CSTRING(255)
locImportFile       CSTRING(255),STATIC
ImportFile    File,Driver('BASIC'),Pre(impfil),Name(locImportFile),Create,Bindable,Thread
Record              Record
InvoiceNumber           LONG()
InvoiceDate             STRING(30) ! Format @D8
PurchaseOrderNumber     LONG()
AccountNumber           STRING(30)
                    End
                End

locCSVFilename  CString(255)


    MAP
ResubmitToLine500       PROCEDURE(BYTE fShowErrors),Byte
    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepRealClass   !REAL            !Xplore: Column displaying inv:Invoice_Number
Xplore1Locator1      EntryLocatorClass                !Xplore: Column displaying inv:Invoice_Number
Xplore1Step2         StepStringClass !STRING          !Xplore: Column displaying inv:Account_Number
Xplore1Locator2      EntryLocatorClass                !Xplore: Column displaying inv:Account_Number
Xplore1Step3         StepCustomClass !DATE            !Xplore: Column displaying inv:Date_Created
Xplore1Locator3      EntryLocatorClass                !Xplore: Column displaying inv:Date_Created
Xplore1Step4         StepCustomClass !                !Xplore: Column displaying Invoice_Type_Temp
Xplore1Locator4      IncrementalLocatorClass          !Xplore: Column displaying Invoice_Type_Temp
Xplore1Step9         StepCustomClass !                !Xplore: Column displaying tmp:RRCLabourCost
Xplore1Locator9      EntryLocatorClass                !Xplore: Column displaying tmp:RRCLabourCost
Xplore1Step10        StepCustomClass !                !Xplore: Column displaying tmp:RRCPartsCost
Xplore1Locator10     EntryLocatorClass                !Xplore: Column displaying tmp:RRCPartsCost
Xplore1Step11        StepRealClass   !REAL            !Xplore: Column displaying inv:Courier_Paid
Xplore1Locator11     EntryLocatorClass                !Xplore: Column displaying inv:Courier_Paid
Xplore1Step12        StepRealClass   !REAL            !Xplore: Column displaying inv:Labour_Paid
Xplore1Locator12     EntryLocatorClass                !Xplore: Column displaying inv:Labour_Paid
Xplore1Step13        StepRealClass   !REAL            !Xplore: Column displaying inv:Parts_Paid
Xplore1Locator13     EntryLocatorClass                !Xplore: Column displaying inv:Parts_Paid
Xplore1Step14        StepStringClass !STRING          !Xplore: Column displaying inv:Invoice_Type
Xplore1Locator14     EntryLocatorClass                !Xplore: Column displaying inv:Invoice_Type
Xplore1Step15        StepRealClass   !REAL            !Xplore: Column displaying inv:Total_Claimed
Xplore1Locator15     EntryLocatorClass                !Xplore: Column displaying inv:Total_Claimed
Xplore1Step16        StepStringClass !STRING          !Xplore: Column displaying inv:InvoiceCredit
Xplore1Locator16     EntryLocatorClass                !Xplore: Column displaying inv:InvoiceCredit
Xplore13             CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore13Step1        StepLongClass   !LONG            !Xplore: Column displaying job:Ref_Number
Xplore13Locator1     IncrementalLocatorClass          !Xplore: Column displaying job:Ref_Number
Xplore13Step2        StepStringClass !STRING          !Xplore: Column displaying job:Model_Number
Xplore13Locator2     IncrementalLocatorClass          !Xplore: Column displaying job:Model_Number
Xplore13Step3        StepStringClass !STRING          !Xplore: Column displaying job:ESN
Xplore13Locator3     IncrementalLocatorClass          !Xplore: Column displaying job:ESN
Xplore13Step4        StepStringClass !STRING          !Xplore: Column displaying job:Unit_Type
Xplore13Locator4     IncrementalLocatorClass          !Xplore: Column displaying job:Unit_Type
Xplore13Step5        StepLongClass   !LONG            !Xplore: Column displaying job:Invoice_Number
Xplore13Locator5     IncrementalLocatorClass          !Xplore: Column displaying job:Invoice_Number
Xplore13Step6        StepLongClass   !LONG            !Xplore: Column displaying job:Invoice_Number_Warranty
Xplore13Locator6     IncrementalLocatorClass          !Xplore: Column displaying job:Invoice_Number_Warranty

  CODE
  GlobalResponse = ThisWindow.Run()

getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Browse_Invoice','?Browse:1')     !Xplore
  BRW1.SequenceNbr = 1                                !Xplore
  BRW1.ViewOrder   = True                             !Xplore
  Xplore1.RestoreHeader = True                        !Xplore
  Xplore1.AscDesc       = 0                           !Xplore
  Xplore13.GetBbSize('Browse_Invoice','?List')        !Xplore
  BRW13.SequenceNbr = 0                               !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020407'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Invoice')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RESUBMIT.Open
  Relate:RETSALES.Open
  Relate:USERS_ALIAS.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSE.UseFile
  Access:LOCATLOG.UseFile
  Access:SUBTRACC.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:INVOICE,SELF)
  BRW13.Init(?List,Queue:Browse.ViewPosition,BRW13::View:Browse,Queue:Browse,Relate:JOBS,SELF)
  BRW14.Init(?List:2,Queue:Browse:2.ViewPosition,BRW14::View:Browse,Queue:Browse:2,Relate:RETSALES,SELF)
  BRW18.Init(?List:3,Queue:Browse:3.ViewPosition,BRW18::View:Browse,Queue:Browse:3,Relate:JOBSINV,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Sheet2{prop:wizard} = 1
  
  tmp:ARCAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  
  ! Allow to resend an invoice message to Line500 - TrkBs: 5110 (DBH: 05-08-2005)
  If SecurityCheck('RESUBMIT TO LINE500') = False
      ?ResubmitToLine500{prop:Hide} = False
      ?buttonResubmitCSV{prop:Hide} = FALSE
  End ! If SecurityCheck('RESUBMIT TO LINE 500')
  ! End   - Allow to resend an invoice message to Line500 - TrkBs: 5110 (DBH: 05-08-2005)
  
  IF (glo:WebJob)
      ! #11983 Don't show if webmaster. (Bryan: 18/02/2011)
      ?buttonResubmitCSV{prop:Hide} = 1
  END
  ! Save Window Name
   AddToLog('Window','Open','Browse_Invoice')
  ?Browse:1{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbj01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  Xplore13.Init(ThisWindow,BRW13,Queue:Browse,QuickWindow,?List,'sbj01app.INI','>Header',0,BRW13.ViewOrder,Xplore13.RestoreHeader,BRW13.SequenceNbr,XploreMask13,XploreMask113,XploreTitle13,BRW13.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,inv:Invoice_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?inv:Invoice_Number,inv:Invoice_Number,1,BRW1)
  BIND('Invoice_Type_Temp',Invoice_Type_Temp)
  BIND('invoice_Credit_temp',invoice_Credit_temp)
  BIND('tmp:couriercost',tmp:couriercost)
  BIND('tmp:LabourCost',tmp:LabourCost)
  BIND('tmp:PartsCost',tmp:PartsCost)
  BIND('tmp:RRCLabourCost',tmp:RRCLabourCost)
  BIND('tmp:RRCPartsCost',tmp:RRCPartsCost)
  BRW1.AddField(inv:Invoice_Number,BRW1.Q.inv:Invoice_Number)
  BRW1.AddField(inv:Account_Number,BRW1.Q.inv:Account_Number)
  BRW1.AddField(inv:Date_Created,BRW1.Q.inv:Date_Created)
  BRW1.AddField(Invoice_Type_Temp,BRW1.Q.Invoice_Type_Temp)
  BRW1.AddField(invoice_Credit_temp,BRW1.Q.invoice_Credit_temp)
  BRW1.AddField(tmp:couriercost,BRW1.Q.tmp:couriercost)
  BRW1.AddField(tmp:LabourCost,BRW1.Q.tmp:LabourCost)
  BRW1.AddField(tmp:PartsCost,BRW1.Q.tmp:PartsCost)
  BRW1.AddField(tmp:RRCLabourCost,BRW1.Q.tmp:RRCLabourCost)
  BRW1.AddField(tmp:RRCPartsCost,BRW1.Q.tmp:RRCPartsCost)
  BRW1.AddField(inv:Courier_Paid,BRW1.Q.inv:Courier_Paid)
  BRW1.AddField(inv:Labour_Paid,BRW1.Q.inv:Labour_Paid)
  BRW1.AddField(inv:Parts_Paid,BRW1.Q.inv:Parts_Paid)
  BRW1.AddField(inv:Invoice_Type,BRW1.Q.inv:Invoice_Type)
  BRW1.AddField(inv:Total_Claimed,BRW1.Q.inv:Total_Claimed)
  BRW1.AddField(inv:InvoiceCredit,BRW1.Q.inv:InvoiceCredit)
  BRW13.Q &= Queue:Browse
  BRW13.RetainRow = 0
  BRW13.AddSortOrder(,job:InvoiceNumberKey)
  BRW13.AddRange(job:Invoice_Number,inv:Invoice_Number)
  BRW13.AddSortOrder(,job:WarInvoiceNoKey)
  BRW13.AddRange(job:Invoice_Number_Warranty,inv:Invoice_Number)
  BRW13.AddSortOrder(,job:InvoiceNumberKey)
  BRW13.AddRange(job:Invoice_Number,inv:Invoice_Number)
  BRW13.AddField(job:Ref_Number,BRW13.Q.job:Ref_Number)
  BRW13.AddField(job:Model_Number,BRW13.Q.job:Model_Number)
  BRW13.AddField(job:ESN,BRW13.Q.job:ESN)
  BRW13.AddField(job:Unit_Type,BRW13.Q.job:Unit_Type)
  BRW13.AddField(job:Invoice_Number,BRW13.Q.job:Invoice_Number)
  BRW13.AddField(job:Invoice_Number_Warranty,BRW13.Q.job:Invoice_Number_Warranty)
  BRW14.Q &= Queue:Browse:2
  BRW14.RetainRow = 0
  BRW14.AddSortOrder(,ret:Invoice_Number_Key)
  BRW14.AddRange(ret:Invoice_Number,inv:Invoice_Number)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,ret:Invoice_Number,1,BRW14)
  BRW14.AddField(ret:Ref_Number,BRW14.Q.ret:Ref_Number)
  BRW14.AddField(ret:Purchase_Order_Number,BRW14.Q.ret:Purchase_Order_Number)
  BRW14.AddField(ret:Consignment_Number,BRW14.Q.ret:Consignment_Number)
  BRW14.AddField(ret:Invoice_Number,BRW14.Q.ret:Invoice_Number)
  BRW18.Q &= Queue:Browse:3
  BRW18.RetainRow = 0
  BRW18.AddSortOrder(,jov:InvoiceNumberKey)
  BRW18.AddRange(jov:InvoiceNumber,inv:Invoice_Number)
  BRW18.AddLocator(BRW18::Sort0:Locator)
  BRW18::Sort0:Locator.Init(,jov:RecordNumber,1,BRW18)
  BIND('tmp:InvoiceNumber',tmp:InvoiceNumber)
  BIND('tmp:InvoiceType',tmp:InvoiceType)
  BRW18.AddField(tmp:InvoiceNumber,BRW18.Q.tmp:InvoiceNumber)
  BRW18.AddField(tmp:InvoiceType,BRW18.Q.tmp:InvoiceType)
  BRW18.AddField(jov:DateCreated,BRW18.Q.jov:DateCreated)
  BRW18.AddField(jov:RecordNumber,BRW18.Q.jov:RecordNumber)
  BRW18.AddField(jov:InvoiceNumber,BRW18.Q.jov:InvoiceNumber)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=256
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW13.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW13.AskProcedure = 0
      CLEAR(BRW13.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW14.AskProcedure = 0
      CLEAR(BRW14.AskProcedure, 1)
    END
  END
  BRW18.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW18.AskProcedure = 0
      CLEAR(BRW18.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RESUBMIT.Close
    Relate:RETSALES.Close
    Relate:USERS_ALIAS.Close
    Relate:WEBJOB.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
    Xplore13.EraseVisual()                            !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Browse_Invoice','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisWindow.Opened                                !Xplore
    Xplore13.sq.Col = Xplore13.CurrentCol
    GET(Xplore13.sq,Xplore13.sq.Col)
    IF Xplore13.ListType = 1 AND BRW13.ViewOrder = False !Xplore
      BRW13.SequenceNbr = 0                           !Xplore
    END                                               !Xplore
    Xplore13.PutInix('Browse_Invoice','?List',BRW13.SequenceNbr,Xplore13.sq.AscDesc) !Xplore
  END                                                 !Xplore
  ! Save Window Name
   AddToLog('Window','Close','Browse_Invoice')
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  Xplore13.Kill()                                     !Xplore
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  If Field() <> ?Change:Job
      case request
          of changerecord
              check_access('AMEND INVOICE',x")
              if x" = false
                  Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  do_update# = false
              Else!if x" = false
                  Case Missive('Are you sure you want to amend this invoice?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          do_update# = false
                  End ! Case Missive
              end!if x" = false
      end !case request
  
  End!If Field() <> ?Change:Job
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJOBS
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  Xplore13.Upper = True                               !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(inv:Invoice_Number,BRW1.Q.inv:Invoice_Number)
  Xplore1.AddField(inv:Account_Number,BRW1.Q.inv:Account_Number)
  Xplore1.AddField(inv:Date_Created,BRW1.Q.inv:Date_Created)
  Xplore1.AddField(tmp:couriercost,BRW1.Q.tmp:couriercost)
  Xplore1.AddField(tmp:LabourCost,BRW1.Q.tmp:LabourCost)
  Xplore1.AddField(tmp:PartsCost,BRW1.Q.tmp:PartsCost)
  Xplore1.AddField(inv:Courier_Paid,BRW1.Q.inv:Courier_Paid)
  Xplore1.AddField(inv:Labour_Paid,BRW1.Q.inv:Labour_Paid)
  Xplore1.AddField(inv:Parts_Paid,BRW1.Q.inv:Parts_Paid)
  Xplore1.AddField(inv:Invoice_Type,BRW1.Q.inv:Invoice_Type)
  Xplore1.AddField(inv:Total_Claimed,BRW1.Q.inv:Total_Claimed)
  Xplore1.AddField(inv:InvoiceCredit,BRW1.Q.inv:InvoiceCredit)
  BRW1.FileOrderNbr = BRW1.AddSortOrder()             !Xplore Sort Order for File Sequence
  BRW1.SetOrder('')                                   !Xplore
  Xplore1.AddAllColumnSortOrders(0)                   !Xplore
  Xplore13.AddField(job:Ref_Number,BRW13.Q.job:Ref_Number)
  Xplore13.AddField(job:Model_Number,BRW13.Q.job:Model_Number)
  Xplore13.AddField(job:ESN,BRW13.Q.job:ESN)
  Xplore13.AddField(job:Unit_Type,BRW13.Q.job:Unit_Type)
  Xplore13.AddField(job:Invoice_Number,BRW13.Q.job:Invoice_Number)
  Xplore13.AddField(job:Invoice_Number_Warranty,BRW13.Q.job:Invoice_Number_Warranty)
  BRW13.FileOrderNbr = BRW13.AddSortOrder(,job:InvoiceNumberKey) !Xplore Sort Order for File Sequence
  BRW13.AddRange(job:Invoice_Number,inv:Invoice_Number) !Xplore
  BRW13.SetOrder('')                                  !Xplore
  BRW13.ViewOrder = True                              !Xplore
  Xplore13.AddAllColumnSortOrders(1)                  !Xplore
  BRW13.ViewOrder = False                             !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020407'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020407'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020407'&'0')
      ***
    OF ?Reprint_Invoice
      ThisWindow.Update
      Brw1.UpdateViewRecord()
      glo:Select1  = inv:invoice_number
      glo:Select2  = inv:batch_number
      glo:Select3  = inv:account_number
      
      Case inv:invoice_type
          Of 'CHA'
              Chargeable_Summary_Invoice
          Of 'SIN'
              Access:JOBS.Clearkey(job:InvoiceNumberKey)
              job:Invoice_NUmber  = inv:Invoice_NUmber
              If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
                  !Found
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Found
      
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Error
                  End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                  If ~SentToHub(job:ref_Number)
                      Case Missive('The selected invoice is not for an ARC job.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else
                      !Single_Invoice('','')
                      VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')
                  End !If ~SentToHub(job:ref_Number)
              Else ! If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
                  !Error
              End !If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
          Of 'WAR'
              Summary_Warranty_Invoice
          Of 'RET'
              Retail_Single_Invoice(1,inv:Invoice_Number)   !1 implies this is a reprint
          Of 'BAT'
              Chargeable_Batch_Invoice
          Of 'WRC'
              RRCWarrantyInvoice(inv:Invoice_Number)
      End
      glo:Select1 = ''
      glo:Select2 = ''
      glo:Select3 = ''
    OF ?ReprintRRCInvoice
      ThisWindow.Update
      Brw1.UpdateViewRecord()
      glo:Select1  = inv:invoice_number
      glo:Select2  = inv:batch_number
      glo:Select3  = inv:account_number
      
      Case inv:invoice_type
          Of 'SIN'
              Access:JOBS.Clearkey(job:InvoiceNumberKey)
              job:Invoice_Number  = inv:Invoice_Number
              If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
                  !Found
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Found
                      If ~jobe:WebJob
                          Case Missive('The selected invoice is not for a RRC job.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                      Else
                          ! Inserting (DBH 22/0/2007) # 8744 - If the RRC hasn't created the invoice, don't let the ARC create it
                          If inv:RRCInvoiceDate = 0
                              Case Missive('The RRC have not yet created an invoice.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Cycle
                          End ! If inv:RRCInvoiceDate = 0
                          ! End (DBH 22/03/2007) #8744
      !! Changing (DBH 21/05/2007) # 9024 - Allow to print different invoices
      !!                    Vodacom_Delivery_Note_Web
      !! to (DBH 21/05/2007) # 9024
      !                    If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
      !                        Vodacom_Delivery_Note_Web('','')
      !                    Else ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI')
      !                        glo:WebJob = 1
      !                        Single_Invoice('','')
      !                        glo:WebJob = 0
      !                    End ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI')
      !! End (DBH 21/05/2007) #9024
                          glo:WebJob = 1 ! Fake RRC
                          VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','') ! #11881 New Single Invoice (Bryan: 16/03/2011)
                          glo:WebJob = 0
                      End !If ~jobe:WebJob
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              Else ! If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
                  !Error
              End !If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
          Else
              Case Missive('The selected invoice is not a RRC Chargeable Invoice.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
      End
      glo:Select1 = ''
      glo:Select2 = ''
      glo:Select3 = ''
    OF ?Reprint_Invoice:2
      ThisWindow.Update
      glo:file_name   = 'I' & FOrmat(Clock(),@n_7) & '.TMP'
      Invoice_Date_Range
      Remove(tradetmp)
      If error()
          access:tradetmp.close()
          Remove(tradetmp)
      End!If error()
      
    OF ?ResubmitToLine500
      ThisWindow.Update
      Case Missive('Are you sure you want to resubmit the selected invoice to Line500?', 'ServiceBase 3g', |
                   'mquest.jpg', '\No|/Yes')
      Of 2 ! Yes Button
          Brw1.UpdateViewRecord()
      
          x# = ResubmitToLine500(1)    ! #11983 Move to procedure (Bryan: 18/02/2011)
      
      Of 1 ! No Button
      End ! Case Missive
    OF ?buttonResubmitCSV
      ThisWindow.Update
      locSavePath = Path()
      locImportFile = ''
      if (fileDialog('Choose File',locImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName))
          !Found
          setPath(locSavePath)
      
      else ! if (fileDialog)
          !Error
          setPath(locSavePath)
          Cycle
      end ! if (fileDialog)
      
      Open(ImportFile)
      IF (ERROR())
          STOP(Error())
          CYCLE
      END
      
      count# = 0
      Set(ImportFile,0)
      LOOP
          Next(ImportFile)
          IF (Error())
              BREAK
          END
          count# += 1
      END
      
      locCSVFileName = CLIP(GETINI('MQ','CSVImportFolder',,PATH() & '\SB2KDEF.INI')) & '\' & |
                      Format(DAY(TODAY()),@n02) & Format(MONTH(TODAY()),@n02) & YEAR(TODAY()) & ' ' |
                      & Format(CLock(),@t05) & ' Resubmitted.txt'
      
      fileCreated# = 0
      prog.ProgressSetup(count#)
      Set(ImportFile,0)
      LOOP
          Next(ImportFile)
          IF (Error())
              BREAK
          END
          IF (prog.InsideLoop())
              BREAK
          END
      
          Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
          inv:Invoice_Number = impfil:InvoiceNumber
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
              IF (ResubmitToLine500(0) = 1)
                  ! Submitted ok
                  LinePrint(Format(Today(),@d06) & ' Invoice ' & clip(inv:Invoice_Number) & |
                              ' Account ' & Clip(inv:Account_Number) & ' - Resubmitted',locCSVFilename)
                  fileCreated# = 1
              END
          END! IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
      
      END
      
      prog.ProgressFinish()
      
      
      Close(ImportFile)
      
      IF (fileCreated# = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Nothing was imported.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
      ELSE
          Beep(Beep:SystemAsterisk)  ;  Yield()
          Case Missive('File imported. Confirmation file created:'&|
              '|'&|
              '|' & Clip(clip(locCSVFilename)) & '','ServiceBase',|
                         'midea.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
      END
    OF ?AmendInvoice
      ThisWindow.Update
      Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
      inv:Invoice_Number = brw1.q.inv:Invoice_Number
      If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
          !Found
          If inv:Invoice_Type <> 'SIN'
              Case Missive('You can only amend Chargeable Invoices.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Else !If inv:Invoice_Type <> 'SIN'
              If SecurityCheck('AMEND INVOICED CHARGEABLE COST')
                  NoAccess('AMEND INVOICED CHARGEABLE COST')
              Else !If SecurityCheck('AMEND INVOICED CHARGEBLE COSTS')
                  GlobalRequest = ChangeRecord
                  UpdateChaInvoice
              End !If SecurityCheck('AMEND INVOICED CHARGEBLE COSTS')
          End !If inv:Invoice_Type <> 'SIN'
      Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
          !Error
      End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    OF ?Remove_Invoice
      ThisWindow.Update
      thiswindow.reset(1)
      do_update# = 1
      check_access('REMOVE INVOICE',x")
      if x" = false
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
        do_update# = 0
      end
      If do_update# = 1
          Case Missive('Are you sure you want to remove invoice number ' & Clip(inv:Invoice_Number) & '?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
      
                  access:invoice.clearkey(inv:invoice_number_key)
                  inv:invoice_number  = brw1.q.inv:invoice_number
                  If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
      
                      setcursor(cursor:wait)
      
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      open(progresswindow)
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:text} = '0% Completed'
      
                      Case inv:invoice_type
                          Of 'WRC'
                              RecordsToProcess = Records(wob:RRCWInvoiceNumberKey)
                              Save_wob_ID = Access:WEBJOB.SaveFile()
                              Access:WEBJOB.ClearKey(wob:RRCWInvoiceNumberKey)
                              wob:RRCWInvoiceNumber = inv:Invoice_Number
                              Set(wob:RRCWInvoiceNumberKey,wob:RRCWInvoiceNumberKey)
                              Loop
                                  If Access:WEBJOB.NEXT()
                                     Break
                                  End !If
                                  If wob:RRCWInvoiceNumber <> inv:Invoice_Number      |
                                      Then Break.  ! End If
                                  Do GetNextRecord2
                                  pos = Position(wob:RRCWInvoiceNumberKey)
                                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                  jobe:RefNumber = wob:RefNumber
                                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                      !Found
                                      jobe:InvRRCWLabourCost = 0
                                      jobe:InvRRCWPartsCost = 0
                                      Access:JOBSE.Update()
                                  Else !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                      !Error
                                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                  wob:EDI = 'APP'
                                  wob:RRCWInvoiceNumber = 0
                                  Access:WEBJOB.Update()
                                  Reset(wob:RRCWInvoiceNumberKey,pos)
      
                              End !Loop
                              Access:WEBJOB.RestoreFile(Save_wob_ID)
      
                              Delete(Invoice)
                          Of 'WAR'
                              recordstoprocess    = Records(job:chainvoicekey)
      
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:warinvoicenokey)
                              job:invoice_number_warranty = inv:invoice_number
                              set(job:warinvoicenokey,job:warinvoicenokey)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:invoice_number_warranty <> inv:invoice_number      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
      
                                  Do GetNextRecord2
      
                                  pos = Position(job:warinvoicenokey)
                                  JOB:WInvoice_Courier_Cost=''
                                  JOB:WInvoice_Labour_Cost=''
                                  JOB:WInvoice_Parts_Cost=''
                                  JOB:WInvoice_Sub_Total=''
                                  JOB:Invoice_Date_Warranty=''
                                  JOB:Invoice_Number_Warranty=''
                                  job:edi = 'YES'
                                  access:jobs.update()
                                  Reset(job:warinvoicenokey,pos)
                              end !loop
                              access:jobs.restorefile(save_job_id)
      
                              Delete(invoice)
      
                          Else
                              recordstoprocess    = Records(job:chainvoicekey)
      
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:InvoiceNumberKey)
                              job:invoice_number = inv:invoice_number
                              set(job:InvoiceNumberKey,job:InvoiceNumberKey)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:invoice_number <> inv:invoice_number      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
      
                                  Do GetNextRecord2
      
                                  pos = Position(job:InvoiceNumberKey)
                                  JOB:Invoice_Sub_Total=''
                                  JOB:Invoice_Parts_Cost=''
                                  JOB:Invoice_Labour_Cost=''
                                  JOB:Invoice_Courier_Cost=''
                                  JOB:Invoice_Date=''
                                  JOB:Invoice_Number=''
                                  Job:InvoiceBatch    = ''
                                  Job:InvoiceStatus   = ''
                                  Job:InvoiceQuery    = ''
                                  Job:InvoiceAccount  = ''
      
                                  access:jobs.update()
                                  Reset(job:InvoiceNumberKey,pos)
      Compile('***',Debug=1)
          message('After Update','Debug Message', icon:exclamation)
      ***
      
                              end !loop
                              access:jobs.restorefile(save_job_id)
      
                              Delete(invoice)
      
                      End!Case inv:invoice_type
      
                      setcursor()
                      close(progresswindow)
                  End!If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
              Of 1 ! No Button
          End ! Case Missive
      End!If do_update# = True
      BRW1.ResetQueue(1)
      Select(?browse:1)
    OF ?Button:PrintInvoice
      ThisWindow.Update
      Brw18.UpdateViewRecord()
      If jov:InvoiceNumber <> ''
          glo:Select1 = jov:InvoiceNumber
      
          glo:WebJob = 1 ! Fake RRC
          If (jov:Type = 'C')
              VodacomSingleInvoice(jov:InvoiceNumber,jov:RefNumber,'CN',jov:Suffix)   ! #11881 New Single Invoice (Bryan: 16/03/2011)
          ELSE
              VodacomSingleInvoice(jov:InvoiceNumber,jov:RefNumber,'',jov:Suffix)    ! #11881 New Single Invoice (Bryan: 16/03/2011)
          END
          glo:WebJob = 0
      !    If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
      !        If jov:Type = 'C'
      !            Vodacom_Delivery_Note_Web('CN',jov:Suffix)
      !        Else ! If jov:Type = 'C'
      !            Vodacom_Delivery_Note_Web('',jov:Suffix)
      !        End ! If jov:Type = 'C'
      !    Else ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI')
      !        glo:WebJob = 1
      !        If jov:Type = 'C'
      !            Single_Invoice('CN',jov:Suffix)
      !        Else ! If jov:Type = 'C'
      !            Single_Invoice('',jov:Suffix)
      !        End ! If jov:Type = 'C'
      !        glo:WebJob = 0
      !    End ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI')
          glo:Select1 = ''
      End ! If jov:InvoiceNumber <> ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore13.IgnoreEvent = True                      !Xplore
     Xplore13.IgnoreEvent = False                     !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  tmp:filter  = brw1::view:browse{prop:filter}
  tmp:order   = brw1::view:browse{prop:order}
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    Case inv:invoice_type
        Of 'RET'
            If Choice(?Sheet2) <> 2
                Select(?Sheet2,2)
            End!If Case(?Sheet2) <> 2
        Else
            If Choice(?sheet2) <> 1
                Select(?sheet2,1)
            End!If Case(?sheet2) <> 1
    End!Case inv:invoice_type
    
    ! Inserting (DBH 02/08/2007) # 9141 - Show the credit note list, if they exist
    ?Group:CreditNote{prop:Hide} = 1
    Access:JOBSINV.Clearkey(jov:InvoiceNumberKey)
    jov:InvoiceNumber = inv:Invoice_Number
    Set(jov:InvoiceNumberKey,jov:InvoiceNumberKey)
    Loop ! Begin Loop
        If Access:JOBSINV.Next()
            Break
        End ! If Access:JOBSINV.Next()
        If jov:InvoiceNumber <> inv:Invoice_Number
            Break
        End ! If jov:InvoiceNumber <> inv:Invoice_Number
        ?Group:CreditNote{prop:Hide} = 0
        Break
    End ! Loop
    tmp:TradeID = ''
    Access:JOBS.ClearKey(job:InvoiceNumberKey)
    job:Invoice_Number = inv:Invoice_Number
    If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
        !Found
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Found
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                tmp:TradeID = tra:BranchIdentification
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    
        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Error
        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
    
    Else ! If Access:JOBS.TryFetch(job:Invoice_Number_Key) = Level:Benign
        !Error
    End ! If Access:JOBS.TryFetch(job:Invoice_Number_Key) = Level:Benign
    
    
    ! End (DBH 02/08/2007) #9141
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Set(Defaults)
      access:defaults.next()
      If def:use_sage = 'YES'
      !    Hide(?reprint_invoice)
          Hide(?remove_invoice)
      !    Unhide(?create_sage_invoice)
      End!If def:use_sage = 'YES'
      Compile('***',Debug=1)
          Unhide(?tmp:filter)
          Unhide(?tmp:order)
      ***
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      POST(xpEVENT:Xplore + 13)                       !Xplore
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      Xplore13.GetColumnInfo()                        !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ResubmitToLine500       PROCEDURE(BYTE fShowErrors)
ReturnValue     Byte(0)
    CODE

    glo:ErrorText = ''      !TB13517 - J added 22/04/15 so we can track what goes wrong in Line500 export

    Case inv:Invoice_Type
    Of 'SIN'
        Access:JOBS.Clearkey(job:InvoiceNumberKey)
        job:Invoice_Number  = inv:Invoice_Number
        If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
            ! Found
            ! Double check that all the correct files are open (DBH: 05-08-2005)

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Found

            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Error
            End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            ! Found

            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            ! Error
            End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            If ~SentToHub(job:Ref_Number)
                ! Inserting (DBH 11/23/2005) #6774 - Cannot resubmit a non-generic, non company owned invoiced. No original message would of been produced
                AOWMessage# = False
                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = job:Account_Number
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    ! Found
                    If sub:Generic_Account <> True
                        ! This is not a Generic Account so no message, except for "Company Owned" franchises which produce an AOW - TrkBs: 6774 (DBH: 22-11-2005)
                        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                        tra:Account_Number  = wob:HeadAccountNumber
                        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                            ! Found
                            If tra:CompanyOwned <> True
                                IF (fShowErrors)
                                    Case Missive('Error! You cannot resubmit a message for this invoice.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                        Of 1 ! OK Button
                                    End ! Case Missive
                                END
                                RETURN 0
                            Else ! If tra:CompanyOwned = True
                                AOWMessage# = True
                            End ! If tra:CompanyOwned = True
                        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                            ! Error
                        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    End ! If sub:Generic_Account <> True
                Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    ! Error
                End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                ! End (DBH 11/23/2005) #6774

                ! Created RIV
                glo:WebJob = 1
                ! Fake the fact that it is an RRC reprinting the invoice - TrkBs: 5110 (DBH: 10-08-2005)
                Line500_XML('RIV')
                if clip(glo:ErrorText) <> '' then
                    !TB13517 - J added 22/04/15 so we can track what goes wrong in Line500 export
                    miss# = missive('ERROR IN XLM EXPORT:|'&clip(glo:ErrorText),'ServiceBase 3g','mexclam.jpg', '/OK')
                END
                glo:WebJob = 0
                ! Inserting Code (DBH, 11/16/2005) - #6724 Record resubmission in new "history" file
                If Access:RESUBMIT.PrimeRecord() = Level:Benign
                    Access:USERS.Clearkey(use:Password_Key)
                    use:Password    = glo:Password
                    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        ! Found

                    Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        ! Error
                    End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                    reb:Usercode = use:User_Code
                    reb:MessageType = 'RIV'

! Deleted (DBH 11/23/2005) #6774 - Company Owned check has already been done
!                     ! Check if this is a company owned RRC and show be producing an AOW instead (DBH: 16-11-2005)
!                     save_TRADEACC_id = Access:TRADEACC.SaveFile()
!                     Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!                     tra:Account_Number  = wob:HeadAccountNumber
!                     If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!                         ! Found
!                         If tra:CompanyOwned
!                             save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
!                             Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
!                             sub:Account_Number  = job:Account_Number
!                             If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!                                 ! Found
!                                 If sub:Generic_Account <> True
!                                     reb:MessageType = 'AOW'
!                                 End ! If sub:Generic_Account <> True
!                             Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!                                 ! Error
!                             End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!                             Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)
!                         End ! If tra:CompanyOwned
!                     Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!                         ! Error
!                     End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!                     Access:TRADEACC.RestoreFile(save_TRADEACC_id)
! End (DBH 11/23/2005) #6774

                    ! Inserting (DBH 11/23/2005) #6774 - Is this a company owned franchise?
                    If AOWMessage# = True
                        reb:MessageType = 'AOW'
                    End ! If AOWMessage# = True
                    ! End (DBH 11/23/2005) #6774

                    reb:Notes = 'INVOICE NO: ' & Clip(inv:Invoice_Number) & '. JOB NO: ' & job:Ref_Number
                    If Access:RESUBMIT.TryInsert() = Level:Benign
                        ! Insert Successful
                    Else ! If Access:RESUBMIT.TryInsert() = Level:Benign
                        ! Insert Failed
                        Access:RESUBMIT.CancelAutoInc()
                    End ! If Access:RESUBMIT.TryInsert() = Level:Benign
                End !If Access:RESUBMIT.PrimeRecord() = Level:Benign
                ! End (DBH, 11/16/2005) - #6724
                IF (fShowErrors)
                    Case Missive('Done.', 'ServiceBase 3g', |
                                 'mexclam.jpg', '/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                END ! IF (fShowErrors)
                ReturnValue = 1
            Else ! If ~SentToHub(job:Ref_Number)
                IF (fShowErrors)
                    rtn# = Missive('Do you wish to resubmit the ARC or RRC invoice to Line 500?', 'ServiceBase 3g', |
                                 'mquest.jpg', '\Cancel|RRC|ARC')
                ELSE
                    ! #11983 If automated, assume its the ARC invoice being resubmitted. (Bryan: 18/02/2011)
                    rtn# = 3
                END

                Case rtn#
                Of 3 ! ARC Button
                    ! AOW
                    Line500_XML('AOW')
                    if clip(glo:ErrorText) <> '' then
                        !TB13517 - J added 22/04/15 so we can track what goes wrong in Line500 export
                        miss# = missive('ERROR IN XLM EXPORT:|'&clip(glo:ErrorText),'ServiceBase 3g','mexclam.jpg', '/OK')
                    END

                    ! Inserting Code (DBH, 11/16/2005) - #6724 Record resubmission in new "history" file
                    If Access:RESUBMIT.PrimeRecord() = Level:Benign
                        Access:USERS.Clearkey(use:Password_Key)
                        use:Password    = glo:Password
                        If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                            ! Found

                        Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                            ! Error
                        End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        reb:Usercode = use:User_Code
                        reb:MessageType = 'AOW'
                        reb:Notes = 'INVOICE NO: ' & Clip(inv:Invoice_Number) & '. JOB NO: ' & job:Ref_Number
                        If Access:RESUBMIT.TryInsert() = Level:Benign
                            ! Insert Successful
                        Else ! If Access:RESUBMIT.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:RESUBMIT.CancelAutoInc()
                        End ! If Access:RESUBMIT.TryInsert() = Level:Benign
                    End !If Access:RESUBMIT.PrimeRecord() = Level:Benign
                    ! End (DBH, 11/16/2005) - #6724
                    IF (fShowErrors)
                        Case Missive('Done.', 'ServiceBase 3g', |
                                     'mexclam.jpg', '/OK')
                        Of 1 ! OK Button
                        End ! Case Missive
                    END ! IF (fShowErrors)
                    ReturnValue = 1
                Of 2 ! RRC Button
                    ! RIV
                    ! Inserting (DBH 11/23/2005) #6774 - Cannot resubmit a non-generic, non company owned invoiced. No original message would of been produced
                    AOWMessage# = False
                    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                    sub:Account_Number  = job:Account_Number
                    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                        ! Found
                        If sub:Generic_Account <> True
                            ! This is not a Generic Account so no message, except for "Company Owned" franchises which produce an AOW - TrkBs: 6774 (DBH: 22-11-2005)
                            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                            tra:Account_Number  = wob:HeadAccountNumber
                            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                ! Found
                                If tra:CompanyOwned <> True
                                    IF (fShowErrors)
                                        Case Missive('Error! You cannot resubmit a message for this invoice.','ServiceBase 3g',|
                                                       'mstop.jpg','/OK')
                                            Of 1 ! OK Button
                                        End ! Case Missive
                                    END ! IF (fShowErrors)
                                    RETURN 0
                                Else ! If tra:CompanyOwned = True
                                    AOWMessage# = True
                                End ! If tra:CompanyOwned = True
                            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                ! Error
                            End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        End ! If sub:Generic_Account <> True
                    Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                        ! Error
                    End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    ! End (DBH 11/23/2005) #6774

                    glo:WebJob = 1
                    ! Fake the fact that it is an RRC reprinting the invoice - TrkBs: 5110 (DBH: 10-08-2005)
                    Line500_XML('RIV')
                    if clip(glo:ErrorText) <> '' then
                        !TB13417 - J added 22/04/15 so we can track what goes wrong in Line500 export
                        miss# = missive('ERROR IN XLM EXPORT:|'&clip(glo:ErrorText),'ServiceBase 3g','mexclam.jpg', '/OK')
                    END

                    glo:WebJob = 0
                    ! Inserting Code (DBH, 11/16/2005) - #6724 Record resubmission in new "history" file
                    If Access:RESUBMIT.PrimeRecord() = Level:Benign
                        Access:USERS.Clearkey(use:Password_Key)
                        use:Password    = glo:Password
                        If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                            ! Found

                        Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                            ! Error
                        End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        reb:Usercode = use:User_Code
                        reb:MessageType = 'RIV'

! Deleted (DBH 11/23/2005) #6774 - Company Owned check has already been done
!                         ! Check if this is a company owned RRC and show be producing an AOW instead (DBH: 16-11-2005)
!                         save_TRADEACC_id = Access:TRADEACC.SaveFile()
!                         Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!                         tra:Account_Number  = wob:HeadAccountNumber
!                         If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!                             ! Found
!                             If tra:CompanyOwned
!                                 save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
!                                 Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
!                                 sub:Account_Number  = job:Account_Number
!                                 If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!                                     ! Found
!                                     If sub:Generic_Account <> True
!                                         reb:MessageType = 'AOW'
!                                     End ! If sub:Generic_Account <> True
!                                 Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!                                     ! Error
!                                 End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!                                 Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)
!                             End ! If tra:CompanyOwned
!                         Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!                             ! Error
!                         End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!                         Access:TRADEACC.RestoreFile(save_TRADEACC_id)
! End (DBH 11/23/2005) #6774

                        ! Inserting (DBH 11/23/2005) #6774 - Is this a company owned franchise?
                        If AOWMessage# = True
                            reb:MessageType = 'AOW'
                        End ! If AOWMessage# = True
                        ! End (DBH 11/23/2005) #6774

                        reb:Notes = 'INVOICE NO: ' & Clip(inv:Invoice_Number) & '. JOB NO: ' & job:Ref_Number
                        If Access:RESUBMIT.TryInsert() = Level:Benign
                            ! Insert Successful
                        Else ! If Access:RESUBMIT.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:RESUBMIT.CancelAutoInc()
                        End ! If Access:RESUBMIT.TryInsert() = Level:Benign
                    End !If Access:RESUBMIT.PrimeRecord() = Level:Benign
                    ! End (DBH, 11/16/2005) - #6724
                    IF (fShowErrors)
                        Case Missive('Done.', 'ServiceBase 3g', |
                                     'mexclam.jpg', '/OK')
                        Of 1 ! OK Button
                        End ! Case Missive
                    END ! IF (fShowErrors)
                    ReturnValue = 1
                Of 1 ! Cancel Button
                End ! Case Missive

            End ! If ~SentToHub(job:Ref_Number)
        Else ! If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
        ! Error
        End ! If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
    Of 'RET'
        ! STF
        Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
        ret:Invoice_Number = inv:Invoice_Number
        If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
            ! Found
            Line500_XML('STF')
            if clip(glo:ErrorText) <> '' then
                !TB13417 - J added 22/04/15 so we can track what goes wrong in Line500 export
                miss# = missive('ERROR IN XLM EXPORT:|'&clip(glo:ErrorText),'ServiceBase 3g','mexclam.jpg', '/OK')
            END

            ! Inserting Code (DBH, 11/16/2005) - #6724 Record resubmission in new "history" file
            If Access:RESUBMIT.PrimeRecord() = Level:Benign
                Access:USERS.Clearkey(use:Password_Key)
                use:Password    = glo:Password
                If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                    ! Found

                Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                    ! Error
                End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                reb:Usercode = use:User_Code
                reb:MessageType = 'STF'
                reb:Notes = 'INVOICE NO: ' & Clip(inv:Invoice_Number) & '. SALE NO: ' & ret:Ref_Number
                If Access:RESUBMIT.TryInsert() = Level:Benign
                    ! Insert Successful
                Else ! If Access:RESUBMIT.TryInsert() = Level:Benign
                    ! Insert Failed
                    Access:RESUBMIT.CancelAutoInc()
                End ! If Access:RESUBMIT.TryInsert() = Level:Benign
            End !If Access:RESUBMIT.PrimeRecord() = Level:Benign
            ! End (DBH, 11/16/2005) - #6724
            IF (fShowErrors)
                if clip(glo:ErrorText) <> '' then
                    !TB13417 - J added 22/04/15 so we can track what goes wrong in Line500 export
                    miss# = missive('ERROR IN XLM EXPORT:|'&clip(glo:ErrorText),'ServiceBase 3g','mexclam.jpg', '/OK')
                ELSE
                    miss# = Missive('DONE', 'ServiceBase 3g','mexclam.jpg', '/OK')

                End !IF GLO:ERRORtest blank
            END ! IF (fShowErrors)
            ReturnValue = 1
        Else ! If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
        ! Error
        End ! If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign

    End ! Case inv:Invoice_Type

    RETURN ReturnValue
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  Case inv:invoice_Type
      Of 'SIN' !Single Invoice
          access:jobs.clearkey(job:invoicenumberkey)
          job:invoice_number  = inv:invoice_number
          If access:jobs.tryfetch(job:invoicenumberkey) = Level:Benign
              !Found
              If SentToHub(job:Ref_Number)
                  tmp:labourcost  = job:invoice_labour_cost
                  tmp:partscost   = job:invoice_parts_cost
                  tmp:couriercost = job:invoice_courier_cost
  
              Else !If SentToHub(job:Ref_Number)
                  tmp:labourcost  = 0
                  tmp:partscost   = 0
                  tmp:couriercost = 0
              End !If SentToHub(job:Ref_Number)
  
              Access:JOBSE.Clearkey(jobe:RefNumberKey)
              jobe:RefNumber  = job:Ref_Number
              If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  !Found
                  If jobe:WebJob
                      tmp:RRCLabourCost   = jobe:InvRRCCLabourCost
                      tmp:RRCPartsCost    = jobe:InvRRCCPartsCost
                  Else
                      tmp:RRCLabourCost   = 0
                      tmp:RRCPartsCost    = 0
                  End !If jobe:WebJob
              Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  !Error
              End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  
          Else! If access:jobs.tryfetch(job:invoice_number_key) = Level:Benign
              !Error
          End! If access:.tryfetch(job:invoice_number_key) = Level:Benign
      Of 'WRC' !RRC Warranty Invoice
          tmp:LabourCost  = 0
          tmp:PartsCost   = 0
          tmp:CourierCost = 0
          tmp:RRCLabourCost   = inv:Labour_Paid
          tmp:RRCPartsCost    = inv:Parts_Paid
      Of 'RET' !Retail Invoice
          tmp:LabourCost  = 0
          tmp:RRCLabourCost = 0
          tmp:PartsCost   = 0
          tmp:RRCPartsCost = inv:Parts_Paid
  
          !Is this an ARC Account?
          Access:TRADEACC.ClearKey(tra:StoresAccountKey)
          tra:StoresAccount = inv:Account_Number
          If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign
              !Found
              If tra:Account_Number = tmp:ARCAccount
                  tmp:PartsCost = inv:Parts_Paid
                  tmp:RRCPartsCost = 0
              End !If tra:Account_Number = tmp:ARCAccount
          Else !If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign
              !Error
          End !If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign
      Else
          tmp:labourcost  = INV:Labour_Paid
          tmp:partscost   = INV:Parts_Paid
          tmp:couriercost = INV:Courier_Paid
          tmp:RRCLabourCost = 0
          tmp:RRCPartsCost = 0
  
  End!Case inv:invoice_Type
  
  CASE (inv:Invoice_Type)
  OF 'CHA'
    Invoice_Type_Temp = 'Chargeable'
  OF 'SIN'
    Invoice_Type_Temp = 'Chargeable'
  OF 'WAR'
    Invoice_Type_Temp = 'Warranty'
  OF 'RET'
    Invoice_Type_Temp = 'Retail'
  OF 'BAT'
    Invoice_Type_Temp = 'Chargeable'
  OF 'WRC'
    Invoice_Type_Temp = 'RRC Warr'
  ELSE
    Invoice_Type_Temp = 'Error'
  END
  CASE (inv:InvoiceCredit)
  OF 'INV'
    invoice_Credit_temp = 'Invoice'
  ELSE
    invoice_Credit_temp = 'Credit'
  END
  PARENT.SetQueueRecord
  SELF.Q.inv:Invoice_Number_NormalFG = -1
  SELF.Q.inv:Invoice_Number_NormalBG = -1
  SELF.Q.inv:Invoice_Number_SelectedFG = -1
  SELF.Q.inv:Invoice_Number_SelectedBG = -1
  SELF.Q.inv:Account_Number_NormalFG = -1
  SELF.Q.inv:Account_Number_NormalBG = -1
  SELF.Q.inv:Account_Number_SelectedFG = -1
  SELF.Q.inv:Account_Number_SelectedBG = -1
  SELF.Q.inv:Date_Created_NormalFG = -1
  SELF.Q.inv:Date_Created_NormalBG = -1
  SELF.Q.inv:Date_Created_SelectedFG = -1
  SELF.Q.inv:Date_Created_SelectedBG = -1
  IF (inv:InvoiceCredit = 'CRE')
    SELF.Q.Invoice_Type_Temp_NormalFG = 255
    SELF.Q.Invoice_Type_Temp_NormalBG = 16777215
    SELF.Q.Invoice_Type_Temp_SelectedFG = 16777215
    SELF.Q.Invoice_Type_Temp_SelectedBG = 255
  ELSE
    SELF.Q.Invoice_Type_Temp_NormalFG = -1
    SELF.Q.Invoice_Type_Temp_NormalBG = -1
    SELF.Q.Invoice_Type_Temp_SelectedFG = -1
    SELF.Q.Invoice_Type_Temp_SelectedBG = -1
  END
  IF (inv:InvoiceCredit = 'CRE')
    SELF.Q.invoice_Credit_temp_NormalFG = 255
    SELF.Q.invoice_Credit_temp_NormalBG = 16777215
    SELF.Q.invoice_Credit_temp_SelectedFG = 16777215
    SELF.Q.invoice_Credit_temp_SelectedBG = 255
  ELSE
    SELF.Q.invoice_Credit_temp_NormalFG = -1
    SELF.Q.invoice_Credit_temp_NormalBG = -1
    SELF.Q.invoice_Credit_temp_SelectedFG = -1
    SELF.Q.invoice_Credit_temp_SelectedBG = -1
  END
  SELF.Q.tmp:couriercost_NormalFG = -1
  SELF.Q.tmp:couriercost_NormalBG = -1
  SELF.Q.tmp:couriercost_SelectedFG = -1
  SELF.Q.tmp:couriercost_SelectedBG = -1
  SELF.Q.tmp:LabourCost_NormalFG = -1
  SELF.Q.tmp:LabourCost_NormalBG = -1
  SELF.Q.tmp:LabourCost_SelectedFG = -1
  SELF.Q.tmp:LabourCost_SelectedBG = -1
  SELF.Q.tmp:PartsCost_NormalFG = -1
  SELF.Q.tmp:PartsCost_NormalBG = -1
  SELF.Q.tmp:PartsCost_SelectedFG = -1
  SELF.Q.tmp:PartsCost_SelectedBG = -1
  SELF.Q.tmp:RRCLabourCost_NormalFG = -1
  SELF.Q.tmp:RRCLabourCost_NormalBG = -1
  SELF.Q.tmp:RRCLabourCost_SelectedFG = -1
  SELF.Q.tmp:RRCLabourCost_SelectedBG = -1
  SELF.Q.tmp:RRCPartsCost_NormalFG = -1
  SELF.Q.tmp:RRCPartsCost_NormalBG = -1
  SELF.Q.tmp:RRCPartsCost_SelectedFG = -1
  SELF.Q.tmp:RRCPartsCost_SelectedBG = -1
  SELF.Q.inv:Courier_Paid_NormalFG = -1
  SELF.Q.inv:Courier_Paid_NormalBG = -1
  SELF.Q.inv:Courier_Paid_SelectedFG = -1
  SELF.Q.inv:Courier_Paid_SelectedBG = -1
  SELF.Q.inv:Labour_Paid_NormalFG = -1
  SELF.Q.inv:Labour_Paid_NormalBG = -1
  SELF.Q.inv:Labour_Paid_SelectedFG = -1
  SELF.Q.inv:Labour_Paid_SelectedBG = -1
  SELF.Q.inv:Parts_Paid_NormalFG = -1
  SELF.Q.inv:Parts_Paid_NormalBG = -1
  SELF.Q.inv:Parts_Paid_SelectedFG = -1
  SELF.Q.inv:Parts_Paid_SelectedBG = -1
  SELF.Q.inv:Invoice_Type_NormalFG = -1
  SELF.Q.inv:Invoice_Type_NormalBG = -1
  SELF.Q.inv:Invoice_Type_SelectedFG = -1
  SELF.Q.inv:Invoice_Type_SelectedBG = -1
  SELF.Q.inv:Total_Claimed_NormalFG = -1
  SELF.Q.inv:Total_Claimed_NormalBG = -1
  SELF.Q.inv:Total_Claimed_SelectedFG = -1
  SELF.Q.inv:Total_Claimed_SelectedBG = -1
  SELF.Q.inv:InvoiceCredit_NormalFG = -1
  SELF.Q.inv:InvoiceCredit_NormalBG = -1
  SELF.Q.inv:InvoiceCredit_SelectedFG = -1
  SELF.Q.inv:InvoiceCredit_SelectedBG = -1
  SELF.Q.Invoice_Type_Temp = Invoice_Type_Temp        !Assign formula result to display queue
  SELF.Q.invoice_Credit_temp = invoice_Credit_temp    !Assign formula result to display queue


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW13.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,13,'GetFreeElementName'
  IF BRW13.ViewOrder = True                           !Xplore
     RETURN('UPPER(' & CLIP(BRW13.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW13.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,13,'GetFreeElementPosition'
  IF BRW13.ViewOrder = True                           !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW13.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change:Job
  END


BRW13.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,13
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore13.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore13.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore13.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW13.ViewOrder = True                           !Xplore
    SavePtr# = POINTER(Xplore13.sq)                   !Xplore
    Xplore13.SetupOrder(BRW13.SortOrderNbr)           !Xplore
    GET(Xplore13.sq,SavePtr#)                         !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW13.SortOrderNbr,Force)       !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW13.FileSeqOn = True                        !Xplore
    RETURN SELF.SetSort(BRW13.FileOrderNbr,Force)     !Xplore
  END                                                 !Xplore
  IF Upper(inv:invoice_type) <> 'WAR'
    RETURN SELF.SetSort(1,Force)
  ELSIF Upper(inv:invoice_type) = 'WAR'
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW13.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,13,'TakeEvent'
  Xplore13.LastEvent = EVENT()                        !Xplore
  IF FOCUS() = ?List                                  !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore13.AdjustAllColumns()                  !Xplore
      OF AltF12                                       !Xplore
         Xplore13.ResetToDefault()                    !Xplore
      OF AltR                                         !Xplore
         Xplore13.ToggleBar()                         !Xplore
      OF AltM                                         !Xplore
         Xplore13.InvokePopup()                       !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore13.RightButtonUp                         !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW13.FileSeqOn = False                        !Xplore
       Xplore13.LeftButtonUp(0)                       !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW13.SavePosition()                          !Xplore
       Xplore13.HandleMyEvents()                      !Xplore
       !BRW13.RestorePosition()                       !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List                             !Xplore
  PARENT.TakeEvent


BRW13.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore13.LeftButton2()                       !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW13.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW18.SetQueueRecord PROCEDURE

  CODE
  IF (jov:Type = 'I')
    tmp:InvoiceNumber = CLIP(jov:InvoiceNumber) & '-' & CLIP(tmp:TradeID) & CLIP(jov:Suffix)
  ELSE
    tmp:InvoiceNumber = 'CN' & CLIP(jov:InvoiceNumber) & '-' & CLIP(tmp:TradeID) & CLIP(jov:Suffix)
  END
  IF (jov:Type = 'I')
    tmp:InvoiceType = 'Invoice'
  ELSE
    tmp:InvoiceType = 'Credit'
  END
  PARENT.SetQueueRecord
  SELF.Q.tmp:InvoiceNumber_NormalFG = -1
  SELF.Q.tmp:InvoiceNumber_NormalBG = -1
  SELF.Q.tmp:InvoiceNumber_SelectedFG = -1
  SELF.Q.tmp:InvoiceNumber_SelectedBG = -1
  SELF.Q.tmp:InvoiceType_NormalFG = -1
  SELF.Q.tmp:InvoiceType_NormalBG = -1
  SELF.Q.tmp:InvoiceType_SelectedFG = -1
  SELF.Q.tmp:InvoiceType_SelectedBG = -1
  SELF.Q.jov:DateCreated_NormalFG = -1
  SELF.Q.jov:DateCreated_NormalBG = -1
  SELF.Q.jov:DateCreated_SelectedFG = -1
  SELF.Q.jov:DateCreated_SelectedBG = -1
  SELF.Q.tmp:InvoiceNumber = tmp:InvoiceNumber        !Assign formula result to display queue
  SELF.Q.tmp:InvoiceType = tmp:InvoiceType            !Assign formula result to display queue
   
   
   IF (jov:Type = 'C')
     SELF.Q.tmp:InvoiceNumber_NormalFG = 255
     SELF.Q.tmp:InvoiceNumber_NormalBG = 16777215
     SELF.Q.tmp:InvoiceNumber_SelectedFG = 16777215
     SELF.Q.tmp:InvoiceNumber_SelectedBG = 255
   ELSE
     SELF.Q.tmp:InvoiceNumber_NormalFG = -1
     SELF.Q.tmp:InvoiceNumber_NormalBG = -1
     SELF.Q.tmp:InvoiceNumber_SelectedFG = -1
     SELF.Q.tmp:InvoiceNumber_SelectedBG = -1
   END
   IF (jov:Type = 'C')
     SELF.Q.tmp:InvoiceType_NormalFG = 255
     SELF.Q.tmp:InvoiceType_NormalBG = 16777215
     SELF.Q.tmp:InvoiceType_SelectedFG = 16777215
     SELF.Q.tmp:InvoiceType_SelectedBG = 255
   ELSE
     SELF.Q.tmp:InvoiceType_NormalFG = -1
     SELF.Q.tmp:InvoiceType_NormalBG = -1
     SELF.Q.tmp:InvoiceType_SelectedFG = -1
     SELF.Q.tmp:InvoiceType_SelectedBG = -1
   END
   IF (jov:Type = 'C')
     SELF.Q.jov:DateCreated_NormalFG = 255
     SELF.Q.jov:DateCreated_NormalBG = 16777215
     SELF.Q.jov:DateCreated_SelectedFG = 16777215
     SELF.Q.jov:DateCreated_SelectedBG = 255
   ELSE
     SELF.Q.jov:DateCreated_NormalFG = -1
     SELF.Q.jov:DateCreated_NormalBG = -1
     SELF.Q.jov:DateCreated_SelectedFG = -1
     SELF.Q.jov:DateCreated_SelectedBG = -1
   END


BRW18.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !inv:Invoice_Number
  OF 6 !inv:Account_Number
  OF 11 !inv:Date_Created
  OF 16 !Invoice_Type_Temp
        ColumnString     = SUB(LEFT(BRW1.Q.Invoice_Type_Temp),1,20)
  OF 21 !invoice_Credit_temp
        ColumnString     = SUB(LEFT(BRW1.Q.invoice_Credit_temp),1,20)
  OF 26 !tmp:couriercost
        ColumnString     = SUB(LEFT(BRW1.Q.tmp:couriercost),1,20)
  OF 31 !tmp:LabourCost
        ColumnString     = SUB(LEFT(BRW1.Q.tmp:LabourCost),1,20)
  OF 36 !tmp:PartsCost
        ColumnString     = SUB(LEFT(BRW1.Q.tmp:PartsCost),1,20)
  OF 41 !tmp:RRCLabourCost
  OF 46 !tmp:RRCPartsCost
  OF 51 !inv:Courier_Paid
  OF 56 !inv:Labour_Paid
  OF 61 !inv:Parts_Paid
  OF 66 !inv:Invoice_Type
  OF 71 !inv:Total_Claimed
  OF 76 !inv:InvoiceCredit
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step16)
  SELF.FQ.SortField = SELF.BC.AddSortOrder()
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(?inv:Invoice_Number,inv:Invoice_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,inv:Account_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,inv:Date_Created,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(,inv:Invoice_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(,tmp:RRCLabourCost,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step10.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator10)
      Xplore1Locator10.Init(,tmp:RRCPartsCost,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step11.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator11)
      Xplore1Locator11.Init(,inv:Courier_Paid,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step12.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator12)
      Xplore1Locator12.Init(,inv:Labour_Paid,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step13.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator13)
      Xplore1Locator13.Init(,inv:Parts_Paid,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step14.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator14)
      Xplore1Locator14.Init(,inv:Invoice_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step15.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator15)
      Xplore1Locator15.Init(,inv:Total_Claimed,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step16.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator16)
      Xplore1Locator16.Init(,inv:InvoiceCredit,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(INVOICE)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('inv:Invoice_Number')         !Field Name
                SHORT(36)                             !Default Column Width
                PSTRING('Invoice No')                 !Header
                PSTRING('@p<<<<<<<<<<<<<<#p')         !Picture
                PSTRING('Invoice No')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('E')                           !Locator Type
                !-------------------------
                PSTRING('inv:Account_Number')         !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Account Number')             !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Account Number')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('E')                           !Locator Type
                !-------------------------
                PSTRING('inv:Date_Created')           !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Date Created')               !Header
                PSTRING('@d6b')                       !Picture
                PSTRING('Date Created')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('E')                           !Locator Type
                !-------------------------
                PSTRING('Invoice_Type_Temp')          !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Invoice_Type_Temp')          !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Invoice_Type_Temp')          !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('invoice_Credit_temp')        !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('invoice_Credit_temp')        !Header
                PSTRING('@S20')                       !Picture
                PSTRING('invoice_Credit_temp')        !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:couriercost')            !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:couriercost')            !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:couriercost')            !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:LabourCost')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:LabourCost')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:LabourCost')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:PartsCost')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:PartsCost')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:PartsCost')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:RRCLabourCost')          !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:RRCLabourCost')          !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:RRCLabourCost')          !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('E')                           !Locator Type
                !-------------------------
                PSTRING('tmp:RRCPartsCost')           !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:RRCPartsCost')           !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:RRCPartsCost')           !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('E')                           !Locator Type
                !-------------------------
                PSTRING('inv:Courier_Paid')           !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Courier Cost')               !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Courier Cost')               !Description
                STRING('D')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('E')                           !Locator Type
                !-------------------------
                PSTRING('inv:Labour_Paid')            !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Labour Cost')                !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Labour Cost')                !Description
                STRING('D')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('E')                           !Locator Type
                !-------------------------
                PSTRING('inv:Parts_Paid')             !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Parts Cost')                 !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Parts Cost')                 !Description
                STRING('D')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('E')                           !Locator Type
                !-------------------------
                PSTRING('inv:Invoice_Type')           !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Invoice Type')               !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Invoice Type')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('E')                           !Locator Type
                !-------------------------
                PSTRING('inv:Total_Claimed')          !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Total Cost')                 !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Total Cost')                 !Description
                STRING('D')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('E')                           !Locator Type
                !-------------------------
                PSTRING('inv:InvoiceCredit')          !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Invoice Credit')             !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Invoice Type: Invoice or Credit') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('E')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(16)
!--------------------------
XpSortFields    GROUP,STATIC
                PSTRING('Invoice_Type_Temp')          !Column Field
                SHORT(1)                              !Nos of Sort Fields
                PSTRING('inv:Invoice_Type')           !Sort Field Seq:1
                BYTE(0)                               !Ascending
                PSTRING('@s3')                        !Picture
                !-------------------------
                PSTRING('invoice_Credit_temp')        !Column Field
                SHORT(1)                              !Nos of Sort Fields
                PSTRING('inv:InvoiceCredit')          !Sort Field Seq:1
                BYTE(0)                               !Ascending
                PSTRING('@s3')                        !Picture
                !-------------------------
                PSTRING('tmp:couriercost')            !Column Field
                SHORT(0)                              !Nos of Sort Fields
                !-------------------------
                PSTRING('tmp:LabourCost')             !Column Field
                SHORT(0)                              !Nos of Sort Fields
                !-------------------------
                PSTRING('tmp:PartsCost')              !Column Field
                SHORT(0)                              !Nos of Sort Fields
                !-------------------------
                END
XpSortDim       SHORT(5)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)
BRW13.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW13.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW13.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW13.ResetPairsQ PROCEDURE()
  CODE
Xplore13.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore13.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore13.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW13.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !job:Ref_Number
  OF 2 !job:Model_Number
  OF 3 !job:ESN
  OF 4 !job:Unit_Type
  OF 5 !job:Invoice_Number
  OF 6 !job:Invoice_Number_Warranty
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore13.SetNewOrderFields PROCEDURE()
  CODE
  BRW13.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore13.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore13Step6,job:InvoiceNumberKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,job:InvoiceNumberKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore13Step1.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore13Locator1)
      Xplore13Locator1.Init(,job:Ref_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore13Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore13Locator2)
      Xplore13Locator2.Init(,job:Model_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore13Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore13Locator3)
      Xplore13Locator3.Init(,job:ESN,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore13Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore13Locator4)
      Xplore13Locator4.Init(,job:Unit_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore13Step5.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore13Locator5)
      Xplore13Locator5.Init(,job:Invoice_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore13Step6.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore13Locator6)
      Xplore13Locator6.Init(,job:Invoice_Number_Warranty,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(job:Invoice_Number,inv:Invoice_Number)
  RETURN
!================================================================================
Xplore13.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore13.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW13.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(JOBS)
  END
  RETURN TotalRecords
!================================================================================
Xplore13.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('job:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job No')                     !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Job No')                     !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:ESN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('ESN/IMEI')                   !Header
                PSTRING('@s16')                       !Picture
                PSTRING('ESN/IMEI')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Unit_Type')              !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Unit Type')                  !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Unit Type')                  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Invoice_Number')         !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Invoice Number')             !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Invoice Number')             !Description
                STRING('R')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Invoice_Number_Warranty') !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Invoice Number')             !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Invoice Number')             !Description
                STRING('R')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(6)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?INV:Invoice_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 20
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            return 1
        Of Button:No
        End !CASE
    End!If cancel# = 1

    return 0
Invoice_Date_Range PROCEDURE                          !Generated from procedure template - Window

FilesOpened          BYTE
save_inv_ali_id      USHORT,AUTO
save_inv_id          USHORT,AUTO
save_tra_id          USHORT,AUTO
save_wob_id          USHORT,AUTO
pos                  STRING(255)
save_sub_id          USHORT,AUTO
start_date_temp      DATE
end_Date_temp        DATE
tmp:accountnumber    STRING(15)
tmp:SelectAccount    BYTE(0)
tmp:SelectBatch      BYTE(0)
tmp:BatchNumber      LONG
tmp:InvoiceType      BYTE(1)
tmp:TradeList        BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:StartNumber      LONG
tmp:EndNumber        LONG
tmp:RangeType        BYTE(0)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:accountnumber
tratmp:Account_Number  LIKE(tratmp:Account_Number)    !List box control field - type derived from field
tratmp:RefNumber       LIKE(tratmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(TRADETMP)
                       PROJECT(tratmp:Account_Number)
                       PROJECT(tratmp:RefNumber)
                     END
window               WINDOW('Date Range'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Invoice Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Date Range'),USE(?Tab1)
                           CHECK('Select Account Number'),AT(244,114),USE(tmp:SelectAccount),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           COMBO(@s15),AT(244,128,124,10),USE(tmp:accountnumber),IMM,DISABLE,VSCROLL,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('60L(2)|M@s15@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           OPTION('Invoice Type'),AT(244,158,268,28),USE(tmp:InvoiceType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All Invoices'),AT(252,170),USE(?tmp:InvoiceType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Service Invoices Only'),AT(320,170),USE(?tmp:InvoiceType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Retail Invoices Only'),AT(420,170),USE(?tmp:InvoiceType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           OPTION('Range Type'),AT(244,192,132,28),USE(tmp:RangeType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Invoice Date'),AT(252,202),USE(?Option2:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Invoice No'),AT(320,202),USE(?Option2:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           GROUP,AT(168,230,179,35),USE(?InvoiceDates)
                             PROMPT('Start Date'),AT(167,235),USE(?Select_Global1:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@d6),AT(243,235,64,10),USE(start_date_temp),FONT(,8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(AltD),CAP
                             BUTTON,AT(311,230),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('End Date'),AT(167,254),USE(?Select_Global2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@d6b),AT(243,254,64,10),USE(end_Date_temp),FONT(,8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(AltD),UPR
                             BUTTON,AT(311,251),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                           END
                           GROUP,AT(348,230,175,35),USE(?InvoiceNumbers),DISABLE
                             PROMPT('Start Number'),AT(347,235),USE(?tmp:StartNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s8),AT(423,235,64,10),USE(tmp:StartNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start NU'),TIP('Start NU'),UPR
                             PROMPT('End Number'),AT(347,254),USE(?tmp:EndNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s8),AT(423,254,64,10),USE(tmp:EndNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Number'),TIP('End Number'),UPR
                           END
                           PROMPT('Account Number'),AT(168,128),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(380,332),USE(?Button4),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020409'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Invoice_Date_Range')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:INVOICE.Open
  Relate:INVOICE_ALIAS.Open
  Relate:TRADETMP.Open
  Access:JOBS.UseFile
  Access:LOCATLOG.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  start_date_temp = Deformat('1/1/1990',@d6b)
  end_date_temp = Today()
  Display()
  ! Save Window Name
   AddToLog('Window','Open','Invoice_Date_Range')
  Bryan.CompFieldColour()
  ?start_date_temp{Prop:Alrt,255} = MouseLeft2
  ?end_Date_temp{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:SelectAccount{Prop:Checked} = True
    ENABLE(?tmp:accountnumber)
  END
  IF ?tmp:SelectAccount{Prop:Checked} = False
    DISABLE(?tmp:accountnumber)
  END
  FDCB7.Init(tmp:accountnumber,?tmp:accountnumber,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:TRADETMP,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(tratmp:AccountNoKey)
  FDCB7.AddField(tratmp:Account_Number,FDCB7.Q.tratmp:Account_Number)
  FDCB7.AddField(tratmp:RefNumber,FDCB7.Q.tratmp:RefNumber)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
    Relate:INVOICE_ALIAS.Close
    Relate:TRADETMP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Invoice_Date_Range')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:SelectAccount
      If ~0{prop:acceptall}
          IF tmp:TradeList = 0
              If tmp:SelectAccount = 1
                  Case Missive('If will take a few moments to build the list of available accounts.'&|
                    '<13,10>'&|
                    '<13,10>Do you wish to continue?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          TradeList('INVOICE')
                          tmp:TradeList = 1
                      Of 1 ! No Button
                          tmp:SelectAccount = 0
                  End ! Case Missive
              End!If tmp:SelectAccount = 1
          End!IF tmp:TradeList = 0
      End!If ~0{prop:acceptall}
      FDCB7.ResetQueue(1)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020409'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020409'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020409'&'0')
      ***
    OF ?tmp:SelectAccount
      IF ?tmp:SelectAccount{Prop:Checked} = True
        ENABLE(?tmp:accountnumber)
      END
      IF ?tmp:SelectAccount{Prop:Checked} = False
        DISABLE(?tmp:accountnumber)
      END
      ThisWindow.Reset
    OF ?tmp:RangeType
      Case tmp:RangeType
          Of 0!Dates
              ?InvoiceDates{prop:Disable} = 0
              ?InvoiceNumbers{prop:Disable} = 1
          Of 1!Numbers
              ?InvoiceNumbers{prop:Disable} = 0
              ?InvoiceDates{prop:Disable} = 1
      End !tmp:RangeType
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          start_date_temp = TINCALENDARStyle1()
          Display(?start_date_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          end_Date_temp = TINCALENDARStyle1(end_Date_temp)
          Display(?end_Date_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4
      ThisWindow.Update
      error# = 0
      If tmp:selectaccount = 1
          If tmp:accountnumber = ''
              Select(?tmp:accountnumber)
              error# = 1
          End!If tmp:accountnumber = ''
      End!If tmp:selectaccount = 1
      
      If error# = 0
          Records# = 0
          If tmp:RangeType = 0
      
              setcursor(cursor:wait)
              save_inv_ali_id = access:INVOICE_ALIAS.savefile()
              access:INVOICE_ALIAS.clearkey(inv_ali:date_created_key)
              inv_ali:date_created = start_date_temp
              set(inv_ali:date_created_key,inv_ali:date_created_key)
              loop
                  if access:INVOICE_ALIAS.next()
                     break
                  end !if
                  if inv_ali:date_created > end_Date_temp      |
                      then break.  ! end if
                  Records# += 1
                  If Records# > 5000
                      Records# = Records(INVOICE)
                      Break
                  End !If RecordsToProcess > 5000
      
              end !loop
              access:INVOICE_ALIAS.restorefile(save_inv_ali_id)
              setcursor()
      
          Else
              Records# = tmp:EndNumber - tmp:StartNumber
          End !If tmp:RangeType = 0
      
          Prog.ProgressSetup(Records#)
      
      
          save_inv_ali_id = access:INVOICE_ALIAS.savefile()
          Case tmp:RangeType
              Of 0
                  access:INVOICE_ALIAS.clearkey(inv_ali:date_created_key)
                  inv_ali:date_created = start_date_temp
                  set(inv_ali:date_created_key,inv_ali:date_created_key)
              Of 1
                  access:INVOICE_ALIAS.clearkey(inv_ali:INVOICE_Number_key)
                  inv_ali:INVOICE_Number = tmp:StartNumber
                  set(inv_ali:INVOICE_Number_Key,inv_ali:INVOICE_Number_Key)
          End !Case tmp:RangeType
          loop
              if access:INVOICE_ALIAS.next()
                 break
              end !if
              Case tmp:RangeType
                  Of 0
                      if inv_ali:date_created > end_Date_temp      |
                          then break.  ! end if
      
                  Of 1
                      If inv_ali:INVOICE_Number > tmp:EndNumber
                          Break
                      End !If inv_ali:INVOICE_ALIAS_Number > tmp:EndNumber
              End !Case tmp:RangeType
      
              If Prog.InsideLoop()
                  Break
              End ! Prog.InsideLoop()
      
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              If tmp:SelectAccount = 1
                  If inv_ali:account_number <> tmp:accountnumber
                      Cycle
                  End!If inv_ali:account_number <> tmp:account"
              End!If tmp:account" <> ''
              If tmp:SelectBatch = 1
                  If inv_ali:batch_number <> tmp:BatchNumber
                      Cycle
                  End!If inv_ali:batch_number <> batch"
              End!If tmp:batch" <> ''
              glo:select1  = inv_ali:INVOICE_number
              glo:select2  = inv_ali:batch_number
              glo:select3  = inv_ali:account_number
              Case tmp:INVOICEType
                  Of 2 !Service Only
                      If inv_ali:INVOICE_Type = 'RET'
                          Cycle
                      End!If inv_ali:INVOICE_ALIAS_Type = 'RET'
                  Of 3 !Retail Only
                      If inv_ali:INVOICE_type <> 'RET'
                          Cycle
                      End!If inv_ali:INVOICE_ALIAS_type <> 'RET'
              End!Case tmp:INVOICE_ALIASType
              Case inv_ali:INVOICE_type
                  Of 'CHA'
                      Chargeable_Summary_Invoice
                  Of 'SIN'
                      Access:JOBS.Clearkey(job:InvoiceNumberKey)
                      job:Invoice_Number  = inv_ali:Invoice_Number
                      If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
                          !Found
                          If ~SentToHub(job:Ref_Number)
                              Cycle
                          End !If ~SentToHub(job:Ref_Number)
                      Else ! If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
                          !Error
                      End !If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
      !                Single_Invoice('','')
                      VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')    ! #11881 New Single Invoice (Bryan: 16/03/2011)
                  Of 'WAR'
      
                      Summary_Warranty_Invoice
                  Of 'RET'
                      Retail_Single_Invoice(1,inv_ali:Invoice_Number)   !1 implies this is a reprint
                  Of 'BAT'
                      Chargeable_Batch_Invoice
                  Of 'WRC'
                      RRCWarrantyInvoice(inv_ali:Invoice_Number)
              End
      
          end !loop
          access:INVOICE_ALIAS.restorefile(save_inv_ali_id)
      
          Prog.ProgressFinish()
      
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
      
      End!If error# = 0
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?start_date_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?end_Date_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?start_date_temp
    CASE EVENT()
    OF EVENT:AlertKey
      start_date_temp = Deformat('1/1/1990',@d6b)
      Display(?start_date_temp)
    END
  OF ?end_Date_temp
    CASE EVENT()
    OF EVENT:AlertKey
      end_date_temp = Today()
      Display(?end_Date_temp)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
InvoiceText PROCEDURE                                 !Generated from procedure template - Window

FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Invoice Text'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Invoice Text'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           TEXT,AT(238,158,206,86),USE(jbn:Invoice_Text),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting'
  OF ChangeRecord
    ActionMessage = 'Changing Fault Description'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020417'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InvoiceText')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:JOBNOTES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBNOTES.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBNOTES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','InvoiceText')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','InvoiceText')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020417'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020417'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020417'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ConfirmSecondEntry PROCEDURE                          !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ConfirmSecondEntry STRING(3)
window               WINDOW('Confirm Second Entry'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Confirm Second Entry'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           OPTION('Confirm Second Entry'),AT(248,192,184,34),USE(tmp:ConfirmSecondEntry),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Engineer'),AT(272,202),USE(?tmp:ConfirmSecondEntry:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ENG')
                             RADIO('Parts'),AT(272,214),USE(?tmp:ConfirmSecondEntry:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('PAR')
                             RADIO('Manufacturer'),AT(348,202),USE(?tmp:ConfirmSecondEntry:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('MAN')
                             RADIO('Unrelated'),AT(348,214),USE(?tmp:ConfirmSecondEntry:Radio3:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('UNR')
                           END
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:ConfirmSecondEntry)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:02398'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ConfirmSecondEntry')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','ConfirmSecondEntry')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','ConfirmSecondEntry')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '02398'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('02398'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('02398'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      If tmp:ConfirmSecondEntry = ''
          Case Missive('You must select an option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !tmp:ConfirmSecondEntry = ''
          Post(Event:CloseWindow)
      End !tmp:ConfirmSecondEntry = ''
    OF ?Cancel
      ThisWindow.Update
      tmp:ConfirmSecondEntry = ''
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
SelectLoanExchangeUnit PROCEDURE (func:Type)          !Generated from procedure template - Window

FilesOpened          BYTE
esn_temp             STRING(16)
tmp:RefNumber        LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Search Loan/Exchange Unit'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Search For Loan / Exchange Unit'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('I.M.E.I. Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s16),AT(248,206,116,10),USE(esn_temp),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(368,199),USE(?Full_Search),SKIP,TRN,FLAT,LEFT,ICON('fullseap.jpg')
                         END
                       END
                       BUTTON,AT(300,258),USE(?Ok),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:RefNumber)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020418'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectLoanExchangeUnit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Access:LOAN.UseFile
  Access:STOCK.UseFile
  Access:STOCKTYP.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','SelectLoanExchangeUnit')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','SelectLoanExchangeUnit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?esn_temp
      ?esn_temp{prop:touched} = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020418'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020418'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020418'&'0')
      ***
    OF ?Full_Search
      ThisWindow.Update
      tmp:RefNumber = ''
      ESN_Temp    = ''
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = job:Account_Number
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Found
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = sub:Main_Account_Number
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              SaveRequest#      = GlobalRequest
              GlobalResponse    = RequestCancelled
              GlobalRequest     = SelectRecord
              Case func:Type
                  Of 'EXC'
                      Browse_Exchange(tra:exchange_stock_type)
                      If Globalresponse = RequestCompleted
                          ESN_Temp    = xch:ESN
                          tmp:RefNumber   = xch:Ref_Number
                          Post(Event:Accepted,?OK)
                      Else
                          esn_temp    = ''
                          tmp:RefNumber  = ''
                          Select(?ESN_Temp)
                      End
      
                  Of 'LOA'
                      Browse_Loan(tra:loan_stock_type)
                      If Globalresponse = RequestCompleted
                          ESN_Temp    = loa:ESN
                          tmp:RefNumber   = loa:Ref_Number
                          Post(Event:Accepted,?OK)
                      Else
                          esn_temp    = ''
                          tmp:RefNumber  = ''
                          Select(?ESN_Temp)
                      End
      
              End !Case func:Type
              GlobalRequest     = SaveRequest#
          Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
    OF ?Ok
      ThisWindow.Update
    close# = 0
    error# = 0


    Access:USERS.Clearkey(use:Password_Key)
    use:Password    = glo:Password
    if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
        ! Found

    else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
        ! Error
    end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)


    Case func:Type
        Of 'LOA'
            access:loan.clearkey(loa:esn_only_key)
            loa:esn = esn_temp
            if access:loan.fetch(loa:esn_only_key) = Level:Benign
                ! Insert --- Do not allow use to select unit from different location (DBH: 18/08/2009) #11029
                if (use:Location <> loa:Location)
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Missive('You cannot select a unit from a different location.','ServiceBase',|
                                   'mstop.jpg','/&OK')
                    Of 1 ! &OK Button
                    End!Case Message
                    error# = 1
                else ! if (use:Location <> loa:Location)
                ! end --- (DBH: 18/08/2009) #11029
                    IF loa:Available = 'AVL'
                        access:stocktyp.clearkey(stp:stock_type_key)
                        stp:stock_type = loa:stock_type
                        if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                           If stp:available <> 1
                                Case Missive('The selected Stock Type is not available.','ServiceBase 3g',|
                                               'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                               error# = 1
                           else
                               tmp:RefNumber   = loa:Ref_Number
                           End!If stp:available <> 1
                        End!if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                    ELSE
                        Case Missive('The selected Loan Unit is not available.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        error# = 1
                    END
                end ! if (use:Location <> loa:Location)
            Else!if access:exchange.fetch(xch:esn_only_key)
                Beep(Beep:SystemHand)  ;  Yield()
                Case Missive('Cannot find the selected I.M.E.I. Number.','ServiceBase',|
                               'mstop.jpg','/&OK')
                Of 1 ! &OK Button
                End!Case Message
               error# = 1
            end!if access:exchange.fetch(xch:esn_only_key)

        Of 'EXC'
            !If there is no ref_number, then the full search button was not pressed
            !and the user hasjust typed/scanned in a imei number.
            If tmp:RefNumber = '' And ESN_temp <> ''
                Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                xch:ESN = ESN_Temp
                If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                    !Found
                    ! Insert --- Do not allow use to select unit from different location (DBH: 18/08/2009) #11029
                    if (use:Location <> xch:Location)
                        Beep(Beep:SystemHand)  ;  Yield()
                        Case Missive('You cannot select a unit from a different location.','ServiceBase',|
                                       'mstop.jpg','/&OK')
                        Of 1 ! &OK Button
                        End!Case Message
                        error# = 1
                    else ! if (use:Location <> loa:Location)
                    ! end --- (DBH: 18/08/2009) #11029
                        tmp:RefNumber   = xch:Ref_Number
                    end ! if (use:Location <> loa:Location)
                Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Missive('Cannot find the selected I.M.E.I. Number.','ServiceBase',|
                                   'mstop.jpg','/&OK')
                    Of 1 ! &OK Button
                    End!Case Message
                    Error# = 1
                End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
            End !If tmp:RefNumber = '' And ESN_temp <> ''
            !If full search is selected, then should have a ref_number
            If tmp:RefNumber <> '' and error# = 0
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = tmp:RefNumber
                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Found
                    ! Insert --- Do not allow use to select unit from different location (DBH: 18/08/2009) #11029
                    if (use:Location <> xch:Location)
                        Beep(Beep:SystemHand)  ;  Yield()
                        Case Missive('You cannot select a unit from a different location.','ServiceBase',|
                                       'mstop.jpg','/&OK')
                        Of 1 ! &OK Button
                        End!Case Message
                        error# = 1
                    end ! if (use:Location <> loa:Location)
                    ! end --- (DBH: 18/08/2009) #11029
                Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Missive('Cannot find the selected I.M.E.I. Number.','ServiceBase',|
                                   'mstop.jpg','/&OK')
                    Of 1 ! &OK Button
                    End!Case Message
                    Error# = 1
                End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            End !If tmp:RefNumber <> ''
            If Error# = 0
                !Found
                IF xch:Available = 'AVL'
                    access:stocktyp.clearkey(stp:stock_type_key)
                    stp:stock_type = xch:stock_type
                    if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                        If stp:available <> 1
                            Case Missive('The selected Stock Type is not available.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                              error# = 1
                        ELSE
                            close# = 1
                        End!If stp:available <> 1
                    End!if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                ELSE
                    Case Missive('The selected Exchange Unit is not available.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                   error# = 1
                END
            End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End!Case f_type
    If error# = 1
        esn_temp = ''
        tmp:RefNumber = ''
        Select(?esn_temp)
        Display()
    Else
        Post(event:closewindow)
    End!If close# = 1
    OF ?CancelButton
      ThisWindow.Update
      tmp:RefNumber = ''
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Case func:Type
          Of 'EXC'
              ?WindowTitle{prop:text} = 'Select Exchange Unit'
              ?Tab1{prop:text} = 'Insert Exchange I.M.E.I. Number'
          Of 'LOA'
              ?WindowTitle{prop:text} = 'Select Loan Unit'
              ?Tab1{prop:text} = 'Insert Loan I.M.E.I. Number'
      End!Case f_type
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowseJobBouncers PROCEDURE (func:Type)               !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_jobb_id         USHORT,AUTO
save_job_ali_id      USHORT,AUTO
tmp:FaultDescription STRING(255)
tmp:EngineersNotes   STRING(255)
tmp:CurrentAccountNumber STRING(30)
tmp:ReturnValue      STRING(30)
tmp:Bouncer          STRING(1)
BRW4::View:Browse    VIEW(JOBBOUNCER)
                       PROJECT(jobb:JobNumber)
                       PROJECT(jobb:AccountName)
                       PROJECT(jobb:Booked)
                       PROJECT(jobb:Completed)
                       PROJECT(jobb:ChargeType)
                       PROJECT(jobb:TheDays)
                       PROJECT(jobb:Type)
                       PROJECT(jobb:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
jobb:JobNumber         LIKE(jobb:JobNumber)           !List box control field - type derived from field
jobb:AccountName       LIKE(jobb:AccountName)         !List box control field - type derived from field
jobb:Booked            LIKE(jobb:Booked)              !List box control field - type derived from field
jobb:Completed         LIKE(jobb:Completed)           !List box control field - type derived from field
jobb:ChargeType        LIKE(jobb:ChargeType)          !List box control field - type derived from field
tmp:Bouncer            LIKE(tmp:Bouncer)              !List box control field - type derived from local data
tmp:Bouncer_Icon       LONG                           !Entry's icon ID
jobb:TheDays           LIKE(jobb:TheDays)             !List box control field - type derived from field
jobb:Type              LIKE(jobb:Type)                !List box control field - type derived from field
tmp:FaultDescription   LIKE(tmp:FaultDescription)     !List box control field - type derived from local data
tmp:EngineersNotes     LIKE(tmp:EngineersNotes)       !List box control field - type derived from local data
jobb:RecordNumber      LIKE(jobb:RecordNumber)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(JOBOUTFL)
                       PROJECT(joo:FaultCode)
                       PROJECT(joo:Description)
                       PROJECT(joo:RecordNumber)
                       PROJECT(joo:JobNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
joo:FaultCode          LIKE(joo:FaultCode)            !List box control field - type derived from field
joo:Description        LIKE(joo:Description)          !List box control field - type derived from field
joo:RecordNumber       LIKE(joo:RecordNumber)         !Primary key field - type derived from field
joo:JobNumber          LIKE(joo:JobNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(PARTS)
                       PROJECT(par:Part_Number)
                       PROJECT(par:Description)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
par:Part_Number        LIKE(par:Part_Number)          !List box control field - type derived from field
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(WARPARTS)
                       PROJECT(wpr:Part_Number)
                       PROJECT(wpr:Description)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
wpr:Part_Number        LIKE(wpr:Part_Number)          !List box control field - type derived from field
wpr:Description        LIKE(wpr:Description)          !List box control field - type derived from field
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Job History'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Job History'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,140),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Model Number:'),AT(68,58),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(132,58),USE(job:Model_Number),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number:'),AT(451,58),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s20),AT(515,58),USE(job:ESN),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           LIST,AT(124,70,432,120),USE(?List),IMM,VSCROLL,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('38R(2)|M~Job No~@s8@128L(2)|M~Account Name~@s30@40R(2)|M~Booked~@d6@40L(2)|M~Com' &|
   'pleted~R@d6@125L(2)|M~Charge Type~@s30@11CJ~B~C(2)@s1@34L(2)|M~Days~@s6@0L(2)|M~' &|
   'Type~@s1@0L(2)|M~Fault Description~@s255@0L(2)|M~EngineersNotes~@s255@'),FROM(Queue:Browse)
                         END
                       END
                       SHEET,AT(64,196,136,80),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Fault Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(68,212,128,60),USE(tmp:FaultDescription),VSCROLL,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                         END
                       END
                       SHEET,AT(64,280,136,82),USE(?Sheet2:2),COLOR(0D6E7EFH),SPREAD
                         TAB('Engineers Notes'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(68,296,128,62),USE(tmp:EngineersNotes),VSCROLL,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                         END
                       END
                       SHEET,AT(204,196,152,166),USE(?Sheet4),COLOR(0D6E7EFH),SPREAD
                         TAB('Out Fault Codes'),USE(?OutFaultsTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(208,212,144,144),USE(?List:2),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('25L(2)|M~Code~@s2@240L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                         END
                       END
                       SHEET,AT(360,196,180,166),USE(?Sheet5),COLOR(0D6E7EFH),SPREAD
                         TAB('Chargeable Parts'),USE(?ChargeableTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(364,212,172,146),USE(?List:3),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@'),FROM(Queue:Browse:2)
                         END
                         TAB('Warranty Parts'),USE(?WarrantyTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(364,212,172,146),USE(?List:4),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@'),FROM(Queue:Browse:3)
                         END
                       END
                       PANEL,AT(544,196,72,166),USE(?Panel5),FILL(09A6A7CH)
                       BUTTON,AT(548,270),USE(?ContactDetails),TRN,FLAT,HIDE,LEFT,ICON('condetp.jpg')
                       BUTTON,AT(548,302),USE(?BouncerHistoryReport),TRN,FLAT,HIDE,LEFT,ICON('histrepp.jpg')
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(548,332),USE(?ViewJob),TRN,FLAT,LEFT,ICON('viewjobp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW6                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:ReturnValue)

BuildList       Routine
    Save_jobb_ID = Access:JOBBOUNCER.SaveFile()
    Set(jobb:RecordNumberKey)
    Loop
        If Access:JOBBOUNCER.NEXT()
           Break
        End !If
        Delete(JOBBOUNCER)
    End !Loop
    Access:JOBBOUNCER.RestoreFile(Save_jobb_ID)

    Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
    Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
    job_ali:ESN = job:ESN
    Set(job_ali:ESN_Key,job_ali:ESN_Key)
    Loop
        If Access:JOBS_ALIAS.NEXT()
           Break
        End !If
        If job_ali:ESN <> job:ESN      |
            Then Break.  ! End If
        If job:Ref_Number = job_ali:Ref_Number
            Cycle
        End !If job:Ref_Number = job_ali:Ref_Number

        !Just show the history of all jobs, as bouncers are worked out until completion

!        If func:Type <> 'V'
!            If job_Ali:Cancelled = 'YES'
!                Cycle
!            End !If job_Ali:Cancelled = 'YES'
!
!            If job_ali:Exchange_Unit_Number <> ''
!                Cycle
!            End !If job_ali:Exchange_Unit_Number <> ''
!            If job_ali:Chargeable_Job = 'YES'
!                If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                    !SolaceViewVars('Bouncer','Ignore Chargeable',,6)
!                    Cycle
!                End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!                Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!                cha:Charge_Type = job_ali:Charge_Type
!                If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Found
!                    If cha:ExcludeFromBouncer
!                        !SolaceViewVars('Bouncer','Charge Type Excluded',job_ali:Charge_Type,6)
!                        Cycle
!                    Else !If cha:ExcludeFromBouncer
!                        If job_ali:Repair_Type <> ''
!                            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
!                            rtd:Manufacturer = job:Manufacturer
!                            rtd:Chargeable   = 'YES'
!                            rtd:Repair_Type  = job_ali:Repair_Type
!                            If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                                !Found
!                                If rtd:ExcludeFromBouncer
!                                    !SolaceViewVars('Bouncer','Repair Type Excluded',job_ali:Repair_Type,6)
!                                    Cycle
!                                End !If rtd:ExcludeFromBouncer
!                            Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!
!                        End !If job_ali:Repair_Type <> ''
!                    End !If cha:ExcludeFromBouncer
!                Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            End !If func:CJob = 'YES'
!
!
!            If job_ali:Warranty_Job = 'YES'
!                Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!                cha:Charge_Type = job_ali:Warranty_Charge_Type
!                If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Found
!                    If cha:ExcludeFromBouncer
!                        !SolaceViewVars('Bouncer','Charge Type Excluded',job_ali:Warranty_Charge_Type,6)
!                        Cycle
!                    Else !If cha:ExcludeFromBouncer
!                        If job_ali:Repair_Type_Warranty <> ''
!                            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
!                            rtd:Manufacturer = job:Manufacturer
!                            rtd:Warranty     = 'YES'
!                            rtd:Repair_Type  = job_ali:Repair_Type_Warranty
!                            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                                !Found
!                                If rtd:ExcludeFromBouncer
!                                    !SolaceViewVars('Bouncer','Repair Type Excluded',job_ali:Repair_Type_Warranty,6)
!                                    Cycle
!                                End !If rtd:ExcludeFromBouncer
!                            Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                        End !If job_ali:Repair_Type_Warranty <> ''
!                    End !If cha:ExcludeFromBouncer
!                Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            End !If func:CJob = 'YES'
!
!            !Has job got the same In Fault?
!
!            Access:MANFAULT.ClearKey(maf:InFaultKey)
!            maf:Manufacturer = job:Manufacturer
!            maf:InFault      = 1
!            If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!                !Found
!                Case maf:Field_Number
!                    Of 1
!                        If job_ali:Fault_Code1 <> job:Fault_Code1
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code1,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                    Of 2
!                        If job_ali:Fault_Code2 <> job:Fault_Code2
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code2,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                    Of 3
!                        If job_ali:Fault_Code3 <> job:Fault_Code3
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code3,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                    Of 4
!                        If job_ali:Fault_Code4 <> job:Fault_Code4
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code4,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                    Of 5
!                        If job_ali:Fault_Code5 <> job:Fault_Code5
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code5,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                    Of 6
!                        If job_ali:Fault_Code6 <> job:Fault_Code6
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code6,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                    Of 7
!                        If job_ali:Fault_Code7 <> job:Fault_Code7
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code7,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                    Of 8
!                        If job_ali:Fault_Code8 <> job:Fault_Code8
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code8,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                    Of 9
!                        If job_ali:Fault_Code9 <> job:Fault_Code9
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code9,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                    Of 10
!                        If job_ali:Fault_Code10 <> job:Fault_Code10
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code10,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                    Of 11
!                        If job_ali:Fault_Code11 <> job:Fault_Code11
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code11,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                    Of 12
!                        If job_ali:Fault_Code12 <> job:Fault_Code12
!                            !SolaceViewVars('Bouncer','In Fault Different',job_ali:Fault_Code12,6)
!                            Cycle
!                        End !If job_ali:Fault_Code1 <> job:Fault_Code1
!
!                End !Case maf:Field_Number
!            Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!
!            Access:MANFAULT.ClearKey(maf:MainFaultKey)
!            maf:Manufacturer = job:Manufacturer
!            maf:MainFault    = 1
!            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!                !Found
!                Access:MANFAULO.ClearKey(mfo:Field_Key)
!                mfo:Manufacturer = job:Manufacturer
!                mfo:Field_Number = maf:Field_Number
!                Case maf:Field_Number
!                    Of 1
!                        mfo:Field        = job_ali:Fault_Code1
!                    Of 2
!                        mfo:Field        = job_ali:Fault_Code2
!                    Of 3
!                        mfo:Field        = job_ali:Fault_Code3
!                    Of 4
!                        mfo:Field        = job_ali:Fault_Code4
!                    Of 5
!                        mfo:Field        = job_ali:Fault_Code5
!                    Of 6
!                        mfo:Field        = job_ali:Fault_Code6
!                    Of 7
!                        mfo:Field        = job_ali:Fault_Code7
!                    Of 8
!                        mfo:Field        = job_ali:Fault_Code8
!                    Of 9
!                        mfo:Field        = job_ali:Fault_Code9
!                    Of 10
!                        mfo:Field        = job_ali:Fault_Code10
!                    Of 11
!                        mfo:Field        = job_ali:Fault_Code11
!                    Of 12
!                        mfo:Field        = job_ali:Fault_Code12
!                End !Case maf:Field_Number
!                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                    !Found
!                    If mfo:ExcludeFromBouncer
!                        Cycle
!                    End !If mfo:ExcludeBouncer
!                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!
!            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!
!
!
!
!            Case GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI')
!                Of 1
!                    If ~(job_ali:date_completed + def:bouncertime > job:Date_Booked And job_ali:date_completed <= job:Date_Booked)
!                        Cycle
!                    End!If job_ali:date_booked + man:warranty_period < Today()
!                Of 2
!                    If ~(job_ali:date_despatched + def:bouncertime > job:Date_Booked And job_ali:date_despatched <= job:Date_Booked)
!                        Cycle
!                    End!If job_ali:date_booked + man:warranty_period < Today()
!                Else !If GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                    If ~(job_ali:date_booked + def:bouncertime > job:Date_Booked And job_ali:date_booked <= job:Date_Booked)
!                        Cycle
!                    End!If job_ali:date_booked + man:warranty_period < Today()
!            End !If GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!        End !If func:Type <> 'V'


        If Access:JOBBOUNCER.PrimeRecord() = Level:Benign
            jobb:JobNumber   = job_ali:Ref_Number
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number  = job_ali:Account_Number
            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Found
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = sub:Main_Account_Number
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:tradeacc.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:tradeacc.Tryfetch(tra:Account_Number_Key) = Level:Benign
            Else ! If Access:subtracc.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:subtracc.Tryfetch(sub:Account_Number_Key) = Level:Benign
            jobb:AccountName = tra:Company_Name
            jobb:Booked      = job_ali:Date_Booked
            jobb:Completed   = job_ali:Date_Completed
            jobb:TheDays     = Today() - job_ali:Date_Booked

            If job_ali:Chargeable_Job = 'YES' and job_ali:Warranty_Job = 'YES'
                jobb:ChargeType = job_ali:Charge_Type
                jobb:Type       = 'C'
                Access:JOBBOUNCER.TryInsert()
                jobb:ChargeType = job_ali:Warranty_Charge_Type
                jobb:Type       = 'W'
                Access:JOBBOUNCER.TryInsert()
            Else !If job_ali:Chargeale_Job = 'YES' and job_ali:Warranty_Job = 'YES'
                If job_ali:Chargeable_Job = 'YES'
                    jobb:ChargeType = job_ali:Charge_Type
                    jobb:Type       = 'C'
                    Access:JOBBOUNCER.TryInsert()
                End !If job_ali:Chargeable_Job = 'YES'
                If job_ali:Warranty_Job = 'YES'
                    jobb:ChargeType = job_ali:Warranty_Charge_Type
                    jobb:Type       = 'W'
                    Access:JOBBOUNCER.TryInsert()
                End !If job_ali:Chargeable_Job = 'YES'
            End !If job_ali:Chargeale_Job = 'YES' and job_ali:Warranty_Job = 'YES'
        End !If Access:JOBBOUNCER.PrimeRecord() = Level:Benign
    End !Loop
    Access:JOBS_ALIAS.RestoreFile(Save_job_ali_ID)

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020389'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseJobBouncers')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBBOUNCER.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:JOBOUTFL.Open
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  SELF.FilesOpened = True
  Do BuildList
  
  
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:JOBBOUNCER,SELF)
  BRW5.Init(?List:2,Queue:Browse:1.ViewPosition,BRW5::View:Browse,Queue:Browse:1,Relate:JOBOUTFL,SELF)
  BRW6.Init(?List:3,Queue:Browse:2.ViewPosition,BRW6::View:Browse,Queue:Browse:2,Relate:PARTS,SELF)
  BRW7.Init(?List:4,Queue:Browse:3.ViewPosition,BRW7::View:Browse,Queue:Browse:3,Relate:WARPARTS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = job:Manufacturer
  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Found
      If ~man:UseInvTextForFaults
          ?List:2{prop:Hide} = 1
          ?OutFaultsTab{prop:Text} = ''
      End !If man:UseInvTextForFaults
  Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Error
  End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
  tmp:ReturnValue = job:Account_Number
  
  If func:Type <> 'V'
      ?ContactDetails{prop:Hide} = 0
      ?ViewJob{prop:Hide} = 1
      ?BouncerHistoryReport{prop:Hide} = 0
  End !func:Type <> 'V'
  ! Save Window Name
   AddToLog('Window','Open','BrowseJobBouncers')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,jobb:JobNumberKey)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,jobb:JobNumber,1,BRW4)
  BIND('tmp:Bouncer',tmp:Bouncer)
  BIND('tmp:FaultDescription',tmp:FaultDescription)
  BIND('tmp:EngineersNotes',tmp:EngineersNotes)
  ?List{PROP:IconList,1} = '~bred.ico'
  ?List{PROP:IconList,2} = '~notick1.ico'
  BRW4.AddField(jobb:JobNumber,BRW4.Q.jobb:JobNumber)
  BRW4.AddField(jobb:AccountName,BRW4.Q.jobb:AccountName)
  BRW4.AddField(jobb:Booked,BRW4.Q.jobb:Booked)
  BRW4.AddField(jobb:Completed,BRW4.Q.jobb:Completed)
  BRW4.AddField(jobb:ChargeType,BRW4.Q.jobb:ChargeType)
  BRW4.AddField(tmp:Bouncer,BRW4.Q.tmp:Bouncer)
  BRW4.AddField(jobb:TheDays,BRW4.Q.jobb:TheDays)
  BRW4.AddField(jobb:Type,BRW4.Q.jobb:Type)
  BRW4.AddField(tmp:FaultDescription,BRW4.Q.tmp:FaultDescription)
  BRW4.AddField(tmp:EngineersNotes,BRW4.Q.tmp:EngineersNotes)
  BRW4.AddField(jobb:RecordNumber,BRW4.Q.jobb:RecordNumber)
  BRW5.Q &= Queue:Browse:1
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,joo:JobNumberKey)
  BRW5.AddRange(joo:JobNumber,jobb:JobNumber)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,joo:FaultCode,1,BRW5)
  BRW5.AddField(joo:FaultCode,BRW5.Q.joo:FaultCode)
  BRW5.AddField(joo:Description,BRW5.Q.joo:Description)
  BRW5.AddField(joo:RecordNumber,BRW5.Q.joo:RecordNumber)
  BRW5.AddField(joo:JobNumber,BRW5.Q.joo:JobNumber)
  BRW6.Q &= Queue:Browse:2
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,par:Part_Number_Key)
  BRW6.AddRange(par:Ref_Number,jobb:JobNumber)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,par:Part_Number,1,BRW6)
  BRW6.AddField(par:Part_Number,BRW6.Q.par:Part_Number)
  BRW6.AddField(par:Description,BRW6.Q.par:Description)
  BRW6.AddField(par:Record_Number,BRW6.Q.par:Record_Number)
  BRW6.AddField(par:Ref_Number,BRW6.Q.par:Ref_Number)
  BRW7.Q &= Queue:Browse:3
  BRW7.RetainRow = 0
  BRW7.AddSortOrder(,wpr:Part_Number_Key)
  BRW7.AddRange(wpr:Ref_Number,jobb:JobNumber)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,wpr:Part_Number,1,BRW7)
  BRW7.AddField(wpr:Part_Number,BRW7.Q.wpr:Part_Number)
  BRW7.AddField(wpr:Description,BRW7.Q.wpr:Description)
  BRW7.AddField(wpr:Record_Number,BRW7.Q.wpr:Record_Number)
  BRW7.AddField(wpr:Ref_Number,BRW7.Q.wpr:Ref_Number)
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  BRW5.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  BRW6.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  BRW7.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBBOUNCER.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:JOBOUTFL.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseJobBouncers')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?BouncerHistoryReport
      glo:Select2 = job:ESN
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020389'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020389'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020389'&'0')
      ***
    OF ?ContactDetails
      ThisWindow.Update
      !Get The First Job Number
      Set(jobb:JobNumberKey,jobb:JobNumberKey)
      Previous(JOBBOUNCER)
      
      !Get the Head Account of the current Job
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = job:Account_Number
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Found
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = sub:Main_Account_Number
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              tmp:CurrentAccountNumber    = tra:Account_Number
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
      !Get the Head Account of the Bouncer Job
      Error# = 0
      Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
      job_ali:Ref_Number  = jobb:JobNumber
      If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
          !Found
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number  = job_ali:Account_Number
          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Found
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number  = sub:Main_Account_Number
              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Found
                  If tra:Account_Number = tmp:CurrentAccountNumber
                      Case Missive('The account of the current job is the same as the previous job.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Error# = 1
                  End !If tra:Account_Number = tmp:CurrentAccountNumber
              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
      Else ! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
      
      If Error# = 0
          If BouncerContactDetails(jobb:JobNumber)
              Set(jobb:JobNumberKey,jobb:JobNumberKey)
              Previous(JOBBOUNCER)
              Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
              job_ali:Ref_Number  = jobb:JobNumber
              If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
                  !Found
                  tmp:ReturnValue = job_ali:Account_Number
              Else ! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
                  !Error
              End !If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
              Post(Event:CloseWindow)
          End !If BouncerContactDetails(jobb:JobNumber)
      End !jobb:JobNumber,tra:Account_Number)
    OF ?BouncerHistoryReport
      ThisWindow.Update
      Bouncer_History
      ThisWindow.Reset
      glo:Select2 = ''
    OF ?ViewJob
      ThisWindow.Update
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw4.q.jobb:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          GlobalRequest = ChangeRecord
          Update_Jobs_Rapid
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.SetQueueRecord PROCEDURE

  CODE
  Access:JOBNOTES_ALIAS.Clearkey(jbn_ali:RefNumberKey)
  jbn_ali:RefNumber   = jobb:JobNumber
  If Access:JOBNOTES_ALIAS.Tryfetch(jbn_ali:RefNumberKey) = Level:Benign
      !Found
      tmp:FaultDescription    = jbn_ali:Fault_Description
      tmp:EngineersNotes      = jbn_ali:Engineers_Notes
  
  Else ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
      !Error
  
  End !If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
  PARENT.SetQueueRecord
  IF (jobb:TheDays < 90)
    SELF.Q.tmp:Bouncer_Icon = 1
  ELSE
    SELF.Q.tmp:Bouncer_Icon = 2
  END


BRW4.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  Case jobb:Type
      Of 'C'
          ?ChargeableTab{prop:Hide} = 0
          ?WarrantyTab{prop:Hide} = 1
      Of 'W'
          ?ChargeableTab{prop:Hide} = 1
          ?WarrantyTab{prop:Hide} = 0
  End !jobb:Type
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  RETURN ReturnValue


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

BouncerContactDetails PROCEDURE (func:JobNumber)      !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ReturnValue      BYTE(0)
window               WINDOW('Contact Details'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Contact Details'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Account Number:'),AT(222,160),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s15),AT(298,160),USE(tra:Account_Number),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(298,170),USE(tra:Company_Name),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Account Name:'),AT(222,170),USE(?Prompt1:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Account Address:'),AT(222,184),USE(?Prompt1:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(298,184),USE(tra:Address_Line1),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Telephone Number:'),AT(222,218),USE(?Prompt1:4),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(298,194),USE(tra:Address_Line2),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(298,208),USE(tra:Address_Line3),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s15),AT(298,218),USE(tra:Telephone_Number),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fax Number:'),AT(222,232),USE(?Prompt1:5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s15),AT(298,232),USE(tra:Fax_Number),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Email Address:'),AT(222,242),USE(?Prompt1:6),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s255),AT(298,242),USE(tra:EmailAddress),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(448,278),USE(?AuthoriseBilling),TRN,FLAT,LEFT,ICON('aubillp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:ReturnValue)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020387'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BouncerContactDetails')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
  job_ali:Ref_Number  = func:JobNumber
  If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
      !Found
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = job_ali:Account_Number
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Found
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = sub:Main_Account_Number
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
  
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  Else ! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
      !Error
  End !If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
  Relate:ACCAREAS_ALIAS.Open
  Relate:JOBS_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BouncerContactDetails')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:JOBS_ALIAS.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BouncerContactDetails')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020387'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020387'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020387'&'0')
      ***
    OF ?AuthoriseBilling
      ThisWindow.Update
      If SecurityCheck('BOUNCER - AUTHORISE BILLING')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('BOUNCER - AUTHORISE BILLING')
          Case Missive('If you continue the account details on the job will be changed.'&|
            '<13,10>'&|
            '<13,10>' & Clip(tra:Company_Name) & ' will be charged for this repair.'&|
            '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  tmp:ReturnValue = 1
              Of 1 ! No Button
          End ! Case Missive
          Post(Event:CloseWindow)
      End !SecurityCheck('BOUNCER - AUTHORISE BILLING')
    OF ?Close
      ThisWindow.Update
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
InsertLoanCharge PROCEDURE                            !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Charge           REAL
window               WINDOW('Insert Charge'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert Loan Charge'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(368,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Charge'),AT(248,202),USE(?tmp:Charge:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(324,202,64,10),USE(tmp:Charge),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Charge'),TIP('Charge'),UPR
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Charge)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020391'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InsertLoanCharge')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?tmp:Charge:Prompt{prop:Text} = Clip(GETINI('RENAME','CourierCostName',,CLIP(PATH())&'\SB2KDEF.INI'))
  ! Save Window Name
   AddToLog('Window','Open','InsertLoanCharge')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','InsertLoanCharge')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020391'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020391'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020391'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateChaInvoice PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
sav:HandlingFee      REAL
sav:ARCCourierCost   REAL
sav:ARCLabourCost    REAL
sav:ARCPartsCost     REAL
sav:RRCCourierCost   REAL
sav:RRCLabourCost    REAL
sav:RRCPartsCost     REAL
sav:ARCVat           REAL
sav:ARCTotal         REAL
sav:RRCVat           REAL
sav:RRCTotal         REAL
ActionMessage        CSTRING(40)
tmp:ARCCourierCost   REAL
tmp:ARCLabourCost    REAL
tmp:ARCPartsCost     REAL
tmp:ARCSubTotal      REAL
tmp:ARCVat           REAL
tmp:ARCTotal         REAL
tmp:RRCCourierCost   REAL
tmp:RRCLabourCost    REAL
tmp:RRCPartsCost     REAL
tmp:RRCSubTotal      REAL
tmp:RRCVat           REAL
tmp:RRCTotal         REAL
tmp:HandlingFee      REAL
locAuditNotes        STRING(255)
History::inv:Record  LIKE(inv:RECORD),STATIC
QuickWindow          WINDOW('Update Invoice'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Invoice'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,160,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('General'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Invoice Number'),AT(168,98),USE(?inv:Invoice_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(244,98,64,10),USE(inv:Invoice_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Date Created'),AT(168,114),USE(?inv:Date_Created:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(244,114,64,10),USE(inv:Date_Created),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Labour V.A.T. Rate'),AT(168,130),USE(?inv:Vat_Rate_Labour:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@N14.2B),AT(244,130,64,10),USE(inv:Vat_Rate_Labour),DECIMAL(14),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Parts V.A.T. Rate'),AT(168,146),USE(?inv:Vat_Rate_Parts:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@N14.2B),AT(244,146,64,10),USE(inv:Vat_Rate_Parts),DECIMAL(14),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       SHEET,AT(328,82,188,248),USE(?Sheet2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('ARC Costs'),USE(?ARCTab),HIDE
                           PROMPT('ARC Invoice Date'),AT(332,106),USE(?inv:ARCInvoiceDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(408,106,64,10),USE(inv:ARCInvoiceDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('ARC Invoice Date'),TIP('ARC Invoice Date'),UPR
                           PROMPT('Lost Loan Fee'),AT(332,122),USE(?tmp:ARCCourierCost:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,122,64,10),USE(tmp:ARCCourierCost),HIDE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Lost Loan Fee'),TIP('Lost Loan Fee'),UPR
                           PROMPT('Labour Cost'),AT(332,138),USE(?tmp:ARCLabourCost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,138,64,10),USE(tmp:ARCLabourCost),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Labour Cost'),TIP('Labour Cost'),UPR
                           PROMPT('Parts Cost'),AT(332,154),USE(?tmp:ARCPartsCost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,154,64,10),USE(tmp:ARCPartsCost),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Parts Cost'),TIP('Parts Cost'),UPR
                           PROMPT('Sub Total'),AT(332,174),USE(?tmp:ARCSubTotal:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,174,64,10),USE(tmp:ARCSubTotal),SKIP,TRN,RIGHT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('ARC SubTotal'),TIP('ARC SubTotal'),UPR,READONLY
                           PROMPT('V.A.T.'),AT(332,190),USE(?tmp:ARCVat:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,190,64,10),USE(tmp:ARCVat),SKIP,TRN,RIGHT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('V.A.T.'),TIP('V.A.T.'),UPR,READONLY
                           PROMPT('Total'),AT(332,206),USE(?tmp:ARCTotal:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,206,64,10),USE(tmp:ARCTotal),SKIP,TRN,RIGHT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Total'),TIP('Total'),UPR,READONLY
                         END
                         TAB('RRC Costs'),USE(?RRCTab),HIDE
                           PROMPT('RRC Invoice Date'),AT(332,106),USE(?inv:RRCInvoiceDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(408,106,64,10),USE(inv:RRCInvoiceDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('RRC Invoice Date'),TIP('RRC Invoice Date'),UPR
                           PROMPT('Lost Loan Fee'),AT(332,122),USE(?tmp:RRCCourierCost:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,122,64,10),USE(tmp:RRCCourierCost),HIDE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Lost Loan Fee'),TIP('Lost Loan Fee'),UPR
                           PROMPT('Labour Cost'),AT(332,138),USE(?tmp:RRCLabourCost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,138,64,10),USE(tmp:RRCLabourCost),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Labour Cost'),TIP('Labour Cost'),UPR
                           PROMPT('Parts Cost'),AT(332,154),USE(?tmp:RRCPartsCost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,154,64,10),USE(tmp:RRCPartsCost),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Parts Cost'),TIP('Parts Cost'),UPR
                           PROMPT('Sub Total'),AT(332,174),USE(?tmp:RRCSubTotal:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,174,64,10),USE(tmp:RRCSubTotal),SKIP,TRN,RIGHT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Sub Total'),TIP('Sub Total'),UPR,READONLY
                           PROMPT('V.A.T.'),AT(332,190),USE(?tmp:RRCVat:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,190,64,10),USE(tmp:RRCVat),SKIP,TRN,RIGHT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('V.A.T.'),TIP('V.A.T.'),UPR,READONLY
                           PROMPT('RRC Total'),AT(332,206),USE(?tmp:RRCTotal:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,206,64,10),USE(tmp:RRCTotal),SKIP,TRN,RIGHT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('RRC Total'),TIP('RRC Total'),UPR,READONLY
                           PROMPT('Handling Fee'),AT(332,234),USE(?tmp:HandlingFee:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(408,234,64,10),USE(tmp:HandlingFee),HIDE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Handling Fee'),TIP('Handling Fee'),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

Totals      Routine
    tmp:ARCSubTotal = tmp:ARCCourierCost + tmp:ARCLabourCost + tmp:ARCPartsCost
    tmp:ARCVat = tmp:ARCCourierCost * inv:Vat_Rate_Labour/100 + |
                tmp:ARCLabourCost * inv:Vat_Rate_Labour/100 + |
                tmp:ARCPartsCost * inv:Vat_Rate_Parts/100
    tmp:ARCTotal = tmp:ARCSubTotal + tmp:ARCVat

    tmp:RRCSubTotal = tmp:RRCCourierCost + tmp:RRCLabourCost + tmp:RRCPartsCost
    tmp:RRCVat = tmp:RRCCourierCost * inv:Vat_Rate_Labour/100 + |
                tmp:RRCLabourCost * inv:Vat_Rate_Labour/100 + |
                tmp:RRCPartsCost * inv:Vat_Rate_Parts/100
    tmp:RRCTotal = tmp:RRCSubTotal + tmp:RRCVat

    Display()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020408'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateChaInvoice')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(inv:Record,History::inv:Record)
  SELF.AddHistoryField(?inv:Invoice_Number,1)
  SELF.AddHistoryField(?inv:Date_Created,4)
  SELF.AddHistoryField(?inv:Vat_Rate_Labour,8)
  SELF.AddHistoryField(?inv:Vat_Rate_Parts,9)
  SELF.AddHistoryField(?inv:ARCInvoiceDate,35)
  SELF.AddHistoryField(?inv:RRCInvoiceDate,34)
  SELF.AddUpdateFile(Access:INVOICE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:INVOICE.Open
  Access:JOBSE.UseFile
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:INVOICE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Access:JOBS.Clearkey(job:InvoiceNumberKey)
  job:Invoice_Number  = inv:Invoice_Number
  If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
      !Found
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
          If jobe:WebJob
              !Show RRC Tab
              ?RRCTab{prop:Hide} = 0
              If SentToHub(job:Ref_Number)
                  !Show ARC Tab
                  ?ARCTab{prop:Hide} = 0
                  If job:Invoice_Courier_Cost <> 0
                      ?tmp:ARCCourierCost{prop:Hide} = 0
                      ?tmp:ARCCourierCost:Prompt{prop:Hide} = 0
                      tmp:ARCCourierCost = job:Invoice_Courier_Cost
                  End !If job:Invoice_Courier_Cost <> 0
                  tmp:ARCLabourCost   = job:Invoice_Labour_Cost
                  tmp:ARCPartsCost    = job:Invoice_Parts_Cost
                  ?tmp:HandlingFee{prop:Hide} = 0
                  ?tmp:HandlingFee:Prompt{prop:Hide} = 0
                  tmp:HandlingFee     = jobe:InvoiceHandlingFee
              End !If SentToHub(job:Ref_Number)
              If job:Invoice_Courier_Cost <> 0
                  ?tmp:RRCCourierCost{prop:Hide} = 0
                  ?tmp:RRCCourierCost:Prompt{prop:Hide} = 0
                  tmp:RRCCourierCost = job:Invoice_Courier_Cost
              End !If job:Invoice_Courier_Cost <> 0
              tmp:RRCLabourCost   = jobe:InvRRCCLabourCost
              tmp:RRCPartsCost    = jobe:InvRRCCPartsCost
          Else !If jobe:WebJob
              !Show ARC Tab
              ?ARCTab{prop:Hide} = 0
              tmp:ARCLabourCost   = job:Invoice_Labour_Cost
              tmp:ARCPartsCost    = job:Invoice_Parts_Cost
              If job:Invoice_Courier_Cost <> 0
                  ?tmp:ARCCourierCost{prop:Hide} = 0
                  ?tmp:ARCCourierCost:Prompt{prop:Hide} = 0
                  tmp:ARCCourierCost = job:Invoice_Courier_Cost
              End !If job:Invoice_Courier_Cost <> 0
  
          End !If jobe:WebJob
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  Else ! If Access:JOBS.Tryfetch(job:Invoice_Number_Key) = Level:Benign
      !Error
  End !If Access:JOBS.Tryfetch(job:Invoice_Number_Key) = Level:Benign
  
  Do Totals
  
  sav:HandlingFee     = tmp:HandlingFee
  sav:ARCCourierCost  = tmp:ARCCourierCost
  sav:ARCLabourCost   = tmp:ARCLabourCost
  sav:ARCPartsCost    = tmp:ARCPartsCost
  sav:RRCCourierCost  = tmp:RRCCourierCost
  sav:RRCLabourCost   = tmp:RRCLabourCost
  sav:RRCPartsCost    = tmp:RRCPartsCost
  sav:ARCVat          = tmp:ARCVat
  sav:ARCTotal        = tmp:ARCTotal
  sav:RRCVat          = tmp:RRCVat
  sav:RRCTotal        = tmp:RRCTotal
  ! Save Window Name
   AddToLog('Window','Open','UpdateChaInvoice')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateChaInvoice')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020408'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020408'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020408'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If sav:HandlingFee <> tmp:HandlingFee Or |
      sav:ARCCourierCost <> tmp:ARCCourierCost Or |
      sav:ARCLabourCost   <> tmp:ARCLabourCost Or |
      sav:ARCPartsCost    <> tmp:ARCPartsCost Or |
      sav:RRCCourierCost  <> tmp:RRCCourierCost Or |
      sav:RRCLabourCost   <> tmp:RRCLabourCost Or |
      sav:RRCPartsCost    <> tmp:RRCPartsCost
      Case Missive('You have made changed to the invoice totals.'&|
        '<13,10>'&|
        '<13,10>Are you sure you want to save these changes and update the job accordingly?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              !Reget the job, to make sure
              Access:JOBS.Clearkey(job:InvoiceNumberKey)
              job:Invoice_Number  = inv:Invoice_Number
              If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
  
                  If JobInUse(job:Ref_Number,1) = Level:Benign
  
                      !Found
                      ARCChanged# = 0
                      RRCChanged# = 0
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
                          If sav:HandlingFee <> tmp:HandlingFee
                              jobe:InvoiceHandlingFee = tmp:HandlingFee
                              RRCChanged# = 1
                          End !If sav:HandlingFee <> tmp:HandlingFee
  
                          If sav:RRCLabourCost <> tmp:RRCLabourCost
                              jobe:InvRRCClabourCost = tmp:RRCLabourCost
                              jobe:InvRRCCSubTotal = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job:Invoice_Courier_Cost
                              RRCChanged# = 1
                          End !If sav:RRCCourierCost <> tmp:RRCCourierCost
  
                          If sav:RRCPartsCost <> tmp:RRCPartsCost
                              jobe:InvRRCCPartsCost = tmp:RRCPartsCost
                              jobe:InvRRCCSubTotal = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job:Invoice_Courier_Cost
                              RRCChanged# = 1
                          End !If sav:RRCPartsCost <> tmp:RRCPartsCost
  
                          If sav:RRCCourierCost <> tmp:RRCCourierCost
                              job:Invoice_Courier_Cost = tmp:RRCCourierCost
                              job:Invoice_Sub_Total = job:Invoice_Courier_Cost + job:Invoice_Labour_Cost + job:Invoice_Parts_Cost
                              RRCChanged# = 1
                          End !If sav:RRCCourierCost <> tmp:RRCCourierCost
  
                          If sav:ARCCourierCost <> tmp:ARCCourierCost
                              job:Invoice_Courier_Cost = tmp:ARCCourierCost
                              job:Invoice_Sub_Total = job:Invoice_Courier_Cost + job:Invoice_Labour_Cost + job:Invoice_Parts_Cost
                              ARCChanged# = 1
                          End !If sav:ARCCourierCost <> tmp:ARCCourierCost
  
                          If sav:ARCLabourCost<> tmp:ARCLabourCost
                              job:Invoice_Labour_Cost = tmp:ARCLabourCost
                              job:Invoice_Sub_Total = job:Invoice_Courier_Cost + job:Invoice_Labour_Cost + job:Invoice_Parts_Cost
                              ARCChanged# = 1
                          End !If sav:ARCLabourCost<> tmp:ARCLabourCost
  
                          If sav:ARCPartsCost <> tmp:ARCPartsCost
                              job:Invoice_Parts_Cost = tmp:ARCPartsCost
                              job:Invoice_Sub_Total = job:Invoice_Courier_Cost + job:Invoice_Labour_Cost + job:Invoice_Parts_Cost
                              ARCChanged# = 1
                          End !If sav:ARCPartsCost <> tmp:ARCPartsCost
  
                          Access:JOBS.Update()
                          Access:JOBSE.Update()
  
                          If ARCChanged#
                              locAuditNotes = 'PREVIOUS:'
                              If tmp:ARCCourierCost <> 0
                                  locAuditNotes = Clip(locAuditNotes) & '<13,10>LOST LOAN: ' & Format(sav:ARCCourierCost,@d14.2)
                              End !If tmp:ARCCourierCost <> 0
                              locAuditNotes = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(sav:ARCLabourCost,@n14.2) & |
                                                      '<13,10>PARTS: ' & Format(sav:ARCPartsCost,@n14.2) & |
                                                      '<13,10>VAT: ' & Format(sav:ARCVat,@n14.2) & |
                                                      '<13,10>TOTAL: ' & Format(sav:ARCTotal,@n14.2) & |
                                                      '<13,10,13,10>NEW:'
                              If tmp:ARCCourierCost <> 0
                                  locAuditNotes = Clip(locAuditNotes) & '<13,10>LOST LOAN: ' & Format(tmp:ARCCourierCost,@n14.2)
                              End !If tmp:ARCCourierCost <> 0
                              locAuditNotes = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(tmp:ARCLabourCost,@n14.2) & |
                                                  '<13,10>PARTS: ' & Format(tmp:ARCPartsCost,@n14.2) & |
                                                  '<13,10>VAT: ' & Format(tmp:ARCVat,@n14.2) & |
                                                  '<13,10>TOTAL: ' & Format(tmp:ARCTotal,@n14.2)
                              IF (AddToAudit(job:Ref_Number,'JOB','ARC INVOICE AMENDED',locAuditNotes))
                              END ! IF
  
                          End !If ARCChanged#
  
                          If RRCChanged#
                              locAuditNotes = 'PREVIOUS:'
                              If tmp:RRCCourierCost <> 0
                                  locAuditNotes = Clip(locAuditNotes) & '<13,10>LOST LOAN: ' & Format(sav:RRCCourierCost,@d14.2)
                              End !If tmp:ARCCourierCost <> 0
                              locAuditNotes = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(sav:RRCLabourCost,@n14.2) & |
                                                      '<13,10>PARTS: ' & Format(sav:RRCPartsCost,@n14.2) & |
                                                      '<13,10>VAT: ' & Format(sav:RRCVat,@n14.2) & |
                                                      '<13,10>TOTAL: ' & Format(sav:RRCTotal,@n14.2) & |
                                                      '<13,10>HANDLING: ' & Format(sav:HandlingFee,@n14.2) & |
                                                      '<13,10,13,10>NEW:'
                              If tmp:RRCCourierCost <> 0
                                  locAuditNotes = Clip(locAuditNotes) & '<13,10>LOST LOAN: ' & Format(tmp:RRCCourierCost,@n14.2)
                              End !If tmp:ARCCourierCost <> 0
                              locAuditNotes = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(tmp:RRCLabourCost,@n14.2) & |
                                                  '<13,10>PARTS: ' & Format(tmp:RRCPartsCost,@n14.2) & |
                                                  '<13,10>VAT: ' & Format(tmp:RRCVat,@n14.2) & |
                                                  '<13,10>TOTAL: ' & Format(tmp:RRCTotal,@n14.2) & |
                                                  '<13,10>HANDLING: ' & Format(tmp:HandlingFee,@n14.2)
  
                             IF (AddToAudit(job:Ref_Number,'JOB','RRC INVOICE COSTS AMENDED',locAuditNotes))
                             END ! IF
  
                          End !If ARCChanged#
                      Else !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  End !If JobInUse(job:Ref_Number,1) = Level:Benign
              Else ! If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
                  !Error
              End !If Access:JOBS.Tryfetch(job:InvoiceNumberKey) = Level:Benign
          Of 1 ! No Button
      End ! Case Missive
  End !sav:ARCTotal <> tmp:ARCTotal Or |
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

