

   MEMBER('celrapsn.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA003.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

LocalRequest         LONG
start_help_temp      STRING(255)
pos                  STRING(255)
loc:lastcount        LONG
loc:lasttime         LONG
FilesOpened          BYTE
CurrentTab           STRING(80)
FP::PopupString      STRING(4096)
FP::ReturnValues     QUEUE,PRE(FP:)
RVPtr                LONG
                     END
comp_name_temp       STRING(255)
AllowUserToExit      BYTE
Date_Temp            STRING(60)
Time_Temp            STRING(60)
SRCF::Version        STRING(512)
SRCF::Format         STRING(512)
CPCSExecutePopUp     BYTE
BackerDDEName        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
AppFrame             WINDOW('Rapid Stock Allocation  - ServiceBase 2000'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Rapid Stock Allocation'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(368,258),USE(?Button6),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(359,196),USE(?RapidStockAllocation:2),TRN,FLAT,LEFT,ICON('rapexcp.jpg')
                       BUTTON,AT(259,196),USE(?RapidStockAllocation),TRN,FLAT,LEFT,ICON('rapstop.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
TempFilePath         CSTRING(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

Login      Routine
    glo:select1 = 'N'
    Insert_Password
    If glo:select1 = 'Y'
        Halt
    Else
        access:users.open
        access:users.usefile
        access:users.clearkey(use:password_key)
        use:password =glo:password
        access:users.fetch(use:password_key)
        set(defaults)
        access:defaults.next()
        

        Do Security_Check

        access:users.close
    End
Log_out      Routine
    access:users.open
    access:users.usefile
    access:logged.open
    access:logged.usefile
    access:users.clearkey(use:password_key)
    use:password =glo:password
    if access:users.fetch(use:password_key) = Level:Benign
        access:logged.clearkey(log:user_code_key)
        log:user_code = use:user_code
        log:time      = glo:timelogged
        if access:logged.fetch(log:user_code_key) = Level:Benign
            delete(logged)
        end !if
    end !if
    access:users.close
    access:logged.close
Security_Check      Routine

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020574'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, AppFrame, 1)  !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  !Look for a redirection file
  !set path and update procedure
  g_path = GETINI('global','datapath','xx7fail8zz', clip(path()) & '\celweb.ini')
  !message('g_path is ' & clip(g_path))
  if clip(g_path)='xx7fail8zz' then
      !not using an ini file - let it go onto own checks
  ELSE
      setpath (clip(g_path))
  end
  
  Relate:DEFAULTS.Open
  Relate:DEFAULTV.Open
  Relate:LOGGED.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  OPEN(AppFrame)
  SELF.Opened=True
      ! Close Splash Window (DBH: 24-05-2005)
      PUTINI('STARTING','Run',True,)
      omit('***',ClarionetUsed=0)
      If ~clarionetserver:Active()
      ***
      Channel = DDESERVER('CELRAPSN') !set program as a DDE Server
      omit('***',ClarionetUsed=0)
      End !If clarionetserver:Active()
      ***
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  error# = 1
  
  !if GLO:webJob then
  !    thiswindow{prop:width} = 677
  !    thiswindow{prop:height} = 339
  !else
  !    thiswindow{prop:width} = 680
  !    thiswindow{prop:height} = 387
  !end
  !
  !if GLO:webJob then
  !    !glo:Password was set on enty to the passed variable - LogonUserPassword
  !    access:users.clearkey(use:password_key)
  !    use:password = glo:Password
  !    if access:users.fetch(use:password_key) = level:benign then error#=0.
  !    If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today() then error#=1.
  !    
  !    !check the traders details
  !    a#=instring(',',ClarioNET:Global.Param2,1,1)
  !    access:tradeacc.clearkey(tra:account_number_key)
  !    tra:account_number = clip(ClarioNET:Global.Param2[1:a#-1])
  !    access:tradeacc.fetch(tra:account_number_key)
  !    If Clip(tra:password)<> ClarioNET:Global.Param2[a#+1:len(clip(ClarioNET:Global.Param2))]
  !        halt(0,'Incorrect Trade details')
  !    END
  !    unhide(?menubutton)
  !    !Send back the version number to the client
  !    set(defaultv)
  !    if access:defaultv.next()
  !        !error  - this is handled elsewhere ignored here
  !    ELSE
  !        !pass current version number back to client
  !        ClarioNET:CallClientProcedure('VERSIONSAVE',defv:VersionNumber)
  !    END
  !
  !ELSE
      Loop x# = 1 To Len(Clip(Command()))
          If Sub(Command(),x#,1) = '%'
              glo:Password = Clip(Sub(Command(),x#+1,30))
              Access:USERS.Clearkey(use:Password_Key)
              use:Password    = glo:Password
              If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  !Found
                  error# = 0
                  If use:RenewPassword <> 0 
                      If use:PasswordLastChanged = ''
                          use:PasswordLastChanged = Today()
                          Access:USERS.Update()
                      Else !If use:PasswordLastChanged = ''
                          If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today() 
                              error#=1
                          End
                      End
                  End !If use:RenewPassword <> 0
              Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              
              Break
          End!If Sub(Command(),x#,1) = '%'
      End!Loop x# = 1 To Len(Comman())
  !END !if GLO:WebJob
  
  If error# = 1
      Do Login
  End
  
  0{prop:statustext,2} = 'Current User: '& Capitalize(Clip(use:forename) & ' ' & Clip(use:surname))
  0{prop:statustext,3} = 'Site Location: '& Clip(use:location)
  
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Do log_out
    Relate:DEFAULTS.Close
    Relate:DEFAULTV.Close
    Relate:LOGGED.Close
    Relate:USERS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button6
      if glo:webJob
          ClarioNET:CallClientProcedure('CLIPBOARD','BREAK')   !'WM30SYS.exe' & ' %' & Clip(glo:Password))
          !Now log out
          access:users.clearkey(use:password_key)
          use:password = glo:password
          if access:users.fetch(use:password_key) = Level:Benign
              access:logged.clearkey(log:user_code_key)
              log:user_code = use:user_code
              log:time      = clip(ClarioNET:Global.Param3)   !glo:TimeLogged
              if access:logged.fetch(log:user_code_key) = Level:Benign
                  delete(logged)
              end !if
          end !if
      end
    OF ?RapidStockAllocation
      !    If GetTempPathA(255,TempFilePath)
      !        If Sub(TempFilePath,-1,1) = '\'
      !            glo:FileName = Clip(TempFilePath) & 'RAPIDSTOCK' & Clock() & '.TMP'
      !        Else !If Sub(TempFilePath,-1,1) = '\'
      !            glo:FileName = Clip(TempFilePath) & '\RAPIDSTOCK' & Clock() & '.TMP'
      !        End !If Sub(TempFilePath,-1,1) = '\'
      !    End
      !
      !    Remove(glo:FileName)
      glo:FileName    = 'RAPSTO' & Format(Today(),@d11) & Clip(Clock()) & '.TMP'
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020574'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020574'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020574'&'0')
      ***
    OF ?Button6
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?RapidStockAllocation:2
      ThisWindow.Update
      RapidExchangeAllocation
      ThisWindow.Reset
    OF ?RapidStockAllocation
      ThisWindow.Update
      RSA_MainBrowse
      ThisWindow.Reset
      Remove(glo:FileName)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      omit('***',ClarionetUsed=0)
      If ~clarionetserver:Active()
      ***
      IF EVENT() = Event:DDEexecute
          IF DDEVALUE() = 'INFRONT' !tells it to bring itself to the front
              AppFrame{PROP:Iconize} = FALSE
              BringWindowToTop(AppFrame{PROP:Handle}) !and this does it
          END
      END
      omit('***',ClarionetUsed=0)
      End !If clarionetserver:Active()
      ***
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Do Log_out
      Do Login
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

XFiles PROCEDURE                                      !Generated from procedure template - Window

window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SRNTEXT.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

