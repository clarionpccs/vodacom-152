

   MEMBER('waycour.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('WAYCO001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

CC_UseSystem         STRING(1)
NewTimer             LONG
window               WINDOW('Waybill Confirmation'),AT(,,135,21),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(500),GRAY,ICONIZE,DOUBLE,IMM
                       BUTTON('Exit'),AT(4,2,56,16),USE(?Button1)
                       STRING('This should be left'),AT(68,2),USE(?String1),TRN
                       STRING('running constantly'),AT(68,10),USE(?String2),TRN
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

CheckWaybillConf    Routine

    !TB13370 - CourierConfirmation
    !J - April 2015
    !Updated J - 18/06/15 - This was holding WAYBCONF open so cannot be updated by other programmes
    !Added check on timer to make this at least 5 seconds and moved opening files to another procedure

    !read the general defaults
    CC_UseSystem     = GetIni('CourierConf','UseSystem'     ,'N', clip(path())&'\SB2KDEF.INI')
    NewTimer         = GetIni('CourierConf','Timer'         ,100 , clip(path())&'\SB2KDEF.INI')

    !check this is not set to something stupid
    !ensure it is between 5 and 60 seconds
    if NewTimer < 500  then Newtimer = 500.
    if NewTimer > 6000 then Newtimer = 6000.

    if CC_UseSystem <> 'Y' then
        !emergency closedown
        POST(Event:CloseWindow)
    END

    !has the timer changed?
    0{Prop:timer} = NewTimer

    !Move main loop (including opening the files) to a separate source procedure
    !TB13615 - J - 08/10/15 - did get a duplicate line being posted.
    !I think this was due to the Confirm loop taking more than five seconds the first time it was being run so I am adding belt and braces
    !Do not let the loop be restarted if it is already running - new global variable in use
    if ProcessRunning = False then
        ConfirmLoop
    END



    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button1
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
      Do CheckWaybillConf
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ConfirmLoop          PROCEDURE                        ! Declare Procedure
CC_UseSystem         STRING(1)
CC_UserName          STRING(50)
CC_UserPassword      STRING(50)
CC_URL               STRING(225)
CC_PathToExe         STRING(255)
Timer                LONG
OutFileName          STRING(255),STATIC
InFilename           STRING(255),STATIC
FromAccountName      STRING(30)
FromAccountAddress   STRING(150)
ToAccountName        STRING(30)
ToAccountAddress     STRING(150)
WaybillPrefix        STRING(20)
MakeModel            STRING(50)
SecurityPack         STRING(30)
OutFile FILE,DRIVER('ASCII'),PRE(OUF),NAME(OutFileName),CREATE,BINDABLE,THREAD
Record  Record
line1       String(255)
            End
        End
  CODE
   Relate:WAYBILLS.Open
   Relate:WAYBCONF.Open
   Relate:TRADEACC.Open
   Relate:SUBTRACC.Open
   Relate:WAYBILLJ.Open
   Relate:JOBS.Open
   Relate:EXCHANGE.Open
   Relate:AUDIT.Open
   Relate:AUDIT2.Open
   Relate:AUDITE.Open
   Relate:COURIER.Open
   Relate:JOBSCONS.Open
    !main access loop through unsent records in WayBConf

    CC_PathToExe     = GetIni('CourierConf','Path'          ,'' , clip(path())&'\SB2KDEF.INI')

    !TB13615 - J - 08/10/15 protect the main loop from being called if still running
    ProcessRunning = true

    Loop

        !key set within the loop becuse WAC:ConfirmationSent is updated in code
        Access:WayBConf.clearkey(WAC:KeyConfirmOnly)
        WAC:ConfirmationSent = false
        Set(WAC:KeyConfirmOnly,WAC:KeyConfirmOnly)

        if access:Waybconf.next() then
            break
        END
        if WAC:ConfirmationSent = true then
            break
        END

        !Look up related files - will be needed later
        Access:Waybills.clearkey(way:WayBillNumberKey)
        way:WayBillNumber = WAC:WaybillNo
        If access:Waybills.fetch(way:WayBillNumberKey) then
            !error
            cou:InterfaceUse = 0       
        ELSE

            IF clip(way:WaybillID) = '300'  !This is a sundry waybill - use the courier on the Waybill
                Access:courier.clearkey(cou:Courier_Key)
                cou:Courier = way:Courier
                if access:Courier.fetch(cou:Courier_Key) then
                    cou:InterfaceUse = 0
                END
            ELSE

                !try for the jobscons - any job on this waybill should share the same courier so find one ...
                !Using the consignment number key did not work - try for a job on this waybill

                !Also sometimes the WAYBILLS.SecurityPackNumber is blank but the WAYBILLSJ.securityPackNumbers have been filled in
                !This allows you to pick up the ...J version if the ...S version is missing
                SecurityPack = way:SecurityPackNumber

                Access:Waybillj.clearkey(waj:JobNumberKey)  !has waybillnumber as first component - stupid name
                waj:WayBillNumber = WAC:WaybillNo
                waj:JobNumber     = 0
                Set(waj:JobNumberKey,waj:JobNumberKey)
                Loop

                    If Access:WAYBILLJ.NEXT() then
                        cou:InterfaceUse = 0    
                        break
                    END

                    If waj:WayBillNumber <> WAC:WaybillNo then
                        cou:InterfaceUse = 0    
                        break
                    END

                    if clip(SecurityPack) = '' and waj:SecurityPackNumber <> ''
                        SecurityPack = waj:SecurityPackNumber
                    END !if SecurityPack needed updating

                    Access:jobscons.clearkey(joc:DateKey)       !Has joc:RefNumber as first component - stupid name
                    joc:RefNumber  = waj:JobNumber
                    joc:TheDate    = 0
                    joc:TheTime    = 0
                    Set(joc:DateKey,joc:DateKey)
                    Loop

                        If Access:Jobscons.next() then
                            Cou:interfaceUse = 0
                            break
                        END

                        if joc:RefNumber <> waj:JobNumber then
                            Cou:Interfaceuse = 0
                            break
                        END

                        if clip(joc:ConsignmentNumber) <> clip(WAC:WaybillNo) then
                            !not found try for another jobscons record
                            cycle
                        END

                        !if we get here we have a valid JOBSCONS record - yippee
                        if joc:DespatchTo = 'CUSTOMER' then
                            !don't want this one saved
                            cou:InterfaceUse = 0
                            Break

                        ELSE
                            Access:courier.clearkey(cou:Courier_Key)
                            cou:Courier = joc:Courier
                            if access:Courier.fetch(cou:Courier_Key) then
                                cou:InterfaceUse = 0 
                            END
                            Break
                        END !if despatch to customer

                    END !Loop through jobscons

                    BREAK

                END !loop through waybillj

            END !if sundry waybill


!the following bit of code does not work! replaced with that above
!                Access:jobscons.clearkey(joc:ConsignmentNumberKey)
!                joc:ConsignmentNumber = clip(way:WayBillNumber)     !change to a string
!                Set(joc:ConsignmentNumberKey,joc:ConsignmentNumberKey)
!                if access:Jobscons.next() then
!                    !not found
!                    cou:InterfaceUse = 3    
!                ELSE
!                    if clip(joc:ConsignmentNumber) <> clip(way:WayBillNumber) then
!                        !not found
!                        cou:InterfaceUse = 4    !will revert to 0
!                    ELSE
!                        if joc:DespatchTo = 'CUSTOMER' then
!                            !don't want this one saved
!                            cou:InterfaceUse = 5    !will revert to 0
!                        ELSE
!                            Access:courier.clearkey(cou:Courier_Key)
!                            cou:Courier = joc:Courier
!                            if access:Courier.fetch(cou:Courier_Key) then cou:InterfaceUse = 6. !will revert to 0
!                        END !if despatch to customer
!                    END !if not matching waybillnuber
!                END !if jobscons.next
!
!            ELSE
!                Access:courier.clearkey(cou:Courier_Key)
!                cou:Courier = way:Courier
!                if access:Courier.fetch(cou:Courier_Key) then cou:InterfaceUse = 7. !Will revert to 0
!            END


        END !if waybill fetched



        if cou:InterfaceUse = 0 then
            !not wanted in the file anymore
            Access:Waybconf.deleterecord()
            cycle
        END  !if courier not using interface


        !Have the setting been made for the courier
        if clip(cou:InterfaceName) = '' or clip(cou:InterfacePassword) = '' or clip(cou:InterfaceURL) = '' then
            !not wanted in the file anymore
            Access:Waybconf.deleterecord()
            cycle
        END

        !these checks moved here - so that the deletes will still work, even if nothing has been set up
        if clip(CC_PathToExe) = '' then cycle.
        if not exists(clip(CC_PathToExe)) then cycle.
        if not exists(clip(CC_PathToExe)&'\CourierClient.exe') then cycle.

        CC_UserName     = cou:InterfaceName
        CC_UserPassword = cou:InterfacePassword
        CC_URL          = cou:InterfaceURL

        Do BuildFile

        !Call Brian's connection programme
        run(clip(CC_PathToExe)&'\CourierClient.exe' &' '&clip(OutFilename)&' '&clip(InFilename), 0)

        !record the number of times this has been tried
        WAC:ConfirmationSent = true     !WAC:ConfirmationSent = true will upset the key on the loop
        WAC:ConfirmResentQty += 1   

        !ensure dates are blank if possible as VCP may have to set them to something
        if WAC:ConfirmationReceived = 0 then
            WAC:ConfirmDate = ''
            WAC:ConfirmTime = ''
        END
        if WAC:Delivered = 0 then
            WAC:DeliverDate = ''
            WAC:DeliverTime = ''
        END
        if WAC:Received = 0 then
            WAC:ReceiveDate = ''
            WAC:ReceiveTime = ''
        END

        Access:WaybConf.update()

    END !Main loop

    !TB13615 - J - 08/10/15 protect the main loop from being called if still running
    ProcessRunning = false
   Relate:WAYBILLS.Close
   Relate:WAYBCONF.Close
   Relate:TRADEACC.Close
   Relate:SUBTRACC.Close
   Relate:WAYBILLJ.Close
   Relate:JOBS.Close
   Relate:EXCHANGE.Close
   Relate:AUDIT.Close
   Relate:AUDIT2.Close
   Relate:AUDITE.Close
   Relate:COURIER.Close
   Relate:JOBSCONS.Close
BuildFile       Routine

    OutFileName = clip(CC_PathToExe)&'\'&clip(clock())&'outfile.txa'
    InFileName  = clip(CC_PathToExe)&'\'&clip(clock())&'infile.txa'

    if exists(outfilename) then remove(outfilename).
    Create(outfile)
    Open(OutFile)

    !From account - could be trade or subtrade
    Access:SubTracc.clearkey(sub:Account_Number_Key)
    sub:Account_Number = way:FromAccount
    if access:Subtracc.fetch(sub:Account_Number_Key) = level:benign
        !this is subtracc
        FromAccountName    =   clip(sub:Company_Name)
        FromAccountAddress =   clip(sub:Address_Line1)&'|'&clip(sub:Address_Line2)&'|'&clip(sub:Address_Line3)&'|'&clip(sub:Postcode)
        WaybillPrefix      =   sub:VCPWaybillPrefix
    ELSE
        !this is a trade account
        Access:tradeacc.clearkey(tra:Account_Number_Key)
        tra:Account_Number = way:FromAccount
        If access:Tradeacc.fetch(tra:Account_Number_Key)
            !error
        END
        !this is tradeacc
        FromAccountName     =   clip(tra:Company_Name)
        FromAccountAddress  =   clip(tra:Address_Line1)&'|'&clip(tra:Address_Line2)&'|'&clip(tra:Address_Line3)&'|'&clip(tra:Postcode)
        WaybillPrefix       =   tra:RRCWaybillPrefix
    END

    If clip(WaybillPrefix) = '' then
        !look up the default used on the printed waybill
        If GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
            WaybillPrefix = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI'))
        Else 
            WaybillPrefix = 'VDC'
        End 
    END


    !To account - could be trade or subtrade
    Access:SubTracc.clearkey(sub:Account_Number_Key)
    sub:Account_Number = way:ToAccount
    if access:Subtracc.fetch(sub:Account_Number_Key) = level:benign
        !this is subtracc
        ToAccountName     =   clip(sub:Company_Name)
        ToAccountAddress  =   clip(sub:Address_Line1)&'|'&clip(sub:Address_Line2)&'|'&clip(sub:Address_Line3)&'|'&clip(sub:Postcode)
        
    ELSE
        !this is a trade account
        Access:tradeacc.clearkey(tra:Account_Number_Key)
        tra:Account_Number = way:ToAccount
        If access:Tradeacc.fetch(tra:Account_Number_Key)
            !error
        END

        !this is subtracc
        ToAccountName     =   clip(tra:Company_Name)
        ToAccountAddress  =   clip(tra:Address_Line1)&'|'&clip(tra:Address_Line2)&'|'&clip(tra:Address_Line3)&'|'&clip(tra:Postcode)

    END


    !now to write the XML
    ouf:line1   = '<?xml version="1.0" encoding="UTF-8"?>'
    Add(Outfile)

    ouf:line1   = '<Doc>'
    Add(Outfile)

    ouf:line1   =   '<Desc>Waybill Generated XML</Desc>'
    Add(Outfile)

    ouf:line1   =   '<URL>'&clip(CC_URL)&'</URL>'
    Add(Outfile)

    ouf:line1   =   '<Username>'&clip(CC_UserName)&'</Username>'
    Add(Outfile)

    ouf:line1   =   '<Password>'&clip(CC_UserPassword)&'</Password>'
    Add(Outfile)

    ouf:line1   =   '<WaybillNo>'&clip(WAC:WaybillNo)&'</WaybillNo>'
    Add(Outfile)

    IF clip(way:WaybillID) = '300'
      ouf:line1   =   '<WaybillType>Sundry</WaybillType>'
    ELSE
      ouf:line1   =   '<WaybillType>Job</WaybillType>'
    END
    Add(Outfile)

    if clip(WaybillPrefix) <> '' then
        ouf:line1   =   '<WaybillPrefix>'&clip(WaybillPrefix)&'</WaybillPrefix>'
        Add(Outfile)
    END !if waybill prefix exists

    ouf:line1   =   '<SecurityPackNo>'&clip(SecurityPack)&'</SecurityPackNo>'
    Add(Outfile)

    !From account - could be trade or subtrade
    ouf:line1   =   '<Sender>'&clip(FromAccountName)&'</Sender>'
    Add(Outfile)

    ouf:line1   =   '<SenderAddress>'&clip(FromAccountAddress)&'</SenderAddress>'
    Add(Outfile)

    ouf:line1   =   '<DateTime>'&format(today(),@d06)&' '&format(clock(),@t1)&'</DateTime>'
    Add(Outfile)

    !To account - could be trade or subtrade
    ouf:line1   =   '<Receiver>'&clip(ToAccountName)&'</Receiver>'
    Add(Outfile)

    ouf:line1   =   '<ReceiverAddress>'&clip(ToAccountAddress)&'</ReceiverAddress>'
    Add(Outfile)


    IF way:WaybillID <> 300 !this is a jobs type waybill, not sundry

        ouf:line1   =   '<Jobs>'
        Add(Outfile)

        Access:Waybillj.clearkey(waj:JobNumberKey)  !has waybillnumber as first component
        waj:WayBillNumber = way:WayBillNumber
        Set(waj:JobNumberKey,waj:JobNumberKey)
        Loop
            If Access:WAYBILLJ.NEXT() then break.
            If waj:WayBillNumber <> way:WayBillNumber then break.

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = waj:JobNumber
            If Access:JOBS.fetch(job:Ref_Number_Key) then cycle.

            Case waj:JobType
                Of 'JOB'
                    MakeModel = clip(job:Manufacturer)&' / '&clip(job:Model_Number)

                Of 'EXC'
                    Access:Exchange.clearkey(xch:Ref_Number_Key)
                    xch:Ref_Number  = job:Exchange_Unit_Number
                    if Access:Exchange.fetch(xch:Ref_Number_Key) then cycle.

                    MakeModel = clip(xch:Manufacturer)&' / '&clip(xch:Model_Number)

            END !case jobtype

            ouf:line1   =       '<Job>'
            Add(Outfile)

            ouf:line1   =       '<JobId>'&clip(job:Ref_Number)&'</JobId>'
            Add(Outfile)

            ouf:line1   =       '<MakeModel>'&clip(MakeModel)&'</MakeModel>'
            Add(Outfile)

            ouf:line1   =       '</Job>'
            Add(Outfile)

            !whilst here update the audit trail
            Access:Audit.primerecord()
            aud:Ref_Number = job:Ref_Number
            aud:Date       = today()
            aud:Time       = clock()
            aud:User       = '&SB'
            aud:Action     = 'COURIER NOTIFICATION SENT'
            aud:Type       = waj:JobType
            Access:Audit.update()

            Access:Audit2.primerecord()
            aud2:AUDRecordNumber = aud:record_number
            if WAC:ConfirmResentQty > 1 then
                aud2:Notes = 'WAYBILL NOTIFICATION RE-SENT TO COURIER'
            ELSE
                aud2:Notes = 'WAYBILL NOTIFICATION SENT TO COURIER'
            END
            Access:Audit2.update()

            Access:Audite.primerecord()
            aude:RefNumber = aud:record_number
            aude:HostName  = 'WayCour.exe'        !was 'SB API'
            Access:Audite.update()


        END !loop through jobs

        ouf:line1   =   '</Jobs>'
        Add(Outfile)

    END !if not a sundrey type waybill

    ouf:line1   = '</Doc>'
    Add(Outfile)

    close(outfile)



