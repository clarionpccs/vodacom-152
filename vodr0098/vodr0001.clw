

   MEMBER('vodr0098.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


ClaimStatusReport PROCEDURE                           !Generated from procedure template - Window

tmp:VersionNumber    STRING(30)
tmp:Clipboard        ANY
StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
TempFilePath         CSTRING(255)
tmp:LocalPath        CSTRING(255)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:LastDetailColumn STRING(2)
tmp:LastSummaryColumn STRING(2)
tmp:UserName         STRING(70)
tmp:HeadAccountNumber STRING(30)
tmp:ARCLocation      STRING(30)
Progress:Thermometer BYTE(0)
tmp:ARCAccount       BYTE(0)
tmp:ARCCompanyName   STRING(30)
tmp:ARCUser          BYTE(0)
TempFileQueue        QUEUE,PRE(tmpque)
AccountNumber        STRING(30)
CompanyName          STRING(30)
Count                LONG
FileName             STRING(255)
JobsBooked           LONG
JobsOpen             LONG
                     END
ManufacturerQueue    QUEUE,PRE(manque)
Manufacturer         STRING(30)
JobNumber            LONG
Status               STRING(30)
                     END
TotalsGroup          GROUP,PRE(total)
Jobs                 LONG
RRCPartsCost         REAL
RRCPartsSale         REAL
RRCLabour            REAL
RRCSubTotal          REAL
RRCVat               REAL
RRCTotal             REAL
ARCPartsCost         REAL
ARCPartsSale         REAL
ARCLabour            REAL
ARCSubTotal          REAL
ARCVAT               REAL
ARCTotal             REAL
MFTRLabour           REAL
MFTRParts            REAL
MFTRSubTotal         REAL
MFTRVat              REAL
MFTRTotal            REAL
                     END
GrandTotalsGroup     GROUP,PRE(grand)
Jobs                 LONG
RRCPartsCost         REAL
RRCPartsSale         REAL
RRCLabour            REAL
RRCSubTotal          REAL
RRCVat               REAL
RRCTotal             REAL
ARCPartsCost         REAL
ARCPartsSale         REAL
ARCLabour            REAL
ARCSubTotal          REAL
ARCVat               REAL
ARCTotal             REAL
MFTRParts            REAL
MFTRLabour           REAL
MFTRSubTotal         REAL
MFTRVat              REAL
MFTRTotal            REAL
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('** Report Title **'),AT(,,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),CENTER,ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,164,192,44),USE(?PanelTip),FILL(0D6EAEFH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(396,172,36,32),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Help.jpg')
                       STRING(@s255),AT(252,184,140,),USE(SRN:TipText),TRN
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Claim Status Report'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       GROUP('Top Tip'),AT(248,168,184,36),USE(?GroupTip),BOXED,TRN
                         SHEET,AT(244,210,192,46),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                           TAB('Criteria'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Submitted From'),AT(248,216),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@d6),AT(324,216,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                             BUTTON,AT(392,212),USE(?PopCalendar),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Submitted To'),AT(248,234),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@d6),AT(324,234,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Jobs Completed To'),TIP('Jobs Completed To'),REQ,UPR
                             BUTTON,AT(392,230),USE(?PopCalendar:2),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT(''),AT(248,250),USE(?StatusText)
                             PROMPT('Ensure Excel is NOT running before you begin!'),AT(560,510,204,12),USE(?Prompt4),CENTER,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           END
                         END
                         BUTTON,AT(300,258),USE(?Print),TRN,FLAT,ICON('printp.jpg'),DEFAULT
                         BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                         PROMPT('Report Version'),AT(248,246),USE(?ReportVersion),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       END
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
local       Class
DrawBox                 Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
UpdateProgressWindow    Procedure(String  func:Text)
WriteLine               Procedure()
            End ! local       Class
!Export Files (DBH: 22-03-2004)
ExportFile    File,Driver('ASCII'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
Line1                   String(2000)
                        End
                    End
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

Reporting       Routine  !Do the Report (DBH: 22-03-2004)
Data
local:CurrentAccount    String(30)
local:ReportStartDate   Date()
local:ReportStartTime   Time()

local:RecordCount       Long()

local:LineCount   Long()
local:RecordCount   Long()
local:Manufacturer  String(30)
local:Status        String(30)
local:Desktop       CString(255)
Code
    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            tmp:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            tmp:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    !Set the folder for the excel file (DBH: 10-03-2004)
    excel:ProgramName = 'Claim Status Report'

    ! Create Folder In My Documents
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))


    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK') 
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

    If COMMAND('/DEBUG')
        Remove('c:\debug.ini')
    End !If COMMAND('/DEBUG')

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('*** DEBUG MODE ***')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('')
    End !If Command('/DEBUG')

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Building Records List...')

    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________

    local.UpdateProgressWindow('')

    Free(ManufacturerQueue)
    Clear(ManufacturerQueue)

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = ''
    Set(tra:Account_Number_Key,tra:Account_Number_Key)
    Loop ! Begin Loop
        If Access:TRADEACC.Next()
            Break
        End ! If Access:TRADEACC.Next()

        If Clip(tra:BranchIdentification) = ''
            Cycle
        End ! If tra:BranchIdentification = ''

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel = 1

        local:RecordCount = 0

        Access:JOBSWARR.Clearkey(jow:SubmittedBranchKey)
        jow:BranchID = tra:BranchIdentification
        jow:ClaimSubmitted = tmp:StartDate
        Set(jow:SubmittedBranchKey,jow:SubmittedBranchKey)
        Loop ! Begin Loop
            If Access:JOBSWARR.Next()
                Break
            End ! If Access:JOBSWARR.Next()
            If jow:BranchID <> tra:BranchIdentification
                Break
            End ! If jow:BranchID <> tra:BranchIdentification
            If jow:ClaimSubmitted > tmp:EndDate
                Break
            End ! If jow:ClaimSubmitted > tmp:EndDate
            Do GetNextRecord2
            Do CancelCheck
            If tmp:Cancel
                Break
            End !If tmp:Cancel = 1

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_number = jow:RefNumber
            If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Found
            Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Error
                Cycle
            End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign


            If job:Warranty_Job <> 'YES'
                Cycle
            End ! If job:Warranty_Job <> 'YES'

            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Error
                Cycle
            End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

            ! --- Chec for duplicate job numbers (although don't know why they should happen) ----
            ! Inserting:  DBH 12/01/2009 #10627
            manque:JobNumber = job:Ref_Number
            Get(ManufacturerQueue,manque:JobNumber)
            If ~Error()
                Cycle
            End ! If ~Error()

            ! End: DBH 12/01/2009 #10627
            ! -----------------------------------------

            manque:Manufacturer = job:Manufacturer
            manque:JobNumber = job:Ref_Number
            manque:Status = jobe:WarrantyClaimStatus
            Add(ManufacturerQueue)


        End ! Loop


        If tmp:Cancel
            Break
        End ! If tmp:Cancel

    End !Loop TRADEACC

    ! ____________________________________________________________________________________
    ! Create Export Files

    If Records(ManufacturerQueue)

        Clear(GrandTotalsGroup)
        Clear(TotalsGroup)

        local.UpdateProgressWindow('Records Found: ' & Records(ManufacturerQueue))
        local.UpdateProgressWindow('')
        local.UpdateProgressWindow('Creating Temporary Files...')

        local:Status = ''
        local:Manufacturer = ''
        Sort(ManufacturerQueue,manque:Status,manque:Manufacturer,manque:JobNumber)
        Loop x# = 1 To Records(ManufacturerQueue)
            Get(ManufacturerQueue,x#)
            
            Do GetNextRecord2

            If manque:Status <> local:Status
                If local:Status <> ''
                    Clear(exp:Record)
                    exp:Line1   = 'Totals,,,,,,,,,,,,' & total:Jobs & ',,,,,,,' & |
                                Format(total:RRCPartsCost,@n_10.2) & ',' &  |
                                Format(total:RRCPartsSale,@n_10.2) & ',' &  |
                                Format(total:RRCLabour,@n_10.2) & ',' &  |
                                Format(total:RRCSubTotal,@n_10.2) & ',' &  |
                                Format(total:RRCVat,@n_10.2) & ',' &  |
                                Format(total:RRCTotal,@n_10.2) & ',' & |
                                Format(total:ARCPartsCost,@n_10.2) & ',' &  |
                                Format(total:ARCPartsSale,@n_10.2) & ',' &  |
                                Format(total:ARCLabour,@n_10.2) & ',' &  |
                                Format(total:ARCSubTotal,@n_10.2) & ',' &  |
                                Format(total:ARCVat,@n_10.2) & ',' &  |
                                Format(total:ARCTotal,@n_10.2) & ',,' & |
                                Format(total:MFTRParts,@n_10.2) & ',' &  |
                                Format(total:MFTRLabour,@n_10.2) & ',' &  |
                                Format(total:MFTRSubTotal,@n_10.2) & ',' &  |
                                Format(total:MFTRVat,@n_10.2) & ',' &  |
                                Format(total:MFTRTotal,@n_10.2)
                    Add(ExportFile)
                    grand:Jobs += total:Jobs
                    grand:RRCPartsCost += total:RRCPartsCost
                    grand:RRCPartsSale += total:RRCPartsSale
                    grand:RRCLabour += total:RRCLabour
                    grand:RRCSubTotal += total:RRCSubTotal
                    grand:RRCVat += total:RRCVat
                    grand:RRCTotal += total:RRCTotal
                    grand:ARCPartsCost += total:ARCPartsCost
                    grand:ARCPartsSale += total:ARCPartsSale
                    grand:ARCLabour += total:ARCLabour
                    grand:ARCSubTotal += total:ARCSubTotal
                    grand:ARCVat += total:ARCVat
                    grand:ARCTotal += total:ARCTotal
                    grand:MFTRParts += total:MFTRParts
                    grand:MFTRLabour += total:MFTRLabour
                    grand:MFTRSubTotal += total:MFTRSubTotal
                    grand:MFTRVat += total:MFTRVat
                    grand:MFTRTotal += total:MFTRTotal

                    Clear(exp:Record)
                    Add(ExportFile)
                    Clear(exp:Record)
                    exp:Line1   = 'Grand Totals,,,,,,,,,,,,' & Grand:Jobs & ',,,,,,,' & |
                                Format(grand:RRCPartsCost,@n_10.2) & ',' &  |
                                Format(Grand:RRCPartsSale,@n_10.2) & ',' &  |
                                Format(grand:RRCLabour,@n_10.2) & ',' &  |
                                Format(Grand:RRCSubTotal,@n_10.2) & ',' &  |
                                Format(Grand:RRCVat,@n_10.2) & ',' &  |
                                Format(Grand:RRCTotal,@n_10.2) & ',' & |
                                Format(Grand:ARCPartsCost,@n_10.2) & ',' &  |
                                Format(Grand:ARCPartsSale,@n_10.2) & ',' &  |
                                Format(Grand:ARCLabour,@n_10.2) & ',' &  |
                                Format(Grand:ARCSubTotal,@n_10.2) & ',' &  |
                                Format(Grand:ARCVat,@n_10.2) & ',' &  |
                                Format(Grand:ARCTotal,@n_10.2) & ',,' & |
                                Format(Grand:MFTRParts,@n_10.2) & ',' &  |
                                Format(Grand:MFTRLabour,@n_10.2) & ',' &  |
                                Format(Grand:MFTRSubTotal,@n_10.2) & ',' &  |
                                Format(Grand:MFTRVat,@n_10.2) & ',' &  |
                                Format(Grand:MFTRTotal,@n_10.2)
                    Add(ExportFile)

                    Clear(GrandTotalsGroup)
                    Clear(TotalsGroup)
                    local:Manufacturer = ''

                    local:LineCount += 4

                    local.UpdateProgressWindow(Clip(local:Status) & ', Jobs Found: ' & local:RecordCount)
                    local.UpdateProgressWindow('')
                    Close(ExportFile)

                    tmpque:AccountNumber = local:Status
                    tmpque:Count = local:LineCount
                    tmpque:FileName = glo:ExportFile
                    Add(TempFileQueue)


                End ! If local:Status <> ''


                glo:ExportFile = Clip(tmp:LocalPath) & Clip(manque:Status) & Clock() & '.CSV'
                Remove(glo:ExportFile)
                Create(ExportFile)
                Open(ExportFIle)

                Clear(TotalsGroup)
                Clear(exp:Record)
                exp:Line1 = 'SB Job No,Franchise Branch ID,Franchise Job No,Completed,Claim Submission Date,Administrator,Date Processed,Time Processed,Head Account No,Head Account Name,Account No,Account Name,Manufacturer,Model Number,Warranty Charge Type,Warranty Repair Type,Engineer,Repair,Exchange,RRC Parts Cost,RRC Part Sale,RRC Labour,RRC Sub Total,RRC VAT,RRC Total,ARC Parts Cost,ARC Part Sale,ARC Labour,ARC Sub Total,ARC VAT,ARC Total,Accepted Date,MFTR Parts,MFTR Labour,MFTR Sub Total,MFTR VAT,MFTR Total,Reconciled Date,Warranty Status,Rejection Date 1,Rejection Reason 1,Rejection Date 2,Rejection Reason 2,Final Rejection Date,Final Rejection Reason,Rejection Accepted By'
                Add(ExportFile)
                local:RecordCount = 0
                local:LineCount = 0

                local:Status = manque:Status
            End ! If manque:Status <> local:Status
            If local:Manufacturer <> manque:Manufacturer
                If local:Manufacturer <> ''
                    ! Do Totals
                    Clear(exp:Record)
                    exp:Line1   = 'Totals,,,,,,,,,,,,' & total:Jobs & ',,,,,,,' & |
                                Format(total:RRCPartsCost,@n_10.2) & ',' &  |
                                Format(total:RRCPartsSale,@n_10.2) & ',' &  |
                                Format(total:RRCLabour,@n_10.2) & ',' &  |
                                Format(total:RRCSubTotal,@n_10.2) & ',' &  |
                                Format(total:RRCVat,@n_10.2) & ',' &  |
                                Format(total:RRCTotal,@n_10.2) & ',' & |
                                Format(total:ARCPartsCost,@n_10.2) & ',' &  |
                                Format(total:ARCPartsSale,@n_10.2) & ',' &  |
                                Format(total:ARCLabour,@n_10.2) & ',' &  |
                                Format(total:ARCSubTotal,@n_10.2) & ',' &  |
                                Format(total:ARCVat,@n_10.2) & ',' &  |
                                Format(total:ARCTotal,@n_10.2) & ',,' & |
                                Format(total:MFTRParts,@n_10.2) & ',' &  |
                                Format(total:MFTRLabour,@n_10.2) & ',' &  |
                                Format(total:MFTRSubTotal,@n_10.2) & ',' &  |
                                Format(total:MFTRVat,@n_10.2) & ',' &  |
                                Format(total:MFTRTotal,@n_10.2)
                    Add(ExportFile)
                    Clear(exp:Record)
                    Add(ExportFile)
                    local:Manufacturer = manque:Manufacturer

                    grand:Jobs += total:Jobs
                    grand:RRCPartsCost += total:RRCPartsCost
                    grand:RRCPartsSale += total:RRCPartsSale
                    grand:RRCLabour += total:RRCLabour
                    grand:RRCSubTotal += total:RRCSubTotal
                    grand:RRCVat += total:RRCVat
                    grand:RRCTotal += total:RRCTotal
                    grand:ARCPartsCost += total:ARCPartsCost
                    grand:ARCPartsSale += total:ARCPartsSale
                    grand:ARCLabour += total:ARCLabour
                    grand:ARCSubTotal += total:ARCSubTotal
                    grand:ARCVat += total:ARCVat
                    grand:ARCTotal += total:ARCTotal
                    grand:MFTRParts += total:MFTRParts
                    grand:MFTRLabour += total:MFTRLabour
                    grand:MFTRSubTotal += total:MFTRSubTotal
                    grand:MFTRVat += total:MFTRVat
                    grand:MFTRTotal += total:MFTRTotal

                    Clear(TotalsGroup)

                    local:LineCount += 2
                End ! If local:Manufacturer <> ''
                local:Manufacturer = manque:Manufacturer
            End ! If local:Manufacturer <> manque:Manufacturer

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = manque:JobNumber
            If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Found
            Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Error
                Cycle
            End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            Do WriteLine
            local:LineCount += 1
            local:RecordCount += 1
        End ! Loop x# = 1 To Records(ManufacturerQueue)

        Clear(exp:Record)
        exp:Line1   = 'Totals,,,,,,,,,,,,' & total:Jobs & ',,,,,,,' & |
                    Format(total:RRCPartsCost,@n_10.2) & ',' &  |
                    Format(total:RRCPartsSale,@n_10.2) & ',' &  |
                    Format(total:RRCLabour,@n_10.2) & ',' &  |
                    Format(total:RRCSubTotal,@n_10.2) & ',' &  |
                    Format(total:RRCVat,@n_10.2) & ',' &  |
                    Format(total:RRCTotal,@n_10.2) & ',' & |
                    Format(total:ARCPartsCost,@n_10.2) & ',' &  |
                    Format(total:ARCPartsSale,@n_10.2) & ',' &  |
                    Format(total:ARCLabour,@n_10.2) & ',' &  |
                    Format(total:ARCSubTotal,@n_10.2) & ',' &  |
                    Format(total:ARCVat,@n_10.2) & ',' &  |
                    Format(total:ARCTotal,@n_10.2) & ',,' & |
                    Format(total:MFTRParts,@n_10.2) & ',' &  |
                    Format(total:MFTRLabour,@n_10.2) & ',' &  |
                    Format(total:MFTRSubTotal,@n_10.2) & ',' &  |
                    Format(total:MFTRVat,@n_10.2) & ',' &  |
                    Format(total:MFTRTotal,@n_10.2)
        Add(ExportFile)
        grand:Jobs += total:Jobs
        grand:RRCPartsCost += total:RRCPartsCost
        grand:RRCPartsSale += total:RRCPartsSale
        grand:RRCLabour += total:RRCLabour
        grand:RRCSubTotal += total:RRCSubTotal
        grand:RRCVat += total:RRCVat
        grand:RRCTotal += total:RRCTotal
        grand:ARCPartsCost += total:ARCPartsCost
        grand:ARCPartsSale += total:ARCPartsSale
        grand:ARCLabour += total:ARCLabour
        grand:ARCSubTotal += total:ARCSubTotal
        grand:ARCVat += total:ARCVat
        grand:ARCTotal += total:ARCTotal
        grand:MFTRParts += total:MFTRParts
        grand:MFTRLabour += total:MFTRLabour
        grand:MFTRSubTotal += total:MFTRSubTotal
        grand:MFTRVat += total:MFTRVat
        grand:MFTRTotal += total:MFTRTotal

        Clear(exp:Record)
        Add(ExportFile)
        Clear(exp:Record)
        exp:Line1   = 'Grand Totals,,,,,,,,,,,,' & Grand:Jobs & ',,,,,,,' & |
                    Format(grand:RRCPartsCost,@n_10.2) & ',' &  |
                    Format(Grand:RRCPartsSale,@n_10.2) & ',' &  |
                    Format(grand:RRCLabour,@n_10.2) & ',' &  |
                    Format(Grand:RRCSubTotal,@n_10.2) & ',' &  |
                    Format(Grand:RRCVat,@n_10.2) & ',' &  |
                    Format(Grand:RRCTotal,@n_10.2) & ',' & |
                    Format(Grand:ARCPartsCost,@n_10.2) & ',' &  |
                    Format(Grand:ARCPartsSale,@n_10.2) & ',' &  |
                    Format(Grand:ARCLabour,@n_10.2) & ',' &  |
                    Format(Grand:ARCSubTotal,@n_10.2) & ',' &  |
                    Format(Grand:ARCVat,@n_10.2) & ',' &  |
                    Format(Grand:ARCTotal,@n_10.2) & ',,' & |
                    Format(Grand:MFTRParts,@n_10.2) & ',' &  |
                    Format(Grand:MFTRLabour,@n_10.2) & ',' &  |
                    Format(Grand:MFTRSubTotal,@n_10.2) & ',' &  |
                    Format(Grand:MFTRVat,@n_10.2) & ',' &  |
                    Format(Grand:MFTRTotal,@n_10.2)
        Add(ExportFile)
        local:LineCount += 4

        local.UpdateProgressWindow(Clip(local:Status) & ', Jobs Found: ' & local:RecordCount)
        local.UpdateProgressWindow('')
        Close(ExportFile)

        tmpque:AccountNumber = local:Status
        tmpque:Count = local:LineCount
        tmpque:FileName = glo:ExportFile
        Add(TempFileQueue)

        local.UpdateProgressWindow('')

    End ! If Records(ManufacturerQueue)


    !_____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Disable} = 1

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Excel Document..')
    !_____________________________________________________________________

    !CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
    !_____________________________________________________________________

        local.UpdateProgressWindow('Creating Excel Sheets...')
        local.UpdateProgressWindow('')

        If E1.Init(0,0) = 0
            Case Missive('An error has occurred finding your Excel document.'&|
              '<13,10>'&|
              '<13,10>Please quit and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If E1.Init(0,0,1) = 0

        tmp:LastDetailColumn = 'AT'

        local.UpdateProgressWindow('Creating Excel Sheets...')
        local.UpdateProgressWindow('')

        E1.NewWorkBook()
        E1.SaveAs(excel:FileName)
        E1.CloseWorkBook(3)

        Sort(TempFileQueue,-tmpque:AccountNumber)

        Loop x# = 1 To Records(TempFileQueue)
            Get(TempFileQueue,x#)
            Do GetNextRecord2

            SetClipBoard('')
            E1.OpenWorkBook(tmpque:FileName)
            E1.Copy('A1',Clip(tmp:LastDetailColumn) & tmpque:Count)
            tmp:Clipboard = ClipBoard()
            Loop Until Clipboard() = ''
                SetClipboard('')
            End ! Loop Until Clipboard() = ''
            E1.CloseWorkBook(3)
            Remove(tmpque:FileName)
            E1.OpenWorkBook(excel:FileName)
            E1.InsertWorkSheet()
            E1.RenameWorkSheet(tmpque:AccountNumber)
            E1.SelectCells('A1')
            SetClipBoard(tmp:ClipBoard)
            E1.Paste()
            Loop Until Clipboard() = ''
                SetClipboard('')
            End ! Loop Until Clipboard() = ''

            

            Loop row# = 2 To tmpque:Count
                Do GetNextRecord2
                If E1.ReadCell('A' & row#) = 'Totals'
                    E1.SetPaletteColor(33,13434879)
                    E1.InterpretClarionColorEquates = 0
                    E1.SetCellBackgroundColor(33,'A' & row#,tmp:LastDetailColumn & row#)
                    E1.SetCellFontStyle('Bold','A' & row#,tmp:LastDetailColumn & row#)
                End ! If E1.ReadCell('A' & tmpque:Count) = 'Totals'
            End ! Loop row# = 2 To tmpque:Count

            E1.SetCellFontName('Tahoma','A1',tmp:LastDetailColumn & tmpque:Count)
            E1.SetCellFontSize('8','A1',tmp:LastDetailColumn & tmpque:Count)
            E1.SetCellFontStyle('Bold','A1',tmp:LastDetailColumn & '1')
            E1.SetPaletteColor(34,13421772)
            E1.InterpretClarionColorEquates = 0
            E1.SetCellBackgroundColor(34,'A1',tmp:LastDetailColumn & '1')
            E1.SetCellFontStyle('Bold','A' & tmpque:Count,tmp:LastDetailColumn & tmpque:Count)
            E1.SetPaletteColor(35,10079487)
            E1.InterpretClarionColorEquates = 0
            E1.SetCellBackgroundColor(35,'A' & tmpque:Count,tmp:LastDetailColumn & tmpque:Count)
            E1.AutoFilter('A1',tmp:LastDetailColumn & '1')
            E1.SetColumnWidth('A',tmp:LastDetailColumn)
            E1.SelectCells('B2')
            E1.FreezePanes(1)
            E1.SelectCells('A2')
            E1.Save()
            E1.CloseWorkBook(3)
            local.UpdateProgressWindow(Clip(tmpque:AccountNumber) & ': Sheet Created.')
            local.UpdateProgressWindow('')
        End ! Loop x# = 1 To Records(TempFileQueue)


        E1.SaveAs(excel:FileName)
        E1.CloseWorkBook(3)
        E1.Kill()

        local.UpdatePRogressWindow('Finishing Off Formatting..')


        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)

    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    ?Button:OpenReportFolder{Prop:Hide} = 0
    Accept
        Case Field()
            Of ?Finish
                Case Event()
                    Of Event:Accepted
                        Break

                End !Case Event()
            Of ?Button:OpenReportFolder
                Case Event()
                Of Event:Accepted
                    RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                End ! Case Event()
        End !Case Field()
    End !Accept
    Close(ProgressWindow)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)
DrawTitle     Routine     !Set Detail Title (DBH: 22-03-2004)
    E1.WriteToCell(excel:ProgramName, 'A1')
    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell(tmp:VersionNumber,'D3')
    E1.WriteToCell('Start Date','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d06),'B4')
    E1.WriteToCell('End Date','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d06),'B5')
    If Clip(tmp:UserName) <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(tmp:UserName,'B6')
    End ! If Clip(tmp:UserName) <> ''
    E1.WriteToCell('Created Date','A7')
    E1.WriteToCell(Format(Today(),@d06),'B7')

    E1.SetCellFontStyle('Bold','A1','D3')
    E1.SetCellFontStyle('Bold','A4','A7')

    local.DrawBox('A1','D1','A1','D1',color:Silver)
    local.DrawBox('A3','D3','A7','D7',color:Silver)

    E1.SetCellBackgroundColor(color:Silver,'A1','D1')
    E1.SetCellBackgroundColor(color:Silver,'A3','D7')

    E1.SetCellFontName('Tahoma','A1','D7')



WriteLine         Routine
Data
Costs               Group(),Pre()
local:HandlingFee       Real
local:PartsSelling      Real
local:Labour            Real
local:VAT               Real
local:Total             Real
local:Parts             Real
                    End
local:RejectionBy      String(65)
local:TradeAccount      String(30)
local:UserName          String(65)
local:UserDate          Date()
local:UserTime          Time()
local:AcceptedDate      Date()
local:ReconciledDate        Date()
Code

    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign


    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

    Access:JOBSWARR.ClearKey(jow:RefNumberKey)
    jow:RefNumber = job:Ref_Number
    If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign

    Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
    jobe2:RefNumber = job:Ref_Number
    If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign

    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign




    Clear(exp:Record)
    ! Job Number
    exp:Line1 = '"' & job:Ref_Number
    ! Branch ID
    exp:Line1 = Clip(exp:Line1) & '","' & tra:BranchIdentification
    ! Webjob Number
    exp:Line1 = Clip(exp:Line1) & '","' & wob:JobNumber
    ! Completed Date
    exp:Line1 = Clip(exp:Line1) & '","' & Format(job:Date_Completed,@d06)
    ! Claim Submitted
    exp:Line1 = Clip(exp:Line1) & '","' & Format(jow:ClaimSubmitted,@d06)
    ! Administrator
    ! Date Processed
    ! Time Processed
    If jow:Status = 'NO' and jobe:WarrantyClaimStatus = 'SUBMITTED'
        ! First time the job has appear into the warranty browse.
        !Can only use the name of the person at the ARC that completed the job (DBH: 07/07/2008)
        Found# = 0
        Access:AUDSTATS.Clearkey(aus:RefRecordNumberKey)
        aus:RefNumber = job:Ref_Number
        Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
        Loop ! Begin Loop
            If Access:AUDSTATS.Next()
                Break
            End ! If Access:AUDSTATS.Next()
            If aus:RefNumber <> job:Ref_Number
                Break
            End ! If aus:RefNumber <> job:Ref_Number
            If Sub(aus:NewStatus,1,3) = '705' Or Sub(aus:NewStatus,1,3) = '706'
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = aus:UserCode
                If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location = 'AA20 ADVANCED REPAIR CENTRE'
                        local:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
                        local:UserDate = aud:Date
                        local:UserTime = aud:Time
                        Break
                    End ! If use:Location = 'AA20 ADVANCED REPAIR CENTRE'
                Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Error
                End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign

            End ! If Sub(aus:NewStatus,1,3) = '705' Or Sub(aus:NewStatus,1,3) = '706'
        End ! Loop
        If local:UserName = ''
            exp:Line1   = Clip(exp:Line1) & '","' & local:UserName
            exp:Line1   = Clip(exp:Line1) & '","' & FOrmat(local:UserDate,@d06)
            exp:Line1   = Clip(exp:Line1) & '","' & Format(local:UserTime,@t01)
        Else ! If local:UserName = ''
            exp:Line1   = Clip(exp:Line1) & '","' 
            exp:Line1   = Clip(exp:Line1) & '","' & FOrmat(job:Date_Completed,@d06)
            exp:Line1   = Clip(exp:Line1) & '","' & Format(job:Time_Completed,@t01)
        End ! If local:UserName = ''
            
        
    Else ! If jow:Status = 'NO' and jobe:WarrantyClaimStatus = 'SUBMITTED'
        Access:AUDIT.Clearkey(aud:Ref_Number_Key)
        aud:Ref_Number = job:Ref_Number
        aud:Date = TOday()
        Set(aud:Ref_Number_Key,aud:Ref_Number_Key)
        Loop ! Begin Loop
            If Access:AUDIT.Next()
                Break
            End ! If Access:AUDIT.Next()
            If aud:Ref_Number <> job:Ref_Number
                Break
            End ! If aud:Ref_Number <> job:Ref_Number
            Case jow:Status
            Of 'NO'
                Case jobe:WarrantyClaimStatus
                Of 'ON TECHNICAL REPORT'
                    If aud:Action <> 'WARRANTY CLAIM ON TECHNICAL REPORT'
                        Cycle
                    End ! If aud:Action <> 'WARRANTY CLAIM ON TECHNICAL REPORT'
                Of 'SUBMITTED TO MFTR'
                    If aud:Action <> 'WARRANTY CLAIM SUBMITTED TO MFTR'
                        Cycle
                    End ! If aud:Action <> 'WARRANTY CLAIM SUBMITTED TO MFTR'
                Of 'RESUBMITTED'
                    If aud:Action <> 'WARRANTY CLAIM RESUBMITTED'
                        Cycle
                    End ! If aud:Action <> 'WARRANTY CLAIM RESUBMITTED'
                End ! Case jobe:WarrantyClaimStatus
            Of 'YES'
                If aud:Action <> 'WARRANTY CLAIM APPROVED'
                    Cycle
                End ! If aud:Action <> 'WARRANTY CLAIM APPROVED'
            Of 'EXC'
                If aud:Action <> 'WARRANTY CLAIM REJECTED'
                    Cycle
                End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
                If aud:Action <> 'CLAIM VETTED: REJECTED'
                    Cycle
                End ! If aud:Action <> 'CLAIM VETTED: REJECTED'
            Of 'QUE'
                If aud:Action <> 'WARRANTY CLAIM QUERY'
                    Cycle
                End ! If aud:Action <> 'WARRANTY CLAIM QUERY'
                If aud:Action <> 'CLAIM VATTED: QUERY'
                    Cycle
                End ! If aud:Action <> 'CLAIM VATTED: QUERY'
            Of 'FIN'
                If aud:Action <> 'WARRANTY CLAIM RECONCILED'
                    Cycle
                End ! If aud:Action <> 'WARRANTY CLAIM RECONCILED'
            Of 'REJ'
                If aud:Action <> 'WARRANTY CLAIM FINAL REJECTION'
                    Cycle
                End ! If aud:Action <> 'WARRANTY CLAIM FINAL REJECTION'
            End ! Case jow:Status
            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = aud:User
            If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                !Found
                local:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
                local:UserDate = aud:Date
                local:UserTime = aud:Time
                Break
            Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                !Error
            End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign

        End ! Loop
        If local:UserName <> ''
            exp:Line1  = Clip(exp:Line1) & '","' & local:UserName
            exp:Line1   = Clip(exp:Line1) & '","' & Format(local:UserDate,@d06)
            exp:Line1   = Clip(exp:Line1) & '","' & Format(local:UserTime,@t01)
        Else ! If local:UserName <> ''
            exp:Line1   = Clip(exp:Line1) & '","'
            exp:Line1   = Clip(exp:Line1) & '","'
            exp:Line1   = Clip(exp:Line1) & '","'
        End ! If local:UserName <> ''
    End ! If jow:Status = 'NO' and jobe:WarrantyClaimStatus = 'SUBMITTED'

    ! Account Number
    exp:Line1 = Clip(exp:Line1) & '","' & tra:Account_Number
    ! Account Name
    exp:Line1 = Clip(exp:Line1) & '","' & tra:Company_Name
    ! Job Account Number
    exp:Line1 = Clip(exp:Line1) & '","' & job:Account_Number
    ! Job Company Name
    exp:Line1 = Clip(exp:Line1) & '","' & BHStripReplace(job:Company_Name,'","',' ')
    ! Manufacturer
    exp:Line1 = Clip(exp:Line1) & '","' & job:Manufacturer
    ! Model Number
    exp:Line1 = Clip(exp:Line1) & '","' & job:Model_Number
    ! Charge Type
    exp:Line1 = Clip(exp:Line1) & '","' & job:Warranty_Charge_Type
    ! Repair Type
    exp:Line1 = Clip(exp:Line1) & '","' & job:Repair_Type_Warranty
    ! Engineer
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = job:Engineer
    If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Found
        exp:Line1 = Clip(exp:Line1) & '","' & BHStripReplace(Clip(use:Forename) & ' ' & Clip(use:Surname),'","',' ')
    Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Error
        exp:Line1 = Clip(exp:Line1) & '","'
    End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
    ! Repair
    exp:Line1 = Clip(exp:Line1) & '","' & jow:RepairedAt
    ! Exchanged
    If job:Exchange_Unit_Number = 0
        exp:Line1 = Clip(exp:Line1) & '","'
    Else ! If job:Exchange_Unit_Number = 0
        If jobe:ExchangedAtRRC
            exp:Line1   = Clip(exp:Line1) & '","RRC'
        Else ! If jobe:ExchangedAtRRC
            exp:Line1   = Clip(exp:Line1) & '","ARC'
        End ! If jobe:ExchangedAtRRC
    End ! If job:Exchange_Unit_Number = 0

    Clear(Costs)
    If jow:RepairedAT = 'RRC'
        If job:Invoice_Number_Warranty <> 0
            Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
            inv:Invoice_Number  = job:Invoice_Number_Warranty
            If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Found
                local:PartsSelling    = jobe:InvRRCWPartsCost
                local:Labour   = jobe:InvRRCWLabourCost
                local:VAT      = jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts/100 + |
                                    jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour/100 +|
                                    job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100
                local:Total    = local:PartsSelling + local:Labour + local:VAT + job:WInvoice_Courier_Cost
            Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Error
                Exit
            End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign

        Else !If job:Invoice_Number_Warranty <> 0
            local:PartsSelling    = jobe:RRCWPartsCost
            local:Labour   = jobe:RRCWLabourCost
            local:VAT      = jobe:RRCWPartsCost * VatRate(job:Account_Number,'L')/100 + |
                                jobe:RRCWLabourCost * VatRate(job:Account_Number,'P')/100 + |
                                job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100
            local:Total    = local:PartsSelling + local:Labour + local:VAT + job:Courier_Cost_Warranty
        End !If job:Invoice_Number_Warranty <> 0
    Else ! If jow:RepairedAT = 'RRC'
        If job:Invoice_Number_Warranty > 0
            Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = job:Invoice_Number_Warranty
            If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                !Found
                local:PartsSelling = job:WInvoice_Parts_Cost
                local:Labour = jobe:InvoiceClaimValue
                local:VAT = job:WInvoice_Parts_Cost * inv:Vat_Rate_Labour/100 + |
                                        jobe:InvoiceClaimValue * inv:Vat_Rate_Parts/100 + |
                                        job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100
                local:Total    = local:PartsSelling + local:Labour + local:VAT + job:WInvoice_Courier_Cost
            Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                !Error
            End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign

        Else ! If job:Invoice_Number_Warranty > 0
            local:PartsSelling    = job:Parts_Cost_Warranty
            local:Labour   = jobe:ClaimValue
            local:VAT      = job:Parts_Cost_Warranty * VatRate(job:Account_Number,'L')/100 + |
                                jobe:ClaimValue * VatRate(job:Account_Number,'P')/100 + |
                                job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100
            local:Total    = local:PartsSelling + local:Labour + local:VAT + job:Courier_Cost_Warranty
        End ! If job:Invoice_Number_Warranty > 0

    End ! If jow:RepairedAT = 'RRC'

    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop ! Begin Loop
        If Access:WARPARTS.Next()
            Break
        End ! If Access:WARPARTS.Next()
        If wpr:Ref_Number <> job:Ref_Number
            Break
        End ! If wpr:Ref_Number <> job:Ref_Number
        If wpr:Correction
            Cycle
        End ! If wpr:Correction
        If jow:RepairedAT = 'RRC'
            local:Parts += (wpr:RRCAveragePurchaseCost * wpr:Quantity)
        Else ! If jow:RepairedAT = 'RRC'
            local:Parts += (wpr:AveragePurchaseCost * wpr:Quantity)
        End ! If jow:RepairedAT = 'RRC'
    End ! Loop
    If jow:RepairedAt = 'ARC'
        exp:Line1   = Clip(exp:Line1) & '",,,,,,"'
    End !If jow:RepairedAt = 'ARC'

    ! Parts Cost
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:Parts,@n_10.2)
    ! Parts Selling Cost
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:PartsSelling,@n_10.2)
    ! Labour Cost
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:Labour,@n_10.2)
    ! Sub Total
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:Labour + local:PartsSelling,@n_10.2)
    ! VAT
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:VAT,@n_10.2)
    ! Total
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:Total,@n_10.2)
    If jow:RepairedAT = 'RRC'
        exp:Line1   = Clip(exp:Line1) & '",,,,,,"'
    End ! If jow:RepairedAT = 'RRC'

    If jow:RepairedAT = 'RRC'
        total:RRCPartsCost += local:Parts
        total:RRCPartsSale += local:PartsSelling
        total:RRCLabour += local:Labour
        total:RRCSubTotal += local:Labour + local:PartsSelling
        total:RRCVAT += local:VAT
        total:RRCTotal += local:Total

    Else ! If jow:RepairedAT = 'RRC'
        total:ARCPartsCost += local:Parts
        total:ARCPartsSale += local:PartsSelling
        total:ARCLabour += local:Labour
        total:ARCSubTotal += local:Labour + local:PartsSelling
        total:ARCVAT += local:VAT
        total:ARCTotal += local:Total
    End ! If jow:RepairedAT = 'RRC'

    ! Accepted Date
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jow:DateAccepted,@d06)

    ! MFTRParts
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe2:WPartsPaid,@N_10.2)
    ! MFRTLabour
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe2:WLabourPaid,@N_10.2)
    ! MFTRSubTotal
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe2:WSubTotal,@N_10.2)
    ! MFTRVAT
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe2:WVat,@N_10.2)
    ! MFTRTotal
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe2:WTotal,@N_10.2)

    total:MFTRParts += jobe2:WPartsPaid
    total:MFTRLabour += jobe2:WLabourPaid
    total:MFTRSubTotal += jobe2:WSubTotal
    total:MFTRVAT += jobe2:WVat
    total:MFTRTotal += jobe2:WTotal

    ! Reconciled Date
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = job:Ref_Number
    aud:Type = 'JOB'
    aud:Action = 'WARRANTY CLAIM RECONCILED'
    Aud:Date = Today()
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> job:Ref_Number
            Break
        End ! If aud:Ref_Number <> job:Ref_Number
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type <> 'JOB'
        If aud:ACtion <> 'WARRANTY CLAIM RECONCILED'
            Break
        End ! If aud:ACtion <> 'WARRANTY CLAIM ACCEPTED'
        local:ReconciledDate = aud:Date
        Break
    End ! Loop

    If local:ReconciledDate > 0
        exp:Line1   = Clip(exp:Line1) & '","' & FOrmat(local:ReconciledDate,@d06)
    Else ! If local:AcceptedDate > 0
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If local:AcceptedDate > 0

    ! Warranty Status
    local:RejectionBy = ''
    exp:Line1   = Clip(exp:Line1) & '","' & jobe:WarrantyClaimStatus

    ! Rejection Reasons
    CountReasons# = 0
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = job:Ref_Number
    aud:Type = 'JOB'
    aud:Action = 'WARRANTY CLAIM REJECTED'
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> job:Ref_Number
            Break
        End ! If aud:Ref_Number <> job:Ref_Number
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type <> 'JOB'
        If aud:Action <> 'WARRANTY CLAIM REJECTED'
            Break
        End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
        CountReasons# += 1
        If CountReasons# > 2
            Break
        End ! If CountReasons# > 3

        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

        END ! IF
        exp:Line1   = Clip(exp:Line1) & '","' & Format(aud:Date,@d06)
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(BHStripNonAlphaNum(BHStripReplace(Sub(aud2:Notes,9,255),',',''),' '))
    End ! Loop

    CountReasons# += 1
    If CountReasons# <= 2
        Loop loop# = CountReasons# To 2
            exp:Line1   = Clip(exp:Line1) & '","'
            exp:Line1   = Clip(exp:Line1) & '","'
        End ! Loop loop# = CountReasons# To 3
    End ! If CountReasons# <= 3

    ! Rejection By
    Found# = 0
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = job:Ref_Number
    aud:Type       = 'JOB'
    aud:Action     = 'WARRANTY CLAIM FINAL REJECTION'
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> job:Ref_Number
            Break
        End ! If aud:Ref_Number <> job:Ref_Number
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type <> 'JOB'
        If aud:Action <> 'WARRANTY CLAIM FINAL REJECTION'
            Break
        End ! If aud:Action <> 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'

        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

        END ! IF

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = aud:User
        If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
            !Found
            exp:Line1   = Clip(exp:Line1) & '","' & Format(aud:Date,@d06)
            exp:Line1   = Clip(exp:Line1) & '","' & Clip(BHStripNonAlphaNum(BHStripReplace(Sub(aud2:Notes,9,255),',',''),' '))
            exp:Line1   = Clip(exp:Line1) & '","' & Clip(use:Forename) & ' ' & Clip(use:Surname)
            Found# = 1
            Break
        Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
            !Error
        End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
    End ! Loop
    If Found# = 0
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If Found# = 0

    ! Da End
    exp:Line1   = Clip(exp:Line1) & '"'

    Add(ExportFile)

    total:Jobs += 1

getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|/Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020720'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ClaimStatusReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  Access:JOBS.UseFile
  Access:JOBSE2.UseFile
  Access:AUDSTATS.UseFile
  Access:INVOICE.UseFile
  Access:JOBSWARR.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  !Initialize Dates and save Head Account Information (DBH: 22-03-2004)
  
  tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
  pos# = Instring('%',COMMAND(),1,1)
  If pos#
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = Clip(Sub(COMMAND(),pos#+1,30))
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  End !pos#
  OPEN(window)
  SELF.Opened=True
  ! ================ Set Report Version =====================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5003'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020720'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020720'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020720'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Print
      ThisWindow.Update
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.WriteLine         Procedure()
Code
    Clear(exp:Record)

    Add(ExportFile)
Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
