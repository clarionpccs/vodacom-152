

   MEMBER('StockHistAdj.clw')                         ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('STOCK001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

save_shi_id          USHORT,AUTO
tmp:ImportFile       STRING(255),STATIC
tmp:ExportFile       STRING(255),STATIC
tmp:SiteLocation     STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:SiteLocation
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
window               WINDOW('Stock History Adjustment'),AT(,,302,103),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,296,68),USE(?Sheet1),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Routine to add an initial stock history entry in selected Location'),AT(8,8,204,20),USE(?Prompt1)
                           COMBO(@s30),AT(84,33,124,8),USE(tmp:SiteLocation),IMM,REQ,FORMAT('120L(2)|M~Location~@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Site Location'),AT(8,32),USE(?Prompt3)
                           ENTRY(@s255),AT(84,52,200,8),USE(tmp:ImportFile),FONT(,,,FONT:bold),COLOR(0BDFFFFH),REQ
                           BUTTON,AT(288,52,10,8),USE(?LookupImportFile),SKIP,ICON('List3.ico')
                           PROMPT('Import File'),AT(8,52),USE(?Prompt2:2)
                         END
                       END
                       PANEL,AT(4,76,296,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('Import'),AT(8,80,56,16),USE(?Import),LEFT,ICON('DISK.GIF')
                       BUTTON('Close'),AT(240,80,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup2          SelectFileClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
StockImport    File,Driver('BASIC'),Pre(stoimp),Name(tmp:ImportFile),Create,Bindable,Thread
Record                  Record
Location            String(30)
PartNumber          String(30)
Quantity            Long
PurchaseCost        Real
ShelfLocation       String(30)
                        End
                    End
!Category            String(1)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Access:STOCK.UseFile
  Access:MANUFACT.UseFile
  Access:MANMARK.UseFile
  Access:LOCSHELF.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  FileLookup2.Init
  FileLookup2.Flags=BOR(FileLookup2.Flags,FILE:LongName)
  FileLookup2.SetMask('CSV File','*.CSV')
  FDCB1.Init(tmp:SiteLocation,?tmp:SiteLocation,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(loc:Main_Store_Key)
  FDCB1.AddField(loc:Location,FDCB1.Q.loc:Location)
  FDCB1.AddField(loc:RecordNumber,FDCB1.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupImportFile
      ThisWindow.Update
      tmp:ImportFile = Upper(FileLookup2.Ask(1)  )
      DISPLAY
    OF ?Import
      ThisWindow.Update
      Beep(Beep:SystemExclamation)  ;  Yield()
      Case Message('Click ''OK'' To Begin', |
              , Icon:Exclamation, |
               Button:OK+Button:Cancel, Button:OK, 0)
      Of Button:OK
          Exceptions# = 0
      
          Error# = 0
          If tmp:ImportFile = ''
              Select(?tmp:ImportFile)
              Error# = 1
          End !tmp:ImportFile = ''
      
      
          If Error# = 0
      
              Open(StockImport)
      
              Count# = 0
              Set(StockImport,0)
              Loop
                  Next(StockImport)
                  If Error()
                      Break
                  End !If Error()
                  Count# += 1
              End !Loop
      
              Prog.ProgressSetup(Count#)
      
              Set(StockImport,0)
              Loop
                  Next(StockImport)
                  If Error()
                      Break
                  End !If Error()
      
                  If Prog.InsideLoop()
                      Break
                  End !If Prog.InsideLoop()
      
      
                  Access:STOCK.ClearKey(sto:Location_Key)
                  sto:Location    = tmp:SiteLocation
                  sto:Part_Number = stoimp:PartNumber
                  If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                      !Found
                      !Ok, thanks to Vodacom's cockup, if there already is a INITIAL entry.. replace it
                      Access:LOCATION.Clearkey(loc:Location_Key)
                      loc:Location    = tmp:SiteLocation
                      If Access:LOCATION.Fetch(loc:Location_Key) = Level:Benign
                      End!If Access:LOCATION.Fetch(loc:Location_Key) = Level:Benign
      
                      Found# = 0
                      Save_shi_ID = Access:STOHIST.SaveFile()
                      Access:STOHIST.ClearKey(shi:Ref_Number_Key)
                      shi:Ref_Number = sto:Ref_Number
                      Set(shi:Ref_Number_Key,shi:Ref_Number_Key)
                      Loop
                          If Access:STOHIST.NEXT()
                             Break
                          End !If
                          If shi:Ref_Number <> sto:Ref_Number       |
                              Then Break.  ! End If
      
                          If shi:Notes = 'INITIAL STOCK QUANTITY'
                              shi:Quantity                = stoimp:Quantity
                              If loc:Main_Store = 'YES'
                                  shi:Purchase_Cost           = Round(stoimp:PurchaseCost,.01)
                                  shi:Sale_Cost               = Markup(shi:Sale_Cost,shi:Purchase_Cost,sto:Percentage_Mark_Up)
                                  shi:Retail_Cost             = Markup(shi:Retail_cost,shi:Purchase_Cost,sto:RetailMarkup)
      
                              Else !If loc:Main_Store = 'YES'
                                  shi:Purchase_Cost           = Markup(shi:Purchase_Cost,Round(stoimp:PurchaseCost,.01),sto:PurchaseMarkup)
                                  shi:Sale_Cost               = Markup(shi:Sale_Cost,Round(stoimp:PurchaseCost,.01),sto:Percentage_Mark_Up)
                                  shi:Retail_Cost             = '' !Doesn't apply if not Main Store
                              End !If loc:Main_Store = 'YES'
                              Access:STOHIST.TryUpdate()
                              Found# = 1
                              Break
                          End !If shi:Notes = 'INITIAL STOCK QUANTITY'
                      End !Loop
                      Access:STOHIST.RestoreFile(Save_shi_ID)
      
                      If Found# = 0
                          If Access:STOHIST.Primerecord() = Level:Benign
                              shi:Ref_Number              = sto:Ref_Number
                              shi:Transaction_Type        = 'ADD'
                              shi:Despatch_Note_Number    = ''
                              shi:Quantity                = stoimp:Quantity
                              shi:Date                    = Deformat('1/8/2002',@d6)
                              If loc:Main_Store = 'YES'
                                  shi:Purchase_Cost           = Round(stoimp:PurchaseCost,.01)
                                  shi:Sale_Cost               = Markup(shi:Sale_Cost,shi:Purchase_Cost,sto:Percentage_Mark_Up)
                                  shi:Retail_Cost             = Markup(shi:Retail_cost,shi:Purchase_Cost,sto:RetailMarkup)
      
                              Else !If loc:Main_Store = 'YES'
                                  shi:Purchase_Cost           = Markup(shi:Purchase_Cost,Round(stoimp:PurchaseCost,.01),sto:PurchaseMarkup)
                                  shi:Sale_Cost               = Markup(shi:Sale_Cost,Round(stoimp:PurchaseCost,.01),sto:Percentage_Mark_Up)
                                  shi:Retail_Cost             = '' !Doesn't apply if not Main Store
      
      
                              End !If loc:Main_Store = 'YES'
                              shi:Job_Number              = 0
                              Access:USERS.Clearkey(use:Password_Key)
                              use:Password                = glo:Password
                              Access:USERS.Tryfetch(use:Password_Key)
                              shi:User                    = use:User_Code
                              shi:Notes                   = 'INITIAL STOCK QUANTITY'
                              shi:Information             = ''
                              If Access:STOHIST.Tryinsert()
                                  Access:STOHIST.Cancelautoinc()
                              End!If Access:STOHIST.Tryinsert()
                          End!If Access:STOHIST.Primerecord() = Level:Benign
                      End !If Found# = 0
                  Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              End !Loop
      
              Prog.ProgressFinish()
      
          End !If Error()
          Close(StockImport)
      
      Of Button:Cancel
      End !CASE
      
      Message('Completed')
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            return 1
        Of Button:No
        End !CASE
    End!If cancel# = 1

    return 0
