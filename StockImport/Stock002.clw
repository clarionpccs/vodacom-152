

   MEMBER('StockHistAdj.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('STOCK002.INC'),ONCE        !Local module procedure declarations
                     END


MarkUP               PROCEDURE  (func:OriginalCost,func:Cost,func:MarkUp) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
    If func:MarkUp <> 0
        Return func:Cost + (func:Cost * (func:MarkUp/100))
    Else !If func:MarkUp <> 0
        Return func:OriginalCost
    End !If func:MarkUp <> 0
    
