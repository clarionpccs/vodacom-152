   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE

!* * * * Line Print Template Generated Code * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

OFSTRUCT    GROUP,TYPE                                                                  
cBytes       BYTE                                                                       ! Specifies the length, in bytes, of the structure
cFixedDisk   BYTE                                                                       ! Specifies whether the file is on a hard (fixed) disk. This member is nonzero if the file is on a hard disk
nErrCode     SIGNED                                                                     ! Specifies the MS-DOS error code if the OpenFile function failed
Reserved1    SIGNED                                                                     ! Reserved, Don't use
Reserved2    SIGNED                                                                     ! Reserved, Don't use
szPathName   BYTE,DIM(128)                                                              ! Specifies the path and filename of the file
            END

LF                  CSTRING(CHR(10))                                                    ! Line Feed
FF                  CSTRING(CHR(12))                                                    ! Form Feed
CR                  CSTRING(CHR(13))                                                    ! Carriage Return

lpszFilename        CSTRING(144)                                                        ! File Name
fnAttribute         SIGNED                                                              ! Attribute
hf                  SIGNED                                                              ! File Handle
hpvBuffer           STRING(500)                                                        ! String To Write
cbBuffer            SIGNED                                                              ! Characters to Write
BytesWritten        SIGNED                                                              ! Characters Written

Succeeded           EQUATE(0)                                                           ! Function Succeeded
OpenError           EQUATE(1)                                                           ! Can Not Open File or Device
WriteError          EQUATE(2)                                                           ! Can not Write to File or Device
CloseError          EQUATE(3)                                                           ! Can not Close File or Device
SeekError           EQUATE(4)                                                           ! Can not Seek File or Device

ATTR_Normal         EQUATE(0)                                                           ! File Attribute Normal
ATTR_ReadOnly       EQUATE(1)                                                           ! File Attribute Read Only
ATTR_Hidden         EQUATE(2)                                                           ! File Attribute Hidden
ATTR_System         EQUATE(3)                                                           ! File Attribute System

OF_READ             EQUATE(0000h)                                                       ! Opens the file for reading only
OF_WRITE            EQUATE(0001h)                                                       ! Opens the file for writing only
OF_READWRITE        EQUATE(0002h)                                                       ! Opens the file for reading and writing
OF_SHARE_COMPAT     EQUATE(0000h)                                                       ! Opens the file with compatibility mode, allowing any program on a given machine to open the file any number of times.
OF_SHARE_EXCLUSIVE  EQUATE(0010h)                                                       ! Opens the file with exclusive mode, denying other programs both read and write access to the file
OF_SHARE_DENY_WRITE EQUATE(0020h)                                                       ! Opens the file and denies other programs write access to the file
OF_SHARE_DENY_READ  EQUATE(0030h)                                                       ! Opens the file and denies other programs read access to the file
OF_SHARE_DENY_NONE  EQUATE(0040h)                                                       ! Opens the file without denying other programs read or write access to the file
OF_PARSE            EQUATE(0100h)                                                       ! Fills the OFSTRUCT structure but carries out no other action
OF_DELETE           EQUATE(0200h)                                                       ! Deletes the file
OF_VERIFY           EQUATE(0400h)                                                       ! Compares the time and date in the OF_STRUCT with the time and date of the specified file
OF_SEARCH           EQUATE(0400h)                                                       ! Windows searches in directories even when the file name includes a full path
OF_CANCEL           EQUATE(0800h)                                                       ! Adds a Cancel button to the OF_PROMPT dialog box. Pressing the Cancel button directs OpenFile to return a file-not-found error message
OF_CREATE           EQUATE(1000h)                                                       ! Creates a new file. If the file already exists, it is truncated to zero length
OF_PROMPT           EQUATE(2000h)                                                       ! Displays a dialog box if the requested file does not exist.
OF_EXIST            EQUATE(4000h)                                                       ! Opens the file, and then closes it. This value is used to test for file existence
OF_REOPEN           EQUATE(8000h)                                                       ! Opens the file using information in the reopen buffer

OF_STRUCT           LIKE(OFSTRUCT)                                                      ! Will Be loaded with File information. See OFSTRUCT above

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

   INCLUDE('FE.INC'),ONCE
!Win32 data types           ! --- After Global Includes ----------------------------------
DWORD                       EQUATE(LONG)
LPVOID                      EQUATE(LONG)
LPCSTR                      EQUATE(CSTRING)
CHAR                        EQUATE(BYTE)
LPSTR                       EQUATE(CSTRING)
WORD                        EQUATE(SIGNED)
LPCTSTR                     EQUATE(LONG)
LPTSTR                      EQUATE(CSTRING)
HANDLE                      EQUATE(UNSIGNED)
PRINTER_INFO_5          GROUP,TYPE ! --- After Global Includes ----------------------------------
pPrinterName              LONG !CSTRING
pPortName                 LONG !&CSTRING Pad CSTRING(260)
dwAttributes              LONG
dwDeviceNotSelectedTO     LONG
dwTransmissionRetryTO     LONG
                END
DOC_INFO_1              GROUP,TYPE ! --- After Global Includes ----------------------------------
pDocName                  LONG !CSTRING
pOutputFile               LONG !CSTRING
pDatatype                 LONG !CSTRING
                END
!Shell Execute Variables
AssocFile    CString(255)
Operation    CString(255)
Param        CString(255) 
Dir          CString(255)
   INCLUDE('CLIENT.INC')                              !---ClarioNET 106
   INCLUDE('PrnProp.clw')

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('CONNEBC.CLW')
DctInit     PROCEDURE
DctKill     PROCEDURE
     END
     MODULE('FileExplorerDLL.Lib')
fe_ClassVersion        PROCEDURE(byte Flag=0),string,name('fe_ClassVersion'),DLL(dll_mode)
feDispose              PROCEDURE(),name('feDispose'),DLL(dll_mode)
fe_OnlineStatus        PROCEDURE(byte pOption=0),byte,name('fe_OnlineStatus'),DLL(dll_mode)
fe_OnlineStatusConnectionName PROCEDURE(byte pOption=0),string,name('fe_OnlineStatusConnectionName'),DLL(dll_mode)
fe_StartIE             PROCEDURE(string pURL),name('fe_StartIE'),DLL(dll_mode)
fe_DebugString         PROCEDURE(string DbgMessage),name('fe_DebugString'),DLL(dll_mode)
fe_GetRegValue         PROCEDURE(string ParentFolder, string NameOfKey, string NameOfValue),string,name('fe_GetRegValue'),DLL(dll_mode)
fe_GetOSVersionInfo    PROCEDURE(),string,name('fe_GetOSVersionInfo'),DLL(dll_mode)
fe_GetIEVersionInfo    PROCEDURE(byte pTranslate=0),string,name('fe_GetIEVersionInfo'),DLL(dll_mode)
fe_ShellExecute        PROCEDURE(string pParm1),name('fe_ShellExecute'),DLL(dll_mode)
fe_SetFocus            PROCEDURE(long pHandle),name('fe_SetFocus'),DLL(dll_mode)
fe_MCIPlay             PROCEDURE(string pFileName),byte,proc,name('fe_MCIPlay'),DLL(dll_mode)
fe_MCIStop             PROCEDURE(<string pFileName>),byte,proc,name('fe_MCIStop'),DLL(dll_mode)
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('CONNE001.CLW')
ClarioNET:ConnectWindow PROCEDURE   !Main Procedure Called if ClarioNET Active
     END
     MODULE('CONNE009.CLW')
print_local_invoice    PROCEDURE   !
     END
     MODULE('CONNE012.CLW')
HelpBrowser            PROCEDURE(String)   !Basic Html Browser - Using COM
     END
     MODULE('CONNE013.CLW')
Missive                FUNCTION(String,String,String,String),byte   !
     END
     INCLUDE('C55Util.INC')
     
     !* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
          LinePrint(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>),BYTE,PROC          ! Declare LinePrint Function
          DeleteFile(STRING FileToDelete),BYTE,PROC                                          ! Declare DeleteFile Function
            MODULE('')                                                                       ! MODULE Start
             OpenFile(*CSTRING,*OFSTRUCT,SIGNED),SIGNED,PASCAL,RAW                           ! Open/Create/Delete File
             _llseek(SIGNED,LONG,SIGNED),LONG,PASCAL                                         ! Control File Pointer
             _lwrite(SIGNED,*STRING,UNSIGNED),UNSIGNED,PASCAL,RAW                           ! Write to File
             _lclose(SIGNED),SIGNED,PASCAL                                                   ! Close File
            END                                                                              ! Terminate Module
     
     !* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
         MODULE('Windows API Call')
           BringWindowToTop(LONG),BYTE,RAW,PASCAL,NAME('BringWindowToTop'),PROC
         END
         Include('DDE.CLW')
         Module('winapi')
           DBGGetExitCodeProcess(unsigned, ulong),signed,pascal,NAME('GetExitCodeProcess')
           DBGCreateProcess(ulong, ulong, ulong, ulong, signed, ulong, ulong, ulong, ulong, ulong ),signed,raw,pascal,name('CreateProcessA')
           DBGSleep(ulong),pascal,raw,name('Sleep'),NAME('Sleep')
         End
     Module('WINAPI') !--- Inside the Global Map ----------------------------------
         GetDesktopWindow(), Unsigned, Pascal 
         ShellExecute(Unsigned, *Cstring, *Cstring, *Cstring, *Cstring, Signed), Unsigned, Pascal, Raw, Proc,name('ShellExecuteA')
     
         sleep(unsigned),pascal,name('sleep')
     End
     
     
     Include('clib.clw')
         MODULE('Windows32.dll') ! --- Inside the Global Map ----------------------------------
           OpenPrinter(*LPSTR,*HANDLE,DWORD),BOOL,PASCAL,RAW,NAME('OpenPrinterA')
           StartDocPrinter(HANDLE,DWORD,DWORD),DWORD,PASCAL,NAME('StartDocPrinterA')
           StartPagePrinter(HANDLE),BOOL,PASCAL
           WritePrinter(HANDLE,DWORD,DWORD,*DWORD),BOOL,PASCAL,RAW
           EndPagePrinter(HANDLE),BOOL,PASCAL,PROC
           EndDocPrinter(HANDLE),BOOL,PASCAL,PROC
           ClosePrinter(HANDLE),BOOL,PASCAL,PROC
           GetPrinterA(HANDLE,DWORD,*PRINTER_INFO_5,DWORD,*DWORD),BOOL,PASCAL,RAW
         END
           MODULE('CNMANAGR.lib')
             CNManager:GetServerConnection(STRING Server, USHORT Port, STRING Request, USHORT Timeout),STRING
             CNManager:GetBalancerConnection(STRING Server, USHORT Port, STRING Request, USHORT Timeout),STRING
             CNManager:GetProgramList(STRING Server, USHORT Port, USHORT Timeout),SHORT,proc
             CNManager:Ping(STRING Server, USHORT Port, USHORT Timeout),STRING,PROC
           END
         INCLUDE('CLIENT.CLW')                        !---ClarioNET 108
         MODULE('win32.lib')                          !---ClarioNET 109
           GetTempPath(UNSIGNED, *CSTRING),UNSIGNED,RAW,PASCAL,NAME('GetTempPathA') !---ClarioNET 110
         END                                          !---ClarioNET 111
     !---                                  
     !--- Auto-Generated By ClarioNET Global Client Template
     !---
     ClarioNET:ClientProcedure               (*WINDOW W1, STRING P1,<STRING P2> ,<STRING P3 >,|
                                                                    <STRING P4> ,<STRING P5 >,|
                                                                    <STRING P6> ,<STRING P7 >,|
                                                                    <STRING P8> ,<STRING P9 >,|
                                                                    <STRING P10>,<STRING P11>,|
                                                                    <STRING P12>,<STRING P13>,|
                                                                    <STRING P14>,<STRING P15>),STRING
     ClarioNET:ClientFilesReceivedFromServer (SIGNED Value),STRING
     ClarioNET:SendClientFilesToServer       (SHORT Flag, STRING ServerFileInfo),STRING
     !---                                  
     MODULE('CPC55L32.LIB')
       Wmf2Ascii(QUEUE, STRING, LONG),NAME('Wmf2Ascii')
       PaperSupport(SHORT,<QUEUE>),STRING,NAME('PaperSupport')
       PrintPreview(QUEUE,SHORT,<STRING>,REPORT,BYTE,<STRING>,<LONG>,<STRING>,<STRING>),LONG,NAME('PrintPreview')
       Supported(LONG),SHORT,NAME('Supported')
       HandleCopies(QUEUE,LONG),NAME('HandleCopies')
       ShowPrinterStatus(STRING,SHORT,<STRING>),NAME('ShowPrinterStatus')
       AssgnPgOfPg(Queue,<STRING>,<BYTE>,<STRING>,<LONG>),NAME('AssgnPgOfPg')
       SaveClipToFile(<STRING>),STRING,NAME('SaveClipToFile')
       SetPrinterDraftMode(),LONG,NAME('SetPrinterDraftMode')
     END
          FillCpcsIniStrings(STRING,STRING,STRING,STRING)
          MODULE('conne_SF.CLW')
            CheckOpen(FILE File,<BYTE OverrideCreate>,<BYTE OverrideOpenMode>)
            StandardWarning(LONG WarningID),LONG,PROC
            StandardWarning(LONG WarningID,STRING WarningText1),LONG,PROC
            StandardWarning(LONG WarningID,STRING WarningText1,STRING WarningText2),LONG,PROC
            RISaveError
          END
   END
   INCLUDE('C55UtilEquates.INC')

LocalSavePath      STRING(255)
supervisor         BYTE
DefaultPrinter     STRING(50)
file_sent          BYTE
file_name          STRING(20)
InvoiceFileName    STRING(255)
LicenceNumber      STRING(30)
LogonID            STRING(30)
LogonPassword      STRING(30)
LogonUserPassword  STRING(15)
Company            STRING(30)
Order_Number       STRING(20)
Consignment        STRING(30)
Who_Booked         STRING(30)
Receipt_Text       STRING(200)
PrinterList        QUEUE,PRE(PList)
ReportName           STRING(30)
PrinterName          STRING(50)
                   END
SilentRunning        BYTE(0)                         !Set true when application is running in silent mode

    Map
BHStripReplace           Procedure(String func:String,String func:Strip,String func:Replace),String
BHStripNonAlphaNum           Procedure(String func:String,String func:Replace),String
BHReturnDaysHoursMins       Procedure(Long func:TotalMins,*Long func:Days,*Long func:Hours,*Long func:Mins)
BHTimeDifference24Hr        Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime),Long
BHTimeDifference24Hr5Days   Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime),Long
BHTimeDifference24Hr5DaysMonday   Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime,String func:MondayStart),Long
BHGetFileFromPath           Procedure(String f:Path),String
BHGetPathFromFile           Procedure(String f:Path),String
BHGetFileNoExtension        Procedure(String fullPath),String
BHAddBackSlash              Procedure(STRING fullPath),STRING
BHRunDOS                    Procedure(String f:Command,<Byte f:Wait>,<Byte f:NoTimeOut>),Long
    End
CPCSStartUpPrintDevice      CSTRING(64)

CPCS:ProgWinTitlePrvw   CSTRING(64)  
CPCS:ProgWinTitlePrnt   CSTRING(64)  
CPCS:ProgWinPctText     CSTRING(64)  
CPCS:ProgWinRecText     CSTRING(64)  
CPCS:ProgWinUsrText     CSTRING(64)  
CPCS:PrtrDlgTitle       CSTRING(64)  
CPCS:AskPrvwDlgTitle    CSTRING(64)  
CPCS:AskPrvwDlgText     CSTRING(64)  
CPCS:AreYouSureTitle    CSTRING(64)  
CPCS:AreYouSureText     CSTRING(64)  
CPCS:PrvwPartialTitle   CSTRING(64)  
CPCS:PrvwPartialText    CSTRING(128) 
CPCS:NoDfltPrtrTitle    CSTRING(64)  
CPCS:NoDfltPrtrText     CSTRING(255) 
CPCS:NthgToPrvwTitle    CSTRING(64)  
CPCS:NthgToPrvwText     CSTRING(64)  
CPCS:NthgToPrntTitle    CSTRING(64)  
CPCS:NthgToPrntText     CSTRING(64)  
CPCS:AsciiOutTitle      CSTRING(64)  
CPCS:AsciiOutmasks      CSTRING(64)  
CPCS:DynLblErrTitle     CSTRING(64)
CPCS:DynLblErrText1     CSTRING(64)  
CPCS:DynLblErrText2     CSTRING(64)  

SaveErrorCode               LONG
SaveError                   CSTRING(255)
SaveFileErrorCode           LONG
SaveFileError               CSTRING(255)

Warn:InvalidFile         EQUATE (1)
Warn:InvalidKey          EQUATE (2)
Warn:RebuildError        EQUATE (3)
Warn:CreateError         EQUATE (4)
Warn:CreateOpenError     EQUATE (5)
Warn:ProcedureToDo       EQUATE (6)
Warn:BadKeyedRec         EQUATE (7)
Warn:OutOfRangeHigh      EQUATE (8)
Warn:OutOfRangeLow       EQUATE (9)
Warn:OutOfRange          EQUATE (10)
Warn:NotInFile           EQUATE (11)
Warn:RestrictUpdate      EQUATE (12)
Warn:RestrictDelete      EQUATE (13)
Warn:InsertError         EQUATE (14)
Warn:RIUpdateError       EQUATE (15)
Warn:UpdateError         EQUATE (16)
Warn:RIDeleteError       EQUATE (17)
Warn:DeleteError         EQUATE (18)
Warn:InsertDisabled      EQUATE (19)
Warn:UpdateDisabled      EQUATE (20)
Warn:DeleteDisabled      EQUATE (21)
Warn:NoCreate            EQUATE (22)
Warn:ConfirmCancel       EQUATE (23)
Warn:DuplicateKey        EQUATE (24)
Warn:AutoIncError        EQUATE (25)
Warn:FileLoadError       EQUATE (26)
Warn:ConfirmCancelLoad   EQUATE (27)
Warn:FileZeroLength      EQUATE (28)
Warn:EndOfAsciiQueue     EQUATE (29)
Warn:DiskError           EQUATE (30)
Warn:ProcessActionError  EQUATE (31)
Warn:StandardDelete      EQUATE (32)
Warn:SaveOnCancel        EQUATE (33)
Warn:LogoutError         EQUATE (34)
Warn:RecordFetchError    EQUATE (35)
Warn:ViewOpenError       EQUATE (36)
Warn:NewRecordAdded      EQUATE (37)
Warn:RIFormUpdateError   EQUATE (38)


Labels               FILE,DRIVER('ASCII'),OEM,PRE(LAB),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Textline                    STRING(255)
                         END
                     END                       

Reports              FILE,DRIVER('ASCII'),OEM,PRE(REP),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Name                        STRING(30)
                         END
                     END                       

Server               FILE,DRIVER('TOPSPEED'),OEM,NAME('Server.tps'),PRE(SRV),CREATE,BINDABLE,THREAD
KeyRecordNo              KEY(SRV:RecordNo),NOCASE
KeyMainServerFlag        KEY(SRV:MainServerFlag),DUP,NOCASE
KeyPriority              KEY(SRV:Priority),NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
IPAddrress                  STRING(20)
Port                        SHORT
MainServerFlag              STRING(1)
ProgramPath                 STRING(40)
Priority                    LONG
                         END
                     END                       

ServerAlias          FILE,DRIVER('TOPSPEED'),OEM,NAME('Server.tps'),PRE(SRA),CREATE,BINDABLE,THREAD
KeyRecordNo              KEY(SRA:RecordNo),NOCASE
KeyMainServerFlag        KEY(SRA:MainServerFlag),DUP,NOCASE
KeyPriority              KEY(SRA:Priority),NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
IPAddrress                  STRING(20)
Port                        SHORT
MainServerFlag              STRING(1)
ProgramPath                 STRING(40)
Priority                    LONG
                         END
                     END                       



!---
CLRNT_C     LIKE(ClarioNETSession),EXTERNAL,DLL(dll_mode) !---ClarioNET 107
!---
Channel     Long
Access:Labels        &FileManager
Relate:Labels        &RelationManager
Access:Reports       &FileManager
Relate:Reports       &RelationManager
Access:Server        &FileManager
Relate:Server        &RelationManager
Access:ServerAlias   &FileManager
Relate:ServerAlias   &RelationManager
FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE(0),THREAD
GlobalResponse       BYTE(0),THREAD
VCRRequest           LONG(0),THREAD
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  DctInit
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('connect.INI')
      omit('***',ClarionetUsed=0)
      If ~clarionetserver:Active()
      ***
      IF (INSTRING(Upper('connect.exe'),Upper(DDEQUERY()),1,1))
      !look for running instances of this program
  !    IF Channel <> 0 ! and if found
          Channel = DDECLIENT('connect.exe')
          DDEEXECUTE(Channel,'INFRONT') ! tell the running instance to take focus
          DDECLOSE(Channel)
          RETURN ! then get out
      END
      omit('***',ClarionetUsed=0)
      End !If clarionetserver:Active()
      ***
  CPCSStartUpPrintDevice = PRINTER{PROPPRINT:DEVICE}
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  ClarioNET:ConnectWindow
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  DctKill
  GlobalErrors.Kill
    
!---                                  
!--- Auto-Generated By ClarioNET Global Client Template
!---                                  
!--- You can Override the default behavior by using the EMBED points
!--- in the Global EMBEDs section
!---                                  
ClarioNET:SendClientFilesToServer       PROCEDURE(SHORT Flag, STRING ServerFileInfo)
!---
!--- This procedure should load FileListQueue with the full drive/directory/name of the files
!--- to send to the server.
!--- The following is SAMPLE CODE ONLY to help you with using FILEDIALOG to load filenames
!--- into the ClarioNET "FileListQueue".
!---
!--- The only requirement of this procedure is that is have a RETURN('') after the CODE statement.
!---
locExportFile       String(255)
locSavePath         String(255)
!---
!--- Place an OMIT('!END of ClarioNET') in your EMBED code (above) if you do not
!--- want the Default Behavior
!---
LocalVars       GROUP,PRE(LOC)
ReturnString        STRING(256)
FileList            STRING(5000)
Extension           STRING('*.*')
PathName            CSTRING(FILE:MaxFilePath)
Pos1                SHORT
Pos2                SHORT
RetVal              SHORT
                END
!END of ClarioNET
!---
 CODE
!---
!Called by the server ready to transfer

! This bit works, just not relevant!
    Free(FileListQueue)
    Case Flag
    Of 1 ! Import CSV
        locSavePath = Path()
        if (fileDialog('Choose File',locExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName))
            !Found
            setPath(locSavePath)
            flq:FileName = Clip(locExportFile)
            add(FileListQueue)
            Return(1)
        else ! if (fileDialog)
            !Error
            setPath(locSavePath)
            Return(0)
        end ! if (fileDialog)
    End

! stop(clip(ServerFileInfo) & '<13,10>' & clip(flag))
! FLQ:filename = 'WM_' & clip(ServerFileInfo) & '.TXT'
!
! if not exists (FLQ:filename) then
!    lineprint ('FAIL', FLQ:filename)
!    !create file and put one word in it
! end
!
! add(FileListQueue)
! file_sent = true
! file_name = flq:filename
! return('Hello World')
! 
!---
!--- Place an OMIT('!END of ClarioNET') in your EMBED code (above) if you do not
!--- want the Default Behavior
!---
   LOC:ReturnString = 'ok'
!---
   FREE(FileListQueue)

   LOC:RetVal = FILEDIALOG('Select Files To Send', LOC:FileList, LOC:Extension, 11000b)
   IF LOC:RetVal = 0
      RETURN('no files selected')
   END

   LOC:Pos1 = INSTRING('<124>', LOC:FileList, 1, 1)
   IF LOC:Pos1 = 0
     FLQ:Filename = CLIP(LOC:FileList)
     ADD(FileListQueue)
   ELSE
     LOC:PathName = LOC:FileList[1 : LOC:Pos1-1] & '\'
     LOOP
       LOC:Pos2 = INSTRING('<124>', LOC:FileList, 1, LOC:Pos1+1)
       IF LOC:Pos2 <> 0
         FLQ:Filename = LOC:PathName & LOC:FileList[LOC:Pos1+1 : LOC:Pos2 - 1]
         ADD(FileListQueue)
         LOC:Pos1 = LOC:Pos2
       ELSE
         LOC:Pos2 = LEN(CLIP(LOC:FileList))
         FLQ:Filename = LOC:PathName & LOC:FileList[LOC:Pos1+1 : LOC:Pos2]
         ADD(FileListQueue)
         BREAK
       END
     END
   END
   !LOOP I# = 1 TO RECORDS(FileListQueue)
   !  GET(FileListQueue, I#)
   !  MESSAGE('File ' & i# & ' : ' & CLIP(FLQ:Filename))
   !END
 RETURN(LOC:ReturnString)
!END of ClarioNET
!------------------------------------------------------------------------------
!---                                  
!--- Auto-Generated By ClarioNET Global Client Template
!---                                  
!--- You can Override the default behavior by using the EMBED points
!--- in the Global EMBEDs section
!---                                  
ClarioNET:ClientFilesReceivedFromServer PROCEDURE(SIGNED Value)
!---
!---
!--- Place an OMIT('!END of ClarioNET') in your EMBED code (above) if you do not
!--- want the Default Behavior
!---
LOC:ReturnString    STRING(256)
LOC:Value1          SIGNED
!---
window WINDOW('List of Files Received'),AT(,,251,124),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI), |
         GRAY,DOUBLE
       STRING('Flag Received fromServer = '),AT(4,4,94,10),USE(?String1)
       STRING(@n12),AT(100,4),USE(LOC:Value1),LEFT
       LIST,AT(4,16,244,88),USE(?List1),FORMAT('20L(2)~Files Received~@s50@'),FROM(FileListQueue)
       BUTTON('OK'),AT(204,108,45,14),USE(?OK)
     END
!END of ClarioNET
 CODE
!to develop

loop x#=1 to records(FileListQueue)
    get(FileListQueue,x#)
    copy(clip(FLQ:Filename),LocalSavePath)
    remove(FLQ:Filename)
END !Loop
return('DONE')
!---
!--- Place an OMIT('!END of ClarioNET') in your EMBED code (above) if you do not
!--- want the Default Behavior
!---
 LOC:ReturnString = 'DONE: ClarioNET:ClientFilesReceivedFromServer'
 LOC:Value1       = Value
!---
 OPEN(window)
 ACCEPT
     IF EVENT() = EVENT:Accepted
        BREAK
     END
 END
 CLOSE(window)
 RETURN(LOC:ReturnString)
!END of ClarioNET
!------------------------------------------------------------------------------
!---                                  
!--- Auto-Generated By ClarioNET Global Client Template
!---                                  
!--- You can Override the default behavior by using the EMBED points
!--- in the Global EMBEDs section
!---                                  
ClarioNET:ClientProcedure               PROCEDURE(*WINDOW W1 , |
                                                  STRING P1  , |
                                                 <STRING P2 >, |
                                                 <STRING P3 >, |
                                                 <STRING P4 >, |
                                                 <STRING P5 >, |
                                                 <STRING P6 >, |
                                                 <STRING P7 >, |
                                                 <STRING P8 >, |
                                                 <STRING P9 >, |
                                                 <STRING P10>, |
                                                 <STRING P11>, |
                                                 <STRING P12>, |
                                                 <STRING P13>, |
                                                 <STRING P14>, |
                                                 <STRING P15>)
!---
!---
!--- Place an OMIT('!END of ClarioNET') in your EMBED code (above) if you do not
!--- want the Default Behavior
!---
LOC:ReturnString    STRING(256)
!END of ClarioNET
!---
 CODE
!---
!This is called by the server program

 Case Upper(clip(p1))

    of 'CLIPBOARD'
        setclipboard(p2)

    of 'MESSAGE'
        message(p2)

    of 'REPORT'
        !Receipt_Report(p2)

    of 'LABEL'
        !Save all the fifteen parameters to the local save file
        !Each is a string of up to 200 characters ...
        LinePrint (clip(p2) ,'Labels')
        LinePrint (clip(p3) ,'Labels')
        LinePrint (clip(p4) ,'Labels')
        LinePrint (clip(p5) ,'Labels')
        LinePrint (clip(p6) ,'Labels')
        LinePrint (clip(p7) ,'Labels')
        LinePrint (clip(p8) ,'Labels')
        LinePrint (clip(p9) ,'Labels')
        LinePrint (clip(p10),'Labels')
        LinePrint (clip(p11),'Labels')
        LinePrint (clip(p12),'Labels')
        LinePrint (clip(p13),'Labels')
        LinePrint (clip(p14),'Labels')
        LinePrint (clip(p15),'Labels')
        
  of 'VERSIONSAVE'
       putini ('global' ,'SBVERSION',p2,path()&'\conweb.ini')

  of 'SETTIME'
       CLRNT_C.Param3 = clip(p2)

  of 'SETCLIENTPRINTER'
        if GETINI('PRINTERS',clip(upper(p2)),'',path()&'\conweb.ini') <> ''
           Printer{PROPPRINT:Device} = GETINI('PRINTERS',clip(upper(p2)),'',path()&'\conweb.ini')
           temp" = 'Found printer'
        ELSE !If getini failed
            temp" = 'Fetch printer failed'
           Printer{PROPPRINT:Device} = DefaultPrinter
        END
!        message('Sent by print routine '&clip(p2)&'|Action : '&temp"&'|Printer set to '&printer{propprint:device} )
  of 'SETPRINTERCOPIES'
    Printer{propprint:copies} = p2

  of 'PRINTINVOICE'
        print_local_invoice
        !Message('Printed')
    Of 'OPENHELP'
!        !Add the Help URL to the remote ini (DBH: 15-10-2004 )
!        PUTINI('ONLINEHELP','URL',Sub(Clip(p2),1,Len(Clip(p2)) - 6),CLIP(Path()) & '\conweb.ini')
!        !Extract The SRN Number (DBH: 15-10-2004 )
!        HelpBrowser(Sub(Clip(p2),Len(Clip(p2)) - 5,6))
        HelpBrowser(Clip(p2)) ! ! Will now be passing the full URL (Bryan: 02/06/2011)
  of 'CLOSESPLASH'
        PUTINI('STARTING','Run',True,)
  Of 'OPENURL'
    AssocFile = Clip(p2)
    Param = ''
    Dir = ''
    Operation = 'Open'
    ShellExecute(GetDesktopWindow(), Operation, AssocFile, Param, Dir, 5)
  ELSE
        !Request not recognised
        MEssage('Unrecognised client command received.|This client may be out of date||Please contact immediately quoting|'&p2,'Client Warning',icon:hand)
 end !Case of P1

!end Case selection
!---
!--- Place an OMIT('!END of ClarioNET') in your EMBED code (above) if you do not
!--- want the Default Behavior
!---
 LOC:ReturnString = 'Successful'
 RETURN(LOC:ReturnString)
!END of ClarioNET
!------------------------------------------------------------------------------

!* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

LinePrint FUNCTION(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>)                ! LinePrint Function

   CODE                                                                                 ! Fuction Code Starts Here
    IF OMITTED(2)                                                                       ! If Device Name is Omitted
       IF SUB(PRINTER{07B29H},1,2) = '\\' 
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})))               ! Use Default Device
       ELSE
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})) - 1)           ! Use Default Device
       END 
    ELSE                                                                                ! Otherwise
       lpszFilename = CLIP(DeviceName)                                                  ! Use passed Device Name
    END                                                                                 ! Terminate IF

    IF (OMITTED(3) OR CRLF = True) AND CLIP(StringToPrint) <> FF                        ! If CRLF parameter is set or omitted and user did not pass FF
        hpvBuffer = StringToPrint & CR & LF                                             ! Print String with CR & LF
    ELSE                                                                                ! Otherwise
       hpvBuffer = StringToPrint                                                        ! Print Text As Is
    END                                                                                 ! Terminate IF

    cbBuffer = LEN(CLIP(hpvBuffer))                                                     ! Check Length of the Data to be Printed

     hf = OpenFile(lpszFilename,OF_STRUCT,OF_WRITE)                                     ! Open file and obtain file handle
     IF hf = -1                                                                         ! If File does not exist
      hf = OpenFile(lpszFilename,OF_STRUCT,OF_CREATE)                                   ! Create file and obtain file handle
      IF hf = -1 THEN RETURN(OpenError).                                                ! If Error then return OpenError
     END                                                                                ! Terminate IF

    IF SUB(lpszFilename,1,3) <> 'COM' AND |                                             ! If user prints to a file
       SUB(lpszFilename,1,3) <> 'LPT' AND |                                            
       SUB(lpszFilename,1,2) <> '\\'
       IF _llseek(hf,0,2) = -1 THEN RETURN(4).                                          ! Set file pointer to the end of the file (Append lines)
    END                                                                                 ! Terminate IF

    BytesWritten = _lwrite(hf,hpvBuffer,cbBuffer)                                       ! Write to the file
    IF BytesWritten < cbBuffer THEN RETURN(WriteError).                                 ! IF Writing to a device or file is not possible, return Write Error
    IF _lclose(hf) THEN RETURN(CloseError) ELSE RETURN(Succeeded).                      ! If error in closing device or a file, return CloseError otherwise return Succeeded


DeleteFile FUNCTION(STRING FileToDelete)                                                ! DeleteFile Function

    CODE                                                                                ! Function Code Starts Here
    lpszFilename = CLIP(FileToDelete)                                                   ! Put file name in the buffer
    hf = OpenFile(lpszFilename,OF_STRUCT,OF_DELETE)                                     ! and delete it
     IF hf = -1 THEN RETURN(OpenError) ELSE RETURN(Succeeded).                          ! If error, return OpenError otherwise Return Succeeded

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
BHStripReplace            Procedure(String func:String,String func:Strip,String func:Replace)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1
    StripLength#    = Len(func:Strip)

    func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

       !Exception for space

        IF SUB(func:String,STR_POS#,StripLength#) = func:Strip
            If func:Replace <> ''
                func:String = SUB(func:String,1,STR_POS#-1) & func:Replace & SUB(func:String,STR_POS#+StripLength#,STR_LEN#-1)

            Else !If func:Replace <> ''
                func:String = SUB(func:String,1,STR_POS#-1) &  SUB(func:String,STR_POS#+StripLength#,STR_LEN#-1)

            End !If func:Replace <> ''

        End
    End
    RETURN(func:String)
BHStripNonAlphaNum      Procedure(String func:String,String func:Replace)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1

    func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

       !Exception for space

     IF VAL(SUB(func:string,STR_POS#,1)) < 32 Or VAL(SUB(func:string,STR_POS#,1)) > 126 Or |
        VAL(SUB(func:string,STR_POS#,1)) = 34 Or VAL(SUB(func:string,STR_POS#,1)) = 44
           func:String = SUB(func:String,1,STR_POS#-1) & func:Replace & SUB(func:String,STR_POS#+1,STR_LEN#-1)
        End
    End
    RETURN(func:String)
BHReturnDaysHoursMins       Procedure(Long func:TotalMins,*Long func:Days,*Long func:Hours,*Long func:Mins)
Code
    func:Hours = 0
    func:Days = 0
    func:Mins = func:TotalMins
    If func:Totalmins >= 60
        Loop Until func:Mins < 60
            func:Mins -= 60
            func:Hours += 1
            If func:Hours > 23
                func:Days += 1
                func:Hours = 0
            End !If func:Hours > 23
        End !Loop Until local:MinutesLeft < 60
    End !If func:Minutes > 60
BHTimeDifference24Hr        Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        local:StartTime   = Deformat('00:00:00',@t4)
        local:EndTime     = Deformat('23:59:59',@t4)

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2

        local:2a = 0
        local:2b = 0
        local:2c = 0
        local:2d = 0

        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)

BHTimeDifference24Hr5Days    Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        local:StartTime   = Deformat('00:00:00',@t4)
        local:EndTime     = Deformat('23:59:59',@t4)

        If (x# % 7) = 0
            Cycle
        End
        If (x# % 7) = 6
            Cycle
        End

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2

        local:2a = 0
        local:2b = 0
        local:2c = 0
        local:2d = 0

        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)

BHTimeDifference24Hr5DaysMonday    Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime,String func:MondayStart)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        If (x# % 7) = 1
            local:StartTime   = Deformat(Clip(func:MondayStart),@t4)
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)
        Else !If (x# % 7) = 1
            local:StartTime   = Deformat('00:00:00',@t4)
            local:2a = 0
            local:2b = 0
            local:2c = 0
            local:2d = 0
        End !If (x# % 7) = 1

        local:EndTime     = Deformat('23:59:59',@t4)

        If (x# % 7) = 0
            Cycle
        End
        If (x# % 7) = 6
            Cycle
        End

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2



        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)
BHGetFileFromPath       Procedure(String f:Path)
Code
     Loop x# = Len(f:Path) To 1 By -1
        If Sub(f:Path,x#,1) = '\'
            Return Clip(Sub(f:Path,x# + 1,255))
            Break
        End ! If Sub(f:Path,x#,1) = '\'
    End ! Loop x# = Len(f:Path) To 1 By -1

BHGetPathFromFile       Procedure(String f:Path)
Code
    Loop x# = Len(f:Path) To 1 By -1
        If Sub(f:Path,x#,1) = '\'
            Return Clip(Sub(f:Path,1,x#))
            Break
        End ! If Sub(f:Path,x#) = '\'
    End ! Loop x# = 1 To Len(f:Path) To 1 By -1

BHGetFileNoExtension    PROCEDURE(STRING fullPath)
count LONG()
locFilename                                             STRING(255)
    code
        locFilename = BHGetFileFromPath(fullPath)
        LOOP count = LEN(CLIP(locFilename)) TO 1 BY -1
            IF (SUB(locFilename,count,1) = '.')
                locFilename = CLIP(SUB(locFilename,1,count - 1))
                BREAK
            END
        END
        RETURN CLIP(locFilename)

BHAddBackSlash              Procedure(STRING fullPath)
ReturnPath  STRING(255)
    code
        ReturnPath = fullPath
        IF (SUB(CLIP(fullPath),-1,1) <> '\')
            ReturnPath = CLIP(fullPath) & '\'
        END
        RETURN CLIP(ReturnPath)

BHRunDOS                Procedure(String f:Command,<f:Wait>,<f:NoTimeOut>)
NORMAL_PRIORITY_CLASS             equate(00000020h)
CREATE_NO_WINDOW                  equate(08000000h)

!STILL_ACTIVE          equate(259)

ProcessExitCode       ulong
CommandLine           cstring(1024)

StartUpInfo           group
Cb                      ulong
lpReserved              ulong
lpDesktop               ulong
lpTitle                 ulong
dwX                     ulong
dwY                     ulong
dwXSize                 ulong
dwYSize                 ulong
dwXCountChars           ulong
dwYCountChars           ulong
dwFillAttribute         ulong
dwFlags                 ulong
wShowWindow             signed
cbReserved2             signed
lpReserved2             ulong
hStdInput               unsigned
hStdOutput              unsigned
hStdError               unsigned
                      end

ProcessInformation    group
hProcess                unsigned
hThread                 unsigned
dwProcessId             ulong
dwThreadId              ulong
                      end
Code
    CommandLine = clip(f:Command)
    StartUpInfo.Cb = size(StartUpInfo)

    ReturnCode# = DBGCreateProcess(0,                                             |
                                address(CommandLine),                          |
                                0,                                             |
                                0,                                             |
                                0,                                             |
                                BOR(NORMAL_PRIORITY_CLASS, CREATE_NO_WINDOW),  |
                                0,                                             |
                                0,                                             |
                                address(StartUpInfo),                          |
                                address(ProcessInformation))

    if ReturnCode# = 0  then
        ! Error Return
        return false
    end

    timeout# = clock() + (f:Wait * 100)
    setcursor(cursor:wait)

    if f:Wait > 0 or f:NoTimeOut > 0 then
        loop
            DBGSleep(100)
            ! check for when process is finished
            ReturnCode# = DBGGetExitCodeProcess(ProcessInformation.hProcess, address(ProcessExitCode))
            if (ProcessExitCode <> 259)  ! 259 = Still Active
                break
            end
            if (f:NoTimeOut <> 1)
                if (clock() < timeout#)
                    break
                end
            end
        end
    end
    setcursor
    return(true)
FillCpcsIniStrings      PROCEDURE(CpcsIniFile,WindowMessage,PreviewDialogTitle,PreviewDialogText)
IniToUse      CSTRING(63)
  CODE
  IF CLIP(CpcsIniFile) <> ''
    IniToUse = CLIP(CpcsIniFile)
  ELSE
    IniToUse = 'WIN.INI'
  END
  CPCS:ProgWinTitlePrvw =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinTitlePrvw',       'Generating Report', IniToUse)
  CPCS:ProgWinTitlePrnt =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinTitlePrnt',       'Printing Report', IniToUse)
  CPCS:ProgWinPctText   =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinPctText',         '% Completed', IniToUse)
  CPCS:ProgWinRecText   =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinRecText',         'Records Read', IniToUse)
  CPCS:ProgWinUsrText   =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinUsrText',         WindowMessage, IniToUse)
  CPCS:PrtrDlgTitle     =  GETINI('DISPLAY STRINGS 2.0', 'PrinterDlgTitle',        'Report Destination', IniToUse)
  CPCS:AskPrvwDlgTitle  =  GETINI('DISPLAY STRINGS 2.0', 'AskPrvwDialogTitle',     PreviewDialogTitle, IniToUse)
  CPCS:AskPrvwDlgText   =  GETINI('DISPLAY STRINGS 2.0', 'AskPrvwDialogText',      PreviewDialogText, IniToUse)
  CPCS:AreYouSureTitle  =  GETINI('DISPLAY STRINGS 2.0', 'CancelAreYouSureTitle',  'Question', IniToUse)
  CPCS:AreYouSureText   =  GETINI('DISPLAY STRINGS 2.0', 'CancelAreYouSureText',   'Are you sure you want to CANCEL this Report?', IniToUse)
  CPCS:PrvwPartialTitle =  GETINI('DISPLAY STRINGS 2.0', 'CancelPrvwPartialTitle', 'Question', IniToUse)
  CPCS:PrvwPartialText  =  GETINI('DISPLAY STRINGS 2.0', 'CancelPrvwPartialText',  'Report Cancellation Requested!||Preview Report generated so far?', IniToUse)
  CPCS:NoDfltPrtrTitle  =  GETINI('DISPLAY STRINGS 2.0', 'NoDfltPrtrTitle',        'WARNING!', IniToUse)
  CPCS:NoDfltPrtrText   =  GETINI('DISPLAY STRINGS 2.0', 'NoDfltPrtrText',         'Warning:  No DEFAULT Printer Driver Found!||Printing may not occur correctly (or at all)|without a Default printer assigned.||Use CONTROL PANEL (PRINTERS) to set a|printer as your Default.||Continue with report anyway?', IniToUse)
  CPCS:NthgToPrvwTitle  =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrvwTitle',     'NOTE...', IniToUse)
  CPCS:NthgToPrvwText   =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrvwText',      'Nothing to Preview.', IniToUse)
  CPCS:NthgToPrntTitle  =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrntTitle',     'NOTE...', IniToUse)
  CPCS:NthgToPrntText   =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrntText',      'Nothing to Print.', IniToUse)
  CPCS:AsciiOutTitle    =  GETINI('DISPLAY STRINGS 2.0', 'AsciiOutTitle',          'Save to what Filename?', IniToUse)
  CPCS:AsciiOutMasks    =  GETINI('DISPLAY STRINGS 2.0', 'AsciiOutMasks',          'Text|*.TXT|All Files|*.*', IniToUse)
  CPCS:DynLblErrTitle   =  GETINI('DISPLAY STRINGS 2.0', 'DynLblErrTitle',         'Problem!', IniToUse)
  CPCS:DynLblErrText1   =  GETINI('DISPLAY STRINGS 2.0', 'DynLblErrText1',         'Sorry!||', IniToUse)
  CPCS:DynLblErrText2   =  GETINI('DISPLAY STRINGS 2.0', 'DynLblErrText2',         '||Does not support paper type:', IniToUse)




