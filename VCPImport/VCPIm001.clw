

   MEMBER('VCPImport.clw')                            ! This is a MEMBER module

                     MAP
                       INCLUDE('VCPIM001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
ConnectionString     STRING(255),STATIC
!J - 26/06/15 - tried swapping the "Long" definitions for Record_number and Ref_number in both these for a real.

TAUDIT    File,Driver('Scalable'),OEM,OWNER(ConnectionString),NAME('TAUDIT'),PRE(vaud),BINDABLE,THREAD
Record              Record
record_number            real,Name('record_number')
ref_Number               real,Name('Ref_Number')
date                     Date,Name('Date')
time                     Time,Name('Time')
user                     String(3),Name('User')
action                   String(80),Name('Action')
notes                    String(255),Name('Notes')
type                     String(3),Name('Type')
                    End
            End

TAUDSTATS   File,Driver('Scalable'),OEM,OWNER(ConnectionString),NAME('TAUDSTATS'),PRE(vaus),BINDABLE,THREAD
Record              Record
recordnumber             Real,Name('recordnumber')
refNumber                Real,Name('RefNumber')
type                     String(3),Name('Type')
dateChanged              Date,Name('DateChanged')
timeChanged              Time,Name('TimeChanged')
oldStatus                String(30),Name('OldStatus')
newStatus                String(30),Name('NewStatus')
userCode                 String(3),Name('UserCode')
                    End
            End
  CODE
    ConnectionString = getini('DEFAULTS', 'Connection', '', path() & '\VCPImport.ini')

    open(TAUDIT)
    if (errorcode())
        stop('Error Opening TAUDIT: ' & fileErrorCode() & ' - ' & fileError())
        Return
    end

    open(TAUDSTATS)
    if (errorcode())
        stop('Error Opening TAUDSTATS: ' & fileErrorCode() & ' - ' & fileError())
        Return
    end


    do ImportJobs

    close(TAUDIT)
    close(TAUDSTATS)

importJobs      Routine
Data
RecordQueue         Queue()
recordNumber            Long()
                    End
Code

    Free(RecordQueue)

    relate:AUDIT.Open()
    relate:AUDIT2.Open()
    TAUDIT{prop:SQL} = 'SELECT record_number,"Ref_Number","Date","Time","User","Action","Notes","Type" FROM TAUDIT'
    loop
        next(TAUDIT)
        if (error())
            break
        end ! if (error())

        Access:AUDIT.Clearkey(aud:DateTypeActionKey)
        aud:Date    = vaud:Date
        aud:Type    = vaud:Type
        aud:Action    = vaud:Action
        aud:Ref_Number    = vaud:Ref_Number
        if (Access:AUDIT.TryFetch(aud:DateTypeActionKey) = Level:Benign)
            ! Found
            ! Already Been Inserted
            RecordQueue.recordNumber = vaud:Record_Number
            Add(RecordQueue)
        else ! if (Access:AUDIT.TryFetch(aud:DateTypeActionKey) = Level:Benign)
            ! Error

            if (Access:AUDIT.PrimeRecord() = Level:Benign)
                aud:Ref_Number    = vaud:Ref_Number
                aud:Date    = vaud:Date
                aud:Time    = vaud:Time
                aud:User    = vaud:User
                aud:Action    = vaud:Action
                !aud:Notes   = vaud:Notes
                aud:Type    = vaud:Type
                if (Access:AUDIT.TryInsert() = Level:Benign)
                    ! Inserted

                    IF (Access:AUDIT2.PrimeRecord() = Level:Benign)
                        aud2:AUDRecordNumber = aud:Record_Number
                        aud2:Notes = vaud:Notes
                        IF (Access:AUDIT2.TryInsert())
                            Access:AUDIT2.CancelAutoInc()
                        END ! IF
                    END ! IF

                    RecordQueue.recordNumber = vaud:Record_Number
                    Add(RecordQueue)
                else ! if (Access:AUDIT.TryInsert() = Level:Benign)
                    ! Error
                    Access:AUDIT.CancelAutoInc()
                end ! if (Access:AUDIT.TryInsert() = Level:Benign)
            end ! if (Access:AUDIT.PrimeRecord() = Level:Benign)
        end ! if (Access:AUDIT.TryFetch(aud:DateTypeActionKey) = Level:Benign)

    end !loop

    relate:AUDIT.Close()
    relate:AUDIT2.Close()

    loop x# = 1 to records(RecordQueue)
        get(RecordQueue,x#)
        TAUDIT{prop:SQL} = 'DELETE FROM TAUDIT WHERE record_number = ' & RecordQueue.recordNumber
        Next(TAUDIT)
    end ! loop x# = 1 to records(RecordQueue)

    Free(RecordQueue)

    relate:AUDSTATS.Open()
    TAUDSTATS{prop:SQL} = 'select recordnumber, refnumber, "type", "datechanged","timechanged","oldstatus","newstatus","usercode" FROM TAUDSTATS'
    loop
        next(TAUDSTATS)
        if (error())
            break
        end !if (error())

        cont# = 1

        Access:AUDSTATS.Clearkey(aus:StatusTimeKey)
        aus:RefNumber    = vaus:RefNumber
        aus:Type    = vaus:Type
        aus:DateChanged    = vaus:DateChanged
        aus:TimeChanged    = vaus:TimeChanged
        if (Access:AUDSTATS.TryFetch(aus:StatusTimeKey) = Level:Benign)
            ! Found
            if (aus:NewStatus = vaus:NewStatus)
                RecordQueue.recordNumber = vaus:RecordNumber
                Add(RecordQueue)
                cont# = 0
            end ! if (aus:NewStatus = vaus:NewStatus)
        else ! if (Access:AUDSTATS.TryFetch(aus:StatusTimeKey) = Level:Benign)
            ! Error
        end ! if (Access:AUDSTATS.TryFetch(aus:StatusTimeKey) = Level:Benign)

        if (cont# = 1)
            if (Access:AUDSTATS.PrimeRecord() = Level:Benign)
                aus:RefNumber    = vaus:RefNumber
                aus:Type    = vaus:Type
                aus:DateChanged    = vaus:DateChanged
                aus:TimeChanged    = vaus:TimeChanged
                aus:OldStatus    = vaus:OldStatus
                aus:NewStatus    = vaus:NewStatus
                aus:UserCode    = vaus:UserCode
                if (Access:AUDSTATS.TryInsert() = Level:Benign)
                    ! Inserted
                    RecordQueue.recordNumber = vaus:RecordNumber
                    Add(RecordQueue)
                else ! if (Access:AUDSTATS.TryInsert() = Level:Benign)
                    ! Error
                    Access:AUDSTATS.CancelAutoInc()
                end ! if (Access:AUDSTATS.TryInsert() = Level:Benign)
            end ! if (Access:AUDSTATS.PrimeRecord() = Level:Benign)
        end ! if (cont# = 1)
    end ! loop

    relate:AUDSTATS.Close()

    loop x# = 1 to records(RecordQueue)
        get(RecordQueue,x#)
        TAUDIT{prop:SQL} = 'DELETE FROM TAUDSTATS WHERE recordnumber = ' & RecordQueue.recordNumber
        Next(TAUDSTATS)
    end ! loop x# = 1 to records(RecordQueue)
