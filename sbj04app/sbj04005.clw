

   MEMBER('sbj04app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ04005.INC'),ONCE        !Local module procedure declarations
                     END


CreateNewJob         PROCEDURE  (f:ExchangeBookingGroup) ! Declare Procedure
  CODE
    glo:Insert_Global = 'YES'
    Clear(wob:Record)
    Clear(job:Record)

    Update_Jobs(0,f:ExchangeBookingGroup)

    ! Inserting (DBH 14/07/2006) # 7975 - What reports are required to be printed?
    If GlobalResponse = 1
        ! Job has booked correctly (DBH: 14/07/2006)
        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            Case glo:Pointer
            Of 'THERMAL LABEL'
                glo:Select1 = job:Ref_Number
                If job:Bouncer = 'B'
                    Thermal_Labels('SE')
                Else ! If job:Bouncer = 'B'
                    If ExchangeAccount(job:Account_Number)
                        Thermal_Labels('ER')
                    Else ! If ExchangeAccount(job:Account_Number)
                        Thermal_Labels('')
                    End ! If ExchangeAccount(job:Account_Number)
                End ! If job:Bouncer = 'B'
                glo:Select1 = ''
            Of 'JOB CARD'
                glo:Select1 = job:Ref_Number
                Job_Card
                glo:Select1 = ''
            Of 'RETAINED ACCESSORIES LABEL'
                glo:Select1 = job:Ref_Number
                Job_Retained_Accessories_Label
                glo:Select1 = ''
            Of 'RECEIPT'
                glo:Select1 = job:Ref_Number
                Job_Receipt
                glo:Select1 = ''
            End ! Case glo:Pointer
        End ! Loop x# = 1 To Records(glo:Queue)

    End ! If GlobalReponse = 1
OutOfBoxFailure_New PROCEDURE (f:ValidationString,f:DisableDOP) !Generated from procedure template - Window

tmp:Pass             BYTE(0)
tmp:HandsetIMEINumber STRING(30)
tmp:BoxIMEINumber    STRING(30)
tmp:Network          STRING(30)
tmp:PurchaseDate     DATE
tmp:ReturnDate       DATE
tmp:TalkTime         REAL
tmp:OriginalDealer   STRING(255)
tmp:BranchOfReturn   STRING(30)
tmp:ProofOfPurchase  BYTE(0)
tmp:OriginalPackaging BYTE(0)
tmp:OriginalAccessories BYTE(0)
tmp:OriginalManuals  BYTE(0)
tmp:PhysicalDamage   BYTE(0)
tmp:Replacement      BYTE(0)
tmp:LAccountNumber   STRING(30)
tmp:ReplacementIMEINumber STRING(30)
tmp:StoreReferenceNumber STRING(30)
tmp:ValidationString STRING('Validating... {242}')
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Handset I.M.E.I. Number'),AT(68,95),USE(?tmp:HandsetIMEINumber:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(164,95,124,10),USE(tmp:HandsetIMEINumber),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Handset I.M.E.I. Number'),TIP('Handset I.M.E.I. Number'),REQ,UPR,READONLY
                           PROMPT('Box I.M.E.I. Number'),AT(68,110),USE(?tmp:BoxIMEINumber:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(164,110,124,10),USE(tmp:BoxIMEINumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Box I.M.E.I. Number'),TIP('Box I.M.E.I. Number'),REQ,UPR
                           PROMPT('Network'),AT(68,124),USE(?tmp:Network:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(164,124,124,10),USE(tmp:Network),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Network'),TIP('Network'),REQ,UPR
                           BUTTON,AT(288,119),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Purchase Date'),AT(68,138),USE(?tmp:PurchaseDate:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(164,138,64,10),USE(tmp:PurchaseDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Purchase Date'),TIP('Purchase Date'),REQ,UPR
                           PROMPT('Return Date'),AT(68,151),USE(?tmp:ReturnDate:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(164,151,64,10),USE(tmp:ReturnDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Return Date'),TIP('Return Date'),REQ,UPR
                           PROMPT('Talk Time'),AT(68,166),USE(?tmp:TalkTime:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14),AT(164,166,64,10),USE(tmp:TalkTime),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Talk Time'),TIP('Talk Time'),UPR
                           PROMPT('Original Dealer (Name and Address)'),AT(68,180,80,26),USE(?tmp:OriginalDealer:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(164,180,124,44),USE(tmp:OriginalDealer),VSCROLL,LEFT,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Original Dealer'),TIP('Original Dealer'),REQ,UPR
                           PROMPT('Branch Of Return'),AT(68,230),USE(?tmp:BranchOfReturn:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(164,230,124,10),USE(tmp:BranchOfReturn),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Branch Of Return'),TIP('Branch Of Return'),REQ,UPR
                           PROMPT('Store Reference Number (CCV, RTV, Repair Order No)'),AT(68,244,96,32),USE(?tmp:StoreReferenceNumber:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(164,244,124,10),USE(tmp:StoreReferenceNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Store Reference Number'),TIP('Store Reference Number'),UPR
                           OPTION('Proof Of Purchase'),AT(320,90,88,24),USE(tmp:ProofOfPurchase),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Proof Of Purchase'),TIP('Proof Of Purchase')
                             RADIO('Yes'),AT(332,100),USE(?tmp:ProofOfPurchase:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('No'),AT(372,100),USE(?tmp:ProofOfPurchase:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           OPTION('Original Packaging'),AT(320,116,88,24),USE(tmp:OriginalPackaging),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Original Packaging'),TIP('Original Packaging')
                             RADIO('Yes'),AT(332,126),USE(?tmp:OriginalPackaging:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('No'),AT(372,126),USE(?tmp:OriginalPackaging:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           OPTION('Original Accessories'),AT(320,142,88,24),USE(tmp:OriginalAccessories),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Original Accessories'),TIP('Original Accessories')
                             RADIO('Yes'),AT(332,151),USE(?tmp:OriginalAccessories:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('No'),AT(372,151),USE(?tmp:OriginalAccessories:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           OPTION('Original Manuals'),AT(320,167,88,24),USE(tmp:OriginalManuals),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Original Manuals'),TIP('Original Manuals')
                             RADIO('Yes'),AT(332,178),USE(?tmp:OriginalManuals:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('No'),AT(372,178),USE(?tmp:OriginalManuals:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           OPTION('Physical Damage'),AT(320,194,88,24),USE(tmp:PhysicalDamage),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Physical Damage'),TIP('Physical Damage')
                             RADIO('Yes'),AT(332,204),USE(?tmp:PhysicalDamage:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('No'),AT(372,204),USE(?tmp:PhysicalDamage:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           OPTION('Replacement'),AT(320,220,88,36),USE(tmp:Replacement),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Replacement'),TIP('Replacement')
                             RADIO('Yes'),AT(332,236),USE(?tmp:Replacement:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('No'),AT(372,236),USE(?tmp:Replacement:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           GROUP,AT(404,223,208,32),USE(?Group:Replacement),DISABLE
                             PROMPT('L/Account Number'),AT(412,226),USE(?tmp:LAccountNumber:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(488,226,124,10),USE(tmp:LAccountNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('L/Account Number'),TIP('L/Account Number'),REQ,UPR
                             PROMPT('Replacement I.M.E.I. Number'),AT(412,238,68,18),USE(?tmp:ReplacementIMEINumber:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(488,242,124,10),USE(tmp:ReplacementIMEINumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Replacement I.M.E.I. Number'),TIP('Replacement I.M.E.I. Number'),REQ,UPR
                           END
                         END
                       END
                       PROMPT('Validation Details:'),AT(194,294),USE(?tmp:ValidationString:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       TEXT,AT(194,306,285,32),USE(tmp:ValidationString),VSCROLL,LEFT,FONT(,,COLOR:Blue,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Validation String'),TIP('Validation String'),READONLY
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Out Of Box Failure'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(544,366),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(544,366),USE(?OK),TRN,FLAT,HIDE,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:Network                Like(tmp:Network)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Pass)

ValidateOBF     Routine
    tmp:ValidationString = ''

    If tmp:ReturnDate > 0 And tmp:PurchaseDate > 0
        If tmp:ReturnDate > tmp:PurchaseDate + 7
            tmp:ValidationString = 'Handset not returned within 7 days of purchase.'
        End ! If tmp:ReturnDate > tmp:PurchaseDate + 7
    End ! If tmp:ReturnDate > 0 And tmp:PurchaseDate > 0

    If tmp:ProofOfPurchase = 2
        tmp:ValidationString = 'Handset Proof Of Purchase Missing.'
    End ! If tmp:ProofOfPurchase = 2

    If tmp:OriginalPackaging = 2
        tmp:ValidationString = 'Handset Original Packaging Missing.'
    End ! If tmp:OriginalPackaging = 2

    If tmp:OriginalAccessories = 2
        tmp:ValidationString = 'Original Accessories Missing.'
    End ! If tmp:OriginalAccessories = 2

    If tmp:OriginalManuals = 2
        tmp:ValidationString = 'Original Operating Manuals Missing.'
    End ! If tmp:OriginalManuals = 2

    If Clip(tmp:HandsetIMEINumber) <> '' And Clip(tmp:BoxIMEINumber) <> '' And tmp:HandsetIMEINumber <> tmp:BoxIMEINumber
        tmp:ValidationString = 'IMEI Number on handset does not match IMEI Number on the box.'
    End ! If tmp:HandsetIMEINumber <> '' And tmp:HandsetIMEINumber <> tmp:BoxIMEINumber

    If tmp:PhysicalDamage = 1
        tmp:ValidationString = 'Handset has signs of physical damage.'
    End ! If tmp:PhysicalDamage = 2

    If Clip(tmp:Network) <> '' And tmp:Network <> '082 VODACOM'
        tmp:ValidationString = 'Network error: Handset not supplied by the Vodacom Service Provider within the last 12 months.'
    End ! If Clip(tmp:Network) <> '' And tmp:Network <> '082 VODACOM'

    If Clip(tmp:ValidationString) <> ''
        tmp:Pass = 0
        f:ValidationString = Upper(tmp:ValidationString)
        tmp:ValidationString = 'Validation Rejected:<13,10>' & Clip(tmp:ValidationString)
        ?Sheet1{prop:Disable} = 1
        ?OK{prop:Hide} = 0
        ?Cancel{prop:Hide} = 1
        ?tmp:ValidationString{prop:FontColor} = color:Red
    Else ! If Clip(tmp:ValidationString) <> ''
        ?OK{prop:Hide} = 1
        ?Cancel{prop:Hide} = 0
        tmp:ValidationString = 'Validating...'
        ?tmp:ValidationString{prop:FontColor} = color:Blue
        If Clip(tmp:HandsetIMEINumber) <> '' And |
            Clip(tmp:BoxIMEINumber) <> '' ANd |
            Clip(tmp:Network) <> '' And |
            tmp:PurchaseDate <> 0 And |
            tmp:ReturnDate <> 0 ANd |
            Clip(tmp:OriginalDealer) <> '' ANd |
            Clip(tmp:BranchOfReturn) <> '' ANd |
            tmp:ProofOfPurchase <> 0 And |
            tmp:OriginalPackaging <> 0 And |
            tmp:OriginalAccessories <> 0 And |
            tmp:OriginalManuals <> 0 And |
            tmp:PhysicalDamage <> 0 And |
            (tmp:Replacement = 2 Or |
            (tmp:Replacement = 1 And (Clip(tmp:LAccountNumber) <> '' Or Clip(tmp:ReplacementIMEINumber) <> '')))
            tmp:ValidationString = 'Validation Accepted.'
            ?tmp:ValidationString{prop:FontColor} = color:Green
            ?OK{prop:Hide} = 0
            ?Cancel{prop:Hide} = 1
            tmp:Pass = 1
        End ! If Clip(tmp:HandsetIMEINumber) = '' Or |
    End ! If Clip(tmp:ValidationString) <> ''



    Display()


UpdateFields        Routine
    job:DOP = tmp:PurchaseDate
    jobe:Network = tmp:Network
    jobe:BoxESN = tmp:BoxIMEINumber
    jobe:ReturnDate = tmp:ReturnDate
    jobe:TalkTime = tmp:TalkTime

    If tmp:ProofOfPurchase = 2
        jobe:ValidPOP = 'NO'
    Else ! If tmp:ProofOfPurchase = 2
        jobe:ValidPOP = 'YES'
    End ! If tmp:ProofOfPurchase = 2

    If tmp:OriginalPackaging = 2
        jobe:OriginalPackaging = 0
    Else ! If tmp:OriginalPackaging = 2
        jobe:OriginalPackaging = 1
    End ! If tmp:OriginalPackaging = 2

    ! Accessories are not seperate therefore will use the Battery field to count for ALL accessories (DBH: 09/03/2007)
    If tmp:OriginalAccessories = 2
        jobe:OriginalBattery = 0
    Else ! If tmp:OriginalPackaging = 2
        jobe:OriginalBattery = 1
    End ! If tmp:OriginalPackaging = 2

    If tmp:OriginalManuals = 2
        jobe:OriginalManuals = 0
    Else ! If tmp:OriginalPackaging = 2
        jobe:OriginalManuals = 1
    End ! If tmp:OriginalPackaging = 2

    If tmp:PhysicalDamage = 2
        jobe:PhysicalDamage = 0
    Else ! If tmp:OriginalPackaging = 2
        jobe:PhysicalDamage = 1
    End ! If tmp:OriginalPackaging = 2

    If tmp:Replacement = 2
        jof:Replacement = 0
        jof:LAccountNumber = ''
        jof:ReplacementIMEI = ''
    Else ! If tmp:Replacement = 2
        jof:Replacement = 1
        jof:LAccountNumber = tmp:LAccountNumber
        jof:ReplacementIMEI = tmp:ReplacementIMEINumber
    End ! If tmp:Replacement = 2

    jobe:OriginalDealer = tmp:OriginalDealer
    jobe:BranchOfReturn  = tmp:BranchOfReturn
    jof:StoreReferenceNumber = tmp:StoreReferenceNumber
    job:Order_Number = tmp:StoreReferenceNumber
    Access:JOBSE.Update()
    Access:JOBSOBF.Update()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020510'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('OutOfBoxFailure_New')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBS.Open
  Relate:NETWORKS.Open
  Access:JOBSE.UseFile
  Access:JOBSOBF.UseFile
  SELF.FilesOpened = True
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  Access:JOBSOBF.ClearKey(jof:RefNumberKey)
  jof:RefNumber = job:Ref_Number
  If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
      !Error
      If Access:JOBSOBF.PrimeRecord() = Level:Benign
          jof:RefNumber = job:Ref_Number
          jof:IMEINumber = job:ESN
          jof:HeadAccountNumber = wob:HeadAccountNumber
          If Access:JOBSOBF.TryInsert() = Level:Benign
              !Insert
          Else ! If Access:JOBSOBF.TryInsert() = Level:Benign
              Access:JOBSOBF.CancelAutoInc()
          End ! If Access:JOBSOBF.TryInsert() = Level:Benign
      End ! If Access.JOBSOBF.PrimeRecord() = Level:Benign
  End ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
  
  
  tmp:HandsetIMEINumber = job:ESN
  tmp:Network = jobe:Network
  tmp:PurchaseDate = job:DOP
  tmp:StoreReferenceNumber = job:Order_Number
  
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Insert --- Disable DOP (DBH: 06/03/2009) #10614
  if (f:DisableDOP)
      ?tmp:PurchaseDate{prop:Disable} = 1
  end ! if (f:DisableDOP)
  ! end --- (DBH: 06/03/2009) #10614
  ! Save Window Name
   AddToLog('Window','Open','OutOfBoxFailure_New')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Network{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:Network{Prop:Tip}
  END
  IF ?tmp:Network{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:Network{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:NETWORKS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','OutOfBoxFailure_New')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickNetworks
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      Case Missive('Are you sure you want to cancel the OBF check?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              f:ValidationString = 'USER CANCELLED'
              tmp:Pass = 0
              Do UpdateFields
          Of 1 ! No Button
              Cycle
      End ! Case Missive
    OF ?OK
      ! Have all the fields been filled in? (DBH: 08/03/2007)
      If tmp:Pass = 1
          If Clip(tmp:HandsetIMEINumber) = '' Or |
              Clip(tmp:BoxIMEINumber) = '' Or |
              Clip(tmp:Network) = '' Or |
              tmp:PurchaseDate = 0 Or |
              tmp:ReturnDate = 0 Or |
              Clip(tmp:OriginalDealer) = '' Or |
              Clip(tmp:BranchOfReturn) = '' Or |
              tmp:ProofOfPurchase = 0 Or |
              tmp:OriginalPackaging = 0 Or |
              tmp:OriginalAccessories = 0 Or |
              tmp:OriginalManuals = 0 Or |
              tmp:PhysicalDamage = 0 Or |
              tmp:Replacement = 0 Or |
              (tmp:Replacement = 1 And (Clip(tmp:LAccountNumber) = '' Or Clip(tmp:ReplacementIMEINumber) = ''))
              Cycle
          End ! If Clip(tmp:HandsetIMEINumber) = '' Or |
      End ! If tmp:Pass = 0
      
      If Clip(tmp:LAccountNumber) <> '' And Sub(tmp:LAccountNUmber,1,1) <> 'L'
          Case Missive('The L/Account Number must begin with the letter "L".','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:LAccountNumber)
          Cycle
      End ! If Clip(tmp:LAccountNumber) <> '' And Sub(tmp:LAccountNUmber,1,1) <> 'L'
      
      Do UpdateFields
      
      Post(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:HandsetIMEINumber
        Do ValidateOBF
    OF ?tmp:BoxIMEINumber
        Do ValidateOBF
    OF ?tmp:Network
      IF tmp:Network OR ?tmp:Network{Prop:Req}
        net:Network = tmp:Network
        !Save Lookup Field Incase Of error
        look:tmp:Network        = tmp:Network
        IF Access:NETWORKS.TryFetch(net:NetworkKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Network = net:Network
          ELSE
            !Restore Lookup On Error
            tmp:Network = look:tmp:Network
            SELECT(?tmp:Network)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
        Do ValidateOBF
    OF ?CallLookup
      ThisWindow.Update
      net:Network = tmp:Network
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Network = net:Network
          Select(?+1)
      ELSE
          Select(?tmp:Network)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Network)
    OF ?tmp:PurchaseDate
        Do ValidateOBF
    OF ?tmp:ReturnDate
        Do ValidateOBF
    OF ?tmp:TalkTime
        Do ValidateOBF
    OF ?tmp:OriginalDealer
        Do ValidateOBF
    OF ?tmp:BranchOfReturn
        Do ValidateOBF
    OF ?tmp:StoreReferenceNumber
        Do ValidateOBF
    OF ?tmp:ProofOfPurchase
        Do ValidateOBF
    OF ?tmp:OriginalPackaging
        Do ValidateOBF
    OF ?tmp:OriginalAccessories
        Do ValidateOBF
    OF ?tmp:OriginalManuals
        Do ValidateOBF
    OF ?tmp:PhysicalDamage
        Do ValidateOBF
    OF ?tmp:Replacement
      Case tmp:Replacement
      Of 2
          ?Group:Replacement{prop:Disable} = 1
          tmp:LAccountNumber = ''
          tmp:ReplacementIMEINumber = ''
          Display()
      Of 1
          ?Group:Replacement{prop:Disable} = 0
      End ! Case tmp:Replacement
        Do ValidateOBF
    OF ?tmp:LAccountNumber
        Do ValidateOBF
    OF ?tmp:ReplacementIMEINumber
        Do ValidateOBF
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020510'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020510'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020510'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?tmp:ValidationString{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:ValidationString{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
SelectInvoiceCreditNote PROCEDURE (f:RefNumber,f:Type) !Generated from procedure template - Window

tmp:InvoiceNumber    STRING(30)
tmp:I                STRING('I')
tmp:C                STRING('C')
tmp:RefNumber        LONG
tmp:InvoiceType      BYTE(0)
tmp:Value            REAL
BRW8::View:Browse    VIEW(JOBSINV)
                       PROJECT(jov:NewTotalCost)
                       PROJECT(jov:InvoiceNumber)
                       PROJECT(jov:RecordNumber)
                       PROJECT(jov:RefNumber)
                       PROJECT(jov:Type)
                       PROJECT(jov:Suffix)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:InvoiceNumber      LIKE(tmp:InvoiceNumber)        !List box control field - type derived from local data
tmp:Value              LIKE(tmp:Value)                !List box control field - type derived from local data
jov:NewTotalCost       LIKE(jov:NewTotalCost)         !List box control field - type derived from field
jov:InvoiceNumber      LIKE(jov:InvoiceNumber)        !List box control field - type derived from field
jov:RecordNumber       LIKE(jov:RecordNumber)         !Primary key field - type derived from field
jov:RefNumber          LIKE(jov:RefNumber)            !Browse key field - type derived from field
jov:Type               LIKE(jov:Type)                 !Browse key field - type derived from field
jov:Suffix             LIKE(jov:Suffix)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::jov:RefNumber        LIKE(jov:RefNumber)
HK9::jov:Type             LIKE(jov:Type)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),BELOW,SPREAD
                         TAB,USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Select Invoice Type'),AT(232,115,216,178),USE(tmp:InvoiceType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Print Original Invoice'),AT(261,129),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Print "Credited" Invoice'),AT(261,142),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                         END
                         TAB,USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select Credit Note To Print'),AT(260,146),USE(?Prompt4),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       LIST,AT(260,164,152,118),USE(?List),IMM,DISABLE,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('93L(2)|M~Invoice Number~@s30@56R(2)|M~Value~@n-14.2@0R(2)|M~Value~@n-14.2@0R(2)|' &|
   'M~Link to INVOICE Invoice_Number~@s8@'),FROM(Queue:Browse)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Select Document To Print'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?Button3),TRN,FLAT,ICON('printp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW8::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet1) = 2
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

Printing        Routine
Data
local:Prefix        String(2)
local:Suffix        String(1)
Code
    Case f:Type
    Of 'I'
        Case tmp:InvoiceType
        Of 0
            local:Prefix = ''
            local:Suffix = ''
        Of 1
            Brw8.UpdateViewRecord()
            local:Prefix = ''
            local:Suffix = jov:Suffix
        End ! Case tmp:InvoiceType
    Of 'C'
        Brw8.UpdateViewRecord()
        local:Prefix = 'CN'
        local:Suffix = jov:Suffix
    End ! Case f:Type

! Changing (DBH 10/10/2007) # 9388 - Pass the invoice number
!    glo:Select1 = f:RefNumber
! to (DBH 10/10/2007) # 9388
    glo:Select1 = jov:InvoiceNumber
! End (DBH 10/10/2007) #9388
!    If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
!        Vodacom_Delivery_Note_Web(local:Prefix,local:Suffix)
!    Else ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
!        Single_Invoice(local:Prefix,local:Suffix)
!    End ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
    VodacomSingleInvoice(jov:InvoiceNumber,jov:RefNumber,local:Prefix,local:Suffix) ! #11881 New Single Invoice (Bryan: 16/03/2011)
    glo:Select1 = ''

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020688'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectInvoiceCreditNote')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBSINV.Open
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  tmp:RefNumber = f:RefNumber
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = Clarionet:Global.Param2
  If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      !Found
  Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:JOBSINV,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Case Clip(f:Type)
  Of 'I'
      ?Tab2{prop:Hide} = 1
  Of 'C'
      ?Tab1{prop:Hide} = 1
      ?List{prop:Disable} = 0
  End ! Case f:Type
  
  ! Save Window Name
   AddToLog('Window','Open','SelectInvoiceCreditNote')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,jov:TypeSuffixKey)
  BRW8.AddRange(jov:Type)
  BRW8.AddLocator(BRW8::Sort1:Locator)
  BRW8::Sort1:Locator.Init(,jov:Suffix,1,BRW8)
  BRW8.AddSortOrder(,jov:TypeSuffixKey)
  BRW8.AddRange(jov:Type)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,jov:Suffix,1,BRW8)
  BIND('tmp:InvoiceNumber',tmp:InvoiceNumber)
  BIND('tmp:Value',tmp:Value)
  BRW8.AddField(tmp:InvoiceNumber,BRW8.Q.tmp:InvoiceNumber)
  BRW8.AddField(tmp:Value,BRW8.Q.tmp:Value)
  BRW8.AddField(jov:NewTotalCost,BRW8.Q.jov:NewTotalCost)
  BRW8.AddField(jov:InvoiceNumber,BRW8.Q.jov:InvoiceNumber)
  BRW8.AddField(jov:RecordNumber,BRW8.Q.jov:RecordNumber)
  BRW8.AddField(jov:RefNumber,BRW8.Q.jov:RefNumber)
  BRW8.AddField(jov:Type,BRW8.Q.jov:Type)
  BRW8.AddField(jov:Suffix,BRW8.Q.jov:Suffix)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBSINV.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','SelectInvoiceCreditNote')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:InvoiceType
      If tmp:InvoiceType = 0 And f:Type = 'I'
          ?List{prop:Disable} = 1
      Else ! If tmp:InvoiceType = 0
          ?List{prop:Disable} = 0
      End ! If tmp:InvoiceType = 0
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020688'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020688'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020688'&'0')
      ***
    OF ?Button3
      ThisWindow.Update
      Do Printing
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?Sheet1) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:RefNumber
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:C
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:RefNumber
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:I
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW8.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet1) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW8.SetQueueRecord PROCEDURE

  CODE
  IF (jov:Type = 'I')
    tmp:InvoiceNumber = CLIP(jov:InvoiceNumber) & '-' & CLIP(tra:BranchIdentification) & CLIP(jov:Suffix)
  ELSE
    tmp:InvoiceNumber = 'CN' & CLIP(jov:InvoiceNumber) & '-' & CLIP(tra:BranchIdentification) & CLIP(jov:Suffix)
  END
  IF (jov:Type = 'I')
    tmp:Value = jov:NewTotalCost
  ELSE
    tmp:Value = jov:CreditAmount
  END
  PARENT.SetQueueRecord
  SELF.Q.tmp:InvoiceNumber = tmp:InvoiceNumber        !Assign formula result to display queue
  SELF.Q.tmp:Value = tmp:Value                        !Assign formula result to display queue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

IMEIHistoryKey PROCEDURE                              !Generated from procedure template - Window

window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PANEL,AT(266,156,12,10),USE(?Panel6),FILL(COLOR:Purple)
                       PROMPT('- N.F.F. / R.T.M. / R. N. R.'),AT(286,156),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(266,176,12,10),USE(?Panel6:2),FILL(COLOR:Black)
                       PROMPT('- Repair / Modification / Exchange'),AT(286,176),USE(?Prompt3:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(266,196,12,10),USE(?Panel6:3),FILL(COLOR:Fuschia)
                       PROMPT('- Excluded (3rd Party Repair)'),AT(286,196),USE(?Prompt3:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(266,216,12,10),USE(?Panel6:4),FILL(COLOR:Navy)
                       PROMPT('- Liquid Damage / B.E.R.'),AT(286,216),USE(?Prompt3:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(266,236,12,10),USE(?Panel6:5),FILL(COLOR:Red)
                       PROMPT('- Refurbished'),AT(286,236),USE(?Prompt3:5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('- Incomplete Jobs (Open)'),AT(286,256),USE(?Prompt3:6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(266,256,12,10),USE(?Panel6:6),FILL(COLOR:Green)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('IMEI History Key'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020707'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('IMEIHistoryKey')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','IMEIHistoryKey')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','IMEIHistoryKey')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020707'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020707'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020707'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ChangeDOP PROCEDURE (f:Date,f:NewDate,f:Reason)       !Generated from procedure template - Window

tmp:PreviousDOP      DATE
tmp:NewDOP           DATE
tmp:ChangeReason     STRING(255)
window               WINDOW('Change DOP'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Enter The New D.O.P. and your reason for changing it.'),AT(241,138),USE(?Prompt6),TRN,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Previous D.O.P.'),AT(220,184),USE(?tmp:PreviousDOP:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@d06b),AT(324,184,64,10),USE(tmp:PreviousDOP),SKIP,FONT(,,,FONT:bold),COLOR(COLOR:White),MSG('Previous D.O.P.'),TIP('Previous D.O.P.'),UPR,READONLY
                       PROMPT('New D.O.P.'),AT(220,210),USE(?tmp:NewDOP:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@d06b),AT(324,210,64,10),USE(tmp:NewDOP),FONT(,,,FONT:bold),COLOR(COLOR:White),MSG('New D.O.P.'),TIP('New D.O.P.'),REQ,UPR
                       BUTTON,AT(392,206),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('Reason for D.O.P. change'),AT(220,238),USE(?tmp:ChangeReason:Prompt),TRN,FONT(,,COLOR:White,FONT:bold)
                       TEXT,AT(324,238,124,60),USE(tmp:ChangeReason),COLOR(COLOR:White),MSG('Reason for D.O.P. change'),TIP('Reason for D.O.P. change'),REQ,UPR
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Change DOP'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Cancel),FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(376,332),USE(?OK),TRN,FLAT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020726'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ChangeDOP')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?OK,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  if (glo:WebJob)
      ?PopCalendar{prop:hide} = 1
  end ! if (glo:WebJob)
  
  tmp:PreviousDOP = f:Date
  ! Save Window Name
   AddToLog('Window','Open','ChangeDOP')
  Bryan.CompFieldColour()
  ?tmp:NewDOP{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','ChangeDOP')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      f:NewDate = 0
      f:Reason = ''
    OF ?OK
      if (tmp:NewDOP = 0 or tmp:NewDOP > Today())
          tmp:NewDOP = 0
          Display()
          select(?tmp:NewDOP)
          cycle
      end !if (tmp:NewDOP = 0)
      
      if (len(clip(tmp:ChangeReason)) < 3)
          select(?tmp:ChangeReason)
          cycle
      end ! if (tmp:ChangeReason = '')
      
      f:NewDate = tmp:NewDOP
      f:Reason = upper(tmp:ChangeReason)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:NewDOP = TINCALENDARStyle1(tmp:NewDOP)
          Display(?tmp:NewDOP)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020726'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020726'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020726'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:NewDOP
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
MobileNumberStatus PROCEDURE (fLifeValue,fSpend,fLoyalty,fUpgrade,fID) !Generated from procedure template - Window

locLifetimeValue     STRING('Information Unavailable {7}')
locAverageSpend      STRING('Information Unavailable {7}')
locLoyaltyStatus     STRING('Information Unavailable {7}')
locUpgradeDate       STRING('Information Unavailable {7}')
locIDNumber          STRING('Information Unavailable {7}')
window               WINDOW('Mobile Number Details'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Mobile Number Details'),AT(168,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('SRN:000000'),AT(468,70),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Lifetime Value'),AT(252,122),USE(?Prompt3),TRN,FONT(,10,COLOR:Yellow,FONT:bold,CHARSET:ANSI)
                           PROMPT('Average Spend'),AT(252,159),USE(?Prompt3:2),TRN,FONT(,10,COLOR:Yellow,FONT:bold,CHARSET:ANSI)
                           PROMPT('Loyalty Status'),AT(252,199),USE(?Prompt3:3),TRN,FONT(,10,COLOR:Yellow,FONT:bold,CHARSET:ANSI)
                           PROMPT('Upgrade Date'),AT(252,238),USE(?Prompt3:4),TRN,FONT(,10,COLOR:Yellow,FONT:bold,CHARSET:ANSI)
                           PROMPT('ID Number'),AT(252,271),USE(?Prompt3:5),TRN,FONT(,10,COLOR:Yellow,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(252,135),USE(locLifetimeValue),TRN,FONT(,10,,FONT:bold)
                           STRING(@s30),AT(252,174),USE(locAverageSpend),TRN,FONT(,10,,FONT:bold)
                           STRING(@s30),AT(252,252),USE(locUpgradeDate),TRN,FONT(,10,,FONT:bold)
                           STRING(@s30),AT(252,287),USE(locIDNumber),TRN,FONT(,10,,FONT:bold)
                           STRING(@s30),AT(252,215),USE(locLoyaltyStatus),TRN,FONT(,10,,FONT:bold)
                         END
                       END
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020736'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MobileNumberStatus')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  if (fLifeValue <> '')
      locLifetimeValue = fLifeValue
  end
  if (fSpend > 0)
      locAverageSpend = fSpend
  !    locAverageSpend = Round(fSpend,.01)
  end
  if (fLoyalty <> '')
      locLoyaltyStatus = fLoyalty
  end
  if (fUpgrade <> 0)
      locUpgradeDate = format(fUpgrade,@d06)
  end
  if (fID <> '')
      locIDNumber = fID
  end
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','MobileNumberStatus')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','MobileNumberStatus')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020736'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020736'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020736'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowsePreJob PROCEDURE                                !Generated from procedure template - Window

AllRegions           STRING('N')
CurrentRecion        STRING(30)
CurrentRecord        LONG
ErrorFound           BYTE
BRW9::View:Browse    VIEW(PREJOB)
                       PROJECT(PRE:RefNumber)
                       PROJECT(PRE:VodacomPortalRef)
                       PROJECT(PRE:Title)
                       PROJECT(PRE:Surname)
                       PROJECT(PRE:Address_Line1)
                       PROJECT(PRE:Suburb)
                       PROJECT(PRE:ESN)
                       PROJECT(PRE:Region)
                       PROJECT(PRE:JobRef_number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
PRE:RefNumber          LIKE(PRE:RefNumber)            !List box control field - type derived from field
PRE:VodacomPortalRef   LIKE(PRE:VodacomPortalRef)     !List box control field - type derived from field
PRE:Title              LIKE(PRE:Title)                !List box control field - type derived from field
PRE:Surname            LIKE(PRE:Surname)              !List box control field - type derived from field
PRE:Address_Line1      LIKE(PRE:Address_Line1)        !List box control field - type derived from field
PRE:Suburb             LIKE(PRE:Suburb)               !List box control field - type derived from field
PRE:ESN                LIKE(PRE:ESN)                  !List box control field - type derived from field
PRE:Region             LIKE(PRE:Region)               !List box control field - type derived from field
PRE:JobRef_number      LIKE(PRE:JobRef_number)        !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB10::View:FileDrop VIEW(REGIONS)
                       PROJECT(reg:Region)
                       PROJECT(reg:RecordNumber)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?CurrentRecion
reg:Region             LIKE(reg:Region)               !List box control field - type derived from field
reg:RecordNumber       LIKE(reg:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(650,4,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(66,41,552,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Vodacom Portal Pre-Booked Jobs'),AT(70,44),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(566,44),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(66,56,552,308),USE(?Panel3),FILL(09A6A7CH)
                       LIST,AT(72,68,124,10),USE(CurrentRecion),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),FORMAT('120L|M~Region~L(2)@s30@'),DROP(5),FROM(Queue:FileDrop)
                       CHECK('All Regions'),AT(200,68),USE(AllRegions),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                       ENTRY(@s30),AT(72,86,124,10),USE(PRE:VodacomPortalRef)
                       LIST,AT(72,100,524,230),USE(?List),IMM,MSG('Browsing Records'),FORMAT('0R(2)|M@n-14@97L(2)|M~Vodacom Portal Ref~@s30@20L(2)|M~Title~@s5@80L(2)|M~Surnam' &|
   'e~@s30@80L(2)|M~Address Line 1~@s30@80L(2)|M~Suburb~@s30@80L(2)|M~IMEI~@s20@120L' &|
   '(2)|M~Region~@s30@0R(2)|M@n-14@'),FROM(Queue:Browse)
                       BUTTON,AT(550,335),USE(?ButtonNewJob),TRN,FLAT,ICON('newjobp.jpg')
                       PANEL,AT(66,367,552,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(551,367),USE(?ButtonExit),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(484,367),USE(?ButtonExport),FLAT,ICON('exportp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  EntryLocatorClass                !Default Locator
FDB10                CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020784'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowsePreJob')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBS.Open
  Relate:PREJOB.Open
  Relate:REGIONS.Open
  Relate:TRADEAC2.Open
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:PREJOB,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowsePreJob')
  ?CurrentRecion{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  !before you open the files set up the local variables used to limit the browse.
      If glo:WebJob
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = ClarioNET:Global.Param2
          Access:TRADEACC.Tryfetch(tra:Account_Number_Key)
      else        
          Access:TRADEACC.Clearkey(tra:Account_Number_key)
          tra:Account_Number  = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
          Access:TRADEACC.Fetch(tra:Account_Number_key)
      END !if glo:webjob
  
  
      !the region to match is in TradeAc2
      Access:TradeAc2.clearkey(TRA2:KeyAccountNumber)
      TRA2:Account_Number = tra:Account_Number
      if access:TradeAc2.fetch(TRA2:KeyAccountNumber) = level:benign then
          CurrentRecion = TRA2:Pre_Book_Region
      ELSE
          CurrentRecion = ''
      END
  
  
      display(?CurrentREcion)
  
  
      if securitycheck('PREBOOK OUT OF REGION') = level:benign
          unhide(?AllRegions)
          unhide(?CurrentRecion)
      else
          hide(?AllRegions)
          hide(?CurrentRecion)
      end
  
  
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,PRE:KeyPortalRef)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(?PRE:VodacomPortalRef,PRE:VodacomPortalRef,1,BRW9)
  BRW9.AddField(PRE:RefNumber,BRW9.Q.PRE:RefNumber)
  BRW9.AddField(PRE:VodacomPortalRef,BRW9.Q.PRE:VodacomPortalRef)
  BRW9.AddField(PRE:Title,BRW9.Q.PRE:Title)
  BRW9.AddField(PRE:Surname,BRW9.Q.PRE:Surname)
  BRW9.AddField(PRE:Address_Line1,BRW9.Q.PRE:Address_Line1)
  BRW9.AddField(PRE:Suburb,BRW9.Q.PRE:Suburb)
  BRW9.AddField(PRE:ESN,BRW9.Q.PRE:ESN)
  BRW9.AddField(PRE:Region,BRW9.Q.PRE:Region)
  BRW9.AddField(PRE:JobRef_number,BRW9.Q.PRE:JobRef_number)
  FDB10.Init(?CurrentRecion,Queue:FileDrop.ViewPosition,FDB10::View:FileDrop,Queue:FileDrop,Relate:REGIONS,ThisWindow)
  FDB10.Q &= Queue:FileDrop
  FDB10.AddSortOrder()
  FDB10.AddField(reg:Region,FDB10.Q.reg:Region)
  FDB10.AddField(reg:RecordNumber,FDB10.Q.reg:RecordNumber)
  ThisWindow.AddItem(FDB10.WindowComponent)
  FDB10.DefaultFill = 0
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:PREJOB.Close
    Relate:REGIONS.Close
    Relate:TRADEAC2.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowsePreJob')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonNewJob
      !from the way Update_Jobs is organised you cannot insert by using an "insert" call
      
      !refetch the full Prejob record so this can be used in Update_jobs
      brw9.updatebuffer()     !    - gets the currently selected record from a browse
      CurrentRecord = PRE:RefNumber
      Access:PreJob.clearkey(PRE:KeyRefNumber)
      PRE:RefNumber = CurrentRecord
      If access:prejob.fetch(PRE:KeyRefNumber) then cycle.
      
      !TB13014 - check if the IMEI is currently on an open job
      ErrorFound = false
      Access:jobs.clearkey(job:ESN_Key)
      job:ESN = PRE:ESN
      Set(job:ESN_Key,job:ESN_Key)
      Loop
      
          if access:jobs.next() then break.
          if job:ESN <> PRE:ESN then break.
      
          !found one
          if job:Date_Completed = '' then
      
              Miss# = missive('A job cannot be created for this pre-booking.'&|
                              '|An incomplete job already exists with this IMEI Number.'&|
                              '|Job Number '&clip(job:Ref_Number),'ServiceBase 3g','MSTOP.jpg','OK')
              ErrorFound = true
              Break
      
          END
      
      END !Loop through jobs by ESN
      
      if ErrorFound then cycle.
      
      glo:insert_global = 'PRE'   !this should be "YES" but I want another trigger on it
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020784'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020784'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020784'&'0')
      ***
    OF ?CurrentRecion
      BRW9.resetsort(1)
    OF ?AllRegions
      BRW9.resetsort(1)
    OF ?ButtonNewJob
      ThisWindow.Update
      GlobalRequest = InsertRecord
      Update_Jobs
      ThisWindow.Reset
      glo:insert_global = ''
      
      !Inserting (DBH 14/07/2006) # 7975 - What reports are required to be printed?
      !copied here JC - 04/03/13    TB13014
      If GlobalResponse = 1
          ! Job has booked correctly
          Loop x# = 1 To Records(glo:Queue)
              Get(glo:Queue,x#)
              Case glo:Pointer
              Of 'THERMAL LABEL'
                  glo:Select1 = job:Ref_Number
                  If job:Bouncer = 'B'
                      Thermal_Labels('SE')
                  Else ! If job:Bouncer = 'B'
                      If ExchangeAccount(job:Account_Number)
                          Thermal_Labels('ER')
                      Else ! If ExchangeAccount(job:Account_Number)
                          Thermal_Labels('')
                      End ! If ExchangeAccount(job:Account_Number)
                  End ! If job:Bouncer = 'B'
                  glo:Select1 = ''
              Of 'JOB CARD'
                  glo:Select1 = job:Ref_Number
                  Job_Card
                  glo:Select1 = ''
              Of 'RETAINED ACCESSORIES LABEL'
                  glo:Select1 = job:Ref_Number
                  Job_Retained_Accessories_Label
                  glo:Select1 = ''
              Of 'RECEIPT'
                  glo:Select1 = job:Ref_Number
                  Job_Receipt
                  glo:Select1 = ''
              End ! Case glo:Pointer
          End ! Loop x# = 1 To Records(glo:Queue)
      
      End ! If GlobalReponse = 1
      
      
      BRW9.resetsort(1)
    OF ?ButtonExit
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?ButtonExport
      ThisWindow.Update
      PreJobReport
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  if AllRegions = 'N'
      if CurrentRecion = ''
          return(record:filtered)
      ELSE
          if CurrentRecion <> PRE:Region then return(Record:filtered).
      END
  END !if all regions = 'N'
  
  !only want to see prejobs that are not turned into jobs
  if PRE:JobRef_number <> 0 then return(Record:filtered).
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  RETURN ReturnValue

